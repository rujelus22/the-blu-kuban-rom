.class public Lcom/android/launcher2/Launcher;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/launcher2/go;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static Y:Z

.field private static aD:Ljava/util/ArrayList;

.field private static af:Lcom/android/launcher2/fr;

.field private static ag:Ljava/util/HashMap;

.field private static ap:[Landroid/graphics/drawable/Drawable$ConstantState;

.field private static aq:[Landroid/graphics/drawable/Drawable$ConstantState;

.field private static ar:[Landroid/graphics/drawable/Drawable$ConstantState;

.field static final d:Ljava/util/ArrayList;

.field private static final w:Ljava/lang/Object;

.field private static x:I

.field private static y:I


# instance fields
.field private final A:Landroid/database/ContentObserver;

.field private B:Lcom/android/launcher2/Workspace;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private E:Lcom/android/launcher2/DragLayer;

.field private F:Lcom/android/launcher2/bk;

.field private G:Landroid/appwidget/AppWidgetManager;

.field private H:Lcom/android/launcher2/fx;

.field private I:Lcom/android/launcher2/di;

.field private J:Landroid/appwidget/AppWidgetProviderInfo;

.field private K:[I

.field private L:Lcom/android/launcher2/cu;

.field private M:Lcom/android/launcher2/Hotseat;

.field private N:Landroid/view/View;

.field private O:Lcom/android/launcher2/SearchDropTargetBar;

.field private P:Z

.field private Q:Landroid/os/Bundle;

.field private R:Lcom/android/launcher2/fv;

.field private S:Landroid/text/SpannableStringBuilder;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Z:Landroid/os/Bundle;

.field public a:Landroid/view/LayoutInflater;

.field private aA:Lcom/android/launcher2/BubbleTextView;

.field private aB:Lcom/anddoes/launcher/s;

.field private aC:Ljava/lang/Runnable;

.field private final aE:Landroid/content/BroadcastReceiver;

.field private aF:I

.field private aG:Landroid/view/ViewPropertyAnimator;

.field private aH:Z

.field private aI:Lcom/anddoes/launcher/PreviewPane;

.field private aJ:Lcom/android/launcher2/as;

.field private aK:Landroid/view/View;

.field private aL:Landroid/view/ViewGroup$LayoutParams;

.field private aM:Landroid/view/View;

.field private aN:Landroid/view/ViewGroup$LayoutParams;

.field private aO:Landroid/widget/ListPopupWindow;

.field private aP:Landroid/os/Vibrator;

.field private aQ:Z

.field private aR:Z

.field private aS:I

.field private aT:I

.field private aU:Landroid/app/Dialog;

.field private aV:Z

.field private aW:Z

.field private aX:I

.field private aa:Lcom/android/launcher2/gb;

.field private ab:Lcom/android/launcher2/da;

.field private ac:Z

.field private ad:Z

.field private ae:Z

.field private ah:Landroid/content/Intent;

.field private final ai:I

.field private final aj:I

.field private final ak:I

.field private al:J

.field private am:J

.field private an:Ljava/util/HashMap;

.field private final ao:I

.field private final as:Ljava/util/ArrayList;

.field private at:Landroid/content/SharedPreferences;

.field private au:I

.field private av:Ljava/util/ArrayList;

.field private aw:Landroid/widget/ImageView;

.field private ax:Landroid/graphics/Bitmap;

.field private ay:Landroid/graphics/Canvas;

.field private az:Landroid/graphics/Rect;

.field public b:Lcom/android/launcher2/AppsCustomizeTabHost;

.field public c:Lcom/android/launcher2/AppsCustomizePagedView;

.field public final e:Landroid/os/Handler;

.field public f:Lcom/anddoes/launcher/j;

.field public g:Lcom/android/launcher2/LauncherApplication;

.field public h:Lcom/anddoes/launcher/preference/f;

.field public i:Lcom/anddoes/launcher/c/l;

.field public j:Lcom/anddoes/launcher/ui/am;

.field public k:Z

.field public l:Lcom/anddoes/launcher/preference/a;

.field public m:Lcom/anddoes/launcher/preference/bc;

.field public n:Lcom/anddoes/launcher/preference/bb;

.field public o:Lcom/anddoes/launcher/preference/c;

.field public p:Z

.field public q:Z

.field public r:Lcom/anddoes/launcher/w;

.field public s:I

.field private t:Lcom/android/launcher2/fv;

.field private u:Landroid/animation/AnimatorSet;

.field private v:Landroid/animation/AnimatorSet;

.field private final z:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x2

    .line 266
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->w:Ljava/lang/Object;

    .line 267
    sput v1, Lcom/android/launcher2/Launcher;->x:I

    .line 270
    const/16 v0, 0xa

    sput v0, Lcom/android/launcher2/Launcher;->y:I

    .line 318
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/Launcher;->Y:Z

    .line 328
    const/4 v0, 0x0

    sput-object v0, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    .line 330
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    .line 348
    new-array v0, v1, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/android/launcher2/Launcher;->ap:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 349
    new-array v0, v1, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/android/launcher2/Launcher;->aq:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 350
    new-array v0, v1, [Landroid/graphics/drawable/Drawable$ConstantState;

    sput-object v0, Lcom/android/launcher2/Launcher;->ar:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 354
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/Launcher;->d:Ljava/util/ArrayList;

    .line 381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 380
    sput-object v0, Lcom/android/launcher2/Launcher;->aD:Ljava/util/ArrayList;

    .line 171
    return-void
.end method

.method public constructor <init>()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 171
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 258
    sget-object v0, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    .line 273
    new-instance v0, Lcom/android/launcher2/fn;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/fn;-><init>(Lcom/android/launcher2/Launcher;B)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->z:Landroid/content/BroadcastReceiver;

    .line 274
    new-instance v0, Lcom/android/launcher2/fm;

    invoke-direct {v0, p0}, Lcom/android/launcher2/fm;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->A:Landroid/database/ContentObserver;

    .line 287
    new-instance v0, Lcom/android/launcher2/di;

    invoke-direct {v0}, Lcom/android/launcher2/di;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    .line 290
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->K:[I

    .line 300
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->P:Z

    .line 306
    sget-object v0, Lcom/android/launcher2/fv;->a:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    .line 308
    iput-object v4, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    .line 310
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->T:Z

    .line 312
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->U:Z

    .line 324
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->ac:Z

    .line 325
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->ad:Z

    .line 326
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->ae:Z

    .line 332
    iput-object v4, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    .line 335
    iput v1, p0, Lcom/android/launcher2/Launcher;->ai:I

    .line 336
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/android/launcher2/Launcher;->aj:I

    .line 337
    const/16 v0, 0xfa

    iput v0, p0, Lcom/android/launcher2/Launcher;->ak:I

    .line 339
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/launcher2/Launcher;->am:J

    .line 341
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    .line 345
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/android/launcher2/Launcher;->ao:I

    .line 352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->as:Ljava/util/ArrayList;

    .line 362
    iput v3, p0, Lcom/android/launcher2/Launcher;->au:I

    .line 363
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    .line 367
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->az:Landroid/graphics/Rect;

    .line 372
    new-instance v0, Lcom/android/launcher2/dj;

    invoke-direct {v0, p0}, Lcom/android/launcher2/dj;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aC:Ljava/lang/Runnable;

    .line 1507
    new-instance v0, Lcom/android/launcher2/dv;

    invoke-direct {v0, p0}, Lcom/android/launcher2/dv;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aE:Landroid/content/BroadcastReceiver;

    .line 1596
    new-instance v0, Lcom/android/launcher2/eg;

    invoke-direct {v0, p0}, Lcom/android/launcher2/eg;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    .line 4479
    iput v3, p0, Lcom/android/launcher2/Launcher;->aF:I

    .line 4481
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->aH:Z

    .line 4494
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->k:Z

    .line 4502
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    .line 4503
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->aR:Z

    .line 4504
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->p:Z

    .line 4507
    iput-object v4, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    .line 4508
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->q:Z

    .line 4513
    iput v3, p0, Lcom/android/launcher2/Launcher;->aX:I

    .line 171
    return-void
.end method

.method private P()V
    .registers 10

    .prologue
    const v7, 0x7f0d0051

    const v6, 0x7f0d003d

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 511
    .line 514
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->X()I

    move-result v3

    .line 515
    sget-object v1, Lcom/android/launcher2/Launcher;->ap:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    if-eqz v1, :cond_1e

    sget-object v1, Lcom/android/launcher2/Launcher;->aq:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    if-eqz v1, :cond_1e

    .line 516
    sget-object v1, Lcom/android/launcher2/Launcher;->ar:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    if-nez v1, :cond_6d

    .line 517
    :cond_1e
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Z()V

    .line 518
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Y()Z

    move-result v1

    .line 519
    invoke-direct {p0, v1}, Lcom/android/launcher2/Launcher;->h(Z)Z

    move-result v0

    move v8, v0

    move v0, v1

    move v1, v8

    .line 521
    :goto_2c
    sget-object v4, Lcom/android/launcher2/Launcher;->ap:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v4, v4, v3

    if-eqz v4, :cond_4a

    .line 522
    sget-object v0, Lcom/android/launcher2/Launcher;->ap:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v4, v0, v3

    const v0, 0x7f0d0052

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v7}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v7, v4}, Lcom/android/launcher2/Launcher;->a(ILandroid/graphics/drawable/Drawable$ConstantState;)V

    invoke-static {v5, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    move v0, v2

    .line 525
    :cond_4a
    sget-object v4, Lcom/android/launcher2/Launcher;->aq:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v4, v4, v3

    if-eqz v4, :cond_6b

    .line 526
    sget-object v1, Lcom/android/launcher2/Launcher;->aq:[Landroid/graphics/drawable/Drawable$ConstantState;

    aget-object v1, v1, v3

    const v3, 0x7f0d0053

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v6}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v6, v1}, Lcom/android/launcher2/Launcher;->a(ILandroid/graphics/drawable/Drawable$ConstantState;)V

    invoke-static {v3, v4}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 530
    :goto_65
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v1, v0, v2}, Lcom/android/launcher2/SearchDropTargetBar;->a(ZZ)V

    .line 531
    return-void

    :cond_6b
    move v2, v1

    goto :goto_65

    :cond_6d
    move v1, v0

    goto :goto_2c
.end method

.method private Q()V
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 534
    sget-object v1, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    if-nez v1, :cond_10

    .line 535
    new-instance v1, Lcom/android/launcher2/fd;

    invoke-direct {v1, p0}, Lcom/android/launcher2/fd;-><init>(Lcom/android/launcher2/Launcher;)V

    new-array v0, v0, [Ljava/lang/Void;

    .line 548
    invoke-virtual {v1, v0}, Lcom/android/launcher2/fd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 580
    :cond_f
    :goto_f
    return-void

    .line 552
    :cond_10
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 554
    sget-object v2, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    iget-object v2, v2, Lcom/android/launcher2/fr;->a:Ljava/lang/String;

    .line 555
    iget-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    .line 557
    sget-object v4, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    iget v4, v4, Lcom/android/launcher2/fr;->b:I

    .line 558
    iget v5, v1, Landroid/content/res/Configuration;->mcc:I

    .line 560
    sget-object v6, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    iget v6, v6, Lcom/android/launcher2/fr;->c:I

    .line 561
    iget v1, v1, Landroid/content/res/Configuration;->mnc:I

    .line 563
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_58

    if-ne v5, v4, :cond_58

    if-ne v1, v6, :cond_58

    .line 565
    :goto_38
    if-eqz v0, :cond_f

    .line 566
    sget-object v0, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    iput-object v3, v0, Lcom/android/launcher2/fr;->a:Ljava/lang/String;

    .line 567
    sget-object v0, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    iput v5, v0, Lcom/android/launcher2/fr;->b:I

    .line 568
    sget-object v0, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    iput v1, v0, Lcom/android/launcher2/fr;->c:I

    .line 570
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    invoke-virtual {v0}, Lcom/android/launcher2/da;->b()V

    .line 572
    sget-object v0, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    .line 573
    new-instance v1, Lcom/android/launcher2/fi;

    const-string v2, "WriteLocaleConfiguration"

    invoke-direct {v1, p0, v2, v0}, Lcom/android/launcher2/fi;-><init>(Lcom/android/launcher2/Launcher;Ljava/lang/String;Lcom/android/launcher2/fr;)V

    .line 578
    invoke-virtual {v1}, Lcom/android/launcher2/fi;->start()V

    goto :goto_f

    .line 563
    :cond_58
    const/4 v0, 0x1

    goto :goto_38
.end method

.method private R()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1118
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    .line 1120
    const v0, 0x7f0d0035

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    .line 1121
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    const v3, 0x7f0d0036

    invoke-virtual {v0, v3}, Lcom/android/launcher2/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Workspace;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 1122
    const v0, 0x7f0d0037

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    .line 1123
    const v0, 0x7f0d0038

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    .line 1126
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0, p0, v2}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/bk;)V

    .line 1129
    const v0, 0x7f0d003a

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Hotseat;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    .line 1130
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_4a

    .line 1131
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Hotseat;->setup(Lcom/android/launcher2/Launcher;)V

    .line 1135
    :cond_4a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setHapticFeedbackEnabled(Z)V

    .line 1136
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/Workspace;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1138
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->a()I

    move-result v3

    .line 1139
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->removeAllViews()V

    move v0, v1

    .line 1140
    :goto_62
    if-lt v0, v3, :cond_10f

    .line 1143
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Workspace;->setup(Lcom/android/launcher2/bk;)V

    .line 1144
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bl;)V

    .line 1147
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    const v1, 0x7f0d003b

    invoke-virtual {v0, v1}, Lcom/android/launcher2/DragLayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/SearchDropTargetBar;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    .line 1148
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    if-eqz v0, :cond_a8

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-nez v0, :cond_a8

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_a8

    .line 1149
    const-string v0, "ICE_CREAM_SANDWICH"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 1150
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    .line 1151
    const v1, 0x7f0d004e

    invoke-virtual {v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1150
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1152
    if-eqz v0, :cond_a8

    .line 1153
    const v1, 0x7f020079

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 1158
    :cond_a8
    const v0, 0x7f0d003e

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1157
    check-cast v0, Lcom/android/launcher2/AppsCustomizeTabHost;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 1160
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    const v1, 0x7f0d0010

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1159
    check-cast v0, Lcom/android/launcher2/AppsCustomizePagedView;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    .line 1161
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setup(Lcom/android/launcher2/Launcher;)V

    .line 1162
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p0, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/bk;)V

    .line 1165
    const v0, 0x7f0d0055

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->N:Landroid/view/View;

    .line 1166
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->N:Landroid/view/View;

    if-eqz v0, :cond_e1

    .line 1167
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->N:Landroid/view/View;

    new-instance v1, Lcom/android/launcher2/dk;

    invoke-direct {v1, p0}, Lcom/android/launcher2/dk;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1177
    :cond_e1
    const v0, 0x7f0d003f

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/PreviewPane;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aI:Lcom/anddoes/launcher/PreviewPane;

    .line 1179
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bs;)V

    .line 1180
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->b(Landroid/view/View;)V

    .line 1181
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->a(Landroid/view/View;)V

    .line 1182
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bx;)V

    .line 1183
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aI:Lcom/anddoes/launcher/PreviewPane;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bx;)V

    .line 1184
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    if-eqz v0, :cond_10e

    .line 1185
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0, p0, v2}, Lcom/android/launcher2/SearchDropTargetBar;->a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/bk;)V

    .line 1187
    :cond_10e
    return-void

    .line 1141
    :cond_10f
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->N()V

    .line 1140
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_62
.end method

.method private S()V
    .registers 12

    .prologue
    const-wide/16 v3, 0x4e20

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1579
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->ad:Z

    if-eqz v0, :cond_2a

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->ac:Z

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    move v0, v1

    .line 1580
    :goto_15
    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->P:Z

    if-eq v0, v5, :cond_29

    .line 1581
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->P:Z

    .line 1582
    if-eqz v0, :cond_2f

    .line 1583
    iget-wide v0, p0, Lcom/android/launcher2/Launcher;->am:J

    const-wide/16 v5, -0x1

    cmp-long v0, v0, v5

    if-nez v0, :cond_2c

    move-wide v0, v3

    .line 1584
    :goto_26
    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(J)V

    .line 1594
    :cond_29
    :goto_29
    return-void

    :cond_2a
    move v0, v2

    .line 1579
    goto :goto_15

    .line 1583
    :cond_2c
    iget-wide v0, p0, Lcom/android/launcher2/Launcher;->am:J

    goto :goto_26

    .line 1586
    :cond_2f
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_47

    .line 1587
    const-wide/16 v5, 0x0

    .line 1588
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/android/launcher2/Launcher;->al:J

    sub-long/2addr v7, v9

    sub-long/2addr v3, v7

    .line 1587
    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/android/launcher2/Launcher;->am:J

    .line 1590
    :cond_47
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1591
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_29
.end method

.method private T()Z
    .registers 2

    .prologue
    .line 1977
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->T:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->W:Z

    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x1

    goto :goto_9
.end method

.method private U()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 1981
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/android/launcher2/di;->j:J

    .line 1982
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput v3, v0, Lcom/android/launcher2/di;->k:I

    .line 1983
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput v3, v1, Lcom/android/launcher2/di;->m:I

    iput v3, v0, Lcom/android/launcher2/di;->l:I

    .line 1984
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput v3, v1, Lcom/android/launcher2/di;->o:I

    iput v3, v0, Lcom/android/launcher2/di;->n:I

    .line 1985
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput v3, v1, Lcom/android/launcher2/di;->q:I

    iput v3, v0, Lcom/android/launcher2/di;->p:I

    .line 1986
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/di;->s:[I

    .line 1987
    return-void
.end method

.method private V()V
    .registers 3

    .prologue
    .line 2257
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2258
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-nez v0, :cond_c

    .line 2276
    :goto_b
    return-void

    .line 2261
    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_WALLPAPER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2262
    const v1, 0x7f070276

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 2275
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    goto :goto_b
.end method

.method private W()V
    .registers 3

    .prologue
    .line 3483
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aR:Z

    if-eqz v0, :cond_17

    .line 3484
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aR:Z

    .line 3485
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    if-nez v0, :cond_17

    .line 3486
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 3487
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    .line 3490
    :cond_17
    return-void
.end method

.method private X()I
    .registers 2

    .prologue
    .line 3751
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_12

    .line 3755
    const/4 v0, 0x0

    :goto_e
    return v0

    .line 3753
    :pswitch_f
    const/4 v0, 0x1

    goto :goto_e

    .line 3751
    nop

    :pswitch_data_12
    .packed-switch 0x2
        :pswitch_f
    .end packed-switch
.end method

.method private Y()Z
    .registers 11

    .prologue
    const v9, 0x7f0d0051

    const/4 v2, 0x0

    const/16 v8, 0x8

    .line 3821
    const v0, 0x7f0d0052

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3822
    invoke-virtual {p0, v9}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3823
    const v1, 0x7f0d0054

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 3824
    const v1, 0x7f0d0053

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 3825
    const v1, 0x7f0d003d

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 3827
    const-string v1, "search"

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 3828
    invoke-virtual {v1}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v7

    .line 3829
    if-eqz v7, :cond_75

    .line 3830
    const-string v1, "ICE_CREAM_SANDWICH"

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->s:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 3831
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->X()I

    move-result v5

    .line 3833
    if-nez v5, :cond_6b

    .line 3834
    if-eqz v1, :cond_67

    const v1, 0x7f02002d

    .line 3838
    :goto_4b
    sget-object v6, Lcom/android/launcher2/Launcher;->ap:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 3839
    const-string v8, ""

    .line 3838
    invoke-direct {p0, v9, v7, v1, v8}, Lcom/android/launcher2/Launcher;->a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    aput-object v1, v6, v5

    .line 3840
    if-eqz v4, :cond_5a

    .line 3841
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3842
    :cond_5a
    if-eqz v3, :cond_5f

    .line 3843
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3844
    :cond_5f
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3845
    invoke-static {v3, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 3846
    const/4 v0, 0x1

    .line 3858
    :goto_66
    return v0

    .line 3834
    :cond_67
    const v1, 0x7f02002e

    goto :goto_4b

    .line 3836
    :cond_6b
    if-eqz v1, :cond_71

    const v1, 0x7f02002f

    goto :goto_4b

    :cond_71
    const v1, 0x7f020030

    goto :goto_4b

    .line 3850
    :cond_75
    if-eqz v4, :cond_7a

    .line 3851
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3852
    :cond_7a
    if-eqz v3, :cond_7f

    .line 3853
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3854
    :cond_7f
    if-eqz v5, :cond_84

    .line 3855
    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3856
    :cond_84
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3857
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    move v0, v2

    .line 3858
    goto :goto_66
.end method

.method private Z()V
    .registers 3

    .prologue
    .line 3914
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.category.APP_MARKET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 3917
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    .line 3918
    if-eqz v1, :cond_1a

    .line 3920
    iput-object v0, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    .line 3932
    :goto_19
    return-void

    .line 3926
    :cond_1a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    goto :goto_19
.end method

.method private a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3788
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3789
    invoke-direct {p0, p2, p4}, Lcom/android/launcher2/Launcher;->a(Landroid/content/ComponentName;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 3791
    if-eqz v0, :cond_11

    .line 3794
    if-nez v1, :cond_18

    .line 3795
    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3801
    :cond_11
    :goto_11
    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    :goto_17
    return-object v0

    .line 3797
    :cond_18
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_11

    .line 3801
    :cond_1c
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private a(Landroid/content/ComponentName;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 3761
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 3764
    const/16 v1, 0x80

    .line 3763
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 3764
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 3765
    if-eqz v1, :cond_3c

    .line 3766
    invoke-virtual {v1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 3767
    if-eqz v1, :cond_3c

    .line 3768
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v0

    .line 3769
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_1b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_1b} :catch_1d
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_1b} :catch_3e

    move-result-object v0

    .line 3781
    :goto_1c
    return-object v0

    .line 3772
    :catch_1d
    move-exception v0

    .line 3774
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load toolbar icon; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 3775
    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3774
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3781
    :cond_3c
    :goto_3c
    const/4 v0, 0x0

    goto :goto_1c

    .line 3776
    :catch_3e
    move-exception v0

    .line 3778
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load toolbar icon from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3c
.end method

.method public static a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 5732
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 5735
    const/16 v1, 0x80

    .line 5734
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 5735
    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 5736
    if-eqz v1, :cond_3e

    .line 5737
    const-string v2, "com.android.launcher.toolbar_icon"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 5738
    if-eqz v1, :cond_3e

    .line 5739
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v0

    .line 5740
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_1d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_1d} :catch_1f
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_1d} :catch_40

    move-result-object v0

    .line 5753
    :goto_1e
    return-object v0

    .line 5743
    :catch_1f
    move-exception v0

    .line 5745
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load toolbar icon; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5746
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 5747
    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5745
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5753
    :cond_3e
    :goto_3e
    const/4 v0, 0x0

    goto :goto_1e

    .line 5748
    :catch_40
    move-exception v0

    .line 5750
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load toolbar icon from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 5751
    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5750
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3e
.end method

.method private a(Lcom/android/launcher2/jd;)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 1197
    .line 1198
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1197
    invoke-virtual {p0, v0, p1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/ViewGroup;Lcom/android/launcher2/jd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;
    .registers 2
    .parameter

    .prologue
    .line 278
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    return-object v0
.end method

.method static a(I)V
    .registers 3
    .parameter

    .prologue
    .line 651
    sget-object v1, Lcom/android/launcher2/Launcher;->w:Ljava/lang/Object;

    monitor-enter v1

    .line 652
    :try_start_3
    sput p0, Lcom/android/launcher2/Launcher;->x:I

    .line 651
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_7

    return-void

    :catchall_7
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(IILjava/lang/String;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 792
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-wide v0, v0, Lcom/android/launcher2/di;->j:J

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v2, v2, Lcom/android/launcher2/di;->k:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v8

    .line 795
    const/4 v5, 0x0

    .line 798
    const/4 v0, -0x1

    if-ne p1, v0, :cond_5c

    .line 799
    const/4 v7, 0x3

    .line 801
    const-string v0, "search_widget"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-static {p0}, Lcom/anddoes/launcher/au;->a(Landroid/content/Context;)Lcom/anddoes/launcher/au;

    move-result-object v0

    :goto_1e
    if-eqz v0, :cond_70

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    iget v0, v0, Lcom/anddoes/launcher/i;->c:I

    invoke-virtual {v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 802
    :goto_28
    if-nez v0, :cond_5a

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    .line 803
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    .line 802
    invoke-virtual {v1, p0, p2, v2}, Lcom/android/launcher2/fx;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v4

    .line 804
    :goto_32
    if-nez v0, :cond_6e

    move-object v6, v4

    .line 807
    :goto_35
    new-instance v0, Lcom/android/launcher2/fj;

    move-object v1, p0

    move-object v2, p3

    move v3, p2

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/fj;-><init>(Lcom/android/launcher2/Launcher;Ljava/lang/String;ILandroid/appwidget/AppWidgetHostView;I)V

    move v5, v7

    move-object v4, v0

    .line 832
    :goto_40
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->getAnimatedView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_68

    .line 833
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    .line 834
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v2}, Lcom/android/launcher2/DragLayer;->getAnimatedView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/bu;

    .line 835
    const/4 v7, 0x1

    move-object v2, v8

    .line 833
    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/di;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/bu;Ljava/lang/Runnable;ILandroid/view/View;Z)V

    .line 840
    :goto_59
    return-void

    :cond_5a
    move-object v4, v6

    .line 803
    goto :goto_32

    .line 821
    :cond_5c
    if-nez p1, :cond_6c

    .line 822
    const/4 v5, 0x4

    .line 823
    new-instance v4, Lcom/android/launcher2/fk;

    invoke-direct {v4, p0, p1}, Lcom/android/launcher2/fk;-><init>(Lcom/android/launcher2/Launcher;I)V

    .line 830
    invoke-direct {p0, p2}, Lcom/android/launcher2/Launcher;->f(I)V

    goto :goto_40

    .line 838
    :cond_68
    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    goto :goto_59

    :cond_6c
    move-object v4, v6

    goto :goto_40

    :cond_6e
    move-object v6, v0

    goto :goto_35

    :cond_70
    move-object v0, v6

    goto :goto_28

    :cond_72
    move-object v0, v6

    goto :goto_1e
.end method

.method private a(IJILandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V
    .registers 31
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1342
    if-nez p6, :cond_c

    .line 1343
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->G:Landroid/appwidget/AppWidgetManager;

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object p6

    .line 1347
    :cond_c
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v4

    .line 1349
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v8

    .line 1350
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v14

    .line 1353
    const/4 v5, 0x0

    aget v5, v8, v5

    if-nez v5, :cond_31

    .line 1354
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, v14, v6

    aput v6, v8, v5

    .line 1356
    :cond_31
    const/4 v5, 0x1

    aget v5, v8, v5

    if-nez v5, :cond_3c

    .line 1357
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget v6, v14, v6

    aput v6, v8, v5

    .line 1363
    :cond_3c
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Launcher;->K:[I

    .line 1364
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v6, v5, Lcom/android/launcher2/di;->s:[I

    .line 1365
    const/4 v5, 0x2

    new-array v12, v5, [I

    .line 1366
    const/4 v5, 0x0

    .line 1367
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->l:I

    if-ltz v7, :cond_a6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->m:I

    if-ltz v7, :cond_a6

    .line 1368
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->n:I

    const/4 v9, 0x0

    aget v9, v14, v9

    if-lt v7, v9, :cond_a6

    .line 1369
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->o:I

    const/4 v9, 0x1

    aget v9, v14, v9

    if-lt v7, v9, :cond_a6

    .line 1370
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->l:I

    aput v6, v11, v5

    .line 1371
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->m:I

    aput v6, v11, v5

    .line 1372
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->n:I

    aput v6, v14, v5

    .line 1373
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->o:I

    aput v6, v14, v5

    .line 1374
    const/4 v5, 0x1

    .line 1396
    :cond_95
    :goto_95
    if-nez v5, :cond_114

    .line 1397
    invoke-direct/range {p0 .. p1}, Lcom/android/launcher2/Launcher;->f(I)V

    .line 1398
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->a(Z)V

    .line 1432
    :goto_a5
    return-void

    .line 1375
    :cond_a6
    if-eqz v6, :cond_df

    if-eqz v4, :cond_df

    .line 1378
    const/4 v5, 0x0

    aget v5, v6, v5

    const/4 v7, 0x1

    aget v6, v6, v7

    const/4 v7, 0x0

    aget v7, v8, v7

    const/4 v9, 0x1

    aget v8, v8, v9

    const/4 v9, 0x0

    aget v9, v14, v9

    .line 1379
    const/4 v10, 0x1

    aget v10, v14, v10

    .line 1377
    invoke-virtual/range {v4 .. v12}, Lcom/android/launcher2/CellLayout;->a(IIIIII[I[I)[I

    move-result-object v6

    .line 1380
    const/4 v5, 0x0

    const/4 v7, 0x0

    aget v7, v12, v7

    aput v7, v14, v5

    .line 1381
    const/4 v5, 0x1

    const/4 v7, 0x1

    aget v7, v12, v7

    aput v7, v14, v5

    .line 1382
    if-eqz v6, :cond_dd

    const/4 v5, 0x1

    .line 1383
    :goto_cf
    const/4 v7, 0x0

    aget v7, v6, v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_db

    const/4 v7, 0x1

    aget v6, v6, v7

    const/4 v7, -0x1

    if-ne v6, v7, :cond_95

    .line 1384
    :cond_db
    const/4 v5, 0x0

    goto :goto_95

    .line 1382
    :cond_dd
    const/4 v5, 0x0

    goto :goto_cf

    .line 1386
    :cond_df
    if-eqz v4, :cond_95

    .line 1387
    const/4 v5, 0x0

    aget v5, v8, v5

    const/4 v6, 0x1

    aget v6, v8, v6

    invoke-virtual {v4, v11, v5, v6}, Lcom/android/launcher2/CellLayout;->a([III)Z

    move-result v5

    .line 1388
    const/4 v6, 0x0

    aget v6, v14, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->n:I

    if-le v6, v7, :cond_ff

    .line 1389
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->n:I

    aput v7, v14, v6

    .line 1391
    :cond_ff
    const/4 v6, 0x1

    aget v6, v14, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->o:I

    if-le v6, v7, :cond_95

    .line 1392
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v7, v7, Lcom/android/launcher2/di;->o:I

    aput v7, v14, v6

    goto :goto_95

    .line 1403
    :cond_114
    new-instance v13, Lcom/android/launcher2/fz;

    .line 1404
    move-object/from16 v0, p6

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 1403
    move/from16 v0, p1

    invoke-direct {v13, v0, v4}, Lcom/android/launcher2/fz;-><init>(ILandroid/content/ComponentName;)V

    .line 1405
    const/4 v4, 0x0

    aget v4, v14, v4

    iput v4, v13, Lcom/android/launcher2/fz;->n:I

    .line 1406
    const/4 v4, 0x1

    aget v4, v14, v4

    iput v4, v13, Lcom/android/launcher2/fz;->o:I

    .line 1407
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v4, v4, Lcom/android/launcher2/di;->p:I

    iput v4, v13, Lcom/android/launcher2/fz;->p:I

    .line 1408
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v4, v4, Lcom/android/launcher2/di;->q:I

    iput v4, v13, Lcom/android/launcher2/fz;->q:I

    .line 1411
    const/4 v4, 0x0

    aget v17, v11, v4

    const/4 v4, 0x1

    aget v18, v11, v4

    const/16 v19, 0x0

    move-object/from16 v12, p0

    move-wide/from16 v14, p2

    move/from16 v16, p4

    .line 1410
    invoke-static/range {v12 .. v19}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V

    .line 1413
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher2/Launcher;->V:Z

    if-nez v4, :cond_1a3

    .line 1414
    if-nez p5, :cond_1a8

    .line 1416
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p6

    invoke-virtual {v4, v0, v1, v2}, Lcom/android/launcher2/fx;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v4

    iput-object v4, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    .line 1417
    iget-object v4, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    move/from16 v0, p1

    move-object/from16 v1, p6

    invoke-virtual {v4, v0, v1}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    .line 1423
    :goto_16b
    iget-object v4, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v4, v13}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 1424
    iget-object v4, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/appwidget/AppWidgetHostView;->setVisibility(I)V

    .line 1425
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/android/launcher2/fz;->a(Lcom/android/launcher2/Launcher;)V

    .line 1426
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v15, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    const/4 v4, 0x0

    aget v19, v11, v4

    const/4 v4, 0x1

    aget v20, v11, v4

    .line 1427
    iget v0, v13, Lcom/android/launcher2/fz;->n:I

    move/from16 v21, v0

    iget v0, v13, Lcom/android/launcher2/fz;->o:I

    move/from16 v22, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v23

    move-wide/from16 v16, p2

    move/from16 v18, p4

    .line 1426
    invoke-virtual/range {v14 .. v23}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 1429
    iget-object v4, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-direct {v0, v4, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 1431
    :cond_1a3
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->U()V

    goto/16 :goto_a5

    .line 1420
    :cond_1a8
    move-object/from16 v0, p5

    iput-object v0, v13, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    goto :goto_16b
.end method

.method private a(ILandroid/graphics/drawable/Drawable$ConstantState;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3806
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 3807
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3808
    return-void
.end method

.method private a(ILcom/android/launcher2/di;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2003
    iget-object v0, p4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v0, :cond_1c

    .line 2004
    iput-object p4, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    .line 2007
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.appwidget.action.APPWIDGET_CONFIGURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2008
    iget-object v1, p4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2009
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2010
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    .line 2018
    :goto_1b
    return-void

    .line 2013
    :cond_1c
    iget-wide v2, p2, Lcom/android/launcher2/di;->j:J

    iget v4, p2, Lcom/android/launcher2/di;->k:I

    move-object v0, p0

    move v1, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/Launcher;->a(IJILandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 2016
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ZZ)V

    goto :goto_1b
.end method

.method private a(J)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1572
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1573
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1574
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1575
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher2/Launcher;->al:J

    .line 1576
    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/android/launcher2/fr;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 588
    const/4 v1, 0x0

    :try_start_1
    new-instance v0, Ljava/io/DataInputStream;

    const-string v2, "launcher.preferences"

    invoke-virtual {p0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_36
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_c} :catch_22
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_2c

    :try_start_c
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/android/launcher2/fr;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iput v1, p1, Lcom/android/launcher2/fr;->b:I

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    iput v1, p1, Lcom/android/launcher2/fr;->c:I
    :try_end_1e
    .catchall {:try_start_c .. :try_end_1e} :catchall_41
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_1e} :catch_48
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_1e} :catch_46

    :try_start_1e
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_21} :catch_3f

    :cond_21
    :goto_21
    return-void

    :catch_22
    move-exception v0

    move-object v0, v1

    :goto_24
    if-eqz v0, :cond_21

    :try_start_26
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_29} :catch_2a

    goto :goto_21

    :catch_2a
    move-exception v0

    goto :goto_21

    :catch_2c
    move-exception v0

    move-object v0, v1

    :goto_2e
    if-eqz v0, :cond_21

    :try_start_30
    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_33} :catch_34

    goto :goto_21

    :catch_34
    move-exception v0

    goto :goto_21

    :catchall_36
    move-exception v0

    :goto_37
    if-eqz v1, :cond_3c

    :try_start_39
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_39 .. :try_end_3c} :catch_3d

    :cond_3c
    :goto_3c
    throw v0

    :catch_3d
    move-exception v1

    goto :goto_3c

    :catch_3f
    move-exception v0

    goto :goto_21

    :catchall_41
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_37

    :catch_46
    move-exception v1

    goto :goto_2e

    :catch_48
    move-exception v1

    goto :goto_24
.end method

.method static synthetic a(Landroid/view/View;F)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2928
    invoke-static {p0, p1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;F)V

    return-void
.end method

.method private a(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1619
    if-eqz p2, :cond_7

    iget v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_8

    .line 1626
    :cond_7
    :goto_7
    return-void

    .line 1620
    :cond_8
    iget v0, p2, Landroid/appwidget/AppWidgetProviderInfo;->autoAdvanceViewId:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1621
    instance-of v1, v0, Landroid/widget/Advanceable;

    if-eqz v1, :cond_7

    .line 1622
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1623
    check-cast v0, Landroid/widget/Advanceable;

    invoke-interface {v0}, Landroid/widget/Advanceable;->fyiWillBeAdvancedByHostKThx()V

    .line 1624
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    goto :goto_7
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3811
    instance-of v0, p0, Lcom/android/launcher2/HolographicLinearLayout;

    if-eqz v0, :cond_a

    .line 3812
    check-cast p0, Lcom/android/launcher2/HolographicLinearLayout;

    .line 3813
    invoke-virtual {p0}, Lcom/android/launcher2/HolographicLinearLayout;->a()V

    .line 3818
    :cond_9
    :goto_9
    return-void

    .line 3814
    :cond_a
    instance-of v0, p1, Lcom/android/launcher2/HolographicImageView;

    if-eqz v0, :cond_9

    .line 3815
    check-cast p1, Lcom/android/launcher2/HolographicImageView;

    .line 3816
    invoke-virtual {p1}, Lcom/android/launcher2/HolographicImageView;->a()V

    goto :goto_9
.end method

.method static synthetic a(Landroid/view/View;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2919
    invoke-static {p0, p1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    return-void
.end method

.method private a(Landroid/view/View;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2914
    instance-of v0, p1, Lcom/android/launcher2/hn;

    if-eqz v0, :cond_9

    .line 2915
    check-cast p1, Lcom/android/launcher2/hn;

    invoke-interface {p1, p0, p2, p3}, Lcom/android/launcher2/hn;->a(Lcom/android/launcher2/Launcher;ZZ)V

    .line 2917
    :cond_9
    return-void
.end method

.method private a(Lcom/android/launcher2/FolderIcon;)V
    .registers 7
    .parameter

    .prologue
    .line 2643
    invoke-virtual {p1}, Lcom/android/launcher2/FolderIcon;->getMeasuredWidth()I

    move-result v1

    .line 2644
    invoke-virtual {p1}, Lcom/android/launcher2/FolderIcon;->getMeasuredHeight()I

    move-result v2

    .line 2647
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    if-nez v0, :cond_13

    .line 2648
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    .line 2650
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ax:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ax:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, v1, :cond_27

    .line 2651
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ax:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, v2, :cond_38

    .line 2652
    :cond_27
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->ax:Landroid/graphics/Bitmap;

    .line 2653
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->ax:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->ay:Landroid/graphics/Canvas;

    .line 2657
    :cond_38
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    if-eqz v0, :cond_b5

    .line 2658
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    .line 2663
    :goto_4a
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->az:Landroid/graphics/Rect;

    invoke-virtual {v3, p1, v4}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2664
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->c:Z

    .line 2665
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->az:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    .line 2666
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->az:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iput v3, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    .line 2667
    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    .line 2668
    iput v2, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    .line 2670
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->ay:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2671
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->ay:Landroid/graphics/Canvas;

    invoke-virtual {p1, v1}, Lcom/android/launcher2/FolderIcon;->draw(Landroid/graphics/Canvas;)V

    .line 2672
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->ax:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2673
    iget-object v1, p1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    if-eqz v1, :cond_92

    .line 2674
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    iget-object v2, p1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v2}, Lcom/android/launcher2/Folder;->getPivotXForIconAnimation()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 2675
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    iget-object v2, p1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v2}, Lcom/android/launcher2/Folder;->getPivotYForIconAnimation()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 2679
    :cond_92
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/DragLayer;->indexOfChild(Landroid/view/View;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_a4

    .line 2680
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    .line 2682
    :cond_a4
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Lcom/android/launcher2/DragLayer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2683
    iget-object v0, p1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    if-eqz v0, :cond_b4

    .line 2684
    iget-object v0, p1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->bringToFront()V

    .line 2686
    :cond_b4
    return-void

    .line 2660
    :cond_b5
    new-instance v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v2}, Lcom/android/launcher2/DragLayer$LayoutParams;-><init>(II)V

    goto :goto_4a
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;IJILandroid/appwidget/AppWidgetHostView;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1340
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/Launcher;->a(IJILandroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;Landroid/app/Dialog;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4507
    iput-object p1, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2934
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;ZZ)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/as;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4488
    iput-object p1, p0, Lcom/android/launcher2/Launcher;->aJ:Lcom/android/launcher2/as;

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/fx;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 285
    iput-object p1, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 324
    iput-boolean p1, p0, Lcom/android/launcher2/Launcher;->ac:Z

    return-void
.end method

.method private a(Lcom/android/launcher2/as;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 5703
    if-nez p1, :cond_4

    .line 5728
    :cond_3
    :goto_3
    return-void

    .line 5707
    :cond_4
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 5709
    if-eqz p1, :cond_51

    iget v0, p1, Lcom/android/launcher2/as;->b:I

    if-ltz v0, :cond_51

    iget v0, p1, Lcom/android/launcher2/as;->c:I

    if-ltz v0, :cond_51

    .line 5710
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, p1, Lcom/android/launcher2/as;->b:I

    iput v1, v0, Lcom/android/launcher2/di;->l:I

    .line 5711
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, p1, Lcom/android/launcher2/as;->c:I

    iput v1, v0, Lcom/android/launcher2/di;->m:I

    .line 5712
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput v4, v1, Lcom/android/launcher2/di;->o:I

    iput v4, v0, Lcom/android/launcher2/di;->n:I

    .line 5713
    iget-wide v0, p1, Lcom/android/launcher2/as;->g:J

    iget v2, p1, Lcom/android/launcher2/as;->f:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 5714
    if-eqz v0, :cond_51

    .line 5715
    iget v1, p1, Lcom/android/launcher2/as;->b:I

    iget v2, p1, Lcom/android/launcher2/as;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/CellLayout;->d(II)Z

    move-result v1

    if-nez v1, :cond_3

    .line 5718
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v1, v1, Lcom/android/launcher2/di;->s:[I

    if-nez v1, :cond_51

    .line 5719
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const/4 v2, 0x2

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/android/launcher2/di;->s:[I

    .line 5720
    iget v1, p1, Lcom/android/launcher2/as;->b:I

    iget v2, p1, Lcom/android/launcher2/as;->c:I

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v3, v3, Lcom/android/launcher2/di;->s:[I

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/CellLayout;->a(II[I)V

    .line 5724
    :cond_51
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const-wide/16 v1, -0x64

    iput-wide v1, v0, Lcom/android/launcher2/di;->j:J

    .line 5725
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v1

    iput v1, v0, Lcom/android/launcher2/di;->k:I

    .line 5726
    iput-boolean v4, p0, Lcom/android/launcher2/Launcher;->W:Z

    .line 5727
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->showDialog(I)V

    goto :goto_3
.end method

.method static a(Lcom/android/launcher2/cu;)V
    .registers 4
    .parameter

    .prologue
    .line 2253
    sget-object v0, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    iget-wide v1, p0, Lcom/android/launcher2/cu;->h:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2254
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/fr;)V
    .registers 1
    .parameter

    .prologue
    .line 328
    sput-object p0, Lcom/android/launcher2/Launcher;->af:Lcom/android/launcher2/fr;

    return-void
.end method

.method private a(Lcom/android/launcher2/fv;ZLjava/lang/Runnable;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3282
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_c

    .line 3283
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 3284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    .line 3286
    :cond_c
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_18

    .line 3287
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 3288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    .line 3290
    :cond_18
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3292
    const v0, 0x7f0a0007

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 3294
    const v0, 0x7f0a000a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    .line 3296
    const v0, 0x7f0a0008

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v7, v0

    .line 3297
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 3298
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 3299
    const/4 v0, 0x0

    .line 3301
    sget-object v6, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne p1, v6, :cond_e2

    .line 3302
    const v0, 0x7f0a000c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 3303
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 3304
    sget-object v6, Lcom/android/launcher2/kg;->a:Lcom/android/launcher2/kg;

    .line 3303
    invoke-virtual {v1, v6, p2, v0}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/kg;ZI)Landroid/animation/Animator;

    move-result-object v0

    move-object v6, v0

    .line 3310
    :goto_4b
    invoke-static {v2}, Lcom/android/launcher2/Launcher;->c(Landroid/view/View;)V

    .line 3311
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3312
    invoke-direct {p0, p2}, Lcom/android/launcher2/Launcher;->f(Z)V

    .line 3313
    if-eqz p2, :cond_f1

    .line 3314
    const-string v0, "FLY"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f1

    .line 3316
    new-instance v8, Lcom/android/launcher2/ho;

    invoke-direct {v8, v2}, Lcom/android/launcher2/ho;-><init>(Landroid/view/View;)V

    .line 3318
    invoke-virtual {v8, v7}, Lcom/android/launcher2/ho;->c(F)Lcom/android/launcher2/ho;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/android/launcher2/ho;->d(F)Lcom/android/launcher2/ho;

    move-result-object v0

    .line 3319
    int-to-long v9, v3

    invoke-virtual {v0, v9, v10}, Lcom/android/launcher2/ho;->setDuration(J)Landroid/animation/Animator;

    move-result-object v0

    .line 3320
    new-instance v1, Lcom/android/launcher2/kj;

    invoke-direct {v1}, Lcom/android/launcher2/kj;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3323
    const-string v0, "alpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_1a6

    invoke-static {v2, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 3324
    int-to-long v9, v5

    invoke-virtual {v0, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 3325
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3326
    new-instance v0, Lcom/android/launcher2/dx;

    invoke-direct {v0, p0, v2, v4}, Lcom/android/launcher2/dx;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 3335
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    .line 3337
    const/4 v0, 0x1

    invoke-direct {p0, v2, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3338
    const/4 v0, 0x1

    invoke-direct {p0, v4, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3340
    iget-object v9, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    new-instance v0, Lcom/android/launcher2/dy;

    move-object v1, p0

    move v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/dy;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLandroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v9, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3356
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v3, 0x0

    aput-object v8, v1, v3

    const/4 v3, 0x1

    aput-object v7, v1, v3

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 3357
    if-eqz v6, :cond_cf

    .line 3358
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3360
    :cond_cf
    invoke-static {v2, p2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    .line 3361
    invoke-static {v4, p2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    .line 3362
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    .line 3363
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    new-instance v2, Lcom/android/launcher2/dz;

    invoke-direct {v2, p0, v0}, Lcom/android/launcher2/dz;-><init>(Lcom/android/launcher2/Launcher;Landroid/animation/Animator;)V

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->post(Ljava/lang/Runnable;)Z

    .line 3454
    :cond_e1
    :goto_e1
    return-void

    .line 3305
    :cond_e2
    sget-object v1, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    if-ne p1, v1, :cond_1a3

    .line 3306
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 3307
    sget-object v1, Lcom/android/launcher2/kg;->b:Lcom/android/launcher2/kg;

    .line 3306
    invoke-virtual {v0, v1, p2}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/kg;Z)Landroid/animation/Animator;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_4b

    .line 3370
    :cond_f1
    if-eqz p2, :cond_131

    .line 3371
    const-string v0, "FADE"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_131

    .line 3372
    const/4 v0, 0x1

    invoke-direct {p0, v2, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3373
    const/4 v0, 0x1

    invoke-direct {p0, v4, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3374
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    .line 3375
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v7, 0x258

    invoke-virtual {v0, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3376
    iget-object v7, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    new-instance v0, Lcom/android/launcher2/ea;

    move-object v1, p0

    move v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ea;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLandroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v7, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3401
    if-eqz v6, :cond_12b

    .line 3402
    invoke-virtual {v6}, Landroid/animation/Animator;->start()V

    .line 3404
    :cond_12b
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_e1

    .line 3405
    :cond_131
    if-eqz p2, :cond_177

    .line 3406
    const-string v0, "SCALE"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_177

    .line 3407
    const/4 v0, 0x1

    invoke-direct {p0, v2, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3408
    const/4 v0, 0x1

    invoke-direct {p0, v4, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3409
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    .line 3410
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v7, 0x190

    invoke-virtual {v0, v7, v8}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3411
    iget-object v7, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    new-instance v0, Lcom/android/launcher2/eb;

    move-object v1, p0

    move v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/eb;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLandroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v7, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3436
    if-eqz v6, :cond_170

    .line 3437
    invoke-virtual {v6}, Landroid/animation/Animator;->start()V

    .line 3439
    :cond_170
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_e1

    .line 3441
    :cond_177
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3442
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 3443
    const/4 v0, 0x1

    invoke-direct {p0, v2, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3444
    invoke-static {v2, p2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    .line 3445
    const/4 v0, 0x1

    invoke-direct {p0, v2, p2, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3446
    const/4 v0, 0x1

    invoke-direct {p0, v4, p2, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3447
    invoke-static {v4, p2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    .line 3448
    const/4 v0, 0x1

    invoke-direct {p0, v4, p2, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3449
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3450
    if-eqz v6, :cond_e1

    .line 3451
    invoke-virtual {v6}, Landroid/animation/Animator;->start()V

    goto/16 :goto_e1

    :cond_1a3
    move-object v6, v0

    goto/16 :goto_4b

    .line 3323
    :array_1a6
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private a(ILandroid/content/ComponentName;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2131
    const/4 v2, -0x1

    if-ne p1, v2, :cond_6

    .line 2159
    :goto_5
    return v0

    .line 2134
    :cond_6
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 2135
    invoke-static {p0, p1, p2}, Lcom/anddoes/launcher/r;->a(Landroid/content/Context;ILandroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_14

    move v0, v1

    .line 2136
    goto :goto_5

    .line 2138
    :cond_14
    iput p1, p0, Lcom/android/launcher2/Launcher;->aF:I

    .line 2139
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.appwidget.action.APPWIDGET_BIND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2140
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2141
    const-string v2, "appWidgetProvider"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2142
    const/16 v2, 0xb

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/Launcher;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_5

    .line 2145
    :cond_2d
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->f:Lcom/anddoes/launcher/j;

    invoke-virtual {v2, p1, p2}, Lcom/anddoes/launcher/j;->a(ILandroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_37

    move v0, v1

    .line 2146
    goto :goto_5

    .line 2148
    :cond_37
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v2, v2, Lcom/anddoes/launcher/preference/f;->bb:Z

    if-eqz v2, :cond_4f

    .line 2149
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->f:Lcom/anddoes/launcher/j;

    invoke-virtual {v2}, Lcom/anddoes/launcher/j;->a()Z

    move-result v2

    if-eqz v2, :cond_4f

    .line 2150
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->f:Lcom/anddoes/launcher/j;

    invoke-virtual {v2, p1, p2}, Lcom/anddoes/launcher/j;->b(ILandroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_4f

    move v0, v1

    .line 2151
    goto :goto_5

    .line 2155
    :cond_4f
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v2}, Lcom/android/launcher2/DragLayer;->b()V

    .line 2156
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->f(I)V

    .line 2157
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2158
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->bb:Z

    if-eqz v1, :cond_6c

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->bm()V

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iput-boolean v0, v1, Lcom/anddoes/launcher/preference/f;->bb:Z

    :cond_6c
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x108009b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070003

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07002b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070007

    new-instance v3, Lcom/android/launcher2/dl;

    invoke-direct {v3, p0}, Lcom/android/launcher2/dl;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070013

    new-instance v3, Lcom/android/launcher2/dm;

    invoke-direct {v3, p0}, Lcom/android/launcher2/dm;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070164

    new-instance v3, Lcom/android/launcher2/dn;

    invoke-direct {v3, p0}, Lcom/android/launcher2/dn;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_5
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;I)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5632
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->e(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/android/launcher2/Launcher;Ljava/lang/String;JI)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1440
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;JI)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/android/launcher2/fu;)Z
    .registers 20
    .parameter

    .prologue
    .line 661
    const/4 v11, 0x0

    .line 662
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/launcher2/fu;->a:I

    packed-switch v2, :pswitch_data_1a6

    :pswitch_8
    move v2, v11

    .line 697
    :goto_9
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 698
    :goto_c
    return v2

    .line 664
    :pswitch_d
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/fu;->b:Landroid/content/Intent;

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/android/launcher2/fu;->c:J

    move-object/from16 v0, p1

    iget v7, v0, Lcom/android/launcher2/fu;->d:I

    .line 665
    move-object/from16 v0, p1

    iget v9, v0, Lcom/android/launcher2/fu;->e:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/android/launcher2/fu;->f:I

    .line 664
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->K:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v4

    if-ltz v9, :cond_79

    if-ltz v10, :cond_79

    const/4 v8, 0x0

    aput v9, v3, v8

    const/4 v8, 0x1

    aput v10, v3, v8

    :cond_35
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v3, v8, v2, v0}, Lcom/android/launcher2/gb;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;)Lcom/android/launcher2/jd;

    move-result-object v3

    if-eqz v3, :cond_8f

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    new-instance v8, Landroid/content/Intent;

    const-string v12, "android.intent.action.MAIN"

    invoke-direct {v8, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v8, v3, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    iget-object v8, v3, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    const-string v12, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v12}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v8, v3, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v8, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v2, v3, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    const/high16 v8, 0x1020

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v2, 0x0

    iput v2, v3, Lcom/android/launcher2/jd;->i:I

    const-wide/16 v12, -0x1

    iput-wide v12, v3, Lcom/android/launcher2/jd;->j:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v8

    invoke-virtual/range {v2 .. v10}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/jd;Lcom/android/launcher2/CellLayout;JIZII)V

    move v2, v11

    goto :goto_9

    :cond_79
    const/4 v8, 0x1

    const/4 v12, 0x1

    invoke-virtual {v4, v3, v8, v12}, Lcom/android/launcher2/CellLayout;->a([III)Z

    move-result v3

    if-nez v3, :cond_35

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Launcher;->a(Z)V

    move v2, v11

    goto/16 :goto_9

    :cond_8f
    const-string v3, "Launcher"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Couldn\'t find ActivityInfo for selected application: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v11

    .line 666
    goto/16 :goto_9

    .line 668
    :pswitch_a6
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/fu;->b:Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Intent;)V

    .line 669
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 672
    :pswitch_b2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/fu;->b:Landroid/content/Intent;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/launcher2/fu;->c:J

    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/launcher2/fu;->d:I

    move/from16 v16, v0

    .line 673
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/launcher2/fu;->e:I

    move-object/from16 v0, p1

    iget v9, v0, Lcom/android/launcher2/fu;->f:I

    .line 672
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/launcher2/Launcher;->K:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v10, v3, Lcom/android/launcher2/di;->s:[I

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v4, v5, v1}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0, v2}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Landroid/content/Intent;)Lcom/android/launcher2/jd;

    move-result-object v17

    if-eqz v17, :cond_11e

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/jd;)Landroid/view/View;

    move-result-object v3

    if-ltz v8, :cond_121

    if-ltz v9, :cond_121

    const/4 v2, 0x0

    aput v8, v7, v2

    const/4 v2, 0x1

    aput v9, v7, v2

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JLcom/android/launcher2/CellLayout;[IFZLcom/android/launcher2/bu;Ljava/lang/Runnable;)Z

    move-result v2

    if-nez v2, :cond_11e

    new-instance v12, Lcom/android/launcher2/bz;

    invoke-direct {v12}, Lcom/android/launcher2/bz;-><init>()V

    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v11, 0x0

    const/4 v13, 0x1

    move-object v9, v6

    move-object v10, v7

    invoke-virtual/range {v8 .. v13}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/CellLayout;[IFLcom/android/launcher2/bz;Z)Z

    move-result v2

    if-eqz v2, :cond_1a4

    .line 674
    :cond_11e
    :goto_11e
    const/4 v2, 0x1

    .line 675
    goto/16 :goto_9

    .line 672
    :cond_121
    if-eqz v10, :cond_140

    const/4 v2, 0x0

    aget v2, v10, v2

    const/4 v8, 0x1

    aget v8, v10, v8

    invoke-virtual {v6, v2, v8, v7}, Lcom/android/launcher2/CellLayout;->b(II[I)[I

    move-result-object v2

    if-eqz v2, :cond_13e

    const/4 v2, 0x1

    :goto_130
    if-nez v2, :cond_147

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Launcher;->a(Z)V

    goto :goto_11e

    :cond_13e
    const/4 v2, 0x0

    goto :goto_130

    :cond_140
    const/4 v2, 0x1

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v2, v8}, Lcom/android/launcher2/CellLayout;->a([III)Z

    move-result v2

    goto :goto_130

    :cond_147
    const/4 v2, 0x0

    aget v13, v7, v2

    const/4 v2, 0x1

    aget v14, v7, v2

    const/4 v15, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, v17

    move-wide v10, v4

    move/from16 v12, v16

    invoke-static/range {v8 .. v15}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/launcher2/Launcher;->V:Z

    if-nez v2, :cond_11e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x0

    aget v12, v7, v6

    const/4 v6, 0x1

    aget v8, v7, v6

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v11

    move/from16 v6, v16

    move v7, v12

    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    goto :goto_11e

    .line 677
    :pswitch_175
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/launcher2/fu;->b:Landroid/content/Intent;

    const-string v3, "appWidgetId"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->G:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v4, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v4

    if-eqz v4, :cond_198

    iget-object v5, v4, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-eqz v5, :cond_198

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v5, v4}, Lcom/android/launcher2/Launcher;->a(ILcom/android/launcher2/di;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 678
    :goto_195
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 677
    :cond_198
    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    const/4 v3, 0x5

    const/4 v4, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Lcom/android/launcher2/Launcher;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_195

    :cond_1a4
    move v2, v14

    goto :goto_130

    .line 662
    :pswitch_data_1a6
    .packed-switch 0x1
        :pswitch_b2
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_d
        :pswitch_a6
        :pswitch_b2
        :pswitch_175
    .end packed-switch
.end method

.method private a(Ljava/lang/String;JI)Z
    .registers 25
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1441
    const/4 v13, 0x0

    .line 1442
    const-string v4, "search_widget"

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1443
    invoke-static/range {p0 .. p0}, Lcom/anddoes/launcher/au;->a(Landroid/content/Context;)Lcom/anddoes/launcher/au;

    move-result-object v13

    .line 1446
    :cond_f
    if-nez v13, :cond_13

    .line 1447
    const/4 v4, 0x0

    .line 1504
    :goto_12
    return v4

    .line 1451
    :cond_13
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v4

    .line 1453
    const/4 v5, 0x2

    new-array v8, v5, [I

    const/4 v5, 0x0

    iget v6, v13, Lcom/anddoes/launcher/i;->n:I

    aput v6, v8, v5

    const/4 v5, 0x1

    iget v6, v13, Lcom/anddoes/launcher/i;->o:I

    aput v6, v8, v5

    .line 1454
    const/4 v5, 0x2

    new-array v14, v5, [I

    const/4 v5, 0x0

    iget v6, v13, Lcom/anddoes/launcher/i;->n:I

    aput v6, v14, v5

    const/4 v5, 0x1

    iget v6, v13, Lcom/anddoes/launcher/i;->o:I

    aput v6, v14, v5

    .line 1459
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/launcher2/Launcher;->K:[I

    .line 1460
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v6, v5, Lcom/android/launcher2/di;->s:[I

    .line 1461
    const/4 v5, 0x2

    new-array v12, v5, [I

    .line 1462
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v5, v5, Lcom/android/launcher2/di;->l:I

    if-ltz v5, :cond_9f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v5, v5, Lcom/android/launcher2/di;->m:I

    if-ltz v5, :cond_9f

    .line 1464
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v5, v5, Lcom/android/launcher2/di;->n:I

    const/4 v7, 0x0

    aget v7, v14, v7

    if-lt v5, v7, :cond_9f

    .line 1465
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v5, v5, Lcom/android/launcher2/di;->o:I

    const/4 v7, 0x1

    aget v7, v14, v7

    if-lt v5, v7, :cond_9f

    .line 1466
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->l:I

    aput v6, v11, v5

    .line 1467
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->m:I

    aput v6, v11, v5

    .line 1468
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->n:I

    aput v6, v14, v5

    .line 1469
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v6, v6, Lcom/android/launcher2/di;->o:I

    aput v6, v14, v5

    .line 1470
    const/4 v5, 0x1

    .line 1486
    :cond_8f
    :goto_8f
    if-nez v5, :cond_e1

    .line 1487
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->a(Z)V

    .line 1488
    const/4 v4, 0x1

    goto/16 :goto_12

    .line 1471
    :cond_9f
    if-eqz v6, :cond_d6

    .line 1474
    const/4 v5, 0x0

    aget v5, v6, v5

    const/4 v7, 0x1

    aget v6, v6, v7

    const/4 v7, 0x0

    aget v7, v8, v7

    const/4 v9, 0x1

    aget v8, v8, v9

    const/4 v9, 0x0

    aget v9, v14, v9

    .line 1475
    const/4 v10, 0x1

    aget v10, v14, v10

    .line 1473
    invoke-virtual/range {v4 .. v12}, Lcom/android/launcher2/CellLayout;->a(IIIIII[I[I)[I

    move-result-object v6

    .line 1476
    const/4 v5, 0x0

    const/4 v7, 0x0

    aget v7, v12, v7

    aput v7, v14, v5

    .line 1477
    const/4 v5, 0x1

    const/4 v7, 0x1

    aget v7, v12, v7

    aput v7, v14, v5

    .line 1478
    if-eqz v6, :cond_d4

    const/4 v5, 0x1

    .line 1479
    :goto_c6
    const/4 v7, 0x0

    aget v7, v6, v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_d2

    const/4 v7, 0x1

    aget v6, v6, v7

    const/4 v7, -0x1

    if-ne v6, v7, :cond_8f

    .line 1480
    :cond_d2
    const/4 v5, 0x0

    goto :goto_8f

    .line 1478
    :cond_d4
    const/4 v5, 0x0

    goto :goto_c6

    .line 1483
    :cond_d6
    const/4 v5, 0x0

    aget v5, v8, v5

    const/4 v6, 0x1

    aget v6, v8, v6

    invoke-virtual {v4, v11, v5, v6}, Lcom/android/launcher2/CellLayout;->a([III)Z

    move-result v5

    goto :goto_8f

    .line 1493
    :cond_e1
    const/4 v4, 0x0

    aget v17, v11, v4

    const/4 v4, 0x1

    aget v18, v11, v4

    const/16 v19, 0x0

    move-object/from16 v12, p0

    move-wide/from16 v14, p2

    move/from16 v16, p4

    .line 1492
    invoke-static/range {v12 .. v19}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V

    .line 1495
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/launcher2/Launcher;->V:Z

    if-nez v4, :cond_11f

    .line 1496
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    iget v5, v13, Lcom/anddoes/launcher/i;->c:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1497
    invoke-virtual {v5, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1499
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v6, 0x0

    aget v9, v11, v6

    const/4 v6, 0x1

    aget v10, v11, v6

    .line 1500
    iget v11, v13, Lcom/anddoes/launcher/i;->n:I

    iget v12, v13, Lcom/anddoes/launcher/i;->o:I

    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v13

    move-wide/from16 v6, p2

    move/from16 v8, p4

    .line 1499
    invoke-virtual/range {v4 .. v13}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 1502
    :cond_11f
    invoke-direct/range {p0 .. p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 1504
    const/4 v4, 0x1

    goto/16 :goto_12
.end method

.method static a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1318
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-static {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Context;Landroid/content/ComponentName;II)[I

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/ComponentName;II)[I
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1309
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1312
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    .line 1313
    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    .line 1314
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/content/res/Resources;II)[I

    move-result-object v0

    return-object v0
.end method

.method private aa()Z
    .registers 4

    .prologue
    .line 4419
    const-string v0, "true"

    .line 4420
    const-string v1, "launcher.force_enable_rotation"

    const-string v2, "false"

    .line 4419
    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 4421
    if-nez v0, :cond_1f

    .line 4422
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_1f

    const/4 v0, 0x0

    :goto_1e
    return v0

    :cond_1f
    const/4 v0, 0x1

    .line 4421
    goto :goto_1e
.end method

.method private ab()V
    .registers 3

    .prologue
    .line 4759
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    if-eqz v0, :cond_13

    .line 4760
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 4761
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aR:Z

    .line 4762
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    .line 4764
    :cond_13
    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 4765
    if-eqz v0, :cond_20

    .line 4766
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->expand()V

    .line 4768
    :cond_20
    return-void
.end method

.method private ac()V
    .registers 3

    .prologue
    .line 4817
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_17

    .line 4818
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->p:Z

    .line 4819
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 4820
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->X:Z

    if-nez v0, :cond_17

    .line 4821
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ad()V

    .line 4824
    :cond_17
    return-void
.end method

.method private ad()V
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 4827
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getPaddingRight()I

    move-result v2

    .line 4828
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getPaddingBottom()I

    move-result v1

    .line 4829
    if-lez v1, :cond_22

    move v1, v2

    .line 4834
    :goto_10
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getPaddingLeft()I

    move-result v3

    .line 4835
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getPaddingTop()I

    move-result v4

    .line 4834
    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/android/launcher2/Workspace;->setPadding(IIII)V

    .line 4836
    return-void

    :cond_22
    move v5, v1

    move v1, v0

    move v0, v5

    .line 4832
    goto :goto_10
.end method

.method private ae()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 4958
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_15

    .line 4959
    sget-object v0, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    invoke-direct {p0, v0, v3, v4}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fv;ZLjava/lang/Runnable;)V

    .line 4963
    :cond_e
    :goto_e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_1f

    .line 4979
    :goto_14
    return-void

    .line 4960
    :cond_15
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_e

    .line 4961
    invoke-virtual {p0, v2, v4}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto :goto_e

    .line 4966
    :cond_1f
    sget-object v0, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    .line 4967
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 4968
    const-string v0, "NONE"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 4969
    invoke-direct {p0, v3}, Lcom/android/launcher2/Launcher;->g(Z)V

    .line 4975
    :goto_35
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    .line 4976
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    .line 4977
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->D()V

    .line 4978
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aI:Lcom/anddoes/launcher/PreviewPane;

    invoke-virtual {v0}, Lcom/anddoes/launcher/PreviewPane;->a()V

    goto :goto_14

    .line 4971
    :cond_48
    invoke-direct {p0, v2}, Lcom/android/launcher2/Launcher;->g(Z)V

    goto :goto_35
.end method

.method private af()V
    .registers 4

    .prologue
    .line 5338
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v0, :cond_3b

    .line 5339
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5340
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5341
    const v1, 0x7f070041

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5342
    const v1, 0x7f070029

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5343
    const v1, 0x7f07000e

    .line 5344
    new-instance v2, Lcom/android/launcher2/eo;

    invoke-direct {v2, p0}, Lcom/android/launcher2/eo;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 5343
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5352
    const v1, 0x7f07000f

    .line 5353
    new-instance v2, Lcom/android/launcher2/ep;

    invoke-direct {v2, p0}, Lcom/android/launcher2/ep;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 5352
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5358
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 5360
    :cond_3b
    return-void
.end method

.method private ag()V
    .registers 4

    .prologue
    .line 5363
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-eqz v0, :cond_41

    .line 5364
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    .line 5365
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->b:Z

    const-string v2, "desktop_locked"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;Z)V

    .line 5366
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5367
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5368
    const v1, 0x7f070042

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5369
    const v1, 0x7f07002a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5370
    const v1, 0x7f070007

    .line 5371
    new-instance v2, Lcom/android/launcher2/eq;

    invoke-direct {v2, p0}, Lcom/android/launcher2/eq;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 5370
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 5375
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 5377
    :cond_41
    return-void
.end method

.method private ah()V
    .registers 4

    .prologue
    .line 5585
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aP:Landroid/os/Vibrator;

    const-wide/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 5586
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 5302
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 5303
    const-wide/16 v0, -0x1

    cmp-long v0, p3, v0

    if-eqz v0, :cond_2d

    .line 5306
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    invoke-virtual {v0, p1, v3}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5307
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    invoke-virtual {v0, p2, v3}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5312
    :goto_18
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3e

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3e

    .line 5313
    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5314
    const/high16 v0, 0x1000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 5318
    :goto_2c
    return-object v2

    .line 5309
    :cond_2d
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0, p1, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5310
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0, p2, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_18

    .line 5316
    :cond_3e
    const-class v0, Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v2, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_2c
.end method

.method static synthetic b(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/DragLayer;
    .registers 2
    .parameter

    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    return-object v0
.end method

.method static synthetic b(Landroid/content/Context;Lcom/android/launcher2/fr;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 610
    const/4 v1, 0x0

    :try_start_1
    new-instance v0, Ljava/io/DataOutputStream;

    const-string v2, "launcher.preferences"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_40
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_d} :catch_23
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_d} :catch_2d

    :try_start_d
    iget-object v1, p1, Lcom/android/launcher2/fr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    iget v1, p1, Lcom/android/launcher2/fr;->b:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v1, p1, Lcom/android/launcher2/fr;->c:I

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1f
    .catchall {:try_start_d .. :try_end_1f} :catchall_4b
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_1f} :catch_52
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_1f} :catch_50

    :try_start_1f
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_22} :catch_49

    :cond_22
    :goto_22
    return-void

    :catch_23
    move-exception v0

    move-object v0, v1

    :goto_25
    if-eqz v0, :cond_22

    :try_start_27
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_2a} :catch_2b

    goto :goto_22

    :catch_2b
    move-exception v0

    goto :goto_22

    :catch_2d
    move-exception v0

    move-object v0, v1

    :goto_2f
    :try_start_2f
    const-string v1, "launcher.preferences"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_38
    .catchall {:try_start_2f .. :try_end_38} :catchall_4b

    if-eqz v0, :cond_22

    :try_start_3a
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V
    :try_end_3d
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_3d} :catch_3e

    goto :goto_22

    :catch_3e
    move-exception v0

    goto :goto_22

    :catchall_40
    move-exception v0

    :goto_41
    if-eqz v1, :cond_46

    :try_start_43
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_46
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_46} :catch_47

    :cond_46
    :goto_46
    throw v0

    :catch_47
    move-exception v1

    goto :goto_46

    :catch_49
    move-exception v0

    goto :goto_22

    :catchall_4b
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_41

    :catch_50
    move-exception v1

    goto :goto_2f

    :catch_52
    move-exception v1

    goto :goto_25
.end method

.method private b(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 2209
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070286

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2210
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070176

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2211
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2213
    if-eqz v0, :cond_4e

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 2214
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2215
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2217
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2218
    const-string v2, "android.intent.extra.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2219
    const-string v0, "android.intent.extra.TITLE"

    const v2, 0x7f070292

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 2220
    const/4 v0, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    .line 2228
    :goto_4d
    return-void

    .line 2221
    :cond_4e
    if-eqz v1, :cond_66

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 2222
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2223
    const-class v1, Lcom/anddoes/launcher/ui/ActPickerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2224
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    goto :goto_4d

    .line 2226
    :cond_66
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    goto :goto_4d
.end method

.method static synthetic b(Landroid/view/View;)V
    .registers 1
    .parameter

    .prologue
    .line 2888
    invoke-static {p0}, Lcom/android/launcher2/Launcher;->c(Landroid/view/View;)V

    return-void
.end method

.method private static b(Landroid/view/View;F)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2929
    instance-of v0, p0, Lcom/android/launcher2/hn;

    if-eqz v0, :cond_9

    .line 2930
    check-cast p0, Lcom/android/launcher2/hn;

    invoke-interface {p0, p1}, Lcom/android/launcher2/hn;->a(F)V

    .line 2932
    :cond_9
    return-void
.end method

.method private static b(Landroid/view/View;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2920
    instance-of v0, p0, Lcom/android/launcher2/hn;

    if-eqz v0, :cond_a

    move-object v0, p0

    .line 2921
    check-cast v0, Lcom/android/launcher2/hn;

    invoke-interface {v0, p1}, Lcom/android/launcher2/hn;->b(Z)V

    .line 2925
    :cond_a
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;F)V

    .line 2926
    return-void
.end method

.method private b(Landroid/view/View;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2935
    instance-of v0, p1, Lcom/android/launcher2/hn;

    if-eqz v0, :cond_a

    move-object v0, p1

    .line 2936
    check-cast v0, Lcom/android/launcher2/hn;

    invoke-interface {v0, p0, p2, p3}, Lcom/android/launcher2/hn;->b(Lcom/android/launcher2/Launcher;ZZ)V

    .line 2940
    :cond_a
    const/high16 v0, 0x3f80

    invoke-static {p1, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;F)V

    .line 2941
    return-void
.end method

.method private b(Lcom/android/launcher2/FolderIcon;)V
    .registers 11
    .parameter

    .prologue
    const/high16 v4, 0x3fc0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2747
    iget-object v0, p1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    .line 2748
    iget-object v1, v0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    .line 2750
    iput-boolean v7, v1, Lcom/android/launcher2/cu;->a:Z

    .line 2754
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_8d

    .line 2755
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/DragLayer;->addView(Landroid/view/View;)V

    .line 2756
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bx;)V

    .line 2761
    :goto_1a
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->g()V

    .line 2762
    if-eqz p1, :cond_8c

    const-string v0, "alpha"

    new-array v1, v7, [F

    const/4 v2, 0x0

    aput v2, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const-string v0, "scaleX"

    new-array v1, v7, [F

    aput v4, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const-string v0, "scaleY"

    new-array v1, v7, [F

    aput v4, v1, v8

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/android/launcher2/FolderIcon;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/cu;

    iget-wide v0, v0, Lcom/android/launcher2/cu;->j:J

    const-wide/16 v5, -0x65

    cmp-long v0, v0, v5

    if-nez v0, :cond_63

    invoke-virtual {p1}, Lcom/android/launcher2/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {p1}, Lcom/android/launcher2/FolderIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout$LayoutParams;

    iget v5, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v1, v1, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    invoke-virtual {v0, v5, v1}, Lcom/android/launcher2/CellLayout;->b(II)V

    :cond_63
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/FolderIcon;)V

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/android/launcher2/FolderIcon;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v1, v8

    aput-object v3, v1, v7

    const/4 v2, 0x2

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2763
    :cond_8c
    return-void

    .line 2758
    :cond_8d
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Opening folder ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") which already has a parent ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2759
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2758
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1a
.end method

.method static synthetic b(Lcom/android/launcher2/Launcher;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 5774
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->f(I)V

    return-void
.end method

.method static synthetic b(Lcom/android/launcher2/Launcher;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 314
    iput-boolean p1, p0, Lcom/android/launcher2/Launcher;->W:Z

    return-void
.end method

.method private b(ZZ)V
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 2989
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_c

    .line 2990
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 2991
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    .line 2993
    :cond_c
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_18

    .line 2994
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 2995
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    .line 2997
    :cond_18
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2999
    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 3000
    const v2, 0x7f0a0009

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 3001
    const v2, 0x7f0a0008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v8, v2

    .line 3002
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 3003
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 3005
    const v5, 0x7f0a000d

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 3007
    invoke-static {v2}, Lcom/android/launcher2/Launcher;->c(Landroid/view/View;)V

    .line 3011
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    sget-object v6, Lcom/android/launcher2/kg;->c:Lcom/android/launcher2/kg;

    invoke-virtual {v5, v6, p1}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/kg;Z)Landroid/animation/Animator;

    move-result-object v6

    .line 3013
    if-eqz p1, :cond_133

    const-string v5, "FLY"

    iget-object v7, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v7, v7, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_133

    .line 3014
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v5, v5, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v7, 0x64

    if-eq v5, v7, :cond_71

    .line 3016
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3017
    const/4 v5, 0x0

    invoke-direct {p0, v5}, Lcom/android/launcher2/Launcher;->g(Z)V

    .line 3018
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3020
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    .line 3022
    :cond_71
    invoke-virtual {v2, v8}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleX(F)V

    .line 3023
    invoke-virtual {v2, v8}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleY(F)V

    .line 3024
    new-instance v5, Lcom/android/launcher2/ho;

    invoke-direct {v5, v2}, Lcom/android/launcher2/ho;-><init>(Landroid/view/View;)V

    .line 3026
    const/high16 v7, 0x3f80

    invoke-virtual {v5, v7}, Lcom/android/launcher2/ho;->c(F)Lcom/android/launcher2/ho;

    move-result-object v7

    const/high16 v9, 0x3f80

    invoke-virtual {v7, v9}, Lcom/android/launcher2/ho;->d(F)Lcom/android/launcher2/ho;

    move-result-object v7

    .line 3027
    int-to-long v9, v1

    invoke-virtual {v7, v9, v10}, Lcom/android/launcher2/ho;->setDuration(J)Landroid/animation/Animator;

    move-result-object v1

    .line 3028
    new-instance v7, Lcom/android/launcher2/kk;

    invoke-direct {v7}, Lcom/android/launcher2/kk;-><init>()V

    invoke-virtual {v1, v7}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3030
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3031
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->setAlpha(F)V

    .line 3033
    const-string v1, "alpha"

    const/4 v7, 0x2

    new-array v7, v7, [F

    fill-array-data v7, :array_274

    invoke-static {v2, v1, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 3034
    int-to-long v9, v4

    invoke-virtual {v1, v9, v10}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 3035
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x3fc0

    invoke-direct {v4, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v1, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 3036
    new-instance v4, Lcom/android/launcher2/dp;

    invoke-direct {v4, p0, v3, v2}, Lcom/android/launcher2/dp;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/AppsCustomizeTabHost;)V

    invoke-virtual {v1, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 3047
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v4, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    .line 3048
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    int-to-long v9, v0

    invoke-virtual {v4, v9, v10}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 3049
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v4, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 3051
    iget-object v7, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    new-instance v0, Lcom/android/launcher2/dq;

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/dq;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/AppsCustomizeTabHost;Landroid/view/View;ZZ)V

    invoke-virtual {v7, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 3094
    if-eqz v6, :cond_ef

    .line 3095
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 3098
    :cond_ef
    const/4 v1, 0x0

    .line 3101
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3102
    const/4 v0, 0x0

    invoke-direct {p0, v2, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3106
    invoke-interface {v2}, Lcom/android/launcher2/hn;->getContent()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_110

    .line 3107
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_110

    .line 3108
    invoke-virtual {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->getMeasuredWidth()I

    move-result v0

    if-nez v0, :cond_12d

    .line 3109
    :cond_110
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 3110
    const/4 v1, 0x1

    .line 3115
    :goto_117
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    .line 3116
    new-instance v4, Lcom/android/launcher2/dr;

    move-object v5, p0

    move-object v7, v2

    move-object v9, v3

    move v10, p1

    invoke-direct/range {v4 .. v10}, Lcom/android/launcher2/dr;-><init>(Lcom/android/launcher2/Launcher;Landroid/animation/AnimatorSet;Lcom/android/launcher2/AppsCustomizeTabHost;FLandroid/view/View;Z)V

    .line 3136
    if-eqz v1, :cond_12f

    .line 3137
    new-instance v1, Lcom/android/launcher2/dt;

    invoke-direct {v1, p0, v2, v4, v0}, Lcom/android/launcher2/dt;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/AppsCustomizeTabHost;Ljava/lang/Runnable;Landroid/view/ViewTreeObserver;)V

    .line 3146
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 3272
    :cond_12c
    :goto_12c
    return-void

    .line 3112
    :cond_12d
    const/4 v0, 0x0

    goto :goto_117

    .line 3148
    :cond_12f
    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    goto :goto_12c

    .line 3150
    :cond_133
    if-eqz p1, :cond_1a7

    const-string v0, "FADE"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a7

    .line 3151
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3152
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3153
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->g(Z)V

    .line 3154
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3156
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    .line 3157
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationX(F)V

    .line 3158
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationY(F)V

    .line 3159
    const/high16 v0, 0x3f80

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleX(F)V

    .line 3160
    const/high16 v0, 0x3f80

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleY(F)V

    .line 3161
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3162
    invoke-virtual {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->bringToFront()V

    .line 3163
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setAlpha(F)V

    .line 3164
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3165
    const/4 v0, 0x0

    invoke-direct {p0, v2, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3166
    invoke-virtual {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    .line 3167
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v4, 0x258

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3168
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    new-instance v4, Lcom/android/launcher2/du;

    move-object v5, p0

    move-object v6, v3

    move v7, p1

    move-object v8, v2

    move v9, p2

    invoke-direct/range {v4 .. v9}, Lcom/android/launcher2/du;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLcom/android/launcher2/AppsCustomizeTabHost;Z)V

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3199
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_12c

    .line 3200
    :cond_1a7
    if-eqz p1, :cond_21c

    const-string v0, "SCALE"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21c

    .line 3201
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3202
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3203
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->g(Z)V

    .line 3204
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3206
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    .line 3207
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationX(F)V

    .line 3208
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationY(F)V

    .line 3209
    const/high16 v0, 0x3f80

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setAlpha(F)V

    .line 3210
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3211
    invoke-virtual {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->bringToFront()V

    .line 3212
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleX(F)V

    .line 3213
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleY(F)V

    .line 3214
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3215
    const/4 v0, 0x0

    invoke-direct {p0, v2, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3216
    invoke-virtual {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    .line 3217
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v4, 0x190

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3218
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/android/launcher2/dw;

    invoke-direct {v1, p0, v3, p1, v2}, Lcom/android/launcher2/dw;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLcom/android/launcher2/AppsCustomizeTabHost;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3242
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aG:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_12c

    .line 3244
    :cond_21c
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationX(F)V

    .line 3245
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationY(F)V

    .line 3246
    const/high16 v0, 0x3f80

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleX(F)V

    .line 3247
    const/high16 v0, 0x3f80

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setScaleY(F)V

    .line 3248
    const/high16 v0, 0x3f80

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setAlpha(F)V

    .line 3249
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3250
    invoke-virtual {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->bringToFront()V

    .line 3252
    if-nez p2, :cond_24f

    .line 3254
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_246

    .line 3255
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3257
    :cond_246
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3260
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    .line 3262
    :cond_24f
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3263
    invoke-static {v3, p1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    .line 3264
    const/4 v0, 0x0

    invoke-direct {p0, v3, p1, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3265
    const/4 v0, 0x0

    invoke-direct {p0, v2, p1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;ZZ)V

    .line 3266
    invoke-static {v2, p1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Z)V

    .line 3267
    const/4 v0, 0x0

    invoke-direct {p0, v2, p1, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;ZZ)V

    .line 3268
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_12c

    .line 3269
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    goto/16 :goto_12c

    .line 3033
    nop

    :array_274
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method static b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1322
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v1, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    iget v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    invoke-static {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Context;Landroid/content/ComponentName;II)[I

    move-result-object v0

    return-object v0
.end method

.method public static c()I
    .registers 2

    .prologue
    .line 645
    sget-object v1, Lcom/android/launcher2/Launcher;->w:Ljava/lang/Object;

    monitor-enter v1

    .line 646
    :try_start_3
    sget v0, Lcom/android/launcher2/Launcher;->x:I

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_7

    return v0

    .line 645
    :catchall_7
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static c(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    const/high16 v1, 0x4000

    .line 2889
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotX(F)V

    .line 2890
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotY(F)V

    .line 2891
    return-void
.end method

.method static synthetic c(Lcom/android/launcher2/Launcher;)V
    .registers 1
    .parameter

    .prologue
    .line 1578
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    return-void
.end method

.method static synthetic d(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/di;
    .registers 2
    .parameter

    .prologue
    .line 287
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    return-object v0
.end method

.method private static d(I)Lcom/android/launcher2/fv;
    .registers 5
    .parameter

    .prologue
    .line 1039
    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    .line 1040
    invoke-static {}, Lcom/android/launcher2/fv;->values()[Lcom/android/launcher2/fv;

    move-result-object v2

    .line 1041
    const/4 v0, 0x0

    :goto_7
    array-length v3, v2

    if-lt v0, v3, :cond_c

    move-object v0, v1

    .line 1047
    :goto_b
    return-object v0

    .line 1042
    :cond_c
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lcom/android/launcher2/fv;->ordinal()I

    move-result v3

    if-ne v3, p0, :cond_17

    .line 1043
    aget-object v0, v2, v0

    goto :goto_b

    .line 1041
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_7
.end method

.method private d(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 5589
    if-eqz p1, :cond_5a

    .line 5590
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_f

    .line 5591
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 5592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    .line 5594
    :cond_f
    new-instance v0, Landroid/widget/ListPopupWindow;

    invoke-direct {v0, p0}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    .line 5595
    new-instance v0, Lcom/android/launcher2/fs;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/fs;-><init>(Lcom/android/launcher2/Launcher;Z)V

    .line 5596
    invoke-virtual {v0}, Lcom/android/launcher2/fs;->getCount()I

    move-result v1

    if-lez v1, :cond_59

    .line 5597
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 5598
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    new-instance v2, Lcom/android/launcher2/ft;

    invoke-direct {v2, p0, v0}, Lcom/android/launcher2/ft;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/fs;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 5599
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    const/high16 v1, 0x4370

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 5600
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 5601
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setInputMethodMode(I)V

    .line 5602
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 5603
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 5626
    :cond_59
    :goto_59
    return-void

    .line 5606
    :cond_5a
    new-instance v0, Lcom/android/launcher2/fs;

    invoke-direct {v0, p0, v3}, Lcom/android/launcher2/fs;-><init>(Lcom/android/launcher2/Launcher;Z)V

    .line 5607
    invoke-virtual {v0}, Lcom/android/launcher2/fs;->getCount()I

    move-result v1

    if-lez v1, :cond_59

    .line 5608
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5609
    new-instance v2, Lcom/android/launcher2/et;

    invoke-direct {v2, p0, v0}, Lcom/android/launcher2/et;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/fs;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5617
    new-instance v0, Lcom/android/launcher2/eu;

    invoke-direct {v0, p0}, Lcom/android/launcher2/eu;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 5622
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    .line 5623
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_59
.end method

.method static synthetic e(Lcom/android/launcher2/Launcher;)Ljava/util/HashMap;
    .registers 2
    .parameter

    .prologue
    .line 340
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    return-object v0
.end method

.method private e(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3553
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_8

    .line 3568
    :goto_7
    return-void

    .line 3555
    :cond_8
    invoke-direct {p0, p1, v2}, Lcom/android/launcher2/Launcher;->b(ZZ)V

    .line 3556
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->requestFocus()Z

    .line 3559
    sget-object v0, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    .line 3562
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->ac:Z

    .line 3563
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    .line 3564
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 3567
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_7
.end method

.method private e(I)Z
    .registers 8
    .parameter

    .prologue
    const/high16 v5, 0x1080

    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 5633
    packed-switch p1, :pswitch_data_8e

    .line 5689
    const/4 v0, 0x0

    :cond_8
    :goto_8
    return v0

    .line 5635
    :pswitch_9
    invoke-virtual {p0, v0, v4}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 5636
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aJ:Lcom/android/launcher2/as;

    invoke-direct {p0, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/as;)V

    goto :goto_8

    .line 5639
    :pswitch_12
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->V()V

    goto :goto_8

    .line 5642
    :pswitch_16
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5643
    const-string v2, "settings"

    invoke-virtual {p0, v4, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_8

    .line 5646
    :pswitch_23
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onSearchRequested()Z

    goto :goto_8

    .line 5649
    :pswitch_27
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ab()V

    goto :goto_8

    .line 5652
    :pswitch_2b
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5653
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5654
    const-string v2, "manage apps"

    invoke-virtual {p0, v4, v1, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_8

    .line 5657
    :pswitch_3b
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ae()V

    goto :goto_8

    .line 5660
    :pswitch_3f
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/anddoes/launcher/ui/DrawerConfigActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 5661
    const-string v2, "manage drawer"

    invoke-virtual {p0, v4, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_8

    .line 5664
    :pswitch_4c
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->J()V

    goto :goto_8

    .line 5667
    :pswitch_50
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5668
    const/high16 v2, 0x1020

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5669
    const-string v2, "settings"

    invoke-virtual {p0, v4, v1, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_8

    .line 5672
    :pswitch_62
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->af()V

    goto :goto_8

    .line 5675
    :pswitch_66
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ag()V

    goto :goto_8

    .line 5678
    :pswitch_6a
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    if-eqz v1, :cond_8

    .line 5679
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    const-string v2, "play store"

    invoke-virtual {p0, v4, v1, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_8

    .line 5683
    :pswitch_76
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "http://blog.anddoes.com/apex-launcher-faq/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 5684
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5686
    const-string v2, "help"

    invoke-virtual {p0, v4, v1, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 5633
    nop

    :pswitch_data_8e
    .packed-switch 0x2
        :pswitch_9
        :pswitch_12
        :pswitch_16
        :pswitch_23
        :pswitch_27
        :pswitch_2b
        :pswitch_3b
        :pswitch_3f
        :pswitch_4c
        :pswitch_50
        :pswitch_62
        :pswitch_66
        :pswitch_6a
        :pswitch_76
    .end packed-switch
.end method

.method private f(I)V
    .registers 4
    .parameter

    .prologue
    .line 5775
    const/4 v0, -0x1

    if-eq p1, v0, :cond_d

    .line 5778
    new-instance v0, Lcom/android/launcher2/ev;

    const-string v1, "deleteAppWidgetId"

    invoke-direct {v0, p0, v1, p1}, Lcom/android/launcher2/ev;-><init>(Lcom/android/launcher2/Launcher;Ljava/lang/String;I)V

    .line 5782
    invoke-virtual {v0}, Lcom/android/launcher2/ev;->start()V

    .line 5784
    :cond_d
    return-void
.end method

.method static synthetic f(Lcom/android/launcher2/Launcher;)V
    .registers 3
    .parameter

    .prologue
    .line 1571
    const-wide/16 v0, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(J)V

    return-void
.end method

.method private f(Z)V
    .registers 6
    .parameter

    .prologue
    const/high16 v2, 0x3f80

    .line 3670
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v0, :cond_2d

    .line 3671
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 3672
    if-eqz p1, :cond_2e

    .line 3673
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher2/SearchDropTargetBar;->getTransitionInDuration()I

    move-result v0

    .line 3674
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 3675
    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3676
    new-instance v0, Lcom/android/launcher2/ee;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ee;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3695
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3700
    :cond_2d
    :goto_2d
    return-void

    .line 3697
    :cond_2e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Hotseat;->setAlpha(F)V

    goto :goto_2d
.end method

.method static synthetic g(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 2335
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    invoke-virtual {v0}, Lcom/android/launcher2/fx;->startListening()V

    :cond_9
    return-void
.end method

.method private g(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3706
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ae:Z

    if-nez v0, :cond_28

    .line 3707
    if-eqz p1, :cond_29

    .line 3708
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher2/SearchDropTargetBar;->getTransitionOutDuration()I

    move-result v0

    .line 3709
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1}, Lcom/android/launcher2/Hotseat;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 3710
    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 3711
    new-instance v0, Lcom/android/launcher2/ef;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ef;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3730
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 3736
    :cond_28
    :goto_28
    return-void

    .line 3732
    :cond_29
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    .line 3733
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Hotseat;->setAlpha(F)V

    goto :goto_28
.end method

.method static synthetic h(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/fv;
    .registers 2
    .parameter

    .prologue
    .line 258
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    return-object v0
.end method

.method private h(Z)Z
    .registers 12
    .parameter

    .prologue
    const v9, 0x7f0d003d

    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 3870
    const v0, 0x7f0d0054

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 3871
    const v0, 0x7f0d0053

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 3872
    invoke-virtual {p0, v9}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 3876
    new-instance v0, Landroid/content/Intent;

    const-string v5, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3877
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    .line 3878
    if-eqz p1, :cond_5e

    if-eqz v5, :cond_5e

    .line 3879
    const-string v0, "ICE_CREAM_SANDWICH"

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v6, v6, Lcom/anddoes/launcher/preference/f;->s:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 3880
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->X()I

    move-result v6

    .line 3881
    sget-object v7, Lcom/android/launcher2/Launcher;->aq:[Landroid/graphics/drawable/Drawable$ConstantState;

    .line 3883
    if-eqz v0, :cond_5a

    const v0, 0x7f020031

    .line 3884
    :goto_40
    const-string v8, ""

    .line 3881
    invoke-direct {p0, v9, v5, v0, v8}, Lcom/android/launcher2/Launcher;->a(ILandroid/content/ComponentName;ILjava/lang/String;)Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v0

    aput-object v0, v7, v6

    .line 3885
    if-eqz v2, :cond_4d

    .line 3886
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3887
    :cond_4d
    if-eqz v3, :cond_52

    .line 3888
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3889
    :cond_52
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3890
    invoke-static {v3, v4}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/view/View;)V

    .line 3891
    const/4 v0, 0x1

    .line 3898
    :goto_59
    return v0

    .line 3883
    :cond_5a
    const v0, 0x7f020032

    goto :goto_40

    .line 3893
    :cond_5e
    if-eqz v2, :cond_63

    .line 3894
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 3895
    :cond_63
    if-eqz v3, :cond_68

    .line 3896
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 3897
    :cond_68
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 3898
    goto :goto_59
.end method

.method static synthetic i(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/as;
    .registers 2
    .parameter

    .prologue
    .line 4488
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aJ:Lcom/android/launcher2/as;

    return-object v0
.end method

.method private i(Z)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    const/high16 v9, 0x3f80

    .line 4224
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 4225
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4228
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/launcher2/ej;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ej;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 4239
    if-eqz p1, :cond_49

    .line 4240
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_39

    .line 4268
    :goto_26
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Launcher;->au:I

    .line 4269
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4270
    new-instance v0, Lcom/android/launcher2/el;

    const-string v1, "clearNewAppsThread"

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/el;-><init>(Lcom/android/launcher2/Launcher;Ljava/lang/String;)V

    .line 4277
    invoke-virtual {v0}, Lcom/android/launcher2/el;->start()V

    .line 4278
    return-void

    .line 4240
    :cond_39
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 4241
    invoke-virtual {v0, v9}, Landroid/view/View;->setAlpha(F)V

    .line 4242
    invoke-virtual {v0, v9}, Landroid/view/View;->setScaleX(F)V

    .line 4243
    invoke-virtual {v0, v9}, Landroid/view/View;->setScaleY(F)V

    goto :goto_20

    :cond_49
    move v1, v2

    .line 4246
    :goto_4a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_61

    .line 4257
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 4258
    new-instance v0, Lcom/android/launcher2/ek;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ek;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 4264
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_26

    .line 4247
    :cond_61
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 4248
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    .line 4249
    const-string v6, "alpha"

    new-array v7, v10, [F

    aput v9, v7, v2

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v5, v2

    .line 4250
    const-string v6, "scaleX"

    new-array v7, v10, [F

    aput v9, v7, v2

    invoke-static {v6, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v6

    aput-object v6, v5, v10

    const/4 v6, 0x2

    .line 4251
    const-string v7, "scaleY"

    new-array v8, v10, [F

    aput v9, v8, v2

    invoke-static {v7, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v7

    aput-object v7, v5, v6

    .line 4248
    invoke-static {v0, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 4252
    const-wide/16 v5, 0x1c2

    invoke-virtual {v0, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 4253
    mul-int/lit8 v5, v1, 0x4b

    int-to-long v5, v5

    invoke-virtual {v0, v5, v6}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 4254
    new-instance v5, Lcom/android/launcher2/jf;

    invoke-direct {v5}, Lcom/android/launcher2/jf;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 4255
    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 4246
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4a
.end method

.method static synthetic j(Lcom/android/launcher2/Launcher;)Landroid/content/Intent;
    .registers 2
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    return-object v0
.end method

.method private j(Z)V
    .registers 4
    .parameter

    .prologue
    .line 5565
    const-string v0, "SINGLE_SCREEN"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 5567
    :try_start_c
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->setWallpaperDimension(Z)V

    .line 5568
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->L()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_16} :catch_17

    .line 5572
    :cond_16
    :goto_16
    return-void

    :catch_17
    move-exception v0

    goto :goto_16
.end method

.method static synthetic k(Lcom/android/launcher2/Launcher;)Landroid/widget/ListPopupWindow;
    .registers 2
    .parameter

    .prologue
    .line 4499
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic l(Lcom/android/launcher2/Launcher;)Lcom/anddoes/launcher/s;
    .registers 2
    .parameter

    .prologue
    .line 370
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aB:Lcom/anddoes/launcher/s;

    return-object v0
.end method

.method static synthetic m(Lcom/android/launcher2/Launcher;)Landroid/app/Dialog;
    .registers 2
    .parameter

    .prologue
    .line 4507
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic n(Lcom/android/launcher2/Launcher;)V
    .registers 6
    .parameter

    .prologue
    .line 5379
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const v2, 0x7f070286

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const v2, 0x7f02007b

    invoke-static {p0, v2}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.extra.INTENT"

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "android.intent.extra.TITLE"

    const v3, 0x7f070291

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v0, 0x7

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic o(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/fx;
    .registers 2
    .parameter

    .prologue
    .line 285
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    return-object v0
.end method

.method static synthetic p(Lcom/android/launcher2/Launcher;)V
    .registers 1
    .parameter

    .prologue
    .line 2256
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->V()V

    return-void
.end method

.method static synthetic q(Lcom/android/launcher2/Launcher;)V
    .registers 1
    .parameter

    .prologue
    .line 533
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Q()V

    return-void
.end method

.method static synthetic r(Lcom/android/launcher2/Launcher;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 372
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aC:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic s(Lcom/android/launcher2/Launcher;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 364
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic t(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/SearchDropTargetBar;
    .registers 2
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    return-object v0
.end method

.method static synthetic u(Lcom/android/launcher2/Launcher;)Landroid/animation/AnimatorSet;
    .registers 2
    .parameter

    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->u:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic v(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Hotseat;
    .registers 2
    .parameter

    .prologue
    .line 294
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    return-object v0
.end method

.method static synthetic w(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 4223
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->i(Z)V

    return-void
.end method

.method static synthetic x(Lcom/android/launcher2/Launcher;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->at:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .registers 4

    .prologue
    .line 4620
    const/4 v0, -0x1

    .line 4621
    const-string v1, "LANDSCAPE"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->al:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 4622
    const/4 v0, 0x0

    .line 4630
    :cond_e
    :goto_e
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->setRequestedOrientation(I)V

    .line 4631
    return-void

    .line 4623
    :cond_12
    const-string v1, "PORTRAIT"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->al:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 4624
    const/4 v0, 0x1

    goto :goto_e

    .line 4625
    :cond_20
    const-string v1, "ROTATE"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->al:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 4626
    const/4 v0, 0x2

    goto :goto_e

    .line 4627
    :cond_2e
    const-string v1, "DEFAULT"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->al:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 4628
    const/4 v0, 0x5

    goto :goto_e
.end method

.method public final B()V
    .registers 3

    .prologue
    const/16 v1, 0x400

    .line 4635
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->u:Z

    if-eqz v0, :cond_13

    .line 4636
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 4637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    .line 4642
    :goto_12
    return-void

    .line 4639
    :cond_13
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 4640
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    goto :goto_12
.end method

.method public final C()V
    .registers 3

    .prologue
    .line 4713
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v0, :cond_11

    .line 4714
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->af:Z

    if-eqz v0, :cond_12

    .line 4715
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4723
    :cond_11
    :goto_11
    return-void

    .line 4717
    :cond_12
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_11

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v0, :cond_11

    .line 4718
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 4719
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_11
.end method

.method public final D()V
    .registers 3

    .prologue
    .line 4727
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 4728
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    if-eqz v0, :cond_1c

    .line 4729
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v0, :cond_1d

    .line 4730
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/DragLayer;->setBackgroundColor(I)V

    .line 4734
    :goto_17
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->postInvalidate()V

    .line 4736
    :cond_1c
    return-void

    .line 4732
    :cond_1d
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Lcom/android/launcher2/DragLayer;->setBackgroundColor(I)V

    goto :goto_17
.end method

.method public final E()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 4740
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_28

    .line 4741
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const/high16 v1, 0x7f08

    const-string v2, "drawer_background_color"

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    .line 4742
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v1, v1, Lcom/anddoes/launcher/preference/f;->B:I

    if-nez v1, :cond_29

    .line 4743
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0, v5}, Lcom/android/launcher2/AppsCustomizeTabHost;->setBackgroundColor(I)V

    .line 4744
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getAnimationBuffer()Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 4753
    :goto_23
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->postInvalidate()V

    .line 4755
    :cond_28
    return-void

    .line 4745
    :cond_29
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v1, v1, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_40

    .line 4746
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setBackgroundColor(I)V

    .line 4747
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->getAnimationBuffer()Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto :goto_23

    .line 4749
    :cond_40
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v3, v3, Lcom/anddoes/launcher/preference/f;->B:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 4750
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 4749
    invoke-static {v2, v3, v4, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->setBackgroundColor(I)V

    .line 4751
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->getAnimationBuffer()Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    goto :goto_23
.end method

.method public final F()Z
    .registers 3

    .prologue
    .line 5004
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method public final G()V
    .registers 3

    .prologue
    .line 5008
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 5016
    return-void
.end method

.method public final H()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    const v4, 0x7f0b0048

    const/4 v5, -0x2

    const/4 v3, 0x0

    .line 5020
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    const v1, 0x7f0d000f

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 5021
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v2, "tab_widget_indicator"

    invoke-virtual {v1, v0, v3, v2}, Lcom/anddoes/launcher/c/l;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 5022
    const-string v1, "NONE"

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 5023
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5067
    :goto_28
    return-void

    .line 5025
    :cond_29
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5026
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 5028
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 5029
    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v3

    .line 5030
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 5031
    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v4

    .line 5027
    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 5032
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 5033
    const v2, 0x7f020044

    .line 5034
    const-string v3, "menu_more_overflow"

    .line 5032
    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 5035
    const-string v1, "SHOP"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_67

    .line 5036
    const v1, 0x7f020046

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 5037
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_28

    .line 5038
    :cond_67
    const-string v1, "MENU"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7d

    .line 5039
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5040
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v5, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_28

    .line 5042
    :cond_7d
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b004d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 5043
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v3, "drawer_tab_icon_pkg"

    invoke-virtual {v1, v3, v6}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 5044
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v4, "drawer_tab_icon_intent"

    invoke-virtual {v3, v4, v6}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5045
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b5

    .line 5047
    const/4 v4, 0x0

    :try_start_a9
    invoke-static {v3, v4}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    .line 5048
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;
    :try_end_b4
    .catch Ljava/net/URISyntaxException; {:try_start_a9 .. :try_end_b4} :catch_dd

    move-result-object v1

    .line 5053
    :cond_b5
    :goto_b5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d8

    .line 5055
    :try_start_bb
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 5056
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 5057
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_cd
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_bb .. :try_end_cd} :catch_cf

    goto/16 :goto_28

    .line 5060
    :catch_cf
    move-exception v1

    const v1, 0x7f020033

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_28

    .line 5063
    :cond_d8
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_28

    :catch_dd
    move-exception v3

    goto :goto_b5
.end method

.method public final I()Z
    .registers 3

    .prologue
    .line 5071
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v1, "HORIZONTAL_PAGINATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 5072
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v1, "VERTICAL_PAGINATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x0

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x1

    .line 5071
    goto :goto_19
.end method

.method public final J()V
    .registers 4

    .prologue
    .line 5322
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 5323
    const-class v1, Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 5324
    const/4 v1, 0x0

    const-string v2, "preference"

    invoke-virtual {p0, v1, v0, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 5325
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_19

    .line 5326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aH:Z

    .line 5328
    :cond_19
    return-void
.end method

.method public final K()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 5331
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aH:Z

    if-eqz v0, :cond_b

    .line 5332
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->aH:Z

    .line 5333
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 5335
    :cond_b
    return-void
.end method

.method public final L()Z
    .registers 2

    .prologue
    .line 5542
    invoke-static {p0}, Lcom/anddoes/launcher/a/e;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anddoes/launcher/a/e;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aV:Z

    .line 5543
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aV:Z

    return v0
.end method

.method public final M()Z
    .registers 2

    .prologue
    .line 5547
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aV:Z

    return v0
.end method

.method public final N()V
    .registers 5

    .prologue
    .line 5551
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03003c

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 5552
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->setMargins(Z)V

    .line 5553
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->addView(Landroid/view/View;)V

    .line 5554
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/Workspace;->j:[F

    .line 5555
    return-void
.end method

.method protected final O()Lcom/android/launcher2/da;
    .registers 2

    .prologue
    .line 5629
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    return-object v0
.end method

.method final a(Landroid/view/ViewGroup;Lcom/android/launcher2/jd;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1211
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 1212
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    iget-wide v2, p2, Lcom/android/launcher2/jd;->j:J

    invoke-static {p0, v2, v3}, Lcom/anddoes/launcher/v;->a(Lcom/android/launcher2/Launcher;J)Z

    move-result v2

    invoke-virtual {v0, p2, v1, v2}, Lcom/android/launcher2/BubbleTextView;->a(Lcom/android/launcher2/jd;Lcom/android/launcher2/da;Z)V

    .line 1213
    invoke-virtual {v0, p0}, Lcom/android/launcher2/BubbleTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1214
    return-object v0
.end method

.method public final a(JI)Lcom/android/launcher2/CellLayout;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2851
    const-wide/16 v0, -0x65

    cmp-long v0, p1, v0

    if-nez v0, :cond_13

    .line 2852
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_11

    .line 2853
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, p3}, Lcom/android/launcher2/Hotseat;->j(I)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 2858
    :goto_10
    return-object v0

    .line 2855
    :cond_11
    const/4 v0, 0x0

    goto :goto_10

    .line 2858
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    goto :goto_10
.end method

.method public final a()Lcom/android/launcher2/DragLayer;
    .registers 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    return-object v0
.end method

.method final a(Lcom/android/launcher2/CellLayout;JIII)Lcom/android/launcher2/FolderIcon;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2236
    new-instance v1, Lcom/android/launcher2/cu;

    invoke-direct {v1}, Lcom/android/launcher2/cu;-><init>()V

    .line 2237
    const v0, 0x7f070275

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, v1, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    .line 2241
    const/4 v7, 0x0

    move-object v0, p0

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    .line 2240
    invoke-static/range {v0 .. v7}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V

    .line 2242
    sget-object v0, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    iget-wide v2, v1, Lcom/android/launcher2/cu;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2246
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    invoke-static {p0, p1, v1}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/Launcher;Landroid/view/ViewGroup;Lcom/android/launcher2/cu;)Lcom/android/launcher2/FolderIcon;

    move-result-object v1

    .line 2247
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v7, 0x1

    const/4 v8, 0x1

    .line 2248
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v9

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    .line 2247
    invoke-virtual/range {v0 .. v9}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 2249
    return-object v1
.end method

.method final a(JI[I[I)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2051
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 2052
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput-wide p1, v0, Lcom/android/launcher2/di;->j:J

    .line 2053
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput p3, v0, Lcom/android/launcher2/di;->k:I

    .line 2054
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/di;->s:[I

    .line 2056
    if-eqz p4, :cond_3c

    .line 2057
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v1, p4, v4

    iput v1, v0, Lcom/android/launcher2/di;->l:I

    .line 2058
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v1, p4, v5

    iput v1, v0, Lcom/android/launcher2/di;->m:I

    .line 2059
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 2060
    if-eqz v0, :cond_3c

    .line 2061
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v2

    aget v3, p4, v4

    sub-int/2addr v2, v3

    iput v2, v1, Lcom/android/launcher2/di;->n:I

    .line 2062
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    aget v2, p4, v5

    sub-int/2addr v0, v2

    iput v0, v1, Lcom/android/launcher2/di;->o:I

    .line 2065
    :cond_3c
    if-eqz p5, :cond_4a

    .line 2066
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v1, p5, v4

    iput v1, v0, Lcom/android/launcher2/di;->n:I

    .line 2067
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v1, p5, v5

    iput v1, v0, Lcom/android/launcher2/di;->o:I

    .line 2070
    :cond_4a
    const-string v0, "search_widget"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;JI)Z

    .line 2072
    invoke-virtual {p0, v5, v4}, Lcom/android/launcher2/Launcher;->a(ZZ)V

    .line 2073
    return-void
.end method

.method final a(Landroid/content/ComponentName;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 2503
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2504
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    .line 2505
    const-string v3, "package"

    invoke-static {v3, v0, v4}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2504
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2506
    const/high16 v0, 0x1080

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2507
    const-string v0, "startApplicationDetailsActivity"

    invoke-virtual {p0, v4, v1, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 2508
    return-void
.end method

.method final a(Landroid/content/ComponentName;JI[I)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2030
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 2031
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput-wide p2, v0, Lcom/android/launcher2/di;->j:J

    .line 2032
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput p4, v0, Lcom/android/launcher2/di;->k:I

    .line 2033
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/di;->s:[I

    .line 2035
    if-eqz p5, :cond_3c

    .line 2036
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v1, p5, v3

    iput v1, v0, Lcom/android/launcher2/di;->l:I

    .line 2037
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v1, p5, v4

    iput v1, v0, Lcom/android/launcher2/di;->m:I

    .line 2038
    invoke-virtual {p0, p2, p3, p4}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 2039
    if-eqz v0, :cond_3c

    .line 2040
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v2

    aget v3, p5, v3

    sub-int/2addr v2, v3

    iput v2, v1, Lcom/android/launcher2/di;->n:I

    .line 2041
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    aget v2, p5, v4

    sub-int/2addr v0, v2

    iput v0, v1, Lcom/android/launcher2/di;->o:I

    .line 2045
    :cond_3c
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2046
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2047
    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Intent;)V

    .line 2048
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    .line 5575
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_12

    if-eqz p1, :cond_12

    .line 5576
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    new-instance v1, Lcom/android/launcher2/er;

    invoke-direct {v1, p0, p1}, Lcom/android/launcher2/er;-><init>(Lcom/android/launcher2/Launcher;Landroid/content/Intent;)V

    .line 5580
    const-wide/16 v2, 0x3e8

    .line 5576
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 5582
    :cond_12
    return-void
.end method

.method final a(Landroid/content/Intent;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const v2, 0x7f070279

    const/4 v1, 0x0

    .line 2587
    if-nez p1, :cond_7

    .line 2602
    :goto_6
    return-void

    .line 2591
    :cond_7
    :try_start_7
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/Launcher;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_7 .. :try_end_a} :catch_b
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_a} :catch_14
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_a} :catch_37

    goto :goto_6

    .line 2593
    :catch_b
    move-exception v0

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_6

    .line 2594
    :catch_14
    move-exception v0

    .line 2595
    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2596
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Launcher does not have the permission to launch "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2597
    const-string v3, ". Make sure to create a MAIN intent-filter for the corresponding activity or use the exported attribute for this activity."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2598
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2596
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 2599
    :catch_37
    move-exception v0

    .line 2600
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to launch. intent="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6
.end method

.method final a(Landroid/view/View;Lcom/android/launcher2/di;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 5849
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EDIT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5850
    const-class v1, Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 5851
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iput-object p2, v1, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    .line 5852
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iput-object p1, v1, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    .line 5853
    const/16 v1, 0xc

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;I)V

    .line 5854
    return-void
.end method

.method final a(Lcom/android/launcher2/Folder;Z)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x3f80

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2780
    invoke-virtual {p1}, Lcom/android/launcher2/Folder;->getInfo()Lcom/android/launcher2/cu;

    move-result-object v0

    iput-boolean v7, v0, Lcom/android/launcher2/cu;->a:Z

    .line 2782
    invoke-virtual {p1}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2783
    if-eqz v0, :cond_7e

    .line 2784
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p1, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->b(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/FolderIcon;

    .line 2785
    if-eqz v0, :cond_7e

    const-string v1, "alpha"

    new-array v2, v8, [F

    aput v5, v2, v7

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const-string v1, "scaleX"

    new-array v3, v8, [F

    aput v5, v3, v7

    invoke-static {v1, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const-string v1, "scaleY"

    new-array v4, v8, [F

    aput v5, v4, v7

    invoke-static {v1, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/FolderIcon;)V

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->aw:Landroid/widget/ImageView;

    const/4 v6, 0x3

    new-array v6, v6, [Landroid/animation/PropertyValuesHolder;

    aput-object v2, v6, v7

    aput-object v3, v6, v8

    const/4 v2, 0x2

    aput-object v4, v6, v2

    invoke-static {v5, v6}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0017

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v3, Lcom/android/launcher2/do;

    invoke-direct {v3, p0, v1, v0}, Lcom/android/launcher2/do;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/FolderIcon;)V

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 2787
    :cond_7e
    invoke-virtual {p1, p2}, Lcom/android/launcher2/Folder;->a(Z)V

    .line 2788
    return-void
.end method

.method public final a(Lcom/android/launcher2/di;)V
    .registers 5
    .parameter

    .prologue
    .line 5757
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    iget-wide v1, p1, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bc;->a(J)V

    .line 5758
    instance-of v0, p1, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_14

    .line 5759
    check-cast p1, Lcom/android/launcher2/cu;

    .line 5760
    invoke-static {p1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/cu;)V

    .line 5761
    invoke-static {p0, p1}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/cu;)V

    .line 5772
    :goto_13
    return-void

    .line 5762
    :cond_14
    instance-of v0, p1, Lcom/android/launcher2/fz;

    if-eqz v0, :cond_29

    move-object v0, p1

    .line 5763
    check-cast v0, Lcom/android/launcher2/fz;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fz;)V

    .line 5764
    invoke-static {p0, p1}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 5766
    check-cast p1, Lcom/android/launcher2/fz;

    .line 5767
    iget v0, p1, Lcom/android/launcher2/fz;->a:I

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->f(I)V

    goto :goto_13

    .line 5769
    :cond_29
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    iget-wide v1, p1, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->c(J)V

    .line 5770
    invoke-static {p0, p1}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto :goto_13
.end method

.method public final a(Lcom/android/launcher2/di;Landroid/view/View;)V
    .registers 16
    .parameter
    .parameter

    .prologue
    const v10, 0x7f070022

    const v9, 0x108003a

    const/4 v6, 0x1

    const-wide/16 v11, -0x65

    const/4 v7, 0x0

    .line 5857
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    if-eqz v0, :cond_f

    .line 6074
    :cond_e
    :goto_e
    return-void

    .line 5861
    :cond_f
    new-instance v5, Lcom/anddoes/launcher/ui/am;

    invoke-direct {v5, p2}, Lcom/anddoes/launcher/ui/am;-><init>(Landroid/view/View;)V

    .line 5862
    iput-object v5, p0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    .line 5864
    new-instance v0, Lcom/android/launcher2/ew;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ew;-><init>(Lcom/android/launcher2/Launcher;)V

    invoke-virtual {v5, v0}, Lcom/anddoes/launcher/ui/am;->a(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 5871
    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    .line 5872
    new-instance v0, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 5873
    const v2, 0x1080038

    .line 5872
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 5874
    const v2, 0x7f070297

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5875
    new-instance v3, Lcom/android/launcher2/ex;

    invoke-direct {v3, p0, p2, p1, v5}, Lcom/android/launcher2/ex;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/di;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 5872
    invoke-virtual {v5, v0}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 5910
    instance-of v0, p1, Lcom/android/launcher2/jd;

    if-nez v0, :cond_4d

    instance-of v0, p1, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_1a8

    .line 5911
    :cond_4d
    new-instance v0, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 5912
    const v2, 0x108003e

    .line 5911
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 5913
    const v2, 0x7f070021

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/launcher2/ey;

    invoke-direct {v3, p0, p2, p1, v5}, Lcom/android/launcher2/ey;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/di;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 5911
    invoke-virtual {v5, v0}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 5962
    :cond_6c
    :goto_6c
    instance-of v0, p1, Lcom/android/launcher2/jd;

    if-eqz v0, :cond_9b

    .line 5963
    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    cmp-long v0, v0, v11

    if-nez v0, :cond_9b

    .line 5964
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 5965
    new-instance v0, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 5966
    const v2, 0x1080055

    .line 5965
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 5967
    const v2, 0x7f0700fa

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/launcher2/fb;

    invoke-direct {v3, p0, p1, v5}, Lcom/android/launcher2/fb;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/di;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 5965
    invoke-virtual {v5, v0}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 5980
    :cond_9b
    instance-of v0, p1, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_c4

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 5981
    new-instance v0, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 5982
    const v2, 0x1080033

    .line 5981
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 5983
    const v2, 0x7f070024

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/launcher2/fc;

    invoke-direct {v3, p0, v5, p1, p2}, Lcom/android/launcher2/fc;-><init>(Lcom/android/launcher2/Launcher;Lcom/anddoes/launcher/ui/am;Lcom/android/launcher2/di;Landroid/view/View;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 5981
    invoke-virtual {v5, v0}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 5997
    :cond_c4
    instance-of v0, p1, Lcom/android/launcher2/jd;

    if-nez v0, :cond_cc

    instance-of v0, p1, Lcom/android/launcher2/fz;

    if-eqz v0, :cond_11a

    .line 5998
    :cond_cc
    const/4 v1, 0x0

    .line 5999
    instance-of v0, p1, Lcom/android/launcher2/jd;

    if-eqz v0, :cond_210

    move-object v0, p1

    .line 6000
    check-cast v0, Lcom/android/launcher2/jd;

    .line 6001
    iget-object v0, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 6009
    :goto_da
    if-eqz v0, :cond_11a

    .line 6011
    new-instance v1, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 6012
    const v3, 0x1080041

    .line 6011
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 6013
    const v3, 0x7f070299

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 6014
    new-instance v4, Lcom/android/launcher2/fe;

    invoke-direct {v4, p0, p2, v0, v5}, Lcom/android/launcher2/fe;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Landroid/content/ComponentName;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 6011
    invoke-virtual {v5, v1}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 6023
    new-instance v1, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 6024
    const v3, 0x1080052

    .line 6023
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 6025
    const v3, 0x7f070023

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 6026
    new-instance v4, Lcom/android/launcher2/ff;

    invoke-direct {v4, p0, p2, v0, v5}, Lcom/android/launcher2/ff;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Landroid/content/ComponentName;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 6023
    invoke-virtual {v5, v1}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 6049
    :cond_11a
    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    const-wide/16 v2, -0x64

    cmp-long v0, v0, v2

    if-nez v0, :cond_1a3

    .line 6050
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aV:Z

    if-eqz v0, :cond_1a3

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_15c

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v2

    if-eqz v2, :cond_15c

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    new-instance v3, Landroid/graphics/Rect;

    iget v1, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v4, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v8, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v9, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    add-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    iget v9, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    add-int/2addr v0, v9

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v3, v1, v4, v8, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v4

    move v1, v7

    :goto_15a
    if-lt v1, v4, :cond_223

    :cond_15c
    move v0, v7

    :goto_15d
    if-eqz v0, :cond_1a3

    .line 6051
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    .line 6052
    new-instance v1, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 6053
    const v3, 0x7f070025

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/android/launcher2/fg;

    invoke-direct {v4, p0, p1, v0, v5}, Lcom/android/launcher2/fg;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/di;Lcom/android/launcher2/ja;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 6052
    invoke-virtual {v5, v1}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 6061
    new-instance v1, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020042

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 6062
    const v3, 0x7f070026

    invoke-virtual {p0, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/android/launcher2/fh;

    invoke-direct {v4, p0, p1, v0, v5}, Lcom/android/launcher2/fh;-><init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/di;Lcom/android/launcher2/ja;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 6061
    invoke-virtual {v5, v1}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    .line 6072
    :cond_1a3
    invoke-virtual {v5}, Lcom/anddoes/launcher/ui/am;->c()V

    goto/16 :goto_e

    .line 5923
    :cond_1a8
    instance-of v0, p1, Lcom/android/launcher2/fz;

    if-eqz v0, :cond_1e5

    move-object v4, p2

    .line 5924
    check-cast v4, Lcom/android/launcher2/fy;

    .line 5925
    if-eqz v4, :cond_6c

    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    cmp-long v0, v0, v11

    if-eqz v0, :cond_6c

    .line 5926
    invoke-virtual {v4}, Lcom/android/launcher2/fy;->getAppWidgetInfo()Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 5927
    if-eqz v0, :cond_1c1

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    if-nez v0, :cond_1c7

    .line 5928
    :cond_1c1
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aX:Z

    if-eqz v0, :cond_6c

    .line 5929
    :cond_1c7
    new-instance v8, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 5931
    invoke-virtual {p0, v10}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lcom/android/launcher2/ez;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/ez;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/di;Lcom/android/launcher2/fy;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v8, v9, v10, v0}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 5929
    invoke-virtual {v5, v8}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    goto/16 :goto_6c

    .line 5944
    :cond_1e5
    instance-of v0, p1, Lcom/anddoes/launcher/au;

    if-eqz v0, :cond_6c

    .line 5945
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aX:Z

    if-eqz v0, :cond_6c

    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    cmp-long v0, v0, v11

    if-eqz v0, :cond_6c

    .line 5946
    new-instance v0, Lcom/anddoes/launcher/ui/e;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 5948
    invoke-virtual {p0, v10}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/android/launcher2/fa;

    invoke-direct {v3, p0, p2, p1, v5}, Lcom/android/launcher2/fa;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/di;Lcom/anddoes/launcher/ui/am;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/anddoes/launcher/ui/e;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 5946
    invoke-virtual {v5, v0}, Lcom/anddoes/launcher/ui/am;->a(Lcom/anddoes/launcher/ui/e;)V

    goto/16 :goto_6c

    :cond_210
    move-object v0, p1

    .line 6003
    check-cast v0, Lcom/android/launcher2/fz;

    .line 6004
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    iget v0, v0, Lcom/android/launcher2/fz;->a:I

    invoke-virtual {v2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 6005
    if-eqz v0, :cond_2bb

    .line 6006
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    goto/16 :goto_da

    .line 6050
    :cond_223
    invoke-virtual {v2, v1}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eq p2, v0, :cond_2b6

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout$LayoutParams;

    iget v8, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    if-lez v8, :cond_2b6

    iget v8, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    if-lez v8, :cond_2b6

    new-instance v8, Landroid/graphics/Rect;

    iget v9, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v10, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v11, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iget v12, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    add-int/2addr v11, v12

    add-int/lit8 v11, v11, -0x1

    iget v12, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iget v0, v0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    add-int/2addr v0, v12

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v8, v9, v10, v11, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iget v0, v3, Landroid/graphics/Rect;->left:I

    iget v9, v8, Landroid/graphics/Rect;->left:I

    if-gt v0, v9, :cond_266

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v9, v8, Landroid/graphics/Rect;->top:I

    if-gt v0, v9, :cond_266

    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v9, v8, Landroid/graphics/Rect;->left:I

    if-lt v0, v9, :cond_266

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    iget v9, v8, Landroid/graphics/Rect;->top:I

    if-ge v0, v9, :cond_2ae

    :cond_266
    iget v0, v8, Landroid/graphics/Rect;->left:I

    iget v9, v3, Landroid/graphics/Rect;->left:I

    if-gt v0, v9, :cond_27e

    iget v0, v8, Landroid/graphics/Rect;->top:I

    iget v9, v3, Landroid/graphics/Rect;->top:I

    if-gt v0, v9, :cond_27e

    iget v0, v8, Landroid/graphics/Rect;->right:I

    iget v9, v3, Landroid/graphics/Rect;->left:I

    if-lt v0, v9, :cond_27e

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, v3, Landroid/graphics/Rect;->top:I

    if-ge v0, v9, :cond_2ae

    :cond_27e
    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v9, v8, Landroid/graphics/Rect;->left:I

    if-lt v0, v9, :cond_296

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v9, v8, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v9, :cond_296

    iget v0, v3, Landroid/graphics/Rect;->right:I

    iget v9, v8, Landroid/graphics/Rect;->right:I

    if-gt v0, v9, :cond_296

    iget v0, v3, Landroid/graphics/Rect;->top:I

    iget v9, v8, Landroid/graphics/Rect;->top:I

    if-ge v0, v9, :cond_2ae

    :cond_296
    iget v0, v8, Landroid/graphics/Rect;->right:I

    iget v9, v3, Landroid/graphics/Rect;->left:I

    if-lt v0, v9, :cond_2b4

    iget v0, v8, Landroid/graphics/Rect;->top:I

    iget v9, v3, Landroid/graphics/Rect;->bottom:I

    if-gt v0, v9, :cond_2b4

    iget v0, v8, Landroid/graphics/Rect;->right:I

    iget v9, v3, Landroid/graphics/Rect;->right:I

    if-gt v0, v9, :cond_2b4

    iget v0, v8, Landroid/graphics/Rect;->top:I

    iget v8, v3, Landroid/graphics/Rect;->top:I

    if-lt v0, v8, :cond_2b4

    :cond_2ae
    move v0, v6

    :goto_2af
    if-eqz v0, :cond_2b6

    move v0, v6

    goto/16 :goto_15d

    :cond_2b4
    move v0, v7

    goto :goto_2af

    :cond_2b6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_15a

    :cond_2bb
    move-object v0, v1

    goto/16 :goto_da
.end method

.method public final a(Lcom/android/launcher2/fz;)V
    .registers 4
    .parameter

    .prologue
    .line 1636
    iget-object v0, p1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    .line 1637
    :cond_12
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    .line 1638
    return-void
.end method

.method final a(Lcom/android/launcher2/h;)V
    .registers 4
    .parameter

    .prologue
    .line 2511
    iget v0, p1, Lcom/android/launcher2/h;->g:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_12

    .line 2514
    const v0, 0x7f0702b6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2519
    :goto_11
    return-void

    .line 2517
    :cond_12
    iget-object v0, p1, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/content/ComponentName;)V

    goto :goto_11
.end method

.method final a(Lcom/android/launcher2/iw;JI[I[I)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, -0x1

    .line 2086
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 2087
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput-wide p2, p1, Lcom/android/launcher2/iw;->j:J

    iput-wide p2, v0, Lcom/android/launcher2/di;->j:J

    .line 2088
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput p4, p1, Lcom/android/launcher2/iw;->k:I

    iput p4, v0, Lcom/android/launcher2/di;->k:I

    .line 2089
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput-object v7, v0, Lcom/android/launcher2/di;->s:[I

    .line 2090
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v2, p1, Lcom/android/launcher2/iw;->p:I

    iput v2, v0, Lcom/android/launcher2/di;->p:I

    .line 2091
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v2, p1, Lcom/android/launcher2/iw;->q:I

    iput v2, v0, Lcom/android/launcher2/di;->q:I

    .line 2093
    if-eqz p5, :cond_4d

    .line 2094
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v2, p5, v5

    iput v2, v0, Lcom/android/launcher2/di;->l:I

    .line 2095
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v2, p5, v6

    iput v2, v0, Lcom/android/launcher2/di;->m:I

    .line 2096
    invoke-virtual {p0, p2, p3, p4}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 2097
    if-eqz v0, :cond_4d

    .line 2098
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v3

    aget v4, p5, v5

    sub-int/2addr v3, v4

    iput v3, v2, Lcom/android/launcher2/di;->n:I

    .line 2099
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    aget v3, p5, v6

    sub-int/2addr v0, v3

    iput v0, v2, Lcom/android/launcher2/di;->o:I

    .line 2102
    :cond_4d
    if-eqz p6, :cond_5b

    .line 2103
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v2, p6, v5

    iput v2, v0, Lcom/android/launcher2/di;->n:I

    .line 2104
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    aget v2, p6, v6

    iput v2, v0, Lcom/android/launcher2/di;->o:I

    .line 2107
    :cond_5b
    iget-object v2, p1, Lcom/android/launcher2/iw;->u:Landroid/appwidget/AppWidgetHostView;

    .line 2109
    if-eqz v2, :cond_8c

    .line 2110
    invoke-virtual {v2}, Landroid/appwidget/AppWidgetHostView;->getAppWidgetId()I

    move-result v0

    .line 2112
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->G:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v3, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v3

    .line 2113
    if-eqz v3, :cond_8a

    .line 2114
    iget-object v3, p1, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {p0, v0, p1, v2, v3}, Lcom/android/launcher2/Launcher;->a(ILcom/android/launcher2/di;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 2119
    :goto_70
    if-ne v0, v1, :cond_89

    .line 2122
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    invoke-virtual {v0}, Lcom/android/launcher2/fx;->allocateAppWidgetId()I

    move-result v0

    .line 2123
    iget-object v1, p1, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v1, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    .line 2124
    iget-object v1, p1, Lcom/android/launcher2/iw;->a:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ILandroid/content/ComponentName;)Z

    move-result v1

    if-eqz v1, :cond_89

    .line 2125
    iget-object v1, p1, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {p0, v0, p1, v7, v1}, Lcom/android/launcher2/Launcher;->a(ILcom/android/launcher2/di;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 2128
    :cond_89
    return-void

    :cond_8a
    move v0, v1

    .line 2116
    goto :goto_70

    :cond_8c
    move v0, v1

    goto :goto_70
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 5076
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 5077
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;J)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x400

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x1

    .line 5081
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    if-eqz v0, :cond_11

    .line 5082
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 5083
    iput-object v3, p0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    .line 5086
    :cond_11
    const-string v0, "HOME_KEY"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 5087
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_23

    .line 5088
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 5299
    :cond_22
    :goto_22
    return-void

    .line 5091
    :cond_23
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->an:Z

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->M()Z

    move-result v0

    if-nez v0, :cond_40

    .line 5092
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->p()V

    .line 5093
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_22

    .line 5094
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->p()V

    goto :goto_22

    .line 5100
    :cond_40
    const-string v0, "APP_DRAWER"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 5101
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_52

    .line 5102
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto :goto_22

    .line 5104
    :cond_52
    invoke-direct {p0, v9}, Lcom/android/launcher2/Launcher;->e(Z)V

    goto :goto_22

    .line 5106
    :cond_56
    const-string v0, "SHOW_PREVIEWS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 5107
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_97

    .line 5108
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_22

    sget-object v0, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    const-string v0, "NONE"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_93

    invoke-direct {p0, v7}, Lcom/android/launcher2/Launcher;->g(Z)V

    :goto_80
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->D()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/Workspace;->a(Z)V

    goto :goto_22

    :cond_93
    invoke-direct {p0, v9}, Lcom/android/launcher2/Launcher;->g(Z)V

    goto :goto_80

    .line 5110
    :cond_97
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto :goto_22

    .line 5112
    :cond_9b
    const-string v0, "MANAGE_SCREENS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 5113
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ae()V

    goto/16 :goto_22

    .line 5114
    :cond_a8
    const-string v0, "GOTO_DEFAULT_SCREEN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 5115
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_c7

    .line 5116
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5117
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->p()V

    .line 5118
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_c7

    .line 5119
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->p()V

    .line 5122
    :cond_c7
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5123
    :cond_cc
    const-string v0, "SHOW_NOTIFICATIONS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d9

    .line 5124
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ab()V

    goto/16 :goto_22

    .line 5125
    :cond_d9
    const-string v0, "TOGGLE_STATUS_BAR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_105

    .line 5126
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eq v2, v0, :cond_fa

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    iput-boolean v9, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    goto/16 :goto_22

    :cond_fa
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    iput-boolean v7, p0, Lcom/android/launcher2/Launcher;->aQ:Z

    goto/16 :goto_22

    .line 5127
    :cond_105
    const-string v0, "TOGGLE_DOCK"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a9

    .line 5128
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v0, :cond_155

    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ac()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v0, :cond_125

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->af:Z

    if-nez v0, :cond_125

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_125
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    if-eqz v0, :cond_149

    const-string v0, "AUTO"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_149

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iput v7, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_149
    :goto_149
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->requestLayout()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->invalidate()V

    goto/16 :goto_22

    :cond_155
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_182

    iput-boolean v7, p0, Lcom/android/launcher2/Launcher;->p:Z

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, v7}, Lcom/android/launcher2/Hotseat;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setAlpha(F)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->X:Z

    if-nez v0, :cond_182

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lcom/android/launcher2/Launcher;->aT:I

    iget v4, p0, Lcom/android/launcher2/Launcher;->aS:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/launcher2/Workspace;->setPadding(IIII)V

    :cond_182
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v0, :cond_191

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->af:Z

    if-nez v0, :cond_191

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_191
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    if-eqz v0, :cond_149

    const-string v0, "AUTO"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_149

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aL:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_149

    .line 5129
    :cond_1a9
    const-string v0, "RECENT_APPS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1ea

    .line 5130
    :try_start_1b1
    const-string v0, "statusbar"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    const-string v1, "com.android.internal.statusbar.IStatusBarService$Stub"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "toggleRecentApps"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const-string v3, "asInterface"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/os/IBinder;

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1e5
    .catch Ljava/lang/Exception; {:try_start_1b1 .. :try_end_1e5} :catch_1e7

    goto/16 :goto_22

    :catch_1e7
    move-exception v0

    goto/16 :goto_22

    .line 5131
    :cond_1ea
    const-string v0, "APEX_MENU"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f7

    .line 5132
    invoke-direct {p0, v3}, Lcom/android/launcher2/Launcher;->d(Landroid/view/View;)V

    goto/16 :goto_22

    .line 5133
    :cond_1f7
    const-string v0, "APEX_SETTINGS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_204

    .line 5134
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->J()V

    goto/16 :goto_22

    .line 5135
    :cond_204
    const-string v0, "LOCK_DESKTOP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_224

    .line 5136
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v0, :cond_217

    .line 5137
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->af()V

    goto/16 :goto_22

    .line 5138
    :cond_217
    const-string v0, "SHORTCUT"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 5139
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ag()V

    goto/16 :goto_22

    .line 5141
    :cond_224
    const-string v0, "GOTO_SCREEN1"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_247

    .line 5142
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_242

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_242

    .line 5143
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5144
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v7}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5146
    :cond_242
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5147
    :cond_247
    const-string v0, "GOTO_SCREEN2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26a

    .line 5148
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_265

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lt v0, v6, :cond_265

    .line 5149
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5150
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5152
    :cond_265
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5153
    :cond_26a
    const-string v0, "GOTO_SCREEN3"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28e

    .line 5154
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_289

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_289

    .line 5155
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5156
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v6}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5158
    :cond_289
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5159
    :cond_28e
    const-string v0, "GOTO_SCREEN4"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b3

    .line 5160
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_2ae

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2ae

    .line 5161
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5162
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5164
    :cond_2ae
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5165
    :cond_2b3
    const-string v0, "GOTO_SCREEN5"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d8

    .line 5166
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_2d3

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_2d3

    .line 5167
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5168
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5170
    :cond_2d3
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5171
    :cond_2d8
    const-string v0, "GOTO_SCREEN6"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2fd

    .line 5172
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_2f8

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_2f8

    .line 5173
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5174
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5176
    :cond_2f8
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5177
    :cond_2fd
    const-string v0, "GOTO_SCREEN7"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_322

    .line 5178
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_31d

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/4 v1, 0x7

    if-lt v0, v1, :cond_31d

    .line 5179
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5180
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5182
    :cond_31d
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5183
    :cond_322
    const-string v0, "GOTO_SCREEN8"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_348

    .line 5184
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_343

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_343

    .line 5185
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5186
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5188
    :cond_343
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5189
    :cond_348
    const-string v0, "GOTO_SCREEN9"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36f

    .line 5190
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_36a

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_36a

    .line 5191
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 5192
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->o(I)V

    .line 5194
    :cond_36a
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22

    .line 5195
    :cond_36f
    const-string v0, "LAUNCH_APP"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a5

    .line 5196
    const-string v2, "home_key_action_pkg"

    .line 5197
    const-string v1, "home_key_action_act"

    .line 5198
    const-string v0, "home_key_action_intent"

    .line 5199
    const-wide/16 v4, -0x1

    cmp-long v4, p3, v4

    if-eqz v4, :cond_439

    .line 5200
    const-string v4, "SWIPE_UP"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_403

    .line 5201
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_up_pkg_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5202
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_up_act_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5203
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "swipe_up_intent_"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5236
    :cond_3b8
    :goto_3b8
    new-array v4, v6, [I

    .line 5237
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v5, v4}, Lcom/android/launcher2/Workspace;->getLocationOnScreen([I)V

    .line 5238
    const-wide/16 v5, -0x1

    cmp-long v5, p3, v5

    if-nez v5, :cond_499

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v5, v0, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5241
    :goto_3cd
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4a2

    .line 5243
    const/4 v5, 0x0

    :try_start_3d4
    invoke-static {v0, v5}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_3d7
    .catch Ljava/net/URISyntaxException; {:try_start_3d4 .. :try_end_3d7} :catch_4a1

    move-result-object v0

    .line 5248
    :goto_3d8
    if-nez v0, :cond_3de

    .line 5249
    invoke-direct {p0, v2, v1, p3, p4}, Lcom/android/launcher2/Launcher;->b(Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 5251
    :cond_3de
    if-eqz v0, :cond_22

    .line 5252
    new-instance v1, Landroid/graphics/Rect;

    aget v5, v4, v7

    aget v6, v4, v9

    aget v7, v4, v7

    .line 5253
    iget-object v8, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    aget v4, v4, v9

    iget-object v8, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getHeight()I

    move-result v8

    add-int/2addr v4, v8

    invoke-direct {v1, v5, v6, v7, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 5252
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    .line 5254
    invoke-virtual {p0, v3, v0, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto/16 :goto_22

    .line 5204
    :cond_403
    const-string v4, "SWIPE_DOWN"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b8

    .line 5205
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_down_pkg_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 5206
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_down_act_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 5207
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "swipe_down_intent_"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3b8

    .line 5210
    :cond_439
    const-string v4, "PINCH_IN"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_449

    .line 5211
    const-string v2, "pinch_in_action_pkg"

    .line 5212
    const-string v1, "pinch_in_action_act"

    .line 5213
    const-string v0, "pinch_in_action_intent"

    goto/16 :goto_3b8

    .line 5214
    :cond_449
    const-string v4, "SWIPE_UP"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_459

    .line 5215
    const-string v2, "swipe_up_action_pkg"

    .line 5216
    const-string v1, "swipe_up_action_act"

    .line 5217
    const-string v0, "swipe_up_action_intent"

    goto/16 :goto_3b8

    .line 5218
    :cond_459
    const-string v4, "SWIPE_DOWN"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_469

    .line 5219
    const-string v2, "swipe_down_action_pkg"

    .line 5220
    const-string v1, "swipe_down_action_act"

    .line 5221
    const-string v0, "swipe_down_action_intent"

    goto/16 :goto_3b8

    .line 5222
    :cond_469
    const-string v4, "TWO_FINGER_SWIPE_UP"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_479

    .line 5223
    const-string v2, "two_finger_swipe_up_action_pkg"

    .line 5224
    const-string v1, "two_finger_swipe_up_action_act"

    .line 5225
    const-string v0, "two_finger_swipe_up_action_intent"

    goto/16 :goto_3b8

    .line 5226
    :cond_479
    const-string v4, "TWO_FINGER_SWIPE_DOWN"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_489

    .line 5227
    const-string v2, "two_finger_swipe_down_action_pkg"

    .line 5228
    const-string v1, "two_finger_swipe_down_action_act"

    .line 5229
    const-string v0, "two_finger_swipe_down_action_intent"

    goto/16 :goto_3b8

    .line 5230
    :cond_489
    const-string v4, "DOUBLE_TAP"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b8

    .line 5231
    const-string v2, "desktop_double_tap_action_pkg"

    .line 5232
    const-string v1, "desktop_double_tap_action_act"

    .line 5233
    const-string v0, "desktop_double_tap_action_intent"

    goto/16 :goto_3b8

    .line 5239
    :cond_499
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    invoke-virtual {v5, v0, v3}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3cd

    :catch_4a1
    move-exception v0

    :cond_4a2
    move-object v0, v3

    goto/16 :goto_3d8

    .line 5256
    :cond_4a5
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_580

    .line 5257
    const-string v0, "home_key_action_shortcut_intent"

    .line 5258
    const-wide/16 v1, -0x1

    cmp-long v1, p3, v1

    if-eqz v1, :cond_532

    .line 5259
    const-string v1, "SWIPE_UP"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_51a

    .line 5260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_up_shortcut_intent_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5279
    :cond_4cc
    :goto_4cc
    const-wide/16 v1, -0x1

    cmp-long v1, p3, v1

    if-nez v1, :cond_578

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1, v0, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5281
    :goto_4da
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_22

    .line 5283
    const/4 v1, 0x2

    :try_start_4e1
    new-array v1, v1, [I

    .line 5284
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Workspace;->getLocationOnScreen([I)V

    .line 5285
    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 5286
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    aget v3, v1, v3

    const/4 v4, 0x1

    aget v4, v1, v4

    const/4 v5, 0x0

    aget v5, v1, v5

    .line 5287
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    const/4 v6, 0x1

    aget v1, v1, v6

    .line 5288
    iget-object v6, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v6}, Lcom/android/launcher2/Workspace;->getHeight()I

    move-result v6

    add-int/2addr v1, v6

    invoke-direct {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 5286
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    .line 5289
    const/4 v1, 0x0

    const-string v2, "launcher shortcut"

    invoke-virtual {p0, v1, v0, v2}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    :try_end_515
    .catch Ljava/net/URISyntaxException; {:try_start_4e1 .. :try_end_515} :catch_517

    goto/16 :goto_22

    .line 5290
    :catch_517
    move-exception v0

    goto/16 :goto_22

    .line 5261
    :cond_51a
    const-string v1, "SWIPE_DOWN"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4cc

    .line 5262
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_down_shortcut_intent_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4cc

    .line 5265
    :cond_532
    const-string v1, "PINCH_IN"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_53d

    .line 5266
    const-string v0, "pinch_in_action_shortcut_intent"

    goto :goto_4cc

    .line 5267
    :cond_53d
    const-string v1, "SWIPE_UP"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_548

    .line 5268
    const-string v0, "swipe_up_action_shortcut_intent"

    goto :goto_4cc

    .line 5269
    :cond_548
    const-string v1, "SWIPE_DOWN"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_554

    .line 5270
    const-string v0, "swipe_down_action_shortcut_intent"

    goto/16 :goto_4cc

    .line 5271
    :cond_554
    const-string v1, "TWO_FINGER_SWIPE_UP"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_560

    .line 5272
    const-string v0, "two_finger_swipe_up_action_shortcut_intent"

    goto/16 :goto_4cc

    .line 5273
    :cond_560
    const-string v1, "TWO_FINGER_SWIPE_DOWN"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56c

    .line 5274
    const-string v0, "two_finger_swipe_down_action_shortcut_intent"

    goto/16 :goto_4cc

    .line 5275
    :cond_56c
    const-string v1, "DOUBLE_TAP"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4cc

    .line 5276
    const-string v0, "desktop_double_tap_action_shortcut_intent"

    goto/16 :goto_4cc

    .line 5280
    :cond_578
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    invoke-virtual {v1, v0, v3}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4da

    .line 5295
    :cond_580
    const-string v0, "HOME_KEY"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 5296
    invoke-virtual {p0, v9, v3}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_22
.end method

.method public final a(Ljava/util/ArrayList;)V
    .registers 5
    .parameter

    .prologue
    .line 4293
    new-instance v1, Lcom/android/launcher2/em;

    invoke-direct {v1, p0, p1}, Lcom/android/launcher2/em;-><init>(Lcom/android/launcher2/Launcher;Ljava/util/ArrayList;)V

    .line 4303
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 4304
    const v2, 0x7f0d0013

    invoke-virtual {v0, v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 4305
    if-eqz v2, :cond_24

    .line 4306
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 4310
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->post(Ljava/lang/Runnable;)Z

    .line 4311
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    .line 4317
    :goto_23
    return-void

    .line 4315
    :cond_24
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_23
.end method

.method public final a(Ljava/util/ArrayList;II)V
    .registers 19
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4043
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->r()Z

    .line 4046
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 4047
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->at:Landroid/content/SharedPreferences;

    const-string v4, "apps.new.list"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v13

    .line 4049
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 4050
    :goto_12
    move/from16 v0, p2

    move/from16 v1, p3

    if-lt v0, v1, :cond_1c

    .line 4102
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->requestLayout()V

    .line 4103
    return-void

    .line 4051
    :cond_1c
    invoke-virtual/range {p1 .. p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/android/launcher2/di;

    .line 4054
    iget-wide v3, v12, Lcom/android/launcher2/di;->j:J

    const-wide/16 v5, -0x65

    cmp-long v3, v3, v5

    if-nez v3, :cond_2f

    .line 4055
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v3, :cond_34

    .line 4056
    :cond_2f
    iget v3, v12, Lcom/android/launcher2/di;->i:I

    sparse-switch v3, :sswitch_data_ce

    .line 4050
    :cond_34
    :goto_34
    add-int/lit8 p2, p2, 0x1

    goto :goto_12

    :sswitch_37
    move-object v3, v12

    .line 4062
    check-cast v3, Lcom/android/launcher2/jd;

    .line 4063
    iget-object v4, v3, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    .line 4064
    invoke-direct {p0, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/jd;)Landroid/view/View;

    move-result-object v3

    .line 4065
    iget-wide v4, v12, Lcom/android/launcher2/di;->j:J

    iget v6, v12, Lcom/android/launcher2/di;->k:I

    iget v7, v12, Lcom/android/launcher2/di;->l:I

    .line 4066
    iget v8, v12, Lcom/android/launcher2/di;->m:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 4065
    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 4067
    const/4 v4, 0x0

    .line 4068
    monitor-enter v13

    .line 4069
    :try_start_59
    invoke-interface {v13, v14}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_63

    .line 4070
    invoke-interface {v13, v14}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v4

    .line 4068
    :cond_63
    monitor-exit v13
    :try_end_64
    .catchall {:try_start_59 .. :try_end_64} :catchall_84

    .line 4073
    if-eqz v4, :cond_34

    .line 4075
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 4076
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setScaleX(F)V

    .line 4077
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setScaleY(F)V

    .line 4078
    iget v4, v12, Lcom/android/launcher2/di;->k:I

    iput v4, p0, Lcom/android/launcher2/Launcher;->au:I

    .line 4079
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_34

    .line 4080
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_34

    .line 4068
    :catchall_84
    move-exception v2

    monitor-exit v13

    throw v2

    .line 4086
    :sswitch_87
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    move-object v4, v12

    .line 4087
    check-cast v4, Lcom/android/launcher2/cu;

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    .line 4085
    invoke-static {p0, v3, v4}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/Launcher;Landroid/view/ViewGroup;Lcom/android/launcher2/cu;)Lcom/android/launcher2/FolderIcon;

    move-result-object v3

    .line 4088
    iget-wide v4, v12, Lcom/android/launcher2/di;->j:J

    iget v6, v12, Lcom/android/launcher2/di;->k:I

    iget v7, v12, Lcom/android/launcher2/di;->l:I

    .line 4089
    iget v8, v12, Lcom/android/launcher2/di;->m:I

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 4088
    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    goto :goto_34

    :sswitch_a9
    move-object v4, v12

    .line 4092
    check-cast v4, Lcom/anddoes/launcher/au;

    .line 4093
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    iget v5, v4, Lcom/anddoes/launcher/au;->c:I

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 4094
    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 4095
    iget-wide v4, v12, Lcom/android/launcher2/di;->j:J

    iget v6, v12, Lcom/android/launcher2/di;->k:I

    .line 4096
    iget v7, v12, Lcom/android/launcher2/di;->l:I

    iget v8, v12, Lcom/android/launcher2/di;->m:I

    iget v9, v12, Lcom/android/launcher2/di;->n:I

    iget v10, v12, Lcom/android/launcher2/di;->o:I

    .line 4097
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v11

    .line 4095
    invoke-virtual/range {v2 .. v11}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    goto/16 :goto_34

    .line 4056
    nop

    :sswitch_data_ce
    .sparse-switch
        0x0 -> :sswitch_37
        0x1 -> :sswitch_37
        0x2 -> :sswitch_87
        0x3e9 -> :sswitch_a9
    .end sparse-switch
.end method

.method public final a(Ljava/util/ArrayList;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4365
    if-eqz p2, :cond_7

    .line 4366
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->a(Ljava/util/ArrayList;)V

    .line 4369
    :cond_7
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_10

    .line 4370
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Ljava/util/ArrayList;)V

    .line 4374
    :cond_10
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/bk;->a(Ljava/util/ArrayList;)V

    .line 4375
    return-void
.end method

.method public final a(Ljava/util/HashMap;)V
    .registers 3
    .parameter

    .prologue
    .line 4109
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->r()Z

    .line 4110
    sget-object v0, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4111
    sget-object v0, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 4112
    return-void
.end method

.method final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1641
    if-eqz p1, :cond_12

    const v0, 0x7f07028c

    .line 1642
    :goto_5
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1643
    return-void

    .line 1641
    :cond_12
    const v0, 0x7f07028b

    goto :goto_5
.end method

.method final a(ZLjava/lang/Runnable;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x4

    const/high16 v7, 0x3f80

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3497
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_11

    .line 3498
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 3499
    iput-object v6, p0, Lcom/android/launcher2/Launcher;->aO:Landroid/widget/ListPopupWindow;

    .line 3501
    :cond_11
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v3, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-eq v0, v3, :cond_ee

    .line 3502
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v3, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    if-ne v0, v3, :cond_108

    move v0, v1

    .line 3503
    :goto_1e
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3, v2}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3504
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->o()V

    .line 3505
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v4, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-eq v3, v4, :cond_34

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v4, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    if-ne v3, v4, :cond_117

    .line 3506
    :cond_34
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v4, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    if-ne v3, v4, :cond_10b

    .line 3507
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->aI:Lcom/anddoes/launcher/PreviewPane;

    invoke-virtual {v3}, Lcom/anddoes/launcher/PreviewPane;->b()V

    .line 3511
    :goto_3f
    const-string v3, "NONE"

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_112

    .line 3512
    invoke-direct {p0, v2}, Lcom/android/launcher2/Launcher;->f(Z)V

    .line 3528
    :cond_4e
    :goto_4e
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v3, v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Z)V

    .line 3531
    if-eqz p1, :cond_13e

    if-eqz v0, :cond_13e

    move v0, v1

    :goto_58
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    if-eqz v3, :cond_141

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->k:Z

    if-nez v3, :cond_141

    move v3, v1

    :goto_61
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v4, :cond_144

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v4, v4, Lcom/anddoes/launcher/preference/f;->af:Z

    if-nez v4, :cond_144

    move v4, v1

    :goto_6c
    if-nez v3, :cond_70

    if-eqz v4, :cond_e5

    :cond_70
    if-eqz v3, :cond_77

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_77
    if-eqz v4, :cond_82

    iget-boolean v5, p0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v5, :cond_82

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_82
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    if-eqz v5, :cond_9b

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->cancel()V

    if-eqz v3, :cond_92

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setAlpha(F)V

    :cond_92
    if-eqz v4, :cond_99

    iget-object v5, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setAlpha(F)V

    :cond_99
    iput-object v6, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    :cond_9b
    if-eqz v0, :cond_e5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz v3, :cond_b3

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    const-string v5, "alpha"

    new-array v6, v1, [F

    aput v7, v6, v2

    invoke-static {v3, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_b3
    if-eqz v4, :cond_c8

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v3, :cond_c8

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    const-string v4, "alpha"

    new-array v5, v1, [F

    aput v7, v5, v2

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_c8
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v2}, Lcom/android/launcher2/SearchDropTargetBar;->getTransitionInDuration()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->v:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 3534
    :cond_e5
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->N:Landroid/view/View;

    if-eqz v0, :cond_ee

    .line 3535
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->N:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 3539
    :cond_ee
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->c(Z)V

    .line 3542
    sget-object v0, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    .line 3545
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->ac:Z

    .line 3546
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    .line 3549
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 3550
    return-void

    :cond_108
    move v0, v2

    .line 3502
    goto/16 :goto_1e

    .line 3509
    :cond_10b
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3, v2}, Lcom/android/launcher2/Workspace;->a(Z)V

    goto/16 :goto_3f

    .line 3514
    :cond_112
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->f(Z)V

    goto/16 :goto_4e

    .line 3517
    :cond_117
    sget-object v3, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    invoke-direct {p0, v3, p1, p2}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fv;ZLjava/lang/Runnable;)V

    .line 3518
    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->p:Z

    if-nez v3, :cond_126

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->af:Z

    if-eqz v3, :cond_12f

    :cond_126
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v3, :cond_12f

    .line 3519
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3521
    :cond_12f
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    if-eqz v3, :cond_4e

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->k:Z

    if-eqz v3, :cond_4e

    .line 3522
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4e

    :cond_13e
    move v0, v2

    .line 3531
    goto/16 :goto_58

    :cond_141
    move v3, v2

    goto/16 :goto_61

    :cond_144
    move v4, v2

    goto/16 :goto_6c
.end method

.method final a(ZZ)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 3582
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_7

    .line 3600
    :goto_6
    return-void

    .line 3584
    :cond_7
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    new-instance v2, Lcom/android/launcher2/ed;

    invoke-direct {v2, p0, p1}, Lcom/android/launcher2/ed;-><init>(Lcom/android/launcher2/Launcher;Z)V

    .line 3597
    if-eqz p2, :cond_17

    .line 3598
    const/16 v0, 0x258

    .line 3597
    :goto_12
    int-to-long v3, v0

    .line 3584
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_6

    .line 3599
    :cond_17
    const/16 v0, 0x12c

    goto :goto_12
.end method

.method final a(Landroid/view/View;)Z
    .registers 3
    .parameter

    .prologue
    .line 2837
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_14

    if-eqz p1, :cond_14

    .line 2838
    instance-of v0, p1, Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Hotseat;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    .line 2837
    goto :goto_13
.end method

.method final a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/high16 v7, 0x4040

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/high16 v9, 0x4000

    .line 2533
    if-nez p2, :cond_b

    move v0, v1

    .line 2567
    :goto_a
    return v0

    .line 2536
    :cond_b
    const/high16 v0, 0x1000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2541
    :try_start_10
    const-string v0, "com.android.launcher.intent.extra.shortcut.INGORE_LAUNCH_ANIMATION"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    move v0, v1

    .line 2543
    :goto_19
    if-eqz v0, :cond_15e

    .line 2544
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 2545
    const-string v0, "SYSTEM"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 2546
    const-string v0, "JELLY_BEAN"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 2547
    :cond_39
    if-eqz p1, :cond_42

    .line 2548
    invoke-static {p0, p1, p2}, Lcom/anddoes/launcher/r;->a(Landroid/content/Context;Landroid/view/View;Landroid/content/Intent;)V

    :cond_3e
    :goto_3e
    move v0, v2

    .line 2559
    goto :goto_a

    :cond_40
    move v0, v2

    .line 2541
    goto :goto_19

    .line 2550
    :cond_42
    invoke-virtual {p0, p2}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_45
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_45} :catch_46

    goto :goto_3e

    .line 2560
    :catch_46
    move-exception v0

    .line 2561
    const v2, 0x7f070279

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2562
    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Launcher does not have the permission to launch "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2563
    const-string v4, ". Make sure to create a MAIN intent-filter for the corresponding activity or use the exported attribute for this activity. tag="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2564
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2562
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    .line 2567
    goto :goto_a

    .line 2553
    :cond_7b
    :try_start_7b
    invoke-virtual {p0, p2}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V

    .line 2554
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v0, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3e

    const-string v0, "SYSTEM"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3e

    const v3, 0x7f040029

    const v0, 0x7f04002a

    const-string v6, "ICE_CREAM_SANDWICH"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a8

    const v3, 0x7f04000c

    const v0, 0x7f04000d

    :cond_a4
    :goto_a4
    invoke-virtual {p0, v3, v0}, Lcom/android/launcher2/Launcher;->overridePendingTransition(II)V

    goto :goto_3e

    :cond_a8
    const-string v6, "JELLY_BEAN"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13e

    if-nez p1, :cond_bc

    const v0, 0x7f040010

    :goto_b5
    const v3, 0x7f04001a

    move v10, v3

    move v3, v0

    move v0, v10

    goto :goto_a4

    :cond_bc
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v7, v0, v7

    int-to-float v0, v3

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_125

    int-to-float v0, v3

    mul-float v8, v6, v9

    cmpg-float v0, v0, v8

    if-gez v0, :cond_125

    move v3, v4

    :goto_ee
    int-to-float v0, v5

    cmpl-float v0, v0, v7

    if-ltz v0, :cond_12f

    int-to-float v0, v5

    mul-float v6, v7, v9

    cmpg-float v0, v0, v6

    if-gez v0, :cond_12f

    move v0, v4

    :goto_fb
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "jb_open_enter_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_115
    .catch Ljava/lang/SecurityException; {:try_start_7b .. :try_end_115} :catch_46

    move-result-object v0

    :try_start_116
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "anim"

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_123
    .catch Ljava/lang/Exception; {:try_start_116 .. :try_end_123} :catch_138
    .catch Ljava/lang/SecurityException; {:try_start_116 .. :try_end_123} :catch_46

    move-result v0

    goto :goto_b5

    :cond_125
    int-to-float v0, v3

    mul-float v3, v6, v9

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_165

    const/4 v0, 0x3

    move v3, v0

    goto :goto_ee

    :cond_12f
    int-to-float v0, v5

    mul-float v4, v7, v9

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_163

    const/4 v0, 0x3

    goto :goto_fb

    :catch_138
    move-exception v0

    const v0, 0x7f040010

    goto/16 :goto_b5

    :cond_13e
    :try_start_13e
    const-string v4, "SLIDE_LEFT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14e

    const v3, 0x7f040025

    const v0, 0x7f040026

    goto/16 :goto_a4

    :cond_14e
    const-string v4, "SLIDE_RIGHT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a4

    const v3, 0x7f040027

    const v0, 0x7f040028

    goto/16 :goto_a4

    .line 2557
    :cond_15e
    invoke-virtual {p0, p2}, Lcom/android/launcher2/Launcher;->startActivity(Landroid/content/Intent;)V
    :try_end_161
    .catch Ljava/lang/SecurityException; {:try_start_13e .. :try_end_161} :catch_46

    goto/16 :goto_3e

    :cond_163
    move v0, v2

    goto :goto_fb

    :cond_165
    move v3, v2

    goto :goto_ee
.end method

.method public final b(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1556
    if-nez p1, :cond_25

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->ad:Z

    .line 1557
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    .line 1561
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->ad:Z

    if-eqz v0, :cond_24

    .line 1562
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->b()V

    .line 1566
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Z()V

    .line 1567
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 1569
    :cond_24
    return-void

    :cond_25
    move v0, v1

    .line 1556
    goto :goto_4
.end method

.method final b(Landroid/content/ComponentName;)V
    .registers 7
    .parameter

    .prologue
    .line 2522
    if-nez p1, :cond_3

    .line 2530
    :goto_2
    return-void

    .line 2525
    :cond_3
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2526
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DELETE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "package:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2527
    const/high16 v0, 0x1080

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2529
    const/4 v0, 0x0

    const-string v2, "uninstall"

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public final b(Lcom/android/launcher2/fz;)V
    .registers 13
    .parameter

    .prologue
    .line 4120
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->r()Z

    .line 4122
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 4128
    iget v1, p1, Lcom/android/launcher2/fz;->a:I

    .line 4129
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->G:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v2, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v10

    .line 4134
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    invoke-virtual {v2, p0, v1, v10}, Lcom/android/launcher2/fx;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v1

    iput-object v1, p1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    .line 4136
    iget-object v1, p1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    invoke-virtual {v1, p1}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    .line 4137
    iget-boolean v1, p1, Lcom/android/launcher2/fz;->e:Z

    if-nez v1, :cond_21

    invoke-virtual {p1, p0}, Lcom/android/launcher2/fz;->a(Lcom/android/launcher2/Launcher;)V

    .line 4139
    :cond_21
    iget-object v1, p1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    iget-wide v2, p1, Lcom/android/launcher2/fz;->j:J

    iget v4, p1, Lcom/android/launcher2/fz;->k:I

    iget v5, p1, Lcom/android/launcher2/fz;->l:I

    .line 4140
    iget v6, p1, Lcom/android/launcher2/fz;->m:I

    iget v7, p1, Lcom/android/launcher2/fz;->n:I

    iget v8, p1, Lcom/android/launcher2/fz;->o:I

    const/4 v9, 0x0

    .line 4139
    invoke-virtual/range {v0 .. v9}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIIIZ)V

    .line 4141
    iget-object v1, p1, Lcom/android/launcher2/fz;->f:Landroid/appwidget/AppWidgetHostView;

    invoke-direct {p0, v1, v10}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 4143
    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->requestLayout()V

    .line 4149
    return-void
.end method

.method public final b(Ljava/util/ArrayList;)V
    .registers 5
    .parameter

    .prologue
    .line 4325
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->r()Z

    .line 4327
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_c

    .line 4328
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Ljava/util/ArrayList;)V

    .line 4331
    :cond_c
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aV:Z

    if-nez v0, :cond_1a

    .line 4332
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 4341
    :cond_1a
    :goto_1a
    return-void

    .line 4332
    :cond_1b
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 4333
    iget-object v2, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    if-eqz v2, :cond_14

    .line 4334
    const-string v2, "com.anddoes.launcher.pro"

    iget-object v0, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 4335
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ApexService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1a
.end method

.method final b(Z)V
    .registers 5
    .parameter

    .prologue
    const/high16 v1, 0x10

    .line 2905
    if-eqz p1, :cond_20

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v0, :cond_20

    move v0, v1

    .line 2906
    :goto_b
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v2, v1

    .line 2908
    if-eq v0, v2, :cond_1f

    .line 2909
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setFlags(II)V

    .line 2911
    :cond_1f
    return-void

    .line 2905
    :cond_20
    const/4 v0, 0x0

    goto :goto_b
.end method

.method final b()Z
    .registers 2

    .prologue
    .line 641
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v0}, Lcom/android/launcher2/gb;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x1

    goto :goto_9
.end method

.method final b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2571
    if-nez p2, :cond_4

    .line 2583
    :goto_3
    return v0

    .line 2576
    :cond_4
    :try_start_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    :try_end_7
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_7} :catch_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_7} :catch_33

    move-result v0

    goto :goto_3

    .line 2577
    :catch_9
    move-exception v1

    .line 2578
    const v2, 0x7f070279

    invoke-static {p0, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2579
    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to launch. tag="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 2580
    :catch_33
    move-exception v1

    .line 2581
    const-string v2, "Launcher"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to launch. tag="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " intent="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method

.method public final c(I)V
    .registers 4
    .parameter

    .prologue
    .line 4152
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->as:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4153
    return-void
.end method

.method public final c(Ljava/util/ArrayList;)V
    .registers 3
    .parameter

    .prologue
    .line 4349
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->r()Z

    .line 4350
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_c

    .line 4351
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->b(Ljava/util/ArrayList;)V

    .line 4354
    :cond_c
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_15

    .line 4355
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->c(Ljava/util/ArrayList;)V

    .line 4357
    :cond_15
    return-void
.end method

.method public final c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 3493
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 3494
    return-void
.end method

.method public final d()Lcom/android/launcher2/fx;
    .registers 2

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    return-object v0
.end method

.method public final d(Z)V
    .registers 6
    .parameter

    .prologue
    .line 4432
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->aa()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4433
    if-eqz p1, :cond_c

    .line 4434
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->A()V

    .line 4445
    :cond_b
    :goto_b
    return-void

    .line 4437
    :cond_c
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    new-instance v1, Lcom/android/launcher2/en;

    invoke-direct {v1, p0}, Lcom/android/launcher2/en;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 4442
    const-wide/16 v2, 0x1f4

    .line 4437
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_b
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2290
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_cf

    .line 2291
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_de

    .line 2308
    :cond_e
    :goto_e
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_12
    :pswitch_12
    :sswitch_12
    return v0

    .line 2295
    :sswitch_13
    const-string v1, "debug.launcher2.dumpstate"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_e

    .line 2296
    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BEGIN launcher2 dump state for launcher "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mSavedState="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->Q:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mWorkspaceLoading="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->T:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mRestoring="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->V:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mWaitingForResult="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->W:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mSavedInstanceState="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->Z:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Launcher"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sFolders.size="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v1}, Lcom/android/launcher2/gb;->i()V

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v1, :cond_c6

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->i()V

    :cond_c6
    const-string v1, "Launcher"

    const-string v2, "END launcher2 dump state"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_12

    .line 2301
    :cond_cf
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_e

    .line 2302
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_e8

    goto/16 :goto_e

    .line 2291
    :sswitch_data_de
    .sparse-switch
        0x3 -> :sswitch_12
        0x19 -> :sswitch_13
    .end sparse-switch

    .line 2302
    :pswitch_data_e8
    .packed-switch 0x3
        :pswitch_12
    .end packed-switch
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 3947
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 3948
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    .line 3949
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 3950
    const v2, 0x7f070273

    invoke-virtual {p0, v2}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3951
    return v0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4468
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 4469
    const-string v0, " "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4470
    const-string v0, "Debug logs: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4471
    const/4 v0, 0x0

    move v1, v0

    :goto_f
    sget-object v0, Lcom/android/launcher2/Launcher;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_18

    .line 4474
    return-void

    .line 4472
    :cond_18
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "  "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/android/launcher2/Launcher;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 4471
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f
.end method

.method public final e()Lcom/android/launcher2/gb;
    .registers 2

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    return-object v0
.end method

.method final f()V
    .registers 2

    .prologue
    .line 1655
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->closeAllPanels()V

    .line 1656
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->dismissDialog(I)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_f

    .line 1662
    :goto_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->W:Z

    .line 1663
    return-void

    :catch_f
    move-exception v0

    goto :goto_b
.end method

.method public final g()Lcom/android/launcher2/bk;
    .registers 2

    .prologue
    .line 1805
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    return-object v0
.end method

.method public final h()V
    .registers 3

    .prologue
    .line 2766
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 2767
    if-eqz v0, :cond_15

    .line 2768
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->a()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2769
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->b()V

    .line 2771
    :cond_11
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Folder;Z)V

    .line 2773
    :cond_15
    return-void
.end method

.method public final i()Lcom/android/launcher2/Hotseat;
    .registers 2

    .prologue
    .line 2841
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    return-object v0
.end method

.method final j()Lcom/android/launcher2/SearchDropTargetBar;
    .registers 2

    .prologue
    .line 2844
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    return-object v0
.end method

.method public final k()Lcom/android/launcher2/Workspace;
    .registers 2

    .prologue
    .line 2863
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    return-object v0
.end method

.method public final l()Z
    .registers 3

    .prologue
    .line 2868
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method public final m()Z
    .registers 2

    .prologue
    .line 2872
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-static {}, Lcom/android/launcher2/Hotseat;->c()Z

    move-result v0

    return v0
.end method

.method final n()V
    .registers 3

    .prologue
    .line 2895
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 2896
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_1e

    .line 2897
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->d()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 2898
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_1e

    .line 2899
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 2902
    :cond_1e
    return-void
.end method

.method final o()V
    .registers 4

    .prologue
    .line 3571
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 3572
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3573
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->o()V

    .line 3574
    sget-object v0, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fv;ZLjava/lang/Runnable;)V

    .line 3575
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3576
    sget-object v0, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    .line 3578
    :cond_1f
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    const/4 v9, 0x0

    .line 704
    .line 705
    const/4 v0, 0x5

    if-ne p1, v0, :cond_1e

    move v0, v8

    .line 706
    :goto_8
    iput-boolean v4, p0, Lcom/android/launcher2/Launcher;->W:Z

    .line 708
    const/16 v2, 0xb

    if-ne p1, v2, :cond_37

    .line 709
    if-eqz p3, :cond_20

    .line 710
    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 711
    :goto_16
    iput v1, p0, Lcom/android/launcher2/Launcher;->aF:I

    .line 712
    if-nez p2, :cond_23

    .line 713
    invoke-direct {p0, v4, v0, v9}, Lcom/android/launcher2/Launcher;->a(IILjava/lang/String;)V

    .line 788
    :cond_1d
    :goto_1d
    return-void

    :cond_1e
    move v0, v4

    .line 705
    goto :goto_8

    .line 710
    :cond_20
    iget v0, p0, Lcom/android/launcher2/Launcher;->aF:I

    goto :goto_16

    .line 714
    :cond_23
    if-ne p2, v1, :cond_1d

    .line 715
    if-eq v0, v1, :cond_2f

    .line 716
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->G:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v1, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    .line 718
    :cond_2f
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {p0, v0, v1, v9, v2}, Lcom/android/launcher2/Launcher;->a(ILcom/android/launcher2/di;Landroid/appwidget/AppWidgetHostView;Landroid/appwidget/AppWidgetProviderInfo;)V

    goto :goto_1d

    .line 724
    :cond_37
    if-eqz v0, :cond_58

    .line 725
    if-eqz p3, :cond_41

    .line 726
    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 727
    :cond_41
    if-gez v1, :cond_4e

    .line 728
    const-string v0, "Launcher"

    const-string v2, "Error: appWidgetId (EXTRA_APPWIDGET_ID) was not returned from the \\widget configuration activity."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-direct {p0, v4, v1, v9}, Lcom/android/launcher2/Launcher;->a(IILjava/lang/String;)V

    goto :goto_1d

    .line 732
    :cond_4e
    const-string v0, "custom_widget"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 733
    invoke-direct {p0, p2, v1, v0}, Lcom/android/launcher2/Launcher;->a(IILjava/lang/String;)V

    goto :goto_1d

    .line 743
    :cond_58
    if-ne p2, v1, :cond_122

    const/16 v0, 0xc

    if-ne p1, v0, :cond_122

    .line 744
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v3, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    if-eqz v0, :cond_1d

    iget-object v0, v3, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    if-eqz v0, :cond_1d

    iget-object v0, v3, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    instance-of v0, v0, Lcom/android/launcher2/jd;

    if-eqz v0, :cond_e8

    iget-object v0, v3, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    check-cast v0, Lcom/android/launcher2/jd;

    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_b1

    iput-boolean v8, v0, Lcom/android/launcher2/jd;->c:Z

    iput-object v9, v0, Lcom/android/launcher2/jd;->e:Landroid/content/Intent$ShortcutIconResource;

    iput-object v1, v0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    :cond_82
    :goto_82
    iput v8, v0, Lcom/android/launcher2/jd;->i:I

    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    const-string v1, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    iput-object v1, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;)V

    iget-object v1, v3, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    check-cast v1, Lcom/android/launcher2/BubbleTextView;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    iget-wide v4, v0, Lcom/android/launcher2/jd;->j:J

    invoke-static {p0, v4, v5}, Lcom/anddoes/launcher/v;->a(Lcom/android/launcher2/Launcher;J)Z

    move-result v4

    invoke-virtual {v1, v0, v2, v4}, Lcom/android/launcher2/BubbleTextView;->a(Lcom/android/launcher2/jd;Lcom/android/launcher2/da;Z)V

    invoke-virtual {v1}, Lcom/android/launcher2/BubbleTextView;->invalidate()V

    :cond_ab
    :goto_ab
    iput-object v9, v3, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    iput-object v9, v3, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    goto/16 :goto_1d

    :cond_b1
    const-string v1, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent$ShortcutIconResource;

    if-eqz v1, :cond_82

    :try_start_bb
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v5, v1, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_c4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_bb .. :try_end_c4} :catch_dc

    move-result-object v2

    :goto_c5
    if-eqz v2, :cond_d7

    iget-object v5, v1, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    invoke-virtual {v2, v5, v9, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_d7

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_df

    iput-object v9, v0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    :cond_d7
    :goto_d7
    iput-boolean v4, v0, Lcom/android/launcher2/jd;->c:Z

    iput-object v1, v0, Lcom/android/launcher2/jd;->e:Landroid/content/Intent$ShortcutIconResource;

    goto :goto_82

    :catch_dc
    move-exception v2

    move-object v2, v9

    goto :goto_c5

    :cond_df
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v0, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    goto :goto_d7

    :cond_e8
    iget-object v0, v3, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    instance-of v0, v0, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_ab

    iget-object v0, v3, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    check-cast v0, Lcom/android/launcher2/cu;

    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/cu;->a(Ljava/lang/CharSequence;)V

    invoke-static {p0, v0}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;)V

    iget-object v1, v3, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    check-cast v1, Lcom/android/launcher2/FolderIcon;

    iget-object v2, v1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    if-eqz v2, :cond_11b

    iget-object v2, v1, Lcom/android/launcher2/FolderIcon;->a:Lcom/android/launcher2/Folder;

    iget-object v0, v0, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Folder;->setText(Ljava/lang/String;)V

    :cond_11b
    invoke-virtual {v1}, Lcom/android/launcher2/FolderIcon;->b()V

    invoke-virtual {v1}, Lcom/android/launcher2/FolderIcon;->invalidate()V

    goto :goto_ab

    .line 746
    :cond_122
    const/16 v0, 0xe

    if-ne p1, v0, :cond_1ad

    .line 747
    if-ne p2, v1, :cond_1a3

    .line 748
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    instance-of v0, v0, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    move-object v7, v0

    check-cast v7, Lcom/android/launcher2/cu;

    sget-object v0, Lcom/anddoes/launcher/ui/MultiPickerActivity;->c:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v2}, Lcom/android/launcher2/LauncherApplication;->b()Lcom/android/launcher2/gb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher2/gb;->j()Lcom/android/launcher2/d;

    move-result-object v2

    iget-object v2, v2, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16a

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_164
    :goto_164
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_174

    :cond_16a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iput-object v9, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iput-object v9, v0, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    goto/16 :goto_1d

    :cond_174
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    array-length v2, v10

    move v1, v4

    :goto_17c
    if-ge v1, v2, :cond_164

    aget-object v3, v10, v1

    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a0

    invoke-virtual {v0}, Lcom/android/launcher2/h;->a()Lcom/android/launcher2/jd;

    move-result-object v1

    iput v8, v1, Lcom/android/launcher2/jd;->n:I

    iput v8, v1, Lcom/android/launcher2/jd;->o:I

    invoke-virtual {v7, v1}, Lcom/android/launcher2/cu;->a(Lcom/android/launcher2/di;)V

    iget-wide v2, v7, Lcom/android/launcher2/cu;->h:J

    iget v5, v1, Lcom/android/launcher2/jd;->l:I

    iget v6, v1, Lcom/android/launcher2/jd;->m:I

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    goto :goto_164

    :cond_1a0
    add-int/lit8 v1, v1, 0x1

    goto :goto_17c

    .line 750
    :cond_1a3
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iput-object v9, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    .line 751
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iput-object v9, v0, Lcom/android/launcher2/LauncherApplication;->h:Landroid/view/View;

    goto/16 :goto_1d

    .line 754
    :cond_1ad
    if-ne p2, v1, :cond_1f7

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-wide v2, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v5, -0x1

    cmp-long v0, v2, v5

    if-eqz v0, :cond_1f7

    .line 755
    new-instance v0, Lcom/android/launcher2/fu;

    invoke-direct {v0, v4}, Lcom/android/launcher2/fu;-><init>(B)V

    .line 756
    iput p1, v0, Lcom/android/launcher2/fu;->a:I

    .line 757
    iput-object p3, v0, Lcom/android/launcher2/fu;->b:Landroid/content/Intent;

    .line 758
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-wide v1, v1, Lcom/android/launcher2/di;->j:J

    iput-wide v1, v0, Lcom/android/launcher2/fu;->c:J

    .line 759
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->k:I

    iput v1, v0, Lcom/android/launcher2/fu;->d:I

    .line 760
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->l:I

    iput v1, v0, Lcom/android/launcher2/fu;->e:I

    .line 761
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->m:I

    iput v1, v0, Lcom/android/launcher2/fu;->f:I

    .line 762
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v1

    if-eqz v1, :cond_1f2

    .line 763
    sget-object v1, Lcom/android/launcher2/Launcher;->aD:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v4

    .line 784
    :goto_1e6
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v1}, Lcom/android/launcher2/DragLayer;->b()V

    .line 786
    if-eqz p2, :cond_229

    .line 787
    :goto_1ed
    invoke-virtual {p0, v8, v0}, Lcom/android/launcher2/Launcher;->a(ZZ)V

    goto/16 :goto_1d

    .line 765
    :cond_1f2
    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fu;)Z

    move-result v0

    goto :goto_1e6

    .line 767
    :cond_1f7
    const/16 v0, 0x9

    if-ne p1, v0, :cond_227

    .line 768
    if-eqz p3, :cond_220

    .line 769
    const-string v0, "appWidgetId"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 770
    :goto_203
    if-eq v0, v1, :cond_227

    .line 771
    if-ne p2, v8, :cond_222

    .line 772
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/android/launcher2/Launcher;->a(ILandroid/content/ComponentName;)Z

    move-result v2

    if-eqz v2, :cond_227

    .line 773
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 774
    const-string v3, "appWidgetId"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 775
    invoke-virtual {p0, p1, v1, v2}, Lcom/android/launcher2/Launcher;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_1d

    :cond_220
    move v0, v1

    .line 769
    goto :goto_203

    .line 778
    :cond_222
    if-nez p2, :cond_227

    .line 780
    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->f(I)V

    :cond_227
    move v0, v4

    goto :goto_1e6

    :cond_229
    move v8, v4

    .line 786
    goto :goto_1ed
.end method

.method public onAttachedToWindow()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 1531
    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    .line 1534
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1535
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1536
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1537
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aE:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1539
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->ae:Z

    .line 1540
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->ad:Z

    .line 1541
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 2313
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-eq v0, v1, :cond_12

    .line 2314
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_1b

    .line 2315
    :cond_12
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2329
    :goto_17
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->W()V

    .line 2330
    return-void

    .line 2316
    :cond_1b
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    if-eqz v0, :cond_37

    .line 2317
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v0

    .line 2318
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->a()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 2319
    invoke-virtual {v0}, Lcom/android/launcher2/Folder;->b()V

    goto :goto_17

    .line 2321
    :cond_33
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    goto :goto_17

    .line 2324
    :cond_37
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->G()V

    .line 2327
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->E()V

    goto :goto_17
.end method

.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2357
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->W()V

    .line 2358
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_c

    .line 2413
    :cond_b
    :goto_b
    return-void

    .line 2363
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 2367
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->k()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2371
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 2372
    instance-of v0, v1, Lcom/android/launcher2/jd;

    if-eqz v0, :cond_b4

    .line 2373
    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_2d

    move-object v0, p1

    .line 2374
    check-cast v0, Lcom/android/launcher2/BubbleTextView;

    .line 2375
    iget-boolean v0, v0, Lcom/android/launcher2/BubbleTextView;->b:Z

    if-nez v0, :cond_b

    :cond_2d
    move-object v0, v1

    .line 2380
    check-cast v0, Lcom/android/launcher2/jd;

    .line 2381
    iget-object v4, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    .line 2382
    iget-object v5, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v5, v5, Lcom/anddoes/launcher/preference/f;->ax:Z

    if-eqz v5, :cond_78

    .line 2383
    iget-wide v5, v0, Lcom/android/launcher2/jd;->j:J

    const-wide/16 v7, -0x65

    cmp-long v0, v5, v7

    if-nez v0, :cond_78

    .line 2384
    if-eqz v4, :cond_b2

    const-string v0, "com.anddoes.launcher.ACTION"

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b2

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_b2

    const-class v5, Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b2

    const-string v0, "APP_DRAWER"

    const-string v5, "LAUNCHER_ACTION"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b2

    move v0, v2

    :goto_73
    if-eqz v0, :cond_78

    .line 2385
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ah()V

    .line 2388
    :cond_78
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 2389
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2390
    new-instance v5, Landroid/graphics/Rect;

    aget v6, v0, v3

    aget v7, v0, v2

    .line 2391
    aget v3, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v3, v8

    aget v0, v0, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v8

    add-int/2addr v0, v8

    invoke-direct {v5, v6, v7, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2390
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    .line 2393
    invoke-virtual {p0, p1, v4, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    move-result v0

    .line 2394
    if-eqz v0, :cond_a1

    .line 2395
    invoke-virtual {p0, v4}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;)V

    .line 2397
    :cond_a1
    if-eqz v0, :cond_b

    instance-of v0, p1, Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_b

    .line 2398
    check-cast p1, Lcom/android/launcher2/BubbleTextView;

    iput-object p1, p0, Lcom/android/launcher2/Launcher;->aA:Lcom/android/launcher2/BubbleTextView;

    .line 2399
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aA:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/BubbleTextView;->setStayPressed(Z)V

    goto/16 :goto_b

    :cond_b2
    move v0, v3

    .line 2384
    goto :goto_73

    .line 2401
    :cond_b4
    instance-of v0, v1, Lcom/android/launcher2/cu;

    if-eqz v0, :cond_129

    .line 2402
    instance-of v0, p1, Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_b

    .line 2403
    check-cast p1, Lcom/android/launcher2/FolderIcon;

    .line 2404
    iget-object v0, p1, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->a(Ljava/lang/Object;)Lcom/android/launcher2/Folder;

    move-result-object v1

    iget-boolean v4, v0, Lcom/android/launcher2/cu;->a:Z

    if-eqz v4, :cond_102

    if-nez v1, :cond_102

    const-string v4, "Launcher"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Folder info marked as open, but associated folder is not open. Screen: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v0, Lcom/android/launcher2/cu;->k:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/launcher2/cu;->l:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v0, Lcom/android/launcher2/cu;->m:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, v0, Lcom/android/launcher2/cu;->a:Z

    :cond_102
    iget-boolean v0, v0, Lcom/android/launcher2/cu;->a:Z

    if-nez v0, :cond_10e

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/FolderIcon;)V

    goto/16 :goto_b

    :cond_10e
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->f(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Folder;Z)V

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v1

    if-eq v0, v1, :cond_b

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/FolderIcon;)V

    goto/16 :goto_b

    .line 2406
    :cond_129
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->N:Landroid/view/View;

    if-ne p1, v0, :cond_b

    .line 2407
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-eqz v0, :cond_139

    .line 2408
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    goto/16 :goto_b

    .line 2410
    :cond_139
    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->onClickAllAppsButton(Landroid/view/View;)V

    goto/16 :goto_b
.end method

.method public onClickAllAppsButton(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 2466
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->e(Z)V

    .line 2467
    return-void
.end method

.method public onClickMenuButton(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 2475
    const-string v0, "SHOP"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2476
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    if-eqz v0, :cond_18

    .line 2477
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ah:Landroid/content/Intent;

    const-string v1, "play store"

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 2500
    :cond_18
    :goto_18
    return-void

    .line 2479
    :cond_19
    const-string v0, "MENU"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 2480
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->d(Landroid/view/View;)V

    goto :goto_18

    .line 2482
    :cond_29
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v1, "drawer_tab_icon_intent"

    invoke-virtual {v0, v1, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2483
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_46

    .line 2485
    const/4 v1, 0x0

    :try_start_3a
    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 2486
    const-string v1, "drawer tab icon"

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z
    :try_end_43
    .catch Ljava/net/URISyntaxException; {:try_start_3a .. :try_end_43} :catch_44

    goto :goto_18

    .line 2487
    :catch_44
    move-exception v0

    goto :goto_18

    .line 2491
    :cond_46
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v1, "drawer_tab_icon_pkg"

    invoke-virtual {v0, v1, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2492
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v2, "drawer_tab_icon_act"

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2493
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_74

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_74

    .line 2494
    const-string v1, "drawer_tab_icon_pkg"

    const-string v2, "drawer_tab_icon_act"

    const-wide/16 v3, -0x1

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/android/launcher2/Launcher;->b(Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, p1, v1, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_18

    .line 2496
    :cond_74
    invoke-direct {p0, p1}, Lcom/android/launcher2/Launcher;->d(Landroid/view/View;)V

    goto :goto_18
.end method

.method public onClickSearchButton(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 2428
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 2430
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onSearchRequested()Z

    .line 2431
    return-void
.end method

.method public onClickVoiceButton(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x1000

    .line 2439
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 2443
    :try_start_7
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 2442
    invoke-virtual {v0}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 2445
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.WEB_SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2446
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2447
    if-eqz v0, :cond_28

    .line 2448
    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2450
    :cond_28
    const/4 v0, 0x0

    const-string v2, "onClickVoiceButton"

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 2451
    const v0, 0x7f040002

    const v1, 0x7f040003

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->overridePendingTransition(II)V
    :try_end_37
    .catch Landroid/content/ActivityNotFoundException; {:try_start_7 .. :try_end_37} :catch_38

    .line 2457
    :goto_37
    return-void

    .line 2453
    :catch_38
    move-exception v0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2454
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2455
    const-string v1, "onClickVoiceButton"

    invoke-virtual {p0, v4, v0, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    goto :goto_37
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 5559
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 5560
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->j(Z)V

    .line 5561
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter

    .prologue
    const-wide/16 v10, -0x1

    const v9, 0x7f0d0012

    const/4 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 409
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 410
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/anddoes/launcher/ApexService;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 411
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->aW:Z

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3f8

    move v0, v1

    :goto_29
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    new-instance v0, Lcom/anddoes/launcher/preference/f;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/f;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/f;->a()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->ba:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/anddoes/launcher/v;->b(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Lcom/anddoes/launcher/c/l;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    invoke-direct {v0, p0, v3}, Lcom/anddoes/launcher/c/l;-><init>(Landroid/content/Context;Lcom/anddoes/launcher/preference/f;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    new-instance v0, Lcom/anddoes/launcher/preference/a;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->l:Lcom/anddoes/launcher/preference/a;

    new-instance v0, Lcom/anddoes/launcher/preference/bb;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/bb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    new-instance v0, Lcom/anddoes/launcher/preference/bc;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/bc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    new-instance v0, Lcom/anddoes/launcher/preference/c;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->o:Lcom/anddoes/launcher/preference/c;

    const-string v0, "NEVER"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_81

    const-string v0, "LANDSCAPE_ONLY"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_83

    invoke-static {p0}, Lcom/android/launcher2/LauncherApplication;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_83

    :cond_81
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->k:Z

    .line 412
    :cond_83
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->L()Z

    .line 414
    new-instance v0, Lcom/anddoes/launcher/s;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/s;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aB:Lcom/anddoes/launcher/s;

    .line 416
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    .line 417
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/android/launcher2/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->at:Landroid/content/SharedPreferences;

    .line 419
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/LauncherApplication;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/gb;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    .line 420
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iput-object v3, v0, Lcom/android/launcher2/gb;->l:Lcom/anddoes/launcher/c/l;

    .line 421
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->a()Lcom/android/launcher2/da;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    .line 422
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->ab:Lcom/android/launcher2/da;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iput-object v3, v0, Lcom/android/launcher2/da;->b:Lcom/anddoes/launcher/c/l;

    .line 423
    new-instance v0, Lcom/android/launcher2/bk;

    invoke-direct {v0, p0}, Lcom/android/launcher2/bk;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    .line 424
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    .line 426
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->G:Landroid/appwidget/AppWidgetManager;

    .line 427
    new-instance v0, Lcom/android/launcher2/fx;

    invoke-direct {v0, p0}, Lcom/android/launcher2/fx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    .line 428
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    new-instance v3, Lcom/android/launcher2/es;

    invoke-direct {v3, p0}, Lcom/android/launcher2/es;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 437
    const-wide/16 v4, 0xbb8

    .line 428
    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 438
    invoke-static {p0}, Lcom/anddoes/launcher/j;->a(Landroid/content/Context;)Lcom/anddoes/launcher/j;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->f:Lcom/anddoes/launcher/j;

    .line 439
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aP:Landroid/os/Vibrator;

    .line 444
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->U:Z

    .line 451
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Q()V

    .line 452
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_3fb

    const v0, 0x7f03001c

    :goto_ff
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->setContentView(I)V

    .line 454
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->R()V

    .line 456
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Lcom/android/launcher2/LauncherProvider;->a:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->A:Landroid/database/ContentObserver;

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 458
    iput-object p1, p0, Lcom/android/launcher2/Launcher;->Q:Landroid/os/Bundle;

    .line 461
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->Q:Landroid/os/Bundle;

    if-eqz v3, :cond_1c0

    const-string v0, "launcher.is_landscape"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v4, "launcher.current_time"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-boolean v6, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eq v0, v6, :cond_149

    const-wide/16 v6, 0x1388

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_149

    const-string v0, "launcher.state"

    sget-object v4, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    invoke-virtual {v4}, Lcom/android/launcher2/fv;->ordinal()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->d(I)Lcom/android/launcher2/fv;

    move-result-object v0

    sget-object v4, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-ne v0, v4, :cond_149

    sget-object v0, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    :cond_149
    const-string v0, "launcher.current_screen"

    invoke-virtual {v3, v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_156

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4, v0}, Lcom/android/launcher2/Workspace;->setCurrentPage(I)V

    :cond_156
    const-string v0, "launcher.add_container"

    invoke-virtual {v3, v0, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v0, "launcher.add_screen"

    invoke-virtual {v3, v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    cmp-long v6, v4, v10

    if-eqz v6, :cond_1a6

    if-ltz v0, :cond_1a6

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput-wide v4, v6, Lcom/android/launcher2/di;->j:J

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iput v0, v4, Lcom/android/launcher2/di;->k:I

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const-string v4, "launcher.add_cell_x"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v0, Lcom/android/launcher2/di;->l:I

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const-string v4, "launcher.add_cell_y"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v0, Lcom/android/launcher2/di;->m:I

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const-string v4, "launcher.add_span_x"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v0, Lcom/android/launcher2/di;->n:I

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    const-string v4, "launcher.add_span_y"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v0, Lcom/android/launcher2/di;->o:I

    const-string v0, "launcher.add_widget_info"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->W:Z

    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->V:Z

    :cond_1a6
    const-string v0, "launcher.rename_folder"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1c0

    const-string v0, "launcher.rename_folder_id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    sget-object v0, Lcom/android/launcher2/Launcher;->ag:Ljava/util/HashMap;

    invoke-static {p0, v0, v3, v4}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Ljava/util/HashMap;J)Lcom/android/launcher2/cu;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->L:Lcom/android/launcher2/cu;

    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->V:Z

    .line 464
    :cond_1c0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_1c9

    .line 465
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Z)V

    .line 472
    :cond_1c9
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->V:Z

    if-nez v0, :cond_1da

    .line 473
    sget-boolean v0, Lcom/android/launcher2/Launcher;->Y:Z

    if-eqz v0, :cond_400

    .line 476
    iput v8, p0, Lcom/android/launcher2/Launcher;->aX:I

    .line 482
    :goto_1d3
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    iget v3, p0, Lcom/android/launcher2/Launcher;->aX:I

    invoke-virtual {v0, v1, v3}, Lcom/android/launcher2/gb;->a(ZI)V

    .line 485
    :cond_1da
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v0}, Lcom/android/launcher2/gb;->g()Z

    move-result v0

    if-nez v0, :cond_1f2

    .line 486
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 487
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->a:Landroid/view/LayoutInflater;

    const v4, 0x7f03000b

    invoke-virtual {v3, v4, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 491
    :cond_1f2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    .line 492
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-static {v0, v2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 494
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 495
    iget-object v3, p0, Lcom/android/launcher2/Launcher;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/android/launcher2/Launcher;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 497
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->P()V

    .line 500
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->d(Z)V

    .line 502
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Launcher;->s:I

    new-instance v0, Lcom/anddoes/launcher/w;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/w;-><init>(Lcom/android/launcher2/Launcher;)V

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aQ:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->a(Z)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aS:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->b(Z)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aU:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->c(Z)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->aW:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->d(Z)V

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->k:Z

    if-eqz v0, :cond_26f

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingLeft()I

    move-result v0

    const v1, 0x7f0d0037

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_25e

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    move v0, v2

    :cond_25e
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v3}, Lcom/android/launcher2/Workspace;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/android/launcher2/Workspace;->setPadding(IIII)V

    :cond_26f
    invoke-virtual {p0, v9}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    if-eqz v0, :cond_28a

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aL:Landroid/view/ViewGroup$LayoutParams;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    const-string v3, "scroll_indicator_holo"

    invoke-virtual {v0, v1, v2, v3}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    :cond_28a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0, v9}, Lcom/android/launcher2/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    if-eqz v0, :cond_2a7

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aN:Landroid/view/ViewGroup$LayoutParams;

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    const-string v3, "scroll_indicator_holo"

    invoke-virtual {v0, v1, v2, v3}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    :cond_2a7
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v0, :cond_2be

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_40a

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_40a

    const-string v0, "dock_divider_land"

    :goto_2bb
    invoke-virtual {v1, v3, v2, v0}, Lcom/anddoes/launcher/c/l;->a(Landroid/view/View;ILjava/lang/String;)V

    :cond_2be
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v1, v1, Lcom/anddoes/launcher/preference/f;->k:I

    iput v1, v0, Lcom/android/launcher2/Workspace;->at:I

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->y()V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->z()V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->D()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_2f8

    const-string v0, "THEME"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_412

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_40e

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_40e

    const-string v0, "dock_background_land"

    :goto_2ed
    invoke-virtual {v1, v2, v0}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2f8

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Hotseat;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2f8
    :goto_2f8
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_3c6

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ae:Z

    if-nez v0, :cond_3c6

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v1, 0x7f0b0017

    const v0, 0x7f0b001a

    const-string v2, "NONE"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_486

    const v1, 0x7f0b000f

    const v0, 0x7f0b0012

    move v12, v0

    move v0, v1

    move v1, v12

    :goto_321
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getPaddingRight()I

    move-result v3

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v6

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_4b4

    if-nez v6, :cond_4b4

    move v0, v2

    move v3, v4

    :goto_33d
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v7, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v7}, Lcom/android/launcher2/Workspace;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v8}, Lcom/android/launcher2/Workspace;->getPaddingTop()I

    move-result v8

    invoke-virtual {v4, v7, v8, v0, v3}, Lcom/android/launcher2/Workspace;->setPadding(IIII)V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v0, :cond_362

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v3, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v3, :cond_4b8

    if-nez v6, :cond_4b8

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    :cond_362
    :goto_362
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_38a

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    iget-object v0, v0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    if-eqz v0, :cond_38a

    const-string v0, "AUTO"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38a

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_37e

    if-eqz v6, :cond_38a

    :cond_37e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    iget-object v0, v0, Lcom/android/launcher2/Hotseat;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    :cond_38a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    if-eqz v0, :cond_3aa

    const-string v0, "AUTO"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3aa

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_3a0

    if-eqz v6, :cond_3aa

    :cond_3a0
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    :cond_3aa
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_4bc

    if-nez v6, :cond_4bc

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_3bc
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->requestLayout()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->requestLayout()V

    :cond_3c6
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ae:Z

    if-eqz v0, :cond_3cf

    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ac()V

    :cond_3cf
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->C()V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->E()V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->H()V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->B()V

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->A()V

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->X:Z

    if-eqz v0, :cond_3e7

    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ad()V

    :cond_3e7
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Launcher;->aS:I

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Launcher;->aT:I

    .line 503
    return-void

    :cond_3f8
    move v0, v2

    .line 411
    goto/16 :goto_29

    .line 453
    :cond_3fb
    const v0, 0x7f03001b

    goto/16 :goto_ff

    .line 480
    :cond_400
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Launcher;->aX:I

    goto/16 :goto_1d3

    .line 502
    :cond_40a
    const-string v0, "dock_divider_port"

    goto/16 :goto_2bb

    :cond_40e
    const-string v0, "dock_background_port"

    goto/16 :goto_2ed

    :cond_412
    const-string v0, "CUSTOM"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f8

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "images/dock_bg.png"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_43b

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_43b

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_47a

    :cond_43b
    const/4 v0, 0x0

    :goto_43c
    if-eqz v0, :cond_2f8

    :try_start_43e
    invoke-static {v0}, Landroid/graphics/drawable/BitmapDrawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v1

    iget-boolean v2, p0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v2, :cond_47f

    if-nez v1, :cond_47f

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v1, -0x3d4c

    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    new-instance v2, Lcom/android/launcher2/ca;

    invoke-direct {v2, v0}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Hotseat;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_475
    .catch Ljava/lang/Exception; {:try_start_43e .. :try_end_475} :catch_477

    goto/16 :goto_2f8

    :catch_477
    move-exception v0

    goto/16 :goto_2f8

    :cond_47a
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_43c

    :cond_47f
    :try_start_47f
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Hotseat;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_484
    .catch Ljava/lang/Exception; {:try_start_47f .. :try_end_484} :catch_477

    goto/16 :goto_2f8

    :cond_486
    const-string v2, "SMALL"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_49d

    const v1, 0x7f0b0013

    const v0, 0x7f0b0016

    move v12, v0

    move v0, v1

    move v1, v12

    goto/16 :goto_321

    :cond_49d
    const-string v2, "LARGE"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4ca

    const v1, 0x7f0b001b

    const v0, 0x7f0b001e

    move v12, v0

    move v0, v1

    move v1, v12

    goto/16 :goto_321

    :cond_4b4
    move v0, v3

    move v3, v2

    goto/16 :goto_33d

    :cond_4b8
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_362

    :cond_4bc
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_3bc

    :cond_4ca
    move v12, v0

    move v0, v1

    move v1, v12

    goto/16 :goto_321
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 4
    .parameter

    .prologue
    .line 5694
    packed-switch p1, :pswitch_data_14

    .line 5699
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 5696
    :pswitch_8
    new-instance v0, Lcom/android/launcher2/fo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/fo;-><init>(Lcom/android/launcher2/Launcher;B)V

    invoke-virtual {v0}, Lcom/android/launcher2/fo;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 5694
    nop

    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x0

    .line 1842
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1920
    :goto_b
    return v0

    .line 1846
    :cond_c
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 1848
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.MANAGE_ALL_APPLICATIONS_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1849
    const/high16 v2, 0x1080

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1851
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1852
    const/high16 v2, 0x1020

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1854
    const v1, 0x7f0702a6

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1855
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1856
    const/high16 v1, 0x1080

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1859
    const v1, 0x7f07029f

    invoke-interface {p1, v4, v4, v0, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1860
    const v2, 0x1080033

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1861
    const/16 v2, 0x41

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1864
    const v1, 0x7f0702a1

    .line 1863
    invoke-interface {p1, v5, v5, v0, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1865
    const v2, 0x108003f

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1866
    const/16 v2, 0x57

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1869
    const v1, 0x7f070108

    .line 1868
    invoke-interface {p1, v6, v6, v0, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1870
    const v2, 0x7f020060

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1871
    const/16 v2, 0x54

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1874
    const v1, 0x7f0702a2

    .line 1873
    invoke-interface {p1, v7, v7, v0, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1875
    const v2, 0x1080060

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1876
    const/16 v2, 0x53

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1878
    const/4 v1, 0x6

    const/4 v2, 0x6

    .line 1879
    const v3, 0x7f0702a3

    .line 1878
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1880
    const v2, 0x7f020045

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1881
    const/16 v2, 0x4e

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1883
    const/4 v1, 0x7

    const/4 v2, 0x7

    .line 1884
    const v3, 0x7f0702a0

    .line 1883
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1885
    const v2, 0x1080042

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1886
    const/16 v2, 0x4d

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1888
    const/16 v1, 0x8

    const/16 v2, 0x8

    .line 1889
    const v3, 0x7f070064

    .line 1888
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1890
    const v2, 0x1080042

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1892
    const/16 v1, 0x9

    const/16 v2, 0x9

    .line 1893
    const v3, 0x7f0700b7

    .line 1892
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1894
    const v2, 0x108003e

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1895
    const/16 v2, 0x45

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1897
    const/16 v1, 0xa

    const/16 v2, 0xa

    .line 1898
    const v3, 0x7f070040

    .line 1897
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1899
    const v2, 0x1080049

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1900
    const/16 v2, 0x43

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1902
    const/16 v1, 0xb

    const/16 v2, 0xb

    .line 1903
    const v3, 0x7f0702a4

    .line 1902
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1904
    const v2, 0x1080049

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1905
    const/16 v2, 0x50

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1907
    const/16 v1, 0xc

    const/16 v2, 0xc

    .line 1908
    const v3, 0x7f070041

    .line 1907
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1909
    const v2, 0x7f020043

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1910
    const/16 v2, 0x4c

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1912
    const/16 v1, 0xd

    const/16 v2, 0xd

    .line 1913
    const v3, 0x7f070042

    .line 1912
    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 1914
    const v2, 0x7f020047

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1915
    const/16 v2, 0x55

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1917
    const/16 v1, 0xf

    const/16 v2, 0xf

    const v3, 0x7f0702a5

    invoke-interface {p1, v1, v2, v0, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 1918
    const v1, 0x1080040

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1919
    const/16 v1, 0x48

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setAlphabeticShortcut(C)Landroid/view/MenuItem;

    .line 1920
    const/4 v0, 0x1

    goto/16 :goto_b
.end method

.method public onDestroy()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1764
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1765
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/f;->b()V

    .line 1766
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->c()V

    .line 1767
    invoke-direct {p0, v1}, Lcom/android/launcher2/Launcher;->j(Z)V

    .line 1770
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1771
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1772
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aC:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1775
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 1776
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v1}, Lcom/android/launcher2/gb;->f()V

    .line 1777
    invoke-virtual {v0, v3}, Lcom/android/launcher2/LauncherApplication;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/gb;

    .line 1780
    :try_start_32
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    invoke-virtual {v0}, Lcom/android/launcher2/fx;->stopListening()V
    :try_end_37
    .catch Ljava/lang/NullPointerException; {:try_start_32 .. :try_end_37} :catch_79

    .line 1784
    :goto_37
    iput-object v3, p0, Lcom/android/launcher2/Launcher;->H:Lcom/android/launcher2/fx;

    .line 1786
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1788
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/method/TextKeyListener;->release()V

    .line 1790
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v0}, Lcom/android/launcher2/gb;->a()V

    .line 1792
    :cond_4e
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->A:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1793
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->z:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1795
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->E:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->a()V

    .line 1796
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1797
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->removeAllViews()V

    .line 1798
    iput-object v3, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 1799
    iput-object v3, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    .line 1801
    invoke-static {}, Landroid/animation/ValueAnimator;->clearAllAnimations()V

    .line 1802
    return-void

    .line 1781
    :catch_79
    move-exception v0

    .line 1782
    const-string v1, "Launcher"

    const-string v2, "problem while stopping AppWidgetHost during Launcher destruction"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_37
.end method

.method public onDetachedFromWindow()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1545
    invoke-super {p0}, Landroid/app/Activity;->onDetachedFromWindow()V

    .line 1546
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->ad:Z

    .line 1548
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->ae:Z

    if-eqz v0, :cond_11

    .line 1549
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aE:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1550
    iput-boolean v1, p0, Lcom/android/launcher2/Launcher;->ae:Z

    .line 1552
    :cond_11
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->S()V

    .line 1553
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 999
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v0

    .line 1000
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1001
    if-lez v0, :cond_47

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(I)Z

    move-result v0

    if-nez v0, :cond_47

    move v4, v3

    .line 1002
    :goto_13
    if-nez v1, :cond_4b

    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v0

    if-eqz v0, :cond_49

    move v0, v2

    :goto_24
    if-eqz v0, :cond_4b

    if-eqz v4, :cond_4b

    .line 1003
    invoke-static {}, Landroid/text/method/TextKeyListener;->getInstance()Landroid/text/method/TextKeyListener;

    move-result-object v0

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v2, v3, p1, p2}, Landroid/text/method/TextKeyListener;->onKeyDown(Landroid/view/View;Landroid/text/Editable;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1005
    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4b

    .line 1012
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onSearchRequested()Z

    move-result v0

    .line 1021
    :goto_46
    return v0

    :cond_47
    move v4, v2

    .line 1001
    goto :goto_13

    :cond_49
    move v0, v3

    .line 1002
    goto :goto_24

    .line 1017
    :cond_4b
    const/16 v0, 0x52

    if-ne p1, v0, :cond_5a

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 1018
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->onSearchRequested()Z

    move-result v0

    goto :goto_46

    :cond_5a
    move v0, v1

    .line 1021
    goto :goto_46
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2791
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->b()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    .line 2833
    :goto_9
    return v0

    .line 2792
    :cond_a
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->T()Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v1

    goto :goto_9

    .line 2793
    :cond_12
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v3, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-eq v0, v3, :cond_1a

    move v0, v1

    goto :goto_9

    .line 2795
    :cond_1a
    instance-of v0, p1, Lcom/android/launcher2/CellLayout;

    if-nez v0, :cond_29

    .line 2796
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p1, v0

    .line 2799
    :cond_29
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->U()V

    .line 2800
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/as;

    .line 2802
    if-nez v0, :cond_36

    move v0, v2

    .line 2803
    goto :goto_9

    .line 2808
    :cond_36
    iget-object v3, v0, Lcom/android/launcher2/as;->a:Landroid/view/View;

    .line 2809
    iget-object v4, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v4, v4, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v4, :cond_4d

    invoke-virtual {p0, p1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;)Z

    move-result v4

    if-nez v4, :cond_4c

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->z()Z

    move-result v4

    if-eqz v4, :cond_4d

    :cond_4c
    move v1, v2

    .line 2810
    :cond_4d
    if-eqz v1, :cond_71

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    invoke-virtual {v1}, Lcom/android/launcher2/bk;->b()Z

    move-result v1

    if-nez v1, :cond_71

    .line 2811
    if-nez v3, :cond_9a

    .line 2812
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->az:Z

    if-eqz v1, :cond_62

    .line 2816
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->ah()V

    .line 2818
    :cond_62
    const-string v1, "ADD_DIALOG"

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->aw:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_73

    .line 2819
    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/as;)V

    :cond_71
    :goto_71
    move v0, v2

    .line 2833
    goto :goto_9

    .line 2820
    :cond_73
    const-string v0, "WALLPAPER"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 2821
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v0, :cond_89

    .line 2822
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->V()V

    goto :goto_71

    .line 2823
    :cond_89
    const-string v0, "APEX_MENU"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->aw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 2824
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->d(Landroid/view/View;)V

    goto :goto_71

    .line 2827
    :cond_9a
    instance-of v1, v3, Lcom/android/launcher2/Folder;

    if-nez v1, :cond_71

    .line 2829
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/as;)V

    goto :goto_71
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 9
    .parameter

    .prologue
    const/high16 v3, 0x40

    const/4 v2, 0x0

    .line 1667
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 1668
    if-nez p1, :cond_9

    .line 1714
    :cond_8
    :goto_8
    return-void

    .line 1672
    :cond_9
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 1674
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->f()V

    .line 1676
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    and-int/2addr v0, v3

    if-eq v0, v3, :cond_6e

    const/4 v0, 0x1

    move v1, v0

    .line 1679
    :goto_21
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v3

    .line 1681
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->G()V

    .line 1687
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 1688
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->p()V

    .line 1691
    :try_start_32
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v4

    .line 1692
    if-eqz v4, :cond_52

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_52

    .line 1694
    const-string v0, "input_method"

    .line 1693
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1695
    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1697
    :cond_52
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    if-eqz v0, :cond_5e

    .line 1698
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1699
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aU:Landroid/app/Dialog;
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_5e} :catch_d5

    .line 1704
    :cond_5e
    :goto_5e
    if-eqz v1, :cond_70

    if-nez v3, :cond_70

    .line 1705
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->am:Ljava/lang/String;

    const-string v1, "HOME_KEY"

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1709
    :goto_6b
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->W:Z

    goto :goto_8

    :cond_6e
    move v1, v2

    .line 1676
    goto :goto_21

    .line 1707
    :cond_70
    sget-object v0, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    goto :goto_6b

    .line 1711
    :cond_75
    const-string v0, "com.anddoes.launcher.ACTION"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1712
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aB:Lcom/anddoes/launcher/s;

    const-string v0, "LAUNCHER_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "LAUNCHER_ACTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/anddoes/launcher/s;->d:[Ljava/lang/String;

    array-length v5, v4

    move v0, v2

    :goto_95
    if-lt v0, v5, :cond_a8

    const-string v0, "MANAGE_SCREENS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bc

    iget-object v0, v1, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    const-string v1, "SHORTCUT"

    invoke-virtual {v0, v3, v1}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_a8
    aget-object v6, v4, v0

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b9

    iget-object v0, v1, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    const-string v1, "SHORTCUT"

    invoke-virtual {v0, v3, v1}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_b9
    add-int/lit8 v0, v0, 0x1

    goto :goto_95

    :cond_bc
    iget-object v0, v1, Lcom/anddoes/launcher/s;->e:[Ljava/lang/String;

    array-length v4, v0

    :goto_bf
    if-ge v2, v4, :cond_8

    aget-object v5, v0, v2

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d2

    iget-object v0, v1, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    const-string v1, "SHORTCUT"

    invoke-virtual {v0, v3, v1}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_d2
    add-int/lit8 v2, v2, 0x1

    goto :goto_bf

    .line 1701
    :catch_d5
    move-exception v0

    goto :goto_5e
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 1962
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->e(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1963
    const/4 v0, 0x1

    .line 1965
    :goto_b
    return v0

    :cond_c
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_b
.end method

.method protected onPause()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    .line 952
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 954
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 955
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->U:Z

    .line 956
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->c()V

    .line 957
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->e()V

    .line 958
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1925
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 1927
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->d()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1957
    :cond_e
    :goto_e
    return v2

    .line 1930
    :cond_f
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v3, Lcom/android/launcher2/fv;->e:Lcom/android/launcher2/fv;

    if-eq v0, v3, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v3, Lcom/android/launcher2/fv;->f:Lcom/android/launcher2/fv;

    if-eq v0, v3, :cond_e

    .line 1933
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aE:Z

    if-eqz v0, :cond_db

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v0, :cond_db

    .line 1934
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v3, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v3, :cond_db

    .line 1935
    invoke-interface {p1, v4, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1937
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->K()Lcom/android/launcher2/as;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->aJ:Lcom/android/launcher2/as;

    .line 1938
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aJ:Lcom/android/launcher2/as;

    if-eqz v0, :cond_d8

    move v0, v1

    :goto_3d
    invoke-interface {p1, v4, v0}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 1943
    :goto_40
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aF:Z

    if-eqz v0, :cond_e0

    .line 1944
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->m:Z

    if-eqz v0, :cond_e0

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v0, :cond_e0

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v4, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v4, :cond_e0

    move v0, v1

    .line 1943
    :goto_5a
    invoke-interface {p1, v3, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1945
    const/4 v0, 0x4

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aG:Z

    invoke-interface {p1, v0, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1946
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aH:Z

    invoke-interface {p1, v0, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1947
    const/4 v0, 0x6

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aI:Z

    invoke-interface {p1, v0, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1948
    const/4 v0, 0x7

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aJ:Z

    invoke-interface {p1, v0, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1949
    const/16 v3, 0x8

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aK:Z

    if-eqz v0, :cond_e3

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v4, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v4, :cond_e3

    move v0, v1

    :goto_8c
    invoke-interface {p1, v3, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1950
    const/16 v3, 0x9

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aL:Z

    if-eqz v0, :cond_e5

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->aV:Z

    if-eqz v0, :cond_e5

    move v0, v1

    :goto_9c
    invoke-interface {p1, v3, v0}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1951
    const/16 v0, 0xa

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aM:Z

    invoke-interface {p1, v0, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1952
    const/16 v0, 0xb

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aN:Z

    invoke-interface {p1, v0, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1953
    const/16 v0, 0xc

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->aO:Z

    if-eqz v3, :cond_c0

    iget-object v3, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v3, :cond_c0

    move v2, v1

    :cond_c0
    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1954
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v2, v2, Lcom/anddoes/launcher/preference/f;->b:Z

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 1955
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v2, v2, Lcom/anddoes/launcher/preference/f;->aP:Z

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    move v2, v1

    .line 1957
    goto/16 :goto_e

    :cond_d8
    move v0, v2

    .line 1938
    goto/16 :goto_3d

    .line 1940
    :cond_db
    invoke-interface {p1, v4, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto/16 :goto_40

    :cond_e0
    move v0, v2

    .line 1944
    goto/16 :goto_5a

    :cond_e3
    move v0, v2

    .line 1949
    goto :goto_8c

    :cond_e5
    move v0, v2

    .line 1950
    goto :goto_9c
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 1718
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1719
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 1722
    return-void

    .line 1719
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1720
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/Workspace;->h(I)V

    goto :goto_9
.end method

.method protected onResume()V
    .registers 10

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 858
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 861
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->b:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_c5

    .line 862
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 866
    :cond_10
    :goto_10
    sget-object v0, Lcom/android/launcher2/fv;->a:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    .line 869
    invoke-static {p0}, Lcom/android/launcher2/InstallShortcutReceiver;->b(Landroid/content/Context;)V

    .line 871
    iput-boolean v6, p0, Lcom/android/launcher2/Launcher;->U:Z

    .line 872
    sput-boolean v6, Lcom/android/launcher2/Launcher;->Y:Z

    .line 873
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->V:Z

    if-nez v0, :cond_23

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->X:Z

    if-eqz v0, :cond_2e

    .line 874
    :cond_23
    iput-boolean v7, p0, Lcom/android/launcher2/Launcher;->T:Z

    .line 875
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v0, v7, v8}, Lcom/android/launcher2/gb;->a(ZI)V

    .line 876
    iput-boolean v6, p0, Lcom/android/launcher2/Launcher;->V:Z

    .line 877
    iput-boolean v6, p0, Lcom/android/launcher2/Launcher;->X:Z

    .line 882
    :cond_2e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aA:Lcom/android/launcher2/BubbleTextView;

    if-eqz v0, :cond_37

    .line 884
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aA:Lcom/android/launcher2/BubbleTextView;

    invoke-virtual {v0, v6}, Lcom/android/launcher2/BubbleTextView;->setStayPressed(Z)V

    .line 886
    :cond_37
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_40

    .line 888
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->k()V

    .line 894
    :cond_40
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->l()V

    .line 898
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->P()V

    .line 900
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->T:Z

    if-nez v0, :cond_5a

    .line 901
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 906
    new-instance v1, Lcom/android/launcher2/fl;

    invoke-direct {v1, p0, v0}, Lcom/android/launcher2/fl;-><init>(Lcom/android/launcher2/Launcher;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 921
    :cond_5a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v0, Lcom/anddoes/launcher/preference/f;->bd:Ljava/lang/String;

    .line 922
    const-string v0, "NEVER"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a9

    .line 924
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-wide v0, v0, Lcom/anddoes/launcher/preference/f;->be:J

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_a9

    .line 926
    :try_start_76
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 927
    new-instance v0, Lcom/anddoes/launcher/k;

    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/k;-><init>(Landroid/app/Activity;ILjava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 928
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/anddoes/launcher/preference/f;->be:J

    .line 929
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "last_check_update"

    invoke-virtual {v0, v3, v1, v2}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;J)V
    :try_end_a9
    .catch Ljava/lang/Exception; {:try_start_76 .. :try_end_a9} :catch_d0

    .line 935
    :cond_a9
    :goto_a9
    invoke-direct {p0, v6}, Lcom/android/launcher2/Launcher;->j(Z)V

    .line 937
    iget v0, p0, Lcom/android/launcher2/Launcher;->aX:I

    if-lez v0, :cond_c4

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->v:Z

    if-nez v0, :cond_c4

    .line 939
    const-wide/16 v0, -0x64

    iget v2, p0, Lcom/android/launcher2/Launcher;->aX:I

    .line 938
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    .line 940
    if-eqz v0, :cond_c2

    .line 941
    iput-boolean v7, v0, Lcom/android/launcher2/CellLayout;->e:Z

    .line 943
    :cond_c2
    iput v8, p0, Lcom/android/launcher2/Launcher;->aX:I

    .line 945
    :cond_c4
    return-void

    .line 863
    :cond_c5
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->R:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_10

    .line 864
    invoke-direct {p0, v6}, Lcom/android/launcher2/Launcher;->e(Z)V

    goto/16 :goto_10

    :catch_d0
    move-exception v0

    goto :goto_a9
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 963
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aa:Lcom/android/launcher2/gb;

    invoke-virtual {v0}, Lcom/android/launcher2/gb;->f()V

    .line 964
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_e

    .line 965
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->j()V

    .line 967
    :cond_e
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 1726
    const-string v0, "launcher.current_screen"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getNextPage()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1727
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1728
    const-string v0, "launcher.is_landscape"

    iget-boolean v1, p0, Lcom/android/launcher2/Launcher;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1729
    const-string v0, "launcher.current_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1730
    const-string v0, "launcher.state"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    invoke-virtual {v1}, Lcom/android/launcher2/fv;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1733
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->h()V

    .line 1735
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-wide v0, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7d

    iget-object v0, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->k:I

    if-ltz v0, :cond_7d

    .line 1736
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->W:Z

    if-eqz v0, :cond_7d

    .line 1737
    const-string v0, "launcher.add_container"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget-wide v1, v1, Lcom/android/launcher2/di;->j:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1738
    const-string v0, "launcher.add_screen"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1739
    const-string v0, "launcher.add_cell_x"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1740
    const-string v0, "launcher.add_cell_y"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1741
    const-string v0, "launcher.add_span_x"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1742
    const-string v0, "launcher.add_span_y"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->I:Lcom/android/launcher2/di;

    iget v1, v1, Lcom/android/launcher2/di;->o:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1743
    const-string v0, "launcher.add_widget_info"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->J:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1746
    :cond_7d
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->L:Lcom/android/launcher2/cu;

    if-eqz v0, :cond_94

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->W:Z

    if-eqz v0, :cond_94

    .line 1747
    const-string v0, "launcher.rename_folder"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1748
    const-string v0, "launcher.rename_folder_id"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->L:Lcom/android/launcher2/cu;

    iget-wide v1, v1, Lcom/android/launcher2/cu;->h:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1760
    :cond_94
    return-void
.end method

.method public onSearchRequested()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1970
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, v1, v2}, Lcom/android/launcher2/Launcher;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 1972
    const v0, 0x7f040002

    const v1, 0x7f040003

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->overridePendingTransition(II)V

    .line 1973
    return v2
.end method

.method protected onStart()V
    .registers 5

    .prologue
    .line 5446
    const-string v0, "SYSTEM"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 5447
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 5448
    const-string v0, "JELLY_BEAN"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 5451
    :cond_1e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v0, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_30

    const-string v0, "SYSTEM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 5453
    :cond_30
    :goto_30
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 5454
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-boolean v0, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    if-eqz v0, :cond_8b

    .line 5455
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    .line 5456
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 5462
    :goto_45
    return-void

    .line 5451
    :cond_46
    const v1, 0x7f040029

    const v0, 0x7f04002a

    const-string v3, "ICE_CREAM_SANDWICH"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5e

    const v1, 0x7f04000a

    const v0, 0x7f04000b

    :cond_5a
    :goto_5a
    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/Launcher;->overridePendingTransition(II)V

    goto :goto_30

    :cond_5e
    const-string v3, "JELLY_BEAN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6d

    const v1, 0x7f04000e

    const v0, 0x7f04000f

    goto :goto_5a

    :cond_6d
    const-string v3, "SLIDE_LEFT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7c

    const v1, 0x7f040027

    const v0, 0x7f040028

    goto :goto_5a

    :cond_7c
    const-string v3, "SLIDE_RIGHT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5a

    const v1, 0x7f040025

    const v0, 0x7f040026

    goto :goto_5a

    .line 5459
    :cond_8b
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->a()V

    .line 5461
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/Launcher"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    goto :goto_45
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2418
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 2419
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchDownAllAppsButton(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 2471
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 2472
    return-void
.end method

.method public onTrimMemory(I)V
    .registers 3
    .parameter

    .prologue
    .line 3458
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 3459
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_c

    .line 3460
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->c()V

    .line 3462
    :cond_c
    return-void
.end method

.method protected onUserLeaveHint()V
    .registers 2

    .prologue
    .line 506
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 507
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/Launcher;->Y:Z

    .line 508
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 6
    .parameter

    .prologue
    .line 3466
    if-nez p1, :cond_7

    .line 3469
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3480
    :goto_6
    return-void

    .line 3472
    :cond_7
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    new-instance v1, Lcom/android/launcher2/ec;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ec;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 3477
    const-wide/16 v2, 0x1f4

    .line 3472
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/Workspace;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 3478
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->W()V

    goto :goto_6
.end method

.method final p()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 3603
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    sget-object v1, Lcom/android/launcher2/fv;->d:Lcom/android/launcher2/fv;

    if-ne v0, v1, :cond_18

    .line 3604
    invoke-direct {p0, v2}, Lcom/android/launcher2/Launcher;->g(Z)V

    .line 3607
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 3608
    invoke-direct {p0, v2, v2}, Lcom/android/launcher2/Launcher;->b(ZZ)V

    .line 3609
    sget-object v0, Lcom/android/launcher2/fv;->c:Lcom/android/launcher2/fv;

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->t:Lcom/android/launcher2/fv;

    .line 3612
    :cond_18
    return-void
.end method

.method final q()V
    .registers 3

    .prologue
    const/4 v1, 0x4

    .line 3615
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 3616
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->C:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3618
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    if-eqz v0, :cond_13

    .line 3619
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3620
    :cond_13
    return-void
.end method

.method public final r()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 3994
    iget-boolean v1, p0, Lcom/android/launcher2/Launcher;->U:Z

    if-eqz v1, :cond_f

    .line 3995
    const-string v1, "Launcher"

    const-string v2, "setLoadOnResume"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3996
    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->X:Z

    .line 3999
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final s()I
    .registers 2

    .prologue
    .line 4007
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_b

    .line 4008
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v0

    .line 4010
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x2

    goto :goto_a
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1810
    if-ltz p2, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Launcher;->W:Z

    .line 1811
    :cond_5
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1812
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1822
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/Launcher;->a(ZLjava/lang/Runnable;)V

    .line 1824
    if-nez p1, :cond_35

    .line 1826
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->S:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1828
    :goto_d
    if-nez p3, :cond_33

    .line 1829
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1830
    const-string v0, "source"

    const-string v2, "launcher-search"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1832
    :goto_1b
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher2/SearchDropTargetBar;->getSearchBarBounds()Landroid/graphics/Rect;

    move-result-object v6

    .line 1835
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 1834
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    move v2, p2

    move v5, p4

    .line 1836
    invoke-virtual/range {v0 .. v6}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;ZLandroid/graphics/Rect;)V

    .line 1838
    return-void

    :cond_33
    move-object v4, p3

    goto :goto_1b

    :cond_35
    move-object v1, p1

    goto :goto_d
.end method

.method public final t()V
    .registers 5

    .prologue
    .line 4020
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    .line 4022
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Launcher;->au:I

    .line 4023
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->av:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4024
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->I()V

    .line 4025
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    .line 4026
    const/4 v0, 0x0

    move v1, v0

    :goto_15
    if-lt v1, v3, :cond_26

    .line 4031
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->an:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 4032
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    if-eqz v0, :cond_25

    .line 4033
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->M:Lcom/android/launcher2/Hotseat;

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->d()V

    .line 4035
    :cond_25
    return-void

    .line 4028
    :cond_26
    invoke-virtual {v2, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 4029
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->removeAllViewsInLayout()V

    .line 4026
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15
.end method

.method public final u()V
    .registers 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4161
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->r()Z

    .line 4163
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->Q:Landroid/os/Bundle;

    if-eqz v0, :cond_23

    .line 4164
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_20

    .line 4165
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 4167
    :cond_20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/Launcher;->Q:Landroid/os/Bundle;

    .line 4170
    :cond_23
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->H()V

    move v1, v2

    .line 4174
    :goto_29
    sget-object v0, Lcom/android/launcher2/Launcher;->aD:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_77

    .line 4177
    sget-object v0, Lcom/android/launcher2/Launcher;->aD:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4181
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Z()V

    .line 4184
    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->ad:Z

    if-nez v0, :cond_41

    iget-boolean v0, p0, Lcom/android/launcher2/Launcher;->T:Z

    if-eqz v0, :cond_74

    .line 4185
    :cond_41
    new-instance v1, Lcom/android/launcher2/ei;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ei;-><init>(Lcom/android/launcher2/Launcher;)V

    .line 4192
    iget v0, p0, Lcom/android/launcher2/Launcher;->au:I

    if-ltz v0, :cond_86

    .line 4193
    iget v0, p0, Lcom/android/launcher2/Launcher;->au:I

    iget-object v4, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v4}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v4

    if-eq v0, v4, :cond_86

    move v0, v3

    .line 4194
    :goto_55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/android/launcher2/Launcher;->F:Lcom/android/launcher2/bk;

    invoke-virtual {v6}, Lcom/android/launcher2/bk;->d()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget v6, Lcom/android/launcher2/Launcher;->y:I

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_88

    :goto_69
    if-eqz v3, :cond_8e

    .line 4197
    if-eqz v0, :cond_8a

    .line 4198
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget v3, p0, Lcom/android/launcher2/Launcher;->au:I

    invoke-virtual {v0, v3, v1}, Lcom/android/launcher2/Workspace;->a(ILjava/lang/Runnable;)V

    .line 4209
    :cond_74
    :goto_74
    iput-boolean v2, p0, Lcom/android/launcher2/Launcher;->T:Z

    .line 4210
    return-void

    .line 4175
    :cond_77
    sget-object v0, Lcom/android/launcher2/Launcher;->aD:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/fu;

    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fu;)Z

    .line 4174
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_29

    :cond_86
    move v0, v2

    .line 4193
    goto :goto_55

    :cond_88
    move v3, v2

    .line 4194
    goto :goto_69

    .line 4200
    :cond_8a
    invoke-direct {p0, v2}, Lcom/android/launcher2/Launcher;->i(Z)V

    goto :goto_74

    .line 4205
    :cond_8e
    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->i(Z)V

    goto :goto_74
.end method

.method public final v()V
    .registers 4

    .prologue
    .line 4282
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->Y()Z

    move-result v0

    .line 4283
    invoke-direct {p0, v0}, Lcom/android/launcher2/Launcher;->h(Z)Z

    move-result v1

    .line 4284
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->O:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v2, v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->a(ZZ)V

    .line 4285
    return-void
.end method

.method public final w()V
    .registers 3

    .prologue
    .line 4381
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_a

    .line 4382
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Z)V

    .line 4384
    :cond_a
    return-void
.end method

.method public final x()V
    .registers 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    .line 4426
    invoke-direct {p0}, Lcom/android/launcher2/Launcher;->aa()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 4427
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 4428
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 4427
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v5

    packed-switch v5, :pswitch_data_48

    move v0, v2

    :goto_23
    :pswitch_23
    const/4 v5, 0x4

    new-array v5, v5, [I

    aput v1, v5, v3

    const/16 v6, 0x9

    aput v6, v5, v2

    const/4 v6, 0x3

    const/16 v7, 0x8

    aput v7, v5, v6

    if-ne v0, v2, :cond_46

    :goto_33
    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v0

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x4

    aget v0, v5, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Launcher;->setRequestedOrientation(I)V

    .line 4430
    :cond_3f
    return-void

    .line 4427
    :pswitch_40
    if-ne v0, v2, :cond_44

    move v0, v1

    goto :goto_23

    :cond_44
    move v0, v2

    goto :goto_23

    :cond_46
    move v1, v3

    goto :goto_33

    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_23
        :pswitch_40
        :pswitch_23
        :pswitch_40
    .end packed-switch
.end method

.method public final y()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 4541
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->o:Z

    iput-boolean v1, v0, Lcom/android/launcher2/Workspace;->aj:Z

    .line 4542
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->q:Z

    iput-boolean v1, v0, Lcom/android/launcher2/Workspace;->ak:Z

    .line 4543
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->o:Z

    if-eqz v0, :cond_8c

    .line 4544
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    if-eqz v0, :cond_5e

    .line 4545
    const-string v0, "AUTO"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 4546
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ae:Z

    if-eqz v0, :cond_34

    .line 4547
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    const-string v1, "BOTTOM"

    iput-object v1, v0, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    .line 4549
    :cond_34
    const-string v0, "TOP"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 4550
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 4553
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 4554
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 4555
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 4556
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4569
    :goto_52
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->q:Z

    if-nez v0, :cond_86

    .line 4570
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->d(Z)V

    .line 4578
    :cond_5e
    :goto_5e
    return-void

    .line 4557
    :cond_5f
    const-string v0, "BOTTOM"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 4558
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 4561
    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 4562
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 4563
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 4564
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_52

    .line 4566
    :cond_7e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aK:Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aL:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_52

    .line 4572
    :cond_86
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->D()V

    goto :goto_5e

    .line 4576
    :cond_8c
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->B:Lcom/android/launcher2/Workspace;

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->D()V

    goto :goto_5e
.end method

.method public final z()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x2

    .line 4582
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->O:Z

    iput-boolean v1, v0, Lcom/android/launcher2/AppsCustomizePagedView;->aj:Z

    .line 4583
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->Q:Z

    iput-boolean v1, v0, Lcom/android/launcher2/AppsCustomizePagedView;->ak:Z

    .line 4584
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->O:Z

    if-eqz v0, :cond_74

    .line 4585
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    if-eqz v0, :cond_46

    .line 4586
    const-string v0, "TOP"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 4587
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 4590
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 4591
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 4592
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 4593
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4607
    :goto_3a
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->Q:Z

    if-nez v0, :cond_6e

    .line 4608
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Z)V

    .line 4616
    :cond_46
    :goto_46
    return-void

    .line 4594
    :cond_47
    const-string v0, "BOTTOM"

    iget-object v1, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 4595
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 4598
    const/16 v1, 0x50

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 4599
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 4600
    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 4601
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3a

    .line 4603
    :cond_66
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->aM:Landroid/view/View;

    .line 4604
    iget-object v1, p0, Lcom/android/launcher2/Launcher;->aN:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3a

    .line 4610
    :cond_6e
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->D()V

    goto :goto_46

    .line 4614
    :cond_74
    iget-object v0, p0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->D()V

    goto :goto_46
.end method
