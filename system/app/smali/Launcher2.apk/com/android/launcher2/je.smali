.class public abstract Lcom/android/launcher2/je;
.super Lcom/android/launcher2/PagedView;
.source "SourceFile"


# static fields
.field private static final b:F


# instance fields
.field a:I

.field private c:F

.field private d:F

.field private e:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 25
    const-wide v0, 0x3f90624dd2f1a9fcL

    const-wide/high16 v2, 0x3fe8

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/android/launcher2/je;->b:F

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/je;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    iput-boolean v1, p0, Lcom/android/launcher2/je;->ad:Z

    .line 85
    iget v2, p0, Lcom/android/launcher2/je;->a:I

    if-eq v2, v0, :cond_e

    :goto_b
    iput-boolean v0, p0, Lcom/android/launcher2/je;->ae:Z

    .line 86
    return-void

    :cond_e
    move v0, v1

    .line 85
    goto :goto_b
.end method

.method private d(IZ)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 130
    invoke-virtual {p0}, Lcom/android/launcher2/je;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 132
    const/4 v0, 0x1

    iget v2, p0, Lcom/android/launcher2/je;->u:I

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 133
    invoke-virtual {p0, v1}, Lcom/android/launcher2/je;->p(I)I

    move-result v0

    invoke-virtual {p0, v1}, Lcom/android/launcher2/je;->q(I)I

    move-result v3

    sub-int/2addr v0, v3

    .line 134
    iget v3, p0, Lcom/android/launcher2/je;->S:I

    sub-int v3, v0, v3

    .line 135
    add-int/lit8 v0, v2, 0x1

    mul-int/lit8 v4, v0, 0x64

    .line 137
    iget-object v0, p0, Lcom/android/launcher2/je;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3a

    .line 138
    iget-object v0, p0, Lcom/android/launcher2/je;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 141
    :cond_3a
    if-eqz p2, :cond_5a

    .line 142
    iget-object v0, p0, Lcom/android/launcher2/je;->e:Landroid/view/animation/Interpolator;

    check-cast v0, Lcom/android/launcher2/jf;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/jf;->a(I)V

    .line 147
    :goto_43
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 148
    if-lez v0, :cond_62

    .line 149
    int-to-float v2, v4

    int-to-float v4, v4

    int-to-float v0, v0

    iget v5, p0, Lcom/android/launcher2/je;->c:F

    div-float/2addr v0, v5

    div-float v0, v4, v0

    iget v4, p0, Lcom/android/launcher2/je;->d:F

    mul-float/2addr v0, v4

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 154
    :goto_56
    invoke-virtual {p0, v1, v3, v0}, Lcom/android/launcher2/je;->a_(III)V

    .line 155
    return-void

    .line 144
    :cond_5a
    iget-object v0, p0, Lcom/android/launcher2/je;->e:Landroid/view/animation/Interpolator;

    check-cast v0, Lcom/android/launcher2/jf;

    invoke-virtual {v0}, Lcom/android/launcher2/jf;->a()V

    goto :goto_43

    .line 151
    :cond_62
    add-int/lit8 v0, v4, 0x64

    goto :goto_56
.end method


# virtual methods
.method protected final b()V
    .registers 2

    .prologue
    .line 97
    invoke-super {p0}, Lcom/android/launcher2/PagedView;->b()V

    .line 99
    invoke-virtual {p0}, Lcom/android/launcher2/je;->getScrollMode()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/je;->a:I

    .line 100
    iget v0, p0, Lcom/android/launcher2/je;->a:I

    if-nez v0, :cond_1a

    .line 101
    const v0, 0x451c4000

    iput v0, p0, Lcom/android/launcher2/je;->c:F

    .line 102
    const v0, 0x3ecccccd

    iput v0, p0, Lcom/android/launcher2/je;->d:F

    .line 105
    invoke-virtual {p0}, Lcom/android/launcher2/je;->n()V

    .line 107
    :cond_1a
    return-void
.end method

.method protected final b(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 120
    iget v0, p0, Lcom/android/launcher2/je;->a:I

    if-ne v0, v1, :cond_9

    .line 121
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/PagedView;->b(II)V

    .line 125
    :goto_8
    return-void

    .line 123
    :cond_9
    invoke-direct {p0, p1, v1}, Lcom/android/launcher2/je;->d(IZ)V

    goto :goto_8
.end method

.method public computeScroll()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 168
    iget v0, p0, Lcom/android/launcher2/je;->a:I

    if-ne v0, v1, :cond_9

    .line 169
    invoke-super {p0}, Lcom/android/launcher2/PagedView;->computeScroll()V

    .line 187
    :cond_8
    :goto_8
    return-void

    .line 171
    :cond_9
    invoke-virtual {p0}, Lcom/android/launcher2/je;->w()Z

    move-result v0

    .line 173
    if-nez v0, :cond_8

    iget v0, p0, Lcom/android/launcher2/je;->C:I

    if-ne v0, v1, :cond_8

    .line 174
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28

    div-float/2addr v0, v1

    .line 175
    iget v1, p0, Lcom/android/launcher2/je;->r:F

    sub-float v1, v0, v1

    sget v2, Lcom/android/launcher2/je;->b:F

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->exp(D)D

    move-result-wide v1

    double-to-float v1, v1

    .line 177
    iget v2, p0, Lcom/android/launcher2/je;->s:F

    iget v3, p0, Lcom/android/launcher2/je;->S:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 178
    iget v3, p0, Lcom/android/launcher2/je;->S:I

    int-to-float v3, v3

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/je;->getScrollY()I

    move-result v3

    invoke-virtual {p0, v1, v3}, Lcom/android/launcher2/je;->scrollTo(II)V

    .line 179
    iput v0, p0, Lcom/android/launcher2/je;->r:F

    .line 182
    const/high16 v0, 0x3f80

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_4d

    const/high16 v0, -0x4080

    cmpg-float v0, v2, v0

    if-gez v0, :cond_8

    .line 183
    :cond_4d
    invoke-virtual {p0}, Lcom/android/launcher2/je;->invalidate()V

    goto :goto_8
.end method

.method protected getScrollMode()I
    .registers 2

    .prologue
    .line 89
    const/4 v0, 0x1

    return v0
.end method

.method protected r(I)V
    .registers 4
    .parameter

    .prologue
    .line 159
    iget v0, p0, Lcom/android/launcher2/je;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 160
    invoke-super {p0, p1}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 164
    :goto_8
    return-void

    .line 162
    :cond_9
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/je;->d(IZ)V

    goto :goto_8
.end method

.method protected final y()V
    .registers 3

    .prologue
    .line 111
    iget v0, p0, Lcom/android/launcher2/je;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 112
    invoke-super {p0}, Lcom/android/launcher2/PagedView;->y()V

    .line 116
    :goto_8
    return-void

    .line 114
    :cond_9
    invoke-virtual {p0}, Lcom/android/launcher2/je;->getPageNearestToCenterOfScreen()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/je;->b(II)V

    goto :goto_8
.end method
