.class final Lcom/android/launcher2/r;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:I

.field c:I

.field final synthetic d:Lcom/android/launcher2/AppsCustomizePagedView;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/AppsCustomizePagedView;Ljava/util/ArrayList;II)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2666
    iput-object p1, p0, Lcom/android/launcher2/r;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2667
    iput-object p2, p0, Lcom/android/launcher2/r;->a:Ljava/util/ArrayList;

    .line 2668
    iput p3, p0, Lcom/android/launcher2/r;->b:I

    .line 2669
    iput p4, p0, Lcom/android/launcher2/r;->c:I

    .line 2670
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 2674
    iget-object v0, p0, Lcom/android/launcher2/r;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 2679
    if-ltz p1, :cond_8

    invoke-virtual {p0}, Lcom/android/launcher2/r;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_a

    .line 2680
    :cond_8
    const/4 v0, 0x0

    .line 2682
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/r;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_9
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 2687
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2692
    if-ltz p1, :cond_8

    invoke-virtual {p0}, Lcom/android/launcher2/r;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_a

    .line 2693
    :cond_8
    const/4 v0, 0x0

    .line 2705
    :goto_9
    return-object v0

    .line 2695
    :cond_a
    invoke-virtual {p0, p1}, Lcom/android/launcher2/r;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2696
    instance-of v1, v0, Lcom/android/launcher2/t;

    if-eqz v1, :cond_2d

    .line 2697
    check-cast v0, Lcom/android/launcher2/t;

    .line 2698
    new-instance v1, Landroid/view/View;

    iget-object v2, p0, Lcom/android/launcher2/r;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v2}, Lcom/android/launcher2/AppsCustomizePagedView;->f(Lcom/android/launcher2/AppsCustomizePagedView;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2699
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    iget v3, p0, Lcom/android/launcher2/r;->b:I

    iget v0, v0, Lcom/android/launcher2/t;->a:I

    invoke-direct {v2, v3, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    .line 2700
    goto :goto_9

    .line 2702
    :cond_2d
    iget-object v1, p0, Lcom/android/launcher2/r;->d:Lcom/android/launcher2/AppsCustomizePagedView;

    check-cast v0, Lcom/android/launcher2/h;

    invoke-virtual {v1, p2, v0, p3}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/View;Lcom/android/launcher2/h;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewIcon;

    move-result-object v0

    .line 2703
    invoke-virtual {v0}, Lcom/android/launcher2/PagedViewIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/r;->b:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2704
    invoke-virtual {v0}, Lcom/android/launcher2/PagedViewIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/r;->c:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_9
.end method
