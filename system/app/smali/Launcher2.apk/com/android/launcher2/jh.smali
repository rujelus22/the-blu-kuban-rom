.class final Lcom/android/launcher2/jh;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final synthetic a:Ljava/util/Set;

.field private final synthetic b:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Set;Landroid/content/SharedPreferences;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p2, p0, Lcom/android/launcher2/jh;->a:Ljava/util/Set;

    iput-object p3, p0, Lcom/android/launcher2/jh;->b:Landroid/content/SharedPreferences;

    .line 148
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 150
    iget-object v1, p0, Lcom/android/launcher2/jh;->a:Ljava/util/Set;

    monitor-enter v1

    .line 151
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/jh;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 152
    const-string v2, "apps.new.list"

    .line 153
    iget-object v3, p0, Lcom/android/launcher2/jh;->a:Ljava/util/Set;

    .line 152
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 154
    iget-object v2, p0, Lcom/android/launcher2/jh;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 156
    const-string v2, "apps.new.page"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 158
    :cond_1e
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 150
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_23

    return-void

    :catchall_23
    move-exception v0

    monitor-exit v1

    throw v0
.end method
