.class public abstract Lcom/android/launcher2/is;
.super Lcom/android/launcher2/PagedView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Landroid/view/View;

.field private b:Z

.field private c:Z

.field private d:F

.field private e:Lcom/android/launcher2/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/is;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/is;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/android/launcher2/is;->e:Lcom/android/launcher2/Launcher;

    .line 52
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 61
    iput-boolean v1, p0, Lcom/android/launcher2/is;->b:Z

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/is;->a:Landroid/view/View;

    .line 63
    iput-boolean v1, p0, Lcom/android/launcher2/is;->c:Z

    .line 64
    return-void
.end method

.method private c(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 67
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 68
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_22

    .line 79
    :cond_a
    :goto_a
    :pswitch_a
    return-void

    .line 70
    :pswitch_b
    invoke-direct {p0}, Lcom/android/launcher2/is;->c()V

    .line 71
    iput-boolean v1, p0, Lcom/android/launcher2/is;->c:Z

    goto :goto_a

    .line 74
    :pswitch_11
    iget v0, p0, Lcom/android/launcher2/is;->C:I

    if-eq v0, v1, :cond_a

    iget-boolean v0, p0, Lcom/android/launcher2/is;->b:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/android/launcher2/is;->c:Z

    if-eqz v0, :cond_a

    .line 75
    invoke-virtual {p0, p1}, Lcom/android/launcher2/is;->b(Landroid/view/MotionEvent;)V

    goto :goto_a

    .line 68
    nop

    :pswitch_data_22
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_11
    .end packed-switch
.end method


# virtual methods
.method protected a(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/android/launcher2/is;->b:Z

    if-nez v0, :cond_7

    invoke-super {p0, p1}, Lcom/android/launcher2/PagedView;->a(Landroid/view/MotionEvent;)V

    .line 123
    :cond_7
    return-void
.end method

.method protected b(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    iget v0, p0, Lcom/android/launcher2/is;->Z:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 135
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 136
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 137
    iget v4, p0, Lcom/android/launcher2/is;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    .line 138
    iget v4, p0, Lcom/android/launcher2/is;->A:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v4, v0

    .line 140
    iget v0, p0, Lcom/android/launcher2/is;->G:I

    .line 141
    if-le v4, v0, :cond_4e

    move v0, v1

    .line 142
    :goto_25
    int-to-float v4, v4

    int-to-float v3, v3

    div-float v3, v4, v3

    iget v4, p0, Lcom/android/launcher2/is;->d:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_50

    .line 144
    :goto_2f
    if-eqz v1, :cond_4d

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/android/launcher2/is;->a:Landroid/view/View;

    if-eqz v0, :cond_4d

    .line 146
    iget-object v0, p0, Lcom/android/launcher2/is;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/is;->b(Landroid/view/View;)Z

    .line 149
    iget-boolean v0, p0, Lcom/android/launcher2/is;->F:Z

    if-eqz v0, :cond_4d

    .line 150
    iput-boolean v2, p0, Lcom/android/launcher2/is;->F:Z

    .line 154
    iget v0, p0, Lcom/android/launcher2/is;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/is;->a(I)Landroid/view/View;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_4d

    .line 156
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 160
    :cond_4d
    return-void

    :cond_4e
    move v0, v2

    .line 141
    goto :goto_25

    :cond_50
    move v1, v2

    .line 142
    goto :goto_2f
.end method

.method protected b(Landroid/view/View;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 55
    iget-boolean v1, p0, Lcom/android/launcher2/is;->b:Z

    .line 56
    iput-boolean v0, p0, Lcom/android/launcher2/is;->b:Z

    .line 57
    if-eqz v1, :cond_8

    const/4 v0, 0x0

    :cond_8
    return v0
.end method

.method protected final e_()V
    .registers 2

    .prologue
    .line 174
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/is;->d(Z)V

    .line 175
    return-void
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 177
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/is;->e(Z)V

    .line 178
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/android/launcher2/is;->c()V

    .line 169
    invoke-super {p0}, Lcom/android/launcher2/PagedView;->onDetachedFromWindow()V

    .line 170
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/android/launcher2/is;->c(Landroid/view/MotionEvent;)V

    .line 84
    invoke-super {p0, p1}, Lcom/android/launcher2/PagedView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_8

    .line 114
    :cond_7
    :goto_7
    return v0

    .line 105
    :cond_8
    iget v1, p0, Lcom/android/launcher2/is;->v:I

    const/16 v2, -0x3e7

    if-ne v1, v2, :cond_7

    .line 107
    iget-object v1, p0, Lcom/android/launcher2/is;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 108
    iget-object v1, p0, Lcom/android/launcher2/is;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->j()Z

    move-result v1

    if-nez v1, :cond_7

    .line 110
    iget-object v1, p0, Lcom/android/launcher2/is;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 111
    iget-object v0, p0, Lcom/android/launcher2/is;->e:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-eqz v0, :cond_34

    .line 112
    const/4 v0, 0x1

    goto :goto_7

    .line 114
    :cond_34
    invoke-virtual {p0, p1}, Lcom/android/launcher2/is;->b(Landroid/view/View;)Z

    move-result v0

    goto :goto_7
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Lcom/android/launcher2/is;->a:Landroid/view/View;

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/is;->c:Z

    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/android/launcher2/is;->c(Landroid/view/MotionEvent;)V

    .line 90
    invoke-super {p0, p1}, Lcom/android/launcher2/PagedView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setDragSlopeThreshold(F)V
    .registers 2
    .parameter

    .prologue
    .line 163
    iput p1, p0, Lcom/android/launcher2/is;->d:F

    .line 164
    return-void
.end method
