.class public Lcom/android/launcher2/ak;
.super Landroid/widget/TextView;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/bl;
.implements Lcom/android/launcher2/bx;


# instance fields
.field protected final a:I

.field protected b:Lcom/android/launcher2/Launcher;

.field protected c:Lcom/android/launcher2/SearchDropTargetBar;

.field protected d:Z

.field protected e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/ak;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/ak;->e:I

    .line 54
    invoke-virtual {p0}, Lcom/android/launcher2/ak;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    const v1, 0x7f0a0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/ak;->a:I

    .line 56
    const v1, 0x7f0b005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/ak;->f:I

    .line 57
    return-void
.end method


# virtual methods
.method final a(IIII)Landroid/graphics/Rect;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lcom/android/launcher2/ak;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    .line 122
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 123
    invoke-virtual {v0, p0, v1}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 124
    iget v0, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/android/launcher2/ak;->getPaddingLeft()I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    iget v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/android/launcher2/ak;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v3, p4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 128
    add-int v3, v0, p3

    add-int v4, v2, p4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 131
    sub-int v0, p1, p3

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 132
    sub-int v2, p2, p4

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    .line 133
    invoke-virtual {v1, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 135
    return-object v1
.end method

.method public a()V
    .registers 1

    .prologue
    .line 110
    return-void
.end method

.method public a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 102
    return-void
.end method

.method public a(Lcom/android/launcher2/bz;)V
    .registers 2
    .parameter

    .prologue
    .line 82
    return-void
.end method

.method public a(Lcom/android/launcher2/bz;Landroid/graphics/PointF;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 86
    return-void
.end method

.method public final a([I)V
    .registers 3
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/launcher2/ak;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 145
    return-void
.end method

.method public b(Lcom/android/launcher2/bz;)V
    .registers 4
    .parameter

    .prologue
    .line 89
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    iget v1, p0, Lcom/android/launcher2/ak;->e:I

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setColor(I)V

    .line 90
    return-void
.end method

.method public final c(Lcom/android/launcher2/bz;)V
    .registers 2
    .parameter

    .prologue
    .line 94
    return-void
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/android/launcher2/ak;->d:Z

    return v0
.end method

.method public d(Lcom/android/launcher2/bz;)V
    .registers 4
    .parameter

    .prologue
    .line 97
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setColor(I)V

    .line 98
    return-void
.end method

.method public e(Lcom/android/launcher2/bz;)Z
    .registers 3
    .parameter

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method protected getCurrentDrawable()Landroid/graphics/drawable/Drawable;
    .registers 4

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/android/launcher2/ak;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 73
    const/4 v0, 0x0

    :goto_5
    array-length v2, v1

    if-lt v0, v2, :cond_a

    .line 78
    const/4 v0, 0x0

    :goto_9
    return-object v0

    .line 74
    :cond_a
    aget-object v2, v1, v0

    if-eqz v2, :cond_11

    .line 75
    aget-object v0, v1, v0

    goto :goto_9

    .line 73
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public final getDropTargetDelegate$2b911dda()Lcom/android/launcher2/bx;
    .registers 2

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHitRect(Landroid/graphics/Rect;)V
    .registers 4
    .parameter

    .prologue
    .line 114
    invoke-super {p0, p1}, Landroid/widget/TextView;->getHitRect(Landroid/graphics/Rect;)V

    .line 115
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/android/launcher2/ak;->f:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 116
    return-void
.end method

.method setLauncher(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/launcher2/ak;->b:Lcom/android/launcher2/Launcher;

    .line 61
    return-void
.end method

.method public setSearchDropTargetBar(Lcom/android/launcher2/SearchDropTargetBar;)V
    .registers 2
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lcom/android/launcher2/ak;->c:Lcom/android/launcher2/SearchDropTargetBar;

    .line 69
    return-void
.end method
