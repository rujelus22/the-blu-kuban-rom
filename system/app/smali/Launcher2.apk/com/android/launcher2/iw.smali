.class final Lcom/android/launcher2/iw;
.super Lcom/android/launcher2/iu;
.source "SourceFile"


# instance fields
.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field t:Landroid/appwidget/AppWidgetProviderInfo;

.field u:Landroid/appwidget/AppWidgetHostView;

.field v:Ljava/lang/String;

.field w:Landroid/os/Parcelable;


# direct methods
.method public constructor <init>(Landroid/appwidget/AppWidgetProviderInfo;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/android/launcher2/iu;-><init>()V

    .line 65
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher2/iw;->i:I

    .line 66
    iput-object p1, p0, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    .line 67
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/android/launcher2/iw;->a:Landroid/content/ComponentName;

    .line 68
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iput v0, p0, Lcom/android/launcher2/iw;->b:I

    .line 69
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    iput v0, p0, Lcom/android/launcher2/iw;->c:I

    .line 70
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeWidth:I

    iput v0, p0, Lcom/android/launcher2/iw;->d:I

    .line 71
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->minResizeHeight:I

    iput v0, p0, Lcom/android/launcher2/iw;->e:I

    .line 72
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iput v0, p0, Lcom/android/launcher2/iw;->f:I

    .line 73
    iget v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    iput v0, p0, Lcom/android/launcher2/iw;->g:I

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/iw;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/android/launcher2/iu;-><init>()V

    .line 82
    iget v0, p1, Lcom/android/launcher2/iw;->b:I

    iput v0, p0, Lcom/android/launcher2/iw;->b:I

    .line 83
    iget v0, p1, Lcom/android/launcher2/iw;->c:I

    iput v0, p0, Lcom/android/launcher2/iw;->c:I

    .line 84
    iget v0, p1, Lcom/android/launcher2/iw;->d:I

    iput v0, p0, Lcom/android/launcher2/iw;->d:I

    .line 85
    iget v0, p1, Lcom/android/launcher2/iw;->e:I

    iput v0, p0, Lcom/android/launcher2/iw;->e:I

    .line 86
    iget v0, p1, Lcom/android/launcher2/iw;->f:I

    iput v0, p0, Lcom/android/launcher2/iw;->f:I

    .line 87
    iget v0, p1, Lcom/android/launcher2/iw;->g:I

    iput v0, p0, Lcom/android/launcher2/iw;->g:I

    .line 88
    iget-object v0, p1, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object v0, p0, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    .line 89
    iget-object v0, p1, Lcom/android/launcher2/iw;->u:Landroid/appwidget/AppWidgetHostView;

    iput-object v0, p0, Lcom/android/launcher2/iw;->u:Landroid/appwidget/AppWidgetHostView;

    .line 90
    iget-object v0, p1, Lcom/android/launcher2/iw;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/launcher2/iw;->v:Ljava/lang/String;

    .line 91
    iget-object v0, p1, Lcom/android/launcher2/iw;->w:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/android/launcher2/iw;->w:Landroid/os/Parcelable;

    .line 92
    iget-object v0, p1, Lcom/android/launcher2/iw;->a:Landroid/content/ComponentName;

    iput-object v0, p0, Lcom/android/launcher2/iw;->a:Landroid/content/ComponentName;

    .line 93
    iget v0, p1, Lcom/android/launcher2/iw;->i:I

    iput v0, p0, Lcom/android/launcher2/iw;->i:I

    .line 94
    iget v0, p1, Lcom/android/launcher2/iw;->n:I

    iput v0, p0, Lcom/android/launcher2/iw;->n:I

    .line 95
    iget v0, p1, Lcom/android/launcher2/iw;->o:I

    iput v0, p0, Lcom/android/launcher2/iw;->o:I

    .line 96
    iget v0, p1, Lcom/android/launcher2/iw;->p:I

    iput v0, p0, Lcom/android/launcher2/iw;->p:I

    .line 97
    iget v0, p1, Lcom/android/launcher2/iw;->q:I

    iput v0, p0, Lcom/android/launcher2/iw;->q:I

    .line 98
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Widget: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/launcher2/iw;->a:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
