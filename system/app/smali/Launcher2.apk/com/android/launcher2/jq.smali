.class abstract Lcom/android/launcher2/jq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/ThreadLocal;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/jq;->a:Ljava/lang/ThreadLocal;

    .line 195
    return-void
.end method


# virtual methods
.method abstract a()Ljava/lang/Object;
.end method

.method public final a(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 200
    iget-object v0, p0, Lcom/android/launcher2/jq;->a:Ljava/lang/ThreadLocal;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 201
    return-void
.end method

.method public final b()Ljava/lang/Object;
    .registers 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/launcher2/jq;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 206
    if-nez v0, :cond_19

    .line 207
    invoke-virtual {p0}, Lcom/android/launcher2/jq;->a()Ljava/lang/Object;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/android/launcher2/jq;->a:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 216
    :cond_18
    :goto_18
    return-object v0

    .line 211
    :cond_19
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 212
    if-nez v0, :cond_18

    .line 213
    invoke-virtual {p0}, Lcom/android/launcher2/jq;->a()Ljava/lang/Object;

    move-result-object v0

    .line 214
    iget-object v1, p0, Lcom/android/launcher2/jq;->a:Ljava/lang/ThreadLocal;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_18
.end method
