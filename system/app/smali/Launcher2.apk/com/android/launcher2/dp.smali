.class final Lcom/android/launcher2/dp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Lcom/android/launcher2/AppsCustomizeTabHost;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/AppsCustomizeTabHost;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/dp;->a:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/dp;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/android/launcher2/dp;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 3036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 4
    .parameter

    .prologue
    .line 3039
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 3040
    iget-object v1, p0, Lcom/android/launcher2/dp;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dp;->b:Landroid/view/View;

    invoke-static {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;F)V

    .line 3041
    iget-object v1, p0, Lcom/android/launcher2/dp;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dp;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;F)V

    .line 3042
    return-void
.end method
