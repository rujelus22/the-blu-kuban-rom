.class public Lcom/android/launcher2/DeleteDropTarget;
.super Lcom/android/launcher2/ak;
.source "SourceFile"


# static fields
.field private static f:I

.field private static g:I

.field private static h:F

.field private static i:I

.field private static j:I


# instance fields
.field private final k:I

.field private l:Landroid/content/res/ColorStateList;

.field private m:Landroid/graphics/drawable/TransitionDrawable;

.field private n:Landroid/graphics/drawable/TransitionDrawable;

.field private o:Landroid/graphics/drawable/TransitionDrawable;

.field private p:Z

.field private q:Z

.field private final r:Landroid/os/Handler;

.field private final s:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 47
    const/16 v0, 0x11d

    sput v0, Lcom/android/launcher2/DeleteDropTarget;->f:I

    .line 48
    const/16 v0, 0x15e

    sput v0, Lcom/android/launcher2/DeleteDropTarget;->g:I

    .line 49
    const v0, 0x3d0f5c29

    sput v0, Lcom/android/launcher2/DeleteDropTarget;->h:F

    .line 50
    const/4 v0, 0x0

    sput v0, Lcom/android/launcher2/DeleteDropTarget;->i:I

    .line 51
    const/4 v0, 0x1

    sput v0, Lcom/android/launcher2/DeleteDropTarget;->j:I

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/DeleteDropTarget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/ak;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    sget v0, Lcom/android/launcher2/DeleteDropTarget;->j:I

    iput v0, p0, Lcom/android/launcher2/DeleteDropTarget;->k:I

    .line 490
    iput-boolean v1, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    .line 491
    iput-boolean v1, p0, Lcom/android/launcher2/DeleteDropTarget;->q:Z

    .line 493
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->r:Landroid/os/Handler;

    .line 495
    new-instance v0, Lcom/android/launcher2/bc;

    invoke-direct {v0, p0}, Lcom/android/launcher2/bc;-><init>(Lcom/android/launcher2/DeleteDropTarget;)V

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->s:Ljava/lang/Runnable;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/DeleteDropTarget;Lcom/android/launcher2/bz;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 244
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/anddoes/launcher/ag;

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/anddoes/launcher/ag;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ag;->b()V

    :cond_f
    :goto_f
    return-void

    :cond_10
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/di;

    iget-boolean v3, p0, Lcom/android/launcher2/DeleteDropTarget;->q:Z

    if-nez v3, :cond_20

    iget-object v3, p1, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    invoke-static {v3, v0}, Lcom/android/launcher2/DeleteDropTarget;->b(Lcom/android/launcher2/bt;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_81

    :cond_20
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_4a

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/jd;

    iget-object v2, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/content/ComponentName;)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    iget-wide v2, v0, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/bb;->c(J)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    iget-wide v2, v0, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/bc;->a(J)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto :goto_f

    :cond_4a
    instance-of v1, v0, Lcom/android/launcher2/fz;

    if-eqz v1, :cond_75

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/fz;

    iget-object v2, p0, Lcom/android/launcher2/DeleteDropTarget;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    iget v1, v1, Lcom/android/launcher2/fz;->a:I

    invoke-virtual {v2, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    if-eqz v1, :cond_66

    iget-object v2, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Launcher;->b(Landroid/content/ComponentName;)V

    :cond_66
    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    iget-wide v2, v0, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/bc;->a(J)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto :goto_f

    :cond_75
    instance-of v1, v0, Lcom/android/launcher2/h;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    check-cast v0, Lcom/android/launcher2/h;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/h;)V

    goto :goto_f

    :cond_81
    invoke-static {p1}, Lcom/android/launcher2/DeleteDropTarget;->f(Lcom/android/launcher2/bz;)Z

    move-result v3

    if-eqz v3, :cond_b1

    iget-object v3, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v3, v3, Lcom/android/launcher2/jd;

    if-eqz v3, :cond_b1

    move v3, v1

    :goto_8e
    if-nez v3, :cond_94

    instance-of v3, v0, Lcom/anddoes/launcher/au;

    if-eqz v3, :cond_b3

    :cond_94
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_a1

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    iget-wide v2, v0, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/bb;->c(J)V

    :cond_a1
    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    iget-wide v2, v0, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/bc;->a(J)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto/16 :goto_f

    :cond_b1
    move v3, v2

    goto :goto_8e

    :cond_b3
    iget-object v3, p1, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    instance-of v3, v3, Lcom/android/launcher2/Workspace;

    if-eqz v3, :cond_d0

    iget-object v3, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v3, v3, Lcom/android/launcher2/cu;

    if-eqz v3, :cond_d0

    move v3, v1

    :goto_c0
    if-eqz v3, :cond_d2

    check-cast v0, Lcom/android/launcher2/cu;

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/cu;)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/cu;)V

    goto/16 :goto_f

    :cond_d0
    move v3, v2

    goto :goto_c0

    :cond_d2
    invoke-static {p1}, Lcom/android/launcher2/DeleteDropTarget;->f(Lcom/android/launcher2/bz;)Z

    move-result v3

    if-eqz v3, :cond_103

    iget-object v3, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v3, v3, Lcom/android/launcher2/fz;

    if-eqz v3, :cond_103

    :goto_de
    if-eqz v1, :cond_f

    iget-object v2, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    move-object v1, v0

    check-cast v1, Lcom/android/launcher2/fz;

    invoke-virtual {v2, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/fz;)V

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v1, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    check-cast v0, Lcom/android/launcher2/fz;

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->d()Lcom/android/launcher2/fx;

    move-result-object v1

    if-eqz v1, :cond_f

    new-instance v2, Lcom/android/launcher2/be;

    const-string v3, "deleteAppWidgetId"

    invoke-direct {v2, p0, v3, v1, v0}, Lcom/android/launcher2/be;-><init>(Lcom/android/launcher2/DeleteDropTarget;Ljava/lang/String;Lcom/android/launcher2/fx;Lcom/android/launcher2/fz;)V

    invoke-virtual {v2}, Lcom/android/launcher2/be;->start()V

    goto/16 :goto_f

    :cond_103
    move v1, v2

    goto :goto_de
.end method

.method static synthetic a(Lcom/android/launcher2/DeleteDropTarget;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 507
    invoke-direct {p0, p1}, Lcom/android/launcher2/DeleteDropTarget;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 508
    if-eqz p1, :cond_26

    .line 509
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->m:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/android/launcher2/DeleteDropTarget;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 513
    :goto_8
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    .line 514
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_25

    .line 515
    if-eqz p1, :cond_2c

    const v0, 0x7f070298

    :goto_22
    invoke-virtual {p0, v0}, Lcom/android/launcher2/DeleteDropTarget;->setText(I)V

    .line 518
    :cond_25
    return-void

    .line 511
    :cond_26
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->n:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/android/launcher2/DeleteDropTarget;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_8

    .line 516
    :cond_2c
    const v0, 0x7f070297

    goto :goto_22
.end method

.method static synthetic a(Lcom/android/launcher2/DeleteDropTarget;)Z
    .registers 2
    .parameter

    .prologue
    .line 490
    iget-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    return v0
.end method

.method private b()V
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 133
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->l:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DeleteDropTarget;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 134
    return-void
.end method

.method static synthetic b(Lcom/android/launcher2/DeleteDropTarget;)V
    .registers 2
    .parameter

    .prologue
    .line 491
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->q:Z

    return-void
.end method

.method private static b(Lcom/android/launcher2/bt;Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 99
    instance-of v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_a

    instance-of v0, p1, Lcom/android/launcher2/h;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method static synthetic c(Lcom/android/launcher2/DeleteDropTarget;)Z
    .registers 2
    .parameter

    .prologue
    .line 491
    iget-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/android/launcher2/DeleteDropTarget;)Landroid/graphics/drawable/TransitionDrawable;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    return-object v0
.end method

.method static synthetic e(Lcom/android/launcher2/DeleteDropTarget;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private static f(Lcom/android/launcher2/bz;)Z
    .registers 2
    .parameter

    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    instance-of v0, v0, Lcom/android/launcher2/Workspace;

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    instance-of v0, v0, Lcom/android/launcher2/Folder;

    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/android/launcher2/ak;->a()V

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->d:Z

    .line 174
    return-void
.end method

.method public final a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 144
    .line 145
    iput-boolean v1, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    .line 148
    instance-of v0, p1, Lcom/android/launcher2/AppsCustomizePagedView;

    if-eqz v0, :cond_14

    instance-of v0, p2, Lcom/android/launcher2/iu;

    if-eqz v0, :cond_14

    move-object v0, p2

    check-cast v0, Lcom/android/launcher2/iu;

    iget v0, v0, Lcom/android/launcher2/iu;->i:I

    packed-switch v0, :pswitch_data_4a

    :cond_14
    :pswitch_14
    move v0, v1

    :goto_15
    if-eqz v0, :cond_48

    move v0, v1

    .line 154
    :goto_18
    invoke-static {p1, p2}, Lcom/android/launcher2/DeleteDropTarget;->b(Lcom/android/launcher2/bt;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_46

    .line 155
    check-cast p2, Lcom/android/launcher2/h;

    .line 156
    iget v3, p2, Lcom/android/launcher2/h;->g:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_41

    .line 157
    iput-boolean v2, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    move v2, v0

    .line 163
    :goto_29
    iget-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    invoke-direct {p0, v0}, Lcom/android/launcher2/DeleteDropTarget;->a(Z)V

    .line 165
    iput-boolean v2, p0, Lcom/android/launcher2/DeleteDropTarget;->d:Z

    .line 166
    invoke-direct {p0}, Lcom/android/launcher2/DeleteDropTarget;->b()V

    .line 167
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_43

    :goto_3b
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 168
    return-void

    :pswitch_3f
    move v0, v2

    .line 148
    goto :goto_15

    :cond_41
    move v2, v1

    .line 159
    goto :goto_29

    .line 167
    :cond_43
    const/16 v1, 0x8

    goto :goto_3b

    :cond_46
    move v2, v0

    goto :goto_29

    :cond_48
    move v0, v2

    goto :goto_18

    .line 148
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_3f
        :pswitch_14
        :pswitch_14
        :pswitch_3f
    .end packed-switch
.end method

.method public final a(Lcom/android/launcher2/bz;)V
    .registers 15
    .parameter

    .prologue
    const v5, 0x3dcccccd

    .line 302
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v1

    iget-object v3, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v3}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v6, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {p0, v1, v3, v4, v6}, Lcom/android/launcher2/DeleteDropTarget;->a(IIII)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v1, v4

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->c:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/android/launcher2/SearchDropTargetBar;->c()V

    new-instance v10, Lcom/android/launcher2/bd;

    invoke-direct {v10, p0, p1}, Lcom/android/launcher2/bd;-><init>(Lcom/android/launcher2/DeleteDropTarget;Lcom/android/launcher2/bz;)V

    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    sget v7, Lcom/android/launcher2/DeleteDropTarget;->f:I

    new-instance v8, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v6, 0x4000

    invoke-direct {v8, v6}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    new-instance v9, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v9}, Landroid/view/animation/LinearInterpolator;-><init>()V

    const/4 v11, 0x0

    const/4 v12, 0x0

    move v6, v5

    invoke-virtual/range {v0 .. v12}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 303
    return-void
.end method

.method public final a(Lcom/android/launcher2/bz;Landroid/graphics/PointF;)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 422
    iget-object v0, p1, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    instance-of v10, v0, Lcom/android/launcher2/AppsCustomizePagedView;

    .line 425
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setColor(I)V

    .line 426
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->a()V

    .line 428
    if-eqz v10, :cond_14

    .line 429
    invoke-direct {p0}, Lcom/android/launcher2/DeleteDropTarget;->b()V

    .line 432
    :cond_14
    iget v0, p0, Lcom/android/launcher2/DeleteDropTarget;->k:I

    sget v1, Lcom/android/launcher2/DeleteDropTarget;->i:I

    if-ne v0, v1, :cond_24

    .line 434
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->c:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher2/SearchDropTargetBar;->c()V

    .line 435
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->c:Lcom/android/launcher2/SearchDropTargetBar;

    invoke-virtual {v0}, Lcom/android/launcher2/SearchDropTargetBar;->b()V

    .line 438
    :cond_24
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 439
    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    .line 440
    sget v11, Lcom/android/launcher2/DeleteDropTarget;->g:I

    .line 441
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    .line 448
    new-instance v12, Lcom/android/launcher2/bh;

    invoke-direct {v12, p0, v5, v6, v11}, Lcom/android/launcher2/bh;-><init>(Lcom/android/launcher2/DeleteDropTarget;JI)V

    .line 464
    const/4 v4, 0x0

    .line 465
    iget v1, p0, Lcom/android/launcher2/DeleteDropTarget;->k:I

    sget v3, Lcom/android/launcher2/DeleteDropTarget;->i:I

    if-ne v1, v3, :cond_bd

    .line 466
    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v1

    iget-object v3, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v3}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v5, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/TransitionDrawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {p0, v1, v3, v4, v5}, Lcom/android/launcher2/DeleteDropTarget;->a(IIII)Landroid/graphics/Rect;

    move-result-object v1

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget-object v4, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v2, v4, v3}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {p2}, Landroid/graphics/PointF;->length()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x3f80

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    int-to-float v0, v0

    const/high16 v6, 0x4000

    div-float/2addr v0, v6

    div-float v0, v4, v0

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v4, v3, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v4, v0

    iget v5, p2, Landroid/graphics/PointF;->y:F

    iget v6, p2, Landroid/graphics/PointF;->x:F

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v5

    int-to-float v8, v0

    iget v0, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v4

    int-to-float v5, v0

    iget v0, v3, Landroid/graphics/Rect;->left:I

    int-to-float v4, v0

    iget v0, v3, Landroid/graphics/Rect;->top:I

    int-to-float v7, v0

    iget v0, v1, Landroid/graphics/Rect;->left:I

    int-to-float v6, v0

    iget v0, v1, Landroid/graphics/Rect;->top:I

    int-to-float v9, v0

    new-instance v3, Lcom/android/launcher2/bf;

    invoke-direct {v3, p0}, Lcom/android/launcher2/bf;-><init>(Lcom/android/launcher2/DeleteDropTarget;)V

    new-instance v0, Lcom/android/launcher2/bg;

    move-object v1, p0

    invoke-direct/range {v0 .. v9}, Lcom/android/launcher2/bg;-><init>(Lcom/android/launcher2/DeleteDropTarget;Lcom/android/launcher2/DragLayer;Landroid/animation/TimeInterpolator;FFFFFF)V

    move-object v4, v0

    .line 471
    :cond_ae
    :goto_ae
    new-instance v7, Lcom/android/launcher2/bi;

    invoke-direct {v7, p0, v10, p1}, Lcom/android/launcher2/bi;-><init>(Lcom/android/launcher2/DeleteDropTarget;ZLcom/android/launcher2/bz;)V

    .line 485
    iget-object v3, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    .line 486
    const/4 v8, 0x0

    const/4 v9, 0x0

    move v5, v11

    move-object v6, v12

    .line 485
    invoke-virtual/range {v2 .. v9}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/animation/ValueAnimator$AnimatorUpdateListener;ILandroid/animation/TimeInterpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 487
    return-void

    .line 467
    :cond_bd
    iget v0, p0, Lcom/android/launcher2/DeleteDropTarget;->k:I

    sget v1, Lcom/android/launcher2/DeleteDropTarget;->j:I

    if-ne v0, v1, :cond_ae

    .line 469
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v2, v0, v4}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    new-instance v1, Lcom/android/launcher2/bj;

    sget v7, Lcom/android/launcher2/DeleteDropTarget;->h:F

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/android/launcher2/bj;-><init>(Lcom/android/launcher2/DragLayer;Landroid/graphics/PointF;Landroid/graphics/Rect;JF)V

    move-object v4, v1

    goto :goto_ae
.end method

.method public final b(Lcom/android/launcher2/bz;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 177
    invoke-super {p0, p1}, Lcom/android/launcher2/ak;->b(Lcom/android/launcher2/bz;)V

    .line 178
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    if-eqz v0, :cond_16

    .line 179
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 180
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    .line 182
    :cond_16
    const/4 v1, 0x1

    .line 183
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/launcher2/jd;

    if-eqz v0, :cond_72

    .line 184
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/jd;

    .line 185
    iget-object v3, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    if-eqz v3, :cond_3d

    .line 186
    iget-object v0, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_3d

    iget-object v3, p0, Lcom/android/launcher2/DeleteDropTarget;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_72

    :cond_3d
    move v0, v2

    .line 194
    :goto_3e
    iget-object v1, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v1, v1, Lcom/android/launcher2/cu;

    if-nez v1, :cond_50

    .line 195
    iget-object v1, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v1, v1, Lcom/anddoes/launcher/au;

    if-nez v1, :cond_50

    .line 196
    iget-object v1, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v1, v1, Lcom/anddoes/launcher/ag;

    if-eqz v1, :cond_51

    :cond_50
    move v0, v2

    .line 199
    :cond_51
    if-eqz v0, :cond_63

    .line 200
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->r:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 201
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->r:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->s:Ljava/lang/Runnable;

    const-wide/16 v3, 0x5dc

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 203
    :cond_63
    iput-boolean v2, p0, Lcom/android/launcher2/DeleteDropTarget;->q:Z

    .line 204
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    iget v1, p0, Lcom/android/launcher2/DeleteDropTarget;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    iget v0, p0, Lcom/android/launcher2/DeleteDropTarget;->e:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DeleteDropTarget;->setTextColor(I)V

    .line 205
    return-void

    :cond_72
    move v0, v1

    goto :goto_3e
.end method

.method public final d(Lcom/android/launcher2/bz;)V
    .registers 4
    .parameter

    .prologue
    .line 208
    invoke-super {p0, p1}, Lcom/android/launcher2/ak;->d(Lcom/android/launcher2/bz;)V

    .line 209
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->r:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/launcher2/DeleteDropTarget;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 210
    iget-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    if-nez v0, :cond_1b

    iget-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->q:Z

    if-eqz v0, :cond_1b

    iget-boolean v0, p1, Lcom/android/launcher2/bz;->e:Z

    if-nez v0, :cond_1b

    .line 211
    iget-boolean v0, p0, Lcom/android/launcher2/DeleteDropTarget;->p:Z

    invoke-direct {p0, v0}, Lcom/android/launcher2/DeleteDropTarget;->a(Z)V

    .line 213
    :cond_1b
    iget-boolean v0, p1, Lcom/android/launcher2/bz;->e:Z

    if-nez v0, :cond_23

    .line 214
    invoke-direct {p0}, Lcom/android/launcher2/DeleteDropTarget;->b()V

    .line 219
    :goto_22
    return-void

    .line 217
    :cond_23
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    iget v1, p0, Lcom/android/launcher2/DeleteDropTarget;->e:I

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setColor(I)V

    goto :goto_22
.end method

.method public final e(Lcom/android/launcher2/bz;)Z
    .registers 3
    .parameter

    .prologue
    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 70
    invoke-super {p0}, Lcom/android/launcher2/ak;->onFinishInflate()V

    .line 73
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->l:Landroid/content/res/ColorStateList;

    .line 76
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 77
    const v0, 0x7f080006

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/DeleteDropTarget;->e:I

    .line 79
    const v0, 0x7f020085

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 78
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->m:Landroid/graphics/drawable/TransitionDrawable;

    .line 80
    const v0, 0x7f020077

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->n:Landroid/graphics/drawable/TransitionDrawable;

    .line 82
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->n:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 83
    iget-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->m:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 87
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getCurrentDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    iput-object v0, p0, Lcom/android/launcher2/DeleteDropTarget;->o:Landroid/graphics/drawable/TransitionDrawable;

    .line 90
    invoke-virtual {p0}, Lcom/android/launcher2/DeleteDropTarget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 91
    const/4 v1, 0x2

    if-ne v0, v1, :cond_57

    .line 92
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_57

    .line 93
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DeleteDropTarget;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_57
    return-void
.end method
