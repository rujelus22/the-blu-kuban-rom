.class public final Lcom/android/launcher2/dg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/animation/ValueAnimator;

.field b:Ljava/lang/Object;

.field c:I

.field private d:J

.field private e:F

.field private f:F

.field private g:Z


# direct methods
.method public constructor <init>(JF)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v3, p0, Lcom/android/launcher2/dg;->g:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/dg;->b:Ljava/lang/Object;

    .line 45
    iput v2, p0, Lcom/android/launcher2/dg;->c:I

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [F

    aput v1, v0, v2

    aput p3, v0, v3

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    .line 49
    iput-wide p1, p0, Lcom/android/launcher2/dg;->d:J

    .line 50
    iput v1, p0, Lcom/android/launcher2/dg;->e:F

    .line 51
    iput p3, p0, Lcom/android/launcher2/dg;->f:F

    .line 53
    iget-object v0, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/dh;

    invoke-direct {v1, p0}, Lcom/android/launcher2/dh;-><init>(Lcom/android/launcher2/dg;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 59
    return-void
.end method


# virtual methods
.method final a(I)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 62
    iget-object v0, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getCurrentPlayTime()J

    move-result-wide v2

    .line 63
    if-ne p1, v10, :cond_45

    iget v0, p0, Lcom/android/launcher2/dg;->f:F

    move v1, v0

    .line 64
    :goto_d
    iget-boolean v0, p0, Lcom/android/launcher2/dg;->g:Z

    if-eqz v0, :cond_49

    iget v0, p0, Lcom/android/launcher2/dg;->e:F

    .line 68
    :goto_13
    iget-object v4, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->cancel()V

    iput v9, p0, Lcom/android/launcher2/dg;->c:I

    .line 72
    iput p1, p0, Lcom/android/launcher2/dg;->c:I

    .line 75
    iget-wide v4, p0, Lcom/android/launcher2/dg;->d:J

    sub-long v2, v4, v2

    .line 76
    iget-object v4, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    const-wide/16 v5, 0x0

    iget-wide v7, p0, Lcom/android/launcher2/dg;->d:J

    invoke-static {v2, v3, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v5, v6, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 78
    iget-object v2, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v0, v3, v9

    aput v1, v3, v10

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 79
    iget-object v0, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 80
    iput-boolean v9, p0, Lcom/android/launcher2/dg;->g:Z

    .line 81
    return-void

    .line 63
    :cond_45
    iget v0, p0, Lcom/android/launcher2/dg;->e:F

    move v1, v0

    goto :goto_d

    .line 65
    :cond_49
    iget-object v0, p0, Lcom/android/launcher2/dg;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_13
.end method
