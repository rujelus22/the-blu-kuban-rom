.class final Lcom/android/launcher2/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field private a:Lcom/android/launcher2/DragLayer;

.field private b:Landroid/graphics/PointF;

.field private c:Landroid/graphics/Rect;

.field private d:J

.field private e:Z

.field private f:F

.field private final g:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/DragLayer;Landroid/graphics/PointF;Landroid/graphics/Rect;JF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3f40

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/bj;->g:Landroid/animation/TimeInterpolator;

    .line 376
    iput-object p1, p0, Lcom/android/launcher2/bj;->a:Lcom/android/launcher2/DragLayer;

    .line 377
    iput-object p2, p0, Lcom/android/launcher2/bj;->b:Landroid/graphics/PointF;

    .line 378
    iput-object p3, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    .line 379
    iput-wide p4, p0, Lcom/android/launcher2/bj;->d:J

    .line 380
    const/high16 v0, 0x3f80

    invoke-virtual {p1}, Lcom/android/launcher2/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, p6

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/bj;->f:F

    .line 381
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 13
    .parameter

    .prologue
    const/high16 v10, 0x447a

    const/high16 v7, 0x4000

    const/high16 v9, 0x3f80

    .line 385
    iget-object v0, p0, Lcom/android/launcher2/bj;->a:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->getAnimatedView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/bu;

    .line 386
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 387
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    .line 389
    iget-boolean v4, p0, Lcom/android/launcher2/bj;->e:Z

    if-nez v4, :cond_4a

    .line 390
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/launcher2/bj;->e:Z

    .line 391
    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getScaleX()F

    move-result v4

    .line 392
    sub-float v5, v4, v9

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v7

    .line 393
    sub-float/2addr v4, v9

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v4, v6

    div-float/2addr v4, v7

    .line 395
    iget-object v6, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    iget v7, v6, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    add-float/2addr v5, v7

    float-to-int v5, v5

    iput v5, v6, Landroid/graphics/Rect;->left:I

    .line 396
    iget-object v5, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    add-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, v5, Landroid/graphics/Rect;->top:I

    .line 399
    :cond_4a
    iget-object v4, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/android/launcher2/bj;->b:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-wide v7, p0, Lcom/android/launcher2/bj;->d:J

    sub-long v7, v2, v7

    long-to-float v7, v7

    mul-float/2addr v6, v7

    div-float/2addr v6, v10

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 400
    iget-object v4, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/android/launcher2/bj;->b:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-wide v7, p0, Lcom/android/launcher2/bj;->d:J

    sub-long v7, v2, v7

    long-to-float v7, v7

    mul-float/2addr v6, v7

    div-float/2addr v6, v10

    add-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 402
    iget-object v4, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/android/launcher2/bu;->setTranslationX(F)V

    .line 403
    iget-object v4, p0, Lcom/android/launcher2/bj;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/android/launcher2/bu;->setTranslationY(F)V

    .line 404
    iget-object v4, p0, Lcom/android/launcher2/bj;->g:Landroid/animation/TimeInterpolator;

    invoke-interface {v4, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    sub-float v1, v9, v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setAlpha(F)V

    .line 406
    iget-object v0, p0, Lcom/android/launcher2/bj;->b:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v4, p0, Lcom/android/launcher2/bj;->f:F

    mul-float/2addr v1, v4

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 407
    iget-object v0, p0, Lcom/android/launcher2/bj;->b:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->y:F

    iget v4, p0, Lcom/android/launcher2/bj;->f:F

    mul-float/2addr v1, v4

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 408
    iput-wide v2, p0, Lcom/android/launcher2/bj;->d:J

    .line 409
    return-void
.end method
