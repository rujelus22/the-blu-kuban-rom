.class public final Lcom/android/launcher2/jk;
.super Landroid/widget/GridView;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/hr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 10
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 11
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 34
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/jk;->setLayerType(ILandroid/graphics/Paint;)V

    .line 35
    return-void
.end method

.method public final a(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p0, p1}, Lcom/android/launcher2/jk;->setNumColumns(I)V

    .line 15
    invoke-virtual {p0, p3}, Lcom/android/launcher2/jk;->setVerticalSpacing(I)V

    .line 16
    invoke-virtual {p0, p2}, Lcom/android/launcher2/jk;->setHorizontalSpacing(I)V

    .line 17
    invoke-virtual {p0, v0}, Lcom/android/launcher2/jk;->setVerticalScrollBarEnabled(Z)V

    .line 18
    invoke-virtual {p0, v0}, Lcom/android/launcher2/jk;->setHorizontalScrollBarEnabled(Z)V

    .line 19
    invoke-virtual {p0, v0}, Lcom/android/launcher2/jk;->setDrawSelectorOnTop(Z)V

    .line 20
    return-void
.end method

.method final b()V
    .registers 3

    .prologue
    .line 47
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/jk;->setLayerType(ILandroid/graphics/Paint;)V

    .line 48
    return-void
.end method

.method public final getPageChildCount()I
    .registers 2

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/android/launcher2/jk;->getChildCount()I

    move-result v0

    return v0
.end method
