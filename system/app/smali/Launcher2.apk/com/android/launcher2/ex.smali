.class final Lcom/android/launcher2/ex;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Lcom/android/launcher2/di;

.field private final synthetic d:Lcom/anddoes/launcher/ui/am;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;Lcom/android/launcher2/di;Lcom/anddoes/launcher/ui/am;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/ex;->b:Landroid/view/View;

    iput-object p3, p0, Lcom/android/launcher2/ex;->c:Lcom/android/launcher2/di;

    iput-object p4, p0, Lcom/android/launcher2/ex;->d:Lcom/anddoes/launcher/ui/am;

    .line 5875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 9
    .parameter

    .prologue
    .line 5878
    iget-object v0, p0, Lcom/android/launcher2/ex;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 5879
    if-eqz v0, :cond_91

    .line 5880
    iget-object v1, p0, Lcom/android/launcher2/ex;->b:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 5881
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Lcom/android/launcher2/Folder;

    if-eqz v1, :cond_97

    .line 5882
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Folder;

    .line 5883
    const/4 v2, 0x0

    .line 5884
    iget-object v1, v0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v3, v1, Lcom/android/launcher2/cu;->j:J

    const-wide/16 v5, -0x65

    cmp-long v1, v3, v5

    if-nez v1, :cond_55

    .line 5885
    iget-object v1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v1

    iget-object v3, v0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-virtual {v1, v3}, Lcom/android/launcher2/Workspace;->b(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/FolderIcon;

    .line 5886
    if-eqz v1, :cond_55

    .line 5887
    invoke-virtual {v1}, Lcom/android/launcher2/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    move-object v2, v1

    .line 5890
    :cond_55
    iget-object v3, v0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-object v1, p0, Lcom/android/launcher2/ex;->c:Lcom/android/launcher2/di;

    check-cast v1, Lcom/android/launcher2/jd;

    invoke-virtual {v3, v1}, Lcom/android/launcher2/cu;->b(Lcom/android/launcher2/di;)V

    .line 5891
    iget-object v1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    iget-object v3, p0, Lcom/android/launcher2/ex;->c:Lcom/android/launcher2/di;

    iget-wide v3, v3, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v3, v4}, Lcom/anddoes/launcher/preference/bb;->c(J)V

    .line 5892
    iget-object v1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->m:Lcom/anddoes/launcher/preference/bc;

    iget-object v3, p0, Lcom/android/launcher2/ex;->c:Lcom/android/launcher2/di;

    iget-wide v3, v3, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v1, v3, v4}, Lcom/anddoes/launcher/preference/bc;->a(J)V

    .line 5893
    iget-object v1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    iget-object v3, p0, Lcom/android/launcher2/ex;->c:Lcom/android/launcher2/di;

    invoke-static {v1, v3}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 5894
    iget-object v1, v0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-object v1, v1, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x1

    if-gt v1, v3, :cond_91

    .line 5895
    iget-object v1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Folder;Z)V

    .line 5896
    if-eqz v2, :cond_91

    .line 5897
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->c()V

    .line 5906
    :cond_91
    :goto_91
    iget-object v0, p0, Lcom/android/launcher2/ex;->d:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 5907
    return-void

    .line 5901
    :cond_97
    iget-object v1, p0, Lcom/android/launcher2/ex;->a:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/ex;->c:Lcom/android/launcher2/di;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/di;)V

    .line 5902
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 5903
    iget-object v1, p0, Lcom/android/launcher2/ex;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    goto :goto_91
.end method
