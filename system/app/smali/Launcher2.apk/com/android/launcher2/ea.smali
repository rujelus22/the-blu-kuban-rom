.class final Lcom/android/launcher2/ea;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;

.field private final synthetic b:Landroid/view/View;

.field private final synthetic c:Z

.field private final synthetic d:Landroid/view/View;

.field private final synthetic e:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLandroid/view/View;Ljava/lang/Runnable;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/ea;->b:Landroid/view/View;

    iput-boolean p3, p0, Lcom/android/launcher2/ea;->c:Z

    iput-object p4, p0, Lcom/android/launcher2/ea;->d:Landroid/view/View;

    iput-object p5, p0, Lcom/android/launcher2/ea;->e:Ljava/lang/Runnable;

    .line 3376
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .registers 2
    .parameter

    .prologue
    .line 3399
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 3385
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3386
    iget-object v0, p0, Lcom/android/launcher2/ea;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3387
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/ea;->b:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/launcher2/ea;->c:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3388
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/ea;->d:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/launcher2/ea;->c:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3389
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 3390
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3392
    :cond_31
    iget-object v0, p0, Lcom/android/launcher2/ea;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_3a

    .line 3393
    iget-object v0, p0, Lcom/android/launcher2/ea;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 3395
    :cond_3a
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .parameter

    .prologue
    .line 3379
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/ea;->b:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/launcher2/ea;->c:Z

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Z)V

    .line 3380
    iget-object v0, p0, Lcom/android/launcher2/ea;->a:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/ea;->d:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/launcher2/ea;->c:Z

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Z)V

    .line 3381
    return-void
.end method
