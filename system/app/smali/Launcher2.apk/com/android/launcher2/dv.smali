.class final Lcom/android/launcher2/dv;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/Launcher;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    .line 1507
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1510
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1511
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 1512
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0, v4}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Z)V

    .line 1513
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->b(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->a()V

    .line 1514
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->c(Lcom/android/launcher2/Launcher;)V

    .line 1518
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->d(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/di;

    move-result-object v0

    iget-wide v0, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_40

    .line 1519
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->a()V

    .line 1520
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v4}, Lcom/android/launcher2/Launcher;->c(Z)V

    .line 1526
    :cond_40
    :goto_40
    return-void

    .line 1522
    :cond_41
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 1523
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Z)V

    .line 1524
    iget-object v0, p0, Lcom/android/launcher2/dv;->a:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->c(Lcom/android/launcher2/Launcher;)V

    goto :goto_40
.end method
