.class public Lcom/android/launcher2/SearchDropTargetBar;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/bl;


# static fields
.field private static final c:Landroid/view/animation/AccelerateInterpolator;


# instance fields
.field private a:Landroid/animation/ObjectAnimator;

.field private b:Landroid/animation/ObjectAnimator;

.field private d:Z

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Lcom/android/launcher2/ak;

.field private h:Lcom/android/launcher2/ak;

.field private i:I

.field private j:Z

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Z

.field private m:Lcom/android/launcher2/Launcher;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 46
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    .line 45
    sput-object v0, Lcom/android/launcher2/SearchDropTargetBar;->c:Landroid/view/animation/AccelerateInterpolator;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/SearchDropTargetBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->j:Z

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    .line 67
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    .line 68
    return-void
.end method

.method private a(Landroid/animation/ObjectAnimator;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 101
    sget-object v0, Lcom/android/launcher2/SearchDropTargetBar;->c:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 102
    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 103
    new-instance v0, Lcom/android/launcher2/iz;

    invoke-direct {v0, p0, p2}, Lcom/android/launcher2/iz;-><init>(Lcom/android/launcher2/SearchDropTargetBar;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 109
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 92
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 94
    :try_start_5
    invoke-virtual {p0}, Landroid/view/View;->buildLayer()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_8} :catch_9

    .line 98
    :goto_8
    return-void

    :catch_9
    move-exception v0

    goto :goto_8
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->j:Z

    if-nez v0, :cond_27

    .line 254
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    if-eqz v0, :cond_26

    .line 256
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 257
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 258
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->k:Z

    if-nez v0, :cond_26

    .line 259
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 260
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 266
    :cond_26
    :goto_26
    return-void

    .line 264
    :cond_27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->j:Z

    goto :goto_26
.end method

.method public final a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/bk;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 71
    invoke-virtual {p2, p0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bl;)V

    .line 72
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->g:Lcom/android/launcher2/ak;

    invoke-virtual {p2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bl;)V

    .line 73
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->h:Lcom/android/launcher2/ak;

    invoke-virtual {p2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bl;)V

    .line 74
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->g:Lcom/android/launcher2/ak;

    invoke-virtual {p2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bx;)V

    .line 75
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->h:Lcom/android/launcher2/ak;

    invoke-virtual {p2, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bx;)V

    .line 76
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->h:Lcom/android/launcher2/ak;

    invoke-virtual {p2, v0}, Lcom/android/launcher2/bk;->c(Lcom/android/launcher2/bx;)V

    .line 77
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->g:Lcom/android/launcher2/ak;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ak;->setLauncher(Lcom/android/launcher2/Launcher;)V

    .line 78
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->h:Lcom/android/launcher2/ak;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ak;->setLauncher(Lcom/android/launcher2/Launcher;)V

    .line 79
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->k:Z

    if-eqz v0, :cond_44

    .line 80
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    const v1, 0x7f0d003c

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_3c

    .line 82
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 84
    :cond_3c
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    .line 87
    :cond_44
    return-void
.end method

.method public final a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 208
    iput-boolean v6, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    .line 209
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->ao:Z

    if-eqz v0, :cond_33

    .line 210
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->k:Z

    if-eqz v0, :cond_33

    .line 211
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/LauncherApplication;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_33

    .line 212
    instance-of v0, p2, Lcom/android/launcher2/jd;

    if-nez v0, :cond_22

    .line 213
    instance-of v0, p2, Lcom/anddoes/launcher/au;

    if-eqz v0, :cond_76

    :cond_22
    move-object v0, p2

    .line 214
    check-cast v0, Lcom/android/launcher2/di;

    .line 215
    iget-wide v1, v0, Lcom/android/launcher2/di;->j:J

    const-wide/16 v3, -0x64

    cmp-long v1, v1, v3

    if-nez v1, :cond_33

    iget v0, v0, Lcom/android/launcher2/di;->m:I

    if-nez v0, :cond_33

    .line 216
    iput-boolean v5, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    .line 228
    :cond_33
    :goto_33
    instance-of v0, p2, Lcom/anddoes/launcher/ag;

    if-eqz v0, :cond_53

    .line 229
    check-cast p2, Lcom/anddoes/launcher/ag;

    .line 230
    iget v0, p2, Lcom/anddoes/launcher/ag;->e:I

    sget v1, Lcom/anddoes/launcher/ag;->a:I

    if-eq v0, v1, :cond_51

    .line 231
    iget v0, p2, Lcom/anddoes/launcher/ag;->e:I

    sget v1, Lcom/anddoes/launcher/ag;->b:I

    if-eq v0, v1, :cond_51

    .line 232
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-ne v0, v6, :cond_53

    .line 233
    :cond_51
    iput-boolean v5, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    .line 236
    :cond_53
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    if-eqz v0, :cond_75

    .line 238
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 239
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 240
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    if-nez v0, :cond_75

    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->k:Z

    if-nez v0, :cond_75

    .line 241
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 242
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 245
    :cond_75
    return-void

    .line 218
    :cond_76
    instance-of v0, p2, Lcom/android/launcher2/fz;

    if-eqz v0, :cond_33

    move-object v0, p2

    .line 219
    check-cast v0, Lcom/android/launcher2/di;

    .line 220
    iget v1, v0, Lcom/android/launcher2/di;->m:I

    if-nez v1, :cond_88

    iget v0, v0, Lcom/android/launcher2/di;->o:I

    if-ne v0, v6, :cond_88

    .line 221
    iput-boolean v5, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    goto :goto_33

    .line 222
    :cond_88
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->g()Lcom/android/launcher2/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->g()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    .line 223
    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_33

    .line 224
    iput-boolean v5, p0, Lcom/android/launcher2/SearchDropTargetBar;->n:Z

    goto :goto_33
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->k:Z

    if-nez v0, :cond_a

    .line 162
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    if-nez v0, :cond_b

    .line 176
    :cond_a
    :goto_a
    return-void

    .line 163
    :cond_b
    if-eqz p1, :cond_1b

    .line 164
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 165
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 174
    :goto_17
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    goto :goto_a

    .line 167
    :cond_1b
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 168
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->l:Z

    if-eqz v0, :cond_2b

    .line 169
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_17

    .line 171
    :cond_2b
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_17
.end method

.method public final a(ZZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    if-eqz v0, :cond_18

    .line 270
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_19

    if-nez p1, :cond_19

    if-nez p2, :cond_19

    .line 273
    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->k:Landroid/graphics/drawable/Drawable;

    .line 274
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 280
    :cond_18
    :goto_18
    return-void

    .line 275
    :cond_19
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_18

    if-nez p1, :cond_21

    if-eqz p2, :cond_18

    .line 277
    :cond_21
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/android/launcher2/SearchDropTargetBar;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_18
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 152
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->a:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 153
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 154
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 155
    return-void
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    if-eqz v0, :cond_5

    .line 191
    :goto_4
    return-void

    .line 179
    :cond_5
    if-eqz p1, :cond_15

    .line 180
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-static {v0}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/view/View;)V

    .line 181
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 190
    :goto_11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->d:Z

    goto :goto_4

    .line 183
    :cond_15
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 184
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->l:Z

    if-eqz v0, :cond_28

    .line 185
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    iget v1, p0, Lcom/android/launcher2/SearchDropTargetBar;->i:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_11

    .line 187
    :cond_28
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_11
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->j:Z

    .line 249
    return-void
.end method

.method public getSearchBarBounds()Landroid/graphics/Rect;
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/high16 v5, 0x3f00

    .line 283
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    if-eqz v0, :cond_54

    .line 284
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    move-result-object v0

    iget v1, v0, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    .line 286
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 287
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 289
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 290
    aget v3, v2, v4

    int-to-float v3, v3

    mul-float/2addr v3, v1

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 291
    aget v3, v2, v6

    int-to-float v3, v3

    mul-float/2addr v3, v1

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v0, Landroid/graphics/Rect;->top:I

    .line 292
    aget v3, v2, v4

    iget-object v4, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v1

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v0, Landroid/graphics/Rect;->right:I

    .line 293
    aget v2, v2, v6

    iget-object v3, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 296
    :goto_53
    return-object v0

    :cond_54
    const/4 v0, 0x0

    goto :goto_53
.end method

.method public getTransitionInDuration()I
    .registers 2

    .prologue
    .line 197
    const/16 v0, 0xc8

    return v0
.end method

.method public getTransitionOutDuration()I
    .registers 2

    .prologue
    .line 200
    const/16 v0, 0xaf

    return v0
.end method

.method protected onFinishInflate()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 113
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 116
    const v0, 0x7f0d004e

    invoke-virtual {p0, v0}, Lcom/android/launcher2/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    .line 117
    const v0, 0x7f0d004f

    invoke-virtual {p0, v0}, Lcom/android/launcher2/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    .line 118
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    const v1, 0x7f0d001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ak;

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->g:Lcom/android/launcher2/ak;

    .line 119
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    const v1, 0x7f0d0019

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ak;

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->h:Lcom/android/launcher2/ak;

    .line 120
    invoke-virtual {p0}, Lcom/android/launcher2/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->i:I

    .line 122
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->g:Lcom/android/launcher2/ak;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/ak;->setSearchDropTargetBar(Lcom/android/launcher2/SearchDropTargetBar;)V

    .line 123
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->h:Lcom/android/launcher2/ak;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/ak;->setSearchDropTargetBar(Lcom/android/launcher2/SearchDropTargetBar;)V

    .line 128
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_56

    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->m:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-nez v0, :cond_9c

    .line 129
    :cond_56
    iput-boolean v7, p0, Lcom/android/launcher2/SearchDropTargetBar;->l:Z

    .line 135
    :goto_58
    iget-boolean v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->l:Z

    if-eqz v0, :cond_9f

    .line 136
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    iget v1, p0, Lcom/android/launcher2/SearchDropTargetBar;->i:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 137
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v4, [F

    .line 138
    iget v3, p0, Lcom/android/launcher2/SearchDropTargetBar;->i:I

    neg-int v3, v3

    int-to-float v3, v3

    aput v3, v2, v6

    aput v5, v2, v7

    .line 137
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->a:Landroid/animation/ObjectAnimator;

    .line 139
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    const-string v1, "translationY"

    new-array v2, v4, [F

    aput v5, v2, v6

    .line 140
    iget v3, p0, Lcom/android/launcher2/SearchDropTargetBar;->i:I

    neg-int v3, v3

    int-to-float v3, v3

    aput v3, v2, v7

    .line 139
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    .line 146
    :goto_8d
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->a:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/animation/ObjectAnimator;Landroid/view/View;)V

    .line 147
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/SearchDropTargetBar;->a(Landroid/animation/ObjectAnimator;Landroid/view/View;)V

    .line 148
    return-void

    .line 131
    :cond_9c
    iput-boolean v6, p0, Lcom/android/launcher2/SearchDropTargetBar;->l:Z

    goto :goto_58

    .line 142
    :cond_9f
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 143
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->f:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_c4

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->a:Landroid/animation/ObjectAnimator;

    .line 144
    iget-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->e:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v4, [F

    fill-array-data v2, :array_cc

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/SearchDropTargetBar;->b:Landroid/animation/ObjectAnimator;

    goto :goto_8d

    .line 143
    nop

    :array_c4
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 144
    :array_cc
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method
