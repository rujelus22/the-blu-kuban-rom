.class public Lcom/android/launcher2/DragLayer;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field private a:Lcom/android/launcher2/bk;

.field private b:[I

.field private c:I

.field private d:I

.field private e:Lcom/android/launcher2/Launcher;

.field private final f:Ljava/util/ArrayList;

.field private g:Lcom/android/launcher2/e;

.field private h:Landroid/animation/ValueAnimator;

.field private i:Landroid/animation/ValueAnimator;

.field private j:Landroid/animation/TimeInterpolator;

.field private k:Lcom/android/launcher2/bu;

.field private l:I

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Landroid/graphics/Rect;

.field private p:I

.field private q:I

.field private r:Z

.field private s:Landroid/graphics/drawable/Drawable;

.field private t:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->b:[I

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->f:Ljava/util/ArrayList;

    .line 62
    iput-object v2, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    .line 63
    iput-object v2, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    .line 64
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc0

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->j:Landroid/animation/TimeInterpolator;

    .line 65
    iput-object v2, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    .line 66
    iput v3, p0, Lcom/android/launcher2/DragLayer;->l:I

    .line 67
    iput-object v2, p0, Lcom/android/launcher2/DragLayer;->m:Landroid/view/View;

    .line 69
    iput-boolean v3, p0, Lcom/android/launcher2/DragLayer;->n:Z

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->o:Landroid/graphics/Rect;

    .line 71
    iput v4, p0, Lcom/android/launcher2/DragLayer;->p:I

    .line 72
    iput v4, p0, Lcom/android/launcher2/DragLayer;->q:I

    .line 87
    invoke-virtual {p0, v3}, Lcom/android/launcher2/DragLayer;->setMotionEventSplittingEnabled(Z)V

    .line 88
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DragLayer;->setChildrenDrawingOrderEnabled(Z)V

    .line 89
    invoke-virtual {p0, p0}, Lcom/android/launcher2/DragLayer;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 91
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->s:Landroid/graphics/drawable/Drawable;

    .line 92
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->t:Landroid/graphics/drawable/Drawable;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bu;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    return-object v0
.end method

.method private a(Z)V
    .registers 7
    .parameter

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 212
    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 214
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_32

    .line 215
    if-eqz p1, :cond_33

    const v1, 0x7f0702c0

    .line 217
    :goto_17
    const/16 v2, 0x8

    .line 216
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 218
    invoke-virtual {p0, v2}, Lcom/android/launcher2/DragLayer;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 219
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 222
    :cond_32
    return-void

    .line 215
    :cond_33
    const v1, 0x7f0702bf

    goto :goto_17
.end method

.method private a(Landroid/view/MotionEvent;Z)Z
    .registers 14
    .parameter
    .parameter

    .prologue
    .line 122
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 123
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v5, v0

    .line 124
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v6, v0

    .line 126
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_15
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_52

    .line 139
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v1

    .line 140
    if-eqz v1, :cond_144

    if-eqz p2, :cond_144

    .line 141
    invoke-virtual {v1}, Lcom/android/launcher2/Folder;->a()Z

    move-result v0

    if-eqz v0, :cond_133

    .line 142
    invoke-virtual {v1}, Lcom/android/launcher2/Folder;->getEditTextRegion()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/android/launcher2/DragLayer;->o:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v2}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->o:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_130

    const/4 v0, 0x1

    :goto_4b
    if-nez v0, :cond_133

    .line 143
    invoke-virtual {v1}, Lcom/android/launcher2/Folder;->b()V

    .line 144
    const/4 v0, 0x1

    .line 154
    :goto_51
    return v0

    .line 126
    :cond_52
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/e;

    .line 127
    invoke-virtual {v0, v4}, Lcom/android/launcher2/e;->getHitRect(Landroid/graphics/Rect;)V

    .line 128
    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 129
    invoke-virtual {v0}, Lcom/android/launcher2/e;->getLeft()I

    move-result v1

    sub-int v8, v5, v1

    invoke-virtual {v0}, Lcom/android/launcher2/e;->getTop()I

    move-result v1

    sub-int v9, v6, v1

    iget v1, v0, Lcom/android/launcher2/e;->n:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_115

    const/4 v1, 0x1

    :goto_74
    iget v2, v0, Lcom/android/launcher2/e;->n:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_118

    const/4 v2, 0x1

    :goto_7b
    iget v3, v0, Lcom/android/launcher2/e;->u:I

    if-ge v8, v3, :cond_11b

    if-eqz v1, :cond_11b

    const/4 v3, 0x1

    :goto_82
    iput-boolean v3, v0, Lcom/android/launcher2/e;->f:Z

    invoke-virtual {v0}, Lcom/android/launcher2/e;->getWidth()I

    move-result v3

    iget v10, v0, Lcom/android/launcher2/e;->u:I

    sub-int/2addr v3, v10

    if-le v8, v3, :cond_11e

    if-eqz v1, :cond_11e

    const/4 v1, 0x1

    :goto_90
    iput-boolean v1, v0, Lcom/android/launcher2/e;->g:Z

    iget v1, v0, Lcom/android/launcher2/e;->u:I

    iget v3, v0, Lcom/android/launcher2/e;->v:I

    add-int/2addr v1, v3

    if-ge v9, v1, :cond_121

    if-eqz v2, :cond_121

    const/4 v1, 0x1

    :goto_9c
    iput-boolean v1, v0, Lcom/android/launcher2/e;->h:Z

    invoke-virtual {v0}, Lcom/android/launcher2/e;->getHeight()I

    move-result v1

    iget v3, v0, Lcom/android/launcher2/e;->u:I

    sub-int/2addr v1, v3

    iget v3, v0, Lcom/android/launcher2/e;->w:I

    add-int/2addr v1, v3

    if-le v9, v1, :cond_124

    if-eqz v2, :cond_124

    const/4 v1, 0x1

    :goto_ad
    iput-boolean v1, v0, Lcom/android/launcher2/e;->i:Z

    iget-boolean v1, v0, Lcom/android/launcher2/e;->f:Z

    if-nez v1, :cond_126

    iget-boolean v1, v0, Lcom/android/launcher2/e;->g:Z

    if-nez v1, :cond_126

    iget-boolean v1, v0, Lcom/android/launcher2/e;->h:Z

    if-nez v1, :cond_126

    iget-boolean v1, v0, Lcom/android/launcher2/e;->i:Z

    if-nez v1, :cond_126

    const/4 v1, 0x0

    :goto_c0
    invoke-virtual {v0}, Lcom/android/launcher2/e;->getMeasuredWidth()I

    move-result v2

    iput v2, v0, Lcom/android/launcher2/e;->j:I

    invoke-virtual {v0}, Lcom/android/launcher2/e;->getMeasuredHeight()I

    move-result v2

    iput v2, v0, Lcom/android/launcher2/e;->k:I

    invoke-virtual {v0}, Lcom/android/launcher2/e;->getLeft()I

    move-result v2

    iput v2, v0, Lcom/android/launcher2/e;->l:I

    invoke-virtual {v0}, Lcom/android/launcher2/e;->getTop()I

    move-result v2

    iput v2, v0, Lcom/android/launcher2/e;->m:I

    if-eqz v1, :cond_106

    iget-object v3, v0, Lcom/android/launcher2/e;->b:Landroid/widget/ImageView;

    iget-boolean v2, v0, Lcom/android/launcher2/e;->f:Z

    if-eqz v2, :cond_128

    const/high16 v2, 0x3f80

    :goto_e2
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, v0, Lcom/android/launcher2/e;->c:Landroid/widget/ImageView;

    iget-boolean v2, v0, Lcom/android/launcher2/e;->g:Z

    if-eqz v2, :cond_12a

    const/high16 v2, 0x3f80

    :goto_ed
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, v0, Lcom/android/launcher2/e;->d:Landroid/widget/ImageView;

    iget-boolean v2, v0, Lcom/android/launcher2/e;->h:Z

    if-eqz v2, :cond_12c

    const/high16 v2, 0x3f80

    :goto_f8
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v3, v0, Lcom/android/launcher2/e;->e:Landroid/widget/ImageView;

    iget-boolean v2, v0, Lcom/android/launcher2/e;->i:Z

    if-eqz v2, :cond_12e

    const/high16 v2, 0x3f80

    :goto_103
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_106
    if-eqz v1, :cond_15

    .line 130
    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->g:Lcom/android/launcher2/e;

    .line 131
    iput v5, p0, Lcom/android/launcher2/DragLayer;->c:I

    .line 132
    iput v6, p0, Lcom/android/launcher2/DragLayer;->d:I

    .line 133
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DragLayer;->requestDisallowInterceptTouchEvent(Z)V

    .line 134
    const/4 v0, 0x1

    goto/16 :goto_51

    .line 129
    :cond_115
    const/4 v1, 0x0

    goto/16 :goto_74

    :cond_118
    const/4 v2, 0x0

    goto/16 :goto_7b

    :cond_11b
    const/4 v3, 0x0

    goto/16 :goto_82

    :cond_11e
    const/4 v1, 0x0

    goto/16 :goto_90

    :cond_121
    const/4 v1, 0x0

    goto/16 :goto_9c

    :cond_124
    const/4 v1, 0x0

    goto :goto_ad

    :cond_126
    const/4 v1, 0x1

    goto :goto_c0

    :cond_128
    const/4 v2, 0x0

    goto :goto_e2

    :cond_12a
    const/4 v2, 0x0

    goto :goto_ed

    :cond_12c
    const/4 v2, 0x0

    goto :goto_f8

    :cond_12e
    const/4 v2, 0x0

    goto :goto_103

    .line 142
    :cond_130
    const/4 v0, 0x0

    goto/16 :goto_4b

    .line 148
    :cond_133
    invoke-virtual {p0, v1, v4}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 149
    invoke-direct {p0, v1, p1}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/Folder;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_144

    .line 150
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->h()V

    .line 151
    const/4 v0, 0x1

    goto/16 :goto_51

    .line 154
    :cond_144
    const/4 v0, 0x0

    goto/16 :goto_51
.end method

.method private a(Lcom/android/launcher2/Folder;Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->o:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 115
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->o:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 116
    const/4 v0, 0x1

    .line 118
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method static synthetic b(Lcom/android/launcher2/DragLayer;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->m:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/android/launcher2/DragLayer;)I
    .registers 2
    .parameter

    .prologue
    .line 66
    iget v0, p0, Lcom/android/launcher2/DragLayer;->l:I

    return v0
.end method

.method static synthetic d(Lcom/android/launcher2/DragLayer;)V
    .registers 4
    .parameter

    .prologue
    .line 664
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_38

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/bq;

    invoke-direct {v1, p0}, Lcom/android/launcher2/bq;-><init>(Lcom/android/launcher2/DragLayer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/br;

    invoke-direct {v1, p0}, Lcom/android/launcher2/br;-><init>(Lcom/android/launcher2/DragLayer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_38
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method static synthetic e(Lcom/android/launcher2/DragLayer;)Lcom/android/launcher2/bk;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    return-object v0
.end method

.method private e()V
    .registers 2

    .prologue
    .line 700
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_1c

    .line 701
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DragLayer;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/DragLayer;->p:I

    .line 702
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->j()Lcom/android/launcher2/SearchDropTargetBar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/DragLayer;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/DragLayer;->q:I

    .line 704
    :cond_1c
    return-void
.end method

.method static synthetic f(Lcom/android/launcher2/DragLayer;)V
    .registers 2
    .parameter

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/graphics/Rect;)F
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 272
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->b:[I

    aput v4, v0, v4

    .line 273
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->b:[I

    aput v4, v0, v5

    .line 274
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->b:[I

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;[I)F

    move-result v0

    .line 275
    iget-object v1, p0, Lcom/android/launcher2/DragLayer;->b:[I

    aget v1, v1, v4

    iget-object v2, p0, Lcom/android/launcher2/DragLayer;->b:[I

    aget v2, v2, v5

    .line 276
    iget-object v3, p0, Lcom/android/launcher2/DragLayer;->b:[I

    aget v3, v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/launcher2/DragLayer;->b:[I

    aget v4, v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 275
    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 277
    return v0
.end method

.method public final a()V
    .registers 4

    .prologue
    .line 395
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_19

    .line 396
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 400
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 402
    :cond_19
    return-void

    .line 396
    :cond_1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/e;

    .line 397
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/launcher2/e;->a(Z)V

    invoke-virtual {v0}, Lcom/android/launcher2/e;->requestLayout()V

    .line 398
    invoke-virtual {p0, v0}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    goto :goto_e
.end method

.method public final a(Landroid/view/View;Lcom/android/launcher2/CellLayout;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 414
    new-instance v0, Lcom/android/launcher2/e;

    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p0}, Lcom/android/launcher2/e;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/DragLayer;)V

    .line 417
    new-instance v1, Lcom/android/launcher2/DragLayer$LayoutParams;

    invoke-direct {v1, v2, v2}, Lcom/android/launcher2/DragLayer$LayoutParams;-><init>(II)V

    .line 418
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/launcher2/DragLayer$LayoutParams;->c:Z

    .line 420
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/DragLayer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 421
    iget-object v1, p0, Lcom/android/launcher2/DragLayer;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/e;->b(Z)V

    .line 424
    return-void
.end method

.method public final a(Landroid/view/View;[I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 281
    aput v1, p2, v1

    .line 282
    const/4 v0, 0x1

    aput v1, p2, v0

    .line 283
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;[I)F

    .line 284
    return-void
.end method

.method public final a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/bk;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    .line 97
    iput-object p2, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    .line 98
    return-void
.end method

.method public final a(Lcom/android/launcher2/bu;IIIIFFFLjava/lang/Runnable;IILandroid/view/View;)V
    .registers 28
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 508
    new-instance v4, Landroid/graphics/Rect;

    .line 509
    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v2

    add-int v2, v2, p2

    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v3

    add-int v3, v3, p3

    .line 508
    move/from16 v0, p2

    move/from16 v1, p3

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 510
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v2

    add-int v2, v2, p4

    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v3

    add-int v3, v3, p5

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 512
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p11

    move-object/from16 v12, p9

    move/from16 v13, p10

    move-object/from16 v14, p12

    .line 511
    invoke-virtual/range {v2 .. v14}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 513
    return-void
.end method

.method public final a(Lcom/android/launcher2/bu;Landroid/animation/ValueAnimator$AnimatorUpdateListener;ILandroid/animation/TimeInterpolator;Ljava/lang/Runnable;ILandroid/view/View;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 609
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 610
    :cond_9
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 613
    :cond_12
    iput-object p1, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    .line 614
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->c()V

    .line 615
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->d()V

    .line 618
    if-eqz p7, :cond_26

    .line 619
    invoke-virtual {p7}, Landroid/view/View;->getScrollX()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/DragLayer;->l:I

    .line 621
    :cond_26
    iput-object p7, p0, Lcom/android/launcher2/DragLayer;->m:Landroid/view/View;

    .line 624
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    .line 625
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 626
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 627
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_5a

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 628
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 629
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/bp;

    invoke-direct {v1, p0, p5, p6}, Lcom/android/launcher2/bp;-><init>(Lcom/android/launcher2/DragLayer;Ljava/lang/Runnable;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 646
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 647
    return-void

    .line 627
    :array_5a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final a(Lcom/android/launcher2/bu;Landroid/graphics/Rect;Landroid/graphics/Rect;FFFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;ILandroid/view/View;)V
    .registers 27
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 543
    move-object/from16 v0, p3

    iget v1, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    int-to-double v1, v1

    const-wide/high16 v3, 0x4000

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    .line 544
    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-double v3, v3

    const-wide/high16 v5, 0x4000

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    .line 543
    add-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float v2, v1

    .line 545
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 546
    const v1, 0x7f0a0018

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v4, v1

    .line 549
    if-gez p7, :cond_54

    .line 550
    const v1, 0x7f0a0016

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 551
    cmpg-float v5, v2, v4

    if-gez v5, :cond_49

    .line 552
    int-to-float v1, v1

    iget-object v5, p0, Lcom/android/launcher2/DragLayer;->j:Landroid/animation/TimeInterpolator;

    div-float/2addr v2, v4

    invoke-interface {v5, v2}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 554
    :cond_49
    const v2, 0x7f0a0015

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result p7

    .line 558
    :cond_54
    const/4 v1, 0x0

    .line 559
    if-eqz p9, :cond_59

    if-nez p8, :cond_89

    .line 560
    :cond_59
    iget-object v1, p0, Lcom/android/launcher2/DragLayer;->j:Landroid/animation/TimeInterpolator;

    move-object v13, v1

    .line 564
    :goto_5c
    invoke-virtual {p1}, Lcom/android/launcher2/bu;->getAlpha()F

    move-result v10

    .line 565
    invoke-virtual {p1}, Lcom/android/launcher2/bu;->getScaleX()F

    move-result v6

    .line 566
    new-instance v1, Lcom/android/launcher2/bo;

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p9

    move-object/from16 v5, p8

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p4

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    invoke-direct/range {v1 .. v12}, Lcom/android/launcher2/bo;-><init>(Lcom/android/launcher2/DragLayer;Lcom/android/launcher2/bu;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;FFFFFLandroid/graphics/Rect;Landroid/graphics/Rect;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, v1

    move/from16 v5, p7

    move-object v6, v13

    move-object/from16 v7, p10

    move/from16 v8, p11

    move-object/from16 v9, p12

    .line 601
    invoke-virtual/range {v2 .. v9}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/animation/ValueAnimator$AnimatorUpdateListener;ILandroid/animation/TimeInterpolator;Ljava/lang/Runnable;ILandroid/view/View;)V

    .line 603
    return-void

    :cond_89
    move-object v13, v1

    goto :goto_5c
.end method

.method public final a(Lcom/android/launcher2/bu;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 427
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 428
    return-void
.end method

.method public final a(Lcom/android/launcher2/bu;Landroid/view/View;ILjava/lang/Runnable;Landroid/view/View;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 449
    if-nez p2, :cond_3

    .line 502
    :goto_2
    return-void

    .line 452
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/ja;

    .line 454
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 455
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/android/launcher2/ja;->a(Landroid/view/View;)V

    .line 457
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 458
    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v5}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 460
    const/4 v2, 0x2

    new-array v4, v2, [I

    .line 461
    const/4 v2, 0x0

    iget v6, v3, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    aput v6, v4, v2

    .line 462
    const/4 v2, 0x1

    iget v3, v3, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    aput v3, v4, v2

    .line 466
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {p0, v2, v4}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;[I)F

    move-result v9

    .line 467
    const/4 v2, 0x0

    aget v3, v4, v2

    .line 468
    const/4 v2, 0x1

    aget v4, v4, v2

    .line 469
    move-object/from16 v0, p2

    instance-of v2, v0, Landroid/widget/TextView;

    if-eqz v2, :cond_95

    move-object/from16 v2, p2

    .line 470
    check-cast v2, Landroid/widget/TextView;

    .line 475
    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    add-int/2addr v2, v4

    .line 476
    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v6, 0x3f80

    sub-float/2addr v6, v9

    mul-float/2addr v4, v6

    const/high16 v6, 0x4000

    div-float/2addr v4, v6

    sub-float/2addr v2, v4

    float-to-int v7, v2

    .line 477
    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    sub-int v6, v3, v2

    .line 489
    :goto_72
    iget v4, v5, Landroid/graphics/Rect;->left:I

    .line 490
    iget v5, v5, Landroid/graphics/Rect;->top:I

    .line 491
    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 492
    new-instance v11, Lcom/android/launcher2/bn;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v11, p0, v0, v1}, Lcom/android/launcher2/bn;-><init>(Lcom/android/launcher2/DragLayer;Landroid/view/View;Ljava/lang/Runnable;)V

    .line 500
    const/high16 v8, 0x3f80

    .line 501
    const/4 v12, 0x0

    move-object v2, p0

    move-object/from16 v3, p1

    move v10, v9

    move/from16 v13, p3

    move-object/from16 v14, p5

    .line 500
    invoke-virtual/range {v2 .. v14}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;IIIIFFFLjava/lang/Runnable;IILandroid/view/View;)V

    goto/16 :goto_2

    .line 478
    :cond_95
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/android/launcher2/FolderIcon;

    if-eqz v2, :cond_b1

    .line 480
    add-int/lit8 v7, v4, -0x1

    .line 482
    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    sub-int v6, v3, v2

    goto :goto_72

    .line 484
    :cond_b1
    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getHeight()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v2, v6

    int-to-float v2, v2

    mul-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v7, v4, v2

    .line 485
    invoke-virtual/range {p1 .. p1}, Lcom/android/launcher2/bu;->getMeasuredWidth()I

    move-result v2

    .line 486
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    .line 485
    mul-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 486
    div-int/lit8 v2, v2, 0x2

    sub-int v6, v3, v2

    goto :goto_72
.end method

.method public final a(Lcom/android/launcher2/bu;Landroid/view/View;Ljava/lang/Runnable;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 444
    const/4 v3, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/view/View;ILjava/lang/Runnable;Landroid/view/View;)V

    .line 445
    return-void
.end method

.method public final a(Lcom/android/launcher2/bu;[ILjava/lang/Runnable;I)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 433
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 434
    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/DragLayer;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 435
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 436
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 438
    const/4 v0, 0x0

    aget v4, p2, v0

    const/4 v0, 0x1

    aget v5, p2, v0

    const/4 v6, 0x0

    const v7, 0x3dcccccd

    const v8, 0x3dcccccd

    .line 439
    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v9, p3

    move/from16 v11, p4

    .line 438
    invoke-virtual/range {v0 .. v12}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;IIIIFFFLjava/lang/Runnable;IILandroid/view/View;)V

    .line 440
    return-void
.end method

.method public final b(Landroid/view/View;[I)F
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 295
    const/4 v0, 0x2

    new-array v2, v0, [F

    aget v0, p2, v6

    int-to-float v0, v0

    aput v0, v2, v6

    aget v0, p2, v7

    int-to-float v0, v0

    aput v0, v2, v7

    .line 297
    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 298
    const/high16 v0, 0x3f80

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v1

    mul-float/2addr v0, v1

    .line 299
    aget v1, v2, v6

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    aput v1, v2, v6

    .line 300
    aget v1, v2, v7

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    aput v1, v2, v7

    .line 301
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    move v8, v0

    move-object v0, v1

    move v1, v8

    .line 302
    :goto_38
    instance-of v3, v0, Landroid/view/View;

    if-eqz v3, :cond_3e

    if-ne v0, p0, :cond_4f

    .line 310
    :cond_3e
    aget v0, v2, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, p2, v6

    .line 311
    aget v0, v2, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    aput v0, p2, v7

    .line 312
    return v1

    .line 303
    :cond_4f
    check-cast v0, Landroid/view/View;

    .line 304
    invoke-virtual {v0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 305
    invoke-virtual {v0}, Landroid/view/View;->getScaleX()F

    move-result v3

    mul-float/2addr v1, v3

    .line 306
    aget v3, v2, v6

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v3, v4

    aput v3, v2, v6

    .line 307
    aget v3, v2, v7

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    add-float/2addr v3, v4

    aput v3, v2, v7

    .line 308
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_38
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 650
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 651
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->h:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 653
    :cond_9
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    if-eqz v0, :cond_14

    .line 654
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    iget-object v1, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bu;)V

    .line 656
    :cond_14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    .line 657
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->invalidate()V

    .line 658
    return-void
.end method

.method public final b(Landroid/view/View;Landroid/graphics/Rect;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 316
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 317
    invoke-virtual {p0, v0}, Lcom/android/launcher2/DragLayer;->getLocationInWindow([I)V

    .line 318
    aget v1, v0, v3

    .line 319
    aget v2, v0, v4

    .line 321
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 322
    aget v3, v0, v3

    .line 323
    aget v0, v0, v4

    .line 325
    sub-int v1, v3, v1

    .line 326
    sub-int/2addr v0, v2

    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 328
    return-void
.end method

.method final c()V
    .registers 2

    .prologue
    .line 737
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/DragLayer;->r:Z

    .line 738
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->invalidate()V

    .line 739
    return-void
.end method

.method final d()V
    .registers 2

    .prologue
    .line 742
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/DragLayer;->r:Z

    .line 743
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->invalidate()V

    .line 744
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 748
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 750
    iget-boolean v0, p0, Lcom/android/launcher2/DragLayer;->r:Z

    if-eqz v0, :cond_54

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_54

    .line 751
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    .line 752
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getWidth()I

    move-result v2

    .line 753
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 754
    invoke-virtual {v1, v5}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 756
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getNextPage()I

    move-result v4

    .line 757
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 758
    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1, v4}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/CellLayout;

    .line 760
    if-eqz v0, :cond_55

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getIsDragOverlapping()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 761
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->s:Landroid/graphics/drawable/Drawable;

    iget v1, v3, Landroid/graphics/Rect;->top:I

    .line 762
    iget-object v2, p0, Lcom/android/launcher2/DragLayer;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    .line 761
    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 763
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->s:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 770
    :cond_54
    :goto_54
    return-void

    .line 764
    :cond_55
    if-eqz v1, :cond_54

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getIsDragOverlapping()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 765
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->t:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/android/launcher2/DragLayer;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int v1, v2, v1

    .line 766
    iget v4, v3, Landroid/graphics/Rect;->top:I

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    .line 765
    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 767
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_54
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->a()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x0

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x1

    goto :goto_f
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/bk;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public getAnimatedView()Landroid/view/View;
    .registers 2

    .prologue
    .line 661
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->k:Lcom/android/launcher2/bu;

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 710
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/launcher2/LauncherApplication;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 711
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->getChildDrawingOrder(II)I

    move-result p2

    .line 728
    :cond_f
    :goto_f
    return p2

    .line 714
    :cond_10
    iget v0, p0, Lcom/android/launcher2/DragLayer;->p:I

    if-eq v0, v1, :cond_f

    iget v0, p0, Lcom/android/launcher2/DragLayer;->q:I

    if-eq v0, v1, :cond_f

    .line 715
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->F()Z

    move-result v0

    if-nez v0, :cond_f

    .line 716
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->k:Z

    if-nez v0, :cond_f

    .line 723
    iget v0, p0, Lcom/android/launcher2/DragLayer;->q:I

    if-ne p2, v0, :cond_31

    .line 724
    iget p2, p0, Lcom/android/launcher2/DragLayer;->p:I

    goto :goto_f

    .line 725
    :cond_31
    iget v0, p0, Lcom/android/launcher2/DragLayer;->p:I

    if-ne p2, v0, :cond_f

    .line 726
    iget p2, p0, Lcom/android/launcher2/DragLayer;->q:I

    goto :goto_f
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 691
    invoke-direct {p0}, Lcom/android/launcher2/DragLayer;->e()V

    .line 692
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 696
    invoke-direct {p0}, Lcom/android/launcher2/DragLayer;->e()V

    .line 697
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 170
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-nez v0, :cond_10

    :cond_e
    move v0, v1

    .line 208
    :goto_f
    return v0

    .line 173
    :cond_10
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->e:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getOpenFolder()Lcom/android/launcher2/Folder;

    move-result-object v3

    .line 174
    if-nez v3, :cond_1e

    move v0, v1

    .line 175
    goto :goto_f

    .line 178
    :cond_1e
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v4, "accessibility"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 177
    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 179
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 180
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 182
    packed-switch v0, :pswitch_data_6c

    :cond_37
    :goto_37
    :pswitch_37
    move v0, v1

    .line 208
    goto :goto_f

    .line 184
    :pswitch_39
    invoke-direct {p0, v3, p1}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/Folder;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 185
    if-nez v0, :cond_4a

    .line 186
    invoke-virtual {v3}, Lcom/android/launcher2/Folder;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/DragLayer;->a(Z)V

    .line 187
    iput-boolean v2, p0, Lcom/android/launcher2/DragLayer;->n:Z

    move v0, v2

    .line 188
    goto :goto_f

    .line 189
    :cond_4a
    if-eqz v0, :cond_63

    .line 190
    iput-boolean v1, p0, Lcom/android/launcher2/DragLayer;->n:Z

    .line 195
    :pswitch_4e
    invoke-direct {p0, v3, p1}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/Folder;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 196
    if-nez v0, :cond_65

    iget-boolean v4, p0, Lcom/android/launcher2/DragLayer;->n:Z

    if-nez v4, :cond_65

    .line 197
    invoke-virtual {v3}, Lcom/android/launcher2/Folder;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/DragLayer;->a(Z)V

    .line 198
    iput-boolean v2, p0, Lcom/android/launcher2/DragLayer;->n:Z

    move v0, v2

    .line 199
    goto :goto_f

    :cond_63
    move v0, v2

    .line 192
    goto :goto_f

    .line 200
    :cond_65
    if-eqz v0, :cond_6a

    .line 201
    iput-boolean v1, p0, Lcom/android/launcher2/DragLayer;->n:Z

    goto :goto_37

    :cond_6a
    move v0, v2

    .line 203
    goto :goto_f

    .line 182
    :pswitch_data_6c
    .packed-switch 0x7
        :pswitch_4e
        :pswitch_37
        :pswitch_39
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 159
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_e

    .line 160
    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/MotionEvent;Z)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 165
    :goto_d
    return v0

    .line 164
    :cond_e
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->a()V

    .line 165
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/bk;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_d
.end method

.method protected onLayout(ZIIII)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 380
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 381
    invoke-virtual {p0}, Lcom/android/launcher2/DragLayer;->getChildCount()I

    move-result v2

    .line 382
    const/4 v0, 0x0

    move v1, v0

    :goto_9
    if-lt v1, v2, :cond_c

    .line 392
    return-void

    .line 383
    :cond_c
    invoke-virtual {p0, v1}, Lcom/android/launcher2/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 384
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 385
    instance-of v4, v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    if-eqz v4, :cond_31

    .line 386
    check-cast v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    .line 387
    iget-boolean v4, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->c:Z

    if-eqz v4, :cond_31

    .line 388
    iget v4, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    iget v5, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    iget v6, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    iget v7, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    add-int/2addr v6, v7

    iget v7, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    iget v0, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    add-int/2addr v0, v7

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    .line 382
    :cond_31
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 233
    .line 234
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 236
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 237
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 239
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_23

    .line 240
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_23

    .line 241
    invoke-direct {p0, p1, v1}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/MotionEvent;Z)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 261
    :cond_22
    :goto_22
    return v0

    .line 247
    :cond_23
    iget-object v5, p0, Lcom/android/launcher2/DragLayer;->g:Lcom/android/launcher2/e;

    if-eqz v5, :cond_2b

    .line 249
    packed-switch v2, :pswitch_data_84

    :goto_2a
    move v1, v0

    .line 260
    :cond_2b
    :goto_2b
    if-nez v1, :cond_22

    .line 261
    iget-object v0, p0, Lcom/android/launcher2/DragLayer;->a:Lcom/android/launcher2/bk;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/bk;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_22

    .line 251
    :pswitch_34
    iget-object v1, p0, Lcom/android/launcher2/DragLayer;->g:Lcom/android/launcher2/e;

    iget v2, p0, Lcom/android/launcher2/DragLayer;->c:I

    sub-int v2, v3, v2

    iget v3, p0, Lcom/android/launcher2/DragLayer;->d:I

    sub-int v3, v4, v3

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher2/e;->a(II)V

    move v1, v0

    .line 252
    goto :goto_2b

    .line 255
    :pswitch_43
    iget-object v2, p0, Lcom/android/launcher2/DragLayer;->g:Lcom/android/launcher2/e;

    iget v5, p0, Lcom/android/launcher2/DragLayer;->c:I

    sub-int/2addr v3, v5

    iget v5, p0, Lcom/android/launcher2/DragLayer;->d:I

    sub-int/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/e;->a(II)V

    .line 256
    iget-object v2, p0, Lcom/android/launcher2/DragLayer;->g:Lcom/android/launcher2/e;

    iget-object v3, v2, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getCellWidth()I

    move-result v3

    iget-object v4, v2, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getWidthGap()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, v2, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getCellHeight()I

    move-result v4

    iget-object v5, v2, Lcom/android/launcher2/e;->a:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v5}, Lcom/android/launcher2/CellLayout;->getHeightGap()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v2, Lcom/android/launcher2/e;->o:I

    mul-int/2addr v3, v5

    iput v3, v2, Lcom/android/launcher2/e;->s:I

    iget v3, v2, Lcom/android/launcher2/e;->p:I

    mul-int/2addr v3, v4

    iput v3, v2, Lcom/android/launcher2/e;->t:I

    iput v1, v2, Lcom/android/launcher2/e;->q:I

    iput v1, v2, Lcom/android/launcher2/e;->r:I

    new-instance v1, Lcom/android/launcher2/f;

    invoke-direct {v1, v2}, Lcom/android/launcher2/f;-><init>(Lcom/android/launcher2/e;)V

    invoke-virtual {v2, v1}, Lcom/android/launcher2/e;->post(Ljava/lang/Runnable;)Z

    .line 257
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher2/DragLayer;->g:Lcom/android/launcher2/e;

    goto :goto_2a

    .line 249
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_43
        :pswitch_34
        :pswitch_43
    .end packed-switch
.end method
