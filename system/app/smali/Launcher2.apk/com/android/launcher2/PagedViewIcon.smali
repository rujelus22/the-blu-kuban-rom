.class public Lcom/android/launcher2/PagedViewIcon;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private a:Lcom/android/launcher2/ip;

.field private b:Z

.field private c:Landroid/graphics/Bitmap;

.field private d:Lcom/android/launcher2/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/PagedViewIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/PagedViewIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedViewIcon;->b:Z

    .line 59
    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/android/launcher2/Launcher;

    iput-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    .line 60
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedViewIcon;->b:Z

    .line 93
    return-void
.end method

.method public final a(Lcom/android/launcher2/h;Lcom/android/launcher2/ip;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 64
    iget-object v0, p1, Lcom/android/launcher2/h;->d:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->c:Landroid/graphics/Bitmap;

    .line 65
    iput-object p2, p0, Lcom/android/launcher2/PagedViewIcon;->a:Lcom/android/launcher2/ip;

    .line 66
    new-instance v0, Lcom/android/launcher2/ca;

    iget-object v3, p0, Lcom/android/launcher2/PagedViewIcon;->c:Landroid/graphics/Bitmap;

    invoke-direct {v0, v3}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v4, v0, v4, v4}, Lcom/android/launcher2/PagedViewIcon;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 68
    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_81

    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->R:Z

    if-eqz v0, :cond_81

    move v0, v1

    .line 71
    :goto_20
    if-eqz v0, :cond_27

    .line 72
    iget-object v0, p1, Lcom/android/launcher2/h;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 74
    :cond_27
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedViewIcon;->setTag(Ljava/lang/Object;)V

    .line 75
    iget-object v3, p0, Lcom/android/launcher2/PagedViewIcon;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_7d

    .line 76
    const v0, 0x7f0701bf

    .line 75
    :goto_35
    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 77
    iget-object v3, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v3, v3, Lcom/anddoes/launcher/preference/f;->x:I

    .line 78
    if-le v3, v0, :cond_6c

    .line 79
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedViewIcon;->setCompoundDrawablePadding(I)V

    .line 80
    const-string v1, "VERTICAL_CONTINUOUS"

    iget-object v4, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6c

    .line 81
    iget-object v1, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-boolean v1, v1, Lcom/android/launcher2/Launcher;->q:Z

    if-nez v1, :cond_64

    add-int/lit8 v0, v0, 0x1

    if-le v3, v0, :cond_6c

    .line 82
    :cond_64
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedViewIcon;->setSingleLine(Z)V

    .line 83
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewIcon;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 86
    :cond_6c
    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v0, p0}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 87
    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->d:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v1, "drawer_icon_text_color"

    invoke-virtual {v0, p0, v1}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 89
    return-void

    .line 76
    :cond_7d
    const v0, 0x7f0701bd

    goto :goto_35

    :cond_81
    move v0, v2

    goto :goto_20
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedViewIcon;->b:Z

    .line 97
    new-instance v0, Lcom/android/launcher2/io;

    invoke-direct {v0, p0}, Lcom/android/launcher2/io;-><init>(Lcom/android/launcher2/PagedViewIcon;)V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewIcon;->post(Ljava/lang/Runnable;)Z

    .line 103
    return-void
.end method

.method protected drawableStateChanged()V
    .registers 2

    .prologue
    .line 106
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    .line 110
    invoke-virtual {p0}, Lcom/android/launcher2/PagedViewIcon;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 111
    const v0, 0x3ecccccd

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewIcon;->setAlpha(F)V

    .line 112
    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->a:Lcom/android/launcher2/ip;

    if-eqz v0, :cond_18

    .line 113
    iget-object v0, p0, Lcom/android/launcher2/PagedViewIcon;->a:Lcom/android/launcher2/ip;

    invoke-interface {v0, p0}, Lcom/android/launcher2/ip;->a(Lcom/android/launcher2/PagedViewIcon;)V

    .line 118
    :cond_18
    :goto_18
    return-void

    .line 115
    :cond_19
    iget-boolean v0, p0, Lcom/android/launcher2/PagedViewIcon;->b:Z

    if-nez v0, :cond_18

    .line 116
    const/high16 v0, 0x3f80

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedViewIcon;->setAlpha(F)V

    goto :goto_18
.end method
