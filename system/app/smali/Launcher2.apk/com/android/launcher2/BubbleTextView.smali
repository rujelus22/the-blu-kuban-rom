.class public Lcom/android/launcher2/BubbleTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# static fields
.field private static u:J


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field public a:I

.field public b:Z

.field private c:I

.field private final d:Lcom/android/launcher2/cx;

.field private final e:Landroid/graphics/Canvas;

.field private final f:Landroid/graphics/Rect;

.field private g:Z

.field private h:Landroid/graphics/Bitmap;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Z

.field private r:F

.field private s:F

.field private t:J

.field private final v:Landroid/os/Handler;

.field private final w:Ljava/lang/Runnable;

.field private x:Lcom/android/launcher2/Launcher;

.field private y:Lcom/android/launcher2/jd;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 415
    const-wide/16 v0, 0x12c

    sput-wide v0, Lcom/android/launcher2/BubbleTextView;->u:J

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->c:I

    .line 64
    new-instance v0, Lcom/android/launcher2/cx;

    invoke-direct {v0}, Lcom/android/launcher2/cx;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->d:Lcom/android/launcher2/cx;

    .line 65
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->e:Landroid/graphics/Canvas;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->f:Landroid/graphics/Rect;

    .line 414
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    .line 416
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->v:Landroid/os/Handler;

    .line 417
    iput-boolean v2, p0, Lcom/android/launcher2/BubbleTextView;->b:Z

    .line 419
    new-instance v0, Lcom/android/launcher2/ai;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ai;-><init>(Lcom/android/launcher2/BubbleTextView;)V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->w:Ljava/lang/Runnable;

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    .line 429
    iput v2, p0, Lcom/android/launcher2/BubbleTextView;->A:I

    .line 85
    invoke-direct {p0, p1}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/content/Context;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->c:I

    .line 64
    new-instance v0, Lcom/android/launcher2/cx;

    invoke-direct {v0}, Lcom/android/launcher2/cx;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->d:Lcom/android/launcher2/cx;

    .line 65
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->e:Landroid/graphics/Canvas;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->f:Landroid/graphics/Rect;

    .line 414
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    .line 416
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->v:Landroid/os/Handler;

    .line 417
    iput-boolean v2, p0, Lcom/android/launcher2/BubbleTextView;->b:Z

    .line 419
    new-instance v0, Lcom/android/launcher2/ai;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ai;-><init>(Lcom/android/launcher2/BubbleTextView;)V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->w:Ljava/lang/Runnable;

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    .line 429
    iput v2, p0, Lcom/android/launcher2/BubbleTextView;->A:I

    .line 90
    invoke-direct {p0, p1}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/content/Context;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->c:I

    .line 64
    new-instance v0, Lcom/android/launcher2/cx;

    invoke-direct {v0}, Lcom/android/launcher2/cx;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->d:Lcom/android/launcher2/cx;

    .line 65
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->e:Landroid/graphics/Canvas;

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->f:Landroid/graphics/Rect;

    .line 414
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    .line 416
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->v:Landroid/os/Handler;

    .line 417
    iput-boolean v2, p0, Lcom/android/launcher2/BubbleTextView;->b:Z

    .line 419
    new-instance v0, Lcom/android/launcher2/ai;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ai;-><init>(Lcom/android/launcher2/BubbleTextView;)V

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->w:Ljava/lang/Runnable;

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    .line 429
    iput v2, p0, Lcom/android/launcher2/BubbleTextView;->A:I

    .line 95
    invoke-direct {p0, p1}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/content/Context;)V

    .line 96
    return-void
.end method

.method private a(Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 243
    sget v0, Lcom/android/launcher2/cx;->a:I

    .line 245
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getWidth()I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getHeight()I

    move-result v2

    add-int/2addr v2, v0

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 244
    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 247
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 248
    iget-object v2, p0, Lcom/android/launcher2/BubbleTextView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0, v2}, Lcom/android/launcher2/BubbleTextView;->getDrawingRect(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getExtendedPaddingTop()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/Layout;->getLineTop(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScaleX()F

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScaleY()F

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getWidth()I

    move-result v5

    add-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getHeight()I

    move-result v6

    add-int/2addr v6, v0

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->scale(FFFF)V

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollX()I

    move-result v3

    neg-int v3, v3

    div-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollY()I

    move-result v4

    neg-int v4, v4

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v0, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    invoke-virtual {p0, p1}, Lcom/android/launcher2/BubbleTextView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 249
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->d:Lcom/android/launcher2/cx;

    invoke-virtual {v0, v1, p1, p3, p2}, Lcom/android/launcher2/cx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    .line 250
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 252
    return-object v1
.end method

.method static synthetic a(Lcom/android/launcher2/BubbleTextView;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 426
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .registers 6
    .parameter

    .prologue
    .line 99
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    .line 101
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    .line 106
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 107
    const v1, 0x7f080001

    const-string v2, "bubble_text_color"

    .line 106
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->B:I

    .line 108
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 109
    const v1, 0x7f080002

    const-string v2, "bubble_ring_color"

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->C:I

    .line 110
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 111
    const v1, 0x7f080003

    const-string v2, "bubble_shadow_color"

    .line 110
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->D:I

    .line 112
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 113
    const v1, 0x7f080004

    const-string v2, "bubble_background_color"

    .line 112
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->E:I

    .line 114
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 115
    const v1, 0x7f080005

    const-string v2, "bubble_shader_color"

    .line 114
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->F:I

    .line 117
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 118
    const v1, 0x1060012

    const-string v2, "outline_color"

    .line 117
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    .line 116
    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->l:I

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->k:I

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->j:I

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->i:I

    .line 119
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 120
    const v1, 0x7f08000c

    const-string v2, "homescreen_icon_text_color"

    .line 119
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->a:I

    .line 121
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 122
    const v1, 0x7f08000d

    const-string v2, "shadow_large_color"

    .line 121
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->m:I

    .line 123
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 124
    const v1, 0x7f08000e

    const-string v2, "shadow_small_color"

    .line 123
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/BubbleTextView;->n:I

    .line 126
    const/high16 v0, 0x4080

    const/4 v1, 0x0

    const/high16 v2, 0x4000

    iget v3, p0, Lcom/android/launcher2/BubbleTextView;->m:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/android/launcher2/BubbleTextView;->setShadowLayer(FFFI)V

    .line 127
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 521
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-eqz v0, :cond_99

    .line 522
    iget-wide v3, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    sget-wide v5, Lcom/android/launcher2/BubbleTextView;->u:J

    add-long/2addr v3, v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-lez v0, :cond_99

    .line 523
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 524
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 525
    iget v4, p0, Lcom/android/launcher2/BubbleTextView;->r:F

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v5, v4

    .line 526
    iget v4, p0, Lcom/android/launcher2/BubbleTextView;->s:F

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v6, v4

    .line 528
    iget-object v4, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-boolean v4, v4, Lcom/android/launcher2/Launcher;->q:Z

    .line 531
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v7

    if-eqz v7, :cond_3e

    move v4, v2

    .line 534
    :cond_3e
    if-nez v4, :cond_44

    if-le v6, v9, :cond_44

    if-lt v5, v8, :cond_4a

    .line 535
    :cond_44
    if-eqz v4, :cond_99

    if-ge v6, v8, :cond_99

    if-le v5, v9, :cond_99

    .line 536
    :cond_4a
    if-eqz v4, :cond_4d

    move v3, v0

    :cond_4d
    if-eqz v4, :cond_9a

    iget v0, p0, Lcom/android/launcher2/BubbleTextView;->r:F

    :goto_51
    cmpg-float v0, v3, v0

    if-gez v0, :cond_9d

    move v0, v1

    .line 537
    :goto_56
    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->y:Lcom/android/launcher2/jd;

    if-eqz v3, :cond_ae

    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->y:Lcom/android/launcher2/jd;

    iget-wide v3, v3, Lcom/android/launcher2/jd;->j:J

    const-wide/16 v5, -0x65

    cmp-long v3, v3, v5

    if-nez v3, :cond_ae

    if-eqz v0, :cond_9f

    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    iget-object v4, p0, Lcom/android/launcher2/BubbleTextView;->y:Lcom/android/launcher2/jd;

    iget-wide v4, v4, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v3, v4, v5}, Lcom/anddoes/launcher/preference/bb;->a(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/launcher2/BubbleTextView;->z:Ljava/lang/String;

    :goto_74
    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->z:Ljava/lang/String;

    if-eqz v3, :cond_ae

    const-string v3, "DO_NOTHING"

    iget-object v4, p0, Lcom/android/launcher2/BubbleTextView;->z:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_ae

    move v3, v1

    :goto_83
    if-eqz v3, :cond_99

    .line 538
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    .line 539
    iput-boolean v1, p0, Lcom/android/launcher2/BubbleTextView;->b:Z

    .line 540
    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->v:Landroid/os/Handler;

    iget-object v4, p0, Lcom/android/launcher2/BubbleTextView;->w:Ljava/lang/Runnable;

    const-wide/16 v5, 0x1f4

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 541
    if-eqz v0, :cond_b0

    .line 542
    invoke-direct {p0, v1}, Lcom/android/launcher2/BubbleTextView;->a(Z)V

    .line 549
    :cond_99
    :goto_99
    return-void

    .line 536
    :cond_9a
    iget v0, p0, Lcom/android/launcher2/BubbleTextView;->s:F

    goto :goto_51

    :cond_9d
    move v0, v2

    goto :goto_56

    .line 537
    :cond_9f
    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->n:Lcom/anddoes/launcher/preference/bb;

    iget-object v4, p0, Lcom/android/launcher2/BubbleTextView;->y:Lcom/android/launcher2/jd;

    iget-wide v4, v4, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v3, v4, v5}, Lcom/anddoes/launcher/preference/bb;->b(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/launcher2/BubbleTextView;->z:Ljava/lang/String;

    goto :goto_74

    :cond_ae
    move v3, v2

    goto :goto_83

    .line 544
    :cond_b0
    invoke-direct {p0, v2}, Lcom/android/launcher2/BubbleTextView;->a(Z)V

    goto :goto_99
.end method

.method private a(Z)V
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 574
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->a()V

    .line 576
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_64

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-nez v0, :cond_64

    move v0, v1

    .line 577
    :goto_12
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 578
    const v4, 0x7f0b001f

    .line 577
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v4, v3

    .line 579
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 581
    if-eqz v0, :cond_66

    const-string v3, "translationX"

    :goto_27
    new-array v6, v1, [F

    .line 582
    if-eqz p1, :cond_2c

    neg-float v4, v4

    :cond_2c
    aput v4, v6, v2

    .line 580
    invoke-static {p0, v3, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 583
    const-wide/16 v6, 0x7d

    invoke-virtual {v3, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 585
    if-eqz v0, :cond_69

    const-string v0, "translationX"

    :goto_3b
    new-array v1, v1, [F

    const/4 v4, 0x0

    aput v4, v1, v2

    .line 584
    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 586
    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 587
    new-instance v1, Lcom/android/launcher2/aj;

    invoke-direct {v1, p0, p1}, Lcom/android/launcher2/aj;-><init>(Lcom/android/launcher2/BubbleTextView;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 594
    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 595
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 596
    invoke-virtual {v5}, Landroid/animation/AnimatorSet;->start()V

    .line 597
    return-void

    :cond_64
    move v0, v2

    .line 576
    goto :goto_12

    .line 581
    :cond_66
    const-string v3, "translationY"

    goto :goto_27

    .line 585
    :cond_69
    const-string v0, "translationY"

    goto :goto_3b
.end method

.method static synthetic b(Lcom/android/launcher2/BubbleTextView;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->z:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/android/launcher2/BubbleTextView;)Lcom/android/launcher2/jd;
    .registers 2
    .parameter

    .prologue
    .line 427
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->y:Lcom/android/launcher2/jd;

    return-object v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/ja;

    if-eqz v0, :cond_1d

    .line 322
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ja;

    .line 323
    if-eqz v0, :cond_1d

    .line 324
    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 325
    iget-object v1, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1e

    :goto_1a
    invoke-virtual {v0, p0}, Lcom/android/launcher2/CellLayout;->setPressedOrFocusedIcon(Lcom/android/launcher2/BubbleTextView;)V

    .line 328
    :cond_1d
    return-void

    .line 325
    :cond_1e
    const/4 p0, 0x0

    goto :goto_1a
.end method


# virtual methods
.method final a()V
    .registers 2

    .prologue
    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    .line 332
    invoke-direct {p0}, Lcom/android/launcher2/BubbleTextView;->c()V

    .line 333
    return-void
.end method

.method public final a(Lcom/android/launcher2/jd;Lcom/android/launcher2/da;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 130
    .line 131
    iget-boolean v0, p1, Lcom/android/launcher2/jd;->c:Z

    if-eqz v0, :cond_59

    .line 132
    new-instance v0, Lcom/android/launcher2/ca;

    invoke-virtual {p1, p2}, Lcom/android/launcher2/jd;->a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    .line 136
    :cond_f
    :goto_f
    if-nez v0, :cond_1a

    .line 137
    new-instance v0, Lcom/android/launcher2/ca;

    invoke-virtual {p1, p2}, Lcom/android/launcher2/jd;->a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    .line 140
    :cond_1a
    invoke-virtual {p0, v1, v0, v1, v1}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 141
    if-eqz p3, :cond_24

    .line 142
    iget-object v0, p1, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    :cond_24
    iget-object v1, p0, Lcom/android/launcher2/BubbleTextView;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_ca

    .line 145
    const v0, 0x7f0701ad

    .line 144
    :goto_2f
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 146
    iget-object v1, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v1, v1, Lcom/anddoes/launcher/preference/f;->c:I

    .line 147
    iget-object v2, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-boolean v2, v2, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v2, :cond_cf

    .line 148
    if-le v1, v0, :cond_4c

    .line 149
    invoke-virtual {p0, v5}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablePadding(I)V

    .line 154
    :cond_4c
    :goto_4c
    invoke-virtual {p0, p1}, Lcom/android/launcher2/BubbleTextView;->setTag(Ljava/lang/Object;)V

    .line 155
    iput-object p1, p0, Lcom/android/launcher2/BubbleTextView;->y:Lcom/android/launcher2/jd;

    .line 156
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v0, p0}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 157
    return-void

    .line 133
    :cond_59
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090007

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_da

    .line 134
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    iget-object v2, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    if-eqz p1, :cond_c7

    iget-object v0, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    if-eqz v0, :cond_c7

    iget-object v0, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_c7

    const-string v0, "com.anddoes.launcher"

    iget-object v3, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c7

    const-string v0, "com.anddoes.launcher.ACTION"

    iget-object v3, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c7

    const-string v0, "APP_DRAWER"

    iget-object v3, p1, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    const-string v4, "LAUNCHER_ACTION"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c7

    iget-object v0, v2, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    if-eqz v0, :cond_d8

    iget-object v0, v2, Lcom/anddoes/launcher/c/l;->b:Lcom/anddoes/launcher/c/g;

    const-string v3, "all_apps_button_icon"

    invoke-virtual {v0, v3}, Lcom/anddoes/launcher/c/g;->e(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_b4
    if-nez v0, :cond_f

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v3

    if-nez v3, :cond_f

    iget-object v0, v2, Lcom/anddoes/launcher/c/l;->a:Landroid/content/res/Resources;

    const v2, 0x7f020001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_f

    :cond_c7
    move-object v0, v1

    goto/16 :goto_f

    .line 145
    :cond_ca
    const v0, 0x7f0701ab

    goto/16 :goto_2f

    .line 151
    :cond_cf
    add-int/lit8 v0, v0, 0x1

    if-le v1, v0, :cond_4c

    .line 152
    invoke-virtual {p0, v5}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablePadding(I)V

    goto/16 :goto_4c

    :cond_d8
    move-object v0, v1

    goto :goto_b4

    :cond_da
    move-object v0, v1

    goto/16 :goto_f
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 515
    iget v0, p0, Lcom/android/launcher2/BubbleTextView;->A:I

    if-lez v0, :cond_8

    .line 516
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/BubbleTextView;->setCounter(I)V

    .line 518
    :cond_8
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 345
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    .line 346
    if-eqz v0, :cond_30

    .line 347
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollX()I

    move-result v1

    .line 348
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollY()I

    move-result v2

    .line 350
    iget-boolean v3, p0, Lcom/android/launcher2/BubbleTextView;->o:Z

    if-eqz v3, :cond_29

    .line 351
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v7, v7, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 352
    iput-boolean v7, p0, Lcom/android/launcher2/BubbleTextView;->o:Z

    .line 355
    :cond_29
    or-int v3, v1, v2

    if-nez v3, :cond_4c

    .line 356
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 365
    :cond_30
    :goto_30
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getCurrentTextColor()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    if-ne v0, v1, :cond_5c

    .line 366
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->clearShadowLayer()V

    .line 367
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 381
    :goto_4b
    return-void

    .line 358
    :cond_4c
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 359
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 360
    neg-int v0, v1

    int-to-float v0, v0

    neg-int v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_30

    .line 372
    :cond_5c
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/high16 v1, 0x4080

    const/high16 v2, 0x4000

    iget v3, p0, Lcom/android/launcher2/BubbleTextView;->m:I

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 373
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 374
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 375
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollX()I

    move-result v0

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getExtendedPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v2, v0

    .line 376
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getWidth()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v3, v0

    .line 377
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getHeight()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object v0, p1

    .line 375
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 378
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/high16 v1, 0x3fe0

    iget v2, p0, Lcom/android/launcher2/BubbleTextView;->n:I

    invoke-virtual {v0, v1, v6, v6, v2}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 379
    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 380
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_4b
.end method

.method protected drawableStateChanged()V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 174
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 177
    iget-boolean v0, p0, Lcom/android/launcher2/BubbleTextView;->g:Z

    if-nez v0, :cond_10

    .line 178
    invoke-direct {p0}, Lcom/android/launcher2/BubbleTextView;->c()V

    .line 205
    :cond_10
    :goto_10
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    .line 206
    if-eqz v0, :cond_21

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 207
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 209
    :cond_21
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    .line 210
    return-void

    .line 183
    :cond_25
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_4f

    move v0, v1

    .line 184
    :goto_2a
    iget-boolean v3, p0, Lcom/android/launcher2/BubbleTextView;->q:Z

    if-nez v3, :cond_30

    .line 185
    iput-object v4, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    .line 187
    :cond_30
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_43

    .line 188
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    if-nez v3, :cond_51

    .line 191
    iput-object v4, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    .line 196
    :goto_3e
    iput-boolean v2, p0, Lcom/android/launcher2/BubbleTextView;->q:Z

    .line 197
    invoke-direct {p0}, Lcom/android/launcher2/BubbleTextView;->c()V

    .line 199
    :cond_43
    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    if-nez v3, :cond_5e

    .line 200
    :goto_47
    if-nez v0, :cond_10

    if-eqz v1, :cond_10

    .line 201
    invoke-direct {p0}, Lcom/android/launcher2/BubbleTextView;->c()V

    goto :goto_10

    :cond_4f
    move v0, v2

    .line 183
    goto :goto_2a

    .line 194
    :cond_51
    iget-object v3, p0, Lcom/android/launcher2/BubbleTextView;->e:Landroid/graphics/Canvas;

    iget v4, p0, Lcom/android/launcher2/BubbleTextView;->j:I

    iget v5, p0, Lcom/android/launcher2/BubbleTextView;->i:I

    .line 193
    invoke-direct {p0, v3, v4, v5}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    goto :goto_3e

    :cond_5e
    move v1, v2

    .line 199
    goto :goto_47
.end method

.method getPressedOrFocusedBackground()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method getPressedOrFocusedBackgroundPadding()I
    .registers 2

    .prologue
    .line 340
    sget v0, Lcom/android/launcher2/cx;->a:I

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 385
    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    .line 386
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 387
    :cond_c
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    .prologue
    .line 391
    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    .line 392
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 393
    :cond_d
    return-void
.end method

.method protected onSetAlpha(I)Z
    .registers 3
    .parameter

    .prologue
    .line 397
    iget v0, p0, Lcom/android/launcher2/BubbleTextView;->c:I

    if-eq v0, p1, :cond_9

    .line 398
    iput p1, p0, Lcom/android/launcher2/BubbleTextView;->c:I

    .line 399
    invoke-super {p0, p1}, Landroid/widget/TextView;->onSetAlpha(I)Z

    .line 401
    :cond_9
    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter

    .prologue
    .line 259
    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_7c

    .line 309
    :cond_b
    :goto_b
    return v0

    .line 263
    :pswitch_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    .line 264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/BubbleTextView;->r:F

    .line 265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/BubbleTextView;->s:F

    .line 267
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anddoes/launcher/v;->c(Landroid/content/Context;)V

    .line 271
    iget-object v1, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    if-nez v1, :cond_35

    .line 273
    iget-object v1, p0, Lcom/android/launcher2/BubbleTextView;->e:Landroid/graphics/Canvas;

    iget v2, p0, Lcom/android/launcher2/BubbleTextView;->l:I

    iget v3, p0, Lcom/android/launcher2/BubbleTextView;->k:I

    .line 272
    invoke-direct {p0, v1, v2, v3}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    .line 277
    :cond_35
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_42

    .line 278
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/launcher2/BubbleTextView;->g:Z

    .line 279
    invoke-direct {p0}, Lcom/android/launcher2/BubbleTextView;->c()V

    goto :goto_b

    .line 281
    :cond_42
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/launcher2/BubbleTextView;->g:Z

    goto :goto_b

    .line 287
    :pswitch_46
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->isPressed()Z

    move-result v1

    if-nez v1, :cond_b

    .line 288
    iget-boolean v1, p0, Lcom/android/launcher2/BubbleTextView;->b:Z

    if-nez v1, :cond_6b

    iget-wide v1, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_6b

    .line 289
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/launcher2/BubbleTextView;->t:J

    .line 290
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/BubbleTextView;->r:F

    .line 291
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/BubbleTextView;->s:F

    goto :goto_b

    .line 293
    :cond_6b
    invoke-direct {p0, p1}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/view/MotionEvent;)V

    goto :goto_b

    .line 301
    :pswitch_6f
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->isPressed()Z

    move-result v1

    if-nez v1, :cond_b

    .line 302
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    .line 303
    invoke-direct {p0, p1}, Lcom/android/launcher2/BubbleTextView;->a(Landroid/view/MotionEvent;)V

    goto :goto_b

    .line 261
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_6f
        :pswitch_46
        :pswitch_6f
    .end packed-switch
.end method

.method public setCounter(I)V
    .registers 26
    .parameter

    .prologue
    .line 438
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->A:I

    move/from16 v0, p1

    if-eq v2, v0, :cond_43

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->L()Z

    move-result v2

    if-eqz v2, :cond_43

    .line 439
    if-lez p1, :cond_44

    :goto_14
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/launcher2/BubbleTextView;->A:I

    .line 440
    invoke-virtual/range {p0 .. p0}, Lcom/android/launcher2/BubbleTextView;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 441
    instance-of v3, v2, Lcom/android/launcher2/jd;

    if-eqz v3, :cond_43

    .line 442
    check-cast v2, Lcom/android/launcher2/jd;

    .line 443
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/launcher2/BubbleTextView;->x:Lcom/android/launcher2/Launcher;

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->O()Lcom/android/launcher2/da;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/jd;->a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 444
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/launcher2/BubbleTextView;->A:I

    if-nez v3, :cond_47

    .line 445
    const/4 v3, 0x0

    .line 446
    new-instance v4, Lcom/android/launcher2/ca;

    invoke-direct {v4, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 445
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 512
    :cond_43
    :goto_43
    return-void

    .line 439
    :cond_44
    const/16 p1, 0x0

    goto :goto_14

    .line 448
    :cond_47
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->f()F

    move-result v3

    .line 449
    new-instance v11, Landroid/graphics/Canvas;

    invoke-direct {v11}, Landroid/graphics/Canvas;-><init>()V

    .line 450
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 451
    invoke-virtual {v11, v12}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 453
    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 454
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->A:I

    const/16 v4, 0x3e8

    if-ge v2, v4, :cond_186

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->A:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    .line 456
    :goto_70
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->A:I

    const/16 v4, 0xa

    if-ge v2, v4, :cond_18b

    .line 457
    const/high16 v2, 0x4160

    .line 464
    :goto_7a
    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    .line 465
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v14, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 466
    mul-float/2addr v2, v3

    invoke-virtual {v14, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 467
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->B:I

    invoke-virtual {v14, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 468
    const/4 v2, 0x1

    invoke-virtual {v14, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 469
    const/4 v2, 0x0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v14, v10, v2, v4, v13}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 471
    iget v2, v13, Landroid/graphics/Rect;->right:I

    iget v4, v13, Landroid/graphics/Rect;->left:I

    sub-int v15, v2, v4

    .line 472
    iget v2, v13, Landroid/graphics/Rect;->bottom:I

    iget v4, v13, Landroid/graphics/Rect;->top:I

    sub-int v16, v2, v4

    .line 474
    const/high16 v2, 0x41a0

    mul-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v17, v0

    .line 475
    add-int/lit8 v2, v15, 0xa

    move/from16 v0, v17

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v18

    .line 476
    new-instance v19, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v17

    int-to-float v5, v0

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 478
    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 479
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->C:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 480
    const/high16 v2, 0x3f80

    const/4 v3, 0x0

    const/high16 v4, 0x3f80

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/launcher2/BubbleTextView;->D:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 481
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 483
    move/from16 v0, v17

    int-to-float v2, v0

    const v3, 0x3f6e978d

    mul-float/2addr v2, v3

    float-to-int v0, v2

    move/from16 v21, v0

    .line 484
    move/from16 v0, v18

    int-to-float v2, v0

    const v3, 0x3f6e978d

    mul-float/2addr v2, v3

    float-to-int v5, v2

    .line 485
    new-instance v22, Landroid/graphics/RectF;

    sub-int v2, v18, v5

    int-to-float v2, v2

    .line 486
    sub-int v3, v17, v21

    int-to-float v3, v3

    int-to-float v4, v5

    move/from16 v0, v21

    int-to-float v6, v0

    .line 485
    move-object/from16 v0, v22

    invoke-direct {v0, v2, v3, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 488
    new-instance v23, Landroid/graphics/Paint;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Paint;-><init>()V

    .line 489
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->E:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 490
    const/4 v2, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 491
    new-instance v2, Landroid/graphics/LinearGradient;

    div-int/lit8 v3, v5, 0x2

    int-to-float v3, v3

    .line 492
    const/4 v4, 0x0

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    move/from16 v0, v21

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/launcher2/BubbleTextView;->F:I

    .line 493
    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/launcher2/BubbleTextView;->E:I

    sget-object v9, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 491
    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 495
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 496
    move/from16 v0, v18

    if-le v2, v0, :cond_14a

    .line 497
    sub-int v2, v2, v18

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 500
    :cond_14a
    div-int/lit8 v2, v17, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v17, 0x2

    int-to-float v3, v3

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v11, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 501
    div-int/lit8 v2, v21, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v21, 0x2

    int-to-float v3, v3

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v11, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 503
    sub-int v2, v18, v15

    div-int/lit8 v2, v2, 0x2

    iget v3, v13, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    .line 504
    sub-int v3, v17, v16

    div-int/lit8 v3, v3, 0x2

    iget v4, v13, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    .line 505
    invoke-virtual {v11, v10, v2, v3, v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 507
    const/4 v2, 0x0

    .line 508
    new-instance v3, Lcom/android/launcher2/ca;

    invoke-direct {v3, v12}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 507
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_43

    .line 454
    :cond_186
    const-string v2, "999+"

    move-object v10, v2

    goto/16 :goto_70

    .line 458
    :cond_18b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/launcher2/BubbleTextView;->A:I

    const/16 v4, 0x64

    if-ge v2, v4, :cond_197

    .line 459
    const/high16 v2, 0x4140

    goto/16 :goto_7a

    .line 461
    :cond_197
    const/high16 v2, 0x4120

    goto/16 :goto_7a
.end method

.method protected setFrame(IIII)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getLeft()I

    move-result v0

    if-ne v0, p1, :cond_18

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getRight()I

    move-result v0

    if-ne v0, p3, :cond_18

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getTop()I

    move-result v0

    if-ne v0, p2, :cond_18

    invoke-virtual {p0}, Lcom/android/launcher2/BubbleTextView;->getBottom()I

    move-result v0

    if-eq v0, p4, :cond_1b

    .line 162
    :cond_18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/BubbleTextView;->o:Z

    .line 164
    :cond_1b
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setFrame(IIII)Z

    move-result v0

    return v0
.end method

.method setStayPressed(Z)V
    .registers 3
    .parameter

    .prologue
    .line 313
    iput-boolean p1, p0, Lcom/android/launcher2/BubbleTextView;->q:Z

    .line 314
    if-nez p1, :cond_7

    .line 315
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/BubbleTextView;->h:Landroid/graphics/Bitmap;

    .line 317
    :cond_7
    invoke-direct {p0}, Lcom/android/launcher2/BubbleTextView;->c()V

    .line 318
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .registers 3
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/launcher2/BubbleTextView;->p:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_c

    invoke-super {p0, p1}, Landroid/widget/TextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x1

    goto :goto_b
.end method
