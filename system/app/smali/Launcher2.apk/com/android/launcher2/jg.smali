.class public final Lcom/android/launcher2/jg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/hq;


# instance fields
.field final a:J

.field final b:J

.field final c:J

.field d:Lcom/android/launcher2/c;

.field e:Lcom/android/launcher2/CellLayout;

.field private f:Lcom/android/launcher2/Launcher;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 4
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/android/launcher2/jg;->a:J

    .line 22
    const-wide/16 v0, 0x3b6

    iput-wide v0, p0, Lcom/android/launcher2/jg;->b:J

    .line 23
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/android/launcher2/jg;->c:J

    .line 32
    iput-object p1, p0, Lcom/android/launcher2/jg;->f:Lcom/android/launcher2/Launcher;

    .line 33
    new-instance v0, Lcom/android/launcher2/c;

    invoke-direct {v0}, Lcom/android/launcher2/c;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/jg;->d:Lcom/android/launcher2/c;

    .line 34
    iget-object v0, p0, Lcom/android/launcher2/jg;->d:Lcom/android/launcher2/c;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/c;->a(Lcom/android/launcher2/hq;)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 51
    iget-object v0, p0, Lcom/android/launcher2/jg;->e:Lcom/android/launcher2/CellLayout;

    if-eqz v0, :cond_1a

    .line 53
    iget-object v0, p0, Lcom/android/launcher2/jg;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/android/launcher2/jg;->e:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 55
    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v2

    if-eq v1, v2, :cond_19

    .line 56
    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->r(I)V

    .line 61
    :cond_19
    :goto_19
    return-void

    .line 59
    :cond_1a
    iget-object v0, p0, Lcom/android/launcher2/jg;->f:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->g()Lcom/android/launcher2/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/bk;->c()V

    goto :goto_19
.end method
