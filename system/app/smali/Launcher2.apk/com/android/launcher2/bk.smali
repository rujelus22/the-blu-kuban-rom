.class public final Lcom/android/launcher2/bk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I

.field public static b:I


# instance fields
.field private A:I

.field private B:[I

.field private C:Landroid/graphics/Rect;

.field private D:Lcom/anddoes/launcher/av;

.field private E:F

.field private F:F

.field protected c:I

.field private d:Lcom/android/launcher2/Launcher;

.field private e:Landroid/os/Handler;

.field private final f:Landroid/os/Vibrator;

.field private g:Landroid/graphics/Rect;

.field private final h:[I

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:Lcom/android/launcher2/bz;

.field private n:Ljava/util/ArrayList;

.field private o:Ljava/util/ArrayList;

.field private p:Lcom/android/launcher2/bx;

.field private q:Landroid/os/IBinder;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Lcom/android/launcher2/bs;

.field private u:I

.field private v:Lcom/android/launcher2/bm;

.field private w:Lcom/android/launcher2/bx;

.field private x:Landroid/view/inputmethod/InputMethodManager;

.field private y:[I

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 47
    const/4 v0, 0x0

    sput v0, Lcom/android/launcher2/bk;->a:I

    .line 50
    const/4 v0, 0x1

    sput v0, Lcom/android/launcher2/bk;->b:I

    .line 43
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/bk;->g:Landroid/graphics/Rect;

    .line 73
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher2/bk;->h:[I

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/bk;->n:Ljava/util/ArrayList;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/bk;->o:Ljava/util/ArrayList;

    .line 105
    iput v3, p0, Lcom/android/launcher2/bk;->u:I

    .line 106
    new-instance v0, Lcom/android/launcher2/bm;

    invoke-direct {v0, p0}, Lcom/android/launcher2/bm;-><init>(Lcom/android/launcher2/bk;)V

    iput-object v0, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    .line 112
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher2/bk;->y:[I

    .line 113
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/launcher2/bk;->z:J

    .line 114
    iput v3, p0, Lcom/android/launcher2/bk;->A:I

    .line 116
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher2/bk;->B:[I

    .line 117
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/bk;->C:Landroid/graphics/Rect;

    .line 149
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 150
    iput-object p1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    .line 151
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    .line 152
    const v0, 0x7f0b005d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/bk;->l:I

    .line 153
    invoke-static {}, Lcom/anddoes/launcher/av;->a()Lcom/anddoes/launcher/av;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    .line 154
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/android/launcher2/bk;->f:Landroid/os/Vibrator;

    .line 156
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 158
    const v2, 0x7f0a0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 157
    iput v0, p0, Lcom/android/launcher2/bk;->c:I

    .line 159
    return-void
.end method

.method private a(Lcom/android/launcher2/bt;)Landroid/graphics/PointF;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 626
    iget-object v1, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    if-nez v1, :cond_6

    .line 643
    :cond_5
    :goto_5
    return-object v0

    .line 627
    :cond_6
    invoke-interface {p1}, Lcom/android/launcher2/bt;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 629
    iget-object v1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 630
    iget-object v2, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Lcom/anddoes/launcher/av;->a(F)V

    .line 632
    iget-object v1, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    invoke-virtual {v1}, Lcom/anddoes/launcher/av;->e()F

    move-result v1

    iget v2, p0, Lcom/android/launcher2/bk;->c:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    .line 634
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    invoke-virtual {v2}, Lcom/anddoes/launcher/av;->d()F

    move-result v2

    .line 635
    iget-object v3, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    invoke-virtual {v3}, Lcom/anddoes/launcher/av;->e()F

    move-result v3

    .line 634
    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 636
    new-instance v2, Landroid/graphics/PointF;

    const/4 v3, 0x0

    const/high16 v4, -0x4080

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 637
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v4, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    iget v4, v1, Landroid/graphics/PointF;->y:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    .line 638
    invoke-virtual {v1}, Landroid/graphics/PointF;->length()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/PointF;->length()F

    move-result v2

    mul-float/2addr v2, v4

    .line 637
    div-float v2, v3, v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 639
    float-to-double v2, v2

    const-wide v4, 0x4041800000000000L

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_5

    move-object v0, v1

    .line 640
    goto :goto_5
.end method

.method static synthetic a(Lcom/android/launcher2/bk;)Lcom/android/launcher2/bs;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/launcher2/bk;->t:Lcom/android/launcher2/bs;

    return-object v0
.end method

.method private a(II[I)Lcom/android/launcher2/bx;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 693
    iget-object v2, p0, Lcom/android/launcher2/bk;->g:Landroid/graphics/Rect;

    .line 695
    iget-object v3, p0, Lcom/android/launcher2/bk;->n:Ljava/util/ArrayList;

    .line 696
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 697
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_d
    if-gez v1, :cond_11

    .line 724
    const/4 v0, 0x0

    :goto_10
    return-object v0

    .line 698
    :cond_11
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/bx;

    .line 699
    invoke-interface {v0}, Lcom/android/launcher2/bx;->c()Z

    move-result v4

    if-eqz v4, :cond_51

    .line 700
    invoke-interface {v0, v2}, Lcom/android/launcher2/bx;->getHitRect(Landroid/graphics/Rect;)V

    .line 705
    invoke-interface {v0, p3}, Lcom/android/launcher2/bx;->a([I)V

    .line 706
    aget v4, p3, v7

    invoke-interface {v0}, Lcom/android/launcher2/bx;->getLeft()I

    move-result v5

    sub-int/2addr v4, v5

    aget v5, p3, v8

    invoke-interface {v0}, Lcom/android/launcher2/bx;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 708
    iget-object v4, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput p1, v4, Lcom/android/launcher2/bz;->a:I

    .line 709
    iget-object v4, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput p2, v4, Lcom/android/launcher2/bz;->b:I

    .line 710
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_51

    .line 711
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    .line 712
    aget v1, p3, v7

    sub-int v1, p1, v1

    aput v1, p3, v7

    .line 719
    aget v1, p3, v8

    sub-int v1, p2, v1

    aput v1, p3, v8

    goto :goto_10

    .line 697
    :cond_51
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_d
.end method

.method private a(II)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const-wide/high16 v9, 0x4000

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 489
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v0, v0, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v0, p1, p2}, Lcom/android/launcher2/bu;->b(II)V

    .line 492
    iget-object v0, p0, Lcom/android/launcher2/bk;->h:[I

    .line 493
    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/bk;->a(II[I)Lcom/android/launcher2/bx;

    move-result-object v1

    .line 494
    iget-object v2, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    aget v3, v0, v8

    iput v3, v2, Lcom/android/launcher2/bz;->a:I

    .line 495
    iget-object v2, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    aget v0, v0, v7

    iput v0, v2, Lcom/android/launcher2/bz;->b:I

    .line 496
    if-eqz v1, :cond_9d

    .line 497
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    .line 498
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    if-eq v0, v1, :cond_35

    .line 503
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    if-eqz v0, :cond_30

    .line 504
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    iget-object v2, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v2}, Lcom/android/launcher2/bx;->d(Lcom/android/launcher2/bz;)V

    .line 506
    :cond_30
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v1, v0}, Lcom/android/launcher2/bx;->b(Lcom/android/launcher2/bz;)V

    .line 508
    :cond_35
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v1, v0}, Lcom/android/launcher2/bx;->c(Lcom/android/launcher2/bz;)V

    .line 514
    :cond_3a
    :goto_3a
    iput-object v1, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    .line 518
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    move-result v0

    .line 519
    iget v1, p0, Lcom/android/launcher2/bk;->A:I

    int-to-double v1, v1

    .line 520
    iget-object v3, p0, Lcom/android/launcher2/bk;->y:[I

    aget v3, v3, v8

    sub-int/2addr v3, p1

    int-to-double v3, v3

    invoke-static {v3, v4, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    iget-object v5, p0, Lcom/android/launcher2/bk;->y:[I

    aget v5, v5, v7

    sub-int/2addr v5, p2

    int-to-double v5, v5

    invoke-static {v5, v6, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    add-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, p0, Lcom/android/launcher2/bk;->A:I

    .line 521
    iget-object v1, p0, Lcom/android/launcher2/bk;->y:[I

    aput p1, v1, v8

    .line 522
    iget-object v1, p0, Lcom/android/launcher2/bk;->y:[I

    aput p2, v1, v7

    .line 523
    iget v1, p0, Lcom/android/launcher2/bk;->A:I

    if-ge v1, v0, :cond_a9

    const/16 v0, 0x2ee

    .line 525
    :goto_74
    iget v1, p0, Lcom/android/launcher2/bk;->l:I

    if-ge p1, v1, :cond_ac

    .line 526
    iget v1, p0, Lcom/android/launcher2/bk;->u:I

    if-nez v1, :cond_9c

    .line 527
    iput v7, p0, Lcom/android/launcher2/bk;->u:I

    .line 528
    iget-object v1, p0, Lcom/android/launcher2/bk;->t:Lcom/android/launcher2/bs;

    invoke-interface {v1, p1, p2, v8}, Lcom/android/launcher2/bs;->a(III)Z

    move-result v1

    if-eqz v1, :cond_9c

    .line 529
    iget-object v1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/DragLayer;->c()V

    .line 530
    iget-object v1, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    invoke-virtual {v1, v8}, Lcom/android/launcher2/bm;->a(I)V

    .line 531
    iget-object v1, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 546
    :cond_9c
    :goto_9c
    return-void

    .line 510
    :cond_9d
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    if-eqz v0, :cond_3a

    .line 511
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    iget-object v2, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v2}, Lcom/android/launcher2/bx;->d(Lcom/android/launcher2/bz;)V

    goto :goto_3a

    .line 523
    :cond_a9
    const/16 v0, 0x1f4

    goto :goto_74

    .line 534
    :cond_ac
    iget-object v1, p0, Lcom/android/launcher2/bk;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/bk;->l:I

    sub-int/2addr v1, v2

    if-le p1, v1, :cond_dc

    .line 535
    iget v1, p0, Lcom/android/launcher2/bk;->u:I

    if-nez v1, :cond_9c

    .line 536
    iput v7, p0, Lcom/android/launcher2/bk;->u:I

    .line 537
    iget-object v1, p0, Lcom/android/launcher2/bk;->t:Lcom/android/launcher2/bs;

    invoke-interface {v1, p1, p2, v7}, Lcom/android/launcher2/bs;->a(III)Z

    move-result v1

    if-eqz v1, :cond_9c

    .line 538
    iget-object v1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/DragLayer;->c()V

    .line 539
    iget-object v1, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    invoke-virtual {v1, v7}, Lcom/android/launcher2/bm;->a(I)V

    .line 540
    iget-object v1, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_9c

    .line 544
    :cond_dc
    invoke-direct {p0}, Lcom/android/launcher2/bk;->i()V

    goto :goto_9c
.end method

.method private a(Landroid/graphics/PointF;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 647
    iget-object v1, p0, Lcom/android/launcher2/bk;->h:[I

    .line 649
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    aget v4, v1, v0

    iput v4, v3, Lcom/android/launcher2/bz;->a:I

    .line 650
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    aget v1, v1, v2

    iput v1, v3, Lcom/android/launcher2/bz;->b:I

    .line 654
    iget-object v1, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    if-eqz v1, :cond_21

    iget-object v1, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    iget-object v3, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    if-eq v1, v3, :cond_21

    .line 655
    iget-object v1, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v1, v3}, Lcom/android/launcher2/bx;->d(Lcom/android/launcher2/bz;)V

    .line 660
    :cond_21
    iget-object v1, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v1, v3}, Lcom/android/launcher2/bx;->b(Lcom/android/launcher2/bz;)V

    .line 663
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-boolean v2, v1, Lcom/android/launcher2/bz;->e:Z

    .line 664
    iget-object v1, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v1, v3}, Lcom/android/launcher2/bx;->d(Lcom/android/launcher2/bz;)V

    .line 665
    iget-object v1, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v1, v3}, Lcom/android/launcher2/bx;->e(Lcom/android/launcher2/bz;)Z

    move-result v1

    if-eqz v1, :cond_5b

    .line 666
    iget-object v0, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget v3, v3, Lcom/android/launcher2/bz;->a:I

    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget v3, v3, Lcom/android/launcher2/bz;->b:I

    invoke-interface {v0, v1, p1}, Lcom/android/launcher2/bx;->a(Lcom/android/launcher2/bz;Landroid/graphics/PointF;)V

    move v1, v2

    .line 670
    :goto_4d
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v3, v0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    iget-object v0, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    check-cast v0, Landroid/view/View;

    iget-object v4, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v3, v0, v4, v2, v1}, Lcom/android/launcher2/bt;->a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V

    .line 672
    return-void

    :cond_5b
    move v1, v0

    goto :goto_4d
.end method

.method static a(Lcom/android/launcher2/bz;)V
    .registers 2
    .parameter

    .prologue
    .line 390
    iget-object v0, p0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    invoke-interface {v0}, Lcom/android/launcher2/bt;->e()V

    .line 391
    return-void
.end method

.method private a(FF)[I
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 397
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/bk;->C:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/DragLayer;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 398
    iget-object v0, p0, Lcom/android/launcher2/bk;->B:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/launcher2/bk;->C:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/launcher2/bk;->C:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 399
    iget-object v0, p0, Lcom/android/launcher2/bk;->B:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/launcher2/bk;->C:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/android/launcher2/bk;->C:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {p2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    .line 400
    iget-object v0, p0, Lcom/android/launcher2/bk;->B:[I

    return-object v0
.end method

.method private b(FF)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 675
    iget-object v3, p0, Lcom/android/launcher2/bk;->h:[I

    .line 676
    float-to-int v0, p1

    float-to-int v4, p2

    invoke-direct {p0, v0, v4, v3}, Lcom/android/launcher2/bk;->a(II[I)Lcom/android/launcher2/bx;

    move-result-object v0

    .line 678
    iget-object v4, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    aget v5, v3, v2

    iput v5, v4, Lcom/android/launcher2/bz;->a:I

    .line 679
    iget-object v4, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    aget v3, v3, v1

    iput v3, v4, Lcom/android/launcher2/bz;->b:I

    .line 681
    if-eqz v0, :cond_3a

    .line 682
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-boolean v1, v3, Lcom/android/launcher2/bz;->e:Z

    .line 683
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v3}, Lcom/android/launcher2/bx;->d(Lcom/android/launcher2/bz;)V

    .line 684
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v3}, Lcom/android/launcher2/bx;->e(Lcom/android/launcher2/bz;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 685
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v3}, Lcom/android/launcher2/bx;->a(Lcom/android/launcher2/bz;)V

    .line 689
    :goto_2e
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v3, v3, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    check-cast v0, Landroid/view/View;

    iget-object v4, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v3, v0, v4, v2, v1}, Lcom/android/launcher2/bt;->a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V

    .line 690
    return-void

    :cond_3a
    move v1, v2

    goto :goto_2e
.end method

.method static synthetic b(Lcom/android/launcher2/bk;)V
    .registers 2
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bk;->u:I

    return-void
.end method

.method private c(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 771
    iget-object v0, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    if-nez v0, :cond_a

    .line 772
    invoke-static {}, Lcom/anddoes/launcher/av;->a()Lcom/anddoes/launcher/av;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    .line 774
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    invoke-virtual {v0, p1}, Lcom/anddoes/launcher/av;->a(Landroid/view/MotionEvent;)V

    .line 775
    return-void
.end method

.method static synthetic c(Lcom/android/launcher2/bk;)V
    .registers 2
    .parameter

    .prologue
    .line 114
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bk;->A:I

    return-void
.end method

.method static synthetic d(Lcom/android/launcher2/bk;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    return-object v0
.end method

.method private h()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 355
    iget-boolean v1, p0, Lcom/android/launcher2/bk;->i:Z

    if-eqz v1, :cond_30

    .line 356
    iput-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    .line 357
    invoke-direct {p0}, Lcom/android/launcher2/bk;->i()V

    .line 359
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v1, v1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    if-eqz v1, :cond_22

    .line 360
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-boolean v0, v0, Lcom/android/launcher2/bz;->k:Z

    .line 361
    if-nez v0, :cond_1e

    .line 362
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v1, v1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v1}, Lcom/android/launcher2/bu;->e()V

    .line 364
    :cond_1e
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-object v2, v1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    .line 367
    :cond_22
    if-nez v0, :cond_30

    .line 368
    iget-object v0, p0, Lcom/android/launcher2/bk;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3c

    .line 374
    :cond_30
    iget-object v0, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    invoke-virtual {v0}, Lcom/anddoes/launcher/av;->b()V

    iput-object v2, p0, Lcom/android/launcher2/bk;->D:Lcom/anddoes/launcher/av;

    .line 375
    :cond_3b
    return-void

    .line 368
    :cond_3c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/bl;

    .line 369
    invoke-interface {v0}, Lcom/android/launcher2/bl;->a()V

    goto :goto_2a
.end method

.method private i()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 479
    iget-object v0, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 480
    iget v0, p0, Lcom/android/launcher2/bk;->u:I

    if-ne v0, v2, :cond_22

    .line 481
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bk;->u:I

    .line 482
    iget-object v0, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/bm;->a(I)V

    .line 483
    iget-object v0, p0, Lcom/android/launcher2/bk;->t:Lcom/android/launcher2/bs;

    invoke-interface {v0}, Lcom/android/launcher2/bs;->h()Z

    .line 484
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/DragLayer;->d()V

    .line 486
    :cond_22
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;IILcom/android/launcher2/bt;Ljava/lang/Object;Landroid/graphics/Point;Landroid/graphics/Rect;F)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 216
    iget-object v1, p0, Lcom/android/launcher2/bk;->x:Landroid/view/inputmethod/InputMethodManager;

    if-nez v1, :cond_10

    .line 218
    iget-object v1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 217
    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/android/launcher2/bk;->x:Landroid/view/inputmethod/InputMethodManager;

    .line 220
    :cond_10
    iget-object v1, p0, Lcom/android/launcher2/bk;->x:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/android/launcher2/bk;->q:Landroid/os/IBinder;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 222
    iget-object v1, p0, Lcom/android/launcher2/bk;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_a8

    .line 226
    iget v1, p0, Lcom/android/launcher2/bk;->j:I

    sub-int v4, v1, p2

    .line 227
    iget v1, p0, Lcom/android/launcher2/bk;->k:I

    sub-int v5, v1, p3

    .line 229
    if-nez p7, :cond_b3

    const/4 v1, 0x0

    move v2, v1

    .line 230
    :goto_30
    if-nez p7, :cond_ba

    const/4 v1, 0x0

    .line 232
    :goto_33
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/launcher2/bk;->i:Z

    .line 234
    new-instance v3, Lcom/android/launcher2/bz;

    invoke-direct {v3}, Lcom/android/launcher2/bz;-><init>()V

    iput-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    .line 236
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    const/4 v6, 0x0

    iput-boolean v6, v3, Lcom/android/launcher2/bz;->e:Z

    .line 237
    iget-object v3, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget v6, p0, Lcom/android/launcher2/bk;->j:I

    add-int/2addr v2, p2

    sub-int v2, v6, v2

    iput v2, v3, Lcom/android/launcher2/bz;->c:I

    .line 238
    iget-object v2, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget v3, p0, Lcom/android/launcher2/bk;->k:I

    add-int/2addr v1, p3

    sub-int v1, v3, v1

    iput v1, v2, Lcom/android/launcher2/bz;->d:I

    .line 239
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-object p4, v1, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    .line 240
    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-object p5, v1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    .line 242
    iget-object v1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->ay:Z

    if-eqz v1, :cond_6b

    .line 243
    iget-object v1, p0, Lcom/android/launcher2/bk;->f:Landroid/os/Vibrator;

    const-wide/16 v2, 0xf

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 246
    :cond_6b
    iget-object v9, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    new-instance v1, Lcom/android/launcher2/bu;

    iget-object v2, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    .line 247
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    move-object v3, p1

    move/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/android/launcher2/bu;-><init>(Lcom/android/launcher2/Launcher;Landroid/graphics/Bitmap;IIIIF)V

    .line 246
    iput-object v1, v9, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    .line 249
    if-eqz p6, :cond_8d

    .line 250
    new-instance v2, Landroid/graphics/Point;

    move-object/from16 v0, p6

    invoke-direct {v2, v0}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v1, v2}, Lcom/android/launcher2/bu;->setDragVisualizeOffset(Landroid/graphics/Point;)V

    .line 252
    :cond_8d
    if-eqz p7, :cond_99

    .line 253
    new-instance v2, Landroid/graphics/Rect;

    move-object/from16 v0, p7

    invoke-direct {v2, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v2}, Lcom/android/launcher2/bu;->setDragRegion(Landroid/graphics/Rect;)V

    .line 256
    :cond_99
    iget v2, p0, Lcom/android/launcher2/bk;->j:I

    iget v3, p0, Lcom/android/launcher2/bk;->k:I

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher2/bu;->a(II)V

    .line 257
    iget v1, p0, Lcom/android/launcher2/bk;->j:I

    iget v2, p0, Lcom/android/launcher2/bk;->k:I

    invoke-direct {p0, v1, v2}, Lcom/android/launcher2/bk;->a(II)V

    .line 258
    return-void

    .line 222
    :cond_a8
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/bl;

    .line 223
    invoke-interface {v1, p4, p5}, Lcom/android/launcher2/bl;->a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V

    goto/16 :goto_1e

    .line 229
    :cond_b3
    move-object/from16 v0, p7

    iget v1, v0, Landroid/graphics/Rect;->left:I

    move v2, v1

    goto/16 :goto_30

    .line 230
    :cond_ba
    move-object/from16 v0, p7

    iget v1, v0, Landroid/graphics/Rect;->top:I

    goto/16 :goto_33
.end method

.method public final a(Landroid/os/IBinder;)V
    .registers 2
    .parameter

    .prologue
    .line 732
    iput-object p1, p0, Lcom/android/launcher2/bk;->q:Landroid/os/IBinder;

    .line 733
    return-void
.end method

.method final a(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 471
    iput-object p1, p0, Lcom/android/launcher2/bk;->s:Landroid/view/View;

    .line 472
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Bitmap;Lcom/android/launcher2/bt;Ljava/lang/Object;IF)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/high16 v4, 0x4000

    .line 179
    iget-object v0, p0, Lcom/android/launcher2/bk;->h:[I

    .line 180
    iget-object v1, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 181
    const/4 v1, 0x0

    aget v1, v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    .line 182
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p6

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v4

    float-to-int v2, v2

    .line 181
    add-int/2addr v2, v1

    .line 183
    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p6

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v1, v3

    div-float/2addr v1, v4

    float-to-int v1, v1

    .line 183
    add-int v3, v0, v1

    move-object v0, p0

    move-object v1, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, v6

    move v8, p6

    .line 186
    invoke-virtual/range {v0 .. v8}, Lcom/android/launcher2/bk;->a(Landroid/graphics/Bitmap;IILcom/android/launcher2/bt;Ljava/lang/Object;Landroid/graphics/Point;Landroid/graphics/Rect;F)V

    .line 189
    sget v0, Lcom/android/launcher2/bk;->a:I

    if-ne p5, v0, :cond_4f

    .line 190
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 192
    :cond_4f
    return-void
.end method

.method public final a(Lcom/android/launcher2/bl;)V
    .registers 3
    .parameter

    .prologue
    .line 739
    iget-object v0, p0, Lcom/android/launcher2/bk;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 740
    return-void
.end method

.method public final a(Lcom/android/launcher2/bs;)V
    .registers 2
    .parameter

    .prologue
    .line 728
    iput-object p1, p0, Lcom/android/launcher2/bk;->t:Lcom/android/launcher2/bs;

    .line 729
    return-void
.end method

.method final a(Lcom/android/launcher2/bu;)V
    .registers 4
    .parameter

    .prologue
    .line 381
    invoke-virtual {p1}, Lcom/android/launcher2/bu;->e()V

    .line 384
    iget-object v0, p0, Lcom/android/launcher2/bk;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 387
    return-void

    .line 384
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/bl;

    .line 385
    invoke-interface {v0}, Lcom/android/launcher2/bl;->a()V

    goto :goto_9
.end method

.method public final a(Lcom/android/launcher2/bx;)V
    .registers 3
    .parameter

    .prologue
    .line 753
    iget-object v0, p0, Lcom/android/launcher2/bk;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .registers 6
    .parameter

    .prologue
    .line 333
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    if-eqz v0, :cond_18

    .line 334
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v0, v0, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    .line 335
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_18

    .line 336
    check-cast v0, Lcom/android/launcher2/jd;

    .line 337
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_19

    .line 352
    :cond_18
    :goto_18
    return-void

    .line 337
    :cond_19
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/h;

    .line 339
    if-eqz v0, :cond_12

    .line 340
    iget-object v3, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    if-eqz v3, :cond_12

    .line 341
    iget-object v3, v1, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    if-eqz v3, :cond_12

    .line 342
    iget-object v3, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-static {v3}, Lcom/android/launcher2/di;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 343
    iget-object v1, v1, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-static {v1}, Lcom/android/launcher2/di;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 342
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 344
    if-eqz v1, :cond_12

    .line 345
    invoke-virtual {p0}, Lcom/android/launcher2/bk;->c()V

    goto :goto_18
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    return v0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    .line 420
    invoke-direct {p0, p1}, Lcom/android/launcher2/bk;->c(Landroid/view/MotionEvent;)V

    .line 429
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 430
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 431
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 432
    invoke-direct {p0, v1, v2}, Lcom/android/launcher2/bk;->a(FF)[I

    move-result-object v3

    .line 433
    const/4 v4, 0x0

    aget v4, v3, v4

    .line 434
    const/4 v5, 0x1

    aget v3, v3, v5

    .line 436
    packed-switch v0, :pswitch_data_50

    .line 464
    :goto_1c
    :pswitch_1c
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    return v0

    .line 441
    :pswitch_1f
    iput v1, p0, Lcom/android/launcher2/bk;->E:F

    .line 442
    iput v2, p0, Lcom/android/launcher2/bk;->F:F

    .line 443
    iput v4, p0, Lcom/android/launcher2/bk;->j:I

    .line 444
    iput v3, p0, Lcom/android/launcher2/bk;->k:I

    .line 445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    goto :goto_1c

    .line 448
    :pswitch_2b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/launcher2/bk;->z:J

    .line 449
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    if-eqz v0, :cond_42

    .line 450
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v0, v0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    invoke-direct {p0, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bt;)Landroid/graphics/PointF;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_46

    .line 452
    invoke-direct {p0, v0}, Lcom/android/launcher2/bk;->a(Landroid/graphics/PointF;)V

    .line 457
    :cond_42
    :goto_42
    invoke-direct {p0}, Lcom/android/launcher2/bk;->h()V

    goto :goto_1c

    .line 454
    :cond_46
    int-to-float v0, v4

    int-to-float v1, v3

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/bk;->b(FF)V

    goto :goto_42

    .line 460
    :pswitch_4c
    invoke-virtual {p0}, Lcom/android/launcher2/bk;->c()V

    goto :goto_1c

    .line 436
    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_1f
        :pswitch_2b
        :pswitch_1c
        :pswitch_4c
    .end packed-switch
.end method

.method public final a(Landroid/view/View;I)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 475
    iget-object v0, p0, Lcom/android/launcher2/bk;->s:Landroid/view/View;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/android/launcher2/bk;->s:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final b(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 788
    iput-object p1, p0, Lcom/android/launcher2/bk;->r:Landroid/view/View;

    .line 789
    return-void
.end method

.method public final b(Lcom/android/launcher2/bx;)V
    .registers 3
    .parameter

    .prologue
    .line 760
    iget-object v0, p0, Lcom/android/launcher2/bk;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 761
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    return v0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 558
    iget-boolean v2, p0, Lcom/android/launcher2/bk;->i:Z

    if-nez v2, :cond_7

    .line 617
    :goto_6
    return v0

    .line 563
    :cond_7
    invoke-direct {p0, p1}, Lcom/android/launcher2/bk;->c(Landroid/view/MotionEvent;)V

    .line 565
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 566
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 567
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 568
    invoke-direct {p0, v3, v4}, Lcom/android/launcher2/bk;->a(FF)[I

    move-result-object v5

    .line 569
    aget v6, v5, v0

    .line 570
    aget v5, v5, v1

    .line 572
    packed-switch v2, :pswitch_data_ac

    :goto_21
    move v0, v1

    .line 617
    goto :goto_6

    .line 575
    :pswitch_23
    iput v3, p0, Lcom/android/launcher2/bk;->E:F

    .line 576
    iput v4, p0, Lcom/android/launcher2/bk;->F:F

    .line 577
    iput v6, p0, Lcom/android/launcher2/bk;->j:I

    .line 578
    iput v5, p0, Lcom/android/launcher2/bk;->k:I

    .line 580
    iget v2, p0, Lcom/android/launcher2/bk;->l:I

    if-lt v6, v2, :cond_3a

    iget-object v2, p0, Lcom/android/launcher2/bk;->r:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/android/launcher2/bk;->l:I

    sub-int/2addr v2, v3

    if-le v6, v2, :cond_46

    .line 581
    :cond_3a
    iput v1, p0, Lcom/android/launcher2/bk;->u:I

    .line 582
    iget-object v0, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_21

    .line 584
    :cond_46
    iput v0, p0, Lcom/android/launcher2/bk;->u:I

    goto :goto_21

    .line 588
    :pswitch_49
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    if-eqz v0, :cond_77

    .line 589
    iget v0, p0, Lcom/android/launcher2/bk;->E:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/android/launcher2/bk;->l:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_6b

    iget v0, p0, Lcom/android/launcher2/bk;->F:F

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/android/launcher2/bk;->l:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_77

    .line 590
    :cond_6b
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/am;->b()V

    .line 591
    iget-object v0, p0, Lcom/android/launcher2/bk;->d:Lcom/android/launcher2/Launcher;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/android/launcher2/Launcher;->j:Lcom/anddoes/launcher/ui/am;

    .line 594
    :cond_77
    invoke-direct {p0, v6, v5}, Lcom/android/launcher2/bk;->a(II)V

    goto :goto_21

    .line 598
    :pswitch_7b
    invoke-direct {p0, v6, v5}, Lcom/android/launcher2/bk;->a(II)V

    .line 599
    iget-object v0, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 601
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    if-eqz v0, :cond_96

    .line 602
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v0, v0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    invoke-direct {p0, v0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bt;)Landroid/graphics/PointF;

    move-result-object v0

    .line 603
    if-eqz v0, :cond_9a

    .line 604
    invoke-direct {p0, v0}, Lcom/android/launcher2/bk;->a(Landroid/graphics/PointF;)V

    .line 609
    :cond_96
    :goto_96
    invoke-direct {p0}, Lcom/android/launcher2/bk;->h()V

    goto :goto_21

    .line 606
    :cond_9a
    int-to-float v0, v6

    int-to-float v2, v5

    invoke-direct {p0, v0, v2}, Lcom/android/launcher2/bk;->b(FF)V

    goto :goto_96

    .line 612
    :pswitch_a0
    iget-object v0, p0, Lcom/android/launcher2/bk;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/launcher2/bk;->v:Lcom/android/launcher2/bm;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 613
    invoke-virtual {p0}, Lcom/android/launcher2/bk;->c()V

    goto/16 :goto_21

    .line 572
    :pswitch_data_ac
    .packed-switch 0x0
        :pswitch_23
        :pswitch_7b
        :pswitch_49
        :pswitch_a0
    .end packed-switch
.end method

.method public final c()V
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 320
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    if-eqz v0, :cond_27

    .line 321
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    if-eqz v0, :cond_11

    .line 322
    iget-object v0, p0, Lcom/android/launcher2/bk;->w:Lcom/android/launcher2/bx;

    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v1}, Lcom/android/launcher2/bx;->d(Lcom/android/launcher2/bz;)V

    .line 324
    :cond_11
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-boolean v3, v0, Lcom/android/launcher2/bz;->k:Z

    .line 325
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-boolean v2, v0, Lcom/android/launcher2/bz;->j:Z

    .line 326
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iput-boolean v2, v0, Lcom/android/launcher2/bz;->e:Z

    .line 327
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget-object v0, v0, Lcom/android/launcher2/bz;->h:Lcom/android/launcher2/bt;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    invoke-interface {v0, v1, v2, v3, v3}, Lcom/android/launcher2/bt;->a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V

    .line 329
    :cond_27
    invoke-direct {p0}, Lcom/android/launcher2/bk;->h()V

    .line 330
    return-void
.end method

.method public final c(Lcom/android/launcher2/bx;)V
    .registers 2
    .parameter

    .prologue
    .line 767
    iput-object p1, p0, Lcom/android/launcher2/bk;->p:Lcom/android/launcher2/bx;

    .line 768
    return-void
.end method

.method final d()J
    .registers 3

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    if-eqz v0, :cond_9

    .line 405
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 407
    :goto_8
    return-wide v0

    :cond_9
    iget-wide v0, p0, Lcom/android/launcher2/bk;->z:J

    goto :goto_8
.end method

.method final e()V
    .registers 3

    .prologue
    .line 412
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/launcher2/bk;->z:J

    .line 413
    return-void
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/android/launcher2/bk;->i:Z

    if-eqz v0, :cond_f

    .line 550
    iget-object v0, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget v0, v0, Lcom/android/launcher2/bz;->a:I

    iget-object v1, p0, Lcom/android/launcher2/bk;->m:Lcom/android/launcher2/bz;

    iget v1, v1, Lcom/android/launcher2/bz;->b:I

    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/bk;->a(II)V

    .line 552
    :cond_f
    return-void
.end method

.method public final g()I
    .registers 2

    .prologue
    .line 830
    iget v0, p0, Lcom/android/launcher2/bk;->k:I

    return v0
.end method
