.class final Lcom/android/launcher2/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/bu;

.field private final synthetic b:F

.field private final synthetic c:F

.field private final synthetic d:F

.field private final synthetic e:F


# direct methods
.method constructor <init>(Lcom/android/launcher2/bu;FFFF)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    iput p2, p0, Lcom/android/launcher2/bv;->b:F

    iput p3, p0, Lcom/android/launcher2/bv;->c:F

    iput p4, p0, Lcom/android/launcher2/bv;->d:F

    iput p5, p0, Lcom/android/launcher2/bv;->e:F

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 10
    .parameter

    .prologue
    const/high16 v7, 0x3f80

    .line 83
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 85
    iget v1, p0, Lcom/android/launcher2/bv;->b:F

    mul-float/2addr v1, v0

    iget-object v2, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-static {v2}, Lcom/android/launcher2/bu;->a(Lcom/android/launcher2/bu;)F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 86
    iget v2, p0, Lcom/android/launcher2/bv;->c:F

    mul-float/2addr v2, v0

    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-static {v3}, Lcom/android/launcher2/bu;->b(Lcom/android/launcher2/bu;)F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 88
    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-static {v3}, Lcom/android/launcher2/bu;->a(Lcom/android/launcher2/bu;)F

    move-result v4

    int-to-float v5, v1

    add-float/2addr v4, v5

    invoke-static {v3, v4}, Lcom/android/launcher2/bu;->a(Lcom/android/launcher2/bu;F)V

    .line 89
    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-static {v3}, Lcom/android/launcher2/bu;->b(Lcom/android/launcher2/bu;)F

    move-result v4

    int-to-float v5, v2

    add-float/2addr v4, v5

    invoke-static {v3, v4}, Lcom/android/launcher2/bu;->b(Lcom/android/launcher2/bu;F)V

    .line 90
    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    iget v4, p0, Lcom/android/launcher2/bv;->d:F

    iget v5, p0, Lcom/android/launcher2/bv;->e:F

    iget v6, p0, Lcom/android/launcher2/bv;->d:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/android/launcher2/bu;->setScaleX(F)V

    .line 91
    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    iget v4, p0, Lcom/android/launcher2/bv;->d:F

    iget v5, p0, Lcom/android/launcher2/bv;->e:F

    iget v6, p0, Lcom/android/launcher2/bv;->d:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/android/launcher2/bu;->setScaleY(F)V

    .line 92
    invoke-static {}, Lcom/android/launcher2/bu;->f()F

    move-result v3

    cmpl-float v3, v3, v7

    if-eqz v3, :cond_69

    .line 93
    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-static {}, Lcom/android/launcher2/bu;->f()F

    move-result v4

    mul-float/2addr v4, v0

    sub-float v0, v7, v0

    add-float/2addr v0, v4

    invoke-virtual {v3, v0}, Lcom/android/launcher2/bu;->setAlpha(F)V

    .line 96
    :cond_69
    iget-object v0, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_75

    .line 97
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 102
    :goto_74
    return-void

    .line 99
    :cond_75
    iget-object v0, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    iget-object v3, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-virtual {v3}, Lcom/android/launcher2/bu;->getTranslationX()F

    move-result v3

    int-to-float v1, v1

    add-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setTranslationX(F)V

    .line 100
    iget-object v0, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    iget-object v1, p0, Lcom/android/launcher2/bv;->a:Lcom/android/launcher2/bu;

    invoke-virtual {v1}, Lcom/android/launcher2/bu;->getTranslationY()F

    move-result v1

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bu;->setTranslationY(F)V

    goto :goto_74
.end method
