.class public final Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/s;


# static fields
.field public static final MUSIC_VIDEO_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah;

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

.field private static final serialVersionUID:J


# instance fields
.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private musicVideo_:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 5208
    new-instance v0, Lcom/google/android/youtube/core/model/proto/q;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/q;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    .line 5591
    new-instance v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;-><init>(Z)V

    .line 5592
    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    .line 5593
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 5168
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5262
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedIsInitialized:B

    .line 5285
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedSerializedSize:I

    .line 5169
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    move v1, v0

    .line 5173
    :cond_11
    :goto_11
    if-nez v1, :cond_57

    .line 5174
    :try_start_13
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v3

    .line 5175
    sparse-switch v3, :sswitch_data_7e

    .line 5180
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 5182
    goto :goto_11

    :sswitch_22
    move v1, v2

    .line 5178
    goto :goto_11

    .line 5187
    :sswitch_24
    and-int/lit8 v3, v0, 0x1

    if-eq v3, v2, :cond_31

    .line 5188
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    .line 5189
    or-int/lit8 v0, v0, 0x1

    .line 5191
    :cond_31
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    sget-object v4, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-virtual {p1, v4, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/ah;Lcom/google/protobuf/i;)Lcom/google/protobuf/ae;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3c
    .catchall {:try_start_13 .. :try_end_3c} :catchall_79
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_13 .. :try_end_3c} :catch_3d
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_3c} :catch_67

    goto :goto_11

    .line 5196
    :catch_3d
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 5197
    :try_start_41
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_46
    .catchall {:try_start_41 .. :try_end_46} :catchall_46

    .line 5202
    :catchall_46
    move-exception v0

    :goto_47
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_53

    .line 5203
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    .line 5205
    :cond_53
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->makeExtensionsImmutable()V

    throw v0

    .line 5202
    :cond_57
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_63

    .line 5203
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    .line 5205
    :cond_63
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->makeExtensionsImmutable()V

    .line 5206
    return-void

    .line 5198
    :catch_67
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 5199
    :try_start_6b
    new-instance v3, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_79
    .catchall {:try_start_6b .. :try_end_79} :catchall_46

    .line 5202
    :catchall_79
    move-exception v1

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_47

    .line 5175
    :sswitch_data_7e
    .sparse-switch
        0x0 -> :sswitch_22
        0xa -> :sswitch_24
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5146
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 5151
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 5262
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedIsInitialized:B

    .line 5285
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedSerializedSize:I

    .line 5153
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5146
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 5154
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 5262
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedIsInitialized:B

    .line 5285
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedSerializedSize:I

    .line 5154
    return-void
.end method

.method static synthetic access$5000(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 5146
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 5146
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 1

    .prologue
    .line 5158
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 5260
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    .line 5261
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/r;
    .registers 1

    .prologue
    .line 5359
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/r;->a()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;
    .registers 2
    .parameter

    .prologue
    .line 5362
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->newBuilder()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/r;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 2
    .parameter

    .prologue
    .line 5339
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5345
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 2
    .parameter

    .prologue
    .line 5309
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5315
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 2
    .parameter

    .prologue
    .line 5350
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5356
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 2
    .parameter

    .prologue
    .line 5329
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5335
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 2
    .parameter

    .prologue
    .line 5319
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5325
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method


# virtual methods
.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;
    .registers 2

    .prologue
    .line 5162
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 5146
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getMusicVideo(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 3
    .parameter

    .prologue
    .line 5249
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    return-object v0
.end method

.method public final getMusicVideoCount()I
    .registers 2

    .prologue
    .line 5243
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getMusicVideoList()Ljava/util/List;
    .registers 2

    .prologue
    .line 5230
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    return-object v0
.end method

.method public final getMusicVideoOrBuilder(I)Lcom/google/android/youtube/core/model/proto/v;
    .registers 3
    .parameter

    .prologue
    .line 5256
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/v;

    return-object v0
.end method

.method public final getMusicVideoOrBuilderList()Ljava/util/List;
    .registers 2

    .prologue
    .line 5237
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    return-object v0
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 5220
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 5287
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedSerializedSize:I

    .line 5288
    const/4 v1, -0x1

    if-eq v2, v1, :cond_7

    .line 5296
    :goto_6
    return v2

    :cond_7
    move v1, v0

    move v2, v0

    .line 5291
    :goto_9
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_23

    .line 5292
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v2, v0

    .line 5291
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 5295
    :cond_23
    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedSerializedSize:I

    goto :goto_6
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5264
    iget-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedIsInitialized:B

    .line 5265
    const/4 v3, -0x1

    if-eq v0, v3, :cond_b

    if-ne v0, v2, :cond_a

    move v1, v2

    .line 5274
    :cond_a
    :goto_a
    return v1

    :cond_b
    move v0, v1

    .line 5267
    :goto_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getMusicVideoCount()I

    move-result v3

    if-ge v0, v3, :cond_22

    .line 5268
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getMusicVideo(I)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1f

    .line 5269
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedIsInitialized:B

    goto :goto_a

    .line 5267
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 5273
    :cond_22
    iput-byte v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->memoizedIsInitialized:B

    move v1, v2

    .line 5274
    goto :goto_a
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/r;
    .registers 2

    .prologue
    .line 5360
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->newBuilder()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 5146
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/r;
    .registers 2

    .prologue
    .line 5364
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;)Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 5146
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->toBuilder()Lcom/google/android/youtube/core/model/proto/r;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 5303
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 5
    .parameter

    .prologue
    .line 5279
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->getSerializedSize()I

    .line 5280
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1d

    .line 5281
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataResponse;->musicVideo_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    .line 5280
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 5283
    :cond_1d
    return-void
.end method
