.class final Lcom/google/android/youtube/core/player/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/aa;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/LightboxActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/LightboxActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/youtube/core/player/af;->a:Lcom/google/android/youtube/core/player/LightboxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/LightboxActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/af;-><init>(Lcom/google/android/youtube/core/player/LightboxActivity;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 2
    .parameter

    .prologue
    .line 146
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 2
    .parameter

    .prologue
    .line 140
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 2
    .parameter

    .prologue
    .line 135
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/Director$StopReason;)V
    .registers 5
    .parameter

    .prologue
    .line 130
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->AUTOPLAY_DENIED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_d

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/core/player/af;->a:Lcom/google/android/youtube/core/player/LightboxActivity;

    const v1, 0x7f0b0068

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    .line 133
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/core/player/af;->a:Lcom/google/android/youtube/core/player/LightboxActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/LightboxActivity;->finish()V

    .line 134
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/DirectorException;)V
    .registers 2
    .parameter

    .prologue
    .line 143
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)V
    .registers 2
    .parameter

    .prologue
    .line 141
    return-void
.end method

.method public final b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 137
    return-void
.end method

.method public final j()V
    .registers 1

    .prologue
    .line 138
    return-void
.end method

.method public final k()V
    .registers 1

    .prologue
    .line 145
    return-void
.end method

.method public final l()V
    .registers 1

    .prologue
    .line 144
    return-void
.end method
