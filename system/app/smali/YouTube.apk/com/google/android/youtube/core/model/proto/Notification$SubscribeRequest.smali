.class public final Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/ad;


# static fields
.field public static final AUTH_TOKEN_FIELD_NUMBER:I = 0x3

.field public static final CHANNEL_IDS_FIELD_NUMBER:I = 0x5

.field public static final DEVICE_ID_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final REGISTRATION_ID_FIELD_NUMBER:I = 0x4

.field public static final USER_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

.field private static final serialVersionUID:J


# instance fields
.field private authToken_:Ljava/lang/Object;

.field private bitField0_:I

.field private channelIds_:Lcom/google/protobuf/ab;

.field private deviceId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private registrationId_:Ljava/lang/Object;

.field private userId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 2201
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ab;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ab;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    .line 3119
    new-instance v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;-><init>(Z)V

    .line 3120
    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->initFields()V

    .line 3121
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v5, 0x10

    .line 2141
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2426
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    .line 2471
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedSerializedSize:I

    .line 2142
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->initFields()V

    move v1, v0

    .line 2146
    :cond_10
    :goto_10
    if-nez v1, :cond_a1

    .line 2147
    :try_start_12
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v3

    .line 2148
    sparse-switch v3, :sswitch_data_b2

    .line 2153
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 2155
    goto :goto_10

    :sswitch_21
    move v1, v2

    .line 2151
    goto :goto_10

    .line 2160
    :sswitch_23
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    .line 2161
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;
    :try_end_2f
    .catchall {:try_start_12 .. :try_end_2f} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_12 .. :try_end_2f} :catch_30
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_2f} :catch_58

    goto :goto_10

    .line 2189
    :catch_30
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2190
    :try_start_34
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_39
    .catchall {:try_start_34 .. :try_end_39} :catchall_39

    .line 2195
    :catchall_39
    move-exception v0

    :goto_3a
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v5, :cond_47

    .line 2196
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v1, v2}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 2198
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->makeExtensionsImmutable()V

    throw v0

    .line 2165
    :sswitch_4b
    :try_start_4b
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    .line 2166
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;
    :try_end_57
    .catchall {:try_start_4b .. :try_end_57} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4b .. :try_end_57} :catch_30
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_57} :catch_58

    goto :goto_10

    .line 2191
    :catch_58
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 2192
    :try_start_5c
    new-instance v2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_39

    .line 2170
    :sswitch_6a
    :try_start_6a
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    .line 2171
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    goto :goto_10

    .line 2195
    :catchall_77
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_3a

    .line 2175
    :sswitch_7c
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    .line 2176
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    goto :goto_10

    .line 2180
    :sswitch_89
    and-int/lit8 v3, v0, 0x10

    if-eq v3, v5, :cond_96

    .line 2181
    new-instance v3, Lcom/google/protobuf/aa;

    invoke-direct {v3}, Lcom/google/protobuf/aa;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 2182
    or-int/lit8 v0, v0, 0x10

    .line 2184
    :cond_96
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/ab;->a(Lcom/google/protobuf/e;)V
    :try_end_9f
    .catchall {:try_start_6a .. :try_end_9f} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_6a .. :try_end_9f} :catch_30
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_9f} :catch_58

    goto/16 :goto_10

    .line 2195
    :cond_a1
    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_ae

    .line 2196
    new-instance v0, Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 2198
    :cond_ae
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->makeExtensionsImmutable()V

    .line 2199
    return-void

    .line 2148
    :sswitch_data_b2
    .sparse-switch
        0x0 -> :sswitch_21
        0xa -> :sswitch_23
        0x12 -> :sswitch_4b
        0x1a -> :sswitch_6a
        0x22 -> :sswitch_7c
        0x2a -> :sswitch_89
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2119
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2124
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 2426
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    .line 2471
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedSerializedSize:I

    .line 2126
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2119
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2127
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2426
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    .line 2471
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedSerializedSize:I

    .line 2127
    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object p1
.end method

.method static synthetic access$2502(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2119
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 1

    .prologue
    .line 2131
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 2420
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 2421
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    .line 2422
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 2423
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    .line 2424
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 2425
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/ac;
    .registers 1

    .prologue
    .line 2566
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/ac;->a()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;
    .registers 2
    .parameter

    .prologue
    .line 2569
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/ac;->a(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 2546
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2552
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 2516
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2522
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 2557
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2563
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 2536
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2542
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 2526
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2532
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method


# virtual methods
.method public final getAuthToken()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2316
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 2317
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2318
    check-cast v0, Ljava/lang/String;

    .line 2326
    :goto_8
    return-object v0

    .line 2320
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 2322
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 2323
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2324
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 2326
    goto :goto_8
.end method

.method public final getAuthTokenBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 2334
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 2335
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2336
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 2339
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 2342
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getChannelIds(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 2409
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelIdsBytes(I)Lcom/google/protobuf/e;
    .registers 3
    .parameter

    .prologue
    .line 2416
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public final getChannelIdsCount()I
    .registers 2

    .prologue
    .line 2403
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->size()I

    move-result v0

    return v0
.end method

.method public final getChannelIdsList()Ljava/util/List;
    .registers 2

    .prologue
    .line 2397
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;
    .registers 2

    .prologue
    .line 2135
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 2119
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2230
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 2231
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2232
    check-cast v0, Ljava/lang/String;

    .line 2240
    :goto_8
    return-object v0

    .line 2234
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 2236
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 2237
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2238
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 2240
    goto :goto_8
.end method

.method public final getDeviceIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 2248
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 2249
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2250
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 2253
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 2256
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 2213
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getRegistrationId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2359
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    .line 2360
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2361
    check-cast v0, Ljava/lang/String;

    .line 2369
    :goto_8
    return-object v0

    .line 2363
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 2365
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 2366
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2367
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 2369
    goto :goto_8
.end method

.method public final getRegistrationIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 2377
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    .line 2378
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2379
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 2382
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->registrationId_:Ljava/lang/Object;

    .line 2385
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getSerializedSize()I
    .registers 7

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2473
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedSerializedSize:I

    .line 2474
    const/4 v2, -0x1

    if-eq v0, v2, :cond_a

    .line 2503
    :goto_9
    return v0

    .line 2477
    :cond_a
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_70

    .line 2478
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2481
    :goto_1a
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_29

    .line 2482
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2485
    :cond_29
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_39

    .line 2486
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2489
    :cond_39
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4a

    .line 2490
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getRegistrationIdBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4a
    move v2, v1

    .line 2495
    :goto_4b
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3}, Lcom/google/protobuf/ab;->size()I

    move-result v3

    if-ge v1, v3, :cond_61

    .line 2496
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3, v1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2495
    add-int/lit8 v1, v1, 0x1

    goto :goto_4b

    .line 2499
    :cond_61
    add-int/2addr v0, v2

    .line 2500
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getChannelIdsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2502
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedSerializedSize:I

    goto :goto_9

    :cond_70
    move v0, v1

    goto :goto_1a
.end method

.method public final getUserId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 2273
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    .line 2274
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2275
    check-cast v0, Ljava/lang/String;

    .line 2283
    :goto_8
    return-object v0

    .line 2277
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 2279
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 2280
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2281
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 2283
    goto :goto_8
.end method

.method public final getUserIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 2291
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    .line 2292
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 2293
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 2296
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->userId_:Ljava/lang/Object;

    .line 2299
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final hasAuthToken()Z
    .registers 3

    .prologue
    .line 2310
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasDeviceId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 2224
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasRegistrationId()Z
    .registers 3

    .prologue
    .line 2353
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasUserId()Z
    .registers 3

    .prologue
    .line 2267
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2428
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    .line 2429
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 2448
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 2429
    goto :goto_9

    .line 2431
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasDeviceId()Z

    move-result v2

    if-nez v2, :cond_16

    .line 2432
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 2433
    goto :goto_9

    .line 2435
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasUserId()Z

    move-result v2

    if-nez v2, :cond_20

    .line 2436
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 2437
    goto :goto_9

    .line 2439
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasAuthToken()Z

    move-result v2

    if-nez v2, :cond_2a

    .line 2440
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 2441
    goto :goto_9

    .line 2443
    :cond_2a
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->hasRegistrationId()Z

    move-result v2

    if-nez v2, :cond_34

    .line 2444
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 2445
    goto :goto_9

    .line 2447
    :cond_34
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/ac;
    .registers 2

    .prologue
    .line 2567
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 2119
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/ac;
    .registers 2

    .prologue
    .line 2571
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 2119
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->toBuilder()Lcom/google/android/youtube/core/model/proto/ac;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2510
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2453
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getSerializedSize()I

    .line 2454
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_13

    .line 2455
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 2457
    :cond_13
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_20

    .line 2458
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 2460
    :cond_20
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2e

    .line 2461
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 2463
    :cond_2e
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3d

    .line 2464
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->getRegistrationIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 2466
    :cond_3d
    const/4 v0, 0x0

    :goto_3e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v1}, Lcom/google/protobuf/ab;->size()I

    move-result v1

    if-ge v0, v1, :cond_53

    .line 2467
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$SubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v2, v0}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 2466
    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    .line 2469
    :cond_53
    return-void
.end method
