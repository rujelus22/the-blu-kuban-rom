.class public Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/ui/ae;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private m:Lcom/google/android/youtube/app/g;

.field private n:Lcom/google/android/youtube/app/k;

.field private o:Lcom/google/android/youtube/app/honeycomb/ui/x;

.field private p:Lcom/google/android/youtube/core/d;

.field private q:Z

.field private r:Z

.field private s:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method private k()V
    .registers 4

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->q:Z

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    if-nez v0, :cond_1a

    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    .line 86
    :cond_1a
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 112
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->k()V

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->e()V

    .line 114
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 117
    const-string v0, "Error authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 120
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 151
    const-string v0, "yt_upload"

    return-object v0
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->d()V

    .line 142
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 124
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->d()V

    .line 147
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 10
    .parameter

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 42
    const v1, 0x7f0400bf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 43
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->setContentView(Landroid/view/View;)V

    .line 44
    const v0, 0x7f0b015f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->e(I)V

    .line 46
    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Z

    if-eqz v0, :cond_36

    .line 47
    const v0, 0x7f080054

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/TabRow;

    .line 48
    const v1, 0x7f080055

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/ui/Workspace;

    .line 49
    invoke-static {p0, v1, v0}, Lcom/google/android/youtube/core/ui/Workspace;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;)Lcom/google/android/youtube/core/ui/Workspace;

    move-result-object v0

    .line 50
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/Workspace;->setCurrentScreen(I)V

    .line 53
    :cond_36
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 54
    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v3

    .line 56
    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/core/d;

    .line 58
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/x;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/core/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v6

    move-object v1, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/ui/x;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/honeycomb/ui/ae;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/compat/r;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    .line 65
    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/app/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->m:Lcom/google/android/youtube/app/g;

    .line 66
    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/youtube/app/k;

    .line 67
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_c

    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->e()V

    .line 101
    :cond_c
    return-void
.end method

.method protected onStart()V
    .registers 4

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->q:Z

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_19

    .line 74
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->m:Lcom/google/android/youtube/app/g;

    const v1, 0x7f0b01bf

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->e()Z

    move-result v2

    invoke-virtual {v0, p0, p0, v1, v2}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    .line 79
    :goto_18
    return-void

    .line 77
    :cond_19
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->k()V

    goto :goto_18
.end method

.method protected onStop()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 106
    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->q:Z

    .line 107
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a()V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    .line 108
    :cond_11
    return-void
.end method
