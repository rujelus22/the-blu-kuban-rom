.class Lcom/google/android/youtube/app/honeycomb/ui/a;
.super Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/youtube/app/compat/t;

.field protected b:Landroid/widget/SearchView;

.field protected c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

.field protected d:Ljava/lang/String;

.field protected final e:Ljava/util/List;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    .line 194
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    .line 195
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->e:Ljava/util/List;

    .line 196
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/a;Ljava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 180
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_a
    :goto_a
    return v0

    :cond_b
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/ui/a;->c()V

    move v0, v1

    goto :goto_a
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;
    .registers 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
    .registers 3
    .parameter

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    .line 280
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 281
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V
    .registers 3
    .parameter

    .prologue
    .line 336
    const-string v0, "listener can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 288
    if-nez p1, :cond_6

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    .line 293
    :goto_5
    return-void

    .line 291
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->d:Ljava/lang/String;

    goto :goto_5
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_a

    .line 297
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 299
    :cond_a
    return-void
.end method

.method public a(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_a

    .line 319
    :cond_9
    :goto_9
    return-void

    .line 312
    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Search"

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez p1, :cond_30

    const/4 v0, 0x1

    :goto_20
    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    .line 314
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 316
    if-nez p1, :cond_9

    .line 317
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_9

    :cond_30
    move v0, v1

    .line 313
    goto :goto_20
.end method

.method public a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x10

    const v5, 0x7f08019e

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 200
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_14

    .line 203
    invoke-virtual {p1, v5}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    .line 262
    :goto_13
    return v1

    .line 207
    :cond_14
    invoke-virtual {p1, v5}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    .line 208
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->e()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    .line 209
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Landroid/app/Activity;

    const-string v3, "search"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 211
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryRefinementEnabled(Z)V

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/ui/b;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/ui/b;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/ui/c;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/ui/c;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_78

    .line 247
    invoke-virtual {p1, v5}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    .line 249
    invoke-virtual {v0, v6, v6}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 251
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/view/View;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 253
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    goto :goto_13

    .line 257
    :cond_78
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v4, :cond_92

    move v0, v1

    :goto_81
    invoke-virtual {v3, v0}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 258
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_94

    const/16 v0, 0xa

    :goto_8e
    invoke-interface {v2, v0}, Lcom/google/android/youtube/app/compat/t;->c(I)V

    goto :goto_13

    :cond_92
    move v0, v2

    .line 257
    goto :goto_81

    .line 258
    :cond_94
    const/4 v0, 0x2

    goto :goto_8e
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_11

    .line 303
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 304
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 306
    :cond_11
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_9

    .line 328
    :cond_8
    :goto_8
    return-void

    .line 325
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_8

    .line 326
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    goto :goto_8
.end method

.method protected final d()V
    .registers 1

    .prologue
    .line 333
    return-void
.end method
