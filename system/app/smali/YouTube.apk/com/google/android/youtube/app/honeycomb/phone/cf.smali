.class final Lcom/google/android/youtube/app/honeycomb/phone/cf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

.field private final b:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1283
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1284
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->b:Lcom/google/android/youtube/core/model/UserAuth;

    .line 1285
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->e(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/d;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1279
    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->elementsPerPage:I

    if-nez v0, :cond_1e

    const-string v0, "empty search result"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->e(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/d;

    move-result-object v0

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    :goto_1d
    return-void

    :cond_1e
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_29
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_29

    :cond_3b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->b:Lcom/google/android/youtube/core/model/UserAuth;

    const/4 v5, 0x0

    move-object v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    goto :goto_1d
.end method
