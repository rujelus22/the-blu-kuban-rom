.class public Lcom/google/android/youtube/core/player/TimeBar;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I


# instance fields
.field private final d:Lcom/google/android/youtube/core/player/bf;

.field private final e:Landroid/graphics/Rect;

.field private final f:Landroid/graphics/Rect;

.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Paint;

.field private final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/drawable/StateListDrawable;

.field private final m:I

.field private n:I

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/lang/String;

.field private final z:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-array v0, v3, [I

    const v1, 0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/TimeBar;->a:[I

    .line 37
    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/TimeBar;->b:[I

    .line 38
    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/youtube/core/player/TimeBar;->c:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/bf;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 73
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/bf;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->d:Lcom/google/android/youtube/core/player/bf;

    .line 76
    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->r:Z

    .line 77
    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->s:Z

    .line 78
    iput-boolean v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->t:Z

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    .line 81
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->f:Landroid/graphics/Rect;

    .line 82
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    .line 84
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->h:Landroid/graphics/Paint;

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->h:Landroid/graphics/Paint;

    const v1, -0x7f7f80

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->i:Landroid/graphics/Paint;

    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 88
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->j:Landroid/graphics/Paint;

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 91
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x4170

    mul-float/2addr v0, v2

    .line 92
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    .line 93
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 97
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    const-string v2, "0:00:00"

    const/4 v3, 0x0

    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 99
    const-wide/16 v2, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/youtube/core/player/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->y:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0201a0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Lcom/google/android/youtube/core/player/TimeBar;->b:[I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 103
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x4100

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->m:I

    .line 104
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .registers 11
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 335
    long-to-int v0, p1

    div-int/lit16 v0, v0, 0x3e8

    .line 336
    rem-int/lit8 v1, v0, 0x3c

    .line 337
    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    .line 338
    div-int/lit16 v0, v0, 0xe10

    .line 340
    iget v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    const v4, 0x36ee80

    if-lt v3, v4, :cond_35

    .line 341
    const-string v3, "%d:%02d:%02d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 343
    :goto_34
    return-object v0

    :cond_35
    const-string v0, "%02d:%02d"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_34
.end method

.method private a(F)V
    .registers 6
    .parameter

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 210
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v0

    .line 211
    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    .line 212
    float-to-int v3, p1

    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    .line 213
    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    .line 214
    return-void
.end method

.method private d()V
    .registers 8

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 110
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v0, :cond_67

    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->w:I

    .line 111
    :goto_14
    iget v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    if-lez v1, :cond_6a

    .line 112
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/google/android/youtube/core/player/TimeBar;->x:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x64

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 113
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-long v3, v3

    iget v5, p0, Lcom/google/android/youtube/core/player/TimeBar;->v:I

    int-to-long v5, v5

    mul-long/2addr v3, v5

    iget v5, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    int-to-long v5, v5

    div-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 115
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    int-to-long v4, v0

    mul-long/2addr v2, v4

    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    int-to-long v4, v0

    div-long/2addr v2, v4

    long-to-int v0, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    .line 122
    :goto_63
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->invalidate()V

    .line 123
    return-void

    .line 110
    :cond_67
    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->v:I

    goto :goto_14

    .line 118
    :cond_6a
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 120
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    goto :goto_63
.end method

.method private e()V
    .registers 3

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    if-lez v0, :cond_1c

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->p:Z

    .line 127
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v0, :cond_1e

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->p:Z

    if-nez v0, :cond_1e

    .line 128
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->g()V

    .line 129
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 134
    :goto_1b
    return-void

    .line 126
    :cond_1c
    const/4 v0, 0x0

    goto :goto_b

    .line 132
    :cond_1e
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v0, :cond_2a

    sget-object v0, Lcom/google/android/youtube/core/player/TimeBar;->c:[I

    :goto_26
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    goto :goto_1b

    :cond_2a
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->p:Z

    if-eqz v0, :cond_31

    sget-object v0, Lcom/google/android/youtube/core/player/TimeBar;->a:[I

    goto :goto_26

    :cond_31
    sget-object v0, Lcom/google/android/youtube/core/player/TimeBar;->b:[I

    goto :goto_26
.end method

.method private f()I
    .registers 5

    .prologue
    .line 217
    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private g()V
    .registers 2

    .prologue
    .line 329
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    .line 330
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->e()V

    .line 331
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->invalidate()V

    .line 332
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-virtual {p0, v0, v0, v0}, Lcom/google/android/youtube/core/player/TimeBar;->setTime(III)V

    .line 168
    return-void
.end method

.method public final b()I
    .registers 3

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->m:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final c()I
    .registers 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->m:I

    add-int/2addr v0, v1

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter

    .prologue
    .line 252
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 253
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 257
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->t:Z

    if-eqz v0, :cond_18

    .line 258
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 260
    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->g:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 263
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->s:Z

    if-eqz v0, :cond_43

    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    iget v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    iget v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->o:I

    iget v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->n:I

    iget-object v4, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/youtube/core/player/TimeBar;->o:I

    iget-object v5, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 269
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 272
    :cond_43
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->r:Z

    if-eqz v0, :cond_8d

    .line 273
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v0, :cond_8e

    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->w:I

    int-to-long v0, v0

    :goto_4e
    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->y:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 284
    :cond_8d
    return-void

    .line 273
    :cond_8e
    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->v:I

    int-to-long v0, v0

    goto :goto_4e
.end method

.method protected onMeasure(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 231
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->r:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->s:Z

    if-eqz v0, :cond_29

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->b()I

    move-result v0

    .line 233
    :goto_d
    invoke-static {v1, p1}, Lcom/google/android/youtube/core/player/TimeBar;->getDefaultSize(II)I

    move-result v2

    .line 234
    invoke-static {v0, p2}, Lcom/google/android/youtube/core/player/TimeBar;->resolveSize(II)I

    move-result v0

    .line 235
    invoke-virtual {p0, v2, v0}, Lcom/google/android/youtube/core/player/TimeBar;->setMeasuredDimension(II)V

    .line 237
    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->r:Z

    if-nez v3, :cond_2b

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->s:Z

    if-nez v3, :cond_2b

    .line 238
    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {v3, v1, v1, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 247
    :goto_25
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 248
    return-void

    :cond_29
    move v0, v1

    .line 231
    goto :goto_d

    .line 240
    :cond_2b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    .line 241
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->z:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->m:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->o:I

    .line 242
    iget v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->o:I

    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    .line 243
    iget-object v3, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->getPaddingLeft()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->getPaddingRight()I

    move-result v5

    sub-int/2addr v2, v5

    sub-int v0, v2, v0

    add-int/lit8 v2, v1, 0x4

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_25
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 289
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->p:Z

    if-eqz v2, :cond_17

    .line 290
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v3, v2

    .line 291
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 293
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_92

    :cond_17
    move v0, v1

    .line 325
    :goto_18
    return v0

    .line 295
    :pswitch_19
    int-to-float v4, v3

    int-to-float v2, v2

    iget v5, p0, Lcom/google/android/youtube/core/player/TimeBar;->o:I

    iget-object v6, p0, Lcom/google/android/youtube/core/player/TimeBar;->l:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/StateListDrawable;->getIntrinsicHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    cmpg-float v6, v6, v4

    if-gez v6, :cond_66

    iget-object v6, p0, Lcom/google/android/youtube/core/player/TimeBar;->e:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    cmpg-float v4, v4, v6

    if-gez v4, :cond_66

    iget v4, p0, Lcom/google/android/youtube/core/player/TimeBar;->o:I

    iget v6, p0, Lcom/google/android/youtube/core/player/TimeBar;->m:I

    sub-int/2addr v4, v6

    int-to-float v4, v4

    cmpg-float v4, v4, v2

    if-gez v4, :cond_66

    iget v4, p0, Lcom/google/android/youtube/core/player/TimeBar;->m:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v2, v2, v4

    if-gez v2, :cond_66

    move v2, v0

    :goto_49
    if-eqz v2, :cond_17

    .line 296
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    .line 297
    int-to-float v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->a(F)V

    .line 298
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->f()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->w:I

    .line 299
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->d:Lcom/google/android/youtube/core/player/bf;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/bf;->n_()V

    .line 300
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->e()V

    .line 301
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 302
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->invalidate()V

    goto :goto_18

    :cond_66
    move v2, v1

    .line 295
    goto :goto_49

    .line 307
    :pswitch_68
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v2, :cond_17

    .line 308
    int-to-float v1, v3

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->a(F)V

    .line 309
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->f()I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->w:I

    .line 310
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 311
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->invalidate()V

    goto :goto_18

    .line 316
    :pswitch_7d
    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v2, :cond_17

    .line 317
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->g()V

    .line 318
    iget v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->w:I

    iput v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->v:I

    .line 319
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TimeBar;->d:Lcom/google/android/youtube/core/player/bf;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->f()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/bf;->a(I)V

    goto :goto_18

    .line 293
    :pswitch_data_92
    .packed-switch 0x0
        :pswitch_19
        :pswitch_7d
        :pswitch_68
    .end packed-switch
.end method

.method public setBufferedPercent(I)V
    .registers 2
    .parameter

    .prologue
    .line 171
    iput p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->x:I

    .line 172
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 173
    return-void
.end method

.method public setEnabled(Z)V
    .registers 2
    .parameter

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->e()V

    .line 140
    return-void
.end method

.method public setProgressColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 145
    return-void
.end method

.method public setScrubberTime(I)V
    .registers 2
    .parameter

    .prologue
    .line 190
    iput p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->w:I

    .line 191
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 192
    return-void
.end method

.method public setScrubbing(Z)V
    .registers 2
    .parameter

    .prologue
    .line 195
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    .line 196
    return-void
.end method

.method public setShowBuffered(Z)V
    .registers 2
    .parameter

    .prologue
    .line 185
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->t:Z

    .line 186
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 187
    return-void
.end method

.method public setShowScrubber(Z)V
    .registers 4
    .parameter

    .prologue
    .line 176
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->s:Z

    .line 177
    if-nez p1, :cond_14

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    if-eqz v0, :cond_14

    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->d:Lcom/google/android/youtube/core/player/bf;

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->f()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/bf;->a(I)V

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->q:Z

    .line 181
    :cond_14
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->requestLayout()V

    .line 182
    return-void
.end method

.method public setShowTimes(Z)V
    .registers 2
    .parameter

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->r:Z

    .line 163
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TimeBar;->requestLayout()V

    .line 164
    return-void
.end method

.method public setTime(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->v:I

    if-ne v0, p1, :cond_c

    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    if-ne v0, p2, :cond_c

    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->x:I

    if-eq v0, p3, :cond_23

    .line 150
    :cond_c
    iget v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    if-eq v0, p2, :cond_1c

    .line 151
    iput p2, p0, Lcom/google/android/youtube/core/player/TimeBar;->u:I

    .line 152
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TimeBar;->y:Ljava/lang/String;

    .line 153
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->e()V

    .line 155
    :cond_1c
    iput p1, p0, Lcom/google/android/youtube/core/player/TimeBar;->v:I

    .line 156
    iput p3, p0, Lcom/google/android/youtube/core/player/TimeBar;->x:I

    .line 157
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/TimeBar;->d()V

    .line 159
    :cond_23
    return-void
.end method
