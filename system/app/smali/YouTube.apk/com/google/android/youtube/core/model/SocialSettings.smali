.class public Lcom/google/android/youtube/core/model/SocialSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ID_TO_NETWORK:Ljava/util/Map;


# instance fields
.field public final facebook:Lcom/google/android/youtube/core/model/k;

.field public final orkut:Lcom/google/android/youtube/core/model/k;

.field public final twitter:Lcom/google/android/youtube/core/model/k;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 28
    sput-object v0, Lcom/google/android/youtube/core/model/SocialSettings;->ID_TO_NETWORK:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->id:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->FACEBOOK:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings;->ID_TO_NETWORK:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->id:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->TWITTER:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/google/android/youtube/core/model/SocialSettings;->ID_TO_NETWORK:Ljava/util/Map;

    sget-object v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->id:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;->ORKUT:Lcom/google/android/youtube/core/model/SocialSettings$SocialNetwork;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/model/k;Lcom/google/android/youtube/core/model/k;Lcom/google/android/youtube/core/model/k;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/youtube/core/model/SocialSettings;->facebook:Lcom/google/android/youtube/core/model/k;

    .line 35
    iput-object p2, p0, Lcom/google/android/youtube/core/model/SocialSettings;->twitter:Lcom/google/android/youtube/core/model/k;

    .line 36
    iput-object p3, p0, Lcom/google/android/youtube/core/model/SocialSettings;->orkut:Lcom/google/android/youtube/core/model/k;

    .line 37
    return-void
.end method

.method static synthetic access$000(Ljava/util/Set;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 19
    invoke-static {p0}, Lcom/google/android/youtube/core/model/SocialSettings;->unmodifiable(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private static unmodifiable(Ljava/util/Set;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 117
    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    if-ne p0, v0, :cond_5

    :goto_4
    return-object p0

    :cond_5
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    goto :goto_4
.end method
