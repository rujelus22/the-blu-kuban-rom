.class public final Lcom/google/android/youtube/app/ui/k;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/al;

.field private final c:Lcom/google/android/youtube/core/async/c;

.field private final d:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

.field private final e:Lcom/google/android/youtube/core/a/c;

.field private final f:Lcom/google/android/youtube/app/adapter/cy;

.field private final g:I

.field private final h:Lcom/google/android/youtube/app/YouTubeApplication;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/a/c;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/cy;Landroid/content/res/Resources;Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 131
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/k;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/k;->a:Lcom/google/android/youtube/core/b/al;

    .line 133
    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/k;->c:Lcom/google/android/youtube/core/async/c;

    .line 134
    const-string v0, "listAdapterOutline cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/c;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    .line 136
    const-string v0, "loadingOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cy;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/k;->f:Lcom/google/android/youtube/app/adapter/cy;

    .line 138
    const-string v0, "category cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/k;->d:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    .line 139
    const v0, 0x7f0d000a

    invoke-virtual {p6, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const v1, 0x7f0d0009

    invoke-virtual {p6, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/ui/k;->g:I

    .line 141
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-virtual {p2, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 144
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ab;->a(Landroid/net/Uri;)V

    .line 193
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/ab;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 3
    .parameter

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ab;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    .line 189
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 147
    sget-object v0, Lcom/google/android/youtube/app/ui/m;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/k;->d:Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline$Category;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4c

    .line 168
    :goto_d
    return-void

    .line 149
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->a:Lcom/google/android/youtube/core/b/al;

    iget v1, p0, Lcom/google/android/youtube/app/ui/k;->g:I

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/k;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/youtube/core/b/al;->f(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_d

    .line 152
    :pswitch_18
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/k;->a:Lcom/google/android/youtube/core/b/al;

    iget v2, p0, Lcom/google/android/youtube/app/ui/k;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/k;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/youtube/core/b/al;->a(ILjava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_d

    .line 156
    :pswitch_28
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->a:Lcom/google/android/youtube/core/b/al;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_VIEWED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    iget v2, p0, Lcom/google/android/youtube/app/ui/k;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/k;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_d

    .line 160
    :pswitch_34
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->a:Lcom/google/android/youtube/core/b/al;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_SUBSCRIBED:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    iget v2, p0, Lcom/google/android/youtube/app/ui/k;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/k;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_d

    .line 164
    :pswitch_40
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->a:Lcom/google/android/youtube/core/b/al;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;->NOTEWORTHY:Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;

    iget v2, p0, Lcom/google/android/youtube/app/ui/k;->g:I

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/k;->c:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/youtube/core/async/l;)V

    goto :goto_d

    .line 147
    :pswitch_data_4c
    .packed-switch 0x1
        :pswitch_e
        :pswitch_18
        :pswitch_28
        :pswitch_34
        :pswitch_40
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 47
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 47
    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->f:Lcom/google/android/youtube/app/adapter/cy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cy;->c(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bt;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->b(Ljava/lang/Iterable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 176
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/ab;->notifyDataSetChanged()V

    .line 197
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/k;->e:Lcom/google/android/youtube/core/a/c;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/c;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ab;->b(Landroid/net/Uri;)V

    .line 201
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 172
    return-void
.end method
