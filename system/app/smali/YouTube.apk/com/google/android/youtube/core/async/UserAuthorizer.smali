.class public final Lcom/google/android/youtube/core/async/UserAuthorizer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/youtube/core/async/a;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Z

.field private final d:Ljava/util/Set;

.field private final e:Lcom/google/android/youtube/core/async/br;

.field private f:Lcom/google/android/youtube/core/b/al;

.field private g:Landroid/accounts/Account;

.field private h:Z

.field private final i:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/a;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/br;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;-><init>(Lcom/google/android/youtube/core/async/a;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/br;Z)V

    .line 103
    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/async/a;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/br;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    const-string v0, "accountManagerWrapper cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    .line 114
    const-string v0, "preferences cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    .line 115
    const-string v0, "signInIntentFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/br;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->e:Lcom/google/android/youtube/core/async/br;

    .line 117
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->d:Ljava/util/Set;

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->c:Z

    .line 120
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/UserAuthorizer;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/accounts/Account;Landroid/app/Activity;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 440
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->f:Lcom/google/android/youtube/core/b/al;

    if-nez v0, :cond_f

    new-instance v0, Lcom/google/android/youtube/core/async/bm;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/core/async/bm;-><init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Landroid/app/Activity;)V

    :goto_b
    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/youtube/core/async/a;->a(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 442
    return-void

    .line 440
    :cond_f
    new-instance v0, Lcom/google/android/youtube/core/async/bs;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/core/async/bs;-><init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Landroid/app/Activity;)V

    goto :goto_b
.end method

.method private declared-synchronized a(Landroid/app/Activity;)V
    .registers 5
    .parameter

    .prologue
    .line 428
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->g:Landroid/accounts/Account;

    if-eqz v0, :cond_22

    .line 430
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    iget-object v2, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->g:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 432
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->g:Landroid/accounts/Account;

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/accounts/Account;Landroid/app/Activity;)V

    .line 433
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->g:Landroid/accounts/Account;
    :try_end_20
    .catchall {:try_start_1 .. :try_end_20} :catchall_26

    .line 437
    :goto_20
    monitor-exit p0

    return-void

    .line 435
    :cond_22
    :try_start_22
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->e()V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_26

    goto :goto_20

    .line 428
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/youtube/core/async/bn;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 213
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    const-string v0, "callback cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_42

    .line 217
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v0

    .line 218
    if-eqz v0, :cond_28

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 219
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a()V

    .line 221
    :cond_28
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/async/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 222
    if-eqz v0, :cond_43

    .line 223
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "user_account"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 225
    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/accounts/Account;Landroid/app/Activity;)V

    .line 231
    :cond_42
    :goto_42
    return-void

    .line 227
    :cond_43
    iput-object v2, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->g:Landroid/accounts/Account;

    .line 228
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->e:Lcom/google/android/youtube/core/async/br;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    invoke-interface {v0, p1, v1, v2, p3}, Lcom/google/android/youtube/core/async/br;->a(Landroid/content/Context;Lcom/google/android/youtube/core/async/a;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x387

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_42
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/UserAuthorizer;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->g()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/async/UserAuthorizer;)Z
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->c:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/async/UserAuthorizer;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->f:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method private declared-synchronized f()Ljava/lang/String;
    .registers 4

    .prologue
    .line 313
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    const-string v1, "username"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_c

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()V
    .registers 3

    .prologue
    .line 317
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "username"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_18

    .line 319
    monitor-exit p0

    return-void

    .line 317
    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 3

    .prologue
    .line 289
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->g()V

    .line 290
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->h:Z

    .line 291
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bo;

    .line 292
    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bo;->z()V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_1d

    goto :goto_d

    .line 289
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 294
    :cond_20
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 181
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/youtube/core/async/bn;)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 182
    monitor-exit p0

    return-void

    .line 181
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 332
    monitor-enter p0

    :try_start_2
    iget-boolean v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->h:Z

    if-eqz v1, :cond_12

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_46

    .line 335
    :cond_12
    :goto_12
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "user_account"

    iget-object v3, p2, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "username"

    iget-object v3, p2, Lcom/google/android/youtube/core/model/UserAuth;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 339
    if-eqz v0, :cond_4b

    .line 340
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_33
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bo;

    .line 341
    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bo;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    :try_end_42
    .catchall {:try_start_2 .. :try_end_42} :catchall_43

    goto :goto_33

    .line 332
    :catchall_43
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_46
    const/4 v0, 0x0

    goto :goto_12

    .line 343
    :cond_48
    const/4 v0, 0x1

    :try_start_49
    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->h:Z

    .line 346
    :cond_4b
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 347
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 348
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bn;

    .line 349
    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/async/bn;->a(Lcom/google/android/youtube/core/model/UserAuth;)V
    :try_end_6a
    .catchall {:try_start_49 .. :try_end_6a} :catchall_43

    goto :goto_5b

    .line 351
    :cond_6b
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Landroid/app/Activity;ZZLcom/google/android/youtube/core/async/bn;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 159
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1c

    if-eqz p2, :cond_1c

    .line 162
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/a;->b()[Landroid/accounts/Account;

    move-result-object v1

    .line 163
    array-length v2, v1

    if-ne v2, v3, :cond_1c

    .line 164
    const/4 v0, 0x0

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 168
    :cond_1c
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 170
    const-string v2, "allowSkip"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 172
    invoke-direct {p0, p1, v0, v1, p4}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/youtube/core/async/bn;)V
    :try_end_2a
    .catchall {:try_start_2 .. :try_end_2a} :catchall_2c

    .line 173
    monitor-exit p0

    return-void

    .line 159
    :catchall_2c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/youtube/core/async/bn;)V
    .registers 5
    .parameter

    .prologue
    .line 262
    monitor-enter p0

    :try_start_1
    const-string v0, "callback cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/async/bp;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/core/async/bp;-><init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/bn;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/a;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/bn;)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 264
    monitor-exit p0

    return-void

    .line 262
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/youtube/core/async/bo;)V
    .registers 3
    .parameter

    .prologue
    .line 277
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 278
    monitor-exit p0

    return-void

    .line 277
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/youtube/core/b/al;)V
    .registers 3
    .parameter

    .prologue
    .line 137
    const-string v0, "gdataClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->f:Lcom/google/android/youtube/core/b/al;

    .line 139
    return-void
.end method

.method final declared-synchronized a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 354
    monitor-enter p0

    :try_start_1
    const-string v0, "authentication error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->c:Z

    if-eqz v0, :cond_d

    .line 356
    invoke-direct {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->g()V

    .line 358
    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 359
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 360
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bn;

    .line 361
    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/bn;->a(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2d

    goto :goto_1d

    .line 354
    :catchall_2d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 363
    :cond_30
    monitor-exit p0

    return-void
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 396
    const/16 v0, 0x387

    if-eq p2, v0, :cond_7

    .line 397
    const/4 v0, 0x0

    .line 424
    :goto_6
    return v0

    .line 400
    :cond_7
    if-nez p3, :cond_e

    .line 401
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;)V

    .line 424
    :cond_c
    :goto_c
    const/4 v0, 0x1

    goto :goto_6

    .line 402
    :cond_e
    const/4 v0, -0x1

    if-ne p3, v0, :cond_c

    .line 404
    if-eqz p4, :cond_36

    .line 405
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    const-string v2, "authAccount"

    invoke-virtual {p4, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 410
    :goto_1f
    if-eqz v0, :cond_3d

    .line 411
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "user_account"

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 413
    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/accounts/Account;Landroid/app/Activity;)V

    goto :goto_c

    .line 408
    :cond_36
    const-string v0, "Authentication failed: intent result can\'t be null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1f

    .line 415
    :cond_3d
    if-nez p4, :cond_46

    move-object v0, v1

    .line 417
    :goto_40
    if-eqz v0, :cond_4f

    .line 418
    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_c

    .line 415
    :cond_46
    const-string v0, "exception"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    goto :goto_40

    .line 420
    :cond_4f
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->e()V

    goto :goto_c
.end method

.method public final declared-synchronized b()V
    .registers 3

    .prologue
    .line 297
    monitor-enter p0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_19

    move-result-object v0

    if-nez v0, :cond_9

    .line 306
    :cond_7
    :goto_7
    monitor-exit p0

    return-void

    .line 300
    :cond_9
    :try_start_9
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v0

    .line 301
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->a:Lcom/google/android/youtube/core/async/a;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/async/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 302
    if-nez v0, :cond_7

    .line 304
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a()V
    :try_end_18
    .catchall {:try_start_9 .. :try_end_18} :catchall_19

    goto :goto_7

    .line 297
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/youtube/core/async/bo;)V
    .registers 3
    .parameter

    .prologue
    .line 281
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 282
    monitor-exit p0

    return-void

    .line 281
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Ljava/lang/String;
    .registers 4

    .prologue
    .line 309
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    const-string v1, "user_account"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_c

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .registers 3

    .prologue
    .line 325
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->b:Landroid/content/SharedPreferences;

    const-string v1, "user_account"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_b

    move-result v0

    monitor-exit p0

    return v0

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized e()V
    .registers 3

    .prologue
    .line 366
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->g()V

    .line 367
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 368
    iget-object v1, p0, Lcom/google/android/youtube/core/async/UserAuthorizer;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 369
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/bn;

    .line 370
    invoke-interface {v0}, Lcom/google/android/youtube/core/async/bn;->i_()V
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_24

    goto :goto_14

    .line 366
    :catchall_24
    move-exception v0

    monitor-exit p0

    throw v0

    .line 372
    :cond_27
    monitor-exit p0

    return-void
.end method
