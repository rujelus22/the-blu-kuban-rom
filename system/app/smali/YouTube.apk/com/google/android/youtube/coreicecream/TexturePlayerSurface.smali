.class public Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/google/android/youtube/core/player/au;


# instance fields
.field private a:Lcom/google/android/youtube/core/player/av;

.field private b:Lcom/google/android/youtube/coreicecream/d;

.field private c:Landroid/view/ViewGroup;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 41
    new-instance v0, Lcom/google/android/youtube/coreicecream/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/coreicecream/e;-><init>(Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->addView(Landroid/view/View;)V

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;)I
    .registers 2
    .parameter

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->d:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;)I
    .registers 2
    .parameter

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->e:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;)Lcom/google/android/youtube/coreicecream/d;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 94
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;)V
    .registers 4
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/d;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-nez v0, :cond_14

    .line 107
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaPlayer should only be attached after SurfaceTexture is available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_14
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v1}, Lcom/google/android/youtube/coreicecream/d;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/aq;->setSurface(Landroid/view/Surface;)V

    .line 111
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/av;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 69
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/av;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/coreicecream/d;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->c()V

    :cond_1f
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v4, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    .line 72
    :cond_28
    new-instance v0, Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/coreicecream/d;-><init>(Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/coreicecream/d;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/coreicecream/d;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/coreicecream/d;->setPivotX(F)V

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/coreicecream/d;->setPivotY(F)V

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 138
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 63
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_16

    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 66
    :cond_16
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1a

    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->measure(II)V

    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 58
    :goto_16
    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->setMeasuredDimension(II)V

    .line 59
    return-void

    .line 55
    :cond_1a
    invoke-static {v2, p1}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->getDefaultSize(II)I

    move-result v1

    .line 56
    invoke-static {v2, p2}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->getDefaultSize(II)I

    move-result v0

    goto :goto_16
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->a()V

    .line 156
    :cond_9
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .registers 3
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->c()V

    .line 162
    :cond_9
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->b()V

    .line 144
    :cond_9
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .registers 3
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->b()V

    .line 150
    :cond_9
    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/aw;)V
    .registers 2
    .parameter

    .prologue
    .line 115
    return-void
.end method

.method public setVideoSize(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 98
    iput p1, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->d:I

    .line 99
    iput p2, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->e:I

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    if-eqz v0, :cond_d

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;->b:Lcom/google/android/youtube/coreicecream/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/d;->requestLayout()V

    .line 103
    :cond_d
    return-void
.end method

.method public setZoom(I)V
    .registers 2
    .parameter

    .prologue
    .line 134
    return-void
.end method
