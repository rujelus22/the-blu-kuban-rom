.class final Lcom/google/android/youtube/app/honeycomb/ui/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SearchView$OnSuggestionListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/a;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/a;)V
    .registers 2
    .parameter

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/c;->a:Lcom/google/android/youtube/app/honeycomb/ui/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSuggestionClick(I)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 232
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/c;->a:Lcom/google/android/youtube/app/honeycomb/ui/a;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 233
    if-eqz v1, :cond_25

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 235
    :try_start_15
    const-string v2, "suggest_intent_query"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 236
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 237
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/c;->a:Lcom/google/android/youtube/app/honeycomb/ui/a;

    invoke-static {v2, v1}, Lcom/google/android/youtube/app/honeycomb/ui/a;->a(Lcom/google/android/youtube/app/honeycomb/ui/a;Ljava/lang/String;)Z
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_24} :catch_26

    move-result v0

    .line 242
    :cond_25
    :goto_25
    return v0

    .line 239
    :catch_26
    move-exception v1

    goto :goto_25
.end method

.method public final onSuggestionSelect(I)Z
    .registers 3
    .parameter

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method
