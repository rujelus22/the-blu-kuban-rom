.class public final Lcom/google/android/youtube/core/player/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/al;

.field private final b:Lcom/google/android/youtube/core/b/an;

.field private final c:Lcom/google/android/youtube/core/player/h;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/youtube/core/player/j;

.field private final f:Lcom/google/android/youtube/core/player/g;

.field private g:Lcom/google/android/youtube/core/async/n;

.field private h:Lcom/google/android/youtube/core/async/n;

.field private i:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/h;Landroid/os/Handler;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v0, "gdataClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->a:Lcom/google/android/youtube/core/b/al;

    .line 45
    const-string v0, "imageClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/an;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->b:Lcom/google/android/youtube/core/b/an;

    .line 46
    const-string v0, "brandingOverlay cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/j;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->e:Lcom/google/android/youtube/core/player/j;

    .line 47
    const-string v0, "listener cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/h;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->c:Lcom/google/android/youtube/core/player/h;

    .line 48
    const-string v0, "uiHandler cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->d:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/google/android/youtube/core/player/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/g;-><init>(Lcom/google/android/youtube/core/player/f;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->f:Lcom/google/android/youtube/core/player/g;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/f;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/f;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/n;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/youtube/core/player/f;->h:Lcom/google/android/youtube/core/async/n;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/async/n;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->h:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->b:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/player/j;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->e:Lcom/google/android/youtube/core/player/j;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/f;)Lcom/google/android/youtube/core/player/h;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->c:Lcom/google/android/youtube/core/player/h;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->e:Lcom/google/android/youtube/core/player/j;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/j;->c()V

    .line 67
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/f;->b()V

    .line 56
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->couldHaveBranding()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->f:Lcom/google/android/youtube/core/player/g;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/f;->g:Lcom/google/android/youtube/core/async/n;

    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->a:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/f;->d:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/f;->g:Lcom/google/android/youtube/core/async/n;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 63
    :goto_20
    return-void

    .line 61
    :cond_21
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->c:Lcom/google/android/youtube/core/player/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/h;->a(Lcom/google/android/youtube/core/model/Branding;)V

    goto :goto_20
.end method

.method public final b()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->e:Lcom/google/android/youtube/core/player/j;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/j;->d()V

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->g:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_11

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->g:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 74
    iput-object v1, p0, Lcom/google/android/youtube/core/player/f;->g:Lcom/google/android/youtube/core/async/n;

    .line 77
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->h:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_1c

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->h:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 79
    iput-object v1, p0, Lcom/google/android/youtube/core/player/f;->h:Lcom/google/android/youtube/core/async/n;

    .line 82
    :cond_1c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->i:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_27

    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/core/player/f;->i:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 84
    iput-object v1, p0, Lcom/google/android/youtube/core/player/f;->i:Lcom/google/android/youtube/core/async/n;

    .line 86
    :cond_27
    return-void
.end method
