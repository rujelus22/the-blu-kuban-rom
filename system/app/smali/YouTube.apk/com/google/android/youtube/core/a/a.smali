.class public abstract Lcom/google/android/youtube/core/a/a;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final g:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    .line 30
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 72
    :cond_10
    return-void
.end method

.method public a(ILjava/lang/Iterable;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 148
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 149
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1d

    .line 150
    add-int/lit8 v0, p1, 0x1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/youtube/core/a/a;->b(ILjava/lang/Object;)V

    move p1, v0

    goto :goto_4

    .line 152
    :cond_1d
    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 153
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Object;)V

    goto :goto_1d

    .line 155
    :cond_2b
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 156
    return-void
.end method

.method protected a(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 85
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/p;)V
    .registers 5
    .parameter

    .prologue
    .line 117
    const/4 v0, 0x0

    .line 118
    iget-object v1, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 119
    :cond_7
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 120
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/youtube/core/utils/p;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 121
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 122
    const/4 v0, 0x1

    goto :goto_7

    .line 125
    :cond_1c
    if-eqz v0, :cond_21

    .line 126
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 128
    :cond_21
    return-void
.end method

.method protected a(Ljava/lang/Iterable;)V
    .registers 4
    .parameter

    .prologue
    .line 62
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 63
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Object;)V

    goto :goto_4

    .line 65
    :cond_12
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .registers 3
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 100
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 102
    :cond_b
    return-void
.end method

.method protected b(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 160
    return-void
.end method

.method public b(Ljava/lang/Iterable;)V
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Iterable;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 59
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 94
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 96
    :cond_b
    return-void
.end method

.method public c(ILjava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/core/a/a;->a(ILjava/lang/Object;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 90
    return-void
.end method

.method public c(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 106
    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 107
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_6

    .line 108
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 110
    const/4 v0, 0x1

    .line 113
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public final d(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Object;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/a;->notifyDataSetChanged()V

    .line 81
    return-void
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 5
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_f

    .line 44
    iget-object v0, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 46
    :cond_f
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "should be < than "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/a/a;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 53
    int-to-long v0, p1

    return-wide v0
.end method
