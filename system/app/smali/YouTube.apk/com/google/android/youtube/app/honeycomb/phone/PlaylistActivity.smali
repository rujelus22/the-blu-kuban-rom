.class public Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/ed;
.implements Lcom/google/android/youtube/app/ui/y;
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Z

.field private D:Ljava/lang/String;

.field private E:Landroid/net/Uri;

.field private F:Lcom/google/android/youtube/core/async/l;

.field private G:Lcom/google/android/youtube/app/prefetch/d;

.field private H:Lcom/google/android/youtube/core/j;

.field private I:Z

.field private J:Ljava/lang/String;

.field private K:I

.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/av;

.field private o:Lcom/google/android/youtube/core/async/av;

.field private p:Lcom/google/android/youtube/core/b/al;

.field private q:Lcom/google/android/youtube/core/b/an;

.field private r:Lcom/google/android/youtube/core/b/ap;

.field private s:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private t:Lcom/google/android/youtube/core/d;

.field private u:Lcom/google/android/youtube/app/ui/s;

.field private v:I

.field private w:Lcom/google/android/youtube/app/ui/eb;

.field private x:Lcom/google/android/youtube/app/adapter/bt;

.field private y:Lcom/google/android/youtube/app/ui/by;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    .line 116
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "playlist_uri"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->c(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    iput p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)I
    .registers 2
    .parameter

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    return v0
.end method

.method private c(I)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 359
    if-nez p1, :cond_17

    .line 360
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    const v1, 0x7f0b01d2

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    :goto_16
    return-void

    .line 361
    :cond_17
    if-ne p1, v4, :cond_2c

    .line 362
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    const v1, 0x7f0b01d3

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_16

    .line 364
    :cond_2c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    const v1, 0x7f0b01d4

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_16
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)I
    .registers 2
    .parameter

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    return v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 354
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 356
    :cond_12
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 104
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    .line 105
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    .line 106
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/b/an;

    .line 107
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->r:Lcom/google/android/youtube/core/b/ap;

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->h()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->n:Lcom/google/android/youtube/core/async/av;

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->i()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->o:Lcom/google/android/youtube/core/async/av;

    .line 110
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 111
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/d;

    .line 112
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/app/prefetch/d;

    .line 113
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->H:Lcom/google/android/youtube/core/j;

    .line 114
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 7
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    invoke-virtual {v3, v4, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 279
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Z

    sget-object v3, Lcom/google/android/youtube/app/m;->O:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v0, v1, p2, v2, v3}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)V

    .line 243
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x8

    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->e()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 59
    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    if-eqz v0, :cond_18

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Z

    sget-object v5, Lcom/google/android/youtube/app/m;->O:Lcom/google/android/youtube/core/b/aq;

    invoke-interface {v0, v3, v2, v4, v5}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    if-nez v0, :cond_36

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v4}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    :cond_36
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->summary:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->summary:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6d

    move v0, v1

    :goto_4f
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    if-eqz v0, :cond_63

    iget v0, p2, Lcom/google/android/youtube/core/model/Playlist;->size:I

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->K:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->c(I)V

    :cond_63
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->J:Ljava/lang/String;

    if-eqz v3, :cond_6f

    :goto_69
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_6d
    move v0, v2

    goto :goto_4f

    :cond_6f
    move v2, v1

    goto :goto_69
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->finish()V

    .line 284
    return-void
.end method

.method public final synthetic a(ILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 59
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:I

    if-ne p1, v1, :cond_19

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/s;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bp;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/d;

    invoke-direct {v1, p0, p2, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/bp;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/d;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    const/4 v0, 0x1

    :cond_19
    return v0
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 323
    packed-switch p1, :pswitch_data_c

    .line 328
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 325
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 323
    :pswitch_data_c
    .packed-switch 0x3ec
        :pswitch_5
    .end packed-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 343
    const-string v0, "yt_playlist"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->finish()V

    .line 288
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 348
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 349
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->i()V

    .line 350
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter

    .prologue
    const v10, 0x7f0a005d

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 155
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 156
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_43

    const v0, 0x7f04009c

    :goto_11
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->setContentView(I)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_4b

    .line 161
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/n;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/n;

    move-result-object v2

    .line 162
    if-eqz v2, :cond_47

    iget-object v0, v2, Lcom/google/android/youtube/core/utils/n;->a:Ljava/lang/String;

    :goto_26
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Ljava/lang/String;

    .line 163
    if-eqz v2, :cond_49

    iget-boolean v0, v2, Lcom/google/android/youtube/core/utils/n;->b:Z

    :goto_2c
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->I:Z

    .line 167
    :goto_2e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_56

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    if-nez v0, :cond_56

    .line 168
    const-string v0, "Invalid intent: Playlist Uri or Playlist Id not set"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->finish()V

    .line 237
    :goto_42
    return-void

    .line 156
    :cond_43
    const v0, 0x7f040085

    goto :goto_11

    .line 162
    :cond_47
    const/4 v0, 0x0

    goto :goto_26

    :cond_49
    move v0, v9

    .line 163
    goto :goto_2c

    .line 165
    :cond_4b
    const-string v0, "playlist_uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    goto :goto_2e

    .line 172
    :cond_56
    const-string v0, "authenticate"

    invoke-virtual {v1, v0, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Z

    .line 174
    const v0, 0x7f0800b5

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->z:Landroid/widget/TextView;

    .line 175
    const v0, 0x7f080123

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->A:Landroid/widget/TextView;

    .line 176
    const v0, 0x7f080124

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->B:Landroid/widget/TextView;

    .line 178
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Z

    if-eqz v0, :cond_130

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->o:Lcom/google/android/youtube/core/async/av;

    move-object v8, v0

    .line 181
    :goto_86
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Z

    if-eqz v0, :cond_135

    .line 182
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x3ec

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b01fb

    const v2, 0x7f02008c

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->v:I

    .line 186
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->r:Lcom/google/android/youtube/core/b/ap;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->H:Lcom/google/android/youtube/core/j;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->u:Lcom/google/android/youtube/app/ui/s;

    const v6, 0x7f04002b

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/app/ui/s;I)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    .line 203
    :goto_ba
    const v0, 0x7f0800c6

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/g;

    .line 204
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/ui/by;

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 207
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    const v5, 0x7f0a005c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 212
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->i()V

    .line 214
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bo;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->y:Lcom/google/android/youtube/app/ui/by;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->t:Lcom/google/android/youtube/core/d;

    move-object v1, p0

    move-object v2, p0

    move-object v5, v8

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/honeycomb/phone/bo;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/ui/ed;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    .line 228
    invoke-static {p0, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Lcom/google/android/youtube/core/async/l;

    .line 230
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    if-nez v0, :cond_145

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    new-array v1, v7, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->i(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto/16 :goto_42

    .line 178
    :cond_130
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->n:Lcom/google/android/youtube/core/async/av;

    move-object v8, v0

    goto/16 :goto_86

    .line 195
    :cond_135
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->q:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->r:Lcom/google/android/youtube/core/b/ap;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->G:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->H:Lcom/google/android/youtube/core/j;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->x:Lcom/google/android/youtube/app/adapter/bt;

    goto/16 :goto_ba

    .line 234
    :cond_145
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    new-array v1, v7, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto/16 :goto_42
.end method

.method public onResume()V
    .registers 6

    .prologue
    .line 256
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->d()V

    .line 258
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->C:Z

    if-eqz v0, :cond_12

    .line 259
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->s:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 272
    :goto_11
    return-void

    .line 261
    :cond_12
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    if-eqz v0, :cond_37

    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->E:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_11

    .line 269
    :cond_37
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->p:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->D:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->F:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_11
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 247
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 251
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->w:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->e()V

    .line 252
    return-void
.end method
