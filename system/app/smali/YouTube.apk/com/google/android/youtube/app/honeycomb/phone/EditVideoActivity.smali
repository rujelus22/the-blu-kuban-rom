.class public Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Lcom/google/android/youtube/core/model/Video;

.field private m:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private n:Landroid/content/res/Resources;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Lcom/google/android/youtube/core/b/an;

.field private q:Lcom/google/android/youtube/core/d;

.field private r:Lcom/google/android/youtube/core/model/UserAuth;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/ImageView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/EditText;

.field private x:Landroid/widget/EditText;

.field private y:Lcom/google/android/youtube/app/ui/PrivacySpinner;

.field private z:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    .line 205
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/model/Video;)Landroid/content/Intent;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 67
    const-string v0, "video can\'t be empty"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v1, "video"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 71
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;)V
    .registers 14
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 42
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->x:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->z:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2d

    const v0, 0x7f0b017c

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    :goto_2c
    return-void

    :cond_2d
    invoke-static {v2}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_34

    move-object v2, v0

    :cond_34
    invoke-static {v5}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3b

    move-object v5, v0

    :cond_3b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->y:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->a()Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->categoryTerm:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v7, v7, Lcom/google/android/youtube/core/model/Video;->accessControl:Ljava/util/Map;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v8, v8, Lcom/google/android/youtube/core/model/Video;->location:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v9, v9, Lcom/google/android/youtube/core/model/Video;->where:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v10, v10, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v11, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->r:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {p0, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v12

    invoke-interface/range {v0 .. v12}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/model/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_2c
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->u:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 129
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->r:Lcom/google/android/youtube/core/model/UserAuth;

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->w:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :cond_1f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_32

    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->x:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :cond_32
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->tags:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/googlemobile/common/util/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->z:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->tags:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    if-eqz v0, :cond_54

    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->y:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->setPrivacy(Lcom/google/android/youtube/core/model/Video$Privacy;)V

    .line 143
    :cond_54
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->t:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->n:Landroid/content/res/Resources;

    const v2, 0x7f0b01de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget v1, v1, Lcom/google/android/youtube/core/model/Video;->duration:I

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_9a

    .line 149
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->p:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->thumbnailUri:Landroid/net/Uri;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/w;

    invoke-direct {v2, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/w;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;B)V

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 154
    :goto_99
    return-void

    .line 152
    :cond_9a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->u:Landroid/widget/ImageView;

    const v1, 0x7f0200e2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_99
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 42
    const-string v0, "Error updating video metadata"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->q:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    const v0, 0x7f0b017b

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    .line 158
    return-void
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 219
    const-string v0, "yt_upload"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    .line 162
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f04002f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->setContentView(I)V

    .line 78
    const v0, 0x7f0b0179

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->e(I)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 81
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 82
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->q:Lcom/google/android/youtube/core/d;

    .line 83
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->n:Landroid/content/res/Resources;

    .line 85
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->m:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 86
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->p:Lcom/google/android/youtube/core/b/an;

    .line 88
    const v0, 0x7f0800db

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 89
    const v0, 0x7f080046

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->s:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f080040

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->u:Landroid/widget/ImageView;

    .line 91
    const v0, 0x7f08006c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->t:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f080042

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->v:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f080174

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->w:Landroid/widget/EditText;

    .line 95
    const v0, 0x7f080175

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->x:Landroid/widget/EditText;

    .line 96
    const v0, 0x7f080095

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/PrivacySpinner;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->y:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    .line 97
    const v0, 0x7f080176

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->z:Landroid/widget/EditText;

    .line 99
    const v0, 0x7f080092

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->A:Landroid/widget/Button;

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->A:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->A:Landroid/widget/Button;

    const v1, 0x104000a

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->A:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/v;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/v;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    return-void
.end method

.method protected onStart()V
    .registers 4

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 113
    const-string v1, "android.intent.action.EDIT"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    .line 126
    :goto_2c
    return-void

    .line 119
    :cond_2d
    const-string v1, "video"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    .line 120
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->B:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_44

    .line 121
    const-string v0, "video not found"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->finish()V

    goto :goto_2c

    .line 125
    :cond_44
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/EditVideoActivity;->m:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    goto :goto_2c
.end method
