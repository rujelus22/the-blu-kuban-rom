.class final Lcom/google/android/youtube/app/ui/fc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/fa;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/fa;)V
    .registers 2
    .parameter

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 283
    check-cast p1, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to get disco results for artist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->f(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/core/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 283
    check-cast p2, Lcom/google/android/youtube/core/model/ArtistBundle;

    if-nez p2, :cond_13

    const-string v0, "got null artist disco result"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    :goto_12
    return-void

    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->d(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/core/model/MusicVideo;

    move-result-object v0

    if-eqz v0, :cond_2d

    iget-object v0, p2, Lcom/google/android/youtube/core/model/ArtistBundle;->artist:Lcom/google/android/youtube/core/model/Artist;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Artist;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/fa;->d(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/core/model/MusicVideo;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/MusicVideo;->artistId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    :cond_2d
    const-string v0, "got stale result for artist disco, ignoring"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    goto :goto_12

    :cond_3c
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->b(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/ArtistBundle;->artist:Lcom/google/android/youtube/core/model/Artist;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Artist;->biography:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ef;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->c(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/ArtistBundle;->artistTape:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eg;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->e(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fe;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/ArtistBundle;->relatedArtists:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/fe;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fc;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    goto :goto_12
.end method
