.class public final Lcom/google/android/youtube/core/player/ae;
.super Landroid/media/MediaPlayer;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
.implements Lcom/google/android/youtube/core/player/aq;


# static fields
.field public static final a:Ljava/util/Set;


# instance fields
.field private b:Lcom/google/android/youtube/core/player/ar;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 39
    const-string v1, "video/mp4"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    const-string v1, "video/3gpp"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v1

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1c

    .line 42
    const-string v1, "application/x-mpegURL"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    :cond_1c
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/ae;->a:Ljava/util/Set;

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 50
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 51
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 52
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 53
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 54
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 55
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 56
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/ae;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/player/ar;)V
    .registers 2
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    .line 61
    return-void
.end method

.method public final b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/youtube/core/player/ae;->a:Ljava/util/Set;

    return-object v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method

.method public final onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/player/ar;->a(I)V

    .line 87
    :cond_9
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .registers 3
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ar;->e()V

    .line 75
    :cond_9
    return-void
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/ar;->b(Lcom/google/android/youtube/core/player/aq;II)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final onInfo(Landroid/media/MediaPlayer;II)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/ar;->a(Lcom/google/android/youtube/core/player/aq;II)Z

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .registers 3
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ar;->a(Lcom/google/android/youtube/core/player/aq;)V

    .line 99
    :cond_9
    return-void
.end method

.method public final onSeekComplete(Landroid/media/MediaPlayer;)V
    .registers 3
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ar;->b(Lcom/google/android/youtube/core/player/aq;)V

    .line 81
    :cond_9
    return-void
.end method

.method public final onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    if-eqz v0, :cond_9

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ae;->b:Lcom/google/android/youtube/core/player/ar;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/youtube/core/player/ar;->c(Lcom/google/android/youtube/core/player/aq;II)V

    .line 93
    :cond_9
    return-void
.end method

.method public final setSurface(Landroid/view/Surface;)V
    .registers 7
    .parameter

    .prologue
    .line 104
    :try_start_0
    const-class v0, Landroid/media/MediaPlayer;

    const-string v1, "setSurface"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/view/Surface;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 105
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_19} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_23

    .line 111
    return-void

    .line 107
    :catch_1a
    move-exception v0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Requires API level 14"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :catch_23
    move-exception v0

    .line 110
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
