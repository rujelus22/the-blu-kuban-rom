.class public abstract Lcom/google/android/youtube/app/honeycomb/tablet/as;
.super Lcom/google/android/youtube/coreicecream/Controller;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/ae;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/ap;

.field protected final b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

.field protected final c:Lcom/google/android/youtube/app/YouTubeApplication;

.field protected final d:Lcom/google/android/youtube/app/a;

.field protected final e:Lcom/google/android/youtube/app/g;

.field private final h:Lcom/google/android/youtube/app/ui/bx;

.field private i:Lcom/google/android/youtube/app/remote/ab;

.field private j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

.field private k:Lcom/google/android/youtube/app/ui/ca;

.field private l:Lcom/google/android/youtube/app/compat/r;

.field private m:Lcom/google/android/youtube/app/compat/m;


# direct methods
.method protected constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p4}, Lcom/google/android/youtube/coreicecream/Controller;-><init>(Landroid/app/Application;Lcom/google/android/youtube/core/Analytics;Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 64
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 65
    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->d:Lcom/google/android/youtube/app/a;

    .line 66
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->m()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    invoke-static {p2, p3, p5, v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    .line 71
    new-instance v0, Lcom/google/android/youtube/app/ui/ap;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/app/ui/ap;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a:Lcom/google/android/youtube/app/ui/ap;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/app/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->e:Lcom/google/android/youtube/app/g;

    .line 74
    new-instance v0, Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->q()I

    move-result v2

    invoke-direct {v0, p2, v1, v2}, Lcom/google/android/youtube/app/ui/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->h:Lcom/google/android/youtube/app/ui/bx;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->i:Lcom/google/android/youtube/app/remote/ab;

    .line 78
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ba;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->c()V

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b(Z)V

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V

    .line 84
    new-instance v7, Lcom/google/android/youtube/app/honeycomb/tablet/at;

    invoke-direct {v7, p0, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/at;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/as;Landroid/app/Activity;)V

    .line 89
    new-instance v0, Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/app/remote/bl;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->r()Lcom/google/android/youtube/app/remote/bm;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->q()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v8

    const v9, 0x7f0400e1

    move-object v1, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/app/ui/ca;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/ab;Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/cl;Lcom/google/android/youtube/core/d;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->k:Lcom/google/android/youtube/app/ui/ca;

    .line 99
    return-void
.end method


# virtual methods
.method protected a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 188
    sparse-switch p1, :sswitch_data_40

    .line 198
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 190
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a:Lcom/google/android/youtube/app/ui/ap;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ap;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 192
    :sswitch_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->e:Lcom/google/android/youtube/app/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 194
    :sswitch_18
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->k:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/ca;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    const v1, 0x7f0b022a

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_7

    .line 196
    :sswitch_38
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->k:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ca;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 188
    nop

    :sswitch_data_40
    .sparse-switch
        0x3eb -> :sswitch_f
        0x3f1 -> :sswitch_8
        0x3f3 -> :sswitch_18
        0x403 -> :sswitch_38
    .end sparse-switch
.end method

.method protected final a(Landroid/view/Menu;)V
    .registers 3
    .parameter

    .prologue
    .line 161
    invoke-super {p0, p1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/Menu;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->m:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 163
    return-void
.end method

.method protected final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 150
    new-instance v0, Lcom/google/android/youtube/app/compat/m;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-direct {v0, v1, p1}, Lcom/google/android/youtube/app/compat/m;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->m:Lcom/google/android/youtube/app/compat/m;

    .line 151
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->m:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->m()Lcom/google/android/youtube/app/compat/r;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V

    .line 152
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400da

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_23

    .line 108
    new-instance v2, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(I)V

    .line 110
    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/view/View;Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;)V

    .line 112
    :cond_23
    return-void
.end method

.method protected a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 3
    .parameter

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e()V

    .line 167
    return-void
.end method

.method protected a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 157
    return-void
.end method

.method public a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 3
    .parameter

    .prologue
    .line 269
    if-eqz p1, :cond_8

    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->h:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bx;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 274
    :goto_7
    return-void

    .line 272
    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->h:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->a()V

    goto :goto_7
.end method

.method protected a(IILandroid/content/Intent;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 242
    :cond_f
    :goto_f
    return v2

    .line 235
    :cond_10
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->k:Lcom/google/android/youtube/app/ui/ca;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/ui/ca;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 239
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    goto :goto_f
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->h:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bx;->b(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 251
    const/4 v0, 0x1

    .line 253
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method protected final a(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 171
    invoke-super {p0, p1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_11

    .line 175
    const/4 v0, 0x0

    .line 179
    :goto_10
    return v0

    .line 177
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->m:Lcom/google/android/youtube/app/compat/m;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_10

    .line 179
    :cond_20
    const/4 v0, 0x1

    goto :goto_10
.end method

.method protected a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/android/youtube/coreicecream/Controller;->b()V

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->o()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->i:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->c()V

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->i:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->a(Lcom/google/android/youtube/app/remote/ae;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->i:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_23

    .line 128
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->h:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 130
    :cond_23
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 246
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->h:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bx;->c(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 259
    const/4 v0, 0x1

    .line 261
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->b()V

    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->i:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->d()V

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->i:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->b(Lcom/google/android/youtube/app/remote/ae;)V

    .line 137
    invoke-super {p0}, Lcom/google/android/youtube/coreicecream/Controller;->c()V

    .line 138
    return-void
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->j:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->d()V

    .line 117
    invoke-super {p0}, Lcom/google/android/youtube/coreicecream/Controller;->d()V

    .line 118
    return-void
.end method

.method public final m()Lcom/google/android/youtube/app/compat/r;
    .registers 4

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->l:Lcom/google/android/youtube/app/compat/r;

    if-nez v0, :cond_13

    .line 142
    new-instance v0, Lcom/google/android/youtube/app/compat/r;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/r;-><init>(Landroid/content/Context;Landroid/view/MenuInflater;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->l:Lcom/google/android/youtube/app/compat/r;

    .line 144
    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->l:Lcom/google/android/youtube/app/compat/r;

    return-object v0
.end method

.method protected final n()Z
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Z)V

    .line 216
    return v1
.end method

.method protected final o()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;
    .registers 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/youtube/coreicecream/TabbedControllersActivity;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/as;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_18

    .line 223
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    .line 225
    :goto_17
    return-object v0

    :cond_18
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->EXPANDED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    goto :goto_17
.end method
