.class public final Lcom/google/android/youtube/app/adapter/cv;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 51
    new-instance v0, Lcom/google/android/youtube/app/adapter/cw;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/cw;-><init>(Lcom/google/android/youtube/app/adapter/cv;Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cv;->a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/cv;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cv;->a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    const-string v0, "thumbnailSize cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    new-instance v0, Lcom/google/android/youtube/app/adapter/cv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/youtube/app/adapter/cv;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/google/android/youtube/app/adapter/cv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/cv;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/model/Video;)Z
    .registers 3
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v1, :cond_12

    iget-object v0, p0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v1, :cond_14

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/youtube/app/adapter/cx;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/cx;-><init>(Lcom/google/android/youtube/app/adapter/cv;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
