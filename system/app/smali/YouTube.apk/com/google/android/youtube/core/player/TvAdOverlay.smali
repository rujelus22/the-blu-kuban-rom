.class public Lcom/google/android/youtube/core/player/TvAdOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/a;


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/al;

.field private final b:Lcom/google/android/youtube/core/b/an;

.field private final c:Lcom/google/android/youtube/core/async/l;

.field private final d:Lcom/google/android/youtube/core/async/l;

.field private e:Lcom/google/android/youtube/core/player/b;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 50
    const-string v0, "gdataClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->a:Lcom/google/android/youtube/core/b/al;

    .line 51
    const-string v0, "imageClient cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/an;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->b:Lcom/google/android/youtube/core/b/an;

    .line 53
    new-instance v0, Lcom/google/android/youtube/core/player/bi;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/bi;-><init>(Lcom/google/android/youtube/core/player/TvAdOverlay;B)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/bv;->a(Landroid/view/View;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/bv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->c:Lcom/google/android/youtube/core/async/l;

    .line 54
    new-instance v0, Lcom/google/android/youtube/core/player/bh;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/bh;-><init>(Lcom/google/android/youtube/core/player/TvAdOverlay;B)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/bv;->a(Landroid/view/View;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/bv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->d:Lcom/google/android/youtube/core/async/l;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 57
    const v1, 0x7f0400bb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 59
    const v0, 0x7f080046

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->f:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f08015f

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->g:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f080160

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->h:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f080040

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->i:Landroid/widget/ImageView;

    .line 64
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->setVisibility(I)V

    .line 66
    new-instance v0, Lcom/google/android/youtube/core/player/bg;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/bg;-><init>(Lcom/google/android/youtube/core/player/TvAdOverlay;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/TvAdOverlay;)Lcom/google/android/youtube/core/player/b;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->e:Lcom/google/android/youtube/core/player/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/TvAdOverlay;Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/TvAdOverlay;)Lcom/google/android/youtube/core/async/l;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->d:Lcom/google/android/youtube/core/async/l;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/TvAdOverlay;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->b:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/TvAdOverlay;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->h:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 76
    return-object p0
.end method

.method public final a(II)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const v8, 0x7f0b0070

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 107
    if-nez p2, :cond_1b

    .line 108
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, ""

    aput-object v2, v1, v6

    invoke-virtual {v0, v8, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    :goto_15
    iget-object v1, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    return-void

    .line 110
    :cond_1b
    sub-int v0, p2, p1

    div-int/lit16 v0, v0, 0x3e8

    .line 111
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/Object;

    const-string v3, "(%02d:%02d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    div-int/lit8 v5, v0, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v1, v8, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_15
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 5
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/VastAd;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->a:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->c:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->setVisibility(I)V

    .line 91
    return-void
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 80
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 94
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/TvAdOverlay;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->i:Landroid/widget/ImageView;

    const v1, 0x7f0200e2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 98
    invoke-virtual {p0, v2, v2}, Lcom/google/android/youtube/core/player/TvAdOverlay;->a(II)V

    .line 99
    return-void
.end method

.method public setFullscreen(Z)V
    .registers 2
    .parameter

    .prologue
    .line 103
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/b;)V
    .registers 2
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/youtube/core/player/TvAdOverlay;->e:Lcom/google/android/youtube/core/player/b;

    .line 85
    return-void
.end method
