.class public Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/app/honeycomb/f;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 279
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .registers 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a:Lcom/google/android/youtube/app/honeycomb/f;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/f;->a()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a:Lcom/google/android/youtube/app/honeycomb/f;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/honeycomb/f;->a(Landroid/os/Bundle;)V

    .line 317
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 2
    .parameter

    .prologue
    .line 304
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onAttach(Landroid/app/Activity;)V

    .line 307
    check-cast p1, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;

    .line 308
    invoke-virtual {p1, p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a(Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;)V

    .line 309
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 293
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 294
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "youtube"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 295
    const v0, 0x7f060005

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->addPreferencesFromResource(I)V

    .line 296
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/f;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v0, "prefetch_subscriptions"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "prefetch_watch_later"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/youtube/app/honeycomb/f;-><init>(Landroid/app/Activity;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a:Lcom/google/android/youtube/app/honeycomb/f;

    .line 300
    return-void
.end method
