.class final Lcom/google/android/youtube/app/player/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/player/a/e;

.field private final b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/player/a/e;)V
    .registers 3
    .parameter

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/youtube/app/player/a/g;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/g;->b:Ljava/util/concurrent/ExecutorService;

    .line 166
    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 170
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_33

    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/g;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-static {v0}, Lcom/google/android/youtube/app/player/a/e;->b(Lcom/google/android/youtube/app/player/a/e;)Ljava/net/ServerSocket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/g;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/youtube/app/player/a/h;

    iget-object v3, p0, Lcom/google/android/youtube/app/player/a/g;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-direct {v2, v3, v0}, Lcom/google/android/youtube/app/player/a/h;-><init>(Lcom/google/android/youtube/app/player/a/e;Ljava/net/Socket;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_1c
    .catchall {:try_start_0 .. :try_end_1c} :catchall_3f
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_1c} :catch_1d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1c} :catch_46

    goto :goto_0

    .line 174
    :catch_1d
    move-exception v0

    .line 175
    :try_start_1e
    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Socket closed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 176
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V
    :try_end_2d
    .catchall {:try_start_1e .. :try_end_2d} :catchall_3f

    .line 183
    :goto_2d
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/g;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 184
    :goto_32
    return-void

    .line 183
    :cond_33
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/g;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_32

    .line 178
    :cond_39
    :try_start_39
    const-string v1, "SocketException when accepting a new connection"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3e
    .catchall {:try_start_39 .. :try_end_3e} :catchall_3f

    goto :goto_2d

    .line 183
    :catchall_3f
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/g;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    throw v0

    .line 180
    :catch_46
    move-exception v0

    .line 181
    :try_start_47
    const-string v1, "IOException when accepting a new connection"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4c
    .catchall {:try_start_47 .. :try_end_4c} :catchall_3f

    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/g;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_32
.end method
