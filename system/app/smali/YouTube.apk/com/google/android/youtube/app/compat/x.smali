.class public final Lcom/google/android/youtube/app/compat/x;
.super Lcom/google/android/youtube/app/compat/w;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/MenuItem;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/compat/w;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/compat/u;)Lcom/google/android/youtube/app/compat/t;
    .registers 4
    .parameter

    .prologue
    .line 54
    if-nez p1, :cond_9

    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/x;->a:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 66
    :goto_8
    return-object p0

    .line 58
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/x;->a:Landroid/view/MenuItem;

    new-instance v1, Lcom/google/android/youtube/app/compat/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/compat/y;-><init>(Lcom/google/android/youtube/app/compat/x;Lcom/google/android/youtube/app/compat/u;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    goto :goto_8
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/x;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/x;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/x;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionProvider()Landroid/view/ActionProvider;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/View;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/x;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
