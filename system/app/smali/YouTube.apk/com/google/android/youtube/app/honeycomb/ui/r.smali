.class final Lcom/google/android/youtube/app/honeycomb/ui/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/Analytics;

.field final synthetic b:Lcom/google/android/youtube/app/honeycomb/ui/p;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/Analytics;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/core/Analytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->c(Lcom/google/android/youtube/app/honeycomb/ui/p;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02018b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/honeycomb/ui/p;Z)Z

    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteControlBarPlay"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 110
    :goto_2a
    return-void

    .line 105
    :cond_2b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02018c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->b:Lcom/google/android/youtube/app/honeycomb/ui/p;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/honeycomb/ui/p;Z)Z

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/r;->a:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteControlBarPause"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_2a
.end method
