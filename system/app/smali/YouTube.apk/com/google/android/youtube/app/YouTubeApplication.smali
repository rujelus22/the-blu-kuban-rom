.class public Lcom/google/android/youtube/app/YouTubeApplication;
.super Lcom/google/android/youtube/core/BaseApplication;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/google/android/youtube/core/async/bo;
.implements Lcom/google/android/youtube/core/b/am;
.implements Lcom/google/android/youtube/core/b/ao;
.implements Lcom/google/android/youtube/core/b/at;
.implements Lcom/google/android/youtube/core/b/av;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/google/android/youtube/app/k;

.field private c:Landroid/provider/SearchRecentSuggestions;

.field private d:Lcom/google/android/youtube/core/suggest/a;

.field private e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private f:Lcom/google/android/youtube/core/c/a;

.field private g:Lcom/google/android/youtube/app/g;

.field private h:Lcom/google/android/youtube/core/b/k;

.field private i:Lcom/google/android/youtube/core/b/d;

.field private j:Lcom/google/android/youtube/core/b/ae;

.field private k:Lcom/google/android/youtube/core/b/ac;

.field private l:Lcom/google/android/youtube/core/b/aa;

.field private m:Lcom/google/android/youtube/app/b/g;

.field private n:Lcom/google/android/youtube/app/remote/bb;

.field private o:Lcom/google/android/youtube/app/remote/bl;

.field private p:Lcom/google/android/youtube/app/remote/bm;

.field private q:Lcom/google/android/youtube/app/remote/e;

.field private r:Lcom/google/android/youtube/app/remote/AtHomeConnection;

.field private s:Lcom/google/android/youtube/app/prefetch/d;

.field private t:Lcom/google/android/youtube/app/a/a;

.field private u:Lcom/google/android/youtube/app/player/a/e;

.field private v:Lcom/google/android/youtube/app/remote/ab;

.field private w:Ljava/util/concurrent/atomic/AtomicReference;

.field private x:Ljava/util/concurrent/atomic/AtomicReference;

.field private y:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/youtube/core/BaseApplication;-><init>()V

    .line 115
    const-string v0, "android"

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->a:Ljava/lang/String;

    return-void
.end method

.method private Z()Lcom/google/android/youtube/app/YouTubePlatformUtil;
    .registers 4

    .prologue
    .line 469
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_17

    .line 470
    const-string v0, "com.google.android.youtube.app.froyo.FroyoYouTubePlatformUtil"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 476
    :goto_10
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubePlatformUtil;

    return-object v0

    .line 473
    :cond_17
    const-string v0, "com.google.android.youtube.app.honeycomb.a"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1c} :catch_1e

    move-result-object v0

    goto :goto_10

    .line 477
    :catch_1e
    move-exception v0

    .line 478
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t create YouTubePlatformUtil object"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private aa()Z
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 736
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const/16 v1, 0xce4

    if-lt v0, v1, :cond_18

    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    iget-object v0, v0, Lcom/google/android/youtube/app/k;->a:Lcom/google/android/youtube/app/l;
    :try_end_18
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_18} :catch_19

    .line 739
    :cond_18
    :goto_18
    return v3

    :catch_19
    move-exception v0

    goto :goto_18
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .registers 2

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final B()I
    .registers 2

    .prologue
    .line 702
    iget v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->y:I

    return v0
.end method

.method public final C()Lcom/google/android/youtube/app/player/a/e;
    .registers 4

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->u:Lcom/google/android/youtube/app/player/a/e;

    if-nez v0, :cond_1a

    .line 745
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/f;->a(Landroid/content/SharedPreferences;)Ljava/security/Key;

    move-result-object v0

    .line 747
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/youtube/app/player/a/e;->a(Ljava/util/concurrent/Executor;Ljava/security/Key;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/player/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->u:Lcom/google/android/youtube/app/player/a/e;

    .line 750
    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->u:Lcom/google/android/youtube/app/player/a/e;

    return-object v0
.end method

.method public final D()Lcom/google/android/youtube/app/remote/ab;
    .registers 2

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/remote/ab;

    return-object v0
.end method

.method public final bridge synthetic E()Lcom/google/android/youtube/core/c;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    return-object v0
.end method

.method protected final a()V
    .registers 21

    .prologue
    .line 155
    new-instance v3, Lcom/google/android/youtube/core/utils/g;

    const-string v2, "connectivity"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    invoke-direct {v3, v2}, Lcom/google/android/youtube/core/utils/g;-><init>(Landroid/net/ConnectivityManager;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/core/utils/g;->e()Z

    move-result v2

    .line 157
    new-instance v3, Lcom/google/android/youtube/app/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/youtube/app/k;-><init>(Landroid/content/ContentResolver;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    .line 158
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->Z()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->k()Z

    move-result v2

    if-eqz v2, :cond_52

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/youtube/app/prefetch/PrefetchService;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/youtube/app/prefetch/PrefetchService$DeviceStateReceiver;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 165
    :cond_52
    invoke-super/range {p0 .. p0}, Lcom/google/android/youtube/core/BaseApplication;->a()V

    .line 167
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v18

    .line 168
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Z

    move-result v2

    if-eqz v2, :cond_74

    .line 169
    invoke-static/range {v18 .. v18}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "download_only_while_charging"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Z)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "transfer_max_connections"

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    .line 174
    :cond_74
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    iget-object v2, v2, Lcom/google/android/youtube/app/k;->b:Ljava/lang/String;

    .line 177
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "distributionChannel "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 178
    const-string v3, "[%s][%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    const-string v5, "-"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 179
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "platformId "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 180
    new-instance v4, Lcom/google/android/youtube/googlemobile/common/a/b;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/youtube/googlemobile/common/a/b;-><init>(Landroid/content/Context;)V

    invoke-static {v4}, Lcom/google/android/youtube/googlemobile/common/b;->a(Lcom/google/android/youtube/googlemobile/common/b;)V

    .line 181
    const-string v4, "http://www.google.com/m/appreq/mobilevideo"

    .line 182
    const-string v5, "youtube"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v3, v2}, Lcom/google/android/youtube/googlemobile/masf/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v2, Lcom/google/android/youtube/core/c/b;

    invoke-direct {v2}, Lcom/google/android/youtube/core/c/b;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->f:Lcom/google/android/youtube/core/c/a;

    .line 190
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/core/utils/Util$StartupType;

    move-result-object v2

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00cc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    const/4 v5, 0x3

    const-string v6, "FormFactor"

    const/4 v7, 0x2

    invoke-virtual {v4, v5, v6, v3, v7}, Lcom/google/android/youtube/core/Analytics;->a(ILjava/lang/String;Ljava/lang/String;I)V

    .line 195
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v3

    const-string v4, "Startup"

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/Util$StartupType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v2, "session_summary"

    const/4 v3, -0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v2, -0x1

    if-eq v3, v2, :cond_171

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-interface {v2, v0, v1}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->a(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/content/Context;)Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    move-result-object v2

    sget-object v4, Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;->Guide:Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    if-ne v2, v4, :cond_171

    const-string v2, "session_watch_count"

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    and-int/lit8 v2, v3, 0x1

    if-nez v2, :cond_4b3

    const-string v2, "Out"

    :goto_136
    and-int/lit16 v5, v3, 0x378

    if-nez v5, :cond_4bf

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    and-int/lit8 v2, v3, 0x2

    if-nez v2, :cond_4b7

    const-string v2, " NoWatch"

    :goto_149
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    and-int/lit8 v2, v3, 0x4

    if-nez v2, :cond_4bb

    const-string v2, " NoHome"

    :goto_160
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_168
    :goto_168
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v3

    const-string v5, "SessionSummary"

    invoke-virtual {v3, v5, v2, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_171
    invoke-static/range {v18 .. v18}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "session_watch_count"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    invoke-static/range {v18 .. v18}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    const-string v3, "session_summary"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/compat/ad;->a()V

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->X()Z

    move-result v2

    if-eqz v2, :cond_1a5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/e;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/google/android/youtube/app/e;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;[Ljava/io/File;)V

    invoke-virtual {v3}, Lcom/google/android/youtube/app/e;->start()V

    .line 200
    :cond_1a5
    new-instance v5, Lcom/google/android/youtube/core/utils/ad;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/google/android/youtube/core/utils/ad;-><init>(Landroid/content/ContentResolver;)V

    .line 201
    new-instance v2, Lcom/google/android/youtube/core/b/j;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    invoke-interface {v6}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->a()[B

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    invoke-interface {v7}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->b()[B

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "android_id"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/core/b/j;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/ad;[B[BLjava/lang/String;)V

    .line 208
    new-instance v10, Lcom/google/android/youtube/core/async/DeviceAuthorizer;

    move-object/from16 v0, v18

    invoke-direct {v10, v2, v0}, Lcom/google/android/youtube/core/async/DeviceAuthorizer;-><init>(Lcom/google/android/youtube/core/b/aj;Landroid/content/SharedPreferences;)V

    .line 210
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v19

    .line 211
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v2

    if-eqz v2, :cond_1ec

    .line 212
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    .line 214
    :cond_1ec
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v3, "username"

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->w:Ljava/util/concurrent/atomic/AtomicReference;

    .line 215
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    const-string v3, "user_account"

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Ljava/util/concurrent/atomic/AtomicReference;

    .line 216
    invoke-virtual/range {v19 .. v20}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bo;)V

    .line 217
    const/4 v2, -0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    .line 219
    new-instance v2, Lcom/google/android/youtube/core/b/ab;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a002d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0032

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xa

    if-le v6, v7, :cond_532

    const/4 v6, 0x1

    :goto_243
    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/youtube/core/b/ab;-><init>(IIIZZ)V

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->J()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lorg/apache/http/client/HttpClient;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-interface {v8, v0}, Lcom/google/android/youtube/core/j;->d(Landroid/content/Context;)Z

    move-result v9

    move-object v8, v2

    invoke-static/range {v3 .. v9}, Lcom/google/android/youtube/core/b/aa;->a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/youtube/core/utils/d;Lcom/google/android/youtube/core/b/ab;Z)Lcom/google/android/youtube/core/b/aa;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->l:Lcom/google/android/youtube/core/b/aa;

    .line 235
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 236
    new-instance v8, Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;-><init>(ILcom/google/android/youtube/core/utils/SafeSearch;Ljava/lang/String;)V

    .line 239
    new-instance v2, Lcom/google/android/youtube/core/b/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lorg/apache/http/client/HttpClient;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->M()Lcom/google/android/youtube/core/converter/l;

    move-result-object v7

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/core/b/k;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/d;Lcom/google/android/youtube/core/converter/l;Lcom/google/android/youtube/core/async/GDataRequestFactory;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->o()Z

    move-result v2

    if-eqz v2, :cond_535

    .line 246
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lcom/google/android/youtube/core/async/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->a()Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v2, v10, v3, v4, v0}, Lcom/google/android/youtube/core/b/k;->a(Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/GDataRequest$Version;Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    .line 252
    :goto_2c4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/b/al;)V

    .line 254
    new-instance v2, Lcom/google/android/youtube/core/b/d;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lorg/apache/http/client/HttpClient;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->M()Lcom/google/android/youtube/core/converter/l;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Lcom/google/android/youtube/core/player/ax;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/YouTubeApplication;->w:Ljava/util/concurrent/atomic/AtomicReference;

    const-string v12, "android"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->c()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->y()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->z()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v3, p0

    move-object/from16 v6, v18

    invoke-direct/range {v2 .. v17}, Lcom/google/android/youtube/core/b/d;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Landroid/content/SharedPreferences;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/converter/l;Lcom/google/android/youtube/core/player/ax;Ljava/util/concurrent/atomic/AtomicReference;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->i:Lcom/google/android/youtube/core/b/d;

    .line 271
    new-instance v2, Lcom/google/android/youtube/core/b/ae;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->M()Lcom/google/android/youtube/core/converter/l;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/youtube/core/b/ae;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/converter/l;Ljava/lang/String;Lcom/google/android/youtube/core/utils/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->j:Lcom/google/android/youtube/core/b/ae;

    .line 278
    new-instance v2, Lcom/google/android/youtube/core/b/ac;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->f:Lcom/google/android/youtube/core/c/a;

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/core/utils/Util;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/youtube/core/b/ac;-><init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/youtube/core/utils/d;Lcom/google/android/youtube/core/c/a;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->k:Lcom/google/android/youtube/core/b/ac;

    .line 285
    new-instance v2, Lcom/google/android/youtube/app/b/c;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    invoke-interface {v5}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->c()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    invoke-interface {v6}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->H()Ljava/util/concurrent/Executor;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v9

    move-object/from16 v10, v19

    invoke-direct/range {v2 .. v10}, Lcom/google/android/youtube/app/b/c;-><init>(Ljava/lang/String;Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;Landroid/accounts/AccountManager;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/app/b/g;

    .line 295
    new-instance v2, Lcom/google/android/youtube/app/g;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/youtube/app/g;-><init>(Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/app/g;

    .line 298
    new-instance v2, Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/k;->m()Z

    move-result v6

    if-eqz v6, :cond_54b

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-interface {v6, v0, v1}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->a(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/content/Context;)Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    move-result-object v6

    sget-object v7, Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;->OriginalTablet:Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    if-eq v6, v7, :cond_54b

    const/4 v8, 0x1

    :goto_3c5
    move-object/from16 v6, v18

    move-object/from16 v7, v19

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/remote/bb;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/l;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    .line 308
    new-instance v2, Lcom/google/android/youtube/app/remote/v;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    move-object/from16 v0, v18

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/youtube/app/remote/v;-><init>(Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Landroid/content/res/Resources;Lcom/google/android/youtube/app/remote/bb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bl;

    .line 310
    new-instance v2, Lcom/google/android/youtube/app/remote/bm;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bl;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v5}, Lcom/google/android/youtube/app/k;->l()Z

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/youtube/app/remote/bm;-><init>(Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/youtube/core/utils/l;ZLcom/google/android/youtube/app/remote/bb;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->p:Lcom/google/android/youtube/app/remote/bm;

    .line 313
    new-instance v2, Lcom/google/android/youtube/app/remote/AtHomeConnection;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/remote/AtHomeConnection;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    .line 314
    new-instance v2, Lcom/google/android/youtube/app/remote/e;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/app/remote/e;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/AtHomeConnection;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/app/remote/e;

    .line 316
    new-instance v2, Landroid/provider/SearchRecentSuggestions;

    const-string v3, "com.google.android.youtube.SuggestionProvider"

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->c:Landroid/provider/SearchRecentSuggestions;

    .line 320
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->aa()Z

    move-result v2

    if-eqz v2, :cond_43d

    .line 321
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_54e

    .line 322
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/autosync/a;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 328
    :cond_43d
    :goto_43d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->k()Z

    move-result v2

    if-eqz v2, :cond_461

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_461

    .line 329
    invoke-static/range {v18 .. v18}, Lcom/google/android/youtube/core/utils/f;->a(Landroid/content/SharedPreferences;)Ljava/security/Key;

    move-result-object v2

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/google/android/youtube/app/player/a/e;->a(Ljava/util/concurrent/Executor;Ljava/security/Key;Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/player/a/e;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->u:Lcom/google/android/youtube/app/player/a/e;

    .line 335
    :cond_461
    new-instance v2, Lcom/google/android/youtube/app/remote/ab;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/app/remote/e;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->p:Lcom/google/android/youtube/app/remote/bm;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/remote/ab;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/AtHomeConnection;Lcom/google/android/youtube/app/remote/e;Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bb;Lcom/google/android/youtube/core/Analytics;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/remote/ab;

    .line 338
    new-instance v2, Lcom/google/android/youtube/app/remote/aq;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->v:Lcom/google/android/youtube/app/remote/ab;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/YouTubeApplication;->l:Lcom/google/android/youtube/core/b/aa;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->b(Landroid/content/Context;)Lcom/google/android/youtube/app/remote/ao;

    move-result-object v8

    move-object/from16 v7, v19

    invoke-direct/range {v2 .. v8}, Lcom/google/android/youtube/app/remote/aq;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/ab;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/remote/ao;)V

    .line 346
    new-instance v2, Lcom/google/android/youtube/app/a/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/prefetch/d;

    move-object/from16 v0, v18

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/app/a/a;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/youtube/app/prefetch/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->t:Lcom/google/android/youtube/app/a/a;

    .line 347
    return-void

    .line 196
    :cond_4b3
    const-string v2, "In"

    goto/16 :goto_136

    :cond_4b7
    const-string v2, " ShowWatch"

    goto/16 :goto_149

    :cond_4bb
    const-string v2, " ShowHome"

    goto/16 :goto_160

    :cond_4bf
    and-int/lit16 v5, v3, 0x370

    if-nez v5, :cond_4d8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GuideExpanded"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_168

    :cond_4d8
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " GuideSelected"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    and-int/lit8 v5, v3, 0x20

    if-eqz v5, :cond_502

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Account"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_502
    and-int/lit8 v5, v3, 0x10

    if-eqz v5, :cond_519

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Channel"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_519
    and-int/lit16 v3, v3, 0x340

    if-eqz v3, :cond_168

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Browse"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_168

    .line 219
    :cond_532
    const/4 v6, 0x0

    goto/16 :goto_243

    .line 249
    :cond_535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/YouTubeApplication;->P()Lcom/google/android/youtube/core/async/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->a()Lcom/google/android/youtube/core/async/GDataRequest$Version;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v10, v3, v4, v5}, Lcom/google/android/youtube/core/b/k;->a(Lcom/google/android/youtube/core/async/DeviceAuthorizer;Lcom/google/android/youtube/core/async/a;Lcom/google/android/youtube/core/async/GDataRequest$Version;Lcom/google/android/youtube/core/async/UserAuthorizer;)V

    goto/16 :goto_2c4

    .line 298
    :cond_54b
    const/4 v8, 0x0

    goto/16 :goto_3c5

    .line 324
    :cond_54e
    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/autosync/a;->b(Landroid/content/Context;)V

    goto/16 :goto_43d
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 391
    const-string v1, "session_summary"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 392
    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v2, "session_summary"

    or-int/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    .line 395
    return-void
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 644
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 645
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->w:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserAuth;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 646
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 647
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    .line 648
    invoke-direct {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->aa()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 649
    invoke-static {p0}, Lcom/google/android/youtube/app/autosync/a;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 651
    :cond_1e
    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    .line 652
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    .line 653
    return-void
.end method

.method protected final b()Lcom/google/android/youtube/core/player/ax;
    .registers 5

    .prologue
    .line 358
    new-instance v0, Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/prefetch/d;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/d;Lcom/google/android/youtube/app/k;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/prefetch/d;

    .line 359
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/prefetch/d;->a()V

    .line 360
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/prefetch/d;->d()V

    .line 362
    invoke-super {p0}, Lcom/google/android/youtube/core/BaseApplication;->b()Lcom/google/android/youtube/core/player/ax;

    move-result-object v0

    .line 363
    new-instance v1, Lcom/google/android/youtube/app/prefetch/j;

    iget-object v2, p0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/prefetch/d;

    invoke-direct {v1, v2, v0}, Lcom/google/android/youtube/app/prefetch/j;-><init>(Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/player/ax;)V

    return-object v1
.end method

.method public final b(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x2

    .line 676
    iput p1, p0, Lcom/google/android/youtube/app/YouTubeApplication;->y:I

    .line 678
    const/4 v0, -0x2

    if-ne p1, v0, :cond_12

    .line 679
    const-string v0, "Signed out"

    .line 689
    :goto_8
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "Engagement"

    invoke-virtual {v1, v3, v2, v0, v3}, Lcom/google/android/youtube/core/Analytics;->a(ILjava/lang/String;Ljava/lang/String;I)V

    .line 691
    return-void

    .line 680
    :cond_12
    const/4 v0, -0x1

    if-ne p1, v0, :cond_18

    .line 681
    const-string v0, "Signed in"

    goto :goto_8

    .line 682
    :cond_18
    if-nez p1, :cond_1d

    .line 683
    const-string v0, "No subscriptions"

    goto :goto_8

    .line 684
    :cond_1d
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->k()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 685
    :cond_2b
    const-string v0, "Subscriptions"

    goto :goto_8

    .line 687
    :cond_2e
    const-string v0, "Prefetching"

    goto :goto_8
.end method

.method public final c()Ljava/lang/String;
    .registers 4

    .prologue
    .line 368
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "youtube_client_id"

    const-string v2, "android-google"

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/e;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/youtube/app/k;
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 378
    const-string v0, "YouTube"

    return-object v0
.end method

.method public final f()V
    .registers 4

    .prologue
    .line 382
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 383
    const-string v1, "session_watch_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 384
    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v2, "session_watch_count"

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    .line 387
    return-void
.end method

.method public final g()Landroid/provider/SearchRecentSuggestions;
    .registers 2

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->c:Landroid/provider/SearchRecentSuggestions;

    return-object v0
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->d:Lcom/google/android/youtube/core/suggest/a;

    if-nez v0, :cond_b

    .line 507
    new-instance v0, Lcom/google/android/youtube/core/suggest/a;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/suggest/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->d:Lcom/google/android/youtube/core/suggest/a;

    .line 509
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->d:Lcom/google/android/youtube/core/suggest/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/suggest/a;->b()V

    .line 510
    return-void
.end method

.method public final i()Lcom/google/android/youtube/app/g;
    .registers 2

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->g:Lcom/google/android/youtube/app/g;

    return-object v0
.end method

.method public final j()Lcom/google/android/youtube/core/b/al;
    .registers 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->h:Lcom/google/android/youtube/core/b/k;

    return-object v0
.end method

.method public final k()Lcom/google/android/youtube/core/b/a;
    .registers 2

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->i:Lcom/google/android/youtube/core/b/d;

    return-object v0
.end method

.method public final l()Lcom/google/android/youtube/core/b/au;
    .registers 11

    .prologue
    .line 527
    new-instance v0, Lcom/google/android/youtube/app/a/c;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->L()Lorg/apache/http/client/HttpClient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->G()Lcom/google/android/youtube/core/utils/aa;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/youtube/app/YouTubeApplication;->t:Lcom/google/android/youtube/app/a/a;

    iget-object v5, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v5}, Lcom/google/android/youtube/app/k;->x()I

    move-result v8

    iget-object v5, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v5}, Lcom/google/android/youtube/app/k;->D()Z

    move-result v9

    move-object v5, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/app/a/c;-><init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/d;Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a/a;IZ)V

    .line 530
    invoke-static {}, Lcom/google/android/youtube/app/m;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/b/au;->a(Ljava/util/List;)V

    .line 531
    return-object v0
.end method

.method public final m()Lcom/google/android/youtube/core/b/as;
    .registers 2

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->j:Lcom/google/android/youtube/core/b/ae;

    return-object v0
.end method

.method public final n()Lcom/google/android/youtube/core/b/ap;
    .registers 2

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->k:Lcom/google/android/youtube/core/b/ac;

    return-object v0
.end method

.method public final o()Lcom/google/android/youtube/app/b/g;
    .registers 2

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->m:Lcom/google/android/youtube/app/b/g;

    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 706
    const-string v0, "prefetch_subscriptions"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "prefetch_watch_later"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 708
    :cond_10
    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    .line 710
    :cond_13
    return-void
.end method

.method public onTerminate()V
    .registers 1

    .prologue
    .line 484
    invoke-static {}, Lcom/google/android/youtube/googlemobile/masf/f;->a()V

    .line 487
    invoke-super {p0}, Lcom/google/android/youtube/core/BaseApplication;->onTerminate()V

    .line 488
    return-void
.end method

.method public final p()Lcom/google/android/youtube/core/b/an;
    .registers 2

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->l:Lcom/google/android/youtube/core/b/aa;

    return-object v0
.end method

.method public final q()Lcom/google/android/youtube/app/remote/bb;
    .registers 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    return-object v0
.end method

.method public final r()Lcom/google/android/youtube/app/remote/bm;
    .registers 2

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->p:Lcom/google/android/youtube/app/remote/bm;

    return-object v0
.end method

.method public final s()Lcom/google/android/youtube/app/remote/bl;
    .registers 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->o:Lcom/google/android/youtube/app/remote/bl;

    return-object v0
.end method

.method public final t()Lcom/google/android/youtube/app/remote/e;
    .registers 2

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/app/remote/e;

    return-object v0
.end method

.method public final u()Lcom/google/android/youtube/app/remote/AtHomeConnection;
    .registers 2

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->r:Lcom/google/android/youtube/app/remote/AtHomeConnection;

    return-object v0
.end method

.method public final v()Lcom/google/android/youtube/app/remote/RemoteControl;
    .registers 3

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_d

    .line 576
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->n:Lcom/google/android/youtube/app/remote/bb;

    .line 580
    :goto_c
    return-object v0

    .line 577
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/app/remote/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/e;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1a

    .line 578
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->q:Lcom/google/android/youtube/app/remote/e;

    goto :goto_c

    .line 580
    :cond_1a
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final w()Lcom/google/android/youtube/app/prefetch/d;
    .registers 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->s:Lcom/google/android/youtube/app/prefetch/d;

    return-object v0
.end method

.method public final x()Lcom/google/android/youtube/app/YouTubePlatformUtil;
    .registers 2

    .prologue
    .line 588
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->e:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .registers 5

    .prologue
    .line 600
    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory;->s:Ljava/util/Set;

    .line 601
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 602
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 603
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 640
    :cond_1e
    :goto_1e
    return-object v0

    :cond_1f
    move-object v1, v0

    .line 609
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 611
    if-eqz v0, :cond_40

    .line 612
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v1

    .line 615
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_40

    .line 616
    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 617
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    move-object v0, v1

    .line 618
    goto :goto_1e

    .line 624
    :cond_40
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 625
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->b:Lcom/google/android/youtube/app/k;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->A()Ljava/lang/String;

    move-result-object v0

    .line 626
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5c

    .line 627
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 628
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 635
    :cond_5c
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 636
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 640
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public final z()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 656
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 657
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->w:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 658
    iget-object v0, p0, Lcom/google/android/youtube/app/YouTubeApplication;->x:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 659
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    .line 660
    invoke-direct {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->aa()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 661
    invoke-static {p0}, Lcom/google/android/youtube/app/autosync/a;->b(Landroid/content/Context;)V

    .line 663
    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/youtube/app/YouTubeApplication;->I()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/f;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/f;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 672
    invoke-static {p0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->c(Landroid/content/Context;)V

    .line 673
    return-void
.end method
