.class public final Lcom/google/android/youtube/core/converter/http/cp;
.super Lcom/google/android/youtube/core/converter/http/ax;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/youtube/core/converter/c;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/converter/l;)V
    .registers 7
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/converter/http/ax;-><init>(Lcom/google/android/youtube/core/converter/l;)V

    .line 27
    new-instance v0, Lcom/google/android/youtube/core/converter/d;

    invoke-direct {v0}, Lcom/google/android/youtube/core/converter/d;-><init>()V

    .line 28
    const-string v1, "/feed/entry"

    .line 29
    const-string v2, "/feed"

    new-instance v3, Lcom/google/android/youtube/core/converter/http/cu;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/cu;-><init>(Lcom/google/android/youtube/core/converter/http/cp;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/converter/http/ct;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/ct;-><init>(Lcom/google/android/youtube/core/converter/http/cp;)V

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/category"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/converter/http/cs;

    invoke-direct {v4, p0}, Lcom/google/android/youtube/core/converter/http/cs;-><init>(Lcom/google/android/youtube/core/converter/http/cp;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/yt:connectedAccount"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/core/converter/http/cr;

    invoke-direct {v4, p0}, Lcom/google/android/youtube/core/converter/http/cr;-><init>(Lcom/google/android/youtube/core/converter/http/cp;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/yt:connectedAccount/yt:accessControl"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/youtube/core/converter/http/cq;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/core/converter/http/cq;-><init>(Lcom/google/android/youtube/core/converter/http/cp;)V

    invoke-virtual {v2, v1, v3}, Lcom/google/android/youtube/core/converter/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/converter/p;)Lcom/google/android/youtube/core/converter/d;

    .line 67
    invoke-virtual {v0}, Lcom/google/android/youtube/core/converter/d;->a()Lcom/google/android/youtube/core/converter/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/converter/http/cp;->b:Lcom/google/android/youtube/core/converter/c;

    .line 68
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/youtube/core/converter/c;
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/core/converter/http/cp;->b:Lcom/google/android/youtube/core/converter/c;

    return-object v0
.end method
