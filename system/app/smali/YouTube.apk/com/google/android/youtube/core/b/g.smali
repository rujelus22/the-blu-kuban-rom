.class final Lcom/google/android/youtube/core/b/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/b/d;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/youtube/core/model/VastAd;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/b/d;Ljava/lang/String;Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    iput-object p2, p0, Lcom/google/android/youtube/core/b/g;->b:Ljava/lang/String;

    .line 263
    iput-object p3, p0, Lcom/google/android/youtube/core/b/g;->c:Lcom/google/android/youtube/core/model/VastAd;

    .line 264
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 256
    const-string v0, "Error retrieving video for the ad"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/d;->b(Lcom/google/android/youtube/core/b/d;)Lcom/google/android/youtube/core/async/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/b/g;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 256
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    if-eqz p2, :cond_8b

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v1, :cond_8b

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->privacy:Lcom/google/android/youtube/core/model/Video$Privacy;

    sget-object v1, Lcom/google/android/youtube/core/model/Video$Privacy;->PUBLIC:Lcom/google/android/youtube/core/model/Video$Privacy;

    if-eq v0, v1, :cond_5b

    iget-object v0, p0, Lcom/google/android/youtube/core/b/g;->c:Lcom/google/android/youtube/core/model/VastAd;

    move-object v1, v0

    :goto_13
    :try_start_13
    iget-object v0, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/d;->d(Lcom/google/android/youtube/core/b/d;)Lcom/google/android/youtube/core/player/ax;

    move-result-object v0

    sget-object v2, Lcom/google/android/youtube/core/player/ae;->a:Ljava/util/Set;

    invoke-interface {v0, p2, v2}, Lcom/google/android/youtube/core/player/ax;->a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;)Lcom/google/android/youtube/core/model/v;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/d;->e(Lcom/google/android/youtube/core/b/d;)Z

    move-result v2

    if-eqz v2, :cond_79

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->b:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    :goto_2b
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "splay"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "dnc"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/d;->b(Lcom/google/android/youtube/core/b/d;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/b/g;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/VastAd;->buildUpon()Lcom/google/android/youtube/core/model/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/q;->q(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/q;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_5a
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_13 .. :try_end_5a} :catch_7e

    :goto_5a
    return-void

    :cond_5b
    iget-object v0, p0, Lcom/google/android/youtube/core/b/g;->c:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->buildUpon()Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->c(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    iget v1, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->a(I)Lcom/google/android/youtube/core/model/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/q;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    move-object v1, v0

    goto :goto_13

    :cond_79
    :try_start_79
    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->c:Lcom/google/android/youtube/core/model/Stream;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;
    :try_end_7d
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_79 .. :try_end_7d} :catch_7e

    goto :goto_2b

    :catch_7e
    move-exception v0

    iget-object v1, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/d;->b(Lcom/google/android/youtube/core/b/d;)Lcom/google/android/youtube/core/async/l;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/b/g;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_5a

    :cond_8b
    iget-object v0, p0, Lcom/google/android/youtube/core/b/g;->a:Lcom/google/android/youtube/core/b/d;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/d;->b(Lcom/google/android/youtube/core/b/d;)Lcom/google/android/youtube/core/async/l;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/b/g;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_5a
.end method
