.class public Lcom/google/android/youtube/core/player/DefaultControllerOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/youtube/core/player/ControllerOverlay;
.implements Lcom/google/android/youtube/core/player/bf;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

.field private I:Z

.field private J:Z

.field protected final a:Landroid/view/animation/Animation;

.field private b:Lcom/google/android/youtube/core/player/k;

.field private final c:Lcom/google/android/youtube/core/Analytics;

.field private final d:F

.field private final e:Landroid/view/View;

.field private final f:Lcom/google/android/youtube/core/player/TimeBar;

.field private final g:Landroid/widget/ImageButton;

.field private final h:Landroid/widget/ImageButton;

.field private final i:Landroid/widget/ImageButton;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/ImageView;

.field private final l:Lcom/google/android/youtube/core/ui/o;

.field private final m:Landroid/widget/LinearLayout;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/ImageView;

.field private final p:Lcom/google/android/youtube/core/player/ao;

.field private final q:Landroid/os/Handler;

.field private final r:I

.field private final s:I

.field private t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x2

    const v8, 0x7f040084

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 131
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 132
    const-string v0, "analytics cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c:Lcom/google/android/youtube/core/Analytics;

    .line 134
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 136
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 137
    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d:F

    .line 139
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 140
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 141
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 143
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    .line 144
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    const v4, 0x7f020052

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    new-instance v0, Lcom/google/android/youtube/core/player/TimeBar;

    invoke-direct {v0, p1, p0}, Lcom/google/android/youtube/core/player/TimeBar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/bf;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    .line 151
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 153
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 154
    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 155
    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 158
    invoke-virtual {v3, v8, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    const v4, 0x7f020195

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    const v4, 0x7f0b0082

    invoke-virtual {p1, v4}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 164
    invoke-virtual {v3, v8, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    const v4, 0x7f020190

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    const v4, 0x7f0b007e

    invoke-virtual {p1, v4}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_230

    .line 172
    invoke-virtual {v3, v8, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    const v3, 0x7f020193

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    const v3, 0x7f0b007c

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 177
    iput-boolean v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->B:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    :goto_f6
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    const v3, 0x7f020129

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 185
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 187
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 188
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    const v3, 0x7f0b007f

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 192
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    .line 193
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 194
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    const/high16 v3, -0x3400

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 198
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    const v2, 0x7f020024

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 200
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    const v2, 0x7f0b0084

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d:F

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(IF)I

    move-result v2

    const/16 v3, 0xc

    iget v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d:F

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(IF)I

    move-result v3

    invoke-virtual {v0, v6, v2, v3, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 210
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    const v2, 0x7f020020

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 217
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    const v2, 0x7f0b0085

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d:F

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(IF)I

    move-result v2

    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d:F

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(IF)I

    move-result v3

    invoke-virtual {v0, v2, v3, v6, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 224
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->q:Landroid/os/Handler;

    .line 228
    new-instance v0, Lcom/google/android/youtube/core/player/ao;

    new-instance v1, Lcom/google/android/youtube/core/player/m;

    invoke-direct {v1, p0, v6}, Lcom/google/android/youtube/core/player/m;-><init>(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;B)V

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/ao;-><init>(Lcom/google/android/youtube/core/player/ap;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->p:Lcom/google/android/youtube/core/player/ao;

    .line 230
    const v0, 0x7f050002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 232
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->r:I

    .line 233
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->s:I

    .line 235
    sget-object v0, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    .line 237
    iput-boolean v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->F:Z

    .line 238
    iput-boolean v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->G:Z

    .line 240
    new-instance v0, Lcom/google/android/youtube/core/ui/o;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/core/ui/o;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l:Lcom/google/android/youtube/core/ui/o;

    .line 242
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setClipToPadding(Z)V

    .line 243
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h()V

    .line 244
    return-void

    .line 180
    :cond_230
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    goto/16 :goto_f6
.end method

.method private static a(IF)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 846
    int-to-float v0, p0

    mul-float/2addr v0, p1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static a(Landroid/view/View;II)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 741
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_18

    .line 742
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 743
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, p2, v1

    add-int v2, p1, v0

    invoke-virtual {p0, p1, v1, v2, p2}, Landroid/view/View;->layout(IIII)V

    .line 746
    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 490
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_b

    .line 491
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 493
    :cond_b
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 821
    if-eqz p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 822
    return-void

    .line 821
    :cond_7
    const/16 v0, 0x8

    goto :goto_3
.end method

.method private a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 473
    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a:Landroid/view/animation/Animation;

    if-eqz p1, :cond_3d

    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->r:I

    int-to-long v0, v0

    :goto_7
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 475
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v0, :cond_13

    .line 476
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 478
    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 479
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 480
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 481
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_2d

    .line 482
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 484
    :cond_2d
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 486
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;)V

    .line 487
    return-void

    .line 473
    :cond_3d
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->s:I

    int-to-long v0, v0

    goto :goto_7
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/TimeBar;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    return-object v0
.end method

.method private static b(Landroid/view/View;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 750
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 751
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 752
    div-int/lit8 v2, v0, 0x2

    sub-int v2, p1, v2

    div-int/lit8 v3, v1, 0x2

    sub-int v3, p2, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    invoke-virtual {p0, v2, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 753
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Z
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->A:Z

    return v0
.end method

.method private e()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 396
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    if-nez v0, :cond_29

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->minimalWhenNotFullscreen:Z

    if-eqz v0, :cond_29

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    .line 397
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-eqz v0, :cond_2b

    .line 398
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/google/android/youtube/core/player/TimeBar;->setPadding(IIII)V

    .line 399
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/TimeBar;->setShowTimes(Z)V

    .line 400
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/player/TimeBar;->setShowScrubber(Z)V

    .line 409
    :goto_22
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 410
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k()V

    .line 411
    return-void

    :cond_29
    move v0, v2

    .line 396
    goto :goto_d

    .line 404
    :cond_2b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->c()I

    move-result v0

    const/16 v3, 0x18

    iget v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d:F

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(IF)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 405
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v3, v0, v2, v0, v2}, Lcom/google/android/youtube/core/player/TimeBar;->setPadding(IIII)V

    .line 406
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setShowTimes(Z)V

    .line 407
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setShowScrubber(Z)V

    goto :goto_22
.end method

.method private k()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 455
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_24

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->q:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_24

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->F:Z

    if-eqz v0, :cond_24

    .line 457
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->q:Landroid/os/Handler;

    const-wide/16 v1, 0x9c4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 459
    :cond_24
    return-void
.end method

.method private l()V
    .registers 3

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->q:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 497
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 498
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->clearAnimation()V

    .line 499
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 500
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 501
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_25

    .line 502
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    .line 504
    :cond_25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 505
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 506
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 507
    return-void
.end method

.method private m()V
    .registers 2

    .prologue
    .line 617
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->C:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_12

    .line 618
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h()V

    .line 619
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->i()V

    .line 621
    :cond_12
    return-void
.end method

.method private n()V
    .registers 2

    .prologue
    .line 624
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->D:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v0, :cond_12

    .line 625
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h()V

    .line 626
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->m()V

    .line 628
    :cond_12
    return-void
.end method

.method private o()V
    .registers 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 756
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->q:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 757
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_66

    .line 758
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    const v1, 0x7f020129

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 759
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b007f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 769
    :goto_26
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->isError()Z

    move-result v0

    if-eqz v0, :cond_9e

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    .line 773
    :goto_30
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    if-nez v1, :cond_3c

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_115

    :cond_3c
    move v1, v2

    .line 775
    :goto_3d
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_ac

    .line 776
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 777
    if-ne v5, v0, :cond_57

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v6, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-eq v3, v6, :cond_5f

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->isError()Z

    move-result v3

    if-nez v3, :cond_5f

    :cond_57
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    if-ne v5, v3, :cond_aa

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-eqz v3, :cond_aa

    :cond_5f
    move v3, v4

    :goto_60
    invoke-static {v5, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 775
    add-int/lit8 v1, v1, 0x1

    goto :goto_3d

    .line 760
    :cond_66
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_85

    .line 761
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    const v1, 0x7f020127

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 762
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b0080

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_26

    .line 764
    :cond_85
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    const v1, 0x7f020132

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 765
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b0081

    invoke-virtual {v1, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_26

    .line 769
    :cond_9e
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_a7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    goto :goto_30

    :cond_a7
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    goto :goto_30

    :cond_aa
    move v3, v2

    .line 777
    goto :goto_60

    .line 781
    :cond_ac
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v1, :cond_be

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v3, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-eq v1, v3, :cond_be

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_113

    :cond_be
    move v1, v4

    :goto_bf
    invoke-static {p0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 802
    :goto_c2
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    if-eqz v1, :cond_ce

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->isError()Z

    move-result v1

    if-eqz v1, :cond_1af

    :cond_ce
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsNextPrevious:Z

    if-eqz v1, :cond_1af

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->C:Z

    if-nez v1, :cond_dc

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->D:Z

    if-eqz v1, :cond_1af

    :cond_dc
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v1, :cond_1af

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    if-ne v0, v1, :cond_1af

    .line 805
    :goto_e4
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 806
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 807
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    if-eqz v4, :cond_1b2

    const v0, 0x7f020022

    :goto_f5
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 812
    if-eqz v4, :cond_112

    .line 813
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->C:Z

    if-eqz v0, :cond_1b7

    const v0, 0x7f020123

    :goto_103
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 815
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->D:Z

    if-eqz v0, :cond_1bc

    const v0, 0x7f02012e

    :goto_10f
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 818
    :cond_112
    return-void

    :cond_113
    move v1, v2

    .line 781
    goto :goto_bf

    .line 783
    :cond_115
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    invoke-static {v1, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 784
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsTimeBar:Z

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 785
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    if-nez v1, :cond_131

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->showButtonsWhenNotFullscreen:Z

    if-nez v1, :cond_131

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->J:Z

    if-eqz v1, :cond_1a1

    :cond_131
    move v1, v4

    .line 787
    :goto_132
    iget-object v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v3, :cond_1a3

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_1a3

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->E:Z

    if-eqz v3, :cond_1a3

    if-eqz v1, :cond_1a3

    move v3, v4

    :goto_145
    invoke-static {v5, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 789
    iget-object v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v3, :cond_1a5

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_1a5

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->A:Z

    if-eqz v3, :cond_1a5

    if-eqz v1, :cond_1a5

    move v3, v4

    :goto_15b
    invoke-static {v5, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 790
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v3

    if-nez v3, :cond_178

    .line 791
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v5, :cond_1a7

    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->B:Z

    if-eqz v5, :cond_1a7

    if-nez v1, :cond_174

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->I:Z

    if-eqz v1, :cond_1a7

    :cond_174
    move v1, v4

    :goto_175
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 795
    :cond_178
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    if-ne v0, v1, :cond_1a9

    move v1, v4

    :goto_17f
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 796
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    if-ne v0, v1, :cond_1ab

    move v1, v4

    :goto_189
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 797
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    if-ne v0, v1, :cond_1ad

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v1, v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v1, :cond_1ad

    move v1, v4

    :goto_199
    invoke-static {v3, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;Z)V

    .line 799
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setVisibility(I)V

    goto/16 :goto_c2

    :cond_1a1
    move v1, v2

    .line 785
    goto :goto_132

    :cond_1a3
    move v3, v2

    .line 787
    goto :goto_145

    :cond_1a5
    move v3, v2

    .line 789
    goto :goto_15b

    :cond_1a7
    move v1, v2

    .line 791
    goto :goto_175

    :cond_1a9
    move v1, v2

    .line 795
    goto :goto_17f

    :cond_1ab
    move v1, v2

    .line 796
    goto :goto_189

    :cond_1ad
    move v1, v2

    .line 797
    goto :goto_199

    :cond_1af
    move v4, v2

    .line 802
    goto/16 :goto_e4

    .line 807
    :cond_1b2
    const v0, 0x7f02018f

    goto/16 :goto_f5

    .line 813
    :cond_1b7
    const v0, 0x7f020124

    goto/16 :goto_103

    .line 815
    :cond_1bc
    const v0, 0x7f02012f

    goto/16 :goto_10f
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 247
    return-object p0
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setScrubbing(Z)V

    .line 833
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/k;->a(I)V

    .line 834
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 315
    if-eqz p2, :cond_49

    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    :goto_4
    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 317
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 318
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    :goto_1b
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p2, :cond_58

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\n\n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_37
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 324
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 326
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    .line 327
    return-void

    .line 315
    :cond_49
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    goto :goto_4

    .line 320
    :cond_4c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0020

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 322
    :cond_58
    const-string v0, ""

    goto :goto_37
.end method

.method public final a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 850
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l:Lcom/google/android/youtube/core/ui/o;

    new-instance v1, Lcom/google/android/youtube/core/player/l;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/l;-><init>(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/ui/o;->a(Ljava/util/List;Lcom/google/android/youtube/core/ui/q;)V

    .line 855
    return-void
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 251
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_7

    .line 301
    :goto_6
    return-void

    .line 297
    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 298
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 300
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    goto :goto_6
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->b()I

    move-result v0

    return v0
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 443
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l()V

    .line 444
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    if-eqz v0, :cond_16

    .line 445
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    .line 446
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 447
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    if-eqz v0, :cond_16

    .line 448
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->q()V

    .line 451
    :cond_16
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k()V

    .line 452
    return-void
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_7

    .line 312
    :goto_6
    return-void

    .line 307
    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 308
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setBufferedPercent(I)V

    .line 309
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 311
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    goto :goto_6
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l()V

    .line 433
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    if-nez v0, :cond_16

    .line 434
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    .line 435
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    if-eqz v0, :cond_13

    .line 436
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->g()V

    .line 438
    :cond_13
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 440
    :cond_16
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 462
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_a

    .line 463
    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Z)V

    .line 469
    :goto_9
    return v0

    .line 465
    :cond_a
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_13

    .line 466
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    goto :goto_9

    :cond_13
    move v0, v1

    .line 469
    goto :goto_9
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->a()V

    .line 373
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l:Lcom/google/android/youtube/core/ui/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/o;->a()V

    .line 859
    return-void
.end method

.method public final n_()V
    .registers 2

    .prologue
    .line 827
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l()V

    .line 828
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->p()V

    .line 829
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 2
    .parameter

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h()V

    .line 519
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2
    .parameter

    .prologue
    .line 515
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 2
    .parameter

    .prologue
    .line 511
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    if-eqz v0, :cond_d

    .line 528
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_e

    .line 529
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->h()V

    .line 551
    :cond_d
    :goto_d
    return-void

    .line 530
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_18

    .line 531
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->f()V

    goto :goto_d

    .line 532
    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_51

    .line 533
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Fullscreen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Button"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-nez v0, :cond_4c

    const-string v0, "On"

    :goto_31
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    if-nez v0, :cond_4f

    const/4 v0, 0x1

    :goto_41
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/k;->c(Z)V

    goto :goto_d

    .line 533
    :cond_4c
    const-string v0, "Off"

    goto :goto_31

    .line 535
    :cond_4f
    const/4 v0, 0x0

    goto :goto_41

    .line 536
    :cond_51
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_59

    .line 537
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m()V

    goto :goto_d

    .line 538
    :cond_59
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_61

    .line 539
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n()V

    goto :goto_d

    .line 540
    :cond_61
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_d

    .line 541
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_78

    .line 542
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Replay"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->n()V

    goto :goto_d

    .line 544
    :cond_78
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_84

    .line 545
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->j()V

    goto :goto_d

    .line 546
    :cond_84
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_d

    .line 547
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->k()V

    goto/16 :goto_d
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 591
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_37

    const/16 v2, 0x5a

    if-eq p1, v2, :cond_34

    const/16 v2, 0x57

    if-eq p1, v2, :cond_34

    const/16 v2, 0x55

    if-eq p1, v2, :cond_34

    const/16 v2, 0x58

    if-eq p1, v2, :cond_34

    const/16 v2, 0x59

    if-eq p1, v2, :cond_34

    const/16 v2, 0x56

    if-eq p1, v2, :cond_34

    const/16 v2, 0x7e

    if-eq p1, v2, :cond_34

    const/16 v2, 0x7f

    if-eq p1, v2, :cond_34

    const/16 v2, 0x82

    if-eq p1, v2, :cond_34

    const/16 v2, 0x4f

    if-eq p1, v2, :cond_34

    const/16 v2, 0xaf

    if-ne p1, v2, :cond_5e

    :cond_34
    move v2, v0

    :goto_35
    if-eqz v2, :cond_60

    :cond_37
    move v2, v0

    .line 592
    :goto_38
    if-eqz v2, :cond_3d

    .line 593
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    .line 595
    :cond_3d
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v4, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v3, v4, :cond_64

    if-eqz v2, :cond_64

    const/16 v2, 0x14

    if-eq p1, v2, :cond_55

    const/16 v2, 0x15

    if-eq p1, v2, :cond_55

    const/16 v2, 0x16

    if-eq p1, v2, :cond_55

    const/16 v2, 0x13

    if-ne p1, v2, :cond_62

    :cond_55
    move v2, v0

    :goto_56
    if-nez v2, :cond_64

    .line 597
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/k;->o()V

    .line 600
    :cond_5d
    :goto_5d
    return v0

    :cond_5e
    move v2, v1

    .line 591
    goto :goto_35

    :cond_60
    move v2, v1

    goto :goto_38

    :cond_62
    move v2, v1

    .line 595
    goto :goto_56

    .line 600
    :cond_64
    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->p:Lcom/google/android/youtube/core/player/ao;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/youtube/core/player/ao;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_5d

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_5d

    move v0, v1

    goto :goto_5d
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 606
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->p:Lcom/google/android/youtube/core/player/ao;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method protected onLayout(ZIIII)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 690
    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 691
    sub-int v1, p4, p2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 693
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    .line 694
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    .line 695
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    .line 697
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v2, v3

    .line 698
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v2, v3

    .line 699
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    move-result v3

    add-int/2addr v2, v3

    .line 701
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v3

    if-nez v3, :cond_4c

    .line 702
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsTimeBar:Z

    if-eqz v3, :cond_9b

    .line 703
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    .line 710
    :cond_4c
    :goto_4c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    const/4 v3, 0x0

    sub-int v4, p5, p3

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 712
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v0

    div-int/lit8 v2, v1, 0x2

    add-int/2addr v0, v2

    .line 716
    sub-int v2, p5, p3

    div-int/lit8 v2, v2, 0x2

    .line 718
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-static {v3, v0, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    .line 719
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-static {v3, v0, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    .line 721
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->isError()Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 722
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v0, v3, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    .line 724
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v3

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Landroid/view/View;II)V

    .line 738
    :goto_9a
    return-void

    .line 705
    :cond_9b
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingRight()I

    move-result v2

    sub-int v2, p4, v2

    .line 706
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-static {v3, v2, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Landroid/view/View;II)I

    goto :goto_4c

    .line 727
    :cond_ae
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 732
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_9a
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x8

    const/high16 v7, 0x4000

    .line 636
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    .line 637
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    .line 638
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    .line 639
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->measureChild(Landroid/view/View;II)V

    .line 641
    invoke-static {v1, p1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getDefaultSize(II)I

    move-result v3

    .line 642
    invoke-static {v1, p2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getDefaultSize(II)I

    move-result v4

    .line 643
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v0

    sub-int v0, v3, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 644
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingTop()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getPaddingBottom()I

    move-result v2

    sub-int v2, v0, v2

    .line 646
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 650
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->c()I

    move-result v0

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 652
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eq v0, v8, :cond_d7

    .line 653
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5, v5}, Landroid/widget/ImageButton;->measure(II)V

    .line 654
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    .line 656
    :goto_65
    iget-object v6, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v6

    if-eq v6, v8, :cond_79

    .line 657
    iget-object v6, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v6, v5, v5}, Landroid/widget/ImageButton;->measure(II)V

    .line 658
    iget-object v6, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v0, v6

    .line 660
    :cond_79
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v6

    if-nez v6, :cond_93

    iget-object v6, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v6

    if-eq v6, v8, :cond_93

    .line 661
    iget-object v6, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v6, v5, v5}, Landroid/widget/ImageButton;->measure(II)V

    .line 662
    iget-object v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v0, v5

    .line 665
    :cond_93
    iget-boolean v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-eqz v5, :cond_c1

    div-int/lit8 v2, v2, 0x30

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 668
    :goto_9d
    iget-object v5, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v5, v0, v2}, Lcom/google/android/youtube/core/player/TimeBar;->measure(II)V

    .line 671
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-eqz v0, :cond_c8

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 675
    :goto_b4
    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e:Landroid/view/View;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 678
    invoke-virtual {p0, v3, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setMeasuredDimension(II)V

    .line 679
    return-void

    .line 665
    :cond_c1
    const/high16 v5, -0x8000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_9d

    .line 671
    :cond_c8
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/TimeBar;->b()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_b4

    :cond_d7
    move v0, v1

    goto :goto_65
.end method

.method protected onSizeChanged(IIII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v2, 0xa

    .line 683
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 684
    int-to-float v0, p1

    const v1, 0x3e2aaaab

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 685
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 686
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 555
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 586
    :cond_7
    :goto_7
    return v3

    .line 558
    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 559
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v2, v0

    .line 560
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_46

    move v0, v1

    :goto_19
    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->u:I

    .line 561
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_49

    move v0, v2

    :goto_22
    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->v:I

    .line 563
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_7

    .line 564
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->u:I

    sub-int/2addr v0, v1

    .line 565
    iget v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->v:I

    sub-int/2addr v1, v2

    .line 566
    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    if-eqz v2, :cond_5c

    .line 567
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4b

    if-le v2, v1, :cond_50

    .line 568
    if-lez v0, :cond_4c

    .line 569
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m()V

    goto :goto_7

    .line 560
    :cond_46
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->u:I

    goto :goto_19

    .line 561
    :cond_49
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->v:I

    goto :goto_22

    .line 571
    :cond_4c
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n()V

    goto :goto_7

    .line 574
    :cond_50
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_5c

    .line 575
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->o()V

    goto :goto_7

    .line 580
    :cond_5c
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->x:Z

    if-eqz v0, :cond_64

    .line 581
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    goto :goto_7

    .line 582
    :cond_64
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->G:Z

    if-eqz v0, :cond_7

    .line 583
    invoke-direct {p0, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Z)V

    goto :goto_7
.end method

.method public setAutoHide(Z)V
    .registers 2
    .parameter

    .prologue
    .line 414
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->F:Z

    .line 415
    if-eqz p1, :cond_8

    .line 416
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k()V

    .line 420
    :goto_7
    return-void

    .line 418
    :cond_8
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->l()V

    goto :goto_7
.end method

.method public setCcEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 842
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 843
    return-void
.end method

.method public setFullscreen(Z)V
    .registers 4
    .parameter

    .prologue
    .line 359
    if-nez p1, :cond_6

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->z:Z

    if-eqz v0, :cond_1a

    :cond_6
    const/4 v0, 0x1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    .line 360
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_16

    .line 361
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->i:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 363
    :cond_16
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e()V

    .line 364
    return-void

    .line 359
    :cond_1a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public setHQ(Z)V
    .registers 5
    .parameter

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 340
    if-eqz p1, :cond_18

    const v0, 0x7f0b0083

    .line 341
    :goto_a
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 342
    return-void

    .line 340
    :cond_18
    const v0, 0x7f0b0082

    goto :goto_a
.end method

.method public setHQisHD(Z)V
    .registers 4
    .parameter

    .prologue
    .line 345
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->g:Landroid/widget/ImageButton;

    if-eqz p1, :cond_b

    const v0, 0x7f020194

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 346
    return-void

    .line 345
    :cond_b
    const v0, 0x7f020195

    goto :goto_7
.end method

.method public setHasCc(Z)V
    .registers 2
    .parameter

    .prologue
    .line 837
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->A:Z

    .line 838
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 839
    return-void
.end method

.method public setHasNext(Z)V
    .registers 2
    .parameter

    .prologue
    .line 349
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->C:Z

    .line 350
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 351
    return-void
.end method

.method public setHasPrevious(Z)V
    .registers 2
    .parameter

    .prologue
    .line 354
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->D:Z

    .line 355
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 356
    return-void
.end method

.method public setHideOnTap(Z)V
    .registers 2
    .parameter

    .prologue
    .line 423
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->G:Z

    .line 424
    return-void
.end method

.method public setListener(Lcom/google/android/youtube/core/player/k;)V
    .registers 2
    .parameter

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b:Lcom/google/android/youtube/core/player/k;

    .line 271
    return-void
.end method

.method public setLoading()V
    .registers 5

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_7

    .line 291
    :goto_6
    return-void

    .line 287
    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 288
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->q:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 290
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k()V

    goto :goto_6
.end method

.method public setLoadingView(Landroid/view/View;Landroid/widget/LinearLayout$LayoutParams;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 259
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 260
    return-void
.end method

.method public setLockedInFullscreen(Z)V
    .registers 3
    .parameter

    .prologue
    .line 427
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->z:Z

    .line 428
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->y:Z

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setFullscreen(Z)V

    .line 429
    return-void
.end method

.method public setPlaying()V
    .registers 3

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_7

    .line 281
    :goto_6
    return-void

    .line 277
    :cond_7
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->t:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 278
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 280
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->k()V

    goto :goto_6
.end method

.method public setScrubbingEnabled(Z)V
    .registers 3
    .parameter

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TimeBar;->setEnabled(Z)V

    .line 377
    return-void
.end method

.method public setShowButtonsWhenNotFullscreen(Z)V
    .registers 2
    .parameter

    .prologue
    .line 862
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->J:Z

    .line 863
    return-void
.end method

.method public setShowFullscreen(Z)V
    .registers 2
    .parameter

    .prologue
    .line 367
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->B:Z

    .line 368
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 369
    return-void
.end method

.method public setShowFullscreenInPortrait(Z)V
    .registers 2
    .parameter

    .prologue
    .line 866
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->I:Z

    .line 867
    return-void
.end method

.method public setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V
    .registers 4
    .parameter

    .prologue
    .line 385
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->H:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    .line 386
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->e()V

    .line 387
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    iget v1, p1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->progressColor:I

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setProgressColor(I)V

    .line 388
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsBuffered:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setShowBuffered(Z)V

    .line 389
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->w:Z

    if-nez v0, :cond_1e

    .line 390
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->supportsScrubber:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setShowScrubber(Z)V

    .line 392
    :cond_1e
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 393
    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .registers 2
    .parameter

    .prologue
    .line 334
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->E:Z

    .line 335
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->o()V

    .line 336
    return-void
.end method

.method public setTimes(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/core/player/TimeBar;->setTime(III)V

    .line 381
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->p:Lcom/google/android/youtube/core/player/ao;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/ao;->a(II)V

    .line 382
    return-void
.end method
