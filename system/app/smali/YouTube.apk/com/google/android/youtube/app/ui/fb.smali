.class final Lcom/google/android/youtube/app/ui/fb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/fa;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/fa;)V
    .registers 2
    .parameter

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 255
    check-cast p1, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "failed to get disco results for ID "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 255
    check-cast p2, Lcom/google/android/youtube/core/async/Optional;

    invoke-interface {p2}, Lcom/google/android/youtube/core/async/Optional;->get()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    :goto_13
    return-void

    :cond_14
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;Lcom/google/android/youtube/core/model/MusicVideo;)Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/fa;->b(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/youtube/core/model/MusicVideo;->artistName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/ef;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/fa;->c(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/youtube/core/model/MusicVideo;->trackId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/eg;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/fa;->c(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/youtube/core/model/MusicVideo;->artistName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/eg;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fb;->a:Lcom/google/android/youtube/app/ui/fa;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/MusicVideo;->artistId:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/fa;Ljava/lang/String;)V

    goto :goto_13
.end method
