.class public final Lcom/google/android/youtube/app/ui/by;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field protected final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/core/a/a;

.field private c:Landroid/widget/AdapterView$OnItemClickListener;

.field private d:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private e:I

.field private f:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private final m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 46
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    .line 36
    iput v4, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    .line 37
    iput v3, p0, Lcom/google/android/youtube/app/ui/by;->f:I

    .line 47
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/by;->a:Landroid/content/Context;

    .line 48
    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    .line 50
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010034

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 52
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lcom/google/android/youtube/app/ui/by;->m:I

    .line 54
    new-instance v0, Lcom/google/android/youtube/app/ui/bz;

    invoke-direct {v0, p0, v3}, Lcom/google/android/youtube/app/ui/bz;-><init>(Lcom/google/android/youtube/app/ui/by;B)V

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/core/a/a;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 55
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->a()V

    .line 159
    return-void
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 76
    if-lez p1, :cond_e

    const/4 v0, 0x1

    :goto_3
    const-string v1, "numColumns must be > 0"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 77
    iput p1, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    .line 78
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/by;->notifyDataSetChanged()V

    .line 79
    return-void

    .line 76
    :cond_e
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(IIII)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    iput p1, p0, Lcom/google/android/youtube/app/ui/by;->h:I

    .line 87
    iput p3, p0, Lcom/google/android/youtube/app/ui/by;->j:I

    .line 88
    iput p2, p0, Lcom/google/android/youtube/app/ui/by;->i:I

    .line 89
    iput p4, p0, Lcom/google/android/youtube/app/ui/by;->k:I

    .line 90
    return-void
.end method

.method public final a(ILjava/lang/Iterable;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/a/a;->a(ILjava/lang/Iterable;)V

    .line 73
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/by;->c:Landroid/widget/AdapterView$OnItemClickListener;

    .line 59
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/util/Collection;)V

    .line 149
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lcom/google/android/youtube/core/a/a;
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    return-object v0
.end method

.method public final b(I)V
    .registers 2
    .parameter

    .prologue
    .line 82
    iput p1, p0, Lcom/google/android/youtube/app/ui/by;->f:I

    .line 83
    return-void
.end method

.method public final b(Ljava/lang/Iterable;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->b(Ljava/lang/Iterable;)V

    .line 68
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->b(Ljava/lang/Object;)V

    .line 139
    return-void
.end method

.method public final c(I)V
    .registers 2
    .parameter

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/youtube/app/ui/by;->l:I

    .line 94
    return-void
.end method

.method public final c(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/a/a;->c(ILjava/lang/Object;)V

    .line 134
    return-void
.end method

.method public final c(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->c(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getCount()I
    .registers 5

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 118
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .registers 3
    .parameter

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v11, -0x1

    const/4 v10, -0x2

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 168
    if-nez p2, :cond_9b

    .line 169
    new-instance p2, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 170
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 171
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v11, v10}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v2, v4

    .line 177
    :goto_1a
    if-nez p1, :cond_a6

    iget v0, p0, Lcom/google/android/youtube/app/ui/by;->f:I

    .line 178
    :goto_1e
    iget v3, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    if-le v3, v9, :cond_a9

    .line 179
    iget v3, p0, Lcom/google/android/youtube/app/ui/by;->h:I

    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->i:I

    add-int/2addr v0, v5

    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->j:I

    iget v6, p0, Lcom/google/android/youtube/app/ui/by;->k:I

    invoke-virtual {p2, v3, v0, v5, v6}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 190
    :goto_2e
    if-eqz v2, :cond_35

    array-length v0, v2

    iget v3, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    if-eq v0, v3, :cond_46

    .line 191
    :cond_35
    iget v0, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    new-array v0, v0, [Landroid/widget/FrameLayout;

    .line 192
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 193
    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 194
    iget v2, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    int-to-float v2, v2

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move-object v2, v0

    :cond_46
    move v0, v1

    .line 197
    :goto_47
    iget v3, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    if-ge v0, v3, :cond_bc

    .line 199
    aget-object v3, v2, v0

    if-nez v3, :cond_b9

    .line 200
    new-instance v3, Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/by;->a:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v0

    .line 201
    aget-object v3, v2, v0

    .line 202
    invoke-virtual {v3, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v5, p0, Lcom/google/android/youtube/app/ui/by;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    if-eqz v5, :cond_64

    .line 205
    invoke-virtual {v3, p0}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 207
    :cond_64
    invoke-virtual {v3, v9}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 208
    invoke-virtual {v3, v9}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 209
    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->m:I

    invoke-virtual {v3, v5}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 210
    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    if-ne v5, v9, :cond_ad

    .line 211
    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->h:I

    iget v6, p0, Lcom/google/android/youtube/app/ui/by;->i:I

    iget v7, p0, Lcom/google/android/youtube/app/ui/by;->j:I

    iget v8, p0, Lcom/google/android/youtube/app/ui/by;->k:I

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 216
    :goto_7e
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v1, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 218
    const/high16 v6, 0x3f80

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 219
    invoke-virtual {p2, v3, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 223
    :goto_8a
    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v0

    .line 224
    const v6, 0x7f080027

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v6, v5}, Landroid/widget/FrameLayout;->setTag(ILjava/lang/Object;)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_47

    .line 174
    :cond_9b
    check-cast p2, Landroid/widget/LinearLayout;

    .line 175
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/widget/FrameLayout;

    move-object v2, v0

    goto/16 :goto_1a

    :cond_a6
    move v0, v1

    .line 177
    goto/16 :goto_1e

    .line 187
    :cond_a9
    invoke-virtual {p2, v1, v0, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto :goto_2e

    .line 213
    :cond_ad
    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->l:I

    iget v6, p0, Lcom/google/android/youtube/app/ui/by;->l:I

    iget v7, p0, Lcom/google/android/youtube/app/ui/by;->l:I

    iget v8, p0, Lcom/google/android/youtube/app/ui/by;->l:I

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    goto :goto_7e

    .line 221
    :cond_b9
    aget-object v3, v2, v0

    goto :goto_8a

    :cond_bc
    move v3, v1

    .line 227
    :goto_bd
    iget v0, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    if-ge v3, v0, :cond_132

    .line 228
    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 229
    iget v5, p0, Lcom/google/android/youtube/app/ui/by;->e:I

    mul-int/2addr v5, p1

    add-int/2addr v5, v3

    .line 231
    iget-object v6, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v6}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v6

    if-ge v5, v6, :cond_110

    .line 232
    iget-object v6, p0, Lcom/google/android/youtube/app/ui/by;->b:Lcom/google/android/youtube/core/a/a;

    invoke-virtual {v6, v5, v0, p2}, Lcom/google/android/youtube/core/a/a;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 233
    if-eq v0, v5, :cond_10c

    .line 236
    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_ea

    .line 237
    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 239
    :cond_ea
    aget-object v0, v2, v3

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 240
    aget-object v0, v2, v3

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_fd

    .line 243
    aget-object v0, v2, v3

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 245
    :cond_fd
    aget-object v0, v2, v3

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 246
    aget-object v0, v2, v3

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v6, v11, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 227
    :cond_10c
    :goto_10c
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_bd

    .line 251
    :cond_110
    aget-object v0, v2, v3

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 252
    aget-object v0, v2, v3

    const v5, 0x7f080027

    invoke-virtual {v0, v5, v4}, Landroid/widget/FrameLayout;->setTag(ILjava/lang/Object;)V

    .line 253
    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setTag(Ljava/lang/Object;)V

    .line 254
    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    aget-object v0, v2, v3

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 256
    aget-object v0, v2, v3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_10c

    .line 260
    :cond_132
    return-object p2
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 128
    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .registers 3
    .parameter

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->c:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_1c

    .line 265
    const v0, 0x7f080027

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->c:Landroid/widget/AdapterView$OnItemClickListener;

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/by;->getItemId(I)J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 268
    :cond_1c
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .registers 8
    .parameter

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->c:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_1e

    .line 272
    const v0, 0x7f080027

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 273
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/by;->d:Landroid/widget/AdapterView$OnItemLongClickListener;

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/youtube/app/ui/by;->getItemId(I)J

    move-result-wide v4

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    .line 275
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method
