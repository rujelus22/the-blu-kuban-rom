.class public final Lcom/google/android/youtube/core/model/proto/h;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/i;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 2668
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 2766
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/h;->b:Ljava/lang/Object;

    .line 2840
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/h;->c:Ljava/lang/Object;

    .line 2669
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/h;
    .registers 1

    .prologue
    .line 2663
    new-instance v0, Lcom/google/android/youtube/core/model/proto/h;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/h;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/h;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 2750
    const/4 v2, 0x0

    .line 2752
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 2757
    if-eqz v0, :cond_e

    .line 2758
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/h;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;)Lcom/google/android/youtube/core/model/proto/h;

    .line 2761
    :cond_e
    return-object p0

    .line 2753
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 2754
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 2755
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 2757
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 2758
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/h;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;)Lcom/google/android/youtube/core/model/proto/h;

    :cond_21
    throw v0

    .line 2757
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/h;
    .registers 3

    .prologue
    .line 2688
    new-instance v0, Lcom/google/android/youtube/core/model/proto/h;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/h;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/h;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/h;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;)Lcom/google/android/youtube/core/model/proto/h;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 2704
    new-instance v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 2705
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    .line 2706
    const/4 v1, 0x0

    .line 2707
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_23

    .line 2710
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/h;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->artistId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->access$2402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2711
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1a

    .line 2712
    or-int/lit8 v0, v0, 0x2

    .line 2714
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/h;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->artistName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->access$2502(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2715
    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->access$2602(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;I)I

    .line 2716
    return-object v2

    :cond_23
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;)Lcom/google/android/youtube/core/model/proto/h;
    .registers 3
    .parameter

    .prologue
    .line 2720
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 2731
    :cond_6
    :goto_6
    return-object p0

    .line 2721
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->hasArtistId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2722
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    .line 2723
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->artistId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->access$2400(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/h;->b:Ljava/lang/Object;

    .line 2726
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->hasArtistName()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2727
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    .line 2728
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->artistName_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->access$2500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/h;->c:Ljava/lang/Object;

    goto :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 2663
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2663
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/h;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2663
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/h;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/h;->g()Lcom/google/android/youtube/core/model/proto/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/h;->g()Lcom/google/android/youtube/core/model/proto/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/h;->g()Lcom/google/android/youtube/core/model/proto/h;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/h;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 2663
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/h;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/h;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 2663
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$RelatedArtist;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2735
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 2743
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 2735
    goto :goto_9

    .line 2739
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/h;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1a

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    move v0, v1

    .line 2743
    goto :goto_b

    :cond_1a
    move v2, v0

    .line 2739
    goto :goto_16
.end method
