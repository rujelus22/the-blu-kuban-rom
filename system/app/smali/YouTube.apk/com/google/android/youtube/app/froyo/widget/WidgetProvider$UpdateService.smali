.class public Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private a:Landroid/appwidget/AppWidgetManager;

.field private b:Lcom/google/android/youtube/app/froyo/widget/c;

.field private c:Landroid/content/ComponentName;

.field private d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private e:Lcom/google/android/youtube/core/async/GDataRequestFactory;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 109
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b:Lcom/google/android/youtube/app/froyo/widget/c;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->e:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-static {}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a()Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/app/froyo/widget/c;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/l;)V

    .line 193
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 4
    .parameter

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b:Lcom/google/android/youtube/app/froyo/widget/c;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->e:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/app/froyo/widget/c;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/l;)V

    .line 177
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 109
    const-string v0, "Widget error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->c(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->d(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->stopSelf()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 109
    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1d

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->c(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->d(Landroid/content/Context;)V

    :goto_19
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->stopSelf()V

    return-void

    :cond_1d
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0, p2}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    goto :goto_19
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b()V

    .line 185
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b()V

    .line 181
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter

    .prologue
    .line 172
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .registers 8

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 121
    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 122
    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    .line 123
    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->e:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 124
    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v3

    .line 125
    new-instance v1, Lcom/google/android/youtube/app/b;

    invoke-direct {v1, v0, v3}, Lcom/google/android/youtube/app/b;-><init>(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;)V

    .line 127
    new-instance v0, Lcom/google/android/youtube/app/froyo/widget/c;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/youtube/app/b;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    const/16 v4, 0x8

    const/16 v5, 0xf

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/froyo/widget/c;-><init>(Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Ljava/util/concurrent/ConcurrentMap;IIZ)V

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->b:Lcom/google/android/youtube/app/froyo/widget/c;

    .line 134
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    .line 135
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    .line 136
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->a:Landroid/appwidget/AppWidgetManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->c:Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->b(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider$UpdateService;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 144
    const/4 v0, 0x1

    return v0
.end method
