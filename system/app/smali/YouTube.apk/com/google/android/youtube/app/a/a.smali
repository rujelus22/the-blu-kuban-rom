.class public final Lcom/google/android/youtube/app/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/prefetch/f;


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private final b:Lcom/google/android/youtube/app/prefetch/d;

.field private c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/google/android/youtube/app/prefetch/d;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "preferences cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/a;->a:Landroid/content/SharedPreferences;

    .line 29
    const-string v0, "prefetchStore cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/a;->b:Lcom/google/android/youtube/app/prefetch/d;

    .line 30
    const-string v0, "prefetch_watched_videos"

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/a/a;->c:Ljava/util/Set;

    .line 32
    invoke-virtual {p2, p0}, Lcom/google/android/youtube/app/prefetch/d;->a(Lcom/google/android/youtube/app/prefetch/f;)V

    .line 33
    return-void
.end method

.method private b()V
    .registers 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/a/a;->a:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "prefetch_watched_videos"

    iget-object v2, p0, Lcom/google/android/youtube/app/a/a;->c:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/youtube/app/a/a;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/app/a/a;->b:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/prefetch/d;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/transfer/Transfer$Status;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-ne v0, v1, :cond_12

    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/a/a;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    invoke-direct {p0}, Lcom/google/android/youtube/app/a/a;->b()V

    .line 44
    :cond_12
    return-void
.end method

.method public final g_()V
    .registers 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/app/a/a;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v2, p0, Lcom/google/android/youtube/app/a/a;->b:Lcom/google/android/youtube/app/prefetch/d;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/prefetch/d;->b(Ljava/lang/String;)Lcom/google/android/youtube/core/transfer/Transfer$Status;

    move-result-object v0

    sget-object v2, Lcom/google/android/youtube/core/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/youtube/core/transfer/Transfer$Status;

    if-eq v0, v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_6

    :cond_20
    invoke-direct {p0}, Lcom/google/android/youtube/app/a/a;->b()V

    .line 49
    return-void
.end method
