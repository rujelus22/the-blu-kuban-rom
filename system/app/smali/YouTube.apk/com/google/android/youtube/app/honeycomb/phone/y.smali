.class public final Lcom/google/android/youtube/app/honeycomb/phone/y;
.super Lcom/google/android/youtube/app/honeycomb/phone/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:Z

.field private G:Z

.field private H:Ljava/lang/String;

.field private I:Lcom/google/android/youtube/app/ui/TutorialView;

.field private J:Z

.field private K:Z

.field private L:Landroid/view/View;

.field private M:Lcom/google/android/youtube/core/model/UserAuth;

.field private N:Lcom/google/android/youtube/app/remote/ab;

.field private O:Lcom/google/android/youtube/app/remote/ae;

.field private P:Ljava/lang/String;

.field private final b:Landroid/util/SparseArray;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/view/View;

.field private final e:Lcom/google/android/youtube/core/ui/PagedListView;

.field private final f:Landroid/widget/ListView;

.field private final g:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final h:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final i:Lcom/google/android/youtube/core/b/al;

.field private final j:Lcom/google/android/youtube/core/b/an;

.field private final k:Lcom/google/android/youtube/core/d;

.field private final l:Lcom/google/android/youtube/core/j;

.field private final m:Lcom/google/android/youtube/core/Analytics;

.field private n:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final o:Lcom/google/android/youtube/app/honeycomb/phone/ao;

.field private final p:Lcom/google/android/youtube/app/honeycomb/phone/aq;

.field private final q:Lcom/google/android/youtube/core/async/l;

.field private final r:Landroid/content/SharedPreferences;

.field private s:Ljava/util/Set;

.field private t:Lcom/google/android/youtube/app/adapter/bt;

.field private u:Lcom/google/android/youtube/core/ui/j;

.field private v:Lcom/google/android/youtube/app/honeycomb/phone/ah;

.field private final w:Landroid/os/Handler;

.field private final x:Lcom/google/android/youtube/app/honeycomb/phone/an;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/an;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 143
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/j;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 120
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->y:I

    .line 121
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    .line 122
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    .line 123
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    .line 124
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->C:I

    .line 125
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->D:I

    .line 126
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->E:I

    .line 129
    iput-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    .line 132
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->J:Z

    .line 133
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->K:Z

    .line 144
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    .line 145
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->c:Landroid/view/LayoutInflater;

    .line 146
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/z;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/z;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->w:Landroid/os/Handler;

    .line 157
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 159
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->i:Lcom/google/android/youtube/core/b/al;

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->j:Lcom/google/android/youtube/core/b/an;

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->k:Lcom/google/android/youtube/core/d;

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->l:Lcom/google/android/youtube/core/j;

    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->m:Lcom/google/android/youtube/core/Analytics;

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->N:Lcom/google/android/youtube/app/remote/ab;

    .line 167
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/aa;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/aa;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->O:Lcom/google/android/youtube/app/remote/ae;

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f04003e

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->d:Landroid/view/View;

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->d:Landroid/view/View;

    const v1, 0x7f0800b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/PagedListView;->i()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->f:Landroid/widget/ListView;

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->r:Landroid/content/SharedPreferences;

    .line 179
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ao;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ao;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->o:Lcom/google/android/youtube/app/honeycomb/phone/ao;

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->P:Ljava/lang/String;

    .line 183
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/aq;

    invoke-direct {v0, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/aq;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->p:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    .line 184
    new-instance v0, Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->b:Landroid/util/SparseArray;

    .line 185
    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->q:Lcom/google/android/youtube/core/async/l;

    .line 186
    return-void
.end method

.method static synthetic A(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/j;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->l:Lcom/google/android/youtube/core/j;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/view/View;)Landroid/view/View;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->L:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/app/honeycomb/phone/an;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_16

    const v0, 0x7f020013

    :goto_b
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    if-eqz v1, :cond_1a

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_12
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void

    :cond_16
    const v0, 0x7f020012

    goto :goto_b

    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->l:Lcom/google/android/youtube/core/j;

    invoke-interface {v0}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_12
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/y;Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 786
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->n:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 787
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->n:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->n:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->l()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 788
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->o:Lcom/google/android/youtube/app/honeycomb/phone/ao;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->n:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ao;->a(Lcom/google/android/youtube/app/remote/ax;)V

    .line 789
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    if-gez v0, :cond_47

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->F:Z

    if-eqz v0, :cond_47

    .line 790
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->o:Lcom/google/android/youtube/app/honeycomb/phone/ao;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/youtube/app/adapter/bt;->a(ILcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    .line 792
    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    if-ltz v0, :cond_43

    move v0, v1

    :goto_36
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    .line 793
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    if-ltz v3, :cond_45

    :goto_3f
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    .line 813
    :cond_42
    :goto_42
    return-void

    :cond_43
    move v0, v2

    .line 792
    goto :goto_36

    :cond_45
    move v1, v2

    .line 793
    goto :goto_3f

    .line 795
    :cond_47
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    goto :goto_42

    .line 798
    :cond_4d
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    if-ltz v0, :cond_42

    .line 799
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/adapter/bt;->a(I)Lcom/google/android/youtube/app/adapter/cf;

    .line 800
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    .line 801
    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    if-ltz v0, :cond_8d

    move v0, v1

    :goto_62
    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    .line 802
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    if-ltz v3, :cond_8f

    :goto_6c
    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    .line 803
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->o:Lcom/google/android/youtube/app/honeycomb/phone/ao;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ao;->a(Lcom/google/android/youtube/app/remote/ax;)V

    .line 804
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    if-eqz v0, :cond_42

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    const-string v1, "REMOTE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 805
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->J:Z

    if-eqz v0, :cond_91

    .line 806
    const-string v0, "THE_FEED"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_42

    :cond_8d
    move v0, v2

    .line 801
    goto :goto_62

    :cond_8f
    move v1, v2

    .line 802
    goto :goto_6c

    .line 808
    :cond_91
    const-string v0, "TRENDING_GUIDE_ITEM"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    goto :goto_42
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/y;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->J:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/y;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->K:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->q()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/y;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->r()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->m:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/app/YouTubeApplication;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 5
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 69
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->K:Z

    if-eqz v0, :cond_31

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->J:Z

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->L:Landroid/view/View;

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    const v2, 0x7f0b01c4

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->L:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/TutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    :cond_31
    :goto_31
    return-void

    :cond_32
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->v:Lcom/google/android/youtube/app/honeycomb/phone/ah;

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->v:Lcom/google/android/youtube/app/honeycomb/phone/ah;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/ah;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    const v2, 0x7f0b01c3

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->v:Lcom/google/android/youtube/app/honeycomb/phone/ah;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/ah;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/TutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/ui/TutorialView;->setVisibility(I)V

    goto :goto_31
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/phone/y;)Landroid/util/SparseArray;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->b:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->B:I

    return v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->y:I

    return v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    return v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    return v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->C:I

    return v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->D:I

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/app/honeycomb/phone/y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->E:I

    return v0
.end method

.method static synthetic q(Lcom/google/android/youtube/app/honeycomb/phone/y;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->r:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private q()V
    .registers 6

    .prologue
    .line 402
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->F:Z

    if-eqz v0, :cond_5

    .line 426
    :goto_4
    return-void

    .line 405
    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/am;

    const v2, 0x7f0b0184

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/am;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/app/adapter/cf;)I

    .line 407
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->M:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_6f

    .line 408
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/af;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->M:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/af;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/content/Context;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->y:I

    .line 414
    :goto_27
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const-string v1, "CHANNEL_STORE"

    const v2, 0x7f0b0181

    const v3, 0x7f020094

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->v:Lcom/google/android/youtube/app/honeycomb/phone/ah;

    .line 417
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->v:Lcom/google/android/youtube/app/honeycomb/phone/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->A:I

    .line 419
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->M:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_66

    .line 420
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/am;

    const v2, 0x7f0b0185

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/am;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/app/adapter/cf;)I

    .line 421
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const-string v2, "THE_FEED"

    const v3, 0x7f0b0180

    const v4, 0x7f020095

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->z:I

    .line 424
    :cond_66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->F:Z

    .line 425
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->n:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    goto :goto_4

    .line 410
    :cond_6f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const-string v2, "ACCOUNT"

    const v3, 0x7f0b00e6

    const v4, 0x7f020096

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->y:I

    goto :goto_27
.end method

.method private r()V
    .registers 9

    .prologue
    .line 429
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->G:Z

    if-eqz v0, :cond_5

    .line 454
    :goto_4
    return-void

    .line 432
    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/am;

    const v2, 0x7f0b0186

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/am;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->b(Lcom/google/android/youtube/app/adapter/cf;)I

    .line 433
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 434
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const-string v2, "RECOMMENDED_GUIDE_ITEM"

    const v3, 0x7f0b0182

    const v4, 0x7f0200a5

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->b(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->C:I

    .line 439
    :cond_2f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const-string v2, "TRENDING_GUIDE_ITEM"

    const v3, 0x7f0b0183

    const v4, 0x7f0200aa

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->b(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->D:I

    .line 441
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->j()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 442
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ap;

    const-string v2, "LIVE_GUIDE_ITEM"

    const v3, 0x7f0b001a

    const v4, 0x7f02009e

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ap;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->b(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->E:I

    .line 445
    :cond_65
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->i:Lcom/google/android/youtube/core/b/al;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v2}, Lcom/google/android/youtube/core/utils/Util;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->q:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->s:Ljava/util/Set;

    if-eqz v0, :cond_d8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->s:Ljava/util/Set;

    move-object v3, v0

    .line 446
    :goto_81
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8b
    :goto_8b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 447
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8b

    .line 448
    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v6, Lcom/google/android/youtube/app/honeycomb/phone/ai;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v6, p0, v1, v7, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ai;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Ljava/lang/String;II)V

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/app/adapter/bt;->b(Lcom/google/android/youtube/app/adapter/cf;)I

    move-result v1

    .line 450
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->b:Landroid/util/SparseArray;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_8b

    .line 445
    :cond_d8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->r:Landroid/content/SharedPreferences;

    const-string v1, "youtube_categories"

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    move-object v3, v0

    goto :goto_81

    .line 453
    :cond_e8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->G:Z

    goto/16 :goto_4
.end method

.method static synthetic r(Lcom/google/android/youtube/app/honeycomb/phone/y;)Z
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->J:Z

    return v0
.end method

.method static synthetic s(Lcom/google/android/youtube/app/honeycomb/phone/y;)Landroid/widget/ListView;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method private s()V
    .registers 2

    .prologue
    .line 457
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->G:Z

    .line 458
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 459
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->b()V

    .line 460
    return-void
.end method

.method private t()V
    .registers 2

    .prologue
    .line 499
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->G:Z

    if-eqz v0, :cond_a

    .line 501
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->s()V

    .line 502
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->r()V

    .line 504
    :cond_a
    return-void
.end method

.method static synthetic t(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->s()V

    return-void
.end method

.method static synthetic u(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->i:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/ui/j;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->u:Lcom/google/android/youtube/core/ui/j;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/youtube/app/honeycomb/phone/y;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->t()V

    return-void
.end method

.method static synthetic x(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->j:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/youtube/app/honeycomb/phone/y;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->c:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/model/UserAuth;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->M:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .registers 4
    .parameter

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ar;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ar;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->a(Lcom/google/android/youtube/core/utils/p;)V

    .line 782
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->t()V

    .line 783
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 3
    .parameter

    .prologue
    .line 773
    if-eqz p1, :cond_f

    .line 774
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/bt;->d(Ljava/lang/Object;)V

    .line 775
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 776
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->t()V

    .line 778
    :cond_f
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->M:Lcom/google/android/youtube/core/model/UserAuth;

    .line 275
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->u:Lcom/google/android/youtube/core/ui/j;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 277
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 69
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 69
    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/converter/http/g;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_17
    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Category;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Category;->term:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->s:Ljava/util/Set;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Category;->term:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_17

    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->r:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/ac;->a(Landroid/content/SharedPreferences;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    const-string v1, "youtube_categories"

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->s:Ljava/util/Set;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/ad;->a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/ad;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 286
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->i_()V

    .line 287
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    if-eqz v0, :cond_15

    .line 291
    if-eqz p2, :cond_16

    .line 292
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/an;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 293
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    .line 294
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 301
    :cond_15
    :goto_15
    return-void

    .line 298
    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->w:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_15
.end method

.method public final b()V
    .registers 8

    .prologue
    .line 190
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->b()V

    .line 192
    new-instance v0, Lcom/google/android/youtube/app/adapter/bt;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v2, 0x7f04003c

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/al;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/al;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;B)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    .line 199
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->t:Lcom/google/android/youtube/app/adapter/bt;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->x()Lcom/google/android/youtube/core/async/av;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->k:Lcom/google/android/youtube/core/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/phone/ab;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->u:Lcom/google/android/youtube/core/ui/j;

    .line 239
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->e:Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ac;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ac;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 271
    return-void
.end method

.method public final e()I
    .registers 2

    .prologue
    .line 384
    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 374
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->g()V

    .line 375
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->N:Lcom/google/android/youtube/app/remote/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->O:Lcom/google/android/youtube/app/remote/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/ab;->b(Lcom/google/android/youtube/app/remote/ae;)V

    .line 376
    return-void
.end method

.method public final h()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 364
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/j;->h()V

    .line 365
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->N:Lcom/google/android/youtube/app/remote/ab;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->O:Lcom/google/android/youtube/app/remote/ae;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/remote/ab;->a(Lcom/google/android/youtube/app/remote/ae;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->N:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 367
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->P:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_31

    const/4 v0, 0x1

    :goto_27
    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->P:Ljava/lang/String;

    if-eqz v0, :cond_30

    .line 368
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->H:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    .line 370
    :cond_30
    return-void

    :cond_31
    move v0, v1

    .line 367
    goto :goto_27
.end method

.method public final i_()V
    .registers 3

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->q()V

    .line 281
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->r()V

    .line 282
    const-string v0, "TRENDING_GUIDE_ITEM"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Ljava/lang/String;Z)V

    .line 283
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    if-eqz v0, :cond_9

    .line 390
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/phone/an;->b()V

    .line 392
    :cond_9
    return-void
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    if-eqz v0, :cond_9

    .line 397
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->x:Lcom/google/android/youtube/app/honeycomb/phone/an;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/phone/an;->e_()V

    .line 399
    :cond_9
    return-void
.end method

.method public final l()V
    .registers 4

    .prologue
    .line 304
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f0400ba

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08015b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/TutorialView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    .line 306
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->h:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ad;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ad;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/TutorialView;->setDismissListener(Lcom/google/android/youtube/app/ui/dc;)V

    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->K:Z

    .line 319
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ae;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ae;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/y;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 326
    return-void
.end method

.method public final m()Z
    .registers 2

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->K:Z

    return v0
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->I:Lcom/google/android/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/TutorialView;->a()V

    .line 334
    return-void
.end method

.method public final o()Landroid/view/View;
    .registers 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->d:Landroid/view/View;

    return-object v0
.end method

.method public final p()V
    .registers 3

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->g:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/y;->p:Lcom/google/android/youtube/app/honeycomb/phone/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 523
    return-void
.end method
