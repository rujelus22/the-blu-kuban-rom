.class public Lcom/google/android/youtube/core/player/DefaultLiveOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/ah;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/d;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/os/Handler;

.field private f:Lcom/google/android/youtube/core/player/ai;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 48
    const v0, 0x7f020129

    const v1, 0x7f02018f

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;-><init>(Landroid/content/Context;II)V

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, -0x2

    const/4 v2, -0x1

    .line 53
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 55
    new-instance v0, Lcom/google/android/youtube/core/utils/aa;

    invoke-direct {v0}, Lcom/google/android/youtube/core/utils/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->a:Lcom/google/android/youtube/core/utils/d;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->b:Landroid/content/res/Resources;

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->e:Landroid/os/Handler;

    .line 60
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2, v2}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->addView(Landroid/view/View;II)V

    .line 66
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    const v1, 0x7f020129

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    const v1, 0x7f02018f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v3, v3}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->addView(Landroid/view/View;II)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->setClickable(Z)V

    .line 75
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->setVisibility(I)V

    .line 76
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultLiveOverlay;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 111
    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    if-ne p1, v0, :cond_16

    move v0, v1

    :goto_9
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    if-ne p1, v3, :cond_18

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 113
    return-void

    :cond_16
    move v0, v2

    .line 111
    goto :goto_9

    :cond_18
    move v1, v2

    .line 112
    goto :goto_12
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultLiveOverlay;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 79
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/model/LiveEvent;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 91
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->a:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/d;->a()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 92
    iget-object v1, p1, Lcom/google/android/youtube/core/model/LiveEvent;->start:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->b:Landroid/content/res/Resources;

    const v2, 0x7f0b0076

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/google/android/youtube/core/utils/ab;->a(Landroid/content/Context;Lcom/google/android/youtube/core/model/LiveEvent;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->c:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->a(Landroid/view/View;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/core/player/n;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/n;-><init>(Lcom/google/android/youtube/core/player/DefaultLiveOverlay;)V

    iget-object v2, p1, Lcom/google/android/youtube/core/model/LiveEvent;->start:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->a:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v4}, Lcom/google/android/youtube/core/utils/d;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 103
    :goto_4b
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->setVisibility(I)V

    .line 104
    return-void

    .line 101
    :cond_4f
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->a(Landroid/view/View;)V

    goto :goto_4b
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 83
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 107
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->setVisibility(I)V

    .line 108
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->f:Lcom/google/android/youtube/core/player/ai;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->d:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_12

    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->f:Lcom/google/android/youtube/core/player/ai;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ai;->h()V

    .line 118
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->setVisibility(I)V

    .line 122
    :goto_11
    return-void

    .line 120
    :cond_12
    const-string v0, "Play button clicked in LiveOverlay, but no listener was registered"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    goto :goto_11
.end method

.method public setListener(Lcom/google/android/youtube/core/player/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 87
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ai;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;->f:Lcom/google/android/youtube/core/player/ai;

    .line 88
    return-void
.end method
