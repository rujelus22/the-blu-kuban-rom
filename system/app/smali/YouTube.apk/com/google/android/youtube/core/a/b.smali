.class public abstract Lcom/google/android/youtube/core/a/b;
.super Lcom/google/android/youtube/core/a/h;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/youtube/core/a/g;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/a/e;Lcom/google/android/youtube/core/a/g;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/a/h;-><init>(Lcom/google/android/youtube/core/a/e;)V

    .line 56
    const-string v0, "viewType cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/g;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/b;->c:Lcom/google/android/youtube/core/a/g;

    .line 57
    if-lez p3, :cond_18

    const/4 v0, 0x1

    :goto_10
    const-string v1, "groupSize must be positive"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 58
    iput p3, p0, Lcom/google/android/youtube/core/a/b;->d:I

    .line 59
    return-void

    .line 57
    :cond_18
    const/4 v0, 0x0

    goto :goto_10
.end method


# virtual methods
.method public final a()I
    .registers 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    div-int/2addr v0, v1

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->c:Lcom/google/android/youtube/core/a/g;

    return-object v0
.end method

.method protected final a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->c:Lcom/google/android/youtube/core/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/youtube/core/a/b;->d:I

    return v0
.end method

.method public final c(I)J
    .registers 4
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/e;->c(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/a/e;->c()Z

    move-result v0

    return v0
.end method

.method public final d(I)Z
    .registers 5
    .parameter

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/a/b;->f(I)I

    move-result v1

    .line 124
    iget v0, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v0, p1

    :goto_7
    if-ge v0, v1, :cond_16

    .line 125
    iget-object v2, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/a/e;->d(I)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 126
    const/4 v0, 0x1

    .line 129
    :goto_12
    return v0

    .line 124
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 129
    :cond_16
    const/4 v0, 0x0

    goto :goto_12
.end method

.method protected final e(I)I
    .registers 3
    .parameter

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v0, p1

    return v0
.end method

.method protected final f(I)I
    .registers 4
    .parameter

    .prologue
    .line 86
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/google/android/youtube/core/a/b;->d:I

    mul-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/core/a/b;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/a/e;->n()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
