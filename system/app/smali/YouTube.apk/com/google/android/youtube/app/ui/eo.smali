.class public final Lcom/google/android/youtube/app/ui/eo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/ui/cr;
.implements Lcom/google/android/youtube/app/ui/dq;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final A:Landroid/widget/ImageButton;

.field private final B:Landroid/widget/ImageButton;

.field private final C:Landroid/widget/TextView;

.field private final D:Landroid/widget/FrameLayout;

.field private final E:Landroid/widget/ProgressBar;

.field private F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private G:I

.field private H:I

.field private I:Lcom/google/android/youtube/core/model/Video;

.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/Analytics;

.field private final c:Lcom/google/android/youtube/app/a;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/b/al;

.field private final f:Lcom/google/android/youtube/core/b/an;

.field private final g:Lcom/google/android/youtube/core/utils/l;

.field private h:Lcom/google/android/youtube/app/ui/di;

.field private final i:Lcom/google/android/youtube/app/ui/et;

.field private final j:Lcom/google/android/youtube/app/ui/es;

.field private final k:Landroid/view/View;

.field private final l:Lcom/google/android/youtube/core/ui/l;

.field private final m:Landroid/widget/ImageView;

.field private final n:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/ProgressBar;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/TextView;

.field private final x:Lcom/google/android/youtube/app/b/g;

.field private final y:Lcom/google/android/youtube/plus1/PlusOneButton;

.field private final z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/ui/m;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/b/g;Lcom/google/android/youtube/core/utils/l;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    .line 114
    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eo;->b:Lcom/google/android/youtube/core/Analytics;

    .line 115
    iput-object p4, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/a;

    .line 116
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 117
    iput-object p5, p0, Lcom/google/android/youtube/app/ui/eo;->e:Lcom/google/android/youtube/core/b/al;

    .line 118
    iput-object p6, p0, Lcom/google/android/youtube/app/ui/eo;->f:Lcom/google/android/youtube/core/b/an;

    .line 119
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->g:Lcom/google/android/youtube/core/utils/l;

    .line 121
    new-instance v1, Lcom/google/android/youtube/app/ui/et;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/et;-><init>(Lcom/google/android/youtube/app/ui/eo;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->i:Lcom/google/android/youtube/app/ui/et;

    .line 122
    new-instance v1, Lcom/google/android/youtube/app/ui/es;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/es;-><init>(Lcom/google/android/youtube/app/ui/eo;B)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->j:Lcom/google/android/youtube/app/ui/es;

    .line 124
    const v1, 0x7f0800e6

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-static {p1, v1, v0}, Lcom/google/android/youtube/core/ui/l;->a(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/core/ui/m;)Lcom/google/android/youtube/core/ui/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->l:Lcom/google/android/youtube/core/ui/l;

    .line 126
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->l:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/ui/l;->a()V

    .line 128
    const v1, 0x7f0800e1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    .line 129
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f0800ec

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->m:Landroid/widget/ImageView;

    .line 130
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f080189

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->n:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    .line 133
    const v1, 0x7f080046

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(I)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->o:Landroid/widget/TextView;

    .line 134
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const-string v2, "title_extended"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->p:Landroid/widget/TextView;

    .line 136
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f08005a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->t:Landroid/widget/ImageView;

    .line 138
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f0800af

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->u:Landroid/view/View;

    .line 139
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->u:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    const v1, 0x7f08005d

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(I)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->v:Landroid/widget/TextView;

    .line 143
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f0800ae

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    .line 144
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    if-eqz v1, :cond_e5

    .line 145
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v6

    .line 147
    new-instance v1, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const-string v8, "Watch"

    move-object v2, p1

    move-object v3, p3

    move-object/from16 v4, p8

    move-object v5, p5

    move-object v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cr;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    .line 149
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/eo;->H:I

    .line 150
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/eo;->G:I

    .line 151
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/youtube/app/ui/dh;

    invoke-direct {v2, p1}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 154
    :cond_e5
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f080068

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    .line 155
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_107

    .line 156
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 157
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 162
    :cond_107
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f08005b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->E:Landroid/widget/ProgressBar;

    .line 164
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->x:Lcom/google/android/youtube/app/b/g;

    .line 165
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f0800ee

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/plus1/PlusOneButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    .line 167
    const v1, 0x7f080094

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(I)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->z:Landroid/widget/TextView;

    .line 168
    const v1, 0x7f0800ed

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(I)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->q:Landroid/widget/TextView;

    .line 169
    const v1, 0x7f0800ea

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(I)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->w:Landroid/widget/TextView;

    .line 170
    const v1, 0x7f0800eb

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(I)Landroid/widget/TextView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->r:Landroid/widget/TextView;

    .line 171
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const v2, 0x7f08018d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->s:Landroid/widget/ProgressBar;

    .line 173
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const-string v2, "like_button"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->A:Landroid/widget/ImageButton;

    .line 174
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->A:Landroid/widget/ImageButton;

    if-eqz v1, :cond_16b

    .line 175
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->A:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    :cond_16b
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const-string v2, "dislike_button"

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->B:Landroid/widget/ImageButton;

    .line 178
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->B:Landroid/widget/ImageButton;

    if-eqz v1, :cond_180

    .line 179
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->B:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_180
    return-void
.end method

.method private a(I)Landroid/widget/TextView;
    .registers 3
    .parameter

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/eo;)Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->n:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    return-object v0
.end method

.method private a(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 336
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_27

    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    const v4, 0x7f0b01d9

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_3f

    .line 343
    add-int v3, p1, p2

    .line 346
    iget-object v4, p0, Lcom/google/android/youtube/app/ui/eo;->s:Landroid/widget/ProgressBar;

    if-nez v3, :cond_62

    move v0, v1

    :goto_32
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 347
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 348
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 350
    :cond_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->w:Landroid/widget/TextView;

    if-eqz v0, :cond_61

    .line 351
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->w:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    const v4, 0x7f0b01d8

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    iget v5, v5, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    :cond_61
    return-void

    :cond_62
    move v0, v2

    .line 346
    goto :goto_32
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/eo;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->m:Landroid/widget/ImageView;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->g:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->a()Z

    move-result v0

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->x:Lcom/google/android/youtube/app/b/g;

    invoke-interface {v0}, Lcom/google/android/youtube/app/b/g;->b()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 265
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/plus1/PlusOneButton;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eo;->x:Lcom/google/android/youtube/app/b/g;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eo;->g:Lcom/google/android/youtube/core/utils/l;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/google/android/youtube/plus1/PlusOneButton;->a(Landroid/app/Activity;Lcom/google/android/youtube/plus1/a;Lcom/google/android/youtube/core/utils/l;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v1}, Lcom/google/android/youtube/plus1/c;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/plus1/PlusOneButton;->setUri(Landroid/net/Uri;)V

    .line 273
    :goto_2f
    return-void

    .line 270
    :cond_30
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/plus1/PlusOneButton;->setVisibility(I)V

    goto :goto_2f
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/eo;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/eo;)Lcom/google/android/youtube/app/ui/es;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->j:Lcom/google/android/youtube/app/ui/es;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/eo;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->f:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/eo;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/eo;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/eo;)V
    .registers 4
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020187

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/eo;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->t:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .registers 7
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 445
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/ui/er;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_90

    .line 446
    :goto_15
    return-void

    .line 445
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    const v2, 0x7f0b018d

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/eo;->H:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_15

    :pswitch_48
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    const v2, 0x7f0b018b

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/ui/eo;->G:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    const v1, 0x7f020084

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_15

    :pswitch_7a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setClickable(Z)V

    goto :goto_15

    :pswitch_data_90
    .packed-switch 0x1
        :pswitch_16
        :pswitch_48
        :pswitch_48
        :pswitch_7a
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 6
    .parameter

    .prologue
    .line 291
    if-eqz p1, :cond_6

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    if-nez v0, :cond_7

    .line 319
    :cond_6
    :goto_6
    return-void

    .line 295
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->f:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/ep;

    invoke-direct {v3, p0, p1}, Lcom/google/android/youtube/app/ui/ep;-><init>(Lcom/google/android/youtube/app/ui/eo;Lcom/google/android/youtube/core/model/Branding;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_6
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 260
    iget-object v0, p1, Lcom/google/android/youtube/core/model/UserAuth;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eo;->b(Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 201
    const-string v0, "video can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->n:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    if-eqz v0, :cond_1b

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->n:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->setVisibility(I)V

    .line 208
    :cond_1b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->o:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_2d

    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->p:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    :cond_2d
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    if-eqz v0, :cond_9f

    .line 217
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    invoke-static {v1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->uploadedDate:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    :goto_47
    iget v0, p1, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    iget v0, p1, Lcom/google/android/youtube/core/model/Video;->likesCount:I

    iget v1, p1, Lcom/google/android/youtube/core/model/Video;->dislikesCount:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(II)V

    .line 225
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->C:Landroid/widget/TextView;

    if-eqz v0, :cond_a5

    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->v:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    :goto_5b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->y:Lcom/google/android/youtube/plus1/PlusOneButton;

    if-eqz v0, :cond_64

    .line 234
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 237
    :cond_64
    invoke-static {p1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->A:Landroid/widget/ImageButton;

    if-eqz v1, :cond_7a

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->B:Landroid/widget/ImageButton;

    if-eqz v1, :cond_7a

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->A:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->B:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 239
    :cond_7a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->z:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 242
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->l:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/l;->b()V

    .line 244
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->t:Landroid/widget/ImageView;

    if-eqz v0, :cond_9e

    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->e:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->ownerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eo;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eo;->i:Lcom/google/android/youtube/app/ui/et;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 249
    :cond_9e
    return-void

    .line 220
    :cond_9f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_47

    .line 228
    :cond_a5
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->v:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5b
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->l:Lcom/google/android/youtube/core/ui/l;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/ui/l;->a(Ljava/lang/CharSequence;Z)V

    .line 198
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 256
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eo;->b(Ljava/lang/String;)V

    .line 257
    return-void
.end method

.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 358
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->viewCount:I

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    iget v3, v0, Lcom/google/android/youtube/core/model/Video;->likesCount:I

    if-eqz p1, :cond_1a

    move v0, v1

    :goto_d
    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    iget v3, v3, Lcom/google/android/youtube/core/model/Video;->dislikesCount:I

    if-eqz p1, :cond_1c

    :goto_14
    add-int v1, v3, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(II)V

    .line 360
    return-void

    :cond_1a
    move v0, v2

    .line 358
    goto :goto_d

    :cond_1c
    move v2, v1

    goto :goto_14
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->l:Lcom/google/android/youtube/core/ui/l;

    const v1, 0x7f0b0121

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/ui/l;->a(IZ)V

    .line 193
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 252
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eo;->b(Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->u:Landroid/view/View;

    if-ne p1, v0, :cond_19

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_19

    .line 323
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "WatchChannel"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->I:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->owner:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/a;->a(Ljava/lang/String;)V

    .line 333
    :cond_18
    :goto_18
    return-void

    .line 326
    :cond_19
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->A:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_27

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->h:Lcom/google/android/youtube/app/ui/di;

    if-eqz v0, :cond_27

    .line 327
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->h:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/di;->a()V

    goto :goto_18

    .line 328
    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->B:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_35

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->h:Lcom/google/android/youtube/app/ui/di;

    if-eqz v0, :cond_35

    .line 329
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->h:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/di;->b()V

    goto :goto_18

    .line 330
    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->D:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_18

    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->F:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    goto :goto_18
.end method
