.class final enum Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field public static final enum ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field public static final enum LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field public static final enum PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field public static final enum PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field public static final enum RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

.field public static final enum UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 52
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 53
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 54
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    const-string v1, "RECOVERABLE_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 55
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    const-string v1, "UNRECOVERABLE_ERROR"

    invoke-direct {v0, v1, v7}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 56
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    const-string v1, "ENDED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    .line 50
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->LOADING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->$VALUES:[Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;
    .registers 2
    .parameter

    .prologue
    .line 50
    const-class v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->$VALUES:[Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    return-object v0
.end method


# virtual methods
.method public final isError()Z
    .registers 2

    .prologue
    .line 59
    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->RECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-eq p0, v0, :cond_8

    sget-object v0, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->UNRECOVERABLE_ERROR:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne p0, v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method
