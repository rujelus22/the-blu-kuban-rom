.class final Lcom/google/android/youtube/app/compat/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/compat/m;

.field private final b:Landroid/view/SubMenu;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    const-string v0, "targetSubMenu cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/SubMenu;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    .line 200
    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/n;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/youtube/app/compat/m;->a(Lcom/google/android/youtube/app/compat/m;Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    .line 201
    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .registers 4
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1}, Landroid/view/SubMenu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/compat/m;->a(Lcom/google/android/youtube/app/compat/m;Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/SubMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/compat/m;->a(Lcom/google/android/youtube/app/compat/m;Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/compat/m;->a(Lcom/google/android/youtube/app/compat/m;Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 4
    .parameter

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v1, p1}, Landroid/view/SubMenu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/compat/m;->a(Lcom/google/android/youtube/app/compat/m;Landroid/view/MenuItem;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Landroid/view/SubMenu;->addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .registers 5
    .parameter

    .prologue
    .line 236
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1}, Landroid/view/SubMenu;->addSubMenu(I)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1, p2, p3, p4}, Landroid/view/SubMenu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1, p2, p3, p4}, Landroid/view/SubMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 5
    .parameter

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v2, p1}, Landroid/view/SubMenu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/compat/n;-><init>(Lcom/google/android/youtube/app/compat/m;Landroid/view/SubMenu;)V

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->clear()V

    .line 249
    return-void
.end method

.method public final clearHeader()V
    .registers 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->clearHeader()V

    .line 310
    return-void
.end method

.method public final close()V
    .registers 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->close()V

    .line 253
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem()Landroid/view/MenuItem;
    .registers 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .registers 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->isShortcutKey(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->performIdentifierAction(II)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/SubMenu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final removeGroup(I)V
    .registers 3
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->removeGroup(I)V

    .line 281
    return-void
.end method

.method public final removeItem(I)V
    .registers 4
    .parameter

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->removeItem(I)V

    .line 285
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-static {v0}, Lcom/google/android/youtube/app/compat/m;->a(Lcom/google/android/youtube/app/compat/m;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2, p3}, Landroid/view/SubMenu;->setGroupCheckable(IZZ)V

    .line 290
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->setGroupEnabled(IZ)V

    .line 294
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1, p2}, Landroid/view/SubMenu;->setGroupVisible(IZ)V

    .line 298
    return-void
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderIcon(I)Landroid/view/SubMenu;

    .line 318
    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 323
    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderTitle(I)Landroid/view/SubMenu;

    .line 328
    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 333
    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;

    .line 338
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setIcon(I)Landroid/view/SubMenu;

    .line 343
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 348
    return-object p0
.end method

.method public final setQwertyMode(Z)V
    .registers 3
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0, p1}, Landroid/view/SubMenu;->setQwertyMode(Z)V

    .line 302
    return-void
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/n;->b:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->size()I

    move-result v0

    return v0
.end method
