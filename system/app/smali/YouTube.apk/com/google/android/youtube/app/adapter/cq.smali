.class public final Lcom/google/android/youtube/app/adapter/cq;
.super Lcom/google/android/youtube/core/a/b;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final c:Landroid/view/LayoutInflater;

.field private final d:I

.field private final e:Lcom/google/android/youtube/app/adapter/cr;

.field private final f:Lcom/google/android/youtube/app/adapter/cs;

.field private final g:I

.field private final h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/adapter/cq;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;I)V

    .line 92
    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;I)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/youtube/core/a/b;-><init>(Lcom/google/android/youtube/core/a/e;Lcom/google/android/youtube/core/a/g;I)V

    .line 74
    const-string v0, "inflater cannot be null"

    invoke-static {p2, v0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cq;->c:Landroid/view/LayoutInflater;

    .line 75
    const-string v0, "clickListener cannot be null"

    invoke-static {p6, v0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cr;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/cq;->e:Lcom/google/android/youtube/app/adapter/cr;

    .line 76
    iput-object p7, p0, Lcom/google/android/youtube/app/adapter/cq;->f:Lcom/google/android/youtube/app/adapter/cs;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/adapter/cq;->h:I

    .line 78
    iput p5, p0, Lcom/google/android/youtube/app/adapter/cq;->d:I

    .line 79
    iput p4, p0, Lcom/google/android/youtube/app/adapter/cq;->g:I

    .line 80
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x7f080028

    const/4 v2, 0x0

    .line 101
    if-eqz p2, :cond_8b

    .line 103
    check-cast p2, Landroid/widget/LinearLayout;

    .line 108
    :goto_8
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/cq;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 110
    invoke-virtual {p2, v3}, Landroid/widget/LinearLayout;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    if-nez v0, :cond_21

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/cq;->b()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    invoke-virtual {p2, v3, v0}, Landroid/widget/LinearLayout;->setTag(ILjava/lang/Object;)V

    .line 112
    :cond_21
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 114
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/cq;->e(I)I

    move-result v4

    .line 115
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/cq;->f(I)I

    move-result v1

    sub-int v5, v1, v4

    move v3, v2

    .line 116
    :goto_2f
    if-ge v3, v5, :cond_9e

    .line 117
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cq;->a:Lcom/google/android/youtube/core/a/e;

    add-int v6, v4, v3

    aget-object v7, v0, v3

    invoke-virtual {v1, v6, v7, p2}, Lcom/google/android/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 118
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v7, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 119
    const/high16 v1, 0x3f80

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 120
    if-nez v3, :cond_99

    move v1, v2

    :goto_48
    iget v8, p0, Lcom/google/android/youtube/app/adapter/cq;->i:I

    add-int/2addr v1, v8

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 121
    iget v1, p0, Lcom/google/android/youtube/app/adapter/cq;->j:I

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 122
    iget v1, p0, Lcom/google/android/youtube/app/adapter/cq;->g:I

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_9c

    iget v1, p0, Lcom/google/android/youtube/app/adapter/cq;->k:I

    :goto_59
    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 123
    iget v1, p0, Lcom/google/android/youtube/app/adapter/cq;->l:I

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 124
    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    add-int v1, v4, v3

    const v7, 0x7f080029

    iget v8, p0, Lcom/google/android/youtube/app/adapter/cq;->h:I

    add-int/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v7, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cq;->f:Lcom/google/android/youtube/app/adapter/cs;

    if-eqz v1, :cond_7e

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_7e
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 126
    aput-object v6, v0, v3

    .line 127
    invoke-virtual {p2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 116
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2f

    .line 105
    :cond_8b
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cq;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0400c8

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto/16 :goto_8

    .line 120
    :cond_99
    iget v1, p0, Lcom/google/android/youtube/app/adapter/cq;->d:I

    goto :goto_48

    :cond_9c
    move v1, v2

    .line 122
    goto :goto_59

    .line 129
    :cond_9e
    return-object p2
.end method

.method public final a(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 133
    iput v0, p0, Lcom/google/android/youtube/app/adapter/cq;->i:I

    .line 134
    iput p2, p0, Lcom/google/android/youtube/app/adapter/cq;->j:I

    .line 135
    iput v0, p0, Lcom/google/android/youtube/app/adapter/cq;->k:I

    .line 136
    iput v0, p0, Lcom/google/android/youtube/app/adapter/cq;->l:I

    .line 137
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/cq;->a:Lcom/google/android/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/a/e;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    const v2, 0x7f08002a

    .line 179
    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 180
    if-nez v0, :cond_1e

    .line 181
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 182
    const v0, 0x7f080029

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 187
    :cond_1e
    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 188
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cq;->e:Lcom/google/android/youtube/app/adapter/cr;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, p0, v0}, Lcom/google/android/youtube/app/adapter/cr;->a(Lcom/google/android/youtube/app/adapter/cq;I)V

    .line 189
    :cond_2d
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .registers 6
    .parameter

    .prologue
    const v3, 0x7f08002a

    const/4 v2, 0x1

    .line 192
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 193
    if-nez v0, :cond_b

    .line 198
    :goto_a
    return v2

    .line 196
    :cond_b
    const/4 v1, 0x0

    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 197
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/cq;->f:Lcom/google/android/youtube/app/adapter/cs;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    goto :goto_a
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const v1, 0x7f08002a

    .line 166
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1c

    .line 175
    :goto_a
    :pswitch_a
    const/4 v0, 0x0

    return v0

    .line 168
    :pswitch_c
    const v0, 0x7f080029

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_a

    .line 172
    :pswitch_17
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_a

    .line 166
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_a
        :pswitch_a
        :pswitch_17
    .end packed-switch
.end method
