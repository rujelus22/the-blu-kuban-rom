.class final Lcom/google/android/youtube/app/adapter/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bk;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/bk;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/bm;->a:Lcom/google/android/youtube/app/adapter/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Landroid/view/View;

    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Landroid/view/View;

    const v1, 0x7f080046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->c:Landroid/widget/TextView;

    .line 104
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 139
    :cond_b
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 92
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/bm;->b()V

    invoke-static {p2}, Lcom/google/android/youtube/plus1/c;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bm;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 114
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->a:Lcom/google/android/youtube/app/adapter/bk;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/bk;->b(Lcom/google/android/youtube/app/adapter/bk;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->e:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/u;

    .line 115
    if-eqz v0, :cond_6a

    iget-object v1, v0, Lcom/google/android/plus1/u;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_25

    iget-object v1, v0, Lcom/google/android/plus1/u;->c:Ljava/util/Set;

    if-eqz v1, :cond_6a

    iget-object v1, v0, Lcom/google/android/plus1/u;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6a

    .line 118
    :cond_25
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    if-nez v1, :cond_3c

    .line 119
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->b:Landroid/view/View;

    const v2, 0x7f080090

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 120
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    .line 122
    :cond_3c
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bm;->a:Lcom/google/android/youtube/app/adapter/bk;

    invoke-static {v2}, Lcom/google/android/youtube/app/adapter/bk;->c(Lcom/google/android/youtube/app/adapter/bk;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/youtube/plus1/c;->b(Landroid/content/Context;Lcom/google/android/plus1/u;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, v0, Lcom/google/android/plus1/u;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_66

    const v0, 0x7f0200f1

    .line 128
    :goto_5b
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bm;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 133
    :goto_65
    return-void

    .line 125
    :cond_66
    const v0, 0x7f0200f0

    goto :goto_5b

    .line 131
    :cond_6a
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/bm;->b()V

    goto :goto_65
.end method
