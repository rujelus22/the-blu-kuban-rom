.class public final Lcom/google/android/youtube/app/honeycomb/tablet/o;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final a:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private final h:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private i:Lcom/google/android/youtube/app/b;

.field private final j:Lcom/google/android/youtube/core/b/an;

.field private final k:Lcom/google/android/youtube/core/b/ap;

.field private final l:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final m:Lcom/google/android/youtube/core/d;

.field private n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

.field private o:Lcom/google/android/youtube/app/ui/dw;

.field private p:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    const/4 v4, 0x0

    const-string v5, "yt_home"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->j:Lcom/google/android/youtube/core/b/an;

    .line 59
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->q:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/google/android/youtube/app/b;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->q:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/b;-><init>(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->i:Lcom/google/android/youtube/app/b;

    .line 61
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->k:Lcom/google/android/youtube/core/b/ap;

    .line 62
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->h:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 63
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->p:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 64
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->l:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 65
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->m:Lcom/google/android/youtube/core/d;

    .line 66
    invoke-static {p2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->a:Lcom/google/android/youtube/app/compat/SupportActionBar;

    .line 67
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/o;)Lcom/google/android/youtube/app/YouTubePlatformUtil;
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->p:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    return-object v0
.end method

.method private f()V
    .registers 14

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->g:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/tablet/g;->a(Landroid/content/Context;Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;)V

    .line 83
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/p;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->g:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->d:Lcom/google/android/youtube/app/a;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->g:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->i:Lcom/google/android/youtube/app/b;

    invoke-virtual {v5}, Lcom/google/android/youtube/app/b;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    invoke-virtual {v6}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->k()I

    move-result v6

    invoke-static {v1, v5, v6}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/app/Activity;Ljava/util/concurrent/ConcurrentHashMap;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->i:Lcom/google/android/youtube/app/b;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->j:Lcom/google/android/youtube/core/b/an;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->k:Lcom/google/android/youtube/core/b/ap;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v9

    sget-object v10, Lcom/google/android/youtube/core/Analytics$VideoCategory;->HomeFeed:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v11, Lcom/google/android/youtube/app/m;->A:Lcom/google/android/youtube/core/b/aq;

    iget-object v12, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->m:Lcom/google/android/youtube/core/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/youtube/app/honeycomb/tablet/p;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/o;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->o:Lcom/google/android/youtube/app/ui/dw;

    .line 117
    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->o:Lcom/google/android/youtube/app/ui/dw;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->l:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-static {}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a()Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 165
    return-void
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 71
    const v0, 0x7f0400db

    return v0
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 77
    const v0, 0x7f0800bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    .line 78
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/o;->f()V

    .line 79
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->o:Lcom/google/android/youtube/app/ui/dw;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->l:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 149
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/o;->i()V

    .line 157
    return-void
.end method

.method protected final b()V
    .registers 4

    .prologue
    .line 121
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->q:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->q:Ljava/lang/String;

    .line 124
    new-instance v0, Lcom/google/android/youtube/app/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->q:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/b;-><init>(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->i:Lcom/google/android/youtube/app/b;

    .line 125
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/o;->f()V

    .line 127
    :cond_2b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->a:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->m()V

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->h:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 131
    return-void
.end method

.method protected final c()V
    .registers 3

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->a:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->o:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->l()V

    .line 138
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 139
    return-void
.end method

.method protected final d()V
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/o;->n:Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/ui/PagedCarouselView;->n()V

    .line 144
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->d()V

    .line 145
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/o;->i()V

    .line 153
    return-void
.end method
