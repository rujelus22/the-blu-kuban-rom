.class public final Lcom/google/android/youtube/app/a/c;
.super Lcom/google/android/youtube/core/b/af;
.source "SourceFile"


# instance fields
.field private final k:Landroid/content/SharedPreferences;

.field private final l:Lcom/google/android/youtube/app/a/a;

.field private m:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/utils/d;Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a/a;IZ)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move/from16 v6, p8

    move/from16 v7, p9

    .line 42
    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/core/b/af;-><init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZ)V

    .line 43
    const-string v0, "preferences cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/c;->k:Landroid/content/SharedPreferences;

    .line 44
    const-string v0, "prefetchPlaybacksTracker cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/app/a/c;->l:Lcom/google/android/youtube/app/a/a;

    .line 46
    return-void
.end method


# virtual methods
.method protected final a(Landroid/net/Uri$Builder;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->k:Landroid/content/SharedPreferences;

    const-string v1, "prefetch_subscriptions"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->k:Landroid/content/SharedPreferences;

    const-string v1, "prefetch_watch_later"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 60
    :cond_15
    const-string v1, "preloaded"

    iget-boolean v0, p0, Lcom/google/android/youtube/app/a/c;->g:Z

    if-eqz v0, :cond_28

    iget-boolean v0, p0, Lcom/google/android/youtube/app/a/c;->m:Z

    if-eqz v0, :cond_25

    const-string v0, "1"

    :goto_21
    invoke-virtual {p1, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 65
    :cond_24
    return-void

    .line 60
    :cond_25
    const-string v0, "2"

    goto :goto_21

    :cond_28
    const-string v0, "0"

    goto :goto_21
.end method

.method protected final a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->l:Lcom/google/android/youtube/app/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/a/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_9
    iput-boolean v0, p0, Lcom/google/android/youtube/app/a/c;->m:Z

    .line 51
    iget-boolean v0, p0, Lcom/google/android/youtube/app/a/c;->m:Z

    if-eqz v0, :cond_14

    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/a/c;->l:Lcom/google/android/youtube/app/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/a/a;->b(Ljava/lang/String;)V

    .line 54
    :cond_14
    return-void

    .line 50
    :cond_15
    const/4 v0, 0x0

    goto :goto_9
.end method
