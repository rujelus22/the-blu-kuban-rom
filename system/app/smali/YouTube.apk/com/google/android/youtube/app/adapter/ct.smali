.class public final Lcom/google/android/youtube/app/adapter/ct;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private b:Landroid/graphics/Typeface;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ct;->c:Z

    .line 34
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/ct;->a:Landroid/content/res/Resources;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/ct;)Landroid/graphics/Typeface;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ct;->b:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/ct;)Z
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/youtube/app/adapter/ct;->c:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/ct;)Landroid/content/res/Resources;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ct;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/youtube/app/adapter/cu;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/cu;-><init>(Lcom/google/android/youtube/app/adapter/ct;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method public final a(Landroid/graphics/Typeface;)V
    .registers 2
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/ct;->b:Landroid/graphics/Typeface;

    .line 44
    return-void
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/youtube/app/adapter/ct;->c:Z

    .line 47
    return-void
.end method
