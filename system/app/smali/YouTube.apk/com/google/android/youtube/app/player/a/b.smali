.class final Lcom/google/android/youtube/app/player/a/b;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/File;

.field private final b:J

.field private final c:J

.field private final d:Ljava/security/Key;


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;JJLjava/security/Key;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 42
    const-string v0, "file cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/b;->a:Ljava/io/File;

    .line 43
    cmp-long v0, p3, p5

    if-gtz v0, :cond_27

    const/4 v0, 0x1

    :goto_12
    const-string v1, "begin must be less than or equal to end"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 44
    iput-wide p3, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    .line 45
    iput-wide p5, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    .line 46
    iput-object p7, p0, Lcom/google/android/youtube/app/player/a/b;->d:Ljava/security/Key;

    .line 47
    const-string v0, "contentType cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/player/a/b;->setContentType(Ljava/lang/String;)V

    .line 48
    return-void

    .line 43
    :cond_27
    const/4 v0, 0x0

    goto :goto_12
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .registers 8

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/b;->d:Ljava/security/Key;

    if-eqz v0, :cond_12

    .line 71
    new-instance v0, Lcom/google/android/youtube/app/player/a/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/b;->a:Ljava/io/File;

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    iget-wide v4, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    iget-object v6, p0, Lcom/google/android/youtube/app/player/a/b;->d:Ljava/security/Key;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/player/a/a;-><init>(Ljava/io/File;JJLjava/security/Key;)V

    .line 73
    :goto_11
    return-object v0

    :cond_12
    new-instance v0, Lcom/google/android/youtube/app/player/a/c;

    iget-object v1, p0, Lcom/google/android/youtube/app/player/a/b;->a:Ljava/io/File;

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    iget-wide v4, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/player/a/c;-><init>(Ljava/io/File;JJ)V

    goto :goto_11
.end method

.method public final getContentLength()J
    .registers 5

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/google/android/youtube/app/player/a/b;->c:J

    iget-wide v2, p0, Lcom/google/android/youtube/app/player/a/b;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final isRepeatable()Z
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .registers 2

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/youtube/app/player/a/b;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/a/a;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 67
    return-void
.end method
