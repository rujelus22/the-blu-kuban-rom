.class public Lcom/google/android/youtube/core/model/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/i;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Landroid/net/Uri;

.field private g:Landroid/net/Uri;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Ljava/util/List;

.field private n:Ljava/util/List;

.field private o:Ljava/util/List;

.field private p:Ljava/util/List;

.field private q:Ljava/util/List;

.field private r:Ljava/util/List;

.field private s:Ljava/util/List;

.field private t:Ljava/util/List;

.field private u:Ljava/util/List;

.field private v:Ljava/util/List;

.field private w:Z

.field private x:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/q;
    .registers 2

    .prologue
    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/q;->x:Z

    .line 368
    return-object p0
.end method

.method public final a(I)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 467
    iput p1, p0, Lcom/google/android/youtube/core/model/q;->e:I

    .line 468
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    if-nez v0, :cond_b

    .line 309
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    .line 311
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->c:Ljava/lang/String;

    .line 453
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    if-nez v0, :cond_b

    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    .line 319
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 320
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 562
    iput-boolean p1, p0, Lcom/google/android/youtube/core/model/q;->w:Z

    .line 563
    return-object p0
.end method

.method public final b()Lcom/google/android/youtube/core/model/VastAd;
    .registers 26

    .prologue
    .line 567
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_13

    .line 569
    :cond_10
    sget-object v1, Lcom/google/android/youtube/core/model/VastAd;->EMPTY_AD:Lcom/google/android/youtube/core/model/VastAd;

    .line 581
    :goto_12
    return-object v1

    .line 571
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/youtube/core/model/q;->x:Z

    if-eqz v1, :cond_49

    .line 575
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    if-eqz v1, :cond_49

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_49

    .line 576
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 577
    const-string v2, "eid\\d=\\d+"

    const-string v3, "eid1=5"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 578
    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/q;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;

    .line 581
    :cond_49
    new-instance v1, Lcom/google/android/youtube/core/model/VastAd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/core/model/q;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/core/model/q;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/core/model/q;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/youtube/core/model/q;->e:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/core/model/q;->f:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/core/model/q;->g:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/core/model/q;->i:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/core/model/q;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/core/model/q;->k:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/core/model/q;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/core/model/q;->m:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/core/model/q;->n:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->o:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->p:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->q:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->r:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->s:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->t:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->u:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/q;->v:Ljava/util/List;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/q;->w:Z

    move/from16 v24, v0

    invoke-direct/range {v1 .. v24}, Lcom/google/android/youtube/core/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    goto/16 :goto_12
.end method

.method public final b(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    if-nez v0, :cond_b

    .line 325
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    .line 327
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 457
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->b:Ljava/lang/String;

    .line 458
    return-object p0
.end method

.method public final b(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 482
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->a:Ljava/util/List;

    .line 483
    return-object p0
.end method

.method public synthetic build()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/q;->b()Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->i:Ljava/util/List;

    if-nez v0, :cond_b

    .line 333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->i:Ljava/util/List;

    .line 335
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 462
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->d:Ljava/lang/String;

    .line 463
    return-object p0
.end method

.method public final c(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 487
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->h:Ljava/util/List;

    .line 488
    return-object p0
.end method

.method public final d(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->j:Ljava/util/List;

    if-nez v0, :cond_b

    .line 341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->j:Ljava/util/List;

    .line 343
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    return-object p0
.end method

.method public final d(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 492
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->i:Ljava/util/List;

    .line 493
    return-object p0
.end method

.method public final e(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->k:Ljava/util/List;

    if-nez v0, :cond_b

    .line 349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->k:Ljava/util/List;

    .line 351
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    return-object p0
.end method

.method public final e(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 497
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->j:Ljava/util/List;

    .line 498
    return-object p0
.end method

.method public final f(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->l:Ljava/util/List;

    if-nez v0, :cond_b

    .line 357
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->l:Ljava/util/List;

    .line 359
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    return-object p0
.end method

.method public final f(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 502
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->k:Ljava/util/List;

    .line 503
    return-object p0
.end method

.method public final g(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->m:Ljava/util/List;

    if-nez v0, :cond_b

    .line 373
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->m:Ljava/util/List;

    .line 375
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    return-object p0
.end method

.method public final g(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 507
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->l:Ljava/util/List;

    .line 508
    return-object p0
.end method

.method public final h(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->n:Ljava/util/List;

    if-nez v0, :cond_b

    .line 381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->n:Ljava/util/List;

    .line 383
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    return-object p0
.end method

.method public final h(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 512
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->m:Ljava/util/List;

    .line 513
    return-object p0
.end method

.method public final i(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->o:Ljava/util/List;

    if-nez v0, :cond_b

    .line 389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->o:Ljava/util/List;

    .line 391
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    return-object p0
.end method

.method public final i(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 517
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->n:Ljava/util/List;

    .line 518
    return-object p0
.end method

.method public final j(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->p:Ljava/util/List;

    if-nez v0, :cond_b

    .line 397
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->p:Ljava/util/List;

    .line 399
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    return-object p0
.end method

.method public final j(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 522
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->o:Ljava/util/List;

    .line 523
    return-object p0
.end method

.method public final k(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->q:Ljava/util/List;

    if-nez v0, :cond_b

    .line 405
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->q:Ljava/util/List;

    .line 407
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408
    return-object p0
.end method

.method public final k(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 527
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->p:Ljava/util/List;

    .line 528
    return-object p0
.end method

.method public final l(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->r:Ljava/util/List;

    if-nez v0, :cond_b

    .line 413
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->r:Ljava/util/List;

    .line 415
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    return-object p0
.end method

.method public final l(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 532
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->q:Ljava/util/List;

    .line 533
    return-object p0
.end method

.method public final m(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->s:Ljava/util/List;

    if-nez v0, :cond_b

    .line 421
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->s:Ljava/util/List;

    .line 423
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    return-object p0
.end method

.method public final m(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 537
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->r:Ljava/util/List;

    .line 538
    return-object p0
.end method

.method public final n(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->t:Ljava/util/List;

    if-nez v0, :cond_b

    .line 429
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->t:Ljava/util/List;

    .line 431
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    return-object p0
.end method

.method public final n(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 542
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->s:Ljava/util/List;

    .line 543
    return-object p0
.end method

.method public final o(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->u:Ljava/util/List;

    if-nez v0, :cond_b

    .line 437
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->u:Ljava/util/List;

    .line 439
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    return-object p0
.end method

.method public final o(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 547
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->t:Ljava/util/List;

    .line 548
    return-object p0
.end method

.method public final p(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 3
    .parameter

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->v:Ljava/util/List;

    if-nez v0, :cond_b

    .line 445
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/q;->v:Ljava/util/List;

    .line 447
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/core/model/q;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    return-object p0
.end method

.method public final p(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 552
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->u:Ljava/util/List;

    .line 553
    return-object p0
.end method

.method public final q(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 472
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->f:Landroid/net/Uri;

    .line 473
    return-object p0
.end method

.method public final q(Ljava/util/List;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 557
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->v:Ljava/util/List;

    .line 558
    return-object p0
.end method

.method public final r(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/q;
    .registers 2
    .parameter

    .prologue
    .line 477
    iput-object p1, p0, Lcom/google/android/youtube/core/model/q;->g:Landroid/net/Uri;

    .line 478
    return-object p0
.end method
