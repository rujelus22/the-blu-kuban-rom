.class public final Lcom/google/android/youtube/core/utils/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Ljava/lang/String;

.field public final c:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/util/List;Ljava/lang/String;I)V

    .line 39
    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    .line 32
    iget-object v0, p0, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    :goto_18
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/o;->a(Z)V

    .line 33
    iput-object p2, p0, Lcom/google/android/youtube/core/utils/ag;->b:Ljava/lang/String;

    .line 34
    iput p3, p0, Lcom/google/android/youtube/core/utils/ag;->c:I

    .line 35
    return-void

    .line 32
    :cond_20
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private static a(Ljava/lang/String;)I
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 177
    if-nez p0, :cond_4

    .line 203
    :goto_3
    return v1

    .line 184
    :cond_4
    const-string v0, "h"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 185
    if-ltz v2, :cond_58

    .line 186
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;I)I

    move-result v0

    const v3, 0x36ee80

    mul-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x0

    .line 187
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 191
    :goto_20
    const-string v2, "m"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 192
    if-ltz v2, :cond_3b

    .line 193
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;I)I

    move-result v3

    const v4, 0xea60

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 194
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 198
    :cond_3b
    const-string v2, "s"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4d

    .line 199
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 201
    :cond_4d
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;F)F

    move-result v1

    const/high16 v2, 0x447a

    mul-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v1, v0

    .line 203
    goto :goto_3

    :cond_58
    move v0, v1

    goto :goto_20
.end method

.method public static a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    .line 58
    const-string v0, "https"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 59
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 61
    :cond_1c
    const-string v0, "vnd.youtube"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 62
    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v2, "//"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_38
    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_48

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_48
    invoke-virtual {p0}, Landroid/net/Uri;->isOpaque()Z

    move-result v2

    if-eqz v2, :cond_55

    :goto_4e
    new-instance v2, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v0, v2

    .line 70
    :goto_54
    return-object v0

    .line 62
    :cond_55
    const-string v1, "t"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/ag;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_4e

    .line 64
    :cond_60
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 65
    const-string v2, "/watch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_74

    const-string v2, "/movie"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13a

    .line 66
    :cond_74
    const-string v0, "v"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "video_ids"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v2, :cond_8c

    if-nez v3, :cond_8c

    new-instance v0, Ljava/text/ParseException;

    const-string v2, "No video id in URI"

    invoke-direct {v0, v2, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_8c
    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v4

    const-string v0, "t"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_aa

    invoke-virtual {p0}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_aa

    const-string v6, "t="

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_aa

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_aa
    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ag;->a(Ljava/lang/String;)I

    move-result v5

    if-nez v3, :cond_d5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "v="

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_bf
    if-eqz v4, :cond_cb

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v6, v7, :cond_e5

    :cond_cb
    const/4 v0, 0x0

    move-object v1, v0

    :goto_cd
    if-nez v3, :cond_129

    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0, v2, v1, v5}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_54

    :cond_d5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "video_ids="

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_bf

    :cond_e5
    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int v7, v6, v0

    const-string v0, ""

    if-lez v6, :cond_108

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_108
    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_127

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_127
    move-object v1, v0

    goto :goto_cd

    :cond_129
    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    const-string v2, ","

    invoke-virtual {v3, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2, v1, v5}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/util/List;Ljava/lang/String;I)V

    goto/16 :goto_54

    .line 67
    :cond_13a
    const-string v2, "/v/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_148

    .line 68
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/ag;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;

    move-result-object v0

    goto/16 :goto_54

    .line 69
    :cond_148
    const-string v2, "/e/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_158

    const-string v2, "/embed/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_171

    .line 70
    :cond_158
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v2

    const-string v0, "start"

    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ag;->a(Ljava/lang/String;)I

    move-result v3

    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_54

    .line 73
    :cond_171
    new-instance v0, Ljava/text/ParseException;

    const-string v2, "Unrecognised URI"

    invoke-direct {v0, v2, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method private static b(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-virtual {p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 140
    const-string v0, "&"

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 141
    if-ltz v0, :cond_46

    .line 142
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 143
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 147
    const-string v0, "&"

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 148
    const-string v5, "start="

    move v0, v1

    .line 149
    :goto_24
    array-length v6, v4

    if-ge v0, v6, :cond_3d

    .line 150
    aget-object v6, v4, v0

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_43

    .line 151
    aget-object v0, v4, v0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ag;->a(Ljava/lang/String;)I

    move-result v1

    .line 155
    :cond_3d
    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 157
    :goto_42
    return-object v0

    .line 149
    :cond_43
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 157
    :cond_46
    new-instance v0, Lcom/google/android/youtube/core/utils/ag;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/youtube/core/utils/ag;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_42
.end method
