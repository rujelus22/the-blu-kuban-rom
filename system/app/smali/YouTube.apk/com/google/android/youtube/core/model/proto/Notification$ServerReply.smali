.class public final Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/aa;


# static fields
.field public static final ERROR_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah;

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 4131
    new-instance v0, Lcom/google/android/youtube/core/model/proto/x;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/x;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    .line 4444
    new-instance v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;-><init>(Z)V

    .line 4445
    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    sget-object v1, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->BAD_TOKEN:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    iput-object v1, v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    .line 4446
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 4093
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4222
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedIsInitialized:B

    .line 4239
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedSerializedSize:I

    .line 4094
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->BAD_TOKEN:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    .line 4095
    const/4 v0, 0x0

    .line 4098
    :cond_e
    :goto_e
    if-nez v0, :cond_3f

    .line 4099
    :try_start_10
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v2

    .line 4100
    sparse-switch v2, :sswitch_data_52

    .line 4105
    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 4107
    goto :goto_e

    :sswitch_1f
    move v0, v1

    .line 4103
    goto :goto_e

    .line 4112
    :sswitch_21
    invoke-virtual {p1}, Lcom/google/protobuf/h;->f()I

    move-result v2

    .line 4113
    invoke-static {v2}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->valueOf(I)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    move-result-object v2

    .line 4114
    if-eqz v2, :cond_e

    .line 4115
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I

    .line 4116
    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;
    :try_end_33
    .catchall {:try_start_10 .. :try_end_33} :catchall_3a
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_10 .. :try_end_33} :catch_34
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_33} :catch_43

    goto :goto_e

    .line 4122
    :catch_34
    move-exception v0

    .line 4123
    :try_start_35
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_3a
    .catchall {:try_start_35 .. :try_end_3a} :catchall_3a

    .line 4128
    :catchall_3a
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->makeExtensionsImmutable()V

    throw v0

    :cond_3f
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->makeExtensionsImmutable()V

    .line 4129
    return-void

    .line 4124
    :catch_43
    move-exception v0

    .line 4125
    :try_start_44
    new-instance v1, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_52
    .catchall {:try_start_44 .. :try_end_52} :catchall_3a

    .line 4100
    :sswitch_data_52
    .sparse-switch
        0x0 -> :sswitch_1f
        0x8 -> :sswitch_21
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4071
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4076
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 4222
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedIsInitialized:B

    .line 4239
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedSerializedSize:I

    .line 4078
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4071
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 4079
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 4222
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedIsInitialized:B

    .line 4239
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedSerializedSize:I

    .line 4079
    return-void
.end method

.method static synthetic access$3702(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4071
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    return-object p1
.end method

.method static synthetic access$3802(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4071
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 1

    .prologue
    .line 4083
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 4220
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->BAD_TOKEN:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    .line 4221
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/y;
    .registers 1

    .prologue
    .line 4313
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/y;->a()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;
    .registers 2
    .parameter

    .prologue
    .line 4316
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->newBuilder()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 2
    .parameter

    .prologue
    .line 4293
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4299
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 2
    .parameter

    .prologue
    .line 4263
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4269
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 2
    .parameter

    .prologue
    .line 4304
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4310
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 2
    .parameter

    .prologue
    .line 4283
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4289
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 2
    .parameter

    .prologue
    .line 4273
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4279
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method


# virtual methods
.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 2

    .prologue
    .line 4087
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4071
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v0

    return-object v0
.end method

.method public final getError()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;
    .registers 2

    .prologue
    .line 4216
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    return-object v0
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 4143
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 4241
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedSerializedSize:I

    .line 4242
    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    .line 4250
    :goto_6
    return v0

    .line 4244
    :cond_7
    const/4 v0, 0x0

    .line 4245
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1f

    .line 4246
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->getNumber()I

    move-result v0

    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v1

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->c(I)I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    .line 4249
    :cond_1f
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedSerializedSize:I

    goto :goto_6
.end method

.method public final hasError()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 4210
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 4224
    iget-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedIsInitialized:B

    .line 4225
    const/4 v2, -0x1

    if-eq v1, v2, :cond_b

    if-ne v1, v0, :cond_9

    .line 4228
    :goto_8
    return v0

    .line 4225
    :cond_9
    const/4 v0, 0x0

    goto :goto_8

    .line 4227
    :cond_b
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->memoizedIsInitialized:B

    goto :goto_8
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/y;
    .registers 2

    .prologue
    .line 4314
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->newBuilder()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 4071
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/y;
    .registers 2

    .prologue
    .line 4318
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 4071
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->toBuilder()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4257
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 4233
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->getSerializedSize()I

    .line 4234
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_17

    .line 4235
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->getNumber()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->d(II)V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedOutputStream;->b(I)V

    .line 4237
    :cond_17
    return-void
.end method
