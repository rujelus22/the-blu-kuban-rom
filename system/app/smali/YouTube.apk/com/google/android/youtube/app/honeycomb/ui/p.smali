.class public final Lcom/google/android/youtube/app/honeycomb/ui/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/aj;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/b/al;

.field private final c:Lcom/google/android/youtube/core/async/l;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/view/View;

.field private final l:Landroid/view/View;

.field private m:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private final q:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Landroid/app/Activity;

    .line 62
    const-string v0, "gDataClient can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->b:Lcom/google/android/youtube/core/b/al;

    .line 63
    const-string v0, "userAuthorizer can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 65
    const-string v0, "analytics can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->q:Lcom/google/android/youtube/core/Analytics;

    .line 67
    const v0, 0x7f080153

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const-string v1, "activity must contain a remote_control_bar layout"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->k:Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f080129

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->l:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f080046

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->g:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f080040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->f:Landroid/widget/ImageView;

    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f08012b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->j:Landroid/widget/TextView;

    .line 78
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/v;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p3, v1}, Lcom/google/android/youtube/app/honeycomb/ui/v;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/b/an;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->c:Lcom/google/android/youtube/core/async/l;

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f08012e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    .line 81
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const v1, 0x7f08012d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->i:Landroid/widget/ImageView;

    .line 83
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/q;

    invoke-direct {v1, p0, p5, p6}, Lcom/google/android/youtube/app/honeycomb/ui/q;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/r;

    invoke-direct {v1, p0, p6}, Lcom/google/android/youtube/app/honeycomb/ui/r;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/Analytics;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->i:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/s;

    invoke-direct {v1, p0, p6}, Lcom/google/android/youtube/app/honeycomb/ui/s;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/Analytics;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/p;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/ui/p;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/ui/p;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/ui/p;)Z
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->n:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/async/l;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->c:Lcom/google/android/youtube/core/async/l;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/ui/p;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->b:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->l:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_9

    .line 203
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/aj;)V

    .line 205
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 207
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .registers 7
    .parameter

    .prologue
    const v4, 0x7f02018c

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 128
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4a

    .line 149
    :goto_10
    return-void

    .line 130
    :pswitch_11
    iput-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->n:Z

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    const v1, 0x7f02018b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_10

    .line 135
    :pswitch_21
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->n:Z

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_10

    .line 140
    :pswitch_2e
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->n:Z

    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    const v1, 0x7f02018d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_10

    .line 147
    :pswitch_3e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_10

    .line 128
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_11
        :pswitch_21
        :pswitch_2e
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .registers 2
    .parameter

    .prologue
    .line 124
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 4
    .parameter

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_9

    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/aj;)V

    .line 193
    :cond_9
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 194
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_23

    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/aj;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->e:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Ljava/lang/String;)V

    .line 199
    :cond_23
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->o:Ljava/lang/String;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->o:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :cond_18
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->o:Ljava/lang/String;

    .line 164
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->l:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 167
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->a:Landroid/app/Activity;

    const v2, 0x7f0b023b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    :goto_4d
    return-void

    .line 171
    :cond_4e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/ui/u;

    invoke-direct {v1, p0, p1, v5}, Lcom/google/android/youtube/app/honeycomb/ui/u;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Ljava/lang/String;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto :goto_4d
.end method

.method public final a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->i:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->m:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 179
    return-void
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 187
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/p;->p:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public final l()V
    .registers 1

    .prologue
    .line 183
    return-void
.end method
