.class public final Lcom/google/android/youtube/app/ui/fa;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/dq;


# static fields
.field private static final a:Lcom/google/android/youtube/core/a/g;

.field private static final c:Lcom/google/android/youtube/core/a/g;

.field private static final d:Lcom/google/android/youtube/core/a/g;

.field private static final e:Lcom/google/android/youtube/core/a/g;

.field private static final f:Lcom/google/android/youtube/core/a/g;

.field private static final g:Lcom/google/android/youtube/core/a/g;


# instance fields
.field private final h:Lcom/google/android/youtube/app/ui/eu;

.field private final i:Lcom/google/android/youtube/app/ui/fh;

.field private final j:Lcom/google/android/youtube/app/ui/ek;

.field private final k:Lcom/google/android/youtube/app/ui/ef;

.field private final l:Lcom/google/android/youtube/app/ui/eg;

.field private final m:Lcom/google/android/youtube/app/ui/fe;

.field private final n:Landroid/app/Activity;

.field private final o:Lcom/google/android/youtube/core/d;

.field private final p:Lcom/google/android/youtube/core/b/ap;

.field private q:Lcom/google/android/youtube/core/model/MusicVideo;

.field private final r:Lcom/google/android/youtube/app/ui/fd;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "Heading"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/fa;->a:Lcom/google/android/youtube/core/a/g;

    .line 52
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "RelatedVideo"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/fa;->c:Lcom/google/android/youtube/core/a/g;

    .line 53
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "RelatedVideoList"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/fa;->d:Lcom/google/android/youtube/core/a/g;

    .line 54
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "Comment"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/fa;->e:Lcom/google/android/youtube/core/a/g;

    .line 55
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "ArtistTrackList"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/fa;->f:Lcom/google/android/youtube/core/a/g;

    .line 56
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "RelatedArtist"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/app/ui/fa;->g:Lcom/google/android/youtube/core/a/g;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/fd;Lcom/google/android/youtube/app/ui/eu;Lcom/google/android/youtube/app/ui/fh;Lcom/google/android/youtube/app/ui/ek;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/fe;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 187
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    aput-object p5, v0, v3

    aput-object p9, v0, v2

    const/4 v1, 0x2

    aput-object p10, v0, v1

    const/4 v1, 0x3

    aput-object p6, v0, v1

    const/4 v1, 0x4

    aput-object p7, v0, v1

    const/4 v1, 0x5

    aput-object p8, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 194
    const-string v0, "infoOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/eu;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->h:Lcom/google/android/youtube/app/ui/eu;

    .line 195
    const-string v0, "relatedVideosOutline cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/fh;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->i:Lcom/google/android/youtube/app/ui/fh;

    .line 197
    const-string v0, "commentsOutline cannot be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ek;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->j:Lcom/google/android/youtube/app/ui/ek;

    .line 199
    const-string v0, "artistInfoOutline cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/ef;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->k:Lcom/google/android/youtube/app/ui/ef;

    .line 201
    const-string v0, "artistTracksOutline cannot be null"

    invoke-static {p9, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/eg;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->l:Lcom/google/android/youtube/app/ui/eg;

    .line 203
    const-string v0, "relatedArtistsOutline cannot be null"

    invoke-static {p10, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/fe;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->m:Lcom/google/android/youtube/app/ui/fe;

    .line 206
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->n:Landroid/app/Activity;

    .line 207
    const-string v0, "musicClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/ap;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->p:Lcom/google/android/youtube/core/b/ap;

    .line 208
    const-string v0, "errorHelper cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->o:Lcom/google/android/youtube/core/d;

    .line 210
    const-string v0, "statusListener cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/fd;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->r:Lcom/google/android/youtube/app/ui/fd;

    .line 214
    invoke-direct {p0, v3}, Lcom/google/android/youtube/app/ui/fa;->b(Z)V

    .line 217
    invoke-virtual {p5, v3}, Lcom/google/android/youtube/app/ui/eu;->b(Z)V

    .line 218
    invoke-virtual {p6, v2}, Lcom/google/android/youtube/app/ui/fh;->a(Z)V

    .line 219
    invoke-virtual {p7, v2}, Lcom/google/android/youtube/app/ui/ek;->a(Z)V

    .line 220
    invoke-virtual {p8, v3}, Lcom/google/android/youtube/app/ui/ef;->a(Z)V

    .line 221
    invoke-virtual {p9, v2}, Lcom/google/android/youtube/app/ui/eg;->a(Z)V

    .line 222
    invoke-virtual {p10, v2}, Lcom/google/android/youtube/app/ui/fe;->a(Z)V

    .line 223
    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;
    .registers 4
    .parameter

    .prologue
    .line 326
    new-instance v0, Lcom/google/android/youtube/app/adapter/cy;

    const v1, 0x7f0400d1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/b/g;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/fd;)Lcom/google/android/youtube/app/ui/fa;
    .registers 44
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0014

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v26

    .line 90
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v27

    .line 91
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0013

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    move-object/from16 v5, p0

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p3

    move-object/from16 v10, p1

    move-object/from16 v11, p10

    move-object/from16 v12, p4

    .line 93
    invoke-static/range {v5 .. v12}, Lcom/google/android/youtube/app/ui/eu;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/b/g;)Lcom/google/android/youtube/app/ui/eu;

    move-result-object v28

    .line 103
    invoke-interface/range {p3 .. p3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v22

    sget-object v14, Lcom/google/android/youtube/app/ui/fa;->c:Lcom/google/android/youtube/core/a/g;

    sget-object v23, Lcom/google/android/youtube/app/ui/fa;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v17, Lcom/google/android/youtube/app/ui/fa;->d:Lcom/google/android/youtube/core/a/g;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v5

    if-eqz v5, :cond_2bc

    const v5, 0x7f0d0012

    :goto_4c
    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a006f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00d3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b01e8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v25

    new-instance v12, Lcom/google/android/youtube/app/adapter/ct;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    sget-object v5, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v15

    new-instance v5, Lcom/google/android/youtube/app/adapter/h;

    move-object/from16 v0, p12

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v9

    const/16 v10, 0xf

    const/4 v11, 0x1

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p11

    invoke-direct/range {v5 .. v11}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v6, Lcom/google/android/youtube/app/adapter/ax;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0074

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0074

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const/4 v10, 0x0

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/youtube/app/adapter/ax;-><init>(IIII)V

    new-instance v7, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v7}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v7, v12}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v7

    invoke-virtual {v7, v15}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v8

    if-eqz p4, :cond_d8

    new-instance v5, Lcom/google/android/youtube/app/adapter/bk;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v5, v0, v1}, Lcom/google/android/youtube/app/adapter/bk;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/b/g;)V

    invoke-virtual {v8, v5}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    :cond_d8
    new-instance v5, Lcom/google/android/youtube/app/adapter/az;

    const v7, 0x7f04002e

    invoke-interface/range {p3 .. p3}, Lcom/google/android/youtube/core/b/al;->y()Lcom/google/android/youtube/core/async/av;

    move-result-object v9

    move-object/from16 v6, p0

    move-object/from16 v10, p13

    move/from16 v11, v26

    move/from16 v12, v27

    invoke-direct/range {v5 .. v14}, Lcom/google/android/youtube/app/adapter/az;-><init>(Landroid/app/Activity;ILcom/google/android/youtube/app/adapter/cb;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;IIILcom/google/android/youtube/core/a/g;)V

    new-instance v14, Lcom/google/android/youtube/app/adapter/cq;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v16

    new-instance v20, Lcom/google/android/youtube/app/ui/fi;

    move-object/from16 v0, v20

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/ui/fi;-><init>(Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;)V

    const/16 v21, 0x0

    move-object v15, v5

    invoke-direct/range {v14 .. v21}, Lcom/google/android/youtube/app/adapter/cq;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;)V

    new-instance v21, Lcom/google/android/youtube/app/ui/b;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-direct {v0, v6, v7, v1}, Lcom/google/android/youtube/app/ui/b;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v19, Lcom/google/android/youtube/core/a/l;

    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/youtube/core/a/e;

    const/4 v7, 0x0

    aput-object v14, v6, v7

    const/4 v7, 0x1

    aput-object v21, v6, v7

    move-object/from16 v0, v19

    invoke-direct {v0, v6}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v20, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    move-object/from16 v4, v19

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;Lcom/google/android/youtube/core/a/e;)V

    new-instance v14, Lcom/google/android/youtube/app/ui/fh;

    move-object/from16 v15, p0

    move-object/from16 v16, p7

    move-object/from16 v17, v22

    move/from16 v18, v26

    move-object/from16 v22, v5

    invoke-direct/range {v14 .. v22}, Lcom/google/android/youtube/app/ui/fh;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;ILcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/app/adapter/az;)V

    .line 123
    sget-object v24, Lcom/google/android/youtube/app/ui/fa;->e:Lcom/google/android/youtube/core/a/g;

    sget-object v25, Lcom/google/android/youtube/app/ui/fa;->a:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v15, p0

    move-object/from16 v16, p8

    move-object/from16 v17, p9

    move-object/from16 v18, p5

    move-object/from16 v19, p3

    move-object/from16 v20, p13

    move/from16 v21, v26

    move/from16 v22, v27

    move/from16 v23, v13

    invoke-static/range {v15 .. v25}, Lcom/google/android/youtube/app/ui/ek;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ek;

    move-result-object v15

    .line 136
    sget-object v5, Lcom/google/android/youtube/app/ui/fa;->a:Lcom/google/android/youtube/core/a/g;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f04000b

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    new-instance v7, Lcom/google/android/youtube/app/adapter/cy;

    invoke-direct {v7, v6}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    new-instance v8, Lcom/google/android/youtube/app/adapter/ac;

    const v9, 0x7f0b01cc

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v5, v9, v7}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;Lcom/google/android/youtube/core/a/e;)V

    new-instance v16, Lcom/google/android/youtube/app/ui/ef;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v8, v7}, Lcom/google/android/youtube/app/ui/ef;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/app/adapter/cy;)V

    .line 140
    sget-object v17, Lcom/google/android/youtube/app/ui/fa;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v8, Lcom/google/android/youtube/app/ui/fa;->f:Lcom/google/android/youtube/core/a/g;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d000f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a006f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b01e8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    new-instance v5, Lcom/google/android/youtube/app/adapter/a;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v5, v0, v1}, Lcom/google/android/youtube/app/adapter/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;)V

    new-instance v12, Lcom/google/android/youtube/app/adapter/bt;

    const v6, 0x7f04000c

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v6, v5}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    new-instance v19, Lcom/google/android/youtube/app/adapter/bc;

    move-object/from16 v0, v19

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v12, v1, v2, v13}, Lcom/google/android/youtube/app/adapter/bc;-><init>(Landroid/widget/BaseAdapter;III)V

    new-instance v6, Lcom/google/android/youtube/core/a/c;

    const/4 v5, 0x1

    const/4 v7, 0x0

    new-array v7, v7, [Lcom/google/android/youtube/core/a/g;

    move-object/from16 v0, v19

    invoke-direct {v6, v0, v5, v7}, Lcom/google/android/youtube/core/a/c;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V

    new-instance v5, Lcom/google/android/youtube/app/adapter/cq;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    new-instance v11, Lcom/google/android/youtube/app/ui/eh;

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {v11, v12, v0, v1}, Lcom/google/android/youtube/app/ui/eh;-><init>(Lcom/google/android/youtube/app/adapter/bt;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;)V

    const/4 v12, 0x0

    invoke-direct/range {v5 .. v12}, Lcom/google/android/youtube/app/adapter/cq;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;)V

    new-instance v11, Lcom/google/android/youtube/app/ui/b;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-direct {v11, v6, v7, v0}, Lcom/google/android/youtube/app/ui/b;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v10, Lcom/google/android/youtube/core/a/l;

    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/youtube/core/a/e;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    const/4 v5, 0x1

    aput-object v11, v6, v5

    invoke-direct {v10, v6}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v9, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v9, v0, v1, v10}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/e;)V

    new-instance v5, Lcom/google/android/youtube/app/ui/eg;

    move-object/from16 v6, p0

    move-object/from16 v7, p3

    move-object/from16 v8, v19

    invoke-direct/range {v5 .. v11}, Lcom/google/android/youtube/app/ui/eg;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/adapter/bc;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/b;)V

    .line 152
    sget-object v17, Lcom/google/android/youtube/app/ui/fa;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v9, Lcom/google/android/youtube/app/ui/fa;->g:Lcom/google/android/youtube/core/a/g;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a006f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b01ce

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b01e8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    new-instance v20, Lcom/google/android/youtube/app/adapter/bn;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/bn;-><init>(Landroid/content/Context;)V

    new-instance v21, Lcom/google/android/youtube/app/adapter/bc;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-direct {v0, v1, v2, v3, v13}, Lcom/google/android/youtube/app/adapter/bc;-><init>(Landroid/widget/BaseAdapter;III)V

    new-instance v7, Lcom/google/android/youtube/core/a/c;

    const/4 v6, 0x1

    const/4 v8, 0x0

    new-array v8, v8, [Lcom/google/android/youtube/core/a/g;

    move-object/from16 v0, v21

    invoke-direct {v7, v0, v6, v8}, Lcom/google/android/youtube/core/a/c;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V

    new-instance v6, Lcom/google/android/youtube/app/adapter/cq;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    new-instance v12, Lcom/google/android/youtube/app/ui/ff;

    move-object/from16 v0, v20

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-direct {v12, v0, v1, v2}, Lcom/google/android/youtube/app/ui/ff;-><init>(Lcom/google/android/youtube/app/adapter/bn;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;)V

    const/4 v13, 0x0

    invoke-direct/range {v6 .. v13}, Lcom/google/android/youtube/app/adapter/cq;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/cr;Lcom/google/android/youtube/app/adapter/cs;)V

    new-instance v11, Lcom/google/android/youtube/app/ui/b;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const/4 v8, 0x0

    move-object/from16 v0, v19

    invoke-direct {v11, v7, v8, v0}, Lcom/google/android/youtube/app/ui/b;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v10, Lcom/google/android/youtube/core/a/l;

    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/youtube/core/a/e;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    const/4 v6, 0x1

    aput-object v11, v7, v6

    invoke-direct {v10, v7}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v9, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v9, v0, v1, v2, v10}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;Lcom/google/android/youtube/core/a/e;)V

    new-instance v6, Lcom/google/android/youtube/app/ui/fe;

    move-object/from16 v7, p0

    move-object/from16 v8, v21

    invoke-direct/range {v6 .. v11}, Lcom/google/android/youtube/app/ui/fe;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/adapter/bc;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/b;)V

    .line 162
    new-instance v8, Lcom/google/android/youtube/app/ui/fa;

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-object/from16 v11, p13

    move-object/from16 v12, p14

    move-object/from16 v13, v28

    move-object/from16 v17, v5

    move-object/from16 v18, v6

    invoke-direct/range {v8 .. v18}, Lcom/google/android/youtube/app/ui/fa;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/fd;Lcom/google/android/youtube/app/ui/eu;Lcom/google/android/youtube/app/ui/fh;Lcom/google/android/youtube/app/ui/ek;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/fe;)V

    return-object v8

    .line 103
    :cond_2bc
    const v5, 0x7f0d0011

    goto/16 :goto_4c
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fd;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->r:Lcom/google/android/youtube/app/ui/fd;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/fa;Lcom/google/android/youtube/core/model/MusicVideo;)Lcom/google/android/youtube/core/model/MusicVideo;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/fa;->q:Lcom/google/android/youtube/core/model/MusicVideo;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/fa;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->p:Lcom/google/android/youtube/core/b/ap;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fa;->n:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/fc;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/ui/fc;-><init>(Lcom/google/android/youtube/app/ui/fa;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/b/ap;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/fa;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/fa;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/ef;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->k:Lcom/google/android/youtube/app/ui/ef;

    return-object v0
.end method

.method private b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->k:Lcom/google/android/youtube/app/ui/ef;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ef;->c(Z)V

    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->l:Lcom/google/android/youtube/app/ui/eg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eg;->c(Z)V

    .line 332
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->m:Lcom/google/android/youtube/app/ui/fe;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fe;->c(Z)V

    .line 333
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/eg;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->l:Lcom/google/android/youtube/app/ui/eg;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/core/model/MusicVideo;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->q:Lcom/google/android/youtube/core/model/MusicVideo;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/app/ui/fe;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->m:Lcom/google/android/youtube/app/ui/fe;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/fa;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->o:Lcom/google/android/youtube/core/d;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/di;)V
    .registers 3
    .parameter

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->h:Lcom/google/android/youtube/app/ui/eu;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eu;->a(Lcom/google/android/youtube/app/ui/di;)V

    .line 319
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 3
    .parameter

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->h:Lcom/google/android/youtube/app/ui/eu;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eu;->a(Lcom/google/android/youtube/core/model/Branding;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->i:Lcom/google/android/youtube/app/ui/fh;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fh;->a(Lcom/google/android/youtube/core/model/Branding;)V

    .line 311
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter

    .prologue
    .line 233
    const-string v0, "video cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->q:Lcom/google/android/youtube/core/model/MusicVideo;

    .line 235
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/fa;->b(Z)V

    .line 236
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->h:Lcom/google/android/youtube/app/ui/eu;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eu;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->i:Lcom/google/android/youtube/app/ui/fh;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fh;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->j:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 239
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->couldBeMusicVideo()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 241
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/fa;->p:Lcom/google/android/youtube/core/b/ap;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/fa;->n:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/fb;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/ui/fb;-><init>(Lcom/google/android/youtube/app/ui/fa;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/b/ap;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 246
    :goto_33
    return-void

    .line 244
    :cond_34
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->r:Lcom/google/android/youtube/app/ui/fd;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/fd;->i()V

    goto :goto_33
.end method

.method public final a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->h:Lcom/google/android/youtube/app/ui/eu;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eu;->a(Z)V

    .line 323
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 226
    iput-object v1, p0, Lcom/google/android/youtube/app/ui/fa;->q:Lcom/google/android/youtube/core/model/MusicVideo;

    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/fa;->b(Z)V

    .line 228
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->i:Lcom/google/android/youtube/app/ui/fh;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/fh;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->j:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ek;->i()V

    .line 230
    return-void
.end method

.method public final m_()Landroid/app/Dialog;
    .registers 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/fa;->j:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ek;->j()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
