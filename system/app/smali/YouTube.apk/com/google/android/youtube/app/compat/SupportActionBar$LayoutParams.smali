.class public Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SourceFile"


# instance fields
.field public a:I


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 62
    const/4 v0, -0x2

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;-><init>(III)V

    .line 63
    return-void
.end method

.method public constructor <init>(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 52
    const/4 v0, -0x2

    invoke-direct {p0, v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 42
    iput v1, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 53
    const/16 v0, 0x13

    iput v0, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 54
    return-void
.end method

.method private constructor <init>(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 57
    const/4 v0, -0x2

    invoke-direct {p0, v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 42
    iput v1, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 58
    iput p3, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    iput v2, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 46
    sget-object v0, Lcom/google/android/youtube/b;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 47
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    .line 73
    return-void
.end method
