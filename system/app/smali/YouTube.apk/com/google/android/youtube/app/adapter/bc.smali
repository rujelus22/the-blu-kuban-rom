.class public final Lcom/google/android/youtube/app/adapter/bc;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private c:I

.field private final d:Landroid/widget/BaseAdapter;


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;III)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 32
    if-lez p2, :cond_37

    move v0, v1

    :goto_8
    const-string v3, "pageSize should be > 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 33
    iput p2, p0, Lcom/google/android/youtube/app/adapter/bc;->a:I

    .line 34
    if-ltz p3, :cond_39

    move v0, v1

    :goto_12
    const-string v3, "maxSize should >= 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 35
    iput p3, p0, Lcom/google/android/youtube/app/adapter/bc;->b:I

    .line 36
    if-ltz p4, :cond_3b

    :goto_1b
    const-string v0, "initialVisibleCount should be >= 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 37
    iput p4, p0, Lcom/google/android/youtube/app/adapter/bc;->c:I

    .line 39
    const-string v0, "wrappedAdapter cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    new-instance v1, Lcom/google/android/youtube/app/adapter/bd;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/adapter/bd;-><init>(Lcom/google/android/youtube/app/adapter/bc;)V

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 52
    return-void

    :cond_37
    move v0, v2

    .line 32
    goto :goto_8

    :cond_39
    move v0, v2

    .line 34
    goto :goto_12

    :cond_3b
    move v1, v2

    .line 36
    goto :goto_1b
.end method


# virtual methods
.method public final a()Landroid/widget/BaseAdapter;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .registers 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bc;->c:I

    if-le v0, v1, :cond_12

    iget v0, p0, Lcom/google/android/youtube/app/adapter/bc;->b:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bc;->c:I

    if-le v0, v1, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final c()Z
    .registers 3

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->b()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 125
    iget v0, p0, Lcom/google/android/youtube/app/adapter/bc;->c:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bc;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/adapter/bc;->c:I

    .line 126
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->notifyDataSetChanged()V

    .line 127
    const/4 v0, 0x1

    .line 129
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final getCount()I
    .registers 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bc;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bc;->b:I

    if-ge v0, v1, :cond_11

    :goto_10
    return v0

    :cond_11
    iget v0, p0, Lcom/google/android/youtube/app/adapter/bc;->b:I

    goto :goto_10
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 5
    .parameter

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_d

    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 66
    :cond_d
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemId(I)J
    .registers 5
    .parameter

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_d

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0

    .line 74
    :cond_d
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .registers 5
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_d

    .line 88
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    return v0

    .line 90
    :cond_d
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_d

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 82
    :cond_d
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .registers 5
    .parameter

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_d

    .line 104
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bc;->d:Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    return v0

    .line 106
    :cond_d
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " should be < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bc;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
