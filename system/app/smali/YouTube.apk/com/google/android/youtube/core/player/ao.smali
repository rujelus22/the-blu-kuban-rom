.class public final Lcom/google/android/youtube/core/player/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/ap;

.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/ap;)V
    .registers 4
    .parameter

    .prologue
    const/high16 v1, -0x8000

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ap;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    .line 40
    iput v1, p0, Lcom/google/android/youtube/core/player/ao;->b:I

    .line 41
    iput v1, p0, Lcom/google/android/youtube/core/player/ao;->c:I

    .line 42
    iput v1, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/ao;->e:I

    .line 44
    return-void
.end method

.method private static b(I)Z
    .registers 2
    .parameter

    .prologue
    .line 119
    const/16 v0, 0x4f

    if-eq p0, v0, :cond_18

    const/16 v0, 0x55

    if-eq p0, v0, :cond_18

    const/16 v0, 0x56

    if-eq p0, v0, :cond_18

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_18

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_18

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method


# virtual methods
.method public final a(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 47
    iput p1, p0, Lcom/google/android/youtube/core/player/ao;->b:I

    .line 48
    iput p2, p0, Lcom/google/android/youtube/core/player/ao;->c:I

    .line 49
    return-void
.end method

.method public final a(I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, -0x8000

    .line 100
    const/16 v2, 0x59

    if-eq p1, v2, :cond_c

    const/16 v2, 0x5a

    if-ne p1, v2, :cond_20

    .line 102
    :cond_c
    iget v2, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    if-eq v2, v4, :cond_1f

    iget v2, p0, Lcom/google/android/youtube/core/player/ao;->e:I

    if-ne p1, v2, :cond_1f

    .line 105
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    iget v3, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/player/ap;->b(I)V

    .line 106
    iput v4, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    .line 107
    iput v1, p0, Lcom/google/android/youtube/core/player/ao;->e:I

    .line 115
    :cond_1f
    :goto_1f
    return v0

    .line 111
    :cond_20
    invoke-static {p1}, Lcom/google/android/youtube/core/player/ao;->b(I)Z

    move-result v2

    if-nez v2, :cond_1f

    move v0, v1

    .line 115
    goto :goto_1f
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x59

    const/4 v2, 0x0

    const/high16 v3, -0x8000

    const/4 v1, 0x1

    .line 52
    iget v0, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    if-eq v0, v3, :cond_10

    iget v0, p0, Lcom/google/android/youtube/core/player/ao;->e:I

    if-eq p1, v0, :cond_10

    move v0, v1

    .line 96
    :goto_f
    return v0

    .line 56
    :cond_10
    if-eq p1, v4, :cond_16

    const/16 v0, 0x5a

    if-ne p1, v0, :cond_52

    .line 58
    :cond_16
    iget v0, p0, Lcom/google/android/youtube/core/player/ao;->b:I

    if-eq v0, v3, :cond_4d

    iget v0, p0, Lcom/google/android/youtube/core/player/ao;->c:I

    if-eq v0, v3, :cond_4d

    .line 59
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_2f

    .line 60
    iget v0, p0, Lcom/google/android/youtube/core/player/ao;->b:I

    iput v0, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    .line 61
    iput p1, p0, Lcom/google/android/youtube/core/player/ao;->e:I

    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ap;->a()V

    .line 64
    :cond_2f
    iget v3, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    if-ne p1, v4, :cond_4f

    const/16 v0, -0x4e20

    :goto_35
    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    .line 66
    iget v0, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    iget v3, p0, Lcom/google/android/youtube/core/player/ao;->c:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    iget v2, p0, Lcom/google/android/youtube/core/player/ao;->d:I

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/ap;->a(I)V

    :cond_4d
    move v0, v1

    .line 69
    goto :goto_f

    .line 64
    :cond_4f
    const/16 v0, 0x4e20

    goto :goto_35

    .line 70
    :cond_52
    invoke-static {p1}, Lcom/google/android/youtube/core/player/ao;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 72
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_60

    move v0, v1

    .line 74
    goto :goto_f

    .line 76
    :cond_60
    sparse-switch p1, :sswitch_data_80

    :goto_63
    move v0, v1

    .line 90
    goto :goto_f

    .line 79
    :sswitch_65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ap;->b()V

    goto :goto_63

    .line 82
    :sswitch_6b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ap;->c()V

    goto :goto_63

    .line 86
    :sswitch_71
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ap;->d()V

    goto :goto_63

    .line 89
    :sswitch_77
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ao;->a:Lcom/google/android/youtube/core/player/ap;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ap;->e()V

    goto :goto_63

    :cond_7d
    move v0, v2

    .line 96
    goto :goto_f

    .line 76
    nop

    :sswitch_data_80
    .sparse-switch
        0x4f -> :sswitch_65
        0x55 -> :sswitch_65
        0x56 -> :sswitch_71
        0x7e -> :sswitch_6b
        0x7f -> :sswitch_71
        0xaf -> :sswitch_77
    .end sparse-switch
.end method
