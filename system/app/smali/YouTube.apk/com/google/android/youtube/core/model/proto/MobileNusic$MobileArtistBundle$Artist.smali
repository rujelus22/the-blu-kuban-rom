.class public final Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/e;


# static fields
.field public static final ARTIST_ID_FIELD_NUMBER:I = 0x1

.field public static final ARTIST_NAME_FIELD_NUMBER:I = 0x2

.field public static final BIOGRAPHY_FIELD_NUMBER:I = 0x3

.field public static PARSER:Lcom/google/protobuf/ah;

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

.field private static final serialVersionUID:J


# instance fields
.field private artistId_:Ljava/lang/Object;

.field private artistName_:Ljava/lang/Object;

.field private biography_:Ljava/lang/Object;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1725
    new-instance v0, Lcom/google/android/youtube/core/model/proto/c;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/c;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    .line 2337
    new-instance v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;-><init>(Z)V

    .line 2338
    sput-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->initFields()V

    .line 2339
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1681
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1875
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    .line 1906
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedSerializedSize:I

    .line 1682
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->initFields()V

    .line 1683
    const/4 v0, 0x0

    .line 1686
    :cond_d
    :goto_d
    if-nez v0, :cond_61

    .line 1687
    :try_start_f
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v2

    .line 1688
    sparse-switch v2, :sswitch_data_66

    .line 1693
    invoke-virtual {p0, p1, p2, v2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1695
    goto :goto_d

    :sswitch_1e
    move v0, v1

    .line 1691
    goto :goto_d

    .line 1700
    :sswitch_20
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    .line 1701
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;
    :try_end_2c
    .catchall {:try_start_f .. :try_end_2c} :catchall_33
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_f .. :try_end_2c} :catch_2d
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_2c} :catch_45

    goto :goto_d

    .line 1716
    :catch_2d
    move-exception v0

    .line 1717
    :try_start_2e
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_33
    .catchall {:try_start_2e .. :try_end_33} :catchall_33

    .line 1722
    :catchall_33
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->makeExtensionsImmutable()V

    throw v0

    .line 1705
    :sswitch_38
    :try_start_38
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    .line 1706
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;
    :try_end_44
    .catchall {:try_start_38 .. :try_end_44} :catchall_33
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_38 .. :try_end_44} :catch_2d
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_44} :catch_45

    goto :goto_d

    .line 1718
    :catch_45
    move-exception v0

    .line 1719
    :try_start_46
    new-instance v1, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_54
    .catchall {:try_start_46 .. :try_end_54} :catchall_33

    .line 1710
    :sswitch_54
    :try_start_54
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    .line 1711
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;
    :try_end_60
    .catchall {:try_start_54 .. :try_end_60} :catchall_33
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_54 .. :try_end_60} :catch_2d
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_60} :catch_45

    goto :goto_d

    .line 1722
    :cond_61
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->makeExtensionsImmutable()V

    .line 1723
    return-void

    .line 1688
    nop

    :sswitch_data_66
    .sparse-switch
        0x0 -> :sswitch_1e
        0xa -> :sswitch_20
        0x12 -> :sswitch_38
        0x1a -> :sswitch_54
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1659
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1664
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 1875
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    .line 1906
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedSerializedSize:I

    .line 1666
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1659
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1667
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1875
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    .line 1906
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedSerializedSize:I

    .line 1667
    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1659
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1659
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1659
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1659
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1659
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 1

    .prologue
    .line 1671
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 1871
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    .line 1872
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    .line 1873
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    .line 1874
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/d;
    .registers 1

    .prologue
    .line 1988
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/d;->g()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;
    .registers 2
    .parameter

    .prologue
    .line 1991
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->newBuilder()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2
    .parameter

    .prologue
    .line 1968
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1974
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2
    .parameter

    .prologue
    .line 1938
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1944
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2
    .parameter

    .prologue
    .line 1979
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1985
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2
    .parameter

    .prologue
    .line 1958
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1964
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2
    .parameter

    .prologue
    .line 1948
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1954
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method


# virtual methods
.method public final getArtistId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    .line 1755
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1756
    check-cast v0, Ljava/lang/String;

    .line 1764
    :goto_8
    return-object v0

    .line 1758
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1760
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1761
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1762
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1764
    goto :goto_8
.end method

.method public final getArtistIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    .line 1773
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1774
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1777
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;

    .line 1780
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getArtistName()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1797
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    .line 1798
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1799
    check-cast v0, Ljava/lang/String;

    .line 1807
    :goto_8
    return-object v0

    .line 1801
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1803
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1804
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1805
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1807
    goto :goto_8
.end method

.method public final getArtistNameBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1815
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    .line 1816
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1817
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1820
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;

    .line 1823
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getBiography()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1840
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    .line 1841
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1842
    check-cast v0, Ljava/lang/String;

    .line 1850
    :goto_8
    return-object v0

    .line 1844
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1846
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1847
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1848
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1850
    goto :goto_8
.end method

.method public final getBiographyBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1858
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    .line 1859
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1860
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1863
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;

    .line 1866
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 2

    .prologue
    .line 1675
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->defaultInstance:Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1659
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    return-object v0
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 1737
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 5

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1908
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedSerializedSize:I

    .line 1909
    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 1925
    :goto_7
    return v0

    .line 1911
    :cond_8
    const/4 v0, 0x0

    .line 1912
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_19

    .line 1913
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getArtistIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1916
    :cond_19
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_28

    .line 1917
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getArtistNameBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1920
    :cond_28
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_39

    .line 1921
    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getBiographyBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1924
    :cond_39
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedSerializedSize:I

    goto :goto_7
.end method

.method public final hasArtistId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1748
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasArtistName()Z
    .registers 3

    .prologue
    .line 1791
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasBiography()Z
    .registers 3

    .prologue
    .line 1834
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1877
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    .line 1878
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 1889
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 1878
    goto :goto_9

    .line 1880
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->hasArtistId()Z

    move-result v2

    if-nez v2, :cond_16

    .line 1881
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    move v0, v1

    .line 1882
    goto :goto_9

    .line 1884
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->hasArtistName()Z

    move-result v2

    if-nez v2, :cond_20

    .line 1885
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    move v0, v1

    .line 1886
    goto :goto_9

    .line 1888
    :cond_20
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/d;
    .registers 2

    .prologue
    .line 1989
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->newBuilder()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1659
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/d;
    .registers 2

    .prologue
    .line 1993
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->newBuilder(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1659
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->toBuilder()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1932
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1894
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getSerializedSize()I

    .line 1895
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_12

    .line 1896
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getArtistIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1898
    :cond_12
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1f

    .line 1899
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getArtistNameBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1901
    :cond_1f
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2e

    .line 1902
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getBiographyBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1904
    :cond_2e
    return-void
.end method
