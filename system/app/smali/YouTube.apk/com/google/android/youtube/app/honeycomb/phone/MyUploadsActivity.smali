.class public Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/y;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private m:Lcom/google/android/youtube/core/async/av;

.field private n:Lcom/google/android/youtube/core/b/al;

.field private o:Lcom/google/android/youtube/core/b/an;

.field private p:Lcom/google/android/youtube/core/b/ap;

.field private q:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private r:Lcom/google/android/youtube/core/d;

.field private s:Lcom/google/android/youtube/app/ui/dd;

.field private t:Lcom/google/android/youtube/app/adapter/cl;

.field private u:Lcom/google/android/youtube/app/ui/s;

.field private v:Lcom/google/android/youtube/app/ui/s;

.field private w:I

.field private x:I

.field private y:Lcom/google/android/youtube/core/model/Video;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/app/ui/dd;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/app/ui/dd;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V
    .registers 5
    .parameter

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bl;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bl;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bm;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/d;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bm;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/async/l;)V

    invoke-virtual {v1, p0, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/app/adapter/cl;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->t:Lcom/google/android/youtube/app/adapter/cl;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 74
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/b/al;

    .line 75
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/b/an;

    .line 76
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->p:Lcom/google/android/youtube/core/b/ap;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/d;

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->l()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->m:Lcom/google/android/youtube/core/async/av;

    .line 79
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->z:Z

    if-eqz v0, :cond_19

    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/app/ui/dd;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->c(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dd;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 152
    :cond_19
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->z:Z

    if-eqz v0, :cond_9

    .line 160
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 162
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->finish()V

    .line 163
    return-void
.end method

.method public final synthetic a(ILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 45
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/core/model/Video;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w:I

    if-ne p1, v1, :cond_18

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "DeleteUpload"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const/16 v1, 0x3ef

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->showDialog(I)V

    :goto_17
    return v0

    :cond_18
    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:I

    if-ne p1, v1, :cond_2f

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "EditMetadata"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/a;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_17

    :cond_2f
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 125
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f11000f

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    .line 167
    sparse-switch p1, :sswitch_data_38

    .line 192
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 170
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 173
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 176
    :sswitch_13
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bk;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bk;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V

    .line 184
    new-instance v1, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b01fe

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/x;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_4

    .line 167
    nop

    :sswitch_data_38
    .sparse-switch
        0x3ef -> :sswitch_13
        0x3f2 -> :sswitch_c
        0x3fc -> :sswitch_5
    .end sparse-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 240
    const-string v0, "yt_upload"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->finish()V

    .line 156
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 16
    .parameter

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 85
    const v0, 0x7f04007b

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->setContentView(I)V

    .line 86
    const v0, 0x7f0b018e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->e(I)V

    .line 88
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x3fc

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/s;

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b0202

    const v2, 0x7f02008d

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->x:I

    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b01fd

    const v2, 0x7f02008c

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w:I

    .line 95
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x3f2

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/ui/s;

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b0200

    const v2, 0x7f02008b

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/ui/s;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bj;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bj;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->u:Lcom/google/android/youtube/app/ui/s;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->v:Lcom/google/android/youtube/app/ui/s;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/cl;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/s;Lcom/google/android/youtube/app/ui/s;)Lcom/google/android/youtube/app/adapter/cl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->t:Lcom/google/android/youtube/app/adapter/cl;

    .line 106
    new-instance v0, Lcom/google/android/youtube/app/ui/dd;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v3

    const v1, 0x7f080056

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/g;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->t:Lcom/google/android/youtube/app/adapter/cl;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->m:Lcom/google/android/youtube/core/async/av;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->n:Lcom/google/android/youtube/core/b/al;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->o:Lcom/google/android/youtube/core/b/an;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->p:Lcom/google/android/youtube/core/b/ap;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->y()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Uploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v12, Lcom/google/android/youtube/app/m;->I:Lcom/google/android/youtube/core/b/aq;

    iget-object v13, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->r:Lcom/google/android/youtube/core/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v13}, Lcom/google/android/youtube/app/ui/dd;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cl;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/app/ui/dd;

    .line 120
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 144
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->z:Z

    .line 146
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 137
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->z:Z

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->q:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 140
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 131
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyUploadsActivity;->s:Lcom/google/android/youtube/app/ui/dd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dd;->e()V

    .line 133
    return-void
.end method
