.class public final Lcom/google/android/youtube/app/compat/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences$Editor;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences$Editor;)V
    .registers 3
    .parameter

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences$Editor;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    .line 89
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 163
    instance-of v0, p2, Ljava/util/HashSet;

    if-eqz v0, :cond_10

    .line 164
    check-cast p2, Ljava/util/HashSet;

    .line 169
    :goto_6
    :try_start_6
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_f} :catch_17

    .line 173
    :goto_f
    return-object p0

    .line 166
    :cond_10
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object p2, v0

    goto :goto_6

    :catch_17
    move-exception v0

    goto :goto_f
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)Lcom/google/android/youtube/app/compat/ad;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 125
    return-object p0
.end method

.method public final a(Ljava/lang/String;J)Lcom/google/android/youtube/app/compat/ad;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 130
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/youtube/app/compat/ad;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 135
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_e

    .line 145
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 147
    :goto_d
    return-object p0

    .line 146
    :cond_e
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_1b

    .line 147
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/compat/ad;->b(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/youtube/app/compat/ad;

    move-result-object p0

    goto :goto_d

    .line 149
    :cond_1b
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "putStringSet not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/youtube/app/compat/ad;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 115
    return-object p0
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 92
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_e

    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 97
    :goto_d
    return-void

    .line 95
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/ad;->a:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_d
.end method
