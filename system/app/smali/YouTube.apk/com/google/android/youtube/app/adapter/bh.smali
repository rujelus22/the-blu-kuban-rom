.class final Lcom/google/android/youtube/app/adapter/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;
.implements Lcom/google/android/youtube/app/adapter/m;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bf;

.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/youtube/app/adapter/k;

.field private d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/bf;Landroid/view/View;Landroid/view/ViewGroup;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/bh;->a:Lcom/google/android/youtube/app/adapter/bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/bh;->b:Landroid/view/View;

    .line 86
    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bf;->a(Lcom/google/android/youtube/app/adapter/bf;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->c:Lcom/google/android/youtube/app/adapter/k;

    .line 88
    const v0, 0x7f08007d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->e:Landroid/widget/ImageView;

    .line 89
    const v0, 0x7f080097

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Landroid/widget/ImageView;

    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_32

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->c:Lcom/google/android/youtube/app/adapter/k;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/adapter/k;->a(Lcom/google/android/youtube/app/adapter/m;)V

    .line 93
    :cond_32
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    check-cast p2, Lcom/google/android/youtube/core/model/Playlist;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->c:Lcom/google/android/youtube/app/adapter/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    if-nez v0, :cond_3d

    move v0, v1

    :goto_17
    iget-object v3, p2, Lcom/google/android/youtube/core/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    if-nez v3, :cond_3f

    :goto_1b
    and-int/2addr v0, v1

    if-eqz v0, :cond_41

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_35

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->b:Landroid/view/View;

    const v1, 0x7f08007e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Landroid/widget/ImageView;

    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3a
    :goto_3a
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->b:Landroid/view/View;

    return-object v0

    :cond_3d
    move v0, v2

    goto :goto_17

    :cond_3f
    move v1, v2

    goto :goto_1b

    :cond_41
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3a
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bh;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    return-void
.end method
