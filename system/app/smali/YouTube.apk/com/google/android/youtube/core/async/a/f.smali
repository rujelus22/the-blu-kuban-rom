.class public final Lcom/google/android/youtube/core/async/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/a/c;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/av;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:Lcom/google/android/youtube/core/async/GDataRequest;

.field private f:Lcom/google/android/youtube/core/async/l;

.field private g:Lcom/google/android/youtube/core/async/GDataRequest;

.field private h:Lcom/google/android/youtube/core/async/GDataRequest;

.field private i:I

.field private j:Lcom/google/android/youtube/core/async/l;

.field private k:I

.field private l:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/GDataRequest;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-string v0, "initialRequest cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->e:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 51
    iput-object p2, p0, Lcom/google/android/youtube/core/async/a/f;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 52
    const-string v0, "requester cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->a:Lcom/google/android/youtube/core/async/av;

    .line 53
    if-ltz p3, :cond_55

    const/4 v0, 0x1

    :goto_1c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startIndex="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be >= 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 54
    add-int/lit8 v0, p3, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/f;->i:I

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->b:Ljava/util/List;

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 57
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 59
    new-instance v0, Lcom/google/android/youtube/core/async/a/g;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/async/a/g;-><init>(Lcom/google/android/youtube/core/async/a/f;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->f:Lcom/google/android/youtube/core/async/l;

    .line 60
    return-void

    .line 53
    :cond_55
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/f;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/youtube/core/async/a/f;->g:Lcom/google/android/youtube/core/async/GDataRequest;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/f;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->b:Ljava/util/List;

    return-object v0
.end method

.method private declared-synchronized a(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    monitor-enter p0

    const/4 v2, -0x1

    :try_start_4
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 104
    if-ltz v4, :cond_38

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/f;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_38

    move v3, v0

    .line 105
    :goto_13
    if-lez v4, :cond_3a

    move v2, v0

    .line 106
    :goto_16
    add-int/lit8 v5, v4, 0x1

    iget-object v6, p0, Lcom/google/android/youtube/core/async/a/f;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v5, v6, :cond_21

    move v1, v0

    .line 107
    :cond_21
    if-eqz v3, :cond_3c

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    .line 109
    :goto_2b
    if-nez v1, :cond_31

    iget-object v3, p0, Lcom/google/android/youtube/core/async/a/f;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-nez v3, :cond_3e

    .line 110
    :cond_31
    iput v4, p0, Lcom/google/android/youtube/core/async/a/f;->i:I

    .line 111
    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/youtube/core/async/a/f;->a(Lcom/google/android/youtube/core/model/Video;ZZ)V
    :try_end_36
    .catchall {:try_start_4 .. :try_end_36} :catchall_42

    .line 115
    :goto_36
    monitor-exit p0

    return-void

    :cond_38
    move v3, v1

    .line 104
    goto :goto_13

    :cond_3a
    move v2, v1

    .line 105
    goto :goto_16

    .line 107
    :cond_3c
    const/4 v0, 0x0

    goto :goto_2b

    .line 113
    :cond_3e
    :try_start_3e
    invoke-direct {p0, v4}, Lcom/google/android/youtube/core/async/a/f;->b(I)V
    :try_end_41
    .catchall {:try_start_3e .. :try_end_41} :catchall_42

    goto :goto_36

    .line 103
    :catchall_42
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/f;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/async/a/f;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/f;Ljava/lang/Exception;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/async/a/f;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/youtube/core/model/Video;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->j:Lcom/google/android/youtube/core/async/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/f;->g:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, p1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_15
    .catchall {:try_start_2 .. :try_end_15} :catchall_17

    .line 133
    monitor-exit p0

    return-void

    .line 129
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/Exception;)V
    .registers 4
    .parameter

    .prologue
    .line 136
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z

    .line 137
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->j:Lcom/google/android/youtube/core/async/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/f;->g:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, p1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_d

    .line 138
    monitor-exit p0

    return-void

    .line 136
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/f;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/a/f;)Lcom/google/android/youtube/core/async/GDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/a/f;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/youtube/core/async/a/f;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    return-object p1
.end method

.method private declared-synchronized b(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 118
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/f;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v1, :cond_1f

    :goto_6
    const-string v1, "there is no next page"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 120
    iput p1, p0, Lcom/google/android/youtube/core/async/a/f;->k:I

    .line 122
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z

    if-nez v0, :cond_1d

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z

    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->a:Lcom/google/android/youtube/core/async/av;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/f;->h:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/f;->f:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    :try_end_1d
    .catchall {:try_start_2 .. :try_end_1d} :catchall_21

    .line 126
    :cond_1d
    monitor-exit p0

    return-void

    .line 118
    :cond_1f
    const/4 v0, 0x0

    goto :goto_6

    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/async/a/f;)Z
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/async/a/f;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/youtube/core/async/a/f;->k:I

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 3

    .prologue
    .line 71
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->j:Lcom/google/android/youtube/core/async/l;

    const-string v1, "call setCallback() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget v0, p0, Lcom/google/android/youtube/core/async/a/f;->i:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/f;->a(I)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 73
    monitor-exit p0

    return-void

    .line 71
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/youtube/core/async/l;)V
    .registers 3
    .parameter

    .prologue
    .line 63
    const-string v0, "callback cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/l;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->j:Lcom/google/android/youtube/core/async/l;

    .line 64
    return-void
.end method

.method public final declared-synchronized b()V
    .registers 3

    .prologue
    .line 76
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->j:Lcom/google/android/youtube/core/async/l;

    const-string v1, "call setCallback() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget v0, p0, Lcom/google/android/youtube/core/async/a/f;->i:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/f;->a(I)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 78
    monitor-exit p0

    return-void

    .line 76
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .registers 3

    .prologue
    .line 81
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->j:Lcom/google/android/youtube/core/async/l;

    const-string v1, "call setCallback() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    iget v0, p0, Lcom/google/android/youtube/core/async/a/f;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/f;->a(I)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 83
    monitor-exit p0

    return-void

    .line 81
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/f;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final declared-synchronized f()V
    .registers 2

    .prologue
    .line 174
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/f;->l:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 175
    monitor-exit p0

    return-void

    .line 174
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()I
    .registers 2

    .prologue
    .line 99
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/youtube/core/async/a/f;->i:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method
