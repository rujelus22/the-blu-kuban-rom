.class final Lcom/google/android/youtube/core/player/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/Director;)V
    .registers 2
    .parameter

    .prologue
    .line 1076
    iput-object p1, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/Director;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1076
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/ab;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1078
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_242

    .line 1175
    :cond_7
    :goto_7
    :pswitch_7
    return v1

    .line 1080
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->t()V

    .line 1081
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    .line 1082
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;Z)Z

    goto :goto_7

    .line 1085
    :pswitch_20
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/au;->n()V

    .line 1086
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director;Z)Z

    .line 1087
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setPlaying()V

    .line 1088
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/be;

    move-result-object v2

    if-eqz v2, :cond_48

    .line 1089
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/be;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/be;->c()V

    .line 1091
    :cond_48
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->g(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->h(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1092
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/player/Director;->c(Lcom/google/android/youtube/core/player/Director;Z)Z

    .line 1093
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;

    sget-object v2, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 1094
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->j(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1095
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->l(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/e;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/Director;->k(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/e;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 1096
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;Z)Z

    goto :goto_7

    .line 1101
    :pswitch_81
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/au;->o()V

    .line 1102
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;Z)Z

    .line 1103
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->d()V

    .line 1104
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->n(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1105
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->o(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 1106
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->g()V

    goto/16 :goto_7

    .line 1108
    :cond_b3
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    goto/16 :goto_7

    .line 1113
    :pswitch_be
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->s()V

    .line 1114
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->n(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1115
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    goto/16 :goto_7

    .line 1119
    :pswitch_da
    iget v2, p1, Landroid/os/Message;->arg1:I

    .line 1120
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1121
    iget v3, p1, Landroid/os/Message;->arg2:I

    .line 1122
    iget-object v4, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v4}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v4

    invoke-interface {v4, v2, v0, v3}, Lcom/google/android/youtube/core/b/au;->a(III)V

    .line 1123
    iget-object v4, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v4}, Lcom/google/android/youtube/core/player/Director;->m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/az;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/youtube/core/player/az;->a(I)V

    .line 1124
    iget-object v4, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v4}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v4

    invoke-interface {v4, v2, v0, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setTimes(III)V

    .line 1125
    iget-object v3, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/Director;->h(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/Director;->p(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/a;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 1126
    iget-object v3, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/youtube/core/player/Director;->p(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/a;

    move-result-object v3

    invoke-interface {v3, v2, v0}, Lcom/google/android/youtube/core/player/a;->a(II)V

    goto/16 :goto_7

    .line 1130
    :pswitch_11c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->q()V

    .line 1131
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setLoading()V

    goto/16 :goto_7

    .line 1134
    :pswitch_130
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->f()V

    goto/16 :goto_7

    .line 1137
    :pswitch_13b
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->r()V

    .line 1138
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->n(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1139
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setPlaying()V

    goto/16 :goto_7

    .line 1143
    :pswitch_157
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->q(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->r(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/model/v;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->b:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream;->isHD()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1144
    invoke-static {}, Lcom/google/android/youtube/core/L;->a()V

    .line 1145
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->o()V

    .line 1146
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;

    goto/16 :goto_7

    .line 1150
    :pswitch_17c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->p()V

    .line 1151
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->d()V

    .line 1152
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->h(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_19d

    .line 1153
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->s(Lcom/google/android/youtube/core/player/Director;)V

    goto/16 :goto_7

    .line 1155
    :cond_19d
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->t(Lcom/google/android/youtube/core/player/Director;)V

    goto/16 :goto_7

    .line 1160
    :pswitch_1a4
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/youtube/core/b/au;->a(Landroid/os/Message;)V

    .line 1161
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/az;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/az;->d()V

    .line 1162
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;Z)Z

    .line 1163
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->h(Lcom/google/android/youtube/core/player/Director;)Z

    move-result v2

    if-eqz v2, :cond_1ca

    .line 1164
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->s(Lcom/google/android/youtube/core/player/Director;)V

    goto/16 :goto_7

    .line 1166
    :cond_1ca
    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/player/Director;->b(Lcom/google/android/youtube/core/player/Director;Z)Z

    .line 1167
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    const v2, 0x7f0b0045

    iget-object v5, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v5}, Lcom/google/android/youtube/core/player/Director;->u(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/utils/l;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/youtube/core/utils/l;->a()Z

    move-result v5

    if-nez v5, :cond_218

    const v0, 0x7f0b0009

    move v2, v0

    move v0, v1

    :goto_1e7
    new-instance v3, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v4, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->PLAYER_ERROR:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v5, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v5}, Lcom/google/android/youtube/core/player/Director;->v(Lcom/google/android/youtube/core/player/Director;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2, v3, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/DirectorException;Z)V

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->i()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/Director;->w(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/bx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/bx;->c()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/Director;I)I

    goto/16 :goto_7

    :cond_218
    if-ne v3, v1, :cond_21d

    sparse-switch v4, :sswitch_data_260

    :cond_21d
    move v0, v1

    goto :goto_1e7

    :sswitch_21f
    const v0, 0x7f0b0047

    move v2, v0

    move v0, v1

    goto :goto_1e7

    :sswitch_225
    const v0, 0x7f0b0048

    move v2, v0

    move v0, v1

    goto :goto_1e7

    :sswitch_22b
    const v2, 0x7f0b0049

    goto :goto_1e7

    :sswitch_22f
    const v0, 0x7f0b004b

    move v2, v0

    move v0, v1

    goto :goto_1e7

    .line 1171
    :pswitch_235
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ab;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/b/au;->c(I)V

    goto/16 :goto_7

    .line 1078
    :pswitch_data_242
    .packed-switch 0x1
        :pswitch_8
        :pswitch_20
        :pswitch_be
        :pswitch_81
        :pswitch_da
        :pswitch_11c
        :pswitch_13b
        :pswitch_17c
        :pswitch_1a4
        :pswitch_235
        :pswitch_7
        :pswitch_130
        :pswitch_157
    .end packed-switch

    .line 1167
    :sswitch_data_260
    .sparse-switch
        -0x3f2 -> :sswitch_22b
        -0x3ed -> :sswitch_225
        -0x3ec -> :sswitch_225
        -0x3eb -> :sswitch_21f
        -0x3ea -> :sswitch_21f
        0x21 -> :sswitch_22f
    .end sparse-switch
.end method
