.class public final Lcom/google/android/youtube/app/adapter/az;
.super Lcom/google/android/youtube/core/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final c:Landroid/app/Activity;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/youtube/core/async/av;

.field private final f:Lcom/google/android/youtube/core/d;

.field private final g:Lcom/google/android/youtube/core/async/c;

.field private final h:I

.field private final i:Lcom/google/android/youtube/app/adapter/cb;

.field private final j:Ljava/util/LinkedList;

.field private k:Lcom/google/android/youtube/core/async/GDataRequest;

.field private l:Lcom/google/android/youtube/app/adapter/bb;

.field private final m:I

.field private final n:I

.field private final o:I

.field private p:I

.field private final q:Lcom/google/android/youtube/core/a/g;

.field private r:Landroid/net/Uri;

.field private s:I

.field private t:Lcom/google/android/youtube/core/utils/p;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILcom/google/android/youtube/app/adapter/cb;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;IIILcom/google/android/youtube/core/a/g;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/e;-><init>()V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    .line 131
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->c:Landroid/app/Activity;

    .line 132
    new-instance v0, Lcom/google/android/youtube/app/adapter/ba;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/adapter/ba;-><init>(Lcom/google/android/youtube/app/adapter/az;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->d:Landroid/os/Handler;

    .line 140
    const-string v0, "requester cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->e:Lcom/google/android/youtube/core/async/av;

    .line 141
    const-string v0, "errorHelper cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->f:Lcom/google/android/youtube/core/d;

    .line 142
    invoke-static {p1, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->g:Lcom/google/android/youtube/core/async/c;

    .line 143
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->j:Ljava/util/LinkedList;

    .line 145
    iput p2, p0, Lcom/google/android/youtube/app/adapter/az;->h:I

    .line 146
    const-string v0, "rendererFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cb;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->i:Lcom/google/android/youtube/app/adapter/cb;

    .line 149
    iput p6, p0, Lcom/google/android/youtube/app/adapter/az;->m:I

    .line 150
    iput p7, p0, Lcom/google/android/youtube/app/adapter/az;->n:I

    .line 151
    iput p8, p0, Lcom/google/android/youtube/app/adapter/az;->o:I

    .line 153
    iput-object p9, p0, Lcom/google/android/youtube/app/adapter/az;->q:Lcom/google/android/youtube/core/a/g;

    .line 154
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/az;)V
    .registers 1
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->h()V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 4
    .parameter

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 344
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->l_()V

    .line 345
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->e:Lcom/google/android/youtube/core/async/av;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->g:Lcom/google/android/youtube/core/async/c;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 346
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 244
    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    .line 245
    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 246
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/adapter/az;->s:I

    .line 247
    iget v0, p0, Lcom/google/android/youtube/app/adapter/az;->o:I

    iput v0, p0, Lcom/google/android/youtube/app/adapter/az;->p:I

    .line 248
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 250
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->b()V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/az;->k()V

    .line 252
    return-void
.end method

.method private g()V
    .registers 2

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 319
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->f()V

    .line 325
    :goto_d
    return-void

    .line 321
    :cond_e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    .line 322
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/adapter/az;->s:I

    .line 323
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/az;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_d
.end method

.method private h()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 328
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    if-nez v0, :cond_9

    .line 329
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->g()V

    .line 340
    :cond_8
    :goto_8
    return-void

    .line 330
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, v0, Lcom/google/android/youtube/core/async/GDataRequest;->c:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    if-eq v0, v1, :cond_8

    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->n:I

    if-ge v0, v1, :cond_29

    .line 332
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/GDataRequest;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    .line 333
    iput-object v2, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    .line 334
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/az;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_8

    .line 336
    :cond_29
    iput-object v2, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->f()V

    goto :goto_8
.end method


# virtual methods
.method public final a()I
    .registers 3

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/youtube/app/adapter/az;->p:I

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    if-nez p2, :cond_20

    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->i:Lcom/google/android/youtube/app/adapter/cb;

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/cb;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    .line 173
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 177
    :goto_18
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/az;->b(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bs;->a(ILjava/lang/Object;)Landroid/view/View;

    .line 178
    return-object p2

    .line 175
    :cond_20
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bs;

    goto :goto_18
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .registers 3
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->q:Lcom/google/android/youtube/core/a/g;

    return-object v0
.end method

.method public final a(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/az;->k()V

    .line 351
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/adapter/bb;)V
    .registers 2
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    .line 158
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/utils/p;)V
    .registers 2
    .parameter

    .prologue
    .line 205
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/az;->t:Lcom/google/android/youtube/core/utils/p;

    .line 206
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 40
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    instance-of v0, p2, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_3d

    move-object v0, p2

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x193

    if-ne v0, v1, :cond_3d

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->c:Landroid/app/Activity;

    const v2, 0x7f0b0017

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/adapter/bb;->a(Ljava/lang/String;Z)V

    :goto_3c
    return-void

    :cond_3d
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->f:Lcom/google/android/youtube/core/d;

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/adapter/bb;->a(Ljava/lang/String;Z)V

    goto :goto_3c
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 40
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-ne p1, v0, :cond_90

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    iget v4, p0, Lcom/google/android/youtube/app/adapter/az;->s:I

    iget v5, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v8, v0

    move v0, v1

    move v1, v8

    :goto_2d
    if-ge v1, v4, :cond_4b

    iget v5, p0, Lcom/google/android/youtube/app/adapter/az;->n:I

    if-ge v0, v5, :cond_4b

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/adapter/az;->t:Lcom/google/android/youtube/core/utils/p;

    if-eqz v6, :cond_43

    iget-object v6, p0, Lcom/google/android/youtube/app/adapter/az;->t:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v6, v5}, Lcom/google/android/youtube/core/utils/p;->a(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_48

    :cond_43
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    :cond_48
    add-int/lit8 v1, v1, 0x1

    goto :goto_2d

    :cond_4b
    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->s:I

    iget v4, p2, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/adapter/az;->s:I

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_91

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/az;->k()V

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    :goto_70
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_85

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->j:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_85

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v1}, Lcom/google/android/youtube/app/adapter/bb;->e()V

    :cond_85
    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->n:I

    if-ne v0, v1, :cond_94

    iput-object v7, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->f()V

    :cond_90
    :goto_90
    return-void

    :cond_91
    iput-object v7, p0, Lcom/google/android/youtube/app/adapter/az;->r:Landroid/net/Uri;

    goto :goto_70

    :cond_94
    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->p:I

    if-ge v0, v1, :cond_9c

    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->h()V

    goto :goto_90

    :cond_9c
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->d()V

    goto :goto_90
.end method

.method protected final a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->q:Lcom/google/android/youtube/core/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 189
    return-void
.end method

.method public final varargs a([Lcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 218
    const-string v0, "requests cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    array-length v0, p1

    if-lez v0, :cond_38

    const/4 v0, 0x1

    :goto_a
    const-string v2, "requests cannot be empty"

    invoke-static {v0, v2}, Lcom/google/android/ytremote/util/b;->a(ZLjava/lang/Object;)V

    .line 220
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->f()V

    .line 221
    :goto_12
    array-length v0, p1

    if-ge v1, v0, :cond_3a

    .line 222
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->j:Ljava/util/LinkedList;

    aget-object v2, p1, v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "request "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " cannot be null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    :cond_38
    move v0, v1

    .line 219
    goto :goto_a

    .line 225
    :cond_3a
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->g()V

    .line 226
    return-void
.end method

.method public final a_()V
    .registers 1

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->f()V

    .line 241
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/az;->f()V

    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->l_()V

    .line 234
    return-void
.end method

.method public final c(I)J
    .registers 4
    .parameter

    .prologue
    .line 198
    int-to-long v0, p1

    return-wide v0
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_9

    .line 362
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->k:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/az;->a(Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 364
    :cond_9
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 371
    iget v0, p0, Lcom/google/android/youtube/app/adapter/az;->p:I

    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->m:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/adapter/az;->p:I

    .line 372
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/app/adapter/az;->p:I

    if-lt v0, v1, :cond_1a

    .line 373
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/az;->k()V

    .line 374
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->l:Lcom/google/android/youtube/app/adapter/bb;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/bb;->d()V

    .line 378
    :goto_19
    return-void

    .line 376
    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/az;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_19
.end method
