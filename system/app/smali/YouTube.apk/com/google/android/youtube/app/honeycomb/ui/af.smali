.class final Lcom/google/android/youtube/app/honeycomb/ui/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/x;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V
    .registers 2
    .parameter

    .prologue
    .line 474
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/x;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 474
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/af;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->p(Lcom/google/android/youtube/app/honeycomb/ui/x;)Lcom/google/android/youtube/core/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->g(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 474
    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/UserProfile;->username:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->a(Lcom/google/android/youtube/app/honeycomb/ui/x;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->n(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/x;->j(Lcom/google/android/youtube/app/honeycomb/ui/x;)Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-static {v0, v5}, Lcom/google/android/youtube/app/honeycomb/ui/x;->b(Lcom/google/android/youtube/app/honeycomb/ui/x;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/af;->a:Lcom/google/android/youtube/app/honeycomb/ui/x;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/x;->o(Lcom/google/android/youtube/app/honeycomb/ui/x;)V

    return-void
.end method
