.class public final Lcom/google/android/youtube/core/player/ag;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Z

.field private c:Lcom/google/android/youtube/core/player/Director;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Bundle;Lcom/google/android/youtube/core/player/PlayerView;Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/b/al;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v1, 0x1080011

    const/4 v4, 0x0

    .line 61
    invoke-static {p2}, Lcom/google/android/youtube/core/player/ag;->a(Landroid/os/Bundle;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 63
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ag;->a:Landroid/app/Activity;

    .line 64
    const-string v0, "extras cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v0, "playerView cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string v0, "director cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/Director;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/ag;->c:Lcom/google/android/youtube/core/player/Director;

    .line 67
    const-string v0, "gdataClient cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-string v0, "lightbox_mode"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/ag;->b:Z

    .line 70
    invoke-virtual {p0, p3}, Lcom/google/android/youtube/core/player/ag;->setContentView(Landroid/view/View;)V

    .line 71
    invoke-static {p2}, Lcom/google/android/youtube/core/player/ag;->a(Landroid/os/Bundle;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTheme(I)V

    .line 73
    const-string v0, "lightbox_mode"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 74
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/ag;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 75
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 80
    :cond_56
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 81
    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 82
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    .line 84
    const-string v0, "video_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    const-string v1, "video_ids"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 86
    const-string v2, "current_index"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 88
    if-eqz v1, :cond_80

    .line 89
    invoke-static {p5, v1, v4, v2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    invoke-virtual {p4, v0, v4}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 94
    :cond_7f
    :goto_7f
    return-void

    .line 91
    :cond_80
    if-eqz v0, :cond_7f

    .line 92
    invoke-static {p5, v0, v4}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    invoke-virtual {p4, v0, v4}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_7f
.end method

.method private static a(Landroid/os/Bundle;)I
    .registers 3
    .parameter

    .prologue
    .line 54
    const-string v0, "lightbox_mode"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x1030011

    :goto_c
    return v0

    :cond_d
    const v0, 0x103000a

    goto :goto_c
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ag;->c:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->t()V

    .line 106
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ag;->c:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 124
    const/4 v0, 0x1

    .line 126
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ag;->c:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 115
    const/4 v0, 0x1

    .line 117
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method protected final onStop()V
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ag;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_d

    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ag;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 101
    :cond_d
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 102
    return-void
.end method
