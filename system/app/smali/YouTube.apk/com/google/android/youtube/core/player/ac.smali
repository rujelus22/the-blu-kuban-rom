.class final Lcom/google/android/youtube/core/player/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ba;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/Director;)V
    .registers 2
    .parameter

    .prologue
    .line 1287
    iput-object p1, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/Director;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1287
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/ac;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasCc(Z)V

    .line 1291
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a(Ljava/util/List;)V

    .line 1309
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasCc(Z)V

    .line 1295
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setCcEnabled(Z)V

    .line 1299
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;

    .line 1300
    return-void
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setCcEnabled(Z)V

    .line 1304
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;

    .line 1305
    return-void
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/google/android/youtube/core/player/ac;->a:Lcom/google/android/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/Director;->v(Lcom/google/android/youtube/core/player/Director;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0036

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    .line 1313
    return-void
.end method
