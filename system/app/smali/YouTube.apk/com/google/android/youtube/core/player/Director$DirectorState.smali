.class public final Lcom/google/android/youtube/core/player/Director$DirectorState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final ad:Lcom/google/android/youtube/core/model/VastAd;

.field public final adStartPositionMillis:I

.field public final hq:Z

.field public final isPlaying:Z

.field public final videoStartPositionMillis:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 1339
    new-instance v0, Lcom/google/android/youtube/core/player/z;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/z;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/player/Director$DirectorState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1359
    const-class v0, Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/VastAd;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    .line 1360
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_31

    move v0, v1

    :goto_1a
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    .line 1361
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    .line 1362
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    .line 1363
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_33

    :goto_2e
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    .line 1364
    return-void

    :cond_31
    move v0, v2

    .line 1360
    goto :goto_1a

    :cond_33
    move v1, v2

    .line 1363
    goto :goto_2e
.end method

.method public constructor <init>(Lcom/google/android/youtube/core/model/VastAd;ZZII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1351
    iput-object p1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    .line 1352
    iput-boolean p2, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    .line 1353
    iput-boolean p3, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    .line 1354
    iput p4, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    .line 1355
    iput p5, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    .line 1356
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .registers 2

    .prologue
    .line 1375
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1380
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Director.DirectorState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ad="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isPlaying="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " adStartPositionMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " videoStartPositionMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1367
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->ad:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1368
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->isPlaying:Z

    if-eqz v0, :cond_21

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1369
    iget v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->adStartPositionMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1370
    iget v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->videoStartPositionMillis:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1371
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director$DirectorState;->hq:Z

    if-eqz v0, :cond_23

    :goto_1d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1372
    return-void

    :cond_21
    move v0, v2

    .line 1368
    goto :goto_c

    :cond_23
    move v1, v2

    .line 1371
    goto :goto_1d
.end method
