.class public final Lcom/google/android/youtube/app/remote/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/ae;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/youtube/core/b/al;

.field private final c:Lcom/google/android/youtube/app/remote/au;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/async/l;

.field private final f:Landroid/content/res/Resources;

.field private final g:Lcom/google/android/youtube/app/remote/at;

.field private final h:Lcom/google/android/youtube/app/remote/ao;

.field private i:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private j:Lcom/google/android/youtube/core/async/m;

.field private k:Lcom/google/android/youtube/core/async/n;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/ab;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/remote/ao;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const-string v0, "context can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->a:Landroid/content/Context;

    .line 65
    const-string v0, "gDataClient can not be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->b:Lcom/google/android/youtube/core/b/al;

    .line 66
    const-string v0, "userAuthorizer can not be null"

    invoke-static {p5, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 68
    const-string v0, "remoteControlClientHelper cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/ao;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->h:Lcom/google/android/youtube/app/remote/ao;

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->f:Landroid/content/res/Resources;

    .line 73
    new-instance v0, Lcom/google/android/youtube/app/remote/aw;

    invoke-direct {v0, p0, p4, v1}, Lcom/google/android/youtube/app/remote/aw;-><init>(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/b/an;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->e:Lcom/google/android/youtube/core/async/l;

    .line 75
    new-instance v0, Lcom/google/android/youtube/app/remote/au;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/remote/au;-><init>(Lcom/google/android/youtube/app/remote/aq;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->c:Lcom/google/android/youtube/app/remote/au;

    .line 77
    new-instance v0, Lcom/google/android/youtube/app/remote/at;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/remote/at;-><init>(Lcom/google/android/youtube/app/remote/aq;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->g:Lcom/google/android/youtube/app/remote/at;

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->g:Lcom/google/android/youtube/app/remote/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/at;->a()V

    .line 81
    invoke-virtual {p2, p0}, Lcom/google/android/youtube/app/remote/ab;->a(Lcom/google/android/youtube/app/remote/ae;)V

    .line 82
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/aq;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    invoke-static {p1, v1, v2, v0, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-static {p1}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->k:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/m;)Lcom/google/android/youtube/core/async/m;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/aq;->j:Lcom/google/android/youtube/core/async/m;

    return-object p1
.end method

.method private a()V
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->g:Lcom/google/android/youtube/app/remote/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/at;->a()V

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->h:Lcom/google/android/youtube/app/remote/ao;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/ao;->a()V

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/remote/aq;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/aq;->a()V

    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->j:Lcom/google/android/youtube/core/async/m;

    if-eqz v0, :cond_c

    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->j:Lcom/google/android/youtube/core/async/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/m;->b()V

    .line 120
    iput-object v1, p0, Lcom/google/android/youtube/app/remote/aq;->j:Lcom/google/android/youtube/core/async/m;

    .line 122
    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->k:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_17

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->k:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 124
    iput-object v1, p0, Lcom/google/android/youtube/app/remote/aq;->k:Lcom/google/android/youtube/core/async/n;

    .line 126
    :cond_17
    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/remote/aq;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/aq;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/ao;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->h:Lcom/google/android/youtube/app/remote/ao;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/m;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->j:Lcom/google/android/youtube/core/async/m;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/async/l;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->e:Lcom/google/android/youtube/core/async/l;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->b:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/at;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->g:Lcom/google/android/youtube/app/remote/at;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/remote/aq;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/remote/aq;)Ljava/lang/CharSequence;
    .registers 6
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v0

    if-nez v0, :cond_14

    :cond_c
    const-string v0, "We should be connected to a screen, but the value is null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    const-string v0, ""

    :goto_13
    return-object v0

    :cond_14
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b023a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_13
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 4
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_11

    .line 86
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/aq;->c:Lcom/google/android/youtube/app/remote/au;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/aj;)V

    .line 87
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/aq;->b()V

    .line 88
    invoke-direct {p0}, Lcom/google/android/youtube/app/remote/aq;->a()V

    .line 90
    :cond_11
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_30

    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/aq;->c:Lcom/google/android/youtube/app/remote/au;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/aj;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/aq;->c:Lcom/google/android/youtube/app/remote/au;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/aj;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aq;->c:Lcom/google/android/youtube/app/remote/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/aq;->i:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/remote/au;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    .line 96
    :cond_30
    return-void
.end method
