.class public final Lcom/google/android/youtube/app/honeycomb/tablet/q;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/e;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/al;

.field private final h:Lcom/google/android/youtube/core/d;

.field private i:Lcom/google/android/youtube/app/ui/dd;

.field private j:Lcom/google/android/youtube/app/ui/dw;

.field private k:Lcom/google/android/youtube/app/ui/dw;

.field private l:Lcom/google/android/youtube/core/ui/j;

.field private m:Lcom/google/android/youtube/app/ui/cy;

.field private n:Lcom/google/android/youtube/app/ui/d;

.field private o:Lcom/google/android/youtube/app/ui/g;

.field private p:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private q:Lcom/google/android/youtube/app/k;

.field private r:Lcom/google/android/youtube/core/Analytics;

.field private s:Lcom/google/android/youtube/core/model/UserAuth;

.field private t:Lcom/google/android/youtube/core/model/Playlist;

.field private final u:Lcom/google/android/youtube/app/honeycomb/tablet/y;

.field private v:Lcom/google/android/youtube/app/adapter/cl;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    const/4 v4, 0x0

    const-string v5, "yt_your_channel"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    .line 110
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    .line 111
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/tablet/y;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->u:Lcom/google/android/youtube/app/honeycomb/tablet/y;

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/honeycomb/tablet/y;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->u:Lcom/google/android/youtube/app/honeycomb/tablet/y;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/adapter/cl;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->v:Lcom/google/android/youtube/app/adapter/cl;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->r:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/app/ui/dd;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->i:Lcom/google/android/youtube/app/ui/dd;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    return-object v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 318
    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/tablet/q;)V
    .registers 1
    .parameter

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->f()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/honeycomb/tablet/q;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .registers 2
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 116
    const v0, 0x7f0400dc

    return v0
.end method

.method public final a(I)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 346
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_7

    .line 367
    :goto_6
    return-void

    .line 349
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    .line 350
    sparse-switch p1, :sswitch_data_64

    goto :goto_6

    .line 352
    :sswitch_11
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->i:Lcom/google/android/youtube/app/ui/dd;

    new-array v2, v2, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->c(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/dd;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 355
    :sswitch_21
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->j:Lcom/google/android/youtube/app/ui/dw;

    new-array v2, v2, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 358
    :sswitch_31
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->k:Lcom/google/android/youtube/app/ui/dw;

    new-array v2, v2, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->e(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 361
    :sswitch_41
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->l:Lcom/google/android/youtube/core/ui/j;

    new-array v2, v2, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    const/16 v4, 0x17

    invoke-virtual {v0, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->e(Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 364
    :sswitch_53
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->m:Lcom/google/android/youtube/app/ui/cy;

    new-array v2, v2, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->h(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/cy;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 350
    nop

    :sswitch_data_64
    .sparse-switch
        0x7f080056 -> :sswitch_11
        0x7f080057 -> :sswitch_21
        0x7f080059 -> :sswitch_41
        0x7f0800c2 -> :sswitch_53
        0x7f0800c3 -> :sswitch_31
    .end sparse-switch
.end method

.method protected final a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 305
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/os/Bundle;)V

    .line 306
    const-string v0, "selected_card_id"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/d;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 307
    const-string v0, "selected_playlist"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->t:Lcom/google/android/youtube/core/model/Playlist;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 308
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 28
    .parameter
    .parameter

    .prologue
    .line 121
    invoke-super/range {p0 .. p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v5

    .line 123
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 124
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->r:Lcom/google/android/youtube/core/Analytics;

    .line 125
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->q:Lcom/google/android/youtube/app/k;

    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->l()Lcom/google/android/youtube/core/async/av;

    move-result-object v13

    .line 128
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->k()Lcom/google/android/youtube/core/async/av;

    .line 129
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->m()Lcom/google/android/youtube/core/async/av;

    .line 130
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->v()Lcom/google/android/youtube/core/async/av;

    move-result-object v22

    .line 132
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->q()Lcom/google/android/youtube/core/async/av;

    move-result-object v23

    .line 134
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->n()Lcom/google/android/youtube/core/async/av;

    move-result-object v24

    .line 136
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->x()Lcom/google/android/youtube/core/async/av;

    .line 138
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v16

    .line 143
    new-instance v1, Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->o:Lcom/google/android/youtube/app/ui/g;

    .line 145
    const v1, 0x7f080056

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 146
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v11, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 147
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/cl;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cl;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->v:Lcom/google/android/youtube/app/adapter/cl;

    .line 148
    new-instance v6, Lcom/google/android/youtube/app/honeycomb/tablet/r;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->p:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->v:Lcom/google/android/youtube/app/adapter/cl;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->r:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Uploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v19, Lcom/google/android/youtube/app/m;->I:Lcom/google/android/youtube/core/b/aq;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    move-object/from16 v20, v0

    move-object/from16 v7, p0

    move-object v15, v5

    move-object/from16 v21, v11

    invoke-direct/range {v6 .. v21}, Lcom/google/android/youtube/app/honeycomb/tablet/r;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cl;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/ui/PagedGridView;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->i:Lcom/google/android/youtube/app/ui/dd;

    .line 223
    const v1, 0x7f080057

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 224
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 225
    new-instance v8, Lcom/google/android/youtube/app/honeycomb/tablet/s;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v13

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->r:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v18, v0

    sget-object v19, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Favorites:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v20, Lcom/google/android/youtube/app/m;->J:Lcom/google/android/youtube/core/b/aq;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    move-object/from16 v21, v0

    move-object/from16 v9, p0

    move-object/from16 v14, v22

    move-object v15, v5

    invoke-direct/range {v8 .. v21}, Lcom/google/android/youtube/app/honeycomb/tablet/s;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;ZLcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->j:Lcom/google/android/youtube/app/ui/dw;

    .line 244
    const v1, 0x7f0800c3

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 245
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 246
    new-instance v9, Lcom/google/android/youtube/app/ui/dw;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v13

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->r:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v18, v0

    sget-object v19, Lcom/google/android/youtube/core/Analytics$VideoCategory;->WatchLater:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v20, Lcom/google/android/youtube/app/m;->R:Lcom/google/android/youtube/core/b/aq;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    move-object/from16 v21, v0

    move-object/from16 v14, v23

    move-object v15, v5

    invoke-direct/range {v9 .. v21}, Lcom/google/android/youtube/app/ui/dw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;ZLcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->k:Lcom/google/android/youtube/app/ui/dw;

    .line 260
    new-instance v9, Lcom/google/android/youtube/app/adapter/bi;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-direct {v9, v1}, Lcom/google/android/youtube/app/adapter/bi;-><init>(Landroid/content/Context;)V

    .line 261
    const v1, 0x7f080059

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 262
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->c(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v8, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 263
    new-instance v6, Lcom/google/android/youtube/app/honeycomb/tablet/ad;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->i()Lcom/google/android/youtube/core/async/av;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    move-object/from16 v10, v24

    move-object v12, v5

    invoke-direct/range {v6 .. v13}, Lcom/google/android/youtube/app/honeycomb/tablet/ad;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/bi;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->l:Lcom/google/android/youtube/core/ui/j;

    .line 272
    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/v;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/v;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;Lcom/google/android/youtube/app/adapter/bi;)V

    invoke-interface {v8, v1}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 274
    new-instance v9, Lcom/google/android/youtube/app/adapter/cg;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-direct {v9, v1}, Lcom/google/android/youtube/app/adapter/cg;-><init>(Landroid/content/Context;)V

    .line 275
    const v1, 0x7f0800c2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 276
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->d(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v8, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 277
    new-instance v6, Lcom/google/android/youtube/app/ui/cy;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/google/android/youtube/app/ui/cy;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cg;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->m:Lcom/google/android/youtube/app/ui/cy;

    .line 284
    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/w;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v9}, Lcom/google/android/youtube/app/honeycomb/tablet/w;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;Lcom/google/android/youtube/app/adapter/cg;)V

    invoke-interface {v8, v1}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->r()Landroid/os/Bundle;

    move-result-object v2

    .line 287
    const v1, 0x7f0800c2

    .line 288
    if-eqz p2, :cond_237

    .line 289
    const-string v1, "selected_card_id"

    const v2, 0x7f0800c2

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 290
    const-string v1, "selected_playlist"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Playlist;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->t:Lcom/google/android/youtube/core/model/Playlist;

    move v3, v2

    .line 295
    :goto_20c
    const v1, 0x7f0800bd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const v4, 0x7f0400e2

    const v2, 0x7f0800bb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    invoke-static {v1, v4, v2, v3}, Lcom/google/android/youtube/app/ui/d;->a(Landroid/widget/ListView;ILandroid/widget/FrameLayout;I)Lcom/google/android/youtube/app/ui/d;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    .line 300
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/d;->a(Lcom/google/android/youtube/app/ui/e;)V

    .line 301
    return-void

    .line 292
    :cond_237
    if-eqz v2, :cond_244

    .line 293
    const-string v1, "selected_card_id"

    const v3, 0x7f0800c2

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move v3, v1

    goto :goto_20c

    :cond_244
    move v3, v1

    goto :goto_20c
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 400
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V

    .line 401
    const v0, 0x7f110003

    invoke-virtual {p2, v0, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 402
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 322
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->o:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/d;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->a(I)V

    .line 324
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 333
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->d:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->a()V

    .line 335
    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 7
    .parameter

    .prologue
    .line 406
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_26

    .line 420
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    :goto_b
    return v0

    .line 408
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->e:Lcom/google/android/youtube/app/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/tablet/t;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->h:Lcom/google/android/youtube/core/d;

    invoke-direct {v2, p0, v3}, Lcom/google/android/youtube/app/honeycomb/tablet/t;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;Lcom/google/android/youtube/core/d;)V

    const v3, 0x7f0b01bf

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->q:Lcom/google/android/youtube/app/k;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/k;->e()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    .line 418
    const/4 v0, 0x1

    goto :goto_b

    .line 406
    nop

    :pswitch_data_26
    .packed-switch 0x7f080197
        :pswitch_c
    .end packed-switch
.end method

.method protected final b()V
    .registers 1

    .prologue
    .line 312
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 313
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->f()V

    .line 314
    return-void
.end method

.method public final b(I)V
    .registers 3
    .parameter

    .prologue
    .line 370
    sparse-switch p1, :sswitch_data_28

    .line 388
    :goto_3
    return-void

    .line 372
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->u:Lcom/google/android/youtube/app/honeycomb/tablet/y;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/y;->b()V

    .line 373
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->i:Lcom/google/android/youtube/app/ui/dd;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dd;->c()V

    goto :goto_3

    .line 376
    :sswitch_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->j:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    goto :goto_3

    .line 379
    :sswitch_15
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->k:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    goto :goto_3

    .line 382
    :sswitch_1b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->l:Lcom/google/android/youtube/core/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/j;->c()V

    goto :goto_3

    .line 385
    :sswitch_21
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->m:Lcom/google/android/youtube/app/ui/cy;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/cy;->c()V

    goto :goto_3

    .line 370
    nop

    :sswitch_data_28
    .sparse-switch
        0x7f080056 -> :sswitch_4
        0x7f080057 -> :sswitch_f
        0x7f080059 -> :sswitch_1b
        0x7f0800c2 -> :sswitch_21
        0x7f0800c3 -> :sswitch_15
    .end sparse-switch
.end method

.method public final b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 339
    if-eqz p1, :cond_e

    .line 340
    const-string v0, "selected_card_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 341
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/d;->a(I)V

    .line 343
    :cond_e
    return-void
.end method

.method protected final c(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    .line 425
    packed-switch p1, :pswitch_data_2c

    .line 443
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 427
    :pswitch_5
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/u;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/u;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/q;)V

    .line 435
    new-instance v1, Lcom/google/android/youtube/core/ui/x;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->g:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b01fe

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/x;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_4

    .line 425
    nop

    :pswitch_data_2c
    .packed-switch 0x3ef
        :pswitch_5
    .end packed-switch
.end method

.method protected final c()V
    .registers 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/d;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/q;->b(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->n:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/d;->notifyDataSetInvalidated()V

    .line 394
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->o:Lcom/google/android/youtube/app/ui/g;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/g;->a()V

    .line 395
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 396
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 328
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/q;->d:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->a()V

    .line 329
    return-void
.end method
