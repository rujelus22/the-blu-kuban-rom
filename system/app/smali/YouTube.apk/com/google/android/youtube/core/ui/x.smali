.class public final Lcom/google/android/youtube/core/ui/x;
.super Landroid/app/AlertDialog$Builder;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method


# virtual methods
.method public final create()Landroid/app/AlertDialog;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/youtube/core/ui/x;->a:Landroid/view/View;

    if-eqz v1, :cond_23

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/x;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_23

    .line 42
    iget-object v1, p0, Lcom/google/android/youtube/core/ui/x;->a:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/youtube/core/ui/x;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090011

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 44
    :cond_23
    iget-object v1, p0, Lcom/google/android/youtube/core/ui/x;->a:Landroid/view/View;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog;->setView(Landroid/view/View;IIII)V

    .line 45
    return-object v0
.end method

.method public final setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;
    .registers 3
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/youtube/core/ui/x;->a:Landroid/view/View;

    .line 51
    invoke-super {p0, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method
