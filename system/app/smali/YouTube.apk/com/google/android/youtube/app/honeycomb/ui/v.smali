.class final Lcom/google/android/youtube/app/honeycomb/ui/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/p;

.field private final b:Lcom/google/android/youtube/core/b/an;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/b/an;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/v;->b:Lcom/google/android/youtube/core/b/an;

    .line 255
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/b/an;B)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/ui/v;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/p;Lcom/google/android/youtube/core/b/an;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 249
    const-string v0, "Error downloading video info"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 249
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/v;->b:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->defaultThumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/p;->h(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/ui/w;

    invoke-direct {v3, p0, p2}, Lcom/google/android/youtube/app/honeycomb/ui/w;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/v;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    :cond_26
    return-void
.end method
