.class final Lcom/google/android/youtube/app/adapter/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

.field private final b:Lcom/google/android/youtube/app/adapter/z;

.field private final c:Lcom/google/android/youtube/core/model/UserProfile;

.field private final d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/t;->b:Lcom/google/android/youtube/app/adapter/z;

    .line 255
    iput-object p3, p0, Lcom/google/android/youtube/app/adapter/t;->c:Lcom/google/android/youtube/core/model/UserProfile;

    .line 256
    iput-boolean p4, p0, Lcom/google/android/youtube/app/adapter/t;->d:Z

    .line 257
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;ZB)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 246
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/adapter/t;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/app/adapter/z;Lcom/google/android/youtube/core/model/UserProfile;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 10
    .parameter

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->h(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)Lcom/google/android/youtube/core/b/al;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/t;->c:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v7, v0, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    new-instance v0, Lcom/google/android/youtube/app/adapter/y;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/t;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/t;->c:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, p0, Lcom/google/android/youtube/app/adapter/t;->b:Lcom/google/android/youtube/app/adapter/z;

    iget-boolean v5, p0, Lcom/google/android/youtube/app/adapter/t;->d:Z

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/y;-><init>(Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/model/UserProfile;Lcom/google/android/youtube/app/adapter/z;Z)V

    invoke-interface {v6, v7, v0}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 267
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 274
    const-string v0, "Ignoring click due to authentication error"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 270
    const-string v0, "Ignoring click due to not authenticated"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 271
    return-void
.end method
