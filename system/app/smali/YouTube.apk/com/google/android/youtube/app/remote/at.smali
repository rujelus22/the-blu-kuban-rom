.class final Lcom/google/android/youtube/app/remote/at;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/aq;

.field private final b:Landroid/app/NotificationManager;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/remote/aq;)V
    .registers 4
    .parameter

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/at;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    invoke-static {p1}, Lcom/google/android/youtube/app/remote/aq;->j(Lcom/google/android/youtube/app/remote/aq;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/at;->b:Landroid/app/NotificationManager;

    .line 276
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 323
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/at;->c:Ljava/lang/String;

    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/at;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 325
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/at;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 320
    :goto_8
    return-void

    .line 283
    :cond_9
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/at;->c:Ljava/lang/String;

    .line 285
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 288
    new-instance v2, Landroid/app/Notification;

    const v3, 0x7f020205

    invoke-direct {v2, v3, p2, v0, v1}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 289
    iget v0, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Landroid/app/Notification;->flags:I

    .line 290
    iget v0, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v2, Landroid/app/Notification;->flags:I

    .line 292
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/at;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->j(Lcom/google/android/youtube/app/remote/aq;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f04007f

    invoke-direct {v1, v0, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 294
    const v0, 0x7f080046

    invoke-virtual {v1, v0, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 295
    const v0, 0x7f080122

    iget-object v3, p0, Lcom/google/android/youtube/app/remote/at;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->k(Lcom/google/android/youtube/app/remote/aq;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 297
    iput-object v1, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 300
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/at;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->c(Lcom/google/android/youtube/app/remote/aq;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->l()Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 301
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 302
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    const-string v3, "guide_selection"

    const-string v4, "REMOTE"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    :goto_66
    iget-object v3, p0, Lcom/google/android/youtube/app/remote/at;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->j(Lcom/google/android/youtube/app/remote/aq;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v3

    const/high16 v4, 0x2000

    or-int/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 312
    iget-object v3, p0, Lcom/google/android/youtube/app/remote/at;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v3}, Lcom/google/android/youtube/app/remote/aq;->j(Lcom/google/android/youtube/app/remote/aq;)Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/high16 v5, 0x1000

    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 315
    iput-object v0, v2, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 316
    if-eqz p3, :cond_94

    .line 317
    const v0, 0x7f080040

    invoke-virtual {v1, v0, p3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 319
    :cond_94
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/at;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_8

    .line 305
    :cond_9d
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://www.youtube.com/watch?v="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_66
.end method
