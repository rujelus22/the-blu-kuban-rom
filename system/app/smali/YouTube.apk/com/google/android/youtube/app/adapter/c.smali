.class final Lcom/google/android/youtube/app/adapter/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/a;

.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/youtube/app/adapter/bs;

.field private final d:Lcom/google/android/youtube/app/adapter/bs;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;Landroid/view/ViewGroup;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/c;->a:Lcom/google/android/youtube/app/adapter/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object p2, p0, Lcom/google/android/youtube/app/adapter/c;->b:Landroid/view/View;

    .line 105
    new-instance v0, Lcom/google/android/youtube/app/adapter/d;

    invoke-direct {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/d;-><init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->c:Lcom/google/android/youtube/app/adapter/bs;

    .line 106
    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/a;->b(Lcom/google/android/youtube/app/adapter/a;)Lcom/google/android/youtube/app/adapter/cb;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/cb;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->d:Lcom/google/android/youtube/app/adapter/bs;

    .line 107
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 97
    check-cast p2, Landroid/util/Pair;

    iget-object v0, p2, Landroid/util/Pair;->first:Ljava/lang/Object;

    const-string v1, "music video cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v1, p2, Landroid/util/Pair;->second:Ljava/lang/Object;

    const-string v2, "video cannot be null"

    invoke-static {v1, v2}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/c;->c:Lcom/google/android/youtube/app/adapter/bs;

    invoke-interface {v2, p1, v0}, Lcom/google/android/youtube/app/adapter/bs;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->d:Lcom/google/android/youtube/app/adapter/bs;

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bs;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/c;->b:Landroid/view/View;

    return-object v0
.end method
