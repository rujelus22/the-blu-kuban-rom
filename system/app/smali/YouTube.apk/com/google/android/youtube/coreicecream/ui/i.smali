.class public final Lcom/google/android/youtube/coreicecream/ui/i;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Lcom/google/android/youtube/core/player/at;
.implements Lcom/google/android/youtube/coreicecream/ui/g;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I


# instance fields
.field private final e:Landroid/view/Window;

.field private final f:Landroid/app/ActionBar;

.field private final g:Landroid/view/View;

.field private final h:[Landroid/view/View;

.field private final i:Lcom/google/android/youtube/coreicecream/ui/h;

.field private j:Landroid/graphics/Rect;

.field private final k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 33
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    sput v0, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    .line 52
    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x1

    .line 54
    sget v2, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_f

    .line 55
    const/4 v0, 0x3

    .line 57
    :cond_f
    sget v2, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_19

    .line 58
    or-int/lit8 v0, v0, 0x4

    .line 59
    const/16 v1, 0x700

    .line 63
    :cond_19
    sput v0, Lcom/google/android/youtube/coreicecream/ui/i;->b:I

    .line 64
    sput v1, Lcom/google/android/youtube/coreicecream/ui/i;->c:I

    .line 65
    or-int/2addr v0, v1

    sput v0, Lcom/google/android/youtube/coreicecream/ui/i;->d:I

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;[Landroid/view/View;Lcom/google/android/youtube/coreicecream/ui/h;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 98
    const-string v0, "window cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->e:Landroid/view/Window;

    .line 99
    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->g:Landroid/view/View;

    .line 101
    invoke-virtual {p3, p0}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->setSystemWindowInsetsListener(Lcom/google/android/youtube/core/player/at;)V

    .line 102
    iput-object p2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->f:Landroid/app/ActionBar;

    .line 103
    iput-object p4, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    .line 104
    iput-object p5, p0, Lcom/google/android/youtube/coreicecream/ui/i;->i:Lcom/google/android/youtube/coreicecream/ui/h;

    .line 105
    invoke-virtual {p3, p0}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 106
    invoke-virtual {p3}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->getSystemUiVisibility()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->l:I

    .line 108
    iput-boolean v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->p:Z

    .line 109
    if-eqz p2, :cond_55

    invoke-virtual {p2}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_55

    move v0, v1

    :goto_36
    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->o:Z

    .line 113
    if-eqz p2, :cond_57

    .line 114
    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [I

    const v3, 0x10102eb

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 116
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/youtube/coreicecream/ui/i;->k:I

    .line 117
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 121
    :goto_54
    return-void

    :cond_55
    move v0, v2

    .line 109
    goto :goto_36

    .line 119
    :cond_57
    iput v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->k:I

    goto :goto_54
.end method

.method private a()V
    .registers 3

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->m:Z

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->n:Z

    if-eqz v0, :cond_10

    sget v0, Lcom/google/android/youtube/coreicecream/ui/i;->d:I

    .line 206
    :goto_a
    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ui/i;->g:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 207
    return-void

    .line 204
    :cond_10
    sget v0, Lcom/google/android/youtube/coreicecream/ui/i;->c:I

    goto :goto_a

    :cond_13
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private b()V
    .registers 9

    .prologue
    const/16 v7, 0x10

    const/4 v1, 0x0

    .line 210
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    if-nez v0, :cond_8

    .line 225
    :cond_7
    return-void

    :cond_8
    move v0, v1

    .line 214
    :goto_9
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 215
    iget-boolean v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->m:Z

    if-eqz v2, :cond_1a

    sget v2, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    if-lt v2, v7, :cond_24

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    if-nez v2, :cond_24

    .line 216
    :cond_1a
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 214
    :goto_21
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 217
    :cond_24
    sget v2, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    if-lt v2, v7, :cond_40

    .line 218
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_21

    .line 219
    :cond_40
    iget-boolean v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->o:Z

    if-eqz v2, :cond_58

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->e:Landroid/view/Window;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/view/Window;->hasFeature(I)Z

    move-result v2

    if-eqz v2, :cond_58

    .line 220
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/google/android/youtube/coreicecream/ui/i;->k:I

    invoke-virtual {v2, v1, v3, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_21

    .line 222
    :cond_58
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->h:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_21
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)V
    .registers 3
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 183
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ui/i;->j:Landroid/graphics/Rect;

    .line 184
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/i;->b()V

    .line 186
    :cond_d
    return-void
.end method

.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    const/16 v2, 0x400

    const/4 v1, 0x0

    .line 124
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->m:Z

    if-eq v0, p1, :cond_47

    .line 125
    if-eqz p1, :cond_18

    .line 126
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->e:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_48

    const/4 v0, 0x1

    :goto_16
    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->p:Z

    .line 130
    :cond_18
    iput-boolean p1, p0, Lcom/google/android/youtube/coreicecream/ui/i;->m:Z

    .line 131
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/i;->a()V

    .line 132
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/i;->b()V

    .line 133
    sget v0, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_32

    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->e:Landroid/view/Window;

    if-nez p1, :cond_2e

    iget-boolean v3, p0, Lcom/google/android/youtube/coreicecream/ui/i;->p:Z

    if-eqz v3, :cond_2f

    :cond_2e
    move v1, v2

    :cond_2f
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 139
    :cond_32
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->o:Z

    if-eqz v0, :cond_47

    .line 140
    if-eqz p1, :cond_4a

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->e:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 147
    :cond_47
    :goto_47
    return-void

    :cond_48
    move v0, v1

    .line 126
    goto :goto_16

    .line 142
    :cond_4a
    if-nez p1, :cond_47

    .line 143
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_47
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/google/android/youtube/coreicecream/ui/i;->n:Z

    .line 151
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ui/i;->removeMessages(I)V

    .line 152
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/i;->a()V

    .line 153
    sget v0, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_28

    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->m:Z

    if-eqz v0, :cond_28

    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->o:Z

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->e:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 155
    if-eqz p1, :cond_29

    .line 156
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 161
    :cond_28
    :goto_28
    return-void

    .line 158
    :cond_29
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ui/i;->f:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_28
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .registers 3
    .parameter

    .prologue
    .line 190
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_a

    .line 195
    :goto_5
    return-void

    .line 192
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/ui/i;->a()V

    goto :goto_5

    .line 190
    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_6
    .end packed-switch
.end method

.method public final onSystemUiVisibilityChange(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 164
    sget v2, Lcom/google/android/youtube/coreicecream/ui/i;->a:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_15

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    if-eq v2, p1, :cond_15

    .line 167
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->g:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 169
    :cond_15
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->i:Lcom/google/android/youtube/coreicecream/ui/h;

    if-eqz v2, :cond_28

    iget v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->l:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_28

    and-int/lit8 v2, p1, 0x2

    if-nez v2, :cond_28

    .line 171
    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->i:Lcom/google/android/youtube/coreicecream/ui/h;

    invoke-interface {v2}, Lcom/google/android/youtube/coreicecream/ui/h;->q()V

    .line 173
    :cond_28
    iput p1, p0, Lcom/google/android/youtube/coreicecream/ui/i;->l:I

    .line 174
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/coreicecream/ui/i;->removeMessages(I)V

    .line 176
    iget-boolean v2, p0, Lcom/google/android/youtube/coreicecream/ui/i;->m:Z

    iget-boolean v3, p0, Lcom/google/android/youtube/coreicecream/ui/i;->n:Z

    and-int/2addr v3, v2

    sget v2, Lcom/google/android/youtube/coreicecream/ui/i;->b:I

    and-int/2addr v2, p1

    sget v4, Lcom/google/android/youtube/coreicecream/ui/i;->b:I

    if-ne v2, v4, :cond_44

    move v2, v0

    :goto_3a
    if-eq v3, v2, :cond_46

    :goto_3c
    if-eqz v0, :cond_43

    .line 177
    const-wide/16 v2, 0x9c4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/youtube/coreicecream/ui/i;->sendEmptyMessageDelayed(IJ)Z

    .line 179
    :cond_43
    return-void

    :cond_44
    move v2, v1

    .line 176
    goto :goto_3a

    :cond_46
    move v0, v1

    goto :goto_3c
.end method
