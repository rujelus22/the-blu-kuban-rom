.class public final Lcom/google/android/youtube/core/player/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field private static final i:Ljava/util/Set;


# instance fields
.field private A:Z

.field private B:Z

.field private final C:Lcom/google/android/youtube/core/player/ca;

.field private final D:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final E:Lcom/google/android/youtube/core/player/ce;

.field private F:Z

.field private G:Z

.field private final j:Landroid/content/Context;

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;

.field private final l:Lcom/google/android/youtube/core/player/cd;

.field private final m:Landroid/os/Handler;

.field private final n:Lcom/google/android/youtube/core/player/au;

.field private final o:Lcom/google/android/youtube/core/player/cc;

.field private p:Z

.field private volatile q:Z

.field private volatile r:Z

.field private volatile s:Z

.field private volatile t:Z

.field private volatile u:Z

.field private volatile v:Z

.field private volatile w:Z

.field private volatile x:I

.field private y:Lcom/google/android/youtube/core/model/Stream;

.field private z:Lcom/google/android/youtube/core/model/Stream;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const v3, 0x7fffffff

    const/16 v2, -0xbb8

    .line 130
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xf

    if-le v0, v1, :cond_c2

    .line 132
    const/16 v0, -0xbb6

    sput v0, Lcom/google/android/youtube/core/player/bx;->a:I

    .line 133
    const/16 v0, -0xbb5

    sput v0, Lcom/google/android/youtube/core/player/bx;->b:I

    .line 134
    const/16 v0, -0xbb4

    sput v0, Lcom/google/android/youtube/core/player/bx;->c:I

    .line 135
    const/16 v0, -0xbb3

    sput v0, Lcom/google/android/youtube/core/player/bx;->d:I

    .line 136
    const/16 v0, -0xbb2

    sput v0, Lcom/google/android/youtube/core/player/bx;->e:I

    .line 137
    const/16 v0, -0xbb1

    sput v0, Lcom/google/android/youtube/core/player/bx;->f:I

    .line 138
    const/16 v0, -0xbb0

    sput v0, Lcom/google/android/youtube/core/player/bx;->g:I

    .line 139
    sput v2, Lcom/google/android/youtube/core/player/bx;->h:I

    .line 172
    :goto_2b
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 173
    const/16 v1, -0x3e81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 174
    const/16 v1, -0x3f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 175
    const/16 v1, -0x7d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 176
    const/16 v1, -0x7d1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 177
    sget v1, Lcom/google/android/youtube/core/player/bx;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 178
    sget v1, Lcom/google/android/youtube/core/player/bx;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 179
    sget v1, Lcom/google/android/youtube/core/player/bx;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 180
    sget v1, Lcom/google/android/youtube/core/player/bx;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 181
    sget v1, Lcom/google/android/youtube/core/player/bx;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 182
    sget v1, Lcom/google/android/youtube/core/player/bx;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 183
    sget v1, Lcom/google/android/youtube/core/player/bx;->g:I

    if-eq v1, v3, :cond_97

    .line 184
    sget v1, Lcom/google/android/youtube/core/player/bx;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_97
    sget v1, Lcom/google/android/youtube/core/player/bx;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 187
    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 188
    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 189
    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/player/bx;->i:Ljava/util/Set;

    .line 191
    return-void

    .line 142
    :cond_c2
    sput v2, Lcom/google/android/youtube/core/player/bx;->a:I

    .line 143
    const/16 v0, -0xbb9

    sput v0, Lcom/google/android/youtube/core/player/bx;->b:I

    .line 144
    const/16 v0, -0xbba

    sput v0, Lcom/google/android/youtube/core/player/bx;->c:I

    .line 145
    const/16 v0, -0xbbb

    sput v0, Lcom/google/android/youtube/core/player/bx;->d:I

    .line 146
    const/16 v0, -0xbbc

    sput v0, Lcom/google/android/youtube/core/player/bx;->e:I

    .line 147
    const/16 v0, -0xbbd

    sput v0, Lcom/google/android/youtube/core/player/bx;->f:I

    .line 148
    sput v3, Lcom/google/android/youtube/core/player/bx;->g:I

    .line 149
    const/16 v0, -0xbbe

    sput v0, Lcom/google/android/youtube/core/player/bx;->h:I

    goto/16 :goto_2b
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/au;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 243
    new-instance v0, Lcom/google/android/youtube/core/player/bz;

    invoke-direct {v0}, Lcom/google/android/youtube/core/player/bz;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/core/player/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/au;Lcom/google/android/youtube/core/player/cc;)V

    .line 244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/au;Lcom/google/android/youtube/core/player/cc;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->j:Landroid/content/Context;

    .line 249
    const-string v0, "playerSurface cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/au;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->n:Lcom/google/android/youtube/core/player/au;

    .line 250
    const-string v0, "mediaPlayerFactory cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/cc;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->o:Lcom/google/android/youtube/core/player/cc;

    .line 252
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    .line 253
    new-instance v0, Lcom/google/android/youtube/core/player/ca;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/ca;-><init>(Lcom/google/android/youtube/core/player/bx;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->C:Lcom/google/android/youtube/core/player/ca;

    .line 254
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 256
    new-instance v0, Lcom/google/android/youtube/core/player/cd;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/cd;-><init>(Lcom/google/android/youtube/core/player/bx;B)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    .line 257
    new-instance v0, Lcom/google/android/youtube/core/player/ce;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/ce;-><init>(Lcom/google/android/youtube/core/player/bx;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    .line 258
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->m:Landroid/os/Handler;

    .line 260
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->C:Lcom/google/android/youtube/core/player/ca;

    invoke-interface {p2, v0}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/av;)V

    .line 262
    invoke-interface {p2}, Lcom/google/android/youtube/core/player/au;->a()Landroid/view/View;

    move-result-object v0

    .line 263
    if-eqz v0, :cond_63

    .line 265
    new-instance v1, Lcom/google/android/youtube/core/player/by;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/by;-><init>(Lcom/google/android/youtube/core/player/bx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 273
    :cond_63
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->start()V

    .line 274
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ce;->start()V

    .line 275
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->s:Z

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aq;

    if-eqz v0, :cond_2a

    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/ce;->b()V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->v:Z

    if-nez v1, :cond_27

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->w:Z

    if-nez v1, :cond_27

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/player/bx;->b(I)V

    :cond_27
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->release()V

    :cond_2a
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/bx;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-static {v0, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_6

    :cond_1c
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;III)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v2, p1, p2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_6

    :cond_1f
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;Lcom/google/android/youtube/core/model/Stream;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/bx;->b(Lcom/google/android/youtube/core/model/Stream;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;Lcom/google/android/youtube/core/player/aq;Landroid/net/Uri;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_a5

    if-eqz p2, :cond_a5

    :try_start_9
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->n:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/aq;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->p:Z

    if-eqz v1, :cond_24

    invoke-interface {p1}, Lcom/google/android/youtube/core/player/aq;->c()Z

    move-result v1

    if-eqz v1, :cond_24

    const-string v1, "x-disconnect-at-highwatermark"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_24
    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->j:Landroid/content/Context;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_89

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_2f
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_2f} :catch_79
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_2f} :catch_8d
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_2f} :catch_9e

    move-result v2

    if-nez v2, :cond_89

    :try_start_32
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setDataSource"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/net/Uri;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-class v6, Ljava/util/Map;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5d
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_32 .. :try_end_5d} :catch_69
    .catch Ljava/lang/NoSuchMethodException; {:try_start_32 .. :try_end_5d} :catch_83
    .catch Ljava/lang/IllegalAccessException; {:try_start_32 .. :try_end_5d} :catch_97
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_5d} :catch_79
    .catch Ljava/lang/IllegalArgumentException; {:try_start_32 .. :try_end_5d} :catch_8d
    .catch Ljava/lang/IllegalStateException; {:try_start_32 .. :try_end_5d} :catch_9e

    :goto_5d
    :try_start_5d
    invoke-interface {p1}, Lcom/google/android/youtube/core/player/aq;->prepareAsync()V

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/aq;->setScreenOnWhilePlaying(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(Z)V

    :goto_68
    return-void

    :catch_69
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    instance-of v2, v2, Ljava/io/IOException;

    if-eqz v2, :cond_89

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0
    :try_end_79
    .catch Ljava/io/IOException; {:try_start_5d .. :try_end_79} :catch_79
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5d .. :try_end_79} :catch_8d
    .catch Ljava/lang/IllegalStateException; {:try_start_5d .. :try_end_79} :catch_9e

    :catch_79
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->a(Ljava/lang/Exception;)V

    goto :goto_68

    :catch_83
    move-exception v0

    :try_start_84
    const-string v2, "invoke failed"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_89
    :goto_89
    invoke-interface {p1, v1, p2}, Lcom/google/android/youtube/core/player/aq;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_8c
    .catch Ljava/io/IOException; {:try_start_84 .. :try_end_8c} :catch_79
    .catch Ljava/lang/IllegalArgumentException; {:try_start_84 .. :try_end_8c} :catch_8d
    .catch Ljava/lang/IllegalStateException; {:try_start_84 .. :try_end_8c} :catch_9e

    goto :goto_5d

    :catch_8d
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->a(Ljava/lang/Exception;)V

    goto :goto_68

    :catch_97
    move-exception v0

    :try_start_98
    const-string v2, "invoke failed"

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9d
    .catch Ljava/io/IOException; {:try_start_98 .. :try_end_9d} :catch_79
    .catch Ljava/lang/IllegalArgumentException; {:try_start_98 .. :try_end_9d} :catch_8d
    .catch Ljava/lang/IllegalStateException; {:try_start_98 .. :try_end_9d} :catch_9e

    goto :goto_89

    :catch_9e
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_68

    :cond_a5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Media Player null pointer preparing video "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->a(Ljava/lang/Exception;)V

    goto :goto_68
.end method

.method private a(Ljava/lang/Exception;)V
    .registers 5
    .parameter

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 929
    const/16 v2, 0x9

    invoke-static {v0, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_6

    .line 931
    :cond_1c
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/bx;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/bx;->A:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/bx;Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/bx;->y:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 922
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    .line 923
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_6

    .line 925
    :cond_16
    return-void
.end method

.method private b(Lcom/google/android/youtube/core/model/Stream;)V
    .registers 5
    .parameter

    .prologue
    .line 330
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->c()V

    .line 332
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->A:Z

    if-nez v0, :cond_f

    .line 333
    iput-object p1, p0, Lcom/google/android/youtube/core/player/bx;->y:Lcom/google/android/youtube/core/model/Stream;

    .line 350
    :goto_e
    return-void

    .line 338
    :cond_f
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->c(Z)V

    .line 341
    :try_start_13
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->o:Lcom/google/android/youtube/core/player/cc;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/player/cc;->a(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/aq;

    move-result-object v0

    .line 342
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aq;->setAudioStreamType(I)V

    .line 343
    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->C:Lcom/google/android/youtube/core/player/ca;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aq;->a(Lcom/google/android/youtube/core/player/ar;)V

    .line 345
    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/core/player/cd;->a(Lcom/google/android/youtube/core/player/aq;Landroid/net/Uri;)V
    :try_end_2b
    .catch Ljava/lang/InstantiationException; {:try_start_13 .. :try_end_2b} :catch_2c

    goto :goto_e

    .line 346
    :catch_2c
    move-exception v0

    .line 347
    const-string v1, "Factory failed to create a MediaPlayer for the stream"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 348
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->a(Ljava/lang/Exception;)V

    goto :goto_e
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/bx;)V
    .registers 3
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aq;

    if-eqz v0, :cond_32

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    :try_start_d
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    if-eqz v1, :cond_33

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    if-nez v1, :cond_1f

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->s:Z

    if-eqz v1, :cond_1f

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    :cond_1f
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->w:Z

    if-nez v0, :cond_2f

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->s:Z

    if-eqz v0, :cond_2f

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    if-eqz v0, :cond_2f

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(I)V

    :cond_2f
    :goto_2f
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->w:Z

    :cond_32
    :goto_32
    return-void

    :cond_33
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bx;->k()Z

    move-result v1

    if-eqz v1, :cond_2f

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->w:Z

    if-nez v0, :cond_47

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(I)V

    :cond_47
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ce;->a()V
    :try_end_4c
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_4c} :catch_4d

    goto :goto_2f

    :catch_4d
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_32
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/bx;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aq;

    if-eqz v0, :cond_33

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bx;->k()Z

    move-result v1

    if-eqz v1, :cond_33

    :try_start_10
    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_34

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-static {v1, v3, p1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_2c
    .catch Ljava/lang/IllegalStateException; {:try_start_10 .. :try_end_2c} :catch_2d

    goto :goto_16

    :catch_2d
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_33
    :goto_33
    return-void

    :cond_34
    :try_start_34
    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aq;->seekTo(I)V
    :try_end_37
    .catch Ljava/lang/IllegalStateException; {:try_start_34 .. :try_end_37} :catch_2d

    goto :goto_33
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/bx;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v0, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_6

    :cond_1c
    return-void
.end method

.method private b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    if-eqz v0, :cond_5

    .line 557
    :cond_4
    :goto_4
    return-void

    .line 553
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->u:Z

    if-eq v0, p1, :cond_4

    .line 554
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/bx;->u:Z

    .line 555
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->u:Z

    if-eqz v0, :cond_14

    const/4 v0, 0x6

    :goto_10
    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(I)V

    goto :goto_4

    :cond_14
    const/4 v0, 0x7

    goto :goto_10
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/bx;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->s:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/bx;)V
    .registers 3
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aq;

    if-eqz v0, :cond_21

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bx;->k()Z

    move-result v1

    if-eqz v1, :cond_21

    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    :try_start_13
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->pause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->b(Z)V
    :try_end_21
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_21} :catch_22

    :cond_21
    :goto_21
    return-void

    :catch_22
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_21
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/bx;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v2, 0xb

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_6

    :cond_1d
    return-void
.end method

.method private c(Z)V
    .registers 4
    .parameter

    .prologue
    .line 973
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/bx;->G:Z

    .line 974
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->n:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/au;->a()Landroid/view/View;

    move-result-object v1

    .line 975
    if-eqz v1, :cond_16

    .line 977
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->F:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->G:Z

    if-eqz v0, :cond_17

    :cond_12
    const/4 v0, 0x1

    :goto_13
    invoke-virtual {v1, v0}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 979
    :cond_16
    return-void

    .line 977
    :cond_17
    const/4 v0, 0x0

    goto :goto_13
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/bx;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/bx;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/bx;->x:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->y:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/bx;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/bx;->v:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/bx;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/bx;->b(Z)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bx;->l()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ce;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/bx;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/bx;->c(Z)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/bx;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->j:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/bx;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/au;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->n:Lcom/google/android/youtube/core/player/au;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/core/player/bx;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/bx;->w:Z

    return p1
.end method

.method static synthetic i(Lcom/google/android/youtube/core/player/bx;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->m:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic j()Ljava/util/Set;
    .registers 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/youtube/core/player/bx;->i:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    return v0
.end method

.method private k()Z
    .registers 2

    .prologue
    .line 523
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->s:Z

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->B:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method static synthetic k(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bx;->k()Z

    move-result v0

    return v0
.end method

.method private l()Z
    .registers 3

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    if-eqz v0, :cond_1e

    const-string v0, "video/wvm"

    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method static synthetic l(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->v:Z

    return v0
.end method

.method static synthetic n(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->w:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/youtube/core/player/bx;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->s:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/core/player/bx;)I
    .registers 3
    .parameter

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/youtube/core/player/bx;->x:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/player/bx;->x:I

    return v0
.end method

.method static synthetic q(Lcom/google/android/youtube/core/player/bx;)I
    .registers 2
    .parameter

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/youtube/core/player/bx;->x:I

    return v0
.end method

.method static synthetic r(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/player/ca;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->C:Lcom/google/android/youtube/core/player/ca;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/core/player/bx;)Lcom/google/android/youtube/core/model/Stream;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/youtube/core/player/bx;)Ljava/util/concurrent/atomic/AtomicReference;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/aq;

    .line 287
    if-nez v0, :cond_d

    sget-object v0, Lcom/google/android/youtube/core/player/ae;->a:Ljava/util/Set;

    :goto_c
    return-object v0

    :cond_d
    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aq;->b()Ljava/util/Set;

    move-result-object v0

    goto :goto_c
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    if-nez v0, :cond_31

    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-eq v0, p1, :cond_31

    .line 477
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 478
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v2}, Lcom/google/android/youtube/core/player/ce;->b(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/cd;->a(I)V

    .line 480
    :cond_31
    return-void
.end method

.method public final a(Landroid/os/Handler;)V
    .registers 3
    .parameter

    .prologue
    .line 908
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->D:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 909
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Stream;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 320
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    .line 321
    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    .line 322
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->B:Z

    .line 323
    iput-object p1, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->n:Lcom/google/android/youtube/core/player/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->C:Lcom/google/android/youtube/core/player/ca;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/av;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 326
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/bx;->b(Lcom/google/android/youtube/core/model/Stream;)V

    .line 327
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Stream;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 299
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/model/Stream;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    const/4 v0, 0x1

    :goto_e
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->q:Z

    .line 300
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/bx;->r:Z

    .line 301
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/model/Stream;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    if-eqz v0, :cond_25

    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->n:Lcom/google/android/youtube/core/player/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/bx;->C:Lcom/google/android/youtube/core/player/ca;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/av;)V

    .line 304
    :cond_25
    iput-object p1, p0, Lcom/google/android/youtube/core/player/bx;->z:Lcom/google/android/youtube/core/model/Stream;

    .line 305
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 307
    const-string v0, "application/x-mpegURL"

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->B:Z

    .line 311
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->B:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/bx;->l()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->B:Z

    .line 313
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/bx;->b(Lcom/google/android/youtube/core/model/Stream;)V

    .line 314
    return-void

    :cond_47
    move v0, v1

    .line 299
    goto :goto_e
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 278
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/bx;->p:Z

    .line 279
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/bx;->t:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->a(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/ce;->b(Lcom/google/android/youtube/core/player/ce;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->a()V

    .line 407
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->b()V

    .line 455
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->c()V

    .line 497
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->d()V

    .line 501
    return-void
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->E:Lcom/google/android/youtube/core/player/ce;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ce;->c()V

    .line 561
    iget-object v0, p0, Lcom/google/android/youtube/core/player/bx;->l:Lcom/google/android/youtube/core/player/cd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/cd;->e()V

    .line 562
    return-void
.end method
