.class public final Lcom/google/android/youtube/app/ui/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/aj;
.implements Lcom/google/android/youtube/core/player/k;


# static fields
.field private static final a:Ljava/util/Set;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Landroid/widget/ImageButton;

.field private K:I

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/youtube/core/b/an;

.field private final d:Lcom/google/android/youtube/core/b/as;

.field private final e:Lcom/google/android/youtube/core/Analytics;

.field private final f:Lcom/google/android/youtube/core/async/l;

.field private g:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final h:Lcom/google/android/youtube/core/player/bx;

.field private final i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/LinearLayout;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/ProgressBar;

.field private s:Lcom/google/android/youtube/core/ui/l;

.field private t:Lcom/google/android/youtube/core/player/ControllerOverlay;

.field private final u:Landroid/os/Handler;

.field private final v:Lcom/google/android/youtube/app/ui/bt;

.field private final w:Lcom/google/android/youtube/app/ui/bs;

.field private final x:Z

.field private y:Lcom/google/android/youtube/core/model/Video;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 100
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/ytremote/a/a/a;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/ui/bg;->a:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;Lcom/google/android/youtube/app/ui/bt;Z)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->G:Z

    .line 199
    const-string v0, "player can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/bx;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->h:Lcom/google/android/youtube/core/player/bx;

    .line 200
    const-string v0, "activity can not be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    .line 201
    const-string v0, "imageClient can not be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/an;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->c:Lcom/google/android/youtube/core/b/an;

    .line 202
    const-string v0, "subtitlesClient cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/as;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->d:Lcom/google/android/youtube/core/b/as;

    .line 204
    const-string v0, "analytics can not be null"

    invoke-static {p5, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    .line 205
    const-string v0, "controllerOverlay cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ControllerOverlay;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    .line 207
    const-string v0, "fullscreenListener can not be null"

    invoke-static {p7, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bs;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->w:Lcom/google/android/youtube/app/ui/bs;

    .line 209
    const-string v0, "listener can not be null"

    invoke-static {p8, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bt;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    .line 210
    iput-boolean p9, p0, Lcom/google/android/youtube/app/ui/bg;->x:Z

    .line 212
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v0, 0x7f040092

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const v1, 0x7f0800cd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->m:Landroid/widget/ImageView;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->l:Landroid/widget/LinearLayout;

    const v1, 0x7f0800ca

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->k:Landroid/widget/TextView;

    const v1, 0x7f0800cc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v1, :cond_ad

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v1, :cond_ad

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->c:Lcom/google/android/youtube/core/b/an;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bg;->f:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v1, v3, v4}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    :cond_ad
    const v1, 0x7f0800ce

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    const v3, 0x7f080127

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->o:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    const v3, 0x7f080128

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setListener(Lcom/google/android/youtube/core/player/k;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setShowFullscreen(Z)V

    new-instance v1, Lcom/google/android/youtube/app/ui/bo;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/bo;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f040091

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->j:Landroid/view/View;

    new-instance v4, Lcom/google/android/youtube/app/ui/bm;

    invoke-direct {v4, p0}, Lcom/google/android/youtube/app/ui/bm;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-static {v1, v3, v4}, Lcom/google/android/youtube/core/ui/l;->a(Landroid/content/Context;Landroid/view/View;Lcom/google/android/youtube/core/ui/m;)Lcom/google/android/youtube/core/ui/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/ui/l;->a()V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->j:Landroid/view/View;

    const v3, 0x7f08012f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->J:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->J:Landroid/widget/ImageButton;

    new-instance v3, Lcom/google/android/youtube/app/ui/bp;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/ui/bp;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->z()V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->j:Landroid/view/View;

    new-instance v3, Lcom/google/android/youtube/app/ui/bq;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/ui/bq;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f080039

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const v1, 0x7f08003a

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->r:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/youtube/app/ui/br;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/ui/br;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->i:Landroid/view/View;

    .line 214
    new-instance v0, Lcom/google/android/youtube/app/ui/bh;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/bh;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->f:Lcom/google/android/youtube/core/async/l;

    .line 227
    new-instance v0, Lcom/google/android/youtube/app/ui/bk;

    invoke-virtual {p2}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/bk;-><init>(Lcom/google/android/youtube/app/ui/bg;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    .line 258
    return-void
.end method

.method private A()V
    .registers 3

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 858
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->f()V

    .line 859
    return-void
.end method

.method private B()V
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 885
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->A()V

    .line 886
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 887
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/l;->b()V

    .line 889
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->D()V

    .line 890
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 891
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_38

    .line 892
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 898
    :cond_38
    :goto_38
    return-void

    .line 896
    :cond_39
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_38
.end method

.method private C()V
    .registers 2

    .prologue
    .line 948
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 949
    return-void
.end method

.method private D()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 963
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_b

    .line 964
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, v3, v3, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setTimes(III)V

    .line 978
    :goto_a
    return-void

    .line 967
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    .line 968
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_45

    .line 969
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_42

    .line 970
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    .line 977
    :goto_38
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget v2, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    const/16 v3, 0x64

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setTimes(III)V

    goto :goto_a

    .line 972
    :cond_42
    iput v0, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    goto :goto_38

    .line 975
    :cond_45
    iput v3, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    goto :goto_38
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bg;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->m:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;)Lcom/google/android/youtube/app/ui/bg;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 179
    new-instance v0, Lcom/google/android/youtube/app/ui/bg;

    sget-object v8, Lcom/google/android/youtube/app/ui/bt;->a:Lcom/google/android/youtube/app/ui/bt;

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/app/ui/bg;-><init>(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;Lcom/google/android/youtube/app/ui/bt;Z)V

    return-object v0
.end method

.method public static a(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;Lcom/google/android/youtube/app/ui/bt;)Lcom/google/android/youtube/app/ui/bg;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/youtube/app/ui/bg;

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/youtube/app/ui/bg;-><init>(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;Lcom/google/android/youtube/app/ui/bt;Z)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/ui/l;->a(Ljava/lang/CharSequence;Z)V

    .line 870
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bt;->p()V

    .line 872
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->x()V

    .line 873
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 874
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 875
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bg;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->C:Z

    return v0
.end method

.method private b(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .registers 5
    .parameter

    .prologue
    .line 931
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq p1, v0, :cond_10

    .line 932
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteStateChange"

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 936
    :goto_f
    return-void

    .line 934
    :cond_10
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteError"

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->x()Lcom/google/android/youtube/app/remote/al;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/app/remote/al;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_f
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/bg;)Z
    .registers 3
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    if-eqz v0, :cond_3a

    sget-object v0, Lcom/google/android/youtube/app/ui/bg;->a:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v0, 0x1

    :goto_39
    return v0

    :cond_3a
    const/4 v0, 0x0

    goto :goto_39
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/bg;)V
    .registers 1
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->D()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 911
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/core/ui/l;->a(Ljava/lang/CharSequence;Z)V

    .line 912
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bt;->p()V

    .line 914
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->x()V

    .line 915
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 916
    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/bg;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    return-object v0
.end method

.method private e(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 952
    if-eqz p1, :cond_14

    .line 953
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const v1, 0x7f020100

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 954
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const v1, 0x7f0b0238

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 960
    :goto_13
    return-void

    .line 956
    :cond_14
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const v1, 0x7f020101

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 958
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const v1, 0x7f0b0239

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_13
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/bg;)V
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    const v1, 0x7f0b024b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/youtube/core/ui/l;->a(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bt;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->x()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/bg;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/player/ControllerOverlay;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/ui/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->C:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/ui/bg;)Z
    .registers 3
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method static synthetic n(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/player/bx;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->h:Lcom/google/android/youtube/core/player/bx;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/ui/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->I:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/app/ui/bg;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/youtube/app/ui/bg;)Landroid/widget/ProgressBar;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->r:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private v()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 657
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    if-nez v0, :cond_21

    .line 658
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->h:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->f()V

    .line 659
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->h:Lcom/google/android/youtube/core/player/bx;

    new-instance v1, Lcom/google/android/youtube/app/ui/bn;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/bn;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/bx;->a(Landroid/os/Handler;)V

    .line 672
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/ui/bt;->c(Z)V

    .line 674
    iput-boolean v2, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    .line 677
    :cond_21
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v0

    if-eqz v0, :cond_39

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v0

    :goto_35
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 679
    return-void

    .line 677
    :cond_39
    const-string v0, ""

    goto :goto_35
.end method

.method private w()Ljava/lang/String;
    .registers 6

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v0

    if-eqz v0, :cond_35

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 687
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->getStringId()I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 690
    :goto_34
    return-object v0

    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    const v1, 0x7f0b0018

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_34
.end method

.method private x()V
    .registers 3

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 695
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 696
    return-void
.end method

.method private y()Z
    .registers 3

    .prologue
    .line 783
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private z()V
    .registers 4

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    if-eqz v0, :cond_1e

    .line 800
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    .line 801
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/bg;->I:Z

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setFullscreen(Z)V

    .line 802
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 805
    :cond_1e
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->J:Landroid/widget/ImageButton;

    if-eqz v0, :cond_30

    .line 806
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->J:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->G:Z

    if-eqz v0, :cond_2c

    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->H:Z

    if-eqz v0, :cond_31

    :cond_2c
    const/4 v0, 0x0

    :goto_2d
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 809
    :cond_30
    return-void

    .line 806
    :cond_31
    const/16 v0, 0x8

    goto :goto_2d
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_16

    .line 288
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    .line 289
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/aj;)V

    .line 290
    sget-object v0, Lcom/google/android/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 293
    :cond_16
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-nez v0, :cond_7

    .line 585
    :cond_6
    :goto_6
    return-void

    .line 581
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(I)V

    .line 582
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    if-ge p1, v0, :cond_6

    .line 583
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    goto :goto_6
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 316
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_17

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->G:Z

    .line 317
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->x:Z

    if-nez v0, :cond_13

    .line 318
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->G:Z

    if-nez v0, :cond_19

    :goto_11
    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/bg;->I:Z

    .line 320
    :cond_13
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->z()V

    .line 321
    return-void

    :cond_17
    move v0, v2

    .line 316
    goto :goto_7

    :cond_19
    move v1, v2

    .line 318
    goto :goto_11
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .registers 10
    .parameter

    .prologue
    const v7, 0x7f0b0250

    const/16 v6, 0x8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x2

    .line 346
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 397
    :cond_20
    :goto_20
    return-void

    .line 350
    :cond_21
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v2

    if-nez v2, :cond_44

    .line 351
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->w()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/ui/bg;->a(Ljava/lang/String;Z)V

    .line 353
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_20

    .line 357
    :cond_44
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v3, "RemotePlayerStateChange"

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    sget-object v2, Lcom/google/android/youtube/app/ui/bj;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_134

    goto :goto_20

    .line 361
    :pswitch_5b
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->B()V

    .line 362
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setPlaying()V

    .line 363
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_20

    .line 366
    :pswitch_69
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->B()V

    .line 367
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    .line 368
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_20

    .line 371
    :pswitch_77
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->B()V

    .line 372
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->g()V

    .line 373
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_20

    .line 376
    :pswitch_85
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->m()Lcom/google/android/youtube/app/remote/ak;

    move-result-object v2

    if-nez v2, :cond_a0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v0, v7, v1}, Lcom/google/android/youtube/core/ui/l;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bt;->p()V

    :goto_97
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->x()V

    .line 377
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_20

    .line 376
    :cond_a0
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/ui/l;->b()V

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setPlaying()V

    iget-object v3, v2, Lcom/google/android/youtube/app/remote/ak;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_de

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->o:Landroid/widget/TextView;

    iget-object v4, v2, Lcom/google/android/youtube/app/remote/ak;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_b9
    iget-object v3, v2, Lcom/google/android/youtube/app/remote/ak;->b:Landroid/net/Uri;

    if-eqz v3, :cond_e4

    sget-object v3, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v4, v2, Lcom/google/android/youtube/app/remote/ak;->b:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e4

    :goto_c7
    if-eqz v0, :cond_e6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->p:Landroid/view/View;

    new-instance v3, Lcom/google/android/youtube/app/ui/bi;

    invoke-direct {v3, p0, v2}, Lcom/google/android/youtube/app/ui/bi;-><init>(Lcom/google/android/youtube/app/ui/bg;Lcom/google/android/youtube/app/remote/ak;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_d8
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_97

    :cond_de
    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->o:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_b9

    :cond_e4
    move v0, v1

    goto :goto_c7

    :cond_e6
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->p:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_d8

    .line 380
    :pswitch_ec
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->g()V

    .line 381
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->x()V

    .line 382
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->o()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Ljava/lang/String;Z)V

    .line 383
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_20

    .line 388
    :pswitch_10a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setLoading()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->A()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/l;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_20

    .line 390
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-static {v1, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_20

    .line 359
    nop

    :pswitch_data_134
    .packed-switch 0x1
        :pswitch_5b
        :pswitch_69
        :pswitch_77
        :pswitch_85
        :pswitch_ec
        :pswitch_10a
        :pswitch_10a
        :pswitch_10a
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V
    .registers 9
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 401
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-nez v2, :cond_e

    .line 402
    const-string v0, "RemoteControlHelper was not initialized with a video"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 439
    :cond_d
    :goto_d
    :pswitch_d
    return-void

    .line 406
    :cond_e
    sget-object v2, Lcom/google/android/youtube/app/ui/bj;->b:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_c6

    .line 425
    :goto_19
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/bg;->b(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 431
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v2, :cond_37

    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/bg;->B:Z

    if-nez v2, :cond_37

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v2, :cond_37

    .line 432
    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->B:Z

    .line 433
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->c:Lcom/google/android/youtube/core/b/an;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bg;->f:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 436
    :cond_37
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->p()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 437
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Video;->captionTracksUri:Landroid/net/Uri;

    if-eqz v3, :cond_c3

    :goto_4b
    invoke-interface {v2, v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasCc(Z)V

    goto :goto_d

    .line 410
    :pswitch_4f
    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    if-eqz v2, :cond_d

    .line 411
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/bg;->b(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 412
    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->i:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/app/ui/bt;->c(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->u:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_d

    .line 416
    :pswitch_6c
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->v()V

    .line 417
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->s:Lcom/google/android/youtube/core/ui/l;

    const v3, 0x7f0b0233

    invoke-virtual {v2, v3, v0}, Lcom/google/android/youtube/core/ui/l;->a(IZ)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v2}, Lcom/google/android/youtube/app/ui/bt;->p()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->x()V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->n:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_19

    .line 420
    :pswitch_8b
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/bg;->a(Ljava/lang/String;)V

    goto :goto_19

    .line 423
    :pswitch_95
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->v()V

    .line 424
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->x()Lcom/google/android/youtube/app/remote/al;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/RemoteControl;->u()Lcom/google/android/youtube/app/remote/ax;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/youtube/app/remote/ax;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    iget v5, v2, Lcom/google/android/youtube/app/remote/al;->b:I

    new-array v6, v0, [Ljava/lang/Object;

    aput-object v3, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v2, v2, Lcom/google/android/youtube/app/remote/al;->c:Z

    invoke-direct {p0, v3, v2}, Lcom/google/android/youtube/app/ui/bg;->a(Ljava/lang/String;Z)V

    goto/16 :goto_19

    :cond_c3
    move v0, v1

    .line 437
    goto :goto_4b

    .line 406
    nop

    :pswitch_data_c6
    .packed-switch 0x1
        :pswitch_d
        :pswitch_4f
        :pswitch_6c
        :pswitch_8b
        :pswitch_95
    .end packed-switch
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 3
    .parameter

    .prologue
    .line 265
    const-string v0, "remoteControl cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_14

    .line 268
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-ne v0, p1, :cond_11

    .line 269
    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 284
    :goto_10
    return-void

    .line 272
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/bg;->a()V

    .line 276
    :cond_14
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 277
    invoke-interface {p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->D:Z

    .line 278
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->E:Z

    if-eqz v0, :cond_26

    .line 279
    invoke-interface {p1, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/aj;)V

    .line 280
    invoke-interface {p1, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/aj;)V

    .line 283
    :cond_26
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->C()V

    goto :goto_10
.end method

.method public final a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V
    .registers 3
    .parameter

    .prologue
    .line 981
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/SubtitleTrack;)V

    .line 982
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 4
    .parameter

    .prologue
    .line 484
    const-string v0, "video can not be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_18

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 487
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->B:Z

    .line 489
    :cond_18
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    .line 491
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_21

    .line 492
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->C()V

    .line 494
    :cond_21
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x4

    const/4 v1, 0x0

    .line 442
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-nez v2, :cond_c

    .line 443
    const-string v0, "Video is null"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 469
    :goto_b
    return-void

    .line 446
    :cond_c
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v2

    if-nez v2, :cond_18

    .line 447
    const-string v0, "Video changed recieved for a non connected remote. Will ignore"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    goto :goto_b

    .line 450
    :cond_18
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->v()V

    .line 451
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_40

    .line 452
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v3, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    .line 453
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    .line 454
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 455
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b

    .line 456
    :cond_40
    iget-boolean v2, p0, Lcom/google/android/youtube/app/ui/bg;->z:Z

    if-eqz v2, :cond_51

    .line 457
    iput v1, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    .line 458
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->A:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/bg;->c(Ljava/lang/String;)V

    .line 459
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b

    .line 461
    :cond_51
    iput v1, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    .line 462
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->B()V

    .line 463
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v3, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->REMOTE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v2, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    .line 464
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    .line 465
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/ui/bg;->D:Z

    if-eqz v3, :cond_69

    move v0, v1

    :cond_69
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 466
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->k()Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7d

    const/4 v1, 0x1

    :cond_7d
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/ui/bg;->e(Z)V

    .line 467
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b
.end method

.method public final a(Ljava/util/List;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 332
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bg;->D:Z

    if-nez v1, :cond_a

    .line 343
    :cond_9
    :goto_9
    return-void

    .line 335
    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 336
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->r:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 340
    iget-boolean v1, p0, Lcom/google/android/youtube/app/ui/bg;->C:Z

    if-nez v1, :cond_9

    .line 341
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_25

    const/4 v0, 0x1

    :cond_25
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/bg;->e(Z)V

    goto :goto_9
.end method

.method public final a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 985
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setCcEnabled(Z)V

    .line 986
    return-void
.end method

.method public final b()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 296
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->E:Z

    if-nez v0, :cond_1f

    move v0, v1

    :goto_6
    const-string v2, "cannot call onResume() multiple times, need to call onPause() first"

    invoke-static {v0, v2}, Lcom/google/android/ytremote/util/b;->b(ZLjava/lang/Object;)V

    .line 298
    iput-boolean v1, p0, Lcom/google/android/youtube/app/ui/bg;->E:Z

    .line 299
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_1e

    .line 300
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/app/remote/aj;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c(Lcom/google/android/youtube/app/remote/aj;)V

    .line 302
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->C()V

    .line 304
    :cond_1e
    return-void

    .line 296
    :cond_1f
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 497
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->z:Z

    .line 498
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bg;->A:Ljava/lang/String;

    .line 500
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-nez v0, :cond_c

    .line 505
    :goto_b
    return-void

    .line 504
    :cond_c
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/bg;->c(Ljava/lang/String;)V

    goto :goto_b
.end method

.method public final b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/bg;->H:Z

    .line 262
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->E:Z

    const-string v1, "cannot call onPause() multiple times, need to call onResume() first"

    invoke-static {v0, v1}, Lcom/google/android/ytremote/util/b;->b(ZLjava/lang/Object;)V

    .line 309
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->E:Z

    .line 310
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_13

    .line 311
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/app/remote/aj;)V

    .line 313
    :cond_13
    return-void
.end method

.method public final c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 599
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/bg;->I:Z

    .line 600
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->w:Lcom/google/android/youtube/app/ui/bs;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/ui/bs;->b(Z)V

    .line 601
    return-void
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    :goto_8
    return-object v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 604
    iput-boolean p1, p0, Lcom/google/android/youtube/app/ui/bg;->I:Z

    .line 605
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->z()V

    .line 606
    return-void
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 328
    iget-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->F:Z

    return v0
.end method

.method public final f()V
    .registers 5

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->d:Lcom/google/android/youtube/core/b/as;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->b:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/bl;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/ui/bl;-><init>(Lcom/google/android/youtube/app/ui/bg;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/as;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 529
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_d

    .line 533
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bt;->o()V

    .line 535
    :cond_d
    return-void
.end method

.method public final h()V
    .registers 1

    .prologue
    .line 538
    return-void
.end method

.method public final i()V
    .registers 1

    .prologue
    .line 541
    return-void
.end method

.method public final j()V
    .registers 3

    .prologue
    .line 544
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-nez v0, :cond_7

    .line 550
    :goto_6
    return-void

    .line 547
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemotePause"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 548
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    .line 549
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    goto :goto_6
.end method

.method public final k()V
    .registers 4

    .prologue
    .line 553
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-nez v0, :cond_7

    .line 563
    :cond_6
    :goto_6
    return-void

    .line 556
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 557
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemotePlay"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    .line 559
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setPlaying()V

    goto :goto_6

    .line 561
    :cond_33
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->e:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemotePlayVideo"

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->categoryLabel:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/ui/bg;->C:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->t:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setLoading()V

    goto :goto_6
.end method

.method public final l()V
    .registers 4

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_2f

    .line 473
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-nez v0, :cond_2f

    .line 474
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->w()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bg;->y:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/youtube/app/remote/RemoteControl$UnavailableReason;->canRetry()Z

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/ui/bg;->a(Ljava/lang/String;Z)V

    .line 478
    :cond_2f
    return-void
.end method

.method public final m()V
    .registers 1

    .prologue
    .line 566
    return-void
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 569
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 570
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    .line 572
    :cond_b
    return-void
.end method

.method public final o()V
    .registers 1

    .prologue
    .line 575
    return-void
.end method

.method public final p()V
    .registers 2

    .prologue
    .line 588
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/bg;->y()Z

    move-result v0

    if-nez v0, :cond_7

    .line 592
    :goto_6
    return-void

    .line 591
    :cond_7
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    goto :goto_6
.end method

.method public final q()V
    .registers 2

    .prologue
    .line 595
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->v:Lcom/google/android/youtube/app/ui/bt;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bt;->p()V

    .line 596
    return-void
.end method

.method public final r()Landroid/view/View;
    .registers 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->i:Landroid/view/View;

    return-object v0
.end method

.method public final s()Z
    .registers 2

    .prologue
    .line 779
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->g:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final t()I
    .registers 2

    .prologue
    .line 989
    iget v0, p0, Lcom/google/android/youtube/app/ui/bg;->K:I

    return v0
.end method

.method public final u()Landroid/view/View;
    .registers 2

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bg;->l:Landroid/widget/LinearLayout;

    return-object v0
.end method
