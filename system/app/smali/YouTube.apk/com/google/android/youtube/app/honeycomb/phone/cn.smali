.class public final Lcom/google/android/youtube/app/honeycomb/phone/cn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/ui/i;


# instance fields
.field private a:Lcom/google/android/youtube/app/compat/t;

.field private b:Lcom/google/android/youtube/app/compat/t;

.field private c:Lcom/google/android/youtube/app/compat/t;

.field private d:Lcom/google/android/youtube/app/compat/t;

.field private e:Lcom/google/android/youtube/app/compat/t;

.field private f:Lcom/google/android/youtube/app/compat/t;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/google/android/youtube/core/model/VastAd;

.field private l:Lcom/google/android/youtube/core/model/Video;

.field private final m:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

.field private final n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;)V
    .registers 3
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "actionBarMenuHelper can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->m:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    .line 45
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    .line 46
    return-void
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->g:Z

    if-nez v0, :cond_7

    .line 113
    :goto_6
    return-void

    .line 102
    :cond_7
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->f()Z

    move-result v3

    .line 103
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->e:Lcom/google/android/youtube/app/compat/t;

    if-eqz v3, :cond_5c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->k:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v0, :cond_5c

    move v0, v1

    :goto_16
    invoke-interface {v4, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 104
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->f:Lcom/google/android/youtube/app/compat/t;

    if-eqz v3, :cond_5e

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->k:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5e

    move v0, v1

    :goto_28
    invoke-interface {v4, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->l:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_60

    move v0, v1

    :goto_30
    if-nez v0, :cond_36

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->j:Z

    if-eqz v0, :cond_62

    :cond_36
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->i:Z

    if-nez v0, :cond_62

    move v0, v1

    .line 107
    :goto_3b
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->c:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v3, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 108
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_64

    move v3, v1

    :goto_45
    invoke-interface {v4, v3}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 110
    if-eqz v0, :cond_66

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->h:Z

    if-eqz v0, :cond_66

    move v0, v1

    .line 111
    :goto_4f
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v3, v0}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    .line 112
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_68

    :goto_58
    invoke-interface {v3, v1}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    goto :goto_6

    :cond_5c
    move v0, v2

    .line 103
    goto :goto_16

    :cond_5e
    move v0, v2

    .line 104
    goto :goto_28

    :cond_60
    move v0, v2

    .line 106
    goto :goto_30

    :cond_62
    move v0, v2

    goto :goto_3b

    :cond_64
    move v3, v2

    .line 108
    goto :goto_45

    :cond_66
    move v0, v2

    .line 110
    goto :goto_4f

    :cond_68
    move v1, v2

    .line 112
    goto :goto_58
.end method

.method private e()V
    .registers 3

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->f()Z

    move-result v0

    .line 117
    if-eqz v0, :cond_e

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->m:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    sget-object v1, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    .line 122
    :goto_d
    return-void

    .line 120
    :cond_e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->m:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->n:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    goto :goto_d
.end method

.method private f()Z
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->k:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->j:Z

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 3
    .parameter

    .prologue
    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->g:Z

    .line 50
    const v0, 0x7f0801a6

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a:Lcom/google/android/youtube/app/compat/t;

    .line 51
    const v0, 0x7f0801b1

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b:Lcom/google/android/youtube/app/compat/t;

    .line 52
    const v0, 0x7f0801a7

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->c:Lcom/google/android/youtube/app/compat/t;

    .line 53
    const v0, 0x7f0801b2

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d:Lcom/google/android/youtube/app/compat/t;

    .line 54
    const v0, 0x7f0801af

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->e:Lcom/google/android/youtube/app/compat/t;

    .line 55
    const v0, 0x7f0801b0

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->f:Lcom/google/android/youtube/app/compat/t;

    .line 56
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 3
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->k:Lcom/google/android/youtube/core/model/VastAd;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->l:Lcom/google/android/youtube/core/model/Video;

    .line 80
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 81
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->e()V

    .line 82
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 3
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->l:Lcom/google/android/youtube/core/model/Video;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->k:Lcom/google/android/youtube/core/model/VastAd;

    .line 87
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 88
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->e()V

    .line 89
    return-void
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->h:Z

    .line 64
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 65
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->i:Z

    .line 74
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 75
    return-void
.end method

.method public final b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->j:Z

    .line 93
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 94
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->e()V

    .line 95
    return-void
.end method

.method public final c()V
    .registers 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 60
    return-void
.end method

.method public final h_()V
    .registers 2

    .prologue
    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cn;->i:Z

    .line 69
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->d()V

    .line 70
    return-void
.end method
