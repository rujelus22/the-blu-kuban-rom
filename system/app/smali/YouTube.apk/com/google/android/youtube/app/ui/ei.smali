.class final Lcom/google/android/youtube/app/ui/ei;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/eg;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/ui/eg;)V
    .registers 2
    .parameter

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/ui/eg;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 231
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/ei;-><init>(Lcom/google/android/youtube/app/ui/eg;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 231
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->e(Lcom/google/android/youtube/app/ui/eg;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00da

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/eg;->d(Lcom/google/android/youtube/app/ui/eg;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/eg;->c(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/ui/b;

    move-result-object v1

    invoke-virtual {v1, v0, v5}, Lcom/google/android/youtube/app/ui/b;->b(Ljava/lang/String;Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 231
    check-cast p2, Ljava/util/List;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/BatchEntry;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/BatchEntry;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_b

    new-instance v3, Landroid/util/Pair;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v4}, Lcom/google/android/youtube/app/ui/eg;->a(Lcom/google/android/youtube/app/ui/eg;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_32
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->b(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bt;->b(Ljava/lang/Iterable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ei;->a:Lcom/google/android/youtube/app/ui/eg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/eg;->c(Lcom/google/android/youtube/app/ui/eg;)Lcom/google/android/youtube/app/ui/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->d()V

    return-void
.end method
