.class public final Lcom/google/android/youtube/app/compat/a;
.super Lcom/google/android/youtube/app/compat/SupportActionBar;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/ActionBar;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/app/ActionBar;)V
    .registers 3
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;-><init>()V

    .line 26
    const-string v0, "actionBar may not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    .line 28
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/a;->b:Ljava/util/List;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 131
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 81
    return-void
.end method

.method public final a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0, p1, p2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 116
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 126
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 61
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    iget v2, p2, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->width:I

    iget v3, p2, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->height:I

    iget v4, p2, Lcom/google/android/youtube/app/compat/SupportActionBar$LayoutParams;->a:I

    invoke-direct {v1, v2, v3, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p1, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 56
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/h;)V
    .registers 4
    .parameter

    .prologue
    .line 33
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v0, Lcom/google/android/youtube/app/compat/b;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/app/compat/b;-><init>(Lcom/google/android/youtube/app/compat/h;)V

    .line 35
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 36
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method

.method public final a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 76
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 136
    return-void
.end method

.method public final b(I)V
    .registers 4
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 121
    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/h;)V
    .registers 5
    .parameter

    .prologue
    .line 40
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/b;

    .line 42
    iget-object v2, v0, Lcom/google/android/youtube/app/compat/b;->a:Lcom/google/android/youtube/app/compat/h;

    if-ne v2, p1, :cond_b

    .line 43
    iget-object v2, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->removeOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    goto :goto_b

    .line 46
    :cond_21
    return-void
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/a;->a:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    return v0
.end method
