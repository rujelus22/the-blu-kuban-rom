.class public final Lcom/google/android/youtube/app/adapter/as;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/res/Resources;

.field private final c:Lcom/google/android/youtube/app/adapter/au;

.field private final d:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 47
    const-string v0, "context can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->a:Landroid/content/Context;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->b:Landroid/content/res/Resources;

    .line 49
    const-string v0, "gdataClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v0, "imageClient can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    new-instance v0, Lcom/google/android/youtube/app/adapter/au;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/au;-><init>(Lcom/google/android/youtube/app/adapter/as;Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->c:Lcom/google/android/youtube/app/adapter/au;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->d:Ljava/util/Map;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/as;)Lcom/google/android/youtube/app/adapter/au;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->c:Lcom/google/android/youtube/app/adapter/au;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/as;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/as;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/as;->d:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/youtube/app/adapter/av;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/av;-><init>(Lcom/google/android/youtube/app/adapter/as;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method
