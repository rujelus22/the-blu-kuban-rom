.class public final Lcom/google/android/youtube/core/model/proto/o;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/p;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Lcom/google/protobuf/ab;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 4847
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 4948
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->b:Ljava/lang/Object;

    .line 5022
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    .line 4848
    return-void
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/o;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 4932
    const/4 v2, 0x0

    .line 4934
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 4939
    if-eqz v0, :cond_e

    .line 4940
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;

    .line 4943
    :cond_e
    return-object p0

    .line 4935
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 4936
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 4937
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 4939
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 4940
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;

    :cond_21
    throw v0

    .line 4939
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method static synthetic g()Lcom/google/android/youtube/core/model/proto/o;
    .registers 1

    .prologue
    .line 4842
    new-instance v0, Lcom/google/android/youtube/core/model/proto/o;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/o;-><init>()V

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/o;
    .registers 3

    .prologue
    .line 4867
    new-instance v0, Lcom/google/android/youtube/core/model/proto/o;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/o;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method private i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 4883
    new-instance v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 4884
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    .line 4885
    const/4 v1, 0x0

    .line 4886
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_32

    .line 4889
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/o;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4890
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_29

    .line 4891
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    invoke-direct {v1, v3}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    .line 4893
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    .line 4895
    :cond_29
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4502(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;

    .line 4896
    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4602(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;I)I

    .line 4897
    return-object v2

    :cond_32
    move v0, v1

    goto :goto_e
.end method

.method private j()V
    .registers 3

    .prologue
    .line 5024
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_16

    .line 5025
    new-instance v0, Lcom/google/protobuf/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/aa;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    .line 5026
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    .line 5028
    :cond_16
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;
    .registers 3

    .prologue
    .line 4875
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    .line 4876
    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    .line 4877
    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 4879
    :cond_f
    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/android/youtube/core/model/proto/o;
    .registers 4
    .parameter

    .prologue
    .line 4901
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 4917
    :cond_6
    :goto_6
    return-object p0

    .line 4902
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 4903
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    .line 4904
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->countryCode_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4400(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->b:Ljava/lang/Object;

    .line 4907
    :cond_19
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 4908
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 4909
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/protobuf/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    .line 4910
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    goto :goto_6

    .line 4912
    :cond_38
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->j()V

    .line 4913
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->videoId_:Lcom/google/protobuf/ab;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->access$4500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;)Lcom/google/protobuf/ab;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/ab;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/android/youtube/core/model/proto/o;
    .registers 3
    .parameter

    .prologue
    .line 5086
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->j()V

    .line 5087
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/o;->c:Lcom/google/protobuf/ab;

    invoke-static {p1, v0}, Lcom/google/protobuf/o;->a(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 5089
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/proto/o;
    .registers 3
    .parameter

    .prologue
    .line 4990
    if-nez p1, :cond_8

    .line 4991
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4993
    :cond_8
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    .line 4994
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/o;->b:Ljava/lang/Object;

    .line 4996
    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 4842
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4842
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4842
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/o;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 4842
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->h()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4842
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->h()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 4842
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->h()Lcom/google/android/youtube/core/model/proto/o;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4842
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/o;->i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4842
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/o;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4842
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetMusicVideoDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 4921
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/o;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 4925
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 4921
    goto :goto_9

    :cond_e
    move v0, v1

    .line 4925
    goto :goto_b
.end method
