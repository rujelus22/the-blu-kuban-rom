.class public abstract Lcom/google/android/youtube/core/async/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;


# instance fields
.field protected final b:Landroid/accounts/AccountManager;

.field public final c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

.field protected final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 39
    const-string v0, "youtube"

    invoke-static {v0}, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->createClientLogin(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/async/a;->a:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    return-void
.end method

.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a;->b:Landroid/accounts/AccountManager;

    .line 59
    sget-object v0, Lcom/google/android/youtube/core/async/a;->a:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a;->d:Ljava/lang/String;

    .line 61
    return-void
.end method

.method protected constructor <init>(Landroid/accounts/AccountManager;Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "accountManager cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a;->b:Landroid/accounts/AccountManager;

    .line 50
    const-string v0, "authMethod cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    .line 51
    const-string v0, "accountType cannot be empty"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a;->d:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/accounts/Account;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, v1

    .line 85
    :goto_8
    return-object v0

    .line 79
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/youtube/core/async/a;->b()[Landroid/accounts/Account;

    move-result-object v2

    .line 80
    const/4 v0, 0x0

    :goto_e
    array-length v3, v2

    if-ge v0, v3, :cond_21

    .line 81
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 82
    aget-object v0, v2, v0

    goto :goto_8

    .line 80
    :cond_1e
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_21
    move-object v0, v1

    .line 85
    goto :goto_8
.end method

.method protected abstract a(Landroid/accounts/Account;)Lcom/google/android/youtube/core/model/UserAuth;
.end method

.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 135
    const-string v0, "account cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    const-string v0, "activity cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    const-string v0, "callback cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/youtube/core/async/a;->b(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 139
    return-void
.end method

.method protected abstract a(Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bn;)V
.end method

.method public final a(Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a;->c:Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/UserAuth$AuthMethod;->scope:Ljava/lang/String;

    move-object v4, v3

    move-object v5, p1

    move-object v6, p2

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 98
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/UserAuth;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/youtube/core/async/bn;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 118
    const-string v0, "callback cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/async/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 120
    if-nez v0, :cond_f

    .line 121
    invoke-interface {p2}, Lcom/google/android/youtube/core/async/bn;->i_()V

    .line 125
    :goto_e
    return-void

    .line 123
    :cond_f
    const-string v1, "account cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "callback cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/youtube/core/async/a;->a(Landroid/accounts/Account;Lcom/google/android/youtube/core/async/bn;)V

    goto :goto_e
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/UserAuth;
    .registers 3
    .parameter

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/async/a;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/async/a;->a(Landroid/accounts/Account;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    goto :goto_7
.end method

.method protected abstract b(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V
.end method

.method public final b()[Landroid/accounts/Account;
    .registers 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a;->b:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method
