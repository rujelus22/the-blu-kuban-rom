.class public Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/compat/h;
.implements Lcom/google/android/youtube/app/honeycomb/ui/i;
.implements Lcom/google/android/youtube/app/remote/af;
.implements Lcom/google/android/youtube/app/ui/bs;
.implements Lcom/google/android/youtube/app/ui/bt;
.implements Lcom/google/android/youtube/app/ui/fd;
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/player/aa;
.implements Lcom/google/android/youtube/core/utils/h;
.implements Lcom/google/android/youtube/core/utils/i;
.implements Lcom/google/android/youtube/core/utils/k;
.implements Lcom/google/android/youtube/coreicecream/ui/h;


# instance fields
.field private A:Lcom/google/android/youtube/core/utils/DockReceiver;

.field private B:Z

.field private C:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

.field private D:Landroid/content/SharedPreferences;

.field private E:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private F:Lcom/google/android/youtube/core/player/Director;

.field private G:Lcom/google/android/youtube/core/async/a/c;

.field private H:Z

.field private I:I

.field private J:Lcom/google/android/youtube/app/honeycomb/phone/ce;

.field private K:Lcom/google/android/youtube/app/honeycomb/phone/cg;

.field private L:Lcom/google/android/youtube/coreicecream/ui/g;

.field private M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

.field private N:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private O:Lcom/google/android/youtube/core/Analytics;

.field private P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

.field private Q:Lcom/google/android/youtube/core/player/a;

.field private R:Z

.field private S:Z

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:Lcom/google/android/youtube/core/b/al;

.field private ab:Lcom/google/android/youtube/core/b/au;

.field private ac:Lcom/google/android/youtube/core/b/ap;

.field private ad:Lcom/google/android/youtube/app/remote/ab;

.field private ae:Lcom/google/android/youtube/app/remote/bb;

.field private af:Lcom/google/android/youtube/app/ui/bg;

.field private ag:Lcom/google/android/youtube/app/YouTubeApplication;

.field private ah:Lcom/google/android/youtube/app/k;

.field private ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

.field private aj:Lcom/google/android/youtube/app/remote/bm;

.field private ak:Landroid/app/KeyguardManager;

.field private al:Z

.field private m:Landroid/media/AudioManager;

.field private n:Lcom/google/android/youtube/app/ui/fa;

.field private o:Landroid/view/ViewGroup;

.field private p:Landroid/widget/ProgressBar;

.field private q:Landroid/widget/ImageView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/Button;

.field private t:Lcom/google/android/youtube/app/ui/di;

.field private u:Lcom/google/android/youtube/core/d;

.field private v:Lcom/google/android/youtube/app/a;

.field private w:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private x:Lcom/google/android/youtube/core/player/bx;

.field private y:Lcom/google/android/youtube/core/player/PlayerView;

.field private z:Lcom/google/android/youtube/core/utils/HdmiReceiver;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    .line 199
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->T:Ljava/lang/String;

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    .line 1312
    return-void
.end method

.method private D()V
    .registers 3

    .prologue
    .line 952
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_2c

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->R:Z

    if-nez v0, :cond_2c

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Y:Z

    if-nez v0, :cond_2c

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Z:Z

    if-nez v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->e()Z

    move-result v0

    if-nez v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->i()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 955
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 957
    :cond_2c
    return-void
.end method

.method private E()V
    .registers 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1174
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->z:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->c()Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->A:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->c()I

    move-result v0

    if-eqz v0, :cond_35

    :cond_12
    move v0, v2

    .line 1176
    :goto_13
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    if-nez v0, :cond_37

    iget-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    if-nez v4, :cond_37

    :goto_1b
    invoke-virtual {v3, v2}, Lcom/google/android/youtube/core/player/Director;->c(Z)V

    .line 1178
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setLockedInFullscreen(Z)V

    .line 1179
    if-eqz v0, :cond_39

    .line 1180
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_34

    .line 1181
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    .line 1182
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    .line 1189
    :cond_34
    :goto_34
    return-void

    :cond_35
    move v0, v1

    .line 1174
    goto :goto_13

    :cond_37
    move v2, v1

    .line 1176
    goto :goto_1b

    .line 1184
    :cond_39
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    if-eqz v0, :cond_34

    .line 1185
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_34

    .line 1186
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    goto :goto_34
.end method

.method private F()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1351
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1352
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->p:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1353
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1354
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1355
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->s:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1356
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/fa;->c(Z)V

    .line 1357
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;IZLcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 286
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "playlist_uri"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "playlist_start_position"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "referrer"

    invoke-static {p4}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 262
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "unfavorite_uri"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "referrer"

    invoke-static {p3}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 237
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "referrer"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 253
    invoke-static {p0, p1, p3}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/app/remote/ab;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ad:Lcom/google/android/youtube/app/remote/ab;

    return-object v0
.end method

.method private a(Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 641
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_15

    .line 642
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    .line 644
    :goto_14
    return-object v0

    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    invoke-static {v0, p1, p2, v2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    goto :goto_14
.end method

.method private a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 531
    const-string v0, "authenticate"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 532
    if-eqz v0, :cond_f

    .line 533
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 537
    :goto_e
    return-void

    .line 535
    :cond_f
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_e
.end method

.method private a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const v2, 0x7f0b0018

    .line 540
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    if-ne v0, v1, :cond_44

    .line 541
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 543
    const-string v0, "empty search query"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 544
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/d;->a(I)V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    .line 574
    :goto_24
    return-void

    .line 548
    :cond_25
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->c()Lcom/google/android/youtube/core/async/av;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/cf;

    invoke-direct {v2, p0, p2}, Lcom/google/android/youtube/app/honeycomb/phone/cf;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_24

    .line 555
    :cond_44
    const-string v0, "artist_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 556
    if-eqz v0, :cond_6f

    .line 557
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_60

    .line 558
    const-string v0, "empty artistId"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/d;->a(I)V

    .line 560
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    goto :goto_24

    .line 563
    :cond_60
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ac:Lcom/google/android/youtube/core/b/ap;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/cd;

    invoke-direct {v2, p0, p2}, Lcom/google/android/youtube/app/honeycomb/phone/cd;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/b/ap;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_24

    .line 568
    :cond_6f
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "playlist_uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const-string v1, "playlist_start_position"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const/4 v3, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_97

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_97
    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    goto :goto_24
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 134
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x7f0b0018

    .line 601
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 603
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->G:Lcom/google/android/youtube/core/async/a/c;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->G:Lcom/google/android/youtube/core/async/a/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/c;->g()I

    move-result p5

    .line 606
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->f()V

    .line 608
    if-eqz p3, :cond_2c

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 609
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-direct {p0, p3, p1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->H:Z

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->I:I

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;ZI)V

    .line 636
    :cond_2b
    :goto_2b
    return-void

    .line 610
    :cond_2c
    if-eqz p4, :cond_42

    .line 611
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    invoke-static {v0, p4, p1, p5}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/b/al;Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->G:Lcom/google/android/youtube/core/async/a/c;

    .line 613
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->G:Lcom/google/android/youtube/core/async/a/c;

    iget-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->H:Z

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->I:I

    invoke-virtual {v0, v1, p1, v2, v3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;ZI)V

    goto :goto_2b

    .line 614
    :cond_42
    if-eqz p2, :cond_8e

    .line 615
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;

    move-result-object v2

    .line 616
    if-eqz v2, :cond_80

    .line 617
    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->I:I

    if-lez v0, :cond_7d

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->I:I

    .line 618
    :goto_50
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    iget-object v4, v2, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-direct {p0, v4, p1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->H:Z

    iget-object v6, v2, Lcom/google/android/youtube/core/utils/ag;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0, v6}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZILjava/lang/String;)V

    .line 621
    const-string v0, "youtube_tv_uid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_2b

    .line 623
    iget-object v1, v2, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/ytremote/model/SsdpId;

    invoke-direct {v1, v0}, Lcom/google/android/ytremote/model/SsdpId;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aj:Lcom/google/android/youtube/app/remote/bm;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/ca;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ca;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/remote/bm;->a(Lcom/google/android/ytremote/model/SsdpId;Lcom/google/android/youtube/app/remote/bt;)V

    goto :goto_2b

    .line 617
    :cond_7d
    iget v0, v2, Lcom/google/android/youtube/core/utils/ag;->c:I

    goto :goto_50

    .line 626
    :cond_80
    const-string v0, "invalid intercepted URI"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 627
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/d;->a(I)V

    .line 628
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    goto :goto_2b

    .line 631
    :cond_8e
    const-string v0, "invalid intent format"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/d;->a(I)V

    .line 633
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    goto :goto_2b
.end method

.method private a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 782
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    .line 783
    if-eqz p1, :cond_11

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_e
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->X:Z

    .line 784
    return-void

    .line 783
    :cond_11
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 279
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "artist_id"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "referrer"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/app/remote/bb;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ae:Lcom/google/android/youtube/app/remote/bb;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1360
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1361
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->p:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1362
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->q:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1363
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1364
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1365
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->s:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 1366
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/ui/fa;->c(Z)V

    .line 1367
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/app/ui/bg;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/app/honeycomb/phone/cn;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    return-object v0
.end method

.method private d(Z)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1053
    const v0, 0x7f08013d

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1054
    if-eqz p1, :cond_41

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v3, 0xb

    if-lt v0, v3, :cond_2f

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const v4, 0x10102eb

    aput v4, v3, v1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    :goto_2b
    invoke-virtual {v2, v1, v0, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1055
    return-void

    .line 1054
    :cond_2f
    const v0, 0x7f0c0032

    sget-object v3, Lcom/google/android/youtube/b;->j:[I

    invoke-virtual {p0, v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_2b

    :cond_41
    move v0, v1

    goto :goto_2b
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/player/bx;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x:Lcom/google/android/youtube/core/player/bx;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay;
    .registers 2
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 295
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 296
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 297
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 4
    .parameter

    .prologue
    .line 1251
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 1252
    if-nez p1, :cond_35

    .line 1253
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->s()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 1254
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->al:Z

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ak:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_29

    .line 1255
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bg;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    .line 1256
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    .line 1258
    :cond_29
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->a()V

    .line 1259
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b(Z)V

    .line 1265
    :cond_34
    :goto_34
    return-void

    .line 1262
    :cond_35
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 1263
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b(Z)V

    goto :goto_34
.end method

.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .registers 3
    .parameter

    .prologue
    .line 986
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/core/model/Branding;)V

    .line 987
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter

    .prologue
    .line 676
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    .line 677
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 4
    .parameter

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(Lcom/google/android/youtube/core/model/VastAd;)V

    .line 994
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0250

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    .line 995
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-eqz v0, :cond_1b

    .line 996
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Ljava/lang/String;)V

    .line 998
    :cond_1b
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_27

    .line 999
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 1001
    :cond_27
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter

    .prologue
    .line 790
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->T:Ljava/lang/String;

    .line 791
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    .line 792
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-eqz v0, :cond_11

    .line 793
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Ljava/lang/String;)V

    .line 795
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "unfavorite_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    .line 796
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 797
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 798
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->A()Lcom/google/android/youtube/app/honeycomb/ui/p;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 799
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->A()Lcom/google/android/youtube/app/honeycomb/ui/p;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->b(Ljava/lang/String;)V

    .line 802
    :cond_39
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    .line 803
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->B:Z

    .line 804
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/ui/dm;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    invoke-direct {v1, p1, p0, v2, v3}, Lcom/google/android/youtube/app/ui/dm;-><init>(Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 805
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/Director$StopReason;)V
    .registers 4
    .parameter

    .prologue
    .line 1013
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_1a

    .line 1014
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    if-nez v0, :cond_e

    .line 1015
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 1021
    :cond_e
    :goto_e
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->W:Z

    if-eqz v0, :cond_19

    .line 1022
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    .line 1024
    :cond_19
    return-void

    .line 1017
    :cond_1a
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->AUTOPLAY_DENIED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_e

    .line 1018
    const v0, 0x7f0b0068

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    goto :goto_e
.end method

.method public final a(Lcom/google/android/youtube/core/player/DirectorException;)V
    .registers 4
    .parameter

    .prologue
    .line 812
    iget-object v0, p1, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    sget-object v1, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    if-ne v0, v1, :cond_16

    .line 813
    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->b(Ljava/lang/String;)V

    .line 814
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bg;->b(Ljava/lang/String;)V

    .line 816
    :cond_16
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 706
    const-string v0, "error authenticating for playlist request"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 707
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    .line 708
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 712
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 713
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v1

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_1a

    const v0, 0x7f110011

    :goto_10
    invoke-virtual {v1, v0, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 715
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 716
    const/4 v0, 0x1

    return v0

    .line 713
    :cond_1a
    const v0, 0x7f110005

    goto :goto_10
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 728
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->X:Z

    if-nez v1, :cond_a

    .line 729
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    .line 764
    :goto_9
    return v0

    .line 731
    :cond_a
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v1

    packed-switch v1, :pswitch_data_5e

    .line 764
    :pswitch_11
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_9

    .line 733
    :pswitch_16
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->a()V

    goto :goto_9

    .line 736
    :pswitch_1c
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->b()V

    goto :goto_9

    .line 740
    :pswitch_22
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->c()V

    goto :goto_9

    .line 744
    :pswitch_28
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->f()V

    goto :goto_9

    .line 747
    :pswitch_2e
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->d()V

    goto :goto_9

    .line 750
    :pswitch_34
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->e()V

    goto :goto_9

    .line 753
    :pswitch_3a
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/di;->g()V

    goto :goto_9

    .line 756
    :pswitch_40
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "LearnMore"

    const-string v3, "Menu"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/Director;->a()V

    goto :goto_9

    .line 760
    :pswitch_4f
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "GotoAd"

    const-string v3, "Menu"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/Director;->d()V

    goto :goto_9

    .line 731
    :pswitch_data_5e
    .packed-switch 0x7f08019f
        :pswitch_3a
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_28
        :pswitch_22
        :pswitch_11
        :pswitch_34
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_40
        :pswitch_4f
        :pswitch_28
        :pswitch_22
        :pswitch_16
        :pswitch_1c
        :pswitch_2e
    .end packed-switch
.end method

.method public final a_(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1144
    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Y:Z

    .line 1145
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D()V

    .line 1146
    return-void
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 1034
    sparse-switch p1, :sswitch_data_14

    .line 1044
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 1039
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/di;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 1041
    :sswitch_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/fa;->m_()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_4

    .line 1034
    nop

    :sswitch_data_14
    .sparse-switch
        0x3e8 -> :sswitch_c
        0x3ed -> :sswitch_5
        0x3f4 -> :sswitch_5
        0x3f7 -> :sswitch_5
        0x3ff -> :sswitch_5
    .end sparse-switch
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 1154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Z:Z

    .line 1155
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b()V

    .line 1156
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D()V

    .line 1157
    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)V
    .registers 4
    .parameter

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 1005
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "unfavorite_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    .line 1006
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    .line 1007
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-eqz v0, :cond_21

    .line 1008
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Ljava/lang/String;)V

    .line 1010
    :cond_21
    return-void
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 960
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_20

    .line 961
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    if-nez v0, :cond_1d

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_1d

    if-eqz p1, :cond_1e

    const/4 v0, 0x6

    :goto_15
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->enable()V

    .line 965
    :cond_1d
    :goto_1d
    return-void

    .line 961
    :cond_1e
    const/4 v0, 0x7

    goto :goto_15

    .line 963
    :cond_20
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-eq v0, p1, :cond_2a

    if-eqz p1, :cond_30

    const/4 v0, 0x0

    :goto_27
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    :cond_2a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->enable()V

    goto :goto_1d

    :cond_30
    const/4 v0, 0x1

    goto :goto_27
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 3
    .parameter

    .prologue
    .line 721
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/youtube/app/compat/m;)Z

    .line 722
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->c()V

    .line 723
    const/4 v0, 0x1

    return v0
.end method

.method public final c(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1197
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-nez v0, :cond_17

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_c
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->d(Z)V

    .line 1198
    if-eqz p1, :cond_19

    .line 1199
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 1203
    :goto_16
    return-void

    :cond_17
    move v0, v1

    .line 1197
    goto :goto_c

    .line 1201
    :cond_19
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D()V

    goto :goto_16
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1193
    const-string v0, "yt_watch"

    return-object v0
.end method

.method public final h_()V
    .registers 2

    .prologue
    .line 1149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Z:Z

    .line 1150
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->h_()V

    .line 1151
    return-void
.end method

.method public final i()V
    .registers 3

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/fa;->c(Z)V

    .line 650
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 702
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    .line 703
    return-void
.end method

.method public final j()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 772
    invoke-direct {p0, v0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    .line 773
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F()V

    .line 774
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/fa;->b()V

    .line 775
    return-void
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 936
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->R:Z

    .line 937
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->B:Z

    if-eqz v0, :cond_c

    .line 938
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->a()V

    .line 940
    :cond_c
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D()V

    .line 941
    return-void
.end method

.method public final l()V
    .registers 3

    .prologue
    .line 944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->R:Z

    .line 945
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    .line 946
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 947
    :cond_12
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 949
    :cond_18
    return-void
.end method

.method public final m()V
    .registers 1

    .prologue
    .line 1160
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E()V

    .line 1161
    return-void
.end method

.method public final n()V
    .registers 2

    .prologue
    .line 1164
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E()V

    .line 1167
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->c()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1168
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a()V

    .line 1169
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b()V

    .line 1171
    :cond_15
    return-void
.end method

.method public final o()V
    .registers 1

    .prologue
    .line 1218
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 829
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    if-eqz v0, :cond_8

    .line 830
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    .line 834
    :goto_7
    return-void

    .line 832
    :cond_8
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onBackPressed()V

    goto :goto_7
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->g()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 820
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Z)V

    .line 825
    :goto_e
    return-void

    .line 822
    :cond_f
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F()V

    .line 823
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Intent;)V

    goto :goto_e
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 8
    .parameter

    .prologue
    const v5, 0x7f09000b

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 892
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 893
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    .line 894
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 895
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    .line 896
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    .line 898
    :cond_1c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 899
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-nez v0, :cond_81

    move v0, v1

    :goto_2a
    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/ui/fa;->c(Z)V

    .line 900
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-nez v0, :cond_83

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_83

    :goto_37
    invoke-direct {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->d(Z)V

    .line 901
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 902
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_4d

    .line 903
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    invoke-interface {v0, v3}, Lcom/google/android/youtube/coreicecream/ui/g;->a(Z)V

    .line 905
    :cond_4d
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-eqz v0, :cond_85

    .line 906
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->U:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Ljava/lang/String;)V

    .line 907
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 909
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D()V

    .line 910
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 911
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 929
    :cond_6f
    :goto_6f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bg;->a(Landroid/content/res/Configuration;)V

    .line 930
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(Z)V

    .line 932
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->closeOptionsMenu()V

    .line 933
    return-void

    :cond_7f
    move v0, v2

    .line 893
    goto :goto_f

    :cond_81
    move v0, v2

    .line 899
    goto :goto_2a

    :cond_83
    move v1, v2

    .line 900
    goto :goto_37

    .line 913
    :cond_85
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_90

    .line 914
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 916
    :cond_90
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x400

    invoke-virtual {v0, v2, v3}, Landroid/view/Window;->setFlags(II)V

    .line 917
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u()V

    .line 918
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_d5

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    :goto_b1
    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 922
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    .line 923
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 924
    int-to-float v0, v0

    const v2, 0x3fe374bc

    div-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 925
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 926
    const/16 v0, 0x1e0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_6f

    .line 918
    :cond_d5
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f02001b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_b1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 38
    .parameter

    .prologue
    .line 301
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C()V

    .line 303
    invoke-super/range {p0 .. p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "force_fullscreen"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    .line 305
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v2

    if-eqz v2, :cond_48a

    const v2, 0x7f0400a2

    :goto_1e
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setContentView(I)V

    .line 306
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v2

    if-nez v2, :cond_2f

    .line 307
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->d(Z)V

    .line 310
    :cond_2f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ah:Lcom/google/android/youtube/app/k;

    .line 312
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    .line 313
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D:Landroid/content/SharedPreferences;

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->u:Lcom/google/android/youtube/core/d;

    .line 315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v20

    .line 317
    const v2, 0x7f08013e

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    .line 318
    new-instance v2, Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v3}, Lcom/google/android/youtube/core/player/PlayerView;->a()Lcom/google/android/youtube/core/player/au;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/player/j;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v6}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/youtube/app/player/j;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/l;)V

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/youtube/core/player/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/au;Lcom/google/android/youtube/core/player/cc;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x:Lcom/google/android/youtube/core/player/bx;

    .line 320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ah:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->i()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/bx;->a(Z)V

    .line 322
    new-instance v2, Lcom/google/android/youtube/core/utils/HdmiReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/k;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->z:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    .line 323
    new-instance v2, Lcom/google/android/youtube/core/utils/DockReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/DockReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/i;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->A:Lcom/google/android/youtube/core/utils/DockReceiver;

    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    .line 327
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    .line 328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v6

    .line 329
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/b/au;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ab:Lcom/google/android/youtube/core/b/au;

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->k()Lcom/google/android/youtube/core/b/a;

    move-result-object v7

    .line 331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->m()Lcom/google/android/youtube/core/b/as;

    move-result-object v9

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ac:Lcom/google/android/youtube/core/b/ap;

    .line 333
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->o()Lcom/google/android/youtube/app/b/g;

    move-result-object v25

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v19

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->v:Lcom/google/android/youtube/app/a;

    .line 336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->q()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ae:Lcom/google/android/youtube/app/remote/bb;

    .line 338
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->C:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;)V

    .line 341
    new-instance v10, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v2, v3, v4}, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;)V

    .line 344
    new-instance v2, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setShowFullscreenInPortrait(Z)V

    .line 347
    new-instance v2, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/youtube/app/ui/YouTubeAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Q:Lcom/google/android/youtube/core/player/a;

    .line 349
    new-instance v15, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;-><init>(Landroid/content/Context;)V

    .line 350
    new-instance v17, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;-><init>(Landroid/content/Context;)V

    .line 351
    new-instance v18, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;-><init>(Landroid/content/Context;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/google/android/youtube/core/player/as;

    const/4 v4, 0x0

    aput-object v15, v3, v4

    const/4 v4, 0x1

    aput-object v18, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Q:Lcom/google/android/youtube/core/player/a;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object v17, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->a([Lcom/google/android/youtube/core/player/as;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->D:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ab:Lcom/google/android/youtube/core/b/au;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->c()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->Q:Lcom/google/android/youtube/core/player/a;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Lcom/google/android/youtube/core/player/ax;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->T()Lcom/google/android/youtube/core/player/e;

    move-result-object v22

    move-object/from16 v3, p0

    move-object/from16 v13, p0

    invoke-static/range {v2 .. v22}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    .line 379
    new-instance v15, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;)V

    .line 380
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setAutoHide(Z)V

    .line 381
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    invoke-virtual {v15, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setShowFullscreenInPortrait(Z)V

    .line 383
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->setHideOnTap(Z)V

    .line 384
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v11, p0

    move-object v12, v6

    move-object v13, v9

    move-object/from16 v16, p0

    move-object/from16 v17, p0

    invoke-static/range {v10 .. v17}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/core/player/bx;Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/app/ui/bs;Lcom/google/android/youtube/app/ui/bt;)Lcom/google/android/youtube/app/ui/bg;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    .line 392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/bg;->b(Z)V

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/ui/bg;->r()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;)V

    .line 396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/Director;->q()[Landroid/view/View;

    move-result-object v3

    .line 397
    array-length v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v3, v2}, Lcom/google/android/youtube/core/utils/Util;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/view/View;

    .line 398
    array-length v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/ui/bg;->u()Landroid/view/View;

    move-result-object v4

    aput-object v4, v2, v3

    .line 400
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->y:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v3, v2, v1}, Lcom/google/android/youtube/app/compat/k;->a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;[Landroid/view/View;Lcom/google/android/youtube/coreicecream/ui/h;)Lcom/google/android/youtube/coreicecream/ui/g;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    .line 405
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v2

    if-eqz v2, :cond_2b0

    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/youtube/coreicecream/ui/g;->a(Z)V

    .line 407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 410
    :cond_2b0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->N:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ac:Lcom/google/android/youtube/core/b/ap;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->v:Lcom/google/android/youtube/app/a;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/app/g;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ah:Lcom/google/android/youtube/app/k;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v33

    move-object/from16 v21, p0

    move-object/from16 v22, v6

    move-object/from16 v31, v20

    move-object/from16 v34, v19

    move-object/from16 v35, p0

    invoke-static/range {v21 .. v35}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/app/b/g;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/fd;)Lcom/google/android/youtube/app/ui/fa;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    .line 429
    const v2, 0x7f080144

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 430
    new-instance v3, Lcom/google/android/youtube/core/a/i;

    invoke-direct {v3}, Lcom/google/android/youtube/core/a/i;-><init>()V

    .line 431
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/a/i;->b(Lcom/google/android/youtube/core/a/e;)V

    .line 432
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 434
    const v2, 0x7f08013f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->o:Landroid/view/ViewGroup;

    .line 435
    const v2, 0x7f080140

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->p:Landroid/widget/ProgressBar;

    .line 436
    const v2, 0x7f080141

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->q:Landroid/widget/ImageView;

    .line 437
    const v2, 0x7f080142

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->r:Landroid/widget/TextView;

    .line 438
    const v2, 0x7f080143

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->s:Landroid/widget/Button;

    .line 439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->s:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F()V

    .line 442
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->r()Lcom/google/android/youtube/app/remote/bm;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aj:Lcom/google/android/youtube/app/remote/bm;

    .line 443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->t()Lcom/google/android/youtube/app/remote/e;

    .line 444
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->u()Lcom/google/android/youtube/app/remote/AtHomeConnection;

    .line 445
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->r()Lcom/google/android/youtube/app/remote/bm;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aj:Lcom/google/android/youtube/app/remote/bm;

    .line 447
    new-instance v9, Lcom/google/android/youtube/app/ui/di;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->aa:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->E:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/app/g;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->O:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->N:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->v:Lcom/google/android/youtube/app/a;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x:Lcom/google/android/youtube/core/player/bx;

    move-object/from16 v20, v0

    move-object/from16 v10, p0

    invoke-direct/range {v9 .. v20}, Lcom/google/android/youtube/app/ui/di;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/dq;Lcom/google/android/youtube/app/YouTubePlatformUtil;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/player/bx;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    .line 459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->n:Lcom/google/android/youtube/app/ui/fa;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->t:Lcom/google/android/youtube/app/ui/di;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/app/ui/fa;->a(Lcom/google/android/youtube/app/ui/di;)V

    .line 461
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v3

    .line 462
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/cn;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    .line 464
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/ce;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/ce;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/ce;

    .line 465
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/cg;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/cg;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/cg;

    .line 467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/player/Director;)V

    .line 468
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v2

    if-nez v2, :cond_42c

    .line 469
    new-instance v2, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/utils/h;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    .line 471
    :cond_42c
    const-string v2, "audio"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->m:Landroid/media/AudioManager;

    .line 473
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->w:Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/h;)V

    .line 474
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V

    .line 479
    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ad:Lcom/google/android/youtube/app/remote/ab;

    .line 484
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 486
    if-eqz p1, :cond_48f

    const-string v2, "stopped"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_48f

    const/4 v2, 0x1

    :goto_477
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->H:Z

    .line 488
    const-string v2, "keyguard"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ak:Landroid/app/KeyguardManager;

    .line 489
    return-void

    .line 305
    :cond_48a
    const v2, 0x7f0400ca

    goto/16 :goto_1e

    .line 486
    :cond_48f
    const/4 v2, 0x0

    goto :goto_477
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->u()V

    .line 884
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_10

    .line 885
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    .line 887
    :cond_10
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onDestroy()V

    .line 888
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 681
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 684
    :try_start_4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ag;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ag;
    :try_end_b
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_b} :catch_27

    move-result-object v0

    .line 691
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bg;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_31

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bg;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 699
    :goto_26
    return-void

    .line 685
    :catch_27
    move-exception v0

    .line 686
    const-string v1, "invalid intercepted URI"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 687
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->finish()V

    goto :goto_26

    .line 697
    :cond_31
    iget-object v0, v0, Lcom/google/android/youtube/core/utils/ag;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/app/m;->T:Lcom/google/android/youtube/core/b/aq;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_26
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 853
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->t()V

    .line 854
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->c()V

    .line 855
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->al:Z

    .line 856
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    .line 857
    return-void
.end method

.method protected onResume()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 838
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 839
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ad:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    .line 840
    if-eqz v0, :cond_1e

    .line 841
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 842
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b(Z)V

    .line 847
    :goto_16
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->b()V

    .line 848
    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->al:Z

    .line 849
    return-void

    .line 844
    :cond_1e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->a()V

    .line 845
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ai:Lcom/google/android/youtube/app/honeycomb/phone/cn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->b(Z)V

    goto :goto_16
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 877
    const-string v0, "stopped"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 878
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 879
    return-void
.end method

.method public onStart()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 493
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    .line 495
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 497
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->V:Z

    if-eqz v1, :cond_23

    .line 498
    iput-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    .line 499
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/Director;->c(Z)V

    .line 500
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1, v4}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    .line 501
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v1

    if-nez v1, :cond_23

    .line 502
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    .line 505
    :cond_23
    const-string v1, "finish_on_ended"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->W:Z

    .line 507
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->z:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->a()V

    .line 508
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->A:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/utils/DockReceiver;->a()V

    .line 510
    const-string v1, "referrer"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 511
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_67

    .line 512
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ab:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/core/b/au;->b(Ljava/lang/String;)V

    .line 516
    :cond_46
    :goto_46
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ag:Lcom/google/android/youtube/app/YouTubeApplication;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    .line 519
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->m:Landroid/media/AudioManager;

    const/4 v2, 0x0

    const/high16 v3, -0x8000

    invoke-virtual {v1, v2, v3, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 522
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/ce;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ce;->a()V

    .line 523
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/cg;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/cg;->a()V

    .line 525
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ad:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v1, p0}, Lcom/google/android/youtube/app/remote/ab;->a(Lcom/google/android/youtube/app/remote/af;)V

    .line 527
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Intent;)V

    .line 528
    return-void

    .line 513
    :cond_67
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    if-ne v1, v2, :cond_46

    .line 514
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ab:Lcom/google/android/youtube/core/b/au;

    sget-object v2, Lcom/google/android/youtube/app/m;->L:Lcom/google/android/youtube/core/b/aq;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/b/aq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/b/au;->b(Ljava/lang/String;)V

    goto :goto_46
.end method

.method protected onStop()V
    .registers 3

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->m:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 862
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->J:Lcom/google/android/youtube/app/honeycomb/phone/ce;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ce;->b()V

    .line 863
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->K:Lcom/google/android/youtube/app/honeycomb/phone/cg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/cg;->b()V

    .line 864
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->z:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->b()V

    .line 865
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->A:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->b()V

    .line 866
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->L:Lcom/google/android/youtube/coreicecream/ui/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/coreicecream/ui/g;->b(Z)V

    .line 867
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->ad:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->b(Lcom/google/android/youtube/app/remote/af;)V

    .line 869
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->w()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->I:I

    .line 871
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->H:Z

    .line 872
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 873
    return-void
.end method

.method public final p()V
    .registers 1

    .prologue
    .line 1221
    return-void
.end method

.method public final q()V
    .registers 2

    .prologue
    .line 1224
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->S:Z

    if-eqz v0, :cond_9

    .line 1225
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->P:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->f()V

    .line 1227
    :cond_9
    return-void
.end method

.method public final r()V
    .registers 2

    .prologue
    .line 1233
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-nez v0, :cond_f

    .line 1234
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->M:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    .line 1235
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->setRequestedOrientation(I)V

    .line 1237
    :cond_f
    return-void
.end method

.method public final s()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final t()I
    .registers 2

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1273
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->af:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bg;->t()I

    move-result v0

    .line 1275
    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->F:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->w()I

    move-result v0

    goto :goto_e
.end method
