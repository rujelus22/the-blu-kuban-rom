.class public final Lcom/google/android/youtube/app/compat/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/g;->a:Landroid/content/Context;

    .line 33
    iput p3, p0, Lcom/google/android/youtube/app/compat/g;->b:I

    .line 34
    iput p2, p0, Lcom/google/android/youtube/app/compat/g;->d:I

    .line 35
    iput p4, p0, Lcom/google/android/youtube/app/compat/g;->c:I

    .line 36
    iput-object p5, p0, Lcom/google/android/youtube/app/compat/g;->e:Ljava/lang/CharSequence;

    .line 37
    iput-boolean v0, p0, Lcom/google/android/youtube/app/compat/g;->i:Z

    .line 38
    iput-boolean v0, p0, Lcom/google/android/youtube/app/compat/g;->j:Z

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/compat/g;->h:I

    .line 40
    return-void
.end method


# virtual methods
.method public final collapseActionView()Z
    .registers 2

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public final expandActionView()Z
    .registers 2

    .prologue
    .line 127
    const/4 v0, 0x0

    return v0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .registers 2

    .prologue
    .line 132
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getActionView()Landroid/view/View;
    .registers 2

    .prologue
    .line 136
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAlphabeticShortcut()C
    .registers 2

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public final getGroupId()I
    .registers 2

    .prologue
    .line 43
    iget v0, p0, Lcom/google/android/youtube/app/compat/g;->d:I

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .registers 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_7

    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->g:Landroid/graphics/drawable/Drawable;

    .line 54
    :goto_6
    return-object v0

    .line 50
    :cond_7
    iget v0, p0, Lcom/google/android/youtube/app/compat/g;->h:I

    if-eqz v0, :cond_1c

    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/app/compat/g;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/g;->g:Landroid/graphics/drawable/Drawable;

    .line 52
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->g:Landroid/graphics/drawable/Drawable;

    goto :goto_6

    .line 54
    :cond_1c
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final getIntent()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 144
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId()I
    .registers 2

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/youtube/app/compat/g;->b:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    .prologue
    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNumericShortcut()C
    .registers 2

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public final getOrder()I
    .registers 2

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/youtube/app/compat/g;->c:I

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .registers 2

    .prologue
    .line 156
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final hasSubMenu()Z
    .registers 2

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public final isActionViewExpanded()Z
    .registers 2

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public final isCheckable()Z
    .registers 2

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public final isChecked()Z
    .registers 2

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled()Z
    .registers 2

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/g;->i:Z

    return v0
.end method

.method public final isVisible()Z
    .registers 2

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/youtube/app/compat/g;->j:Z

    return v0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 177
    return-object p0
.end method

.method public final setActionView(I)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 185
    return-object p0
.end method

.method public final setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 181
    return-object p0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 189
    return-object p0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 193
    return-object p0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 197
    return-object p0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/google/android/youtube/app/compat/g;->i:Z

    .line 95
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/g;->g:Landroid/graphics/drawable/Drawable;

    .line 89
    iput p1, p0, Lcom/google/android/youtube/app/compat/g;->h:I

    .line 90
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/g;->g:Landroid/graphics/drawable/Drawable;

    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/compat/g;->h:I

    .line 84
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 201
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 205
    return-object p0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 210
    return-object p0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 119
    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 214
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .registers 2
    .parameter

    .prologue
    .line 218
    return-void
.end method

.method public final setShowAsActionFlags(I)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 221
    return-object p0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/g;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/g;->e:Ljava/lang/CharSequence;

    .line 105
    return-object p0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/g;->e:Ljava/lang/CharSequence;

    .line 100
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/g;->f:Ljava/lang/CharSequence;

    .line 110
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/google/android/youtube/app/compat/g;->j:Z

    .line 115
    return-object p0
.end method
