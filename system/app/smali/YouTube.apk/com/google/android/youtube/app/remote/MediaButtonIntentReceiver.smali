.class public Lcom/google/android/youtube/app/remote/MediaButtonIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Received intent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 26
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 27
    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 28
    const-string v1, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/KeyEvent;

    .line 29
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_56

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v2, v3, :cond_56

    .line 32
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    packed-switch v2, :pswitch_data_9a

    .line 59
    :pswitch_44
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized event: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 63
    :cond_56
    :goto_56
    return-void

    .line 34
    :pswitch_57
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v1, v2, :cond_63

    .line 35
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->e()V

    goto :goto_56

    .line 36
    :cond_63
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_73

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v1, v2, :cond_56

    .line 38
    :cond_73
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->d()V

    goto :goto_56

    .line 42
    :pswitch_77
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->q()Z

    move-result v1

    if-eqz v1, :cond_56

    .line 43
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->r()V

    goto :goto_56

    .line 47
    :pswitch_81
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->s()Z

    move-result v1

    if-eqz v1, :cond_8b

    .line 48
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()V

    goto :goto_56

    .line 53
    :cond_8b
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_56

    .line 54
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->b(I)V

    goto :goto_56

    .line 32
    :pswitch_data_9a
    .packed-switch 0x55
        :pswitch_57
        :pswitch_44
        :pswitch_77
        :pswitch_81
    .end packed-switch
.end method
