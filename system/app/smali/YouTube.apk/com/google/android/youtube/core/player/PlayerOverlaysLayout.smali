.class public Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/core/player/at;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->setFocusable(Z)V

    .line 52
    const/high16 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->setDescendantFocusability(I)V

    .line 53
    return-void
.end method


# virtual methods
.method public final varargs a([Lcom/google/android/youtube/core/player/as;)V
    .registers 6
    .parameter

    .prologue
    .line 59
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_35

    .line 60
    aget-object v1, p1, v0

    invoke-interface {v1}, Lcom/google/android/youtube/core/player/as;->a()Landroid/view/View;

    move-result-object v1

    .line 61
    if-nez v1, :cond_29

    .line 62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overlay "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v0, p1, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " does not provide a View and LayoutParams"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 65
    :cond_29
    aget-object v2, p1, v0

    invoke-interface {v2}, Lcom/google/android/youtube/core/player/as;->b()Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v2

    .line 66
    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 68
    :cond_35
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .registers 4
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->a:Lcom/google/android/youtube/core/player/at;

    if-eqz v0, :cond_e

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 80
    iget-object v1, p0, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->a:Lcom/google/android/youtube/core/player/at;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/at;->a(Landroid/graphics/Rect;)V

    .line 82
    :cond_e
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public setSystemWindowInsetsListener(Lcom/google/android/youtube/core/player/at;)V
    .registers 2
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/youtube/core/player/PlayerOverlaysLayout;->a:Lcom/google/android/youtube/core/player/at;

    .line 72
    return-void
.end method
