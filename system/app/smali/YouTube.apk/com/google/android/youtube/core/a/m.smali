.class public abstract Lcom/google/android/youtube/core/a/m;
.super Lcom/google/android/youtube/core/a/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/core/a/g;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/a/g;)V
    .registers 3
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/e;-><init>()V

    .line 33
    const-string v0, "viewType cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/g;

    iput-object v0, p0, Lcom/google/android/youtube/core/a/m;->a:Lcom/google/android/youtube/core/a/g;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 38
    const/4 v0, 0x1

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/core/a/g;
    .registers 3
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/core/a/m;->a:Lcom/google/android/youtube/core/a/g;

    return-object v0
.end method

.method protected final a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/core/a/m;->a:Lcom/google/android/youtube/core/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method
