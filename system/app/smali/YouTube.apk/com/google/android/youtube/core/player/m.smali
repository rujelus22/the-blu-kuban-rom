.class final Lcom/google/android/youtube/core/player/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ap;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)V
    .registers 2
    .parameter

    .prologue
    .line 869
    iput-object p1, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 869
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/m;-><init>(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 872
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/TimeBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setScrubbing(Z)V

    .line 873
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->n_()V

    .line 874
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 877
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/TimeBar;->setScrubberTime(I)V

    .line 878
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 886
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_14

    .line 887
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->j()V

    .line 893
    :cond_13
    :goto_13
    return-void

    .line 888
    :cond_14
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_28

    .line 889
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->k()V

    goto :goto_13

    .line 890
    :cond_28
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_13

    .line 891
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->n()V

    goto :goto_13
.end method

.method public final b(I)V
    .registers 4
    .parameter

    .prologue
    .line 881
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->b(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setScrubbing(Z)V

    .line 882
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(I)V

    .line 883
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 896
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PAUSED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_14

    .line 897
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->k()V

    .line 901
    :cond_13
    :goto_13
    return-void

    .line 898
    :cond_14
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->ENDED:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_13

    .line 899
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->n()V

    goto :goto_13
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 904
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->c(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;->PLAYING:Lcom/google/android/youtube/core/player/DefaultControllerOverlay$State;

    if-ne v0, v1, :cond_13

    .line 905
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->j()V

    .line 907
    :cond_13
    return-void
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 910
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 911
    iget-object v0, p0, Lcom/google/android/youtube/core/player/m;->a:Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    invoke-static {v0}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->a(Lcom/google/android/youtube/core/player/DefaultControllerOverlay;)Lcom/google/android/youtube/core/player/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/k;->f()V

    .line 913
    :cond_11
    return-void
.end method
