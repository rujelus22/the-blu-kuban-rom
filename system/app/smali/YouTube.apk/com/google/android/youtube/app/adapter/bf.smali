.class public final Lcom/google/android/youtube/app/adapter/bf;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/youtube/app/adapter/bg;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bg;-><init>(Lcom/google/android/youtube/app/adapter/bf;Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bf;->a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bf;)Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bf;->a:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/bf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    const-string v0, "thumbnailSize cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    new-instance v0, Lcom/google/android/youtube/app/adapter/bf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/youtube/app/adapter/bf;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/youtube/app/adapter/bh;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/bh;-><init>(Lcom/google/android/youtube/app/adapter/bf;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
