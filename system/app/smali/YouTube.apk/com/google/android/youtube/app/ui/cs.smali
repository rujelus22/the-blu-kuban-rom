.class final Lcom/google/android/youtube/app/ui/cs;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/SubscribeHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/d;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    .line 214
    invoke-direct {p0, p2}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/d;)V

    .line 215
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0, p1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/model/UserAuth;

    .line 219
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->e(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->f(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/app/YouTubeApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->B()I

    move-result v2

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZ)V

    .line 220
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/model/UserProfile;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->g(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/app/ui/ct;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-direct {v3, v4, v5}, Lcom/google/android/youtube/app/ui/ct;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->g(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/cs;)Lcom/google/android/youtube/app/ui/cs;

    .line 225
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->h(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/app/ui/cs;

    move-result-object v0

    if-nez v0, :cond_9

    .line 247
    :goto_8
    return-void

    .line 244
    :cond_9
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/e;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/cs;)Lcom/google/android/youtube/app/ui/cs;

    goto :goto_8
.end method

.method public final i_()V
    .registers 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->h(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/app/ui/cs;

    move-result-object v0

    if-nez v0, :cond_9

    .line 237
    :goto_8
    return-void

    .line 234
    :cond_9
    invoke-super {p0}, Lcom/google/android/youtube/core/ui/e;->i_()V

    .line 235
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/cs;->a:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/cs;)Lcom/google/android/youtube/app/ui/cs;

    goto :goto_8
.end method
