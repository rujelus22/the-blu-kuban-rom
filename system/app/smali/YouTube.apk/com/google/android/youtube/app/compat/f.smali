.class public final Lcom/google/android/youtube/app/compat/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/Menu;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "conetext cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/f;->a:Landroid/content/Context;

    .line 28
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    .line 29
    return-void
.end method

.method private a(Lcom/google/android/youtube/app/compat/g;)V
    .registers 5
    .parameter

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/google/android/youtube/app/compat/g;->getOrder()I

    move-result v2

    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_d
    if-ltz v1, :cond_29

    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/g;

    .line 52
    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/g;->getOrder()I

    move-result v0

    if-gt v0, v2, :cond_25

    .line 53
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 58
    :goto_24
    return-void

    .line 50
    :cond_25
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_d

    .line 57
    :cond_29
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_24
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 39
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v1, v1, v0}, Lcom/google/android/youtube/app/compat/f;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/youtube/app/compat/f;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/youtube/app/compat/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/f;->a:Landroid/content/Context;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/compat/g;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 44
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/compat/f;->a(Lcom/google/android/youtube/app/compat/g;)V

    .line 45
    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/google/android/youtube/app/compat/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/f;->a:Landroid/content/Context;

    move v3, v2

    move v4, v2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/compat/g;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 34
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/compat/f;->a(Lcom/google/android/youtube/app/compat/g;)V

    .line 35
    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 66
    return-void
.end method

.method public final close()V
    .registers 1

    .prologue
    .line 125
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .registers 5
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/g;

    .line 74
    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/g;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_6

    .line 79
    :goto_18
    return-object v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .registers 2

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .registers 2
    .parameter

    .prologue
    .line 140
    return-void
.end method

.method public final removeItem(I)V
    .registers 4
    .parameter

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/compat/f;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_b

    .line 85
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 87
    :cond_b
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 147
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 150
    return-void
.end method

.method public final setQwertyMode(Z)V
    .registers 2
    .parameter

    .prologue
    .line 153
    return-void
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
