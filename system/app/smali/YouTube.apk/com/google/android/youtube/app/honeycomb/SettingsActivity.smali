.class public Lcom/google/android/youtube/app/honeycomb/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/j;


# instance fields
.field private a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private b:Lcom/google/android/youtube/app/ui/df;

.field private c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 364
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/SettingsActivity;)Lcom/google/android/youtube/app/YouTubeApplication;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    .line 167
    return-void
.end method

.method public final f()Lcom/google/android/youtube/app/compat/SupportActionBar;
    .registers 2

    .prologue
    .line 408
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    return-object v0
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .registers 7
    .parameter

    .prologue
    .line 56
    const v0, 0x7f060004

    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/YouTubePlatformUtil;->a(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/content/Context;)Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    move-result-object v1

    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->k()Z

    move-result v0

    if-eqz v0, :cond_2e

    sget-object v0, Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;->OriginalTablet:Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    if-ne v1, v0, :cond_4f

    .line 62
    :cond_2e
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 63
    iget-object v3, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    const-class v4, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 64
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 70
    :cond_4f
    sget-object v0, Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;->OriginalTablet:Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    if-ne v1, v0, :cond_74

    .line 71
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_57
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_74

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 72
    iget-object v2, v0, Landroid/preference/PreferenceActivity$Header;->fragment:Ljava/lang/String;

    const-class v3, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_57

    .line 73
    invoke-interface {p1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 78
    :cond_74
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0xc

    .line 82
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    .line 86
    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 90
    new-instance v0, Lcom/google/android/youtube/app/ui/df;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/df;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->b:Lcom/google/android/youtube/app/ui/df;

    .line 91
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    .line 101
    sparse-switch p1, :sswitch_data_66

    .line 136
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 103
    :sswitch_8
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/x;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/h;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/honeycomb/h;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x104

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_7

    .line 116
    :sswitch_2c
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/x;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120001

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch;->a()Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;->ordinal()I

    move-result v2

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/i;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/i;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivity;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_7

    .line 130
    :sswitch_57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->b:Lcom/google/android/youtube/app/ui/df;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/df;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 133
    :sswitch_5e
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 101
    nop

    :sswitch_data_66
    .sparse-switch
        0x3ea -> :sswitch_8
        0x3f6 -> :sswitch_2c
        0x3fd -> :sswitch_57
        0x401 -> :sswitch_5e
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 150
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_e

    .line 155
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 152
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->finish()V

    .line 153
    const/4 v0, 0x1

    goto :goto_8

    .line 150
    :pswitch_data_e
    .packed-switch 0x102002c
        :pswitch_9
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 141
    const/16 v0, 0x401

    if-ne p1, v0, :cond_a

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->c:Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$PrefetchPrefsFragment;->a(Landroid/app/Dialog;Landroid/os/Bundle;)V

    .line 146
    :goto_9
    return-void

    .line 144
    :cond_a
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_9
.end method

.method protected onResume()V
    .registers 3

    .prologue
    .line 95
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 96
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method public onSearchRequested()Z
    .registers 2

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method
