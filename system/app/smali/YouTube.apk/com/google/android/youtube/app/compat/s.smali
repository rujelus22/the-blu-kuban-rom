.class final Lcom/google/android/youtube/app/compat/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/Menu;


# instance fields
.field private final a:Lcom/google/android/youtube/app/compat/m;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/compat/m;)V
    .registers 3
    .parameter

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/compat/m;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    .line 106
    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->a(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->a(IIII)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->a(IIILjava/lang/CharSequence;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->a(Ljava/lang/CharSequence;)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/youtube/app/compat/m;->a(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->b(I)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->b(IIII)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/compat/m;->b(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .registers 3
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->b(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->a()V

    .line 163
    return-void
.end method

.method public final close()V
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->b()V

    .line 167
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .registers 3
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->d(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->c()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->a(II)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/compat/m;->a(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final removeGroup(I)V
    .registers 3
    .parameter

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->e(I)V

    .line 195
    return-void
.end method

.method public final removeItem(I)V
    .registers 3
    .parameter

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    .line 199
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/app/compat/m;->a(IZZ)V

    .line 203
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->a(IZ)V

    .line 207
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/compat/m;->b(IZ)V

    .line 211
    return-void
.end method

.method public final setQwertyMode(Z)V
    .registers 3
    .parameter

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/m;->a(Z)V

    .line 215
    return-void
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/s;->a:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/m;->d()I

    move-result v0

    return v0
.end method
