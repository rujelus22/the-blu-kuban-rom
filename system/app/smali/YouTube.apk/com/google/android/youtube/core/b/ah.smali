.class final Lcom/google/android/youtube/core/b/ah;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/b/af;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/b/af;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    .line 285
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 286
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 290
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_3f0

    .line 501
    :cond_a
    :goto_a
    return-void

    .line 292
    :sswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    aget-object v0, v0, v5

    check-cast v0, Ljava/lang/String;

    .line 293
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    aget-object v1, v1, v4

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 294
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    aget-object v1, v1, v3

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 295
    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-virtual {v3, v0, v2, v1}, Lcom/google/android/youtube/core/b/af;->a(Ljava/lang/String;ZI)V

    goto :goto_a

    .line 299
    :sswitch_31
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/model/VastAd;

    .line 300
    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/model/VastAd;)V

    goto :goto_a

    .line 304
    :sswitch_3b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/model/Stream$Quality;

    .line 305
    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/model/Stream$Quality;)V

    goto :goto_a

    .line 309
    :sswitch_45
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/af;->c()V

    goto :goto_a

    .line 313
    :sswitch_4b
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 314
    invoke-virtual {p0, v6}, Lcom/google/android/youtube/core/b/ah;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_a

    .line 318
    :sswitch_56
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 319
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    goto :goto_a

    .line 324
    :sswitch_6c
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 325
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    goto :goto_a

    .line 330
    :sswitch_82
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 331
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    goto/16 :goto_a

    .line 336
    :sswitch_99
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    goto/16 :goto_a

    .line 342
    :sswitch_b0
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    if-eqz v0, :cond_d1

    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isDummy()Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 344
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 348
    :cond_d1
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-nez v0, :cond_f0

    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->c(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-nez v0, :cond_f0

    .line 349
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Z)Z

    .line 350
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->d(Lcom/google/android/youtube/core/b/af;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/b/af;->j()V

    .line 353
    :cond_f0
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;Z)Z

    goto/16 :goto_a

    .line 357
    :sswitch_f7
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_10e

    .line 358
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    goto/16 :goto_a

    .line 363
    :cond_10e
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->e(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-nez v0, :cond_138

    .line 364
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayStopped"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->f(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/b/af;->g:Z

    iget-object v4, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v4}, Lcom/google/android/youtube/core/b/af;->g(Lcom/google/android/youtube/core/b/af;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;ZI)V

    .line 368
    :goto_131
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->i(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 366
    :cond_138
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;Z)Z

    goto :goto_131

    .line 373
    :sswitch_13e
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_19d

    .line 374
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->j(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-nez v0, :cond_17f

    .line 375
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 376
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-eqz v0, :cond_17a

    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    if-eqz v0, :cond_17a

    .line 377
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->k(Lcom/google/android/youtube/core/b/af;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Landroid/net/Uri;)Z

    .line 379
    :cond_17a
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/b/af;->c(Lcom/google/android/youtube/core/b/af;Z)Z

    .line 381
    :cond_17f
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 382
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/b/af;->d(Lcom/google/android/youtube/core/b/af;Z)Z

    .line 390
    :goto_191
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;I)I

    .line 391
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;Z)Z

    goto/16 :goto_a

    .line 384
    :cond_19d
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayEnded"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->f(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/b/af;->g:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;Z)V

    .line 385
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->l(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_1bf

    .line 386
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->m(Lcom/google/android/youtube/core/b/af;)V

    .line 388
    :cond_1bf
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->i(Lcom/google/android/youtube/core/b/af;)V

    goto :goto_191

    .line 395
    :sswitch_1c5
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_1ff

    .line 396
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->n(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    if-eq v0, v2, :cond_1e7

    .line 399
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 400
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;I)I

    .line 402
    :cond_1e7
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v5}, Lcom/google/android/youtube/core/b/af;->d(Lcom/google/android/youtube/core/b/af;Z)Z

    .line 403
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "AdPlayError"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->g(Lcom/google/android/youtube/core/b/af;)I

    move-result v2

    invoke-virtual {v0, v1, v6, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_a

    .line 405
    :cond_1ff
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_225

    .line 406
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayErrorException"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->f(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/b/af;->g:Z

    iget-object v4, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v4}, Lcom/google/android/youtube/core/b/af;->g(Lcom/google/android/youtube/core/b/af;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;ZI)V

    .line 417
    :goto_21e
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->i(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 409
    :cond_225
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_258

    iget v0, p1, Landroid/os/Message;->arg2:I

    if-eqz v0, :cond_258

    .line 410
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PlayErrorMediaUnknown"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->f(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/b/af;->g:Z

    iget-object v4, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v4}, Lcom/google/android/youtube/core/b/af;->g(Lcom/google/android/youtube/core/b/af;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;ZI)V

    goto :goto_21e

    .line 413
    :cond_258
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PlayError"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->f(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-boolean v3, v3, Lcom/google/android/youtube/core/b/af;->g:Z

    iget-object v4, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v4}, Lcom/google/android/youtube/core/b/af;->g(Lcom/google/android/youtube/core/b/af;)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;ZI)V

    goto :goto_21e

    .line 422
    :sswitch_283
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 423
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    div-int/lit16 v2, v1, 0x3e8

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;I)I

    .line 424
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_30e

    .line 425
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 426
    invoke-static {v1, v0}, Lcom/google/android/youtube/core/b/af;->a(II)I

    move-result v1

    .line 427
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->n(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    if-lt v1, v0, :cond_2c9

    move v0, v1

    .line 429
    :goto_2a9
    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2}, Lcom/google/android/youtube/core/b/af;->n(Lcom/google/android/youtube/core/b/af;)I

    move-result v2

    if-lt v0, v2, :cond_2c2

    .line 430
    iget-object v2, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v2, v0}, Lcom/google/android/youtube/core/b/af;->c(Lcom/google/android/youtube/core/b/af;I)Ljava/util/List;

    move-result-object v2

    .line 431
    iget-object v3, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v3, v2}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_2c2

    .line 432
    add-int/lit8 v0, v0, -0x1

    goto :goto_2a9

    .line 435
    :cond_2c2
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;I)I

    .line 437
    :cond_2c9
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->j(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->g(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_a

    .line 438
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 439
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/VastAd;->shouldPingVssOnEngaged:Z

    if-eqz v0, :cond_307

    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    if-eqz v0, :cond_307

    .line 440
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->k(Lcom/google/android/youtube/core/b/af;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Landroid/net/Uri;)Z

    .line 442
    :cond_307
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/b/af;->c(Lcom/google/android/youtube/core/b/af;Z)Z

    goto/16 :goto_a

    .line 444
    :cond_30e
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->l(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 445
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->o(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    if-ne v0, v4, :cond_320

    if-gez v1, :cond_339

    :cond_320
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->o(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    if-ne v0, v3, :cond_32c

    const/16 v0, 0x4e20

    if-ge v1, v0, :cond_339

    :cond_32c
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->o(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_a

    const/16 v0, 0x7530

    if-lt v1, v0, :cond_a

    .line 448
    :cond_339
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->d(Lcom/google/android/youtube/core/b/af;I)Landroid/net/Uri;

    move-result-object v0

    .line 449
    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Landroid/net/Uri;)Z

    .line 450
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->p(Lcom/google/android/youtube/core/b/af;)I

    goto/16 :goto_a

    .line 456
    :sswitch_34b
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->c(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 457
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->q(Lcom/google/android/youtube/core/b/af;)I

    .line 458
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->i(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 464
    :sswitch_35f
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->c(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 465
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->r(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 470
    :sswitch_36e
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->i(Lcom/google/android/youtube/core/b/af;)V

    .line 471
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget v1, p1, Landroid/os/Message;->arg1:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;I)I

    goto/16 :goto_a

    .line 475
    :sswitch_37e
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_393

    .line 476
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 478
    :cond_393
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->i(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 482
    :sswitch_39a
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;)Z

    move-result v0

    if-eqz v0, :cond_3c9

    .line 483
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->n(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    if-nez v0, :cond_3d0

    .line 484
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->impressionUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 485
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    .line 486
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0, v4}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;I)I

    .line 491
    :cond_3c9
    :goto_3c9
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->r(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 488
    :cond_3d0
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v1}, Lcom/google/android/youtube/core/b/af;->b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z

    goto :goto_3c9

    .line 495
    :sswitch_3de
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->s(Lcom/google/android/youtube/core/b/af;)I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_a

    .line 496
    iget-object v0, p0, Lcom/google/android/youtube/core/b/ah;->a:Lcom/google/android/youtube/core/b/af;

    invoke-static {v0}, Lcom/google/android/youtube/core/b/af;->d(Lcom/google/android/youtube/core/b/af;)V

    goto/16 :goto_a

    .line 290
    nop

    :sswitch_data_3f0
    .sparse-switch
        0x1 -> :sswitch_b0
        0x2 -> :sswitch_39a
        0x3 -> :sswitch_f7
        0x4 -> :sswitch_13e
        0x5 -> :sswitch_1c5
        0x6 -> :sswitch_34b
        0x7 -> :sswitch_35f
        0x8 -> :sswitch_36e
        0x9 -> :sswitch_37e
        0xa -> :sswitch_283
        0x65 -> :sswitch_b
        0x66 -> :sswitch_31
        0x67 -> :sswitch_3b
        0x68 -> :sswitch_45
        0x69 -> :sswitch_4b
        0x6a -> :sswitch_56
        0x6b -> :sswitch_3de
        0x6c -> :sswitch_82
        0x6d -> :sswitch_99
        0x6e -> :sswitch_6c
    .end sparse-switch
.end method
