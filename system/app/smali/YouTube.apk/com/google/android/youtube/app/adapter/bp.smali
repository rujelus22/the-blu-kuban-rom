.class final Lcom/google/android/youtube/app/adapter/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/bs;
.implements Lcom/google/android/youtube/core/player/bf;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bo;

.field private final b:Landroid/widget/RelativeLayout;

.field private final c:Lcom/google/android/youtube/core/player/TimeBar;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/LinearLayout;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/os/Handler;

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/TextView;

.field private final l:Lcom/google/android/youtube/app/adapter/am;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/bo;Landroid/view/View;Landroid/view/ViewGroup;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    .line 69
    iput-object p1, p0, Lcom/google/android/youtube/app/adapter/bp;->a:Lcom/google/android/youtube/app/adapter/bo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const v0, 0x7f080084

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f08007d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->d:Landroid/widget/ImageView;

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f080080

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->e:Landroid/widget/LinearLayout;

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f080046

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->k:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f080042

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->f:Landroid/view/View;

    .line 76
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    const v1, 0x7f080085

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->g:Landroid/view/View;

    .line 78
    new-instance v0, Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bo;->a(Lcom/google/android/youtube/app/adapter/bo;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->i:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->i:Landroid/view/View;

    const v1, 0x7f020052

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 80
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 82
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bp;->i:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    new-instance v0, Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bo;->a(Lcom/google/android/youtube/app/adapter/bo;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    const v1, 0x7f02012d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    const v1, 0x7f020029

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 87
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 89
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 90
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/adapter/bq;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/adapter/bq;-><init>(Lcom/google/android/youtube/app/adapter/bp;Lcom/google/android/youtube/app/adapter/bo;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    new-instance v0, Lcom/google/android/youtube/core/player/TimeBar;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bo;->a(Lcom/google/android/youtube/app/adapter/bo;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/youtube/core/player/TimeBar;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/bf;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    .line 100
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/core/player/TimeBar;->setShowScrubber(Z)V

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/core/player/TimeBar;->setEnabled(Z)V

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bo;->a(Lcom/google/android/youtube/app/adapter/bo;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/TimeBar;->setProgressColor(I)V

    .line 103
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 105
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 106
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    new-instance v0, Lcom/google/android/youtube/app/adapter/br;

    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bo;->a(Lcom/google/android/youtube/app/adapter/bo;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/youtube/app/adapter/br;-><init>(Lcom/google/android/youtube/app/adapter/bp;Landroid/os/Looper;Lcom/google/android/youtube/app/adapter/bo;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->h:Landroid/os/Handler;

    .line 122
    invoke-static {p1}, Lcom/google/android/youtube/app/adapter/bo;->d(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/app/adapter/al;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/al;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/am;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->l:Lcom/google/android/youtube/app/adapter/am;

    .line 124
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/adapter/bo;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/app/adapter/bp;-><init>(Lcom/google/android/youtube/app/adapter/bo;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bp;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->h:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bp;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/adapter/bp;->b(I)V

    return-void
.end method

.method private b(I)V
    .registers 5
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->a:Lcom/google/android/youtube/app/adapter/bo;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/bo;->b(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_25

    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->a:Lcom/google/android/youtube/app/adapter/bo;

    invoke-static {v0}, Lcom/google/android/youtube/app/adapter/bo;->b(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->j()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 172
    :goto_1d
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/youtube/core/player/TimeBar;->setTime(III)V

    .line 173
    return-void

    :cond_25
    move v0, p1

    .line 170
    goto :goto_1d
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, -0x1

    const/4 v7, -0x2

    const v6, 0x7f08007c

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 51
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->a:Lcom/google/android/youtube/app/adapter/bo;

    invoke-static {v1}, Lcom/google/android/youtube/app/adapter/bo;->b(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/core/player/TimeBar;->setVisibility(I)V

    iget v0, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/adapter/bp;->b(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->h:Landroid/os/Handler;

    iget v2, p2, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-static {v1, v4, v2, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->d:Landroid/widget/ImageView;

    const v1, 0x7f020200

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->f:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->g:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->l:Lcom/google/android/youtube/app/adapter/am;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/adapter/am;->a(Z)V

    :goto_6c
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->l:Lcom/google/android/youtube/app/adapter/am;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/adapter/am;->a(Lcom/google/android/youtube/core/model/Video;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->b:Landroid/widget/RelativeLayout;

    return-object v0

    :cond_74
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->c:Lcom/google/android/youtube/core/player/TimeBar;

    invoke-virtual {v0, v5}, Lcom/google/android/youtube/core/player/TimeBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->h:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->d:Landroid/widget/ImageView;

    const v1, 0x7f0201ff

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bp;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->j:Landroid/widget/ImageView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bp;->l:Lcom/google/android/youtube/app/adapter/am;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/am;->a(Z)V

    goto :goto_6c
.end method

.method public final a(I)V
    .registers 2
    .parameter

    .prologue
    .line 179
    return-void
.end method

.method public final n_()V
    .registers 1

    .prologue
    .line 176
    return-void
.end method
