.class public final Lcom/google/android/youtube/app/player/a/i;
.super Lcom/google/android/youtube/core/player/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/player/a/e;

.field private final b:Lcom/google/android/youtube/app/player/a/j;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/player/aq;Lcom/google/android/youtube/app/player/a/e;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/player/s;-><init>(Lcom/google/android/youtube/core/player/aq;)V

    .line 49
    const-string v0, "mediaServer cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/player/a/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    .line 50
    new-instance v0, Lcom/google/android/youtube/app/player/a/j;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/player/a/j;-><init>(Lcom/google/android/youtube/app/player/a/i;Landroid/os/Looper;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->b:Lcom/google/android/youtube/app/player/a/j;

    .line 51
    return-void
.end method

.method private a()V
    .registers 5

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/a/i;->c:Z

    if-eqz v0, :cond_11

    .line 78
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->b:Lcom/google/android/youtube/app/player/a/j;

    const/4 v1, 0x2

    const/16 v2, 0x64

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/app/player/a/j;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 80
    :cond_11
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a/i;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/player/a/i;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/player/a/i;II)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    const/4 v0, 0x1

    const/16 v1, -0x3eb

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/player/a/i;->b(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)V
    .registers 2
    .parameter

    .prologue
    .line 74
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/ar;)V
    .registers 2
    .parameter

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/player/s;->a(Lcom/google/android/youtube/core/player/ar;)V

    .line 68
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->a()V

    .line 69
    return-void
.end method

.method public final setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 56
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/player/a/e;->a()Z

    move-result v0

    if-nez v0, :cond_16

    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->b:Lcom/google/android/youtube/app/player/a/j;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/player/a/j;->sendEmptyMessage(I)Z

    .line 58
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaServer is not started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/app/player/a/i;->a:Lcom/google/android/youtube/app/player/a/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/player/a/e;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0}, Lcom/google/android/youtube/core/player/s;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 61
    iput-boolean v1, p0, Lcom/google/android/youtube/app/player/a/i;->c:Z

    .line 62
    invoke-direct {p0}, Lcom/google/android/youtube/app/player/a/i;->a()V

    .line 63
    return-void
.end method
