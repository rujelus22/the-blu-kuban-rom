.class final Lcom/google/android/youtube/app/honeycomb/phone/aj;
.super Lcom/google/android/youtube/app/adapter/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/y;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 551
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    .line 552
    iget-object v0, p1, Lcom/google/android/youtube/app/honeycomb/phone/y;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    const v1, 0x7f080040

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/youtube/app/adapter/k;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    .line 553
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->b:Landroid/view/View;

    .line 554
    const v0, 0x7f080046

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->c:Landroid/widget/TextView;

    .line 555
    const v0, 0x7f08003f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 556
    const v1, 0x7f02000c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 557
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 547
    check-cast p2, Lcom/google/android/youtube/core/model/Subscription;

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Subscription;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->l(Lcom/google/android/youtube/app/honeycomb/phone/y;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_33

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->b:Landroid/view/View;

    const v2, 0x7f08003f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/y;->a(Lcom/google/android/youtube/app/honeycomb/phone/y;Landroid/view/View;)Landroid/view/View;

    :cond_33
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->b:Landroid/view/View;

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/youtube/core/async/l;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 547
    check-cast p1, Lcom/google/android/youtube/core/model/Subscription;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/aj;->a:Lcom/google/android/youtube/app/honeycomb/phone/y;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/y;->u(Lcom/google/android/youtube/app/honeycomb/phone/y;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Subscription;->channelUri:Landroid/net/Uri;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/ak;

    invoke-direct {v2, p0, p3}, Lcom/google/android/youtube/app/honeycomb/phone/ak;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/aj;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method
