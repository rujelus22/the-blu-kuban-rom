.class final Lcom/google/android/youtube/app/honeycomb/phone/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/e;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/e;)V
    .registers 2
    .parameter

    .prologue
    .line 375
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/f;->a:Lcom/google/android/youtube/app/honeycomb/phone/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/e;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 375
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/f;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 375
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 375
    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_41

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/f;->a:Lcom/google/android/youtube/app/honeycomb/phone/e;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->h(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/f;->a:Lcom/google/android/youtube/app/honeycomb/phone/e;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->g(Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/f;->a:Lcom/google/android/youtube/app/honeycomb/phone/e;

    iget-object v3, v3, Lcom/google/android/youtube/app/honeycomb/phone/e;->a:Lcom/google/android/youtube/app/honeycomb/phone/c;

    iget-object v3, v3, Lcom/google/android/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;

    iget-object v3, v3, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v4, Lcom/google/android/youtube/app/honeycomb/phone/g;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/f;->a:Lcom/google/android/youtube/app/honeycomb/phone/e;

    sget-object v6, Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;->PLAYLISTS:Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;

    invoke-direct {v4, v5, v6}, Lcom/google/android/youtube/app/honeycomb/phone/g;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/e;Lcom/google/android/youtube/app/honeycomb/phone/AccountLayer$AccountLayerItem;)V

    invoke-static {v3, v4}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    :cond_41
    return-void
.end method
