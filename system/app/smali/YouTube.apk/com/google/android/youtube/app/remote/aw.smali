.class final Lcom/google/android/youtube/app/remote/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/remote/aq;

.field private final b:Lcom/google/android/youtube/core/b/an;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/b/an;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    iput-object p2, p0, Lcom/google/android/youtube/app/remote/aw;->b:Lcom/google/android/youtube/core/b/an;

    .line 223
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/b/an;B)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/remote/aw;-><init>(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/b/an;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 217
    const-string v0, "Error downloading video info"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/aq;->b(Lcom/google/android/youtube/app/remote/aq;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 217
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/remote/aw;->b:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/aq;

    new-instance v3, Lcom/google/android/youtube/app/remote/as;

    iget-object v4, p0, Lcom/google/android/youtube/app/remote/aw;->a:Lcom/google/android/youtube/app/remote/aq;

    invoke-direct {v3, v4, p2}, Lcom/google/android/youtube/app/remote/as;-><init>(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/model/Video;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/remote/aq;->a(Lcom/google/android/youtube/app/remote/aq;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/an;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method
