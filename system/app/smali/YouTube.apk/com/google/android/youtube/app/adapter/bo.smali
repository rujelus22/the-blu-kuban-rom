.class public final Lcom/google/android/youtube/app/adapter/bo;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/remote/RemoteControl;

.field private final c:Lcom/google/android/youtube/app/adapter/al;

.field private final d:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/RemoteControl;Lcom/google/android/youtube/app/ui/s;Lcom/google/android/youtube/core/Analytics;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 41
    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->a:Landroid/app/Activity;

    .line 42
    const-string v0, "remoteControl can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/RemoteControl;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->b:Lcom/google/android/youtube/app/remote/RemoteControl;

    .line 43
    new-instance v0, Lcom/google/android/youtube/app/adapter/al;

    invoke-direct {v0, p3}, Lcom/google/android/youtube/app/adapter/al;-><init>(Lcom/google/android/youtube/app/ui/s;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->c:Lcom/google/android/youtube/app/adapter/al;

    .line 44
    const-string v0, "analytics can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->d:Lcom/google/android/youtube/core/Analytics;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/bo;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->b:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->d:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/adapter/bo;)Lcom/google/android/youtube/app/adapter/al;
    .registers 2
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bo;->c:Lcom/google/android/youtube/app/adapter/al;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/youtube/app/adapter/bp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/youtube/app/adapter/bp;-><init>(Lcom/google/android/youtube/app/adapter/bo;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method
