.class public final Lcom/google/android/youtube/app/ui/ba;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/core/a/a;

.field private final b:Lcom/google/android/youtube/app/ui/by;

.field private final h:Lcom/google/android/youtube/app/ui/bb;

.field private i:Lcom/google/android/youtube/app/ui/bc;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/bb;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-direct/range {p0 .. p5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;)V

    .line 83
    const-string v0, "onPlaylistClickListener can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bb;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->h:Lcom/google/android/youtube/app/ui/bb;

    .line 86
    instance-of v0, p3, Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_24

    move-object v0, p3

    .line 87
    check-cast v0, Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/by;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 88
    check-cast p3, Lcom/google/android/youtube/app/ui/by;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ba;->b:Lcom/google/android/youtube/app/ui/by;

    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->b:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/by;->b()Lcom/google/android/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    .line 95
    :goto_23
    return-void

    .line 91
    :cond_24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->b:Lcom/google/android/youtube/app/ui/by;

    .line 92
    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    .line 93
    invoke-interface {p2, p0}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_23
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 101
    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_28

    iget-object v0, p1, Lcom/google/android/youtube/core/async/GDataRequest;->d:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/UserAuth;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_28

    instance-of v0, p2, Lcom/google/android/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_28

    move-object v0, p2

    check-cast v0, Lcom/google/android/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 104
    new-instance v0, Lcom/google/android/youtube/core/model/Page$Builder;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/Page$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Page$Builder;->build()Lcom/google/android/youtube/core/model/Page;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/ui/ba;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    .line 108
    :goto_27
    return-void

    .line 106
    :cond_28
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    goto :goto_27
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/ui/ba;->a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    if-eqz p1, :cond_1a

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 113
    :goto_6
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_19

    .line 114
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    .line 115
    if-eqz v0, :cond_19

    .line 116
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ba;->h:Lcom/google/android/youtube/app/ui/bb;

    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/ui/bb;->a(Lcom/google/android/youtube/core/model/Playlist;)V

    .line 119
    :cond_19
    return-void

    .line 111
    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    goto :goto_6
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 122
    if-eqz p1, :cond_15

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 123
    :goto_6
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    .line 124
    if-eqz v0, :cond_18

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->i:Lcom/google/android/youtube/app/ui/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/bc;->a()Z

    move-result v0

    .line 127
    :goto_14
    return v0

    .line 122
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ba;->a:Lcom/google/android/youtube/core/a/a;

    goto :goto_6

    .line 127
    :cond_18
    const/4 v0, 0x0

    goto :goto_14
.end method
