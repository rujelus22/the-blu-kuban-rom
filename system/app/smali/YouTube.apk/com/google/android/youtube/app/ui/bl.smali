.class final Lcom/google/android/youtube/app/ui/bl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bg;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bg;)V
    .registers 2
    .parameter

    .prologue
    .line 509
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 509
    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->i(Lcom/google/android/youtube/app/ui/bg;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0036

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 509
    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->i(Lcom/google/android/youtube/app/ui/bg;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0036

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    :goto_15
    return-void

    :cond_16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    if-nez v1, :cond_39

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->i(Lcom/google/android/youtube/app/ui/bg;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b0035

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_39
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bl;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->j(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/player/ControllerOverlay;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a(Ljava/util/List;)V

    goto :goto_15
.end method
