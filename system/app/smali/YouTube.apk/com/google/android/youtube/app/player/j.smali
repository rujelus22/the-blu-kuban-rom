.class public final Lcom/google/android/youtube/app/player/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/player/i;
.implements Lcom/google/android/youtube/core/player/cc;


# instance fields
.field private final a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final b:Lcom/google/android/youtube/core/utils/l;

.field private final c:I

.field private final d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/l;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/youtube/app/player/j;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 39
    const-string v0, "networkStatus cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/player/j;->b:Lcom/google/android/youtube/core/utils/l;

    .line 41
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2d

    invoke-virtual {p2}, Lcom/google/android/youtube/app/k;->h()Z

    move-result v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x1

    :goto_1e
    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/j;->e:Z

    .line 43
    invoke-virtual {p2}, Lcom/google/android/youtube/app/k;->B()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/player/j;->c:I

    .line 44
    invoke-virtual {p2}, Lcom/google/android/youtube/app/k;->C()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/player/j;->d:I

    .line 45
    return-void

    .line 41
    :cond_2d
    const/4 v0, 0x0

    goto :goto_1e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Stream;Z)Lcom/google/android/youtube/core/player/aq;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 49
    new-instance v1, Lcom/google/android/youtube/core/player/ae;

    invoke-direct {v1}, Lcom/google/android/youtube/core/player/ae;-><init>()V

    .line 50
    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    if-eqz v0, :cond_1f

    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/player/j;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->C()Lcom/google/android/youtube/app/player/a/e;

    move-result-object v2

    .line 52
    if-eqz v2, :cond_17

    .line 53
    new-instance v0, Lcom/google/android/youtube/app/player/a/i;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/player/a/i;-><init>(Lcom/google/android/youtube/core/player/aq;Lcom/google/android/youtube/app/player/a/e;)V

    .line 60
    :goto_16
    return-object v0

    .line 56
    :cond_17
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "Cannot create ProxyPlayer because MediaServer is null"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_1f
    iget-boolean v0, p0, Lcom/google/android/youtube/app/player/j;->e:Z

    if-eqz v0, :cond_3a

    if-nez p2, :cond_3a

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3a

    new-instance v0, Lcom/google/android/youtube/app/player/a;

    iget v2, p0, Lcom/google/android/youtube/app/player/j;->c:I

    iget v3, p0, Lcom/google/android/youtube/app/player/j;->d:I

    iget-object v4, p0, Lcom/google/android/youtube/app/player/j;->b:Lcom/google/android/youtube/core/utils/l;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/player/a;-><init>(Lcom/google/android/youtube/core/player/aq;IILcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/player/i;)V

    goto :goto_16

    :cond_3a
    move-object v0, v1

    goto :goto_16
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/player/j;->e:Z

    .line 69
    return-void
.end method
