.class public abstract Lcom/google/android/youtube/coreicecream/ControllerActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"


# instance fields
.field private final m:Ljava/util/List;

.field private n:Lcom/google/android/youtube/coreicecream/Controller;

.field private o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

.field private p:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    .line 45
    sget-object v0, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 46
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/coreicecream/Controller;)I
    .registers 3
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected a(Lcom/google/android/youtube/coreicecream/Controller;Landroid/os/Bundle;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const/4 v0, 0x0

    .line 66
    if-eqz p2, :cond_1f

    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "controller_state_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 71
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/LayoutInflater;Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_37

    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->b(Lcom/google/android/youtube/coreicecream/Controller;)V

    .line 77
    :cond_37
    return-void
.end method

.method protected final b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 162
    if-eqz p1, :cond_1e

    .line 163
    const-string v0, "selected_controller_index"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 164
    if-ltz v0, :cond_1e

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1e

    .line 165
    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->b(Lcom/google/android/youtube/coreicecream/Controller;)V

    .line 168
    :cond_1e
    return-void
.end method

.method protected final b(Lcom/google/android/youtube/coreicecream/Controller;)V
    .registers 6
    .parameter

    .prologue
    .line 106
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/o;->a(Z)V

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    if-eq p1, v0, :cond_38

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    if-eqz v0, :cond_30

    .line 111
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    sget-object v1, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    const-string v2, "controller cannot be null"

    invoke-static {v0, v2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "newState cannot be null"

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "given controller not managed by this activity"

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V

    .line 113
    :cond_30
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->invalidateOptionsMenu()V

    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->c(Lcom/google/android/youtube/coreicecream/Controller;)V

    .line 118
    :cond_38
    return-void
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x4

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->p:Z

    .line 244
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 245
    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 247
    return-void
.end method

.method protected c(Lcom/google/android/youtube/coreicecream/Controller;)V
    .registers 3
    .parameter

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/google/android/youtube/coreicecream/Controller;->p()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 151
    invoke-virtual {p1}, Lcom/google/android/youtube/coreicecream/Controller;->p()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->setContentView(Landroid/view/View;)V

    .line 153
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/coreicecream/Controller;->a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V

    .line 154
    return-void
.end method

.method protected final d(Lcom/google/android/youtube/coreicecream/Controller;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 390
    invoke-virtual {p1}, Lcom/google/android/youtube/coreicecream/Controller;->p()Landroid/view/View;

    move-result-object v0

    .line 391
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x10102eb

    aput v3, v2, v4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 392
    const/4 v2, 0x0

    invoke-virtual {v1, v4, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    .line 393
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 398
    return-void
.end method

.method protected e()V
    .registers 1

    .prologue
    .line 349
    return-void
.end method

.method protected final f()Lcom/google/android/youtube/coreicecream/Controller;
    .registers 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    return-object v0
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .registers 2

    .prologue
    .line 235
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getActionBar()Landroid/app/ActionBar;
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 238
    :goto_4
    return-object v0

    :catch_5
    move-exception v0

    const/4 v0, 0x0

    goto :goto_4
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/youtube/coreicecream/Controller;->a(IILandroid/content/Intent;)Z

    .line 383
    return-void
.end method

.method public final onBackPressed()V
    .registers 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/Controller;->h()Z

    move-result v0

    if-nez v0, :cond_b

    .line 354
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 356
    :cond_b
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 56
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->setVolumeControlStream(I)V

    .line 57
    return-void
.end method

.method protected final onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/coreicecream/Controller;->c(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method protected final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    if-eqz v0, :cond_d

    .line 316
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 318
    :cond_d
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 319
    const/4 v0, 0x1

    return v0
.end method

.method protected final onDestroy()V
    .registers 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/Controller;

    .line 227
    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/Controller;->d()V

    goto :goto_6

    .line 229
    :cond_16
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 230
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/coreicecream/Controller;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 333
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_12

    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->p:Z

    if-eqz v0, :cond_12

    .line 335
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->e()V

    .line 336
    const/4 v0, 0x1

    .line 338
    :goto_11
    return v0

    :cond_12
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_11
.end method

.method public onOptionsMenuClosed(Landroid/view/Menu;)V
    .registers 3
    .parameter

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    .line 345
    return-void
.end method

.method protected final onPause()V
    .registers 3

    .prologue
    .line 210
    sget-object v0, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 211
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    sget-object v1, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V

    .line 212
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 213
    return-void
.end method

.method protected final onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 307
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    .line 309
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    if-eqz v0, :cond_9

    .line 325
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/view/Menu;)V

    .line 327
    :cond_9
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 328
    const/4 v0, 0x1

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    move v1, v2

    .line 280
    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_38

    .line 281
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/Controller;

    .line 282
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "controller_state_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 283
    const-string v4, "controller_ui_state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v3

    .line 287
    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/Controller;->p()Landroid/view/View;

    move-result-object v0

    .line 288
    if-eqz v0, :cond_34

    .line 289
    invoke-virtual {v0, v3}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 280
    :cond_34
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 292
    :cond_38
    const-string v0, "selected_controller_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_49

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_49

    const/4 v2, 0x1

    :cond_49
    const-string v1, "controller index out of bound"

    invoke-static {v2, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/ControllerActivity;->b(Lcom/google/android/youtube/coreicecream/Controller;)V

    .line 293
    return-void
.end method

.method protected onResume()V
    .registers 3

    .prologue
    .line 203
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 204
    sget-object v0, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->RESUMED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 205
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    sget-object v1, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->RESUMED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V

    .line 206
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 260
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_43

    .line 261
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/Controller;

    .line 262
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 263
    invoke-virtual {v0, v2}, Lcom/google/android/youtube/coreicecream/Controller;->a(Landroid/os/Bundle;)V

    .line 265
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 266
    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/Controller;->p()Landroid/view/View;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_28

    .line 268
    invoke-virtual {v0, v3}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 270
    :cond_28
    const-string v0, "controller_ui_state"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "controller_state_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 260
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 274
    :cond_43
    const-string v0, "selected_controller_index"

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    return-void
.end method

.method public final onSearchRequested()Z
    .registers 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    invoke-virtual {v0}, Lcom/google/android/youtube/coreicecream/Controller;->n()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 252
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    move-result v0

    .line 254
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 172
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 173
    sget-object v0, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    if-eqz v0, :cond_12

    .line 178
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->n:Lcom/google/android/youtube/coreicecream/Controller;

    sget-object v1, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->PAUSED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/coreicecream/Controller;->a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V

    .line 182
    :cond_12
    return-void
.end method

.method protected onStop()V
    .registers 4

    .prologue
    .line 217
    sget-object v0, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->o:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/ControllerActivity;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/coreicecream/Controller;

    .line 219
    sget-object v2, Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;->STOPPED:Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/coreicecream/Controller;->a(Lcom/google/android/youtube/coreicecream/Controller$LifecycleState;)V

    goto :goto_a

    .line 221
    :cond_1c
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 222
    return-void
.end method
