.class public abstract Lcom/google/android/youtube/core/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final b:Lcom/google/android/youtube/core/a/g;


# instance fields
.field private a:Lcom/google/android/youtube/core/a/f;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/youtube/core/a/g;

    const-string v1, "IGNORE_VIEW_TYPE"

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/a/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/youtube/core/a/e;->b:Lcom/google/android/youtube/core/a/g;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/a/e;->c:Z

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(I)Lcom/google/android/youtube/core/a/g;
.end method

.method final a(Lcom/google/android/youtube/core/a/f;)V
    .registers 2
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/youtube/core/a/e;->a:Lcom/google/android/youtube/core/a/f;

    .line 95
    return-void
.end method

.method protected abstract a(Ljava/util/Set;)V
.end method

.method public abstract b(I)Ljava/lang/Object;
.end method

.method public c(I)J
    .registers 4
    .parameter

    .prologue
    .line 185
    int-to-long v0, p1

    return-wide v0
.end method

.method public final c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/e;->c:Z

    if-eq v0, p1, :cond_7

    .line 119
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/e;->m()V

    .line 121
    :cond_7
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method protected final k()V
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/core/a/e;->a:Lcom/google/android/youtube/core/a/f;

    if-eqz v0, :cond_9

    .line 103
    iget-object v0, p0, Lcom/google/android/youtube/core/a/e;->a:Lcom/google/android/youtube/core/a/f;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/a/f;->a(Lcom/google/android/youtube/core/a/e;)V

    .line 105
    :cond_9
    return-void
.end method

.method public final l()Z
    .registers 2

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/e;->c:Z

    return v0
.end method

.method public final m()V
    .registers 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/e;->c:Z

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/youtube/core/a/e;->c:Z

    .line 128
    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/e;->k()V

    .line 129
    return-void

    .line 127
    :cond_b
    const/4 v0, 0x0

    goto :goto_5
.end method

.method final n()I
    .registers 2

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/youtube/core/a/e;->c:Z

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/youtube/core/a/e;->a()I

    move-result v0

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method
