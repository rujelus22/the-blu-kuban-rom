.class public abstract Lcom/google/android/youtube/athome/server/BinderActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/athome/server/a;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPause()V
    .registers 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_5
    const-string v1, "should called setHandler in onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    .line 35
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 36
    return-void

    .line 33
    :cond_10
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected onResume()V
    .registers 3

    .prologue
    .line 26
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 27
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_8
    const-string v1, "should called setHandler in onCreate()"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lcom/google/android/youtube/athome/server/BinderActivity;->a:Lcom/google/android/youtube/athome/server/a;

    .line 29
    return-void

    .line 27
    :cond_10
    const/4 v0, 0x0

    goto :goto_8
.end method
