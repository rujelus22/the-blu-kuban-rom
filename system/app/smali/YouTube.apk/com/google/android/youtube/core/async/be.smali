.class final Lcom/google/android/youtube/core/async/be;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/bc;

.field private final b:Lcom/google/android/youtube/core/async/l;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/bc;Lcom/google/android/youtube/core/async/l;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/youtube/core/async/be;->a:Lcom/google/android/youtube/core/async/bc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/l;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/be;->b:Lcom/google/android/youtube/core/async/l;

    .line 105
    iput-boolean p3, p0, Lcom/google/android/youtube/core/async/be;->c:Z

    .line 106
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 98
    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/be;->b:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 98
    check-cast p1, Landroid/net/Uri;

    check-cast p2, Ljava/lang/Long;

    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/be;->c:Z

    if-eqz v0, :cond_24

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_24

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/be;->c:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/async/be;->a:Lcom/google/android/youtube/core/async/bc;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/bc;->a(Lcom/google/android/youtube/core/async/bc;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/async/be;->a:Lcom/google/android/youtube/core/async/bc;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/bc;->b(Lcom/google/android/youtube/core/async/bc;)Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    :goto_23
    return-void

    :cond_24
    iget-object v0, p0, Lcom/google/android/youtube/core/async/be;->b:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_23
.end method
