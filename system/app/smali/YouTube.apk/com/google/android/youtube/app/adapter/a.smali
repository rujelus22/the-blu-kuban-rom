.class public final Lcom/google/android/youtube/app/adapter/a;
.super Lcom/google/android/youtube/app/adapter/j;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/adapter/cb;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/an;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/youtube/app/adapter/j;-><init>()V

    .line 31
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->a:Landroid/app/Activity;

    .line 32
    const-string v0, "imageClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/google/android/youtube/app/adapter/b;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/adapter/b;-><init>(Lcom/google/android/youtube/app/adapter/a;)V

    .line 38
    sget-object v1, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p1, p2, v1}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v1

    .line 40
    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->b:Lcom/google/android/youtube/app/adapter/cb;

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/adapter/a;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/adapter/a;)Lcom/google/android/youtube/app/adapter/cb;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/a;->b:Lcom/google/android/youtube/app/adapter/cb;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/youtube/app/adapter/c;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/adapter/c;-><init>(Lcom/google/android/youtube/app/adapter/a;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
