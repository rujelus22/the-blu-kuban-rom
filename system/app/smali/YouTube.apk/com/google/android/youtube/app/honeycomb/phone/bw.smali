.class public final Lcom/google/android/youtube/app/honeycomb/phone/bw;
.super Lcom/google/android/youtube/app/honeycomb/phone/u;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/prefetch/f;
.implements Lcom/google/android/youtube/core/async/bn;


# instance fields
.field private A:Lcom/google/android/youtube/app/adapter/bt;

.field private B:Lcom/google/android/youtube/app/adapter/bt;

.field private C:Ljava/lang/String;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/b/an;

.field private final f:Lcom/google/android/youtube/core/b/ap;

.field private final g:Lcom/google/android/youtube/core/utils/l;

.field private final h:Lcom/google/android/youtube/app/prefetch/d;

.field private final i:Lcom/google/android/youtube/core/j;

.field private final j:Lcom/google/android/youtube/app/YouTubePlatformUtil;

.field private final k:Lcom/google/android/youtube/core/d;

.field private final l:Lcom/google/android/youtube/core/Analytics;

.field private final m:Lcom/google/android/youtube/core/b/al;

.field private final n:Landroid/content/SharedPreferences;

.field private final o:Lcom/google/android/youtube/app/a;

.field private final p:Lcom/google/android/youtube/app/ui/aw;

.field private final q:Landroid/view/ViewGroup;

.field private final r:Lcom/google/android/youtube/core/ui/PagedListView;

.field private final s:Lcom/google/android/youtube/app/honeycomb/phone/bz;

.field private t:Landroid/view/View;

.field private u:Z

.field private v:Lcom/google/android/youtube/app/ui/by;

.field private w:Lcom/google/android/youtube/app/ui/by;

.field private x:Lcom/google/android/youtube/app/ui/a;

.field private y:Lcom/google/android/youtube/app/ui/eb;

.field private z:Lcom/google/android/youtube/core/async/av;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/honeycomb/phone/bz;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 97
    const-string v0, "listener may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/bz;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->s:Lcom/google/android/youtube/app/honeycomb/phone/bz;

    .line 98
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 99
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Landroid/view/LayoutInflater;

    .line 100
    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Landroid/view/LayoutInflater;

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v1

    if-eqz v1, :cond_96

    const v1, 0x7f04009f

    move v2, v1

    :goto_25
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->o()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q:Landroid/view/ViewGroup;

    .line 102
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    .line 103
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 104
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->e:Lcom/google/android/youtube/core/b/an;

    .line 105
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->f:Lcom/google/android/youtube/core/b/ap;

    .line 106
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->g:Lcom/google/android/youtube/core/utils/l;

    .line 107
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->h:Lcom/google/android/youtube/app/prefetch/d;

    .line 108
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->k:Lcom/google/android/youtube/core/d;

    .line 109
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->l:Lcom/google/android/youtube/core/Analytics;

    .line 110
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->i:Lcom/google/android/youtube/core/j;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/app/YouTubePlatformUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->j:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    .line 112
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    .line 113
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->n:Landroid/content/SharedPreferences;

    .line 114
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->o:Lcom/google/android/youtube/app/a;

    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q:Landroid/view/ViewGroup;

    const v1, 0x7f080135

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->r:Lcom/google/android/youtube/core/ui/PagedListView;

    .line 116
    new-instance v0, Lcom/google/android/youtube/app/ui/aw;

    invoke-direct {v0}, Lcom/google/android/youtube/app/ui/aw;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->p:Lcom/google/android/youtube/app/ui/aw;

    .line 117
    return-void

    .line 100
    :cond_96
    const v1, 0x7f0400b4

    move v2, v1

    goto :goto_25
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/core/ui/PagedListView;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->r:Lcom/google/android/youtube/core/ui/PagedListView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/bw;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->u:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/app/YouTubePlatformUtil;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->j:Lcom/google/android/youtube/app/YouTubePlatformUtil;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/bw;)V
    .registers 1
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->p()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/app/ui/aw;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->p:Lcom/google/android/youtube/app/ui/aw;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->l:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/app/a;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->o:Lcom/google/android/youtube/app/a;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .registers 4

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->n:Landroid/content/SharedPreferences;

    const-string v1, "channel_feed_content"

    const-string v2, "all_activity"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .registers 13

    .prologue
    const v11, 0x7f0a005d

    const/4 v0, 0x0

    .line 168
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->B:Lcom/google/android/youtube/app/adapter/bt;

    .line 169
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->y:Lcom/google/android/youtube/app/ui/eb;

    .line 170
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->w:Lcom/google/android/youtube/app/ui/by;

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->p:Lcom/google/android/youtube/app/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/aw;->a()V

    .line 174
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->e:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->f:Lcom/google/android/youtube/core/b/ap;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->g:Lcom/google/android/youtube/core/utils/l;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->h:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->i:Lcom/google/android/youtube/core/j;

    new-instance v9, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v9, v1}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    invoke-interface {v4}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v9, v5}, Lcom/google/android/youtube/app/adapter/ct;->a(Landroid/graphics/Typeface;)V

    invoke-static {v1, v8, v0}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v10

    new-instance v0, Lcom/google/android/youtube/app/adapter/h;

    invoke-interface {v4, v1}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v9}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/ap;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/android/youtube/app/adapter/ap;-><init>(Landroid/content/res/Resources;Lcom/google/android/youtube/app/adapter/cb;)V

    new-instance v0, Lcom/google/android/youtube/app/adapter/cd;

    invoke-direct {v0, v1, v7, v8}, Lcom/google/android/youtube/app/adapter/cd;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v3}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/bt;

    const v3, 0x7f0400b3

    invoke-direct {v2, v1, v3, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->A:Lcom/google/android/youtube/app/adapter/bt;

    .line 183
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->A:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->v:Lcom/google/android/youtube/app/ui/by;

    .line 184
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    invoke-virtual {v4, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 189
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q()V

    .line 191
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->r:Lcom/google/android/youtube/core/ui/PagedListView;

    const v1, 0x7f0b011f

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setEmptyText(I)V

    .line 193
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bx;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->r:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->k:Lcom/google/android/youtube/core/d;

    const/4 v7, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/honeycomb/phone/bx;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/bw;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->x:Lcom/google/android/youtube/app/ui/a;

    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->x:Lcom/google/android/youtube/app/ui/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/a;->d()V

    .line 250
    return-void
.end method

.method private n()V
    .registers 14

    .prologue
    const v7, 0x7f0a005d

    const/4 v12, 0x0

    const/4 v0, 0x0

    .line 253
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->A:Lcom/google/android/youtube/app/adapter/bt;

    .line 254
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->x:Lcom/google/android/youtube/app/ui/a;

    .line 255
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->v:Lcom/google/android/youtube/app/ui/by;

    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->e:Lcom/google/android/youtube/core/b/an;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->f:Lcom/google/android/youtube/core/b/ap;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->g:Lcom/google/android/youtube/core/utils/l;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->h:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->i:Lcom/google/android/youtube/core/j;

    invoke-static/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->B:Lcom/google/android/youtube/app/adapter/bt;

    .line 266
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->B:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->w:Lcom/google/android/youtube/app/ui/by;

    .line 267
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->w:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 273
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->k()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->z:Lcom/google/android/youtube/core/async/av;

    .line 275
    new-instance v0, Lcom/google/android/youtube/app/ui/eb;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->r:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->w:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->z:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->k:Lcom/google/android/youtube/core/d;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->o:Lcom/google/android/youtube/app/a;

    sget-object v9, Lcom/google/android/youtube/app/m;->c:Lcom/google/android/youtube/core/b/aq;

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->l:Lcom/google/android/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move v6, v12

    move v8, v12

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->y:Lcom/google/android/youtube/app/ui/eb;

    .line 288
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q()V

    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->y:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->d()V

    .line 291
    return-void
.end method

.method private p()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 330
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->u:Z

    if-nez v0, :cond_6

    .line 346
    :goto_5
    return-void

    .line 333
    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    if-eqz v0, :cond_11

    .line 334
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 336
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0400b5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    const v1, 0x7f080157

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 341
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->i:Lcom/google/android/youtube/core/j;

    invoke-interface {v1}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->r:Lcom/google/android/youtube/core/ui/PagedListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/PagedListView;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->t:Landroid/view/View;

    const v1, 0x7f080158

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 345
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5
.end method

.method private q()V
    .registers 3

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 350
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->v:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_16

    .line 351
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->v:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 353
    :cond_16
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->w:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_1f

    .line 354
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->w:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 356
    :cond_1f
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 324
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;->a(Landroid/content/res/Configuration;)V

    .line 325
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->q()V

    .line 326
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->p()V

    .line 327
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 306
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->x:Lcom/google/android/youtube/app/ui/a;

    if-nez v0, :cond_1a

    .line 307
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->y:Lcom/google/android/youtube/app/ui/eb;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->i(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 312
    :goto_19
    return-void

    .line 310
    :cond_1a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->x:Lcom/google/android/youtube/app/ui/a;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->k(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/a;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_19
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 320
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 121
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->b()V

    .line 138
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->C:Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->C:Ljava/lang/String;

    const-string v1, "all_activity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 140
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m()V

    .line 145
    :goto_16
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 146
    return-void

    .line 142
    :cond_1c
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->n()V

    goto :goto_16
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->c()V

    .line 296
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->a(Lcom/google/android/youtube/app/prefetch/f;)V

    .line 297
    return-void
.end method

.method public final d()Ljava/lang/String;
    .registers 3

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b0180

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g_()V
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->A:Lcom/google/android/youtube/app/adapter/bt;

    if-nez v0, :cond_a

    .line 360
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->B:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 364
    :goto_9
    return-void

    .line 362
    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->A:Lcom/google/android/youtube/app/adapter/bt;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    goto :goto_9
.end method

.method public final h()V
    .registers 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->C:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 152
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->C:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->C:Ljava/lang/String;

    const-string v1, "all_activity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 154
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->m()V

    .line 158
    :goto_1f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bn;)V

    .line 160
    :cond_24
    return-void

    .line 156
    :cond_25
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->n()V

    goto :goto_1f
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 301
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/u;->i()V

    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->h:Lcom/google/android/youtube/app/prefetch/d;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/prefetch/d;->b(Lcom/google/android/youtube/app/prefetch/f;)V

    .line 303
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 316
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bw;->s:Lcom/google/android/youtube/app/honeycomb/phone/bz;

    invoke-interface {v0}, Lcom/google/android/youtube/app/honeycomb/phone/bz;->a()V

    .line 373
    return-void
.end method
