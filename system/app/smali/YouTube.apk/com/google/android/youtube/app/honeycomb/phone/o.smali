.class public final Lcom/google/android/youtube/app/honeycomb/phone/o;
.super Lcom/google/android/youtube/app/honeycomb/phone/bt;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/ui/cr;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/g;

.field private B:Lcom/google/android/youtube/app/ui/a;

.field private C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private final D:I

.field private final E:I

.field private final F:Lcom/google/android/youtube/app/ui/cx;

.field private G:Z

.field private final b:Landroid/net/Uri;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/content/res/Resources;

.field private final e:Lcom/google/android/youtube/core/b/an;

.field private final f:Lcom/google/android/youtube/core/b/ap;

.field private final g:Lcom/google/android/youtube/core/d;

.field private final h:Lcom/google/android/youtube/app/prefetch/d;

.field private final i:Lcom/google/android/youtube/core/j;

.field private final j:Lcom/google/android/youtube/core/utils/l;

.field private final k:Lcom/google/android/youtube/core/b/al;

.field private final l:Lcom/google/android/youtube/core/async/av;

.field private final m:Lcom/google/android/youtube/core/Analytics;

.field private final n:Lcom/google/android/youtube/app/a;

.field private final o:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private p:Lcom/google/android/youtube/app/ui/by;

.field private q:Lcom/google/android/youtube/app/ui/by;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/ProgressBar;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Lcom/google/android/youtube/core/model/UserProfile;

.field private w:Landroid/view/View;

.field private x:Lcom/google/android/youtube/app/ui/g;

.field private y:Lcom/google/android/youtube/app/ui/eb;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/net/Uri;Lcom/google/android/youtube/app/ui/cx;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bt;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 108
    const-string v0, "channeluri cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->b:Landroid/net/Uri;

    .line 109
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->c:Landroid/view/LayoutInflater;

    .line 111
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 112
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    .line 113
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->e:Lcom/google/android/youtube/core/b/an;

    .line 114
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->f:Lcom/google/android/youtube/core/b/ap;

    .line 115
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->j:Lcom/google/android/youtube/core/utils/l;

    .line 116
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/app/prefetch/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->h:Lcom/google/android/youtube/app/prefetch/d;

    .line 117
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->i:Lcom/google/android/youtube/core/j;

    .line 118
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    .line 119
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->k()Lcom/google/android/youtube/core/async/av;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->l:Lcom/google/android/youtube/core/async/av;

    .line 120
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->g:Lcom/google/android/youtube/core/d;

    .line 121
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->m:Lcom/google/android/youtube/core/Analytics;

    .line 122
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->n:Lcom/google/android/youtube/app/a;

    .line 123
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 124
    new-instance v0, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->m:Lcom/google/android/youtube/core/Analytics;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->g:Lcom/google/android/youtube/core/d;

    const-string v7, "ChannelLayer"

    move-object v1, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cr;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    .line 127
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->D:I

    .line 128
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->E:I

    .line 130
    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->F:Lcom/google/android/youtube/app/ui/cx;

    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->G:Z

    .line 132
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/util/Pair;
    .registers 8
    .parameter

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f04003b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 256
    new-instance v0, Lcom/google/android/youtube/app/ui/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->e:Lcom/google/android/youtube/core/b/an;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->g:Lcom/google/android/youtube/core/d;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/d;)V

    .line 259
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v1, :cond_20

    .line 260
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/g;->a(Lcom/google/android/youtube/core/model/UserProfile;)V

    .line 262
    :cond_20
    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->m:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/o;Lcom/google/android/youtube/core/model/UserProfile;)Lcom/google/android/youtube/core/model/UserProfile;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 266
    const v0, 0x7f080068

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    .line 267
    const v0, 0x7f0800ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    .line 268
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_24

    .line 269
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/youtube/app/ui/dh;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 271
    :cond_24
    const v0, 0x7f08005b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    .line 272
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->s()V

    .line 274
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/o;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/o;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/app/a;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->n:Lcom/google/android/youtube/app/a;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_18

    .line 286
    const v0, 0x7f0800af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->u:Landroid/view/View;

    .line 287
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->u:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 288
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    :cond_18
    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/core/model/UserProfile;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/app/ui/eb;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->y:Lcom/google/android/youtube/app/ui/eb;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/o;)V
    .registers 1
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->q()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/phone/o;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->w:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/app/ui/g;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->x:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/app/ui/a;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->B:Lcom/google/android/youtube/app/ui/a;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/honeycomb/phone/o;)V
    .registers 1
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->r()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/youtube/app/honeycomb/phone/o;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->z:Landroid/view/View;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/honeycomb/phone/o;)Lcom/google/android/youtube/app/ui/g;
    .registers 2
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->A:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method private p()V
    .registers 6

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/phone/r;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/r;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/o;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 144
    return-void
.end method

.method private q()V
    .registers 6

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->y:Lcom/google/android/youtube/app/ui/eb;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/UserProfile;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 192
    return-void
.end method

.method private r()V
    .registers 6

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->B:Lcom/google/android/youtube/app/ui/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/UserProfile;->activityUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/a;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 251
    return-void
.end method

.method private s()V
    .registers 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 335
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    .line 336
    sget-object v1, Lcom/google/android/youtube/app/honeycomb/phone/q;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_9a

    .line 379
    :cond_15
    :goto_15
    return-void

    .line 338
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    if-eqz v0, :cond_1f

    .line 339
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 341
    :cond_1f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_3f

    .line 342
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->D:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 343
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    const v1, 0x7f0b0195

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 346
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 348
    :cond_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_15

    .line 349
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_15

    .line 353
    :pswitch_49
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    if-eqz v0, :cond_52

    .line 354
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 356
    :cond_52
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_72

    .line 357
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->E:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 358
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    const v1, 0x7f0b018b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 359
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    const v1, 0x7f020084

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 361
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 363
    :cond_72
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_15

    .line 364
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_15

    .line 368
    :pswitch_7c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    if-eqz v0, :cond_85

    .line 369
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 371
    :cond_85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_8f

    .line 372
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->r:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 374
    :cond_8f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_15

    .line 375
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_15

    .line 336
    :pswitch_data_9a
    .packed-switch 0x1
        :pswitch_16
        :pswitch_49
        :pswitch_7c
    .end packed-switch
.end method

.method private t()V
    .registers 3

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 417
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->p:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_16

    .line 418
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->p:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 420
    :cond_16
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->q:Lcom/google/android/youtube/app/ui/by;

    if-eqz v1, :cond_1f

    .line 421
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->q:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 423
    :cond_1f
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/core/ui/PagedListView;)Lcom/google/android/youtube/core/a/a;
    .registers 15
    .parameter

    .prologue
    const v9, 0x7f0a005d

    const/4 v6, 0x0

    .line 148
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->e:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->f:Lcom/google/android/youtube/core/b/ap;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->j:Lcom/google/android/youtube/core/utils/l;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->h:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->i:Lcom/google/android/youtube/core/j;

    new-instance v7, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v7, v1}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    invoke-interface {v5}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/youtube/app/adapter/ct;->a(Landroid/graphics/Typeface;)V

    invoke-static {v1, v0, v4}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v8

    new-instance v0, Lcom/google/android/youtube/app/adapter/h;

    invoke-interface {v5, v1}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v4

    const/16 v5, 0x8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v7}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v12, Lcom/google/android/youtube/app/adapter/bt;

    const v2, 0x7f04002b

    invoke-direct {v12, v1, v2, v0}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 155
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/o;->a(Landroid/view/ViewGroup;)Landroid/util/Pair;

    move-result-object v1

    .line 156
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->w:Landroid/view/View;

    .line 157
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->x:Lcom/google/android/youtube/app/ui/g;

    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->w:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    .line 160
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v0, v1, v12}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->p:Lcom/google/android/youtube/app/ui/by;

    .line 161
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->p:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->t()V

    .line 168
    new-instance v0, Lcom/google/android/youtube/app/ui/eb;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->p:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->l:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->g:Lcom/google/android/youtube/core/d;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->n:Lcom/google/android/youtube/app/a;

    sget-object v9, Lcom/google/android/youtube/app/m;->c:Lcom/google/android/youtube/core/b/aq;

    iget-object v10, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->m:Lcom/google/android/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v2, p1

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->y:Lcom/google/android/youtube/app/ui/eb;

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_ad

    .line 182
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->q()V

    .line 186
    :goto_ac
    return-object v12

    .line 184
    :cond_ad
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->y:Lcom/google/android/youtube/app/ui/eb;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eb;->e()V

    goto :goto_ac
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 411
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bt;->a(Landroid/content/res/Configuration;)V

    .line 412
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->t()V

    .line 413
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 383
    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-eq p1, v1, :cond_9

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v1, :cond_e

    .line 384
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->G:Z

    .line 385
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->G:Z

    move v0, v1

    .line 387
    :cond_e
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->s()V

    .line 388
    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v1, :cond_16

    .line 398
    :cond_15
    :goto_15
    return-void

    .line 391
    :cond_16
    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->F:Lcom/google/android/youtube/app/ui/cx;

    if-eqz v0, :cond_15

    .line 392
    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v0, :cond_2c

    .line 393
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->F:Lcom/google/android/youtube/app/ui/cx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d()Lcom/google/android/youtube/core/model/Subscription;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cx;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    goto :goto_15

    .line 394
    :cond_2c
    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne p1, v0, :cond_15

    .line 395
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->F:Lcom/google/android/youtube/app/ui/cx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cx;->a(Landroid/net/Uri;)V

    goto :goto_15
.end method

.method protected final b(Lcom/google/android/youtube/core/ui/PagedListView;)Lcom/google/android/youtube/core/a/a;
    .registers 12
    .parameter

    .prologue
    const v9, 0x7f0a005d

    .line 196
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->e:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->f:Lcom/google/android/youtube/core/b/ap;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->j:Lcom/google/android/youtube/core/utils/l;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->h:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->i:Lcom/google/android/youtube/core/j;

    new-instance v7, Lcom/google/android/youtube/app/adapter/ct;

    invoke-direct {v7, v1}, Lcom/google/android/youtube/app/adapter/ct;-><init>(Landroid/content/Context;)V

    invoke-interface {v5}, Lcom/google/android/youtube/core/j;->a()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/google/android/youtube/app/adapter/ct;->a(Landroid/graphics/Typeface;)V

    invoke-static {v1, v0, v4}, Lcom/google/android/youtube/app/adapter/cv;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/utils/l;)Lcom/google/android/youtube/app/adapter/cv;

    move-result-object v8

    new-instance v0, Lcom/google/android/youtube/app/adapter/h;

    invoke-interface {v5, v1}, Lcom/google/android/youtube/core/j;->c(Landroid/content/Context;)Z

    move-result v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/adapter/h;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/app/prefetch/d;ZIZ)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/ah;

    invoke-direct {v2}, Lcom/google/android/youtube/app/adapter/ah;-><init>()V

    invoke-virtual {v2, v7}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/ah;->a(Lcom/google/android/youtube/app/adapter/cb;)Lcom/google/android/youtube/app/adapter/ah;

    move-result-object v0

    new-instance v2, Lcom/google/android/youtube/app/adapter/ap;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/google/android/youtube/app/adapter/ap;-><init>(Landroid/content/res/Resources;Lcom/google/android/youtube/app/adapter/cb;)V

    new-instance v8, Lcom/google/android/youtube/app/adapter/bt;

    const v0, 0x7f040028

    invoke-direct {v8, v1, v0, v2}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 203
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/o;->a(Landroid/view/ViewGroup;)Landroid/util/Pair;

    move-result-object v1

    .line 204
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->z:Landroid/view/View;

    .line 205
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/ui/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->A:Lcom/google/android/youtube/app/ui/g;

    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->z:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    .line 208
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v0, v1, v8}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->q:Lcom/google/android/youtube/app/ui/by;

    .line 209
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->q:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->d:Landroid/content/res/Resources;

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 215
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->t()V

    .line 217
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/p;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->q:Lcom/google/android/youtube/app/ui/by;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->k:Lcom/google/android/youtube/core/b/al;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->g:Lcom/google/android/youtube/core/d;

    const/4 v7, 0x1

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/honeycomb/phone/p;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/o;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Z)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->B:Lcom/google/android/youtube/app/ui/a;

    .line 240
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_af

    .line 241
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->r()V

    .line 245
    :goto_ae
    return-object v8

    .line 243
    :cond_af
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->B:Lcom/google/android/youtube/app/ui/a;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/a;->e()V

    goto :goto_ae
.end method

.method public final b()V
    .registers 1

    .prologue
    .line 136
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bt;->b()V

    .line 137
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->p()V

    .line 138
    return-void
.end method

.method public final l()V
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_a

    .line 278
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a()V

    .line 282
    :goto_9
    return-void

    .line 280
    :cond_a
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->p()V

    goto :goto_9
.end method

.method protected final m()V
    .registers 2

    .prologue
    .line 294
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bt;->m()V

    .line 295
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->w:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->a(Landroid/view/View;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->w:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->b(Landroid/view/View;)V

    .line 297
    return-void
.end method

.method protected final n()V
    .registers 2

    .prologue
    .line 301
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bt;->n()V

    .line 302
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->z:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->a(Landroid/view/View;)V

    .line 303
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->z:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/o;->b(Landroid/view/View;)V

    .line 304
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->t:Landroid/view/View;

    if-ne p1, v0, :cond_d

    .line 402
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->G:Z

    .line 403
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->C:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    .line 407
    :cond_c
    :goto_c
    return-void

    .line 404
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->u:Landroid/view/View;

    if-ne p1, v0, :cond_c

    .line 405
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->n:Lcom/google/android/youtube/app/a;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/o;->v:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;)V

    goto :goto_c
.end method
