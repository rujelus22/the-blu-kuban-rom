.class public abstract Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/i;
.implements Lcom/google/android/youtube/app/compat/j;
.implements Lcom/google/android/youtube/app/remote/ae;
.implements Lcom/google/android/youtube/core/async/bo;


# instance fields
.field private A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

.field private B:Z

.field private C:Lcom/google/android/youtube/app/compat/o;

.field private D:Lcom/google/android/youtube/app/honeycomb/ui/p;

.field private m:Lcom/google/android/youtube/app/YouTubeApplication;

.field private n:Landroid/content/SharedPreferences;

.field private o:Lcom/google/android/youtube/core/Analytics;

.field private p:Lcom/google/android/youtube/app/a;

.field private q:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private r:Lcom/google/android/youtube/app/compat/r;

.field private s:Lcom/google/android/youtube/app/compat/m;

.field private t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

.field private u:Z

.field private v:Lcom/google/android/youtube/app/ui/ap;

.field private w:Z

.field private x:Lcom/google/android/youtube/app/ui/bx;

.field private y:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private z:Lcom/google/android/youtube/app/remote/ab;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Z

    return v0
.end method

.method private i()V
    .registers 3

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/youtube/app/compat/f;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/compat/f;-><init>(Landroid/content/Context;)V

    .line 147
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 148
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 149
    return-void
.end method


# virtual methods
.method protected final A()Lcom/google/android/youtube/app/honeycomb/ui/p;
    .registers 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    return-object v0
.end method

.method protected final B()Lcom/google/android/youtube/app/honeycomb/phone/ba;
    .registers 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    return-object v0
.end method

.method protected final C()V
    .registers 3

    .prologue
    .line 525
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_12

    .line 526
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 530
    :goto_11
    return-void

    .line 528
    :cond_12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Z

    goto :goto_11
.end method

.method public a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 493
    return-void
.end method

.method protected a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 2
    .parameter

    .prologue
    .line 296
    return-void
.end method

.method public a(Lcom/google/android/youtube/app/remote/RemoteControl;)V
    .registers 3
    .parameter

    .prologue
    .line 507
    if-eqz p1, :cond_11

    .line 508
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bx;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 509
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    if-eqz v0, :cond_10

    .line 510
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 518
    :cond_10
    :goto_10
    return-void

    .line 513
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->a()V

    .line 514
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    if-eqz v0, :cond_10

    .line 515
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a()V

    goto :goto_10
.end method

.method protected final a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x8

    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 234
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    .line 235
    return-void
.end method

.method protected a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    .line 322
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 323
    return v0
.end method

.method public a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    return v0
.end method

.method protected b(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 441
    const/4 v0, 0x0

    return-object v0
.end method

.method protected b(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 3
    .parameter

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->e()V

    .line 340
    const/4 v0, 0x1

    return v0
.end method

.method public b_()Z
    .registers 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/app/a;

    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->a()V

    .line 358
    const/4 v0, 0x1

    return v0
.end method

.method public final c_()Z
    .registers 2

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->B:Z

    return v0
.end method

.method public final e()I
    .registers 2

    .prologue
    .line 537
    const v0, 0x7f080154

    return v0
.end method

.method protected final e(I)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x8

    .line 226
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 228
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(I)V

    .line 229
    return-void
.end method

.method public final f()Lcom/google/android/youtube/app/compat/SupportActionBar;
    .registers 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    if-nez v0, :cond_a

    .line 220
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    .line 222
    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    return-object v0
.end method

.method public final f_()V
    .registers 3

    .prologue
    .line 204
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->f_()V

    .line 205
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1a

    .line 206
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Z

    if-nez v0, :cond_1a

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->u:Z

    .line 208
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/co;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/co;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 216
    :cond_1a
    return-void
.end method

.method protected abstract g()Ljava/lang/String;
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .registers 2

    .prologue
    .line 301
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected h()Z
    .registers 2

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 450
    :goto_c
    return-void

    .line 449
    :cond_d
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    goto :goto_c
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 542
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/o;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 544
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/o;->a()V

    .line 546
    :cond_14
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 89
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setRequestedOrientation(I)V

    .line 93
    :cond_c
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setDefaultKeyMode(I)V

    .line 95
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setVolumeControlStream(I)V

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w:Z

    .line 98
    const v0, 0x7f0400b0

    invoke-super {p0, v0}, Landroid/support/v4/app/FragmentActivity;->setContentView(I)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->f()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->h()Z

    move-result v0

    if-nez v0, :cond_2f

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 106
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/youtube/core/Analytics;

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->n:Landroid/content/SharedPreferences;

    .line 111
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bn;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bn;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/app/a;

    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/app/a;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    .line 115
    new-instance v0, Lcom/google/android/youtube/app/ui/ap;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/ap;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/youtube/app/ui/ap;

    .line 116
    new-instance v0, Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->q()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/youtube/app/ui/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 120
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bo;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/app/remote/ab;

    .line 123
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->c()V

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->b(Z)V

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->n()Z

    move-result v0

    if-eqz v0, :cond_db

    .line 130
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/p;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/app/a;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/youtube/core/Analytics;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/ui/p;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/Analytics;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    .line 134
    :cond_db
    return-void
.end method

.method protected final onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 392
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 401
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 411
    :goto_7
    return-object v0

    .line 404
    :cond_8
    sparse-switch p1, :sswitch_data_32

    .line 411
    if-eqz p2, :cond_2c

    packed-switch p1, :pswitch_data_3c

    goto :goto_7

    :pswitch_11
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->m:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/app/g;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 406
    :sswitch_1c
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/o;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    .line 407
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    goto :goto_7

    .line 409
    :sswitch_25
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->v:Lcom/google/android/youtube/app/ui/ap;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ap;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 411
    :cond_2c
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 404
    nop

    :sswitch_data_32
    .sparse-switch
        0x3f1 -> :sswitch_25
        0x405 -> :sswitch_1c
    .end sparse-switch

    .line 411
    :pswitch_data_3c
    .packed-switch 0x3eb
        :pswitch_11
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    .line 313
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 314
    new-instance v0, Lcom/google/android/youtube/app/compat/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/youtube/app/compat/m;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    .line 315
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    .line 316
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 317
    return v0
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->y:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->b(Lcom/google/android/youtube/core/async/bo;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->A:Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;->d()V

    .line 155
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 156
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 458
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/ui/bx;->b(I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 467
    :goto_9
    return v0

    .line 461
    :cond_a
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v1

    const/16 v2, 0xb

    if-gt v1, v2, :cond_28

    const/16 v1, 0x52

    if-ne p1, v1, :cond_28

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_28

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_28

    .line 464
    const/16 v1, 0x405

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->showDialog(I)V

    goto :goto_9

    .line 467
    :cond_28
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bx;->c(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 473
    const/4 v0, 0x1

    .line 475
    :goto_9
    return v0

    :cond_a
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_9
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 345
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1b

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_12
    if-eqz v0, :cond_1b

    .line 346
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b_()Z

    move-result v0

    .line 349
    :goto_18
    return v0

    .line 345
    :cond_19
    const/4 v0, 0x0

    goto :goto_12

    .line 348
    :cond_1b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    .line 349
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_18
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w:Z

    if-eqz v0, :cond_9

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->c()V

    .line 167
    :cond_9
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 168
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 139
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-ge v0, v1, :cond_e

    .line 140
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->i()V

    .line 142
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->f_()V

    .line 143
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 4
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 418
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 419
    packed-switch p1, :pswitch_data_14

    .line 429
    :cond_6
    :goto_6
    return-void

    .line 424
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    if-eqz v0, :cond_6

    .line 425
    check-cast p2, Lcom/google/android/youtube/app/compat/o;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p2, v0}, Lcom/google/android/youtube/app/compat/o;->a(Lcom/google/android/youtube/app/compat/m;)V

    goto :goto_6

    .line 419
    nop

    :pswitch_data_14
    .packed-switch 0x405
        :pswitch_7
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    .line 328
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 329
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 330
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 332
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    if-eqz v1, :cond_27

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/compat/o;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 333
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->C:Lcom/google/android/youtube/app/compat/o;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->s:Lcom/google/android/youtube/app/compat/m;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/compat/o;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 335
    :cond_27
    return v0
.end method

.method protected onResume()V
    .registers 3

    .prologue
    .line 172
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public onSearchRequested()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 369
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v1

    const/16 v2, 0xb

    if-lt v1, v2, :cond_f

    .line 370
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Z)V

    .line 373
    :goto_e
    return v0

    :cond_f
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_e
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 178
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 179
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->c()V

    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->a(Lcom/google/android/youtube/app/remote/ae;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    .line 182
    if-eqz v0, :cond_24

    .line 183
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 184
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    if-eqz v1, :cond_23

    .line 185
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    .line 193
    :cond_23
    :goto_23
    return-void

    .line 188
    :cond_24
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    if-eqz v0, :cond_2d

    .line 189
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->D:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a()V

    .line 191
    :cond_2d
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->x:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bx;->a()V

    goto :goto_23
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ab;->d()V

    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->z:Lcom/google/android/youtube/app/remote/ab;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ab;->b(Lcom/google/android/youtube/app/remote/ae;)V

    .line 199
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 200
    return-void
.end method

.method public final setContentView(I)V
    .registers 4
    .parameter

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080154

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 262
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 264
    return-void
.end method

.method public final setContentView(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 268
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 273
    return-void
.end method

.method public final setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080154

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 279
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 280
    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 281
    return-void
.end method

.method protected final u()V
    .registers 4

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->q:Lcom/google/android/youtube/app/compat/SupportActionBar;

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 240
    return-void
.end method

.method public final v()Lcom/google/android/youtube/app/compat/r;
    .registers 3

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/r;

    if-nez v0, :cond_f

    .line 306
    new-instance v0, Lcom/google/android/youtube/app/compat/r;

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/compat/r;-><init>(Landroid/content/Context;Landroid/view/MenuInflater;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/r;

    .line 308
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->r:Lcom/google/android/youtube/app/compat/r;

    return-object v0
.end method

.method protected final w()Lcom/google/android/youtube/app/a;
    .registers 2

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->p:Lcom/google/android/youtube/app/a;

    return-object v0
.end method

.method protected final x()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
    .registers 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->t:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    return-object v0
.end method

.method public final y()Lcom/google/android/youtube/core/Analytics;
    .registers 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->o:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method public z()V
    .registers 1

    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->finish()V

    .line 500
    return-void
.end method
