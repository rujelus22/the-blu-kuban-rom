.class public final Lcom/google/android/youtube/app/honeycomb/tablet/i;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/cr;
.implements Lcom/google/android/youtube/app/ui/e;


# instance fields
.field a:Lcom/google/android/youtube/app/ui/am;

.field private h:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private i:Lcom/google/android/youtube/core/Analytics;

.field private j:Lcom/google/android/youtube/core/d;

.field private final k:Lcom/google/android/youtube/core/b/al;

.field private l:Ljava/lang/String;

.field private m:Lcom/google/android/youtube/core/model/UserProfile;

.field private n:Lcom/google/android/youtube/app/ui/dw;

.field private o:Lcom/google/android/youtube/app/ui/dw;

.field private p:Lcom/google/android/youtube/core/ui/j;

.field private q:Lcom/google/android/youtube/app/ui/d;

.field private r:Lcom/google/android/youtube/app/ui/g;

.field private s:Lcom/google/android/youtube/app/compat/t;

.field private t:Lcom/google/android/youtube/app/ui/SubscribeHelper;

.field private u:Lcom/google/android/youtube/core/model/Playlist;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 110
    const-string v5, "yt_channel"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/i;)Lcom/google/android/youtube/app/ui/g;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->r:Lcom/google/android/youtube/app/ui/g;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/i;Lcom/google/android/youtube/core/model/UserProfile;)Lcom/google/android/youtube/core/model/UserProfile;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->m:Lcom/google/android/youtube/core/model/UserProfile;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/tablet/i;)Lcom/google/android/youtube/app/ui/SubscribeHelper;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->t:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/tablet/i;)Lcom/google/android/youtube/app/ui/d;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->q:Lcom/google/android/youtube/app/ui/d;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/tablet/i;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/tablet/i;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    return-object v0
.end method

.method private f()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 299
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->t:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    move-result-object v0

    .line 300
    sget-object v1, Lcom/google/android/youtube/app/honeycomb/tablet/k;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_46

    .line 316
    :goto_12
    return-void

    .line 302
    :pswitch_13
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    const v1, 0x7f0b018c

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->d(I)Lcom/google/android/youtube/app/compat/t;

    .line 303
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    const v1, 0x7f02020f

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->b(I)Lcom/google/android/youtube/app/compat/t;

    .line 304
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    goto :goto_12

    .line 308
    :pswitch_29
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    const v1, 0x7f0b018b

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->d(I)Lcom/google/android/youtube/app/compat/t;

    .line 309
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    const v1, 0x7f02020b

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->b(I)Lcom/google/android/youtube/app/compat/t;

    .line 310
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    goto :goto_12

    .line 313
    :pswitch_3f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    goto :goto_12

    .line 300
    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_13
        :pswitch_29
        :pswitch_29
        :pswitch_3f
    .end packed-switch
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 116
    const v0, 0x7f0400d7

    return v0
.end method

.method public final a(I)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->m:Lcom/google/android/youtube/core/model/UserProfile;

    if-nez v0, :cond_7

    .line 259
    :goto_6
    return-void

    .line 244
    :cond_7
    packed-switch p1, :pswitch_data_66

    goto :goto_6

    .line 246
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->o:Lcom/google/android/youtube/app/ui/dw;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->d(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 249
    :pswitch_21
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->n:Lcom/google/android/youtube/app/ui/dw;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->e(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 252
    :pswitch_37
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->a:Lcom/google/android/youtube/app/ui/am;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Ljava/lang/String;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/am;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 255
    :pswitch_4d
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->p:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    const/16 v4, 0x17

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_6

    .line 244
    nop

    :pswitch_data_66
    .packed-switch 0x7f080056
        :pswitch_b
        :pswitch_21
        :pswitch_37
        :pswitch_4d
    .end packed-switch
.end method

.method protected final a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 226
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/os/Bundle;)V

    .line 227
    const-string v0, "selected_card_id"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->q:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/d;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 228
    const-string v0, "selected_playlist"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->u:Lcom/google/android/youtube/core/model/Playlist;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 229
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 23
    .parameter
    .parameter

    .prologue
    .line 121
    invoke-super/range {p0 .. p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    const v2, 0x7f0b0217

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setTitle(I)V

    .line 123
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->h:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 124
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->i:Lcom/google/android/youtube/core/Analytics;

    .line 125
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v14

    .line 128
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->k()Lcom/google/android/youtube/core/async/av;

    move-result-object v15

    .line 129
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->p()Lcom/google/android/youtube/core/async/av;

    move-result-object v16

    .line 130
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v1}, Lcom/google/android/youtube/core/b/al;->o()Lcom/google/android/youtube/core/async/av;

    move-result-object v17

    .line 132
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v18

    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/tablet/i;->r()Landroid/os/Bundle;

    move-result-object v19

    .line 138
    const-string v1, "username"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    .line 140
    new-instance v1, Lcom/google/android/youtube/app/ui/SubscribeHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->i:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->h:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    const-string v8, "ChannelController"

    move-object/from16 v7, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/app/ui/SubscribeHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/cr;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->t:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    .line 143
    new-instance v1, Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    move-object/from16 v3, p1

    move-object v5, v14

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/app/ui/g;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->r:Lcom/google/android/youtube/app/ui/g;

    .line 144
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->r:Lcom/google/android/youtube/app/ui/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/g;->a(Ljava/lang/String;)V

    .line 146
    const v1, 0x7f080056

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 147
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 148
    new-instance v1, Lcom/google/android/youtube/app/ui/dw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-static {v5}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v5

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->i:Lcom/google/android/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v12, Lcom/google/android/youtube/app/m;->G:Lcom/google/android/youtube/core/b/aq;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    move-object v6, v15

    move-object v7, v14

    move-object/from16 v8, v18

    invoke-direct/range {v1 .. v13}, Lcom/google/android/youtube/app/ui/dw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;ZLcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->o:Lcom/google/android/youtube/app/ui/dw;

    .line 161
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->o:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dw;->d()V

    .line 163
    const v1, 0x7f080057

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 164
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v4, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 165
    new-instance v1, Lcom/google/android/youtube/app/ui/dw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-static {v5}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v5

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->i:Lcom/google/android/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelFavorites:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v12, Lcom/google/android/youtube/app/m;->H:Lcom/google/android/youtube/core/b/aq;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    move-object/from16 v6, v16

    move-object v7, v14

    move-object/from16 v8, v18

    invoke-direct/range {v1 .. v13}, Lcom/google/android/youtube/app/ui/dw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;ZLcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->n:Lcom/google/android/youtube/app/ui/dw;

    .line 178
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->n:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dw;->d()V

    .line 180
    new-instance v1, Lcom/google/android/youtube/app/ui/am;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->d:Lcom/google/android/youtube/app/a;

    const v4, 0x7f080058

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedListView;

    new-instance v5, Lcom/google/android/youtube/app/adapter/an;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lcom/google/android/youtube/app/adapter/an;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->i:Lcom/google/android/youtube/core/Analytics;

    sget-object v9, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ChannelActivity:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v10, Lcom/google/android/youtube/app/m;->F:Lcom/google/android/youtube/core/b/aq;

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/youtube/app/ui/am;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->a:Lcom/google/android/youtube/app/ui/am;

    .line 191
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->a:Lcom/google/android/youtube/app/ui/am;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/am;->d()V

    .line 193
    new-instance v4, Lcom/google/android/youtube/app/adapter/bi;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-direct {v4, v1}, Lcom/google/android/youtube/app/adapter/bi;-><init>(Landroid/content/Context;)V

    .line 194
    const v1, 0x7f080059

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 195
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->c(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 196
    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/ad;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->d:Lcom/google/android/youtube/app/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v5}, Lcom/google/android/youtube/core/b/al;->h()Lcom/google/android/youtube/core/async/av;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->j:Lcom/google/android/youtube/core/d;

    move-object/from16 v5, v17

    move-object v7, v14

    invoke-direct/range {v1 .. v8}, Lcom/google/android/youtube/app/honeycomb/tablet/ad;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/bi;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/d;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->p:Lcom/google/android/youtube/core/ui/j;

    .line 205
    new-instance v1, Lcom/google/android/youtube/app/honeycomb/tablet/j;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v4}, Lcom/google/android/youtube/app/honeycomb/tablet/j;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/i;Lcom/google/android/youtube/app/adapter/bi;)V

    invoke-interface {v3, v1}, Lcom/google/android/youtube/core/ui/g;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->p:Lcom/google/android/youtube/core/ui/j;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/ui/j;->d()V

    .line 208
    const v1, 0x7f080056

    .line 209
    if-eqz p2, :cond_213

    .line 210
    const-string v1, "selected_card_id"

    const v2, 0x7f080056

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 211
    const-string v1, "selected_playlist"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Playlist;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->u:Lcom/google/android/youtube/core/model/Playlist;

    move v3, v2

    .line 216
    :goto_1e8
    const v1, 0x7f0800bd

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const v4, 0x7f0400e2

    const v2, 0x7f0800bb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    invoke-static {v1, v4, v2, v3}, Lcom/google/android/youtube/app/ui/d;->a(Landroid/widget/ListView;ILandroid/widget/FrameLayout;I)Lcom/google/android/youtube/app/ui/d;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->q:Lcom/google/android/youtube/app/ui/d;

    .line 221
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->q:Lcom/google/android/youtube/app/ui/d;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/d;->a(Lcom/google/android/youtube/app/ui/e;)V

    .line 222
    return-void

    .line 213
    :cond_213
    if-eqz v19, :cond_222

    .line 214
    const-string v1, "selected_card_id"

    const v2, 0x7f080056

    move-object/from16 v0, v19

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    move v3, v1

    goto :goto_1e8

    :cond_222
    move v3, v1

    goto :goto_1e8
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 3
    .parameter

    .prologue
    .line 293
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;)V

    .line 294
    const v0, 0x7f080199

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    .line 295
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/i;->f()V

    .line 296
    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 287
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V

    .line 288
    const/high16 v0, 0x7f11

    invoke-virtual {p2, v0, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 289
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .registers 3
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->s:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_7

    .line 338
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/i;->f()V

    .line 340
    :cond_7
    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 4
    .parameter

    .prologue
    .line 320
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v0

    const v1, 0x7f080199

    if-ne v0, v1, :cond_10

    .line 321
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->t:Lcom/google/android/youtube/app/ui/SubscribeHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b()V

    .line 322
    const/4 v0, 0x1

    .line 324
    :goto_f
    return v0

    :cond_10
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_f
.end method

.method protected final b()V
    .registers 6

    .prologue
    .line 233
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 235
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->k:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->g:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/tablet/l;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/app/honeycomb/tablet/l;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/i;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->d(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 237
    return-void
.end method

.method public final b(I)V
    .registers 3
    .parameter

    .prologue
    .line 262
    packed-switch p1, :pswitch_data_1c

    .line 276
    :goto_3
    return-void

    .line 264
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->o:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    goto :goto_3

    .line 267
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->n:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    goto :goto_3

    .line 270
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->a:Lcom/google/android/youtube/app/ui/am;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/am;->c()V

    goto :goto_3

    .line 273
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->p:Lcom/google/android/youtube/core/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/ui/j;->c()V

    goto :goto_3

    .line 262
    :pswitch_data_1c
    .packed-switch 0x7f080056
        :pswitch_4
        :pswitch_a
        :pswitch_10
        :pswitch_16
    .end packed-switch
.end method

.method protected final c()V
    .registers 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->q:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/d;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/tablet/i;->b(I)V

    .line 281
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/i;->q:Lcom/google/android/youtube/app/ui/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/d;->notifyDataSetInvalidated()V

    .line 282
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 283
    return-void
.end method
