.class public final Lcom/google/android/youtube/app/compat/o;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Ljava/util/List;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/ListView;

.field private final e:Lcom/google/android/youtube/app/compat/q;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .registers 5
    .parameter

    .prologue
    .line 49
    const v0, 0x7f0c0045

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 50
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    .line 51
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    .line 52
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 53
    const v1, 0x7f0400ad

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->c:Landroid/widget/LinearLayout;

    .line 54
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f080152

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/youtube/app/compat/p;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/compat/p;-><init>(Lcom/google/android/youtube/app/compat/o;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 62
    new-instance v0, Lcom/google/android/youtube/app/compat/q;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-direct {v0, p1, v1}, Lcom/google/android/youtube/app/compat/q;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/o;->e:Lcom/google/android/youtube/app/compat/q;

    .line 63
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/o;->e:Lcom/google/android/youtube/app/compat/q;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/compat/o;->setContentView(Landroid/view/View;)V

    .line 65
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/o;
    .registers 2
    .parameter

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/youtube/app/compat/o;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/compat/o;-><init>(Landroid/app/Activity;)V

    .line 45
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/compat/o;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/compat/o;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    .line 90
    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    const-string v0, "window"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_5b

    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_2c
    int-to-double v2, v0

    const-wide v4, 0x3fe999999999999aL

    mul-double/2addr v2, v4

    double-to-int v0, v2

    const/4 v2, -0x2

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 92
    if-ne v0, v6, :cond_5e

    .line 93
    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 100
    :goto_5a
    return-void

    .line 90
    :cond_5b
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_2c

    .line 96
    :cond_5e
    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x53

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/youtube/app/compat/o;->a:Landroid/app/Activity;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_5a
.end method


# virtual methods
.method public final a()V
    .registers 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/o;->b()V

    .line 87
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .registers 4
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/youtube/app/compat/o;->b()V

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 81
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->b:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/youtube/app/compat/c;->a(Landroid/content/Context;Lcom/google/android/youtube/app/compat/m;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/o;->e:Lcom/google/android/youtube/app/compat/q;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/q;->notifyDataSetChanged()V

    .line 83
    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 69
    const/16 v0, 0x52

    if-ne p1, v0, :cond_15

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_15

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_15

    .line 72
    invoke-virtual {p0}, Lcom/google/android/youtube/app/compat/o;->dismiss()V

    .line 73
    const/4 v0, 0x1

    .line 75
    :goto_14
    return v0

    :cond_15
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_14
.end method
