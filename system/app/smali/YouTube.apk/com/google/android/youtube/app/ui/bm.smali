.class final Lcom/google/android/youtube/app/ui/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/ui/m;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bg;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bg;)V
    .registers 2
    .parameter

    .prologue
    .line 609
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final f()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 611
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->e(Lcom/google/android/youtube/app/ui/bg;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 652
    :goto_a
    return-void

    .line 614
    :cond_b
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v2, v3, :cond_27

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v2, v3, :cond_66

    :cond_27
    move v2, v1

    .line 616
    :goto_28
    if-eqz v2, :cond_7d

    .line 617
    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ERROR:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v2, v3, :cond_46

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->x()Lcom/google/android/youtube/app/remote/al;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/youtube/app/remote/al;->c:Z

    if-eqz v2, :cond_47

    :cond_46
    move v0, v1

    .line 619
    :cond_47
    if-eqz v0, :cond_68

    .line 620
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->k(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteRetryOnError"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 621
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    goto :goto_a

    :cond_66
    move v2, v0

    .line 614
    goto :goto_28

    .line 623
    :cond_68
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->k(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteDisconnectOnFatalError"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 624
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    goto :goto_a

    .line 627
    :cond_7d
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-eq v1, v2, :cond_9a

    .line 628
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl$State;)V

    .line 630
    :cond_9a
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    if-eqz v1, :cond_ba

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c9

    .line 631
    :cond_ba
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/bg;->a(Ljava/lang/String;)V

    .line 634
    :cond_c9
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    if-eqz v1, :cond_f6

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v1

    if-nez v1, :cond_f6

    .line 635
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->f(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 637
    :cond_f6
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->g()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    .line 638
    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_10c

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v1, v2, :cond_10c

    sget-object v2, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v1, v2, :cond_12f

    .line 641
    :cond_10c
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->l(Lcom/google/android/youtube/app/ui/bg;)Z

    move-result v1

    if-eqz v1, :cond_124

    .line 642
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->f()V

    .line 643
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/ui/bg;Z)Z

    goto/16 :goto_a

    .line 645
    :cond_124
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bg;->g(Lcom/google/android/youtube/app/ui/bg;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->c()V

    goto/16 :goto_a

    .line 648
    :cond_12f
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bm;->a:Lcom/google/android/youtube/app/ui/bg;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    goto/16 :goto_a
.end method
