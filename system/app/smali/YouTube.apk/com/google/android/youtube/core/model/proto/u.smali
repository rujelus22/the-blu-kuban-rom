.class public final Lcom/google/android/youtube/core/model/proto/u;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/v;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;

.field private e:Ljava/lang/Object;

.field private f:Ljava/lang/Object;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 720
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 903
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->b:Ljava/lang/Object;

    .line 977
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->c:Ljava/lang/Object;

    .line 1051
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->d:Ljava/lang/Object;

    .line 1125
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->e:Ljava/lang/Object;

    .line 1199
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->f:Ljava/lang/Object;

    .line 721
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/u;
    .registers 1

    .prologue
    .line 715
    new-instance v0, Lcom/google/android/youtube/core/model/proto/u;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/u;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/u;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 887
    const/4 v2, 0x0

    .line 889
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 894
    if-eqz v0, :cond_e

    .line 895
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;

    .line 898
    :cond_e
    return-object p0

    .line 890
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 891
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 892
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 894
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 895
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;

    :cond_21
    throw v0

    .line 894
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/u;
    .registers 3

    .prologue
    .line 754
    new-instance v0, Lcom/google/android/youtube/core/model/proto/u;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/u;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/u;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 770
    new-instance v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 771
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    .line 772
    const/4 v1, 0x0

    .line 773
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7d

    .line 776
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/u;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$302(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1a

    .line 778
    or-int/lit8 v0, v0, 0x2

    .line 780
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/u;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$402(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_26

    .line 782
    or-int/lit8 v0, v0, 0x4

    .line 784
    :cond_26
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/u;->d:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$502(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_33

    .line 786
    or-int/lit8 v0, v0, 0x8

    .line 788
    :cond_33
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/u;->e:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$602(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;

    .line 789
    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_40

    .line 790
    or-int/lit8 v0, v0, 0x10

    .line 792
    :cond_40
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/u;->f:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$702(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4d

    .line 794
    or-int/lit8 v0, v0, 0x20

    .line 796
    :cond_4d
    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/u;->g:Z

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->licensed_:Z
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$802(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Z)Z

    .line 797
    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5a

    .line 798
    or-int/lit8 v0, v0, 0x40

    .line 800
    :cond_5a
    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/u;->h:Z

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->partnerUploaded_:Z
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Z)Z

    .line 801
    and-int/lit16 v1, v3, 0x80

    const/16 v4, 0x80

    if-ne v1, v4, :cond_67

    .line 802
    or-int/lit16 v0, v0, 0x80

    .line 804
    :cond_67
    iget-boolean v1, p0, Lcom/google/android/youtube/core/model/proto/u;->i:Z

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->vevo_:Z
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$1002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;Z)Z

    .line 805
    and-int/lit16 v1, v3, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_74

    .line 806
    or-int/lit16 v0, v0, 0x100

    .line 808
    :cond_74
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/u;->j:I

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->videoLengthInSeconds_:I
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$1102(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;I)I

    .line 809
    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$1202(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;I)I

    .line 810
    return-object v2

    :cond_7d
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Lcom/google/android/youtube/core/model/proto/u;
    .registers 4
    .parameter

    .prologue
    .line 814
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 852
    :cond_6
    :goto_6
    return-object p0

    .line 815
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasEncryptedId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 816
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    .line 817
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->encryptedId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$300(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->b:Ljava/lang/Object;

    .line 820
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasArtistId()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 821
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    .line 822
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$400(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->c:Ljava/lang/Object;

    .line 825
    :cond_2b
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasArtistName()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 826
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    .line 827
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->artistName_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$500(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->d:Ljava/lang/Object;

    .line 830
    :cond_3d
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasTrackId()Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 831
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    .line 832
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$600(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->e:Ljava/lang/Object;

    .line 835
    :cond_4f
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasTrackName()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 836
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    .line 837
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->trackName_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->access$700(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/u;->f:Ljava/lang/Object;

    .line 840
    :cond_61
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasLicensed()Z

    move-result v0

    if-eqz v0, :cond_73

    .line 841
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getLicensed()Z

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/u;->g:Z

    .line 843
    :cond_73
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasPartnerUploaded()Z

    move-result v0

    if-eqz v0, :cond_85

    .line 844
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getPartnerUploaded()Z

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/u;->h:Z

    .line 846
    :cond_85
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasVevo()Z

    move-result v0

    if-eqz v0, :cond_97

    .line 847
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getVevo()Z

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    iput-boolean v0, p0, Lcom/google/android/youtube/core/model/proto/u;->i:Z

    .line 849
    :cond_97
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->hasVideoLengthInSeconds()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 850
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getVideoLengthInSeconds()I

    move-result v0

    iget v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/u;->j:I

    goto/16 :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 715
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 715
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 715
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/u;->g()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/u;->g()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/u;->g()Lcom/google/android/youtube/core/model/proto/u;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/u;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/u;->h()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/u;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 715
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileMusicVideoData;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 856
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 880
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 856
    goto :goto_9

    .line 860
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_45

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    .line 864
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_47

    move v2, v1

    :goto_20
    if-eqz v2, :cond_b

    .line 868
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_49

    move v2, v1

    :goto_2b
    if-eqz v2, :cond_b

    .line 872
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_4b

    move v2, v1

    :goto_36
    if-eqz v2, :cond_b

    .line 876
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/u;->a:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_4d

    move v2, v1

    :goto_41
    if-eqz v2, :cond_b

    move v0, v1

    .line 880
    goto :goto_b

    :cond_45
    move v2, v0

    .line 860
    goto :goto_16

    :cond_47
    move v2, v0

    .line 864
    goto :goto_20

    :cond_49
    move v2, v0

    .line 868
    goto :goto_2b

    :cond_4b
    move v2, v0

    .line 872
    goto :goto_36

    :cond_4d
    move v2, v0

    .line 876
    goto :goto_41
.end method
