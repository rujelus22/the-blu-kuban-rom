.class final Lcom/google/android/youtube/core/transfer/v;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

.field private final b:Landroid/net/ConnectivityManager;

.field private volatile c:Z

.field private volatile d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/transfer/TransfersExecutor;)V
    .registers 4
    .parameter

    .prologue
    .line 580
    iput-object p1, p0, Lcom/google/android/youtube/core/transfer/v;->a:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 581
    invoke-static {p1}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->b(Lcom/google/android/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/youtube/core/transfer/v;->b:Landroid/net/ConnectivityManager;

    .line 582
    return-void
.end method

.method private e()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 611
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/v;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 612
    if-eqz v4, :cond_21

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    move v3, v0

    .line 613
    :goto_f
    if-eqz v3, :cond_25

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_23

    move v0, v1

    .line 614
    :goto_18
    iget-boolean v4, p0, Lcom/google/android/youtube/core/transfer/v;->c:Z

    if-ne v4, v3, :cond_27

    iget-boolean v4, p0, Lcom/google/android/youtube/core/transfer/v;->d:Z

    if-ne v4, v0, :cond_27

    .line 620
    :goto_20
    return v2

    :cond_21
    move v3, v2

    .line 612
    goto :goto_f

    :cond_23
    move v0, v2

    .line 613
    goto :goto_18

    :cond_25
    move v0, v2

    goto :goto_18

    .line 618
    :cond_27
    iput-boolean v3, p0, Lcom/google/android/youtube/core/transfer/v;->c:Z

    .line 619
    iput-boolean v0, p0, Lcom/google/android/youtube/core/transfer/v;->d:Z

    move v2, v1

    .line 620
    goto :goto_20
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 585
    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/v;->c:Z

    return v0
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 589
    iget-boolean v0, p0, Lcom/google/android/youtube/core/transfer/v;->d:Z

    return v0
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 593
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 594
    iget-object v1, p0, Lcom/google/android/youtube/core/transfer/v;->a:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-static {v1}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->b(Lcom/google/android/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 596
    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/v;->e()Z

    .line 597
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/v;->a:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-static {v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->b(Lcom/google/android/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 601
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 605
    invoke-direct {p0}, Lcom/google/android/youtube/core/transfer/v;->e()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 606
    iget-object v0, p0, Lcom/google/android/youtube/core/transfer/v;->a:Lcom/google/android/youtube/core/transfer/TransfersExecutor;

    invoke-static {v0}, Lcom/google/android/youtube/core/transfer/TransfersExecutor;->c(Lcom/google/android/youtube/core/transfer/TransfersExecutor;)V

    .line 608
    :cond_b
    return-void
.end method
