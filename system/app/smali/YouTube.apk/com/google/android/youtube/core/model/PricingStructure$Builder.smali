.class public final Lcom/google/android/youtube/core/model/PricingStructure$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private duration:Lcom/google/android/youtube/core/model/Duration;

.field private formats:Ljava/util/List;

.field private infoUri:Landroid/net/Uri;

.field private offerId:Ljava/lang/String;

.field private price:Lcom/google/android/youtube/core/model/Money;

.field private type:Lcom/google/android/youtube/core/model/PricingStructure$Type;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 222
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/PricingStructure$Type;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    .line 223
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Duration;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    .line 224
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    .line 225
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Money;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    .line 226
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    .line 227
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    .line 228
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->build()Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 218
    return-void
.end method


# virtual methods
.method public final build()Lcom/google/android/youtube/core/model/PricingStructure;
    .registers 6

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    .line 200
    :goto_6
    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    sget-object v2, Lcom/google/android/youtube/core/model/PricingStructure$Type;->RENT:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    if-ne v1, v2, :cond_1e

    .line 201
    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/youtube/core/model/PricingStructure;->createRental(Lcom/google/android/youtube/core/model/Duration;Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    .line 203
    :goto_18
    return-object v0

    .line 199
    :cond_19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_6

    .line 203
    :cond_1e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/youtube/core/model/PricingStructure;->createPurchase(Lcom/google/android/youtube/core/model/Money;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure;

    move-result-object v0

    goto :goto_18
.end method

.method public final duration(Lcom/google/android/youtube/core/model/Duration;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .registers 2
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->duration:Lcom/google/android/youtube/core/model/Duration;

    .line 175
    return-object p0
.end method

.method public final formats(Ljava/util/List;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .registers 2
    .parameter

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->formats:Ljava/util/List;

    .line 190
    return-object p0
.end method

.method public final infoUri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .registers 2
    .parameter

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->infoUri:Landroid/net/Uri;

    .line 180
    return-object p0
.end method

.method public final offerId(Ljava/lang/String;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .registers 2
    .parameter

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->offerId:Ljava/lang/String;

    .line 195
    return-object p0
.end method

.method public final price(Lcom/google/android/youtube/core/model/Money;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .registers 2
    .parameter

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->price:Lcom/google/android/youtube/core/model/Money;

    .line 185
    return-object p0
.end method

.method public final type(Lcom/google/android/youtube/core/model/PricingStructure$Type;)Lcom/google/android/youtube/core/model/PricingStructure$Builder;
    .registers 2
    .parameter

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/android/youtube/core/model/PricingStructure$Builder;->type:Lcom/google/android/youtube/core/model/PricingStructure$Type;

    .line 170
    return-object p0
.end method
