.class public final Lcom/google/android/youtube/app/prefetch/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ax;


# instance fields
.field private final a:Lcom/google/android/youtube/core/player/ax;

.field private final b:Lcom/google/android/youtube/app/prefetch/d;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/player/ax;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/ax;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ax;

    .line 24
    const-string v0, "prefetchStore cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/prefetch/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->b:Lcom/google/android/youtube/app/prefetch/d;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Lcom/google/android/youtube/core/model/Stream$Quality;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ax;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ax;->a(Ljava/util/Set;Z)Lcom/google/android/youtube/core/model/Stream$Quality;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;)Lcom/google/android/youtube/core/model/v;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->b:Lcom/google/android/youtube/app/prefetch/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/prefetch/d;->a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/v;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_b

    .line 33
    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ax;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ax;->a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;)Lcom/google/android/youtube/core/model/v;

    move-result-object v0

    goto :goto_a
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/prefetch/j;->a:Lcom/google/android/youtube/core/player/ax;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ax;->a()Z

    move-result v0

    return v0
.end method
