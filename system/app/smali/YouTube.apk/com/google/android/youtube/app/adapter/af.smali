.class public final Lcom/google/android/youtube/app/adapter/af;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/content/res/Resources;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/adapter/af;-><init>(Landroid/content/Context;I)V

    .line 30
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    .line 33
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/af;->b:Landroid/content/res/Resources;

    .line 35
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/af;->a:Landroid/view/LayoutInflater;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/adapter/af;->c:I

    .line 37
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public final getItemViewType(I)I
    .registers 3
    .parameter

    .prologue
    .line 85
    iget v0, p0, Lcom/google/android/youtube/app/adapter/af;->c:I

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    if-nez p2, :cond_37

    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/af;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f040021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 43
    new-instance v0, Lcom/google/android/youtube/app/adapter/ag;

    invoke-direct {v0, p2}, Lcom/google/android/youtube/app/adapter/ag;-><init>(Landroid/view/View;)V

    .line 44
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 49
    :goto_15
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/af;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Comment;

    .line 50
    iget-object v2, v1, Lcom/google/android/youtube/app/adapter/ag;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Comment;->author:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    iget-object v2, v0, Lcom/google/android/youtube/core/model/Comment;->publishedDate:Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/youtube/app/adapter/af;->b:Landroid/content/res/Resources;

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/utils/ab;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 52
    iget-object v3, v1, Lcom/google/android/youtube/app/adapter/ag;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v1, v1, Lcom/google/android/youtube/app/adapter/ag;->c:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Comment;->content:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-object p2

    .line 47
    :cond_37
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/ag;

    move-object v1, v0

    goto :goto_15
.end method

.method public final isEnabled(I)Z
    .registers 3
    .parameter

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method
