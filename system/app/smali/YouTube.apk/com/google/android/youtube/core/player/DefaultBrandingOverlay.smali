.class public Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/core/player/j;


# instance fields
.field private a:Z

.field private b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 34
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {p0, p0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->d()V

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 41
    return-object p0
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 45
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 46
    const/high16 v1, 0x4170

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 47
    const/high16 v2, 0x428c

    mul-float/2addr v2, v0

    float-to-int v2, v2

    .line 48
    const/high16 v3, 0x4160

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 50
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 52
    invoke-virtual {v3, v4, v4, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 53
    const/16 v0, 0xb

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 54
    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 55
    return-object v3
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->a:Z

    .line 60
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setVisibility(I)V

    .line 63
    :cond_d
    return-void
.end method

.method public final d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->a:Z

    .line 67
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setVisibility(I)V

    .line 68
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    iput-object v1, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->b:Landroid/net/Uri;

    .line 70
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setFocusable(Z)V

    .line 71
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->b:Landroid/net/Uri;

    if-eqz v0, :cond_d

    .line 91
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->b:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/content/Context;Landroid/net/Uri;)V

    .line 93
    :cond_d
    return-void
.end method

.method public setInterstitial(Landroid/graphics/Bitmap;Landroid/net/Uri;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 101
    return-void
.end method

.method public setWatermark(Landroid/graphics/Bitmap;Landroid/net/Uri;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 76
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->a:Z

    if-eqz v0, :cond_23

    move v0, v1

    :goto_9
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setVisibility(I)V

    .line 78
    iput-object p2, p0, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->b:Landroid/net/Uri;

    .line 80
    if-eqz p2, :cond_25

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setFocusable(Z)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0088

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 87
    :goto_22
    return-void

    .line 76
    :cond_23
    const/4 v0, 0x4

    goto :goto_9

    .line 84
    :cond_25
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setFocusable(Z)V

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_22
.end method
