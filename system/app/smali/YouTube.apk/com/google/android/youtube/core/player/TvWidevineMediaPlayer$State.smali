.class final enum Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

.field public static final enum BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

.field public static final enum READY:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    new-instance v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    const-string v1, "BUFFER_FILLING"

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    .line 53
    new-instance v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    const-string v1, "READY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->READY:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->BUFFER_FILLING:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->READY:Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->$VALUES:[Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;
    .registers 2
    .parameter

    .prologue
    .line 51
    const-class v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;
    .registers 1

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->$VALUES:[Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    invoke-virtual {v0}, [Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/core/player/TvWidevineMediaPlayer$State;

    return-object v0
.end method
