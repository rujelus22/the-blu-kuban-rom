.class public Lcom/google/android/youtube/app/adapter/bt;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/youtube/app/adapter/cb;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 40
    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    .line 41
    iput p2, p0, Lcom/google/android/youtube/app/adapter/bt;->a:I

    .line 42
    const-string v0, "renderer can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cb;

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->b:Lcom/google/android/youtube/app/adapter/cb;

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->c:Landroid/view/LayoutInflater;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->e:Ljava/util/List;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/youtube/app/adapter/cf;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 61
    iget v1, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    invoke-interface {p2}, Lcom/google/android/youtube/app/adapter/cf;->a()Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x0

    :goto_e
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    .line 62
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 63
    return p1

    .line 61
    :cond_15
    const/4 v0, 0x1

    goto :goto_e
.end method

.method public final a(Lcom/google/android/youtube/app/adapter/cf;)I
    .registers 3
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 56
    invoke-virtual {p0, v0, p1}, Lcom/google/android/youtube/app/adapter/bt;->a(ILcom/google/android/youtube/app/adapter/cf;)I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/youtube/app/adapter/cf;
    .registers 5
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_23

    .line 72
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cf;

    .line 73
    iget v2, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/cf;->a()Z

    move-result v1

    if-eqz v1, :cond_21

    const/4 v1, 0x0

    :goto_19
    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    .line 74
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 75
    return-object v0

    .line 73
    :cond_21
    const/4 v1, 0x1

    goto :goto_19

    .line 77
    :cond_23
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method protected final a(Ljava/lang/Iterable;)V
    .registers 3
    .parameter

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/youtube/core/a/a;->a(Ljava/lang/Iterable;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->b:Lcom/google/android/youtube/app/adapter/cb;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/adapter/cb;->a(Ljava/lang/Iterable;)V

    .line 52
    return-void
.end method

.method public areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final b(Lcom/google/android/youtube/app/adapter/cf;)I
    .registers 5
    .parameter

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bt;->getCount()I

    move-result v1

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    iget v2, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    invoke-interface {p1}, Lcom/google/android/youtube/app/adapter/cf;->a()Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x0

    :goto_12
    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/youtube/app/adapter/bt;->f:I

    .line 84
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 85
    return v1

    .line 83
    :cond_19
    const/4 v0, 0x1

    goto :goto_12
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 154
    return-void
.end method

.method public getCount()I
    .registers 3

    .prologue
    .line 170
    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 158
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_a

    .line 165
    :cond_9
    :goto_9
    return-object v0

    .line 161
    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    .line 162
    if-gez v1, :cond_9

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/youtube/core/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_9
.end method

.method public getItemViewType(I)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 180
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_a

    .line 187
    :cond_9
    :goto_9
    return v0

    .line 183
    :cond_a
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    .line 184
    if-gez v1, :cond_9

    .line 187
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_15

    .line 124
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cf;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(I)Landroid/view/View;

    move-result-object p2

    .line 139
    :goto_14
    return-object p2

    .line 126
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 127
    if-ltz v0, :cond_31

    .line 128
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cf;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/adapter/cf;->a(I)Landroid/view/View;

    move-result-object p2

    goto :goto_14

    .line 131
    :cond_31
    if-nez p2, :cond_4d

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->c:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/youtube/app/adapter/bt;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->b:Lcom/google/android/youtube/app/adapter/cb;

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/app/adapter/cb;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bs;

    move-result-object v0

    .line 134
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 138
    :goto_45
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/adapter/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/app/adapter/bs;->a(ILjava/lang/Object;)Landroid/view/View;

    goto :goto_14

    .line 136
    :cond_4d
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bs;

    goto :goto_45
.end method

.method public getViewTypeCount()I
    .registers 2

    .prologue
    .line 175
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .registers 4
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_15

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cf;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/cf;->a()Z

    move-result v0

    .line 118
    :goto_14
    return v0

    .line 114
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/bt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcom/google/android/youtube/core/a/a;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    .line 115
    if-ltz v0, :cond_31

    .line 116
    iget-object v1, p0, Lcom/google/android/youtube/app/adapter/bt;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/cf;

    invoke-interface {v0}, Lcom/google/android/youtube/app/adapter/cf;->a()Z

    move-result v0

    goto :goto_14

    .line 118
    :cond_31
    const/4 v0, 0x1

    goto :goto_14
.end method
