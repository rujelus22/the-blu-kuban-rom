.class final Lcom/google/android/youtube/app/ui/dl;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/di;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/di;)V
    .registers 3
    .parameter

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    .line 377
    invoke-static {p1}, Lcom/google/android/youtube/app/ui/di;->b(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/d;)V

    .line 378
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/di;->h(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "Favorite"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 382
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/di;->e(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/di;->f(Lcom/google/android/youtube/app/ui/di;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v2, v2, Lcom/google/android/youtube/app/ui/di;->a:Landroid/app/Activity;

    invoke-static {v2, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/b/al;->d(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V

    .line 383
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 373
    const-string v0, "GData"

    const-string v1, "InvalidEntryException"

    const/4 v2, 0x0

    const-string v3, "Video already in favorites list."

    invoke-static {p2, v0, v1, v2, v3}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    const v1, 0x7f0b01ea

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/app/ui/di;I)V

    :goto_15
    return-void

    :cond_16
    const-string v0, "Error adding to favorites"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dl;->c:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    goto :goto_15
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 373
    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/app/ui/di;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dl;->a:Lcom/google/android/youtube/app/ui/di;

    const v1, 0x7f0b01e9

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/di;->a(Lcom/google/android/youtube/app/ui/di;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 392
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/e;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 393
    return-void
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 387
    invoke-super {p0}, Lcom/google/android/youtube/core/ui/e;->i_()V

    .line 388
    return-void
.end method
