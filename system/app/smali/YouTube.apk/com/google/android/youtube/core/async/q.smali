.class public final Lcom/google/android/youtube/core/async/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/av;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Lcom/google/android/youtube/core/async/av;

.field private final b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/av;)V
    .registers 3
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/q;->a:Lcom/google/android/youtube/core/async/av;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 31
    const-string v0, "request cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string v0, "callback cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iget-object v1, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    monitor-enter v1

    .line 34
    :try_start_d
    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 36
    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    :goto_20
    monitor-exit v1

    return-void

    .line 39
    :cond_22
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 40
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v2, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->a:Lcom/google/android/youtube/core/async/av;

    invoke-interface {v0, p1, p0}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    :try_end_35
    .catchall {:try_start_d .. :try_end_35} :catchall_36

    goto :goto_20

    .line 44
    :catchall_36
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 59
    iget-object v1, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    monitor-enter v1

    .line 60
    :try_start_3
    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 61
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_21

    .line 62
    const/4 v1, 0x0

    move v2, v1

    :goto_e
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_24

    .line 63
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/l;

    invoke-interface {v1, p1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 62
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_e

    .line 61
    :catchall_21
    move-exception v0

    monitor-exit v1

    throw v0

    .line 65
    :cond_24
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 49
    iget-object v1, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    monitor-enter v1

    .line 50
    :try_start_3
    iget-object v0, p0, Lcom/google/android/youtube/core/async/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 51
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_21

    .line 52
    const/4 v1, 0x0

    move v2, v1

    :goto_e
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_24

    .line 53
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/l;

    invoke-interface {v1, p1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 52
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_e

    .line 51
    :catchall_21
    move-exception v0

    monitor-exit v1

    throw v0

    .line 55
    :cond_24
    return-void
.end method
