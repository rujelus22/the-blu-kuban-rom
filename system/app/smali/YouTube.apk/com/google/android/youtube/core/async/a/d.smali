.class public final Lcom/google/android/youtube/core/async/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/a/c;


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/al;

.field private final b:Lcom/google/android/youtube/core/model/UserAuth;

.field private final c:Ljava/util/List;

.field private final d:[Lcom/google/android/youtube/core/model/Video;

.field private final e:[Lcom/google/android/youtube/core/async/GDataRequest;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:Lcom/google/android/youtube/core/async/l;

.field private j:I

.field private k:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/model/UserAuth;I)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "gdataClient cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/b/al;

    .line 46
    const-string v0, "videoIds cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->c:Ljava/util/List;

    .line 47
    iput-object p3, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    .line 48
    if-ltz p4, :cond_21

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt p4, v0, :cond_27

    :cond_21
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_60

    :cond_27
    const/4 v0, 0x1

    :goto_28
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startIndex="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be >= 0 and < videoIds.size()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 50
    add-int/lit8 v0, p4, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    .line 51
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->d:[Lcom/google/android/youtube/core/model/Video;

    .line 52
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/youtube/core/async/GDataRequest;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:[Lcom/google/android/youtube/core/async/GDataRequest;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/async/a/d;->j:I

    .line 54
    return-void

    .line 48
    :cond_60
    const/4 v0, 0x0

    goto :goto_28
.end method

.method private declared-synchronized a(I)V
    .registers 10
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    monitor-enter p0

    const/4 v3, -0x1

    :try_start_5
    invoke-static {p1, v3}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 99
    if-ltz v5, :cond_39

    iget-object v3, p0, Lcom/google/android/youtube/core/async/a/d;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_39

    move v4, v1

    .line 100
    :goto_14
    if-lez v5, :cond_3b

    move v3, v1

    .line 101
    :goto_17
    add-int/lit8 v6, v5, 0x1

    iget-object v7, p0, Lcom/google/android/youtube/core/async/a/d;->c:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_22

    move v2, v1

    .line 102
    :cond_22
    if-eqz v4, :cond_3d

    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->d:[Lcom/google/android/youtube/core/model/Video;

    aget-object v1, v1, v5

    .line 103
    :goto_28
    if-eqz v4, :cond_2e

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:[Lcom/google/android/youtube/core/async/GDataRequest;

    aget-object v0, v0, v5

    .line 105
    :cond_2e
    if-nez v1, :cond_32

    if-nez v4, :cond_3f

    .line 106
    :cond_32
    iput v5, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    .line 107
    invoke-direct {p0, v1, v0, v3, v2}, Lcom/google/android/youtube/core/async/a/d;->a(Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/core/async/GDataRequest;ZZ)V
    :try_end_37
    .catchall {:try_start_5 .. :try_end_37} :catchall_43

    .line 111
    :goto_37
    monitor-exit p0

    return-void

    :cond_39
    move v4, v2

    .line 99
    goto :goto_14

    :cond_3b
    move v3, v2

    .line 100
    goto :goto_17

    :cond_3d
    move-object v1, v0

    .line 102
    goto :goto_28

    .line 109
    :cond_3f
    :try_start_3f
    invoke-direct {p0, v5}, Lcom/google/android/youtube/core/async/a/d;->b(I)V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_43

    goto :goto_37

    .line 97
    :catchall_43
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ILcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 135
    monitor-enter p0

    if-lez p1, :cond_1d

    move v2, v0

    :goto_6
    :try_start_6
    iput-boolean v2, p0, Lcom/google/android/youtube/core/async/a/d;->g:Z

    .line 136
    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/google/android/youtube/core/async/a/d;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1f

    :goto_12
    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->f:Z

    .line 137
    iput p1, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, p2, p3}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_1b
    .catchall {:try_start_6 .. :try_end_1b} :catchall_21

    .line 139
    monitor-exit p0

    return-void

    :cond_1d
    move v2, v1

    .line 135
    goto :goto_6

    :cond_1f
    move v0, v1

    .line 136
    goto :goto_12

    .line 135
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/async/a/d;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;ILcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/youtube/core/async/a/d;->a(ILcom/google/android/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/youtube/core/model/Video;Lcom/google/android/youtube/core/async/GDataRequest;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    monitor-enter p0

    :try_start_1
    iput-boolean p3, p0, Lcom/google/android/youtube/core/async/a/d;->g:Z

    .line 129
    iput-boolean p4, p0, Lcom/google/android/youtube/core/async/a/d;->f:Z

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:Lcom/google/android/youtube/core/async/l;

    invoke-interface {v0, p2, p1}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 131
    monitor-exit p0

    return-void

    .line 128
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->k:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/async/a/d;)[Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->d:[Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method private declared-synchronized b(I)V
    .registers 6
    .parameter

    .prologue
    .line 114
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/youtube/core/async/a/d;->j:I

    .line 116
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->k:Z

    if-nez v0, :cond_22

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->k:Z

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_24

    .line 119
    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/b/al;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/async/a/d;->b:Lcom/google/android/youtube/core/model/UserAuth;

    new-instance v3, Lcom/google/android/youtube/core/async/a/e;

    invoke-direct {v3, p0, p1}, Lcom/google/android/youtube/core/async/a/e;-><init>(Lcom/google/android/youtube/core/async/a/d;I)V

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/l;)V
    :try_end_22
    .catchall {:try_start_1 .. :try_end_22} :catchall_37

    .line 124
    :cond_22
    :goto_22
    monitor-exit p0

    return-void

    .line 121
    :cond_24
    :try_start_24
    iget-object v1, p0, Lcom/google/android/youtube/core/async/a/d;->a:Lcom/google/android/youtube/core/b/al;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lcom/google/android/youtube/core/async/a/e;

    invoke-direct {v2, p0, p1}, Lcom/google/android/youtube/core/async/a/e;-><init>(Lcom/google/android/youtube/core/async/a/d;I)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/b/al;->c(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V
    :try_end_36
    .catchall {:try_start_24 .. :try_end_36} :catchall_37

    goto :goto_22

    .line 114
    :catchall_37
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/async/a/d;)[Lcom/google/android/youtube/core/async/GDataRequest;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->e:[Lcom/google/android/youtube/core/async/GDataRequest;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/async/a/d;)Z
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->k:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/async/a/d;)I
    .registers 2
    .parameter

    .prologue
    .line 23
    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->j:I

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 3

    .prologue
    .line 65
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:Lcom/google/android/youtube/core/async/l;

    const-string v1, "call setCallback() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/d;->a(I)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 67
    monitor-exit p0

    return-void

    .line 65
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/youtube/core/async/l;)V
    .registers 3
    .parameter

    .prologue
    .line 57
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/l;

    iput-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:Lcom/google/android/youtube/core/async/l;

    .line 58
    return-void
.end method

.method public final declared-synchronized b()V
    .registers 3

    .prologue
    .line 70
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:Lcom/google/android/youtube/core/async/l;

    const-string v1, "call setCallback() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/d;->a(I)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 72
    monitor-exit p0

    return-void

    .line 70
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .registers 3

    .prologue
    .line 75
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/youtube/core/async/a/d;->i:Lcom/google/android/youtube/core/async/l;

    const-string v1, "call setCallback() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/async/a/d;->a(I)V
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 77
    monitor-exit p0

    return-void

    .line 75
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .registers 2

    .prologue
    .line 85
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->f:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .registers 2

    .prologue
    .line 89
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->g:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .registers 2

    .prologue
    .line 147
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/youtube/core/async/a/d;->k:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 148
    monitor-exit p0

    return-void

    .line 147
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()I
    .registers 2

    .prologue
    .line 93
    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/youtube/core/async/a/d;->h:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method
