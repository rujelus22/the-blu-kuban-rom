.class final Lcom/google/android/youtube/core/async/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/async/ax;

.field private final b:Lcom/google/android/youtube/core/async/GDataRequest;

.field private final c:Lcom/google/android/youtube/core/async/l;

.field private final d:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/ax;Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/l;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/youtube/core/async/ay;->a:Lcom/google/android/youtube/core/async/ax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p2, p0, Lcom/google/android/youtube/core/async/ay;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    .line 122
    iput-object p3, p0, Lcom/google/android/youtube/core/async/ay;->c:Lcom/google/android/youtube/core/async/l;

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/async/ay;->d:Ljava/util/List;

    .line 124
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/youtube/core/async/ay;->c:Lcom/google/android/youtube/core/async/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ay;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 113
    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ay;->d:Ljava/util/List;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/youtube/core/async/ay;->a:Lcom/google/android/youtube/core/async/ax;

    invoke-static {v0}, Lcom/google/android/youtube/core/async/ax;->a(Lcom/google/android/youtube/core/async/ax;)Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ay;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p2, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/async/GDataRequest;->b(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    :goto_1e
    return-void

    :cond_1f
    iget-object v0, p0, Lcom/google/android/youtube/core/async/ay;->c:Lcom/google/android/youtube/core/async/l;

    iget-object v1, p0, Lcom/google/android/youtube/core/async/ay;->b:Lcom/google/android/youtube/core/async/GDataRequest;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/youtube/core/async/ay;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1e
.end method
