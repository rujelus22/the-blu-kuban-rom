.class public final Lcom/google/android/youtube/app/honeycomb/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/youtube/core/Analytics;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/d;

.field private final f:Landroid/preference/CheckBoxPreference;

.field private final g:Landroid/preference/CheckBoxPreference;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private j:Lcom/google/android/youtube/app/honeycomb/g;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->a:Landroid/app/Activity;

    .line 50
    const-string v0, "prefetchSubscriptionsPreference cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->f:Landroid/preference/CheckBoxPreference;

    .line 52
    const-string v0, "prefetchWatchLaterPreference cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->g:Landroid/preference/CheckBoxPreference;

    .line 55
    const v0, 0x7f0b00c9

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->h:Ljava/lang/String;

    .line 56
    const v0, 0x7f0b00ca

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->i:Ljava/lang/String;

    .line 58
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/f;->b:Landroid/content/SharedPreferences;

    .line 60
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/f;->c:Lcom/google/android/youtube/core/Analytics;

    .line 61
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/f;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->e:Lcom/google/android/youtube/core/d;

    .line 68
    invoke-virtual {p2, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 69
    invoke-virtual {p2, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 70
    invoke-virtual {p3, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 71
    invoke-virtual {p3, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 81
    return-void
.end method

.method private a(Landroid/preference/CheckBoxPreference;)V
    .registers 4
    .parameter

    .prologue
    .line 132
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->h:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 134
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "PrefetchSubscriptionsOn"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 138
    :cond_17
    :goto_17
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-nez v0, :cond_27

    .line 139
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->e:Lcom/google/android/youtube/core/d;

    const v1, 0x7f0b0155

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    .line 141
    :cond_27
    return-void

    .line 135
    :cond_28
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "PrefetchWatchLaterOn"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_17
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/f;Landroid/preference/CheckBoxPreference;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/f;->a(Landroid/preference/CheckBoxPreference;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .registers 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->j:Lcom/google/android/youtube/app/honeycomb/g;

    if-nez v0, :cond_c

    .line 85
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/g;-><init>(Lcom/google/android/youtube/app/honeycomb/f;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->j:Lcom/google/android/youtube/app/honeycomb/g;

    .line 87
    :cond_c
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/f;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/x;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b014f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/f;->j:Lcom/google/android/youtube/app/honeycomb/g;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x104

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 96
    const-string v0, "prefetch_pref_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/f;->j:Lcom/google/android/youtube/app/honeycomb/g;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/f;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->f:Landroid/preference/CheckBoxPreference;

    :goto_12
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/g;->a(Landroid/preference/CheckBoxPreference;)V

    .line 100
    return-void

    .line 97
    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->g:Landroid/preference/CheckBoxPreference;

    goto :goto_12
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 114
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .line 115
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1f

    move v0, v1

    .line 116
    :goto_11
    if-eqz v0, :cond_33

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 119
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/f;->a(Landroid/preference/CheckBoxPreference;)V

    .line 128
    :cond_1e
    :goto_1e
    return v1

    :cond_1f
    move v0, v2

    .line 115
    goto :goto_11

    .line 121
    :cond_21
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    const-string v2, "prefetch_pref_key"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/f;->a:Landroid/app/Activity;

    const/16 v3, 0x401

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_1e

    .line 126
    :cond_33
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->h:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "PrefetchSubscriptionsOff"

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_1e

    :cond_4a
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/f;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "PrefetchWatchLaterOff"

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_1e
.end method
