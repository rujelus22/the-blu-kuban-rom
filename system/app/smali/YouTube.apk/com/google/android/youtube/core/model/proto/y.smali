.class public final Lcom/google/android/youtube/core/model/proto/y;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/aa;


# instance fields
.field private a:I

.field private b:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 4328
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 4405
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;->BAD_TOKEN:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/y;->b:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    .line 4329
    return-void
.end method

.method static synthetic a()Lcom/google/android/youtube/core/model/proto/y;
    .registers 1

    .prologue
    .line 4323
    new-instance v0, Lcom/google/android/youtube/core/model/proto/y;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/y;-><init>()V

    return-object v0
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/y;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 4389
    const/4 v2, 0x0

    .line 4391
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 4396
    if-eqz v0, :cond_e

    .line 4397
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;

    .line 4400
    :cond_e
    return-object p0

    .line 4392
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 4393
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 4394
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 4396
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 4397
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;

    :cond_21
    throw v0

    .line 4396
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method private g()Lcom/google/android/youtube/core/model/proto/y;
    .registers 3

    .prologue
    .line 4346
    new-instance v0, Lcom/google/android/youtube/core/model/proto/y;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/y;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/y;->h()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 4362
    new-instance v2, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V

    .line 4363
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/y;->a:I

    .line 4364
    const/4 v1, 0x0

    .line 4365
    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_17

    .line 4368
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/y;->b:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->error_:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->access$3702(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;)Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    .line 4369
    #setter for: Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->access$3802(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;I)I

    .line 4370
    return-object v2

    :cond_17
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;)Lcom/google/android/youtube/core/model/proto/y;
    .registers 4
    .parameter

    .prologue
    .line 4374
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 4378
    :cond_6
    :goto_6
    return-object p0

    .line 4375
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->hasError()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4376
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->getError()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    move-result-object v0

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/y;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/model/proto/y;->a:I

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/y;->b:Lcom/google/android/youtube/core/model/proto/Notification$ServerReply$ErrorCode;

    goto :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 4323
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4323
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4323
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 4323
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/y;->g()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4323
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/y;->g()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 4323
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/y;->g()Lcom/google/android/youtube/core/model/proto/y;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4323
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/y;->h()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 4323
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/y;->h()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/y;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4323
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$ServerReply;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 4382
    const/4 v0, 0x1

    return v0
.end method
