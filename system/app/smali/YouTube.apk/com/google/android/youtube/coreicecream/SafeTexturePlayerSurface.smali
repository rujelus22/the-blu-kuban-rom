.class public final Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/au;


# instance fields
.field private a:Lcom/google/android/youtube/core/player/au;

.field private b:Z

.field private c:Lcom/google/android/youtube/core/player/av;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    if-nez v0, :cond_c

    .line 123
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PlayerSurface method called before surface created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_c
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 77
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c()V

    .line 82
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/aq;)V

    .line 83
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/av;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/av;

    .line 67
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    if-eqz v0, :cond_f

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->b:Z

    .line 69
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/av;)V

    .line 74
    :goto_e
    return-void

    .line 71
    :cond_f
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->b:Z

    goto :goto_e
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c()V

    .line 118
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/au;->b()V

    .line 119
    return-void
.end method

.method protected final onAttachedToWindow()V
    .registers 3

    .prologue
    .line 31
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 34
    new-instance v0, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;

    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/coreicecream/TexturePlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    .line 38
    :goto_14
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/au;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->addView(Landroid/view/View;)V

    .line 40
    iget-boolean v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->b:Z

    if-eqz v0, :cond_2b

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->b:Z

    .line 42
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    iget-object v1, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/au;->a(Lcom/google/android/youtube/core/player/av;)V

    .line 44
    :cond_2b
    return-void

    .line 36
    :cond_2c
    new-instance v0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;

    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    goto :goto_14
.end method

.method protected final onLayout(ZIIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->getChildCount()I

    move-result v0

    if-lez v0, :cond_16

    .line 60
    invoke-virtual {p0, v3}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 63
    :cond_16
    return-void
.end method

.method protected final onMeasure(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1a

    .line 49
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 51
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->setMeasuredDimension(II)V

    .line 55
    :goto_19
    return-void

    .line 53
    :cond_1a
    invoke-virtual {p0, v1, v1}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->setMeasuredDimension(II)V

    goto :goto_19
.end method

.method public final setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/aw;)V
    .registers 3
    .parameter

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c()V

    .line 92
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/au;->setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/aw;)V

    .line 93
    return-void
.end method

.method public final setVideoSize(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c()V

    .line 87
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/au;->setVideoSize(II)V

    .line 88
    return-void
.end method

.method public final setZoom(I)V
    .registers 3
    .parameter

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->c()V

    .line 112
    iget-object v0, p0, Lcom/google/android/youtube/coreicecream/SafeTexturePlayerSurface;->a:Lcom/google/android/youtube/core/player/au;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/au;->setZoom(I)V

    .line 114
    return-void
.end method
