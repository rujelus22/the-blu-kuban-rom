.class public final Lcom/google/android/youtube/app/honeycomb/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/remote/ao;


# instance fields
.field private final a:Landroid/media/AudioManager;

.field private final b:Landroid/media/RemoteControlClient;

.field private final c:Landroid/content/ComponentName;

.field private final d:Lcom/google/android/youtube/app/honeycomb/a/c;

.field private e:Ljava/lang/String;

.field private f:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    .line 38
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/youtube/app/remote/MediaButtonIntentReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->c:Landroid/content/ComponentName;

    .line 40
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 44
    new-instance v1, Landroid/media/RemoteControlClient;

    invoke-direct {v1, v0}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    .line 45
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/a/c;

    invoke-direct {v0, v3}, Lcom/google/android/youtube/app/honeycomb/a/c;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->d:Lcom/google/android/youtube/app/honeycomb/a/c;

    .line 46
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->d:Lcom/google/android/youtube/app/honeycomb/a/c;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 109
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 111
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .registers 5
    .parameter

    .prologue
    .line 49
    const/4 v0, 0x1

    .line 50
    sget-object v1, Lcom/google/android/youtube/app/honeycomb/a/b;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1c

    .line 67
    :goto_c
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 68
    return-void

    .line 52
    :pswitch_12
    const/4 v0, 0x2

    .line 53
    goto :goto_c

    .line 55
    :pswitch_14
    const/16 v0, 0x9

    .line 56
    goto :goto_c

    .line 58
    :pswitch_17
    const/4 v0, 0x3

    .line 59
    goto :goto_c

    .line 64
    :pswitch_19
    const/16 v0, 0x8

    goto :goto_c

    .line 50
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_12
        :pswitch_14
        :pswitch_17
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 91
    const-string v0, "videoTitle cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->e:Ljava/lang/String;

    .line 92
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->f:Landroid/graphics/Bitmap;

    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v3}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    .line 94
    const/4 v1, 0x7

    invoke-virtual {v0, v1, p1}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 95
    if-eqz p2, :cond_1e

    .line 96
    const/16 v1, 0x64

    invoke-virtual {v0, v1, p2}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 98
    :cond_1e
    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->d:Lcom/google/android/youtube/app/honeycomb/a/c;

    const/high16 v2, -0x8000

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 101
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->a:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 103
    return-void
.end method

.method public final a(ZZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    if-eqz p1, :cond_5

    .line 74
    const/16 v0, 0x8

    .line 76
    :cond_5
    if-eqz p2, :cond_9

    .line 77
    or-int/lit8 v0, v0, 0x1

    .line 79
    :cond_9
    if-eqz p3, :cond_d

    .line 80
    or-int/lit16 v0, v0, 0x80

    .line 82
    :cond_d
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->b:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v0}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->e:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/a/a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/a/a;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 87
    :cond_1d
    return-void
.end method
