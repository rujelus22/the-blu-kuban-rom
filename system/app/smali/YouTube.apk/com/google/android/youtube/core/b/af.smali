.class public Lcom/google/android/youtube/core/b/af;
.super Lcom/google/android/youtube/core/b/c;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;
.implements Lcom/google/android/youtube/core/b/au;


# static fields
.field private static final k:Ljava/security/SecureRandom;

.field private static final l:Landroid/net/Uri;

.field private static final m:Landroid/net/Uri;

.field private static final n:Ljava/util/regex/Pattern;

.field private static final o:[J


# instance fields
.field private A:Lcom/google/android/youtube/core/b/aq;

.field private B:J

.field private C:Landroid/net/Uri;

.field private D:Lcom/google/android/youtube/core/model/Stream$Quality;

.field private E:Z

.field private F:I

.field private G:I

.field private H:Lcom/google/android/youtube/core/model/VastAd;

.field private I:Z

.field private J:Z

.field private K:I

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:I

.field private P:I

.field private Q:I

.field private final R:Ljava/util/ArrayList;

.field private S:J

.field private T:Z

.field private final U:Landroid/os/HandlerThread;

.field protected g:Z

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field protected final j:Landroid/os/Handler;

.field private final p:Lcom/google/android/youtube/core/async/av;

.field private final q:Ljava/util/Random;

.field private final r:Lcom/google/android/youtube/core/Analytics;

.field private final s:Lcom/google/android/youtube/core/b/ai;

.field private final t:Ljava/lang/String;

.field private final u:I

.field private final v:Z

.field private final w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:I

.field private z:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 66
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/b/af;->k:Ljava/security/SecureRandom;

    .line 90
    const-string v0, "http://video.google.com/s"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/b/af;->l:Landroid/net/Uri;

    .line 91
    const-string v0, "http://s2.youtube.com/s?ns=yt"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/b/af;->m:Landroid/net/Uri;

    .line 93
    const-string v0, "itag[/=](\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/core/b/af;->n:Ljava/util/regex/Pattern;

    .line 100
    const/4 v0, 0x3

    new-array v0, v0, [J

    fill-array-data v0, :array_28

    sput-object v0, Lcom/google/android/youtube/core/b/af;->o:[J

    return-void

    :array_28
    .array-data 0x8
        0x10t 0x27t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x20t 0x4et 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0x40t 0x9ct 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZ)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 177
    const-string v8, "android"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/core/b/af;-><init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZLjava/lang/String;)V

    .line 179
    return-void
.end method

.method private constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZLjava/lang/String;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 169
    sget-object v4, Lcom/google/android/youtube/core/b/af;->k:Ljava/security/SecureRandom;

    new-instance v5, Lcom/google/android/youtube/core/b/ag;

    invoke-direct {v5, p4}, Lcom/google/android/youtube/core/b/ag;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p4}, Lcom/google/android/youtube/core/utils/Util;->d(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v6, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/youtube/core/b/af;-><init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Ljava/util/Random;Lcom/google/android/youtube/core/b/ai;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZLjava/lang/String;)V

    .line 173
    return-void
.end method

.method private constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/youtube/core/utils/d;Ljava/util/Random;Lcom/google/android/youtube/core/b/ai;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZLjava/lang/String;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    invoke-direct {p0, p2, p1, p3}, Lcom/google/android/youtube/core/b/c;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/youtube/core/utils/d;)V

    .line 186
    iput-object p4, p0, Lcom/google/android/youtube/core/b/af;->q:Ljava/util/Random;

    .line 187
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->e:Lcom/google/android/youtube/core/converter/http/dp;

    invoke-static {}, Lcom/google/android/youtube/core/converter/http/bi;->c()Lcom/google/android/youtube/core/converter/http/bi;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/core/b/af;->a(Lcom/google/android/youtube/core/converter/a;Lcom/google/android/youtube/core/converter/http/bi;)Lcom/google/android/youtube/core/async/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->p:Lcom/google/android/youtube/core/async/av;

    .line 188
    const-string v0, "analytics cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->r:Lcom/google/android/youtube/core/Analytics;

    .line 189
    const-string v0, "volumeProvider cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/ai;

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->s:Lcom/google/android/youtube/core/b/ai;

    .line 191
    iput-object p7, p0, Lcom/google/android/youtube/core/b/af;->t:Ljava/lang/String;

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->R:Ljava/util/ArrayList;

    .line 193
    iput p8, p0, Lcom/google/android/youtube/core/b/af;->u:I

    .line 194
    iput-boolean p9, p0, Lcom/google/android/youtube/core/b/af;->v:Z

    .line 195
    const-string v0, "playerStyle cannot be empty"

    invoke-static {p10, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->w:Ljava/lang/String;

    .line 197
    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->U:Landroid/os/HandlerThread;

    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->U:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 199
    new-instance v0, Lcom/google/android/youtube/core/b/ah;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->U:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/b/ah;-><init>(Lcom/google/android/youtube/core/b/af;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    .line 201
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->z:Ljava/util/Map;

    .line 202
    invoke-static {}, Lcom/google/android/youtube/core/b/ar;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/b/af;->a(Ljava/util/List;)V

    .line 203
    return-void
.end method

.method static a(II)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 509
    if-lez p1, :cond_6

    .line 510
    mul-int/lit8 v0, p0, 0x4

    div-int/2addr v0, p1

    .line 512
    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method static synthetic a(Lcom/google/android/youtube/core/b/af;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput p1, p0, Lcom/google/android/youtube/core/b/af;->G:I

    return p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/net/Uri$Builder;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 672
    sget-object v0, Lcom/google/android/youtube/core/b/af;->l:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "docid"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "app"

    const-string v2, "youtube_gdata"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "yt"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "len"

    invoke-static {p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "el"

    invoke-virtual {v0, v1, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ps"

    invoke-virtual {v0, v1, p5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "rt"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "av"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 683
    return-object v0
.end method

.method private static a(J)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    .line 756
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-float v4, p0

    const/high16 v5, 0x447a

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/net/Uri;)Z
    .registers 4
    .parameter

    .prologue
    .line 529
    if-eqz p1, :cond_1a

    .line 530
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pinging "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    .line 531
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->p:Lcom/google/android/youtube/core/async/av;

    invoke-interface {v0, p1, p0}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 532
    const/4 v0, 0x1

    .line 534
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method static synthetic a(Lcom/google/android/youtube/core/b/af;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->I:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/b/af;Landroid/net/Uri;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/b/af;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/b/af;Ljava/util/List;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 61
    if-eqz p1, :cond_31

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_31

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pinging "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->p:Lcom/google/android/youtube/core/async/av;

    invoke-interface {v2, v0, p0}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_c

    :cond_2f
    const/4 v0, 0x1

    :goto_30
    return v0

    :cond_31
    const/4 v0, 0x0

    goto :goto_30
.end method

.method static synthetic a(Lcom/google/android/youtube/core/b/af;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->M:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/b/af;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput p1, p0, Lcom/google/android/youtube/core/b/af;->K:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/VastAd;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/b/af;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/google/android/youtube/core/b/af;->J:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/youtube/core/b/af;I)Ljava/util/List;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    packed-switch p1, :pswitch_data_14

    const/4 v0, 0x0

    :goto_4
    return-object v0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    goto :goto_4

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->midpointPingUris:Ljava/util/List;

    goto :goto_4

    :pswitch_f
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    goto :goto_4

    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_5
        :pswitch_a
        :pswitch_f
    .end packed-switch
.end method

.method static synthetic c(Lcom/google/android/youtube/core/b/af;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->M:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/b/af;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->L:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/b/af;I)Landroid/net/Uri;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 61
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(J)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/youtube/core/b/af;->m:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "docid"

    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->x:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "plid"

    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ps"

    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->w:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "yttk"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "st"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "et"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "el"

    const-string v2, "detailpage"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ctp"

    iget v2, p0, Lcom/google/android/youtube/core/b/af;->F:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/b/af;)V
    .registers 14
    .parameter

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->T:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    if-nez v0, :cond_116

    :cond_a
    iget-boolean v6, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    if-eqz v0, :cond_13

    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->x()V

    :cond_13
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->c:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/d;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/youtube/core/b/af;->B:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/b/af;->a(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->x:Ljava/lang/String;

    const-string v3, "detailpage"

    iget v4, p0, Lcom/google/android/youtube/core/b/af;->y:I

    iget-object v5, p0, Lcom/google/android/youtube/core/b/af;->w:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/b/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v0, "plid"

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->i:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "sw"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%.1f"

    new-array v4, v12, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/youtube/core/b/af;->u:I

    int-to-float v5, v5

    const/high16 v7, 0x4120

    div-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v11

    invoke-static {v1, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    if-nez v0, :cond_117

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->v:Z

    if-eqz v0, :cond_5e

    const-string v0, "playback"

    const-string v1, "1"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_5e
    const-string v0, "st"

    const-string v1, "0.0"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "et"

    const-string v1, "0.0"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_6c
    :goto_6c
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->A:Lcom/google/android/youtube/core/b/aq;

    if-eqz v0, :cond_96

    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->A:Lcom/google/android/youtube/core/b/aq;

    iget-object v0, v0, Lcom/google/android/youtube/core/b/aq;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_83

    const-string v0, "sourceid"

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->A:Lcom/google/android/youtube/core/b/aq;

    iget-object v1, v1, Lcom/google/android/youtube/core/b/aq;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_83
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->A:Lcom/google/android/youtube/core/b/aq;

    iget-object v0, v0, Lcom/google/android/youtube/core/b/aq;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_96

    const-string v0, "sdetail"

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->A:Lcom/google/android/youtube/core/b/aq;

    iget-object v1, v1, Lcom/google/android/youtube/core/b/aq;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_96
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->C:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/youtube/core/b/af;->n:Ljava/util/regex/Pattern;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1be

    invoke-virtual {v0, v12}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    :goto_ac
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1c1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unable to infer iTag [videoId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", streamUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->C:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_d8
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/core/b/af;->a(Landroid/net/Uri$Builder;)V

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/b/af;->a(Landroid/net/Uri;)Z

    iput v11, p0, Lcom/google/android/youtube/core/b/af;->P:I

    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->c:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/d;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/youtube/core/b/af;->S:J

    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->R:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz v6, :cond_f6

    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->w()V

    :cond_f6
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->T:Z

    if-eqz v0, :cond_116

    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    sget-object v1, Lcom/google/android/youtube/core/b/af;->o:[J

    array-length v1, v1

    if-gt v0, v1, :cond_1c8

    sget-object v0, Lcom/google/android/youtube/core/b/af;->o:[J

    iget v1, p0, Lcom/google/android/youtube/core/b/af;->O:I

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    :goto_10f
    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v3, 0x6b

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_116
    return-void

    :cond_117
    const-string v0, "nbe"

    iget v1, p0, Lcom/google/android/youtube/core/b/af;->P:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->R:Ljava/util/ArrayList;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_132
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_177

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v9, "%.1f,"

    new-array v10, v12, [Ljava/lang/Object;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%.1f,"

    new-array v9, v12, [Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v9, v11

    invoke-static {v1, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_132

    :cond_177
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a4

    const-string v0, ""

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    :goto_186
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6c

    const-string v3, "st"

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v3, "et"

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto/16 :goto_6c

    :cond_1a4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v11, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v5, v11, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_186

    :cond_1be
    const/4 v0, 0x0

    goto/16 :goto_ac

    :cond_1c1
    const-string v1, "fmt"

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto/16 :goto_d8

    :cond_1c8
    sget-object v0, Lcom/google/android/youtube/core/b/af;->o:[J

    sget-object v1, Lcom/google/android/youtube/core/b/af;->o:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-wide v0, v0, v1

    goto/16 :goto_10f
.end method

.method static synthetic d(Lcom/google/android/youtube/core/b/af;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->I:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/b/af;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->J:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/model/Stream$Quality;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->D:Lcom/google/android/youtube/core/model/Stream$Quality;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/b/af;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->G:I

    return v0
.end method

.method static synthetic h(Lcom/google/android/youtube/core/b/af;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->r:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/core/b/af;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->x()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/youtube/core/b/af;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->L:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/youtube/core/b/af;)Landroid/net/Uri;
    .registers 7
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->G:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    const-string v3, "adunit"

    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget v4, v0, Lcom/google/android/youtube/core/model/VastAd;->duration:I

    const-string v5, "trueview-instream"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/b/af;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "tv"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "st"

    iget v2, p0, Lcom/google/android/youtube/core/b/af;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "et"

    iget v2, p0, Lcom/google/android/youtube/core/b/af;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "content_v"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "eurl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://www.youtube.com/watch?v="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/VastAd;->originalVideoId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "vol"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->s:Lcom/google/android/youtube/core/b/ai;

    invoke-interface {v2}, Lcom/google/android/youtube/core/b/ai;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/core/b/af;)Z
    .registers 2
    .parameter

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->E:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/youtube/core/b/af;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->u()V

    return-void
.end method

.method static synthetic n(Lcom/google/android/youtube/core/b/af;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->K:I

    return v0
.end method

.method static synthetic o(Lcom/google/android/youtube/core/b/af;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->F:I

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/core/b/af;)I
    .registers 3
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->F:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/b/af;->F:I

    return v0
.end method

.method static synthetic q(Lcom/google/android/youtube/core/b/af;)I
    .registers 3
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->P:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/youtube/core/b/af;->P:I

    return v0
.end method

.method static synthetic r(Lcom/google/android/youtube/core/b/af;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->w()V

    return-void
.end method

.method static synthetic s(Lcom/google/android/youtube/core/b/af;)I
    .registers 2
    .parameter

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    return v0
.end method

.method private u()V
    .registers 2

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->h:Ljava/lang/String;

    .line 259
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/b/af;->F:I

    .line 260
    return-void
.end method

.method private v()Ljava/lang/String;
    .registers 3

    .prologue
    .line 572
    const/16 v0, 0xc

    new-array v0, v0, [B

    .line 573
    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->q:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 574
    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .registers 3

    .prologue
    .line 792
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    if-nez v0, :cond_19

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->I:Z

    if-nez v0, :cond_19

    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    const/16 v1, 0x190

    if-ge v0, v1, :cond_19

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->T:Z

    if-eqz v0, :cond_19

    .line 794
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    .line 795
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->G:I

    iput v0, p0, Lcom/google/android/youtube/core/b/af;->Q:I

    .line 797
    :cond_19
    return-void
.end method

.method private x()V
    .registers 4

    .prologue
    .line 800
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    if-eqz v0, :cond_28

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->I:Z

    if-nez v0, :cond_28

    .line 801
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    .line 802
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->G:I

    iget v1, p0, Lcom/google/android/youtube/core/b/af;->Q:I

    sub-int/2addr v0, v1

    .line 803
    const/4 v1, 0x3

    if-le v0, v1, :cond_28

    .line 804
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->R:Ljava/util/ArrayList;

    iget v1, p0, Lcom/google/android/youtube/core/b/af;->Q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/youtube/core/b/af;->G:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 807
    :cond_28
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 249
    return-void
.end method

.method public final a(III)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 893
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, p1, p3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 895
    return-void
.end method

.method protected a(Landroid/net/Uri$Builder;)V
    .registers 2
    .parameter

    .prologue
    .line 748
    return-void
.end method

.method public final a(Landroid/net/Uri;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 615
    iput-object p1, p0, Lcom/google/android/youtube/core/b/af;->C:Landroid/net/Uri;

    .line 616
    iput-boolean p2, p0, Lcom/google/android/youtube/core/b/af;->g:Z

    .line 617
    return-void
.end method

.method public final a(Landroid/os/Message;)V
    .registers 4
    .parameter

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    invoke-static {v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    move-result-object v0

    .line 868
    invoke-virtual {v0, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    .line 869
    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 870
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Stream$Quality;)V
    .registers 5
    .parameter

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v2, 0x67

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 239
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 5
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-static {v1, v2, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 231
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 10
    .parameter

    .prologue
    .line 212
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-boolean v1, p1, Lcom/google/android/youtube/core/model/Video;->claimed:Z

    iget v2, p1, Lcom/google/android/youtube/core/model/Video;->duration:I

    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v5, 0x65

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v4, v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 213
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 61
    check-cast p1, Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ping failed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->c()V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 612
    return-void
.end method

.method final a(Ljava/lang/String;ZI)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/youtube/core/b/af;->x:Ljava/lang/String;

    .line 222
    iput p3, p0, Lcom/google/android/youtube/core/b/af;->y:I

    .line 223
    iput-boolean p2, p0, Lcom/google/android/youtube/core/b/af;->E:Z

    .line 224
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->E:Z

    if-eqz v0, :cond_d

    .line 225
    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->u()V

    .line 227
    :cond_d
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 831
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    .line 832
    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->z:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/youtube/core/b/aq;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_28

    const/4 v1, 0x1

    :goto_1b
    const-string v3, "Cannot override an existing referrer value"

    invoke-static {v1, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 834
    iget-object v1, p0, Lcom/google/android/youtube/core/b/af;->z:Ljava/util/Map;

    iget-object v3, v0, Lcom/google/android/youtube/core/b/aq;->a:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 832
    :cond_28
    const/4 v1, 0x0

    goto :goto_1b

    .line 836
    :cond_2a
    return-void
.end method

.method final b(Lcom/google/android/youtube/core/model/Stream$Quality;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 242
    iput-object p1, p0, Lcom/google/android/youtube/core/b/af;->D:Lcom/google/android/youtube/core/model/Stream$Quality;

    .line 243
    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->I:Z

    .line 244
    iput v0, p0, Lcom/google/android/youtube/core/b/af;->K:I

    .line 245
    return-void
.end method

.method final b(Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 2
    .parameter

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/youtube/core/b/af;->H:Lcom/google/android/youtube/core/model/VastAd;

    .line 235
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->z:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/aq;

    .line 640
    if-nez v0, :cond_f

    .line 641
    const-string v1, "Referrer cannot be recognized"

    invoke-static {v1}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 643
    :cond_f
    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->A:Lcom/google/android/youtube/core/b/aq;

    .line 644
    return-void
.end method

.method final c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->I:Z

    .line 253
    iput v1, p0, Lcom/google/android/youtube/core/b/af;->K:I

    .line 254
    iput-boolean v1, p0, Lcom/google/android/youtube/core/b/af;->L:Z

    .line 255
    return-void
.end method

.method public final c(I)V
    .registers 5
    .parameter

    .prologue
    .line 881
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 882
    return-void
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 264
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 268
    return-void
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x6e

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 272
    return-void
.end method

.method public final g()V
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x6d

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 276
    return-void
.end method

.method public final h()V
    .registers 3

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x6c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 280
    return-void
.end method

.method public final i()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 595
    invoke-direct {p0}, Lcom/google/android/youtube/core/b/af;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/b/af;->i:Ljava/lang/String;

    .line 596
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->c:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/d;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/youtube/core/b/af;->B:J

    .line 597
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v2, 0x6b

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 598
    iput-boolean v1, p0, Lcom/google/android/youtube/core/b/af;->N:Z

    .line 599
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->q:Ljava/util/Random;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    iget v0, p0, Lcom/google/android/youtube/core/b/af;->u:I

    if-le v0, v2, :cond_63

    const/4 v0, 0x1

    :goto_25
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "VSS sampling weight is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/youtube/core/b/af;->u:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", generated number is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", will ping - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-static {}, Lcom/google/android/youtube/core/L;->b()V

    iput-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->T:Z

    .line 600
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->R:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 601
    iput-boolean v1, p0, Lcom/google/android/youtube/core/b/af;->M:Z

    .line 602
    iput v1, p0, Lcom/google/android/youtube/core/b/af;->O:I

    .line 603
    iput v1, p0, Lcom/google/android/youtube/core/b/af;->P:I

    .line 604
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/youtube/core/b/af;->S:J

    .line 605
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->x:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/b/af;->a(Ljava/lang/String;)V

    .line 606
    return-void

    :cond_63
    move v0, v1

    .line 599
    goto :goto_25
.end method

.method public final j()V
    .registers 6

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->r:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "PlayStarted"

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->D:Lcom/google/android/youtube/core/model/Stream$Quality;

    iget-boolean v3, p0, Lcom/google/android/youtube/core/b/af;->g:Z

    iget v4, p0, Lcom/google/android/youtube/core/b/af;->y:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;ZI)V

    .line 621
    return-void
.end method

.method public final k()V
    .registers 3

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x6b

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 626
    return-void
.end method

.method public final l()V
    .registers 12

    .prologue
    const/16 v10, 0x6b

    .line 630
    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    if-lez v0, :cond_59

    iget v0, p0, Lcom/google/android/youtube/core/b/af;->O:I

    const/16 v1, 0x190

    if-ge v0, v1, :cond_59

    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_59

    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->T:Z

    if-eqz v0, :cond_59

    .line 632
    iget-object v3, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    iget-wide v0, p0, Lcom/google/android/youtube/core/b/af;->S:J

    iget-object v2, p0, Lcom/google/android/youtube/core/b/af;->c:Lcom/google/android/youtube/core/utils/d;

    invoke-interface {v2}, Lcom/google/android/youtube/core/utils/d;->b()J

    move-result-wide v4

    iget v2, p0, Lcom/google/android/youtube/core/b/af;->O:I

    :goto_24
    sget-object v6, Lcom/google/android/youtube/core/b/af;->o:[J

    array-length v6, v6

    if-gt v2, v6, :cond_37

    cmp-long v6, v0, v4

    if-gez v6, :cond_37

    sget-object v6, Lcom/google/android/youtube/core/b/af;->o:[J

    add-int/lit8 v7, v2, -0x1

    aget-wide v6, v6, v7

    add-long/2addr v0, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_24

    :cond_37
    cmp-long v2, v0, v4

    if-gez v2, :cond_55

    sub-long v6, v4, v0

    sget-object v2, Lcom/google/android/youtube/core/b/af;->o:[J

    sget-object v8, Lcom/google/android/youtube/core/b/af;->o:[J

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    aget-wide v8, v2, v8

    div-long/2addr v6, v8

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    sget-object v2, Lcom/google/android/youtube/core/b/af;->o:[J

    sget-object v8, Lcom/google/android/youtube/core/b/af;->o:[J

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    aget-wide v8, v2, v8

    mul-long/2addr v6, v8

    add-long/2addr v0, v6

    :cond_55
    sub-long/2addr v0, v4

    invoke-virtual {v3, v10, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 636
    :cond_59
    return-void
.end method

.method public final m()Z
    .registers 2

    .prologue
    .line 827
    iget-boolean v0, p0, Lcom/google/android/youtube/core/b/af;->v:Z

    return v0
.end method

.method public final n()V
    .registers 3

    .prologue
    .line 854
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 855
    return-void
.end method

.method public final o()V
    .registers 3

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 859
    return-void
.end method

.method public final p()V
    .registers 3

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 863
    return-void
.end method

.method public final q()V
    .registers 3

    .prologue
    .line 873
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 874
    return-void
.end method

.method public final r()V
    .registers 3

    .prologue
    .line 877
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 878
    return-void
.end method

.method public final s()V
    .registers 3

    .prologue
    .line 885
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 886
    return-void
.end method

.method public final t()V
    .registers 3

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/youtube/core/b/af;->j:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 890
    return-void
.end method
