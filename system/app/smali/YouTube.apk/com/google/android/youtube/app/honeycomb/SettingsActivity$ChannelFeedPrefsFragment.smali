.class public Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private b:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 364
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 371
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 372
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "youtube"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 373
    const v0, 0x7f060001

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->addPreferencesFromResource(I)V

    .line 375
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 377
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "channel_feed_content"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->b:Landroid/preference/ListPreference;

    .line 379
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "channel_feed_content"

    const-string v2, "all_activity"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384
    const-string v1, "all_activity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 385
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b0149

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 391
    :goto_53
    return-void

    .line 388
    :cond_54
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b014a

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_53
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 394
    check-cast p2, Ljava/lang/String;

    .line 395
    const-string v0, "all_activity"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 396
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b0149

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 402
    :goto_16
    const/4 v0, 0x1

    return v0

    .line 399
    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b014a

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivity$ChannelFeedPrefsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_16
.end method
