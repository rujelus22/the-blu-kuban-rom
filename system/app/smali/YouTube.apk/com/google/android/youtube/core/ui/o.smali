.class public final Lcom/google/android/youtube/core/ui/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/o;->a:Landroid/content/Context;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/o;->b:Landroid/app/Dialog;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/youtube/core/ui/o;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/o;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 64
    :cond_11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/o;->b:Landroid/app/Dialog;

    .line 65
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/youtube/core/ui/q;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 38
    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-virtual {p0}, Lcom/google/android/youtube/core/ui/o;->a()V

    .line 41
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/core/ui/o;->a:Landroid/content/Context;

    const v2, 0x1090011

    invoke-direct {v0, v1, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 44
    new-instance v1, Lcom/google/android/youtube/core/ui/p;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/youtube/core/ui/p;-><init>(Lcom/google/android/youtube/core/ui/o;Landroid/widget/ArrayAdapter;Lcom/google/android/youtube/core/ui/q;)V

    .line 53
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/youtube/core/ui/o;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b0034

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/ui/o;->b:Landroid/app/Dialog;

    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/core/ui/o;->b:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 58
    return-void
.end method
