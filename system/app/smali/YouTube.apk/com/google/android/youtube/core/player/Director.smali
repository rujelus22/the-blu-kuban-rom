.class public final Lcom/google/android/youtube/core/player/Director;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/ai;
.implements Lcom/google/android/youtube/core/player/b;
.implements Lcom/google/android/youtube/core/player/bw;
.implements Lcom/google/android/youtube/core/player/d;


# instance fields
.field private A:Lcom/google/android/youtube/core/model/v;

.field private B:Lcom/google/android/youtube/core/model/VastAd;

.field private C:Z

.field private D:Lcom/google/android/youtube/core/model/VastAd;

.field private E:Z

.field private F:Ljava/lang/String;

.field private G:Z

.field private H:Z

.field private I:Lcom/google/android/youtube/core/async/a/a;

.field private J:Lcom/google/android/youtube/core/async/n;

.field private K:Z

.field private L:Z

.field private M:Z

.field private final N:Lcom/google/android/youtube/core/player/e;

.field private O:Z

.field private final P:Lcom/google/android/youtube/core/player/aa;

.field private final Q:Ljava/lang/String;

.field private volatile R:Z

.field private final S:Landroid/widget/Toast;

.field private T:Lcom/google/android/youtube/core/async/l;

.field private U:Lcom/google/android/youtube/core/async/l;

.field private V:Lcom/google/android/youtube/core/async/l;

.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/youtube/core/player/bx;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/youtube/core/Analytics;

.field private final e:Lcom/google/android/youtube/core/b/al;

.field private final f:Lcom/google/android/youtube/core/utils/l;

.field private final g:[Landroid/view/View;

.field private final h:Lcom/google/android/youtube/core/player/ControllerOverlay;

.field private final i:Lcom/google/android/youtube/core/player/a;

.field private final j:Lcom/google/android/youtube/core/player/ah;

.field private final k:Lcom/google/android/youtube/core/player/be;

.field private final l:Lcom/google/android/youtube/core/player/az;

.field private final m:Lcom/google/android/youtube/core/player/f;

.field private final n:Lcom/google/android/youtube/core/player/c;

.field private final o:Lcom/google/android/youtube/core/player/bs;

.field private final p:Lcom/google/android/youtube/core/b/a;

.field private final q:Lcom/google/android/youtube/core/b/au;

.field private final r:Lcom/google/android/youtube/core/player/ax;

.field private final s:Lcom/google/android/youtube/core/d;

.field private final t:Z

.field private u:Z

.field private v:Z

.field private w:I

.field private x:I

.field private y:Ljava/lang/String;

.field private z:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/be;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;Z)V
    .registers 33
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 251
    const-string v1, "player cannot be null"

    invoke-static {p1, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/bx;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    .line 252
    const-string v1, "context cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    .line 253
    const-string v1, "preferences cannot be null"

    invoke-static {p3, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    const-string v1, "gdataClient cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/b/al;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/b/al;

    .line 255
    iput-object p6, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/b/a;

    .line 256
    const-string v1, "statsClient cannot be null"

    move-object/from16 v0, p7

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/b/au;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    .line 257
    const-string v1, "subtitlesClient cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    const-string v1, "adultContentHelper cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/c;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/c;

    .line 259
    const-string v1, "analytics cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    .line 260
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Q:Ljava/lang/String;

    .line 261
    const-string v1, "listener cannot be null"

    move-object/from16 v0, p12

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/aa;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    .line 262
    const-string v1, "controllerOverlay cannot be null"

    move-object/from16 v0, p13

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/ControllerOverlay;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    .line 263
    const-string v1, "brandingOverlay cannot be null"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/be;

    .line 265
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    .line 266
    const-string v1, "liveOverlay cannot be null"

    move-object/from16 v0, p17

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/ah;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/player/ah;

    .line 267
    const-string v1, "subtitleOverlay cannot be null"

    move-object/from16 v0, p18

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    const-string v1, "errorHelper cannot be null"

    move-object/from16 v0, p19

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/d;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/d;

    .line 269
    const-string v1, "networkStatus cannot be null"

    move-object/from16 v0, p20

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/utils/l;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->f:Lcom/google/android/youtube/core/utils/l;

    .line 270
    const-string v1, "streamSelector cannot be null"

    move-object/from16 v0, p21

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/ax;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->r:Lcom/google/android/youtube/core/player/ax;

    .line 271
    const-string v1, "autoplayHelper cannot be null"

    move-object/from16 v0, p22

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/player/e;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->N:Lcom/google/android/youtube/core/player/e;

    .line 272
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->t:Z

    .line 274
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/youtube/core/player/ab;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/youtube/core/player/ab;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    .line 276
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Z

    .line 279
    if-eqz p16, :cond_193

    const/4 v1, 0x3

    :goto_da
    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->g:[Landroid/view/View;

    .line 281
    const/4 v1, 0x0

    move-object/from16 v0, p13

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    .line 282
    new-instance v1, Lcom/google/android/youtube/core/player/y;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/core/player/y;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    move-object/from16 v0, p13

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setListener(Lcom/google/android/youtube/core/player/k;)V

    .line 283
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->g:[Landroid/view/View;

    const/4 v3, 0x0

    const/4 v1, 0x1

    invoke-interface/range {p13 .. p13}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a()Landroid/view/View;

    move-result-object v4

    aput-object v4, v2, v3

    .line 285
    if-eqz p16, :cond_10a

    .line 286
    move-object/from16 v0, p16

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/a;->setListener(Lcom/google/android/youtube/core/player/b;)V

    .line 287
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->g:[Landroid/view/View;

    const/4 v3, 0x1

    const/4 v1, 0x2

    invoke-interface/range {p16 .. p16}, Lcom/google/android/youtube/core/player/a;->a()Landroid/view/View;

    move-result-object v4

    aput-object v4, v2, v3

    :cond_10a
    move v8, v1

    .line 290
    move-object/from16 v0, p17

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/ah;->setListener(Lcom/google/android/youtube/core/player/ai;)V

    .line 292
    const-string v1, "default_hq"

    const/4 v2, 0x0

    invoke-interface {p3, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_196

    const/4 v1, 0x1

    :goto_11a
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    .line 293
    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    move-object/from16 v0, p13

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHQ(Z)V

    .line 295
    new-instance v1, Lcom/google/android/youtube/core/player/az;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/youtube/core/player/ac;

    const/4 v3, 0x0

    invoke-direct {v5, p0, v3}, Lcom/google/android/youtube/core/player/ac;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    const v3, 0x7f0b0035

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v3, p3

    move-object/from16 v4, p18

    move-object/from16 v6, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/core/player/az;-><init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/player/ba;Lcom/google/android/youtube/core/b/as;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/az;

    .line 298
    new-instance v1, Lcom/google/android/youtube/core/player/f;

    new-instance v5, Lcom/google/android/youtube/core/player/x;

    const/4 v2, 0x0

    invoke-direct {v5, p0, v2}, Lcom/google/android/youtube/core/player/x;-><init>(Lcom/google/android/youtube/core/player/Director;B)V

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    move-object v2, p4

    move-object v3, p5

    move-object/from16 v4, p14

    invoke-direct/range {v1 .. v6}, Lcom/google/android/youtube/core/player/f;-><init>(Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/h;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/f;

    .line 301
    new-instance v1, Lcom/google/android/youtube/core/player/bs;

    const-string v2, "warning_3d"

    const v3, 0x7f0b000c

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p2, p0, v2, v3}, Lcom/google/android/youtube/core/player/bs;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/bw;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/bs;

    .line 304
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->g:[Landroid/view/View;

    invoke-interface/range {p17 .. p17}, Lcom/google/android/youtube/core/player/ah;->a()Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v8

    .line 306
    const-string v1, ""

    const/4 v2, 0x1

    invoke-static {p2, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->S:Landroid/widget/Toast;

    .line 308
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/youtube/core/player/u;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/core/player/u;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->T:Lcom/google/android/youtube/core/async/l;

    new-instance v1, Lcom/google/android/youtube/core/player/v;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/v;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->U:Lcom/google/android/youtube/core/async/l;

    new-instance v1, Lcom/google/android/youtube/core/player/w;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/w;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->V:Lcom/google/android/youtube/core/async/l;

    .line 309
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/core/player/bx;->a(Landroid/os/Handler;)V

    .line 310
    return-void

    .line 279
    :cond_193
    const/4 v1, 0x2

    goto/16 :goto_da

    .line 292
    :cond_196
    invoke-interface/range {p21 .. p21}, Lcom/google/android/youtube/core/player/ax;->a()Z

    move-result v1

    goto :goto_11a
.end method

.method private A()V
    .registers 2

    .prologue
    .line 645
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    .line 646
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    if-eqz v0, :cond_c

    .line 647
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/a;->c()V

    .line 649
    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->i()V

    .line 650
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()V

    .line 651
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/b/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/a;->a()V

    .line 652
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->i()V

    .line 654
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    .line 655
    return-void
.end method

.method private B()V
    .registers 3

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 659
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->LIVE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    .line 667
    :goto_13
    return-void

    .line 660
    :cond_14
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isMovie()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 661
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->MOVIE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    goto :goto_13

    .line 662
    :cond_28
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->t:Z

    if-eqz v0, :cond_34

    .line 663
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->EMBEDDED:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    goto :goto_13

    .line 665
    :cond_34
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->YOUTUBE:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    goto :goto_13
.end method

.method private C()V
    .registers 5

    .prologue
    .line 670
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 672
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    .line 673
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    if-eqz v0, :cond_59

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->b:Lcom/google/android/youtube/core/model/Stream;

    .line 674
    :goto_e
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()V

    .line 675
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/f;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/f;->a()V

    .line 676
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v1

    if-nez v1, :cond_2e

    .line 677
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/model/v;->a:Z

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setSupportsQualityToggle(Z)V

    .line 678
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHQ(Z)V

    .line 680
    :cond_2e
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/aa;->b(Lcom/google/android/youtube/core/model/Video;)V

    .line 681
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Stream;->quality:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/b/au;->a(Lcom/google/android/youtube/core/model/Stream$Quality;)V

    .line 682
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    iget-boolean v3, v0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/b/au;->a(Landroid/net/Uri;Z)V

    .line 683
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    .line 684
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v1

    if-eqz v1, :cond_5e

    .line 685
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/model/Stream;)V

    .line 689
    :goto_58
    return-void

    .line 673
    :cond_59
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->c:Lcom/google/android/youtube/core/model/Stream;

    goto :goto_e

    .line 687
    :cond_5e
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    iget v2, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_58
.end method

.method private D()V
    .registers 3

    .prologue
    .line 711
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Z

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 712
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->S:Landroid/widget/Toast;

    const v1, 0x7f0b0067

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    .line 713
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->S:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 723
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/youtube/core/player/t;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/player/t;-><init>(Lcom/google/android/youtube/core/player/Director;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 731
    :cond_23
    return-void
.end method

.method private E()V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 794
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 795
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Next"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 796
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->NEXT:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 798
    const/4 v1, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/player/Director;->a(ZIILcom/google/android/youtube/core/model/VastAd;Z)V

    .line 800
    :cond_1c
    return-void
.end method

.method private F()V
    .registers 2

    .prologue
    .line 924
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_9

    .line 925
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->f()V

    .line 927
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/async/n;

    if-eqz v0, :cond_15

    .line 928
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/async/n;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/n;->a()V

    .line 929
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/async/n;

    .line 931
    :cond_15
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/f;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/f;->b()V

    .line 932
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->a()V

    .line 933
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/bs;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bs;->b()V

    .line 934
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/c;->b()V

    .line 935
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->j()V

    .line 936
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput p1, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    return p1
.end method

.method private a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;
    .registers 4
    .parameter

    .prologue
    .line 1213
    invoke-static {p1}, Lcom/google/android/youtube/core/async/n;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/n;

    move-result-object v0

    .line 1214
    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->J:Lcom/google/android/youtube/core/async/n;

    .line 1215
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->a:Landroid/os/Handler;

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/async/ai;->a(Landroid/os/Handler;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/ai;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;
    .registers 5
    .parameter

    .prologue
    .line 949
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->Q:Ljava/lang/String;

    if-eqz v0, :cond_22

    .line 950
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "androidcid"

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 952
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object p1

    .line 954
    :cond_22
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_59

    .line 955
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 956
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object p1

    .line 958
    :cond_59
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->m()Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 959
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "dnc"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 960
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Stream;->buildUpon()Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/youtube/core/model/Stream$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream$Builder;->build()Lcom/google/android/youtube/core/model/Stream;

    move-result-object p1

    .line 962
    :cond_7f
    return-object p1
.end method

.method public static a(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;)Lcom/google/android/youtube/core/player/Director;
    .registers 46
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 218
    const-string v1, "adsClient cannot be null: use createAdFreeDirector"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    const-string v1, "revShareClientId cannot be empty: use createAdFreeDirector"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 220
    const-string v1, "adOverlay cannot be empty: use createAdFreeDirector"

    move-object/from16 v0, p14

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    new-instance v1, Lcom/google/android/youtube/core/player/Director;

    const/16 v16, 0x0

    const/16 v24, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v17, p14

    move-object/from16 v18, p15

    move-object/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    move-object/from16 v22, p19

    move-object/from16 v23, p20

    invoke-direct/range {v1 .. v24}, Lcom/google/android/youtube/core/player/Director;-><init>(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/be;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;Z)V

    return-object v1
.end method

.method public static a(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;)Lcom/google/android/youtube/core/player/Director;
    .registers 42
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 204
    new-instance v0, Lcom/google/android/youtube/core/player/Director;

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v23, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v17, p12

    move-object/from16 v18, p13

    move-object/from16 v19, p14

    move-object/from16 v20, p15

    move-object/from16 v21, p16

    move-object/from16 v22, p17

    invoke-direct/range {v0 .. v23}, Lcom/google/android/youtube/core/player/Director;-><init>(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/be;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;Z)V

    return-object v0
.end method

.method private a(IILcom/google/android/youtube/core/model/VastAd;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 395
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->F()V

    .line 397
    iput p1, p0, Lcom/google/android/youtube/core/player/Director;->x:I

    .line 398
    iput p2, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    .line 399
    iput-object p3, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    .line 400
    iput-boolean p4, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    .line 402
    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    .line 403
    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    .line 404
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    .line 405
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    .line 406
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    .line 407
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->v:Z

    .line 408
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    .line 410
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->LOADING:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 412
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->g()V

    .line 414
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->B()V

    .line 415
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    .line 416
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, p2, v1, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setTimes(III)V

    .line 417
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setLoading()V

    .line 418
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/f;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/f;->b()V

    .line 419
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    if-eqz v0, :cond_44

    .line 420
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/a;->c()V

    .line 422
    :cond_44
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/player/ah;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ah;->c()V

    .line 423
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/be;

    if-eqz v0, :cond_52

    .line 424
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/be;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/be;->c()V

    .line 426
    :cond_52
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/aa;->j()V

    .line 427
    return-void
.end method

.method private a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 6
    .parameter

    .prologue
    .line 569
    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->r:Lcom/google/android/youtube/core/player/ax;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/bx;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/player/ax;->a(Lcom/google/android/youtube/core/model/Video;Ljava/util/Set;)Lcom/google/android/youtube/core/model/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/core/model/v;

    iget-object v2, v0, Lcom/google/android/youtube/core/model/v;->b:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/youtube/core/model/v;->c:Lcom/google/android/youtube/core/model/Stream;

    invoke-direct {p0, v3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Stream;)Lcom/google/android/youtube/core/model/Stream;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/model/v;-><init>(Lcom/google/android/youtube/core/model/Stream;Lcom/google/android/youtube/core/model/Stream;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->b:Lcom/google/android/youtube/core/model/Stream;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Stream;->isHD()Z

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHQisHD(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_32

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->z()V

    .line 573
    :goto_31
    return-void

    .line 569
    :cond_32
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/b/a;

    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/Video;->isMonetized(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_74

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/b/a;

    iget-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/b/a;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/b/a;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->U:Lcom/google/android/youtube/core/async/l;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/a;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V
    :try_end_5a
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_0 .. :try_end_5a} :catch_5b

    goto :goto_31

    .line 570
    :catch_5b
    move-exception v0

    .line 571
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    new-instance v1, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->NO_STREAMS:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/d;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    goto :goto_31

    .line 569
    :cond_74
    :try_start_74
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V
    :try_end_77
    .catch Lcom/google/android/youtube/core/player/MissingStreamException; {:try_start_74 .. :try_end_77} :catch_5b

    goto :goto_31
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/LiveEvent;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/LiveEvent;->isUpcoming()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->j:Lcom/google/android/youtube/core/player/ah;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ah;->a(Lcom/google/android/youtube/core/model/LiveEvent;)V

    :goto_15
    return-void

    :cond_16
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_15
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/model/Video;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 65
    if-nez p1, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->EMPTY_PLAYLIST:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    :goto_b
    return-void

    :cond_c
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-nez v0, :cond_24

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Z

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->N:Lcom/google/android/youtube/core/player/e;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/e;->b(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->AUTOPLAY_DENIED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    goto :goto_b

    :cond_24
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget v3, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    iget v4, p1, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v4, v4, 0x3e8

    invoke-interface {v0, v3, v4, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setTimes(III)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v3}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasNext(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v3}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasPrevious(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/b/au;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->F:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_62

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->i()V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->F:Ljava/lang/String;

    :cond_62
    iput-object p1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/az;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->m:Lcom/google/android/youtube/core/player/f;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/f;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PLAYABLE:Lcom/google/android/youtube/core/model/Video$State;

    if-eq v0, v3, :cond_85

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v3, :cond_bb

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_bb

    :cond_85
    move v0, v2

    :goto_86
    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/Director;->t:Z

    if-eqz v3, :cond_92

    iget-boolean v3, p0, Lcom/google/android/youtube/core/player/Director;->t:Z

    if-eqz v3, :cond_bd

    iget-boolean v3, p1, Lcom/google/android/youtube/core/model/Video;->embedAllowed:Z

    if-eqz v3, :cond_bd

    :cond_92
    move v3, v2

    :goto_93
    if-eqz v0, :cond_97

    if-nez v3, :cond_ff

    :cond_97
    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v4, "PlayCannotProceeed"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;Z)V

    iput-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    if-eqz v0, :cond_bf

    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->EMBEDDING_DISABLED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    const v4, 0x7f0b004a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;)V

    :goto_b3
    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;Z)V

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->D()V

    goto/16 :goto_b

    :cond_bb
    move v0, v1

    goto :goto_86

    :cond_bd
    move v3, v1

    goto :goto_93

    :cond_bf
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    sget-object v3, Lcom/google/android/youtube/core/model/Video$State;->PROCESSING:Lcom/google/android/youtube/core/model/Video$State;

    if-ne v0, v3, :cond_ed

    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v3, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->BAD_STATE:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    const-string v4, "%s\n%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    iget-object v7, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v7, v7, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    iget-object v6, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    const v7, 0x7f0b0042

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;)V

    goto :goto_b3

    :cond_ed
    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v2, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->BAD_STATE:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v3, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    iget v4, v4, Lcom/google/android/youtube/core/model/Video$State;->explanationId:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;)V

    goto :goto_b3

    :cond_ff
    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Video;->is3d:Z

    if-eqz v0, :cond_112

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->f:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->f()Z

    move-result v0

    if-nez v0, :cond_112

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->o:Lcom/google/android/youtube/core/player/bs;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bs;->a()V

    goto/16 :goto_b

    :cond_112
    iget-boolean v0, p1, Lcom/google/android/youtube/core/model/Video;->adultContent:Z

    if-eqz v0, :cond_11d

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/c;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/c;->a(Lcom/google/android/youtube/core/player/d;)V

    goto/16 :goto_b

    :cond_11d
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->x()V

    goto/16 :goto_b
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/player/DirectorException;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v1, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/d;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v1}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasNext(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v1}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHasPrevious(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Ljava/lang/String;Lcom/google/android/youtube/core/model/VastAd;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p2, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/b/au;->a(Lcom/google/android/youtube/core/model/VastAd;)V

    if-eqz p2, :cond_b

    iput-object p2, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    :cond_b
    if-eqz p2, :cond_17

    invoke-virtual {p2}, Lcom/google/android/youtube/core/model/VastAd;->isDummy()Z

    move-result v0

    if-nez v0, :cond_17

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->z()V

    :goto_16
    return-void

    :cond_17
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    goto :goto_16
.end method

.method private a(Lcom/google/android/youtube/core/player/DirectorException;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 734
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PlayCannotProceeed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/Stream$Quality;Z)V

    .line 735
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->a(Ljava/lang/String;Z)V

    .line 736
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/DirectorException;)V

    .line 737
    return-void
.end method

.method private a(ZIILcom/google/android/youtube/core/model/VastAd;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 431
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p3, v1, p5}, Lcom/google/android/youtube/core/player/Director;->a(IILcom/google/android/youtube/core/model/VastAd;Z)V

    .line 433
    if-eqz p1, :cond_d

    .line 434
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->a()V

    .line 438
    :goto_c
    return-void

    .line 436
    :cond_d
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->b()V

    goto :goto_c
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/Director;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Exception;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/Director;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/Analytics;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/Director;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/youtube/core/player/DirectorException;

    sget-object v1, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/d;

    invoke-virtual {v2, p1}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/youtube/core/player/DirectorException;-><init>(Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/DirectorException;Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/Director;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->v:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/b/au;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/Director;Z)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/ControllerOverlay;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/be;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/be;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->v:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/aa;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->O:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/e;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->N:Lcom/google/android/youtube/core/player/e;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/az;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/az;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->M:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/a;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/youtube/core/player/Director;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    return v0
.end method

.method static synthetic r(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/model/v;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/youtube/core/player/Director;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->A()V

    return-void
.end method

.method static synthetic t(Lcom/google/android/youtube/core/player/Director;)V
    .registers 3
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 65
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_ENDED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_17

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->O:Z

    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    :goto_16
    return-void

    :cond_17
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    :goto_24
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    goto :goto_16

    :cond_2c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->g()V

    goto :goto_24
.end method

.method static synthetic u(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/utils/l;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->f:Lcom/google/android/youtube/core/utils/l;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/youtube/core/player/Director;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/youtube/core/player/Director;)Lcom/google/android/youtube/core/player/bx;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    return-object v0
.end method

.method private x()V
    .registers 3

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->LOADED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 519
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-nez v0, :cond_c

    .line 520
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    .line 531
    :cond_b
    :goto_b
    return-void

    .line 522
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    if-eqz v0, :cond_21

    .line 523
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->g()V

    .line 527
    :goto_15
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/be;

    if-eqz v0, :cond_b

    .line 528
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->k:Lcom/google/android/youtube/core/player/be;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/be;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_b

    .line 525
    :cond_21
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->c()V

    goto :goto_15
.end method

.method static synthetic x(Lcom/google/android/youtube/core/player/Director;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->C()V

    return-void
.end method

.method private y()V
    .registers 4

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_a

    .line 535
    const-string v0, "startVideo called without a video."

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    .line 545
    :goto_9
    return-void

    .line 538
    :cond_a
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setLoading()V

    .line 540
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 541
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->e:Lcom/google/android/youtube/core/b/al;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->liveEventUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->V:Lcom/google/android/youtube/core/async/l;

    invoke-direct {p0, v2}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/l;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/al;->b(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    goto :goto_9

    .line 543
    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Video;)V

    goto :goto_9
.end method

.method private z()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 621
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 622
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    .line 623
    iget v0, p0, Lcom/google/android/youtube/core/player/Director;->x:I

    if-nez v0, :cond_f

    .line 624
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->AD_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 626
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/model/VastAd;)V

    .line 627
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/ControllerOverlay;->i()V

    .line 628
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    sget-object v1, Lcom/google/android/youtube/core/player/ControllerOverlay$Style;->AD:Lcom/google/android/youtube/core/player/ControllerOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setStyle(Lcom/google/android/youtube/core/player/ControllerOverlay$Style;)V

    .line 629
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    if-eqz v0, :cond_2d

    .line 630
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/a;->a(Lcom/google/android/youtube/core/model/VastAd;)V

    .line 633
    :cond_2d
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->a()V

    .line 634
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/b/au;->a(Landroid/net/Uri;Z)V

    .line 636
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    .line 637
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    new-instance v1, Lcom/google/android/youtube/core/model/Stream;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/youtube/core/model/Stream$Quality;->UNKNOWN:Lcom/google/android/youtube/core/model/Stream$Quality;

    invoke-direct {v1, v2, v3}, Lcom/google/android/youtube/core/model/Stream;-><init>(Landroid/net/Uri;Lcom/google/android/youtube/core/model/Stream$Quality;)V

    iget v2, p0, Lcom/google/android/youtube/core/player/Director;->x:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    .line 638
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 854
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    if-eqz v0, :cond_1d

    .line 855
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->p:Lcom/google/android/youtube/core/b/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/a;->a()V

    .line 856
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->e()V

    .line 857
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/content/Context;Landroid/net/Uri;)V

    .line 859
    :cond_1d
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 813
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-eqz v0, :cond_11

    .line 814
    iput p1, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    .line 818
    :cond_6
    :goto_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_1b

    const/4 v0, -0x1

    :goto_b
    if-ge p1, v0, :cond_10

    .line 819
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->j()V

    .line 821
    :cond_10
    return-void

    .line 815
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    if-nez v0, :cond_6

    .line 816
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/bx;->a(I)V

    goto :goto_6

    .line 818
    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-nez v0, :cond_27

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    if-nez v0, :cond_27

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-nez v0, :cond_2e

    :cond_27
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    mul-int/lit16 v0, v0, 0x3e8

    goto :goto_b

    :cond_2e
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->d()I

    move-result v0

    goto :goto_b
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 366
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;ZI)V

    .line 367
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/model/UserAuth;ZI)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 344
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p3, p4, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;ZILjava/lang/String;)V

    .line 345
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/a/a;ZILjava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 323
    if-ltz p3, :cond_26

    move v0, v1

    :goto_5
    const-string v3, "startPosition has to be >= 0"

    invoke-static {v0, v3}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 325
    iput-object p4, p0, Lcom/google/android/youtube/core/player/Director;->y:Ljava/lang/String;

    .line 326
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/a/a;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    .line 328
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->h()V

    .line 329
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->T:Lcom/google/android/youtube/core/async/l;

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/async/a/a;->a(Lcom/google/android/youtube/core/async/l;)V

    .line 331
    const/4 v4, 0x0

    move-object v0, p0

    move v3, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/player/Director;->a(ZIILcom/google/android/youtube/core/model/VastAd;Z)V

    .line 332
    return-void

    :cond_26
    move v0, v2

    .line 323
    goto :goto_5
.end method

.method public final a(Ljava/lang/Exception;)V
    .registers 4
    .parameter

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->s:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 904
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->ADULT_CONTENT_ERROR:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    .line 905
    return-void
.end method

.method public final a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 386
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->g()Z

    move-result v0

    const-string v1, "call init() first"

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->b(ZLjava/lang/Object;)V

    .line 388
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->h()V

    .line 389
    iget v0, p0, Lcom/google/android/youtube/core/player/Director;->x:I

    iget v1, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/youtube/core/player/Director;->a(IILcom/google/android/youtube/core/model/VastAd;Z)V

    .line 390
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->c()V

    .line 391
    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 968
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 863
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->g()V

    .line 865
    :cond_11
    return-void
.end method

.method public final b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 976
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->G:Z

    if-eq p1, v0, :cond_19

    .line 977
    iput-boolean p1, p0, Lcom/google/android/youtube/core/player/Director;->G:Z

    .line 978
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setFullscreen(Z)V

    .line 979
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    if-eqz v0, :cond_14

    .line 980
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->i:Lcom/google/android/youtube/core/player/a;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/a;->setFullscreen(Z)V

    .line 982
    :cond_14
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/aa;->b(Z)V

    .line 984
    :cond_19
    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 972
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 869
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->h()V

    .line 870
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->A()V

    .line 872
    :cond_14
    return-void
.end method

.method public final c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 987
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setShowFullscreen(Z)V

    .line 988
    return-void
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 876
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->f()V

    .line 877
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->D:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/VastAd;->adVideoId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 879
    :cond_1c
    return-void
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 895
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->x()V

    .line 896
    return-void
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 899
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->ADULT_CONTENT_DECLINED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    .line 900
    return-void
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 565
    return-void
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 641
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    return v0
.end method

.method public final j()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 740
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    if-nez v0, :cond_d

    .line 741
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->VIDEO_STARTED:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 743
    :cond_d
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    .line 745
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-nez v0, :cond_17

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-nez v0, :cond_2b

    :cond_17
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->B:Lcom/google/android/youtube/core/model/VastAd;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/VastAd;->streamUri:Landroid/net/Uri;

    if-eqz v0, :cond_2b

    .line 746
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->z()V

    .line 752
    :cond_28
    :goto_28
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    .line 753
    return-void

    .line 747
    :cond_2b
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-nez v0, :cond_33

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-nez v0, :cond_37

    .line 748
    :cond_33
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->y()V

    goto :goto_28

    .line 749
    :cond_37
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->b()Z

    move-result v0

    if-nez v0, :cond_28

    .line 750
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->e()V

    goto :goto_28
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 756
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 757
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 758
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->f()V

    .line 760
    :cond_10
    return-void
.end method

.method public final l()V
    .registers 2

    .prologue
    .line 772
    invoke-static {}, Lcom/google/android/youtube/core/L;->e()V

    .line 773
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->b()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 774
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->k()V

    .line 778
    :goto_e
    return-void

    .line 776
    :cond_f
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/Director;->j()V

    goto :goto_e
.end method

.method public final m()V
    .registers 2

    .prologue
    .line 789
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->H:Z

    .line 790
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->E()V

    .line 791
    return-void
.end method

.method public final n()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 803
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->H:Z

    .line 804
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->I:Lcom/google/android/youtube/core/async/a/a;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 805
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Previous"

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 806
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v0, Lcom/google/android/youtube/core/player/Director$LifecycleEvent;->PREVIOUS:Lcom/google/android/youtube/core/player/Director$LifecycleEvent;

    .line 808
    const/4 v4, 0x0

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/player/Director;->a(ZIILcom/google/android/youtube/core/model/VastAd;Z)V

    .line 810
    :cond_1e
    return-void
.end method

.method public final o()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 828
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->R:Z

    .line 829
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    .line 830
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    if-nez v0, :cond_25

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    .line 831
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    iget-boolean v2, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setHQ(Z)V

    .line 832
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v3, "HQ"

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    if-eqz v0, :cond_27

    const-string v0, "On"

    :goto_1d
    invoke-virtual {v2, v3, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    if-nez v0, :cond_2a

    .line 842
    :goto_24
    return-void

    :cond_25
    move v0, v1

    .line 830
    goto :goto_a

    .line 832
    :cond_27
    const-string v0, "Off"

    goto :goto_1d

    .line 837
    :cond_2a
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->E:Z

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->b:Lcom/google/android/youtube/core/model/Stream;

    .line 838
    :goto_32
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    iget-object v3, v0, Lcom/google/android/youtube/core/model/Stream;->uri:Landroid/net/Uri;

    iget-boolean v4, v0, Lcom/google/android/youtube/core/model/Stream;->isOffline:Z

    invoke-interface {v2, v3, v4}, Lcom/google/android/youtube/core/b/au;->a(Landroid/net/Uri;Z)V

    .line 840
    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->h:Lcom/google/android/youtube/core/player/ControllerOverlay;

    invoke-interface {v2, v1}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setScrubbingEnabled(Z)V

    .line 841
    iget-object v1, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    iget-object v2, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/bx;->c()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/core/player/bx;->a(Lcom/google/android/youtube/core/model/Stream;I)V

    goto :goto_24

    .line 837
    :cond_4c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->A:Lcom/google/android/youtube/core/model/v;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/v;->c:Lcom/google/android/youtube/core/model/Stream;

    goto :goto_32
.end method

.method public final p()V
    .registers 3

    .prologue
    .line 845
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->d:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "CC"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 846
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->b()V

    .line 847
    return-void
.end method

.method public final q()[Landroid/view/View;
    .registers 2

    .prologue
    .line 850
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->g:[Landroid/view/View;

    return-object v0
.end method

.method public final r()V
    .registers 2

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget-boolean v0, v0, Lcom/google/android/youtube/core/model/Video;->adultContent:Z

    if-eqz v0, :cond_c

    .line 884
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->n:Lcom/google/android/youtube/core/player/c;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/core/player/c;->a(Lcom/google/android/youtube/core/player/d;)V

    .line 888
    :goto_b
    return-void

    .line 886
    :cond_c
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->x()V

    goto :goto_b
.end method

.method public final s()V
    .registers 3

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->P:Lcom/google/android/youtube/core/player/aa;

    sget-object v1, Lcom/google/android/youtube/core/player/Director$StopReason;->BANDWIDTH_WARNING_DECLINED:Lcom/google/android/youtube/core/player/Director$StopReason;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/player/aa;->a(Lcom/google/android/youtube/core/player/Director$StopReason;)V

    .line 892
    return-void
.end method

.method public final t()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 908
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-nez v0, :cond_9

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-eqz v0, :cond_15

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->b()Z

    move-result v0

    if-eqz v0, :cond_34

    :cond_15
    move v0, v1

    :goto_16
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->L:Z

    .line 909
    iput-boolean v1, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    .line 910
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    if-eqz v0, :cond_36

    .line 911
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director;->x:I

    .line 915
    :goto_26
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->k()V

    .line 916
    invoke-direct {p0}, Lcom/google/android/youtube/core/player/Director;->F()V

    .line 917
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->h()V

    .line 918
    return-void

    .line 908
    :cond_34
    const/4 v0, 0x0

    goto :goto_16

    .line 913
    :cond_36
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    goto :goto_26
.end method

.method public final u()V
    .registers 2

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->l:Lcom/google/android/youtube/core/player/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/az;->c()V

    .line 940
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->i()V

    .line 941
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->q:Lcom/google/android/youtube/core/b/au;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/au;->d()V

    .line 942
    return-void
.end method

.method public final v()Ljava/lang/String;
    .registers 2

    .prologue
    .line 991
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->z:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    goto :goto_5
.end method

.method public final w()I
    .registers 2

    .prologue
    .line 1007
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->C:Z

    if-eqz v0, :cond_6

    .line 1008
    const/4 v0, 0x0

    .line 1012
    :goto_5
    return v0

    .line 1009
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->K:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/Director;->u:Z

    if-nez v0, :cond_11

    .line 1010
    :cond_e
    iget v0, p0, Lcom/google/android/youtube/core/player/Director;->w:I

    goto :goto_5

    .line 1012
    :cond_11
    iget-object v0, p0, Lcom/google/android/youtube/core/player/Director;->b:Lcom/google/android/youtube/core/player/bx;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/bx;->c()I

    move-result v0

    goto :goto_5
.end method
