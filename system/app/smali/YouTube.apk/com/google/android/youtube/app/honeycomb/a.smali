.class public Lcom/google/android/youtube/app/honeycomb/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/YouTubePlatformUtil;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/content/Context;)Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/youtube/core/j;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;->OriginalTablet:Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    :goto_c
    return-object v0

    :cond_d
    sget-object v0, Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;->Guide:Lcom/google/android/youtube/app/YouTubePlatformUtil$UiType;

    goto :goto_c
.end method

.method public final a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 54
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_c

    .line 55
    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/widget/WidgetProvider;->a(Landroid/content/Context;)V

    .line 59
    :goto_b
    return-void

    .line 57
    :cond_c
    invoke-static {p1}, Lcom/google/android/youtube/app/froyo/widget/WidgetProvider;->a(Landroid/content/Context;)V

    goto :goto_b
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xb

    if-lt v0, v1, :cond_19

    .line 34
    const-string v0, "clipboard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    const/4 v1, 0x0

    invoke-static {v1, p2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 38
    :goto_18
    return-void

    .line 36
    :cond_19
    const-string v0, "clipboard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    invoke-virtual {v0, p2}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_18
.end method

.method public final a()[B
    .registers 2

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/b;->a:[B

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Lcom/google/android/youtube/app/remote/ao;
    .registers 4
    .parameter

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0xe

    if-lt v0, v1, :cond_e

    .line 76
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/youtube/app/honeycomb/a/a;-><init>(Landroid/content/Context;)V

    .line 78
    :goto_d
    return-object v0

    :cond_e
    new-instance v0, Lcom/google/android/youtube/app/remote/ap;

    invoke-direct {v0}, Lcom/google/android/youtube/app/remote/ap;-><init>()V

    goto :goto_d
.end method

.method public final b()[B
    .registers 2

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/b;->b:[B

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    const-string v0, "AIzaSyA8eiZmM1FaDVjRy-df2KTyQ_vz_yYM39w"

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    const-string v0, "414843287017.apps.googleusercontent.com"

    return-object v0
.end method
