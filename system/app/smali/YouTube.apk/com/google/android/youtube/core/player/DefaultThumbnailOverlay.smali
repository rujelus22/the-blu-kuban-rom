.class public Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/player/be;


# instance fields
.field private final a:Lcom/google/android/youtube/core/b/an;

.field private b:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;)Lcom/google/android/youtube/core/model/Video;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;->b:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 39
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 5
    .parameter

    .prologue
    .line 47
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;->b:Lcom/google/android/youtube/core/model/Video;

    .line 48
    new-instance v0, Lcom/google/android/youtube/core/player/r;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/r;-><init>(Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/bv;->a(Landroid/view/View;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/bv;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;->a:Lcom/google/android/youtube/core/b/an;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/Video;->hqThumbnailUri:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, Lcom/google/android/youtube/core/b/an;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/async/l;)V

    .line 51
    return-void
.end method

.method public final b()Landroid/widget/RelativeLayout$LayoutParams;
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 43
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 54
    iput-object v1, p0, Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;->b:Lcom/google/android/youtube/core/model/Video;

    .line 55
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;->setVisibility(I)V

    .line 56
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/player/DefaultThumbnailOverlay;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 57
    return-void
.end method
