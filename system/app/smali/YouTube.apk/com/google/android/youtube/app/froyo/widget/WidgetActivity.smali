.class public Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    .line 76
    if-nez v0, :cond_9

    .line 79
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->finish()V

    .line 83
    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 53
    const-string v3, "com.google.android.youtube.action.widget_camera"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 54
    const-string v1, "WidgetCamera"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 55
    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/app/Activity;)V

    .line 70
    :goto_26
    return-void

    .line 56
    :cond_27
    const-string v3, "com.google.android.youtube.action.widget_search"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 57
    const-string v1, "WidgetSearch"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 58
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_26

    .line 59
    :cond_3c
    const-string v3, "com.google.android.youtube.action.widget_home"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_51

    .line 60
    const-string v1, "WidgetLogo"

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    .line 61
    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/phone/HomeActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_26

    .line 62
    :cond_51
    const-string v3, "com.google.android.youtube.action.widget_play"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6e

    .line 63
    sget-object v2, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Widget:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    .line 64
    const-string v0, "video_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/google/android/youtube/app/m;->N:Lcom/google/android/youtube/core/b/aq;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/youtube/core/b/aq;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_26

    .line 67
    :cond_6e
    const-string v0, "missing a widget launch action"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/youtube/app/froyo/widget/WidgetActivity;->finish()V

    goto :goto_26
.end method
