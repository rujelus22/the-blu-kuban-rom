.class final Lcom/google/android/youtube/app/remote/bn;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/utils/l;

.field final synthetic b:Lcom/google/android/youtube/app/remote/bl;

.field final synthetic c:Lcom/google/android/youtube/app/remote/bb;

.field final synthetic d:Lcom/google/android/youtube/app/remote/bm;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/remote/bl;Lcom/google/android/youtube/app/remote/bb;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    iput-object p2, p0, Lcom/google/android/youtube/app/remote/bn;->a:Lcom/google/android/youtube/core/utils/l;

    iput-object p3, p0, Lcom/google/android/youtube/app/remote/bn;->b:Lcom/google/android/youtube/app/remote/bl;

    iput-object p4, p0, Lcom/google/android/youtube/app/remote/bn;->c:Lcom/google/android/youtube/app/remote/bb;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bm;->a(Lcom/google/android/youtube/app/remote/bm;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 130
    :cond_8
    :goto_8
    return-void

    .line 88
    :cond_9
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_e6

    goto :goto_8

    .line 90
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->a:Lcom/google/android/youtube/core/utils/l;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/l;->a()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 91
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->b:Lcom/google/android/youtube/app/remote/bl;

    iget-object v1, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v1}, Lcom/google/android/youtube/app/remote/bm;->b(Lcom/google/android/youtube/app/remote/bm;)Lcom/google/android/youtube/core/async/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/remote/bl;->a(Lcom/google/android/youtube/core/async/l;)V

    .line 100
    :cond_22
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bm;->e(Lcom/google/android/youtube/app/remote/bm;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_8

    .line 93
    :cond_2f
    invoke-static {}, Lcom/google/android/youtube/core/L;->a()V

    .line 94
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bm;->c(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bk;

    .line 95
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bm;->c(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 96
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bm;->d(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 97
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2, v0}, Lcom/google/android/youtube/app/remote/bm;->a(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bk;)V

    goto :goto_3c

    .line 103
    :pswitch_60
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bm;->f(Lcom/google/android/youtube/app/remote/bm;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bm;->e(Lcom/google/android/youtube/app/remote/bm;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_8

    .line 108
    :pswitch_72
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    .line 109
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 110
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_80
    :goto_80
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bk;

    .line 111
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->c:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bb;->w()Lcom/google/android/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/youtube/app/remote/RemoteControl$State;

    if-ne v2, v3, :cond_a2

    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->c:Lcom/google/android/youtube/app/remote/bb;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/remote/bb;->y()Lcom/google/android/youtube/app/remote/bk;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/remote/bk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_80

    .line 113
    :cond_a2
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bm;->g(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 116
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bm;->d(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 117
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2, v0}, Lcom/google/android/youtube/app/remote/bm;->a(Lcom/google/android/youtube/app/remote/bm;Lcom/google/android/youtube/app/remote/bk;)V

    goto :goto_80

    .line 123
    :pswitch_ba
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 124
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/ytremote/model/SsdpId;

    .line 125
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/app/remote/bt;

    .line 126
    iget-object v2, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v2}, Lcom/google/android/youtube/app/remote/bm;->h(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_db

    .line 127
    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/bt;->a()V

    .line 129
    :cond_db
    iget-object v0, p0, Lcom/google/android/youtube/app/remote/bn;->d:Lcom/google/android/youtube/app/remote/bm;

    invoke-static {v0}, Lcom/google/android/youtube/app/remote/bm;->h(Lcom/google/android/youtube/app/remote/bm;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_8

    .line 88
    :pswitch_data_e6
    .packed-switch 0x0
        :pswitch_f
        :pswitch_60
        :pswitch_72
        :pswitch_ba
    .end packed-switch
.end method
