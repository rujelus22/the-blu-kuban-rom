.class public Lcom/google/android/youtube/core/player/DefaultPlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/youtube/core/player/au;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Z

.field private c:Lcom/google/android/youtube/core/player/aw;

.field private d:Lcom/google/android/youtube/core/player/av;

.field private e:Lcom/google/android/youtube/core/player/o;

.field private f:Lcom/google/android/youtube/core/player/aq;

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 48
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    .line 49
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->b()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_22

    const/4 v0, 0x1

    :goto_1f
    iput-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Z

    .line 50
    return-void

    .line 49
    :cond_22
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method static synthetic a(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Lcom/google/android/youtube/core/player/aw;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c:Lcom/google/android/youtube/core/player/aw;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->g:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->h:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)Z
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    return v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .registers 1

    .prologue
    .line 53
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/core/player/aq;)V
    .registers 3
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->f:Lcom/google/android/youtube/core/player/aq;

    .line 80
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/o;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/youtube/core/player/aq;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 81
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/player/av;)V
    .registers 4
    .parameter

    .prologue
    .line 57
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/player/av;

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    .line 58
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/o;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->c()V

    :cond_20
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    .line 59
    :cond_28
    new-instance v0, Lcom/google/android/youtube/core/player/o;

    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/core/player/o;-><init>(Lcom/google/android/youtube/core/player/DefaultPlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    .line 60
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/o;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 61
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 62
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    .line 65
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 171
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    if-eqz v0, :cond_31

    .line 172
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/o;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/o;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/youtube/core/player/o;->layout(IIII)V

    .line 174
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_31

    .line 175
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/youtube/core/player/o;->layout(IIII)V

    .line 178
    :cond_31
    return-void
.end method

.method protected onMeasure(II)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4000

    const/4 v2, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    if-eqz v0, :cond_33

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/o;->measure(II)V

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/o;->getMeasuredWidth()I

    move-result v1

    .line 155
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/o;->getMeasuredHeight()I

    move-result v0

    .line 161
    :goto_18
    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2f

    .line 162
    iget-object v2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 166
    :cond_2f
    invoke-virtual {p0, v1, v0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->setMeasuredDimension(II)V

    .line 167
    return-void

    .line 157
    :cond_33
    invoke-static {v2, p1}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->getDefaultSize(II)I

    move-result v1

    .line 158
    invoke-static {v2, p2}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->getDefaultSize(II)I

    move-result v0

    goto :goto_18
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/youtube/core/player/aw;)V
    .registers 2
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->c:Lcom/google/android/youtube/core/player/aw;

    .line 107
    return-void
.end method

.method public setVideoSize(II)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 88
    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->g:I

    .line 89
    iput p2, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->h:I

    .line 90
    iget-boolean v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->b:Z

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->f:Lcom/google/android/youtube/core/player/aq;

    if-eqz v0, :cond_2c

    .line 91
    :try_start_c
    const-class v0, Landroid/media/MediaPlayer;

    const-string v1, "setVideoScalingMode"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->f:Lcom/google/android/youtube/core/player/aq;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_2c} :catch_32

    .line 93
    :cond_2c
    :goto_2c
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->e:Lcom/google/android/youtube/core/player/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/o;->requestLayout()V

    .line 94
    return-void

    .line 91
    :catch_32
    move-exception v0

    const-string v1, "invoke failed"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2c
.end method

.method public setZoom(I)V
    .registers 3
    .parameter

    .prologue
    .line 122
    iget v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    if-eq v0, p1, :cond_9

    .line 123
    iput p1, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->i:I

    .line 124
    invoke-virtual {p0}, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->requestLayout()V

    .line 126
    :cond_9
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 130
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->b()V

    .line 132
    :cond_9
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 3
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->a()V

    .line 138
    :cond_9
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 4
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    if-eqz v0, :cond_9

    .line 142
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->d:Lcom/google/android/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/youtube/core/player/av;->c()V

    .line 144
    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/core/player/DefaultPlayerSurface;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 145
    return-void
.end method
