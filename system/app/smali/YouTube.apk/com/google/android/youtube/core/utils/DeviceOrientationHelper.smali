.class public final Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;
.super Landroid/view/OrientationEventListener;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Lcom/google/android/youtube/core/utils/h;

.field private final b:Landroid/os/Handler;

.field private final c:Z

.field private d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

.field private e:I

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/utils/h;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 91
    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/utils/h;

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a:Lcom/google/android/youtube/core/utils/h;

    .line 93
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 94
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 95
    if-eqz v0, :cond_2b

    if-ne v0, v4, :cond_3e

    .line 96
    :cond_2b
    if-ne v3, v4, :cond_3c

    move v0, v1

    :goto_2e
    iput-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->c:Z

    .line 101
    :goto_30
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    .line 102
    return-void

    :cond_3c
    move v0, v2

    .line 96
    goto :goto_2e

    .line 98
    :cond_3e
    if-ne v3, v1, :cond_43

    :goto_40
    iput-boolean v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->c:Z

    goto :goto_30

    :cond_43
    move v1, v2

    goto :goto_40
.end method


# virtual methods
.method public final disable()V
    .registers 3

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->f:Z

    .line 152
    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 153
    invoke-super {p0}, Landroid/view/OrientationEventListener;->disable()V

    .line 154
    return-void
.end method

.method public final enable()V
    .registers 2

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UNKNOWN:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    .line 144
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->f:Z

    .line 146
    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    .line 147
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .registers 4
    .parameter

    .prologue
    .line 130
    iget v0, p1, Landroid/os/Message;->what:I

    .line 132
    iget v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_f

    .line 133
    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a:Lcom/google/android/youtube/core/utils/h;

    .line 137
    :goto_9
    iget v0, p1, Landroid/os/Message;->what:I

    iput v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    .line 138
    const/4 v0, 0x1

    return v0

    .line 135
    :cond_f
    iget-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->a:Lcom/google/android/youtube/core/utils/h;

    invoke-interface {v0}, Lcom/google/android/youtube/core/utils/h;->r()V

    goto :goto_9
.end method

.method public final onOrientationChanged(I)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    if-ltz p1, :cond_8

    const/16 v0, 0x1e

    if-le p1, v0, :cond_10

    :cond_8
    const/16 v0, 0x14a

    if-lt p1, v0, :cond_3c

    const/16 v0, 0x168

    if-ge p1, v0, :cond_3c

    :cond_10
    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UPRIGHT:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    .line 108
    :goto_12
    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-eq v0, v1, :cond_39

    .line 109
    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 111
    sget-object v1, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UNKNOWN:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-eq v0, v1, :cond_39

    .line 112
    sget-object v1, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UPRIGHT:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-eq v0, v1, :cond_28

    sget-object v1, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->BOTTOMUP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    if-ne v0, v1, :cond_60

    :cond_28
    move v1, v3

    .line 115
    :goto_29
    iget-boolean v4, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->c:Z

    if-eqz v4, :cond_62

    .line 117
    :goto_2d
    if-eqz v1, :cond_68

    .line 118
    :goto_2f
    iget v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_6a

    .line 119
    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 126
    :cond_39
    :goto_39
    iput-object v0, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->d:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    .line 127
    return-void

    .line 106
    :cond_3c
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_47

    const/16 v0, 0x78

    if-gt p1, v0, :cond_47

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->LEFTONTOP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_12

    :cond_47
    const/16 v0, 0x96

    if-lt p1, v0, :cond_52

    const/16 v0, 0xd2

    if-gt p1, v0, :cond_52

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->BOTTOMUP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_12

    :cond_52
    const/16 v0, 0xf0

    if-lt p1, v0, :cond_5d

    const/16 v0, 0x12c

    if-gt p1, v0, :cond_5d

    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->RIGHTONTOP:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_12

    :cond_5d
    sget-object v0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;->UNKNOWN:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper$DeviceOrientation;

    goto :goto_12

    :cond_60
    move v1, v2

    .line 112
    goto :goto_29

    .line 115
    :cond_62
    if-nez v1, :cond_66

    move v1, v3

    goto :goto_2d

    :cond_66
    move v1, v2

    goto :goto_2d

    :cond_68
    move v3, v2

    .line 117
    goto :goto_2f

    .line 120
    :cond_6a
    iget v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->e:I

    if-eq v1, v3, :cond_39

    .line 121
    iget-object v1, p0, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->b:Landroid/os/Handler;

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_39
.end method
