.class public Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/bb;
.implements Lcom/google/android/youtube/core/async/bn;


# static fields
.field private static final D:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/youtube/app/ui/z;

.field private B:Lcom/google/android/youtube/core/model/Playlist;

.field private C:Lcom/google/android/youtube/app/g;

.field private m:Landroid/content/res/Resources;

.field private n:Lcom/google/android/youtube/core/async/av;

.field private o:Lcom/google/android/youtube/core/b/al;

.field private p:Lcom/google/android/youtube/core/b/an;

.field private q:Lcom/google/android/youtube/core/b/ap;

.field private r:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private s:Lcom/google/android/youtube/core/model/UserAuth;

.field private t:Lcom/google/android/youtube/core/d;

.field private u:Lcom/google/android/youtube/core/j;

.field private v:Lcom/google/android/youtube/app/ui/by;

.field private w:Lcom/google/android/youtube/app/k;

.field private x:Lcom/google/android/youtube/app/ui/ba;

.field private y:Lcom/google/android/youtube/app/adapter/bt;

.field private z:Lcom/google/android/youtube/app/ui/s;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".selectedPlaylist"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->D:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;Lcom/google/android/youtube/core/model/Playlist;)Lcom/google/android/youtube/core/model/Playlist;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V
    .registers 5
    .parameter

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bh;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bh;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bi;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/d;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bi;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/async/l;)V

    invoke-virtual {v1, p0, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/app/ui/ba;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/core/model/Playlist;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/app/adapter/bt;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->y:Lcom/google/android/youtube/app/adapter/bt;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/core/d;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/d;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)Lcom/google/android/youtube/core/b/al;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/b/al;

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/by;

    if-eqz v0, :cond_12

    .line 296
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0d0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 298
    :cond_12
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 91
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    .line 93
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Q()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    .line 94
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/b/al;

    .line 95
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/b/an;

    .line 96
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->w:Lcom/google/android/youtube/app/k;

    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->n()Lcom/google/android/youtube/core/async/av;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->n:Lcom/google/android/youtube/core/async/av;

    .line 98
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/app/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->C:Lcom/google/android/youtube/app/g;

    .line 99
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/d;

    .line 100
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/j;

    .line 101
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Playlist;)V
    .registers 5
    .parameter

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v0

    .line 302
    iget-object v1, p1, Lcom/google/android/youtube/core/model/Playlist;->contentUri:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/a;->a(Landroid/net/Uri;Z)V

    .line 303
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 6
    .parameter

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->s:Lcom/google/android/youtube/core/model/UserAuth;

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v3}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->g(Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ba;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 202
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->finish()V

    .line 210
    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 165
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    .line 166
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f11000e

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    .line 167
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .registers 6
    .parameter

    .prologue
    .line 172
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->g()I

    move-result v0

    const v1, 0x7f0801ae

    if-ne v0, v1, :cond_20

    .line 173
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->C:Lcom/google/android/youtube/app/g;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/be;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/d;

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/be;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;Lcom/google/android/youtube/core/d;)V

    const v2, 0x7f0b01bd

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->w:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->b()Z

    move-result v3

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    .line 183
    const/4 v0, 0x1

    .line 185
    :goto_1f
    return v0

    :cond_20
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_1f
.end method

.method protected final b(I)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    .line 225
    sparse-switch p1, :sswitch_data_52

    .line 256
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 228
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->b()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 231
    :sswitch_f
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bf;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bf;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V

    .line 240
    new-instance v1, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b0211

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/x;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_7

    .line 249
    :sswitch_45
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->A:Lcom/google/android/youtube/app/ui/z;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bg;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bg;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/z;->a(Lcom/google/android/youtube/app/ui/ac;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 225
    nop

    :sswitch_data_52
    .sparse-switch
        0x3ed -> :sswitch_45
        0x3ee -> :sswitch_f
        0x3f5 -> :sswitch_8
    .end sparse-switch
.end method

.method protected final g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 262
    const-string v0, "yt_playlist"

    return-object v0
.end method

.method public final i_()V
    .registers 1

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->finish()V

    .line 206
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 289
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/s;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/s;->a()V

    .line 291
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->i()V

    .line 292
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    const v5, 0x7f0a005d

    .line 106
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    if-eqz p1, :cond_12

    .line 109
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->D:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Playlist;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    .line 112
    :cond_12
    const v0, 0x7f040078

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->setContentView(I)V

    .line 113
    const v0, 0x7f0b0193

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->e(I)V

    .line 115
    new-instance v0, Lcom/google/android/youtube/app/ui/z;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->o:Lcom/google/android/youtube/core/b/al;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/d;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/youtube/app/ui/z;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->A:Lcom/google/android/youtube/app/ui/z;

    .line 117
    new-instance v0, Lcom/google/android/youtube/app/ui/s;

    const/16 v1, 0x3f5

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/s;-><init>(Landroid/app/Activity;I)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/s;

    .line 119
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/s;

    const v1, 0x7f0b0210

    const v2, 0x7f02008c

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(II)I

    move-result v0

    .line 121
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/s;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/bd;

    invoke-direct {v2, p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;I)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/s;->a(Lcom/google/android/youtube/app/ui/y;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->p:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->q:Lcom/google/android/youtube/core/b/ap;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->u:Lcom/google/android/youtube/core/j;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->z:Lcom/google/android/youtube/app/ui/s;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/app/ui/s;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->y:Lcom/google/android/youtube/app/adapter/bt;

    .line 135
    new-instance v0, Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->y:Lcom/google/android/youtube/app/adapter/bt;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/by;

    .line 136
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/by;->b(I)V

    .line 138
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    const v2, 0x7f0a005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    const v4, 0x7f0a005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/youtube/app/ui/by;->a(IIII)V

    .line 143
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->i()V

    .line 145
    const v0, 0x7f08011d

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/g;

    .line 146
    new-instance v0, Lcom/google/android/youtube/app/ui/ba;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->v:Lcom/google/android/youtube/app/ui/by;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->n:Lcom/google/android/youtube/core/async/av;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->t:Lcom/google/android/youtube/core/d;

    move-object v1, p0

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/ba;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/app/ui/bb;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    .line 153
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 214
    const/16 v0, 0x3ee

    if-ne p1, v0, :cond_1f

    .line 215
    const v0, 0x7f0b0211

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    iget-object v3, v3, Lcom/google/android/youtube/core/model/Playlist;->title:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    check-cast p2, Landroid/app/AlertDialog;

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 221
    :goto_1e
    return-void

    .line 219
    :cond_1f
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    goto :goto_1e
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 195
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->r:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;)V

    .line 197
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    if-eqz v0, :cond_e

    .line 159
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->D:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->B:Lcom/google/android/youtube/core/model/Playlist;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 161
    :cond_e
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 189
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    .line 190
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/MyPlaylistsActivity;->x:Lcom/google/android/youtube/app/ui/ba;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ba;->e()V

    .line 191
    return-void
.end method
