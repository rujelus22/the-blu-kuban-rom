.class public final Lcom/google/android/youtube/app/froyo/widget/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/av;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/google/android/youtube/core/async/av;

.field private final d:Ljava/util/concurrent/ConcurrentMap;

.field private final e:Lcom/google/android/youtube/core/b/an;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Ljava/util/concurrent/ConcurrentMap;IIZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/av;

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->c:Lcom/google/android/youtube/core/async/av;

    .line 50
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/an;

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->e:Lcom/google/android/youtube/core/b/an;

    .line 51
    invoke-static {p3}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    iput-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->d:Ljava/util/concurrent/ConcurrentMap;

    .line 52
    const-string v0, "minDesiredTeasers must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 53
    const-string v0, "maxTeasers must be > minDesiredTeasers"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/utils/o;->a(ZLjava/lang/Object;)V

    .line 55
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->a:I

    .line 56
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->b:I

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->f:Z

    .line 58
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/froyo/widget/c;)Z
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/froyo/widget/c;)Ljava/util/concurrent/ConcurrentMap;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->d:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/froyo/widget/c;)I
    .registers 2
    .parameter

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->b:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/froyo/widget/c;)I
    .registers 2
    .parameter

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->a:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/froyo/widget/c;)Lcom/google/android/youtube/core/async/av;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->c:Lcom/google/android/youtube/core/async/av;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/froyo/widget/c;)Lcom/google/android/youtube/core/b/an;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->e:Lcom/google/android/youtube/core/b/an;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/l;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/youtube/app/froyo/widget/c;->c:Lcom/google/android/youtube/core/async/av;

    new-instance v1, Lcom/google/android/youtube/app/froyo/widget/d;

    invoke-direct {v1, p0, p2}, Lcom/google/android/youtube/app/froyo/widget/d;-><init>(Lcom/google/android/youtube/app/froyo/widget/c;Lcom/google/android/youtube/core/async/l;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/youtube/core/async/av;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V

    .line 63
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/l;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/froyo/widget/c;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/async/l;)V

    return-void
.end method
