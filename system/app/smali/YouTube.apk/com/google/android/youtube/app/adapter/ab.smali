.class public final Lcom/google/android/youtube/app/adapter/ab;
.super Lcom/google/android/youtube/app/adapter/bt;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    const v0, 0x7f04001c

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/youtube/app/adapter/bt;-><init>(Landroid/content/Context;ILcom/google/android/youtube/app/adapter/cb;)V

    .line 27
    iput-object p4, p0, Lcom/google/android/youtube/app/adapter/ab;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ab;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Landroid/net/Uri;)V

    .line 42
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ab;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .registers 3
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ab;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    .line 38
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ab;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Landroid/net/Uri;)V

    .line 46
    return-void
.end method

.method public final notifyDataSetChanged()V
    .registers 2

    .prologue
    .line 32
    invoke-super {p0}, Lcom/google/android/youtube/app/adapter/bt;->notifyDataSetChanged()V

    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/app/adapter/ab;->a:Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/ChannelStoreItemRendererFactory;->b()V

    .line 34
    return-void
.end method
