.class public abstract Lcom/google/android/youtube/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Object;

.field private final c:Lcom/google/android/youtube/a/b;

.field private final d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/youtube/a/a;->a:Landroid/content/Context;

    .line 26
    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/google/android/youtube/a/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/a/b;-><init>(Lcom/google/android/youtube/a/a;B)V

    iput-object v0, p0, Lcom/google/android/youtube/a/a;->c:Lcom/google/android/youtube/a/b;

    .line 28
    const v0, 0x800001

    iput v0, p0, Lcom/google/android/youtube/a/a;->d:I

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/youtube/a/a;->e:I

    .line 30
    return-void
.end method

.method private a()V
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 52
    iget-object v1, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    invoke-static {v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;)I

    move-result v2

    move v1, v0

    .line 54
    :goto_8
    if-ge v1, v2, :cond_1e

    .line 55
    iget-object v3, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    invoke-static {v3, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    .line 56
    invoke-static {v3}, Lcom/android/athome/picker/media/q;->d(Ljava/lang/Object;)I

    move-result v3

    .line 57
    iget v4, p0, Lcom/google/android/youtube/a/a;->d:I

    and-int/2addr v3, v4

    if-eqz v3, :cond_1b

    .line 58
    add-int/lit8 v0, v0, 0x1

    .line 54
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 61
    :cond_1e
    iget v1, p0, Lcom/google/android/youtube/a/a;->e:I

    if-eq v1, v0, :cond_27

    .line 62
    iput v0, p0, Lcom/google/android/youtube/a/a;->e:I

    .line 63
    invoke-virtual {p0}, Lcom/google/android/youtube/a/a;->e()V

    .line 65
    :cond_27
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/a/a;)V
    .registers 1
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/youtube/a/a;->a()V

    return-void
.end method


# virtual methods
.method public final c()V
    .registers 4

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/youtube/a/a;->d:I

    iget-object v2, p0, Lcom/google/android/youtube/a/a;->c:Lcom/google/android/youtube/a/b;

    invoke-static {v0, v1, v2}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V

    .line 34
    invoke-direct {p0}, Lcom/google/android/youtube/a/a;->a()V

    .line 35
    return-void
.end method

.method protected abstract c(Z)V
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/a/a;->c:Lcom/google/android/youtube/a/b;

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V

    .line 39
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 42
    iget v1, p0, Lcom/google/android/youtube/a/a;->e:I

    if-le v1, v0, :cond_9

    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/a/a;->c(Z)V

    .line 43
    return-void

    .line 42
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected final f()V
    .registers 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/youtube/a/a;->b:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/youtube/a/a;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/youtube/a/c;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 47
    return-void
.end method
