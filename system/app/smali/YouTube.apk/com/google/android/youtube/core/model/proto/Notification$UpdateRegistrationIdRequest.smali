.class public final Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/am;


# static fields
.field public static final AUTH_TOKEN_FIELD_NUMBER:I = 0x3

.field public static final CHANNEL_IDS_FIELD_NUMBER:I = 0x5

.field public static final DEVICE_ID_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final REGISTRATION_ID_FIELD_NUMBER:I = 0x4

.field public static final USER_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

.field private static final serialVersionUID:J


# instance fields
.field private authToken_:Ljava/lang/Object;

.field private bitField0_:I

.field private channelIds_:Lcom/google/protobuf/ab;

.field private deviceId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private registrationId_:Ljava/lang/Object;

.field private userId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1108
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ak;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ak;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    .line 2026
    new-instance v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;-><init>(Z)V

    .line 2027
    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->initFields()V

    .line 2028
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v5, 0x10

    .line 1048
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1333
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    .line 1378
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedSerializedSize:I

    .line 1049
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->initFields()V

    move v1, v0

    .line 1053
    :cond_10
    :goto_10
    if-nez v1, :cond_a1

    .line 1054
    :try_start_12
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v3

    .line 1055
    sparse-switch v3, :sswitch_data_b2

    .line 1060
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 1062
    goto :goto_10

    :sswitch_21
    move v1, v2

    .line 1058
    goto :goto_10

    .line 1067
    :sswitch_23
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    .line 1068
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;
    :try_end_2f
    .catchall {:try_start_12 .. :try_end_2f} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_12 .. :try_end_2f} :catch_30
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_2f} :catch_58

    goto :goto_10

    .line 1096
    :catch_30
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 1097
    :try_start_34
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_39
    .catchall {:try_start_34 .. :try_end_39} :catchall_39

    .line 1102
    :catchall_39
    move-exception v0

    :goto_3a
    and-int/lit8 v1, v1, 0x10

    if-ne v1, v5, :cond_47

    .line 1103
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v1, v2}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 1105
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->makeExtensionsImmutable()V

    throw v0

    .line 1072
    :sswitch_4b
    :try_start_4b
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    .line 1073
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;
    :try_end_57
    .catchall {:try_start_4b .. :try_end_57} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4b .. :try_end_57} :catch_30
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_57} :catch_58

    goto :goto_10

    .line 1098
    :catch_58
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 1099
    :try_start_5c
    new-instance v2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_39

    .line 1077
    :sswitch_6a
    :try_start_6a
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    .line 1078
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    goto :goto_10

    .line 1102
    :catchall_77
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_3a

    .line 1082
    :sswitch_7c
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    .line 1083
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    goto :goto_10

    .line 1087
    :sswitch_89
    and-int/lit8 v3, v0, 0x10

    if-eq v3, v5, :cond_96

    .line 1088
    new-instance v3, Lcom/google/protobuf/aa;

    invoke-direct {v3}, Lcom/google/protobuf/aa;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 1089
    or-int/lit8 v0, v0, 0x10

    .line 1091
    :cond_96
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/ab;->a(Lcom/google/protobuf/e;)V
    :try_end_9f
    .catchall {:try_start_6a .. :try_end_9f} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_6a .. :try_end_9f} :catch_30
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_9f} :catch_58

    goto/16 :goto_10

    .line 1102
    :cond_a1
    and-int/lit8 v0, v0, 0x10

    if-ne v0, v5, :cond_ae

    .line 1103
    new-instance v0, Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 1105
    :cond_ae
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->makeExtensionsImmutable()V

    .line 1106
    return-void

    .line 1055
    :sswitch_data_b2
    .sparse-switch
        0x0 -> :sswitch_21
        0xa -> :sswitch_23
        0x12 -> :sswitch_4b
        0x1a -> :sswitch_6a
        0x22 -> :sswitch_7c
        0x2a -> :sswitch_89
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1026
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1031
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 1333
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    .line 1378
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedSerializedSize:I

    .line 1033
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1026
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1034
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1333
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    .line 1378
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedSerializedSize:I

    .line 1034
    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1026
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 1

    .prologue
    .line 1038
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 1327
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    .line 1328
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    .line 1329
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    .line 1330
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    .line 1331
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 1332
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/al;
    .registers 1

    .prologue
    .line 1473
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/al;->a()Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Lcom/google/android/youtube/core/model/proto/al;
    .registers 2
    .parameter

    .prologue
    .line 1476
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/al;->a(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 2
    .parameter

    .prologue
    .line 1453
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1459
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 2
    .parameter

    .prologue
    .line 1423
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1429
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 2
    .parameter

    .prologue
    .line 1464
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1470
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 2
    .parameter

    .prologue
    .line 1443
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1449
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 2
    .parameter

    .prologue
    .line 1433
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1439
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method


# virtual methods
.method public final getAuthToken()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1223
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    .line 1224
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1225
    check-cast v0, Ljava/lang/String;

    .line 1233
    :goto_8
    return-object v0

    .line 1227
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1229
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1230
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1231
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1233
    goto :goto_8
.end method

.method public final getAuthTokenBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1241
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    .line 1242
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1243
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1246
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->authToken_:Ljava/lang/Object;

    .line 1249
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getChannelIds(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelIdsBytes(I)Lcom/google/protobuf/e;
    .registers 3
    .parameter

    .prologue
    .line 1323
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public final getChannelIdsCount()I
    .registers 2

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->size()I

    move-result v0

    return v0
.end method

.method public final getChannelIdsList()Ljava/util/List;
    .registers 2

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;
    .registers 2

    .prologue
    .line 1042
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1026
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    .line 1138
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1139
    check-cast v0, Ljava/lang/String;

    .line 1147
    :goto_8
    return-object v0

    .line 1141
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1143
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1144
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1145
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1147
    goto :goto_8
.end method

.method public final getDeviceIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    .line 1156
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1157
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1160
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->deviceId_:Ljava/lang/Object;

    .line 1163
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 1120
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getRegistrationId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    .line 1267
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1268
    check-cast v0, Ljava/lang/String;

    .line 1276
    :goto_8
    return-object v0

    .line 1270
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1272
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1273
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1274
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1276
    goto :goto_8
.end method

.method public final getRegistrationIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    .line 1285
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1286
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1289
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->registrationId_:Ljava/lang/Object;

    .line 1292
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getSerializedSize()I
    .registers 7

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1380
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedSerializedSize:I

    .line 1381
    const/4 v2, -0x1

    if-eq v0, v2, :cond_a

    .line 1410
    :goto_9
    return v0

    .line 1384
    :cond_a
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_70

    .line 1385
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1388
    :goto_1a
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_29

    .line 1389
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1392
    :cond_29
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_39

    .line 1393
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1396
    :cond_39
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4a

    .line 1397
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getRegistrationIdBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4a
    move v2, v1

    .line 1402
    :goto_4b
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3}, Lcom/google/protobuf/ab;->size()I

    move-result v3

    if-ge v1, v3, :cond_61

    .line 1403
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3, v1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1402
    add-int/lit8 v1, v1, 0x1

    goto :goto_4b

    .line 1406
    :cond_61
    add-int/2addr v0, v2

    .line 1407
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getChannelIdsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1409
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedSerializedSize:I

    goto :goto_9

    :cond_70
    move v0, v1

    goto :goto_1a
.end method

.method public final getUserId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    .line 1181
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1182
    check-cast v0, Ljava/lang/String;

    .line 1190
    :goto_8
    return-object v0

    .line 1184
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 1186
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 1187
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1188
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 1190
    goto :goto_8
.end method

.method public final getUserIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    .line 1199
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 1200
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 1203
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->userId_:Ljava/lang/Object;

    .line 1206
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final hasAuthToken()Z
    .registers 3

    .prologue
    .line 1217
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasDeviceId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1131
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasRegistrationId()Z
    .registers 3

    .prologue
    .line 1260
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasUserId()Z
    .registers 3

    .prologue
    .line 1174
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1335
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    .line 1336
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 1355
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 1336
    goto :goto_9

    .line 1338
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->hasDeviceId()Z

    move-result v2

    if-nez v2, :cond_16

    .line 1339
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 1340
    goto :goto_9

    .line 1342
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->hasUserId()Z

    move-result v2

    if-nez v2, :cond_20

    .line 1343
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 1344
    goto :goto_9

    .line 1346
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->hasAuthToken()Z

    move-result v2

    if-nez v2, :cond_2a

    .line 1347
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 1348
    goto :goto_9

    .line 1350
    :cond_2a
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->hasRegistrationId()Z

    move-result v2

    if-nez v2, :cond_34

    .line 1351
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 1352
    goto :goto_9

    .line 1354
    :cond_34
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/al;
    .registers 2

    .prologue
    .line 1474
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1026
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/al;
    .registers 2

    .prologue
    .line 1478
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;)Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1026
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->toBuilder()Lcom/google/android/youtube/core/model/proto/al;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1417
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1360
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getSerializedSize()I

    .line 1361
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_13

    .line 1362
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1364
    :cond_13
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_20

    .line 1365
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1367
    :cond_20
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2e

    .line 1368
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1370
    :cond_2e
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3d

    .line 1371
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->getRegistrationIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1373
    :cond_3d
    const/4 v0, 0x0

    :goto_3e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v1}, Lcom/google/protobuf/ab;->size()I

    move-result v1

    if-ge v0, v1, :cond_53

    .line 1374
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UpdateRegistrationIdRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v2, v0}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 1373
    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    .line 1376
    :cond_53
    return-void
.end method
