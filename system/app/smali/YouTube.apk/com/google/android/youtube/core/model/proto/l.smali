.class public final Lcom/google/android/youtube/core/model/proto/l;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/m;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 4268
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 4366
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/l;->b:Ljava/lang/Object;

    .line 4440
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/l;->c:Ljava/lang/Object;

    .line 4269
    return-void
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/l;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 4350
    const/4 v2, 0x0

    .line 4352
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 4357
    if-eqz v0, :cond_e

    .line 4358
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/l;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;)Lcom/google/android/youtube/core/model/proto/l;

    .line 4361
    :cond_e
    return-object p0

    .line 4353
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 4354
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 4355
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 4357
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 4358
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/l;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;)Lcom/google/android/youtube/core/model/proto/l;

    :cond_21
    throw v0

    .line 4357
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method static synthetic g()Lcom/google/android/youtube/core/model/proto/l;
    .registers 1

    .prologue
    .line 4263
    new-instance v0, Lcom/google/android/youtube/core/model/proto/l;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/l;-><init>()V

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/l;
    .registers 3

    .prologue
    .line 4288
    new-instance v0, Lcom/google/android/youtube/core/model/proto/l;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/l;-><init>()V

    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/l;->i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/l;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;)Lcom/google/android/youtube/core/model/proto/l;

    move-result-object v0

    return-object v0
.end method

.method private i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 4304
    new-instance v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 4305
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    .line 4306
    const/4 v1, 0x0

    .line 4307
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_23

    .line 4310
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/l;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->artistId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->access$3802(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4311
    and-int/lit8 v1, v3, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1a

    .line 4312
    or-int/lit8 v0, v0, 0x2

    .line 4314
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/l;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->countryCode_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->access$3902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4315
    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->access$4002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;I)I

    .line 4316
    return-object v2

    :cond_23
    move v0, v1

    goto :goto_e
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;
    .registers 3

    .prologue
    .line 4296
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/l;->i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v0

    .line 4297
    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    .line 4298
    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/l;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 4300
    :cond_f
    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;)Lcom/google/android/youtube/core/model/proto/l;
    .registers 3
    .parameter

    .prologue
    .line 4320
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 4331
    :cond_6
    :goto_6
    return-object p0

    .line 4321
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->hasArtistId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 4322
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    .line 4323
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->artistId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->access$3800(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/l;->b:Ljava/lang/Object;

    .line 4326
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4327
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    .line 4328
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->countryCode_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->access$3900(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/l;->c:Ljava/lang/Object;

    goto :goto_6
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/youtube/core/model/proto/l;
    .registers 3
    .parameter

    .prologue
    .line 4408
    if-nez p1, :cond_8

    .line 4409
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4411
    :cond_8
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    .line 4412
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/l;->b:Ljava/lang/Object;

    .line 4414
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/youtube/core/model/proto/l;
    .registers 3
    .parameter

    .prologue
    .line 4482
    if-nez p1, :cond_8

    .line 4483
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 4485
    :cond_8
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    .line 4486
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/l;->c:Ljava/lang/Object;

    .line 4488
    return-object p0
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 4263
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4263
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/l;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4263
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/l;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 4263
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/l;->h()Lcom/google/android/youtube/core/model/proto/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 4263
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/l;->h()Lcom/google/android/youtube/core/model/proto/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 4263
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/l;->h()Lcom/google/android/youtube/core/model/proto/l;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4263
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/l;->i()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4263
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/l;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 4263
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileGetArtistOneCallRequest;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 4335
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 4343
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 4335
    goto :goto_9

    .line 4339
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/l;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1a

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    move v0, v1

    .line 4343
    goto :goto_b

    :cond_1a
    move v2, v0

    .line 4339
    goto :goto_16
.end method
