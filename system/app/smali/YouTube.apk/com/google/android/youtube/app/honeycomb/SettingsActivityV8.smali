.class public Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/google/android/youtube/app/compat/i;


# instance fields
.field private a:Lcom/google/android/youtube/app/YouTubeApplication;

.field private b:Landroid/preference/ListPreference;

.field private c:Lcom/google/android/youtube/app/honeycomb/f;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;)Lcom/google/android/youtube/app/YouTubeApplication;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method


# virtual methods
.method public final b_()Z
    .registers 2

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bn;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bn;-><init>(Landroid/app/Activity;)V

    .line 227
    invoke-interface {v0}, Lcom/google/android/youtube/app/a;->a()V

    .line 228
    const/4 v0, 0x1

    return v0
.end method

.method public final c_()Z
    .registers 2

    .prologue
    .line 236
    const/4 v0, 0x0

    return v0
.end method

.method public final e()I
    .registers 2

    .prologue
    .line 240
    const v0, 0x102000a

    return v0
.end method

.method public final f_()V
    .registers 1

    .prologue
    .line 233
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x4

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 53
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f0400af

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->setContentView(I)V

    .line 57
    invoke-static {p0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b(I)V

    .line 59
    invoke-virtual {v0, v1, v1}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(II)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "youtube"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 63
    const v0, 0x7f06000a

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->addPreferencesFromResource(I)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    .line 66
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    .line 68
    const-string v0, "version"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    .line 72
    const-string v0, "safe_search"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 73
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/SafeSearch;->b()Z

    move-result v0

    if-nez v0, :cond_d1

    const/4 v0, 0x1

    :goto_59
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 75
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->k()Z

    move-result v0

    if-nez v0, :cond_77

    .line 76
    const-string v0, "prefetch_category"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 78
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 81
    :cond_77
    const-string v0, "general_category"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 84
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_a7

    .line 85
    const-string v1, "default_hq"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 86
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 87
    const-string v1, "default_hq"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_a7

    .line 88
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "default_hq"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 92
    :cond_a7
    const-string v0, "country"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 94
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 95
    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory;->s:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_ba
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 96
    new-instance v6, Ljava/util/Locale;

    const-string v7, ""

    invoke-direct {v6, v7, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_ba

    :cond_d1
    move v0, v2

    .line 73
    goto :goto_59

    .line 99
    :cond_d3
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v1

    .line 100
    new-instance v3, Lcom/google/android/youtube/app/honeycomb/k;

    invoke-direct {v3, p0, v1}, Lcom/google/android/youtube/app/honeycomb/k;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;Ljava/text/Collator;)V

    invoke-static {v5, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 106
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v6, v1, [Ljava/lang/CharSequence;

    .line 107
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v7, v1, [Ljava/lang/CharSequence;

    .line 109
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v8

    move v3, v4

    .line 110
    :goto_f2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_11d

    .line 111
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v2

    .line 112
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v2

    .line 113
    aget-object v1, v7, v2

    invoke-virtual {v1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_182

    move v1, v2

    .line 110
    :goto_119
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    goto :goto_f2

    .line 117
    :cond_11d
    invoke-virtual {v0, v6}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 118
    invoke-virtual {v0, v7}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 119
    if-eq v3, v4, :cond_128

    .line 120
    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 123
    :cond_128
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const-string v1, "channel_feed_content"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Landroid/preference/ListPreference;

    .line 125
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->O()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "channel_feed_content"

    const-string v2, "all_activity"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    const-string v1, "all_activity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_175

    .line 131
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b0149

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    :goto_15d
    new-instance v2, Lcom/google/android/youtube/app/honeycomb/f;

    const-string v0, "prefetch_subscriptions"

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "prefetch_watch_later"

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/f;-><init>(Landroid/app/Activity;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/f;

    .line 142
    return-void

    .line 134
    :cond_175
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b014a

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_15d

    :cond_182
    move v1, v3

    goto :goto_119
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    .line 182
    sparse-switch p1, :sswitch_data_5e

    .line 213
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_7
    return-object v0

    .line 184
    :sswitch_8
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/x;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/l;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/honeycomb/l;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x104

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_7

    .line 196
    :sswitch_2c
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/x;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f120001

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->R()Lcom/google/android/youtube/core/utils/SafeSearch;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch;->a()Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/utils/SafeSearch$SafeSearchMode;->ordinal()I

    move-result v2

    new-instance v3, Lcom/google/android/youtube/app/honeycomb/m;

    invoke-direct {v3, p0}, Lcom/google/android/youtube/app/honeycomb/m;-><init>(Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_7

    .line 210
    :sswitch_57
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/f;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/f;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_7

    .line 182
    :sswitch_data_5e
    .sparse-switch
        0x3ea -> :sswitch_8
        0x3f6 -> :sswitch_2c
        0x401 -> :sswitch_57
    .end sparse-switch
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 244
    check-cast p2, Ljava/lang/String;

    .line 245
    const-string v0, "all_activity"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b0149

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 252
    :goto_16
    const/4 v0, 0x1

    return v0

    .line 249
    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->b:Landroid/preference/ListPreference;

    const v1, 0x7f0b014a

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_16
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 152
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 153
    const-string v2, "clear_history"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 154
    const/16 v1, 0x3ea

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->showDialog(I)V

    .line 177
    :goto_12
    return v0

    .line 156
    :cond_13
    const-string v2, "safe_search"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 157
    const/16 v1, 0x3f6

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->showDialog(I)V

    goto :goto_12

    .line 159
    :cond_21
    const-string v0, "mobile_terms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 160
    const v0, 0x7f0b0220

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 177
    :cond_37
    :goto_37
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    goto :goto_12

    .line 161
    :cond_3c
    const-string v0, "youtube_terms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 162
    const v0, 0x7f0b0221

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_37

    .line 163
    :cond_53
    const-string v0, "mobile_privacy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 164
    const v0, 0x7f0b0222

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_37

    .line 165
    :cond_6a
    const-string v0, "youtube_privacy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 166
    const v0, 0x7f0b0223

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_37

    .line 167
    :cond_81
    const-string v0, "open_source_licenses"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 168
    invoke-static {p0}, Lcom/google/android/youtube/core/LicensesActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->startActivity(Landroid/content/Intent;)V

    goto :goto_37

    .line 169
    :cond_91
    const-string v0, "feedback"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 170
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->V()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0b021f

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_37

    .line 173
    :cond_ae
    const-string v0, "help"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 174
    const v0, 0x7f0b021d

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/j;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto/16 :goto_37
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 218
    const/16 v0, 0x401

    if-ne p1, v0, :cond_a

    .line 219
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->c:Lcom/google/android/youtube/app/honeycomb/f;

    invoke-virtual {v0, p3}, Lcom/google/android/youtube/app/honeycomb/f;->a(Landroid/os/Bundle;)V

    .line 223
    :goto_9
    return-void

    .line 221
    :cond_a
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_9
.end method

.method protected onResume()V
    .registers 3

    .prologue
    .line 146
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/SettingsActivityV8;->a:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;)V

    .line 148
    return-void
.end method
