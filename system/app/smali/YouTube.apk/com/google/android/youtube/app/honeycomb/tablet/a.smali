.class public final Lcom/google/android/youtube/app/honeycomb/tablet/a;
.super Lcom/google/android/youtube/app/honeycomb/tablet/as;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private a:Landroid/widget/ListView;

.field private h:Landroid/widget/ArrayAdapter;

.field private final i:Lcom/google/android/youtube/core/b/al;

.field private final j:Lcom/google/android/youtube/core/b/an;

.field private final k:Lcom/google/android/youtube/core/b/ap;

.field private final l:Lcom/google/android/youtube/core/d;

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private o:Lcom/google/android/youtube/core/model/Category;

.field private p:Lcom/google/android/youtube/app/ui/at;

.field private q:Lcom/google/android/youtube/app/ui/at;

.field private r:Lcom/google/android/youtube/app/ui/at;

.field private s:Lcom/google/android/youtube/app/ui/dw;

.field private t:Lcom/google/android/youtube/app/honeycomb/ui/j;

.field private u:Lcom/google/android/youtube/app/honeycomb/ui/l;

.field private v:Landroid/view/View;

.field private w:Z

.field private x:Z


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    const/4 v4, 0x0

    const-string v5, "yt_browse"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/tablet/as;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Landroid/os/Bundle;Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->w:Z

    .line 86
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i:Lcom/google/android/youtube/core/b/al;

    .line 87
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->j:Lcom/google/android/youtube/core/b/an;

    .line 88
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->k:Lcom/google/android/youtube/core/b/ap;

    .line 89
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->l:Lcom/google/android/youtube/core/d;

    .line 90
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->m:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->y()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->n:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->d()Lcom/google/android/youtube/app/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/k;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->x:Z

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/a;)V
    .registers 1
    .parameter

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i()V

    return-void
.end method

.method private a(Z)V
    .registers 9
    .parameter

    .prologue
    const v6, 0x7f080053

    const v5, 0x7f080048

    const/4 v4, 0x4

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 262
    if-eqz p1, :cond_43

    .line 263
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    const v1, 0x7f0800b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    const v1, 0x7f0800b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    const v1, 0x7f0800b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 275
    :goto_42
    return-void

    .line 269
    :cond_43
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    const v1, 0x7f0800b9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    const v1, 0x7f0800b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    const v1, 0x7f0800b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_42
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/tablet/a;Lcom/google/android/youtube/core/model/LiveEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 53
    sget-object v2, Lcom/google/android/youtube/app/honeycomb/tablet/f;->a:[I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->r:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_2e

    :cond_14
    move v0, v1

    :goto_15
    return v0

    :pswitch_16
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/LiveEvent;->isLiveNow()Z

    move-result v0

    goto :goto_15

    :pswitch_1b
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/LiveEvent;->isUpcoming()Z

    move-result v0

    goto :goto_15

    :pswitch_20
    iget-object v0, p1, Lcom/google/android/youtube/core/model/LiveEvent;->status:Lcom/google/android/youtube/core/model/LiveEvent$Status;

    sget-object v2, Lcom/google/android/youtube/core/model/LiveEvent$Status;->COMPLETED:Lcom/google/android/youtube/core/model/LiveEvent$Status;

    if-ne v0, v2, :cond_2c

    iget-object v0, p1, Lcom/google/android/youtube/core/model/LiveEvent;->video:Lcom/google/android/youtube/core/model/Video;

    iget v0, v0, Lcom/google/android/youtube/core/model/Video;->duration:I

    if-nez v0, :cond_14

    :cond_2c
    const/4 v0, 0x0

    goto :goto_15

    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_16
        :pswitch_1b
        :pswitch_20
    .end packed-switch
.end method

.method private f()V
    .registers 5

    .prologue
    .line 227
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/Util;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i:Lcom/google/android/youtube/core/b/al;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->m:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v3, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/core/b/al;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    .line 229
    return-void
.end method

.method private i()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 232
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v0}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    .line 233
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/Category;->isLive()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 234
    invoke-direct {p0, v6}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a(Z)V

    .line 235
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->u:Lcom/google/android/youtube/app/honeycomb/ui/l;

    new-array v2, v6, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->r:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/l;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 237
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->t:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->b()V

    .line 247
    :goto_31
    return-void

    .line 239
    :cond_32
    invoke-direct {p0, v5}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a(Z)V

    .line 240
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->t:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->c()V

    .line 241
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    if-eqz v0, :cond_63

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Category;->term:Ljava/lang/String;

    move-object v2, v0

    .line 242
    :goto_43
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->q:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    .line 243
    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->p:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v3, v0, v2, v4, v1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v0

    .line 245
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->s:Lcom/google/android/youtube/app/ui/dw;

    new-array v2, v6, [Lcom/google/android/youtube/core/async/GDataRequest;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/ui/dw;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    goto :goto_31

    .line 241
    :cond_63
    const/4 v0, 0x0

    move-object v2, v0

    goto :goto_43
.end method


# virtual methods
.method protected final a()I
    .registers 2

    .prologue
    .line 97
    const v0, 0x7f0400d5

    return v0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/os/Bundle;)V

    .line 195
    const-string v0, "selected_feed_filter"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->q:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 197
    const-string v0, "selected_time_filter"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->p:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 198
    const-string v0, "selected_category"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 199
    const-string v0, "selected_live_filter"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->r:Lcom/google/android/youtube/app/ui/at;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/at;->b()Ljava/lang/Enum;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 201
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 103
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->v:Landroid/view/View;

    .line 104
    const v0, 0x7f08006a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a:Landroid/widget/ListView;

    .line 105
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    const v2, 0x7f0400e2

    const v3, 0x7f08007a

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    .line 107
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 110
    new-instance v4, Lcom/google/android/youtube/app/honeycomb/tablet/b;

    invoke-direct {v4, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/b;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/a;)V

    .line 116
    new-instance v5, Lcom/google/android/youtube/app/honeycomb/tablet/c;

    invoke-direct {v5, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/c;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/a;)V

    .line 122
    new-instance v6, Lcom/google/android/youtube/app/honeycomb/tablet/d;

    invoke-direct {v6, p0}, Lcom/google/android/youtube/app/honeycomb/tablet/d;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/a;)V

    .line 129
    sget-object v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->TOP_RATED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    .line 130
    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 131
    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;->LIVE_NOW:Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    .line 132
    if-eqz p2, :cond_64

    .line 133
    const-string v0, "selected_feed_filter"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    .line 135
    const-string v1, "selected_time_filter"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    .line 137
    const-string v2, "selected_live_filter"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;

    .line 139
    const-string v3, "selected_category"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/model/Category;

    iput-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    :cond_64
    move-object v3, v0

    .line 143
    const v0, 0x7f080048

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 144
    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    const v8, 0x7f0400d9

    invoke-static {v7, v4, v1, v0, v8}, Lcom/google/android/youtube/app/ui/at;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/av;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/youtube/app/ui/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->p:Lcom/google/android/youtube/app/ui/at;

    .line 146
    const v0, 0x7f0800b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 147
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v1, v5, v3, v0}, Lcom/google/android/youtube/app/ui/at;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/av;Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Landroid/widget/Spinner;)Lcom/google/android/youtube/app/ui/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->q:Lcom/google/android/youtube/app/ui/at;

    .line 149
    const v0, 0x7f0800b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 150
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v1, v6, v2, v0}, Lcom/google/android/youtube/app/ui/at;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/av;Lcom/google/android/youtube/core/async/GDataRequestFactory$LiveFeed;Landroid/widget/Spinner;)Lcom/google/android/youtube/app/ui/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->r:Lcom/google/android/youtube/app/ui/at;

    .line 153
    const v0, 0x7f080053

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 154
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 155
    new-instance v0, Lcom/google/android/youtube/app/ui/dw;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->d:Lcom/google/android/youtube/app/a;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v4}, Lcom/google/android/youtube/app/adapter/cn;->b(Landroid/app/Activity;)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i:Lcom/google/android/youtube/core/b/al;

    invoke-interface {v5}, Lcom/google/android/youtube/core/b/al;->j()Lcom/google/android/youtube/core/async/av;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->j:Lcom/google/android/youtube/core/b/an;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->k:Lcom/google/android/youtube/core/b/ap;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v8}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v8

    sget-object v9, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Browse:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v10, Lcom/google/android/youtube/app/m;->M:Lcom/google/android/youtube/core/b/aq;

    iget-object v11, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->l:Lcom/google/android/youtube/core/d;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/dw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/d;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->s:Lcom/google/android/youtube/app/ui/dw;

    .line 168
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Landroid/content/Context;)Lcom/google/android/youtube/app/honeycomb/ui/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->t:Lcom/google/android/youtube/app/honeycomb/ui/j;

    .line 169
    const v0, 0x7f0800b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/core/ui/PagedGridView;

    .line 170
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/tablet/m;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/google/android/youtube/core/ui/PagedGridView;->setNumColumns(I)V

    .line 171
    new-instance v0, Lcom/google/android/youtube/app/honeycomb/tablet/e;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->d:Lcom/google/android/youtube/app/a;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->t:Lcom/google/android/youtube/app/honeycomb/ui/j;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i:Lcom/google/android/youtube/core/b/al;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->l:Lcom/google/android/youtube/core/d;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->j:Lcom/google/android/youtube/core/b/an;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->c:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v9

    sget-object v10, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Browse:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    sget-object v11, Lcom/google/android/youtube/app/m;->M:Lcom/google/android/youtube/core/b/aq;

    move-object v1, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/honeycomb/tablet/e;-><init>(Lcom/google/android/youtube/app/honeycomb/tablet/a;Landroid/app/Activity;Lcom/google/android/youtube/app/a;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/b/aq;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->u:Lcom/google/android/youtube/app/honeycomb/ui/l;

    .line 189
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->f()V

    .line 190
    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 222
    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->a(Lcom/google/android/youtube/app/compat/m;Lcom/google/android/youtube/app/compat/r;)V

    .line 223
    const v0, 0x7f080197

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    .line 224
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 53
    const-string v0, "Categories request failed"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    check-cast p2, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/model/Category;->createAllCategory(Landroid/content/res/Resources;Ljava/lang/String;)Lcom/google/android/youtube/core/model/Category;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->x:Z

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->g:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/youtube/core/model/Category;->createLiveCategory(Landroid/content/res/Resources;Ljava/lang/String;)Lcom/google/android/youtube/core/model/Category;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_2f
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/converter/http/g;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Category;

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_3b

    :cond_4b
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    if-eqz v0, :cond_64

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7a

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_64
    :goto_64
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    if-nez v0, :cond_77

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Category;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_77
    iput-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->w:Z

    return-void

    :cond_7a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    goto :goto_64
.end method

.method protected final b()V
    .registers 2

    .prologue
    .line 205
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->b()V

    .line 206
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->w:Z

    if-nez v0, :cond_a

    .line 208
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->f()V

    .line 210
    :cond_a
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i()V

    .line 211
    return-void
.end method

.method protected final c()V
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->s:Lcom/google/android/youtube/app/ui/dw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dw;->c()V

    .line 216
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->t:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->c()V

    .line 217
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/as;->c()V

    .line 218
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Category;

    .line 311
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    if-eq v1, v0, :cond_1d

    .line 312
    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->o:Lcom/google/android/youtube/core/model/Category;

    .line 313
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->a:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/tablet/a;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, v0}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 314
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/tablet/a;->i()V

    .line 316
    :cond_1d
    return-void
.end method
