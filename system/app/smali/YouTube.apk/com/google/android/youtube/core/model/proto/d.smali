.class public final Lcom/google/android/youtube/core/model/proto/d;
.super Lcom/google/protobuf/o;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/e;


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 2003
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    .line 2112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/d;->b:Ljava/lang/Object;

    .line 2186
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/d;->c:Ljava/lang/Object;

    .line 2260
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/d;->d:Ljava/lang/Object;

    .line 2004
    return-void
.end method

.method private a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/d;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 2096
    const/4 v2, 0x0

    .line 2098
    :try_start_1
    sget-object v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_9} :catch_f

    .line 2103
    if-eqz v0, :cond_e

    .line 2104
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    .line 2107
    :cond_e
    return-object p0

    .line 2099
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 2100
    :try_start_11
    invoke-virtual {v1}, Lcom/google/protobuf/InvalidProtocolBufferException;->getUnfinishedMessage()Lcom/google/protobuf/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 2101
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 2103
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 2104
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    :cond_21
    throw v0

    .line 2103
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method static synthetic g()Lcom/google/android/youtube/core/model/proto/d;
    .registers 1

    .prologue
    .line 1998
    new-instance v0, Lcom/google/android/youtube/core/model/proto/d;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/d;-><init>()V

    return-object v0
.end method

.method private h()Lcom/google/android/youtube/core/model/proto/d;
    .registers 3

    .prologue
    .line 2025
    new-instance v0, Lcom/google/android/youtube/core/model/proto/d;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/d;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/d;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 2041
    new-instance v2, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;-><init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/a;)V

    .line 2042
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    .line 2043
    const/4 v1, 0x0

    .line 2044
    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_2f

    .line 2047
    :goto_e
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/d;->b:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$1702(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2048
    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1a

    .line 2049
    or-int/lit8 v0, v0, 0x2

    .line 2051
    :cond_1a
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/d;->c:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$1802(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2052
    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_26

    .line 2053
    or-int/lit8 v0, v0, 0x4

    .line 2055
    :cond_26
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/d;->d:Ljava/lang/Object;

    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;
    invoke-static {v2, v1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$1902(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2056
    #setter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->bitField0_:I
    invoke-static {v2, v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$2002(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;I)I

    .line 2057
    return-object v2

    :cond_2f
    move v0, v1

    goto :goto_e
.end method

.method public final a(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Lcom/google/android/youtube/core/model/proto/d;
    .registers 3
    .parameter

    .prologue
    .line 2061
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 2077
    :cond_6
    :goto_6
    return-object p0

    .line 2062
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->hasArtistId()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2063
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    .line 2064
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistId_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$1700(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/d;->b:Ljava/lang/Object;

    .line 2067
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->hasArtistName()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 2068
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    .line 2069
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->artistName_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$1800(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/d;->c:Ljava/lang/Object;

    .line 2072
    :cond_2b
    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->hasBiography()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2073
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    .line 2074
    #getter for: Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->biography_:Ljava/lang/Object;
    invoke-static {p1}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->access$1900(Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/d;->d:Ljava/lang/Object;

    goto :goto_6
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 1998
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1998
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1998
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 1998
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/d;->h()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1998
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/d;->h()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 1998
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/d;->h()Lcom/google/android/youtube/core/model/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1998
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/d;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 3

    .prologue
    .line 1998
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/d;->a()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-static {v0}, Lcom/google/android/youtube/core/model/proto/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_f
    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1998
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;->getDefaultInstance()Lcom/google/android/youtube/core/model/proto/MobileNusic$MobileArtistBundle$Artist;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2081
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_c

    move v2, v1

    :goto_9
    if-nez v2, :cond_e

    .line 2089
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v2, v0

    .line 2081
    goto :goto_9

    .line 2085
    :cond_e
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/d;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1a

    move v2, v1

    :goto_16
    if-eqz v2, :cond_b

    move v0, v1

    .line 2089
    goto :goto_b

    :cond_1a
    move v2, v0

    .line 2085
    goto :goto_16
.end method
