.class public Lcom/google/android/youtube/app/compat/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/t;


# instance fields
.field protected final a:Landroid/view/MenuItem;

.field private final b:Landroid/content/Context;

.field private c:I

.field private d:Lcom/google/android/youtube/app/compat/ab;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/youtube/app/compat/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/MenuItem;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/z;->b:Landroid/content/Context;

    .line 35
    const-string v0, "target cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/compat/z;->c:I

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuItem;
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/youtube/app/compat/t;
    .registers 5
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 165
    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/youtube/app/compat/z;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/compat/z;->e:Landroid/view/View;

    .line 166
    return-object p0
.end method

.method public a(Lcom/google/android/youtube/app/compat/u;)Lcom/google/android/youtube/app/compat/t;
    .registers 2
    .parameter

    .prologue
    .line 212
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;
    .registers 4
    .parameter

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/z;->f:Lcom/google/android/youtube/app/compat/v;

    .line 217
    if-nez p1, :cond_b

    .line 218
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 227
    :goto_a
    return-object p0

    .line 221
    :cond_b
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    new-instance v1, Lcom/google/android/youtube/app/compat/aa;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/compat/aa;-><init>(Lcom/google/android/youtube/app/compat/z;Lcom/google/android/youtube/app/compat/v;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_a
.end method

.method public final a(Z)Lcom/google/android/youtube/app/compat/t;
    .registers 3
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 186
    return-object p0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/ab;)V
    .registers 2
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/youtube/app/compat/z;->d:Lcom/google/android/youtube/app/compat/ab;

    .line 55
    return-void
.end method

.method final a(Lcom/google/android/youtube/app/compat/t;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->f:Lcom/google/android/youtube/app/compat/v;

    if-eqz v0, :cond_9

    .line 44
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->f:Lcom/google/android/youtube/app/compat/v;

    invoke-interface {v0, p1}, Lcom/google/android/youtube/app/compat/v;->a(Lcom/google/android/youtube/app/compat/t;)Z

    .line 46
    :cond_9
    return-void
.end method

.method public final b(I)Lcom/google/android/youtube/app/compat/t;
    .registers 3
    .parameter

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 196
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/youtube/app/compat/t;
    .registers 4
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    .line 267
    iget-object v1, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v1, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 268
    if-eq v0, p1, :cond_16

    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->d:Lcom/google/android/youtube/app/compat/ab;

    if-eqz v0, :cond_16

    .line 269
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->d:Lcom/google/android/youtube/app/compat/ab;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/ab;->d()V

    .line 271
    :cond_16
    return-object p0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public c(I)V
    .registers 2
    .parameter

    .prologue
    .line 237
    iput p1, p0, Lcom/google/android/youtube/app/compat/z;->c:I

    .line 238
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public final d(I)Lcom/google/android/youtube/app/compat/t;
    .registers 3
    .parameter

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 257
    return-object p0
.end method

.method public d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Landroid/view/View;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->e:Landroid/view/View;

    return-object v0
.end method

.method public final f()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    return v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/youtube/app/compat/z;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    return v0
.end method

.method public final j()I
    .registers 2

    .prologue
    .line 241
    iget v0, p0, Lcom/google/android/youtube/app/compat/z;->c:I

    return v0
.end method
