.class final Lcom/google/android/youtube/app/honeycomb/ui/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/model/Video;

.field final synthetic b:Lcom/google/android/youtube/app/honeycomb/ui/v;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/v;Lcom/google/android/youtube/core/model/Video;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->b:Lcom/google/android/youtube/app/honeycomb/ui/v;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->a:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 263
    const-string v0, "Error downloading thumbnail"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 263
    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->b:Lcom/google/android/youtube/app/honeycomb/ui/v;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->b:Lcom/google/android/youtube/app/honeycomb/ui/v;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->i(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->b:Lcom/google/android/youtube/app/honeycomb/ui/v;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->j(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->a:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->b:Lcom/google/android/youtube/app/honeycomb/ui/v;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->k(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/w;->b:Lcom/google/android/youtube/app/honeycomb/ui/v;

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/ui/v;->a:Lcom/google/android/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/p;->l(Lcom/google/android/youtube/app/honeycomb/ui/p;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_47
    return-void
.end method
