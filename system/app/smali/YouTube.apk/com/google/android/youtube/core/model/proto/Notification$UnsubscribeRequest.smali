.class public final Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/model/proto/aj;


# static fields
.field public static final AUTH_TOKEN_FIELD_NUMBER:I = 0x3

.field public static final CHANNEL_IDS_FIELD_NUMBER:I = 0x4

.field public static final DEVICE_ID_FIELD_NUMBER:I = 0x1

.field public static PARSER:Lcom/google/protobuf/ah; = null

.field public static final USER_ID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

.field private static final serialVersionUID:J


# instance fields
.field private authToken_:Ljava/lang/Object;

.field private bitField0_:I

.field private channelIds_:Lcom/google/protobuf/ab;

.field private deviceId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private userId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 3274
    new-instance v0, Lcom/google/android/youtube/core/model/proto/ah;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/ah;-><init>()V

    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    .line 4048
    new-instance v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;-><init>(Z)V

    .line 4049
    sput-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    invoke-direct {v0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->initFields()V

    .line 4050
    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/16 v5, 0x8

    .line 3219
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3455
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    .line 3493
    iput v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedSerializedSize:I

    .line 3220
    invoke-direct {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->initFields()V

    move v1, v0

    .line 3224
    :cond_10
    :goto_10
    if-nez v1, :cond_94

    .line 3225
    :try_start_12
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v3

    .line 3226
    sparse-switch v3, :sswitch_data_a6

    .line 3231
    invoke-virtual {p0, p1, p2, v3}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 3233
    goto :goto_10

    :sswitch_21
    move v1, v2

    .line 3229
    goto :goto_10

    .line 3238
    :sswitch_23
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    .line 3239
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;
    :try_end_2f
    .catchall {:try_start_12 .. :try_end_2f} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_12 .. :try_end_2f} :catch_30
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_2f} :catch_58

    goto :goto_10

    .line 3262
    :catch_30
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 3263
    :try_start_34
    invoke-virtual {v0, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_39
    .catchall {:try_start_34 .. :try_end_39} :catchall_39

    .line 3268
    :catchall_39
    move-exception v0

    :goto_3a
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_47

    .line 3269
    new-instance v1, Lcom/google/protobuf/au;

    iget-object v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v1, v2}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 3271
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->makeExtensionsImmutable()V

    throw v0

    .line 3243
    :sswitch_4b
    :try_start_4b
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    .line 3244
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;
    :try_end_57
    .catchall {:try_start_4b .. :try_end_57} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4b .. :try_end_57} :catch_30
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_57} :catch_58

    goto :goto_10

    .line 3264
    :catch_58
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 3265
    :try_start_5c
    new-instance v2, Lcom/google/protobuf/InvalidProtocolBufferException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/protobuf/InvalidProtocolBufferException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/google/protobuf/InvalidProtocolBufferException;->setUnfinishedMessage(Lcom/google/protobuf/ae;)Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0
    :try_end_6a
    .catchall {:try_start_5c .. :try_end_6a} :catchall_39

    .line 3248
    :sswitch_6a
    :try_start_6a
    iget v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    .line 3249
    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    goto :goto_10

    .line 3268
    :catchall_77
    move-exception v1

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_3a

    .line 3253
    :sswitch_7c
    and-int/lit8 v3, v0, 0x8

    if-eq v3, v5, :cond_89

    .line 3254
    new-instance v3, Lcom/google/protobuf/aa;

    invoke-direct {v3}, Lcom/google/protobuf/aa;-><init>()V

    iput-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 3255
    or-int/lit8 v0, v0, 0x8

    .line 3257
    :cond_89
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-virtual {p1}, Lcom/google/protobuf/h;->e()Lcom/google/protobuf/e;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/ab;->a(Lcom/google/protobuf/e;)V
    :try_end_92
    .catchall {:try_start_6a .. :try_end_92} :catchall_77
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_6a .. :try_end_92} :catch_30
    .catch Ljava/io/IOException; {:try_start_6a .. :try_end_92} :catch_58

    goto/16 :goto_10

    .line 3268
    :cond_94
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_a1

    .line 3269
    new-instance v0, Lcom/google/protobuf/au;

    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-direct {v0, v1}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 3271
    :cond_a1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->makeExtensionsImmutable()V

    .line 3272
    return-void

    .line 3226
    nop

    :sswitch_data_a6
    .sparse-switch
        0x0 -> :sswitch_21
        0xa -> :sswitch_23
        0x12 -> :sswitch_4b
        0x1a -> :sswitch_6a
        0x22 -> :sswitch_7c
    .end sparse-switch
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3197
    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;-><init>(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/protobuf/o;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 3202
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/o;)V

    .line 3455
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    .line 3493
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedSerializedSize:I

    .line 3204
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/o;Lcom/google/android/youtube/core/model/proto/w;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3197
    invoke-direct {p0, p1}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;-><init>(Lcom/google/protobuf/o;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 3205
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 3455
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    .line 3493
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedSerializedSize:I

    .line 3205
    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 3197
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 3197
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 3197
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 3197
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 3197
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 3197
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter

    .prologue
    .line 3197
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;Lcom/google/protobuf/ab;)Lcom/google/protobuf/ab;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 3197
    iput-object p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object p1
.end method

.method static synthetic access$3302(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 3197
    iput p1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 1

    .prologue
    .line 3209
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 3450
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 3451
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    .line 3452
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 3453
    sget-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    .line 3454
    return-void
.end method

.method public static newBuilder()Lcom/google/android/youtube/core/model/proto/ai;
    .registers 1

    .prologue
    .line 3584
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/ai;->a()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;
    .registers 2
    .parameter

    .prologue
    .line 3587
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/model/proto/ai;->a(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 3564
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3570
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 3534
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3540
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 3575
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/h;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3581
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 3554
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3560
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 2
    .parameter

    .prologue
    .line 3544
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0}, Lcom/google/protobuf/ah;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3550
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    invoke-interface {v0, p0, p1}, Lcom/google/protobuf/ah;->a([BLcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method


# virtual methods
.method public final getAuthToken()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3389
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 3390
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 3391
    check-cast v0, Ljava/lang/String;

    .line 3399
    :goto_8
    return-object v0

    .line 3393
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 3395
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 3396
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 3397
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 3399
    goto :goto_8
.end method

.method public final getAuthTokenBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 3407
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 3408
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 3409
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 3412
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->authToken_:Ljava/lang/Object;

    .line 3415
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getChannelIds(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 3439
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelIdsBytes(I)Lcom/google/protobuf/e;
    .registers 3
    .parameter

    .prologue
    .line 3446
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0, p1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v0

    return-object v0
.end method

.method public final getChannelIdsCount()I
    .registers 2

    .prologue
    .line 3433
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->size()I

    move-result v0

    return v0
.end method

.method public final getChannelIdsList()Ljava/util/List;
    .registers 2

    .prologue
    .line 3427
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;
    .registers 2

    .prologue
    .line 3213
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->defaultInstance:Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 3197
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getDefaultInstanceForType()Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3303
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 3304
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 3305
    check-cast v0, Ljava/lang/String;

    .line 3313
    :goto_8
    return-object v0

    .line 3307
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 3309
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 3310
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 3311
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 3313
    goto :goto_8
.end method

.method public final getDeviceIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 3321
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 3322
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 3323
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 3326
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->deviceId_:Ljava/lang/Object;

    .line 3329
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final getParserForType()Lcom/google/protobuf/ah;
    .registers 2

    .prologue
    .line 3286
    sget-object v0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->PARSER:Lcom/google/protobuf/ah;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3495
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedSerializedSize:I

    .line 3496
    const/4 v2, -0x1

    if-eq v0, v2, :cond_9

    .line 3521
    :goto_8
    return v0

    .line 3499
    :cond_9
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_5f

    .line 3500
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3503
    :goto_19
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_28

    .line 3504
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3507
    :cond_28
    iget v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_39

    .line 3508
    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/e;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_39
    move v2, v1

    .line 3513
    :goto_3a
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3}, Lcom/google/protobuf/ab;->size()I

    move-result v3

    if-ge v1, v3, :cond_50

    .line 3514
    iget-object v3, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v3, v1}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3513
    add-int/lit8 v1, v1, 0x1

    goto :goto_3a

    .line 3517
    :cond_50
    add-int/2addr v0, v2

    .line 3518
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getChannelIdsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3520
    iput v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedSerializedSize:I

    goto :goto_8

    :cond_5f
    move v0, v1

    goto :goto_19
.end method

.method public final getUserId()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3346
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    .line 3347
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 3348
    check-cast v0, Ljava/lang/String;

    .line 3356
    :goto_8
    return-object v0

    .line 3350
    :cond_9
    check-cast v0, Lcom/google/protobuf/e;

    .line 3352
    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    .line 3353
    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 3354
    iput-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    :cond_17
    move-object v0, v1

    .line 3356
    goto :goto_8
.end method

.method public final getUserIdBytes()Lcom/google/protobuf/e;
    .registers 3

    .prologue
    .line 3364
    iget-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    .line 3365
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 3366
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 3369
    iput-object v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->userId_:Ljava/lang/Object;

    .line 3372
    :goto_e
    return-object v0

    :cond_f
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_e
.end method

.method public final hasAuthToken()Z
    .registers 3

    .prologue
    .line 3383
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasDeviceId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 3297
    iget v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasUserId()Z
    .registers 3

    .prologue
    .line 3340
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3457
    iget-byte v2, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    .line 3458
    const/4 v3, -0x1

    if-eq v2, v3, :cond_c

    if-ne v2, v0, :cond_a

    .line 3473
    :goto_9
    return v0

    :cond_a
    move v0, v1

    .line 3458
    goto :goto_9

    .line 3460
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->hasDeviceId()Z

    move-result v2

    if-nez v2, :cond_16

    .line 3461
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 3462
    goto :goto_9

    .line 3464
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->hasUserId()Z

    move-result v2

    if-nez v2, :cond_20

    .line 3465
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 3466
    goto :goto_9

    .line 3468
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->hasAuthToken()Z

    move-result v2

    if-nez v2, :cond_2a

    .line 3469
    iput-byte v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    move v0, v1

    .line 3470
    goto :goto_9

    .line 3472
    :cond_2a
    iput-byte v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->memoizedIsInitialized:B

    goto :goto_9
.end method

.method public final newBuilderForType()Lcom/google/android/youtube/core/model/proto/ai;
    .registers 2

    .prologue
    .line 3585
    invoke-static {}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->newBuilder()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 3197
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->newBuilderForType()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/youtube/core/model/proto/ai;
    .registers 2

    .prologue
    .line 3589
    invoke-static {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->newBuilder(Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;)Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 3197
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->toBuilder()Lcom/google/android/youtube/core/model/proto/ai;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3528
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 3478
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getSerializedSize()I

    .line 3479
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_13

    .line 3480
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getDeviceIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 3482
    :cond_13
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_20

    .line 3483
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getUserIdBytes()Lcom/google/protobuf/e;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 3485
    :cond_20
    iget v0, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2e

    .line 3486
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->getAuthTokenBytes()Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 3488
    :cond_2e
    const/4 v0, 0x0

    :goto_2f
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v1}, Lcom/google/protobuf/ab;->size()I

    move-result v1

    if-ge v0, v1, :cond_43

    .line 3489
    iget-object v1, p0, Lcom/google/android/youtube/core/model/proto/Notification$UnsubscribeRequest;->channelIds_:Lcom/google/protobuf/ab;

    invoke-interface {v1, v0}, Lcom/google/protobuf/ab;->a(I)Lcom/google/protobuf/e;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/e;)V

    .line 3488
    add-int/lit8 v0, v0, 0x1

    goto :goto_2f

    .line 3491
    :cond_43
    return-void
.end method
