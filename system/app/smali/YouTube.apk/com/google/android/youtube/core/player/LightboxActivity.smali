.class public Lcom/google/android/youtube/core/player/LightboxActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/youtube/core/player/ag;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 128
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 44
    .parameter

    .prologue
    .line 54
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/core/player/LightboxActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/youtube/core/player/ad;

    .line 59
    new-instance v41, Lcom/google/android/youtube/core/player/PlayerView;

    invoke-direct/range {v41 .. v42}, Lcom/google/android/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v2, Lcom/google/android/youtube/core/player/bx;

    invoke-virtual/range {v41 .. v41}, Lcom/google/android/youtube/core/player/PlayerView;->a()Lcom/google/android/youtube/core/player/au;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/google/android/youtube/core/player/bx;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/au;)V

    .line 61
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v5

    .line 62
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v6

    .line 63
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->l()Lcom/google/android/youtube/core/b/au;

    move-result-object v8

    .line 64
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->m()Lcom/google/android/youtube/core/b/as;

    move-result-object v9

    .line 65
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v11

    .line 66
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->S()Lcom/google/android/youtube/core/player/ax;

    move-result-object v21

    .line 67
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v20

    .line 68
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->N()Lcom/google/android/youtube/core/d;

    move-result-object v19

    .line 69
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->b()Lcom/google/android/youtube/core/player/e;

    move-result-object v22

    .line 70
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->a()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 71
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->c()Lcom/google/android/youtube/core/player/c;

    move-result-object v10

    .line 72
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->f()Ljava/lang/String;

    move-result-object v12

    .line 73
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->e()Z

    move-result v7

    .line 74
    new-instance v13, Lcom/google/android/youtube/core/player/af;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Lcom/google/android/youtube/core/player/af;-><init>(Lcom/google/android/youtube/core/player/LightboxActivity;B)V

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/core/player/LightboxActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const-string v15, "com.google.android.tv"

    invoke-virtual {v14, v15}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_dc

    .line 79
    new-instance v14, Lcom/google/android/youtube/core/player/TvControllerOverlay;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/youtube/core/player/TvControllerOverlay;-><init>(Landroid/content/Context;)V

    .line 80
    new-instance v16, Lcom/google/android/youtube/core/player/TvAdOverlay;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v6}, Lcom/google/android/youtube/core/player/TvAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;)V

    .line 88
    :goto_72
    const/4 v15, 0x0

    invoke-interface {v14, v15}, Lcom/google/android/youtube/core/player/ControllerOverlay;->setShowFullscreen(Z)V

    .line 89
    new-instance v15, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/youtube/core/player/DefaultBrandingOverlay;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v17, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/DefaultLiveOverlay;-><init>(Landroid/content/Context;)V

    .line 91
    new-instance v18, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/DefaultSubtitleOverlay;-><init>(Landroid/content/Context;)V

    .line 93
    const/16 v23, 0x5

    move/from16 v0, v23

    new-array v0, v0, [Lcom/google/android/youtube/core/player/as;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v15, v23, v24

    const/16 v24, 0x1

    aput-object v18, v23, v24

    const/16 v24, 0x2

    aput-object v14, v23, v24

    const/16 v24, 0x3

    aput-object v16, v23, v24

    const/16 v24, 0x4

    aput-object v17, v23, v24

    move-object/from16 v0, v41

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/PlayerView;->a([Lcom/google/android/youtube/core/player/as;)V

    .line 97
    if-eqz v7, :cond_f1

    .line 98
    invoke-interface {v3}, Lcom/google/android/youtube/core/player/ad;->d()Lcom/google/android/youtube/core/b/a;

    move-result-object v7

    move-object/from16 v3, p0

    .line 99
    invoke-static/range {v2 .. v22}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/a;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v10

    .line 110
    :goto_be
    new-instance v6, Lcom/google/android/youtube/core/player/ag;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/core/player/LightboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    move-object/from16 v7, p0

    move-object/from16 v9, v41

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/google/android/youtube/core/player/ag;-><init>(Landroid/app/Activity;Landroid/os/Bundle;Lcom/google/android/youtube/core/player/PlayerView;Lcom/google/android/youtube/core/player/Director;Lcom/google/android/youtube/core/b/al;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ag;

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/player/ag;->show()V

    .line 112
    return-void

    .line 82
    :cond_dc
    new-instance v14, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v11}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;)V

    .line 85
    new-instance v16, Lcom/google/android/youtube/core/player/DefaultAdOverlay;

    invoke-virtual {v14}, Lcom/google/android/youtube/core/player/DefaultControllerOverlay;->d()I

    move-result v15

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v15}, Lcom/google/android/youtube/core/player/DefaultAdOverlay;-><init>(Landroid/content/Context;I)V

    goto :goto_72

    :cond_f1
    move-object/from16 v23, v2

    move-object/from16 v24, p0

    move-object/from16 v25, v4

    move-object/from16 v26, v5

    move-object/from16 v27, v6

    move-object/from16 v28, v8

    move-object/from16 v29, v9

    move-object/from16 v30, v10

    move-object/from16 v31, v11

    move-object/from16 v32, v13

    move-object/from16 v33, v14

    move-object/from16 v34, v15

    move-object/from16 v35, v17

    move-object/from16 v36, v18

    move-object/from16 v37, v19

    move-object/from16 v38, v20

    move-object/from16 v39, v21

    move-object/from16 v40, v22

    .line 104
    invoke-static/range {v23 .. v40}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/bx;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/au;Lcom/google/android/youtube/core/b/as;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/aa;Lcom/google/android/youtube/core/player/ControllerOverlay;Lcom/google/android/youtube/core/player/j;Lcom/google/android/youtube/core/player/ah;Lcom/google/android/youtube/core/player/bd;Lcom/google/android/youtube/core/d;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/core/player/ax;Lcom/google/android/youtube/core/player/e;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v10

    goto :goto_be
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ag;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 123
    iget-object v0, p0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ag;->dismiss()V

    .line 125
    :cond_d
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 126
    return-void
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/youtube/core/player/LightboxActivity;->a:Lcom/google/android/youtube/core/player/ag;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/ag;->a()V

    .line 117
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 118
    return-void
.end method
