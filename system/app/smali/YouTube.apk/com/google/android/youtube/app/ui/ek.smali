.class public final Lcom/google/android/youtube/app/ui/ek;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/adapter/bb;
.implements Lcom/google/android/youtube/app/ui/c;
.implements Lcom/google/android/youtube/core/async/bn;
.implements Lcom/google/android/youtube/core/async/l;


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/az;

.field private final c:Lcom/google/android/youtube/app/ui/b;

.field private final d:Lcom/google/android/youtube/core/a/l;

.field private final e:Landroid/app/Activity;

.field private final f:Lcom/google/android/youtube/app/g;

.field private final g:Lcom/google/android/youtube/app/k;

.field private final h:Lcom/google/android/youtube/core/Analytics;

.field private final i:Lcom/google/android/youtube/core/b/al;

.field private final j:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private final k:Lcom/google/android/youtube/core/d;

.field private final l:Landroid/view/View;

.field private final m:Landroid/widget/EditText;

.field private final n:Ljava/lang/CharSequence;

.field private o:Lcom/google/android/youtube/core/model/Video;

.field private p:Lcom/google/android/youtube/core/model/UserAuth;

.field private final q:I

.field private final r:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;ILandroid/view/View;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/az;Lcom/google/android/youtube/app/ui/b;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p9, v0, v1

    const/4 v1, 0x1

    aput-object p10, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/youtube/app/ui/fa;->a(Landroid/view/LayoutInflater;)Lcom/google/android/youtube/app/adapter/cy;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 161
    const-string v0, "pagedOutline cannot be null"

    invoke-static {p11, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/az;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    .line 162
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/adapter/az;->a(Lcom/google/android/youtube/app/adapter/bb;)V

    .line 163
    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p12, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/b;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    .line 165
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/b;->a(Lcom/google/android/youtube/app/ui/c;)V

    .line 166
    const-string v0, "bodyOutline cannot be null"

    invoke-static {p10, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->d:Lcom/google/android/youtube/core/a/l;

    .line 168
    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    .line 169
    const-string v0, "youTubeAuthorizer cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/g;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/g;

    .line 171
    const-string v0, "config cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->g:Lcom/google/android/youtube/app/k;

    .line 172
    const-string v0, "analytics cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->h:Lcom/google/android/youtube/core/Analytics;

    .line 173
    const-string v0, "gdataClient cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/b/al;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->i:Lcom/google/android/youtube/core/b/al;

    .line 174
    invoke-interface {p5}, Lcom/google/android/youtube/core/b/al;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->j:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    .line 175
    const-string v0, "errorHelper cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/d;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->k:Lcom/google/android/youtube/core/d;

    .line 177
    const-string v0, "addCommentHeader cannot be null"

    invoke-static {p8, v0}, Lcom/google/android/ytremote/util/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/view/View;

    .line 179
    const v0, 0x7f080037

    invoke-virtual {p8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/EditText;

    .line 180
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    .line 181
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iput p7, p0, Lcom/google/android/youtube/app/ui/ek;->q:I

    .line 185
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->r:Ljava/lang/String;

    .line 186
    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/ek;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ek;
    .registers 29
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00d0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 96
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 98
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v2

    if-eqz v2, :cond_8b

    const v2, 0x7f040097

    :goto_23
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    .line 100
    new-instance v17, Lcom/google/android/youtube/app/adapter/cy;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/youtube/app/adapter/cy;-><init>(Landroid/view/View;)V

    .line 102
    new-instance v2, Lcom/google/android/youtube/app/adapter/az;

    const v4, 0x7f040021

    new-instance v5, Lcom/google/android/youtube/app/adapter/ad;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/youtube/app/adapter/ad;-><init>(Landroid/content/Context;)V

    invoke-interface/range {p4 .. p4}, Lcom/google/android/youtube/core/b/al;->w()Lcom/google/android/youtube/core/async/av;

    move-result-object v6

    move-object/from16 v3, p0

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v2 .. v11}, Lcom/google/android/youtube/app/adapter/az;-><init>(Landroid/app/Activity;ILcom/google/android/youtube/app/adapter/cb;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;IIILcom/google/android/youtube/core/a/g;)V

    .line 113
    new-instance v15, Lcom/google/android/youtube/app/ui/b;

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v15, v3, v4, v12}, Lcom/google/android/youtube/app/ui/b;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/String;)V

    .line 118
    new-instance v13, Lcom/google/android/youtube/core/a/l;

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/youtube/core/a/e;

    const/4 v4, 0x0

    aput-object v17, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    const/4 v4, 0x2

    aput-object v15, v3, v4

    invoke-direct {v13, v3}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    .line 123
    new-instance v12, Lcom/google/android/youtube/app/adapter/ac;

    move-object/from16 v0, p0

    move-object/from16 v1, p10

    invoke-direct {v12, v0, v1, v14, v13}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;Lcom/google/android/youtube/core/a/e;)V

    .line 129
    new-instance v3, Lcom/google/android/youtube/app/ui/ek;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, v16

    move-object v14, v2

    invoke-direct/range {v3 .. v15}, Lcom/google/android/youtube/app/ui/ek;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/g;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/d;ILandroid/view/View;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/adapter/az;Lcom/google/android/youtube/app/ui/b;)V

    return-object v3

    .line 98
    :cond_8b
    const v2, 0x7f040006

    goto :goto_23
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/ek;Landroid/widget/EditText;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_45

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->h:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "Comment"

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->i:Lcom/google/android/youtube/core/b/al;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ek;->o:Lcom/google/android/youtube/core/model/Video;

    iget-object v2, v2, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->p:Lcom/google/android/youtube/core/model/UserAuth;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    invoke-static {v4, p0}, Lcom/google/android/youtube/core/async/c;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/l;)Lcom/google/android/youtube/core/async/c;

    move-result-object v4

    invoke-interface {v1, v2, v3, v0, v4}, Lcom/google/android/youtube/core/b/al;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Ljava/lang/String;Lcom/google/android/youtube/core/async/l;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/widget/EditText;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :goto_44
    return-void

    :cond_45
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->k:Lcom/google/android/youtube/core/d;

    const v2, 0x7f0b01f7

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/d;->a(I)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_44
.end method

.method private a(Ljava/lang/CharSequence;)V
    .registers 3
    .parameter

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->m:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 292
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .registers 4
    .parameter

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ek;->p:Lcom/google/android/youtube/core/model/UserAuth;

    .line 256
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 257
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    .line 258
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .registers 8
    .parameter

    .prologue
    const/16 v1, 0x8

    const/4 v5, 0x0

    .line 193
    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ek;->o:Lcom/google/android/youtube/core/model/Video;

    .line 194
    if-eqz p1, :cond_42

    .line 195
    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    if-eqz v0, :cond_2a

    .line 196
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/ek;->j:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v3, p1, Lcom/google/android/youtube/core/model/Video;->commentsUri:Landroid/net/Uri;

    iget v4, p0, Lcom/google/android/youtube/app/ui/ek;->q:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b(Landroid/net/Uri;I)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/az;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 208
    :goto_29
    return-void

    .line 200
    :cond_2a
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0117

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/google/android/youtube/app/ui/b;->a(Ljava/lang/String;Z)V

    goto :goto_29

    .line 205
    :cond_42
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->a_()V

    goto :goto_29
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 51
    if-eqz p2, :cond_18

    const/16 v0, 0x193

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/d;->a(Ljava/lang/Throwable;I)Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->k:Lcom/google/android/youtube/core/d;

    const v1, 0x7f0b00d9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    :goto_12
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    return-void

    :cond_18
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->k:Lcom/google/android/youtube/core/d;

    const v1, 0x7f0b00d8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/d;->a(I)V

    goto :goto_12
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 51
    check-cast p2, Lcom/google/android/youtube/core/model/Comment;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const v1, 0x7f0b01f6

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0, v2, p2}, Lcom/google/android/youtube/app/adapter/az;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->k:Lcom/google/android/youtube/core/d;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/d;->b(Ljava/lang/Throwable;)V

    .line 267
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/b;->b(Ljava/lang/String;Z)V

    .line 338
    return-void
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->d:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    .line 350
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->d()V

    .line 313
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->d()V

    .line 326
    return-void
.end method

.method public final d(I)Z
    .registers 3
    .parameter

    .prologue
    .line 354
    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .registers 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->r:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/b;->a(Ljava/lang/String;Z)V

    .line 330
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->k_()V

    .line 334
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->e()V

    .line 342
    return-void
.end method

.method public final h()V
    .registers 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->a:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->d()V

    .line 346
    return-void
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/core/model/Video;)V

    .line 190
    return-void
.end method

.method public final i_()V
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->n:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    .line 262
    return-void
.end method

.method public final j()Landroid/app/Dialog;
    .registers 7

    .prologue
    const/4 v2, 0x5

    .line 211
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040005

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 213
    new-instance v0, Lcom/google/android/youtube/core/ui/x;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    invoke-direct {v0, v3}, Lcom/google/android/youtube/core/ui/x;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b01f3

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/ui/x;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const v4, 0x7f0b01f2

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/ui/em;

    invoke-direct {v4, p0, v1}, Lcom/google/android/youtube/app/ui/em;-><init>(Lcom/google/android/youtube/app/ui/ek;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const/high16 v4, 0x104

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/youtube/app/ui/el;

    invoke-direct {v4, p0, v1}, Lcom/google/android/youtube/app/ui/el;-><init>(Lcom/google/android/youtube/app/ui/ek;Landroid/widget/EditText;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    move v3, v2

    move v4, v2

    move v5, v2

    .line 229
    invoke-virtual/range {v0 .. v5}, Landroid/app/AlertDialog;->setView(Landroid/view/View;IIII)V

    .line 230
    new-instance v2, Lcom/google/android/youtube/app/ui/en;

    invoke-direct {v2, p0, v1}, Lcom/google/android/youtube/app/ui/en;-><init>(Lcom/google/android/youtube/app/ui/ek;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 242
    return-object v0
.end method

.method public final l_()V
    .registers 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->c:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/b;->e()V

    .line 317
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const v1, 0x7f0b01f5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/ek;->a(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ek;->f:Lcom/google/android/youtube/app/g;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ek;->e:Landroid/app/Activity;

    const v2, 0x7f0b01bc

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ek;->g:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->d()Z

    move-result v3

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/google/android/youtube/app/g;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bn;IZ)V

    .line 252
    return-void
.end method
