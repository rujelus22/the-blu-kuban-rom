.class public final Lcom/google/android/youtube/app/honeycomb/phone/x;
.super Lcom/google/android/youtube/app/honeycomb/phone/u;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/youtube/app/ui/by;

.field private final c:Landroid/view/View;

.field private final d:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/async/av;[Lcom/google/android/youtube/core/async/GDataRequest;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    .line 43
    const-string v1, "requester cannot be null"

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string v1, "request cannot be null"

    move-object/from16 v0, p6

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 47
    invoke-static {}, Lcom/google/android/youtube/core/utils/Util;->c()Z

    move-result v1

    if-eqz v1, :cond_88

    const v1, 0x7f04009b

    move v2, v1

    :goto_1f
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->o()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    .line 51
    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->d:Ljava/lang/String;

    .line 53
    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/google/android/youtube/app/YouTubeApplication;

    .line 55
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->c:Landroid/view/View;

    const v2, 0x7f080053

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/google/android/youtube/core/ui/PagedListView;

    .line 59
    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/b/al;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->p()Lcom/google/android/youtube/core/b/an;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/core/b/ap;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/utils/l;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->Y()Lcom/google/android/youtube/core/j;

    move-result-object v7

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/google/android/youtube/app/adapter/bu;->a(Landroid/content/Context;Lcom/google/android/youtube/core/b/al;Lcom/google/android/youtube/core/b/an;Lcom/google/android/youtube/core/b/ap;Lcom/google/android/youtube/core/utils/l;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/j;)Lcom/google/android/youtube/app/adapter/bt;

    move-result-object v1

    .line 68
    new-instance v2, Lcom/google/android/youtube/app/ui/by;

    invoke-direct {v2, p1, v1}, Lcom/google/android/youtube/app/ui/by;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->b:Lcom/google/android/youtube/app/ui/by;

    .line 69
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a()V

    .line 71
    new-instance v1, Lcom/google/android/youtube/app/ui/eb;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->b:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->N()Lcom/google/android/youtube/core/d;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->w()Lcom/google/android/youtube/app/a;

    move-result-object v8

    invoke-virtual {v9}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Lcom/google/android/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/youtube/core/Analytics$VideoCategory;->HomeFeed:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v2, p1

    move-object v3, v10

    move-object/from16 v5, p5

    move/from16 v9, p3

    move-object/from16 v10, p4

    invoke-direct/range {v1 .. v12}, Lcom/google/android/youtube/app/ui/eb;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/g;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/av;Lcom/google/android/youtube/core/d;ZLcom/google/android/youtube/app/a;ZLcom/google/android/youtube/core/b/aq;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    .line 83
    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/eb;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    .line 84
    return-void

    .line 47
    :cond_88
    const v1, 0x7f040038

    move v2, v1

    goto :goto_1f
.end method

.method private a()V
    .registers 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 94
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->b:Lcom/google/android/youtube/app/ui/by;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/by;->a(I)V

    .line 95
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/u;->a(Landroid/content/res/Configuration;)V

    .line 89
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a()V

    .line 90
    return-void
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/x;->d:Ljava/lang/String;

    return-object v0
.end method
