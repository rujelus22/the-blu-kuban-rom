.class public final Lcom/google/android/plus1/proto/e;
.super Lcom/google/protobuf/o;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 501
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/plus1/proto/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 2
    .parameter

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/e;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/e;->a()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/google/android/plus1/proto/e;
    .registers 4
    .parameter

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasTotalPlusOneCount:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1502(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z

    .line 676
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->totalPlusOneCount_:I
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1602(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;I)I

    .line 677
    return-object p0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/plus1/proto/e;
    .registers 4
    .parameter

    .prologue
    .line 636
    if-nez p1, :cond_8

    .line 637
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 639
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasUri:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1102(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z

    .line 640
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->uri_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1202(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/lang/String;)Ljava/lang/String;

    .line 641
    return-object p0
.end method

.method private a(Z)Lcom/google/android/plus1/proto/e;
    .registers 4
    .parameter

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasValue:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1302(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z

    .line 658
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->value_:Z
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1402(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z

    .line 659
    return-object p0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/plus1/proto/e;
    .registers 4
    .parameter

    .prologue
    .line 744
    if-nez p1, :cond_8

    .line 745
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 747
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasAbuseToken:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1702(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Z)Z

    .line 748
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->abuseToken_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1802(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/lang/String;)Ljava/lang/String;

    .line 749
    return-object p0
.end method

.method static synthetic g()Lcom/google/android/plus1/proto/e;
    .registers 1

    .prologue
    .line 495
    invoke-static {}, Lcom/google/android/plus1/proto/e;->h()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method private static h()Lcom/google/android/plus1/proto/e;
    .registers 3

    .prologue
    .line 504
    new-instance v0, Lcom/google/android/plus1/proto/e;

    invoke-direct {v0}, Lcom/google/android/plus1/proto/e;-><init>()V

    .line 505
    new-instance v1, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;-><init>(Lcom/google/android/plus1/proto/a;)V

    iput-object v1, v0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    .line 506
    return-object v0
.end method

.method private i()Lcom/google/android/plus1/proto/e;
    .registers 3

    .prologue
    .line 523
    invoke-static {}, Lcom/google/android/plus1/proto/e;->h()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    invoke-virtual {v0, v1}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    if-nez v0, :cond_c

    .line 551
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 554
    :cond_c
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_25

    .line 555
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    iget-object v1, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1002(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/util/List;)Ljava/util/List;

    .line 558
    :cond_25
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    .line 559
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    .line 560
    return-object v0
.end method

.method public final a(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Lcom/google/android/plus1/proto/e;
    .registers 4
    .parameter

    .prologue
    .line 564
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 583
    :cond_6
    :goto_6
    return-object p0

    .line 565
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasUri()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 566
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/e;

    .line 568
    :cond_14
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 569
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->a(Z)Lcom/google/android/plus1/proto/e;

    .line 571
    :cond_21
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasTotalPlusOneCount()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 572
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getTotalPlusOneCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->a(I)Lcom/google/android/plus1/proto/e;

    .line 574
    :cond_2e
    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 575
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 576
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1002(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/util/List;)Ljava/util/List;

    .line 578
    :cond_4e
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v0

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 580
    :cond_5b
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->hasAbuseToken()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 581
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getAbuseToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/e;

    goto :goto_6
.end method

.method public final a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/e;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v0

    .line 592
    sparse-switch v0, :sswitch_data_62

    .line 596
    invoke-virtual {p1, v0}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 597
    :sswitch_d
    return-object p0

    .line 602
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/e;

    goto :goto_0

    .line 606
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/h;->c()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->a(Z)Lcom/google/android/plus1/proto/e;

    goto :goto_0

    .line 610
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->a(I)Lcom/google/android/plus1/proto/e;

    goto :goto_0

    .line 614
    :sswitch_26
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    .line 615
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/af;Lcom/google/protobuf/i;)V

    .line 616
    invoke-virtual {v0}, Lcom/google/android/plus1/proto/d;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    if-nez v0, :cond_39

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_39
    iget-object v1, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4f

    iget-object v1, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1002(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;Ljava/util/List;)Ljava/util/List;

    :cond_4f
    iget-object v1, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->friend_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->access$1000(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 620
    :sswitch_59
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/e;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/e;

    goto :goto_0

    .line 592
    nop

    :sswitch_data_62
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x10 -> :sswitch_16
        0x18 -> :sswitch_1e
        0x22 -> :sswitch_26
        0x2a -> :sswitch_59
    .end sparse-switch
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 495
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 495
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 495
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/android/plus1/proto/e;->i()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/android/plus1/proto/e;->i()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/android/plus1/proto/e;->i()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/e;->a()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/e;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    invoke-static {v0}, Lcom/google/android/plus1/proto/e;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/e;->a()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 495
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/plus1/proto/e;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    invoke-virtual {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->isInitialized()Z

    move-result v0

    return v0
.end method
