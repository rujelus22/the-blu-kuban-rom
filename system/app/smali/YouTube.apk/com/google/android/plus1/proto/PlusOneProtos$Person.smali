.class public final Lcom/google/android/plus1/proto/PlusOneProtos$Person;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"


# static fields
.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0x1

.field public static final THUMBNAILURL_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Person;


# instance fields
.field private displayName_:Ljava/lang/String;

.field private hasDisplayName:Z

.field private hasThumbnailUrl:Z

.field private memoizedSerializedSize:I

.field private thumbnailUrl_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 300
    new-instance v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;-><init>(Z)V

    .line 301
    sput-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    .line 303
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->displayName_:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->thumbnailUrl_:Ljava/lang/String;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->memoizedSerializedSize:I

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/plus1/proto/a;)V
    .registers 2
    .parameter

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->displayName_:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->thumbnailUrl_:Ljava/lang/String;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->memoizedSerializedSize:I

    .line 17
    return-void
.end method

.method static synthetic access$302(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasDisplayName:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->displayName_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 11
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasThumbnailUrl:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 11
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->thumbnailUrl_:Ljava/lang/String;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    return-object v0
.end method

.method private initFields()V
    .registers 1

    .prologue
    .line 43
    return-void
.end method

.method public static newBuilder()Lcom/google/android/plus1/proto/d;
    .registers 1

    .prologue
    .line 144
    invoke-static {}, Lcom/google/android/plus1/proto/d;->g()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$Person;)Lcom/google/android/plus1/proto/d;
    .registers 2
    .parameter

    .prologue
    .line 147
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/PlusOneProtos$Person;)Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3
    .parameter

    .prologue
    .line 113
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    .line 114
    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/d;->b(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 115
    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    .line 117
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    .line 125
    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/d;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 126
    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    .line 128
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 2
    .parameter

    .prologue
    .line 80
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 2
    .parameter

    .prologue
    .line 134
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 140
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 2
    .parameter

    .prologue
    .line 102
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/d;->a(Ljava/io/InputStream;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/d;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 2
    .parameter

    .prologue
    .line 91
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/d;->a([B)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/d;->a([BLcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/d;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 2

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->displayName_:Ljava/lang/String;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 4

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->memoizedSerializedSize:I

    .line 62
    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 74
    :goto_5
    return v0

    .line 64
    :cond_6
    const/4 v0, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasDisplayName()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 69
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasThumbnailUrl()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 70
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_28
    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->memoizedSerializedSize:I

    goto :goto_5
.end method

.method public final getThumbnailUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->thumbnailUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public final hasDisplayName()Z
    .registers 2

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasDisplayName:Z

    return v0
.end method

.method public final hasThumbnailUrl()Z
    .registers 2

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasThumbnailUrl:Z

    return v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public final newBuilderForType()Lcom/google/android/plus1/proto/d;
    .registers 2

    .prologue
    .line 145
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilderForType()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/plus1/proto/d;
    .registers 2

    .prologue
    .line 149
    invoke-static {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$Person;)Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->toBuilder()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 4
    .parameter

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getSerializedSize()I

    .line 51
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 54
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasThumbnailUrl()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 55
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILjava/lang/String;)V

    .line 57
    :cond_1f
    return-void
.end method
