.class public final Lcom/google/android/plus1/proto/f;
.super Lcom/google/protobuf/o;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 1651
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 2
    .parameter

    .prologue
    .line 1645
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/f;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-direct {p0}, Lcom/google/android/plus1/proto/f;->i()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lcom/google/android/plus1/proto/f;
    .registers 1

    .prologue
    .line 1645
    invoke-static {}, Lcom/google/android/plus1/proto/f;->g()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method private static g()Lcom/google/android/plus1/proto/f;
    .registers 3

    .prologue
    .line 1654
    new-instance v0, Lcom/google/android/plus1/proto/f;

    invoke-direct {v0}, Lcom/google/android/plus1/proto/f;-><init>()V

    .line 1655
    new-instance v1, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;-><init>(Lcom/google/android/plus1/proto/a;)V

    iput-object v1, v0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    .line 1656
    return-object v0
.end method

.method private h()Lcom/google/android/plus1/proto/f;
    .registers 3

    .prologue
    .line 1673
    invoke-static {}, Lcom/google/android/plus1/proto/f;->g()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-virtual {v0, v1}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method private i()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 3

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    if-nez v0, :cond_c

    .line 1701
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1704
    :cond_c
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_25

    .line 1705
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4202(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;

    .line 1708
    :cond_25
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-eq v0, v1, :cond_3e

    .line 1709
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4302(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;

    .line 1712
    :cond_3e
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    .line 1713
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    .line 1714
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/f;
    .registers 5
    .parameter

    .prologue
    .line 1718
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 1734
    :cond_6
    :goto_6
    return-object p0

    .line 1719
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 1720
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getAccountStatus()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-virtual {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus()Z

    move-result v1

    if-eqz v1, :cond_9e

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4500(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v1

    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v2

    if-eq v1, v2, :cond_9e

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    iget-object v2, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    invoke-static {v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4500(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/plus1/proto/b;->a()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    invoke-static {v1, v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4502(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    :goto_3c
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4402(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Z)Z

    .line 1722
    :cond_42
    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6f

    .line 1723
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_62

    .line 1724
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4202(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;

    .line 1726
    :cond_62
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1728
    :cond_6f
    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1729
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 1730
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4302(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;

    .line 1732
    :cond_8f
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v0

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    .line 1720
    :cond_9e
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    invoke-static {v1, v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4502(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    goto :goto_3c
.end method

.method public final a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/f;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1742
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v0

    .line 1743
    sparse-switch v0, :sswitch_data_a6

    .line 1747
    invoke-virtual {p1, v0}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748
    :sswitch_d
    return-object p0

    .line 1753
    :sswitch_e
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->newBuilder()Lcom/google/android/plus1/proto/b;

    move-result-object v0

    .line 1754
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-virtual {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 1755
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-virtual {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getAccountStatus()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/plus1/proto/b;->a(Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/b;

    .line 1757
    :cond_23
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/af;Lcom/google/protobuf/i;)V

    .line 1758
    invoke-virtual {v0}, Lcom/google/android/plus1/proto/b;->a()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    if-nez v0, :cond_32

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_32
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    const/4 v2, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus:Z
    invoke-static {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4402(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Z)Z

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    invoke-static {v1, v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4502(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    goto :goto_0

    .line 1762
    :sswitch_3e
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;->newBuilder()Lcom/google/android/plus1/proto/e;

    move-result-object v0

    .line 1763
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/af;Lcom/google/protobuf/i;)V

    .line 1764
    invoke-virtual {v0}, Lcom/google/android/plus1/proto/e;->a()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    move-result-object v0

    if-nez v0, :cond_51

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_51
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_67

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4202(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;

    :cond_67
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1768
    :sswitch_71
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;->newBuilder()Lcom/google/android/plus1/proto/c;

    move-result-object v0

    .line 1769
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/h;->a(Lcom/google/protobuf/af;Lcom/google/protobuf/i;)V

    .line 1770
    invoke-virtual {v0}, Lcom/google/android/plus1/proto/c;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    move-result-object v0

    if-nez v0, :cond_84

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_84
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9a

    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4302(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;

    :cond_9a
    iget-object v1, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    #getter for: Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1743
    nop

    :sswitch_data_a6
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_3e
        0x1a -> :sswitch_71
    .end sparse-switch
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 1645
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1645
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1645
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 1645
    invoke-direct {p0}, Lcom/google/android/plus1/proto/f;->h()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1645
    invoke-direct {p0}, Lcom/google/android/plus1/proto/f;->h()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 1645
    invoke-direct {p0}, Lcom/google/android/plus1/proto/f;->h()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1645
    invoke-direct {p0}, Lcom/google/android/plus1/proto/f;->i()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/f;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-direct {p0}, Lcom/google/android/plus1/proto/f;->i()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1645
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/google/android/plus1/proto/f;->a:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-virtual {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->isInitialized()Z

    move-result v0

    return v0
.end method
