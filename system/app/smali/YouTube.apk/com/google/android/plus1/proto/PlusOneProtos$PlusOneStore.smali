.class public final Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"


# static fields
.field public static final ACCOUNTSTATUS_FIELD_NUMBER:I = 0x1

.field public static final OPERATION_FIELD_NUMBER:I = 0x3

.field public static final PLUSONE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;


# instance fields
.field private accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

.field private hasAccountStatus:Z

.field private memoizedSerializedSize:I

.field private operation_:Ljava/util/List;

.field private plusOne_:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1921
    new-instance v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;-><init>(Z)V

    .line 1922
    sput-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 1924
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 1483
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1506
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    .line 1518
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    .line 1549
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->memoizedSerializedSize:I

    .line 1484
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 1485
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/plus1/proto/a;)V
    .registers 2
    .parameter

    .prologue
    .line 1480
    invoke-direct {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1486
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1506
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    .line 1518
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    .line 1549
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->memoizedSerializedSize:I

    .line 1486
    return-void
.end method

.method static synthetic access$4200(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 1480
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4300(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 1480
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4402(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1480
    iput-boolean p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter

    .prologue
    .line 1480
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;)Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 1

    .prologue
    .line 1490
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    return-object v0
.end method

.method private initFields()V
    .registers 2

    .prologue
    .line 1529
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    .line 1530
    return-void
.end method

.method public static newBuilder()Lcom/google/android/plus1/proto/f;
    .registers 1

    .prologue
    .line 1638
    invoke-static {}, Lcom/google/android/plus1/proto/f;->a()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/f;
    .registers 2
    .parameter

    .prologue
    .line 1641
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 3
    .parameter

    .prologue
    .line 1607
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    .line 1608
    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/f;->b(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1609
    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    .line 1611
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1618
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    .line 1619
    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/f;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1620
    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    .line 1622
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static parseFrom(Lcom/google/protobuf/e;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 2
    .parameter

    .prologue
    .line 1574
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1580
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 2
    .parameter

    .prologue
    .line 1628
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1634
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 2
    .parameter

    .prologue
    .line 1596
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/f;->a(Ljava/io/InputStream;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1602
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/f;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 2
    .parameter

    .prologue
    .line 1585
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/plus1/proto/f;->a([B)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/i;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1591
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/android/plus1/proto/f;->a([BLcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/f;

    invoke-static {v0}, Lcom/google/android/plus1/proto/f;->a(Lcom/google/android/plus1/proto/f;)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAccountStatus()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;
    .registers 2

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->accountStatus_:Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;
    .registers 2

    .prologue
    .line 1494
    sget-object v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->defaultInstance:Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 1480
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getDefaultInstanceForType()Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;

    move-result-object v0

    return-object v0
.end method

.method public final getOperation(I)Lcom/google/android/plus1/proto/PlusOneProtos$Operation;
    .registers 3
    .parameter

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    return-object v0
.end method

.method public final getOperationCount()I
    .registers 2

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getOperationList()Ljava/util/List;
    .registers 2

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->operation_:Ljava/util/List;

    return-object v0
.end method

.method public final getPlusOne(I)Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;
    .registers 3
    .parameter

    .prologue
    .line 1513
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    return-object v0
.end method

.method public final getPlusOneCount()I
    .registers 2

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getPlusOneList()Ljava/util/List;
    .registers 2

    .prologue
    .line 1509
    iget-object v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->plusOne_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 5

    .prologue
    .line 1551
    iget v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->memoizedSerializedSize:I

    .line 1552
    const/4 v0, -0x1

    if-eq v1, v0, :cond_6

    .line 1568
    :goto_5
    return v1

    .line 1554
    :cond_6
    const/4 v0, 0x0

    .line 1555
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1556
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getAccountStatus()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1559
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getPlusOneList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    .line 1560
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_21

    .line 1563
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getOperationList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    .line 1564
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->b(ILcom/google/protobuf/ae;)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_3d

    .line 1567
    :cond_50
    iput v1, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->memoizedSerializedSize:I

    goto :goto_5
.end method

.method public final hasAccountStatus()Z
    .registers 2

    .prologue
    .line 1501
    iget-boolean v0, p0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus:Z

    return v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 1532
    const/4 v0, 0x1

    return v0
.end method

.method public final newBuilderForType()Lcom/google/android/plus1/proto/f;
    .registers 2

    .prologue
    .line 1639
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1480
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilderForType()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final toBuilder()Lcom/google/android/plus1/proto/f;
    .registers 2

    .prologue
    .line 1643
    invoke-static {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->newBuilder(Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;)Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/af;
    .registers 2

    .prologue
    .line 1480
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->toBuilder()Lcom/google/android/plus1/proto/f;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 5
    .parameter

    .prologue
    .line 1537
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getSerializedSize()I

    .line 1538
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->hasAccountStatus()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1539
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getAccountStatus()Lcom/google/android/plus1/proto/PlusOneProtos$AccountStatus;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    .line 1541
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getPlusOneList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOne;

    .line 1542
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    goto :goto_19

    .line 1544
    :cond_2a
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/PlusOneProtos$PlusOneStore;->getOperationList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_32
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/plus1/proto/PlusOneProtos$Operation;

    .line 1545
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->a(ILcom/google/protobuf/ae;)V

    goto :goto_32

    .line 1547
    :cond_43
    return-void
.end method
