.class public final Lcom/google/android/plus1/proto/d;
.super Lcom/google/protobuf/o;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/protobuf/o;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/plus1/proto/d;)Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 2
    .parameter

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/d;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/d;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/plus1/proto/d;
    .registers 4
    .parameter

    .prologue
    .line 262
    if-nez p1, :cond_8

    .line 263
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 265
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasDisplayName:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->access$302(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Z)Z

    .line 266
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Person;->displayName_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->access$402(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    .line 267
    return-object p0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/plus1/proto/d;
    .registers 4
    .parameter

    .prologue
    .line 283
    if-nez p1, :cond_8

    .line 284
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 286
    :cond_8
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    const/4 v1, 0x1

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasThumbnailUrl:Z
    invoke-static {v0, v1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->access$502(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Z)Z

    .line 287
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    #setter for: Lcom/google/android/plus1/proto/PlusOneProtos$Person;->thumbnailUrl_:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->access$602(Lcom/google/android/plus1/proto/PlusOneProtos$Person;Ljava/lang/String;)Ljava/lang/String;

    .line 288
    return-object p0
.end method

.method static synthetic g()Lcom/google/android/plus1/proto/d;
    .registers 1

    .prologue
    .line 151
    invoke-static {}, Lcom/google/android/plus1/proto/d;->h()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method private static h()Lcom/google/android/plus1/proto/d;
    .registers 3

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/plus1/proto/d;

    invoke-direct {v0}, Lcom/google/android/plus1/proto/d;-><init>()V

    .line 161
    new-instance v1, Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;-><init>(Lcom/google/android/plus1/proto/a;)V

    iput-object v1, v0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    .line 162
    return-object v0
.end method

.method private i()Lcom/google/android/plus1/proto/d;
    .registers 3

    .prologue
    .line 179
    invoke-static {}, Lcom/google/android/plus1/proto/d;->h()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    invoke-virtual {v0, v1}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/android/plus1/proto/PlusOneProtos$Person;)Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/plus1/proto/PlusOneProtos$Person;
    .registers 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    if-nez v0, :cond_c

    .line 207
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "build() has already been called on this Builder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :cond_c
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    .line 211
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    .line 212
    return-object v0
.end method

.method public final a(Lcom/google/android/plus1/proto/PlusOneProtos$Person;)Lcom/google/android/plus1/proto/d;
    .registers 3
    .parameter

    .prologue
    .line 216
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 223
    :cond_6
    :goto_6
    return-object p0

    .line 217
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 218
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/d;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/d;

    .line 220
    :cond_14
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->hasThumbnailUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 221
    invoke-virtual {p1}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/d;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/d;

    goto :goto_6
.end method

.method public final a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/d;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/h;->a()I

    move-result v0

    .line 232
    sparse-switch v0, :sswitch_data_1e

    .line 236
    invoke-virtual {p1, v0}, Lcom/google/protobuf/h;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    :sswitch_d
    return-object p0

    .line 242
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/d;->a(Ljava/lang/String;)Lcom/google/android/plus1/proto/d;

    goto :goto_0

    .line 246
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/plus1/proto/d;->b(Ljava/lang/String;)Lcom/google/android/plus1/proto/d;

    goto :goto_0

    .line 232
    :sswitch_data_1e
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_16
    .end sparse-switch
.end method

.method public final synthetic b()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 151
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-virtual {p0, p1, p2}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c()Lcom/google/protobuf/o;
    .registers 2

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/plus1/proto/d;->i()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/plus1/proto/d;->i()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/google/protobuf/b;
    .registers 2

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/google/android/plus1/proto/d;->i()Lcom/google/android/plus1/proto/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic e()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/d;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    if-eqz v0, :cond_11

    invoke-virtual {p0}, Lcom/google/android/plus1/proto/d;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    invoke-static {v0}, Lcom/google/android/plus1/proto/d;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/plus1/proto/d;->a()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic getDefaultInstanceForType()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 151
    invoke-static {}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->getDefaultInstance()Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/plus1/proto/d;->a:Lcom/google/android/plus1/proto/PlusOneProtos$Person;

    invoke-virtual {v0}, Lcom/google/android/plus1/proto/PlusOneProtos$Person;->isInitialized()Z

    move-result v0

    return v0
.end method
