.class public final Lcom/google/api/client/http/c/a;
.super Lcom/google/api/client/http/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lcom/google/api/client/json/d;


# direct methods
.method public constructor <init>(Lcom/google/api/client/json/d;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 64
    sget-object v0, Lcom/google/api/client/json/c;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/api/client/http/a;-><init>(Ljava/lang/String;)V

    .line 65
    invoke-static {p1}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/json/d;

    iput-object v0, p0, Lcom/google/api/client/http/c/a;->b:Lcom/google/api/client/json/d;

    .line 66
    invoke-static {p2}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/http/c/a;->a:Ljava/lang/Object;

    .line 67
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)V
    .registers 4
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/api/client/http/c/a;->b:Lcom/google/api/client/json/d;

    invoke-virtual {p0}, Lcom/google/api/client/http/c/a;->c()Ljava/nio/charset/Charset;

    invoke-virtual {v0, p1}, Lcom/google/api/client/json/d;->a(Ljava/io/OutputStream;)Lcom/google/api/client/json/e;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/google/api/client/http/c/a;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/api/client/json/e;->a(Ljava/lang/Object;)V

    .line 72
    invoke-virtual {v0}, Lcom/google/api/client/json/e;->a()V

    .line 73
    return-void
.end method
