.class public final Lcom/google/api/client/http/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/api/client/http/t;

.field private final b:Lcom/google/api/client/http/q;


# direct methods
.method constructor <init>(Lcom/google/api/client/http/t;Lcom/google/api/client/http/q;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/api/client/http/p;->a:Lcom/google/api/client/http/t;

    .line 54
    iput-object p2, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/q;

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/api/client/http/HttpMethod;Lcom/google/api/client/http/g;Lcom/google/api/client/http/h;)Lcom/google/api/client/http/n;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/api/client/http/p;->a:Lcom/google/api/client/http/t;

    invoke-virtual {v0}, Lcom/google/api/client/http/t;->b()Lcom/google/api/client/http/n;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/q;

    if-eqz v1, :cond_f

    .line 91
    iget-object v1, p0, Lcom/google/api/client/http/p;->b:Lcom/google/api/client/http/q;

    invoke-interface {v1, v0}, Lcom/google/api/client/http/q;->a(Lcom/google/api/client/http/n;)V

    .line 93
    :cond_f
    invoke-virtual {v0, p1}, Lcom/google/api/client/http/n;->a(Lcom/google/api/client/http/HttpMethod;)Lcom/google/api/client/http/n;

    .line 94
    if-eqz p2, :cond_17

    .line 95
    invoke-virtual {v0, p2}, Lcom/google/api/client/http/n;->a(Lcom/google/api/client/http/g;)Lcom/google/api/client/http/n;

    .line 97
    :cond_17
    if-eqz p3, :cond_1c

    .line 98
    invoke-virtual {v0, p3}, Lcom/google/api/client/http/n;->a(Lcom/google/api/client/http/h;)Lcom/google/api/client/http/n;

    .line 100
    :cond_1c
    return-object v0
.end method
