.class public Lcom/google/api/client/auth/oauth2/TokenResponseException;
.super Lcom/google/api/client/http/HttpResponseException;
.source "SourceFile"


# static fields
.field private static final serialVersionUID:J = 0x37cc5b6d7204050cL


# instance fields
.field private final transient details:Lcom/google/api/client/auth/oauth2/a;


# direct methods
.method private constructor <init>(Lcom/google/api/client/http/r;Lcom/google/api/client/auth/oauth2/a;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1, p3}, Lcom/google/api/client/http/HttpResponseException;-><init>(Lcom/google/api/client/http/r;Ljava/lang/String;)V

    .line 58
    iput-object p2, p0, Lcom/google/api/client/auth/oauth2/TokenResponseException;->details:Lcom/google/api/client/auth/oauth2/a;

    .line 59
    return-void
.end method

.method public static from(Lcom/google/api/client/json/d;Lcom/google/api/client/http/r;)Lcom/google/api/client/auth/oauth2/TokenResponseException;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-static {p0}, Lcom/google/common/base/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->a()Ljava/lang/String;

    move-result-object v0

    .line 86
    :try_start_8
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->c()Z

    move-result v2

    if-nez v2, :cond_51

    if-eqz v0, :cond_51

    sget-object v2, Lcom/google/api/client/json/c;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/api/client/http/l;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 88
    new-instance v0, Lcom/google/api/client/json/f;

    invoke-direct {v0, p0}, Lcom/google/api/client/json/f;-><init>(Lcom/google/api/client/json/d;)V

    invoke-virtual {p1}, Lcom/google/api/client/http/r;->f()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/api/client/http/r;->j()Ljava/nio/charset/Charset;

    move-result-object v3

    const-class v4, Lcom/google/api/client/auth/oauth2/a;

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/api/client/json/f;->a(Ljava/io/InputStream;Ljava/nio/charset/Charset;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/client/auth/oauth2/a;
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_2d} :catch_56

    .line 90
    :try_start_2d
    invoke-virtual {v0}, Lcom/google/api/client/auth/oauth2/a;->toPrettyString()Ljava/lang/String;
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_30} :catch_60

    move-result-object v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 99
    :goto_34
    invoke-static {p1}, Lcom/google/api/client/http/HttpResponseException;->computeMessageBuffer(Lcom/google/api/client/http/r;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 100
    invoke-static {v0}, Lcom/google/common/base/al;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_47

    .line 101
    sget-object v3, Lcom/google/api/client/util/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    :cond_47
    new-instance v0, Lcom/google/api/client/auth/oauth2/TokenResponseException;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/api/client/auth/oauth2/TokenResponseException;-><init>(Lcom/google/api/client/http/r;Lcom/google/api/client/auth/oauth2/a;Ljava/lang/String;)V

    return-object v0

    .line 92
    :cond_51
    :try_start_51
    invoke-virtual {p1}, Lcom/google/api/client/http/r;->i()Ljava/lang/String;
    :try_end_54
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_54} :catch_56

    move-result-object v0

    goto :goto_34

    .line 94
    :catch_56
    move-exception v0

    move-object v2, v0

    move-object v0, v1

    .line 96
    :goto_59
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_34

    .line 94
    :catch_60
    move-exception v2

    goto :goto_59
.end method


# virtual methods
.method public final getDetails()Lcom/google/api/client/auth/oauth2/a;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/api/client/auth/oauth2/TokenResponseException;->details:Lcom/google/api/client/auth/oauth2/a;

    return-object v0
.end method
