.class public abstract Lcom/google/api/client/http/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/api/client/http/h;


# instance fields
.field private a:Lcom/google/api/client/http/l;

.field private b:J


# direct methods
.method protected constructor <init>(Lcom/google/api/client/http/l;)V
    .registers 4
    .parameter

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/api/client/http/a;->b:J

    .line 61
    iput-object p1, p0, Lcom/google/api/client/http/a;->a:Lcom/google/api/client/http/l;

    .line 62
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 53
    if-nez p1, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/api/client/http/a;-><init>(Lcom/google/api/client/http/l;)V

    .line 54
    return-void

    .line 53
    :cond_7
    new-instance v0, Lcom/google/api/client/http/l;

    invoke-direct {v0, p1}, Lcom/google/api/client/http/l;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private f()J
    .registers 3

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/api/client/http/a;->e()Z

    move-result v0

    if-nez v0, :cond_9

    .line 130
    const-wide/16 v0, -0x1

    .line 138
    :goto_8
    return-wide v0

    .line 132
    :cond_9
    new-instance v0, Lcom/google/api/client/http/e;

    invoke-direct {v0}, Lcom/google/api/client/http/e;-><init>()V

    .line 134
    :try_start_e
    invoke-virtual {p0, v0}, Lcom/google/api/client/http/a;->a(Ljava/io/OutputStream;)V
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_17

    .line 136
    invoke-virtual {v0}, Lcom/google/api/client/http/e;->close()V

    .line 138
    iget-wide v0, v0, Lcom/google/api/client/http/e;->a:J

    goto :goto_8

    .line 136
    :catchall_17
    move-exception v1

    invoke-virtual {v0}, Lcom/google/api/client/http/e;->close()V

    throw v1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()J
    .registers 5

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/google/api/client/http/a;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    .line 75
    invoke-direct {p0}, Lcom/google/api/client/http/a;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/api/client/http/a;->b:J

    .line 77
    :cond_e
    iget-wide v0, p0, Lcom/google/api/client/http/a;->b:J

    return-wide v0
.end method

.method protected final c()Ljava/nio/charset/Charset;
    .registers 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/api/client/http/a;->a:Lcom/google/api/client/http/l;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/api/client/http/a;->a:Lcom/google/api/client/http/l;

    invoke-virtual {v0}, Lcom/google/api/client/http/l;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    if-nez v0, :cond_f

    :cond_c
    sget-object v0, Lcom/google/common/base/s;->c:Ljava/nio/charset/Charset;

    :goto_e
    return-object v0

    :cond_f
    iget-object v0, p0, Lcom/google/api/client/http/a;->a:Lcom/google/api/client/http/l;

    invoke-virtual {v0}, Lcom/google/api/client/http/l;->b()Ljava/nio/charset/Charset;

    move-result-object v0

    goto :goto_e
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/api/client/http/a;->a:Lcom/google/api/client/http/l;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/api/client/http/a;->a:Lcom/google/api/client/http/l;

    invoke-virtual {v0}, Lcom/google/api/client/http/l;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 143
    const/4 v0, 0x1

    return v0
.end method
