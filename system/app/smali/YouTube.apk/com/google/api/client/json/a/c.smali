.class final Lcom/google/api/client/json/a/c;
.super Lcom/google/api/client/json/e;
.source "SourceFile"


# instance fields
.field private final a:Lorg/codehaus/jackson/JsonGenerator;

.field private final b:Lcom/google/api/client/json/a/a;


# direct methods
.method constructor <init>(Lcom/google/api/client/json/a/a;Lorg/codehaus/jackson/JsonGenerator;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/api/client/json/e;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/api/client/json/a/c;->b:Lcom/google/api/client/json/a/a;

    .line 45
    iput-object p2, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    .line 46
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->g()V

    .line 51
    return-void
.end method

.method public final a(D)V
    .registers 4
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/JsonGenerator;->a(D)V

    .line 111
    return-void
.end method

.method public final a(F)V
    .registers 3
    .parameter

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->a(F)V

    .line 116
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->a(I)V

    .line 86
    return-void
.end method

.method public final a(J)V
    .registers 4
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/jackson/JsonGenerator;->a(J)V

    .line 91
    return-void
.end method

.method public final a(Lcom/google/common/primitives/UnsignedInteger;)V
    .registers 5
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {p1}, Lcom/google/common/primitives/UnsignedInteger;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/codehaus/jackson/JsonGenerator;->a(J)V

    .line 101
    return-void
.end method

.method public final a(Lcom/google/common/primitives/UnsignedLong;)V
    .registers 4
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {p1}, Lcom/google/common/primitives/UnsignedLong;->bigIntegerValue()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/jackson/JsonGenerator;->a(Ljava/math/BigInteger;)V

    .line 106
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->a(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public final a(Ljava/math/BigDecimal;)V
    .registers 3
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->a(Ljava/math/BigDecimal;)V

    .line 121
    return-void
.end method

.method public final a(Ljava/math/BigInteger;)V
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->a(Ljava/math/BigInteger;)V

    .line 96
    return-void
.end method

.method public final a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->a(Z)V

    .line 61
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->b()V

    .line 131
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0, p1}, Lorg/codehaus/jackson/JsonGenerator;->b(Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->c()V

    .line 66
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->d()V

    .line 136
    return-void
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->e()V

    .line 71
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->f()V

    .line 81
    return-void
.end method

.method public final g()V
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/api/client/json/a/c;->a:Lorg/codehaus/jackson/JsonGenerator;

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonGenerator;->a()Lorg/codehaus/jackson/JsonGenerator;

    .line 146
    return-void
.end method
