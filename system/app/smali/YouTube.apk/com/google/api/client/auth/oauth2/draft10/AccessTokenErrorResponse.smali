.class public final Lcom/google/api/client/auth/oauth2/draft10/AccessTokenErrorResponse;
.super Lcom/google/api/client/util/GenericData;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public error:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
    .end annotation
.end field

.field public errorDescription:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "error_description"
    .end annotation
.end field

.field public errorUri:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/q;
        a = "error_uri"
    .end annotation
.end field
