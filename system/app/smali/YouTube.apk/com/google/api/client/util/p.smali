.class final Lcom/google/api/client/util/p;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/api/client/util/GenericData;

.field private final b:Lcom/google/api/client/util/m;


# direct methods
.method constructor <init>(Lcom/google/api/client/util/GenericData;)V
    .registers 4
    .parameter

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/api/client/util/p;->a:Lcom/google/api/client/util/GenericData;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 207
    new-instance v0, Lcom/google/api/client/util/j;

    iget-object v1, p1, Lcom/google/api/client/util/GenericData;->classInfo:Lcom/google/api/client/util/g;

    invoke-virtual {v1}, Lcom/google/api/client/util/g;->a()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/google/api/client/util/j;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {v0}, Lcom/google/api/client/util/j;->a()Lcom/google/api/client/util/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/api/client/util/p;->b:Lcom/google/api/client/util/m;

    .line 208
    return-void
.end method


# virtual methods
.method public final clear()V
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/api/client/util/p;->a:Lcom/google/api/client/util/GenericData;

    iget-object v0, v0, Lcom/google/api/client/util/GenericData;->unknownFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 223
    iget-object v0, p0, Lcom/google/api/client/util/p;->b:Lcom/google/api/client/util/m;

    invoke-virtual {v0}, Lcom/google/api/client/util/m;->clear()V

    .line 224
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 212
    new-instance v0, Lcom/google/api/client/util/o;

    iget-object v1, p0, Lcom/google/api/client/util/p;->a:Lcom/google/api/client/util/GenericData;

    iget-object v2, p0, Lcom/google/api/client/util/p;->b:Lcom/google/api/client/util/m;

    invoke-direct {v0, v1, v2}, Lcom/google/api/client/util/o;-><init>(Lcom/google/api/client/util/GenericData;Lcom/google/api/client/util/m;)V

    return-object v0
.end method

.method public final size()I
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/api/client/util/p;->a:Lcom/google/api/client/util/GenericData;

    iget-object v0, v0, Lcom/google/api/client/util/GenericData;->unknownFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/api/client/util/p;->b:Lcom/google/api/client/util/m;

    invoke-virtual {v1}, Lcom/google/api/client/util/m;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
