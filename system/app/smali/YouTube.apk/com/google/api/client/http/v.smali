.class final Lcom/google/api/client/http/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/api/client/http/h;


# instance fields
.field private final a:Lcom/google/api/client/http/h;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:I


# direct methods
.method constructor <init>(Lcom/google/api/client/http/h;Ljava/lang/String;Ljava/lang/String;JI)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/api/client/http/v;->a:Lcom/google/api/client/http/h;

    .line 51
    iput-object p2, p0, Lcom/google/api/client/http/v;->b:Ljava/lang/String;

    .line 52
    iput-wide p4, p0, Lcom/google/api/client/http/v;->d:J

    .line 53
    iput-object p3, p0, Lcom/google/api/client/http/v;->c:Ljava/lang/String;

    .line 54
    iput p6, p0, Lcom/google/api/client/http/v;->e:I

    .line 55
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/api/client/http/v;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .registers 6
    .parameter

    .prologue
    .line 58
    new-instance v1, Lcom/google/api/client/util/t;

    sget-object v0, Lcom/google/api/client/http/t;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    iget v3, p0, Lcom/google/api/client/http/v;->e:I

    invoke-direct {v1, p1, v0, v2, v3}, Lcom/google/api/client/util/t;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Logger;Ljava/util/logging/Level;I)V

    .line 61
    :try_start_b
    iget-object v0, p0, Lcom/google/api/client/http/v;->a:Lcom/google/api/client/http/h;

    invoke-interface {v0, v1}, Lcom/google/api/client/http/h;->a(Ljava/io/OutputStream;)V
    :try_end_10
    .catchall {:try_start_b .. :try_end_10} :catchall_1b

    .line 64
    invoke-virtual {v1}, Lcom/google/api/client/util/t;->a()Lcom/google/api/client/util/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/util/r;->close()V

    .line 66
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 67
    return-void

    .line 64
    :catchall_1b
    move-exception v0

    invoke-virtual {v1}, Lcom/google/api/client/util/t;->a()Lcom/google/api/client/util/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/util/r;->close()V

    throw v0
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/google/api/client/http/v;->d:J

    return-wide v0
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/api/client/http/v;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/api/client/http/v;->a:Lcom/google/api/client/http/h;

    invoke-interface {v0}, Lcom/google/api/client/http/h;->e()Z

    move-result v0

    return v0
.end method
