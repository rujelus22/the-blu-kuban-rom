.class final Lcom/google/protobuf/ac;
.super Lcom/google/protobuf/e;
.source "SourceFile"


# instance fields
.field protected final c:[B

.field private d:I


# direct methods
.method constructor <init>([B)V
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/protobuf/e;-><init>()V

    .line 205
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/ac;->d:I

    .line 34
    iput-object p1, p0, Lcom/google/protobuf/ac;->c:[B

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(III)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 133
    add-int/lit8 v0, p2, 0x0

    .line 134
    iget-object v1, p0, Lcom/google/protobuf/ac;->c:[B

    add-int v2, v0, p3

    invoke-static {p1, v1, v0, v2}, Lcom/google/protobuf/ax;->a(I[BII)I

    move-result v0

    return v0
.end method

.method public final a()Lcom/google/protobuf/f;
    .registers 3

    .prologue
    .line 265
    new-instance v0, Lcom/google/protobuf/ad;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/protobuf/ad;-><init>(Lcom/google/protobuf/ac;B)V

    return-object v0
.end method

.method final a(Lcom/google/protobuf/ac;II)Z
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 177
    iget-object v1, p1, Lcom/google/protobuf/ac;->c:[B

    array-length v1, v1

    if-le p3, v1, :cond_22

    .line 178
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Length too large: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_22
    add-int v1, p2, p3

    iget-object v2, p1, Lcom/google/protobuf/ac;->c:[B

    array-length v2, v2

    if-le v1, v2, :cond_55

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ran off end of other: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/protobuf/ac;->c:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_55
    iget-object v3, p0, Lcom/google/protobuf/ac;->c:[B

    .line 188
    iget-object v4, p1, Lcom/google/protobuf/ac;->c:[B

    .line 189
    add-int/lit8 v5, p3, 0x0

    .line 190
    add-int/lit8 v1, p2, 0x0

    move v2, v1

    move v1, v0

    .line 192
    :goto_5f
    if-ge v1, v5, :cond_6d

    .line 193
    aget-byte v6, v3, v1

    aget-byte v7, v4, v2

    if-eq v6, v7, :cond_68

    .line 197
    :goto_67
    return v0

    .line 192
    :cond_68
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_5f

    .line 197
    :cond_6d
    const/4 v0, 0x1

    goto :goto_67
.end method

.method public final b(I)B
    .registers 3
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v0, v0

    return v0
.end method

.method protected final b(III)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    iget-object v1, p0, Lcom/google/protobuf/ac;->c:[B

    .line 236
    add-int/lit8 v0, p2, 0x0

    add-int v2, v0, p3

    :goto_6
    if-ge v0, v2, :cond_11

    .line 238
    mul-int/lit8 v3, p1, 0x1f

    aget-byte v4, v1, v0

    add-int p1, v3, v4

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 240
    :cond_11
    return p1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 119
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/protobuf/ac;->c:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v3, v3

    invoke-direct {v0, v1, v2, v3, p1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    return-object v0
.end method

.method protected final b([BIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    invoke-static {v0, p2, p1, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    if-ne p1, p0, :cond_6

    move v0, v1

    .line 159
    :goto_5
    return v0

    .line 145
    :cond_6
    instance-of v0, p1, Lcom/google/protobuf/e;

    if-nez v0, :cond_c

    move v0, v2

    .line 146
    goto :goto_5

    .line 149
    :cond_c
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v3, v0

    move-object v0, p1

    check-cast v0, Lcom/google/protobuf/e;

    invoke-virtual {v0}, Lcom/google/protobuf/e;->b()I

    move-result v0

    if-eq v3, v0, :cond_1a

    move v0, v2

    .line 150
    goto :goto_5

    .line 152
    :cond_1a
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v0, v0

    if-nez v0, :cond_21

    move v0, v1

    .line 153
    goto :goto_5

    .line 156
    :cond_21
    instance-of v0, p1, Lcom/google/protobuf/ac;

    if-eqz v0, :cond_2f

    .line 157
    check-cast p1, Lcom/google/protobuf/ac;

    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v0, v0

    invoke-virtual {p0, p1, v2, v0}, Lcom/google/protobuf/ac;->a(Lcom/google/protobuf/ac;II)Z

    move-result v0

    goto :goto_5

    .line 158
    :cond_2f
    instance-of v0, p1, Lcom/google/protobuf/ai;

    if-eqz v0, :cond_38

    .line 159
    invoke-virtual {p1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5

    .line 161
    :cond_38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Has a new type of ByteString been created? Found "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final f()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lcom/google/protobuf/ac;->c:[B

    iget-object v2, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v2, v2

    add-int/lit8 v2, v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/protobuf/ax;->a([BII)I

    move-result v1

    if-nez v1, :cond_f

    const/4 v0, 0x1

    :cond_f
    return v0
.end method

.method public final g()Ljava/io/InputStream;
    .registers 5

    .prologue
    .line 248
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lcom/google/protobuf/ac;->c:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v3, v3

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    return-object v0
.end method

.method public final h()Lcom/google/protobuf/h;
    .registers 4

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/protobuf/h;->a([BII)Lcom/google/protobuf/h;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 215
    iget v0, p0, Lcom/google/protobuf/ac;->d:I

    .line 217
    if-nez v0, :cond_11

    .line 218
    iget-object v0, p0, Lcom/google/protobuf/ac;->c:[B

    array-length v0, v0

    .line 219
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v0}, Lcom/google/protobuf/ac;->b(III)I

    move-result v0

    .line 220
    if-nez v0, :cond_f

    .line 221
    const/4 v0, 0x1

    .line 223
    :cond_f
    iput v0, p0, Lcom/google/protobuf/ac;->d:I

    .line 225
    :cond_11
    return v0
.end method

.method protected final i()I
    .registers 2

    .prologue
    .line 230
    iget v0, p0, Lcom/google/protobuf/ac;->d:I

    return v0
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/protobuf/ac;->a()Lcom/google/protobuf/f;

    move-result-object v0

    return-object v0
.end method
