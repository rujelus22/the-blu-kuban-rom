.class public final Lcom/google/protobuf/aa;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/ab;
.implements Ljava/util/RandomAccess;


# static fields
.field public static final a:Lcom/google/protobuf/ab;


# instance fields
.field private final b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/protobuf/au;

    new-instance v1, Lcom/google/protobuf/aa;

    invoke-direct {v1}, Lcom/google/protobuf/aa;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/protobuf/au;-><init>(Lcom/google/protobuf/ab;)V

    sput-object v0, Lcom/google/protobuf/aa;->a:Lcom/google/protobuf/ab;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/google/protobuf/ab;)V
    .registers 4
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Lcom/google/protobuf/ab;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    .line 50
    invoke-virtual {p0, p1}, Lcom/google/protobuf/aa;->addAll(Ljava/util/Collection;)Z

    .line 51
    return-void
.end method

.method private static a(Ljava/lang/Object;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 136
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 137
    check-cast p0, Ljava/lang/String;

    .line 139
    :goto_6
    return-object p0

    :cond_7
    check-cast p0, Lcom/google/protobuf/e;

    invoke-virtual {p0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object p0

    goto :goto_6
.end method


# virtual methods
.method public final a(I)Lcom/google/protobuf/e;
    .registers 4
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 126
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 127
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/e;->a(Ljava/lang/String;)Lcom/google/protobuf/e;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 131
    :goto_15
    return-object v0

    :cond_16
    check-cast v0, Lcom/google/protobuf/e;

    goto :goto_15
.end method

.method public final a()Ljava/util/List;
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/e;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    iget v0, p0, Lcom/google/protobuf/aa;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protobuf/aa;->modCount:I

    .line 121
    return-void
.end method

.method public final bridge synthetic add(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 36
    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget v0, p0, Lcom/google/protobuf/aa;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protobuf/aa;->modCount:I

    return-void
.end method

.method public final addAll(ILjava/util/Collection;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 98
    instance-of v0, p2, Lcom/google/protobuf/ab;

    if-eqz v0, :cond_a

    check-cast p2, Lcom/google/protobuf/ab;

    invoke-interface {p2}, Lcom/google/protobuf/ab;->a()Ljava/util/List;

    move-result-object p2

    .line 100
    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result v0

    .line 101
    iget v1, p0, Lcom/google/protobuf/aa;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protobuf/aa;->modCount:I

    .line 102
    return v0
.end method

.method public final addAll(Ljava/util/Collection;)Z
    .registers 3
    .parameter

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/protobuf/aa;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/protobuf/aa;->addAll(ILjava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public final clear()V
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 114
    iget v0, p0, Lcom/google/protobuf/aa;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/protobuf/aa;->modCount:I

    .line 115
    return-void
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_d

    check-cast v0, Ljava/lang/String;

    :goto_c
    return-object v0

    :cond_d
    check-cast v0, Lcom/google/protobuf/e;

    invoke-virtual {v0}, Lcom/google/protobuf/e;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/protobuf/e;->f()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_1e
    move-object v0, v1

    goto :goto_c
.end method

.method public final synthetic remove(I)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/google/protobuf/aa;->modCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/protobuf/aa;->modCount:I

    invoke-static {v0}, Lcom/google/protobuf/aa;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 36
    check-cast p2, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/aa;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final size()I
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/protobuf/aa;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
