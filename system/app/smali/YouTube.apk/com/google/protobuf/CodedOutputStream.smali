.class public final Lcom/google/protobuf/CodedOutputStream;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I

.field private final d:Ljava/io/OutputStream;


# direct methods
.method private constructor <init>(Ljava/io/OutputStream;[B)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    .line 58
    iput-object p2, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    .line 60
    array-length v0, p2

    iput v0, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    .line 61
    return-void
.end method

.method private constructor <init>([BII)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    .line 51
    iput-object p1, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    .line 52
    iput p2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    .line 53
    add-int v0, p2, p3

    iput v0, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    .line 54
    return-void
.end method

.method static a(I)I
    .registers 2
    .parameter

    .prologue
    const/16 v0, 0x1000

    .line 44
    if-le p0, v0, :cond_5

    move p0, v0

    .line 45
    :cond_5
    return p0
.end method

.method public static a(J)I
    .registers 6
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 1018
    const-wide/16 v0, -0x80

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    const/4 v0, 0x1

    .line 1027
    :goto_a
    return v0

    .line 1019
    :cond_b
    const-wide/16 v0, -0x4000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_14

    const/4 v0, 0x2

    goto :goto_a

    .line 1020
    :cond_14
    const-wide/32 v0, -0x200000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1e

    const/4 v0, 0x3

    goto :goto_a

    .line 1021
    :cond_1e
    const-wide/32 v0, -0x10000000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_28

    const/4 v0, 0x4

    goto :goto_a

    .line 1022
    :cond_28
    const-wide v0, -0x800000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_34

    const/4 v0, 0x5

    goto :goto_a

    .line 1023
    :cond_34
    const-wide v0, -0x40000000000L

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_40

    const/4 v0, 0x6

    goto :goto_a

    .line 1024
    :cond_40
    const-wide/high16 v0, -0x2

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_49

    const/4 v0, 0x7

    goto :goto_a

    .line 1025
    :cond_49
    const-wide/high16 v0, -0x100

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_53

    const/16 v0, 0x8

    goto :goto_a

    .line 1026
    :cond_53
    const-wide/high16 v0, -0x8000

    and-long/2addr v0, p0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5d

    const/16 v0, 0x9

    goto :goto_a

    .line 1027
    :cond_5d
    const/16 v0, 0xa

    goto :goto_a
.end method

.method public static a(Lcom/google/protobuf/ae;)I
    .registers 3
    .parameter

    .prologue
    .line 721
    invoke-interface {p0}, Lcom/google/protobuf/ae;->getSerializedSize()I

    move-result v0

    .line 722
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(Lcom/google/protobuf/e;)I
    .registers 3
    .parameter

    .prologue
    .line 739
    invoke-virtual {p0}, Lcom/google/protobuf/e;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/protobuf/e;->b()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(Lcom/google/protobuf/x;)I
    .registers 3
    .parameter

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/google/protobuf/x;->b()I

    move-result v0

    .line 731
    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static a(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 687
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 688
    array-length v1, v0

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v1

    array-length v0, v0
    :try_end_c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_c} :catch_e

    add-int/2addr v0, v1

    return v0

    .line 690
    :catch_e
    move-exception v0

    .line 691
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "UTF-8 not supported."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/io/OutputStream;I)Lcom/google/protobuf/CodedOutputStream;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 77
    new-instance v0, Lcom/google/protobuf/CodedOutputStream;

    new-array v1, p1, [B

    invoke-direct {v0, p0, v1}, Lcom/google/protobuf/CodedOutputStream;-><init>(Ljava/io/OutputStream;[B)V

    return-object v0
.end method

.method public static a([B)Lcom/google/protobuf/CodedOutputStream;
    .registers 4
    .parameter

    .prologue
    .line 88
    array-length v0, p0

    new-instance v1, Lcom/google/protobuf/CodedOutputStream;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/protobuf/CodedOutputStream;-><init>([BII)V

    return-object v1
.end method

.method public static b(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 434
    invoke-static {p0}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->c(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILcom/google/protobuf/ae;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 502
    invoke-static {p0}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/ae;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILcom/google/protobuf/e;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 511
    invoke-static {p0}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Lcom/google/protobuf/e;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILjava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 470
    invoke-static {p0}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(IZ)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 461
    invoke-static {p0}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static c(I)I
    .registers 2
    .parameter

    .prologue
    .line 649
    if-ltz p0, :cond_7

    .line 650
    invoke-static {p0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v0

    .line 653
    :goto_6
    return v0

    :cond_7
    const/16 v0, 0xa

    goto :goto_6
.end method

.method public static c(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 528
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->d(I)I

    move-result v0

    invoke-static {p1}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private c()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 798
    iget-object v0, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_b

    .line 800
    new-instance v0, Lcom/google/protobuf/CodedOutputStream$OutOfSpaceException;

    invoke-direct {v0}, Lcom/google/protobuf/CodedOutputStream$OutOfSpaceException;-><init>()V

    throw v0

    .line 805
    :cond_b
    iget-object v0, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 806
    iput v3, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    .line 807
    return-void
.end method

.method public static d(I)I
    .registers 2
    .parameter

    .prologue
    .line 971
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/protobuf/WireFormat;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/CodedOutputStream;->f(I)I

    move-result v0

    return v0
.end method

.method public static f(I)I
    .registers 2
    .parameter

    .prologue
    .line 996
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_6

    const/4 v0, 0x1

    .line 1000
    :goto_5
    return v0

    .line 997
    :cond_6
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_c

    const/4 v0, 0x2

    goto :goto_5

    .line 998
    :cond_c
    const/high16 v0, -0x20

    and-int/2addr v0, p0

    if-nez v0, :cond_13

    const/4 v0, 0x3

    goto :goto_5

    .line 999
    :cond_13
    const/high16 v0, -0x1000

    and-int/2addr v0, p0

    if-nez v0, :cond_1a

    const/4 v0, 0x4

    goto :goto_5

    .line 1000
    :cond_1a
    const/4 v0, 0x5

    goto :goto_5
.end method

.method private g(I)V
    .registers 6
    .parameter

    .prologue
    .line 872
    int-to-byte v0, p1

    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    if-ne v1, v2, :cond_a

    invoke-direct {p0}, Lcom/google/protobuf/CodedOutputStream;->c()V

    :cond_a
    iget-object v1, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    aput-byte v0, v1, v2

    .line 873
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    if-eqz v0, :cond_7

    .line 815
    invoke-direct {p0}, Lcom/google/protobuf/CodedOutputStream;->c()V

    .line 817
    :cond_7
    return-void
.end method

.method public final a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)V

    .line 138
    invoke-virtual {p0, p2}, Lcom/google/protobuf/CodedOutputStream;->b(I)V

    .line 139
    return-void
.end method

.method public final a(ILcom/google/protobuf/ae;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 193
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)V

    .line 194
    invoke-interface {p2}, Lcom/google/protobuf/ae;->getSerializedSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    invoke-interface {p2, p0}, Lcom/google/protobuf/ae;->writeTo(Lcom/google/protobuf/CodedOutputStream;)V

    .line 195
    return-void
.end method

.method public final a(ILcom/google/protobuf/e;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 200
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)V

    .line 201
    invoke-virtual {p2}, Lcom/google/protobuf/e;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    invoke-virtual {p2}, Lcom/google/protobuf/e;->b()I

    move-result v0

    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_24

    iget-object v1, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-virtual {p2, v1, v7, v2, v0}, Lcom/google/protobuf/e;->a([BIII)V

    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    .line 202
    :cond_23
    :goto_23
    return-void

    .line 201
    :cond_24
    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    iget v3, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-virtual {p2, v2, v7, v3, v1}, Lcom/google/protobuf/e;->a([BIII)V

    add-int/lit8 v2, v1, 0x0

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iput v1, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-direct {p0}, Lcom/google/protobuf/CodedOutputStream;->c()V

    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    if-gt v0, v1, :cond_46

    iget-object v1, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    invoke-virtual {p2, v1, v2, v7, v0}, Lcom/google/protobuf/e;->a([BIII)V

    iput v0, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    goto :goto_23

    :cond_46
    invoke-virtual {p2}, Lcom/google/protobuf/e;->g()Ljava/io/InputStream;

    move-result-object v1

    int-to-long v3, v2

    int-to-long v5, v2

    invoke-virtual {v1, v5, v6}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v5

    cmp-long v2, v3, v5

    if-eqz v2, :cond_64

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Skip failed? Should never happen."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5c
    iget-object v2, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    iget-object v4, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    invoke-virtual {v2, v4, v7, v3}, Ljava/io/OutputStream;->write([BII)V

    sub-int/2addr v0, v3

    :cond_64
    if-lez v0, :cond_23

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    invoke-virtual {v1, v3, v7, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    if-eq v3, v2, :cond_5c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Read failed? Should never happen"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(ILjava/lang/String;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 165
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)V

    .line 166
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    array-length v1, v0

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iget v3, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    sub-int/2addr v2, v3

    if-lt v2, v1, :cond_24

    iget-object v2, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    iget v3, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-static {v0, v5, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    .line 167
    :goto_23
    return-void

    .line 166
    :cond_24
    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iget v3, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    iget v4, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-static {v0, v5, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v3, v2, 0x0

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iput v2, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    invoke-direct {p0}, Lcom/google/protobuf/CodedOutputStream;->c()V

    iget v2, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    if-gt v1, v2, :cond_46

    iget-object v2, p0, Lcom/google/protobuf/CodedOutputStream;->a:[B

    invoke-static {v0, v3, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v1, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    goto :goto_23

    :cond_46
    iget-object v2, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    invoke-virtual {v2, v0, v3, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_23
.end method

.method public final a(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 158
    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/CodedOutputStream;->d(II)V

    .line 159
    if-eqz p2, :cond_7

    const/4 v0, 0x1

    :cond_7
    invoke-direct {p0, v0}, Lcom/google/protobuf/CodedOutputStream;->g(I)V

    .line 160
    return-void
.end method

.method public final b()V
    .registers 3

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/protobuf/CodedOutputStream;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_13

    iget v0, p0, Lcom/google/protobuf/CodedOutputStream;->b:I

    iget v1, p0, Lcom/google/protobuf/CodedOutputStream;->c:I

    sub-int/2addr v0, v1

    if-eqz v0, :cond_1b

    .line 842
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 841
    :cond_13
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 845
    :cond_1b
    return-void
.end method

.method public final b(I)V
    .registers 8
    .parameter

    .prologue
    .line 299
    if-ltz p1, :cond_6

    .line 300
    invoke-virtual {p0, p1}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    .line 303
    :goto_5
    return-void

    :cond_6
    int-to-long v0, p1

    :goto_7
    const-wide/16 v2, -0x80

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_15

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/CodedOutputStream;->g(I)V

    goto :goto_5

    :cond_15
    long-to-int v2, v0

    and-int/lit8 v2, v2, 0x7f

    or-int/lit16 v2, v2, 0x80

    invoke-direct {p0, v2}, Lcom/google/protobuf/CodedOutputStream;->g(I)V

    const/4 v2, 0x7

    ushr-long/2addr v0, v2

    goto :goto_7
.end method

.method public final d(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 966
    invoke-static {p1, p2}, Lcom/google/protobuf/WireFormat;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/CodedOutputStream;->e(I)V

    .line 967
    return-void
.end method

.method public final e(I)V
    .registers 3
    .parameter

    .prologue
    .line 980
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_8

    .line 981
    invoke-direct {p0, p1}, Lcom/google/protobuf/CodedOutputStream;->g(I)V

    .line 982
    return-void

    .line 984
    :cond_8
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Lcom/google/protobuf/CodedOutputStream;->g(I)V

    .line 985
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method
