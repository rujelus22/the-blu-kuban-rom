.class public abstract Lcom/google/protobuf/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/af;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    return-void
.end method

.method protected static a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/UninitializedMessageException;
    .registers 2
    .parameter

    .prologue
    .line 278
    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0, p0}, Lcom/google/protobuf/UninitializedMessageException;-><init>(Lcom/google/protobuf/ae;)V

    return-object v0
.end method

.method private a([BII)Lcom/google/protobuf/b;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p1, v0, p3}, Lcom/google/protobuf/h;->a([BII)Lcom/google/protobuf/h;

    move-result-object v0

    .line 142
    invoke-virtual {p0, v0}, Lcom/google/protobuf/b;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    .line 143
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/h;->a(I)V
    :try_end_c
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_c} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_f

    .line 144
    return-object p0

    .line 145
    :catch_d
    move-exception v0

    .line 146
    throw v0

    .line 147
    :catch_f
    move-exception v0

    .line 148
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a([BIILcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 166
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p1, v0, p3}, Lcom/google/protobuf/h;->a([BII)Lcom/google/protobuf/h;

    move-result-object v0

    .line 168
    invoke-virtual {p0, v0, p4}, Lcom/google/protobuf/b;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    .line 169
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/h;->a(I)V
    :try_end_c
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_c} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_f

    .line 170
    return-object p0

    .line 171
    :catch_d
    move-exception v0

    .line 172
    throw v0

    .line 173
    :catch_f
    move-exception v0

    .line 174
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/Iterable;)V
    .registers 3
    .parameter

    .prologue
    .line 308
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 309
    if-nez v1, :cond_4

    .line 310
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 313
    :cond_16
    return-void
.end method

.method protected static a(Ljava/lang/Iterable;Ljava/util/Collection;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 290
    instance-of v0, p0, Lcom/google/protobuf/ab;

    if-eqz v0, :cond_18

    move-object v0, p0

    .line 293
    check-cast v0, Lcom/google/protobuf/ab;

    invoke-interface {v0}, Lcom/google/protobuf/ab;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/b;->a(Ljava/lang/Iterable;)V

    .line 297
    :goto_e
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_1c

    .line 298
    check-cast p0, Ljava/util/Collection;

    .line 299
    invoke-interface {p1, p0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 305
    :cond_17
    return-void

    .line 295
    :cond_18
    invoke-static {p0}, Lcom/google/protobuf/b;->a(Ljava/lang/Iterable;)V

    goto :goto_e

    .line 301
    :cond_1c
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_20
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 302
    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_20
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/e;)Lcom/google/protobuf/b;
    .registers 5
    .parameter

    .prologue
    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/e;->h()Lcom/google/protobuf/h;

    move-result-object v0

    .line 101
    invoke-virtual {p0, v0}, Lcom/google/protobuf/b;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    .line 102
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/h;->a(I)V
    :try_end_b
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_b} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b} :catch_e

    .line 103
    return-object p0

    .line 104
    :catch_c
    move-exception v0

    .line 105
    throw v0

    .line 106
    :catch_e
    move-exception v0

    .line 107
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 118
    :try_start_0
    invoke-virtual {p1}, Lcom/google/protobuf/e;->h()Lcom/google/protobuf/h;

    move-result-object v0

    .line 119
    invoke-virtual {p0, v0, p2}, Lcom/google/protobuf/b;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    .line 120
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/h;->a(I)V
    :try_end_b
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_0 .. :try_end_b} :catch_c
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_b} :catch_e

    .line 121
    return-object p0

    .line 122
    :catch_c
    move-exception v0

    .line 123
    throw v0

    .line 124
    :catch_e
    move-exception v0

    .line 125
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Reading from a ByteString threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;
    .registers 3
    .parameter

    .prologue
    .line 88
    invoke-static {}, Lcom/google/protobuf/i;->a()Lcom/google/protobuf/i;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/b;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/io/InputStream;)Lcom/google/protobuf/b;
    .registers 4
    .parameter

    .prologue
    .line 181
    invoke-static {p1}, Lcom/google/protobuf/h;->a(Ljava/io/InputStream;)Lcom/google/protobuf/h;

    move-result-object v0

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/protobuf/b;->a(Lcom/google/protobuf/h;)Lcom/google/protobuf/b;

    .line 183
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/h;->a(I)V

    .line 184
    return-object p0
.end method

.method public final a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 191
    invoke-static {p1}, Lcom/google/protobuf/h;->a(Ljava/io/InputStream;)Lcom/google/protobuf/h;

    move-result-object v0

    .line 192
    invoke-virtual {p0, v0, p2}, Lcom/google/protobuf/b;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    .line 193
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/h;->a(I)V

    .line 194
    return-object p0
.end method

.method public final a([B)Lcom/google/protobuf/b;
    .registers 4
    .parameter

    .prologue
    .line 133
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/protobuf/b;->a([BII)Lcom/google/protobuf/b;

    move-result-object v0

    return-object v0
.end method

.method public final a([BLcom/google/protobuf/i;)Lcom/google/protobuf/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 158
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/protobuf/b;->a([BIILcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b([B)Lcom/google/protobuf/af;
    .registers 3
    .parameter

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/google/protobuf/b;->a([B)Lcom/google/protobuf/b;

    move-result-object v0

    return-object v0
.end method

.method public abstract b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;
.end method

.method public final b(Ljava/io/InputStream;)Z
    .registers 3
    .parameter

    .prologue
    .line 268
    invoke-static {}, Lcom/google/protobuf/i;->a()Lcom/google/protobuf/i;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/protobuf/b;->b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/io/InputStream;Lcom/google/protobuf/i;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 256
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 257
    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    .line 258
    const/4 v0, 0x0

    .line 263
    :goto_8
    return v0

    .line 260
    :cond_9
    invoke-static {v0, p1}, Lcom/google/protobuf/h;->a(ILjava/io/InputStream;)I

    move-result v0

    .line 261
    new-instance v1, Lcom/google/protobuf/c;

    invoke-direct {v1, p1, v0}, Lcom/google/protobuf/c;-><init>(Ljava/io/InputStream;I)V

    .line 262
    invoke-virtual {p0, v1, p2}, Lcom/google/protobuf/b;->a(Ljava/io/InputStream;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    .line 263
    const/4 v0, 0x1

    goto :goto_8
.end method

.method public synthetic c(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/af;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/google/protobuf/b;->b(Lcom/google/protobuf/h;Lcom/google/protobuf/i;)Lcom/google/protobuf/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/protobuf/b;->d()Lcom/google/protobuf/b;

    move-result-object v0

    return-object v0
.end method

.method public abstract d()Lcom/google/protobuf/b;
.end method
