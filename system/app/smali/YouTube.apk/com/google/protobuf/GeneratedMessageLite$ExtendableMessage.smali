.class public abstract Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/r;


# instance fields
.field private final extensions:Lcom/google/protobuf/k;


# direct methods
.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 128
    invoke-static {}, Lcom/google/protobuf/k;->a()Lcom/google/protobuf/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    .line 129
    return-void
.end method

.method protected constructor <init>(Lcom/google/protobuf/p;)V
    .registers 3
    .parameter

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 132
    invoke-static {p1}, Lcom/google/protobuf/p;->a(Lcom/google/protobuf/p;)Lcom/google/protobuf/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    .line 133
    return-void
.end method

.method static synthetic access$400(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;)Lcom/google/protobuf/k;
    .registers 2
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    return-object v0
.end method

.method private verifyExtensionContainingType(Lcom/google/protobuf/t;)V
    .registers 4
    .parameter

    .prologue
    .line 137
    invoke-virtual {p1}, Lcom/google/protobuf/t;->a()Lcom/google/protobuf/ae;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->getDefaultInstanceForType()Lcom/google/protobuf/ae;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_12
    return-void
.end method


# virtual methods
.method protected extensionsAreInitialized()Z
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-virtual {v0}, Lcom/google/protobuf/k;->d()Z

    move-result v0

    return v0
.end method

.method protected extensionsSerializedSize()I
    .registers 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-virtual {v0}, Lcom/google/protobuf/k;->e()I

    move-result v0

    return v0
.end method

.method protected extensionsSerializedSizeAsMessageSet()I
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-virtual {v0}, Lcom/google/protobuf/k;->f()I

    move-result v0

    return v0
.end method

.method public final getExtension(Lcom/google/protobuf/t;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/t;)V

    .line 168
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-static {p1}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/k;->b(Lcom/google/protobuf/m;)Ljava/lang/Object;

    move-result-object v0

    .line 169
    if-nez v0, :cond_13

    .line 170
    invoke-static {p1}, Lcom/google/protobuf/t;->b(Lcom/google/protobuf/t;)Ljava/lang/Object;

    move-result-object v0

    .line 172
    :cond_13
    return-object v0
.end method

.method public final getExtension(Lcom/google/protobuf/t;I)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/t;)V

    .line 183
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-static {p1}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/m;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getExtensionCount(Lcom/google/protobuf/t;)I
    .registers 4
    .parameter

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/t;)V

    .line 159
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-static {p1}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/k;->c(Lcom/google/protobuf/m;)I

    move-result v0

    return v0
.end method

.method public final hasExtension(Lcom/google/protobuf/t;)Z
    .registers 4
    .parameter

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->verifyExtensionContainingType(Lcom/google/protobuf/t;)V

    .line 151
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-static {p1}, Lcom/google/protobuf/t;->a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/k;->a(Lcom/google/protobuf/m;)Z

    move-result v0

    return v0
.end method

.method protected makeExtensionsImmutable()V
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-virtual {v0}, Lcom/google/protobuf/k;->b()V

    .line 214
    return-void
.end method

.method protected newExtensionWriter()Lcom/google/protobuf/q;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 260
    new-instance v0, Lcom/google/protobuf/q;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/protobuf/q;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;ZB)V

    return-object v0
.end method

.method protected newMessageSetExtensionWriter()Lcom/google/protobuf/q;
    .registers 4

    .prologue
    .line 263
    new-instance v0, Lcom/google/protobuf/q;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/protobuf/q;-><init>(Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;ZB)V

    return-object v0
.end method

.method protected parseUnknownField(Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->extensions:Lcom/google/protobuf/k;

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;->getDefaultInstanceForType()Lcom/google/protobuf/ae;

    move-result-object v1

    #calls: Lcom/google/protobuf/GeneratedMessageLite;->parseUnknownField(Lcom/google/protobuf/k;Lcom/google/protobuf/ae;Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z
    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/protobuf/GeneratedMessageLite;->access$300(Lcom/google/protobuf/k;Lcom/google/protobuf/ae;Lcom/google/protobuf/h;Lcom/google/protobuf/i;I)Z

    move-result v0

    return v0
.end method
