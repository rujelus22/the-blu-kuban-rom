.class final Lcom/google/protobuf/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private final a:Ljava/util/Stack;

.field private b:Lcom/google/protobuf/ac;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/e;)V
    .registers 3
    .parameter

    .prologue
    .line 678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/aj;->a:Ljava/util/Stack;

    .line 679
    invoke-direct {p0, p1}, Lcom/google/protobuf/aj;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/aj;->b:Lcom/google/protobuf/ac;

    .line 680
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/e;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 673
    invoke-direct {p0, p1}, Lcom/google/protobuf/aj;-><init>(Lcom/google/protobuf/e;)V

    return-void
.end method

.method private a(Lcom/google/protobuf/e;)Lcom/google/protobuf/ac;
    .registers 4
    .parameter

    .prologue
    move-object v0, p1

    .line 684
    :goto_1
    instance-of v1, v0, Lcom/google/protobuf/ai;

    if-eqz v1, :cond_11

    .line 685
    check-cast v0, Lcom/google/protobuf/ai;

    .line 686
    iget-object v1, p0, Lcom/google/protobuf/aj;->a:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 687
    invoke-static {v0}, Lcom/google/protobuf/ai;->a(Lcom/google/protobuf/ai;)Lcom/google/protobuf/e;

    move-result-object v0

    goto :goto_1

    .line 689
    :cond_11
    check-cast v0, Lcom/google/protobuf/ac;

    return-object v0
.end method

.method private b()Lcom/google/protobuf/ac;
    .registers 3

    .prologue
    .line 696
    :cond_0
    iget-object v0, p0, Lcom/google/protobuf/aj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 697
    const/4 v0, 0x0

    .line 701
    :goto_9
    return-object v0

    .line 699
    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/aj;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ai;

    invoke-static {v0}, Lcom/google/protobuf/ai;->b(Lcom/google/protobuf/ai;)Lcom/google/protobuf/e;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/protobuf/aj;->a(Lcom/google/protobuf/e;)Lcom/google/protobuf/ac;

    move-result-object v0

    .line 700
    invoke-virtual {v0}, Lcom/google/protobuf/ac;->c()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_9
.end method


# virtual methods
.method public final a()Lcom/google/protobuf/ac;
    .registers 3

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/protobuf/aj;->b:Lcom/google/protobuf/ac;

    if-nez v0, :cond_a

    .line 718
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 720
    :cond_a
    iget-object v0, p0, Lcom/google/protobuf/aj;->b:Lcom/google/protobuf/ac;

    .line 721
    invoke-direct {p0}, Lcom/google/protobuf/aj;->b()Lcom/google/protobuf/ac;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protobuf/aj;->b:Lcom/google/protobuf/ac;

    .line 722
    return-object v0
.end method

.method public final hasNext()Z
    .registers 2

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/protobuf/aj;->b:Lcom/google/protobuf/ac;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ac;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .registers 2

    .prologue
    .line 726
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
