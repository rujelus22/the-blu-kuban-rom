.class final Lcom/google/protobuf/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map$Entry;


# instance fields
.field private a:Ljava/util/Map$Entry;


# direct methods
.method private constructor <init>(Ljava/util/Map$Entry;)V
    .registers 2
    .parameter

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p1, p0, Lcom/google/protobuf/y;->a:Ljava/util/Map$Entry;

    .line 130
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map$Entry;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/google/protobuf/y;-><init>(Ljava/util/Map$Entry;)V

    return-void
.end method


# virtual methods
.method public final getKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/protobuf/y;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/protobuf/y;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/x;

    .line 140
    if-nez v0, :cond_c

    .line 141
    const/4 v0, 0x0

    .line 143
    :goto_b
    return-object v0

    :cond_c
    invoke-virtual {v0}, Lcom/google/protobuf/x;->a()Lcom/google/protobuf/ae;

    move-result-object v0

    goto :goto_b
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 152
    instance-of v0, p1, Lcom/google/protobuf/ae;

    if-nez v0, :cond_c

    .line 153
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_c
    iget-object v0, p0, Lcom/google/protobuf/y;->a:Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/x;

    check-cast p1, Lcom/google/protobuf/ae;

    invoke-virtual {v0, p1}, Lcom/google/protobuf/x;->a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;

    move-result-object v0

    return-object v0
.end method
