.class final Lcom/google/protobuf/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/protobuf/m;


# instance fields
.field private final a:Lcom/google/protobuf/w;

.field private final b:I

.field private final c:Lcom/google/protobuf/WireFormat$FieldType;

.field private final d:Z

.field private final e:Z


# direct methods
.method private constructor <init>(Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 608
    iput-object p1, p0, Lcom/google/protobuf/s;->a:Lcom/google/protobuf/w;

    .line 609
    iput p2, p0, Lcom/google/protobuf/s;->b:I

    .line 610
    iput-object p3, p0, Lcom/google/protobuf/s;->c:Lcom/google/protobuf/WireFormat$FieldType;

    .line 611
    iput-boolean p4, p0, Lcom/google/protobuf/s;->d:Z

    .line 612
    iput-boolean p5, p0, Lcom/google/protobuf/s;->e:Z

    .line 613
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;ZZB)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 599
    invoke-direct/range {p0 .. p5}, Lcom/google/protobuf/s;-><init>(Lcom/google/protobuf/w;ILcom/google/protobuf/WireFormat$FieldType;ZZ)V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/s;)Z
    .registers 2
    .parameter

    .prologue
    .line 599
    iget-boolean v0, p0, Lcom/google/protobuf/s;->d:Z

    return v0
.end method

.method static synthetic b(Lcom/google/protobuf/s;)Lcom/google/protobuf/WireFormat$FieldType;
    .registers 2
    .parameter

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/protobuf/s;->c:Lcom/google/protobuf/WireFormat$FieldType;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 622
    iget v0, p0, Lcom/google/protobuf/s;->b:I

    return v0
.end method

.method public final b()Lcom/google/protobuf/WireFormat$FieldType;
    .registers 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/protobuf/s;->c:Lcom/google/protobuf/WireFormat$FieldType;

    return-object v0
.end method

.method public final c()Lcom/google/protobuf/WireFormat$JavaType;
    .registers 2

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/protobuf/s;->c:Lcom/google/protobuf/WireFormat$FieldType;

    invoke-virtual {v0}, Lcom/google/protobuf/WireFormat$FieldType;->getJavaType()Lcom/google/protobuf/WireFormat$JavaType;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 4
    .parameter

    .prologue
    .line 599
    check-cast p1, Lcom/google/protobuf/s;

    iget v0, p0, Lcom/google/protobuf/s;->b:I

    iget v1, p1, Lcom/google/protobuf/s;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 634
    iget-boolean v0, p0, Lcom/google/protobuf/s;->d:Z

    return v0
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 638
    iget-boolean v0, p0, Lcom/google/protobuf/s;->e:Z

    return v0
.end method

.method public final f()Lcom/google/protobuf/w;
    .registers 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/protobuf/s;->a:Lcom/google/protobuf/w;

    return-object v0
.end method
