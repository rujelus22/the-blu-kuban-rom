.class public final Lcom/google/protobuf/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/protobuf/ae;

.field private final b:Ljava/lang/Object;

.field private final c:Lcom/google/protobuf/ae;

.field private final d:Lcom/google/protobuf/s;


# direct methods
.method private constructor <init>(Lcom/google/protobuf/ae;Ljava/lang/Object;Lcom/google/protobuf/ae;Lcom/google/protobuf/s;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672
    if-nez p1, :cond_d

    .line 673
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null containingTypeDefaultInstance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 676
    :cond_d
    invoke-virtual {p4}, Lcom/google/protobuf/s;->b()Lcom/google/protobuf/WireFormat$FieldType;

    move-result-object v0

    sget-object v1, Lcom/google/protobuf/WireFormat$FieldType;->MESSAGE:Lcom/google/protobuf/WireFormat$FieldType;

    if-ne v0, v1, :cond_1f

    if-nez p3, :cond_1f

    .line 678
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null messageDefaultInstance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 681
    :cond_1f
    iput-object p1, p0, Lcom/google/protobuf/t;->a:Lcom/google/protobuf/ae;

    .line 682
    iput-object p2, p0, Lcom/google/protobuf/t;->b:Ljava/lang/Object;

    .line 683
    iput-object p3, p0, Lcom/google/protobuf/t;->c:Lcom/google/protobuf/ae;

    .line 684
    iput-object p4, p0, Lcom/google/protobuf/t;->d:Lcom/google/protobuf/s;

    .line 685
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/protobuf/ae;Ljava/lang/Object;Lcom/google/protobuf/ae;Lcom/google/protobuf/s;B)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 662
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/protobuf/t;-><init>(Lcom/google/protobuf/ae;Ljava/lang/Object;Lcom/google/protobuf/ae;Lcom/google/protobuf/s;)V

    return-void
.end method

.method static synthetic a(Lcom/google/protobuf/t;)Lcom/google/protobuf/s;
    .registers 2
    .parameter

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/protobuf/t;->d:Lcom/google/protobuf/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/protobuf/t;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/protobuf/t;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/google/protobuf/t;)Lcom/google/protobuf/ae;
    .registers 2
    .parameter

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/protobuf/t;->c:Lcom/google/protobuf/ae;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/protobuf/t;->a:Lcom/google/protobuf/ae;

    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/protobuf/t;->d:Lcom/google/protobuf/s;

    invoke-virtual {v0}, Lcom/google/protobuf/s;->a()I

    move-result v0

    return v0
.end method
