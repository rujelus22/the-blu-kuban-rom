.class final Lcom/google/protobuf/al;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/protobuf/ai;

.field private b:Lcom/google/protobuf/aj;

.field private c:Lcom/google/protobuf/ac;

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/protobuf/ai;)V
    .registers 2
    .parameter

    .prologue
    .line 789
    iput-object p1, p0, Lcom/google/protobuf/al;->a:Lcom/google/protobuf/ai;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 790
    invoke-direct {p0}, Lcom/google/protobuf/al;->a()V

    .line 791
    return-void
.end method

.method private a([BII)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    move v1, p3

    move v0, p2

    .line 825
    :goto_2
    if-lez v1, :cond_29

    .line 826
    invoke-direct {p0}, Lcom/google/protobuf/al;->b()V

    .line 827
    iget-object v2, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    if-nez v2, :cond_f

    .line 828
    if-ne v1, p3, :cond_29

    .line 830
    const/4 v0, -0x1

    .line 846
    :goto_e
    return v0

    .line 835
    :cond_f
    iget v2, p0, Lcom/google/protobuf/al;->d:I

    iget v3, p0, Lcom/google/protobuf/al;->e:I

    sub-int/2addr v2, v3

    .line 836
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 837
    if-eqz p1, :cond_22

    .line 838
    iget-object v3, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    iget v4, p0, Lcom/google/protobuf/al;->e:I

    invoke-virtual {v3, p1, v4, v0, v2}, Lcom/google/protobuf/ac;->a([BIII)V

    .line 839
    add-int/2addr v0, v2

    .line 841
    :cond_22
    iget v3, p0, Lcom/google/protobuf/al;->e:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/protobuf/al;->e:I

    .line 842
    sub-int/2addr v1, v2

    .line 843
    goto :goto_2

    .line 846
    :cond_29
    sub-int v0, p3, v1

    goto :goto_e
.end method

.method private a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 885
    new-instance v0, Lcom/google/protobuf/aj;

    iget-object v1, p0, Lcom/google/protobuf/al;->a:Lcom/google/protobuf/ai;

    invoke-direct {v0, v1, v2}, Lcom/google/protobuf/aj;-><init>(Lcom/google/protobuf/e;B)V

    iput-object v0, p0, Lcom/google/protobuf/al;->b:Lcom/google/protobuf/aj;

    .line 886
    iget-object v0, p0, Lcom/google/protobuf/al;->b:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    .line 887
    iget-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    invoke-virtual {v0}, Lcom/google/protobuf/ac;->b()I

    move-result v0

    iput v0, p0, Lcom/google/protobuf/al;->d:I

    .line 888
    iput v2, p0, Lcom/google/protobuf/al;->e:I

    .line 889
    iput v2, p0, Lcom/google/protobuf/al;->f:I

    .line 890
    return-void
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 898
    iget-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    if-eqz v0, :cond_2c

    iget v0, p0, Lcom/google/protobuf/al;->e:I

    iget v1, p0, Lcom/google/protobuf/al;->d:I

    if-ne v0, v1, :cond_2c

    .line 901
    iget v0, p0, Lcom/google/protobuf/al;->f:I

    iget v1, p0, Lcom/google/protobuf/al;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/al;->f:I

    .line 902
    iput v2, p0, Lcom/google/protobuf/al;->e:I

    .line 903
    iget-object v0, p0, Lcom/google/protobuf/al;->b:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 904
    iget-object v0, p0, Lcom/google/protobuf/al;->b:Lcom/google/protobuf/aj;

    invoke-virtual {v0}, Lcom/google/protobuf/aj;->a()Lcom/google/protobuf/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    .line 905
    iget-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    invoke-virtual {v0}, Lcom/google/protobuf/ac;->b()I

    move-result v0

    iput v0, p0, Lcom/google/protobuf/al;->d:I

    .line 911
    :cond_2c
    :goto_2c
    return-void

    .line 907
    :cond_2d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    .line 908
    iput v2, p0, Lcom/google/protobuf/al;->d:I

    goto :goto_2c
.end method


# virtual methods
.method public final available()I
    .registers 3

    .prologue
    .line 861
    iget v0, p0, Lcom/google/protobuf/al;->f:I

    iget v1, p0, Lcom/google/protobuf/al;->e:I

    add-int/2addr v0, v1

    .line 862
    iget-object v1, p0, Lcom/google/protobuf/al;->a:Lcom/google/protobuf/ai;

    invoke-virtual {v1}, Lcom/google/protobuf/ai;->b()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method

.method public final mark(I)V
    .registers 4
    .parameter

    .prologue
    .line 873
    iget v0, p0, Lcom/google/protobuf/al;->f:I

    iget v1, p0, Lcom/google/protobuf/al;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/protobuf/al;->g:I

    .line 874
    return-void
.end method

.method public final markSupported()Z
    .registers 2

    .prologue
    .line 867
    const/4 v0, 0x1

    return v0
.end method

.method public final read()I
    .registers 4

    .prologue
    .line 851
    invoke-direct {p0}, Lcom/google/protobuf/al;->b()V

    .line 852
    iget-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    if-nez v0, :cond_9

    .line 853
    const/4 v0, -0x1

    .line 855
    :goto_8
    return v0

    :cond_9
    iget-object v0, p0, Lcom/google/protobuf/al;->c:Lcom/google/protobuf/ac;

    iget v1, p0, Lcom/google/protobuf/al;->e:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/protobuf/al;->e:I

    invoke-virtual {v0, v1}, Lcom/google/protobuf/ac;->b(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_8
.end method

.method public final read([BII)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 795
    if-nez p1, :cond_8

    .line 796
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 797
    :cond_8
    if-ltz p2, :cond_10

    if-ltz p3, :cond_10

    array-length v0, p1

    sub-int/2addr v0, p2

    if-le p3, v0, :cond_16

    .line 798
    :cond_10
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 800
    :cond_16
    invoke-direct {p0, p1, p2, p3}, Lcom/google/protobuf/al;->a([BII)I

    move-result v0

    return v0
.end method

.method public final declared-synchronized reset()V
    .registers 4

    .prologue
    .line 879
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/protobuf/al;->a()V

    .line 880
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/protobuf/al;->g:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/protobuf/al;->a([BII)I
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_d

    .line 881
    monitor-exit p0

    return-void

    .line 879
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final skip(J)J
    .registers 7
    .parameter

    .prologue
    const-wide/32 v0, 0x7fffffff

    .line 805
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gez v2, :cond_f

    .line 806
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 807
    :cond_f
    cmp-long v2, p1, v0

    if-lez v2, :cond_14

    move-wide p1, v0

    .line 810
    :cond_14
    const/4 v0, 0x0

    const/4 v1, 0x0

    long-to-int v2, p1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/protobuf/al;->a([BII)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
