.class final Lcom/google/protobuf/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/protobuf/ae;

.field private final b:Lcom/google/protobuf/i;

.field private c:Lcom/google/protobuf/e;

.field private volatile d:Lcom/google/protobuf/ae;

.field private volatile e:Z


# direct methods
.method private c()V
    .registers 4

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    if-eqz v0, :cond_5

    .line 116
    :goto_4
    return-void

    .line 103
    :cond_5
    monitor-enter p0

    .line 104
    :try_start_6
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    if-eqz v0, :cond_f

    .line 105
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_6 .. :try_end_b} :catchall_c

    goto :goto_4

    .line 116
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 108
    :cond_f
    :try_start_f
    iget-object v0, p0, Lcom/google/protobuf/x;->c:Lcom/google/protobuf/e;

    if-eqz v0, :cond_25

    .line 109
    iget-object v0, p0, Lcom/google/protobuf/x;->a:Lcom/google/protobuf/ae;

    invoke-interface {v0}, Lcom/google/protobuf/ae;->getParserForType()Lcom/google/protobuf/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/protobuf/x;->c:Lcom/google/protobuf/e;

    iget-object v2, p0, Lcom/google/protobuf/x;->b:Lcom/google/protobuf/i;

    invoke-interface {v0, v1, v2}, Lcom/google/protobuf/ah;->a(Lcom/google/protobuf/e;Lcom/google/protobuf/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/ae;

    iput-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;
    :try_end_25
    .catchall {:try_start_f .. :try_end_25} :catchall_c
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_25} :catch_27

    .line 116
    :cond_25
    :goto_25
    :try_start_25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_c

    goto :goto_4

    :catch_27
    move-exception v0

    goto :goto_25
.end method


# virtual methods
.method public final a()Lcom/google/protobuf/ae;
    .registers 2

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/protobuf/x;->c()V

    .line 40
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/ae;)Lcom/google/protobuf/ae;
    .registers 4
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    .line 49
    iput-object p1, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    .line 50
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/protobuf/x;->c:Lcom/google/protobuf/e;

    .line 51
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/protobuf/x;->e:Z

    .line 52
    return-object v0
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/protobuf/x;->e:Z

    if-eqz v0, :cond_b

    .line 62
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    invoke-interface {v0}, Lcom/google/protobuf/ae;->getSerializedSize()I

    move-result v0

    .line 64
    :goto_a
    return v0

    :cond_b
    iget-object v0, p0, Lcom/google/protobuf/x;->c:Lcom/google/protobuf/e;

    invoke-virtual {v0}, Lcom/google/protobuf/e;->b()I

    move-result v0

    goto :goto_a
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/protobuf/x;->c()V

    .line 90
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .registers 2

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/protobuf/x;->c()V

    .line 84
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/protobuf/x;->c()V

    .line 96
    iget-object v0, p0, Lcom/google/protobuf/x;->d:Lcom/google/protobuf/ae;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
