.class public final Lcom/google/zxing/datamatrix/detector/Detector;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/b;

.field private final b:Lcom/google/zxing/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/zxing/common/b;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    .line 48
    new-instance v0, Lcom/google/zxing/common/a/b;

    invoke-direct {v0, p1}, Lcom/google/zxing/common/a/b;-><init>(Lcom/google/zxing/common/b;)V

    iput-object v0, p0, Lcom/google/zxing/datamatrix/detector/Detector;->b:Lcom/google/zxing/common/a/b;

    .line 49
    return-void
.end method

.method private static a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 310
    invoke-static {p0, p1}, Lcom/google/zxing/g;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)F

    move-result v0

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private static a(Lcom/google/zxing/common/b;Lcom/google/zxing/g;Lcom/google/zxing/g;Lcom/google/zxing/g;Lcom/google/zxing/g;II)Lcom/google/zxing/common/b;
    .registers 28
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 329
    invoke-static {}, Lcom/google/zxing/common/h;->a()Lcom/google/zxing/common/h;

    move-result-object v1

    .line 331
    const/high16 v5, 0x3f00

    const/high16 v6, 0x3f00

    move/from16 v0, p5

    int-to-float v2, v0

    const/high16 v3, 0x3f00

    sub-float v7, v2, v3

    const/high16 v8, 0x3f00

    move/from16 v0, p5

    int-to-float v2, v0

    const/high16 v3, 0x3f00

    sub-float v9, v2, v3

    move/from16 v0, p6

    int-to-float v2, v0

    const/high16 v3, 0x3f00

    sub-float v10, v2, v3

    const/high16 v11, 0x3f00

    move/from16 v0, p6

    int-to-float v2, v0

    const/high16 v3, 0x3f00

    sub-float v12, v2, v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/g;->a()F

    move-result v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/g;->b()F

    move-result v14

    invoke-virtual/range {p4 .. p4}, Lcom/google/zxing/g;->a()F

    move-result v15

    invoke-virtual/range {p4 .. p4}, Lcom/google/zxing/g;->b()F

    move-result v16

    invoke-virtual/range {p3 .. p3}, Lcom/google/zxing/g;->a()F

    move-result v17

    invoke-virtual/range {p3 .. p3}, Lcom/google/zxing/g;->b()F

    move-result v18

    invoke-virtual/range {p2 .. p2}, Lcom/google/zxing/g;->a()F

    move-result v19

    invoke-virtual/range {p2 .. p2}, Lcom/google/zxing/g;->b()F

    move-result v20

    move-object/from16 v2, p0

    move/from16 v3, p5

    move/from16 v4, p6

    invoke-virtual/range {v1 .. v20}, Lcom/google/zxing/common/h;->a(Lcom/google/zxing/common/b;IIFFFFFFFFFFFFFFFF)Lcom/google/zxing/common/b;

    move-result-object v1

    return-object v1
.end method

.method private static a(Ljava/util/Map;Lcom/google/zxing/g;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 317
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 318
    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    return-void

    .line 318
    :cond_11
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_9
.end method

.method private a(Lcom/google/zxing/g;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 306
    invoke-virtual {p1}, Lcom/google/zxing/g;->a()F

    move-result v0

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_31

    invoke-virtual {p1}, Lcom/google/zxing/g;->a()F

    move-result v0

    iget-object v1, p0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->d()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_31

    invoke-virtual {p1}, Lcom/google/zxing/g;->b()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_31

    invoke-virtual {p1}, Lcom/google/zxing/g;->b()F

    move-result v0

    iget-object v1, p0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->e()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_31

    const/4 v0, 0x1

    :goto_30
    return v0

    :cond_31
    const/4 v0, 0x0

    goto :goto_30
.end method

.method private b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;
    .registers 22
    .parameter
    .parameter

    .prologue
    .line 357
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/g;->a()F

    move-result v2

    float-to-int v4, v2

    .line 358
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/g;->b()F

    move-result v2

    float-to-int v5, v2

    .line 359
    invoke-virtual/range {p2 .. p2}, Lcom/google/zxing/g;->a()F

    move-result v2

    float-to-int v2, v2

    .line 360
    invoke-virtual/range {p2 .. p2}, Lcom/google/zxing/g;->b()F

    move-result v3

    float-to-int v3, v3

    .line 361
    sub-int v6, v3, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    sub-int v7, v2, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v6, v7, :cond_71

    const/4 v6, 0x1

    move v13, v6

    .line 362
    :goto_24
    if-eqz v13, :cond_8d

    .line 371
    :goto_26
    sub-int v6, v3, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v14

    .line 372
    sub-int v6, v2, v4

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v15

    .line 373
    neg-int v6, v14

    shr-int/lit8 v10, v6, 0x1

    .line 374
    if-ge v4, v2, :cond_74

    const/4 v6, 0x1

    move v12, v6

    .line 375
    :goto_39
    if-ge v5, v3, :cond_77

    const/4 v6, 0x1

    .line 376
    :goto_3c
    const/4 v8, 0x0

    .line 377
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    if-eqz v13, :cond_79

    move v9, v4

    :goto_44
    if-eqz v13, :cond_7b

    move v7, v5

    :goto_47
    invoke-virtual {v11, v9, v7}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    move v11, v10

    move v7, v4

    .line 378
    :goto_4d
    if-eq v5, v3, :cond_81

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    move-object/from16 v16, v0

    if-eqz v13, :cond_7d

    move v10, v7

    :goto_58
    if-eqz v13, :cond_7f

    move v4, v5

    :goto_5b
    move-object/from16 v0, v16

    invoke-virtual {v0, v10, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v4

    .line 380
    if-eq v4, v9, :cond_66

    .line 381
    add-int/lit8 v8, v8, 0x1

    move v9, v4

    .line 384
    :cond_66
    add-int v4, v11, v15

    .line 385
    if-lez v4, :cond_6e

    .line 386
    if-eq v7, v2, :cond_81

    .line 387
    add-int/2addr v7, v12

    .line 390
    sub-int/2addr v4, v14

    .line 378
    :cond_6e
    add-int/2addr v5, v6

    move v11, v4

    goto :goto_4d

    .line 361
    :cond_71
    const/4 v6, 0x0

    move v13, v6

    goto :goto_24

    .line 374
    :cond_74
    const/4 v6, -0x1

    move v12, v6

    goto :goto_39

    .line 375
    :cond_77
    const/4 v6, -0x1

    goto :goto_3c

    :cond_79
    move v9, v5

    .line 377
    goto :goto_44

    :cond_7b
    move v7, v4

    goto :goto_47

    :cond_7d
    move v10, v5

    .line 379
    goto :goto_58

    :cond_7f
    move v4, v7

    goto :goto_5b

    :cond_81
    move v2, v8

    .line 393
    new-instance v3, Lcom/google/zxing/datamatrix/detector/b;

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v3, v0, v1, v2, v4}, Lcom/google/zxing/datamatrix/detector/b;-><init>(Lcom/google/zxing/g;Lcom/google/zxing/g;IB)V

    return-object v3

    :cond_8d
    move/from16 v17, v3

    move v3, v2

    move/from16 v2, v17

    move/from16 v18, v5

    move v5, v4

    move/from16 v4, v18

    goto :goto_26
.end method


# virtual methods
.method public final a()Lcom/google/zxing/common/f;
    .registers 13

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/zxing/datamatrix/detector/Detector;->b:Lcom/google/zxing/common/a/b;

    invoke-virtual {v0}, Lcom/google/zxing/common/a/b;->a()[Lcom/google/zxing/g;

    move-result-object v0

    .line 60
    const/4 v1, 0x0

    aget-object v5, v0, v1

    .line 61
    const/4 v1, 0x1

    aget-object v6, v0, v1

    .line 62
    const/4 v1, 0x2

    aget-object v7, v0, v1

    .line 63
    const/4 v1, 0x3

    aget-object v8, v0, v1

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 69
    invoke-direct {p0, v5, v6}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-direct {p0, v5, v7}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    invoke-direct {p0, v6, v8}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-direct {p0, v7, v8}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    new-instance v0, Lcom/google/zxing/datamatrix/detector/Detector$ResultPointsAndTransitionsComparator;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/zxing/datamatrix/detector/Detector$ResultPointsAndTransitionsComparator;-><init>(Lcom/google/zxing/datamatrix/detector/a;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 77
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/datamatrix/detector/b;

    .line 78
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/zxing/datamatrix/detector/b;

    .line 82
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 83
    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/detector/b;->a()Lcom/google/zxing/g;

    move-result-object v2

    invoke-static {v9, v2}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Ljava/util/Map;Lcom/google/zxing/g;)V

    .line 84
    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/detector/b;->b()Lcom/google/zxing/g;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Ljava/util/Map;Lcom/google/zxing/g;)V

    .line 85
    invoke-virtual {v1}, Lcom/google/zxing/datamatrix/detector/b;->a()Lcom/google/zxing/g;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Ljava/util/Map;Lcom/google/zxing/g;)V

    .line 86
    invoke-virtual {v1}, Lcom/google/zxing/datamatrix/detector/b;->b()Lcom/google/zxing/g;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Ljava/util/Map;Lcom/google/zxing/g;)V

    .line 88
    const/4 v2, 0x0

    .line 89
    const/4 v0, 0x0

    .line 90
    const/4 v1, 0x0

    .line 91
    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-object v3, v0

    move-object v4, v2

    move-object v2, v1

    :goto_7a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 92
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/zxing/g;

    .line 93
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 94
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v11, 0x2

    if-ne v0, v11, :cond_9b

    move-object v3, v1

    .line 95
    goto :goto_7a

    .line 98
    :cond_9b
    if-nez v4, :cond_9f

    move-object v4, v1

    .line 99
    goto :goto_7a

    :cond_9f
    move-object v2, v1

    .line 104
    goto :goto_7a

    .line 106
    :cond_a1
    if-eqz v4, :cond_a7

    if-eqz v3, :cond_a7

    if-nez v2, :cond_ac

    .line 107
    :cond_a7
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 111
    :cond_ac
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/zxing/g;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v2, v0, v1

    .line 113
    invoke-static {v0}, Lcom/google/zxing/g;->a([Lcom/google/zxing/g;)V

    .line 116
    const/4 v1, 0x0

    aget-object v3, v0, v1

    .line 117
    const/4 v1, 0x1

    aget-object v2, v0, v1

    .line 118
    const/4 v1, 0x2

    aget-object v1, v0, v1

    .line 122
    invoke-interface {v9, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a8

    move-object v0, v5

    .line 141
    :goto_cb
    invoke-direct {p0, v1, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v5

    .line 142
    invoke-direct {p0, v3, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v4

    .line 144
    and-int/lit8 v6, v5, 0x1

    const/4 v7, 0x1

    if-ne v6, v7, :cond_e2

    .line 146
    add-int/lit8 v5, v5, 0x1

    .line 148
    :cond_e2
    add-int/lit8 v6, v5, 0x2

    .line 150
    and-int/lit8 v5, v4, 0x1

    const/4 v7, 0x1

    if-ne v5, v7, :cond_eb

    .line 152
    add-int/lit8 v4, v4, 0x1

    .line 154
    :cond_eb
    add-int/lit8 v7, v4, 0x2

    .line 162
    mul-int/lit8 v4, v6, 0x4

    mul-int/lit8 v5, v7, 0x7

    if-ge v4, v5, :cond_f9

    mul-int/lit8 v4, v7, 0x4

    mul-int/lit8 v5, v6, 0x7

    if-lt v4, v5, :cond_200

    .line 165
    :cond_f9
    invoke-static {v2, v3}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, v6

    div-float v5, v4, v5

    invoke-static {v1, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v8

    invoke-virtual {v1}, Lcom/google/zxing/g;->a()F

    move-result v9

    sub-float/2addr v8, v9

    int-to-float v9, v4

    div-float/2addr v8, v9

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v9

    invoke-virtual {v1}, Lcom/google/zxing/g;->b()F

    move-result v10

    sub-float/2addr v9, v10

    int-to-float v4, v4

    div-float/2addr v9, v4

    new-instance v4, Lcom/google/zxing/g;

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v10

    mul-float/2addr v8, v5

    add-float/2addr v8, v10

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v10

    mul-float/2addr v5, v9

    add-float/2addr v5, v10

    invoke-direct {v4, v8, v5}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-static {v2, v1}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v5

    int-to-float v5, v5

    int-to-float v8, v7

    div-float v8, v5, v8

    invoke-static {v3, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v9

    invoke-virtual {v3}, Lcom/google/zxing/g;->a()F

    move-result v10

    sub-float/2addr v9, v10

    int-to-float v10, v5

    div-float/2addr v9, v10

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v10

    invoke-virtual {v3}, Lcom/google/zxing/g;->b()F

    move-result v11

    sub-float/2addr v10, v11

    int-to-float v5, v5

    div-float/2addr v10, v5

    new-instance v5, Lcom/google/zxing/g;

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v11

    mul-float/2addr v9, v8

    add-float/2addr v9, v11

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v11

    mul-float/2addr v8, v10

    add-float/2addr v8, v11

    invoke-direct {v5, v9, v8}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-direct {p0, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;)Z

    move-result v8

    if-nez v8, :cond_1bd

    invoke-direct {p0, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;)Z

    move-result v4

    if-nez v4, :cond_1fd

    const/4 v4, 0x0

    .line 167
    :cond_16c
    :goto_16c
    if-nez v4, :cond_16f

    move-object v4, v0

    .line 171
    :cond_16f
    invoke-direct {p0, v1, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v5

    .line 172
    invoke-direct {p0, v3, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v6

    .line 174
    and-int/lit8 v0, v5, 0x1

    const/4 v7, 0x1

    if-ne v0, v7, :cond_186

    .line 176
    add-int/lit8 v5, v5, 0x1

    .line 179
    :cond_186
    and-int/lit8 v0, v6, 0x1

    const/4 v7, 0x1

    if-ne v0, v7, :cond_18d

    .line 181
    add-int/lit8 v6, v6, 0x1

    .line 184
    :cond_18d
    iget-object v0, p0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    invoke-static/range {v0 .. v6}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/common/b;Lcom/google/zxing/g;Lcom/google/zxing/g;Lcom/google/zxing/g;Lcom/google/zxing/g;II)Lcom/google/zxing/common/b;

    move-result-object v0

    .line 213
    :goto_193
    new-instance v5, Lcom/google/zxing/common/f;

    const/4 v6, 0x4

    new-array v6, v6, [Lcom/google/zxing/g;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v1, 0x1

    aput-object v2, v6, v1

    const/4 v1, 0x2

    aput-object v3, v6, v1

    const/4 v1, 0x3

    aput-object v4, v6, v1

    invoke-direct {v5, v0, v6}, Lcom/google/zxing/common/f;-><init>(Lcom/google/zxing/common/b;[Lcom/google/zxing/g;)V

    return-object v5

    .line 124
    :cond_1a8
    invoke-interface {v9, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b1

    move-object v0, v6

    .line 125
    goto/16 :goto_cb

    .line 126
    :cond_1b1
    invoke-interface {v9, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1ba

    move-object v0, v7

    .line 127
    goto/16 :goto_cb

    :cond_1ba
    move-object v0, v8

    .line 129
    goto/16 :goto_cb

    .line 165
    :cond_1bd
    invoke-direct {p0, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;)Z

    move-result v8

    if-eqz v8, :cond_16c

    invoke-direct {p0, v1, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v8

    sub-int v8, v6, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    invoke-direct {p0, v3, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v9

    sub-int v9, v7, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {p0, v1, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v9

    sub-int/2addr v6, v9

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-direct {p0, v3, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v9

    sub-int/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    add-int/2addr v6, v7

    if-le v8, v6, :cond_16c

    :cond_1fd
    move-object v4, v5

    goto/16 :goto_16c

    .line 189
    :cond_200
    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 191
    invoke-static {v2, v3}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v4

    int-to-float v4, v4

    int-to-float v6, v5

    div-float v6, v4, v6

    invoke-static {v1, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v7

    invoke-virtual {v1}, Lcom/google/zxing/g;->a()F

    move-result v8

    sub-float/2addr v7, v8

    int-to-float v8, v4

    div-float/2addr v7, v8

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v8

    invoke-virtual {v1}, Lcom/google/zxing/g;->b()F

    move-result v9

    sub-float/2addr v8, v9

    int-to-float v4, v4

    div-float/2addr v8, v4

    new-instance v4, Lcom/google/zxing/g;

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v9

    mul-float/2addr v7, v6

    add-float/2addr v7, v9

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v9

    mul-float/2addr v6, v8

    add-float/2addr v6, v9

    invoke-direct {v4, v7, v6}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-static {v2, v1}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v6

    int-to-float v6, v6

    int-to-float v5, v5

    div-float/2addr v6, v5

    invoke-static {v3, v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;Lcom/google/zxing/g;)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v7

    invoke-virtual {v3}, Lcom/google/zxing/g;->a()F

    move-result v8

    sub-float/2addr v7, v8

    int-to-float v8, v5

    div-float/2addr v7, v8

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v8

    invoke-virtual {v3}, Lcom/google/zxing/g;->b()F

    move-result v9

    sub-float/2addr v8, v9

    int-to-float v5, v5

    div-float/2addr v8, v5

    new-instance v5, Lcom/google/zxing/g;

    invoke-virtual {v0}, Lcom/google/zxing/g;->a()F

    move-result v9

    mul-float/2addr v7, v6

    add-float/2addr v7, v9

    invoke-virtual {v0}, Lcom/google/zxing/g;->b()F

    move-result v9

    mul-float/2addr v6, v8

    add-float/2addr v6, v9

    invoke-direct {v5, v7, v6}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-direct {p0, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;)Z

    move-result v6

    if-nez v6, :cond_29f

    invoke-direct {p0, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;)Z

    move-result v4

    if-nez v4, :cond_2d1

    const/4 v4, 0x0

    .line 192
    :cond_276
    :goto_276
    if-nez v4, :cond_279

    move-object v4, v0

    .line 197
    :cond_279
    invoke-direct {p0, v1, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v0

    invoke-direct {p0, v3, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 199
    add-int/lit8 v5, v0, 0x1

    .line 200
    and-int/lit8 v0, v5, 0x1

    const/4 v6, 0x1

    if-ne v0, v6, :cond_296

    .line 201
    add-int/lit8 v5, v5, 0x1

    .line 204
    :cond_296
    iget-object v0, p0, Lcom/google/zxing/datamatrix/detector/Detector;->a:Lcom/google/zxing/common/b;

    move v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/common/b;Lcom/google/zxing/g;Lcom/google/zxing/g;Lcom/google/zxing/g;Lcom/google/zxing/g;II)Lcom/google/zxing/common/b;

    move-result-object v0

    goto/16 :goto_193

    .line 191
    :cond_29f
    invoke-direct {p0, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->a(Lcom/google/zxing/g;)Z

    move-result v6

    if-eqz v6, :cond_276

    invoke-direct {p0, v1, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v6

    invoke-direct {p0, v3, v4}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-direct {p0, v1, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v7

    invoke-direct {p0, v3, v5}, Lcom/google/zxing/datamatrix/detector/Detector;->b(Lcom/google/zxing/g;Lcom/google/zxing/g;)Lcom/google/zxing/datamatrix/detector/b;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/zxing/datamatrix/detector/b;->c()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v6, v7, :cond_276

    :cond_2d1
    move-object v4, v5

    goto :goto_276
.end method
