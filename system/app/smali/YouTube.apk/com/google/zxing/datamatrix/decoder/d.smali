.class public final Lcom/google/zxing/datamatrix/decoder/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/reedsolomon/c;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/zxing/common/reedsolomon/c;

    sget-object v1, Lcom/google/zxing/common/reedsolomon/a;->f:Lcom/google/zxing/common/reedsolomon/a;

    invoke-direct {v0, v1}, Lcom/google/zxing/common/reedsolomon/c;-><init>(Lcom/google/zxing/common/reedsolomon/a;)V

    iput-object v0, p0, Lcom/google/zxing/datamatrix/decoder/d;->a:Lcom/google/zxing/common/reedsolomon/c;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;
    .registers 13
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 75
    new-instance v0, Lcom/google/zxing/datamatrix/decoder/a;

    invoke-direct {v0, p1}, Lcom/google/zxing/datamatrix/decoder/a;-><init>(Lcom/google/zxing/common/b;)V

    .line 76
    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/a;->a()Lcom/google/zxing/datamatrix/decoder/e;

    move-result-object v2

    .line 79
    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/a;->b()[B

    move-result-object v0

    .line 81
    invoke-static {v0, v2}, Lcom/google/zxing/datamatrix/decoder/b;->a([BLcom/google/zxing/datamatrix/decoder/e;)[Lcom/google/zxing/datamatrix/decoder/b;

    move-result-object v3

    .line 83
    array-length v4, v3

    .line 87
    array-length v5, v3

    move v0, v1

    move v2, v1

    :goto_16
    if-ge v0, v5, :cond_22

    aget-object v6, v3, v0

    .line 88
    invoke-virtual {v6}, Lcom/google/zxing/datamatrix/decoder/b;->a()I

    move-result v6

    add-int/2addr v2, v6

    .line 87
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    .line 90
    :cond_22
    new-array v5, v2, [B

    move v2, v1

    .line 93
    :goto_25
    if-ge v2, v4, :cond_69

    .line 94
    aget-object v0, v3, v2

    .line 95
    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/b;->b()[B

    move-result-object v6

    .line 96
    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/decoder/b;->a()I

    move-result v7

    .line 97
    array-length v8, v6

    new-array v9, v8, [I

    move v0, v1

    :goto_35
    if-ge v0, v8, :cond_40

    aget-byte v10, v6, v0

    and-int/lit16 v10, v10, 0xff

    aput v10, v9, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_35

    :cond_40
    array-length v0, v6

    sub-int/2addr v0, v7

    :try_start_42
    iget-object v8, p0, Lcom/google/zxing/datamatrix/decoder/d;->a:Lcom/google/zxing/common/reedsolomon/c;

    invoke-virtual {v8, v9, v0}, Lcom/google/zxing/common/reedsolomon/c;->a([II)V
    :try_end_47
    .catch Lcom/google/zxing/common/reedsolomon/ReedSolomonException; {:try_start_42 .. :try_end_47} :catch_52

    move v0, v1

    :goto_48
    if-ge v0, v7, :cond_58

    aget v8, v9, v0

    int-to-byte v8, v8

    aput-byte v8, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_48

    :catch_52
    move-exception v0

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_58
    move v0, v1

    .line 98
    :goto_59
    if-ge v0, v7, :cond_65

    .line 100
    mul-int v8, v0, v4

    add-int/2addr v8, v2

    aget-byte v9, v6, v0

    aput-byte v9, v5, v8

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_59

    .line 93
    :cond_65
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_25

    .line 105
    :cond_69
    invoke-static {v5}, Lcom/google/zxing/datamatrix/decoder/DecodedBitStreamParser;->a([B)Lcom/google/zxing/common/d;

    move-result-object v0

    return-object v0
.end method
