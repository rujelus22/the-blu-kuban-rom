.class public final Lcom/google/zxing/datamatrix/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# static fields
.field private static final a:[Lcom/google/zxing/g;


# instance fields
.field private final b:Lcom/google/zxing/datamatrix/decoder/d;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/zxing/g;

    sput-object v0, Lcom/google/zxing/datamatrix/a;->a:[Lcom/google/zxing/g;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/google/zxing/datamatrix/decoder/d;

    invoke-direct {v0}, Lcom/google/zxing/datamatrix/decoder/d;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/datamatrix/a;->b:Lcom/google/zxing/datamatrix/decoder/d;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 67
    if-eqz p2, :cond_b4

    sget-object v0, Lcom/google/zxing/DecodeHintType;->PURE_BARCODE:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 68
    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/zxing/common/b;->b()[I

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/zxing/common/b;->c()[I

    move-result-object v4

    if-eqz v2, :cond_1c

    if-nez v4, :cond_21

    :cond_1c
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_21
    invoke-virtual {v3}, Lcom/google/zxing/common/b;->d()I

    move-result v5

    aget v0, v2, v1

    aget v6, v2, v8

    :goto_29
    if-ge v0, v5, :cond_34

    invoke-virtual {v3, v0, v6}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v7

    if-eqz v7, :cond_34

    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    :cond_34
    if-ne v0, v5, :cond_3b

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_3b
    aget v5, v2, v1

    sub-int v5, v0, v5

    if-nez v5, :cond_46

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_46
    aget v0, v2, v8

    aget v6, v4, v8

    aget v2, v2, v1

    aget v4, v4, v1

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, 0x1

    div-int/2addr v4, v5

    sub-int/2addr v6, v0

    add-int/lit8 v6, v6, 0x1

    div-int/2addr v6, v5

    if-lez v4, :cond_5a

    if-gtz v6, :cond_5f

    :cond_5a
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_5f
    shr-int/lit8 v7, v5, 0x1

    add-int v8, v0, v7

    add-int/2addr v7, v2

    new-instance v9, Lcom/google/zxing/common/b;

    invoke-direct {v9, v4, v6}, Lcom/google/zxing/common/b;-><init>(II)V

    move v2, v1

    :goto_6a
    if-ge v2, v6, :cond_86

    mul-int v0, v2, v5

    add-int v10, v8, v0

    move v0, v1

    :goto_71
    if-ge v0, v4, :cond_82

    mul-int v11, v0, v5

    add-int/2addr v11, v7

    invoke-virtual {v3, v11, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v11

    if-eqz v11, :cond_7f

    invoke-virtual {v9, v0, v2}, Lcom/google/zxing/common/b;->b(II)V

    :cond_7f
    add-int/lit8 v0, v0, 0x1

    goto :goto_71

    :cond_82
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6a

    .line 69
    :cond_86
    iget-object v0, p0, Lcom/google/zxing/datamatrix/a;->b:Lcom/google/zxing/datamatrix/decoder/d;

    invoke-virtual {v0, v9}, Lcom/google/zxing/datamatrix/decoder/d;->a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;

    move-result-object v1

    .line 70
    sget-object v0, Lcom/google/zxing/datamatrix/a;->a:[Lcom/google/zxing/g;

    .line 76
    :goto_8e
    new-instance v2, Lcom/google/zxing/f;

    invoke-virtual {v1}, Lcom/google/zxing/common/d;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/zxing/common/d;->a()[B

    move-result-object v4

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->DATA_MATRIX:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    .line 78
    invoke-virtual {v1}, Lcom/google/zxing/common/d;->c()Ljava/util/List;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_a8

    .line 80
    sget-object v3, Lcom/google/zxing/ResultMetadataType;->BYTE_SEGMENTS:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v2, v3, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 82
    :cond_a8
    invoke-virtual {v1}, Lcom/google/zxing/common/d;->d()Ljava/lang/String;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_b3

    .line 84
    sget-object v1, Lcom/google/zxing/ResultMetadataType;->ERROR_CORRECTION_LEVEL:Lcom/google/zxing/ResultMetadataType;

    invoke-virtual {v2, v1, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 86
    :cond_b3
    return-object v2

    .line 72
    :cond_b4
    new-instance v0, Lcom/google/zxing/datamatrix/detector/Detector;

    invoke-virtual {p1}, Lcom/google/zxing/b;->c()Lcom/google/zxing/common/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/zxing/datamatrix/detector/Detector;-><init>(Lcom/google/zxing/common/b;)V

    invoke-virtual {v0}, Lcom/google/zxing/datamatrix/detector/Detector;->a()Lcom/google/zxing/common/f;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/zxing/datamatrix/a;->b:Lcom/google/zxing/datamatrix/decoder/d;

    invoke-virtual {v0}, Lcom/google/zxing/common/f;->d()Lcom/google/zxing/common/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/zxing/datamatrix/decoder/d;->a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;

    move-result-object v1

    .line 74
    invoke-virtual {v0}, Lcom/google/zxing/common/f;->e()[Lcom/google/zxing/g;

    move-result-object v0

    goto :goto_8e
.end method

.method public final a()V
    .registers 1

    .prologue
    .line 92
    return-void
.end method
