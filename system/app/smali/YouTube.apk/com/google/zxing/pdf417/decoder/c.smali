.class public final Lcom/google/zxing/pdf417/decoder/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/pdf417/decoder/a/a;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/zxing/pdf417/decoder/a/a;

    invoke-direct {v0}, Lcom/google/zxing/pdf417/decoder/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/pdf417/decoder/c;->a:Lcom/google/zxing/pdf417/decoder/a/a;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/zxing/common/b;)Lcom/google/zxing/common/d;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 71
    new-instance v0, Lcom/google/zxing/pdf417/decoder/a;

    invoke-direct {v0, p1}, Lcom/google/zxing/pdf417/decoder/a;-><init>(Lcom/google/zxing/common/b;)V

    .line 72
    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a;->a()[I

    move-result-object v1

    .line 73
    array-length v2, v1

    if-nez v2, :cond_12

    .line 74
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 77
    :cond_12
    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a;->c()I

    move-result v2

    .line 78
    const/4 v3, 0x1

    add-int/lit8 v2, v2, 0x1

    shl-int v2, v3, v2

    .line 79
    invoke-virtual {v0}, Lcom/google/zxing/pdf417/decoder/a;->b()[I

    move-result-object v0

    .line 81
    array-length v3, v0

    div-int/lit8 v4, v2, 0x2

    add-int/lit8 v4, v4, 0x3

    if-gt v3, v4, :cond_2c

    if-ltz v2, :cond_2c

    const/16 v3, 0x200

    if-le v2, v3, :cond_31

    :cond_2c
    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_31
    iget-object v3, p0, Lcom/google/zxing/pdf417/decoder/c;->a:Lcom/google/zxing/pdf417/decoder/a/a;

    invoke-virtual {v3, v1, v2, v0}, Lcom/google/zxing/pdf417/decoder/a/a;->a([II[I)V

    .line 82
    array-length v0, v1

    const/4 v3, 0x4

    if-ge v0, v3, :cond_3f

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_3f
    aget v0, v1, v5

    array-length v3, v1

    if-le v0, v3, :cond_49

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_49
    if-nez v0, :cond_52

    array-length v0, v1

    if-ge v2, v0, :cond_57

    array-length v0, v1

    sub-int/2addr v0, v2

    aput v0, v1, v5

    .line 85
    :cond_52
    invoke-static {v1}, Lcom/google/zxing/pdf417/decoder/DecodedBitStreamParser;->a([I)Lcom/google/zxing/common/d;

    move-result-object v0

    return-object v0

    .line 82
    :cond_57
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method
