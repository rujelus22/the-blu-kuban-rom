.class public final Lcom/google/zxing/oned/c;
.super Lcom/google/zxing/oned/k;
.source "SourceFile"


# static fields
.field static final a:[I

.field private static final b:[C

.field private static final c:I


# instance fields
.field private final d:Z

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 39
    const-string v0, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/zxing/oned/c;->b:[C

    .line 46
    const/16 v0, 0x2c

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    .line 54
    sput-object v0, Lcom/google/zxing/oned/c;->a:[I

    const/16 v1, 0x27

    aget v0, v0, v1

    sput v0, Lcom/google/zxing/oned/c;->c:I

    return-void

    .line 46
    :array_18
    .array-data 0x4
        0x34t 0x0t 0x0t 0x0t
        0x21t 0x1t 0x0t 0x0t
        0x61t 0x0t 0x0t 0x0t
        0x60t 0x1t 0x0t 0x0t
        0x31t 0x0t 0x0t 0x0t
        0x30t 0x1t 0x0t 0x0t
        0x70t 0x0t 0x0t 0x0t
        0x25t 0x0t 0x0t 0x0t
        0x24t 0x1t 0x0t 0x0t
        0x64t 0x0t 0x0t 0x0t
        0x9t 0x1t 0x0t 0x0t
        0x49t 0x0t 0x0t 0x0t
        0x48t 0x1t 0x0t 0x0t
        0x19t 0x0t 0x0t 0x0t
        0x18t 0x1t 0x0t 0x0t
        0x58t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0xct 0x1t 0x0t 0x0t
        0x4ct 0x0t 0x0t 0x0t
        0x1ct 0x0t 0x0t 0x0t
        0x3t 0x1t 0x0t 0x0t
        0x43t 0x0t 0x0t 0x0t
        0x42t 0x1t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
        0x12t 0x1t 0x0t 0x0t
        0x52t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x6t 0x1t 0x0t 0x0t
        0x46t 0x0t 0x0t 0x0t
        0x16t 0x0t 0x0t 0x0t
        0x81t 0x1t 0x0t 0x0t
        0xc1t 0x0t 0x0t 0x0t
        0xc0t 0x1t 0x0t 0x0t
        0x91t 0x0t 0x0t 0x0t
        0x90t 0x1t 0x0t 0x0t
        0xd0t 0x0t 0x0t 0x0t
        0x85t 0x0t 0x0t 0x0t
        0x84t 0x1t 0x0t 0x0t
        0xc4t 0x0t 0x0t 0x0t
        0x94t 0x0t 0x0t 0x0t
        0xa8t 0x0t 0x0t 0x0t
        0xa2t 0x0t 0x0t 0x0t
        0x8at 0x0t 0x0t 0x0t
        0x2at 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Lcom/google/zxing/oned/k;-><init>()V

    .line 64
    iput-boolean v0, p0, Lcom/google/zxing/oned/c;->d:Z

    .line 65
    iput-boolean v0, p0, Lcom/google/zxing/oned/c;->e:Z

    .line 66
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 3
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/zxing/oned/k;-><init>()V

    .line 76
    iput-boolean p1, p0, Lcom/google/zxing/oned/c;->d:Z

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/oned/c;->e:Z

    .line 78
    return-void
.end method

.method private static a(I)C
    .registers 3
    .parameter

    .prologue
    .line 253
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/google/zxing/oned/c;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_14

    .line 254
    sget-object v1, Lcom/google/zxing/oned/c;->a:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_11

    .line 255
    sget-object v1, Lcom/google/zxing/oned/c;->b:[C

    aget-char v0, v1, v0

    return v0

    .line 253
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 258
    :cond_14
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method private static a([I)I
    .registers 14
    .parameter

    .prologue
    const/4 v11, 0x3

    const/4 v5, -0x1

    const/4 v1, 0x0

    .line 210
    array-length v7, p0

    move v0, v1

    .line 214
    :goto_5
    const v2, 0x7fffffff

    .line 215
    array-length v6, p0

    move v4, v1

    :goto_a
    if-ge v4, v6, :cond_17

    aget v3, p0, v4

    .line 216
    if-ge v3, v2, :cond_13

    if-le v3, v0, :cond_13

    move v2, v3

    .line 215
    :cond_13
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_a

    :cond_17
    move v6, v1

    move v0, v1

    move v3, v1

    move v4, v1

    .line 224
    :goto_1b
    if-ge v6, v7, :cond_2f

    .line 225
    aget v8, p0, v6

    .line 226
    aget v9, p0, v6

    if-le v9, v2, :cond_2c

    .line 227
    const/4 v9, 0x1

    add-int/lit8 v10, v7, -0x1

    sub-int/2addr v10, v6

    shl-int/2addr v9, v10

    or-int/2addr v0, v9

    .line 228
    add-int/lit8 v4, v4, 0x1

    .line 229
    add-int/2addr v3, v8

    .line 224
    :cond_2c
    add-int/lit8 v6, v6, 0x1

    goto :goto_1b

    .line 232
    :cond_2f
    if-ne v4, v11, :cond_49

    move v12, v1

    move v1, v4

    move v4, v12

    .line 236
    :goto_34
    if-ge v4, v7, :cond_45

    if-lez v1, :cond_45

    .line 237
    aget v6, p0, v4

    .line 238
    aget v8, p0, v4

    if-le v8, v2, :cond_46

    .line 239
    add-int/lit8 v1, v1, -0x1

    .line 241
    shl-int/lit8 v6, v6, 0x1

    if-lt v6, v3, :cond_46

    move v0, v5

    .line 249
    :cond_45
    :goto_45
    return v0

    .line 236
    :cond_46
    add-int/lit8 v4, v4, 0x1

    goto :goto_34

    .line 248
    :cond_49
    if-gt v4, v11, :cond_4d

    move v0, v5

    .line 249
    goto :goto_45

    :cond_4d
    move v0, v2

    goto :goto_5
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v7, 0x5a

    const/16 v6, 0x41

    .line 262
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 263
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    move v2, v1

    .line 264
    :goto_f
    if-ge v2, v3, :cond_85

    .line 265
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 266
    const/16 v5, 0x2b

    if-eq v0, v5, :cond_25

    const/16 v5, 0x24

    if-eq v0, v5, :cond_25

    const/16 v5, 0x25

    if-eq v0, v5, :cond_25

    const/16 v5, 0x2f

    if-ne v0, v5, :cond_80

    .line 267
    :cond_25
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p0, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 269
    sparse-switch v0, :sswitch_data_8a

    move v0, v1

    .line 307
    :goto_2f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 309
    add-int/lit8 v0, v2, 0x1

    .line 264
    :goto_34
    add-int/lit8 v2, v0, 0x1

    goto :goto_f

    .line 272
    :sswitch_37
    if-lt v5, v6, :cond_3f

    if-gt v5, v7, :cond_3f

    .line 273
    add-int/lit8 v0, v5, 0x20

    int-to-char v0, v0

    goto :goto_2f

    .line 275
    :cond_3f
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 280
    :sswitch_44
    if-lt v5, v6, :cond_4c

    if-gt v5, v7, :cond_4c

    .line 281
    add-int/lit8 v0, v5, -0x40

    int-to-char v0, v0

    goto :goto_2f

    .line 283
    :cond_4c
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 288
    :sswitch_51
    if-lt v5, v6, :cond_5b

    const/16 v0, 0x45

    if-gt v5, v0, :cond_5b

    .line 289
    add-int/lit8 v0, v5, -0x26

    int-to-char v0, v0

    goto :goto_2f

    .line 290
    :cond_5b
    const/16 v0, 0x46

    if-lt v5, v0, :cond_67

    const/16 v0, 0x57

    if-gt v5, v0, :cond_67

    .line 291
    add-int/lit8 v0, v5, -0xb

    int-to-char v0, v0

    goto :goto_2f

    .line 293
    :cond_67
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 298
    :sswitch_6c
    if-lt v5, v6, :cond_76

    const/16 v0, 0x4f

    if-gt v5, v0, :cond_76

    .line 299
    add-int/lit8 v0, v5, -0x20

    int-to-char v0, v0

    goto :goto_2f

    .line 300
    :cond_76
    if-ne v5, v7, :cond_7b

    .line 301
    const/16 v0, 0x3a

    goto :goto_2f

    .line 303
    :cond_7b
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 311
    :cond_80
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v2

    goto :goto_34

    .line 314
    :cond_85
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 269
    :sswitch_data_8a
    .sparse-switch
        0x24 -> :sswitch_44
        0x25 -> :sswitch_51
        0x2b -> :sswitch_37
        0x2f -> :sswitch_6c
    .end sparse-switch
.end method


# virtual methods
.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 99
    const/16 v0, 0x9

    new-array v4, v0, [I

    .line 100
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v5

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    array-length v6, v4

    move v3, v0

    :goto_11
    if-ge v3, v5, :cond_91

    invoke-virtual {p2, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v7

    xor-int/2addr v7, v2

    if-eqz v7, :cond_23

    aget v7, v4, v1

    add-int/lit8 v7, v7, 0x1

    aput v7, v4, v1

    :goto_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    :cond_23
    add-int/lit8 v7, v6, -0x1

    if-ne v1, v7, :cond_8c

    invoke-static {v4}, Lcom/google/zxing/oned/c;->a([I)I

    move-result v7

    sget v8, Lcom/google/zxing/oned/c;->c:I

    if-ne v7, v8, :cond_6a

    const/4 v7, 0x0

    sub-int v8, v3, v0

    shr-int/lit8 v8, v8, 0x1

    sub-int v8, v0, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    const/4 v8, 0x0

    invoke-virtual {p2, v7, v0, v8}, Lcom/google/zxing/common/a;->a(IIZ)Z

    move-result v7

    if-eqz v7, :cond_6a

    const/4 v1, 0x2

    new-array v5, v1, [I

    const/4 v1, 0x0

    aput v0, v5, v1

    const/4 v0, 0x1

    aput v3, v5, v0

    .line 102
    const/4 v0, 0x1

    aget v0, v5, v0

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    .line 103
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v6

    .line 105
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 109
    :goto_5c
    invoke-static {p2, v0, v4}, Lcom/google/zxing/oned/c;->a(Lcom/google/zxing/common/a;I[I)V

    .line 110
    invoke-static {v4}, Lcom/google/zxing/oned/c;->a([I)I

    move-result v1

    .line 111
    if-gez v1, :cond_96

    .line 112
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 100
    :cond_6a
    const/4 v7, 0x0

    aget v7, v4, v7

    const/4 v8, 0x1

    aget v8, v4, v8

    add-int/2addr v7, v8

    add-int/2addr v0, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    add-int/lit8 v9, v6, -0x2

    invoke-static {v4, v7, v4, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v7, v6, -0x2

    const/4 v8, 0x0

    aput v8, v4, v7

    add-int/lit8 v7, v6, -0x1

    const/4 v8, 0x0

    aput v8, v4, v7

    add-int/lit8 v1, v1, -0x1

    :goto_85
    const/4 v7, 0x1

    aput v7, v4, v1

    if-nez v2, :cond_8f

    const/4 v2, 0x1

    goto :goto_20

    :cond_8c
    add-int/lit8 v1, v1, 0x1

    goto :goto_85

    :cond_8f
    const/4 v2, 0x0

    goto :goto_20

    :cond_91
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 114
    :cond_96
    invoke-static {v1}, Lcom/google/zxing/oned/c;->a(I)C

    move-result v3

    .line 115
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 117
    array-length v8, v4

    const/4 v1, 0x0

    move v2, v0

    :goto_a0
    if-ge v1, v8, :cond_a8

    aget v9, v4, v1

    .line 118
    add-int/2addr v2, v9

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_a0

    .line 121
    :cond_a8
    invoke-virtual {p2, v2}, Lcom/google/zxing/common/a;->c(I)I

    move-result v1

    .line 122
    const/16 v2, 0x2a

    if-ne v3, v2, :cond_148

    .line 123
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 126
    const/4 v3, 0x0

    .line 127
    array-length v8, v4

    const/4 v2, 0x0

    :goto_bc
    if-ge v2, v8, :cond_c4

    aget v9, v4, v2

    .line 128
    add-int/2addr v3, v9

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_bc

    .line 130
    :cond_c4
    sub-int v2, v1, v0

    sub-int/2addr v2, v3

    .line 133
    if-eq v1, v6, :cond_d2

    shr-int/lit8 v2, v2, 0x1

    if-ge v2, v3, :cond_d2

    .line 134
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 137
    :cond_d2
    iget-boolean v2, p0, Lcom/google/zxing/oned/c;->d:Z

    if-eqz v2, :cond_102

    .line 138
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v4, v2, -0x1

    .line 139
    const/4 v3, 0x0

    .line 140
    const/4 v2, 0x0

    :goto_de
    if-ge v2, v4, :cond_ee

    .line 141
    const-string v6, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%"

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 140
    add-int/lit8 v2, v2, 0x1

    goto :goto_de

    .line 143
    :cond_ee
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    sget-object v6, Lcom/google/zxing/oned/c;->b:[C

    rem-int/lit8 v3, v3, 0x2b

    aget-char v3, v6, v3

    if-eq v2, v3, :cond_ff

    .line 144
    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    .line 146
    :cond_ff
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 149
    :cond_102
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_10d

    .line 151
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 155
    :cond_10d
    iget-boolean v2, p0, Lcom/google/zxing/oned/c;->e:Z

    if-eqz v2, :cond_143

    .line 156
    invoke-static {v7}, Lcom/google/zxing/oned/c;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 161
    :goto_115
    const/4 v3, 0x1

    aget v3, v5, v3

    const/4 v4, 0x0

    aget v4, v5, v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    .line 162
    add-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    .line 163
    new-instance v1, Lcom/google/zxing/f;

    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/zxing/g;

    const/4 v6, 0x0

    new-instance v7, Lcom/google/zxing/g;

    int-to-float v8, p1

    invoke-direct {v7, v3, v8}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v7, v5, v6

    const/4 v3, 0x1

    new-instance v6, Lcom/google/zxing/g;

    int-to-float v7, p1

    invoke-direct {v6, v0, v7}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v5, v3

    sget-object v0, Lcom/google/zxing/BarcodeFormat;->CODE_39:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v1

    .line 158
    :cond_143
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_115

    :cond_148
    move v0, v1

    goto/16 :goto_5c
.end method
