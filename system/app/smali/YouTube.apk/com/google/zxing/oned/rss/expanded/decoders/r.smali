.class final Lcom/google/zxing/oned/rss/expanded/decoders/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/a;

.field private final b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

.field private final c:Ljava/lang/StringBuilder;


# direct methods
.method constructor <init>(Lcom/google/zxing/common/a;)V
    .registers 3
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-direct {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    .line 43
    iput-object p1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    .line 44
    return-void
.end method

.method static a(Lcom/google/zxing/common/a;II)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 107
    const/16 v1, 0x20

    if-le p2, v1, :cond_d

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "extractNumberValueFromBitArray can\'t handle more than 32 bits"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    move v1, v0

    .line 112
    :goto_e
    if-ge v1, p2, :cond_22

    .line 113
    add-int v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 114
    const/4 v2, 0x1

    sub-int v3, p2, v1

    add-int/lit8 v3, v3, -0x1

    shl-int/2addr v2, v3

    or-int/2addr v0, v2

    .line 112
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 118
    :cond_22
    return v0
.end method

.method private a()Lcom/google/zxing/oned/rss/expanded/decoders/l;
    .registers 9

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 164
    :goto_5
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v3

    add-int/lit8 v0, v3, 0x7

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-le v0, v4, :cond_72

    add-int/lit8 v0, v3, 0x4

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v3}, Lcom/google/zxing/common/a;->a()I

    move-result v3

    if-gt v0, v3, :cond_70

    move v0, v2

    :goto_20
    if-eqz v0, :cond_f8

    .line 165
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v3

    add-int/lit8 v0, v3, 0x7

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-le v0, v4, :cond_9b

    invoke-virtual {p0, v3, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v3

    if-nez v3, :cond_8d

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/o;

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v3}, Lcom/google/zxing/common/a;->a()I

    move-result v3

    invoke-direct {v0, v3, v7, v7}, Lcom/google/zxing/oned/rss/expanded/decoders/o;-><init>(III)V

    .line 166
    :goto_43
    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->e()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a(I)V

    .line 168
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->c()Z

    move-result v3

    if-eqz v3, :cond_c7

    .line 170
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->d()Z

    move-result v1

    if-eqz v1, :cond_b0

    .line 171
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v1

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;)V

    .line 175
    :goto_69
    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v1, v0, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Lcom/google/zxing/oned/rss/expanded/decoders/n;Z)V

    move-object v0, v1

    .line 190
    :goto_6f
    return-object v0

    :cond_70
    move v0, v1

    .line 164
    goto :goto_20

    :cond_72
    move v0, v3

    :goto_73
    add-int/lit8 v4, v3, 0x3

    if-ge v0, v4, :cond_84

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v4

    if-eqz v4, :cond_81

    move v0, v2

    goto :goto_20

    :cond_81
    add-int/lit8 v0, v0, 0x1

    goto :goto_73

    :cond_84
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v0

    goto :goto_20

    .line 165
    :cond_8d
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/o;

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v0, v4, v3, v7}, Lcom/google/zxing/oned/rss/expanded/decoders/o;-><init>(III)V

    goto :goto_43

    :cond_9b
    const/4 v0, 0x7

    invoke-virtual {p0, v3, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v0

    add-int/lit8 v4, v0, -0x8

    div-int/lit8 v4, v4, 0xb

    add-int/lit8 v0, v0, -0x8

    rem-int/lit8 v5, v0, 0xb

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/o;

    add-int/lit8 v3, v3, 0x7

    invoke-direct {v0, v3, v4, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/o;-><init>(III)V

    goto :goto_43

    .line 173
    :cond_b0
    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v3}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->b()I

    move-result v0

    invoke-direct {v1, v3, v4, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;I)V

    move-object v0, v1

    goto :goto_69

    .line 177
    :cond_c7
    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->d()Z

    move-result v3

    if-eqz v3, :cond_ed

    .line 180
    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;)V

    .line 181
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Lcom/google/zxing/oned/rss/expanded/decoders/n;Z)V

    goto :goto_6f

    .line 183
    :cond_ed
    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/o;->b()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 186
    :cond_f8
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-le v0, v4, :cond_11c

    move v0, v1

    :goto_109
    if-eqz v0, :cond_115

    .line 187
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->e()V

    .line 188
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->b(I)V

    .line 190
    :cond_115
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v0, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Z)V

    goto/16 :goto_6f

    :cond_11c
    move v0, v1

    .line 186
    :goto_11d
    if-ge v0, v6, :cond_138

    add-int v4, v0, v3

    iget-object v5, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v5}, Lcom/google/zxing/common/a;->a()I

    move-result v5

    if-ge v4, v5, :cond_138

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    add-int v5, v3, v0

    invoke-virtual {v4, v5}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v4

    if-eqz v4, :cond_135

    move v0, v1

    goto :goto_109

    :cond_135
    add-int/lit8 v0, v0, 0x1

    goto :goto_11d

    :cond_138
    move v0, v2

    goto :goto_109
.end method

.method private a(I)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 427
    add-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v2}, Lcom/google/zxing/common/a;->a()I

    move-result v2

    if-le v1, v2, :cond_c

    .line 441
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v1, v0

    .line 431
    :goto_d
    const/4 v2, 0x5

    if-ge v1, v2, :cond_35

    add-int v2, v1, p1

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v3}, Lcom/google/zxing/common/a;->a()I

    move-result v3

    if-ge v2, v3, :cond_35

    .line 432
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2a

    .line 433
    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    add-int/lit8 v3, p1, 0x2

    invoke-virtual {v2, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 431
    :cond_27
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 436
    :cond_2a
    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    add-int v3, p1, v1

    invoke-virtual {v2, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_27

    goto :goto_b

    .line 441
    :cond_35
    const/4 v0, 0x1

    goto :goto_b
.end method

.method private b()Lcom/google/zxing/oned/rss/expanded/decoders/l;
    .registers 9

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x7

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x5

    .line 194
    :goto_6
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    add-int/lit8 v1, v0, 0x5

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-gt v1, v4, :cond_90

    invoke-virtual {p0, v0, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    if-lt v1, v5, :cond_60

    const/16 v4, 0x10

    if-ge v1, v4, :cond_60

    move v0, v2

    :goto_21
    if-eqz v0, :cond_140

    .line 195
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v4

    invoke-virtual {p0, v4, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    const/16 v0, 0xf

    if-ne v1, v0, :cond_92

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v1, v4, 0x5

    const/16 v4, 0x24

    invoke-direct {v0, v1, v4}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    .line 196
    :goto_3a
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;->e()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a(I)V

    .line 198
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;->b()Z

    move-result v1

    if-eqz v1, :cond_135

    .line 199
    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;)V

    .line 200
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Lcom/google/zxing/oned/rss/expanded/decoders/n;Z)V

    .line 217
    :goto_5f
    return-object v0

    .line 194
    :cond_60
    add-int/lit8 v1, v0, 0x7

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-gt v1, v4, :cond_90

    invoke-virtual {p0, v0, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    const/16 v4, 0x40

    if-lt v1, v4, :cond_78

    const/16 v4, 0x74

    if-ge v1, v4, :cond_78

    move v0, v2

    goto :goto_21

    :cond_78
    add-int/lit8 v1, v0, 0x8

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v4}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    if-gt v1, v4, :cond_90

    invoke-virtual {p0, v0, v7}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v0

    const/16 v1, 0xe8

    if-lt v0, v1, :cond_90

    const/16 v1, 0xfd

    if-ge v0, v1, :cond_90

    move v0, v2

    goto :goto_21

    :cond_90
    move v0, v3

    goto :goto_21

    .line 195
    :cond_92
    if-lt v1, v5, :cond_a5

    const/16 v0, 0xf

    if-ge v1, v0, :cond_a5

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v4, v4, 0x5

    add-int/lit8 v1, v1, 0x30

    add-int/lit8 v1, v1, -0x5

    int-to-char v1, v1

    invoke-direct {v0, v4, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    goto :goto_3a

    :cond_a5
    invoke-virtual {p0, v4, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    const/16 v0, 0x40

    if-lt v1, v0, :cond_bd

    const/16 v0, 0x5a

    if-ge v1, v0, :cond_bd

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v4, v4, 0x7

    add-int/lit8 v1, v1, 0x1

    int-to-char v1, v1

    invoke-direct {v0, v4, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    goto/16 :goto_3a

    :cond_bd
    const/16 v0, 0x5a

    if-lt v1, v0, :cond_d1

    const/16 v0, 0x74

    if-ge v1, v0, :cond_d1

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v4, v4, 0x7

    add-int/lit8 v1, v1, 0x7

    int-to-char v1, v1

    invoke-direct {v0, v4, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    goto/16 :goto_3a

    :cond_d1
    invoke-virtual {p0, v4, v7}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v0

    packed-switch v0, :pswitch_data_192

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Decoding invalid ISO/IEC 646 value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_ed
    const/16 v0, 0x21

    :goto_ef
    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v4, v4, 0x8

    invoke-direct {v1, v4, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    move-object v0, v1

    goto/16 :goto_3a

    :pswitch_f9
    const/16 v0, 0x22

    goto :goto_ef

    :pswitch_fc
    const/16 v0, 0x25

    goto :goto_ef

    :pswitch_ff
    const/16 v0, 0x26

    goto :goto_ef

    :pswitch_102
    const/16 v0, 0x27

    goto :goto_ef

    :pswitch_105
    const/16 v0, 0x28

    goto :goto_ef

    :pswitch_108
    const/16 v0, 0x29

    goto :goto_ef

    :pswitch_10b
    const/16 v0, 0x2a

    goto :goto_ef

    :pswitch_10e
    const/16 v0, 0x2b

    goto :goto_ef

    :pswitch_111
    const/16 v0, 0x2c

    goto :goto_ef

    :pswitch_114
    const/16 v0, 0x2d

    goto :goto_ef

    :pswitch_117
    const/16 v0, 0x2e

    goto :goto_ef

    :pswitch_11a
    const/16 v0, 0x2f

    goto :goto_ef

    :pswitch_11d
    const/16 v0, 0x3a

    goto :goto_ef

    :pswitch_120
    const/16 v0, 0x3b

    goto :goto_ef

    :pswitch_123
    const/16 v0, 0x3c

    goto :goto_ef

    :pswitch_126
    const/16 v0, 0x3d

    goto :goto_ef

    :pswitch_129
    const/16 v0, 0x3e

    goto :goto_ef

    :pswitch_12c
    const/16 v0, 0x3f

    goto :goto_ef

    :pswitch_12f
    const/16 v0, 0x5f

    goto :goto_ef

    :pswitch_132
    const/16 v0, 0x20

    goto :goto_ef

    .line 202
    :cond_135
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;->a()C

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 205
    :cond_140
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b(I)Z

    move-result v0

    if-eqz v0, :cond_15e

    .line 206
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->b(I)V

    .line 207
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->d()V

    .line 217
    :cond_157
    :goto_157
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v0, v3}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Z)V

    goto/16 :goto_5f

    .line 208
    :cond_15e
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(I)Z

    move-result v0

    if-eqz v0, :cond_157

    .line 209
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v1}, Lcom/google/zxing/common/a;->a()I

    move-result v1

    if-ge v0, v1, :cond_185

    .line 210
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->b(I)V

    .line 215
    :goto_17f
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->e()V

    goto :goto_157

    .line 212
    :cond_185
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v1}, Lcom/google/zxing/common/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a(I)V

    goto :goto_17f

    .line 195
    nop

    :pswitch_data_192
    .packed-switch 0xe8
        :pswitch_ed
        :pswitch_f9
        :pswitch_fc
        :pswitch_ff
        :pswitch_102
        :pswitch_105
        :pswitch_108
        :pswitch_10b
        :pswitch_10e
        :pswitch_111
        :pswitch_114
        :pswitch_117
        :pswitch_11a
        :pswitch_11d
        :pswitch_120
        :pswitch_123
        :pswitch_126
        :pswitch_129
        :pswitch_12c
        :pswitch_12f
        :pswitch_132
    .end packed-switch
.end method

.method private b(I)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 446
    add-int/lit8 v1, p1, 0x3

    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v2}, Lcom/google/zxing/common/a;->a()I

    move-result v2

    if-le v1, v2, :cond_c

    .line 455
    :cond_b
    :goto_b
    return v0

    :cond_c
    move v1, p1

    .line 450
    :goto_d
    add-int/lit8 v2, p1, 0x3

    if-ge v1, v2, :cond_1c

    .line 451
    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v2, v1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-nez v2, :cond_b

    .line 450
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 455
    :cond_1c
    const/4 v0, 0x1

    goto :goto_b
.end method


# virtual methods
.method final a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-static {v0, p1, p2}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(Lcom/google/zxing/common/a;II)I

    move-result v0

    return v0
.end method

.method final a(ILjava/lang/String;)Lcom/google/zxing/oned/rss/expanded/decoders/n;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/16 v8, 0xf

    const/4 v7, 0x6

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x5

    .line 122
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 124
    if-eqz p2, :cond_12

    .line 125
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    :cond_12
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0, p1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a(I)V

    .line 130
    :cond_17
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v2

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->b()Z

    move-result v0

    if-eqz v0, :cond_183

    :goto_25
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    add-int/lit8 v1, v0, 0x5

    iget-object v5, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v5}, Lcom/google/zxing/common/a;->a()I

    move-result v5

    if-gt v1, v5, :cond_ca

    invoke-virtual {p0, v0, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    if-lt v1, v6, :cond_b1

    const/16 v5, 0x10

    if-ge v1, v5, :cond_b1

    move v0, v3

    :goto_40
    if-eqz v0, :cond_132

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v5

    invoke-virtual {p0, v5, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    if-ne v1, v8, :cond_cd

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v1, v5, 0x5

    const/16 v5, 0x24

    invoke-direct {v0, v1, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    :goto_57
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;->e()I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a(I)V

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;->b()Z

    move-result v1

    if-eqz v1, :cond_127

    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    iget-object v5, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v5}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;)V

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v0, v1, v3}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Lcom/google/zxing/oned/rss/expanded/decoders/n;Z)V

    :goto_7c
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/l;->b()Z

    move-result v1

    :goto_80
    iget-object v5, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v5}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v5

    if-eq v2, v5, :cond_19f

    move v2, v3

    :goto_89
    if-nez v2, :cond_8d

    if-eqz v1, :cond_8f

    :cond_8d
    if-eqz v1, :cond_17

    :cond_8f
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/l;->a()Lcom/google/zxing/oned/rss/expanded/decoders/n;

    move-result-object v1

    .line 131
    if-eqz v1, :cond_1a2

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->b()Z

    move-result v0

    if-eqz v0, :cond_1a2

    .line 132
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v2

    iget-object v3, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->c()I

    move-result v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;I)V

    .line 134
    :goto_b0
    return-object v0

    .line 130
    :cond_b1
    add-int/lit8 v1, v0, 0x6

    iget-object v5, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v5}, Lcom/google/zxing/common/a;->a()I

    move-result v5

    if-gt v1, v5, :cond_ca

    invoke-virtual {p0, v0, v7}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_ca

    const/16 v1, 0x3f

    if-ge v0, v1, :cond_ca

    move v0, v3

    goto/16 :goto_40

    :cond_ca
    move v0, v4

    goto/16 :goto_40

    :cond_cd
    if-lt v1, v6, :cond_df

    if-ge v1, v8, :cond_df

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v5, v5, 0x5

    add-int/lit8 v1, v1, 0x30

    add-int/lit8 v1, v1, -0x5

    int-to-char v1, v1

    invoke-direct {v0, v5, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    goto/16 :goto_57

    :cond_df
    invoke-virtual {p0, v5, v7}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(II)I

    move-result v1

    const/16 v0, 0x20

    if-lt v1, v0, :cond_f7

    const/16 v0, 0x3a

    if-ge v1, v0, :cond_f7

    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v5, v5, 0x6

    add-int/lit8 v1, v1, 0x21

    int-to-char v1, v1

    invoke-direct {v0, v5, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    goto/16 :goto_57

    :cond_f7
    packed-switch v1, :pswitch_data_1b6

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Decoding invalid alphanumeric value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_10f
    const/16 v0, 0x2a

    :goto_111
    new-instance v1, Lcom/google/zxing/oned/rss/expanded/decoders/m;

    add-int/lit8 v5, v5, 0x6

    invoke-direct {v1, v5, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;-><init>(IC)V

    move-object v0, v1

    goto/16 :goto_57

    :pswitch_11b
    const/16 v0, 0x2c

    goto :goto_111

    :pswitch_11e
    const/16 v0, 0x2d

    goto :goto_111

    :pswitch_121
    const/16 v0, 0x2e

    goto :goto_111

    :pswitch_124
    const/16 v0, 0x2f

    goto :goto_111

    :cond_127
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/m;->a()C

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_25

    :cond_132
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b(I)Z

    move-result v0

    if-eqz v0, :cond_150

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->b(I)V

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->d()V

    :cond_149
    :goto_149
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/l;

    invoke-direct {v0, v4}, Lcom/google/zxing/oned/rss/expanded/decoders/l;-><init>(Z)V

    goto/16 :goto_7c

    :cond_150
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(I)Z

    move-result v0

    if-eqz v0, :cond_149

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v1}, Lcom/google/zxing/common/a;->a()I

    move-result v1

    if-ge v0, v1, :cond_177

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0, v6}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->b(I)V

    :goto_171
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->f()V

    goto :goto_149

    :cond_177
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a:Lcom/google/zxing/common/a;

    invoke-virtual {v1}, Lcom/google/zxing/common/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a(I)V

    goto :goto_171

    :cond_183
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->c()Z

    move-result v0

    if-eqz v0, :cond_195

    invoke-direct {p0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b()Lcom/google/zxing/oned/rss/expanded/decoders/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/l;->b()Z

    move-result v1

    goto/16 :goto_80

    :cond_195
    invoke-direct {p0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a()Lcom/google/zxing/oned/rss/expanded/decoders/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/l;->b()Z

    move-result v1

    goto/16 :goto_80

    :cond_19f
    move v2, v4

    goto/16 :goto_89

    .line 134
    :cond_1a2
    new-instance v0, Lcom/google/zxing/oned/rss/expanded/decoders/n;

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->b:Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/expanded/decoders/CurrentParsingState;->a()I

    move-result v1

    iget-object v2, p0, Lcom/google/zxing/oned/rss/expanded/decoders/r;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/oned/rss/expanded/decoders/n;-><init>(ILjava/lang/String;)V

    goto/16 :goto_b0

    .line 130
    nop

    :pswitch_data_1b6
    .packed-switch 0x3a
        :pswitch_10f
        :pswitch_11b
        :pswitch_11e
        :pswitch_121
        :pswitch_124
    .end packed-switch
.end method

.method final a(Ljava/lang/StringBuilder;I)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    move-object v0, v1

    .line 50
    :goto_2
    invoke-virtual {p0, p2, v0}, Lcom/google/zxing/oned/rss/expanded/decoders/r;->a(ILjava/lang/String;)Lcom/google/zxing/oned/rss/expanded/decoders/n;

    move-result-object v2

    .line 51
    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_13

    .line 53
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    :cond_13
    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->b()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 56
    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_21
    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->e()I

    move-result v3

    if-eq p2, v3, :cond_2e

    .line 62
    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/expanded/decoders/n;->e()I

    move-result p2

    goto :goto_2

    :cond_2c
    move-object v0, v1

    .line 58
    goto :goto_21

    .line 67
    :cond_2e
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
