.class public final Lcom/google/zxing/oned/rss/expanded/b;
.super Lcom/google/zxing/oned/rss/a;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[[I

.field private static final e:[[I

.field private static final f:[[I

.field private static final g:I


# instance fields
.field private final h:Ljava/util/List;

.field private final i:[I

.field private final j:[I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/16 v3, 0x8

    .line 51
    new-array v0, v5, [I

    fill-array-data v0, :array_178

    sput-object v0, Lcom/google/zxing/oned/rss/expanded/b;->a:[I

    .line 52
    new-array v0, v5, [I

    fill-array-data v0, :array_186

    sput-object v0, Lcom/google/zxing/oned/rss/expanded/b;->b:[I

    .line 53
    new-array v0, v5, [I

    fill-array-data v0, :array_194

    sput-object v0, Lcom/google/zxing/oned/rss/expanded/b;->c:[I

    .line 55
    const/4 v0, 0x6

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v4, [I

    fill-array-data v2, :array_1a2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v4, [I

    fill-array-data v2, :array_1ae

    aput-object v2, v0, v1

    new-array v1, v4, [I

    fill-array-data v1, :array_1ba

    aput-object v1, v0, v6

    new-array v1, v4, [I

    fill-array-data v1, :array_1c6

    aput-object v1, v0, v7

    new-array v1, v4, [I

    fill-array-data v1, :array_1d2

    aput-object v1, v0, v4

    new-array v1, v4, [I

    fill-array-data v1, :array_1de

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/zxing/oned/rss/expanded/b;->d:[[I

    .line 64
    const/16 v0, 0x17

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_1ea

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1fe

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_212

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_226

    aput-object v1, v0, v7

    new-array v1, v3, [I

    fill-array-data v1, :array_23a

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_24e

    aput-object v1, v0, v5

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_262

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_276

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_28a

    aput-object v1, v0, v3

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_29e

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_2b2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_2c6

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [I

    fill-array-data v2, :array_2da

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [I

    fill-array-data v2, :array_2ee

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [I

    fill-array-data v2, :array_302

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [I

    fill-array-data v2, :array_316

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [I

    fill-array-data v2, :array_32a

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [I

    fill-array-data v2, :array_33e

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [I

    fill-array-data v2, :array_352

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [I

    fill-array-data v2, :array_366

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [I

    fill-array-data v2, :array_37a

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [I

    fill-array-data v2, :array_38e

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [I

    fill-array-data v2, :array_3a2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/oned/rss/expanded/b;->e:[[I

    .line 97
    const/16 v0, 0xa

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v6, [I

    fill-array-data v2, :array_3b6

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v7, [I

    fill-array-data v2, :array_3be

    aput-object v2, v0, v1

    new-array v1, v4, [I

    fill-array-data v1, :array_3c8

    aput-object v1, v0, v6

    new-array v1, v5, [I

    fill-array-data v1, :array_3d4

    aput-object v1, v0, v7

    const/4 v1, 0x6

    new-array v1, v1, [I

    fill-array-data v1, :array_3e2

    aput-object v1, v0, v4

    const/4 v1, 0x7

    new-array v1, v1, [I

    fill-array-data v1, :array_3f2

    aput-object v1, v0, v5

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_404

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x9

    new-array v2, v2, [I

    fill-array-data v2, :array_418

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    fill-array-data v1, :array_42e

    aput-object v1, v0, v3

    const/16 v1, 0x9

    const/16 v2, 0xb

    new-array v2, v2, [I

    fill-array-data v2, :array_446

    aput-object v2, v0, v1

    .line 110
    sput-object v0, Lcom/google/zxing/oned/rss/expanded/b;->f:[[I

    sget-object v1, Lcom/google/zxing/oned/rss/expanded/b;->f:[[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    array-length v0, v0

    sput v0, Lcom/google/zxing/oned/rss/expanded/b;->g:I

    return-void

    .line 51
    :array_178
    .array-data 0x4
        0x7t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    .line 52
    :array_186
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x14t 0x0t 0x0t 0x0t
        0x34t 0x0t 0x0t 0x0t
        0x68t 0x0t 0x0t 0x0t
        0xcct 0x0t 0x0t 0x0t
    .end array-data

    .line 53
    :array_194
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x5ct 0x1t 0x0t 0x0t
        0x6ct 0x5t 0x0t 0x0t
        0x84t 0xbt 0x0t 0x0t
        0x94t 0xft 0x0t 0x0t
    .end array-data

    .line 55
    :array_1a2
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_1ae
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_1ba
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_1c6
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_1d2
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_1de
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    .line 64
    :array_1ea
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x1bt 0x0t 0x0t 0x0t
        0x51t 0x0t 0x0t 0x0t
        0x20t 0x0t 0x0t 0x0t
        0x60t 0x0t 0x0t 0x0t
        0x4dt 0x0t 0x0t 0x0t
    .end array-data

    :array_1fe
    .array-data 0x4
        0x14t 0x0t 0x0t 0x0t
        0x3ct 0x0t 0x0t 0x0t
        0xb4t 0x0t 0x0t 0x0t
        0x76t 0x0t 0x0t 0x0t
        0x8ft 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x15t 0x0t 0x0t 0x0t
        0x3ft 0x0t 0x0t 0x0t
    .end array-data

    :array_212
    .array-data 0x4
        0xbdt 0x0t 0x0t 0x0t
        0x91t 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0x27t 0x0t 0x0t 0x0t
        0x75t 0x0t 0x0t 0x0t
        0x8ct 0x0t 0x0t 0x0t
        0xd1t 0x0t 0x0t 0x0t
        0xcdt 0x0t 0x0t 0x0t
    .end array-data

    :array_226
    .array-data 0x4
        0xc1t 0x0t 0x0t 0x0t
        0x9dt 0x0t 0x0t 0x0t
        0x31t 0x0t 0x0t 0x0t
        0x93t 0x0t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
        0x39t 0x0t 0x0t 0x0t
        0xabt 0x0t 0x0t 0x0t
        0x5bt 0x0t 0x0t 0x0t
    .end array-data

    :array_23a
    .array-data 0x4
        0x3et 0x0t 0x0t 0x0t
        0xbat 0x0t 0x0t 0x0t
        0x88t 0x0t 0x0t 0x0t
        0xc5t 0x0t 0x0t 0x0t
        0xa9t 0x0t 0x0t 0x0t
        0x55t 0x0t 0x0t 0x0t
        0x2ct 0x0t 0x0t 0x0t
        0x84t 0x0t 0x0t 0x0t
    .end array-data

    :array_24e
    .array-data 0x4
        0xb9t 0x0t 0x0t 0x0t
        0x85t 0x0t 0x0t 0x0t
        0xbct 0x0t 0x0t 0x0t
        0x8et 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0xct 0x0t 0x0t 0x0t
        0x24t 0x0t 0x0t 0x0t
        0x6ct 0x0t 0x0t 0x0t
    .end array-data

    :array_262
    .array-data 0x4
        0x71t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
        0xadt 0x0t 0x0t 0x0t
        0x61t 0x0t 0x0t 0x0t
        0x50t 0x0t 0x0t 0x0t
        0x1dt 0x0t 0x0t 0x0t
        0x57t 0x0t 0x0t 0x0t
        0x32t 0x0t 0x0t 0x0t
    .end array-data

    :array_276
    .array-data 0x4
        0x96t 0x0t 0x0t 0x0t
        0x1ct 0x0t 0x0t 0x0t
        0x54t 0x0t 0x0t 0x0t
        0x29t 0x0t 0x0t 0x0t
        0x7bt 0x0t 0x0t 0x0t
        0x9et 0x0t 0x0t 0x0t
        0x34t 0x0t 0x0t 0x0t
        0x9ct 0x0t 0x0t 0x0t
    .end array-data

    :array_28a
    .array-data 0x4
        0x2et 0x0t 0x0t 0x0t
        0x8at 0x0t 0x0t 0x0t
        0xcbt 0x0t 0x0t 0x0t
        0xbbt 0x0t 0x0t 0x0t
        0x8bt 0x0t 0x0t 0x0t
        0xcet 0x0t 0x0t 0x0t
        0xc4t 0x0t 0x0t 0x0t
        0xa6t 0x0t 0x0t 0x0t
    .end array-data

    :array_29e
    .array-data 0x4
        0x4ct 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0x33t 0x0t 0x0t 0x0t
        0x99t 0x0t 0x0t 0x0t
        0x25t 0x0t 0x0t 0x0t
        0x6ft 0x0t 0x0t 0x0t
        0x7at 0x0t 0x0t 0x0t
        0x9bt 0x0t 0x0t 0x0t
    .end array-data

    :array_2b2
    .array-data 0x4
        0x2bt 0x0t 0x0t 0x0t
        0x81t 0x0t 0x0t 0x0t
        0xb0t 0x0t 0x0t 0x0t
        0x6at 0x0t 0x0t 0x0t
        0x6bt 0x0t 0x0t 0x0t
        0x6et 0x0t 0x0t 0x0t
        0x77t 0x0t 0x0t 0x0t
        0x92t 0x0t 0x0t 0x0t
    .end array-data

    :array_2c6
    .array-data 0x4
        0x10t 0x0t 0x0t 0x0t
        0x30t 0x0t 0x0t 0x0t
        0x90t 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
        0x1et 0x0t 0x0t 0x0t
        0x5at 0x0t 0x0t 0x0t
        0x3bt 0x0t 0x0t 0x0t
        0xb1t 0x0t 0x0t 0x0t
    .end array-data

    :array_2da
    .array-data 0x4
        0x6dt 0x0t 0x0t 0x0t
        0x74t 0x0t 0x0t 0x0t
        0x89t 0x0t 0x0t 0x0t
        0xc8t 0x0t 0x0t 0x0t
        0xb2t 0x0t 0x0t 0x0t
        0x70t 0x0t 0x0t 0x0t
        0x7dt 0x0t 0x0t 0x0t
        0xa4t 0x0t 0x0t 0x0t
    .end array-data

    :array_2ee
    .array-data 0x4
        0x46t 0x0t 0x0t 0x0t
        0xd2t 0x0t 0x0t 0x0t
        0xd0t 0x0t 0x0t 0x0t
        0xcat 0x0t 0x0t 0x0t
        0xb8t 0x0t 0x0t 0x0t
        0x82t 0x0t 0x0t 0x0t
        0xb3t 0x0t 0x0t 0x0t
        0x73t 0x0t 0x0t 0x0t
    .end array-data

    :array_302
    .array-data 0x4
        0x86t 0x0t 0x0t 0x0t
        0xbft 0x0t 0x0t 0x0t
        0x97t 0x0t 0x0t 0x0t
        0x1ft 0x0t 0x0t 0x0t
        0x5dt 0x0t 0x0t 0x0t
        0x44t 0x0t 0x0t 0x0t
        0xcct 0x0t 0x0t 0x0t
        0xbet 0x0t 0x0t 0x0t
    .end array-data

    :array_316
    .array-data 0x4
        0x94t 0x0t 0x0t 0x0t
        0x16t 0x0t 0x0t 0x0t
        0x42t 0x0t 0x0t 0x0t
        0xc6t 0x0t 0x0t 0x0t
        0xact 0x0t 0x0t 0x0t
        0x5et 0x0t 0x0t 0x0t
        0x47t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_32a
    .array-data 0x4
        0x6t 0x0t 0x0t 0x0t
        0x12t 0x0t 0x0t 0x0t
        0x36t 0x0t 0x0t 0x0t
        0xa2t 0x0t 0x0t 0x0t
        0x40t 0x0t 0x0t 0x0t
        0xc0t 0x0t 0x0t 0x0t
        0x9at 0x0t 0x0t 0x0t
        0x28t 0x0t 0x0t 0x0t
    .end array-data

    :array_33e
    .array-data 0x4
        0x78t 0x0t 0x0t 0x0t
        0x95t 0x0t 0x0t 0x0t
        0x19t 0x0t 0x0t 0x0t
        0x4bt 0x0t 0x0t 0x0t
        0xet 0x0t 0x0t 0x0t
        0x2at 0x0t 0x0t 0x0t
        0x7et 0x0t 0x0t 0x0t
        0xa7t 0x0t 0x0t 0x0t
    .end array-data

    :array_352
    .array-data 0x4
        0x4ft 0x0t 0x0t 0x0t
        0x1at 0x0t 0x0t 0x0t
        0x4et 0x0t 0x0t 0x0t
        0x17t 0x0t 0x0t 0x0t
        0x45t 0x0t 0x0t 0x0t
        0xcft 0x0t 0x0t 0x0t
        0xc7t 0x0t 0x0t 0x0t
        0xaft 0x0t 0x0t 0x0t
    .end array-data

    :array_366
    .array-data 0x4
        0x67t 0x0t 0x0t 0x0t
        0x62t 0x0t 0x0t 0x0t
        0x53t 0x0t 0x0t 0x0t
        0x26t 0x0t 0x0t 0x0t
        0x72t 0x0t 0x0t 0x0t
        0x83t 0x0t 0x0t 0x0t
        0xb6t 0x0t 0x0t 0x0t
        0x7ct 0x0t 0x0t 0x0t
    .end array-data

    :array_37a
    .array-data 0x4
        0xa1t 0x0t 0x0t 0x0t
        0x3dt 0x0t 0x0t 0x0t
        0xb7t 0x0t 0x0t 0x0t
        0x7ft 0x0t 0x0t 0x0t
        0xaat 0x0t 0x0t 0x0t
        0x58t 0x0t 0x0t 0x0t
        0x35t 0x0t 0x0t 0x0t
        0x9ft 0x0t 0x0t 0x0t
    .end array-data

    :array_38e
    .array-data 0x4
        0x37t 0x0t 0x0t 0x0t
        0xa5t 0x0t 0x0t 0x0t
        0x49t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x18t 0x0t 0x0t 0x0t
        0x48t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0xft 0x0t 0x0t 0x0t
    .end array-data

    :array_3a2
    .array-data 0x4
        0x2dt 0x0t 0x0t 0x0t
        0x87t 0x0t 0x0t 0x0t
        0xc2t 0x0t 0x0t 0x0t
        0xa0t 0x0t 0x0t 0x0t
        0x3at 0x0t 0x0t 0x0t
        0xaet 0x0t 0x0t 0x0t
        0x64t 0x0t 0x0t 0x0t
        0x59t 0x0t 0x0t 0x0t
    .end array-data

    .line 97
    :array_3b6
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    :array_3be
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_3c8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    :array_3d4
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    :array_3e2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    :array_3f2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    :array_404
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    :array_418
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data

    :array_42e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data

    :array_446
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/zxing/oned/rss/a;-><init>()V

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    .line 115
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    .line 116
    sget v0, Lcom/google/zxing/oned/rss/expanded/b;->g:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->j:[I

    return-void
.end method

.method private a(Lcom/google/zxing/common/a;Lcom/google/zxing/oned/rss/c;ZZ)Lcom/google/zxing/oned/rss/b;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 396
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->c()[I

    move-result-object v2

    .line 397
    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 398
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 399
    const/4 v0, 0x2

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 400
    const/4 v0, 0x3

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 401
    const/4 v0, 0x4

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 402
    const/4 v0, 0x5

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 403
    const/4 v0, 0x6

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 404
    const/4 v0, 0x7

    const/4 v1, 0x0

    aput v1, v2, v0

    .line 406
    if-eqz p4, :cond_6c

    .line 407
    invoke-virtual {p2}, Lcom/google/zxing/oned/rss/c;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {p1, v0, v2}, Lcom/google/zxing/oned/rss/expanded/b;->b(Lcom/google/zxing/common/a;I[I)V

    .line 418
    :cond_30
    invoke-static {v2}, Lcom/google/zxing/oned/rss/expanded/b;->a([I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x4188

    div-float v3, v0, v1

    .line 421
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->f()[I

    move-result-object v6

    .line 422
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->g()[I

    move-result-object v7

    .line 423
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->d()[F

    move-result-object v4

    .line 424
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->e()[F

    move-result-object v5

    .line 426
    const/4 v0, 0x0

    :goto_4a
    array-length v1, v2

    if-ge v0, v1, :cond_9a

    .line 427
    const/high16 v1, 0x3f80

    aget v8, v2, v0

    int-to-float v8, v8

    mul-float/2addr v1, v8

    div-float v8, v1, v3

    .line 428
    const/high16 v1, 0x3f00

    add-float/2addr v1, v8

    float-to-int v1, v1

    .line 429
    if-gtz v1, :cond_8b

    .line 430
    const/4 v1, 0x1

    .line 434
    :cond_5c
    :goto_5c
    shr-int/lit8 v9, v0, 0x1

    .line 435
    and-int/lit8 v10, v0, 0x1

    if-nez v10, :cond_92

    .line 436
    aput v1, v6, v9

    .line 437
    int-to-float v1, v1

    sub-float v1, v8, v1

    aput v1, v4, v9

    .line 426
    :goto_69
    add-int/lit8 v0, v0, 0x1

    goto :goto_4a

    .line 409
    :cond_6c
    invoke-virtual {p2}, Lcom/google/zxing/oned/rss/c;->b()[I

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0, v2}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/common/a;I[I)V

    .line 411
    const/4 v1, 0x0

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    :goto_7c
    if-ge v1, v0, :cond_30

    .line 412
    aget v3, v2, v1

    .line 413
    aget v4, v2, v0

    aput v4, v2, v1

    .line 414
    aput v3, v2, v0

    .line 411
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_7c

    .line 431
    :cond_8b
    const/16 v9, 0x8

    if-le v1, v9, :cond_5c

    .line 432
    const/16 v1, 0x8

    goto :goto_5c

    .line 439
    :cond_92
    aput v1, v7, v9

    .line 440
    int-to-float v1, v1

    sub-float v1, v8, v1

    aput v1, v5, v9

    goto :goto_69

    .line 444
    :cond_9a
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->f()[I

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/oned/rss/expanded/b;->a([I)I

    move-result v8

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->g()[I

    move-result-object v0

    invoke-static {v0}, Lcom/google/zxing/oned/rss/expanded/b;->a([I)I

    move-result v9

    add-int v0, v8, v9

    add-int/lit8 v10, v0, -0x11

    and-int/lit8 v0, v8, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d5

    const/4 v0, 0x1

    move v5, v0

    :goto_b5
    and-int/lit8 v0, v9, 0x1

    if-nez v0, :cond_d8

    const/4 v0, 0x1

    move v4, v0

    :goto_bb
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v0, 0xd

    if-le v8, v0, :cond_db

    const/4 v2, 0x1

    :cond_c2
    :goto_c2
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/16 v11, 0xd

    if-le v9, v11, :cond_e0

    const/4 v0, 0x1

    :cond_c9
    :goto_c9
    const/4 v11, 0x1

    if-ne v10, v11, :cond_f8

    if-eqz v5, :cond_ef

    if-eqz v4, :cond_e5

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_d5
    const/4 v0, 0x0

    move v5, v0

    goto :goto_b5

    :cond_d8
    const/4 v0, 0x0

    move v4, v0

    goto :goto_bb

    :cond_db
    const/4 v0, 0x4

    if-ge v8, v0, :cond_c2

    const/4 v3, 0x1

    goto :goto_c2

    :cond_e0
    const/4 v11, 0x4

    if-ge v9, v11, :cond_c9

    const/4 v1, 0x1

    goto :goto_c9

    :cond_e5
    const/4 v2, 0x1

    :cond_e6
    :goto_e6
    if-eqz v3, :cond_139

    if-eqz v2, :cond_12e

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_ef
    if-nez v4, :cond_f6

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_f6
    const/4 v0, 0x1

    goto :goto_e6

    :cond_f8
    const/4 v11, -0x1

    if-ne v10, v11, :cond_10f

    if-eqz v5, :cond_106

    if-eqz v4, :cond_104

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_104
    const/4 v3, 0x1

    goto :goto_e6

    :cond_106
    if-nez v4, :cond_10d

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_10d
    const/4 v1, 0x1

    goto :goto_e6

    :cond_10f
    if-nez v10, :cond_129

    if-eqz v5, :cond_122

    if-nez v4, :cond_11a

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_11a
    if-ge v8, v9, :cond_11f

    const/4 v3, 0x1

    const/4 v0, 0x1

    goto :goto_e6

    :cond_11f
    const/4 v2, 0x1

    const/4 v1, 0x1

    goto :goto_e6

    :cond_122
    if-eqz v4, :cond_e6

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_129
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_12e
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->f()[I

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->d()[F

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/zxing/oned/rss/expanded/b;->a([I[F)V

    :cond_139
    if-eqz v2, :cond_146

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->f()[I

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->d()[F

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/zxing/oned/rss/expanded/b;->b([I[F)V

    :cond_146
    if-eqz v1, :cond_15a

    if-eqz v0, :cond_14f

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_14f
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->g()[I

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->d()[F

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/zxing/oned/rss/expanded/b;->a([I[F)V

    :cond_15a
    if-eqz v0, :cond_167

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->g()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->e()[F

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/zxing/oned/rss/expanded/b;->b([I[F)V

    .line 446
    :cond_167
    invoke-virtual {p2}, Lcom/google/zxing/oned/rss/c;->a()I

    move-result v0

    mul-int/lit8 v1, v0, 0x4

    if-eqz p3, :cond_19b

    const/4 v0, 0x0

    :goto_170
    add-int/2addr v1, v0

    if-eqz p4, :cond_19d

    const/4 v0, 0x0

    :goto_174
    add-int/2addr v0, v1

    add-int/lit8 v4, v0, -0x1

    .line 448
    const/4 v2, 0x0

    .line 449
    const/4 v1, 0x0

    .line 450
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    move v3, v2

    move v12, v1

    move v1, v0

    move v0, v12

    :goto_180
    if-ltz v1, :cond_19f

    .line 451
    invoke-static/range {p2 .. p4}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/oned/rss/c;ZZ)Z

    move-result v2

    if-eqz v2, :cond_194

    .line 452
    sget-object v2, Lcom/google/zxing/oned/rss/expanded/b;->e:[[I

    aget-object v2, v2, v4

    mul-int/lit8 v5, v1, 0x2

    aget v2, v2, v5

    .line 453
    aget v5, v6, v1

    mul-int/2addr v2, v5

    add-int/2addr v0, v2

    .line 455
    :cond_194
    aget v2, v6, v1

    add-int/2addr v2, v3

    .line 450
    add-int/lit8 v1, v1, -0x1

    move v3, v2

    goto :goto_180

    .line 446
    :cond_19b
    const/4 v0, 0x2

    goto :goto_170

    :cond_19d
    const/4 v0, 0x1

    goto :goto_174

    .line 457
    :cond_19f
    const/4 v1, 0x0

    .line 458
    array-length v2, v7

    add-int/lit8 v2, v2, -0x1

    :goto_1a3
    if-ltz v2, :cond_1bc

    .line 460
    invoke-static/range {p2 .. p4}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/oned/rss/c;ZZ)Z

    move-result v5

    if-eqz v5, :cond_1b9

    .line 461
    sget-object v5, Lcom/google/zxing/oned/rss/expanded/b;->e:[[I

    aget-object v5, v5, v4

    mul-int/lit8 v8, v2, 0x2

    add-int/lit8 v8, v8, 0x1

    aget v5, v5, v8

    .line 462
    aget v8, v7, v2

    mul-int/2addr v5, v8

    add-int/2addr v1, v5

    .line 464
    :cond_1b9
    add-int/lit8 v2, v2, -0x1

    goto :goto_1a3

    .line 466
    :cond_1bc
    add-int/2addr v0, v1

    .line 468
    and-int/lit8 v1, v3, 0x1

    if-nez v1, :cond_1c8

    const/16 v1, 0xd

    if-gt v3, v1, :cond_1c8

    const/4 v1, 0x4

    if-ge v3, v1, :cond_1cd

    .line 469
    :cond_1c8
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 472
    :cond_1cd
    rsub-int/lit8 v1, v3, 0xd

    div-int/lit8 v1, v1, 0x2

    .line 473
    sget-object v2, Lcom/google/zxing/oned/rss/expanded/b;->a:[I

    aget v2, v2, v1

    .line 474
    rsub-int/lit8 v3, v2, 0x9

    .line 475
    const/4 v4, 0x1

    invoke-static {v6, v2, v4}, Lcom/google/zxing/oned/rss/f;->a([IIZ)I

    move-result v2

    .line 476
    const/4 v4, 0x0

    invoke-static {v7, v3, v4}, Lcom/google/zxing/oned/rss/f;->a([IIZ)I

    move-result v3

    .line 477
    sget-object v4, Lcom/google/zxing/oned/rss/expanded/b;->b:[I

    aget v4, v4, v1

    .line 478
    sget-object v5, Lcom/google/zxing/oned/rss/expanded/b;->c:[I

    aget v1, v5, v1

    .line 479
    mul-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 481
    new-instance v2, Lcom/google/zxing/oned/rss/b;

    invoke-direct {v2, v1, v0}, Lcom/google/zxing/oned/rss/b;-><init>(II)V

    return-object v2
.end method

.method private a(Lcom/google/zxing/common/a;IZ)Lcom/google/zxing/oned/rss/c;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 353
    if-eqz p3, :cond_40

    .line 356
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    aget v0, v0, v5

    add-int/lit8 v0, v0, -0x1

    .line 358
    :goto_a
    if-ltz v0, :cond_15

    invoke-virtual {p1, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v1

    if-nez v1, :cond_15

    .line 359
    add-int/lit8 v0, v0, -0x1

    goto :goto_a

    .line 362
    :cond_15
    add-int/lit8 v3, v0, 0x1

    .line 363
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    aget v0, v0, v5

    sub-int/2addr v0, v3

    .line 365
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    aget v4, v1, v6

    .line 379
    :goto_20
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->b()[I

    move-result-object v1

    .line 380
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v5, v1, v6, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 382
    aput v0, v1, v5

    .line 385
    :try_start_2c
    sget-object v0, Lcom/google/zxing/oned/rss/expanded/b;->d:[[I

    invoke-static {v1, v0}, Lcom/google/zxing/oned/rss/expanded/b;->a([I[[I)I
    :try_end_31
    .catch Lcom/google/zxing/NotFoundException; {:try_start_2c .. :try_end_31} :catch_55

    move-result v1

    .line 389
    new-instance v0, Lcom/google/zxing/oned/rss/c;

    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v3, v2, v5

    aput v4, v2, v6

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/zxing/oned/rss/c;-><init>(I[IIII)V

    :goto_3f
    return-object v0

    .line 370
    :cond_40
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    aget v3, v0, v5

    .line 372
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    aget v0, v0, v6

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/zxing/common/a;->d(I)I

    move-result v4

    .line 375
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    aget v0, v0, v6

    sub-int v0, v4, v0

    goto :goto_20

    .line 387
    :catch_55
    move-exception v0

    const/4 v0, 0x0

    goto :goto_3f
.end method

.method private a(Lcom/google/zxing/common/a;Ljava/util/List;I)Lcom/google/zxing/oned/rss/expanded/a;
    .registers 18
    .parameter
    .parameter
    .parameter

    .prologue
    .line 207
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_43

    const/4 v1, 0x1

    move v2, v1

    .line 211
    :goto_a
    const/4 v3, 0x1

    .line 212
    const/4 v1, -0x1

    move v4, v3

    move v3, v1

    .line 214
    :goto_e
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->b()[I

    move-result-object v9

    const/4 v1, 0x0

    const/4 v5, 0x0

    aput v5, v9, v1

    const/4 v1, 0x1

    const/4 v5, 0x0

    aput v5, v9, v1

    const/4 v1, 0x2

    const/4 v5, 0x0

    aput v5, v9, v1

    const/4 v1, 0x3

    const/4 v5, 0x0

    aput v5, v9, v1

    invoke-virtual {p1}, Lcom/google/zxing/common/a;->a()I

    move-result v10

    if-ltz v3, :cond_46

    move v1, v3

    :goto_29
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v5

    rem-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_68

    const/4 v5, 0x1

    :goto_32
    const/4 v6, 0x0

    move v7, v1

    move v1, v6

    :goto_35
    if-ge v7, v10, :cond_6c

    invoke-virtual {p1, v7}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v1

    if-nez v1, :cond_6a

    const/4 v1, 0x1

    :goto_3e
    if-eqz v1, :cond_6c

    add-int/lit8 v7, v7, 0x1

    goto :goto_35

    .line 207
    :cond_43
    const/4 v1, 0x0

    move v2, v1

    goto :goto_a

    .line 214
    :cond_46
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4e

    const/4 v1, 0x0

    goto :goto_29

    :cond_4e
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/expanded/a;->d()Lcom/google/zxing/oned/rss/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/c;->b()[I

    move-result-object v1

    const/4 v5, 0x1

    aget v1, v1, v5

    goto :goto_29

    :cond_68
    const/4 v5, 0x0

    goto :goto_32

    :cond_6a
    const/4 v1, 0x0

    goto :goto_3e

    :cond_6c
    const/4 v6, 0x0

    move v8, v7

    move v13, v6

    move v6, v7

    move v7, v1

    move v1, v13

    :goto_72
    if-ge v8, v10, :cond_f9

    invoke-virtual {p1, v8}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v11

    xor-int/2addr v11, v7

    if-eqz v11, :cond_84

    aget v11, v9, v1

    add-int/lit8 v11, v11, 0x1

    aput v11, v9, v1

    :goto_81
    add-int/lit8 v8, v8, 0x1

    goto :goto_72

    :cond_84
    const/4 v11, 0x3

    if-ne v1, v11, :cond_f4

    if-eqz v5, :cond_8c

    invoke-static {v9}, Lcom/google/zxing/oned/rss/expanded/b;->c([I)V

    :cond_8c
    invoke-static {v9}, Lcom/google/zxing/oned/rss/expanded/b;->b([I)Z

    move-result v11

    if-eqz v11, :cond_ca

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    const/4 v5, 0x0

    aput v6, v1, v5

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    const/4 v5, 0x1

    aput v8, v1, v5

    .line 215
    move/from16 v0, p3

    invoke-direct {p0, p1, v0, v2}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/common/a;IZ)Lcom/google/zxing/oned/rss/c;

    move-result-object v5

    .line 216
    if-nez v5, :cond_107

    .line 217
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/b;->i:[I

    const/4 v3, 0x0

    aget v1, v1, v3

    invoke-virtual {p1, v1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_fe

    invoke-virtual {p1, v1}, Lcom/google/zxing/common/a;->d(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/zxing/common/a;->c(I)I

    move-result v1

    :goto_b7
    move v3, v4

    .line 221
    :goto_b8
    if-nez v3, :cond_176

    .line 223
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v6, v1, 0x1

    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/b;->j:[I

    array-length v1, v1

    if-le v6, v1, :cond_10b

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 214
    :cond_ca
    if-eqz v5, :cond_cf

    invoke-static {v9}, Lcom/google/zxing/oned/rss/expanded/b;->c([I)V

    :cond_cf
    const/4 v11, 0x0

    aget v11, v9, v11

    const/4 v12, 0x1

    aget v12, v9, v12

    add-int/2addr v11, v12

    add-int/2addr v6, v11

    const/4 v11, 0x0

    const/4 v12, 0x2

    aget v12, v9, v12

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x3

    aget v12, v9, v12

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput v12, v9, v11

    const/4 v11, 0x3

    const/4 v12, 0x0

    aput v12, v9, v11

    add-int/lit8 v1, v1, -0x1

    :goto_ed
    const/4 v11, 0x1

    aput v11, v9, v1

    if-nez v7, :cond_f7

    const/4 v7, 0x1

    goto :goto_81

    :cond_f4
    add-int/lit8 v1, v1, 0x1

    goto :goto_ed

    :cond_f7
    const/4 v7, 0x0

    goto :goto_81

    :cond_f9
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 217
    :cond_fe
    invoke-virtual {p1, v1}, Lcom/google/zxing/common/a;->c(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/zxing/common/a;->d(I)I

    move-result v1

    goto :goto_b7

    .line 219
    :cond_107
    const/4 v4, 0x0

    move v1, v3

    move v3, v4

    goto :goto_b8

    .line 223
    :cond_10b
    const/4 v1, 0x0

    move v3, v1

    :goto_10d
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_12b

    iget-object v4, p0, Lcom/google/zxing/oned/rss/expanded/b;->j:[I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/expanded/a;->d()Lcom/google/zxing/oned/rss/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/c;->a()I

    move-result v1

    aput v1, v4, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_10d

    :cond_12b
    iget-object v1, p0, Lcom/google/zxing/oned/rss/expanded/b;->j:[I

    add-int/lit8 v3, v6, -0x1

    invoke-virtual {v5}, Lcom/google/zxing/oned/rss/c;->a()I

    move-result v4

    aput v4, v1, v3

    sget-object v7, Lcom/google/zxing/oned/rss/expanded/b;->f:[[I

    array-length v8, v7

    const/4 v1, 0x0

    :goto_139
    if-ge v1, v8, :cond_16b

    aget-object v9, v7, v1

    array-length v3, v9

    if-lt v3, v6, :cond_168

    const/4 v3, 0x1

    const/4 v4, 0x0

    :goto_142
    if-ge v4, v6, :cond_14d

    iget-object v10, p0, Lcom/google/zxing/oned/rss/expanded/b;->j:[I

    aget v10, v10, v4

    aget v11, v9, v4

    if-eq v10, v11, :cond_163

    const/4 v3, 0x0

    :cond_14d
    if-eqz v3, :cond_168

    array-length v1, v9

    if-ne v6, v1, :cond_166

    const/4 v1, 0x1

    .line 225
    :goto_153
    const/4 v3, 0x1

    invoke-direct {p0, p1, v5, v2, v3}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/common/a;Lcom/google/zxing/oned/rss/c;ZZ)Lcom/google/zxing/oned/rss/b;

    move-result-object v3

    .line 228
    const/4 v4, 0x0

    :try_start_159
    invoke-direct {p0, p1, v5, v2, v4}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/common/a;Lcom/google/zxing/oned/rss/c;ZZ)Lcom/google/zxing/oned/rss/b;
    :try_end_15c
    .catch Lcom/google/zxing/NotFoundException; {:try_start_159 .. :try_end_15c} :catch_170

    move-result-object v2

    .line 236
    :goto_15d
    new-instance v4, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-direct {v4, v3, v2, v5, v1}, Lcom/google/zxing/oned/rss/expanded/a;-><init>(Lcom/google/zxing/oned/rss/b;Lcom/google/zxing/oned/rss/b;Lcom/google/zxing/oned/rss/c;Z)V

    return-object v4

    .line 223
    :cond_163
    add-int/lit8 v4, v4, 0x1

    goto :goto_142

    :cond_166
    const/4 v1, 0x0

    goto :goto_153

    :cond_168
    add-int/lit8 v1, v1, 0x1

    goto :goto_139

    :cond_16b
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    .line 229
    :catch_170
    move-exception v2

    .line 230
    if-eqz v1, :cond_175

    .line 231
    const/4 v2, 0x0

    goto :goto_15d

    .line 233
    :cond_175
    throw v2

    :cond_176
    move v4, v3

    move v3, v1

    goto/16 :goto_e
.end method

.method private static a(Lcom/google/zxing/oned/rss/c;ZZ)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/c;->a()I

    move-result v0

    if-nez v0, :cond_a

    if-eqz p1, :cond_a

    if-nez p2, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private static c([I)V
    .registers 5
    .parameter

    .prologue
    .line 339
    array-length v1, p0

    .line 340
    const/4 v0, 0x0

    :goto_2
    div-int/lit8 v2, v1, 0x2

    if-ge v0, v2, :cond_19

    .line 341
    aget v2, p0, v0

    .line 342
    sub-int v3, v1, v0

    add-int/lit8 v3, v3, -0x1

    aget v3, p0, v3

    aput v3, p0, v0

    .line 343
    sub-int v3, v1, v0

    add-int/lit8 v3, v3, -0x1

    aput v2, p0, v3

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 345
    :cond_19
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/16 v6, 0xb

    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 122
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/expanded/b;->a()V

    .line 123
    :cond_8
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-direct {p0, p2, v0, p1}, Lcom/google/zxing/oned/rss/expanded/b;->a(Lcom/google/zxing/common/a;Ljava/util/List;I)Lcom/google/zxing/oned/rss/expanded/a;

    move-result-object v8

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Lcom/google/zxing/oned/rss/expanded/a;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->b()Lcom/google/zxing/oned/rss/b;

    move-result-object v9

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->c()Lcom/google/zxing/oned/rss/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/b;->b()I

    move-result v0

    move v1, v2

    move v3, v4

    move v5, v0

    :goto_30
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5c

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->b()Lcom/google/zxing/oned/rss/b;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/zxing/oned/rss/b;->b()I

    move-result v10

    add-int/2addr v5, v10

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->c()Lcom/google/zxing/oned/rss/b;

    move-result-object v0

    if-eqz v0, :cond_58

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/b;->b()I

    move-result v0

    add-int/2addr v5, v0

    add-int/lit8 v3, v3, 0x1

    :cond_58
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_30

    :cond_5c
    rem-int/lit16 v0, v5, 0xd3

    add-int/lit8 v1, v3, -0x4

    mul-int/lit16 v1, v1, 0xd3

    add-int/2addr v0, v1

    invoke-virtual {v9}, Lcom/google/zxing/oned/rss/b;->a()I

    move-result v1

    if-ne v0, v1, :cond_b2

    move v0, v2

    :goto_6a
    if-eqz v0, :cond_b4

    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    .line 124
    iget-object v8, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v0, -0x1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->c()Lcom/google/zxing/oned/rss/b;

    move-result-object v0

    if-nez v0, :cond_151

    add-int/lit8 v0, v1, -0x1

    :goto_8c
    mul-int/lit8 v0, v0, 0xc

    new-instance v9, Lcom/google/zxing/common/a;

    invoke-direct {v9, v0}, Lcom/google/zxing/common/a;-><init>(I)V

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->c()Lcom/google/zxing/oned/rss/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/b;->a()I

    move-result v3

    move v0, v7

    move v1, v6

    :goto_a3
    if-ltz v1, :cond_bf

    shl-int v5, v2, v1

    and-int/2addr v5, v3

    if-eqz v5, :cond_ad

    invoke-virtual {v9, v0}, Lcom/google/zxing/common/a;->b(I)V

    :cond_ad
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_a3

    :cond_b2
    move v0, v7

    .line 123
    goto :goto_6a

    :cond_b4
    invoke-virtual {v8}, Lcom/google/zxing/oned/rss/expanded/a;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_bf
    move v1, v0

    move v3, v2

    .line 124
    :goto_c1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_10a

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->b()Lcom/google/zxing/oned/rss/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/zxing/oned/rss/b;->a()I

    move-result v10

    move v5, v6

    :goto_d6
    if-ltz v5, :cond_e5

    shl-int v11, v2, v5

    and-int/2addr v11, v10

    if-eqz v11, :cond_e0

    invoke-virtual {v9, v1}, Lcom/google/zxing/common/a;->b(I)V

    :cond_e0
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v5, v5, -0x1

    goto :goto_d6

    :cond_e5
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->c()Lcom/google/zxing/oned/rss/b;

    move-result-object v5

    if-eqz v5, :cond_104

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->c()Lcom/google/zxing/oned/rss/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/b;->a()I

    move-result v5

    move v0, v1

    move v1, v6

    :goto_f5
    if-ltz v1, :cond_105

    shl-int v10, v2, v1

    and-int/2addr v10, v5

    if-eqz v10, :cond_ff

    invoke-virtual {v9, v0}, Lcom/google/zxing/common/a;->b(I)V

    :cond_ff
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, -0x1

    goto :goto_f5

    :cond_104
    move v0, v1

    :cond_105
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_c1

    :cond_10a
    invoke-static {v9}, Lcom/google/zxing/oned/rss/expanded/decoders/j;->a(Lcom/google/zxing/common/a;)Lcom/google/zxing/oned/rss/expanded/decoders/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/decoders/j;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->d()Lcom/google/zxing/oned/rss/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/c;->c()[Lcom/google/zxing/g;

    move-result-object v3

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/expanded/a;

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/expanded/a;->d()Lcom/google/zxing/oned/rss/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/c;->c()[Lcom/google/zxing/g;

    move-result-object v0

    new-instance v5, Lcom/google/zxing/f;

    const/4 v6, 0x0

    const/4 v8, 0x4

    new-array v8, v8, [Lcom/google/zxing/g;

    aget-object v9, v3, v7

    aput-object v9, v8, v7

    aget-object v3, v3, v2

    aput-object v3, v8, v2

    aget-object v3, v0, v7

    aput-object v3, v8, v4

    const/4 v3, 0x3

    aget-object v0, v0, v2

    aput-object v0, v8, v3

    sget-object v0, Lcom/google/zxing/BarcodeFormat;->RSS_EXPANDED:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v5, v1, v6, v8, v0}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v5

    :cond_151
    move v0, v1

    goto/16 :goto_8c
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/zxing/oned/rss/expanded/b;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 130
    return-void
.end method
