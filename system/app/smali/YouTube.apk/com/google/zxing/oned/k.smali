.class public abstract Lcom/google/zxing/oned/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static a([I[II)I
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const v0, 0x7fffffff

    const/4 v1, 0x0

    .line 259
    array-length v5, p0

    move v2, v1

    move v3, v1

    move v4, v1

    .line 262
    :goto_8
    if-ge v2, v5, :cond_13

    .line 263
    aget v6, p0, v2

    add-int/2addr v4, v6

    .line 264
    aget v6, p1, v2

    add-int/2addr v3, v6

    .line 262
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 266
    :cond_13
    if-ge v4, v3, :cond_16

    .line 287
    :cond_15
    :goto_15
    return v0

    .line 274
    :cond_16
    shl-int/lit8 v2, v4, 0x8

    div-int v6, v2, v3

    .line 275
    mul-int v2, p2, v6

    shr-int/lit8 v7, v2, 0x8

    move v2, v1

    move v3, v1

    .line 278
    :goto_20
    if-ge v2, v5, :cond_36

    .line 279
    aget v1, p0, v2

    shl-int/lit8 v1, v1, 0x8

    .line 280
    aget v8, p1, v2

    mul-int/2addr v8, v6

    .line 281
    if-le v1, v8, :cond_33

    sub-int/2addr v1, v8

    .line 282
    :goto_2c
    if-gt v1, v7, :cond_15

    .line 285
    add-int/2addr v3, v1

    .line 278
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_20

    .line 281
    :cond_33
    sub-int v1, v8, v1

    goto :goto_2c

    .line 287
    :cond_36
    div-int v0, v3, v4

    goto :goto_15
.end method

.method protected static a(Lcom/google/zxing/common/a;I[I)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 196
    array-length v5, p2

    .line 197
    invoke-static {p2, v2, v5, v2}, Ljava/util/Arrays;->fill([IIII)V

    .line 198
    invoke-virtual {p0}, Lcom/google/zxing/common/a;->a()I

    move-result v6

    .line 199
    if-lt p1, v6, :cond_11

    .line 200
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 202
    :cond_11
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_2e

    move v0, v1

    :goto_18
    move v4, v0

    move v0, v2

    .line 205
    :goto_1a
    if-ge p1, v6, :cond_3f

    .line 206
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v3

    xor-int/2addr v3, v4

    if-eqz v3, :cond_30

    .line 207
    aget v3, p2, v0

    add-int/lit8 v3, v3, 0x1

    aput v3, p2, v0

    move v3, v4

    .line 217
    :goto_2a
    add-int/lit8 p1, p1, 0x1

    move v4, v3

    goto :goto_1a

    :cond_2e
    move v0, v2

    .line 202
    goto :goto_18

    .line 209
    :cond_30
    add-int/lit8 v3, v0, 0x1

    .line 210
    if-eq v3, v5, :cond_40

    .line 211
    aput v1, p2, v3

    .line 214
    if-nez v4, :cond_3d

    move v0, v1

    :goto_39
    move v7, v3

    move v3, v0

    move v0, v7

    goto :goto_2a

    :cond_3d
    move v0, v2

    goto :goto_39

    :cond_3f
    move v3, v0

    .line 221
    :cond_40
    if-eq v3, v5, :cond_4d

    add-int/lit8 v0, v5, -0x1

    if-ne v3, v0, :cond_48

    if-eq p1, v6, :cond_4d

    .line 222
    :cond_48
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 224
    :cond_4d
    return-void
.end method

.method private b(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 20
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/b;->a()I

    move-result v8

    .line 109
    invoke-virtual/range {p1 .. p1}, Lcom/google/zxing/b;->b()I

    move-result v2

    .line 110
    new-instance v4, Lcom/google/zxing/common/a;

    invoke-direct {v4, v8}, Lcom/google/zxing/common/a;-><init>(I)V

    .line 112
    shr-int/lit8 v9, v2, 0x1

    .line 113
    if-eqz p2, :cond_c6

    sget-object v1, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c6

    const/4 v1, 0x1

    move v3, v1

    .line 114
    :goto_1d
    const/4 v5, 0x1

    if-eqz v3, :cond_ca

    const/16 v1, 0x8

    :goto_22
    shr-int v1, v2, v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 116
    if-eqz v3, :cond_cd

    move v1, v2

    .line 122
    :goto_2b
    const/4 v3, 0x0

    move v7, v3

    move-object v3, v4

    move-object/from16 v4, p2

    :goto_30
    if-ge v7, v1, :cond_e4

    .line 125
    add-int/lit8 v5, v7, 0x1

    shr-int/lit8 v5, v5, 0x1

    .line 126
    and-int/lit8 v6, v7, 0x1

    if-nez v6, :cond_d1

    const/4 v6, 0x1

    .line 127
    :goto_3b
    if-eqz v6, :cond_d4

    :goto_3d
    mul-int/2addr v5, v10

    add-int v11, v9, v5

    .line 128
    if-ltz v11, :cond_e4

    if-ge v11, v2, :cond_e4

    .line 130
    :try_start_44
    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v3}, Lcom/google/zxing/b;->a(ILcom/google/zxing/common/a;)Lcom/google/zxing/common/a;
    :try_end_49
    .catch Lcom/google/zxing/NotFoundException; {:try_start_44 .. :try_end_49} :catch_de

    move-result-object v3

    .line 142
    const/4 v5, 0x0

    :goto_4b
    const/4 v6, 0x2

    if-ge v5, v6, :cond_df

    .line 143
    const/4 v6, 0x1

    if-ne v5, v6, :cond_e9

    .line 144
    invoke-virtual {v3}, Lcom/google/zxing/common/a;->c()V

    .line 149
    if-eqz v4, :cond_e9

    sget-object v6, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v4, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e9

    .line 150
    new-instance v6, Ljava/util/EnumMap;

    const-class v12, Lcom/google/zxing/DecodeHintType;

    invoke-direct {v6, v12}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 151
    invoke-interface {v6, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 152
    sget-object v4, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {v6, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    :goto_6d
    :try_start_6d
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v3, v6}, Lcom/google/zxing/oned/k;->a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;

    move-result-object v4

    .line 160
    const/4 v12, 0x1

    if-ne v5, v12, :cond_c5

    .line 162
    sget-object v12, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    const/16 v13, 0xb4

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 164
    invoke-virtual {v4}, Lcom/google/zxing/f;->c()[Lcom/google/zxing/g;

    move-result-object v12

    .line 165
    if-eqz v12, :cond_c5

    .line 166
    const/4 v13, 0x0

    new-instance v14, Lcom/google/zxing/g;

    int-to-float v15, v8

    const/16 v16, 0x0

    aget-object v16, v12, v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/g;->a()F

    move-result v16

    sub-float v15, v15, v16

    const/high16 v16, 0x3f80

    sub-float v15, v15, v16

    const/16 v16, 0x0

    aget-object v16, v12, v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/g;->b()F

    move-result v16

    invoke-direct/range {v14 .. v16}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v14, v12, v13

    .line 167
    const/4 v13, 0x1

    new-instance v14, Lcom/google/zxing/g;

    int-to-float v15, v8

    const/16 v16, 0x1

    aget-object v16, v12, v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/g;->a()F

    move-result v16

    sub-float v15, v15, v16

    const/high16 v16, 0x3f80

    sub-float v15, v15, v16

    const/16 v16, 0x1

    aget-object v16, v12, v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/zxing/g;->b()F

    move-result v16

    invoke-direct/range {v14 .. v16}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v14, v12, v13
    :try_end_c5
    .catch Lcom/google/zxing/ReaderException; {:try_start_6d .. :try_end_c5} :catch_d7

    .line 170
    :cond_c5
    return-object v4

    .line 113
    :cond_c6
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_1d

    .line 114
    :cond_ca
    const/4 v1, 0x5

    goto/16 :goto_22

    .line 119
    :cond_cd
    const/16 v1, 0xf

    goto/16 :goto_2b

    .line 126
    :cond_d1
    const/4 v6, 0x0

    goto/16 :goto_3b

    .line 127
    :cond_d4
    neg-int v5, v5

    goto/16 :goto_3d

    .line 142
    :catch_d7
    move-exception v4

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v6

    goto/16 :goto_4b

    .line 137
    :catch_de
    move-exception v5

    .line 122
    :cond_df
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto/16 :goto_30

    .line 177
    :cond_e4
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v1

    throw v1

    :cond_e9
    move-object v6, v4

    goto :goto_6d
.end method

.method protected static b(Lcom/google/zxing/common/a;I[I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 229
    array-length v1, p2

    .line 230
    invoke-virtual {p0, p1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v0

    .line 231
    :cond_5
    :goto_5
    if-lez p1, :cond_19

    if-ltz v1, :cond_19

    .line 232
    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p0, p1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-eq v2, v0, :cond_5

    .line 233
    add-int/lit8 v1, v1, -0x1

    .line 234
    if-nez v0, :cond_17

    const/4 v0, 0x1

    goto :goto_5

    :cond_17
    const/4 v0, 0x0

    goto :goto_5

    .line 237
    :cond_19
    if-ltz v1, :cond_20

    .line 238
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 240
    :cond_20
    add-int/lit8 v0, p1, 0x1

    invoke-static {p0, v0, p2}, Lcom/google/zxing/oned/k;->a(Lcom/google/zxing/common/a;I[I)V

    .line 241
    return-void
.end method


# virtual methods
.method public abstract a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
.end method

.method public a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 57
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/zxing/oned/k;->b(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    :try_end_4
    .catch Lcom/google/zxing/NotFoundException; {:try_start_1 .. :try_end_4} :catch_6

    move-result-object v0

    .line 80
    :goto_5
    return-object v0

    .line 58
    :catch_6
    move-exception v2

    .line 59
    if-eqz p2, :cond_74

    sget-object v0, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_74

    const/4 v0, 0x1

    .line 60
    :goto_12
    if-eqz v0, :cond_78

    invoke-virtual {p1}, Lcom/google/zxing/b;->d()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 61
    invoke-virtual {p1}, Lcom/google/zxing/b;->e()Lcom/google/zxing/b;

    move-result-object v3

    .line 62
    invoke-direct {p0, v3, p2}, Lcom/google/zxing/oned/k;->b(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;

    move-result-object v2

    .line 64
    invoke-virtual {v2}, Lcom/google/zxing/f;->e()Ljava/util/Map;

    move-result-object v4

    .line 65
    const/16 v0, 0x10e

    .line 66
    if-eqz v4, :cond_42

    sget-object v5, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_42

    .line 68
    sget-object v0, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit16 v0, v0, 0x10e

    rem-int/lit16 v0, v0, 0x168

    .line 71
    :cond_42
    sget-object v4, Lcom/google/zxing/ResultMetadataType;->ORIENTATION:Lcom/google/zxing/ResultMetadataType;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/zxing/f;->a(Lcom/google/zxing/ResultMetadataType;Ljava/lang/Object;)V

    .line 73
    invoke-virtual {v2}, Lcom/google/zxing/f;->c()[Lcom/google/zxing/g;

    move-result-object v4

    .line 74
    if-eqz v4, :cond_76

    .line 75
    invoke-virtual {v3}, Lcom/google/zxing/b;->b()I

    move-result v3

    move v0, v1

    .line 76
    :goto_56
    array-length v1, v4

    if-ge v0, v1, :cond_76

    .line 77
    new-instance v1, Lcom/google/zxing/g;

    int-to-float v5, v3

    aget-object v6, v4, v0

    invoke-virtual {v6}, Lcom/google/zxing/g;->b()F

    move-result v6

    sub-float/2addr v5, v6

    const/high16 v6, 0x3f80

    sub-float/2addr v5, v6

    aget-object v6, v4, v0

    invoke-virtual {v6}, Lcom/google/zxing/g;->a()F

    move-result v6

    invoke-direct {v1, v5, v6}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v1, v4, v0

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_56

    :cond_74
    move v0, v1

    .line 59
    goto :goto_12

    :cond_76
    move-object v0, v2

    .line 80
    goto :goto_5

    .line 82
    :cond_78
    throw v2
.end method

.method public a()V
    .registers 1

    .prologue
    .line 90
    return-void
.end method
