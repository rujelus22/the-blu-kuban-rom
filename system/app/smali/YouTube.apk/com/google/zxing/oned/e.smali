.class public final Lcom/google/zxing/oned/e;
.super Lcom/google/zxing/oned/p;
.source "SourceFile"


# static fields
.field static final a:[I


# instance fields
.field private final f:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 61
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/zxing/oned/e;->a:[I

    return-void

    :array_a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xbt 0x0t 0x0t 0x0t
        0xdt 0x0t 0x0t 0x0t
        0xet 0x0t 0x0t 0x0t
        0x13t 0x0t 0x0t 0x0t
        0x19t 0x0t 0x0t 0x0t
        0x1ct 0x0t 0x0t 0x0t
        0x15t 0x0t 0x0t 0x0t
        0x16t 0x0t 0x0t 0x0t
        0x1at 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/zxing/oned/p;-><init>()V

    .line 68
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/oned/e;->f:[I

    .line 69
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/zxing/common/a;[ILjava/lang/StringBuilder;)I
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    iget-object v4, p0, Lcom/google/zxing/oned/e;->f:[I

    .line 76
    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v4, v0

    .line 77
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, v4, v0

    .line 78
    const/4 v0, 0x2

    const/4 v1, 0x0

    aput v1, v4, v0

    .line 79
    const/4 v0, 0x3

    const/4 v1, 0x0

    aput v1, v4, v0

    .line 80
    invoke-virtual {p1}, Lcom/google/zxing/common/a;->a()I

    move-result v5

    .line 81
    const/4 v0, 0x1

    aget v2, p2, v0

    .line 83
    const/4 v1, 0x0

    .line 85
    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    :goto_1d
    const/4 v1, 0x6

    if-ge v3, v1, :cond_47

    if-ge v2, v5, :cond_47

    .line 86
    sget-object v1, Lcom/google/zxing/oned/e;->e:[[I

    invoke-static {p1, v4, v2, v1}, Lcom/google/zxing/oned/e;->a(Lcom/google/zxing/common/a;[II[[I)I

    move-result v6

    .line 87
    rem-int/lit8 v1, v6, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 88
    array-length v7, v4

    const/4 v1, 0x0

    :goto_32
    if-ge v1, v7, :cond_3a

    aget v8, v4, v1

    .line 89
    add-int/2addr v2, v8

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_32

    .line 91
    :cond_3a
    const/16 v1, 0xa

    if-lt v6, v1, :cond_43

    .line 92
    const/4 v1, 0x1

    rsub-int/lit8 v6, v3, 0x5

    shl-int/2addr v1, v6

    or-int/2addr v0, v1

    .line 85
    :cond_43
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1d

    .line 96
    :cond_47
    const/4 v1, 0x0

    :goto_48
    const/16 v3, 0xa

    if-ge v1, v3, :cond_84

    sget-object v3, Lcom/google/zxing/oned/e;->a:[I

    aget v3, v3, v1

    if-ne v0, v3, :cond_81

    const/4 v0, 0x0

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {p3, v0, v1}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 98
    const/4 v0, 0x1

    sget-object v1, Lcom/google/zxing/oned/e;->c:[I

    invoke-static {p1, v2, v0, v1}, Lcom/google/zxing/oned/e;->a(Lcom/google/zxing/common/a;IZ[I)[I

    move-result-object v0

    .line 99
    const/4 v1, 0x1

    aget v1, v0, v1

    .line 101
    const/4 v0, 0x0

    :goto_64
    const/4 v2, 0x6

    if-ge v0, v2, :cond_8c

    if-ge v1, v5, :cond_8c

    .line 102
    sget-object v2, Lcom/google/zxing/oned/e;->d:[[I

    invoke-static {p1, v4, v1, v2}, Lcom/google/zxing/oned/e;->a(Lcom/google/zxing/common/a;[II[[I)I

    move-result v2

    .line 103
    add-int/lit8 v2, v2, 0x30

    int-to-char v2, v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    array-length v6, v4

    const/4 v2, 0x0

    :goto_77
    if-ge v2, v6, :cond_89

    aget v3, v4, v2

    .line 105
    add-int/2addr v3, v1

    .line 104
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_77

    .line 96
    :cond_81
    add-int/lit8 v1, v1, 0x1

    goto :goto_48

    :cond_84
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 101
    :cond_89
    add-int/lit8 v0, v0, 0x1

    goto :goto_64

    .line 109
    :cond_8c
    return v1
.end method

.method final b()Lcom/google/zxing/BarcodeFormat;
    .registers 2

    .prologue
    .line 114
    sget-object v0, Lcom/google/zxing/BarcodeFormat;->EAN_13:Lcom/google/zxing/BarcodeFormat;

    return-object v0
.end method
