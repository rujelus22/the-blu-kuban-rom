.class public final Lcom/google/zxing/oned/j;
.super Lcom/google/zxing/oned/k;
.source "SourceFile"


# instance fields
.field private final a:[Lcom/google/zxing/oned/p;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .registers 5
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/zxing/oned/k;-><init>()V

    .line 43
    if-nez p1, :cond_6a

    const/4 v0, 0x0

    .line 45
    :goto_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 46
    if-eqz v0, :cond_3d

    .line 47
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->EAN_13:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_73

    .line 48
    new-instance v2, Lcom/google/zxing/oned/e;

    invoke-direct {v2}, Lcom/google/zxing/oned/e;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_1d
    :goto_1d
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->EAN_8:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 53
    new-instance v2, Lcom/google/zxing/oned/f;

    invoke-direct {v2}, Lcom/google/zxing/oned/f;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_2d
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->UPC_E:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 56
    new-instance v0, Lcom/google/zxing/oned/q;

    invoke-direct {v0}, Lcom/google/zxing/oned/q;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 59
    :cond_3d
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5b

    .line 60
    new-instance v0, Lcom/google/zxing/oned/e;

    invoke-direct {v0}, Lcom/google/zxing/oned/e;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v0, Lcom/google/zxing/oned/f;

    invoke-direct {v0}, Lcom/google/zxing/oned/f;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v0, Lcom/google/zxing/oned/q;

    invoke-direct {v0}, Lcom/google/zxing/oned/q;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_5b
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/zxing/oned/p;

    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/oned/p;

    iput-object v0, p0, Lcom/google/zxing/oned/j;->a:[Lcom/google/zxing/oned/p;

    .line 66
    return-void

    .line 43
    :cond_6a
    sget-object v0, Lcom/google/zxing/DecodeHintType;->POSSIBLE_FORMATS:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_6

    .line 49
    :cond_73
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 50
    new-instance v2, Lcom/google/zxing/oned/l;

    invoke-direct {v2}, Lcom/google/zxing/oned/l;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1d
.end method


# virtual methods
.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-static {p2}, Lcom/google/zxing/oned/p;->a(Lcom/google/zxing/common/a;)[I

    move-result-object v1

    .line 74
    iget-object v4, p0, Lcom/google/zxing/oned/j;->a:[Lcom/google/zxing/oned/p;

    array-length v5, v4

    move v0, v2

    :goto_a
    if-ge v0, v5, :cond_6b

    aget-object v6, v4, v0

    .line 77
    :try_start_e
    invoke-virtual {v6, p1, p2, v1, p3}, Lcom/google/zxing/oned/p;->a(ILcom/google/zxing/common/a;[ILjava/util/Map;)Lcom/google/zxing/f;
    :try_end_11
    .catch Lcom/google/zxing/ReaderException; {:try_start_e .. :try_end_11} :catch_67

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lcom/google/zxing/f;->d()Lcom/google/zxing/BarcodeFormat;

    move-result-object v0

    sget-object v4, Lcom/google/zxing/BarcodeFormat;->EAN_13:Lcom/google/zxing/BarcodeFormat;

    if-ne v0, v4, :cond_58

    invoke-virtual {v1}, Lcom/google/zxing/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v4, 0x30

    if-ne v0, v4, :cond_58

    move v4, v3

    .line 96
    :goto_27
    if-nez p3, :cond_5a

    const/4 v0, 0x0

    .line 98
    :goto_2a
    if-eqz v0, :cond_34

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    :cond_34
    move v0, v3

    .line 100
    :goto_35
    if-eqz v4, :cond_65

    if-eqz v0, :cond_65

    .line 102
    new-instance v0, Lcom/google/zxing/f;

    invoke-virtual {v1}, Lcom/google/zxing/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/zxing/f;->b()[B

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/zxing/f;->c()[Lcom/google/zxing/g;

    move-result-object v4

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    .line 106
    invoke-virtual {v1}, Lcom/google/zxing/f;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/zxing/f;->a(Ljava/util/Map;)V

    .line 109
    :goto_57
    return-object v0

    :cond_58
    move v4, v2

    .line 93
    goto :goto_27

    .line 96
    :cond_5a
    sget-object v0, Lcom/google/zxing/DecodeHintType;->POSSIBLE_FORMATS:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto :goto_2a

    :cond_63
    move v0, v2

    .line 98
    goto :goto_35

    :cond_65
    move-object v0, v1

    .line 109
    goto :goto_57

    .line 79
    :catch_67
    move-exception v6

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 112
    :cond_6b
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method public final a()V
    .registers 5

    .prologue
    .line 117
    iget-object v1, p0, Lcom/google/zxing/oned/j;->a:[Lcom/google/zxing/oned/p;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 118
    invoke-interface {v3}, Lcom/google/zxing/e;->a()V

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 120
    :cond_e
    return-void
.end method
