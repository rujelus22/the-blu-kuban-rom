.class public final Lcom/google/zxing/oned/rss/e;
.super Lcom/google/zxing/oned/rss/a;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[I

.field private static final f:[I

.field private static final g:[[I


# instance fields
.field private final h:Ljava/util/List;

.field private final i:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    .line 37
    new-array v0, v4, [I

    fill-array-data v0, :array_7a

    sput-object v0, Lcom/google/zxing/oned/rss/e;->a:[I

    .line 38
    new-array v0, v3, [I

    fill-array-data v0, :array_88

    sput-object v0, Lcom/google/zxing/oned/rss/e;->b:[I

    .line 39
    new-array v0, v4, [I

    fill-array-data v0, :array_94

    sput-object v0, Lcom/google/zxing/oned/rss/e;->c:[I

    .line 40
    new-array v0, v3, [I

    fill-array-data v0, :array_a2

    sput-object v0, Lcom/google/zxing/oned/rss/e;->d:[I

    .line 41
    new-array v0, v4, [I

    fill-array-data v0, :array_ae

    sput-object v0, Lcom/google/zxing/oned/rss/e;->e:[I

    .line 42
    new-array v0, v3, [I

    fill-array-data v0, :array_bc

    sput-object v0, Lcom/google/zxing/oned/rss/e;->f:[I

    .line 44
    const/16 v0, 0x9

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_c8

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_d4

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_e0

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_ec

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_f8

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_104

    aput-object v1, v0, v4

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_110

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_11c

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_128

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/oned/rss/e;->g:[[I

    return-void

    .line 37
    :array_7a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
        0x22t 0x0t 0x0t 0x0t
        0x46t 0x0t 0x0t 0x0t
        0x7et 0x0t 0x0t 0x0t
    .end array-data

    .line 38
    :array_88
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x14t 0x0t 0x0t 0x0t
        0x30t 0x0t 0x0t 0x0t
        0x51t 0x0t 0x0t 0x0t
    .end array-data

    .line 39
    :array_94
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0xa1t 0x0t 0x0t 0x0t
        0xc1t 0x3t 0x0t 0x0t
        0xdft 0x7t 0x0t 0x0t
        0x9bt 0xat 0x0t 0x0t
    .end array-data

    .line 40
    :array_a2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x50t 0x1t 0x0t 0x0t
        0xct 0x4t 0x0t 0x0t
        0xect 0x5t 0x0t 0x0t
    .end array-data

    .line 41
    :array_ae
    .array-data 0x4
        0x8t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    .line 42
    :array_bc
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data

    .line 44
    :array_c8
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_d4
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_e0
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_ec
    .array-data 0x4
        0x3t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_f8
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_104
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_110
    .array-data 0x4
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_11c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data

    :array_128
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x9t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/zxing/oned/rss/a;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/rss/e;->h:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/oned/rss/e;->i:Ljava/util/List;

    .line 62
    return-void
.end method

.method private a(Lcom/google/zxing/common/a;Lcom/google/zxing/oned/rss/c;Z)Lcom/google/zxing/oned/rss/b;
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->c()[I

    move-result-object v3

    .line 190
    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 191
    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 192
    const/4 v0, 0x2

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 193
    const/4 v0, 0x3

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 194
    const/4 v0, 0x4

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 195
    const/4 v0, 0x5

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 196
    const/4 v0, 0x6

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 197
    const/4 v0, 0x7

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 199
    if-eqz p3, :cond_6c

    .line 200
    invoke-virtual {p2}, Lcom/google/zxing/oned/rss/c;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {p1, v0, v3}, Lcom/google/zxing/oned/rss/e;->b(Lcom/google/zxing/common/a;I[I)V

    .line 211
    :cond_30
    if-eqz p3, :cond_8b

    const/16 v0, 0x10

    .line 212
    :goto_34
    invoke-static {v3}, Lcom/google/zxing/oned/rss/e;->a([I)I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    div-float v4, v1, v2

    .line 214
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->f()[I

    move-result-object v6

    .line 215
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->g()[I

    move-result-object v7

    .line 216
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->d()[F

    move-result-object v5

    .line 217
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->e()[F

    move-result-object v8

    .line 219
    const/4 v1, 0x0

    :goto_4d
    array-length v2, v3

    if-ge v1, v2, :cond_9d

    .line 220
    aget v2, v3, v1

    int-to-float v2, v2

    div-float v9, v2, v4

    .line 221
    const/high16 v2, 0x3f00

    add-float/2addr v2, v9

    float-to-int v2, v2

    .line 222
    if-gtz v2, :cond_8e

    .line 223
    const/4 v2, 0x1

    .line 227
    :cond_5c
    :goto_5c
    shr-int/lit8 v10, v1, 0x1

    .line 228
    and-int/lit8 v11, v1, 0x1

    if-nez v11, :cond_95

    .line 229
    aput v2, v6, v10

    .line 230
    int-to-float v2, v2

    sub-float v2, v9, v2

    aput v2, v5, v10

    .line 219
    :goto_69
    add-int/lit8 v1, v1, 0x1

    goto :goto_4d

    .line 202
    :cond_6c
    invoke-virtual {p2}, Lcom/google/zxing/oned/rss/c;->b()[I

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-static {p1, v0, v3}, Lcom/google/zxing/oned/rss/e;->a(Lcom/google/zxing/common/a;I[I)V

    .line 204
    const/4 v1, 0x0

    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    :goto_7c
    if-ge v1, v0, :cond_30

    .line 205
    aget v2, v3, v1

    .line 206
    aget v4, v3, v0

    aput v4, v3, v1

    .line 207
    aput v2, v3, v0

    .line 204
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_7c

    .line 211
    :cond_8b
    const/16 v0, 0xf

    goto :goto_34

    .line 224
    :cond_8e
    const/16 v10, 0x8

    if-le v2, v10, :cond_5c

    .line 225
    const/16 v2, 0x8

    goto :goto_5c

    .line 232
    :cond_95
    aput v2, v7, v10

    .line 233
    int-to-float v2, v2

    sub-float v2, v9, v2

    aput v2, v8, v10

    goto :goto_69

    .line 237
    :cond_9d
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->f()[I

    move-result-object v1

    invoke-static {v1}, Lcom/google/zxing/oned/rss/e;->a([I)I

    move-result v8

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->g()[I

    move-result-object v1

    invoke-static {v1}, Lcom/google/zxing/oned/rss/e;->a([I)I

    move-result v9

    add-int v1, v8, v9

    sub-int v10, v1, v0

    and-int/lit8 v1, v8, 0x1

    if-eqz p3, :cond_dd

    const/4 v0, 0x1

    :goto_b6
    if-ne v1, v0, :cond_df

    const/4 v0, 0x1

    move v5, v0

    :goto_ba
    and-int/lit8 v0, v9, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e2

    const/4 v0, 0x1

    move v4, v0

    :goto_c1
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    if-eqz p3, :cond_ef

    const/16 v11, 0xc

    if-le v8, v11, :cond_e5

    const/4 v2, 0x1

    :cond_cc
    :goto_cc
    const/16 v11, 0xc

    if-le v9, v11, :cond_ea

    const/4 v0, 0x1

    :cond_d1
    :goto_d1
    const/4 v11, 0x1

    if-ne v10, v11, :cond_117

    if-eqz v5, :cond_10e

    if-eqz v4, :cond_104

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_dd
    const/4 v0, 0x0

    goto :goto_b6

    :cond_df
    const/4 v0, 0x0

    move v5, v0

    goto :goto_ba

    :cond_e2
    const/4 v0, 0x0

    move v4, v0

    goto :goto_c1

    :cond_e5
    const/4 v11, 0x4

    if-ge v8, v11, :cond_cc

    const/4 v3, 0x1

    goto :goto_cc

    :cond_ea
    const/4 v11, 0x4

    if-ge v9, v11, :cond_d1

    const/4 v1, 0x1

    goto :goto_d1

    :cond_ef
    const/16 v11, 0xb

    if-le v8, v11, :cond_fa

    const/4 v2, 0x1

    :cond_f4
    :goto_f4
    const/16 v11, 0xa

    if-le v9, v11, :cond_ff

    const/4 v0, 0x1

    goto :goto_d1

    :cond_fa
    const/4 v11, 0x5

    if-ge v8, v11, :cond_f4

    const/4 v3, 0x1

    goto :goto_f4

    :cond_ff
    const/4 v11, 0x4

    if-ge v9, v11, :cond_d1

    const/4 v1, 0x1

    goto :goto_d1

    :cond_104
    const/4 v2, 0x1

    :cond_105
    :goto_105
    if-eqz v3, :cond_158

    if-eqz v2, :cond_14d

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_10e
    if-nez v4, :cond_115

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_115
    const/4 v0, 0x1

    goto :goto_105

    :cond_117
    const/4 v11, -0x1

    if-ne v10, v11, :cond_12e

    if-eqz v5, :cond_125

    if-eqz v4, :cond_123

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_123
    const/4 v3, 0x1

    goto :goto_105

    :cond_125
    if-nez v4, :cond_12c

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_12c
    const/4 v1, 0x1

    goto :goto_105

    :cond_12e
    if-nez v10, :cond_148

    if-eqz v5, :cond_141

    if-nez v4, :cond_139

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_139
    if-ge v8, v9, :cond_13e

    const/4 v3, 0x1

    const/4 v0, 0x1

    goto :goto_105

    :cond_13e
    const/4 v2, 0x1

    const/4 v1, 0x1

    goto :goto_105

    :cond_141
    if-eqz v4, :cond_105

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_148
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_14d
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->f()[I

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->d()[F

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/zxing/oned/rss/e;->a([I[F)V

    :cond_158
    if-eqz v2, :cond_165

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->f()[I

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->d()[F

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/zxing/oned/rss/e;->b([I[F)V

    :cond_165
    if-eqz v1, :cond_179

    if-eqz v0, :cond_16e

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_16e
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->g()[I

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->d()[F

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/zxing/oned/rss/e;->a([I[F)V

    :cond_179
    if-eqz v0, :cond_186

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->g()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->e()[F

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/zxing/oned/rss/e;->b([I[F)V

    .line 239
    :cond_186
    const/4 v2, 0x0

    .line 240
    const/4 v1, 0x0

    .line 241
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    move v3, v1

    move v4, v2

    :goto_18d
    if-ltz v0, :cond_19c

    .line 242
    mul-int/lit8 v1, v3, 0x9

    .line 243
    aget v2, v6, v0

    add-int/2addr v1, v2

    .line 244
    aget v2, v6, v0

    add-int/2addr v2, v4

    .line 241
    add-int/lit8 v0, v0, -0x1

    move v3, v1

    move v4, v2

    goto :goto_18d

    .line 246
    :cond_19c
    const/4 v1, 0x0

    .line 247
    const/4 v0, 0x0

    .line 248
    array-length v2, v7

    add-int/lit8 v2, v2, -0x1

    :goto_1a1
    if-ltz v2, :cond_1ae

    .line 249
    mul-int/lit8 v1, v1, 0x9

    .line 250
    aget v5, v7, v2

    add-int/2addr v1, v5

    .line 251
    aget v5, v7, v2

    add-int/2addr v0, v5

    .line 248
    add-int/lit8 v2, v2, -0x1

    goto :goto_1a1

    .line 253
    :cond_1ae
    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v1, v3

    .line 255
    if-eqz p3, :cond_1e8

    .line 256
    and-int/lit8 v0, v4, 0x1

    if-nez v0, :cond_1be

    const/16 v0, 0xc

    if-gt v4, v0, :cond_1be

    const/4 v0, 0x4

    if-ge v4, v0, :cond_1c3

    .line 257
    :cond_1be
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 259
    :cond_1c3
    rsub-int/lit8 v0, v4, 0xc

    div-int/lit8 v0, v0, 0x2

    .line 260
    sget-object v2, Lcom/google/zxing/oned/rss/e;->e:[I

    aget v2, v2, v0

    .line 261
    rsub-int/lit8 v3, v2, 0x9

    .line 262
    const/4 v4, 0x0

    invoke-static {v6, v2, v4}, Lcom/google/zxing/oned/rss/f;->a([IIZ)I

    move-result v2

    .line 263
    const/4 v4, 0x1

    invoke-static {v7, v3, v4}, Lcom/google/zxing/oned/rss/f;->a([IIZ)I

    move-result v3

    .line 264
    sget-object v4, Lcom/google/zxing/oned/rss/e;->a:[I

    aget v4, v4, v0

    .line 265
    sget-object v5, Lcom/google/zxing/oned/rss/e;->c:[I

    aget v5, v5, v0

    .line 266
    new-instance v0, Lcom/google/zxing/oned/rss/b;

    mul-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v2, v5

    invoke-direct {v0, v2, v1}, Lcom/google/zxing/oned/rss/b;-><init>(II)V

    .line 278
    :goto_1e7
    return-object v0

    .line 268
    :cond_1e8
    and-int/lit8 v2, v0, 0x1

    if-nez v2, :cond_1f3

    const/16 v2, 0xa

    if-gt v0, v2, :cond_1f3

    const/4 v2, 0x4

    if-ge v0, v2, :cond_1f8

    .line 269
    :cond_1f3
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 271
    :cond_1f8
    rsub-int/lit8 v0, v0, 0xa

    div-int/lit8 v0, v0, 0x2

    .line 272
    sget-object v2, Lcom/google/zxing/oned/rss/e;->f:[I

    aget v2, v2, v0

    .line 273
    rsub-int/lit8 v3, v2, 0x9

    .line 274
    const/4 v4, 0x1

    invoke-static {v6, v2, v4}, Lcom/google/zxing/oned/rss/f;->a([IIZ)I

    move-result v2

    .line 275
    const/4 v4, 0x0

    invoke-static {v7, v3, v4}, Lcom/google/zxing/oned/rss/f;->a([IIZ)I

    move-result v3

    .line 276
    sget-object v4, Lcom/google/zxing/oned/rss/e;->b:[I

    aget v4, v4, v0

    .line 277
    sget-object v5, Lcom/google/zxing/oned/rss/e;->d:[I

    aget v5, v5, v0

    .line 278
    new-instance v0, Lcom/google/zxing/oned/rss/b;

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/2addr v2, v5

    invoke-direct {v0, v2, v1}, Lcom/google/zxing/oned/rss/b;-><init>(II)V

    goto :goto_1e7
.end method

.method private a(Lcom/google/zxing/common/a;ZILjava/util/Map;)Lcom/google/zxing/oned/rss/d;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 161
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->b()[I

    move-result-object v5

    const/4 v0, 0x0

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x1

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x2

    const/4 v2, 0x0

    aput v2, v5, v0

    const/4 v0, 0x3

    const/4 v2, 0x0

    aput v2, v5, v0

    invoke-virtual {p1}, Lcom/google/zxing/common/a;->a()I

    move-result v6

    const/4 v0, 0x0

    :goto_1a
    if-ge v1, v6, :cond_2a

    invoke-virtual {p1, v1}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_28

    const/4 v0, 0x1

    :goto_23
    if-eq p2, v0, :cond_2a

    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    :cond_28
    const/4 v0, 0x0

    goto :goto_23

    :cond_2a
    const/4 v2, 0x0

    move v4, v1

    move v8, v2

    move v2, v0

    move v0, v8

    :goto_2f
    if-ge v4, v6, :cond_a2

    invoke-virtual {p1, v4}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v3

    xor-int/2addr v3, v2

    if-eqz v3, :cond_48

    aget v3, v5, v0

    add-int/lit8 v3, v3, 0x1

    aput v3, v5, v0

    move v8, v2

    move v2, v1

    move v1, v8

    :goto_41
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v8, v1

    move v1, v2

    move v2, v8

    goto :goto_2f

    :cond_48
    const/4 v3, 0x3

    if-ne v0, v3, :cond_9b

    invoke-static {v5}, Lcom/google/zxing/oned/rss/e;->b([I)Z

    move-result v3

    if-eqz v3, :cond_72

    const/4 v0, 0x2

    new-array v6, v0, [I

    const/4 v0, 0x0

    aput v1, v6, v0

    const/4 v0, 0x1

    aput v4, v6, v0

    .line 162
    const/4 v0, 0x0

    aget v0, v6, v0

    invoke-virtual {p1, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v1

    const/4 v0, 0x0

    aget v0, v6, v0

    add-int/lit8 v0, v0, -0x1

    :goto_66
    if-ltz v0, :cond_aa

    invoke-virtual {p1, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    xor-int/2addr v2, v1

    if-eqz v2, :cond_aa

    add-int/lit8 v0, v0, -0x1

    goto :goto_66

    .line 161
    :cond_72
    const/4 v3, 0x0

    aget v3, v5, v3

    const/4 v7, 0x1

    aget v7, v5, v7

    add-int/2addr v3, v7

    add-int/2addr v3, v1

    const/4 v1, 0x0

    const/4 v7, 0x2

    aget v7, v5, v7

    aput v7, v5, v1

    const/4 v1, 0x1

    const/4 v7, 0x3

    aget v7, v5, v7

    aput v7, v5, v1

    const/4 v1, 0x2

    const/4 v7, 0x0

    aput v7, v5, v1

    const/4 v1, 0x3

    const/4 v7, 0x0

    aput v7, v5, v1

    add-int/lit8 v1, v0, -0x1

    :goto_90
    const/4 v0, 0x1

    aput v0, v5, v1

    if-nez v2, :cond_a0

    const/4 v0, 0x1

    :goto_96
    move v2, v3

    move v8, v0

    move v0, v1

    move v1, v8

    goto :goto_41

    :cond_9b
    add-int/lit8 v0, v0, 0x1

    move v3, v1

    move v1, v0

    goto :goto_90

    :cond_a0
    const/4 v0, 0x0

    goto :goto_96

    :cond_a2
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    .line 182
    :catch_a7
    move-exception v0

    const/4 v0, 0x0

    :goto_a9
    return-object v0

    .line 162
    :cond_aa
    add-int/lit8 v5, v0, 0x1

    const/4 v0, 0x0

    aget v0, v6, v0

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/google/zxing/oned/rss/e;->b()[I

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    invoke-static {v1, v2, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v2, 0x0

    aput v0, v1, v2

    sget-object v0, Lcom/google/zxing/oned/rss/e;->g:[[I

    invoke-static {v1, v0}, Lcom/google/zxing/oned/rss/e;->a([I[[I)I

    move-result v1

    const/4 v0, 0x1

    aget v4, v6, v0

    if-eqz p2, :cond_143

    invoke-virtual {p1}, Lcom/google/zxing/common/a;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int v3, v0, v5

    invoke-virtual {p1}, Lcom/google/zxing/common/a;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sub-int v4, v0, v4

    :goto_da
    new-instance v0, Lcom/google/zxing/oned/rss/c;

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v7, 0x0

    aput v5, v2, v7

    const/4 v5, 0x1

    const/4 v7, 0x1

    aget v7, v6, v7

    aput v7, v2, v5

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/zxing/oned/rss/c;-><init>(I[IIII)V

    .line 164
    if-nez p4, :cond_139

    const/4 v1, 0x0

    move-object v2, v1

    .line 167
    :goto_f0
    if-eqz v2, :cond_111

    .line 168
    const/4 v1, 0x0

    aget v1, v6, v1

    const/4 v3, 0x1

    aget v3, v6, v3

    add-int/2addr v1, v3

    int-to-float v1, v1

    const/high16 v3, 0x4000

    div-float/2addr v1, v3

    .line 169
    if-eqz p2, :cond_108

    .line 171
    invoke-virtual {p1}, Lcom/google/zxing/common/a;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    sub-float v1, v3, v1

    .line 173
    :cond_108
    new-instance v3, Lcom/google/zxing/g;

    int-to-float v4, p3

    invoke-direct {v3, v1, v4}, Lcom/google/zxing/g;-><init>(FF)V

    invoke-interface {v2, v3}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    .line 176
    :cond_111
    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/zxing/oned/rss/e;->a(Lcom/google/zxing/common/a;Lcom/google/zxing/oned/rss/c;Z)Lcom/google/zxing/oned/rss/b;

    move-result-object v2

    .line 177
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/zxing/oned/rss/e;->a(Lcom/google/zxing/common/a;Lcom/google/zxing/oned/rss/c;Z)Lcom/google/zxing/oned/rss/b;

    move-result-object v3

    .line 178
    new-instance v1, Lcom/google/zxing/oned/rss/d;

    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/b;->a()I

    move-result v4

    mul-int/lit16 v4, v4, 0x63d

    invoke-virtual {v3}, Lcom/google/zxing/oned/rss/b;->a()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/b;->b()I

    move-result v2

    invoke-virtual {v3}, Lcom/google/zxing/oned/rss/b;->b()I

    move-result v3

    mul-int/lit8 v3, v3, 0x4

    add-int/2addr v2, v3

    invoke-direct {v1, v4, v2, v0}, Lcom/google/zxing/oned/rss/d;-><init>(IILcom/google/zxing/oned/rss/c;)V

    move-object v0, v1

    goto/16 :goto_a9

    .line 164
    :cond_139
    sget-object v1, Lcom/google/zxing/DecodeHintType;->NEED_RESULT_POINT_CALLBACK:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/zxing/h;
    :try_end_141
    .catch Lcom/google/zxing/NotFoundException; {:try_start_1 .. :try_end_141} :catch_a7

    move-object v2, v1

    goto :goto_f0

    :cond_143
    move v3, v5

    goto :goto_da
.end method

.method private static a(Ljava/util/Collection;Lcom/google/zxing/oned/rss/d;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 89
    if-nez p1, :cond_3

    .line 103
    :cond_2
    :goto_2
    return-void

    .line 92
    :cond_3
    const/4 v1, 0x0

    .line 93
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/d;

    .line 94
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->a()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/zxing/oned/rss/d;->a()I

    move-result v4

    if-ne v3, v4, :cond_8

    .line 95
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->e()V

    .line 96
    const/4 v0, 0x1

    .line 100
    :goto_22
    if-nez v0, :cond_2

    .line 101
    invoke-interface {p0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_28
    move v0, v1

    goto :goto_22
.end method


# virtual methods
.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 68
    invoke-direct {p0, p2, v3, p1, p3}, Lcom/google/zxing/oned/rss/e;->a(Lcom/google/zxing/common/a;ZILjava/util/Map;)Lcom/google/zxing/oned/rss/d;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/google/zxing/oned/rss/e;->h:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/google/zxing/oned/rss/e;->a(Ljava/util/Collection;Lcom/google/zxing/oned/rss/d;)V

    .line 70
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->c()V

    .line 71
    invoke-direct {p0, p2, v6, p1, p3}, Lcom/google/zxing/oned/rss/e;->a(Lcom/google/zxing/common/a;ZILjava/util/Map;)Lcom/google/zxing/oned/rss/d;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/google/zxing/oned/rss/e;->i:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/google/zxing/oned/rss/e;->a(Ljava/util/Collection;Lcom/google/zxing/oned/rss/d;)V

    .line 73
    invoke-virtual {p2}, Lcom/google/zxing/common/a;->c()V

    .line 74
    iget-object v0, p0, Lcom/google/zxing/oned/rss/e;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_20
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_112

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/zxing/oned/rss/d;

    .line 75
    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/d;->d()I

    move-result v0

    if-le v0, v6, :cond_20

    .line 76
    iget-object v0, p0, Lcom/google/zxing/oned/rss/e;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_39
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/oned/rss/d;

    .line 77
    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->d()I

    move-result v2

    if-le v2, v6, :cond_39

    .line 78
    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/d;->c()Lcom/google/zxing/oned/rss/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/c;->a()I

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->c()Lcom/google/zxing/oned/rss/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/c;->a()I

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/d;->b()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->b()I

    move-result v7

    mul-int/lit8 v7, v7, 0x10

    add-int/2addr v2, v7

    rem-int/lit8 v7, v2, 0x4f

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/d;->c()Lcom/google/zxing/oned/rss/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/zxing/oned/rss/c;->a()I

    move-result v2

    mul-int/lit8 v2, v2, 0x9

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->c()Lcom/google/zxing/oned/rss/c;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/zxing/oned/rss/c;->a()I

    move-result v8

    add-int/2addr v2, v8

    const/16 v8, 0x48

    if-le v2, v8, :cond_7f

    add-int/lit8 v2, v2, -0x1

    :cond_7f
    const/16 v8, 0x8

    if-le v2, v8, :cond_85

    add-int/lit8 v2, v2, -0x1

    :cond_85
    if-ne v7, v2, :cond_b4

    move v2, v6

    :goto_88
    if-eqz v2, :cond_39

    .line 79
    const-wide/32 v4, 0x453af5

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/d;->a()I

    move-result v2

    int-to-long v7, v2

    mul-long/2addr v4, v7

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->a()I

    move-result v2

    int-to-long v7, v2

    add-long/2addr v4, v7

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v2, 0xe

    invoke-direct {v7, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    rsub-int/lit8 v2, v2, 0xd

    :goto_aa
    if-lez v2, :cond_b6

    const/16 v5, 0x30

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, -0x1

    goto :goto_aa

    :cond_b4
    move v2, v3

    .line 78
    goto :goto_88

    .line 79
    :cond_b6
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v4, v3

    move v5, v3

    :goto_bb
    const/16 v2, 0xd

    if-ge v4, v2, :cond_d0

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v2

    add-int/lit8 v2, v2, -0x30

    and-int/lit8 v8, v4, 0x1

    if-nez v8, :cond_cb

    mul-int/lit8 v2, v2, 0x3

    :cond_cb
    add-int/2addr v5, v2

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_bb

    :cond_d0
    rem-int/lit8 v2, v5, 0xa

    rsub-int/lit8 v2, v2, 0xa

    const/16 v4, 0xa

    if-ne v2, v4, :cond_d9

    move v2, v3

    :cond_d9
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/d;->c()Lcom/google/zxing/oned/rss/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/zxing/oned/rss/c;->c()[Lcom/google/zxing/g;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/d;->c()Lcom/google/zxing/oned/rss/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/oned/rss/c;->c()[Lcom/google/zxing/g;

    move-result-object v0

    new-instance v2, Lcom/google/zxing/f;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/google/zxing/g;

    aget-object v8, v1, v3

    aput-object v8, v7, v3

    aget-object v1, v1, v6

    aput-object v1, v7, v6

    const/4 v1, 0x2

    aget-object v3, v0, v3

    aput-object v3, v7, v1

    const/4 v1, 0x3

    aget-object v0, v0, v6

    aput-object v0, v7, v1

    sget-object v0, Lcom/google/zxing/BarcodeFormat;->RSS_14:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v2, v4, v5, v7, v0}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v2

    .line 85
    :cond_112
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method

.method public final a()V
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/zxing/oned/rss/e;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 108
    iget-object v0, p0, Lcom/google/zxing/oned/rss/e;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 109
    return-void
.end method
