.class public Lcom/google/zxing/qrcode/detector/FinderPatternFinder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/b;

.field private final b:Ljava/util/List;

.field private c:Z

.field private final d:[I

.field private final e:Lcom/google/zxing/h;


# direct methods
.method public constructor <init>(Lcom/google/zxing/common/b;Lcom/google/zxing/h;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a:Lcom/google/zxing/common/b;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    .line 66
    iput-object p2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->e:Lcom/google/zxing/h;

    .line 67
    return-void
.end method

.method private static a([II)F
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 191
    const/4 v0, 0x4

    aget v0, p0, v0

    sub-int v0, p1, v0

    const/4 v1, 0x3

    aget v1, p0, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x2

    aget v1, p0, v1

    int-to-float v1, v1

    const/high16 v2, 0x4000

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method private static a([I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    move v2, v0

    move v3, v0

    .line 201
    :goto_4
    const/4 v4, 0x5

    if-ge v2, v4, :cond_10

    .line 202
    aget v4, p0, v2

    .line 203
    if-nez v4, :cond_c

    .line 214
    :cond_b
    :goto_b
    return v0

    .line 206
    :cond_c
    add-int/2addr v3, v4

    .line 201
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 208
    :cond_10
    const/4 v2, 0x7

    if-lt v3, v2, :cond_b

    .line 211
    shl-int/lit8 v2, v3, 0x8

    div-int/lit8 v2, v2, 0x7

    .line 212
    div-int/lit8 v3, v2, 0x2

    .line 214
    aget v4, p0, v0

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v3, :cond_b

    aget v4, p0, v1

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v3, :cond_b

    mul-int/lit8 v4, v2, 0x3

    const/4 v5, 0x2

    aget v5, p0, v5

    shl-int/lit8 v5, v5, 0x8

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v5, v3, 0x3

    if-ge v4, v5, :cond_b

    const/4 v4, 0x3

    aget v4, p0, v4

    shl-int/lit8 v4, v4, 0x8

    sub-int v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-ge v4, v3, :cond_b

    const/4 v4, 0x4

    aget v4, p0, v4

    shl-int/lit8 v4, v4, 0x8

    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-ge v2, v3, :cond_b

    move v0, v1

    goto :goto_b
.end method

.method private a([III)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 394
    const/4 v0, 0x0

    aget v0, p1, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    add-int/2addr v0, v1

    const/4 v1, 0x2

    aget v1, p1, v1

    add-int/2addr v0, v1

    const/4 v1, 0x3

    aget v1, p1, v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    aget v1, p1, v1

    add-int v3, v0, v1

    .line 396
    invoke-static {p1, p3}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([II)F

    move-result v2

    .line 397
    float-to-int v1, v2

    const/4 v0, 0x2

    aget v4, p1, v0

    iget-object v5, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v5}, Lcom/google/zxing/common/b;->e()I

    move-result v6

    invoke-direct {p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a()[I

    move-result-object v7

    move v0, p2

    :goto_27
    if-ltz v0, :cond_39

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-eqz v8, :cond_39

    const/4 v8, 0x2

    aget v9, v7, v8

    add-int/lit8 v9, v9, 0x1

    aput v9, v7, v8

    add-int/lit8 v0, v0, -0x1

    goto :goto_27

    :cond_39
    if-gez v0, :cond_66

    const/high16 v0, 0x7fc0

    move v1, v0

    .line 398
    :goto_3e
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_260

    .line 400
    float-to-int v2, v2

    float-to-int v4, v1

    const/4 v0, 0x2

    aget v5, p1, v0

    iget-object v6, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v6}, Lcom/google/zxing/common/b;->d()I

    move-result v7

    invoke-direct {p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a()[I

    move-result-object v8

    move v0, v2

    :goto_54
    if-ltz v0, :cond_13c

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    if-eqz v9, :cond_13c

    const/4 v9, 0x2

    aget v10, v8, v9

    add-int/lit8 v10, v10, 0x1

    aput v10, v8, v9

    add-int/lit8 v0, v0, -0x1

    goto :goto_54

    .line 397
    :cond_66
    :goto_66
    if-ltz v0, :cond_7d

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-nez v8, :cond_7d

    const/4 v8, 0x1

    aget v8, v7, v8

    if-gt v8, v4, :cond_7d

    const/4 v8, 0x1

    aget v9, v7, v8

    add-int/lit8 v9, v9, 0x1

    aput v9, v7, v8

    add-int/lit8 v0, v0, -0x1

    goto :goto_66

    :cond_7d
    if-ltz v0, :cond_84

    const/4 v8, 0x1

    aget v8, v7, v8

    if-le v8, v4, :cond_88

    :cond_84
    const/high16 v0, 0x7fc0

    move v1, v0

    goto :goto_3e

    :cond_88
    :goto_88
    if-ltz v0, :cond_9f

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-eqz v8, :cond_9f

    const/4 v8, 0x0

    aget v8, v7, v8

    if-gt v8, v4, :cond_9f

    const/4 v8, 0x0

    aget v9, v7, v8

    add-int/lit8 v9, v9, 0x1

    aput v9, v7, v8

    add-int/lit8 v0, v0, -0x1

    goto :goto_88

    :cond_9f
    const/4 v0, 0x0

    aget v0, v7, v0

    if-le v0, v4, :cond_a8

    const/high16 v0, 0x7fc0

    move v1, v0

    goto :goto_3e

    :cond_a8
    add-int/lit8 v0, p2, 0x1

    :goto_aa
    if-ge v0, v6, :cond_bc

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-eqz v8, :cond_bc

    const/4 v8, 0x2

    aget v9, v7, v8

    add-int/lit8 v9, v9, 0x1

    aput v9, v7, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_aa

    :cond_bc
    if-ne v0, v6, :cond_c3

    const/high16 v0, 0x7fc0

    move v1, v0

    goto/16 :goto_3e

    :cond_c3
    :goto_c3
    if-ge v0, v6, :cond_da

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-nez v8, :cond_da

    const/4 v8, 0x3

    aget v8, v7, v8

    if-ge v8, v4, :cond_da

    const/4 v8, 0x3

    aget v9, v7, v8

    add-int/lit8 v9, v9, 0x1

    aput v9, v7, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_c3

    :cond_da
    if-eq v0, v6, :cond_e1

    const/4 v8, 0x3

    aget v8, v7, v8

    if-lt v8, v4, :cond_e6

    :cond_e1
    const/high16 v0, 0x7fc0

    move v1, v0

    goto/16 :goto_3e

    :cond_e6
    :goto_e6
    if-ge v0, v6, :cond_fd

    invoke-virtual {v5, v1, v0}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v8

    if-eqz v8, :cond_fd

    const/4 v8, 0x4

    aget v8, v7, v8

    if-ge v8, v4, :cond_fd

    const/4 v8, 0x4

    aget v9, v7, v8

    add-int/lit8 v9, v9, 0x1

    aput v9, v7, v8

    add-int/lit8 v0, v0, 0x1

    goto :goto_e6

    :cond_fd
    const/4 v1, 0x4

    aget v1, v7, v1

    if-lt v1, v4, :cond_107

    const/high16 v0, 0x7fc0

    move v1, v0

    goto/16 :goto_3e

    :cond_107
    const/4 v1, 0x0

    aget v1, v7, v1

    const/4 v4, 0x1

    aget v4, v7, v4

    add-int/2addr v1, v4

    const/4 v4, 0x2

    aget v4, v7, v4

    add-int/2addr v1, v4

    const/4 v4, 0x3

    aget v4, v7, v4

    add-int/2addr v1, v4

    const/4 v4, 0x4

    aget v4, v7, v4

    add-int/2addr v1, v4

    sub-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x5

    mul-int/lit8 v4, v3, 0x2

    if-lt v1, v4, :cond_12a

    const/high16 v0, 0x7fc0

    move v1, v0

    goto/16 :goto_3e

    :cond_12a
    invoke-static {v7}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([I)Z

    move-result v1

    if-eqz v1, :cond_137

    invoke-static {v7, v0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([II)F

    move-result v0

    move v1, v0

    goto/16 :goto_3e

    :cond_137
    const/high16 v0, 0x7fc0

    move v1, v0

    goto/16 :goto_3e

    .line 400
    :cond_13c
    if-gez v0, :cond_186

    const/high16 v0, 0x7fc0

    move v2, v0

    .line 401
    :goto_141
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_260

    .line 402
    int-to-float v0, v3

    const/high16 v3, 0x40e0

    div-float v5, v0, v3

    .line 403
    const/4 v4, 0x0

    .line 404
    const/4 v0, 0x0

    move v3, v0

    :goto_14f
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_263

    .line 405
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    .line 407
    invoke-virtual {v0, v5, v1, v2}, Lcom/google/zxing/qrcode/detector/d;->a(FFF)Z

    move-result v6

    if-eqz v6, :cond_25b

    .line 408
    iget-object v4, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/zxing/qrcode/detector/d;->b(FFF)Lcom/google/zxing/qrcode/detector/d;

    move-result-object v0

    invoke-interface {v4, v3, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 409
    const/4 v0, 0x1

    .line 413
    :goto_16f
    if-nez v0, :cond_184

    .line 414
    new-instance v0, Lcom/google/zxing/qrcode/detector/d;

    invoke-direct {v0, v2, v1, v5}, Lcom/google/zxing/qrcode/detector/d;-><init>(FFF)V

    .line 415
    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->e:Lcom/google/zxing/h;

    if-eqz v1, :cond_184

    .line 417
    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->e:Lcom/google/zxing/h;

    invoke-interface {v1, v0}, Lcom/google/zxing/h;->a(Lcom/google/zxing/g;)V

    .line 420
    :cond_184
    const/4 v0, 0x1

    .line 423
    :goto_185
    return v0

    .line 400
    :cond_186
    :goto_186
    if-ltz v0, :cond_19d

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    if-nez v9, :cond_19d

    const/4 v9, 0x1

    aget v9, v8, v9

    if-gt v9, v5, :cond_19d

    const/4 v9, 0x1

    aget v10, v8, v9

    add-int/lit8 v10, v10, 0x1

    aput v10, v8, v9

    add-int/lit8 v0, v0, -0x1

    goto :goto_186

    :cond_19d
    if-ltz v0, :cond_1a4

    const/4 v9, 0x1

    aget v9, v8, v9

    if-le v9, v5, :cond_1a8

    :cond_1a4
    const/high16 v0, 0x7fc0

    move v2, v0

    goto :goto_141

    :cond_1a8
    :goto_1a8
    if-ltz v0, :cond_1bf

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    if-eqz v9, :cond_1bf

    const/4 v9, 0x0

    aget v9, v8, v9

    if-gt v9, v5, :cond_1bf

    const/4 v9, 0x0

    aget v10, v8, v9

    add-int/lit8 v10, v10, 0x1

    aput v10, v8, v9

    add-int/lit8 v0, v0, -0x1

    goto :goto_1a8

    :cond_1bf
    const/4 v0, 0x0

    aget v0, v8, v0

    if-le v0, v5, :cond_1c9

    const/high16 v0, 0x7fc0

    move v2, v0

    goto/16 :goto_141

    :cond_1c9
    add-int/lit8 v0, v2, 0x1

    :goto_1cb
    if-ge v0, v7, :cond_1dd

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v2

    if-eqz v2, :cond_1dd

    const/4 v2, 0x2

    aget v9, v8, v2

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1cb

    :cond_1dd
    if-ne v0, v7, :cond_1e4

    const/high16 v0, 0x7fc0

    move v2, v0

    goto/16 :goto_141

    :cond_1e4
    :goto_1e4
    if-ge v0, v7, :cond_1fb

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v2

    if-nez v2, :cond_1fb

    const/4 v2, 0x3

    aget v2, v8, v2

    if-ge v2, v5, :cond_1fb

    const/4 v2, 0x3

    aget v9, v8, v2

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1e4

    :cond_1fb
    if-eq v0, v7, :cond_202

    const/4 v2, 0x3

    aget v2, v8, v2

    if-lt v2, v5, :cond_207

    :cond_202
    const/high16 v0, 0x7fc0

    move v2, v0

    goto/16 :goto_141

    :cond_207
    :goto_207
    if-ge v0, v7, :cond_21e

    invoke-virtual {v6, v0, v4}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v2

    if-eqz v2, :cond_21e

    const/4 v2, 0x4

    aget v2, v8, v2

    if-ge v2, v5, :cond_21e

    const/4 v2, 0x4

    aget v9, v8, v2

    add-int/lit8 v9, v9, 0x1

    aput v9, v8, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_207

    :cond_21e
    const/4 v2, 0x4

    aget v2, v8, v2

    if-lt v2, v5, :cond_228

    const/high16 v0, 0x7fc0

    move v2, v0

    goto/16 :goto_141

    :cond_228
    const/4 v2, 0x0

    aget v2, v8, v2

    const/4 v4, 0x1

    aget v4, v8, v4

    add-int/2addr v2, v4

    const/4 v4, 0x2

    aget v4, v8, v4

    add-int/2addr v2, v4

    const/4 v4, 0x3

    aget v4, v8, v4

    add-int/2addr v2, v4

    const/4 v4, 0x4

    aget v4, v8, v4

    add-int/2addr v2, v4

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    if-lt v2, v3, :cond_249

    const/high16 v0, 0x7fc0

    move v2, v0

    goto/16 :goto_141

    :cond_249
    invoke-static {v8}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([I)Z

    move-result v2

    if-eqz v2, :cond_256

    invoke-static {v8, v0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([II)F

    move-result v0

    move v2, v0

    goto/16 :goto_141

    :cond_256
    const/high16 v0, 0x7fc0

    move v2, v0

    goto/16 :goto_141

    .line 404
    :cond_25b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_14f

    .line 423
    :cond_260
    const/4 v0, 0x0

    goto/16 :goto_185

    :cond_263
    move v0, v4

    goto/16 :goto_16f
.end method

.method private a()[I
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 222
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    aput v2, v0, v2

    .line 223
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 224
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 225
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 226
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    const/4 v1, 0x4

    aput v2, v0, v1

    .line 227
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->d:[I

    return-object v0
.end method

.method private b()Z
    .registers 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 465
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 466
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    move v3, v4

    :goto_10
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    .line 467
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->d()I

    move-result v7

    const/4 v8, 0x2

    if-lt v7, v8, :cond_5c

    .line 468
    add-int/lit8 v3, v3, 0x1

    .line 469
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v0

    add-float/2addr v0, v1

    move v1, v3

    :goto_2b
    move v3, v1

    move v1, v0

    goto :goto_10

    .line 472
    :cond_2e
    const/4 v0, 0x3

    if-ge v3, v0, :cond_32

    .line 484
    :cond_31
    :goto_31
    return v4

    .line 479
    :cond_32
    int-to-float v0, v5

    div-float v3, v1, v0

    .line 481
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    .line 482
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v0

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    add-float/2addr v2, v0

    goto :goto_3b

    .line 484
    :cond_52
    const v0, 0x3d4ccccd

    mul-float/2addr v0, v1

    cmpg-float v0, v2, v0

    if-gtz v0, :cond_31

    const/4 v4, 0x1

    goto :goto_31

    :cond_5c
    move v0, v1

    move v1, v3

    goto :goto_2b
.end method


# virtual methods
.method final a(Ljava/util/Map;)Lcom/google/zxing/qrcode/detector/f;
    .registers 14
    .parameter

    .prologue
    .line 78
    if-eqz p1, :cond_5c

    sget-object v0, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5c

    const/4 v0, 0x1

    .line 79
    :goto_b
    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->e()I

    move-result v6

    .line 80
    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->d()I

    move-result v7

    .line 88
    mul-int/lit8 v1, v6, 0x3

    div-int/lit16 v1, v1, 0xe4

    .line 89
    const/4 v2, 0x3

    if-lt v1, v2, :cond_20

    if-eqz v0, :cond_259

    .line 90
    :cond_20
    const/4 v0, 0x3

    .line 93
    :goto_21
    const/4 v4, 0x0

    .line 94
    const/4 v1, 0x5

    new-array v8, v1, [I

    .line 95
    add-int/lit8 v3, v0, -0x1

    move v1, v0

    :goto_28
    if-ge v3, v6, :cond_159

    if-nez v4, :cond_159

    .line 97
    const/4 v0, 0x0

    const/4 v2, 0x0

    aput v2, v8, v0

    .line 98
    const/4 v0, 0x1

    const/4 v2, 0x0

    aput v2, v8, v0

    .line 99
    const/4 v0, 0x2

    const/4 v2, 0x0

    aput v2, v8, v0

    .line 100
    const/4 v0, 0x3

    const/4 v2, 0x0

    aput v2, v8, v0

    .line 101
    const/4 v0, 0x4

    const/4 v2, 0x0

    aput v2, v8, v0

    .line 102
    const/4 v0, 0x0

    .line 103
    const/4 v2, 0x0

    :goto_42
    if-ge v2, v7, :cond_13f

    .line 104
    iget-object v5, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v5, v2, v3}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v5

    if-eqz v5, :cond_5e

    .line 106
    and-int/lit8 v5, v0, 0x1

    const/4 v9, 0x1

    if-ne v5, v9, :cond_53

    .line 107
    add-int/lit8 v0, v0, 0x1

    .line 109
    :cond_53
    aget v5, v8, v0

    add-int/lit8 v5, v5, 0x1

    aput v5, v8, v0

    .line 103
    :goto_59
    add-int/lit8 v2, v2, 0x1

    goto :goto_42

    .line 78
    :cond_5c
    const/4 v0, 0x0

    goto :goto_b

    .line 111
    :cond_5e
    and-int/lit8 v5, v0, 0x1

    if-nez v5, :cond_137

    .line 112
    const/4 v5, 0x4

    if-ne v0, v5, :cond_12d

    .line 113
    invoke-static {v8}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([I)Z

    move-result v0

    if-eqz v0, :cond_110

    .line 114
    invoke-direct {p0, v8, v3, v2}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([III)Z

    move-result v0

    .line 115
    if-eqz v0, :cond_f3

    .line 118
    const/4 v5, 0x2

    .line 119
    iget-boolean v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->c:Z

    if-eqz v0, :cond_93

    .line 120
    invoke-direct {p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b()Z

    move-result v0

    .line 146
    :goto_7a
    const/4 v1, 0x0

    .line 147
    const/4 v4, 0x0

    const/4 v9, 0x0

    aput v9, v8, v4

    .line 148
    const/4 v4, 0x1

    const/4 v9, 0x0

    aput v9, v8, v4

    .line 149
    const/4 v4, 0x2

    const/4 v9, 0x0

    aput v9, v8, v4

    .line 150
    const/4 v4, 0x3

    const/4 v9, 0x0

    aput v9, v8, v4

    .line 151
    const/4 v4, 0x4

    const/4 v9, 0x0

    aput v9, v8, v4

    move v4, v0

    move v0, v1

    move v1, v5

    .line 152
    goto :goto_59

    .line 122
    :cond_93
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_f1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v1, v0

    :cond_a4
    :goto_a4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->d()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_a4

    if-nez v1, :cond_bb

    move-object v1, v0

    goto :goto_a4

    :cond_bb
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->c:Z

    invoke-virtual {v1}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v9

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->a()F

    move-result v10

    sub-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-virtual {v1}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v1

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->b()F

    move-result v0

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float v0, v9, v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 123
    :goto_de
    const/4 v1, 0x2

    aget v1, v8, v1

    if-le v0, v1, :cond_255

    .line 132
    const/4 v1, 0x2

    aget v1, v8, v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    add-int v1, v3, v0

    .line 133
    add-int/lit8 v0, v7, -0x1

    :goto_ed
    move v2, v0

    move v3, v1

    move v0, v4

    .line 135
    goto :goto_7a

    .line 122
    :cond_f1
    const/4 v0, 0x0

    goto :goto_de

    .line 137
    :cond_f3
    const/4 v0, 0x0

    const/4 v5, 0x2

    aget v5, v8, v5

    aput v5, v8, v0

    .line 138
    const/4 v0, 0x1

    const/4 v5, 0x3

    aget v5, v8, v5

    aput v5, v8, v0

    .line 139
    const/4 v0, 0x2

    const/4 v5, 0x4

    aget v5, v8, v5

    aput v5, v8, v0

    .line 140
    const/4 v0, 0x3

    const/4 v5, 0x1

    aput v5, v8, v0

    .line 141
    const/4 v0, 0x4

    const/4 v5, 0x0

    aput v5, v8, v0

    .line 142
    const/4 v0, 0x3

    .line 143
    goto/16 :goto_59

    .line 153
    :cond_110
    const/4 v0, 0x0

    const/4 v5, 0x2

    aget v5, v8, v5

    aput v5, v8, v0

    .line 154
    const/4 v0, 0x1

    const/4 v5, 0x3

    aget v5, v8, v5

    aput v5, v8, v0

    .line 155
    const/4 v0, 0x2

    const/4 v5, 0x4

    aget v5, v8, v5

    aput v5, v8, v0

    .line 156
    const/4 v0, 0x3

    const/4 v5, 0x1

    aput v5, v8, v0

    .line 157
    const/4 v0, 0x4

    const/4 v5, 0x0

    aput v5, v8, v0

    .line 158
    const/4 v0, 0x3

    goto/16 :goto_59

    .line 161
    :cond_12d
    add-int/lit8 v0, v0, 0x1

    aget v5, v8, v0

    add-int/lit8 v5, v5, 0x1

    aput v5, v8, v0

    goto/16 :goto_59

    .line 164
    :cond_137
    aget v5, v8, v0

    add-int/lit8 v5, v5, 0x1

    aput v5, v8, v0

    goto/16 :goto_59

    .line 168
    :cond_13f
    invoke-static {v8}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([I)Z

    move-result v0

    if-eqz v0, :cond_156

    .line 169
    invoke-direct {p0, v8, v3, v7}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->a([III)Z

    move-result v0

    .line 170
    if-eqz v0, :cond_156

    .line 171
    const/4 v0, 0x0

    aget v1, v8, v0

    .line 172
    iget-boolean v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->c:Z

    if-eqz v0, :cond_156

    .line 174
    invoke-direct {p0}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b()Z

    move-result v4

    .line 95
    :cond_156
    add-int/2addr v3, v1

    goto/16 :goto_28

    .line 180
    :cond_159
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x3

    if-ge v3, v0, :cond_167

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_167
    const/4 v0, 0x3

    if-le v3, v0, :cond_1db

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    move v1, v0

    :goto_174
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_189

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v0

    add-float/2addr v2, v0

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    move v1, v0

    goto :goto_174

    :cond_189
    int-to-float v0, v3

    div-float/2addr v2, v0

    int-to-float v0, v3

    div-float v0, v1, v0

    mul-float v1, v2, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    new-instance v3, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$FurthestFromAverageComparator;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v4}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$FurthestFromAverageComparator;-><init>(FLcom/google/zxing/qrcode/detector/e;)V

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const v1, 0x3e4ccccd

    mul-float/2addr v1, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/4 v1, 0x0

    :goto_1ab
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1db

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x3

    if-le v0, v4, :cond_1db

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v0

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_253

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v1, -0x1

    :goto_1d8
    add-int/lit8 v1, v0, 0x1

    goto :goto_1ab

    :cond_1db
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_223

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1ec
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1ff

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/detector/d;->c()F

    move-result v0

    add-float/2addr v0, v1

    move v1, v0

    goto :goto_1ec

    :cond_1ff
    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iget-object v1, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    new-instance v2, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$CenterComparator;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/zxing/qrcode/detector/FinderPatternFinder$CenterComparator;-><init>(FLcom/google/zxing/qrcode/detector/e;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_223
    const/4 v0, 0x3

    new-array v1, v0, [Lcom/google/zxing/qrcode/detector/d;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    aput-object v0, v1, v2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    aput-object v0, v1, v2

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/google/zxing/qrcode/detector/FinderPatternFinder;->b:Ljava/util/List;

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/qrcode/detector/d;

    aput-object v0, v1, v2

    .line 181
    invoke-static {v1}, Lcom/google/zxing/g;->a([Lcom/google/zxing/g;)V

    .line 183
    new-instance v0, Lcom/google/zxing/qrcode/detector/f;

    invoke-direct {v0, v1}, Lcom/google/zxing/qrcode/detector/f;-><init>([Lcom/google/zxing/qrcode/detector/d;)V

    return-object v0

    :cond_253
    move v0, v1

    goto :goto_1d8

    :cond_255
    move v0, v2

    move v1, v3

    goto/16 :goto_ed

    :cond_259
    move v0, v1

    goto/16 :goto_21
.end method
