.class final Lcom/google/zxing/qrcode/decoder/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/zxing/common/b;

.field private b:Lcom/google/zxing/qrcode/decoder/o;

.field private c:Lcom/google/zxing/qrcode/decoder/n;


# direct methods
.method constructor <init>(Lcom/google/zxing/common/b;)V
    .registers 4
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Lcom/google/zxing/common/b;->e()I

    move-result v0

    .line 37
    const/16 v1, 0x15

    if-lt v0, v1, :cond_10

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_15

    .line 38
    :cond_10
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 40
    :cond_15
    iput-object p1, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    .line 41
    return-void
.end method

.method private a(III)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_d

    shl-int/lit8 v0, p3, 0x1

    or-int/lit8 v0, v0, 0x1

    :goto_c
    return v0

    :cond_d
    shl-int/lit8 v0, p3, 0x1

    goto :goto_c
.end method


# virtual methods
.method final a()Lcom/google/zxing/qrcode/decoder/n;
    .registers 7

    .prologue
    const/4 v4, 0x7

    const/4 v1, 0x0

    const/16 v5, 0x8

    .line 52
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->c:Lcom/google/zxing/qrcode/decoder/n;

    if-eqz v0, :cond_b

    .line 53
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->c:Lcom/google/zxing/qrcode/decoder/n;

    .line 83
    :goto_a
    return-object v0

    :cond_b
    move v0, v1

    move v2, v1

    .line 58
    :goto_d
    const/4 v3, 0x6

    if-ge v0, v3, :cond_17

    .line 59
    invoke-direct {p0, v0, v5, v2}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v2

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 62
    :cond_17
    invoke-direct {p0, v4, v5, v2}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v0

    .line 63
    invoke-direct {p0, v5, v5, v0}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v0

    .line 64
    invoke-direct {p0, v5, v4, v0}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v2

    .line 66
    const/4 v0, 0x5

    :goto_24
    if-ltz v0, :cond_2d

    .line 67
    invoke-direct {p0, v5, v0, v2}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v2

    .line 66
    add-int/lit8 v0, v0, -0x1

    goto :goto_24

    .line 71
    :cond_2d
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v0}, Lcom/google/zxing/common/b;->e()I

    move-result v3

    .line 73
    add-int/lit8 v4, v3, -0x7

    .line 74
    add-int/lit8 v0, v3, -0x1

    :goto_37
    if-lt v0, v4, :cond_40

    .line 75
    invoke-direct {p0, v5, v0, v1}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v1

    .line 74
    add-int/lit8 v0, v0, -0x1

    goto :goto_37

    .line 77
    :cond_40
    add-int/lit8 v0, v3, -0x8

    :goto_42
    if-ge v0, v3, :cond_4b

    .line 78
    invoke-direct {p0, v0, v5, v1}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v1

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_42

    .line 81
    :cond_4b
    invoke-static {v2, v1}, Lcom/google/zxing/qrcode/decoder/n;->b(II)Lcom/google/zxing/qrcode/decoder/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->c:Lcom/google/zxing/qrcode/decoder/n;

    .line 82
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->c:Lcom/google/zxing/qrcode/decoder/n;

    if-eqz v0, :cond_58

    .line 83
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->c:Lcom/google/zxing/qrcode/decoder/n;

    goto :goto_a

    .line 85
    :cond_58
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method final b()Lcom/google/zxing/qrcode/decoder/o;
    .registers 8

    .prologue
    const/4 v1, 0x5

    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->b:Lcom/google/zxing/qrcode/decoder/o;

    if-eqz v0, :cond_9

    .line 98
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->b:Lcom/google/zxing/qrcode/decoder/o;

    .line 134
    :goto_8
    return-object v0

    .line 101
    :cond_9
    iget-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v0}, Lcom/google/zxing/common/b;->e()I

    move-result v5

    .line 103
    add-int/lit8 v0, v5, -0x11

    shr-int/lit8 v0, v0, 0x2

    .line 104
    const/4 v3, 0x6

    if-gt v0, v3, :cond_1b

    .line 105
    invoke-static {v0}, Lcom/google/zxing/qrcode/decoder/o;->b(I)Lcom/google/zxing/qrcode/decoder/o;

    move-result-object v0

    goto :goto_8

    .line 110
    :cond_1b
    add-int/lit8 v6, v5, -0xb

    move v4, v1

    move v3, v2

    .line 111
    :goto_1f
    if-ltz v4, :cond_30

    .line 112
    add-int/lit8 v0, v5, -0x9

    :goto_23
    if-lt v0, v6, :cond_2c

    .line 113
    invoke-direct {p0, v0, v4, v3}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v3

    .line 112
    add-int/lit8 v0, v0, -0x1

    goto :goto_23

    .line 111
    :cond_2c
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1f

    .line 117
    :cond_30
    invoke-static {v3}, Lcom/google/zxing/qrcode/decoder/o;->c(I)Lcom/google/zxing/qrcode/decoder/o;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_3f

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/o;->d()I

    move-result v3

    if-ne v3, v5, :cond_3f

    .line 119
    iput-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->b:Lcom/google/zxing/qrcode/decoder/o;

    goto :goto_8

    :cond_3f
    move v0, v1

    move v1, v2

    .line 125
    :goto_41
    if-ltz v0, :cond_53

    .line 126
    add-int/lit8 v2, v5, -0x9

    :goto_45
    if-lt v2, v6, :cond_50

    .line 127
    invoke-direct {p0, v0, v2, v1}, Lcom/google/zxing/qrcode/decoder/a;->a(III)I

    move-result v3

    .line 126
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v3

    goto :goto_45

    .line 125
    :cond_50
    add-int/lit8 v0, v0, -0x1

    goto :goto_41

    .line 131
    :cond_53
    invoke-static {v1}, Lcom/google/zxing/qrcode/decoder/o;->c(I)Lcom/google/zxing/qrcode/decoder/o;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_62

    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/o;->d()I

    move-result v1

    if-ne v1, v5, :cond_62

    .line 133
    iput-object v0, p0, Lcom/google/zxing/qrcode/decoder/a;->b:Lcom/google/zxing/qrcode/decoder/o;

    goto :goto_8

    .line 136
    :cond_62
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0
.end method

.method final c()[B
    .registers 16

    .prologue
    const/4 v4, 0x0

    .line 153
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/decoder/a;->a()Lcom/google/zxing/qrcode/decoder/n;

    move-result-object v0

    .line 154
    invoke-virtual {p0}, Lcom/google/zxing/qrcode/decoder/a;->b()Lcom/google/zxing/qrcode/decoder/o;

    move-result-object v9

    .line 158
    invoke-virtual {v0}, Lcom/google/zxing/qrcode/decoder/n;->b()B

    move-result v0

    invoke-static {v0}, Lcom/google/zxing/qrcode/decoder/c;->a(I)Lcom/google/zxing/qrcode/decoder/c;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v1}, Lcom/google/zxing/common/b;->e()I

    move-result v10

    .line 160
    iget-object v1, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    invoke-virtual {v0, v1, v10}, Lcom/google/zxing/qrcode/decoder/c;->a(Lcom/google/zxing/common/b;I)V

    .line 162
    invoke-virtual {v9}, Lcom/google/zxing/qrcode/decoder/o;->e()Lcom/google/zxing/common/b;

    move-result-object v11

    .line 164
    const/4 v1, 0x1

    .line 165
    invoke-virtual {v9}, Lcom/google/zxing/qrcode/decoder/o;->c()I

    move-result v0

    new-array v12, v0, [B

    .line 170
    add-int/lit8 v0, v10, -0x1

    move v3, v4

    move v5, v4

    move v6, v4

    move v8, v1

    :goto_2d
    if-lez v0, :cond_72

    .line 171
    const/4 v1, 0x6

    if-ne v0, v1, :cond_34

    .line 174
    add-int/lit8 v0, v0, -0x1

    :cond_34
    move v2, v4

    .line 177
    :goto_35
    if-ge v2, v10, :cond_6c

    .line 178
    if-eqz v8, :cond_67

    add-int/lit8 v1, v10, -0x1

    sub-int/2addr v1, v2

    :goto_3c
    move v7, v4

    .line 179
    :goto_3d
    const/4 v13, 0x2

    if-ge v7, v13, :cond_69

    .line 181
    sub-int v13, v0, v7

    invoke-virtual {v11, v13, v1}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v13

    if-nez v13, :cond_64

    .line 183
    add-int/lit8 v3, v3, 0x1

    .line 184
    shl-int/lit8 v5, v5, 0x1

    .line 185
    iget-object v13, p0, Lcom/google/zxing/qrcode/decoder/a;->a:Lcom/google/zxing/common/b;

    sub-int v14, v0, v7

    invoke-virtual {v13, v14, v1}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v13

    if-eqz v13, :cond_58

    .line 186
    or-int/lit8 v5, v5, 0x1

    .line 189
    :cond_58
    const/16 v13, 0x8

    if-ne v3, v13, :cond_64

    .line 190
    add-int/lit8 v3, v6, 0x1

    int-to-byte v5, v5

    aput-byte v5, v12, v6

    move v5, v4

    move v6, v3

    move v3, v4

    .line 179
    :cond_64
    add-int/lit8 v7, v7, 0x1

    goto :goto_3d

    :cond_67
    move v1, v2

    .line 178
    goto :goto_3c

    .line 177
    :cond_69
    add-int/lit8 v2, v2, 0x1

    goto :goto_35

    .line 197
    :cond_6c
    xor-int/lit8 v1, v8, 0x1

    .line 170
    add-int/lit8 v0, v0, -0x2

    move v8, v1

    goto :goto_2d

    .line 199
    :cond_72
    invoke-virtual {v9}, Lcom/google/zxing/qrcode/decoder/o;->c()I

    move-result v0

    if-eq v6, v0, :cond_7d

    .line 200
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 202
    :cond_7d
    return-object v12
.end method
