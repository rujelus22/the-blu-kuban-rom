.class public final Lcom/google/zxing/aztec/decoder/Decoder;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# instance fields
.field private j:I

.field private k:I

.field private l:Lcom/google/zxing/aztec/a;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x5

    .line 44
    new-array v0, v3, [I

    fill-array-data v0, :array_364

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->a:[I

    .line 48
    const/16 v0, 0x21

    new-array v0, v0, [I

    fill-array-data v0, :array_372

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->b:[I

    .line 53
    new-array v0, v3, [I

    fill-array-data v0, :array_3b8

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->c:[I

    .line 57
    const/16 v0, 0x21

    new-array v0, v0, [I

    fill-array-data v0, :array_3c6

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->d:[I

    .line 62
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CTRL_PS"

    aput-object v1, v0, v4

    const-string v1, " "

    aput-object v1, v0, v5

    const-string v1, "A"

    aput-object v1, v0, v6

    const-string v1, "B"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "C"

    aput-object v2, v0, v1

    const-string v1, "D"

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "E"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "F"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "G"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "H"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "I"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "J"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "K"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "L"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "M"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "N"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "O"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "P"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Q"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "R"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "S"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "T"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "U"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "V"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "W"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "X"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "Y"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "Z"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CTRL_LL"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CTRL_ML"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CTRL_DL"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CTRL_BS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->e:[Ljava/lang/String;

    .line 67
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CTRL_PS"

    aput-object v1, v0, v4

    const-string v1, " "

    aput-object v1, v0, v5

    const-string v1, "a"

    aput-object v1, v0, v6

    const-string v1, "b"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "c"

    aput-object v2, v0, v1

    const-string v1, "d"

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "e"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "f"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "g"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "h"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "i"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "j"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "k"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "l"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "m"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "n"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "o"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "p"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "q"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "r"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "s"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "t"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "u"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "w"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "x"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "y"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "z"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CTRL_US"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CTRL_ML"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CTRL_DL"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CTRL_BS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->f:[Ljava/lang/String;

    .line 72
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CTRL_PS"

    aput-object v1, v0, v4

    const-string v1, " "

    aput-object v1, v0, v5

    const-string v1, "\u0001"

    aput-object v1, v0, v6

    const-string v1, "\u0002"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "\u0003"

    aput-object v2, v0, v1

    const-string v1, "\u0004"

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "\u0005"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\u0006"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\u0007"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\u0008"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\t"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\n"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\u000b"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\u000c"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "\r"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "\u001b"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "\u001c"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "\u001d"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "\u001e"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "\u001f"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "@"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "^"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "_"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "`"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "~"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "\u007f"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CTRL_LL"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CTRL_UL"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CTRL_PL"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CTRL_BS"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->g:[Ljava/lang/String;

    .line 78
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v4

    const-string v1, "\r"

    aput-object v1, v0, v5

    const-string v1, "\r\n"

    aput-object v1, v0, v6

    const-string v1, ". "

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, ", "

    aput-object v2, v0, v1

    const-string v1, ": "

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "!"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "\""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "#"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "$"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "%"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "&"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\'"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "("

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, ")"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "*"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "+"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "-"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "/"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, ":"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, ";"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "<"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "="

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "?"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "["

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "]"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "{"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "}"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CTRL_UL"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->h:[Ljava/lang/String;

    .line 83
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CTRL_PS"

    aput-object v1, v0, v4

    const-string v1, " "

    aput-object v1, v0, v5

    const-string v1, "0"

    aput-object v1, v0, v6

    const-string v1, "1"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "2"

    aput-object v2, v0, v1

    const-string v1, "3"

    aput-object v1, v0, v3

    const/4 v1, 0x6

    const-string v2, "4"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "5"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "6"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "7"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "8"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "9"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, ","

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "."

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CTRL_UL"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CTRL_US"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->i:[Ljava/lang/String;

    return-void

    .line 44
    nop

    :array_364
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x68t 0x0t 0x0t 0x0t
        0xf0t 0x0t 0x0t 0x0t
        0x98t 0x1t 0x0t 0x0t
        0x60t 0x2t 0x0t 0x0t
    .end array-data

    .line 48
    :array_372
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x80t 0x0t 0x0t 0x0t
        0x20t 0x1t 0x0t 0x0t
        0xe0t 0x1t 0x0t 0x0t
        0xc0t 0x2t 0x0t 0x0t
        0xc0t 0x3t 0x0t 0x0t
        0xe0t 0x4t 0x0t 0x0t
        0x20t 0x6t 0x0t 0x0t
        0x80t 0x7t 0x0t 0x0t
        0x0t 0x9t 0x0t 0x0t
        0xa0t 0xat 0x0t 0x0t
        0x60t 0xct 0x0t 0x0t
        0x40t 0xet 0x0t 0x0t
        0x40t 0x10t 0x0t 0x0t
        0x60t 0x12t 0x0t 0x0t
        0xa0t 0x14t 0x0t 0x0t
        0x0t 0x17t 0x0t 0x0t
        0x80t 0x19t 0x0t 0x0t
        0x20t 0x1ct 0x0t 0x0t
        0xe0t 0x1et 0x0t 0x0t
        0xc0t 0x21t 0x0t 0x0t
        0xc0t 0x24t 0x0t 0x0t
        0xe0t 0x27t 0x0t 0x0t
        0x20t 0x2bt 0x0t 0x0t
        0x80t 0x2et 0x0t 0x0t
        0x0t 0x32t 0x0t 0x0t
        0xa0t 0x35t 0x0t 0x0t
        0x60t 0x39t 0x0t 0x0t
        0x40t 0x3dt 0x0t 0x0t
        0x40t 0x41t 0x0t 0x0t
        0x60t 0x45t 0x0t 0x0t
        0xa0t 0x49t 0x0t 0x0t
        0x0t 0x4et 0x0t 0x0t
    .end array-data

    .line 53
    :array_3b8
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x11t 0x0t 0x0t 0x0t
        0x28t 0x0t 0x0t 0x0t
        0x33t 0x0t 0x0t 0x0t
        0x4ct 0x0t 0x0t 0x0t
    .end array-data

    .line 57
    :array_3c6
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x15t 0x0t 0x0t 0x0t
        0x30t 0x0t 0x0t 0x0t
        0x3ct 0x0t 0x0t 0x0t
        0x58t 0x0t 0x0t 0x0t
        0x78t 0x0t 0x0t 0x0t
        0x9ct 0x0t 0x0t 0x0t
        0xc4t 0x0t 0x0t 0x0t
        0xf0t 0x0t 0x0t 0x0t
        0xe6t 0x0t 0x0t 0x0t
        0x10t 0x1t 0x0t 0x0t
        0x3ct 0x1t 0x0t 0x0t
        0x6ct 0x1t 0x0t 0x0t
        0xa0t 0x1t 0x0t 0x0t
        0xd6t 0x1t 0x0t 0x0t
        0x10t 0x2t 0x0t 0x0t
        0x4ct 0x2t 0x0t 0x0t
        0x8ct 0x2t 0x0t 0x0t
        0xd0t 0x2t 0x0t 0x0t
        0x16t 0x3t 0x0t 0x0t
        0x60t 0x3t 0x0t 0x0t
        0xact 0x3t 0x0t 0x0t
        0xfct 0x3t 0x0t 0x0t
        0x98t 0x3t 0x0t 0x0t
        0xe0t 0x3t 0x0t 0x0t
        0x2at 0x4t 0x0t 0x0t
        0x78t 0x4t 0x0t 0x0t
        0xc8t 0x4t 0x0t 0x0t
        0x1at 0x5t 0x0t 0x0t
        0x70t 0x5t 0x0t 0x0t
        0xc8t 0x5t 0x0t 0x0t
        0x22t 0x6t 0x0t 0x0t
        0x80t 0x6t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method private static a([ZII)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 462
    const/4 v0, 0x0

    move v1, p1

    .line 464
    :goto_2
    add-int v2, p1, p2

    if-ge v1, v2, :cond_11

    .line 465
    shl-int/lit8 v0, v0, 0x1

    .line 466
    aget-boolean v2, p0, v1

    if-eqz v2, :cond_e

    .line 467
    add-int/lit8 v0, v0, 0x1

    .line 464
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 471
    :cond_11
    return v0
.end method

.method private a([Z)[Z
    .registers 15
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 277
    iget-object v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a;->a()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_60

    .line 278
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    .line 279
    sget-object v0, Lcom/google/zxing/common/reedsolomon/a;->c:Lcom/google/zxing/common/reedsolomon/a;

    .line 291
    :goto_12
    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->b()I

    move-result v9

    .line 295
    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->c()Z

    move-result v1

    if-eqz v1, :cond_85

    .line 296
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder;->a:[I

    iget-object v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v2}, Lcom/google/zxing/aztec/a;->a()I

    move-result v2

    aget v1, v1, v2

    iget v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->j:I

    iget v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    mul-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 297
    sget-object v2, Lcom/google/zxing/aztec/decoder/Decoder;->c:[I

    iget-object v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v3}, Lcom/google/zxing/aztec/a;->a()I

    move-result v3

    aget v2, v2, v3

    sub-int/2addr v2, v9

    .line 303
    :goto_3b
    iget v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->j:I

    new-array v10, v3, [I

    move v3, v4

    .line 304
    :goto_40
    iget v5, p0, Lcom/google/zxing/aztec/decoder/Decoder;->j:I

    if-ge v3, v5, :cond_a4

    move v5, v6

    move v7, v6

    .line 306
    :goto_46
    iget v8, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    if-gt v5, v8, :cond_a1

    .line 307
    iget v8, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    mul-int/2addr v8, v3

    iget v11, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    add-int/2addr v8, v11

    sub-int/2addr v8, v5

    add-int/2addr v8, v1

    aget-boolean v8, p1, v8

    if-eqz v8, :cond_5b

    .line 308
    aget v8, v10, v3

    add-int/2addr v8, v7

    aput v8, v10, v3

    .line 310
    :cond_5b
    shl-int/lit8 v7, v7, 0x1

    .line 306
    add-int/lit8 v5, v5, 0x1

    goto :goto_46

    .line 280
    :cond_60
    iget-object v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a;->a()I

    move-result v0

    if-gt v0, v2, :cond_6d

    .line 281
    iput v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    .line 282
    sget-object v0, Lcom/google/zxing/common/reedsolomon/a;->g:Lcom/google/zxing/common/reedsolomon/a;

    goto :goto_12

    .line 283
    :cond_6d
    iget-object v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a;->a()I

    move-result v0

    const/16 v1, 0x16

    if-gt v0, v1, :cond_7e

    .line 284
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    .line 285
    sget-object v0, Lcom/google/zxing/common/reedsolomon/a;->b:Lcom/google/zxing/common/reedsolomon/a;

    goto :goto_12

    .line 287
    :cond_7e
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    .line 288
    sget-object v0, Lcom/google/zxing/common/reedsolomon/a;->a:Lcom/google/zxing/common/reedsolomon/a;

    goto :goto_12

    .line 299
    :cond_85
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder;->b:[I

    iget-object v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v2}, Lcom/google/zxing/aztec/a;->a()I

    move-result v2

    aget v1, v1, v2

    iget v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->j:I

    iget v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    mul-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 300
    sget-object v2, Lcom/google/zxing/aztec/decoder/Decoder;->d:[I

    iget-object v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v3}, Lcom/google/zxing/aztec/a;->a()I

    move-result v3

    aget v2, v2, v3

    sub-int/2addr v2, v9

    goto :goto_3b

    .line 304
    :cond_a1
    add-int/lit8 v3, v3, 0x1

    goto :goto_40

    .line 319
    :cond_a4
    :try_start_a4
    new-instance v1, Lcom/google/zxing/common/reedsolomon/c;

    invoke-direct {v1, v0}, Lcom/google/zxing/common/reedsolomon/c;-><init>(Lcom/google/zxing/common/reedsolomon/a;)V

    .line 320
    invoke-virtual {v1, v10, v2}, Lcom/google/zxing/common/reedsolomon/c;->a([II)V
    :try_end_ac
    .catch Lcom/google/zxing/common/reedsolomon/ReedSolomonException; {:try_start_a4 .. :try_end_ac} :catch_d9

    .line 326
    iput v4, p0, Lcom/google/zxing/aztec/decoder/Decoder;->m:I

    .line 328
    iget v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    mul-int/2addr v0, v9

    new-array v11, v0, [Z

    move v8, v4

    move v0, v4

    .line 329
    :goto_b5
    if-ge v8, v9, :cond_104

    .line 333
    iget v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    add-int/lit8 v1, v1, -0x1

    shl-int v1, v6, v1

    move v2, v4

    move v3, v1

    move v5, v4

    move v1, v0

    move v0, v4

    .line 335
    :goto_c2
    iget v7, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    if-ge v2, v7, :cond_ff

    .line 337
    aget v7, v10, v8

    and-int/2addr v7, v3

    if-ne v7, v3, :cond_df

    move v7, v6

    .line 339
    :goto_cc
    iget v12, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    add-int/lit8 v12, v12, -0x1

    if-ne v5, v12, :cond_f0

    .line 341
    if-ne v7, v0, :cond_e1

    .line 343
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    .line 322
    :catch_d9
    move-exception v0

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_df
    move v7, v4

    .line 337
    goto :goto_cc

    .line 348
    :cond_e1
    add-int/lit8 v1, v1, 0x1

    .line 349
    iget v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->m:I

    move v0, v4

    move v5, v4

    .line 363
    :goto_eb
    ushr-int/lit8 v3, v3, 0x1

    .line 335
    add-int/lit8 v2, v2, 0x1

    goto :goto_c2

    .line 352
    :cond_f0
    if-ne v0, v7, :cond_fc

    .line 353
    add-int/lit8 v5, v5, 0x1

    .line 359
    :goto_f4
    iget v12, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    mul-int/2addr v12, v8

    add-int/2addr v12, v2

    sub-int/2addr v12, v1

    aput-boolean v7, v11, v12

    goto :goto_eb

    :cond_fc
    move v0, v7

    move v5, v6

    .line 356
    goto :goto_f4

    .line 329
    :cond_ff
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v0, v1

    goto :goto_b5

    .line 367
    :cond_104
    return-object v11
.end method


# virtual methods
.method public final a(Lcom/google/zxing/aztec/a;)Lcom/google/zxing/common/d;
    .registers 16
    .parameter

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    .line 94
    invoke-virtual {p1}, Lcom/google/zxing/aztec/a;->d()Lcom/google/zxing/common/b;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->c()Z

    move-result v1

    if-nez v1, :cond_6c

    .line 97
    iget-object v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v0}, Lcom/google/zxing/aztec/a;->d()Lcom/google/zxing/common/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/zxing/common/b;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x10

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    new-instance v4, Lcom/google/zxing/common/b;

    invoke-virtual {v5}, Lcom/google/zxing/common/b;->d()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {v5}, Lcom/google/zxing/common/b;->e()I

    move-result v2

    sub-int v0, v2, v0

    invoke-direct {v4, v1, v0}, Lcom/google/zxing/common/b;-><init>(II)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_34
    invoke-virtual {v5}, Lcom/google/zxing/common/b;->d()I

    move-result v2

    if-ge v0, v2, :cond_6b

    invoke-virtual {v5}, Lcom/google/zxing/common/b;->d()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, v0

    rem-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_68

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_47
    invoke-virtual {v5}, Lcom/google/zxing/common/b;->e()I

    move-result v6

    if-ge v2, v6, :cond_66

    invoke-virtual {v5}, Lcom/google/zxing/common/b;->d()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v6, v2

    rem-int/lit8 v6, v6, 0x10

    if-eqz v6, :cond_63

    invoke-virtual {v5, v0, v2}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v6

    if-eqz v6, :cond_61

    invoke-virtual {v4, v1, v3}, Lcom/google/zxing/common/b;->b(II)V

    :cond_61
    add-int/lit8 v3, v3, 0x1

    :cond_63
    add-int/lit8 v2, v2, 0x1

    goto :goto_47

    :cond_66
    add-int/lit8 v1, v1, 0x1

    :cond_68
    add-int/lit8 v0, v0, 0x1

    goto :goto_34

    :cond_6b
    move-object v0, v4

    .line 100
    :cond_6c
    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->c()Z

    move-result v1

    if-eqz v1, :cond_da

    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->a()I

    move-result v1

    sget-object v2, Lcom/google/zxing/aztec/decoder/Decoder;->a:[I

    array-length v2, v2

    if-le v1, v2, :cond_84

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_84
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder;->a:[I

    iget-object v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v2}, Lcom/google/zxing/aztec/a;->a()I

    move-result v2

    aget v1, v1, v2

    new-array v1, v1, [Z

    sget-object v2, Lcom/google/zxing/aztec/decoder/Decoder;->c:[I

    iget-object v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v3}, Lcom/google/zxing/aztec/a;->a()I

    move-result v3

    aget v2, v2, v3

    iput v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->j:I

    :goto_9c
    iget-object v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v2}, Lcom/google/zxing/aztec/a;->a()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/zxing/common/b;->e()I

    move-result v4

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_a8
    if-eqz v5, :cond_152

    const/4 v7, 0x0

    const/4 v6, 0x0

    :goto_ac
    mul-int/lit8 v8, v4, 0x2

    add-int/lit8 v8, v8, -0x4

    if-ge v6, v8, :cond_103

    add-int v8, v3, v6

    add-int v9, v2, v7

    div-int/lit8 v10, v6, 0x2

    add-int/2addr v10, v2

    invoke-virtual {v0, v9, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    aput-boolean v9, v1, v8

    mul-int/lit8 v8, v4, 0x2

    add-int/2addr v8, v3

    add-int/lit8 v8, v8, -0x4

    add-int/2addr v8, v6

    div-int/lit8 v9, v6, 0x2

    add-int/2addr v9, v2

    add-int v10, v2, v4

    add-int/lit8 v10, v10, -0x1

    sub-int/2addr v10, v7

    invoke-virtual {v0, v9, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    aput-boolean v9, v1, v8

    add-int/lit8 v7, v7, 0x1

    rem-int/lit8 v7, v7, 0x2

    add-int/lit8 v6, v6, 0x1

    goto :goto_ac

    :cond_da
    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->a()I

    move-result v1

    sget-object v2, Lcom/google/zxing/aztec/decoder/Decoder;->b:[I

    array-length v2, v2

    if-le v1, v2, :cond_ea

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_ea
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder;->b:[I

    iget-object v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v2}, Lcom/google/zxing/aztec/a;->a()I

    move-result v2

    aget v1, v1, v2

    new-array v1, v1, [Z

    sget-object v2, Lcom/google/zxing/aztec/decoder/Decoder;->d:[I

    iget-object v3, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v3}, Lcom/google/zxing/aztec/a;->a()I

    move-result v3

    aget v2, v2, v3

    iput v2, p0, Lcom/google/zxing/aztec/decoder/Decoder;->j:I

    goto :goto_9c

    :cond_103
    const/4 v7, 0x0

    mul-int/lit8 v6, v4, 0x2

    add-int/lit8 v6, v6, 0x1

    :goto_108
    const/4 v8, 0x5

    if-le v6, v8, :cond_145

    mul-int/lit8 v8, v4, 0x4

    add-int/2addr v8, v3

    add-int/lit8 v8, v8, -0x8

    mul-int/lit8 v9, v4, 0x2

    sub-int/2addr v9, v6

    add-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x1

    add-int v9, v2, v4

    add-int/lit8 v9, v9, -0x1

    sub-int/2addr v9, v7

    div-int/lit8 v10, v6, 0x2

    add-int/2addr v10, v2

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v0, v9, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    aput-boolean v9, v1, v8

    mul-int/lit8 v8, v4, 0x6

    add-int/2addr v8, v3

    add-int/lit8 v8, v8, -0xc

    mul-int/lit8 v9, v4, 0x2

    sub-int/2addr v9, v6

    add-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x1

    div-int/lit8 v9, v6, 0x2

    add-int/2addr v9, v2

    add-int/lit8 v9, v9, -0x1

    add-int v10, v2, v7

    invoke-virtual {v0, v9, v10}, Lcom/google/zxing/common/b;->a(II)Z

    move-result v9

    aput-boolean v9, v1, v8

    add-int/lit8 v7, v7, 0x1

    rem-int/lit8 v7, v7, 0x2

    add-int/lit8 v6, v6, -0x1

    goto :goto_108

    :cond_145
    add-int/lit8 v2, v2, 0x2

    mul-int/lit8 v6, v4, 0x8

    add-int/lit8 v6, v6, -0x10

    add-int/2addr v3, v6

    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v4, v4, -0x4

    goto/16 :goto_a8

    .line 102
    :cond_152
    invoke-direct {p0, v1}, Lcom/google/zxing/aztec/decoder/Decoder;->a([Z)[Z

    move-result-object v9

    .line 104
    iget v0, p0, Lcom/google/zxing/aztec/decoder/Decoder;->k:I

    iget-object v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->l:Lcom/google/zxing/aztec/a;

    invoke-virtual {v1}, Lcom/google/zxing/aztec/a;->b()I

    move-result v1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/zxing/aztec/decoder/Decoder;->m:I

    sub-int v10, v0, v1

    array-length v0, v9

    if-le v10, v0, :cond_16b

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_16b
    sget-object v8, Lcom/google/zxing/aztec/decoder/Decoder$Table;->UPPER:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    sget-object v6, Lcom/google/zxing/aztec/decoder/Decoder$Table;->UPPER:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    const/4 v2, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v0, 0x14

    invoke-direct {v11, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v0, 0x0

    move v3, v0

    move v5, v2

    move v0, v7

    move v2, v1

    move-object v1, v8

    :goto_180
    if-nez v2, :cond_26e

    if-eqz v4, :cond_1c7

    const/4 v0, 0x1

    move v7, v0

    move-object v8, v1

    :goto_187
    if-eqz v3, :cond_1d9

    sub-int v0, v10, v5

    const/4 v1, 0x5

    if-lt v0, v1, :cond_26e

    const/4 v0, 0x5

    invoke-static {v9, v5, v0}, Lcom/google/zxing/aztec/decoder/Decoder;->a([ZII)I

    move-result v0

    add-int/lit8 v1, v5, 0x5

    if-nez v0, :cond_1a7

    sub-int v0, v10, v1

    const/16 v3, 0xb

    if-lt v0, v3, :cond_26e

    const/16 v0, 0xb

    invoke-static {v9, v1, v0}, Lcom/google/zxing/aztec/decoder/Decoder;->a([ZII)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    add-int/lit8 v1, v1, 0xb

    :cond_1a7
    const/4 v3, 0x0

    move v13, v3

    move v3, v1

    move v1, v13

    :goto_1ab
    if-ge v1, v0, :cond_28b

    sub-int v5, v10, v3

    const/16 v12, 0x8

    if-ge v5, v12, :cond_1ca

    const/4 v0, 0x1

    :goto_1b4
    const/4 v1, 0x0

    move v2, v3

    move-object v3, v6

    move v13, v1

    move v1, v4

    move v4, v0

    move v0, v13

    :goto_1bb
    if-eqz v7, :cond_27b

    const/4 v1, 0x0

    const/4 v7, 0x0

    move v3, v0

    move v5, v2

    move-object v6, v8

    move v0, v7

    move v2, v4

    move v4, v1

    move-object v1, v8

    goto :goto_180

    :cond_1c7
    move v7, v0

    move-object v8, v6

    goto :goto_187

    :cond_1ca
    const/16 v5, 0x8

    invoke-static {v9, v3, v5}, Lcom/google/zxing/aztec/decoder/Decoder;->a([ZII)I

    move-result v5

    int-to-char v5, v5

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x8

    add-int/lit8 v1, v1, 0x1

    goto :goto_1ab

    :cond_1d9
    sget-object v0, Lcom/google/zxing/aztec/decoder/Decoder$Table;->BINARY:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    if-ne v6, v0, :cond_1f5

    sub-int v0, v10, v5

    const/16 v1, 0x8

    if-lt v0, v1, :cond_26e

    const/16 v0, 0x8

    invoke-static {v9, v5, v0}, Lcom/google/zxing/aztec/decoder/Decoder;->a([ZII)I

    move-result v1

    add-int/lit8 v0, v5, 0x8

    int-to-char v1, v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v1, v4

    move v4, v2

    move v2, v0

    move v0, v3

    move-object v3, v6

    goto :goto_1bb

    :cond_1f5
    const/4 v0, 0x5

    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->DIGIT:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    if-ne v6, v1, :cond_1fb

    const/4 v0, 0x4

    :cond_1fb
    sub-int v1, v10, v5

    if-lt v1, v0, :cond_26e

    invoke-static {v9, v5, v0}, Lcom/google/zxing/aztec/decoder/Decoder;->a([ZII)I

    move-result v1

    add-int/2addr v5, v0

    sget-object v0, Lcom/google/zxing/aztec/decoder/a;->a:[I

    invoke-virtual {v6}, Lcom/google/zxing/aztec/decoder/Decoder$Table;->ordinal()I

    move-result v12

    aget v0, v0, v12

    packed-switch v0, :pswitch_data_28e

    const-string v0, ""

    :goto_211
    const-string v1, "CTRL_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_264

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_29c

    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->UPPER:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    :goto_223
    const/4 v6, 0x6

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v12, 0x53

    if-ne v6, v12, :cond_284

    const/4 v4, 0x1

    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v6, 0x42

    if-ne v0, v6, :cond_284

    const/4 v0, 0x1

    move-object v3, v1

    move v1, v4

    move v4, v2

    move v2, v5

    goto :goto_1bb

    :pswitch_23c
    sget-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->e:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_211

    :pswitch_241
    sget-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->f:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_211

    :pswitch_246
    sget-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->g:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_211

    :pswitch_24b
    sget-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->h:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_211

    :pswitch_250
    sget-object v0, Lcom/google/zxing/aztec/decoder/Decoder;->i:[Ljava/lang/String;

    aget-object v0, v0, v1

    goto :goto_211

    :sswitch_255
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->LOWER:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    goto :goto_223

    :sswitch_258
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->PUNCT:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    goto :goto_223

    :sswitch_25b
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->MIXED:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    goto :goto_223

    :sswitch_25e
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->DIGIT:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    goto :goto_223

    :sswitch_261
    sget-object v1, Lcom/google/zxing/aztec/decoder/Decoder$Table;->BINARY:Lcom/google/zxing/aztec/decoder/Decoder$Table;

    goto :goto_223

    :cond_264
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v3

    move v1, v4

    move-object v3, v6

    move v4, v2

    move v2, v5

    goto/16 :goto_1bb

    :cond_26e
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/google/zxing/common/d;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/zxing/common/d;-><init>([BLjava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v1

    :cond_27b
    move v5, v2

    move-object v6, v3

    move v3, v0

    move v2, v4

    move v0, v7

    move v4, v1

    move-object v1, v8

    goto/16 :goto_180

    :cond_284
    move v0, v3

    move-object v3, v1

    move v1, v4

    move v4, v2

    move v2, v5

    goto/16 :goto_1bb

    :cond_28b
    move v0, v2

    goto/16 :goto_1b4

    .line 104
    :pswitch_data_28e
    .packed-switch 0x1
        :pswitch_23c
        :pswitch_241
        :pswitch_246
        :pswitch_24b
        :pswitch_250
    .end packed-switch

    :sswitch_data_29c
    .sparse-switch
        0x42 -> :sswitch_261
        0x44 -> :sswitch_25e
        0x4c -> :sswitch_255
        0x4d -> :sswitch_25b
        0x50 -> :sswitch_258
    .end sparse-switch
.end method
