.class public final Lcom/google/zxing/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/zxing/e;


# instance fields
.field private a:Ljava/util/Map;

.field private b:[Lcom/google/zxing/e;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lcom/google/zxing/b;)Lcom/google/zxing/f;
    .registers 7
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/zxing/d;->b:[Lcom/google/zxing/e;

    if-eqz v0, :cond_17

    .line 168
    iget-object v1, p0, Lcom/google/zxing/d;->b:[Lcom/google/zxing/e;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_17

    aget-object v3, v1, v0

    .line 170
    :try_start_c
    iget-object v4, p0, Lcom/google/zxing/d;->a:Ljava/util/Map;

    invoke-interface {v3, p1, v4}, Lcom/google/zxing/e;->a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    :try_end_11
    .catch Lcom/google/zxing/ReaderException; {:try_start_c .. :try_end_11} :catch_13

    move-result-object v0

    return-object v0

    .line 168
    :catch_13
    move-exception v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 176
    :cond_17
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/zxing/b;)Lcom/google/zxing/f;
    .registers 3
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/zxing/d;->b:[Lcom/google/zxing/e;

    if-nez v0, :cond_8

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/zxing/d;->a(Ljava/util/Map;)V

    .line 85
    :cond_8
    invoke-direct {p0, p1}, Lcom/google/zxing/d;->b(Lcom/google/zxing/b;)Lcom/google/zxing/f;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/zxing/b;Ljava/util/Map;)Lcom/google/zxing/f;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-virtual {p0, p2}, Lcom/google/zxing/d;->a(Ljava/util/Map;)V

    .line 69
    invoke-direct {p0, p1}, Lcom/google/zxing/d;->b(Lcom/google/zxing/b;)Lcom/google/zxing/f;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .registers 5

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/zxing/d;->b:[Lcom/google/zxing/e;

    if-eqz v0, :cond_12

    .line 160
    iget-object v1, p0, Lcom/google/zxing/d;->b:[Lcom/google/zxing/e;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    .line 161
    invoke-interface {v3}, Lcom/google/zxing/e;->a()V

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 164
    :cond_12
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 96
    iput-object p1, p0, Lcom/google/zxing/d;->a:Ljava/util/Map;

    .line 98
    if-eqz p1, :cond_12b

    sget-object v0, Lcom/google/zxing/DecodeHintType;->TRY_HARDER:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12b

    move v3, v2

    .line 99
    :goto_f
    if-nez p1, :cond_12e

    const/4 v0, 0x0

    .line 101
    :goto_12
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 102
    if-eqz v0, :cond_da

    .line 103
    sget-object v5, Lcom/google/zxing/BarcodeFormat;->UPC_A:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->UPC_E:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->EAN_13:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->EAN_8:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->CODABAR:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->CODE_39:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->CODE_93:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->CODE_128:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->ITF:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->RSS_14:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_71

    sget-object v5, Lcom/google/zxing/BarcodeFormat;->RSS_EXPANDED:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v5}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_72

    :cond_71
    move v1, v2

    .line 116
    :cond_72
    if-eqz v1, :cond_7e

    if-nez v3, :cond_7e

    .line 117
    new-instance v2, Lcom/google/zxing/oned/i;

    invoke-direct {v2, p1}, Lcom/google/zxing/oned/i;-><init>(Ljava/util/Map;)V

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_7e
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->QR_CODE:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8e

    .line 120
    new-instance v2, Lcom/google/zxing/qrcode/a;

    invoke-direct {v2}, Lcom/google/zxing/qrcode/a;-><init>()V

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_8e
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->DATA_MATRIX:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9e

    .line 123
    new-instance v2, Lcom/google/zxing/datamatrix/a;

    invoke-direct {v2}, Lcom/google/zxing/datamatrix/a;-><init>()V

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_9e
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->AZTEC:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_ae

    .line 126
    new-instance v2, Lcom/google/zxing/aztec/b;

    invoke-direct {v2}, Lcom/google/zxing/aztec/b;-><init>()V

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_ae
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->PDF_417:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_be

    .line 129
    new-instance v2, Lcom/google/zxing/pdf417/a;

    invoke-direct {v2}, Lcom/google/zxing/pdf417/a;-><init>()V

    invoke-interface {v4, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_be
    sget-object v2, Lcom/google/zxing/BarcodeFormat;->MAXICODE:Lcom/google/zxing/BarcodeFormat;

    invoke-interface {v0, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ce

    .line 132
    new-instance v0, Lcom/google/zxing/a/a;

    invoke-direct {v0}, Lcom/google/zxing/a/a;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_ce
    if-eqz v1, :cond_da

    if-eqz v3, :cond_da

    .line 136
    new-instance v0, Lcom/google/zxing/oned/i;

    invoke-direct {v0, p1}, Lcom/google/zxing/oned/i;-><init>(Ljava/util/Map;)V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_da
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11c

    .line 140
    if-nez v3, :cond_ea

    .line 141
    new-instance v0, Lcom/google/zxing/oned/i;

    invoke-direct {v0, p1}, Lcom/google/zxing/oned/i;-><init>(Ljava/util/Map;)V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_ea
    new-instance v0, Lcom/google/zxing/qrcode/a;

    invoke-direct {v0}, Lcom/google/zxing/qrcode/a;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v0, Lcom/google/zxing/datamatrix/a;

    invoke-direct {v0}, Lcom/google/zxing/datamatrix/a;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 146
    new-instance v0, Lcom/google/zxing/aztec/b;

    invoke-direct {v0}, Lcom/google/zxing/aztec/b;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 147
    new-instance v0, Lcom/google/zxing/pdf417/a;

    invoke-direct {v0}, Lcom/google/zxing/pdf417/a;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 148
    new-instance v0, Lcom/google/zxing/a/a;

    invoke-direct {v0}, Lcom/google/zxing/a/a;-><init>()V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 150
    if-eqz v3, :cond_11c

    .line 151
    new-instance v0, Lcom/google/zxing/oned/i;

    invoke-direct {v0, p1}, Lcom/google/zxing/oned/i;-><init>(Ljava/util/Map;)V

    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_11c
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/zxing/e;

    invoke-interface {v4, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/e;

    iput-object v0, p0, Lcom/google/zxing/d;->b:[Lcom/google/zxing/e;

    .line 155
    return-void

    :cond_12b
    move v3, v1

    .line 98
    goto/16 :goto_f

    .line 99
    :cond_12e
    sget-object v0, Lcom/google/zxing/DecodeHintType;->POSSIBLE_FORMATS:Lcom/google/zxing/DecodeHintType;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    goto/16 :goto_12
.end method
