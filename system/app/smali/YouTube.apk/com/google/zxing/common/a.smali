.class public final Lcom/google/zxing/common/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[I

.field private b:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/zxing/common/a;->b:I

    .line 31
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    .line 32
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput p1, p0, Lcom/google/zxing/common/a;->b:I

    .line 36
    add-int/lit8 v0, p1, 0x1f

    shr-int/lit8 v0, v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    .line 37
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 40
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    return v0
.end method

.method public final a(I)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 60
    iget-object v1, p0, Lcom/google/zxing/common/a;->a:[I

    shr-int/lit8 v2, p1, 0x5

    aget v1, v1, v2

    and-int/lit8 v2, p1, 0x1f

    shl-int v2, v0, v2

    and-int/2addr v1, v2

    if-eqz v1, :cond_f

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a(IIZ)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x1f

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    if-ge p2, p1, :cond_c

    .line 190
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 192
    :cond_c
    if-ne p2, p1, :cond_10

    move v0, v1

    .line 217
    :goto_f
    return v0

    .line 195
    :cond_10
    add-int/lit8 v8, p2, -0x1

    .line 196
    shr-int/lit8 v7, p1, 0x5

    .line 197
    shr-int/lit8 v9, v8, 0x5

    move v6, v7

    .line 198
    :goto_17
    if-gt v6, v9, :cond_42

    .line 199
    if-le v6, v7, :cond_2d

    move v0, v2

    .line 200
    :goto_1c
    if-ge v6, v9, :cond_30

    move v4, v5

    .line 202
    :goto_1f
    if-nez v0, :cond_34

    if-ne v4, v5, :cond_34

    .line 203
    const/4 v0, -0x1

    .line 213
    :cond_24
    iget-object v3, p0, Lcom/google/zxing/common/a;->a:[I

    aget v3, v3, v6

    and-int/2addr v0, v3

    if-eqz v0, :cond_3e

    move v0, v2

    .line 214
    goto :goto_f

    .line 199
    :cond_2d
    and-int/lit8 v0, p1, 0x1f

    goto :goto_1c

    .line 200
    :cond_30
    and-int/lit8 v3, v8, 0x1f

    move v4, v3

    goto :goto_1f

    :cond_34
    move v3, v0

    move v0, v2

    .line 206
    :goto_36
    if-gt v3, v4, :cond_24

    .line 207
    shl-int v10, v1, v3

    or-int/2addr v0, v10

    .line 206
    add-int/lit8 v3, v3, 0x1

    goto :goto_36

    .line 198
    :cond_3e
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_17

    :cond_42
    move v0, v1

    .line 217
    goto :goto_f
.end method

.method public final b()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 173
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    array-length v2, v0

    move v0, v1

    .line 174
    :goto_5
    if-ge v0, v2, :cond_e

    .line 175
    iget-object v3, p0, Lcom/google/zxing/common/a;->a:[I

    aput v1, v3, v0

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 177
    :cond_e
    return-void
.end method

.method public final b(I)V
    .registers 7
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    shr-int/lit8 v1, p1, 0x5

    aget v2, v0, v1

    const/4 v3, 0x1

    and-int/lit8 v4, p1, 0x1f

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    aput v2, v0, v1

    .line 70
    return-void
.end method

.method public final c(I)I
    .registers 6
    .parameter

    .prologue
    .line 88
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    if-lt p1, v0, :cond_7

    .line 89
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    .line 102
    :cond_6
    :goto_6
    return v0

    .line 91
    :cond_7
    shr-int/lit8 v1, p1, 0x5

    .line 92
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    aget v0, v0, v1

    .line 94
    const/4 v2, 0x1

    and-int/lit8 v3, p1, 0x1f

    shl-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v0, v2

    .line 95
    :goto_16
    if-nez v0, :cond_27

    .line 96
    add-int/lit8 v1, v1, 0x1

    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    array-length v0, v0

    if-ne v1, v0, :cond_22

    .line 97
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    goto :goto_6

    .line 99
    :cond_22
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    aget v0, v0, v1

    goto :goto_16

    .line 101
    :cond_27
    shl-int/lit8 v1, v1, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 102
    iget v1, p0, Lcom/google/zxing/common/a;->b:I

    if-le v0, v1, :cond_6

    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    goto :goto_6
.end method

.method public final c()V
    .registers 8

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    array-length v0, v0

    new-array v1, v0, [I

    .line 296
    iget v2, p0, Lcom/google/zxing/common/a;->b:I

    .line 297
    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_22

    .line 298
    sub-int v3, v2, v0

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 299
    shr-int/lit8 v3, v0, 0x5

    aget v4, v1, v3

    const/4 v5, 0x1

    and-int/lit8 v6, v0, 0x1f

    shl-int/2addr v5, v6

    or-int/2addr v4, v5

    aput v4, v1, v3

    .line 297
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 302
    :cond_22
    iput-object v1, p0, Lcom/google/zxing/common/a;->a:[I

    .line 303
    return-void
.end method

.method public final d(I)I
    .registers 6
    .parameter

    .prologue
    .line 109
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    if-lt p1, v0, :cond_7

    .line 110
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    .line 123
    :cond_6
    :goto_6
    return v0

    .line 112
    :cond_7
    shr-int/lit8 v1, p1, 0x5

    .line 113
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    aget v0, v0, v1

    xor-int/lit8 v0, v0, -0x1

    .line 115
    const/4 v2, 0x1

    and-int/lit8 v3, p1, 0x1f

    shl-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v0, v2

    .line 116
    :goto_18
    if-nez v0, :cond_2b

    .line 117
    add-int/lit8 v1, v1, 0x1

    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    array-length v0, v0

    if-ne v1, v0, :cond_24

    .line 118
    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    goto :goto_6

    .line 120
    :cond_24
    iget-object v0, p0, Lcom/google/zxing/common/a;->a:[I

    aget v0, v0, v1

    xor-int/lit8 v0, v0, -0x1

    goto :goto_18

    .line 122
    :cond_2b
    shl-int/lit8 v1, v1, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 123
    iget v1, p0, Lcom/google/zxing/common/a;->b:I

    if-le v0, v1, :cond_6

    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    goto :goto_6
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 311
    new-instance v2, Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/zxing/common/a;->b:I

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 312
    const/4 v0, 0x0

    :goto_8
    iget v1, p0, Lcom/google/zxing/common/a;->b:I

    if-ge v0, v1, :cond_26

    .line 313
    and-int/lit8 v1, v0, 0x7

    if-nez v1, :cond_15

    .line 314
    const/16 v1, 0x20

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 316
    :cond_15
    invoke-virtual {p0, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_23

    const/16 v1, 0x58

    :goto_1d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 312
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 316
    :cond_23
    const/16 v1, 0x2e

    goto :goto_1d

    .line 318
    :cond_26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
