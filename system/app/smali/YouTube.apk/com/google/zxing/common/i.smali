.class public final Lcom/google/zxing/common/i;
.super Lcom/google/zxing/common/g;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/zxing/common/b;


# direct methods
.method public constructor <init>(Lcom/google/zxing/c;)V
    .registers 2
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/zxing/common/g;-><init>(Lcom/google/zxing/c;)V

    .line 54
    return-void
.end method

.method private static a(III)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x2

    .line 134
    if-ge p0, v0, :cond_5

    move p2, v0

    :cond_4
    :goto_4
    return p2

    :cond_5
    if-gt p0, p2, :cond_4

    move p2, p0

    goto :goto_4
.end method


# virtual methods
.method public final b()Lcom/google/zxing/common/b;
    .registers 21

    .prologue
    .line 63
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/zxing/common/i;->a:Lcom/google/zxing/common/b;

    if-eqz v1, :cond_b

    .line 64
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/zxing/common/i;->a:Lcom/google/zxing/common/b;

    .line 88
    :goto_a
    return-object v1

    .line 66
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/google/zxing/common/i;->a()Lcom/google/zxing/c;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/google/zxing/c;->b()I

    move-result v15

    .line 68
    invoke-virtual {v1}, Lcom/google/zxing/c;->c()I

    move-result v16

    .line 69
    const/16 v2, 0x28

    if-lt v15, v2, :cond_175

    const/16 v2, 0x28

    move/from16 v0, v16

    if-lt v0, v2, :cond_175

    .line 70
    invoke-virtual {v1}, Lcom/google/zxing/c;->a()[B

    move-result-object v17

    .line 71
    shr-int/lit8 v1, v15, 0x3

    .line 72
    and-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_195

    .line 73
    add-int/lit8 v1, v1, 0x1

    move v14, v1

    .line 75
    :goto_2e
    shr-int/lit8 v1, v16, 0x3

    .line 76
    and-int/lit8 v2, v16, 0x7

    if-eqz v2, :cond_192

    .line 77
    add-int/lit8 v1, v1, 0x1

    move v2, v1

    .line 79
    :goto_37
    filled-new-array {v2, v14}, [I

    move-result-object v1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[I

    const/4 v3, 0x0

    move v13, v3

    :goto_45
    if-ge v13, v2, :cond_ea

    shl-int/lit8 v4, v13, 0x3

    add-int/lit8 v3, v16, -0x8

    if-le v4, v3, :cond_18f

    :goto_4d
    const/4 v4, 0x0

    move v12, v4

    :goto_4f
    if-ge v12, v14, :cond_e5

    shl-int/lit8 v5, v12, 0x3

    add-int/lit8 v4, v15, -0x8

    if-le v5, v4, :cond_18c

    :goto_57
    const/4 v8, 0x0

    const/16 v9, 0xff

    const/4 v5, 0x0

    const/4 v7, 0x0

    mul-int v6, v3, v15

    add-int/2addr v6, v4

    :goto_5f
    const/16 v4, 0x8

    if-ge v7, v4, :cond_b3

    const/4 v4, 0x0

    move v10, v4

    :goto_65
    const/16 v4, 0x8

    if-ge v10, v4, :cond_7d

    add-int v4, v6, v10

    aget-byte v4, v17, v4

    and-int/lit16 v4, v4, 0xff

    add-int v11, v8, v4

    if-ge v4, v9, :cond_189

    move v8, v4

    :goto_74
    if-le v4, v5, :cond_186

    :goto_76
    add-int/lit8 v5, v10, 0x1

    move v10, v5

    move v9, v8

    move v5, v4

    move v8, v11

    goto :goto_65

    :cond_7d
    sub-int v4, v5, v9

    const/16 v10, 0x18

    if-le v4, v10, :cond_a6

    add-int/lit8 v7, v7, 0x1

    add-int v4, v6, v15

    move v6, v7

    move v7, v8

    :goto_89
    const/16 v8, 0x8

    if-ge v6, v8, :cond_a9

    const/4 v8, 0x0

    move/from16 v19, v8

    move v8, v7

    move/from16 v7, v19

    :goto_93
    const/16 v10, 0x8

    if-ge v7, v10, :cond_a1

    add-int v10, v4, v7

    aget-byte v10, v17, v10

    and-int/lit16 v10, v10, 0xff

    add-int/2addr v8, v10

    add-int/lit8 v7, v7, 0x1

    goto :goto_93

    :cond_a1
    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v15

    move v7, v8

    goto :goto_89

    :cond_a6
    move v4, v6

    move v6, v7

    move v7, v8

    :cond_a9
    add-int/lit8 v8, v6, 0x1

    add-int v6, v4, v15

    move/from16 v19, v8

    move v8, v7

    move/from16 v7, v19

    goto :goto_5f

    :cond_b3
    shr-int/lit8 v4, v8, 0x6

    sub-int/2addr v5, v9

    const/16 v6, 0x18

    if-gt v5, v6, :cond_dc

    shr-int/lit8 v5, v9, 0x1

    if-lez v13, :cond_183

    if-lez v12, :cond_183

    add-int/lit8 v4, v13, -0x1

    aget-object v4, v1, v4

    aget v4, v4, v12

    aget-object v6, v1, v13

    add-int/lit8 v7, v12, -0x1

    aget v6, v6, v7

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    add-int/lit8 v6, v13, -0x1

    aget-object v6, v1, v6

    add-int/lit8 v7, v12, -0x1

    aget v6, v6, v7

    add-int/2addr v4, v6

    shr-int/lit8 v4, v4, 0x2

    if-ge v9, v4, :cond_183

    :cond_dc
    :goto_dc
    aget-object v5, v1, v13

    aput v4, v5, v12

    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto/16 :goto_4f

    :cond_e5
    add-int/lit8 v3, v13, 0x1

    move v13, v3

    goto/16 :goto_45

    .line 81
    :cond_ea
    new-instance v10, Lcom/google/zxing/common/b;

    move/from16 v0, v16

    invoke-direct {v10, v15, v0}, Lcom/google/zxing/common/b;-><init>(II)V

    .line 82
    const/4 v3, 0x0

    move v6, v3

    :goto_f3
    if-ge v6, v2, :cond_16b

    shl-int/lit8 v4, v6, 0x3

    add-int/lit8 v3, v16, -0x8

    if-le v4, v3, :cond_17e

    move v5, v3

    :goto_fc
    const/4 v3, 0x0

    move v8, v3

    :goto_fe
    if-ge v8, v14, :cond_167

    shl-int/lit8 v4, v8, 0x3

    add-int/lit8 v3, v15, -0x8

    if-le v4, v3, :cond_181

    :goto_106
    const/4 v4, 0x2

    add-int/lit8 v7, v14, -0x3

    invoke-static {v8, v4, v7}, Lcom/google/zxing/common/i;->a(III)I

    move-result v9

    const/4 v4, 0x2

    add-int/lit8 v7, v2, -0x3

    invoke-static {v6, v4, v7}, Lcom/google/zxing/common/i;->a(III)I

    move-result v11

    const/4 v7, 0x0

    const/4 v4, -0x2

    :goto_116
    const/4 v12, 0x2

    if-gt v4, v12, :cond_13a

    add-int v12, v11, v4

    aget-object v12, v1, v12

    add-int/lit8 v13, v9, -0x2

    aget v13, v12, v13

    add-int/lit8 v18, v9, -0x1

    aget v18, v12, v18

    add-int v13, v13, v18

    aget v18, v12, v9

    add-int v13, v13, v18

    add-int/lit8 v18, v9, 0x1

    aget v18, v12, v18

    add-int v13, v13, v18

    add-int/lit8 v18, v9, 0x2

    aget v12, v12, v18

    add-int/2addr v12, v13

    add-int/2addr v7, v12

    add-int/lit8 v4, v4, 0x1

    goto :goto_116

    :cond_13a
    div-int/lit8 v11, v7, 0x19

    const/4 v7, 0x0

    mul-int v4, v5, v15

    add-int/2addr v4, v3

    move v9, v7

    move v7, v4

    :goto_142
    const/16 v4, 0x8

    if-ge v9, v4, :cond_163

    const/4 v4, 0x0

    :goto_147
    const/16 v12, 0x8

    if-ge v4, v12, :cond_15d

    add-int v12, v7, v4

    aget-byte v12, v17, v12

    and-int/lit16 v12, v12, 0xff

    if-gt v12, v11, :cond_15a

    add-int v12, v3, v4

    add-int v13, v5, v9

    invoke-virtual {v10, v12, v13}, Lcom/google/zxing/common/b;->b(II)V

    :cond_15a
    add-int/lit8 v4, v4, 0x1

    goto :goto_147

    :cond_15d
    add-int/lit8 v9, v9, 0x1

    add-int v4, v7, v15

    move v7, v4

    goto :goto_142

    :cond_163
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_fe

    :cond_167
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_f3

    .line 83
    :cond_16b
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/zxing/common/i;->a:Lcom/google/zxing/common/b;

    .line 88
    :goto_16f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/zxing/common/i;->a:Lcom/google/zxing/common/b;

    goto/16 :goto_a

    .line 86
    :cond_175
    invoke-super/range {p0 .. p0}, Lcom/google/zxing/common/g;->b()Lcom/google/zxing/common/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/zxing/common/i;->a:Lcom/google/zxing/common/b;

    goto :goto_16f

    :cond_17e
    move v5, v4

    goto/16 :goto_fc

    :cond_181
    move v3, v4

    goto :goto_106

    :cond_183
    move v4, v5

    goto/16 :goto_dc

    :cond_186
    move v4, v5

    goto/16 :goto_76

    :cond_189
    move v8, v9

    goto/16 :goto_74

    :cond_18c
    move v4, v5

    goto/16 :goto_57

    :cond_18f
    move v3, v4

    goto/16 :goto_4d

    :cond_192
    move v2, v1

    goto/16 :goto_37

    :cond_195
    move v14, v1

    goto/16 :goto_2e
.end method
