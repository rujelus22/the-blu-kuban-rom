.class public final Lcom/google/zxing/common/reedsolomon/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/zxing/common/reedsolomon/a;

.field public static final b:Lcom/google/zxing/common/reedsolomon/a;

.field public static final c:Lcom/google/zxing/common/reedsolomon/a;

.field public static final d:Lcom/google/zxing/common/reedsolomon/a;

.field public static final e:Lcom/google/zxing/common/reedsolomon/a;

.field public static final f:Lcom/google/zxing/common/reedsolomon/a;

.field public static final g:Lcom/google/zxing/common/reedsolomon/a;

.field public static final h:Lcom/google/zxing/common/reedsolomon/a;


# instance fields
.field private i:[I

.field private j:[I

.field private k:Lcom/google/zxing/common/reedsolomon/b;

.field private l:Lcom/google/zxing/common/reedsolomon/b;

.field private final m:I

.field private final n:I

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/16 v3, 0x100

    .line 32
    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x1069

    const/16 v2, 0x1000

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->a:Lcom/google/zxing/common/reedsolomon/a;

    .line 33
    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x409

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->b:Lcom/google/zxing/common/reedsolomon/a;

    .line 34
    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x43

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->c:Lcom/google/zxing/common/reedsolomon/a;

    .line 35
    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x13

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->d:Lcom/google/zxing/common/reedsolomon/a;

    .line 36
    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x11d

    invoke-direct {v0, v1, v3}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->e:Lcom/google/zxing/common/reedsolomon/a;

    .line 37
    new-instance v0, Lcom/google/zxing/common/reedsolomon/a;

    const/16 v1, 0x12d

    invoke-direct {v0, v1, v3}, Lcom/google/zxing/common/reedsolomon/a;-><init>(II)V

    .line 38
    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->f:Lcom/google/zxing/common/reedsolomon/a;

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->g:Lcom/google/zxing/common/reedsolomon/a;

    .line 39
    sget-object v0, Lcom/google/zxing/common/reedsolomon/a;->c:Lcom/google/zxing/common/reedsolomon/a;

    sput-object v0, Lcom/google/zxing/common/reedsolomon/a;->h:Lcom/google/zxing/common/reedsolomon/a;

    return-void
.end method

.method private constructor <init>(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/common/reedsolomon/a;->o:Z

    .line 59
    iput p1, p0, Lcom/google/zxing/common/reedsolomon/a;->n:I

    .line 60
    iput p2, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    .line 62
    if-gtz p2, :cond_f

    .line 63
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->d()V

    .line 65
    :cond_f
    return-void
.end method

.method static b(II)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 129
    xor-int v0, p0, p1

    return v0
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 68
    iget v0, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    .line 69
    iget v0, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    move v0, v1

    move v2, v3

    .line 71
    :goto_10
    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    if-ge v0, v4, :cond_29

    .line 72
    iget-object v4, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    aput v2, v4, v0

    .line 73
    shl-int/lit8 v2, v2, 0x1

    .line 74
    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    if-lt v2, v4, :cond_26

    .line 75
    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->n:I

    xor-int/2addr v2, v4

    .line 76
    iget v4, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    add-int/lit8 v4, v4, -0x1

    and-int/2addr v2, v4

    .line 71
    :cond_26
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_29
    move v0, v1

    .line 79
    :goto_2a
    iget v2, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3b

    .line 80
    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    iget-object v4, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    aget v4, v4, v0

    aput v0, v2, v4

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    .line 83
    :cond_3b
    new-instance v0, Lcom/google/zxing/common/reedsolomon/b;

    new-array v2, v3, [I

    aput v1, v2, v1

    invoke-direct {v0, p0, v2}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->k:Lcom/google/zxing/common/reedsolomon/b;

    .line 84
    new-instance v0, Lcom/google/zxing/common/reedsolomon/b;

    new-array v2, v3, [I

    aput v3, v2, v1

    invoke-direct {v0, p0, v2}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    iput-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->l:Lcom/google/zxing/common/reedsolomon/b;

    .line 85
    iput-boolean v3, p0, Lcom/google/zxing/common/reedsolomon/a;->o:Z

    .line 86
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/zxing/common/reedsolomon/a;->o:Z

    if-nez v0, :cond_7

    .line 90
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->d()V

    .line 92
    :cond_7
    return-void
.end method


# virtual methods
.method final a(I)I
    .registers 3
    .parameter

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 138
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    aget v0, v0, p1

    return v0
.end method

.method final a()Lcom/google/zxing/common/reedsolomon/b;
    .registers 2

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 97
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->k:Lcom/google/zxing/common/reedsolomon/b;

    return-object v0
.end method

.method final a(II)Lcom/google/zxing/common/reedsolomon/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 112
    if-gez p1, :cond_b

    .line 113
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 115
    :cond_b
    if-nez p2, :cond_10

    .line 116
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->k:Lcom/google/zxing/common/reedsolomon/b;

    .line 120
    :goto_f
    return-object v0

    .line 118
    :cond_10
    add-int/lit8 v0, p1, 0x1

    new-array v1, v0, [I

    .line 119
    const/4 v0, 0x0

    aput p2, v1, v0

    .line 120
    new-instance v0, Lcom/google/zxing/common/reedsolomon/b;

    invoke-direct {v0, p0, v1}, Lcom/google/zxing/common/reedsolomon/b;-><init>(Lcom/google/zxing/common/reedsolomon/a;[I)V

    goto :goto_f
.end method

.method final b(I)I
    .registers 3
    .parameter

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 147
    if-nez p1, :cond_b

    .line 148
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 150
    :cond_b
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v0, v0, p1

    return v0
.end method

.method final b()Lcom/google/zxing/common/reedsolomon/b;
    .registers 2

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 103
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->l:Lcom/google/zxing/common/reedsolomon/b;

    return-object v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 178
    iget v0, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    return v0
.end method

.method final c(I)I
    .registers 5
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 159
    if-nez p1, :cond_b

    .line 160
    new-instance v0, Ljava/lang/ArithmeticException;

    invoke-direct {v0}, Ljava/lang/ArithmeticException;-><init>()V

    throw v0

    .line 162
    :cond_b
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    iget v1, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v2, v2, p1

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method final c(II)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/google/zxing/common/reedsolomon/a;->e()V

    .line 171
    if-eqz p1, :cond_7

    if-nez p2, :cond_9

    .line 172
    :cond_7
    const/4 v0, 0x0

    .line 174
    :goto_8
    return v0

    :cond_9
    iget-object v0, p0, Lcom/google/zxing/common/reedsolomon/a;->i:[I

    iget-object v1, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v1, v1, p1

    iget-object v2, p0, Lcom/google/zxing/common/reedsolomon/a;->j:[I

    aget v2, v2, p2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/zxing/common/reedsolomon/a;->m:I

    add-int/lit8 v2, v2, -0x1

    rem-int/2addr v1, v2

    aget v0, v0, v1

    goto :goto_8
.end method
