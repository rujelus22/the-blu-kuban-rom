.class public Lak/x;
.super Lak/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lak/n;)V
    .registers 2
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lak/e;-><init>(Lak/n;)V

    .line 28
    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;)Lak/f;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3e00

    .line 50
    .line 51
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ae

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    .line 52
    invoke-virtual {v0}, Lak/j;->b()I

    move-result v3

    if-ne v3, v5, :cond_a

    move-object v1, v0

    .line 58
    :goto_1d
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-virtual {p3, v0}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 59
    :cond_25
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_ac

    .line 60
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    .line 61
    invoke-virtual {v0}, Lak/j;->b()I

    move-result v4

    if-ne v4, v5, :cond_25

    .line 66
    :goto_37
    if-eqz v1, :cond_3b

    if-nez v0, :cond_3e

    .line 67
    :cond_3b
    sget-object v0, Lak/f;->a:Lak/f;

    .line 89
    :goto_3d
    return-object v0

    .line 71
    :cond_3e
    invoke-virtual {v0}, Lak/j;->a()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    if-lez v2, :cond_4c

    .line 72
    sget-object v0, Lak/f;->a:Lak/f;

    goto :goto_3d

    .line 78
    :cond_4c
    invoke-virtual {v0, v7}, Lak/j;->a(I)F

    move-result v2

    invoke-virtual {v1, v7}, Lak/j;->a(I)F

    move-result v3

    sub-float/2addr v2, v3

    .line 79
    invoke-virtual {v0, v7}, Lak/j;->b(I)F

    move-result v3

    invoke-virtual {v1, v7}, Lak/j;->b(I)F

    move-result v4

    sub-float/2addr v3, v4

    .line 80
    invoke-virtual {v0, v8}, Lak/j;->a(I)F

    move-result v4

    invoke-virtual {v1, v8}, Lak/j;->a(I)F

    move-result v5

    sub-float/2addr v4, v5

    .line 81
    invoke-virtual {v0, v8}, Lak/j;->b(I)F

    move-result v5

    invoke-virtual {v1, v8}, Lak/j;->b(I)F

    move-result v1

    sub-float v1, v5, v1

    .line 82
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v0}, Lak/j;->c()F

    move-result v5

    div-float/2addr v2, v5

    cmpl-float v2, v2, v6

    if-gtz v2, :cond_a6

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v0}, Lak/j;->d()F

    move-result v3

    div-float/2addr v2, v3

    cmpl-float v2, v2, v6

    if-gtz v2, :cond_a6

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {v0}, Lak/j;->c()F

    move-result v3

    div-float/2addr v2, v3

    cmpl-float v2, v2, v6

    if-gtz v2, :cond_a6

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0}, Lak/j;->d()F

    move-result v0

    div-float v0, v1, v0

    cmpl-float v0, v0, v6

    if-lez v0, :cond_a9

    .line 86
    :cond_a6
    sget-object v0, Lak/f;->a:Lak/f;

    goto :goto_3d

    .line 89
    :cond_a9
    sget-object v0, Lak/f;->c:Lak/f;

    goto :goto_3d

    :cond_ac
    move-object v0, v2

    goto :goto_37

    :cond_ae
    move-object v1, v2

    goto/16 :goto_1d
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 32
    const/4 v0, 0x1

    return v0
.end method

.method protected b(Lak/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lak/x;->a:Lak/n;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lak/n;->b(Lak/k;Z)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method protected d(Lak/k;)V
    .registers 4
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lak/x;->a:Lak/n;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lak/n;->c(Lak/k;Z)V

    .line 100
    return-void
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method protected f(Lak/k;)Z
    .registers 4
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lak/x;->a:Lak/n;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lak/n;->a(Lak/k;Z)Z

    move-result v0

    return v0
.end method
