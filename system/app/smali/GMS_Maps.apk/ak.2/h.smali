.class public Lak/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:I

.field private static final g:I

.field private static final h:I


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private final i:Landroid/os/Handler;

.field private final j:Landroid/view/GestureDetector$OnGestureListener;

.field private k:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Landroid/view/MotionEvent;

.field private q:Landroid/view/MotionEvent;

.field private r:Z

.field private s:F

.field private t:F

.field private u:F

.field private v:F

.field private w:Z

.field private x:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 59
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lak/h;->f:I

    .line 60
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lak/h;->g:I

    .line 61
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lak/h;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 192
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lak/h;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    if-eqz p3, :cond_1b

    .line 210
    new-instance v0, Lak/i;

    invoke-direct {v0, p0, p3}, Lak/i;-><init>(Lak/h;Landroid/os/Handler;)V

    iput-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    .line 214
    :goto_c
    iput-object p2, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    .line 215
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_17

    .line 216
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-virtual {p0, p2}, Lak/h;->a(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 218
    :cond_17
    invoke-direct {p0, p1}, Lak/h;->a(Landroid/content/Context;)V

    .line 219
    return-void

    .line 212
    :cond_1b
    new-instance v0, Lak/i;

    invoke-direct {v0, p0}, Lak/i;-><init>(Lak/h;)V

    iput-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    goto :goto_c
.end method

.method static synthetic a(Lak/h;)Landroid/view/MotionEvent;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lak/h;->p:Landroid/view/MotionEvent;

    return-object v0
.end method

.method private a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 526
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 527
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 528
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 529
    iget-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 530
    const/4 v0, 0x0

    iput-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    .line 531
    iput-boolean v2, p0, Lak/h;->r:Z

    .line 532
    iput-boolean v2, p0, Lak/h;->l:Z

    .line 533
    iput-boolean v2, p0, Lak/h;->n:Z

    .line 534
    iput-boolean v2, p0, Lak/h;->o:Z

    .line 535
    iget-boolean v0, p0, Lak/h;->m:Z

    if-eqz v0, :cond_29

    .line 536
    iput-boolean v2, p0, Lak/h;->m:Z

    .line 538
    :cond_29
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v0, :cond_c

    .line 240
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "OnGestureListener must not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lak/h;->w:Z

    .line 246
    if-nez p1, :cond_2f

    .line 248
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v0

    .line 256
    mul-int/lit8 v1, v0, 0x2

    .line 258
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lak/h;->d:I

    .line 259
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v2

    iput v2, p0, Lak/h;->e:I

    move v2, v0

    .line 274
    :goto_24
    mul-int/2addr v2, v2

    iput v2, p0, Lak/h;->a:I

    .line 275
    mul-int/2addr v0, v0

    iput v0, p0, Lak/h;->b:I

    .line 276
    mul-int v0, v1, v1

    iput v0, p0, Lak/h;->c:I

    .line 277
    return-void

    .line 261
    :cond_2f
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    .line 262
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 269
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    .line 270
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 271
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lak/h;->d:I

    .line 272
    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Lak/h;->e:I

    goto :goto_24
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 554
    iget-boolean v1, p0, Lak/h;->o:Z

    if-nez v1, :cond_6

    .line 564
    :cond_5
    :goto_5
    return v0

    .line 558
    :cond_6
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    sget v3, Lak/h;->h:I

    int-to-long v3, v3

    cmp-long v1, v1, v3

    if-gtz v1, :cond_5

    .line 562
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 563
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    .line 564
    mul-int/2addr v1, v1

    mul-int/2addr v2, v2

    add-int/2addr v1, v2

    iget v2, p0, Lak/h;->c:I

    if-ge v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method static synthetic b(Lak/h;)Landroid/view/GestureDetector$OnGestureListener;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 541
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 542
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 543
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 544
    iput-boolean v2, p0, Lak/h;->r:Z

    .line 545
    iput-boolean v2, p0, Lak/h;->n:Z

    .line 546
    iput-boolean v2, p0, Lak/h;->o:Z

    .line 547
    iget-boolean v0, p0, Lak/h;->m:Z

    if-eqz v0, :cond_1f

    .line 548
    iput-boolean v2, p0, Lak/h;->m:Z

    .line 550
    :cond_1f
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 568
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 569
    const/4 v0, 0x1

    iput-boolean v0, p0, Lak/h;->m:Z

    .line 570
    iget-object v0, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 571
    return-void
.end method

.method static synthetic c(Lak/h;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lak/h;->c()V

    return-void
.end method

.method static synthetic d(Lak/h;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic e(Lak/h;)Z
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-boolean v0, p0, Lak/h;->l:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .registers 2
    .parameter

    .prologue
    .line 287
    iput-object p1, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 288
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 300
    iput-boolean p1, p0, Lak/h;->w:Z

    .line 301
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 15
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x3

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 328
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    .line 330
    iget-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    if-nez v0, :cond_13

    .line 331
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    .line 333
    :cond_13
    iget-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 335
    and-int/lit16 v0, v9, 0xff

    const/4 v1, 0x6

    if-ne v0, v1, :cond_32

    move v7, v8

    .line 337
    :goto_1e
    if-eqz v7, :cond_34

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 341
    :goto_24
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    move v5, v3

    move v1, v6

    move v2, v6

    .line 342
    :goto_2b
    if-ge v5, v4, :cond_41

    .line 343
    if-ne v0, v5, :cond_36

    .line 342
    :goto_2f
    add-int/lit8 v5, v5, 0x1

    goto :goto_2b

    :cond_32
    move v7, v3

    .line 335
    goto :goto_1e

    .line 337
    :cond_34
    const/4 v0, -0x1

    goto :goto_24

    .line 344
    :cond_36
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    add-float/2addr v2, v10

    .line 345
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    add-float/2addr v1, v10

    goto :goto_2f

    .line 347
    :cond_41
    if-eqz v7, :cond_4f

    add-int/lit8 v0, v4, -0x1

    .line 348
    :goto_45
    int-to-float v5, v0

    div-float/2addr v2, v5

    .line 349
    int-to-float v0, v0

    div-float/2addr v1, v0

    .line 353
    and-int/lit16 v0, v9, 0xff

    packed-switch v0, :pswitch_data_240

    .line 522
    :cond_4e
    :goto_4e
    :pswitch_4e
    return v3

    :cond_4f
    move v0, v4

    .line 347
    goto :goto_45

    .line 355
    :pswitch_51
    iput v2, p0, Lak/h;->s:F

    iput v2, p0, Lak/h;->u:F

    .line 356
    iput v1, p0, Lak/h;->t:F

    iput v1, p0, Lak/h;->v:F

    .line 358
    invoke-direct {p0}, Lak/h;->b()V

    goto :goto_4e

    .line 362
    :pswitch_5d
    iput v2, p0, Lak/h;->s:F

    iput v2, p0, Lak/h;->u:F

    .line 363
    iput v1, p0, Lak/h;->t:F

    iput v1, p0, Lak/h;->v:F

    .line 367
    iget-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lak/h;->e:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 368
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    .line 369
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 370
    iget-object v2, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v2

    .line 371
    iget-object v5, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v5, v0}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    move v0, v3

    .line 372
    :goto_84
    if-ge v0, v4, :cond_4e

    .line 373
    if-ne v0, v1, :cond_8b

    .line 372
    :cond_88
    add-int/lit8 v0, v0, 0x1

    goto :goto_84

    .line 375
    :cond_8b
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    .line 376
    iget-object v8, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v8, v7}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v8

    mul-float/2addr v8, v2

    .line 377
    iget-object v9, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v9, v7}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v7

    mul-float/2addr v7, v5

    .line 379
    add-float/2addr v7, v8

    .line 380
    cmpg-float v7, v7, v6

    if-gez v7, :cond_88

    .line 381
    iget-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_4e

    .line 388
    :pswitch_a8
    iget-object v0, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_13b

    .line 389
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    .line 390
    if-eqz v0, :cond_b9

    iget-object v4, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v4, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 391
    :cond_b9
    iget-object v4, p0, Lak/h;->p:Landroid/view/MotionEvent;

    if-eqz v4, :cond_133

    iget-object v4, p0, Lak/h;->q:Landroid/view/MotionEvent;

    if-eqz v4, :cond_133

    if-eqz v0, :cond_133

    iget-object v0, p0, Lak/h;->p:Landroid/view/MotionEvent;

    iget-object v4, p0, Lak/h;->q:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v4, p1}, Lak/h;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_133

    .line 394
    iput-boolean v8, p0, Lak/h;->r:Z

    .line 396
    iget-object v0, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v4, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-interface {v0, v4}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 398
    iget-object v4, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v4, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v0, v4

    .line 405
    :goto_df
    iput v2, p0, Lak/h;->s:F

    iput v2, p0, Lak/h;->u:F

    .line 406
    iput v1, p0, Lak/h;->t:F

    iput v1, p0, Lak/h;->v:F

    .line 407
    iget-object v1, p0, Lak/h;->p:Landroid/view/MotionEvent;

    if-eqz v1, :cond_f0

    .line 408
    iget-object v1, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 410
    :cond_f0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lak/h;->p:Landroid/view/MotionEvent;

    .line 411
    iput-boolean v8, p0, Lak/h;->n:Z

    .line 412
    iput-boolean v8, p0, Lak/h;->o:Z

    .line 413
    iput-boolean v8, p0, Lak/h;->l:Z

    .line 414
    iput-boolean v3, p0, Lak/h;->m:Z

    .line 416
    iget-boolean v1, p0, Lak/h;->w:Z

    if-eqz v1, :cond_11a

    .line 417
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 418
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    iget-object v2, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, Lak/h;->g:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    sget v4, Lak/h;->f:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v12, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 421
    :cond_11a
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    iget-object v2, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v2

    sget v4, Lak/h;->g:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v8, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 422
    iget-object v1, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v1, p1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int v3, v0, v1

    .line 423
    goto/16 :goto_4e

    .line 401
    :cond_133
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    sget v4, Lak/h;->h:I

    int-to-long v4, v4

    invoke-virtual {v0, v11, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_13b
    move v0, v3

    goto :goto_df

    .line 435
    :pswitch_13d
    iget v0, p0, Lak/h;->s:F

    sub-float/2addr v0, v2

    .line 436
    iget v4, p0, Lak/h;->t:F

    sub-float/2addr v4, v1

    .line 437
    iget-boolean v5, p0, Lak/h;->r:Z

    if-eqz v5, :cond_150

    .line 439
    iget-object v0, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v3, v0

    goto/16 :goto_4e

    .line 440
    :cond_150
    iget-boolean v5, p0, Lak/h;->n:Z

    if-eqz v5, :cond_18b

    .line 441
    iget v5, p0, Lak/h;->u:F

    sub-float v5, v2, v5

    float-to-int v5, v5

    .line 442
    iget v6, p0, Lak/h;->v:F

    sub-float v6, v1, v6

    float-to-int v6, v6

    .line 443
    mul-int/2addr v5, v5

    mul-int/2addr v6, v6

    add-int/2addr v5, v6

    .line 444
    iget v6, p0, Lak/h;->a:I

    if-le v5, v6, :cond_23d

    .line 445
    iget-object v6, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v7, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-interface {v6, v7, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 446
    iput v2, p0, Lak/h;->s:F

    .line 447
    iput v1, p0, Lak/h;->t:F

    .line 448
    iput-boolean v3, p0, Lak/h;->n:Z

    .line 449
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v1, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 450
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 451
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 453
    :goto_182
    iget v1, p0, Lak/h;->b:I

    if-le v5, v1, :cond_188

    .line 454
    iput-boolean v3, p0, Lak/h;->o:Z

    :cond_188
    move v3, v0

    .line 456
    goto/16 :goto_4e

    :cond_18b
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f80

    cmpl-float v5, v5, v6

    if-gez v5, :cond_19f

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x3f80

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_4e

    .line 457
    :cond_19f
    iget-object v3, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v5, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-interface {v3, v5, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    .line 458
    iput v2, p0, Lak/h;->s:F

    .line 459
    iput v1, p0, Lak/h;->t:F

    goto/16 :goto_4e

    .line 464
    :pswitch_1ad
    iput-boolean v3, p0, Lak/h;->l:Z

    .line 465
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 466
    iget-boolean v0, p0, Lak/h;->r:Z

    if-eqz v0, :cond_1e4

    .line 468
    iget-object v0, p0, Lak/h;->k:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v3

    .line 493
    :goto_1be
    iget-object v2, p0, Lak/h;->q:Landroid/view/MotionEvent;

    if-eqz v2, :cond_1c7

    .line 494
    iget-object v2, p0, Lak/h;->q:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    .line 497
    :cond_1c7
    iput-object v1, p0, Lak/h;->q:Landroid/view/MotionEvent;

    .line 498
    iget-object v1, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_1d5

    .line 501
    iget-object v1, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 502
    const/4 v1, 0x0

    iput-object v1, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    .line 504
    :cond_1d5
    iput-boolean v3, p0, Lak/h;->r:Z

    .line 505
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v1, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 506
    iget-object v1, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v1, v12}, Landroid/os/Handler;->removeMessages(I)V

    move v3, v0

    .line 507
    goto/16 :goto_4e

    .line 469
    :cond_1e4
    iget-boolean v0, p0, Lak/h;->m:Z

    if-eqz v0, :cond_1f6

    .line 470
    iget-object v0, p0, Lak/h;->i:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->removeMessages(I)V

    .line 471
    iput-boolean v3, p0, Lak/h;->m:Z

    .line 476
    iget-object v0, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1be

    .line 477
    :cond_1f6
    iget-boolean v0, p0, Lak/h;->n:Z

    if-eqz v0, :cond_201

    .line 478
    iget-object v0, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1be

    .line 482
    :cond_201
    iget-object v0, p0, Lak/h;->x:Landroid/view/VelocityTracker;

    .line 483
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 484
    const/16 v4, 0x3e8

    iget v5, p0, Lak/h;->e:I

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 485
    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v4

    .line 486
    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    .line 488
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, Lak/h;->d:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_22d

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v5, p0, Lak/h;->d:I

    int-to-float v5, v5

    cmpl-float v2, v2, v5

    if-lez v2, :cond_23b

    .line 490
    :cond_22d
    iget-object v2, p0, Lak/h;->j:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v5, p0, Lak/h;->p:Landroid/view/MotionEvent;

    invoke-interface {v2, v5, p1, v0, v4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_1be

    .line 510
    :pswitch_236
    invoke-direct {p0}, Lak/h;->a()V

    goto/16 :goto_4e

    :cond_23b
    move v0, v3

    goto :goto_1be

    :cond_23d
    move v0, v3

    goto/16 :goto_182

    .line 353
    :pswitch_data_240
    .packed-switch 0x0
        :pswitch_a8
        :pswitch_1ad
        :pswitch_13d
        :pswitch_236
        :pswitch_4e
        :pswitch_51
        :pswitch_5d
    .end packed-switch
.end method
