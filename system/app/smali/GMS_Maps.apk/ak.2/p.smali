.class public Lak/p;
.super Lak/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lak/n;)V
    .registers 2
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lak/e;-><init>(Lak/n;)V

    .line 66
    return-void
.end method


# virtual methods
.method public a(JLjava/util/LinkedList;Ljava/util/List;)Lak/f;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/high16 v5, 0x3f00

    .line 75
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/e;

    .line 76
    invoke-virtual {v0}, Lak/e;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 77
    sget-object v0, Lak/f;->a:Lak/f;

    .line 132
    :goto_1b
    return-object v0

    .line 83
    :cond_1c
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_26

    .line 84
    sget-object v0, Lak/f;->b:Lak/f;

    goto :goto_1b

    .line 88
    :cond_26
    const/4 v1, 0x0

    .line 89
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    .line 90
    invoke-virtual {v0}, Lak/j;->b()I

    move-result v3

    if-le v3, v4, :cond_2b

    move-object v1, v0

    .line 95
    :cond_3e
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lak/j;

    .line 96
    if-eqz v1, :cond_4c

    invoke-virtual {v0}, Lak/j;->b()I

    move-result v2

    if-gt v2, v4, :cond_4f

    .line 97
    :cond_4c
    sget-object v0, Lak/f;->a:Lak/f;

    goto :goto_1b

    .line 101
    :cond_4f
    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6f

    const v2, 0x3db2b8c2

    .line 105
    :goto_58
    invoke-virtual {v1}, Lak/j;->f()F

    move-result v3

    .line 106
    invoke-virtual {v0}, Lak/j;->f()F

    move-result v4

    .line 107
    invoke-static {v3, v4}, Lak/p;->a(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 108
    cmpg-float v2, v3, v2

    if-gez v2, :cond_73

    .line 109
    sget-object v0, Lak/f;->a:Lak/f;

    goto :goto_1b

    .line 101
    :cond_6f
    const v2, 0x3e32b8c2

    goto :goto_58

    .line 113
    :cond_73
    invoke-virtual {v0}, Lak/j;->c()F

    move-result v2

    invoke-virtual {v0}, Lak/j;->d()F

    move-result v4

    add-float/2addr v2, v4

    mul-float/2addr v2, v5

    .line 114
    invoke-virtual {v0}, Lak/j;->g()F

    move-result v0

    div-float/2addr v0, v2

    .line 115
    const/high16 v4, 0x3f40

    cmpg-float v4, v0, v4

    if-gez v4, :cond_8b

    .line 116
    sget-object v0, Lak/f;->a:Lak/f;

    goto :goto_1b

    .line 121
    :cond_8b
    invoke-virtual {v1}, Lak/j;->g()F

    move-result v1

    div-float/2addr v1, v2

    .line 122
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 123
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_af

    .line 124
    div-float v0, v3, v0

    .line 125
    cmpg-float v1, v0, v5

    if-gez v1, :cond_a4

    .line 126
    sget-object v0, Lak/f;->a:Lak/f;

    goto/16 :goto_1b

    .line 127
    :cond_a4
    const v1, 0x3f666666

    cmpg-float v0, v0, v1

    if-gez v0, :cond_af

    .line 128
    sget-object v0, Lak/f;->b:Lak/f;

    goto/16 :goto_1b

    .line 132
    :cond_af
    sget-object v0, Lak/f;->c:Lak/f;

    goto/16 :goto_1b
.end method

.method protected b(Lak/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 137
    const-string v0, "r"

    invoke-virtual {p0, v0}, Lak/p;->a(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lak/p;->a:Lak/n;

    invoke-interface {v0, p1}, Lak/n;->e(Lak/k;)Z

    move-result v0

    return v0
.end method

.method protected d(Lak/k;)V
    .registers 3
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, Lak/p;->a:Lak/n;

    invoke-interface {v0, p1}, Lak/n;->f(Lak/k;)V

    .line 144
    return-void
.end method

.method protected f(Lak/k;)Z
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lak/p;->a:Lak/n;

    invoke-interface {v0, p1}, Lak/n;->d(Lak/k;)Z

    move-result v0

    return v0
.end method
