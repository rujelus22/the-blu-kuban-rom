.class public Lh/o;
.super Lh/n;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;)V
    .registers 3
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lh/n;-><init>(Landroid/view/animation/Interpolator;)V

    .line 18
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    .line 19
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    .line 20
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lh/o;->c:Ljava/lang/Object;

    .line 21
    return-void
.end method


# virtual methods
.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 33
    iget-boolean v0, p0, Lh/o;->d:Z

    if-nez v0, :cond_8

    .line 34
    invoke-virtual {p0, p1, p2, p1, p2}, Lh/o;->a(IIII)V

    .line 39
    :goto_7
    return-void

    .line 36
    :cond_8
    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    iget-object v1, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v1, Lo/T;

    invoke-virtual {v0, v1}, Lo/T;->b(Lo/T;)V

    .line 37
    iget-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1, p2}, Lo/T;->d(II)V

    goto :goto_7
.end method

.method public a(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1, p2}, Lo/T;->d(II)V

    .line 26
    iget-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p3, p4}, Lo/T;->d(II)V

    .line 27
    iget-object v0, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1, p2}, Lo/T;->d(II)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lh/o;->d:Z

    .line 29
    return-void
.end method

.method protected a(J)V
    .registers 7
    .parameter

    .prologue
    .line 51
    invoke-virtual {p0, p1, p2}, Lh/o;->c(J)F

    move-result v3

    .line 54
    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    iget-object v1, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v1, Lo/T;

    iget-object v2, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v2, Lo/T;

    invoke-static {v0, v1, v3, v2}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    .line 55
    return-void
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 15
    check-cast p1, Lo/T;

    invoke-virtual {p0, p1}, Lh/o;->a(Lo/T;)V

    return-void
.end method

.method protected a(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lh/o;->a:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    .line 60
    return-void
.end method

.method public b(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2, p1, p2}, Lh/o;->a(IIII)V

    .line 47
    return-void
.end method

.method protected bridge synthetic b(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 15
    check-cast p1, Lo/T;

    invoke-virtual {p0, p1}, Lh/o;->b(Lo/T;)V

    return-void
.end method

.method protected b(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lh/o;->b:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    .line 65
    return-void
.end method

.method protected bridge synthetic c(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 15
    check-cast p1, Lo/T;

    invoke-virtual {p0, p1}, Lh/o;->c(Lo/T;)V

    return-void
.end method

.method protected c(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lh/o;->c:Ljava/lang/Object;

    check-cast v0, Lo/T;

    invoke-virtual {v0, p1}, Lo/T;->b(Lo/T;)V

    .line 70
    return-void
.end method
