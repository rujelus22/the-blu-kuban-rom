.class public Lt/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:I

.field static final b:I

.field static final synthetic c:Z

.field private static final d:[B

.field private static e:I


# instance fields
.field private A:Lt/x;

.field private B:Lt/s;

.field private C:Ljava/util/Set;

.field private D:I

.field private final f:Ljava/lang/String;

.field private final g:Lj/d;

.field private h:Lj/c;

.field private i:Lt/k;

.field private final j:Lt/r;

.field private final k:Lt/j;

.field private final l:[Lj/c;

.field private final m:LR/h;

.field private final n:Ljava/util/Set;

.field private final o:Ljava/util/concurrent/locks/ReentrantLock;

.field private final p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 214
    const-class v0, Lt/h;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_1e

    const/4 v0, 0x1

    :goto_a
    sput-boolean v0, Lt/h;->c:Z

    .line 218
    new-array v0, v1, [B

    sput-object v0, Lt/h;->d:[B

    .line 294
    const/16 v0, 0x14

    sput v0, Lt/h;->a:I

    .line 295
    const v0, 0x13f88

    sput v0, Lt/h;->b:I

    .line 299
    sget v0, Lt/h;->a:I

    sput v0, Lt/h;->e:I

    return-void

    :cond_1e
    move v0, v1

    .line 214
    goto :goto_a
.end method

.method private constructor <init>(Ljava/lang/String;Lt/k;Lt/r;Lt/j;Lj/c;Lj/d;Lt/s;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 366
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    .line 371
    new-instance v2, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v2, v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>(Z)V

    iput-object v2, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    .line 384
    const/4 v2, -0x1

    iput v2, p0, Lt/h;->y:I

    .line 385
    iput-boolean v0, p0, Lt/h;->z:Z

    .line 389
    iput-object v3, p0, Lt/h;->A:Lt/x;

    .line 393
    iput-object v3, p0, Lt/h;->B:Lt/s;

    .line 404
    const/4 v2, 0x4

    iput v2, p0, Lt/h;->D:I

    .line 418
    iput-object p1, p0, Lt/h;->f:Ljava/lang/String;

    .line 419
    iput-object p2, p0, Lt/h;->i:Lt/k;

    .line 420
    iput-object p3, p0, Lt/h;->j:Lt/r;

    .line 421
    iput-object p4, p0, Lt/h;->k:Lt/j;

    .line 422
    iput-object p5, p0, Lt/h;->h:Lj/c;

    .line 423
    iput-object p6, p0, Lt/h;->g:Lj/d;

    .line 424
    iget v2, p2, Lt/k;->d:I

    new-array v2, v2, [Lj/c;

    iput-object v2, p0, Lt/h;->l:[Lj/c;

    .line 425
    new-instance v2, LR/h;

    const/16 v3, 0x800

    invoke-virtual {p0}, Lt/h;->f()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-direct {v2, v3}, LR/h;-><init>(I)V

    iput-object v2, p0, Lt/h;->m:LR/h;

    .line 426
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lt/h;->n:Ljava/util/Set;

    .line 427
    iput-object p7, p0, Lt/h;->B:Lt/s;

    .line 430
    iget-object v2, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    move v2, v0

    .line 433
    :goto_56
    :try_start_56
    iget-object v3, p0, Lt/h;->i:Lt/k;

    iget v3, v3, Lt/k;->d:I

    if-ge v2, v3, :cond_c1

    .line 434
    iget-object v3, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v3, v2}, Lt/r;->c(I)Z

    move-result v3

    if-nez v3, :cond_74

    iget-object v3, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v3, v2}, Lt/r;->d(I)Z

    move-result v3

    if-eqz v3, :cond_a2

    iget-object v3, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v3, v2}, Lt/j;->c(I)Z

    move-result v3

    if-nez v3, :cond_a2

    .line 436
    :cond_74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rebuilding inconsistent shard: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lt/h;->a(Ljava/lang/String;)V

    .line 438
    iget v0, p0, Lt/h;->r:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/h;->r:I
    :try_end_90
    .catchall {:try_start_56 .. :try_end_90} :catchall_e0

    .line 440
    :try_start_90
    invoke-direct {p0, v2}, Lt/h;->d(I)Lt/p;

    move-result-object v0

    .line 441
    iget-object v3, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v3, v0}, Lt/r;->a(Lt/p;)V

    .line 442
    iget-object v3, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v3, v0}, Lt/j;->a(Lt/p;)V

    .line 443
    invoke-direct {p0, v2}, Lt/h;->f(I)V
    :try_end_a1
    .catchall {:try_start_90 .. :try_end_a1} :catchall_e0
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_a1} :catch_a5

    move v0, v1

    .line 433
    :cond_a2
    :goto_a2
    add-int/lit8 v2, v2, 0x1

    goto :goto_56

    .line 444
    :catch_a5
    move-exception v0

    .line 445
    :try_start_a6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rebuilding shard: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 448
    invoke-direct {p0, v2}, Lt/h;->g(I)V

    move v0, v1

    goto :goto_a2

    .line 452
    :cond_c1
    if-eqz v0, :cond_c6

    .line 453
    invoke-direct {p0}, Lt/h;->n()V
    :try_end_c6
    .catchall {:try_start_a6 .. :try_end_c6} :catchall_e0

    .line 456
    :cond_c6
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 460
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_df

    .line 461
    iget-object v0, p0, Lt/h;->k:Lt/j;

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0, v1}, Lt/j;->a(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V

    .line 462
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0, v1}, Lt/r;->a(Ljava/util/concurrent/locks/ReentrantReadWriteLock;)V

    .line 464
    :cond_df
    return-void

    .line 456
    :catchall_e0
    move-exception v0

    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private static a(II)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 661
    shl-int/lit8 v0, p0, 0x10

    add-int/2addr v0, p1

    return v0
.end method

.method private a(Z)I
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 1718
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lt/h;->j:Lt/r;

    iget v1, v1, Lt/r;->g:I

    if-ge v0, v1, :cond_14

    .line 1719
    iget-object v1, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v1, v0}, Lt/r;->d(I)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1752
    :cond_10
    :goto_10
    return v0

    .line 1718
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1724
    :cond_14
    if-eqz p1, :cond_24

    .line 1725
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v1, p0, Lt/h;->C:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lt/r;->a(Ljava/util/Set;)I

    move-result v0

    .line 1726
    if-eq v0, v2, :cond_24

    .line 1727
    invoke-direct {p0, v0}, Lt/h;->g(I)V

    goto :goto_10

    .line 1733
    :cond_24
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget v0, v0, Lt/r;->g:I

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v1, v1, Lt/k;->d:I

    if-ge v0, v1, :cond_5e

    .line 1736
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 1738
    :try_start_37
    iget-object v0, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v0}, Lt/r;->b()I

    move-result v0

    .line 1739
    iget-object v1, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v1, v0}, Lt/j;->b(I)V

    .line 1740
    iget-object v1, p0, Lt/h;->k:Lt/j;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lt/j;->a(I)V
    :try_end_49
    .catchall {:try_start_37 .. :try_end_49} :catchall_53

    .line 1743
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    goto :goto_10

    :catchall_53
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0

    .line 1748
    :cond_5e
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v1, p0, Lt/h;->C:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lt/r;->a(Ljava/util/Set;)I

    move-result v0

    .line 1749
    if-eq v0, v2, :cond_10

    .line 1750
    invoke-direct {p0, v0}, Lt/h;->g(I)V

    goto :goto_10
.end method

.method public static a([BI)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 3090
    add-int/lit8 v0, p1, 0x1

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    .line 3091
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    .line 3092
    add-int/lit8 v3, v2, 0x1

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    .line 3093
    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    .line 3094
    shl-int/lit8 v1, v1, 0x18

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0
.end method

.method public static a(Ljava/lang/String;IILjava/util/Locale;Lj/d;Lt/s;)Lt/h;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 482
    const/4 v0, 0x0

    .line 483
    const/4 v3, -0x1

    if-ne p1, v3, :cond_9

    .line 484
    sget p1, Lt/h;->b:I

    move v0, v1

    .line 489
    :cond_9
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v3

    if-eqz v3, :cond_5c

    .line 490
    const/4 v3, -0x2

    if-ne p1, v3, :cond_3a

    .line 491
    const/16 p1, 0x2fee

    move v3, v1

    move v0, p1

    .line 499
    :goto_16
    if-ge v0, v2, :cond_19

    move v0, v2

    .line 502
    :cond_19
    if-nez v3, :cond_43

    sget v1, Lt/h;->b:I

    if-le v0, v1, :cond_43

    .line 503
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number of records must be between 4 and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lt/h;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 493
    :cond_3a
    const/4 v3, -0x3

    if-ne p1, v3, :cond_5c

    .line 494
    const p1, 0x28d71

    move v3, v1

    move v0, p1

    .line 495
    goto :goto_16

    .line 506
    :cond_43
    add-int/lit8 v1, v0, -0x1

    div-int/lit16 v1, v1, 0x199

    add-int/lit8 v1, v1, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 507
    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x1

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 509
    invoke-static/range {v0 .. v7}, Lt/h;->a(Ljava/lang/String;IIZILjava/util/Locale;Lj/d;Lt/s;)Lt/h;

    move-result-object v0

    return-object v0

    :cond_5c
    move v3, v0

    move v0, p1

    goto :goto_16
.end method

.method static a(Ljava/lang/String;IIZILjava/util/Locale;Lj/d;Lt/s;)Lt/h;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 532
    sget-boolean v1, Lt/h;->c:Z

    if-nez v1, :cond_11

    const/4 v1, 0x4

    if-lt p1, v1, :cond_b

    const/16 v1, 0x199

    if-le p1, v1, :cond_11

    :cond_b
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 533
    :cond_11
    sget-boolean v1, Lt/h;->c:Z

    if-nez v1, :cond_21

    if-nez p3, :cond_21

    const/16 v1, 0x23

    if-le p1, v1, :cond_21

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 534
    :cond_21
    sget-boolean v1, Lt/h;->c:Z

    if-nez v1, :cond_32

    const/4 v1, 0x1

    if-lt p2, v1, :cond_2c

    const/16 v1, 0x199

    if-le p2, v1, :cond_32

    :cond_2c
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 537
    :cond_32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-interface {v0, v1}, Lj/d;->a(Ljava/lang/String;)V

    .line 540
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p6

    invoke-interface {v0, v1, v2}, Lj/d;->a(Ljava/lang/String;Z)Lj/c;

    move-result-object v11

    .line 543
    new-instance v1, Lt/k;

    invoke-static {}, Lt/h;->j()I

    move-result v2

    invoke-static {}, Lt/h;->k()I

    move-result v3

    invoke-static {v2, v3}, Lt/h;->a(II)I

    move-result v2

    const/16 v3, 0x2000

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v8

    move v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lt/k;-><init>(IIIIZIJLjava/util/Locale;)V

    .line 547
    new-instance v5, Lt/r;

    invoke-direct {v5, p1}, Lt/r;-><init>(I)V

    .line 548
    new-instance v6, Lt/j;

    const/4 v2, 0x0

    invoke-direct {v6, p1, v2}, Lt/j;-><init>(II)V

    .line 550
    invoke-static {v1, v5, v6, v11}, Lt/h;->a(Lt/k;Lt/r;Lt/j;Lj/c;)V

    .line 552
    invoke-interface {v11}, Lj/c;->b()V

    .line 554
    new-instance v2, Lt/h;

    move-object v3, p0

    move-object v4, v1

    move-object v7, v11

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lt/h;-><init>(Ljava/lang/String;Lt/k;Lt/r;Lt/j;Lj/c;Lj/d;Lt/s;)V

    .line 559
    return-object v2
.end method

.method public static a(Ljava/lang/String;Lj/d;Lt/s;)Lt/h;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0xffff

    const/16 v4, 0x2000

    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".m"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lj/d;->a(Ljava/lang/String;Z)Lj/c;

    move-result-object v5

    .line 578
    new-array v0, v4, [B

    .line 579
    invoke-interface {v5, v0}, Lj/c;->a([B)V

    .line 580
    new-instance v2, Lt/k;

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lt/k;-><init>([BI)V

    .line 583
    invoke-static {}, Lt/h;->k()I

    move-result v0

    .line 584
    invoke-static {}, Lt/h;->j()I

    move-result v8

    .line 585
    invoke-static {v8, v0}, Lt/h;->a(II)I

    move-result v9

    .line 587
    iget v1, v2, Lt/k;->a:I

    shr-int/lit8 v1, v1, 0x10

    and-int v10, v1, v3

    .line 589
    iget v1, v2, Lt/k;->a:I

    and-int/2addr v1, v3

    .line 591
    if-nez v10, :cond_74

    if-eq v1, v0, :cond_74

    .line 592
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid Cache Header(1): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; cached sever schema is zero but client schema part doesn\'t match:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " cachedClientSchema = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expectedClientSchema = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 596
    :cond_74
    if-eqz v10, :cond_7a

    iget v0, v2, Lt/k;->a:I

    if-ne v0, v9, :cond_7e

    :cond_7a
    iget v0, v2, Lt/k;->c:I

    if-eq v0, v4, :cond_ab

    .line 598
    :cond_7e
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid Cache Header(2): "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", expect expectedSchema="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mBlockSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 603
    :cond_ab
    new-instance v3, Lt/r;

    iget v0, v2, Lt/k;->d:I

    invoke-direct {v3, v0}, Lt/r;-><init>(I)V

    .line 604
    invoke-virtual {v3, v5}, Lt/r;->b(Lj/c;)V

    .line 607
    new-instance v4, Lt/j;

    iget v0, v2, Lt/k;->d:I

    iget v1, v3, Lt/r;->g:I

    invoke-direct {v4, v0, v1}, Lt/j;-><init>(II)V

    .line 608
    invoke-virtual {v4, v5}, Lt/j;->b(Lj/c;)V

    .line 610
    new-instance v0, Lt/h;

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lt/h;-><init>(Ljava/lang/String;Lt/k;Lt/r;Lt/j;Lj/c;Lj/d;Lt/s;)V

    .line 614
    if-nez v10, :cond_d4

    if-eqz v8, :cond_d4

    .line 615
    iget-object v1, v0, Lt/h;->i:Lt/k;

    iget v1, v1, Lt/k;->g:I

    invoke-direct {v0, v1, v9}, Lt/h;->b(II)V

    .line 620
    :cond_d4
    return-object v0
.end method

.method public static a(JLjava/lang/String;I[B)Lt/l;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1438
    new-instance v0, Lt/l;

    invoke-static {p0, p1, p2}, Lt/h;->c(JLjava/lang/String;)Lt/m;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p3, p4, v2}, Lt/l;-><init>(Lt/m;I[BLt/i;)V

    return-object v0
.end method

.method public static a(JLjava/lang/String;[B)Lt/l;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1423
    new-instance v0, Lt/l;

    invoke-static {p0, p1, p2}, Lt/h;->c(JLjava/lang/String;)Lt/m;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p3, v2}, Lt/l;-><init>(Lt/m;[BLt/i;)V

    return-object v0
.end method

.method public static a(J[B)Lt/l;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1430
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, Lt/h;->a(JLjava/lang/String;[B)Lt/l;

    move-result-object v0

    return-object v0
.end method

.method private a(Lt/m;)Lt/o;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1187
    invoke-virtual {p1}, Lt/m;->a()J

    move-result-wide v2

    .line 1188
    iget-object v4, p0, Lt/h;->m:LR/h;

    monitor-enter v4

    .line 1189
    :try_start_8
    iget-object v0, p0, Lt/h;->m:LR/h;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/o;

    .line 1190
    monitor-exit v4
    :try_end_15
    .catchall {:try_start_8 .. :try_end_15} :catchall_43

    .line 1194
    if-eqz v0, :cond_1e

    invoke-direct {p0, v0, p1}, Lt/h;->a(Lt/o;Lt/m;)Z

    move-result v4

    if-nez v4, :cond_1e

    move-object v0, v1

    .line 1198
    :cond_1e
    if-eqz v0, :cond_38

    iget-object v4, p0, Lt/h;->j:Lt/r;

    iget v5, v0, Lt/o;->g:I

    invoke-virtual {v4, v5}, Lt/r;->d(I)Z

    move-result v4

    if-nez v4, :cond_38

    .line 1201
    iget-object v4, p0, Lt/h;->m:LR/h;

    monitor-enter v4

    .line 1202
    :try_start_2d
    iget-object v0, p0, Lt/h;->m:LR/h;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1203
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_2d .. :try_end_37} :catchall_46

    move-object v0, v1

    .line 1208
    :cond_38
    if-nez v0, :cond_42

    .line 1209
    invoke-direct {p0, p1}, Lt/h;->b(Lt/m;)Lt/q;

    move-result-object v1

    .line 1210
    if-eqz v1, :cond_42

    .line 1211
    iget-object v0, v1, Lt/q;->b:Lt/o;

    .line 1214
    :cond_42
    return-object v0

    .line 1190
    :catchall_43
    move-exception v0

    :try_start_44
    monitor-exit v4
    :try_end_45
    .catchall {:try_start_44 .. :try_end_45} :catchall_43

    throw v0

    .line 1203
    :catchall_46
    move-exception v0

    :try_start_47
    monitor-exit v4
    :try_end_48
    .catchall {:try_start_47 .. :try_end_48} :catchall_46

    throw v0
.end method

.method private a(III)Lt/p;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1803
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->e:I

    mul-int/lit8 v0, v0, 0x32

    div-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v1, v1, Lt/k;->e:I

    sub-int/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1806
    const v0, 0x7ffffff

    sub-int v2, v0, p3

    .line 1807
    :goto_16
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget v0, v0, Lt/r;->g:I

    if-ge p1, v0, :cond_4a

    .line 1808
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v0, v0, Lt/r;->e:[I

    aget v0, v0, p1

    if-lez v0, :cond_47

    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v0, v0, Lt/r;->e:[I

    aget v0, v0, p1

    if-gt v0, v1, :cond_47

    iget-object v0, p0, Lt/h;->C:Ljava/util/Set;

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lt/h;->C:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_47

    .line 1812
    :cond_3c
    invoke-direct {p0, p1}, Lt/h;->d(I)Lt/p;

    move-result-object v0

    .line 1814
    invoke-virtual {v0}, Lt/p;->d()I

    move-result v3

    if-gt v3, v2, :cond_47

    .line 1820
    :goto_46
    return-object v0

    .line 1818
    :cond_47
    add-int/lit8 p1, p1, 0x1

    goto :goto_16

    .line 1820
    :cond_4a
    const/4 v0, 0x0

    goto :goto_46
.end method

.method private a(Lt/m;I)Lt/q;
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1124
    invoke-virtual {p1}, Lt/m;->a()J

    move-result-wide v2

    .line 1125
    iget v0, p0, Lt/h;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/h;->u:I

    .line 1129
    :try_start_b
    invoke-direct {p0, p2}, Lt/h;->d(I)Lt/p;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_45

    move-result-object v3

    .line 1146
    invoke-virtual {v3}, Lt/p;->b()I

    move-result v4

    .line 1147
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1148
    const/4 v0, 0x0

    move v2, v0

    :goto_1a
    if-ge v2, v4, :cond_87

    .line 1149
    invoke-virtual {v3, v2}, Lt/p;->f(I)Lt/o;

    move-result-object v6

    .line 1150
    iget-wide v7, v6, Lt/o;->a:J

    const-wide/16 v9, -0x1

    cmp-long v0, v7, v9

    if-eqz v0, :cond_ca

    .line 1151
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1152
    iget-wide v7, v6, Lt/o;->a:J

    invoke-virtual {p1}, Lt/m;->a()J

    move-result-wide v9

    cmp-long v0, v7, v9

    if-nez v0, :cond_ca

    invoke-direct {p0, v6, p1}, Lt/h;->a(Lt/o;Lt/m;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 1153
    new-instance v0, Lt/q;

    invoke-direct {v0, v3, v6, v2}, Lt/q;-><init>(Lt/p;Lt/o;I)V

    .line 1148
    :goto_40
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_1a

    .line 1130
    :catch_45
    move-exception v0

    .line 1131
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lookupShardRecordIndexFromShard: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1136
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1138
    :try_start_6f
    invoke-direct {p0, p2}, Lt/h;->e(I)V
    :try_end_72
    .catchall {:try_start_6f .. :try_end_72} :catchall_7c

    .line 1140
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1178
    :goto_7b
    return-object v1

    .line 1140
    :catchall_7c
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    throw v0

    .line 1157
    :cond_87
    if-eqz v1, :cond_c3

    .line 1164
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ac

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/o;

    .line 1165
    iget-object v3, p0, Lt/h;->m:LR/h;

    monitor-enter v3

    .line 1166
    :try_start_9c
    iget-object v4, p0, Lt/h;->m:LR/h;

    iget-wide v5, v0, Lt/o;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1167
    monitor-exit v3

    goto :goto_8d

    :catchall_a9
    move-exception v0

    monitor-exit v3
    :try_end_ab
    .catchall {:try_start_9c .. :try_end_ab} :catchall_a9

    throw v0

    .line 1172
    :cond_ac
    iget-object v2, p0, Lt/h;->m:LR/h;

    monitor-enter v2

    .line 1173
    :try_start_af
    iget-object v0, p0, Lt/h;->m:LR/h;

    iget-object v3, v1, Lt/q;->b:Lt/o;

    iget-wide v3, v3, Lt/o;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v1, Lt/q;->b:Lt/o;

    invoke-virtual {v0, v3, v4}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1174
    monitor-exit v2

    goto :goto_7b

    :catchall_c0
    move-exception v0

    monitor-exit v2
    :try_end_c2
    .catchall {:try_start_af .. :try_end_c2} :catchall_c0

    throw v0

    .line 1176
    :cond_c3
    iget v0, p0, Lt/h;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/h;->t:I

    goto :goto_7b

    :cond_ca
    move-object v0, v1

    goto/16 :goto_40
.end method

.method private static a(ILt/m;Ljava/util/Map;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1983
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1984
    if-eqz v0, :cond_18

    .line 1985
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0}, Lr/u;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1990
    :goto_14
    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1991
    return-void

    .line 1988
    :cond_18
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_14
.end method

.method private static a(Lj/c;I[B)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3078
    monitor-enter p0

    .line 3079
    int-to-long v0, p1

    :try_start_2
    invoke-interface {p0, v0, v1}, Lj/c;->a(J)V

    .line 3080
    invoke-interface {p0, p2}, Lj/c;->a([B)V

    .line 3081
    monitor-exit p0

    .line 3082
    return-void

    .line 3081
    :catchall_a
    move-exception v0

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_2 .. :try_end_c} :catchall_a

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 3151
    return-void
.end method

.method private static a(Lt/k;Lt/r;Lt/j;Lj/c;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 670
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 671
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lt/k;->a([BI)I

    .line 672
    invoke-interface {p3, v0}, Lj/c;->b([B)V

    .line 673
    invoke-virtual {p1, p3}, Lt/r;->a(Lj/c;)V

    .line 674
    invoke-virtual {p2, p3}, Lt/j;->a(Lj/c;)V

    .line 675
    return-void
.end method

.method private a(Lt/n;Lt/p;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1551
    invoke-virtual {p1}, Lt/n;->b()V

    .line 1552
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lt/h;->a(Lt/p;Z)V

    .line 1553
    return-void
.end method

.method private a(Lt/o;Ljava/io/IOException;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1006
    iget v0, p0, Lt/h;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/h;->s:I

    .line 1007
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1008
    iget-object v0, p0, Lt/h;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1009
    return-void
.end method

.method private a(Lt/p;Lt/p;Lt/n;)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1766
    invoke-virtual {p1}, Lt/p;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lt/h;->b(I)Lj/c;

    move-result-object v11

    .line 1767
    const/4 v0, 0x0

    move v10, v0

    :goto_a
    invoke-virtual {p1}, Lt/p;->b()I

    move-result v0

    if-ge v10, v0, :cond_7d

    .line 1768
    invoke-virtual {p1, v10}, Lt/p;->b(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_79

    invoke-virtual {p1, v10}, Lt/p;->e(I)I

    move-result v0

    if-lez v0, :cond_79

    .line 1769
    invoke-virtual {p2}, Lt/p;->b()I

    move-result v0

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v1, v1, Lt/k;->e:I

    if-ge v0, v1, :cond_33

    invoke-virtual {p2}, Lt/p;->c()I

    move-result v0

    const v1, 0x7ffffff

    if-lt v0, v1, :cond_3b

    .line 1772
    :cond_33
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Couldn\'t fit refcounted records into collecting shard"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1774
    :cond_3b
    invoke-virtual {p1, v10}, Lt/p;->f(I)Lt/o;

    move-result-object v7

    .line 1775
    iget v0, v7, Lt/o;->d:I

    iget v1, v7, Lt/o;->e:I

    add-int/2addr v0, v1

    new-array v0, v0, [B

    .line 1776
    iget v1, v7, Lt/o;->b:I

    invoke-static {v11, v1, v0}, Lt/h;->a(Lj/c;I[B)V

    .line 1777
    invoke-virtual {p3, v0}, Lt/n;->a([B)V

    .line 1778
    new-instance v0, Lt/o;

    iget-wide v1, v7, Lt/o;->a:J

    invoke-virtual {p2}, Lt/p;->c()I

    move-result v3

    iget v4, v7, Lt/o;->d:I

    iget v5, v7, Lt/o;->e:I

    iget v6, v7, Lt/o;->c:I

    iget v7, v7, Lt/o;->f:I

    invoke-virtual {p2}, Lt/p;->a()I

    move-result v8

    invoke-virtual {p2}, Lt/p;->b()I

    move-result v9

    invoke-direct/range {v0 .. v9}, Lt/o;-><init>(JIIIIIII)V

    .line 1782
    invoke-virtual {p2, v0}, Lt/p;->a(Lt/o;)V

    .line 1783
    iget-object v1, p0, Lt/h;->B:Lt/s;

    if-eqz v1, :cond_79

    .line 1784
    iget-object v1, p0, Lt/h;->B:Lt/s;

    iget-wide v2, v0, Lt/o;->a:J

    iget v0, v0, Lt/o;->g:I

    invoke-interface {v1, v2, v3, v0}, Lt/s;->b(JI)V

    .line 1767
    :cond_79
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_a

    .line 1788
    :cond_7d
    return-void
.end method

.method private a(Lt/p;Z)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 946
    invoke-virtual {p0}, Lt/h;->h()V

    .line 947
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v0, v0, Lt/r;->a:[I

    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v1

    aget v0, v0, v1

    .line 949
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V

    .line 953
    :try_start_16
    iget-object v1, p0, Lt/h;->j:Lt/r;

    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v2

    invoke-virtual {v1, v2}, Lt/r;->b(I)V

    .line 954
    invoke-direct {p0}, Lt/h;->n()V

    .line 961
    iget-object v1, p0, Lt/h;->h:Lj/c;

    monitor-enter v1
    :try_end_25
    .catchall {:try_start_16 .. :try_end_25} :catchall_71

    .line 962
    :try_start_25
    iget-object v2, p0, Lt/h;->h:Lj/c;

    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v3

    mul-int/lit16 v3, v3, 0x2000

    iget-object v4, p0, Lt/h;->i:Lt/k;

    iget v4, v4, Lt/k;->k:I

    add-int/2addr v3, v4

    int-to-long v3, v3

    invoke-interface {v2, v3, v4}, Lj/c;->a(J)V

    .line 963
    iget-object v2, p0, Lt/h;->h:Lj/c;

    invoke-virtual {p1, v2}, Lt/p;->b(Lj/c;)V

    .line 964
    iget-object v2, p0, Lt/h;->h:Lj/c;

    invoke-interface {v2}, Lj/c;->b()V

    .line 965
    monitor-exit v1
    :try_end_41
    .catchall {:try_start_25 .. :try_end_41} :catchall_6e

    .line 968
    :try_start_41
    iget-object v1, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v1, p1}, Lt/j;->a(Lt/p;)V

    .line 969
    iget-object v1, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v1, p1}, Lt/r;->a(Lt/p;)V

    .line 970
    if-eqz p2, :cond_7c

    .line 971
    iget-object v0, p0, Lt/h;->j:Lt/r;

    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v1

    invoke-direct {p0}, Lt/h;->p()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lt/r;->a(II)V
    :try_end_5a
    .catchall {:try_start_41 .. :try_end_5a} :catchall_71

    .line 976
    :goto_5a
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    .line 980
    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v0

    invoke-direct {p0, v0}, Lt/h;->f(I)V

    .line 981
    invoke-direct {p0}, Lt/h;->n()V

    .line 982
    return-void

    .line 965
    :catchall_6e
    move-exception v0

    :try_start_6f
    monitor-exit v1
    :try_end_70
    .catchall {:try_start_6f .. :try_end_70} :catchall_6e

    :try_start_70
    throw v0
    :try_end_71
    .catchall {:try_start_70 .. :try_end_71} :catchall_71

    .line 976
    :catchall_71
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0

    .line 973
    :cond_7c
    :try_start_7c
    iget-object v1, p0, Lt/h;->j:Lt/r;

    invoke-static {p1}, Lt/p;->a(Lt/p;)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lt/r;->a(II)V
    :try_end_85
    .catchall {:try_start_7c .. :try_end_85} :catchall_71

    goto :goto_5a
.end method

.method public static a([BII)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3103
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x18

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    .line 3104
    add-int/lit8 v1, v0, 0x1

    shr-int/lit8 v2, p2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    .line 3105
    add-int/lit8 v0, v1, 0x1

    shr-int/lit8 v2, p2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    .line 3106
    int-to-byte v1, p2

    aput-byte v1, p0, v0

    .line 3107
    return-void
.end method

.method public static a([BIJ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3138
    const/16 v0, 0x20

    shr-long v0, p2, v0

    long-to-int v0, v0

    invoke-static {p0, p1, v0}, Lt/h;->a([BII)V

    .line 3139
    add-int/lit8 v0, p1, 0x4

    long-to-int v1, p2

    invoke-static {p0, v0, v1}, Lt/h;->a([BII)V

    .line 3140
    return-void
.end method

.method private a(Lt/o;Lt/m;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1083
    invoke-virtual {p2}, Lt/m;->b()[B

    move-result-object v1

    .line 1084
    array-length v2, v1

    iget v3, p1, Lt/o;->d:I

    if-eq v2, v3, :cond_b

    .line 1097
    :goto_a
    return v0

    .line 1088
    :cond_b
    array-length v2, v1

    if-nez v2, :cond_10

    .line 1090
    const/4 v0, 0x1

    goto :goto_a

    .line 1094
    :cond_10
    :try_start_10
    invoke-direct {p0, p1}, Lt/h;->a(Lt/o;)[B

    move-result-object v2

    .line 1095
    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_17} :catch_19

    move-result v0

    goto :goto_a

    .line 1096
    :catch_19
    move-exception v1

    goto :goto_a
.end method

.method private a(Lt/o;)[B
    .registers 5
    .parameter

    .prologue
    .line 1016
    iget v0, p1, Lt/o;->d:I

    if-nez v0, :cond_7

    .line 1017
    sget-object v0, Lt/h;->d:[B

    .line 1025
    :goto_6
    return-object v0

    .line 1022
    :cond_7
    :try_start_7
    iget v0, p1, Lt/o;->g:I

    invoke-virtual {p0, v0}, Lt/h;->b(I)Lj/c;

    move-result-object v1

    .line 1023
    iget v0, p1, Lt/o;->d:I

    new-array v0, v0, [B

    .line 1024
    iget v2, p1, Lt/o;->b:I

    invoke-static {v1, v2, v0}, Lt/h;->a(Lj/c;I[B)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_16} :catch_17

    goto :goto_6

    .line 1026
    :catch_17
    move-exception v0

    .line 1027
    invoke-direct {p0, p1, v0}, Lt/h;->a(Lt/o;Ljava/io/IOException;)V

    .line 1028
    throw v0
.end method

.method private b(Ljava/util/Collection;)I
    .registers 6
    .parameter

    .prologue
    .line 1530
    const/4 v0, 0x0

    .line 1531
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/l;

    .line 1534
    iget-object v3, v0, Lt/l;->d:[B

    array-length v3, v3

    iget-object v0, v0, Lt/l;->b:[B

    array-length v0, v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_6

    .line 1536
    :cond_1c
    return v1
.end method

.method public static b([BI)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3110
    add-int/lit8 v0, p1, 0x1

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    .line 3111
    add-int/lit8 v2, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    .line 3112
    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method private b(Lt/m;)Lt/q;
    .registers 6
    .parameter

    .prologue
    .line 1225
    invoke-virtual {p1}, Lt/m;->a()J

    move-result-wide v0

    .line 1226
    invoke-static {v0, v1}, Lt/j;->a(J)[I

    move-result-object v2

    .line 1227
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget v3, v0, Lt/r;->g:I

    .line 1228
    const/4 v0, 0x0

    move v1, v0

    :goto_e
    if-ge v1, v3, :cond_2b

    .line 1229
    iget-object v0, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v0, v1}, Lt/r;->d(I)Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v0, v2, v1}, Lt/j;->a([II)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1230
    invoke-direct {p0, p1, v1}, Lt/h;->a(Lt/m;I)Lt/q;

    move-result-object v0

    .line 1231
    if-eqz v0, :cond_27

    .line 1236
    :goto_26
    return-object v0

    .line 1228
    :cond_27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 1236
    :cond_2b
    const/4 v0, 0x0

    goto :goto_26
.end method

.method private b(II)V
    .registers 13
    .parameter
    .parameter

    .prologue
    .line 843
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 845
    :try_start_5
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->g:I

    if-ne p1, v0, :cond_11

    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->a:I

    if-eq p2, v0, :cond_4f

    .line 848
    :cond_11
    new-instance v0, Lt/k;

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v2, v1, Lt/k;->c:I

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v3, v1, Lt/k;->d:I

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v4, v1, Lt/k;->e:I

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget-boolean v5, v1, Lt/k;->f:Z

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget-wide v7, v1, Lt/k;->h:J

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget-object v9, v1, Lt/k;->i:Ljava/util/Locale;

    move v1, p2

    move v6, p1

    invoke-direct/range {v0 .. v9}, Lt/k;-><init>(IIIIZIJLjava/util/Locale;)V

    .line 852
    const/16 v1, 0x2000

    new-array v1, v1, [B

    .line 853
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt/k;->a([BI)I

    .line 854
    iget-object v2, p0, Lt/h;->h:Lj/c;

    monitor-enter v2
    :try_end_3b
    .catchall {:try_start_5 .. :try_end_3b} :catchall_5d
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_3b} :catch_58

    .line 855
    :try_start_3b
    iget-object v3, p0, Lt/h;->h:Lj/c;

    const-wide/16 v4, 0x0

    invoke-interface {v3, v4, v5}, Lj/c;->a(J)V

    .line 856
    iget-object v3, p0, Lt/h;->h:Lj/c;

    invoke-interface {v3, v1}, Lj/c;->b([B)V

    .line 857
    iget-object v1, p0, Lt/h;->h:Lj/c;

    invoke-interface {v1}, Lj/c;->b()V

    .line 858
    monitor-exit v2
    :try_end_4d
    .catchall {:try_start_3b .. :try_end_4d} :catchall_55

    .line 859
    :try_start_4d
    iput-object v0, p0, Lt/h;->i:Lt/k;
    :try_end_4f
    .catchall {:try_start_4d .. :try_end_4f} :catchall_5d
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_4f} :catch_58

    .line 865
    :cond_4f
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 867
    return-void

    .line 858
    :catchall_55
    move-exception v0

    :try_start_56
    monitor-exit v2
    :try_end_57
    .catchall {:try_start_56 .. :try_end_57} :catchall_55

    :try_start_57
    throw v0
    :try_end_58
    .catchall {:try_start_57 .. :try_end_58} :catchall_5d
    .catch Ljava/io/IOException; {:try_start_57 .. :try_end_58} :catch_58

    .line 861
    :catch_58
    move-exception v0

    .line 862
    :try_start_59
    invoke-virtual {p0}, Lt/h;->g()V

    .line 863
    throw v0
    :try_end_5d
    .catchall {:try_start_59 .. :try_end_5d} :catchall_5d

    .line 865
    :catchall_5d
    move-exception v0

    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private b(ILjava/util/Locale;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x0

    .line 880
    invoke-virtual {p0}, Lt/h;->h()V

    .line 883
    iget-object v1, p0, Lt/h;->m:LR/h;

    monitor-enter v1

    .line 884
    :try_start_7
    iget-object v0, p0, Lt/h;->m:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 885
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_36

    .line 888
    iget-object v0, p0, Lt/h;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    move v0, v10

    .line 891
    :goto_13
    iget-object v1, p0, Lt/h;->l:[Lj/c;

    array-length v1, v1

    if-ge v0, v1, :cond_39

    .line 892
    iget-object v1, p0, Lt/h;->l:[Lj/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2a

    .line 893
    iget-object v1, p0, Lt/h;->l:[Lj/c;

    aget-object v1, v1, v0

    invoke-interface {v1}, Lj/c;->a()V

    .line 894
    iget-object v1, p0, Lt/h;->l:[Lj/c;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 896
    :cond_2a
    iget-object v1, p0, Lt/h;->g:Lj/d;

    invoke-virtual {p0, v0}, Lt/h;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lj/d;->a(Ljava/lang/String;)V

    .line 891
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 885
    :catchall_36
    move-exception v0

    :try_start_37
    monitor-exit v1
    :try_end_38
    .catchall {:try_start_37 .. :try_end_38} :catchall_36

    throw v0

    .line 900
    :cond_39
    iget-object v0, p0, Lt/h;->h:Lj/c;

    invoke-interface {v0}, Lj/c;->a()V

    .line 901
    iget-object v0, p0, Lt/h;->g:Lj/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lj/d;->a(Ljava/lang/String;)V

    .line 902
    iget-object v0, p0, Lt/h;->g:Lj/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".m"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lj/d;->a(Ljava/lang/String;Z)Lj/c;

    move-result-object v0

    iput-object v0, p0, Lt/h;->h:Lj/c;

    .line 906
    new-instance v0, Lt/k;

    invoke-static {}, Lt/h;->j()I

    move-result v1

    invoke-static {}, Lt/h;->k()I

    move-result v2

    invoke-static {v1, v2}, Lt/h;->a(II)I

    move-result v1

    iget-object v2, p0, Lt/h;->i:Lt/k;

    iget v2, v2, Lt/k;->c:I

    iget-object v3, p0, Lt/h;->i:Lt/k;

    iget v3, v3, Lt/k;->d:I

    iget-object v4, p0, Lt/h;->i:Lt/k;

    iget v4, v4, Lt/k;->e:I

    iget-object v5, p0, Lt/h;->i:Lt/k;

    iget-boolean v5, v5, Lt/k;->f:Z

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v7

    move v6, p1

    move-object v9, p2

    invoke-direct/range {v0 .. v9}, Lt/k;-><init>(IIIIZIJLjava/util/Locale;)V

    iput-object v0, p0, Lt/h;->i:Lt/k;

    .line 911
    iget-object v0, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v0}, Lt/r;->a()V

    .line 912
    iget-object v0, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v0}, Lt/j;->a()V

    .line 914
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget-object v1, p0, Lt/h;->j:Lt/r;

    iget-object v2, p0, Lt/h;->k:Lt/j;

    iget-object v3, p0, Lt/h;->h:Lj/c;

    invoke-static {v0, v1, v2, v3}, Lt/h;->a(Lt/k;Lt/r;Lt/j;Lj/c;)V

    .line 915
    iget-object v0, p0, Lt/h;->h:Lj/c;

    invoke-interface {v0}, Lj/c;->b()V

    .line 917
    iput-boolean v10, p0, Lt/h;->q:Z

    .line 920
    iget-object v0, p0, Lt/h;->A:Lt/x;

    if-eqz v0, :cond_cc

    .line 921
    iget-object v0, p0, Lt/h;->A:Lt/x;

    invoke-interface {v0}, Lt/x;->a()V

    .line 923
    :cond_cc
    return-void
.end method

.method private b(Lt/o;)V
    .registers 7
    .parameter

    .prologue
    .line 1945
    invoke-virtual {p0}, Lt/h;->h()V

    .line 1948
    :try_start_3
    iget v0, p1, Lt/o;->g:I

    invoke-direct {p0, v0}, Lt/h;->d(I)Lt/p;

    move-result-object v0

    .line 1949
    invoke-virtual {v0}, Lt/p;->b()I

    move-result v1

    iget v2, p1, Lt/o;->h:I

    if-le v1, v2, :cond_1f

    iget v1, p1, Lt/o;->h:I

    invoke-virtual {v0, v1}, Lt/p;->f(I)Lt/o;

    move-result-object v1

    iget-wide v1, v1, Lt/o;->a:J

    iget-wide v3, p1, Lt/o;->a:J

    cmp-long v1, v1, v3

    if-eqz v1, :cond_20

    .line 1971
    :cond_1f
    :goto_1f
    return-void

    .line 1956
    :cond_20
    iget v1, p1, Lt/o;->h:I

    invoke-virtual {v0, v1}, Lt/p;->a(I)V

    .line 1957
    iget-object v1, p0, Lt/h;->m:LR/h;

    monitor-enter v1
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_28} :catch_4f

    .line 1958
    :try_start_28
    iget-object v2, p0, Lt/h;->m:LR/h;

    iget-wide v3, p1, Lt/o;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1959
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_28 .. :try_end_34} :catchall_69

    .line 1961
    :try_start_34
    iget-object v1, p0, Lt/h;->A:Lt/x;

    if-eqz v1, :cond_3f

    .line 1962
    iget-object v1, p0, Lt/h;->A:Lt/x;

    iget-wide v2, p1, Lt/o;->a:J

    invoke-interface {v1, v2, v3}, Lt/x;->a(J)V

    .line 1964
    :cond_3f
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lt/h;->a(Lt/p;Z)V

    .line 1965
    iget-object v0, p0, Lt/h;->B:Lt/s;

    if-eqz v0, :cond_1f

    .line 1966
    iget-object v0, p0, Lt/h;->B:Lt/s;

    iget-wide v1, p1, Lt/o;->a:J

    invoke-interface {v0, v1, v2}, Lt/s;->a(J)V
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_34 .. :try_end_4e} :catch_4f

    goto :goto_1f

    .line 1968
    :catch_4f
    move-exception v0

    .line 1969
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1f

    .line 1959
    :catchall_69
    move-exception v0

    :try_start_6a
    monitor-exit v1
    :try_end_6b
    .catchall {:try_start_6a .. :try_end_6b} :catchall_69

    :try_start_6b
    throw v0
    :try_end_6c
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6c} :catch_4f
.end method

.method private b(Z)V
    .registers 15
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1844
    invoke-virtual {p0}, Lt/h;->h()V

    .line 1847
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->d:I

    iget v1, p0, Lt/h;->D:I

    if-gt v0, v1, :cond_e

    .line 1941
    :cond_d
    :goto_d
    return-void

    .line 1852
    :cond_e
    if-eqz p1, :cond_b9

    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget v0, v0, Lt/r;->g:I

    .line 1854
    :goto_14
    iget-object v1, p0, Lt/h;->j:Lt/r;

    iget v1, v1, Lt/r;->h:I

    sub-int/2addr v0, v1

    .line 1855
    iget-object v1, p0, Lt/h;->C:Ljava/util/Set;

    if-eqz v1, :cond_24

    .line 1856
    iget-object v1, p0, Lt/h;->C:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1859
    :cond_24
    iget v1, p0, Lt/h;->D:I

    if-ge v0, v1, :cond_d

    .line 1860
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 1865
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    :try_start_2f
    invoke-direct {p0, v0, v1, v4}, Lt/h;->a(III)Lt/p;

    move-result-object v4

    .line 1866
    if-eqz v4, :cond_d

    .line 1870
    invoke-virtual {v4}, Lt/p;->a()I

    move-result v0

    .line 1871
    add-int/lit8 v1, v0, 0x1

    iget-object v7, p0, Lt/h;->j:Lt/r;

    iget-object v7, v7, Lt/r;->e:[I

    aget v0, v7, v0

    invoke-virtual {v4}, Lt/p;->d()I

    move-result v7

    invoke-direct {p0, v1, v0, v7}, Lt/h;->a(III)Lt/p;

    move-result-object v0

    .line 1873
    if-eqz v0, :cond_d

    .line 1876
    invoke-direct {p0, p1}, Lt/h;->a(Z)I

    move-result v7

    .line 1877
    const/4 v1, -0x1

    if-eq v7, v1, :cond_d

    .line 1882
    new-instance v1, Lt/p;

    invoke-direct {v1, v7}, Lt/p;-><init>(I)V
    :try_end_57
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_57} :catch_13b

    .line 1883
    const/high16 v8, 0x2

    :try_start_59
    new-array v8, v8, [B

    .line 1884
    const/4 v9, 0x0

    .line 1885
    new-instance v10, Lt/n;

    invoke-virtual {p0, v7}, Lt/h;->b(I)Lj/c;

    move-result-object v7

    invoke-direct {v10, v7, v9, v8}, Lt/n;-><init>(Lj/c;I[B)V

    .line 1887
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 1889
    :goto_6a
    if-eqz v4, :cond_81

    .line 1891
    invoke-direct {p0, v4, v1, v10}, Lt/h;->a(Lt/p;Lt/p;Lt/n;)V

    .line 1892
    invoke-virtual {v4}, Lt/p;->a()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1895
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v8

    const/4 v9, 0x4

    if-lt v8, v9, :cond_bf

    .line 1909
    :cond_81
    invoke-direct {p0, v10, v1}, Lt/h;->a(Lt/n;Lt/p;)V

    .line 1910
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_88
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_97
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_97} :catch_e1

    move-result v4

    .line 1914
    :try_start_98
    invoke-direct {p0, v4}, Lt/h;->d(I)Lt/p;

    move-result-object v8

    move v0, v3

    .line 1915
    :goto_9d
    invoke-virtual {v8}, Lt/p;->b()I

    move-result v9

    if-ge v0, v9, :cond_d7

    .line 1916
    invoke-virtual {v8, v0}, Lt/p;->b(I)J

    move-result-wide v9

    const-wide/16 v11, -0x1

    cmp-long v9, v9, v11

    if-eqz v9, :cond_b6

    invoke-virtual {v8, v0}, Lt/p;->e(I)I

    move-result v9

    if-lez v9, :cond_b6

    .line 1917
    invoke-virtual {v8, v0}, Lt/p;->a(I)V
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_98 .. :try_end_b6} :catch_dc

    .line 1915
    :cond_b6
    add-int/lit8 v0, v0, 0x1

    goto :goto_9d

    .line 1852
    :cond_b9
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->d:I

    goto/16 :goto_14

    .line 1900
    :cond_bf
    if-eqz v0, :cond_c4

    move-object v4, v0

    move-object v0, v2

    .line 1902
    goto :goto_6a

    .line 1904
    :cond_c4
    :try_start_c4
    invoke-virtual {v4}, Lt/p;->a()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v1}, Lt/p;->b()I

    move-result v8

    invoke-virtual {v1}, Lt/p;->c()I

    move-result v9

    invoke-direct {p0, v4, v8, v9}, Lt/h;->a(III)Lt/p;
    :try_end_d5
    .catch Ljava/io/IOException; {:try_start_c4 .. :try_end_d5} :catch_e1

    move-result-object v4

    goto :goto_6a

    .line 1920
    :cond_d7
    const/4 v0, 0x0

    :try_start_d8
    invoke-direct {p0, v8, v0}, Lt/h;->a(Lt/p;Z)V
    :try_end_db
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_db} :catch_dc

    goto :goto_88

    .line 1921
    :catch_dc
    move-exception v0

    .line 1922
    :try_start_dd
    invoke-direct {p0, v4}, Lt/h;->g(I)V
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_dd .. :try_end_e0} :catch_e1

    goto :goto_88

    .line 1932
    :catch_e1
    move-exception v0

    .line 1935
    :goto_e2
    if-eqz v1, :cond_eb

    .line 1936
    invoke-virtual {v1}, Lt/p;->a()I

    move-result v1

    invoke-direct {p0, v1}, Lt/h;->g(I)V

    .line 1938
    :cond_eb
    const-string v1, "Failed to combine refCounted records"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_d

    .line 1925
    :cond_f2
    :try_start_f2
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1926
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v5, v2

    .line 1927
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Combined refCounted records from "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " shards in "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " milliseconds"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1930
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cache:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_139
    .catch Ljava/io/IOException; {:try_start_f2 .. :try_end_139} :catch_e1

    goto/16 :goto_d

    .line 1932
    :catch_13b
    move-exception v0

    move-object v1, v2

    goto :goto_e2
.end method

.method public static b([BII)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3117
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0x8

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    .line 3118
    int-to-byte v1, p2

    aput-byte v1, p0, v0

    .line 3119
    return-void
.end method

.method static c([BII)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3143
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 3144
    invoke-virtual {v0, p0, p1, p2}, Ljava/util/zip/CRC32;->update([BII)V

    .line 3145
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    .line 3146
    long-to-int v0, v0

    return v0
.end method

.method public static c([BI)J
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 3127
    invoke-static {p0, p1}, Lt/h;->a([BI)I

    move-result v0

    int-to-long v0, v0

    .line 3128
    add-int/lit8 v2, p1, 0x4

    invoke-static {p0, v2}, Lt/h;->a([BI)I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    .line 3129
    const/16 v4, 0x20

    shl-long/2addr v0, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private c(Ljava/util/Collection;)Ljava/util/Collection;
    .registers 15
    .parameter

    .prologue
    .line 2001
    invoke-virtual {p0}, Lt/h;->h()V

    .line 2002
    new-instance v5, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 2003
    new-instance v6, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 2004
    new-instance v7, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v7, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 2008
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2009
    invoke-static {v8}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 2011
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2a
    :goto_2a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_77

    .line 2012
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/l;

    .line 2013
    iget-object v2, v0, Lt/l;->a:Lt/m;

    invoke-virtual {v2}, Lt/m;->a()J

    move-result-wide v2

    const-wide/16 v9, -0x1

    cmp-long v2, v2, v9

    if-eqz v2, :cond_49

    iget-object v2, v0, Lt/l;->b:[B

    array-length v2, v2

    const/16 v3, 0xff

    if-le v2, v3, :cond_4d

    .line 2016
    :cond_49
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_2a

    .line 2018
    :cond_4d
    iget-object v2, v0, Lt/l;->a:Lt/m;

    invoke-interface {v6, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_64

    .line 2020
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 2022
    iget v2, v0, Lt/l;->c:I

    if-lez v2, :cond_2a

    .line 2023
    iget v2, v0, Lt/l;->c:I

    iget-object v0, v0, Lt/l;->a:Lt/m;

    invoke-static {v2, v0, v7}, Lt/h;->a(ILt/m;Ljava/util/Map;)V

    goto :goto_2a

    .line 2026
    :cond_64
    iget-object v2, v0, Lt/l;->a:Lt/m;

    invoke-virtual {v2}, Lt/m;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2027
    iget-object v0, v0, Lt/l;->a:Lt/m;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2a

    .line 2035
    :cond_77
    const/4 v0, 0x0

    move v1, v0

    :goto_79
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget v0, v0, Lt/r;->g:I

    if-ge v1, v0, :cond_147

    .line 2036
    const/4 v2, 0x0

    .line 2037
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_84
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_190

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2038
    iget-object v4, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v4, v1}, Lt/r;->d(I)Z

    move-result v4

    if-eqz v4, :cond_84

    iget-object v4, p0, Lt/h;->k:Lt/j;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-virtual {v4, v9, v10, v1}, Lt/j;->b(JI)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 2039
    const/4 v0, 0x1

    .line 2043
    :goto_a5
    if-eqz v0, :cond_13b

    .line 2044
    const/4 v0, 0x0

    .line 2046
    :try_start_a8
    invoke-direct {p0, v1}, Lt/h;->d(I)Lt/p;
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_ab} :catch_115

    move-result-object v0

    move-object v4, v0

    .line 2052
    :goto_ad
    if-eqz v4, :cond_13b

    .line 2053
    const/4 v2, 0x0

    .line 2054
    const/4 v0, 0x0

    :goto_b1
    invoke-virtual {v4}, Lt/p;->b()I

    move-result v3

    if-ge v0, v3, :cond_135

    .line 2055
    invoke-virtual {v4, v0}, Lt/p;->b(I)J

    move-result-wide v9

    .line 2056
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_112

    .line 2057
    sget-object v3, Lt/h;->d:[B

    .line 2061
    invoke-virtual {v4, v0}, Lt/p;->c(I)I

    move-result v11

    if-lez v11, :cond_d5

    .line 2062
    invoke-virtual {v4, v0}, Lt/p;->f(I)Lt/o;

    move-result-object v3

    .line 2065
    :try_start_d1
    invoke-direct {p0, v3}, Lt/h;->a(Lt/o;)[B
    :try_end_d4
    .catch Ljava/io/IOException; {:try_start_d1 .. :try_end_d4} :catch_18d

    move-result-object v3

    .line 2072
    :cond_d5
    new-instance v11, Lt/m;

    invoke-direct {v11, v9, v10, v3}, Lt/m;-><init>(J[B)V

    .line 2073
    invoke-interface {v6, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_112

    .line 2074
    iget v2, p0, Lt/h;->w:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lt/h;->w:I

    .line 2075
    iget-object v2, p0, Lt/h;->B:Lt/s;

    if-eqz v2, :cond_ef

    .line 2076
    iget-object v2, p0, Lt/h;->B:Lt/s;

    invoke-interface {v2, v9, v10}, Lt/s;->a(J)V

    .line 2078
    :cond_ef
    iget-object v2, p0, Lt/h;->m:LR/h;

    monitor-enter v2

    .line 2079
    :try_start_f2
    iget-object v3, p0, Lt/h;->m:LR/h;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v3, v12}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2080
    monitor-exit v2
    :try_end_fc
    .catchall {:try_start_f2 .. :try_end_fc} :catchall_132

    .line 2081
    iget-object v2, p0, Lt/h;->A:Lt/x;

    if-eqz v2, :cond_105

    .line 2082
    iget-object v2, p0, Lt/h;->A:Lt/x;

    invoke-interface {v2, v9, v10}, Lt/x;->a(J)V

    .line 2087
    :cond_105
    invoke-virtual {v4, v0}, Lt/p;->e(I)I

    move-result v2

    .line 2088
    if-lez v2, :cond_10e

    .line 2090
    invoke-static {v2, v11, v7}, Lt/h;->a(ILt/m;Ljava/util/Map;)V

    .line 2093
    :cond_10e
    invoke-virtual {v4, v0}, Lt/p;->a(I)V

    .line 2094
    const/4 v2, 0x1

    .line 2054
    :cond_112
    :goto_112
    add-int/lit8 v0, v0, 0x1

    goto :goto_b1

    .line 2047
    :catch_115
    move-exception v2

    .line 2048
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeOldRecordsAndFilterInsertions: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2049
    invoke-direct {p0, v1}, Lt/h;->g(I)V

    move-object v4, v0

    goto/16 :goto_ad

    .line 2080
    :catchall_132
    move-exception v0

    :try_start_133
    monitor-exit v2
    :try_end_134
    .catchall {:try_start_133 .. :try_end_134} :catchall_132

    throw v0

    .line 2098
    :cond_135
    if-eqz v2, :cond_140

    .line 2099
    const/4 v0, 0x0

    invoke-direct {p0, v4, v0}, Lt/h;->a(Lt/p;Z)V

    .line 2035
    :cond_13b
    :goto_13b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_79

    .line 2105
    :cond_140
    iget v0, p0, Lt/h;->v:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lt/h;->v:I

    goto :goto_13b

    .line 2113
    :cond_147
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2114
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_157
    if-ltz v2, :cond_18f

    .line 2115
    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/l;

    .line 2116
    iget-object v1, v0, Lt/l;->a:Lt/m;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2117
    if-eqz v1, :cond_186

    .line 2120
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v4, v0, Lt/l;->c:I

    invoke-static {v1, v4}, Lr/u;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2122
    new-instance v1, Lt/l;

    iget-object v5, v0, Lt/l;->a:Lt/m;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v0, Lt/l;->d:[B

    const/4 v6, 0x0

    invoke-direct {v1, v5, v4, v0, v6}, Lt/l;-><init>(Lt/m;I[BLt/i;)V

    move-object v0, v1

    .line 2124
    :cond_186
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2114
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_157

    .line 2066
    :catch_18d
    move-exception v3

    goto :goto_112

    .line 2126
    :cond_18f
    return-object v3

    :cond_190
    move v0, v2

    goto/16 :goto_a5
.end method

.method private static c(JLjava/lang/String;)Lt/m;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1309
    sget-object v0, Lt/h;->d:[B

    .line 1311
    if-eqz p2, :cond_8

    .line 1312
    invoke-static {p2}, Lcom/google/googlenav/common/io/o;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 1315
    :cond_8
    new-instance v1, Lt/m;

    invoke-direct {v1, p0, p1, v0}, Lt/m;-><init>(J[B)V

    return-object v1
.end method

.method private c(Lt/m;)[B
    .registers 7
    .parameter

    .prologue
    .line 1349
    invoke-direct {p0, p1}, Lt/h;->a(Lt/m;)Lt/o;

    move-result-object v1

    .line 1350
    if-eqz v1, :cond_4f

    .line 1353
    :try_start_6
    iget v0, v1, Lt/o;->g:I

    invoke-virtual {p0, v0}, Lt/h;->b(I)Lj/c;

    move-result-object v2

    .line 1354
    iget v0, v1, Lt/o;->e:I

    new-array v0, v0, [B

    .line 1355
    iget v3, v1, Lt/o;->b:I

    iget v4, v1, Lt/o;->d:I

    add-int/2addr v3, v4

    invoke-static {v2, v3, v0}, Lt/h;->a(Lj/c;I[B)V

    .line 1358
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Lt/h;->c([BII)I

    move-result v2

    .line 1359
    iget v3, v1, Lt/o;->f:I

    if-eq v2, v3, :cond_51

    .line 1360
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Checksum mismatch: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " record ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4b
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_4b} :catch_4b

    .line 1367
    :catch_4b
    move-exception v0

    .line 1368
    invoke-direct {p0, v1, v0}, Lt/h;->a(Lt/o;Ljava/io/IOException;)V

    .line 1371
    :cond_4f
    const/4 v0, 0x0

    :goto_50
    return-object v0

    .line 1365
    :cond_51
    :try_start_51
    iget-object v2, p0, Lt/h;->j:Lt/r;

    iget v3, v1, Lt/o;->g:I

    invoke-direct {p0}, Lt/h;->p()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lt/r;->a(II)V
    :try_end_5c
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_5c} :catch_4b

    goto :goto_50
.end method

.method private d(I)Lt/p;
    .registers 6
    .parameter

    .prologue
    .line 929
    iget-object v1, p0, Lt/h;->h:Lj/c;

    monitor-enter v1

    .line 930
    :try_start_3
    iget-object v0, p0, Lt/h;->h:Lj/c;

    mul-int/lit16 v2, p1, 0x2000

    iget-object v3, p0, Lt/h;->i:Lt/k;

    iget v3, v3, Lt/k;->k:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lj/c;->a(J)V

    .line 931
    iget-object v0, p0, Lt/h;->h:Lj/c;

    invoke-static {v0}, Lt/p;->a(Lj/c;)Lt/p;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 932
    :catchall_18
    move-exception v0

    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method private e(I)V
    .registers 4
    .parameter

    .prologue
    .line 989
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 991
    :try_start_8
    invoke-direct {p0, p1}, Lt/h;->g(I)V
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_11

    .line 993
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 996
    :cond_10
    return-void

    .line 993
    :catchall_11
    move-exception v0

    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private f(I)V
    .registers 6
    .parameter

    .prologue
    .line 1604
    invoke-virtual {p0}, Lt/h;->h()V

    .line 1605
    iget-object v1, p0, Lt/h;->h:Lj/c;

    monitor-enter v1

    .line 1606
    :try_start_6
    iget-object v0, p0, Lt/h;->h:Lj/c;

    mul-int/lit16 v2, p1, 0x400

    add-int/lit16 v2, v2, 0x4000

    int-to-long v2, v2

    invoke-interface {v0, v2, v3}, Lj/c;->a(J)V

    .line 1607
    iget-object v0, p0, Lt/h;->k:Lt/j;

    iget-object v2, p0, Lt/h;->h:Lj/c;

    invoke-virtual {v0, v2, p1}, Lt/j;->a(Lj/c;I)V

    .line 1608
    iget-object v0, p0, Lt/h;->h:Lj/c;

    invoke-interface {v0}, Lj/c;->b()V

    .line 1609
    monitor-exit v1

    .line 1610
    return-void

    .line 1609
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_6 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method private g(I)V
    .registers 5
    .parameter

    .prologue
    .line 2136
    invoke-virtual {p0}, Lt/h;->h()V

    .line 2138
    new-instance v0, Lt/p;

    invoke-direct {v0, p1}, Lt/p;-><init>(I)V

    .line 2140
    const/4 v1, 0x0

    :try_start_9
    invoke-direct {p0, v0, v1}, Lt/h;->a(Lt/p;Z)V

    .line 2141
    iget-object v1, p0, Lt/h;->m:LR/h;

    monitor-enter v1
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_f} :catch_27

    .line 2142
    :try_start_f
    iget-object v0, p0, Lt/h;->m:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 2143
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_f .. :try_end_15} :catchall_24

    .line 2144
    :try_start_15
    iget-object v0, p0, Lt/h;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2145
    iget-object v0, p0, Lt/h;->B:Lt/s;

    if-eqz v0, :cond_23

    .line 2146
    iget-object v0, p0, Lt/h;->B:Lt/s;

    invoke-interface {v0, p1}, Lt/s;->a(I)V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_23} :catch_27

    .line 2151
    :cond_23
    :goto_23
    return-void

    .line 2143
    :catchall_24
    move-exception v0

    :try_start_25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    :try_start_26
    throw v0
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_27} :catch_27

    .line 2148
    :catch_27
    move-exception v0

    .line 2149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_23
.end method

.method private h(I)V
    .registers 4
    .parameter

    .prologue
    .line 2161
    invoke-virtual {p0}, Lt/h;->h()V

    .line 2162
    invoke-direct {p0, p1}, Lt/h;->g(I)V

    .line 2165
    iget-object v0, p0, Lt/h;->l:[Lj/c;

    aget-object v0, v0, p1

    if-eqz v0, :cond_18

    .line 2166
    iget-object v0, p0, Lt/h;->l:[Lj/c;

    aget-object v0, v0, p1

    invoke-interface {v0}, Lj/c;->a()V

    .line 2167
    iget-object v0, p0, Lt/h;->l:[Lj/c;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 2169
    :cond_18
    iget-object v0, p0, Lt/h;->g:Lj/d;

    invoke-virtual {p0, p1}, Lt/h;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lj/d;->a(Ljava/lang/String;)V

    .line 2170
    return-void
.end method

.method static synthetic i()[B
    .registers 1

    .prologue
    .line 214
    sget-object v0, Lt/h;->d:[B

    return-object v0
.end method

.method private static j()I
    .registers 2

    .prologue
    .line 630
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    .line 632
    if-nez v0, :cond_e

    .line 633
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "VectorMapsParameters is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 635
    :cond_e
    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->g()I

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    return v0
.end method

.method private static k()I
    .registers 2

    .prologue
    .line 644
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 645
    sget v0, Lt/h;->e:I

    .line 647
    :goto_8
    return v0

    :cond_9
    sget v0, Lt/h;->a:I

    const v1, 0xffff

    and-int/2addr v0, v1

    goto :goto_8
.end method

.method private l()V
    .registers 4

    .prologue
    .line 1036
    iget-object v0, p0, Lt/h;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 1051
    :cond_8
    return-void

    .line 1040
    :cond_9
    invoke-virtual {p0}, Lt/h;->h()V

    .line 1043
    iget-object v1, p0, Lt/h;->n:Ljava/util/Set;

    monitor-enter v1

    .line 1044
    :try_start_f
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lt/h;->n:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1045
    iget-object v2, p0, Lt/h;->n:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 1046
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_f .. :try_end_1c} :catchall_30

    .line 1048
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/o;

    .line 1049
    invoke-direct {p0, v0}, Lt/h;->b(Lt/o;)V

    goto :goto_20

    .line 1046
    :catchall_30
    move-exception v0

    :try_start_31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    throw v0
.end method

.method private m()V
    .registers 3

    .prologue
    .line 1060
    iget-object v0, p0, Lt/h;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 1073
    :cond_8
    :goto_8
    return-void

    .line 1064
    :cond_9
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1069
    :try_start_11
    invoke-direct {p0}, Lt/h;->l()V
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_1a

    .line 1071
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_8

    :catchall_1a
    move-exception v0

    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private n()V
    .registers 5

    .prologue
    .line 1618
    invoke-virtual {p0}, Lt/h;->h()V

    .line 1619
    iget-object v1, p0, Lt/h;->h:Lj/c;

    monitor-enter v1

    .line 1620
    :try_start_6
    iget-object v0, p0, Lt/h;->h:Lj/c;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v2, v3}, Lj/c;->a(J)V

    .line 1621
    iget-object v0, p0, Lt/h;->j:Lt/r;

    iget-object v2, p0, Lt/h;->h:Lj/c;

    invoke-virtual {v0, v2}, Lt/r;->a(Lj/c;)V

    .line 1622
    iget-object v0, p0, Lt/h;->h:Lj/c;

    invoke-interface {v0}, Lj/c;->b()V

    .line 1623
    monitor-exit v1

    .line 1624
    return-void

    .line 1623
    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_6 .. :try_end_1d} :catchall_1b

    throw v0
.end method

.method private o()Lt/p;
    .registers 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v4, -0x1

    .line 1640
    invoke-virtual {p0}, Lt/h;->h()V

    .line 1641
    sget-boolean v0, Lt/h;->c:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lt/h;->C:Ljava/util/Set;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_14
    move v0, v1

    .line 1647
    :goto_15
    iget-object v3, p0, Lt/h;->j:Lt/r;

    iget v3, v3, Lt/r;->g:I

    if-ge v0, v3, :cond_bc

    .line 1649
    iget-object v3, p0, Lt/h;->j:Lt/r;

    iget-object v3, v3, Lt/r;->c:[I

    aget v3, v3, v0

    iget-object v5, p0, Lt/h;->i:Lt/k;

    iget v5, v5, Lt/k;->e:I

    if-ge v3, v5, :cond_77

    iget-object v3, p0, Lt/h;->j:Lt/r;

    iget-object v3, v3, Lt/r;->b:[I

    aget v3, v3, v0

    const v5, 0x7ffffff

    if-gt v3, v5, :cond_77

    .line 1653
    :try_start_32
    invoke-direct {p0, v0}, Lt/h;->d(I)Lt/p;
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_35} :catch_5d

    move-result-object v3

    move-object v7, v3

    move v3, v0

    move-object v0, v7

    .line 1667
    :goto_39
    if-ne v3, v4, :cond_9c

    .line 1670
    invoke-direct {p0}, Lt/h;->q()Z

    move-result v3

    .line 1671
    invoke-direct {p0, v3}, Lt/h;->b(Z)V

    .line 1675
    iget-object v5, p0, Lt/h;->i:Lt/k;

    iget-boolean v5, v5, Lt/k;->f:Z

    if-eqz v5, :cond_7a

    if-eqz v3, :cond_7a

    .line 1679
    :goto_4a
    const/4 v5, 0x2

    if-ge v1, v5, :cond_7a

    .line 1680
    iget-object v5, p0, Lt/h;->j:Lt/r;

    iget-object v6, p0, Lt/h;->C:Ljava/util/Set;

    invoke-virtual {v5, v6}, Lt/r;->a(Ljava/util/Set;)I

    move-result v5

    .line 1681
    if-eq v5, v4, :cond_5a

    .line 1682
    invoke-direct {p0, v5}, Lt/h;->h(I)V

    .line 1679
    :cond_5a
    add-int/lit8 v1, v1, 0x1

    goto :goto_4a

    .line 1657
    :catch_5d
    move-exception v3

    .line 1659
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "allocateShardToUse: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v3}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v3, v0

    move-object v0, v2

    .line 1661
    goto :goto_39

    .line 1647
    :cond_77
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 1687
    :cond_7a
    invoke-direct {p0, v3}, Lt/h;->a(Z)I

    move-result v1

    .line 1688
    if-ne v1, v4, :cond_9d

    .line 1691
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cache:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Tile store full, unable to allocate shard"

    invoke-static {v0, v1}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 1706
    :goto_9b
    return-object v0

    :cond_9c
    move v1, v3

    .line 1697
    :cond_9d
    if-eqz v0, :cond_ab

    invoke-virtual {v0}, Lt/p;->b()I

    move-result v2

    iget-object v3, p0, Lt/h;->j:Lt/r;

    iget-object v3, v3, Lt/r;->c:[I

    aget v3, v3, v1

    if-eq v2, v3, :cond_b0

    .line 1700
    :cond_ab
    new-instance v0, Lt/p;

    invoke-direct {v0, v1}, Lt/p;-><init>(I)V

    .line 1704
    :cond_b0
    iput v1, p0, Lt/h;->x:I

    .line 1705
    iget-object v2, p0, Lt/h;->C:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9b

    :cond_bc
    move-object v0, v2

    move v3, v4

    goto/16 :goto_39
.end method

.method private p()I
    .registers 5

    .prologue
    .line 3041
    iget v0, p0, Lt/h;->y:I

    if-ltz v0, :cond_7

    .line 3042
    iget v0, p0, Lt/h;->y:I

    .line 3044
    :goto_6
    return v0

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_6
.end method

.method private q()Z
    .registers 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3051
    invoke-virtual {p0}, Lt/h;->h()V

    .line 3052
    iget-boolean v2, p0, Lt/h;->z:Z

    if-eqz v2, :cond_a

    .line 3066
    :cond_9
    :goto_9
    return v0

    .line 3056
    :cond_a
    iget-object v2, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v2}, Lt/r;->e()I

    move-result v2

    const/16 v3, 0x14

    if-ge v2, v3, :cond_16

    move v0, v1

    .line 3057
    goto :goto_9

    .line 3061
    :cond_16
    invoke-static {}, LJ/a;->f()J

    move-result-wide v2

    .line 3062
    iget-object v4, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v4}, Lt/r;->d()J

    move-result-wide v4

    .line 3063
    add-long/2addr v2, v4

    long-to-double v2, v2

    const-wide/high16 v6, 0x3fd0

    mul-double/2addr v2, v6

    double-to-long v2, v2

    .line 3066
    cmp-long v2, v2, v4

    if-ltz v2, :cond_9

    move v0, v1

    goto :goto_9
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 694
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->g:I

    return v0
.end method

.method public a(JLjava/lang/String;I)I
    .registers 18
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1565
    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1568
    :try_start_5
    invoke-static/range {p1 .. p3}, Lt/h;->c(JLjava/lang/String;)Lt/m;

    move-result-object v1

    .line 1569
    invoke-direct {p0, v1}, Lt/h;->b(Lt/m;)Lt/q;
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_69

    move-result-object v11

    .line 1570
    if-nez v11, :cond_16

    .line 1571
    const/4 v7, -0x1

    .line 1593
    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1595
    :goto_15
    return v7

    .line 1574
    :cond_16
    :try_start_16
    iget-object v1, p0, Lt/h;->j:Lt/r;

    iget-object v1, v1, Lt/r;->e:[I

    iget-object v2, v11, Lt/q;->a:Lt/p;

    invoke-virtual {v2}, Lt/p;->a()I

    move-result v2

    aget v12, v1, v2

    .line 1577
    iget-object v10, v11, Lt/q;->b:Lt/o;

    .line 1579
    iget v1, v10, Lt/o;->c:I

    move/from16 v0, p4

    invoke-static {v1, v0}, Lr/u;->a(II)I

    move-result v1

    and-int/lit8 v7, v1, 0x1f

    .line 1582
    new-instance v1, Lt/o;

    iget-wide v2, v10, Lt/o;->a:J

    iget v4, v10, Lt/o;->b:I

    iget v5, v10, Lt/o;->d:I

    iget v6, v10, Lt/o;->e:I

    iget v8, v10, Lt/o;->f:I

    iget v9, v10, Lt/o;->g:I

    iget v10, v10, Lt/o;->h:I

    invoke-direct/range {v1 .. v10}, Lt/o;-><init>(JIIIIIII)V

    .line 1584
    iget-object v2, v11, Lt/q;->a:Lt/p;

    iget v3, v11, Lt/q;->c:I

    invoke-virtual {v2, v1, v3}, Lt/p;->a(Lt/o;I)V

    .line 1585
    iget-object v1, v11, Lt/q;->a:Lt/p;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lt/h;->a(Lt/p;Z)V

    .line 1588
    if-nez v12, :cond_63

    iget-object v1, p0, Lt/h;->j:Lt/r;

    iget-object v1, v1, Lt/r;->e:[I

    iget-object v2, v11, Lt/q;->a:Lt/p;

    invoke-virtual {v2}, Lt/p;->a()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_63

    .line 1590
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lt/h;->b(Z)V
    :try_end_63
    .catchall {:try_start_16 .. :try_end_63} :catchall_69

    .line 1593
    :cond_63
    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_15

    :catchall_69
    move-exception v1

    iget-object v2, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public a(Ljava/util/Collection;)I
    .registers 18
    .parameter

    .prologue
    .line 1457
    move-object/from16 v0, p0

    iget-object v1, v0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1459
    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lt/h;->q:Z
    :try_end_b
    .catchall {:try_start_7 .. :try_end_b} :catchall_bb

    if-eqz v1, :cond_1b

    .line 1460
    const/4 v1, -0x1

    .line 1523
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lt/h;->C:Ljava/util/Set;

    .line 1524
    move-object/from16 v0, p0

    iget-object v2, v0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1526
    :goto_1a
    return v1

    .line 1462
    :cond_1b
    :try_start_1b
    invoke-direct/range {p0 .. p1}, Lt/h;->c(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    .line 1465
    invoke-direct/range {p0 .. p0}, Lt/h;->l()V

    .line 1468
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lt/h;->b(Ljava/util/Collection;)I

    move-result v2

    .line 1469
    const/high16 v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    new-array v13, v2, [B

    .line 1471
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lt/h;->C:Ljava/util/Set;

    .line 1472
    invoke-direct/range {p0 .. p0}, Lt/h;->o()Lt/p;
    :try_end_3c
    .catchall {:try_start_1b .. :try_end_3c} :catchall_bb

    move-result-object v3

    .line 1473
    if-nez v3, :cond_4d

    .line 1474
    const/4 v1, -0x1

    .line 1523
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lt/h;->C:Ljava/util/Set;

    .line 1524
    move-object/from16 v0, p0

    iget-object v2, v0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1a

    .line 1476
    :cond_4d
    :try_start_4d
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 1477
    new-instance v2, Lt/n;

    invoke-virtual {v3}, Lt/p;->a()I

    move-result v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lt/h;->b(I)Lj/c;

    move-result-object v4

    invoke-virtual {v3}, Lt/p;->c()I

    move-result v5

    invoke-direct {v2, v4, v5, v13}, Lt/n;-><init>(Lj/c;I[B)V

    .line 1479
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_69
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_137

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lt/l;

    move-object v8, v0

    .line 1482
    iget v1, v8, Lt/l;->c:I

    and-int/lit8 v7, v1, 0x1f

    .line 1484
    invoke-virtual {v3}, Lt/p;->b()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lt/h;->i:Lt/k;

    iget v4, v4, Lt/k;->e:I

    if-ge v1, v4, :cond_90

    invoke-virtual {v3}, Lt/p;->c()I

    move-result v1

    const v4, 0x7ffffff

    if-le v1, v4, :cond_171

    .line 1486
    :cond_90
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lt/h;->a(Lt/n;Lt/p;)V

    .line 1487
    move-object/from16 v0, p0

    iget-object v1, v0, Lt/h;->B:Lt/s;

    if-eqz v1, :cond_c9

    .line 1488
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1489
    move-object/from16 v0, p0

    iget-object v1, v0, Lt/h;->B:Lt/s;

    invoke-virtual {v3}, Lt/p;->a()I

    move-result v6

    invoke-interface {v1, v4, v5, v6}, Lt/s;->a(JI)V
    :try_end_ba
    .catchall {:try_start_4d .. :try_end_ba} :catchall_bb

    goto :goto_9f

    .line 1523
    :catchall_bb
    move-exception v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lt/h;->C:Ljava/util/Set;

    .line 1524
    move-object/from16 v0, p0

    iget-object v2, v0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    .line 1492
    :cond_c9
    :try_start_c9
    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 1494
    invoke-direct/range {p0 .. p0}, Lt/h;->o()Lt/p;
    :try_end_cf
    .catchall {:try_start_c9 .. :try_end_cf} :catchall_bb

    move-result-object v2

    .line 1495
    if-nez v2, :cond_e1

    .line 1496
    const/4 v1, -0x1

    .line 1523
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lt/h;->C:Ljava/util/Set;

    .line 1524
    move-object/from16 v0, p0

    iget-object v2, v0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto/16 :goto_1a

    .line 1498
    :cond_e1
    :try_start_e1
    new-instance v1, Lt/n;

    invoke-virtual {v2}, Lt/p;->a()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lt/h;->b(I)Lj/c;

    move-result-object v3

    invoke-virtual {v2}, Lt/p;->c()I

    move-result v4

    invoke-direct {v1, v3, v4, v13}, Lt/n;-><init>(Lj/c;I[B)V

    move-object v11, v1

    move-object v12, v2

    .line 1502
    :goto_f6
    iget-object v1, v8, Lt/l;->b:[B

    invoke-virtual {v11, v1}, Lt/n;->a([B)V

    .line 1503
    iget-object v1, v8, Lt/l;->d:[B

    invoke-virtual {v11, v1}, Lt/n;->a([B)V

    .line 1504
    new-instance v1, Lt/o;

    iget-object v2, v8, Lt/l;->a:Lt/m;

    invoke-virtual {v2}, Lt/m;->a()J

    move-result-wide v2

    invoke-virtual {v12}, Lt/p;->c()I

    move-result v4

    iget-object v5, v8, Lt/l;->b:[B

    array-length v5, v5

    iget-object v6, v8, Lt/l;->d:[B

    array-length v6, v6

    iget-object v9, v8, Lt/l;->d:[B

    const/4 v10, 0x0

    iget-object v8, v8, Lt/l;->d:[B

    array-length v8, v8

    invoke-static {v9, v10, v8}, Lt/h;->c([BII)I

    move-result v8

    invoke-virtual {v12}, Lt/p;->a()I

    move-result v9

    invoke-virtual {v12}, Lt/p;->b()I

    move-result v10

    invoke-direct/range {v1 .. v10}, Lt/o;-><init>(JIIIIIII)V

    .line 1509
    invoke-virtual {v12, v1}, Lt/p;->a(Lt/o;)V

    .line 1510
    iget-wide v1, v1, Lt/o;->a:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v11

    move-object v3, v12

    .line 1514
    goto/16 :goto_69

    .line 1516
    :cond_137
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lt/h;->a(Lt/n;Lt/p;)V

    .line 1517
    move-object/from16 v0, p0

    iget-object v1, v0, Lt/h;->B:Lt/s;

    if-eqz v1, :cond_162

    .line 1518
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_146
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_162

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1519
    move-object/from16 v0, p0

    iget-object v1, v0, Lt/h;->B:Lt/s;

    invoke-virtual {v3}, Lt/p;->a()I

    move-result v6

    invoke-interface {v1, v4, v5, v6}, Lt/s;->a(JI)V
    :try_end_161
    .catchall {:try_start_e1 .. :try_end_161} :catchall_bb

    goto :goto_146

    .line 1523
    :cond_162
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lt/h;->C:Ljava/util/Set;

    .line 1524
    move-object/from16 v0, p0

    iget-object v1, v0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1526
    const/4 v1, 0x0

    goto/16 :goto_1a

    :cond_171
    move-object v11, v2

    move-object v12, v3

    goto :goto_f6
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 876
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->a:I

    invoke-direct {p0, p1, v0}, Lt/h;->b(II)V

    .line 877
    return-void
.end method

.method public a(ILjava/util/Locale;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 811
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 815
    const/4 v0, 0x1

    :try_start_6
    iput-boolean v0, p0, Lt/h;->q:Z

    .line 816
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_11
    .catchall {:try_start_6 .. :try_end_11} :catchall_3c

    .line 818
    :try_start_11
    iget-object v0, p0, Lt/h;->B:Lt/s;

    if-eqz v0, :cond_1a

    .line 819
    iget-object v0, p0, Lt/h;->B:Lt/s;

    invoke-interface {v0}, Lt/s;->a()V

    .line 821
    :cond_1a
    invoke-direct {p0, p1, p2}, Lt/h;->b(ILjava/util/Locale;)V
    :try_end_1d
    .catchall {:try_start_11 .. :try_end_1d} :catchall_31
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_1d} :catch_2c

    .line 826
    :try_start_1d
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_26
    .catchall {:try_start_1d .. :try_end_26} :catchall_3c

    .line 829
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 831
    return-void

    .line 822
    :catch_2c
    move-exception v0

    .line 823
    :try_start_2d
    invoke-virtual {p0}, Lt/h;->g()V

    .line 824
    throw v0
    :try_end_31
    .catchall {:try_start_2d .. :try_end_31} :catchall_31

    .line 826
    :catchall_31
    move-exception v0

    :try_start_32
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
    :try_end_3c
    .catchall {:try_start_32 .. :try_end_3c} :catchall_3c

    .line 829
    :catchall_3c
    move-exception v0

    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public a(J)[B
    .registers 4
    .parameter

    .prologue
    .line 1344
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lt/h;->a(JLjava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/lang/String;)[B
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1326
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1328
    :try_start_9
    iget-boolean v0, p0, Lt/h;->q:Z
    :try_end_b
    .catchall {:try_start_9 .. :try_end_b} :catchall_30

    if-eqz v0, :cond_1b

    .line 1329
    const/4 v0, 0x0

    .line 1333
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1334
    invoke-direct {p0}, Lt/h;->m()V

    :goto_1a
    return-object v0

    .line 1331
    :cond_1b
    :try_start_1b
    invoke-static {p1, p2, p3}, Lt/h;->c(JLjava/lang/String;)Lt/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lt/h;->c(Lt/m;)[B
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_30

    move-result-object v0

    .line 1333
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1334
    invoke-direct {p0}, Lt/h;->m()V

    goto :goto_1a

    .line 1333
    :catchall_30
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1334
    invoke-direct {p0}, Lt/h;->m()V

    throw v0
.end method

.method public b()J
    .registers 3

    .prologue
    .line 702
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget-wide v0, v0, Lt/k;->h:J

    return-wide v0
.end method

.method b(I)Lj/c;
    .registers 7
    .parameter

    .prologue
    .line 2177
    iget-object v1, p0, Lt/h;->l:[Lj/c;

    monitor-enter v1

    .line 2178
    :try_start_3
    iget-object v0, p0, Lt/h;->l:[Lj/c;

    aget-object v0, v0, p1

    if-nez v0, :cond_18

    .line 2179
    iget-object v0, p0, Lt/h;->l:[Lj/c;

    iget-object v2, p0, Lt/h;->g:Lj/d;

    invoke-virtual {p0, p1}, Lt/h;->c(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lj/d;->a(Ljava/lang/String;Z)Lj/c;

    move-result-object v2

    aput-object v2, v0, p1

    .line 2181
    :cond_18
    iget-object v0, p0, Lt/h;->l:[Lj/c;

    aget-object v0, v0, p1

    monitor-exit v1

    return-object v0

    .line 2182
    :catchall_1e
    move-exception v0

    monitor-exit v1
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    throw v0
.end method

.method public b(JLjava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1378
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 1380
    :try_start_9
    invoke-static {p1, p2, p3}, Lt/h;->c(JLjava/lang/String;)Lt/m;

    move-result-object v0

    invoke-direct {p0, v0}, Lt/h;->a(Lt/m;)Lt/o;
    :try_end_10
    .catchall {:try_start_9 .. :try_end_10} :catchall_23

    move-result-object v0

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    .line 1382
    :goto_14
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1383
    invoke-direct {p0}, Lt/h;->m()V

    return v0

    .line 1380
    :cond_21
    const/4 v0, 0x0

    goto :goto_14

    .line 1382
    :catchall_23
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 1383
    invoke-direct {p0}, Lt/h;->m()V

    throw v0
.end method

.method c(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 2212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Locale;
    .registers 2

    .prologue
    .line 709
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget-object v0, v0, Lt/k;->i:Ljava/util/Locale;

    return-object v0
.end method

.method public d()I
    .registers 3

    .prologue
    .line 716
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 718
    :try_start_9
    iget-object v0, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v0}, Lt/r;->c()I
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_19

    move-result v0

    .line 720
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return v0

    :catchall_19
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public e()J
    .registers 4

    .prologue
    .line 728
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 730
    :try_start_9
    iget-object v0, p0, Lt/h;->j:Lt/r;

    invoke-virtual {v0}, Lt/r;->d()J
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_19

    move-result-wide v0

    .line 732
    iget-object v2, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    return-wide v0

    :catchall_19
    move-exception v0

    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method public f()I
    .registers 3

    .prologue
    .line 740
    iget-object v0, p0, Lt/h;->i:Lt/k;

    iget v0, v0, Lt/k;->d:I

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v1, v1, Lt/k;->e:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public g()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 753
    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 755
    :try_start_6
    iget-boolean v1, p0, Lt/h;->q:Z
    :try_end_8
    .catchall {:try_start_6 .. :try_end_8} :catchall_4d

    if-eqz v1, :cond_10

    .line 794
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 796
    :goto_f
    return-void

    .line 761
    :cond_10
    const/4 v1, 0x1

    :try_start_11
    iput-boolean v1, p0, Lt/h;->q:Z

    .line 762
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->lock()V
    :try_end_1c
    .catchall {:try_start_11 .. :try_end_1c} :catchall_4d

    .line 768
    :try_start_1c
    invoke-direct {p0}, Lt/h;->n()V
    :try_end_1f
    .catchall {:try_start_1c .. :try_end_1f} :catchall_42
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1f} :catch_63

    .line 773
    :goto_1f
    :try_start_1f
    iget-object v1, p0, Lt/h;->h:Lj/c;

    invoke-interface {v1}, Lj/c;->a()V
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_42
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_24} :catch_67

    .line 777
    :goto_24
    const/4 v1, 0x0

    :goto_25
    :try_start_25
    iget-object v2, p0, Lt/h;->l:[Lj/c;

    array-length v2, v2

    if-ge v1, v2, :cond_3f

    .line 778
    iget-object v2, p0, Lt/h;->l:[Lj/c;

    aget-object v2, v2, v1
    :try_end_2e
    .catchall {:try_start_25 .. :try_end_2e} :catchall_42

    if-eqz v2, :cond_3c

    .line 780
    :try_start_30
    iget-object v2, p0, Lt/h;->l:[Lj/c;

    aget-object v2, v2, v1

    invoke-interface {v2}, Lj/c;->a()V
    :try_end_37
    .catchall {:try_start_30 .. :try_end_37} :catchall_42
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_37} :catch_65

    .line 784
    :goto_37
    :try_start_37
    iget-object v2, p0, Lt/h;->l:[Lj/c;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 777
    :cond_3c
    add-int/lit8 v1, v1, 0x1

    goto :goto_25

    .line 787
    :cond_3f
    if-eqz v0, :cond_54

    .line 788
    throw v0
    :try_end_42
    .catchall {:try_start_37 .. :try_end_42} :catchall_42

    .line 791
    :catchall_42
    move-exception v0

    :try_start_43
    iget-object v1, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V

    throw v0
    :try_end_4d
    .catchall {:try_start_43 .. :try_end_4d} :catchall_4d

    .line 794
    :catchall_4d
    move-exception v0

    iget-object v1, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    .line 791
    :cond_54
    :try_start_54
    iget-object v0, p0, Lt/h;->p:Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;->unlock()V
    :try_end_5d
    .catchall {:try_start_54 .. :try_end_5d} :catchall_4d

    .line 794
    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_f

    .line 769
    :catch_63
    move-exception v0

    goto :goto_1f

    .line 781
    :catch_65
    move-exception v0

    goto :goto_37

    .line 774
    :catch_67
    move-exception v0

    goto :goto_24
.end method

.method h()V
    .registers 3

    .prologue
    .line 3264
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lt/h;->o:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_16

    .line 3265
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Write lock must be held"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3267
    :cond_16
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3071
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lt/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ver:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lt/h;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " locale: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lt/h;->c()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " auto:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget-boolean v1, v1, Lt/k;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lt/h;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lt/h;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " max_shards:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lt/h;->i:Lt/k;

    iget v1, v1, Lt/k;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
