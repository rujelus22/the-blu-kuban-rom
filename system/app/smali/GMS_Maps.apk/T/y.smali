.class public Lt/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt/f;


# static fields
.field private static final a:Lo/ap;

.field private static final b:[B


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lt/J;

.field private final f:Ljava/util/HashMap;

.field private g:Lt/h;

.field private final h:I

.field private final i:LA/c;

.field private j:I

.field private k:Lcom/google/googlenav/common/a;

.field private l:Lt/A;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 58
    new-instance v0, Lo/N;

    invoke-direct {v0}, Lo/N;-><init>()V

    sput-object v0, Lt/y;->a:Lo/ap;

    .line 59
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lt/y;->b:[B

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILt/J;LA/c;Lt/g;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lt/y;->j:I

    .line 107
    new-instance v0, Lal/a;

    invoke-direct {v0}, Lal/a;-><init>()V

    iput-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Lt/y;->l:Lt/A;

    .line 378
    iput-object p1, p0, Lt/y;->c:Ljava/lang/String;

    .line 379
    iput p2, p0, Lt/y;->d:I

    .line 380
    iput-object p3, p0, Lt/y;->e:Lt/J;

    .line 381
    invoke-static {}, Lt/y;->h()I

    move-result v0

    iput v0, p0, Lt/y;->h:I

    .line 382
    iget v0, p0, Lt/y;->h:I

    invoke-static {v0}, Lcom/google/common/collect/Maps;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    .line 383
    iput-object p4, p0, Lt/y;->i:LA/c;

    .line 384
    if-eqz p5, :cond_31

    .line 385
    new-instance v0, Lt/A;

    iget-object v1, p0, Lt/y;->i:LA/c;

    invoke-direct {v0, v1, p5}, Lt/A;-><init>(LA/c;Lt/g;)V

    iput-object v0, p0, Lt/y;->l:Lt/A;

    .line 387
    :cond_31
    return-void
.end method

.method private a(Lo/aq;Lo/ap;[BI)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v8, -0x1

    .line 581
    iget-object v1, p0, Lt/y;->g:Lt/h;

    if-nez v1, :cond_10

    .line 582
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Uninitialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 584
    :cond_10
    instance-of v1, p2, Lo/P;

    if-eqz v1, :cond_1c

    .line 585
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Can\'t insert a MutableVectorTile into SD cache"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 587
    :cond_1c
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v1

    const/16 v2, 0x15

    if-le v1, v2, :cond_25

    .line 671
    :goto_24
    return-void

    .line 590
    :cond_25
    iget-object v1, p0, Lt/y;->i:LA/c;

    invoke-static {v1, p1}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v10

    .line 592
    array-length v1, p3

    if-lez v1, :cond_108

    .line 596
    instance-of v1, p2, Lo/m;

    if-eqz v1, :cond_104

    move-object v1, p2

    .line 597
    check-cast v1, Lo/m;

    invoke-interface {v1}, Lo/m;->a()J

    move-result-wide v1

    .line 600
    cmp-long v3, v1, v8

    if-eqz v3, :cond_101

    .line 601
    iget-object v3, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v6

    sub-long/2addr v1, v6

    iget-object v3, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v6

    add-long/2addr v1, v6

    .line 602
    cmp-long v3, v1, v4

    if-gez v3, :cond_101

    move-wide v2, v4

    :goto_50
    move-object v1, p2

    .line 608
    check-cast v1, Lo/m;

    invoke-interface {v1}, Lo/m;->b()J

    move-result-wide v6

    .line 611
    cmp-long v1, v6, v8

    if-eqz v1, :cond_fe

    .line 612
    iget-object v1, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget-object v1, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v8

    add-long/2addr v6, v8

    .line 614
    cmp-long v1, v6, v4

    if-gez v1, :cond_fe

    .line 624
    :goto_6d
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    array-length v1, p3

    add-int/lit8 v1, v1, 0x18

    invoke-direct {v7, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 626
    new-instance v8, Ljava/io/DataOutputStream;

    invoke-direct {v8, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 628
    const/16 v1, 0x18

    :try_start_7c
    invoke-virtual {v8, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 631
    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 632
    invoke-virtual {v8, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 633
    invoke-virtual {v8, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 634
    invoke-virtual {v8, p3}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_8c
    .catchall {:try_start_7c .. :try_end_8c} :catchall_ea
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_8c} :catch_dc

    .line 638
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 640
    :try_start_90
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    .line 641
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_96
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_96} :catch_f9

    .line 647
    :goto_96
    iget-object v11, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v11

    .line 649
    :try_start_99
    iget-object v1, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lt/I;

    move-object v9, v0

    .line 650
    if-eqz v9, :cond_fb

    .line 657
    iget v1, v9, Lt/I;->d:I

    move/from16 v0, p4

    invoke-static {v1, v0}, Lr/u;->a(II)I

    move-result v5

    .line 660
    :goto_ad
    new-instance v1, Lt/I;

    iget-object v2, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    if-eqz v9, :cond_f5

    iget-object v8, v9, Lt/I;->h:Ls/e;

    :goto_bf
    move-object v7, p2

    invoke-direct/range {v1 .. v8}, Lt/I;-><init>(JLjava/lang/String;I[BLo/ap;Ls/e;)V

    .line 663
    iget v2, p0, Lt/y;->j:I

    iget v3, p0, Lt/y;->h:I

    if-ge v2, v3, :cond_d6

    .line 665
    iget-object v2, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    if-nez v9, :cond_d6

    .line 667
    iget v1, p0, Lt/y;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lt/y;->j:I

    .line 670
    :cond_d6
    monitor-exit v11

    goto/16 :goto_24

    :catchall_d9
    move-exception v1

    monitor-exit v11
    :try_end_db
    .catchall {:try_start_99 .. :try_end_db} :catchall_d9

    throw v1

    .line 635
    :catch_dc
    move-exception v1

    .line 638
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 640
    :try_start_e1
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    .line 641
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_e7
    .catch Ljava/io/IOException; {:try_start_e1 .. :try_end_e7} :catch_e8

    goto :goto_96

    .line 642
    :catch_e8
    move-exception v1

    goto :goto_96

    .line 638
    :catchall_ea
    move-exception v1

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    .line 640
    :try_start_ee
    invoke-virtual {v8}, Ljava/io/DataOutputStream;->close()V

    .line 641
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_f4
    .catch Ljava/io/IOException; {:try_start_ee .. :try_end_f4} :catch_f7

    .line 644
    :goto_f4
    throw v1

    .line 660
    :cond_f5
    const/4 v8, 0x0

    goto :goto_bf

    .line 642
    :catch_f7
    move-exception v2

    goto :goto_f4

    :catch_f9
    move-exception v1

    goto :goto_96

    :cond_fb
    move/from16 v5, p4

    goto :goto_ad

    :cond_fe
    move-wide v4, v6

    goto/16 :goto_6d

    :cond_101
    move-wide v2, v1

    goto/16 :goto_50

    :cond_104
    move-wide v4, v8

    move-wide v2, v8

    goto/16 :goto_6d

    :cond_108
    move-object v6, p3

    goto :goto_96
.end method

.method private a(ILjava/util/Locale;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 771
    :try_start_0
    iget-object v1, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_1f

    .line 772
    :try_start_3
    iget-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 773
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0, p1, p2}, Lt/h;->a(ILjava/util/Locale;)V

    .line 774
    invoke-direct {p0}, Lt/y;->j()V

    .line 775
    iget-object v0, p0, Lt/y;->l:Lt/A;

    if-eqz v0, :cond_19

    .line 776
    iget-object v0, p0, Lt/y;->l:Lt/A;

    invoke-virtual {v0}, Lt/A;->b()V

    .line 778
    :cond_19
    monitor-exit v1

    .line 779
    const/4 v0, 0x1

    .line 782
    :goto_1b
    return v0

    .line 778
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    :try_start_1e
    throw v0
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1f} :catch_1f

    .line 780
    :catch_1f
    move-exception v0

    .line 781
    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 782
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private b(Ljava/io/File;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 411
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-eqz v0, :cond_8

    move v0, v6

    .line 449
    :goto_7
    return v0

    .line 415
    :cond_8
    new-instance v4, Lj/b;

    invoke-direct {v4, p1}, Lj/b;-><init>(Ljava/io/File;)V

    .line 416
    iget-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v8

    .line 419
    :try_start_13
    iget-object v0, p0, Lt/y;->c:Ljava/lang/String;

    iget-object v1, p0, Lt/y;->l:Lt/A;

    invoke-static {v0, v4, v1}, Lt/h;->a(Ljava/lang/String;Lj/d;Lt/s;)Lt/h;

    move-result-object v0

    iput-object v0, p0, Lt/y;->g:Lt/h;
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_1d} :catch_5f

    move v0, v7

    .line 427
    :goto_1e
    if-nez v0, :cond_2e

    invoke-virtual {p0}, Lt/y;->i()J

    move-result-wide v0

    iget-object v2, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v2}, Lt/h;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_62

    :cond_2e
    move v0, v6

    .line 429
    :goto_2f
    if-eqz v0, :cond_56

    .line 431
    :try_start_31
    iget-object v0, p0, Lt/y;->c:Ljava/lang/String;

    iget v1, p0, Lt/y;->d:I

    const/4 v2, -0x1

    new-instance v3, Ljava/util/Locale;

    const-string v5, ""

    invoke-direct {v3, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lt/y;->l:Lt/A;

    invoke-static/range {v0 .. v5}, Lt/h;->a(Ljava/lang/String;IILjava/util/Locale;Lj/d;Lt/s;)Lt/h;

    move-result-object v0

    iput-object v0, p0, Lt/y;->g:Lt/h;

    .line 433
    invoke-direct {p0}, Lt/y;->j()V

    .line 434
    iget-object v0, p0, Lt/y;->l:Lt/A;

    if-eqz v0, :cond_56

    .line 435
    iget-object v0, p0, Lt/y;->l:Lt/A;

    invoke-virtual {v0}, Lt/A;->a()V

    .line 436
    iget-object v0, p0, Lt/y;->l:Lt/A;

    invoke-virtual {v0}, Lt/A;->b()V
    :try_end_56
    .catch Ljava/io/IOException; {:try_start_31 .. :try_end_56} :catch_64

    .line 444
    :cond_56
    iget-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v0

    sub-long/2addr v0, v8

    move v0, v6

    .line 449
    goto :goto_7

    .line 420
    :catch_5f
    move-exception v0

    move v0, v6

    .line 421
    goto :goto_1e

    :cond_62
    move v0, v7

    .line 427
    goto :goto_2f

    .line 438
    :catch_64
    move-exception v0

    .line 439
    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Creating cache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 440
    goto :goto_7
.end method

.method static h()I
    .registers 2

    .prologue
    .line 397
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->h()I

    move-result v0

    shr-int/lit8 v0, v0, 0x3

    .line 398
    const/16 v1, 0x100

    mul-int/lit8 v0, v0, 0x20

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v1, 0x40

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private j()V
    .registers 5

    .prologue
    .line 788
    new-instance v0, Laq/b;

    invoke-direct {v0}, Laq/b;-><init>()V

    .line 789
    iget-object v1, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v1}, Lt/h;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Laq/b;->writeLong(J)V

    .line 790
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-virtual {v0}, Laq/b;->a()[B

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disk_creation_time_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lt/y;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    .line 792
    return-void
.end method


# virtual methods
.method public a(Lo/aq;Lo/ap;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 680
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t store unencrypted tiles into SD cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lo/aq;Lo/ap;[B)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 569
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lt/y;->a(Lo/aq;Lo/ap;[BI)V

    .line 570
    return-void
.end method

.method public a(Lo/aq;Ls/e;I)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 512
    iget-object v1, p0, Lt/y;->g:Lt/h;

    if-nez v1, :cond_c

    .line 513
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Uninitialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 515
    :cond_c
    iget-object v9, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v9

    .line 516
    :try_start_f
    iget-object v1, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lt/I;

    move-object v7, v0

    .line 517
    if-eqz v7, :cond_5b

    .line 519
    iget-object v1, v7, Lt/I;->h:Ls/e;

    if-eqz v1, :cond_7e

    .line 520
    if-eqz p2, :cond_46

    .line 521
    new-instance v8, Ls/b;

    iget-object v1, v7, Lt/I;->h:Ls/e;

    invoke-direct {v8, v1, p2}, Ls/b;-><init>(Ls/e;Ls/e;)V

    .line 527
    :goto_28
    iget-object v1, v7, Lt/I;->f:Lo/ap;

    if-eqz v1, :cond_49

    .line 529
    new-instance v1, Lt/I;

    iget-wide v2, v7, Lt/I;->b:J

    iget-object v4, v7, Lt/I;->c:Ljava/lang/String;

    iget v5, v7, Lt/I;->d:I

    invoke-static {v5, p3}, Lr/u;->a(II)I

    move-result v5

    iget-object v6, v7, Lt/I;->e:[B

    iget-object v7, v7, Lt/I;->f:Lo/ap;

    invoke-direct/range {v1 .. v8}, Lt/I;-><init>(JLjava/lang/String;I[BLo/ap;Ls/e;)V

    .line 538
    :goto_3f
    iget-object v2, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    :goto_44
    monitor-exit v9

    .line 546
    return-void

    .line 523
    :cond_46
    iget-object v8, v7, Lt/I;->h:Ls/e;

    goto :goto_28

    .line 534
    :cond_49
    new-instance v2, Lt/I;

    iget-wide v3, v7, Lt/I;->b:J

    iget-object v5, v7, Lt/I;->c:Ljava/lang/String;

    iget v1, v7, Lt/I;->d:I

    invoke-static {v1, p3}, Lr/u;->a(II)I

    move-result v6

    move-object v7, p1

    invoke-direct/range {v2 .. v8}, Lt/I;-><init>(JLjava/lang/String;ILo/aq;Ls/e;)V

    move-object v1, v2

    goto :goto_3f

    .line 540
    :cond_5b
    iget-object v1, p0, Lt/y;->i:LA/c;

    invoke-static {v1, p1}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v4

    .line 541
    new-instance v1, Lt/I;

    iget-object v2, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move v5, p3

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lt/I;-><init>(JLjava/lang/String;ILo/aq;Ls/e;)V

    .line 543
    iget-object v2, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_44

    .line 545
    :catchall_7b
    move-exception v1

    monitor-exit v9
    :try_end_7d
    .catchall {:try_start_f .. :try_end_7d} :catchall_7b

    throw v1

    :cond_7e
    move-object v8, p2

    goto :goto_28
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 763
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 764
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 766
    :cond_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->a()I

    move-result v0

    iget-object v1, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v1}, Lt/h;->c()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lt/y;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public a(I)Z
    .registers 4
    .parameter

    .prologue
    .line 718
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 719
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 722
    :cond_c
    :try_start_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0, p1}, Lt/h;->a(I)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_11} :catch_13

    .line 723
    const/4 v0, 0x1

    .line 726
    :goto_12
    return v0

    .line 724
    :catch_13
    move-exception v0

    .line 725
    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 726
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public declared-synchronized a(Ljava/io/File;)Z
    .registers 4
    .parameter

    .prologue
    .line 404
    monitor-enter p0

    :try_start_1
    const-string v0, "SDCardTileCache.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 405
    invoke-direct {p0, p1}, Lt/y;->b(Ljava/io/File;)Z

    move-result v0

    .line 406
    const-string v1, "SDCardTileCache.initialize"

    invoke-static {v1}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 407
    monitor-exit p0

    return v0

    .line 404
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Locale;)Z
    .registers 4
    .parameter

    .prologue
    .line 742
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 743
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 745
    :cond_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->a()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lt/y;->a(ILjava/util/Locale;)Z

    move-result v0

    return v0
.end method

.method public a(Lo/ap;)Z
    .registers 3
    .parameter

    .prologue
    .line 969
    sget-object v0, Lt/y;->a:Lo/ap;

    if-ne p1, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(Lo/aq;)[B
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 899
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_d

    .line 900
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 902
    :cond_d
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0x15

    if-le v0, v1, :cond_17

    move-object v0, v2

    .line 943
    :goto_16
    return-object v0

    .line 907
    :cond_17
    iget-object v3, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v3

    .line 908
    :try_start_1a
    iget-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/I;

    .line 909
    if-eqz v0, :cond_d0

    .line 910
    iget-object v1, v0, Lt/I;->e:[B

    .line 912
    :goto_26
    monitor-exit v3
    :try_end_27
    .catchall {:try_start_1a .. :try_end_27} :catchall_48

    .line 914
    if-nez v1, :cond_41

    .line 915
    iget-object v0, p0, Lt/y;->i:LA/c;

    invoke-static {v0, p1}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v1

    .line 916
    iget-object v3, p0, Lt/y;->g:Lt/h;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0}, Lt/h;->a(JLjava/lang/String;)[B

    move-result-object v1

    .line 920
    :cond_41
    if-eqz v1, :cond_46

    array-length v0, v1

    if-nez v0, :cond_4b

    :cond_46
    move-object v0, v1

    .line 921
    goto :goto_16

    .line 912
    :catchall_48
    move-exception v0

    :try_start_49
    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_49 .. :try_end_4a} :catchall_48

    throw v0

    .line 925
    :cond_4b
    :try_start_4b
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 926
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 927
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 928
    array-length v0, v1

    sub-int v4, v0, v3

    .line 929
    if-ltz v4, :cond_62

    const/16 v0, 0x18

    if-le v3, v0, :cond_93

    .line 930
    :cond_62
    const-string v0, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid tile data length["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v1, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] in "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lt/y;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 932
    goto :goto_16

    .line 937
    :cond_93
    new-array v0, v4, [B
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_4b .. :try_end_95} :catch_cc

    .line 938
    const/4 v2, 0x0

    :try_start_96
    invoke-static {v1, v3, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_99
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_99} :catch_9b

    goto/16 :goto_16

    .line 939
    :catch_9b
    move-exception v1

    .line 940
    :goto_9c
    const-string v2, "SDCardTileCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid tile data in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lt/y;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_16

    .line 939
    :catch_cc
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto :goto_9c

    :cond_d0
    move-object v1, v2

    goto/16 :goto_26
.end method

.method public a_(Lo/aq;)V
    .registers 4
    .parameter

    .prologue
    .line 686
    sget-object v0, Lt/y;->a:Lo/ap;

    sget-object v1, Lt/y;->b:[B

    invoke-virtual {p0, p1, v0, v1}, Lt/y;->a(Lo/aq;Lo/ap;[B)V

    .line 687
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 1057
    iget-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public b(Lo/aq;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 949
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_e

    .line 950
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 952
    :cond_e
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/16 v3, 0x15

    if-le v0, v3, :cond_18

    move v0, v1

    .line 961
    :goto_17
    return v0

    .line 955
    :cond_18
    iget-object v0, p0, Lt/y;->i:LA/c;

    invoke-static {v0, p1}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v3

    .line 956
    iget-object v4, p0, Lt/y;->g:Lt/h;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v0}, Lt/h;->b(JLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    move v0, v2

    .line 957
    goto :goto_17

    .line 959
    :cond_34
    iget-object v3, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v3

    .line 960
    :try_start_37
    iget-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/I;

    .line 961
    if-eqz v0, :cond_4b

    iget-object v0, v0, Lt/I;->e:[B

    if-eqz v0, :cond_4b

    move v0, v2

    :goto_46
    monitor-exit v3

    goto :goto_17

    .line 962
    :catchall_48
    move-exception v0

    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_37 .. :try_end_4a} :catchall_48

    throw v0

    :cond_4b
    move v0, v1

    .line 961
    goto :goto_46
.end method

.method public c()I
    .registers 3

    .prologue
    .line 692
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 693
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 695
    :cond_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->a()I

    move-result v0

    return v0
.end method

.method public c(Lo/aq;)Lo/ap;
    .registers 15
    .parameter

    .prologue
    const-wide/16 v9, -0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 819
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_11

    .line 820
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 822
    :cond_11
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0x15

    if-le v0, v1, :cond_1b

    move-object v0, v8

    .line 893
    :goto_1a
    return-object v0

    .line 827
    :cond_1b
    iget-object v1, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v1

    .line 828
    :try_start_1e
    iget-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/I;

    .line 829
    if-eqz v0, :cond_2f

    .line 830
    iget-object v0, v0, Lt/I;->f:Lo/ap;

    monitor-exit v1

    goto :goto_1a

    .line 832
    :catchall_2c
    move-exception v0

    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_1e .. :try_end_2e} :catchall_2c

    throw v0

    :cond_2f
    :try_start_2f
    monitor-exit v1
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_2c

    .line 834
    iget-object v0, p0, Lt/y;->i:LA/c;

    invoke-static {v0, p1}, LJ/a;->a(LA/c;Lo/aq;)Landroid/util/Pair;

    move-result-object v1

    .line 835
    iget-object v2, p0, Lt/y;->g:Lt/h;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lt/h;->a(JLjava/lang/String;)[B

    move-result-object v2

    .line 836
    if-nez v2, :cond_4c

    move-object v0, v8

    .line 837
    goto :goto_1a

    .line 840
    :cond_4c
    :try_start_4c
    array-length v0, v2

    if-nez v0, :cond_52

    .line 841
    sget-object v0, Lt/y;->a:Lo/ap;

    goto :goto_1a

    .line 848
    :cond_52
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 849
    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 850
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 852
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    .line 853
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v4

    .line 854
    cmp-long v0, v4, v9

    if-eqz v0, :cond_7e

    .line 858
    iget-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v11

    sub-long/2addr v4, v11

    iget-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v11

    add-long/2addr v4, v11

    .line 859
    cmp-long v0, v4, v6

    if-gez v0, :cond_7e

    move-wide v4, v6

    .line 864
    :cond_7e
    const/16 v0, 0x10

    if-le v3, v0, :cond_d9

    .line 865
    invoke-virtual {v1}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v0

    .line 866
    cmp-long v9, v0, v9

    if-eqz v9, :cond_d7

    .line 870
    iget-object v9, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v9}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v9

    sub-long/2addr v0, v9

    iget-object v9, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v9}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v9

    add-long/2addr v0, v9

    .line 872
    cmp-long v9, v0, v6

    if-gez v9, :cond_d7

    .line 889
    :goto_9c
    iget-object v0, p0, Lt/y;->e:Lt/J;

    move-object v1, p1

    invoke-interface/range {v0 .. v7}, Lt/J;->a(Lo/aq;[BIJJ)Lo/ap;
    :try_end_a2
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_a2} :catch_a5

    move-result-object v0

    goto/16 :goto_1a

    .line 891
    :catch_a5
    move-exception v0

    .line 892
    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not unpack tile in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lt/y;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v8

    .line 893
    goto/16 :goto_1a

    :cond_d7
    move-wide v6, v0

    goto :goto_9c

    :cond_d9
    move-wide v6, v9

    goto :goto_9c
.end method

.method public d()J
    .registers 3

    .prologue
    .line 701
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 702
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 704
    :cond_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()J
    .registers 3

    .prologue
    .line 709
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 710
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 712
    :cond_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Ljava/util/Locale;
    .registers 3

    .prologue
    .line 733
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_c

    .line 734
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 736
    :cond_c
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->c()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public f_()V
    .registers 16

    .prologue
    .line 975
    iget-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v4

    .line 976
    const-wide/16 v0, 0x0

    .line 978
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 979
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 980
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 982
    iget-object v9, p0, Lt/y;->f:Ljava/util/HashMap;

    monitor-enter v9

    .line 983
    :try_start_17
    iget-object v2, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v2, v0

    :goto_22
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/I;

    .line 984
    iget-object v1, v0, Lt/I;->e:[B

    if-nez v1, :cond_3d

    const/4 v1, 0x0

    :goto_33
    int-to-long v11, v1

    add-long v1, v2, v11

    .line 985
    iget v3, v0, Lt/I;->a:I

    packed-switch v3, :pswitch_data_10c

    :cond_3b
    :goto_3b
    move-wide v2, v1

    .line 999
    goto :goto_22

    .line 984
    :cond_3d
    iget-object v1, v0, Lt/I;->e:[B

    array-length v1, v1

    goto :goto_33

    .line 987
    :pswitch_41
    iget-wide v11, v0, Lt/I;->b:J

    iget-object v3, v0, Lt/I;->c:Ljava/lang/String;

    iget v13, v0, Lt/I;->d:I

    iget-object v14, v0, Lt/I;->e:[B

    invoke-static {v11, v12, v3, v13, v14}, Lt/h;->a(JLjava/lang/String;I[B)Lt/l;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 989
    iget-object v3, p0, Lt/y;->l:Lt/A;

    if-eqz v3, :cond_5b

    .line 990
    iget-object v3, p0, Lt/y;->l:Lt/A;

    iget-object v11, v0, Lt/I;->f:Lo/ap;

    invoke-virtual {v3, v11}, Lt/A;->a(Lo/ap;)V

    .line 992
    :cond_5b
    iget-object v3, v0, Lt/I;->h:Ls/e;

    if-eqz v3, :cond_3b

    .line 993
    iget-object v3, v0, Lt/I;->h:Ls/e;

    iget-object v0, v0, Lt/I;->f:Lo/ap;

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3b

    .line 1004
    :catchall_6b
    move-exception v0

    monitor-exit v9
    :try_end_6d
    .catchall {:try_start_17 .. :try_end_6d} :catchall_6b

    throw v0

    .line 998
    :pswitch_6e
    :try_start_6e
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3b

    .line 1002
    :cond_72
    const/4 v0, 0x0

    iput v0, p0, Lt/y;->j:I

    .line 1003
    iget-object v0, p0, Lt/y;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1004
    monitor-exit v9
    :try_end_7b
    .catchall {:try_start_6e .. :try_end_7b} :catchall_6b

    .line 1005
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_ba

    .line 1006
    const/4 v0, 0x0

    .line 1010
    :try_start_82
    iget-object v1, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v1, v6}, Lt/h;->a(Ljava/util/Collection;)I
    :try_end_87
    .catch Ljava/io/IOException; {:try_start_82 .. :try_end_87} :catch_b1

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_8c

    .line 1011
    const/4 v0, 0x1

    :cond_8c
    move v3, v0

    .line 1017
    :goto_8d
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_91
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ba

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1020
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ls/e;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lo/ap;

    invoke-interface {v2}, Lo/ap;->d()Lo/aq;

    move-result-object v2

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/ap;

    invoke-interface {v1, v2, v3, v0}, Ls/e;->a(Lo/aq;ILo/ap;)V

    goto :goto_91

    .line 1013
    :catch_b1
    move-exception v0

    .line 1014
    const-string v1, "SDCardTileCache"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1015
    const/4 v0, 0x1

    move v3, v0

    goto :goto_8d

    .line 1023
    :cond_ba
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_be
    :goto_be
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ef

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/I;

    .line 1024
    const/4 v1, 0x0

    .line 1025
    iget v3, v0, Lt/I;->d:I

    if-lez v3, :cond_df

    .line 1029
    :try_start_cf
    iget-object v3, p0, Lt/y;->g:Lt/h;

    iget-wide v6, v0, Lt/I;->b:J

    iget-object v8, v0, Lt/I;->c:Ljava/lang/String;

    iget v9, v0, Lt/I;->d:I

    invoke-virtual {v3, v6, v7, v8, v9}, Lt/h;->a(JLjava/lang/String;I)I
    :try_end_da
    .catch Ljava/io/IOException; {:try_start_cf .. :try_end_da} :catch_ec

    move-result v3

    const/4 v6, -0x1

    if-ne v3, v6, :cond_df

    .line 1031
    const/4 v1, 0x2

    .line 1037
    :cond_df
    :goto_df
    iget-object v3, v0, Lt/I;->h:Ls/e;

    if-eqz v3, :cond_be

    .line 1038
    iget-object v3, v0, Lt/I;->h:Ls/e;

    iget-object v0, v0, Lt/I;->g:Lo/aq;

    const/4 v6, 0x0

    invoke-interface {v3, v0, v1, v6}, Ls/e;->a(Lo/aq;ILo/ap;)V

    goto :goto_be

    .line 1033
    :catch_ec
    move-exception v1

    .line 1034
    const/4 v1, 0x1

    goto :goto_df

    .line 1042
    :cond_ef
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_fc

    .line 1043
    iget-object v0, p0, Lt/y;->k:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->d()J

    move-result-wide v0

    sub-long/2addr v0, v4

    .line 1049
    :cond_fc
    iget-object v0, p0, Lt/y;->l:Lt/A;

    if-eqz v0, :cond_10a

    .line 1050
    iget-object v0, p0, Lt/y;->l:Lt/A;

    invoke-virtual {v0}, Lt/A;->c()V

    .line 1051
    iget-object v0, p0, Lt/y;->l:Lt/A;

    invoke-virtual {v0}, Lt/A;->b()V

    .line 1053
    :cond_10a
    return-void

    .line 985
    nop

    :pswitch_data_10c
    .packed-switch 0x0
        :pswitch_41
        :pswitch_6e
    .end packed-switch
.end method

.method public declared-synchronized g()V
    .registers 5

    .prologue
    .line 500
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lt/y;->g:Lt/h;

    if-nez v0, :cond_10

    .line 501
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Uninitialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    .line 500
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 504
    :cond_10
    :try_start_10
    iget-object v0, p0, Lt/y;->g:Lt/h;

    invoke-virtual {v0}, Lt/h;->g()V
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_d
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_15} :catch_17

    .line 508
    :goto_15
    monitor-exit p0

    return-void

    .line 505
    :catch_17
    move-exception v0

    .line 506
    :try_start_18
    const-string v1, "SDCardTileCache"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shutDown(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_30
    .catchall {:try_start_18 .. :try_end_30} :catchall_d

    goto :goto_15
.end method

.method i()J
    .registers 6

    .prologue
    const-wide/16 v0, 0x0

    .line 800
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disk_creation_time_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lt/y;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v2

    .line 802
    if-nez v2, :cond_26

    .line 812
    :goto_25
    return-wide v0

    .line 806
    :cond_26
    new-instance v3, Laq/a;

    invoke-direct {v3, v2}, Laq/a;-><init>([B)V

    .line 808
    :try_start_2b
    invoke-virtual {v3}, Laq/a;->readLong()J
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2e} :catch_30

    move-result-wide v0

    goto :goto_25

    .line 809
    :catch_30
    move-exception v2

    .line 810
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disk_creation_time_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lt/y;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_25
.end method
