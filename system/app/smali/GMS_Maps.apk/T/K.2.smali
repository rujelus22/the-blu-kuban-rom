.class public Lt/K;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt/M;


# static fields
.field protected static final b:Lo/ap;


# instance fields
.field protected final a:LR/h;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 53
    new-instance v0, Lo/N;

    invoke-direct {v0}, Lo/N;-><init>()V

    sput-object v0, Lt/K;->b:Lo/ap;

    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, LR/h;

    invoke-direct {v0, p1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lt/K;->a:LR/h;

    .line 57
    return-void
.end method


# virtual methods
.method public a(Lo/aq;Lo/ap;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 61
    iget-object v1, p0, Lt/K;->a:LR/h;

    monitor-enter v1

    .line 62
    :try_start_3
    iget-object v0, p0, Lt/K;->a:LR/h;

    new-instance v2, Lt/L;

    invoke-direct {v2, p2}, Lt/L;-><init>(Lo/ap;)V

    invoke-virtual {v0, p1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    monitor-exit v1

    .line 65
    return-void

    .line 64
    :catchall_f
    move-exception v0

    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 95
    iget-object v1, p0, Lt/K;->a:LR/h;

    monitor-enter v1

    .line 96
    :try_start_3
    iget-object v0, p0, Lt/K;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 97
    monitor-exit v1

    .line 98
    const/4 v0, 0x1

    return v0

    .line 97
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public a(Lo/ap;)Z
    .registers 3
    .parameter

    .prologue
    .line 103
    sget-object v0, Lt/K;->b:Lo/ap;

    if-ne p1, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a_(Lo/aq;)V
    .registers 3
    .parameter

    .prologue
    .line 69
    sget-object v0, Lt/K;->b:Lo/ap;

    invoke-virtual {p0, p1, v0}, Lt/K;->a(Lo/aq;Lo/ap;)V

    .line 70
    return-void
.end method

.method public b(Lo/aq;)Z
    .registers 3
    .parameter

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lt/K;->c(Lo/aq;)Lo/ap;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public c(Lo/aq;)Lo/ap;
    .registers 5
    .parameter

    .prologue
    .line 79
    iget-object v1, p0, Lt/K;->a:LR/h;

    monitor-enter v1

    .line 80
    :try_start_3
    iget-object v0, p0, Lt/K;->a:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt/L;

    .line 81
    if-eqz v0, :cond_1a

    .line 82
    invoke-virtual {v0}, Lt/L;->a()Lo/ap;

    move-result-object v0

    .line 83
    if-nez v0, :cond_18

    .line 85
    iget-object v2, p0, Lt/K;->a:LR/h;

    invoke-virtual {v2, p1}, LR/h;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_18
    monitor-exit v1

    .line 89
    :goto_19
    return-object v0

    :cond_1a
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_19

    .line 90
    :catchall_1d
    move-exception v0

    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    throw v0
.end method
