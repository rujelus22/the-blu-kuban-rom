.class public Lt/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LR/h;

.field private final b:LR/h;

.field private c:Lt/a;

.field private final d:Lcom/google/googlenav/common/a;

.field private e:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Lcom/google/googlenav/common/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    new-instance v0, LR/h;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lt/u;->a:LR/h;

    .line 82
    new-instance v0, LR/h;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lt/u;->b:LR/h;

    .line 83
    iput-object p2, p0, Lt/u;->d:Lcom/google/googlenav/common/a;

    .line 84
    iput-object p1, p0, Lt/u;->e:Ljava/util/Locale;

    .line 85
    return-void
.end method

.method private a(Lo/r;Z)Lo/y;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 130
    iget-object v2, p0, Lt/u;->b:LR/h;

    monitor-enter v2

    .line 131
    :try_start_5
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 132
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/r;

    move-object p1, v0

    .line 134
    :cond_16
    monitor-exit v2
    :try_end_17
    .catchall {:try_start_5 .. :try_end_17} :catchall_26

    .line 136
    iget-object v2, p0, Lt/u;->a:LR/h;

    monitor-enter v2

    .line 137
    :try_start_1a
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/y;

    .line 138
    if-eqz v0, :cond_29

    .line 139
    monitor-exit v2
    :try_end_25
    .catchall {:try_start_1a .. :try_end_25} :catchall_32

    .line 185
    :cond_25
    :goto_25
    return-object v0

    .line 134
    :catchall_26
    move-exception v0

    :try_start_27
    monitor-exit v2
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    throw v0

    .line 141
    :cond_29
    :try_start_29
    monitor-exit v2
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_32

    .line 143
    if-eqz p2, :cond_30

    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-nez v0, :cond_35

    :cond_30
    move-object v0, v1

    .line 144
    goto :goto_25

    .line 141
    :catchall_32
    move-exception v0

    :try_start_33
    monitor-exit v2
    :try_end_34
    .catchall {:try_start_33 .. :try_end_34} :catchall_32

    throw v0

    .line 147
    :cond_35
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {p1}, Lo/r;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lt/a;->a(Ljava/lang/String;)Lt/d;

    move-result-object v0

    .line 148
    if-nez v0, :cond_43

    move-object v0, v1

    .line 149
    goto :goto_25

    .line 154
    :cond_43
    iget-object v2, v0, Lt/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v2

    .line 156
    if-nez v2, :cond_51

    move-object v0, v1

    .line 159
    goto :goto_25

    .line 161
    :cond_51
    invoke-virtual {p1, v2}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a5

    .line 162
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v2}, Lo/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lt/a;->a(Ljava/lang/String;)Lt/d;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_b7

    .line 164
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_9c

    .line 165
    iget-object v1, v0, Lt/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    .line 167
    invoke-virtual {v2, v1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9c

    .line 168
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected ID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_9c
    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    .line 173
    :try_start_9f
    iget-object v3, p0, Lt/u;->b:LR/h;

    invoke-virtual {v3, p1, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 174
    monitor-exit v1
    :try_end_a5
    .catchall {:try_start_9f .. :try_end_a5} :catchall_b4

    .line 180
    :cond_a5
    iget-object v1, v0, Lt/d;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-wide v2, v0, Lt/d;->b:J

    invoke-static {v1, v2, v3}, Lo/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lo/y;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_25

    .line 182
    invoke-direct {p0, v0}, Lt/u;->b(Lo/y;)V

    goto/16 :goto_25

    .line 174
    :catchall_b4
    move-exception v0

    :try_start_b5
    monitor-exit v1
    :try_end_b6
    .catchall {:try_start_b5 .. :try_end_b6} :catchall_b4

    throw v0

    :cond_b7
    move-object v0, v1

    .line 176
    goto/16 :goto_25
.end method

.method private b(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 196
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {p1}, Lo/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lt/a;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 198
    invoke-virtual {p2, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 199
    if-nez v1, :cond_12

    .line 219
    :cond_11
    return-void

    .line 208
    :cond_12
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbJ/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 209
    invoke-virtual {p1}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 211
    const/4 v0, 0x0

    :goto_21
    if-ge v0, v1, :cond_11

    .line 212
    invoke-virtual {p2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 213
    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v3

    .line 215
    if-eqz v3, :cond_3a

    .line 216
    iget-object v4, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v3}, Lo/r;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3, v2}, Lt/a;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 211
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    goto :goto_21
.end method

.method private b(Lo/y;)V
    .registers 7
    .parameter

    .prologue
    .line 259
    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v1

    .line 260
    iget-object v2, p0, Lt/u;->a:LR/h;

    monitor-enter v2

    .line 261
    :try_start_7
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0, v1, p1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 262
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_31

    .line 263
    iget-object v2, p0, Lt/u;->b:LR/h;

    monitor-enter v2

    .line 264
    :try_start_10
    invoke-virtual {p1}, Lo/y;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    .line 265
    iget-object v4, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {v4, v0, v1}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_18

    .line 267
    :catchall_2e
    move-exception v0

    monitor-exit v2
    :try_end_30
    .catchall {:try_start_10 .. :try_end_30} :catchall_2e

    throw v0

    .line 262
    :catchall_31
    move-exception v0

    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_31

    throw v0

    .line 267
    :cond_34
    :try_start_34
    monitor-exit v2
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_2e

    .line 268
    return-void
.end method


# virtual methods
.method public a(Lo/r;)Lo/y;
    .registers 3
    .parameter

    .prologue
    .line 115
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lt/u;->a(Lo/r;Z)Lo/y;

    move-result-object v0

    return-object v0
.end method

.method public a(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/y;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 230
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v1

    .line 232
    if-eqz p1, :cond_e

    if-nez v1, :cond_f

    .line 248
    :cond_e
    :goto_e
    return-object v0

    .line 236
    :cond_f
    iget-object v2, p0, Lt/u;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    .line 237
    iget-object v4, p0, Lt/u;->c:Lt/a;

    if-eqz v4, :cond_20

    .line 238
    invoke-direct {p0, v1, p2}, Lt/u;->b(Lo/r;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 241
    :cond_20
    invoke-static {p2, v2, v3}, Lo/y;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)Lo/y;

    move-result-object v1

    .line 242
    if-eqz v1, :cond_e

    .line 246
    invoke-direct {p0, v1}, Lt/u;->b(Lo/y;)V

    move-object v0, v1

    .line 248
    goto :goto_e
.end method

.method public a()V
    .registers 3

    .prologue
    .line 328
    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    .line 329
    :try_start_3
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 330
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_13

    .line 332
    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    .line 333
    :try_start_c
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 334
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_16

    .line 335
    return-void

    .line 330
    :catchall_13
    move-exception v0

    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    throw v0

    .line 334
    :catchall_16
    move-exception v0

    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public a(Ljava/io/File;)V
    .registers 9
    .parameter

    .prologue
    .line 94
    new-instance v0, Lt/a;

    iget-object v1, p0, Lt/u;->d:Lcom/google/googlenav/common/a;

    const-string v2, "bd"

    sget-object v3, LbJ/a;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    const/16 v4, 0xbb8

    const-wide/32 v5, 0x5265c00

    invoke-direct/range {v0 .. v6}, Lt/a;-><init>(Lcom/google/googlenav/common/a;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;IJ)V

    .line 98
    invoke-virtual {v0, p1}, Lt/a;->a(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 99
    iget-object v1, p0, Lt/u;->e:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lt/a;->a(Ljava/util/Locale;)Z

    .line 100
    iput-object v0, p0, Lt/u;->c:Lt/a;

    .line 102
    :cond_1d
    return-void
.end method

.method public a(Lo/y;)Z
    .registers 3
    .parameter

    .prologue
    .line 290
    instance-of v0, p1, Lt/w;

    return v0
.end method

.method public b(Lo/r;)Lo/y;
    .registers 3
    .parameter

    .prologue
    .line 125
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lt/u;->a(Lo/r;Z)Lo/y;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .registers 3

    .prologue
    .line 341
    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    .line 342
    :try_start_3
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 343
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_1c

    .line 345
    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    .line 346
    :try_start_c
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 347
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_1f

    .line 349
    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-eqz v0, :cond_1b

    .line 350
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v0}, Lt/a;->b()Z

    .line 352
    :cond_1b
    return-void

    .line 343
    :catchall_1c
    move-exception v0

    :try_start_1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_1c

    throw v0

    .line 347
    :catchall_1f
    move-exception v0

    :try_start_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public c()V
    .registers 3

    .prologue
    .line 358
    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    .line 359
    :try_start_3
    iget-object v0, p0, Lt/u;->a:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 360
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_1c

    .line 362
    iget-object v1, p0, Lt/u;->b:LR/h;

    monitor-enter v1

    .line 363
    :try_start_c
    iget-object v0, p0, Lt/u;->b:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 364
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_1f

    .line 366
    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-eqz v0, :cond_1b

    .line 367
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v0}, Lt/a;->a()V

    .line 369
    :cond_1b
    return-void

    .line 360
    :catchall_1c
    move-exception v0

    :try_start_1d
    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_1d .. :try_end_1e} :catchall_1c

    throw v0

    .line 364
    :catchall_1f
    move-exception v0

    :try_start_20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_20 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public c(Lo/r;)V
    .registers 5
    .parameter

    .prologue
    .line 276
    new-instance v0, Lt/w;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lt/w;-><init>(Lo/r;Lt/v;)V

    .line 279
    iget-object v1, p0, Lt/u;->a:LR/h;

    monitor-enter v1

    .line 280
    :try_start_9
    iget-object v2, p0, Lt/u;->a:LR/h;

    invoke-virtual {v2, p1, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 281
    monitor-exit v1

    .line 282
    return-void

    .line 281
    :catchall_10
    move-exception v0

    monitor-exit v1
    :try_end_12
    .catchall {:try_start_9 .. :try_end_12} :catchall_10

    throw v0
.end method

.method public d()J
    .registers 3

    .prologue
    .line 375
    iget-object v0, p0, Lt/u;->c:Lt/a;

    if-eqz v0, :cond_b

    .line 376
    iget-object v0, p0, Lt/u;->c:Lt/a;

    invoke-virtual {v0}, Lt/a;->c()J

    move-result-wide v0

    .line 379
    :goto_a
    return-wide v0

    :cond_b
    const-wide/16 v0, 0x0

    goto :goto_a
.end method
