.class Lt/b;
.super LR/c;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Lt/a;


# direct methods
.method constructor <init>(Ljava/lang/String;ILt/a;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 527
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheCommitter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 528
    iput p2, p0, Lt/b;->a:I

    .line 529
    iput-object p3, p0, Lt/b;->b:Lt/a;

    .line 530
    invoke-virtual {p0}, Lt/b;->start()V

    .line 531
    return-void
.end method


# virtual methods
.method public l()V
    .registers 5

    .prologue
    .line 538
    :try_start_0
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_9
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_9} :catch_18

    .line 545
    :cond_9
    :goto_9
    :try_start_9
    iget v0, p0, Lt/b;->a:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lt/b;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_f} :catch_34

    .line 550
    iget-object v0, p0, Lt/b;->b:Lt/a;

    invoke-static {v0}, Lt/a;->a(Lt/a;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 554
    :goto_17
    return-void

    .line 539
    :catch_18
    move-exception v0

    .line 540
    invoke-virtual {p0}, Lt/b;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 546
    :catch_34
    move-exception v0

    goto :goto_17
.end method
