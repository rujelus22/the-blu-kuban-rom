.class public Li/b;
.super Li/a;
.source "SourceFile"


# static fields
.field private static final e:Li/e;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 25
    new-instance v0, Li/c;

    const/16 v1, 0x64

    const-string v2, "ByteChunkArrayManager"

    invoke-direct {v0, v1, v2}, Li/c;-><init>(ILjava/lang/String;)V

    sput-object v0, Li/b;->e:Li/e;

    return-void
.end method

.method public constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 36
    const/16 v0, 0xc

    sget-object v1, Li/b;->e:Li/e;

    invoke-direct {p0, p1, v0, v1}, Li/a;-><init>(IILi/e;)V

    .line 37
    return-void
.end method


# virtual methods
.method public a(Li/d;)V
    .registers 5
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget v0, p0, Li/b;->b:I

    if-ge v1, v0, :cond_17

    .line 62
    iget-object v0, p0, Li/b;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    const/16 v2, 0x1000

    invoke-interface {p1, v0, v2}, Li/d;->a([BI)V

    .line 61
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 64
    :cond_17
    iget v0, p0, Li/b;->b:I

    iget-object v1, p0, Li/b;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-eq v0, v1, :cond_2a

    .line 65
    iget-object v0, p0, Li/b;->c:Ljava/lang/Object;

    check-cast v0, [B

    iget v1, p0, Li/b;->d:I

    invoke-interface {p1, v0, v1}, Li/d;->a([BI)V

    .line 67
    :cond_2a
    return-void
.end method
