.class public abstract LaM/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/H;


# instance fields
.field protected final a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LaM/v;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 35
    iput-boolean p2, p0, LaM/v;->b:Z

    .line 36
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 69
    const/4 v0, 0x3

    return v0
.end method

.method public a(Landroid/view/View;)LaR/bE;
    .registers 4
    .parameter

    .prologue
    .line 59
    new-instance v1, LaM/w;

    invoke-direct {v1}, LaM/w;-><init>()V

    .line 60
    const v0, 0x7f100270

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LaM/w;->b:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f100271

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, LaM/w;->c:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f10025a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, LaM/w;->a:Landroid/widget/ImageView;

    .line 63
    const v0, 0x7f100302

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, LaM/w;->d:Landroid/view/View;

    .line 64
    return-object v1
.end method

.method protected abstract a(LaM/w;)V
.end method

.method public a(Lcom/google/googlenav/ui/g;LaR/bE;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 45
    check-cast p2, LaM/w;

    .line 46
    invoke-virtual {p0, p2}, LaM/v;->a(LaM/w;)V

    .line 47
    iget-boolean v0, p0, LaM/v;->b:Z

    if-eqz v0, :cond_10

    .line 48
    iget-object v0, p2, LaM/w;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    :goto_f
    return-void

    .line 50
    :cond_10
    iget-object v0, p2, LaM/w;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_f
.end method

.method public b()I
    .registers 2

    .prologue
    .line 74
    const v0, 0x7f040125

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 79
    const/4 v0, 0x1

    return v0
.end method
