.class public LaM/a;
.super LaM/f;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/googlenav/android/aa;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Z

.field private final h:Lbm/c;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/googlenav/android/aa;Lbm/c;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0}, LaM/f;-><init>()V

    .line 82
    iput-object v0, p0, LaM/a;->c:Ljava/lang/String;

    .line 85
    iput-object v0, p0, LaM/a;->d:Ljava/lang/String;

    .line 86
    iput-object v0, p0, LaM/a;->e:Ljava/lang/String;

    .line 89
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 103
    iput-object p1, p0, LaM/a;->a:Landroid/content/Context;

    .line 104
    iput-object p2, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    .line 105
    iput-object p3, p0, LaM/a;->h:Lbm/c;

    .line 108
    invoke-direct {p0}, LaM/a;->y()V

    .line 111
    invoke-direct {p0}, LaM/a;->x()V

    .line 113
    invoke-super {p0}, LaM/f;->i()V

    .line 114
    return-void
.end method

.method private A()Ljava/lang/String;
    .registers 3

    .prologue
    .line 218
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "sid_token_encrypted"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private B()Ljava/lang/String;
    .registers 3

    .prologue
    .line 222
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "lsid_token_encrypted"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private C()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 273
    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 276
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.google"

    iget-object v2, p0, LaM/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-direct {p0, v3, v3, v3}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_15
    return-void
.end method

.method private D()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 483
    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 488
    iget-object v0, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_18

    .line 489
    iget-object v0, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    new-instance v1, LaM/b;

    invoke-direct {v1, p0}, LaM/b;-><init>(LaM/a;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 499
    :goto_14
    invoke-virtual {p0}, LaM/a;->u()V

    .line 500
    return-void

    .line 495
    :cond_18
    invoke-virtual {p0}, LaM/a;->p()V

    goto :goto_14
.end method

.method private E()V
    .registers 1

    .prologue
    .line 507
    invoke-virtual {p0}, LaM/a;->m()V

    .line 508
    invoke-virtual {p0}, LaM/a;->s()V

    .line 509
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 122
    const-string v0, "AndroidLoginHelper.load"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 127
    invoke-static {p0}, Lbm/c;->a(Landroid/content/Context;)Lbm/c;

    move-result-object v0

    .line 128
    new-instance v1, LaM/a;

    invoke-direct {v1, p0, p1, v0}, LaM/a;-><init>(Landroid/content/Context;Lcom/google/googlenav/android/aa;Lbm/c;)V

    invoke-static {v1}, LaM/f;->a(LaM/f;)V

    .line 133
    :goto_1b
    const-string v0, "AndroidLoginHelper.load"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 134
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    return-object v0

    .line 130
    :cond_25
    new-instance v0, LaM/d;

    invoke-direct {v0}, LaM/d;-><init>()V

    invoke-static {v0}, LaM/f;->a(LaM/f;)V

    goto :goto_1b
.end method

.method private a(Landroid/content/SharedPreferences;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 175
    if-eqz p1, :cond_39

    const-string v0, "auth_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_39

    .line 177
    const-string v0, "auth_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 178
    const-string v0, "sid_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 179
    const-string v0, "lsid_token"

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    if-eqz p2, :cond_2b

    .line 183
    invoke-direct {p0, v2}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 184
    invoke-direct {p0, v1}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 189
    :cond_2b
    invoke-direct {p0, v2, v1, v0}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 195
    :cond_39
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 444
    if-nez p2, :cond_7

    .line 445
    const/4 v0, 0x0

    invoke-interface {p3, p1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 461
    :goto_6
    return-void

    .line 452
    :cond_7
    :try_start_7
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 453
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 454
    invoke-virtual {v1, p2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 455
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p3, p1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_1b} :catch_1c

    goto :goto_6

    .line 457
    :catch_1c
    move-exception v0

    .line 458
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LoginHelper - setting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6
.end method

.method private a(ZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 360
    if-eqz p1, :cond_e

    if-eqz p2, :cond_e

    .line 362
    :try_start_4
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_7} :catch_f

    .line 372
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 374
    :cond_e
    :goto_e
    return-void

    .line 363
    :catch_f
    move-exception v0

    goto :goto_e
.end method

.method private a(ZZ)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 314
    if-eqz p1, :cond_3

    .line 318
    :cond_3
    if-eqz p2, :cond_48

    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_48

    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    move-object v1, v0

    .line 322
    :goto_c
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 325
    if-eqz p2, :cond_1b

    if-eqz v1, :cond_1b

    .line 326
    const-string v0, "com.google"

    invoke-virtual {v3, v0, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_1b
    const-string v0, "com.google"

    invoke-virtual {v3, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    invoke-static {v0}, LaM/j;->a([Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    .line 334
    if-nez v4, :cond_2c

    if-eqz p1, :cond_2c

    .line 338
    invoke-static {p2}, LaM/j;->a(Z)V

    .line 341
    :cond_2c
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    .line 342
    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v5

    if-eqz v5, :cond_44

    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/android/b;->i()Landroid/app/Activity;

    move-result-object v5

    if-nez v5, :cond_4a

    .line 345
    :cond_44
    invoke-static {v3, v4, p1, p2, v2}, LaM/j;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V

    .line 352
    :goto_47
    return-void

    :cond_48
    move-object v1, v2

    .line 318
    goto :goto_c

    .line 348
    :cond_4a
    invoke-direct {p0, p2, v1}, LaM/a;->a(ZLjava/lang/String;)V

    .line 349
    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->a()Lcom/google/googlenav/android/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/android/b;->i()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v3, v4, p1, p2, v0}, LaM/j;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;ZZLandroid/app/Activity;)V

    goto :goto_47
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 595
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    iget-object v0, p0, LaM/a;->h:Lbm/c;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lbm/c;->a([B)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->a([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 404
    invoke-direct {p0, p1, p2, p3}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    invoke-virtual {p0}, LaM/a;->n()V

    .line 410
    invoke-virtual {p0}, LaM/a;->t()V

    .line 411
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 390
    if-eqz p4, :cond_6

    .line 391
    invoke-direct {p0, p1, p2, p3}, LaM/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :goto_5
    return-void

    .line 393
    :cond_6
    invoke-direct {p0, p1, p2, p3}, LaM/a;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 601
    :try_start_1
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2e

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 602
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->b([B)[B

    move-result-object v0

    .line 603
    if-eqz v0, :cond_2e

    array-length v2, v0

    if-lez v2, :cond_2e

    .line 604
    iget-object v2, p0, LaM/a;->h:Lbm/c;

    invoke-virtual {v2, v0}, Lbm/c;->b([B)[B

    move-result-object v2

    .line 605
    if-eqz v2, :cond_2e

    array-length v0, v2

    if-lez v0, :cond_2e

    .line 606
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_2a
    .catch Lcom/google/googlenav/common/util/d; {:try_start_1 .. :try_end_2a} :catch_2b

    .line 612
    :goto_2a
    return-object v0

    .line 609
    :catch_2b
    move-exception v0

    move-object v0, v1

    .line 610
    goto :goto_2a

    :cond_2e
    move-object v0, v1

    .line 612
    goto :goto_2a
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 415
    invoke-direct {p0, p1, p2, p3}, LaM/a;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    invoke-virtual {p0}, LaM/a;->s()V

    .line 419
    return-void
.end method

.method private c(Z)V
    .registers 2
    .parameter

    .prologue
    .line 471
    if-eqz p1, :cond_6

    .line 472
    invoke-direct {p0}, LaM/a;->D()V

    .line 476
    :goto_5
    return-void

    .line 474
    :cond_6
    invoke-direct {p0}, LaM/a;->E()V

    goto :goto_5
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 429
    iput-object p1, p0, LaM/a;->c:Ljava/lang/String;

    .line 430
    iput-object p2, p0, LaM/a;->d:Ljava/lang/String;

    .line 431
    iput-object p3, p0, LaM/a;->e:Ljava/lang/String;

    .line 433
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 435
    const-string v1, "auth_token_encrypted"

    iget-object v2, p0, LaM/a;->c:Ljava/lang/String;

    invoke-direct {p0, v2}, LaM/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    .line 436
    const-string v1, "sid_token_encrypted"

    iget-object v2, p0, LaM/a;->d:Ljava/lang/String;

    invoke-direct {p0, v2}, LaM/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    .line 437
    const-string v1, "lsid_token_encrypted"

    iget-object v2, p0, LaM/a;->e:Ljava/lang/String;

    invoke-direct {p0, v2}, LaM/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, LaM/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/j;)V

    .line 439
    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 440
    return-void
.end method

.method private x()V
    .registers 3

    .prologue
    .line 144
    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 147
    invoke-direct {p0}, LaM/a;->z()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/a;->c:Ljava/lang/String;

    .line 148
    invoke-direct {p0}, LaM/a;->A()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/a;->d:Ljava/lang/String;

    .line 149
    invoke-direct {p0}, LaM/a;->B()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LaM/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaM/a;->e:Ljava/lang/String;

    .line 150
    return-void
.end method

.method private y()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 155
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    const-string v1, "login_helper"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 157
    invoke-direct {p0, v0, v2}, LaM/a;->a(Landroid/content/SharedPreferences;Z)V

    .line 160
    iget-object v0, p0, LaM/a;->a:Landroid/content/Context;

    const-string v1, "ids"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 162
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LaM/a;->a(Landroid/content/SharedPreferences;Z)V

    .line 163
    return-void
.end method

.method private z()Ljava/lang/String;
    .registers 3

    .prologue
    .line 214
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "auth_token_encrypted"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 204
    invoke-direct {p0}, LaM/a;->x()V

    .line 205
    invoke-super {p0}, LaM/f;->a()V

    .line 206
    return-void
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    .line 522
    if-gez p1, :cond_3

    .line 570
    :goto_2
    :pswitch_2
    return-void

    .line 527
    :cond_3
    packed-switch p1, :pswitch_data_1e

    .line 554
    :pswitch_6
    iget-object v0, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_1a

    .line 558
    const/4 v0, 0x0

    .line 559
    iget-object v1, p0, LaM/a;->b:Lcom/google/googlenav/android/aa;

    new-instance v2, LaM/c;

    invoke-direct {v2, p0}, LaM/c;-><init>(LaM/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    goto :goto_2

    .line 542
    :pswitch_16
    invoke-virtual {p0}, LaM/a;->h()V

    goto :goto_2

    .line 566
    :cond_1a
    invoke-virtual {p0}, LaM/a;->p()V

    goto :goto_2

    .line 527
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_2
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_16
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected a(LaM/g;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 300
    invoke-virtual {p0, v0}, LaM/a;->b(Z)V

    .line 301
    invoke-virtual {p0, p1}, LaM/a;->d(LaM/g;)V

    .line 304
    if-nez p2, :cond_e

    :goto_a
    invoke-direct {p0, v0, v1}, LaM/a;->a(ZZ)V

    .line 305
    return-void

    :cond_e
    move v0, v1

    .line 304
    goto :goto_a
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 384
    invoke-direct {p0, p1, p2, p3, p4}, LaM/a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 385
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 466
    invoke-direct {p0, p1}, LaM/a;->c(Z)V

    .line 467
    return-void
.end method

.method protected b()Z
    .registers 2

    .prologue
    .line 230
    invoke-virtual {p0}, LaM/a;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 238
    iget-object v0, p0, LaM/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 246
    iget-object v0, p0, LaM/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 254
    iget-object v0, p0, LaM/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected f()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 263
    invoke-direct {p0}, LaM/a;->C()V

    .line 266
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->b(Ljava/lang/String;)V

    .line 269
    invoke-virtual {p0, v1}, LaM/a;->a(Ljava/lang/String;)V

    .line 270
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 287
    invoke-direct {p0, v0, v0}, LaM/a;->a(ZZ)V

    .line 288
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 576
    iget-object v0, p0, LaM/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_13

    .line 582
    iget-boolean v0, p0, LaM/a;->g:Z

    if-nez v0, :cond_14

    .line 585
    iput-boolean v1, p0, LaM/a;->g:Z

    .line 586
    invoke-direct {p0, v2, v1}, LaM/a;->a(ZZ)V

    .line 592
    :cond_13
    :goto_13
    return-void

    .line 589
    :cond_14
    iput-boolean v2, p0, LaM/a;->g:Z

    goto :goto_13
.end method
