.class public LE/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:[B

.field protected b:I

.field protected c:I

.field d:Ljava/nio/ByteBuffer;

.field protected e:I

.field protected f:Li/b;

.field private g:I

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LE/a;-><init>(IZ)V

    .line 84
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput v0, p0, LE/a;->e:I

    .line 76
    iput v0, p0, LE/a;->i:I

    .line 93
    iput-boolean p2, p0, LE/a;->h:Z

    .line 94
    iput p1, p0, LE/a;->b:I

    .line 95
    invoke-direct {p0}, LE/a;->d()V

    .line 96
    return-void
.end method

.method static synthetic a(LE/a;[BI)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, LE/a;->a([BI)V

    return-void
.end method

.method private a([BI)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 312
    iget v1, p0, LE/a;->i:I

    if-lez v1, :cond_1e

    .line 313
    :goto_5
    if-ge v0, p2, :cond_23

    .line 314
    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    .line 315
    iget v2, p0, LE/a;->i:I

    iget v3, p0, LE/a;->i:I

    rsub-int v3, v3, 0xff

    mul-int/2addr v1, v3

    div-int/lit16 v1, v1, 0xff

    add-int/2addr v1, v2

    .line 316
    iget-object v2, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    int-to-byte v1, v1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 313
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 319
    :cond_1e
    iget-object v1, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1, v0, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 321
    :cond_23
    return-void
.end method

.method private d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 205
    iput v2, p0, LE/a;->g:I

    .line 206
    iget-object v0, p0, LE/a;->a:[B

    if-nez v0, :cond_28

    .line 207
    iget v0, p0, LE/a;->b:I

    mul-int/lit8 v0, v0, 0x4

    .line 208
    const/16 v1, 0x1000

    if-lt v0, v1, :cond_13

    iget-boolean v1, p0, LE/a;->h:Z

    if-eqz v1, :cond_1d

    .line 209
    :cond_13
    new-array v0, v0, [B

    iput-object v0, p0, LE/a;->a:[B

    .line 218
    :cond_17
    :goto_17
    iput v2, p0, LE/a;->c:I

    .line 219
    const/4 v0, 0x0

    iput-object v0, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    .line 220
    return-void

    .line 211
    :cond_1d
    new-instance v1, Li/b;

    invoke-direct {v1, v0}, Li/b;-><init>(I)V

    iput-object v1, p0, LE/a;->f:Li/b;

    .line 212
    invoke-virtual {p0}, LE/a;->a()V

    goto :goto_17

    .line 214
    :cond_28
    iget-object v0, p0, LE/a;->f:Li/b;

    if-eqz v0, :cond_17

    .line 215
    iget-object v0, p0, LE/a;->f:Li/b;

    invoke-virtual {v0}, Li/b;->a()V

    .line 216
    invoke-virtual {p0}, LE/a;->a()V

    goto :goto_17
.end method


# virtual methods
.method protected a()V
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, LE/a;->f:Li/b;

    if-eqz v0, :cond_19

    .line 115
    iget-object v0, p0, LE/a;->f:Li/b;

    iget v1, p0, LE/a;->g:I

    invoke-virtual {v0, v1}, Li/b;->b(I)V

    .line 116
    iget-object v0, p0, LE/a;->f:Li/b;

    iget-object v0, v0, Li/b;->c:Ljava/lang/Object;

    check-cast v0, [B

    iput-object v0, p0, LE/a;->a:[B

    .line 117
    iget-object v0, p0, LE/a;->f:Li/b;

    iget v0, v0, Li/b;->d:I

    iput v0, p0, LE/a;->g:I

    .line 119
    :cond_19
    return-void
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 110
    iput p1, p0, LE/a;->i:I

    .line 111
    return-void
.end method

.method public a(II)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 157
    ushr-int/lit8 v0, p1, 0x18

    int-to-byte v1, v0

    .line 158
    ushr-int/lit8 v0, p1, 0x10

    int-to-byte v2, v0

    .line 159
    ushr-int/lit8 v0, p1, 0x8

    int-to-byte v3, v0

    .line 160
    int-to-byte v4, p1

    .line 161
    const/4 v0, 0x0

    :goto_b
    if-ge v0, p2, :cond_41

    .line 162
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v1, v5, v6

    .line 163
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v2, v5, v6

    .line 164
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v3, v5, v6

    .line 165
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v4, v5, v6

    .line 166
    iget v5, p0, LE/a;->g:I

    const/16 v6, 0x1000

    if-lt v5, v6, :cond_3e

    .line 167
    invoke-virtual {p0}, LE/a;->a()V

    .line 161
    :cond_3e
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 170
    :cond_41
    iget v0, p0, LE/a;->c:I

    add-int/2addr v0, p2

    iput v0, p0, LE/a;->c:I

    .line 171
    return-void
.end method

.method protected a(IZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 330
    iget-object v0, p0, LE/a;->f:Li/b;

    if-nez v0, :cond_25

    .line 331
    iget-object v0, p0, LE/a;->a:[B

    invoke-direct {p0, v0, p1}, LE/a;->a([BI)V

    .line 341
    :goto_a
    iget-object v0, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 342
    iget-object v0, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 344
    if-eqz p2, :cond_24

    .line 345
    iget-object v0, p0, LE/a;->f:Li/b;

    if-eqz v0, :cond_22

    .line 346
    iget-object v0, p0, LE/a;->f:Li/b;

    invoke-virtual {v0}, Li/b;->c()V

    .line 347
    iput-object v2, p0, LE/a;->f:Li/b;

    .line 349
    :cond_22
    iput-object v2, p0, LE/a;->a:[B

    .line 351
    :cond_24
    return-void

    .line 333
    :cond_25
    invoke-virtual {p0}, LE/a;->a()V

    .line 334
    iget-object v0, p0, LE/a;->f:Li/b;

    new-instance v1, LE/b;

    invoke-direct {v1, p0}, LE/b;-><init>(LE/a;)V

    invoke-virtual {v0, v1}, Li/b;->a(Li/d;)V

    goto :goto_a
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 236
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 355
    iget v0, p0, LE/a;->e:I

    return v0
.end method

.method public b(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 257
    iget v0, p0, LE/a;->b:I

    if-le p1, v0, :cond_40

    .line 258
    iget v0, p0, LE/a;->b:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 259
    mul-int/lit8 v0, v1, 0x4

    .line 260
    iget-object v2, p0, LE/a;->f:Li/b;

    if-nez v2, :cond_60

    .line 261
    const/16 v2, 0x1000

    if-lt v0, v2, :cond_1b

    iget-boolean v2, p0, LE/a;->h:Z

    if-eqz v2, :cond_41

    .line 262
    :cond_1b
    iget-boolean v2, p0, LE/a;->h:Z

    if-eqz v2, :cond_33

    .line 263
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_2b

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_33

    .line 264
    :cond_2b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_33
    new-array v0, v0, [B

    .line 270
    iget-object v2, p0, LE/a;->a:[B

    iget v3, p0, LE/a;->g:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 271
    iput-object v0, p0, LE/a;->a:[B

    .line 281
    :goto_3e
    iput v1, p0, LE/a;->b:I

    .line 283
    :cond_40
    return-void

    .line 273
    :cond_41
    new-instance v2, Li/b;

    invoke-direct {v2, v0}, Li/b;-><init>(I)V

    iput-object v2, p0, LE/a;->f:Li/b;

    .line 274
    iget-object v0, p0, LE/a;->f:Li/b;

    iget-object v2, p0, LE/a;->a:[B

    iget v3, p0, LE/a;->g:I

    invoke-virtual {v0, v2, v3}, Li/b;->a(Ljava/lang/Object;I)V

    .line 275
    iget-object v0, p0, LE/a;->f:Li/b;

    iget-object v0, v0, Li/b;->c:Ljava/lang/Object;

    check-cast v0, [B

    iput-object v0, p0, LE/a;->a:[B

    .line 276
    iget-object v0, p0, LE/a;->f:Li/b;

    iget v0, v0, Li/b;->d:I

    iput v0, p0, LE/a;->g:I

    goto :goto_3e

    .line 279
    :cond_60
    iget-object v2, p0, LE/a;->f:Li/b;

    invoke-virtual {v2, v0}, Li/b;->c(I)V

    goto :goto_3e
.end method

.method public b(II)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 179
    ushr-int/lit8 v0, p1, 0x18

    int-to-byte v1, v0

    .line 180
    ushr-int/lit8 v0, p1, 0x10

    int-to-byte v2, v0

    .line 181
    ushr-int/lit8 v0, p1, 0x8

    int-to-byte v3, v0

    .line 182
    int-to-byte v4, p1

    .line 183
    const/4 v0, 0x0

    :goto_b
    if-ge v0, p2, :cond_41

    .line 184
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v2, v5, v6

    .line 185
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v3, v5, v6

    .line 186
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v4, v5, v6

    .line 187
    iget-object v5, p0, LE/a;->a:[B

    iget v6, p0, LE/a;->g:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, LE/a;->g:I

    aput-byte v1, v5, v6

    .line 188
    iget v5, p0, LE/a;->g:I

    const/16 v6, 0x1000

    if-lt v5, v6, :cond_3e

    .line 189
    invoke-virtual {p0}, LE/a;->a()V

    .line 183
    :cond_3e
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 192
    :cond_41
    iget v0, p0, LE/a;->c:I

    add-int/2addr v0, p2

    iput v0, p0, LE/a;->c:I

    .line 193
    return-void
.end method

.method public b(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 243
    invoke-virtual {p0, p1}, LE/a;->a(LD/a;)V

    .line 244
    iget-object v0, p0, LE/a;->f:Li/b;

    if-eqz v0, :cond_f

    .line 245
    iget-object v0, p0, LE/a;->f:Li/b;

    invoke-virtual {v0}, Li/b;->c()V

    .line 246
    iput-object v1, p0, LE/a;->f:Li/b;

    .line 248
    :cond_f
    iput-object v1, p0, LE/a;->a:[B

    .line 249
    return-void
.end method

.method public c()I
    .registers 3

    .prologue
    .line 362
    const/16 v0, 0x20

    .line 363
    iget-object v1, p0, LE/a;->f:Li/b;

    if-eqz v1, :cond_19

    .line 365
    iget-object v1, p0, LE/a;->f:Li/b;

    invoke-virtual {v1}, Li/b;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_d
    :goto_d
    iget-object v1, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_18

    .line 370
    iget-object v1, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    add-int/2addr v0, v1

    .line 372
    :cond_18
    return v0

    .line 366
    :cond_19
    iget-object v1, p0, LE/a;->a:[B

    if-eqz v1, :cond_d

    .line 367
    iget-object v1, p0, LE/a;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_d
.end method

.method public c(LD/a;)V
    .registers 7
    .parameter

    .prologue
    .line 290
    iget-object v0, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_7

    .line 291
    invoke-virtual {p0, p1}, LE/a;->d(LD/a;)V

    .line 293
    :cond_7
    iget-object v0, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    iput v0, p0, LE/a;->e:I

    .line 299
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x4

    const/16 v2, 0x1401

    const/4 v3, 0x0

    iget-object v4, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColorPointer(IIILjava/nio/Buffer;)V

    .line 301
    return-void
.end method

.method protected d(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 305
    iget v0, p0, LE/a;->c:I

    mul-int/lit8 v0, v0, 0x4

    .line 306
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lx/k;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, LE/a;->d:Ljava/nio/ByteBuffer;

    .line 308
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LE/a;->a(IZ)V

    .line 309
    return-void
.end method
