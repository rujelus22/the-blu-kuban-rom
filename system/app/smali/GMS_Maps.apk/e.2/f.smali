.class public LE/f;
.super LE/d;
.source "SourceFile"


# instance fields
.field private final h:[I

.field private volatile i:J


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, LE/d;-><init>(I)V

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/f;->h:[I

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/f;->i:J

    .line 44
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, LE/d;-><init>(IZ)V

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/f;->h:[I

    .line 40
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/f;->i:J

    .line 48
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-super {p0, p1}, LE/d;->a(LD/a;)V

    .line 53
    iget-object v0, p0, LE/f;->h:[I

    if-eqz v0, :cond_c

    .line 54
    iget-object v0, p0, LE/f;->h:[I

    aput v1, v0, v1

    .line 56
    :cond_c
    return-void
.end method

.method public a(LD/a;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const v5, 0x8893

    const/4 v4, 0x0

    .line 113
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/f;->i:J

    .line 114
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_14

    .line 115
    invoke-super {p0, p1, p2}, LE/d;->a(LD/a;I)V

    .line 139
    :cond_13
    :goto_13
    return-void

    .line 119
    :cond_14
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 120
    iget-object v1, p0, LE/f;->h:[I

    aget v1, v1, v4

    if-nez v1, :cond_53

    .line 121
    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    if-nez v1, :cond_27

    .line 122
    invoke-virtual {p0, p1}, LE/f;->d(LD/a;)V

    .line 124
    :cond_27
    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_13

    .line 128
    const/4 v1, 0x1

    iget-object v2, p0, LE/f;->h:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    .line 129
    iget-object v1, p0, LE/f;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 130
    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->limit()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, LE/f;->f:I

    .line 131
    iget v1, p0, LE/f;->f:I

    iget-object v2, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 133
    const/4 v1, 0x0

    iput-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    .line 136
    :cond_53
    iget-object v1, p0, LE/f;->h:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 137
    iget v1, p0, LE/f;->c:I

    const/16 v2, 0x1403

    invoke-interface {v0, p2, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glDrawElements(IIII)V

    .line 138
    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_13
.end method

.method public b(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 68
    iget-object v0, p0, LE/f;->h:[I

    if-eqz v0, :cond_27

    iget-object v0, p0, LE/f;->h:[I

    aget v0, v0, v3

    if-eqz v0, :cond_27

    .line 72
    iget-wide v0, p0, LE/f;->i:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    .line 73
    if-ne v0, p1, :cond_21

    if-eqz v0, :cond_21

    .line 74
    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 75
    const/4 v1, 0x1

    iget-object v2, p0, LE/f;->h:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    .line 77
    :cond_21
    iget-object v0, p0, LE/f;->h:[I

    aput v3, v0, v3

    .line 78
    iput v3, p0, LE/f;->f:I

    .line 80
    :cond_27
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/f;->i:J

    .line 81
    return-void
.end method

.method public d()I
    .registers 3

    .prologue
    .line 147
    const/16 v0, 0x38

    .line 148
    iget-object v1, p0, LE/f;->g:Li/j;

    if-eqz v1, :cond_10

    .line 150
    iget-object v1, p0, LE/f;->g:Li/j;

    invoke-virtual {v1}, Li/j;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 154
    :cond_f
    :goto_f
    return v0

    .line 151
    :cond_10
    iget-object v1, p0, LE/f;->a:[S

    if-eqz v1, :cond_f

    .line 152
    iget-object v1, p0, LE/f;->a:[S

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_f
.end method

.method protected d(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 86
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 87
    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->b()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    .line 88
    iget-object v0, p0, LE/f;->g:Li/j;

    if-nez v0, :cond_3d

    .line 89
    iget-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    iget-object v1, p0, LE/f;->a:[S

    iget v2, p0, LE/f;->c:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    .line 94
    :goto_1f
    iget-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    iget v1, p0, LE/f;->c:I

    invoke-virtual {v0, v1}, Ljava/nio/ShortBuffer;->limit(I)Ljava/nio/Buffer;

    .line 95
    iget-object v0, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 98
    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_3c

    .line 99
    iget-object v0, p0, LE/f;->g:Li/j;

    if-eqz v0, :cond_3a

    .line 100
    iget-object v0, p0, LE/f;->g:Li/j;

    invoke-virtual {v0}, Li/j;->c()V

    .line 101
    iput-object v4, p0, LE/f;->g:Li/j;

    .line 103
    :cond_3a
    iput-object v4, p0, LE/f;->a:[S

    .line 108
    :cond_3c
    :goto_3c
    return-void

    .line 91
    :cond_3d
    invoke-virtual {p0}, LE/f;->a()V

    .line 92
    iget-object v0, p0, LE/f;->g:Li/j;

    iget-object v1, p0, LE/f;->d:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, v1}, Li/j;->a(Ljava/nio/ShortBuffer;)V

    goto :goto_1f

    .line 106
    :cond_48
    invoke-super {p0, p1}, LE/d;->d(LD/a;)V

    goto :goto_3c
.end method
