.class public LE/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LE/q;


# instance fields
.field protected a:I

.field b:[I

.field c:I

.field d:I

.field e:Ljava/nio/IntBuffer;

.field f:I

.field protected g:Li/h;

.field private h:Z


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, LE/o;->a:I

    .line 102
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LE/o;-><init>(IZ)V

    .line 84
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput v0, p0, LE/o;->a:I

    .line 93
    iput-boolean p2, p0, LE/o;->h:Z

    .line 94
    iput p1, p0, LE/o;->c:I

    .line 95
    invoke-direct {p0}, LE/o;->e()V

    .line 96
    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 189
    iput v2, p0, LE/o;->f:I

    .line 190
    iget-object v0, p0, LE/o;->b:[I

    if-nez v0, :cond_28

    .line 191
    iget v0, p0, LE/o;->c:I

    mul-int/lit8 v0, v0, 0x3

    .line 192
    const/16 v1, 0x400

    if-lt v0, v1, :cond_13

    iget-boolean v1, p0, LE/o;->h:Z

    if-eqz v1, :cond_1d

    .line 193
    :cond_13
    new-array v0, v0, [I

    iput-object v0, p0, LE/o;->b:[I

    .line 202
    :cond_17
    :goto_17
    iput v2, p0, LE/o;->d:I

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    .line 204
    return-void

    .line 195
    :cond_1d
    new-instance v1, Li/h;

    invoke-direct {v1, v0}, Li/h;-><init>(I)V

    iput-object v1, p0, LE/o;->g:Li/h;

    .line 196
    invoke-virtual {p0}, LE/o;->b()V

    goto :goto_17

    .line 198
    :cond_28
    iget-object v0, p0, LE/o;->g:Li/h;

    if-eqz v0, :cond_17

    .line 199
    iget-object v0, p0, LE/o;->g:Li/h;

    invoke-virtual {v0}, Li/h;->a()V

    .line 200
    invoke-virtual {p0}, LE/o;->b()V

    goto :goto_17
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 184
    iget v0, p0, LE/o;->d:I

    return v0
.end method

.method public a(FFF)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x4780

    .line 155
    iget v0, p0, LE/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/o;->d:I

    .line 156
    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/o;->f:I

    mul-float v2, p1, v3

    float-to-int v2, v2

    aput v2, v0, v1

    .line 157
    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/o;->f:I

    mul-float v2, p2, v3

    float-to-int v2, v2

    aput v2, v0, v1

    .line 158
    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LE/o;->f:I

    mul-float v2, p3, v3

    float-to-int v2, v2

    aput v2, v0, v1

    .line 159
    iget v0, p0, LE/o;->f:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_38

    .line 160
    invoke-virtual {p0}, LE/o;->b()V

    .line 162
    :cond_38
    return-void
.end method

.method public a(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 238
    iget v0, p0, LE/o;->c:I

    if-le p1, v0, :cond_40

    .line 239
    iget v0, p0, LE/o;->c:I

    mul-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 240
    mul-int/lit8 v0, v1, 0x3

    .line 241
    iget-object v2, p0, LE/o;->g:Li/h;

    if-nez v2, :cond_60

    .line 242
    const/16 v2, 0x400

    if-lt v0, v2, :cond_1b

    iget-boolean v2, p0, LE/o;->h:Z

    if-eqz v2, :cond_41

    .line 243
    :cond_1b
    iget-boolean v2, p0, LE/o;->h:Z

    if-eqz v2, :cond_33

    .line 244
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v2

    if-nez v2, :cond_2b

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_33

    .line 245
    :cond_2b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to grow fixed size buffer"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_33
    new-array v0, v0, [I

    .line 251
    iget-object v2, p0, LE/o;->b:[I

    iget v3, p0, LE/o;->f:I

    invoke-static {v2, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 252
    iput-object v0, p0, LE/o;->b:[I

    .line 262
    :goto_3e
    iput v1, p0, LE/o;->c:I

    .line 264
    :cond_40
    return-void

    .line 254
    :cond_41
    new-instance v2, Li/h;

    invoke-direct {v2, v0}, Li/h;-><init>(I)V

    iput-object v2, p0, LE/o;->g:Li/h;

    .line 255
    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v2, p0, LE/o;->b:[I

    iget v3, p0, LE/o;->f:I

    invoke-virtual {v0, v2, v3}, Li/h;->a(Ljava/lang/Object;I)V

    .line 256
    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/o;->b:[I

    .line 257
    iget-object v0, p0, LE/o;->g:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/o;->f:I

    goto :goto_3e

    .line 260
    :cond_60
    iget-object v2, p0, LE/o;->g:Li/h;

    invoke-virtual {v2, v0}, Li/h;->c(I)V

    goto :goto_3e
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 208
    invoke-virtual {p0, p1}, LE/o;->b(LD/a;)V

    .line 209
    invoke-direct {p0}, LE/o;->e()V

    .line 210
    return-void
.end method

.method public a(Lo/T;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 131
    iget v0, p0, LE/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LE/o;->d:I

    .line 132
    iget-object v0, p0, LE/o;->b:[I

    iget v1, p0, LE/o;->f:I

    invoke-virtual {p1, p2, v0, v1}, Lo/T;->a(I[II)V

    .line 133
    iget v0, p0, LE/o;->f:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, LE/o;->f:I

    .line 134
    iget v0, p0, LE/o;->f:I

    const/16 v1, 0x400

    if-lt v0, v1, :cond_1c

    .line 135
    invoke-virtual {p0}, LE/o;->b()V

    .line 137
    :cond_1c
    return-void
.end method

.method public a(Lo/T;IB)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    invoke-virtual {p0, p1, p2}, LE/o;->a(Lo/T;I)V

    .line 147
    return-void
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 105
    iget-object v0, p0, LE/o;->g:Li/h;

    if-eqz v0, :cond_19

    .line 106
    iget-object v0, p0, LE/o;->g:Li/h;

    iget v1, p0, LE/o;->f:I

    invoke-virtual {v0, v1}, Li/h;->b(I)V

    .line 107
    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v0, v0, Li/h;->c:Ljava/lang/Object;

    check-cast v0, [I

    iput-object v0, p0, LE/o;->b:[I

    .line 108
    iget-object v0, p0, LE/o;->g:Li/h;

    iget v0, v0, Li/h;->d:I

    iput v0, p0, LE/o;->f:I

    .line 110
    :cond_19
    return-void
.end method

.method public b(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 217
    return-void
.end method

.method public c()I
    .registers 2

    .prologue
    .line 314
    iget v0, p0, LE/o;->a:I

    return v0
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 224
    invoke-virtual {p0, p1}, LE/o;->b(LD/a;)V

    .line 225
    iget-object v0, p0, LE/o;->g:Li/h;

    if-eqz v0, :cond_f

    .line 226
    iget-object v0, p0, LE/o;->g:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    .line 227
    iput-object v1, p0, LE/o;->g:Li/h;

    .line 229
    :cond_f
    iput-object v1, p0, LE/o;->b:[I

    .line 230
    return-void
.end method

.method public d()I
    .registers 3

    .prologue
    .line 321
    const/16 v0, 0x20

    .line 322
    iget-object v1, p0, LE/o;->g:Li/h;

    if-eqz v1, :cond_1d

    .line 324
    iget-object v1, p0, LE/o;->g:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 328
    :cond_f
    :goto_f
    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    if-eqz v1, :cond_1c

    .line 329
    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v1}, Ljava/nio/IntBuffer;->capacity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 331
    :cond_1c
    return v0

    .line 325
    :cond_1d
    iget-object v1, p0, LE/o;->b:[I

    if-eqz v1, :cond_f

    .line 326
    iget-object v1, p0, LE/o;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_f
.end method

.method public d(LD/a;)V
    .registers 7
    .parameter

    .prologue
    .line 278
    monitor-enter p0

    .line 279
    :try_start_1
    iget-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    if-nez v0, :cond_8

    .line 280
    invoke-virtual {p0, p1}, LE/o;->e(LD/a;)V

    .line 282
    :cond_8
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_21

    .line 283
    iget-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0}, Ljava/nio/IntBuffer;->limit()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iput v0, p0, LE/o;->a:I

    .line 290
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x3

    const/16 v2, 0x140c

    const/4 v3, 0x0

    iget-object v4, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glVertexPointer(IIILjava/nio/Buffer;)V

    .line 292
    return-void

    .line 282
    :catchall_21
    move-exception v0

    :try_start_22
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_21

    throw v0
.end method

.method protected e(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 296
    iget v0, p0, LE/o;->d:I

    mul-int/lit8 v0, v0, 0x3

    .line 297
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v1

    mul-int/lit8 v2, v0, 0x4

    invoke-virtual {v1, v2}, Lx/k;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 298
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 299
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    iput-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    .line 300
    iget-object v1, p0, LE/o;->g:Li/h;

    if-nez v1, :cond_30

    .line 301
    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/o;->b:[I

    invoke-virtual {v1, v2, v3, v0}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    .line 308
    :goto_28
    iget-object v0, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 309
    iput-object v4, p0, LE/o;->b:[I

    .line 310
    return-void

    .line 303
    :cond_30
    invoke-virtual {p0}, LE/o;->b()V

    .line 304
    iget-object v0, p0, LE/o;->g:Li/h;

    iget-object v1, p0, LE/o;->e:Ljava/nio/IntBuffer;

    invoke-virtual {v0, v1}, Li/h;->a(Ljava/nio/IntBuffer;)V

    .line 305
    iget-object v0, p0, LE/o;->g:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    .line 306
    iput-object v4, p0, LE/o;->g:Li/h;

    goto :goto_28
.end method
