.class public LE/l;
.super LE/i;
.source "SourceFile"


# instance fields
.field private final k:[I

.field private volatile l:J


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, LE/i;-><init>(I)V

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/l;->k:[I

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/l;->l:J

    .line 51
    return-void
.end method

.method protected constructor <init>(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 58
    invoke-direct {p0, p1, p2, p3, v0}, LE/i;-><init>(IIIZ)V

    .line 35
    new-array v0, v0, [I

    iput-object v0, p0, LE/l;->k:[I

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/l;->l:J

    .line 59
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, LE/i;-><init>(IZ)V

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/l;->k:[I

    .line 47
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/l;->l:J

    .line 55
    return-void
.end method

.method public static b(II)LE/l;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 62
    new-instance v0, LE/l;

    const/16 v1, 0x1402

    invoke-direct {v0, p0, v1, p1}, LE/l;-><init>(III)V

    return-object v0
.end method

.method private f(LD/a;)Z
    .registers 8
    .parameter

    .prologue
    const v5, 0x8892

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 207
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    if-nez v0, :cond_c

    .line 208
    invoke-virtual {p0, p1}, LE/l;->e(LD/a;)V

    .line 210
    :cond_c
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v0

    if-nez v0, :cond_16

    move v0, v1

    .line 220
    :goto_15
    return v0

    .line 214
    :cond_16
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 215
    iget-object v3, p0, LE/l;->k:[I

    invoke-interface {v0, v2, v3, v1}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    .line 216
    iget-object v3, p0, LE/l;->k:[I

    aget v1, v3, v1

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 217
    iget-object v1, p0, LE/l;->e:Ljava/nio/Buffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->limit()I

    move-result v1

    iget v3, p0, LE/l;->g:I

    mul-int/2addr v1, v3

    iput v1, p0, LE/l;->a:I

    .line 218
    iget v1, p0, LE/l;->a:I

    iget-object v3, p0, LE/l;->e:Ljava/nio/Buffer;

    const v4, 0x88e4

    invoke-interface {v0, v5, v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 219
    const/4 v0, 0x0

    iput-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    move v0, v2

    .line 220
    goto :goto_15
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-super {p0, p1}, LE/i;->a(LD/a;)V

    .line 76
    iget-object v0, p0, LE/l;->k:[I

    if-eqz v0, :cond_c

    .line 77
    iget-object v0, p0, LE/l;->k:[I

    aput v1, v0, v1

    .line 79
    :cond_c
    return-void
.end method

.method public a(LD/a;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const v6, 0x8892

    const/4 v5, 0x0

    .line 181
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/l;->l:J

    .line 182
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_14

    .line 183
    invoke-super {p0, p1, p2}, LE/i;->a(LD/a;I)V

    .line 203
    :cond_13
    :goto_13
    return-void

    .line 187
    :cond_14
    iget-object v0, p0, LE/l;->k:[I

    aget v0, v0, v5

    if-nez v0, :cond_20

    .line 188
    invoke-direct {p0, p1}, LE/l;->f(LD/a;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 192
    :cond_20
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 193
    iget-object v1, p0, LE/l;->k:[I

    aget v1, v1, v5

    invoke-interface {v0, v6, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 199
    const/4 v1, 0x2

    iget v2, p0, LE/l;->f:I

    mul-int/lit8 v3, p2, 0x2

    iget v4, p0, LE/l;->g:I

    mul-int/2addr v3, v4

    invoke-interface {v0, v1, v2, v5, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 202
    invoke-interface {v0, v6, v5}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_13
.end method

.method public b(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 91
    iget-object v0, p0, LE/l;->k:[I

    if-eqz v0, :cond_27

    iget-object v0, p0, LE/l;->k:[I

    aget v0, v0, v3

    if-eqz v0, :cond_27

    .line 95
    iget-wide v0, p0, LE/l;->l:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    .line 96
    if-ne v0, p1, :cond_21

    if-eqz v0, :cond_21

    .line 97
    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 98
    const/4 v1, 0x1

    iget-object v2, p0, LE/l;->k:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    .line 100
    :cond_21
    iget-object v0, p0, LE/l;->k:[I

    aput v3, v0, v3

    .line 101
    iput v3, p0, LE/l;->a:I

    .line 103
    :cond_27
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/l;->l:J

    .line 104
    return-void
.end method

.method public c()I
    .registers 3

    .prologue
    .line 229
    const/16 v0, 0x38

    .line 230
    iget-object v1, p0, LE/l;->j:Li/h;

    if-eqz v1, :cond_10

    .line 232
    iget-object v1, p0, LE/l;->j:Li/h;

    invoke-virtual {v1}, Li/h;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 236
    :cond_f
    :goto_f
    return v0

    .line 233
    :cond_10
    iget-object v1, p0, LE/l;->b:[I

    if-eqz v1, :cond_f

    .line 234
    iget-object v1, p0, LE/l;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_f
.end method

.method public d(LD/a;)V
    .registers 7
    .parameter

    .prologue
    const v4, 0x8892

    const/4 v3, 0x0

    .line 156
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/l;->l:J

    .line 157
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_14

    .line 158
    invoke-super {p0, p1}, LE/i;->d(LD/a;)V

    .line 177
    :cond_13
    :goto_13
    return-void

    .line 162
    :cond_14
    iget-object v0, p0, LE/l;->k:[I

    aget v0, v0, v3

    if-nez v0, :cond_20

    .line 163
    invoke-direct {p0, p1}, LE/l;->f(LD/a;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 167
    :cond_20
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 168
    iget-object v1, p0, LE/l;->k:[I

    aget v1, v1, v3

    invoke-interface {v0, v4, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 174
    const/4 v1, 0x2

    iget v2, p0, LE/l;->f:I

    invoke-interface {v0, v1, v2, v3, v3}, Ljavax/microedition/khronos/opengles/GL11;->glTexCoordPointer(IIII)V

    .line 176
    invoke-interface {v0, v4, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_13
.end method

.method protected e(LD/a;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 109
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 110
    iget v0, p0, LE/l;->d:I

    mul-int/lit8 v1, v0, 0x2

    .line 112
    iget v0, p0, LE/l;->f:I

    const/16 v2, 0x1402

    if-ne v0, v2, :cond_52

    .line 113
    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->b()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    .line 114
    iget-object v0, p0, LE/l;->j:Li/h;

    if-nez v0, :cond_43

    .line 115
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    invoke-virtual {p0, v0, v1}, LE/l;->a(Ljava/nio/ShortBuffer;I)V

    .line 138
    :goto_27
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    invoke-virtual {v0, v1}, Ljava/nio/Buffer;->limit(I)Ljava/nio/Buffer;

    .line 139
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    invoke-virtual {v0, v4}, Ljava/nio/Buffer;->position(I)Ljava/nio/Buffer;

    .line 142
    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_42

    .line 143
    iget-object v0, p0, LE/l;->j:Li/h;

    if-eqz v0, :cond_40

    .line 144
    iget-object v0, p0, LE/l;->j:Li/h;

    invoke-virtual {v0}, Li/h;->c()V

    .line 145
    iput-object v5, p0, LE/l;->j:Li/h;

    .line 147
    :cond_40
    iput-object v5, p0, LE/l;->b:[I

    .line 152
    :cond_42
    :goto_42
    return-void

    .line 117
    :cond_43
    invoke-virtual {p0}, LE/l;->a()V

    .line 118
    iget-object v2, p0, LE/l;->j:Li/h;

    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ShortBuffer;

    iget v3, p0, LE/l;->h:I

    invoke-virtual {v2, v0, v3}, Li/h;->a(Ljava/nio/ShortBuffer;I)V

    goto :goto_27

    .line 120
    :cond_52
    iget v0, p0, LE/l;->f:I

    const/16 v2, 0x1401

    if-eq v0, v2, :cond_5e

    iget v0, p0, LE/l;->f:I

    const/16 v2, 0x1400

    if-ne v0, v2, :cond_83

    .line 121
    :cond_5e
    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    .line 122
    iget-object v0, p0, LE/l;->j:Li/h;

    if-nez v0, :cond_74

    .line 123
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0, v1}, LE/l;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_27

    .line 125
    :cond_74
    invoke-virtual {p0}, LE/l;->a()V

    .line 126
    iget-object v2, p0, LE/l;->j:Li/h;

    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/ByteBuffer;

    iget v3, p0, LE/l;->h:I

    invoke-virtual {v2, v0, v3}, Li/h;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_27

    .line 129
    :cond_83
    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->c()Ljava/nio/IntBuffer;

    move-result-object v0

    iput-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    .line 130
    iget-object v0, p0, LE/l;->j:Li/h;

    if-nez v0, :cond_9b

    .line 131
    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    iget-object v2, p0, LE/l;->b:[I

    invoke-virtual {v0, v2, v4, v1}, Ljava/nio/IntBuffer;->put([III)Ljava/nio/IntBuffer;

    goto :goto_27

    .line 133
    :cond_9b
    invoke-virtual {p0}, LE/l;->a()V

    .line 134
    iget-object v2, p0, LE/l;->j:Li/h;

    iget-object v0, p0, LE/l;->e:Ljava/nio/Buffer;

    check-cast v0, Ljava/nio/IntBuffer;

    invoke-virtual {v2, v0}, Li/h;->a(Ljava/nio/IntBuffer;)V

    goto :goto_27

    .line 150
    :cond_a8
    invoke-super {p0, p1}, LE/i;->e(LD/a;)V

    goto :goto_42
.end method
