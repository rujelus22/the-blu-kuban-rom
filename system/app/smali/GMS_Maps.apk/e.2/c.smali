.class public LE/c;
.super LE/a;
.source "SourceFile"


# instance fields
.field private final g:[I

.field private volatile h:J


# direct methods
.method public constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, LE/a;-><init>(I)V

    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/c;->g:[I

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/c;->h:J

    .line 49
    return-void
.end method

.method public constructor <init>(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, LE/a;-><init>(IZ)V

    .line 33
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LE/c;->g:[I

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LE/c;->h:J

    .line 53
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 73
    iget-object v0, p0, LE/c;->g:[I

    if-eqz v0, :cond_27

    iget-object v0, p0, LE/c;->g:[I

    aget v0, v0, v3

    if-eqz v0, :cond_27

    .line 77
    iget-wide v0, p0, LE/c;->h:J

    invoke-static {v0, v1}, LD/a;->b(J)LD/a;

    move-result-object v0

    .line 78
    if-ne v0, p1, :cond_21

    if-eqz v0, :cond_21

    .line 79
    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 80
    const/4 v1, 0x1

    iget-object v2, p0, LE/c;->g:[I

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glDeleteBuffers(I[II)V

    .line 82
    :cond_21
    iget-object v0, p0, LE/c;->g:[I

    aput v3, v0, v3

    .line 83
    iput v3, p0, LE/c;->e:I

    .line 85
    :cond_27
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/c;->h:J

    .line 86
    return-void
.end method

.method public c()I
    .registers 3

    .prologue
    .line 145
    const/16 v0, 0x38

    .line 146
    iget-object v1, p0, LE/c;->f:Li/b;

    if-eqz v1, :cond_e

    .line 148
    iget-object v1, p0, LE/c;->f:Li/b;

    invoke-virtual {v1}, Li/b;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_d
    :goto_d
    return v0

    .line 149
    :cond_e
    iget-object v1, p0, LE/c;->a:[B

    if-eqz v1, :cond_d

    .line 150
    iget-object v1, p0, LE/c;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    goto :goto_d
.end method

.method public c(LD/a;)V
    .registers 8
    .parameter

    .prologue
    const v5, 0x8892

    const/4 v4, 0x0

    .line 105
    invoke-static {p1}, LD/a;->a(LD/a;)J

    move-result-wide v0

    iput-wide v0, p0, LE/c;->h:J

    .line 106
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-nez v0, :cond_14

    .line 107
    invoke-super {p0, p1}, LE/a;->c(LD/a;)V

    .line 137
    :cond_13
    :goto_13
    return-void

    .line 111
    :cond_14
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/opengles/GL11;

    .line 112
    iget-object v1, p0, LE/c;->g:[I

    aget v1, v1, v4

    if-nez v1, :cond_51

    .line 113
    iget-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_27

    .line 114
    invoke-virtual {p0, p1}, LE/c;->d(LD/a;)V

    .line 116
    :cond_27
    iget-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-eqz v1, :cond_13

    .line 120
    const/4 v1, 0x1

    iget-object v2, p0, LE/c;->g:[I

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL11;->glGenBuffers(I[II)V

    .line 121
    iget-object v1, p0, LE/c;->g:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 122
    iget-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    iput v1, p0, LE/c;->e:I

    .line 123
    iget v1, p0, LE/c;->e:I

    iget-object v2, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    const v3, 0x88e4

    invoke-interface {v0, v5, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL11;->glBufferData(IILjava/nio/Buffer;I)V

    .line 124
    const/4 v1, 0x0

    iput-object v1, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    .line 127
    :cond_51
    iget-object v1, p0, LE/c;->g:[I

    aget v1, v1, v4

    invoke-interface {v0, v5, v1}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    .line 134
    const/4 v1, 0x4

    const/16 v2, 0x1401

    invoke-interface {v0, v1, v2, v4, v4}, Ljavax/microedition/khronos/opengles/GL11;->glColorPointer(IIII)V

    .line 136
    invoke-interface {v0, v5, v4}, Ljavax/microedition/khronos/opengles/GL11;->glBindBuffer(II)V

    goto :goto_13
.end method

.method protected d(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 91
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 92
    iget v0, p0, LE/c;->c:I

    mul-int/lit8 v1, v0, 0x4

    .line 93
    invoke-virtual {p1}, LD/a;->L()LE/n;

    move-result-object v0

    invoke-virtual {v0}, LE/n;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LE/c;->d:Ljava/nio/ByteBuffer;

    .line 96
    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_1d

    const/4 v0, 0x1

    :goto_19
    invoke-virtual {p0, v1, v0}, LE/c;->a(IZ)V

    .line 100
    :goto_1c
    return-void

    .line 96
    :cond_1d
    const/4 v0, 0x0

    goto :goto_19

    .line 98
    :cond_1f
    invoke-super {p0, p1}, LE/a;->d(LD/a;)V

    goto :goto_1c
.end method
