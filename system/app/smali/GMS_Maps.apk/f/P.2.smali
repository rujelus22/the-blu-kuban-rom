.class public LF/P;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final h:Ljava/util/Map;


# instance fields
.field private volatile b:LD/b;

.field private c:LE/i;

.field private final d:[B

.field private e:Lh/e;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 66
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LF/P;->h:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>([BLjava/util/Set;Lo/aq;LD/a;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-direct {p0, p2}, LF/i;-><init>(Ljava/util/Set;)V

    .line 92
    iput-object p1, p0, LF/P;->d:[B

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/P;->f:Z

    .line 94
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LF/P;->g:J

    .line 108
    return-void
.end method

.method private static a(F)LE/i;
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 177
    sget-object v0, LF/P;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 178
    sget-object v0, LF/P;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LE/i;

    .line 188
    :goto_19
    return-object v0

    .line 180
    :cond_1a
    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    .line 182
    const/high16 v1, 0x4780

    mul-float/2addr v1, p0

    float-to-int v1, v1

    .line 183
    invoke-virtual {v0, v2, v2}, LE/i;->a(II)V

    .line 184
    invoke-virtual {v0, v2, v1}, LE/i;->a(II)V

    .line 185
    invoke-virtual {v0, v1, v2}, LE/i;->a(II)V

    .line 186
    invoke-virtual {v0, v1, v1}, LE/i;->a(II)V

    .line 187
    sget-object v1, LF/P;->h:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_19
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;LD/a;)LF/P;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 76
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/ac;

    .line 77
    invoke-virtual {v0}, Lo/ac;->l()[I

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_11
    if-ge v1, v4, :cond_22

    aget v5, v3, v1

    .line 78
    if-ltz v5, :cond_1f

    array-length v6, p1

    if-ge v5, v6, :cond_1f

    .line 79
    aget-object v5, p1, v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_1f
    add-int/lit8 v1, v1, 0x1

    goto :goto_11

    .line 82
    :cond_22
    new-instance v1, LF/P;

    invoke-virtual {v0}, Lo/ac;->b()[B

    move-result-object v0

    invoke-direct {v1, v0, v2, p0, p3}, LF/P;-><init>([BLjava/util/Set;Lo/aq;LD/a;)V

    return-object v1
.end method

.method public static a([BLo/aq;LD/a;)LF/P;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    new-instance v0, LF/P;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {v0, p0, v1, p1, p2}, LF/P;-><init>([BLjava/util/Set;Lo/aq;LD/a;)V

    return-object v0
.end method

.method private a(LD/a;LC/a;LD/b;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x1

    const/4 v3, 0x0

    .line 228
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    .line 230
    iget-object v0, p0, LF/P;->c:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 233
    invoke-virtual {p3, v2}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 237
    iget-object v0, p0, LF/P;->e:Lh/e;

    if-eqz v0, :cond_29

    .line 238
    iget-object v0, p0, LF/P;->e:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    .line 239
    if-ne v0, v1, :cond_20

    .line 240
    const/4 v1, 0x0

    iput-object v1, p0, LF/P;->e:Lh/e;

    .line 241
    iput-boolean v3, p0, LF/P;->f:Z

    .line 246
    :cond_20
    :goto_20
    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 248
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v2, v0, v3, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 249
    return-void

    :cond_29
    move v0, v1

    .line 244
    goto :goto_20
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 156
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 157
    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 158
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 159
    invoke-virtual {p0}, LD/a;->p()V

    .line 162
    iget-object v0, p0, LD/a;->f:LE/o;

    invoke-virtual {v0, p0}, LE/o;->d(LD/a;)V

    .line 163
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    iget-object v1, p0, LF/P;->b:LD/b;

    .line 269
    if-eqz v1, :cond_a

    .line 270
    invoke-virtual {v1}, LD/b;->i()I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_a
    return v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 262
    iput-wide p1, p0, LF/P;->g:J

    .line 263
    return-void
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, LF/P;->b:LD/b;

    if-eqz v0, :cond_f

    .line 141
    iget-object v0, p0, LF/P;->b:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, LF/P;->b:LD/b;

    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/P;->f:Z

    .line 146
    :cond_f
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x4e20

    .line 202
    iget-object v6, p0, LF/P;->b:LD/b;

    .line 203
    if-nez v6, :cond_50

    .line 204
    const/4 v0, 0x0

    .line 205
    iget-boolean v1, p0, LF/P;->f:Z

    if-nez v1, :cond_33

    .line 206
    invoke-virtual {p1, v2}, LD/a;->b(I)V

    .line 207
    invoke-virtual {p0, p1}, LF/P;->c(LD/a;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 212
    :goto_13
    if-eqz v1, :cond_50

    .line 213
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    .line 214
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LD/b;->c(Z)V

    .line 215
    invoke-virtual {v0, v1}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 216
    iput-object v0, p0, LF/P;->b:LD/b;

    .line 217
    invoke-virtual {v0}, LD/b;->b()F

    move-result v2

    invoke-static {v2}, LF/P;->a(F)LE/i;

    move-result-object v2

    iput-object v2, p0, LF/P;->c:LE/i;

    .line 218
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 221
    :goto_30
    if-nez v0, :cond_4c

    .line 225
    :goto_32
    return-void

    .line 208
    :cond_33
    invoke-virtual {p1, v2}, LD/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 209
    invoke-virtual {p0, p1}, LF/P;->c(LD/a;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 210
    new-instance v0, Lh/e;

    iget-wide v1, p0, LF/P;->g:J

    const-wide/16 v3, 0xfa

    sget-object v5, Lh/g;->a:Lh/g;

    invoke-direct/range {v0 .. v5}, Lh/e;-><init>(JJLh/g;)V

    iput-object v0, p0, LF/P;->e:Lh/e;

    move-object v1, v7

    goto :goto_13

    .line 224
    :cond_4c
    invoke-direct {p0, p1, p2, v0}, LF/P;->a(LD/a;LC/a;LD/b;)V

    goto :goto_32

    :cond_50
    move-object v0, v6

    goto :goto_30

    :cond_52
    move-object v1, v0

    goto :goto_13
.end method

.method public b()I
    .registers 3

    .prologue
    .line 277
    const/16 v0, 0x60

    .line 278
    iget-object v1, p0, LF/P;->d:[B

    if-eqz v1, :cond_c

    .line 279
    iget-object v1, p0, LF/P;->d:[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    .line 281
    :cond_c
    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 150
    invoke-virtual {p0, p1}, LF/P;->a(LD/a;)V

    .line 151
    iget-object v0, p0, LF/P;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 152
    return-void
.end method

.method c(LD/a;)Landroid/graphics/Bitmap;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 116
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 117
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 118
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 119
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 122
    :try_start_e
    invoke-virtual {p1}, LD/a;->k()Lx/k;

    move-result-object v1

    iget-object v2, p0, LF/P;->d:[B

    invoke-virtual {v1, v2, v0}, Lx/k;->a([BLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_17
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_17} :catch_19

    move-result-object v0

    .line 127
    :goto_18
    return-object v0

    .line 123
    :catch_19
    move-exception v0

    .line 124
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 253
    iget-boolean v0, p0, LF/P;->f:Z

    return v0
.end method

.method public e()V
    .registers 2

    .prologue
    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, LF/P;->f:Z

    .line 259
    return-void
.end method
