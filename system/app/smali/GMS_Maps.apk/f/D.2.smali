.class public LF/D;
.super LF/m;
.source "SourceFile"


# instance fields
.field private final A:F

.field private final B:F

.field private final C:F

.field private D:I

.field private E:I

.field private F:F

.field private final G:I

.field private H:Z

.field private I:I

.field private J:Z

.field private final K:Ljava/lang/String;

.field private final L:F

.field private final M:[F

.field private final l:Ljava/lang/String;

.field private final m:Lcom/google/android/maps/driveabout/vector/aX;

.field private final n:Lo/X;

.field private o:Lo/X;

.field private final p:F

.field private q:Lo/W;

.field private r:[LF/F;

.field private final s:Lcom/google/android/maps/driveabout/vector/aV;

.field private t:LD/b;

.field private u:LE/i;

.field private v:LE/i;

.field private w:Z

.field private x:LE/i;

.field private y:LE/o;

.field private z:Lh/e;


# direct methods
.method constructor <init>(Lo/n;Ly/b;Ljava/lang/String;Lo/aj;IIZFFFILo/X;FFLcom/google/android/maps/driveabout/vector/aX;FLcom/google/android/maps/driveabout/vector/aV;Z)V
    .registers 29
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 600
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move/from16 v5, p8

    move/from16 v6, p9

    move v7, p5

    move/from16 v8, p7

    move/from16 v9, p18

    invoke-direct/range {v1 .. v9}, LF/m;-><init>(Lo/n;Ly/b;Lo/aj;FFIZZ)V

    .line 179
    const/4 v1, 0x0

    iput-boolean v1, p0, LF/D;->w:Z

    .line 247
    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, LF/D;->M:[F

    .line 602
    iput-object p3, p0, LF/D;->l:Ljava/lang/String;

    .line 603
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "L"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LF/D;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LF/D;->K:Ljava/lang/String;

    .line 604
    move-object/from16 v0, p12

    iput-object v0, p0, LF/D;->n:Lo/X;

    .line 605
    mul-float v1, p16, p10

    iput v1, p0, LF/D;->p:F

    .line 606
    move/from16 v0, p6

    iput v0, p0, LF/D;->G:I

    .line 607
    move-object/from16 v0, p17

    iput-object v0, p0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    .line 608
    move/from16 v0, p10

    iput v0, p0, LF/D;->A:F

    .line 609
    move/from16 v0, p13

    iput v0, p0, LF/D;->B:F

    .line 610
    move/from16 v0, p14

    iput v0, p0, LF/D;->C:F

    .line 611
    move-object/from16 v0, p15

    iput-object v0, p0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    .line 615
    const/4 v1, 0x0

    iput-boolean v1, p0, LF/D;->J:Z

    .line 616
    move/from16 v0, p11

    iput v0, p0, LF/D;->E:I

    .line 618
    const/4 v1, 0x0

    iput v1, p0, LF/D;->I:I

    .line 619
    mul-float v1, p16, p13

    iput v1, p0, LF/D;->L:F

    .line 620
    return-void
.end method

.method static a(F[FI)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1255
    .line 1256
    aget v0, p1, p2

    sub-float v0, p0, v0

    .line 1257
    cmpg-float v1, v0, v3

    if-gtz v1, :cond_d

    .line 1272
    :cond_9
    :goto_9
    return p2

    .line 1270
    :cond_a
    add-int/lit8 p2, p2, 0x1

    move v0, v1

    .line 1260
    :cond_d
    cmpl-float v1, v0, v3

    if-lez v1, :cond_28

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge p2, v1, :cond_28

    .line 1261
    add-int/lit8 v1, p2, 0x1

    aget v1, p1, v1

    sub-float v1, p0, v1

    .line 1262
    cmpg-float v2, v1, v3

    if-gtz v2, :cond_a

    .line 1263
    neg-float v1, v1

    cmpg-float v0, v1, v0

    if-gez v0, :cond_9

    .line 1264
    add-int/lit8 p2, p2, 0x1

    goto :goto_9

    .line 1272
    :cond_28
    array-length v0, p1

    add-int/lit8 p2, v0, -0x1

    goto :goto_9
.end method

.method static a(Lo/X;)I
    .registers 4
    .parameter

    .prologue
    .line 1357
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 1358
    iget-object v1, v0, Lx/l;->a:Lo/T;

    .line 1359
    iget-object v0, v0, Lx/l;->b:Lo/T;

    .line 1362
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Lo/X;->a(ILo/T;)V

    .line 1363
    invoke-virtual {p0, v0}, Lo/X;->a(Lo/T;)V

    .line 1364
    invoke-static {v1, v0}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(Lo/aj;ZLcom/google/android/maps/driveabout/vector/q;)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 893
    if-eqz p1, :cond_2a

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_2a

    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    if-lez v0, :cond_2a

    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    const/4 v1, 0x2

    if-gt v0, v1, :cond_2a

    .line 903
    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    .line 904
    invoke-static {v0}, Lx/d;->a(I)I

    move-result v1

    const/16 v2, 0x80

    if-lt v1, v2, :cond_2a

    .line 908
    :goto_29
    return v0

    :cond_2a
    invoke-static {p0, p2}, LF/m;->b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    goto :goto_29
.end method

.method public static a(Lo/K;Ly/b;Lo/H;Lo/X;ZFLcom/google/android/maps/driveabout/vector/aX;FLC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 435
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p7

    move-object/from16 v8, p6

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v11}, LF/D;->a(Lo/n;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lo/af;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 356
    invoke-static/range {p0 .. p11}, LF/D;->a(Lo/n;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lo/n;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;
    .registers 34
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 522
    invoke-virtual/range {p9 .. p9}, LC/a;->y()F

    move-result v13

    .line 523
    invoke-virtual/range {p3 .. p3}, Lo/X;->b()I

    move-result v3

    .line 524
    const/4 v4, 0x2

    if-le v3, v4, :cond_9c

    .line 525
    mul-float v3, p6, v13

    .line 526
    const v4, 0x3e4ccccd

    mul-float/2addr v3, v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lo/X;->b(F)Lo/X;

    move-result-object v15

    .line 532
    :goto_17
    invoke-interface/range {p0 .. p0}, Lo/n;->e()Lo/aj;

    move-result-object v7

    .line 533
    const/4 v3, 0x0

    :goto_1c
    invoke-virtual/range {p2 .. p2}, Lo/H;->b()I

    move-result v4

    if-ge v3, v4, :cond_38

    .line 534
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lo/H;->a(I)Lo/I;

    move-result-object v4

    invoke-virtual {v4}, Lo/I;->a()Z

    move-result v4

    if-eqz v4, :cond_61

    .line 535
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lo/H;->a(I)Lo/I;

    move-result-object v3

    invoke-virtual {v3}, Lo/I;->j()Lo/aj;

    move-result-object v7

    .line 542
    :cond_38
    invoke-virtual/range {p2 .. p2}, Lo/H;->a()Ljava/lang/String;

    move-result-object v6

    .line 543
    if-eqz v7, :cond_64

    invoke-virtual {v7}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_42
    move-object/from16 v0, p10

    move-object/from16 v1, p8

    move/from16 v2, p6

    invoke-virtual {v0, v6, v1, v3, v2}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;F)F

    move-result v19

    .line 545
    const/high16 v3, 0x3f80

    add-float v3, v3, v19

    invoke-virtual/range {p9 .. p9}, LC/a;->o()F

    move-result v4

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v4}, LC/a;->a(FF)F

    move-result v3

    .line 546
    const/4 v4, 0x0

    cmpl-float v4, v19, v4

    if-nez v4, :cond_66

    .line 547
    const/4 v3, 0x0

    .line 578
    :cond_60
    :goto_60
    return-object v3

    .line 533
    :cond_61
    add-int/lit8 v3, v3, 0x1

    goto :goto_1c

    .line 543
    :cond_64
    const/4 v3, 0x0

    goto :goto_42

    .line 549
    :cond_66
    invoke-virtual {v15}, Lo/X;->d()F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_70

    .line 552
    const/4 v3, 0x0

    goto :goto_60

    .line 555
    :cond_70
    new-instance v3, LF/D;

    invoke-interface/range {p0 .. p0}, Lo/n;->i()I

    move-result v8

    const/high16 v11, -0x4080

    const/high16 v12, -0x4080

    invoke-virtual/range {p9 .. p9}, LC/a;->p()F

    move-result v4

    float-to-int v14, v4

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v16, p6

    move/from16 v17, p7

    move-object/from16 v18, p8

    move-object/from16 v20, p10

    move/from16 v21, p11

    invoke-direct/range {v3 .. v21}, LF/D;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/aj;IIZFFFILo/X;FFLcom/google/android/maps/driveabout/vector/aX;FLcom/google/android/maps/driveabout/vector/aV;Z)V

    .line 574
    invoke-virtual {v3}, LF/D;->a()Z

    move-result v4

    if-nez v4, :cond_60

    .line 576
    const/4 v3, 0x0

    goto :goto_60

    :cond_9c
    move-object/from16 v15, p3

    goto/16 :goto_17
.end method

.method static a(Lo/X;FF)Lo/X;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1280
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 1281
    iget-object v6, v0, Lx/l;->i:Lo/T;

    .line 1282
    iget-object v7, v0, Lx/l;->j:Lo/T;

    .line 1283
    iget-object v8, v0, Lx/l;->k:Lo/T;

    .line 1284
    iget-object v9, v0, Lx/l;->l:Lo/T;

    .line 1285
    const/4 v0, 0x0

    .line 1286
    const/4 v1, 0x0

    .line 1288
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v2

    .line 1289
    add-int/lit8 v10, v2, -0x1

    .line 1294
    const/4 v2, 0x0

    move v3, p2

    :goto_1a
    if-ge v2, v10, :cond_40

    .line 1295
    invoke-virtual {p0, v2}, Lo/X;->b(I)F

    move-result v4

    .line 1296
    sub-float/2addr p1, v4

    .line 1297
    const v5, 0x38d1b717

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_91

    .line 1299
    const v5, -0x472e48e9

    cmpg-float v5, p1, v5

    if-gez v5, :cond_40

    .line 1300
    const/4 v0, 0x1

    .line 1301
    const/high16 v5, 0x3f80

    div-float v4, p1, v4

    add-float/2addr v4, v5

    .line 1302
    invoke-virtual {p0, v2, v9}, Lo/X;->a(ILo/T;)V

    .line 1303
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v5, v8}, Lo/X;->a(ILo/T;)V

    .line 1304
    invoke-static {v9, v8, v4, v6}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    :cond_40
    move v4, v2

    .line 1314
    :goto_41
    if-ge v4, v10, :cond_ac

    .line 1315
    invoke-virtual {p0, v4}, Lo/X;->b(I)F

    move-result v11

    .line 1316
    sub-float v5, v3, v11

    .line 1317
    const v3, 0x38d1b717

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_95

    .line 1319
    const v3, 0x38d1b717

    cmpg-float v3, v5, v3

    if-gez v3, :cond_ac

    .line 1320
    const/4 v1, 0x1

    .line 1321
    const/high16 v3, 0x3f80

    div-float/2addr v5, v11

    add-float/2addr v3, v5

    .line 1322
    invoke-virtual {p0, v4, v9}, Lo/X;->a(ILo/T;)V

    .line 1323
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v5, v8}, Lo/X;->a(ILo/T;)V

    .line 1324
    invoke-static {v9, v8, v3, v7}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    move v3, v1

    .line 1331
    :goto_68
    add-int/lit8 v2, v2, 0x1

    .line 1333
    sub-int v1, v4, v2

    add-int/lit8 v5, v1, 0x1

    if-eqz v0, :cond_9a

    const/4 v1, 0x1

    :goto_71
    add-int/2addr v5, v1

    if-eqz v3, :cond_9c

    const/4 v1, 0x1

    :goto_75
    add-int/2addr v1, v5

    .line 1337
    mul-int/lit8 v1, v1, 0x3

    new-array v5, v1, [I

    .line 1338
    const/4 v1, 0x0

    .line 1339
    if-eqz v0, :cond_aa

    .line 1340
    const/4 v0, 0x1

    invoke-virtual {v6, v5, v1}, Lo/T;->a([II)V

    :goto_81
    move v1, v0

    move v0, v2

    .line 1342
    :goto_83
    if-gt v0, v4, :cond_9e

    .line 1343
    invoke-virtual {p0, v0, v8}, Lo/X;->a(ILo/T;)V

    .line 1344
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v8, v5, v1}, Lo/T;->a([II)V

    .line 1342
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_83

    .line 1308
    :cond_91
    sub-float/2addr v3, v4

    .line 1294
    add-int/lit8 v2, v2, 0x1

    goto :goto_1a

    .line 1314
    :cond_95
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_41

    .line 1333
    :cond_9a
    const/4 v1, 0x0

    goto :goto_71

    :cond_9c
    const/4 v1, 0x0

    goto :goto_75

    .line 1346
    :cond_9e
    if-eqz v3, :cond_a5

    .line 1347
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v7, v5, v1}, Lo/T;->a([II)V

    .line 1349
    :cond_a5
    invoke-static {v5}, Lo/X;->a([I)Lo/X;

    move-result-object v0

    return-object v0

    :cond_aa
    move v0, v1

    goto :goto_81

    :cond_ac
    move v3, v1

    goto :goto_68
.end method

.method private a(LD/a;LC/a;LF/F;F)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v6, 0x3f80

    const/4 v5, 0x0

    .line 862
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 865
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 866
    iget-object v2, v0, Lx/l;->a:Lo/T;

    .line 867
    iget-object v0, v0, Lx/l;->b:Lo/T;

    .line 868
    invoke-virtual {p2, v0}, LC/a;->a(Lo/T;)V

    .line 869
    iget-object v3, p3, LF/F;->a:Lo/T;

    invoke-static {v3, v0, v2}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 870
    invoke-virtual {p2}, LC/a;->w()F

    move-result v0

    .line 871
    invoke-virtual {v2}, Lo/T;->f()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    .line 872
    invoke-virtual {v2}, Lo/T;->g()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    .line 873
    invoke-virtual {v2}, Lo/T;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 874
    invoke-interface {v1, v3, v4, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 877
    const/high16 v2, 0x42b4

    iget v3, p3, LF/F;->d:F

    sub-float/2addr v2, v3

    invoke-interface {v1, v2, v5, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 880
    cmpl-float v2, p4, v5

    if-eqz v2, :cond_43

    .line 881
    invoke-interface {v1, p4, v6, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 885
    :cond_43
    iget v2, p3, LF/F;->b:F

    mul-float/2addr v2, v0

    .line 886
    iget v3, p3, LF/F;->c:F

    mul-float/2addr v0, v3

    .line 887
    invoke-interface {v1, v2, v0, v6}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 888
    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 889
    return-void
.end method

.method private a(LD/a;Lo/X;F)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 989
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 990
    iget-object v2, v0, Lx/l;->a:Lo/T;

    .line 991
    iget-object v1, v0, Lx/l;->b:Lo/T;

    .line 994
    invoke-virtual {p2}, Lo/X;->b()I

    move-result v6

    .line 995
    add-int/lit8 v3, v6, -0x1

    .line 996
    mul-int/lit8 v0, v3, 0x4

    .line 997
    const/4 v4, 0x1

    .line 998
    new-instance v5, LE/l;

    invoke-direct {v5, v0, v4}, LE/l;-><init>(IZ)V

    iput-object v5, p0, LF/D;->u:LE/i;

    .line 999
    new-instance v5, LE/l;

    invoke-direct {v5, v0, v4}, LE/l;-><init>(IZ)V

    iput-object v5, p0, LF/D;->v:LE/i;

    .line 1003
    new-array v0, v3, [LF/F;

    iput-object v0, p0, LF/D;->r:[LF/F;

    .line 1004
    new-array v7, v6, [F

    .line 1005
    const/4 v0, 0x0

    const/4 v4, 0x0

    aput v4, v7, v0

    .line 1006
    const/4 v0, 0x0

    invoke-virtual {p2, v0, v2}, Lo/X;->a(ILo/T;)V

    .line 1007
    const/4 v0, 0x0

    :goto_32
    if-ge v0, v3, :cond_54

    .line 1008
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p2, v4, v1}, Lo/X;->a(ILo/T;)V

    .line 1009
    iget-object v4, p0, LF/D;->r:[LF/F;

    new-instance v5, LF/F;

    const/4 v8, 0x0

    invoke-direct {v5, v2, v1, p3, v8}, LF/F;-><init>(Lo/T;Lo/T;FLF/E;)V

    aput-object v5, v4, v0

    .line 1011
    invoke-virtual {v2, v1}, Lo/T;->c(Lo/T;)F

    move-result v4

    .line 1012
    add-int/lit8 v5, v0, 0x1

    aget v8, v7, v0

    add-float/2addr v4, v8

    aput v4, v7, v5

    .line 1007
    add-int/lit8 v0, v0, 0x1

    move-object v10, v2

    move-object v2, v1

    move-object v1, v10

    goto :goto_32

    .line 1020
    :cond_54
    iget-object v0, p0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v1, p0, LF/D;->l:Ljava/lang/String;

    iget-object v2, p0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v3, p0, LF/D;->b:Lo/aj;

    if-eqz v3, :cond_80

    iget-object v3, p0, LF/D;->b:Lo/aj;

    invoke-virtual {v3}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    :goto_64
    iget v4, p0, LF/D;->B:F

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->b(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v3

    .line 1022
    const/high16 v0, 0x3f80

    array-length v1, v3

    add-int/lit8 v1, v1, -0x1

    aget v1, v3, v1

    div-float v1, v0, v1

    .line 1023
    const/4 v0, 0x0

    :goto_75
    array-length v2, v3

    if-ge v0, v2, :cond_82

    .line 1024
    aget v2, v3, v0

    mul-float/2addr v2, v1

    aput v2, v3, v0

    .line 1023
    add-int/lit8 v0, v0, 0x1

    goto :goto_75

    .line 1020
    :cond_80
    const/4 v3, 0x0

    goto :goto_64

    .line 1029
    :cond_82
    const/high16 v0, 0x3f80

    array-length v1, v7

    add-int/lit8 v1, v1, -0x1

    aget v1, v7, v1

    div-float v1, v0, v1

    .line 1030
    new-array v4, v6, [F

    .line 1031
    const/4 v0, 0x0

    :goto_8e
    if-ge v0, v6, :cond_a2

    .line 1032
    aget v2, v7, v0

    mul-float/2addr v2, v1

    aput v2, v7, v0

    .line 1033
    add-int/lit8 v2, v6, -0x1

    sub-int/2addr v2, v0

    const/high16 v5, 0x3f80

    aget v8, v7, v0

    sub-float/2addr v5, v8

    aput v5, v4, v2

    .line 1031
    add-int/lit8 v0, v0, 0x1

    goto :goto_8e

    .line 1038
    :cond_a2
    const/4 v2, 0x0

    .line 1039
    const/4 v1, 0x0

    .line 1040
    const/4 v0, 0x0

    :goto_a5
    if-ge v0, v6, :cond_be

    .line 1041
    aget v5, v7, v0

    invoke-static {v5, v3, v2}, LF/D;->a(F[FI)I

    move-result v2

    .line 1042
    aget v5, v3, v2

    aput v5, v7, v0

    .line 1043
    aget v5, v4, v0

    invoke-static {v5, v3, v1}, LF/D;->a(F[FI)I

    move-result v1

    .line 1045
    aget v5, v3, v1

    aput v5, v4, v0

    .line 1040
    add-int/lit8 v0, v0, 0x1

    goto :goto_a5

    .line 1051
    :cond_be
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v1

    .line 1052
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->c()F

    move-result v2

    .line 1053
    const/4 v0, 0x0

    :goto_cb
    if-ge v0, v6, :cond_f0

    .line 1054
    aget v3, v7, v0

    mul-float/2addr v3, v1

    .line 1055
    sub-int v5, v6, v0

    add-int/lit8 v5, v5, -0x1

    aget v5, v4, v5

    mul-float/2addr v5, v1

    .line 1056
    iget-object v8, p0, LF/D;->u:LE/i;

    const/4 v9, 0x0

    invoke-virtual {v8, v3, v9}, LE/i;->a(FF)V

    .line 1057
    iget-object v8, p0, LF/D;->u:LE/i;

    invoke-virtual {v8, v3, v2}, LE/i;->a(FF)V

    .line 1058
    iget-object v3, p0, LF/D;->v:LE/i;

    invoke-virtual {v3, v5, v2}, LE/i;->a(FF)V

    .line 1059
    iget-object v3, p0, LF/D;->v:LE/i;

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v8}, LE/i;->a(FF)V

    .line 1053
    add-int/lit8 v0, v0, 0x1

    goto :goto_cb

    .line 1061
    :cond_f0
    return-void
.end method

.method private b(F)V
    .registers 14
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v11, 0x0

    .line 1214
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 1215
    iget-object v2, v0, Lx/l;->a:Lo/T;

    .line 1216
    iget-object v3, v0, Lx/l;->b:Lo/T;

    .line 1217
    iget-object v4, v0, Lx/l;->c:Lo/T;

    .line 1218
    iget-object v5, v0, Lx/l;->d:Lo/T;

    .line 1220
    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v6

    .line 1221
    iget v0, p0, LF/D;->A:F

    iget v7, p0, LF/D;->B:F

    mul-float/2addr v0, v7

    mul-float/2addr v0, p1

    const/high16 v7, 0x4000

    div-float v7, v0, v7

    .line 1222
    mul-int/lit8 v0, v6, 0x2

    new-array v8, v0, [Lo/T;

    .line 1224
    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-virtual {v0, v11, v5}, Lo/X;->a(ILo/T;)V

    move v0, v1

    .line 1225
    :goto_2c
    if-ge v0, v6, :cond_62

    .line 1226
    iget-object v9, p0, LF/D;->o:Lo/X;

    invoke-virtual {v9, v0, v4}, Lo/X;->a(ILo/T;)V

    .line 1232
    invoke-static {v4, v5, v2}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 1233
    invoke-static {v2, v7, v3}, Lo/V;->a(Lo/T;FLo/T;)V

    .line 1234
    invoke-virtual {v4, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v9

    aput-object v9, v8, v0

    .line 1235
    mul-int/lit8 v9, v6, 0x2

    sub-int/2addr v9, v0

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v4, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v10

    aput-object v10, v8, v9

    .line 1237
    if-ne v0, v1, :cond_5c

    .line 1239
    invoke-virtual {v5, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v9

    aput-object v9, v8, v11

    .line 1240
    mul-int/lit8 v9, v6, 0x2

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v5, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v10

    aput-object v10, v8, v9

    .line 1242
    :cond_5c
    invoke-virtual {v5, v4}, Lo/T;->b(Lo/T;)V

    .line 1225
    add-int/lit8 v0, v0, 0x1

    goto :goto_2c

    .line 1244
    :cond_62
    new-instance v0, Lo/W;

    invoke-direct {v0, v8}, Lo/W;-><init>([Lo/T;)V

    iput-object v0, p0, LF/D;->q:Lo/W;

    .line 1245
    return-void
.end method

.method static b(Lo/X;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1372
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v2

    add-int/lit8 v3, v2, -0x1

    .line 1373
    const/4 v2, 0x2

    if-ge v3, v2, :cond_c

    .line 1386
    :cond_b
    :goto_b
    return v0

    .line 1377
    :cond_c
    invoke-virtual {p0, v0}, Lo/X;->d(I)F

    move-result v4

    move v2, v1

    .line 1378
    :goto_11
    if-ge v2, v3, :cond_b

    .line 1379
    invoke-virtual {p0, v2}, Lo/X;->d(I)F

    move-result v5

    .line 1380
    sub-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1381
    const/high16 v6, 0x4270

    cmpl-float v6, v5, v6

    if-lez v6, :cond_2a

    const/high16 v6, 0x4396

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2a

    move v0, v1

    .line 1383
    goto :goto_b

    .line 1378
    :cond_2a
    add-int/lit8 v2, v2, 0x1

    goto :goto_11
.end method

.method private d(LD/a;)V
    .registers 9
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 964
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->e()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LF/D;->A:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    mul-float/2addr v0, v1

    const/high16 v1, 0x3fc0

    div-float v1, v0, v1

    .line 967
    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_59

    .line 969
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 970
    iget-object v2, v0, Lx/l;->a:Lo/T;

    .line 971
    iget-object v0, v0, Lx/l;->b:Lo/T;

    .line 972
    iget-object v3, p0, LF/D;->o:Lo/X;

    invoke-virtual {v3, v6, v2}, Lo/X;->a(ILo/T;)V

    .line 973
    iget-object v3, p0, LF/D;->o:Lo/X;

    invoke-virtual {v3, v4, v0}, Lo/X;->a(ILo/T;)V

    .line 974
    new-array v3, v4, [LF/F;

    iput-object v3, p0, LF/D;->r:[LF/F;

    .line 975
    iget-object v3, p0, LF/D;->r:[LF/F;

    new-instance v4, LF/F;

    const/4 v5, 0x0

    invoke-direct {v4, v2, v0, v1, v5}, LF/F;-><init>(Lo/T;Lo/T;FLF/E;)V

    aput-object v4, v3, v6

    .line 977
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->b()F

    move-result v0

    .line 978
    iget-object v1, p0, LF/D;->t:LD/b;

    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    .line 979
    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v6

    iput v0, v2, LF/F;->e:F

    .line 980
    iget-object v0, p0, LF/D;->r:[LF/F;

    aget-object v0, v0, v6

    iput v1, v0, LF/F;->f:F

    .line 985
    :goto_58
    return-void

    .line 982
    :cond_59
    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-direct {p0, p1, v0, v1}, LF/D;->a(LD/a;Lo/X;F)V

    goto :goto_58
.end method

.method private e()Lo/X;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1113
    invoke-direct {p0}, LF/D;->h()Lo/X;

    move-result-object v1

    .line 1114
    invoke-virtual {v1}, Lo/X;->b()I

    move-result v2

    .line 1115
    const/4 v3, 0x2

    if-le v2, v3, :cond_1f

    .line 1117
    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    iget-object v3, p0, LF/D;->l:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v2, v3, :cond_19

    .line 1125
    :cond_18
    :goto_18
    return-object v0

    .line 1121
    :cond_19
    invoke-static {v1}, LF/D;->b(Lo/X;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_1f
    move-object v0, v1

    .line 1125
    goto :goto_18
.end method

.method private h()Lo/X;
    .registers 9

    .prologue
    const/high16 v4, 0x4000

    const/high16 v7, 0x3e80

    .line 1133
    iget-object v0, p0, LF/D;->n:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    .line 1134
    add-int/lit8 v2, v0, -0x1

    .line 1138
    iget v0, p0, LF/D;->I:I

    if-nez v0, :cond_58

    .line 1139
    const/4 v0, 0x0

    move v1, v0

    :goto_12
    if-ge v1, v2, :cond_55

    .line 1140
    iget-object v0, p0, LF/D;->n:Lo/X;

    invoke-virtual {v0, v1}, Lo/X;->b(I)F

    move-result v3

    .line 1141
    iget v0, p0, LF/D;->p:F

    cmpl-float v0, v3, v0

    if-lez v0, :cond_51

    .line 1142
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 1143
    iget-object v2, v0, Lx/l;->a:Lo/T;

    .line 1144
    iget-object v4, v0, Lx/l;->b:Lo/T;

    .line 1145
    iget-object v5, v0, Lx/l;->c:Lo/T;

    .line 1146
    iget-object v0, v0, Lx/l;->d:Lo/T;

    .line 1147
    iget-object v6, p0, LF/D;->n:Lo/X;

    invoke-virtual {v6, v1, v5}, Lo/X;->a(ILo/T;)V

    .line 1148
    iget-object v6, p0, LF/D;->n:Lo/X;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v6, v1, v0}, Lo/X;->a(ILo/T;)V

    .line 1153
    iget v1, p0, LF/D;->p:F

    sub-float v1, v3, v1

    div-float/2addr v1, v3

    .line 1154
    mul-float v3, v1, v7

    invoke-static {v5, v0, v3, v2}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    .line 1155
    const/high16 v3, 0x3f40

    mul-float/2addr v1, v3

    invoke-static {v0, v5, v1, v4}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    .line 1156
    invoke-static {v2, v4}, Lo/X;->a(Lo/T;Lo/T;)Lo/X;

    move-result-object v0

    .line 1206
    :goto_50
    return-object v0

    .line 1139
    :cond_51
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 1161
    :cond_55
    const/4 v0, 0x1

    iput v0, p0, LF/D;->I:I

    .line 1164
    :cond_58
    iget-object v0, p0, LF/D;->n:Lo/X;

    invoke-virtual {v0}, Lo/X;->d()F

    move-result v0

    .line 1165
    iget v1, p0, LF/D;->I:I

    packed-switch v1, :pswitch_data_c6

    .line 1206
    const/4 v0, 0x0

    goto :goto_50

    .line 1171
    :pswitch_65
    iget v1, p0, LF/D;->p:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v7

    .line 1172
    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    .line 1173
    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_50

    .line 1179
    :pswitch_73
    iget v1, p0, LF/D;->A:F

    mul-float/2addr v1, v4

    iget v2, p0, LF/D;->B:F

    mul-float/2addr v1, v2

    iget v2, p0, LF/D;->p:F

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1181
    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    .line 1182
    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_50

    .line 1188
    :pswitch_8a
    const/4 v1, 0x0

    iget v2, p0, LF/D;->p:F

    sub-float/2addr v0, v2

    iget v2, p0, LF/D;->A:F

    mul-float/2addr v2, v4

    iget v3, p0, LF/D;->B:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1190
    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    .line 1191
    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_50

    .line 1195
    :pswitch_a3
    iget v1, p0, LF/D;->p:F

    sub-float/2addr v0, v1

    const v1, 0x3ea8f5c3

    mul-float/2addr v0, v1

    .line 1196
    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    .line 1197
    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_50

    .line 1201
    :pswitch_b4
    iget v1, p0, LF/D;->p:F

    sub-float/2addr v0, v1

    const v1, 0x3f2b851f

    mul-float/2addr v0, v1

    .line 1202
    iget v1, p0, LF/D;->p:F

    add-float/2addr v1, v0

    .line 1203
    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-static {v2, v0, v1}, LF/D;->a(Lo/X;FF)Lo/X;

    move-result-object v0

    goto :goto_50

    .line 1165
    nop

    :pswitch_data_c6
    .packed-switch 0x1
        :pswitch_65
        :pswitch_73
        :pswitch_8a
        :pswitch_a3
        :pswitch_b4
    .end packed-switch
.end method


# virtual methods
.method a(LC/a;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1082
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 1083
    iget-object v1, v0, Lx/l;->a:Lo/T;

    .line 1084
    iget-object v0, v0, Lx/l;->b:Lo/T;

    .line 1085
    iget-object v2, p0, LF/D;->o:Lo/X;

    invoke-virtual {v2, v4, v1}, Lo/X;->a(ILo/T;)V

    .line 1086
    iget-object v2, p0, LF/D;->o:Lo/X;

    invoke-virtual {v2, v0}, Lo/X;->a(Lo/T;)V

    .line 1088
    iget-object v2, p0, LF/D;->M:[F

    invoke-virtual {p1, v1, v2}, LC/a;->a(Lo/T;[F)V

    .line 1089
    iget-object v1, p0, LF/D;->M:[F

    aget v1, v1, v4

    .line 1090
    iget-object v2, p0, LF/D;->M:[F

    aget v2, v2, v5

    .line 1092
    iget-object v3, p0, LF/D;->M:[F

    invoke-virtual {p1, v0, v3}, LC/a;->a(Lo/T;[F)V

    .line 1093
    iget-object v0, p0, LF/D;->M:[F

    aget v3, v0, v4

    sub-float v1, v3, v1

    aput v1, v0, v4

    .line 1094
    iget-object v0, p0, LF/D;->M:[F

    aget v1, v0, v5

    sub-float/2addr v1, v2

    aput v1, v0, v5

    .line 1095
    return-void
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 677
    invoke-super {p0, p1}, LF/m;->a(LD/a;)V

    .line 678
    iget-object v0, p0, LF/D;->t:LD/b;

    if-eqz v0, :cond_f

    .line 679
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 680
    const/4 v0, 0x0

    iput-object v0, p0, LF/D;->t:LD/b;

    .line 682
    :cond_f
    iget-object v0, p0, LF/D;->u:LE/i;

    if-eqz v0, :cond_1d

    .line 683
    iget-object v0, p0, LF/D;->u:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 684
    iget-object v0, p0, LF/D;->v:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 686
    :cond_1d
    iget-object v0, p0, LF/D;->y:LE/o;

    if-eqz v0, :cond_26

    .line 687
    iget-object v0, p0, LF/D;->y:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 689
    :cond_26
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v9, 0x4000

    const/4 v4, 0x0

    const/high16 v1, 0x3f80

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 740
    iget-boolean v0, p0, LF/D;->J:Z

    if-nez v0, :cond_16

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LF/D;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 854
    :cond_15
    :goto_15
    return-void

    .line 743
    :cond_16
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, LF/D;->r:[LF/F;

    array-length v0, v0

    if-lez v0, :cond_15

    .line 744
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    .line 745
    invoke-virtual {p1}, LD/a;->p()V

    .line 746
    iget-object v0, p0, LF/D;->r:[LF/F;

    array-length v0, v0

    if-ne v0, v3, :cond_12c

    .line 749
    iget-object v0, p1, LD/a;->e:LE/g;

    invoke-virtual {v0, p1}, LE/g;->a(LD/a;)V

    .line 756
    :goto_32
    iget-object v0, p0, LF/D;->t:LD/b;

    invoke-virtual {v0, v6}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 759
    iget-object v0, p0, LF/D;->z:Lh/e;

    if-eqz v0, :cond_133

    .line 760
    iget-object v0, p0, LF/D;->z:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    .line 761
    const/high16 v2, 0x1

    if-ne v0, v2, :cond_48

    .line 762
    const/4 v2, 0x0

    iput-object v2, p0, LF/D;->z:Lh/e;

    .line 768
    :cond_48
    :goto_48
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 771
    iget-object v0, p0, LF/D;->x:LE/i;

    if-eqz v0, :cond_63

    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_63

    invoke-virtual {p2}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_186

    .line 776
    :cond_63
    invoke-virtual {p0, p2}, LF/D;->a(LC/a;)V

    .line 777
    iget-object v0, p0, LF/D;->M:[F

    aget v0, v0, v4

    .line 778
    iget-object v2, p0, LF/D;->M:[F

    aget v7, v2, v3

    .line 779
    mul-float v2, v0, v0

    mul-float v8, v7, v7

    add-float/2addr v2, v8

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v8

    .line 780
    cmpl-float v0, v0, v5

    if-ltz v0, :cond_137

    move v2, v1

    .line 781
    :goto_7c
    cmpl-float v0, v7, v5

    if-ltz v0, :cond_13c

    move v0, v1

    .line 782
    :goto_81
    mul-float/2addr v7, v0

    div-float/2addr v7, v8

    sub-float v7, v1, v7

    mul-float/2addr v7, v2

    .line 789
    iget-object v2, p0, LF/D;->x:LE/i;

    if-nez v2, :cond_99

    .line 790
    cmpl-float v2, v0, v5

    if-lez v2, :cond_140

    iget-object v2, p0, LF/D;->v:LE/i;

    :goto_90
    iput-object v2, p0, LF/D;->x:LE/i;

    .line 791
    cmpl-float v0, v0, v5

    if-lez v0, :cond_144

    move v0, v3

    :goto_97
    iput-boolean v0, p0, LF/D;->w:Z

    .line 793
    :cond_99
    iget-object v0, p0, LF/D;->x:LE/i;

    iget-object v2, p0, LF/D;->v:LE/i;

    if-ne v0, v2, :cond_14e

    .line 794
    const v0, 0x3a83126f

    cmpg-float v0, v7, v0

    if-gez v0, :cond_147

    iget-object v0, p0, LF/D;->v:LE/i;

    :goto_a8
    iput-object v0, p0, LF/D;->x:LE/i;

    .line 796
    const v0, 0x3a83126f

    cmpg-float v0, v7, v0

    if-gez v0, :cond_14b

    move v0, v3

    :goto_b2
    iput-boolean v0, p0, LF/D;->w:Z

    .line 808
    :goto_b4
    iget-boolean v0, p0, LF/D;->H:Z

    if-nez v0, :cond_186

    const/high16 v0, 0x3f40

    cmpl-float v0, v7, v0

    if-gtz v0, :cond_c4

    const/high16 v0, -0x40c0

    cmpg-float v0, v7, v0

    if-gez v0, :cond_186

    .line 810
    :cond_c4
    invoke-virtual {p2}, LC/a;->q()F

    move-result v0

    mul-float/2addr v0, v7

    .line 815
    :goto_c9
    iget-object v2, p0, LF/D;->r:[LF/F;

    array-length v2, v2

    if-ge v4, v2, :cond_172

    .line 816
    iget-object v2, p0, LF/D;->r:[LF/F;

    array-length v2, v2

    if-ne v2, v3, :cond_16a

    .line 817
    const/16 v2, 0x1702

    invoke-interface {v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 818
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 819
    iget-boolean v2, p0, LF/D;->w:Z

    if-eqz v2, :cond_108

    .line 821
    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    iget v2, v2, LF/F;->e:F

    div-float/2addr v2, v9

    iget-object v7, p0, LF/D;->r:[LF/F;

    aget-object v7, v7, v4

    iget v7, v7, LF/F;->f:F

    div-float/2addr v7, v9

    invoke-interface {v6, v2, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 822
    const/high16 v2, 0x4334

    invoke-interface {v6, v2, v5, v5, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 823
    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    iget v2, v2, LF/F;->e:F

    neg-float v2, v2

    div-float/2addr v2, v9

    iget-object v7, p0, LF/D;->r:[LF/F;

    aget-object v7, v7, v4

    iget v7, v7, LF/F;->f:F

    neg-float v7, v7

    div-float/2addr v7, v9

    invoke-interface {v6, v2, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 826
    :cond_108
    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    iget v2, v2, LF/F;->e:F

    iget-object v7, p0, LF/D;->r:[LF/F;

    aget-object v7, v7, v4

    iget v7, v7, LF/F;->f:F

    invoke-interface {v6, v2, v7, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 827
    const/16 v2, 0x1700

    invoke-interface {v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 831
    :goto_11c
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 832
    iget-object v2, p0, LF/D;->r:[LF/F;

    aget-object v2, v2, v4

    invoke-direct {p0, p1, p2, v2, v0}, LF/D;->a(LD/a;LC/a;LF/F;F)V

    .line 833
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 815
    add-int/lit8 v4, v4, 0x1

    goto :goto_c9

    .line 753
    :cond_12c
    iget-object v0, p1, LD/a;->h:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    goto/16 :goto_32

    .line 766
    :cond_133
    iget v0, p0, LF/D;->k:I

    goto/16 :goto_48

    .line 780
    :cond_137
    const/high16 v0, -0x4080

    move v2, v0

    goto/16 :goto_7c

    .line 781
    :cond_13c
    const/high16 v0, -0x4080

    goto/16 :goto_81

    .line 790
    :cond_140
    iget-object v2, p0, LF/D;->u:LE/i;

    goto/16 :goto_90

    :cond_144
    move v0, v4

    .line 791
    goto/16 :goto_97

    .line 794
    :cond_147
    iget-object v0, p0, LF/D;->u:LE/i;

    goto/16 :goto_a8

    :cond_14b
    move v0, v4

    .line 796
    goto/16 :goto_b2

    .line 798
    :cond_14e
    const v0, -0x457ced91

    cmpg-float v0, v7, v0

    if-gez v0, :cond_165

    iget-object v0, p0, LF/D;->v:LE/i;

    :goto_157
    iput-object v0, p0, LF/D;->x:LE/i;

    .line 800
    const v0, -0x457ced91

    cmpg-float v0, v7, v0

    if-gez v0, :cond_168

    move v0, v3

    :goto_161
    iput-boolean v0, p0, LF/D;->w:Z

    goto/16 :goto_b4

    .line 798
    :cond_165
    iget-object v0, p0, LF/D;->u:LE/i;

    goto :goto_157

    :cond_168
    move v0, v4

    .line 800
    goto :goto_161

    .line 829
    :cond_16a
    iget-object v2, p0, LF/D;->x:LE/i;

    mul-int/lit8 v7, v4, 0x2

    invoke-virtual {v2, p1, v7}, LE/i;->a(LD/a;I)V

    goto :goto_11c

    .line 836
    :cond_172
    iget-object v0, p0, LF/D;->r:[LF/F;

    array-length v0, v0

    if-ne v0, v3, :cond_15

    .line 837
    const/16 v0, 0x1702

    invoke-interface {v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 838
    invoke-interface {v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 839
    const/16 v0, 0x1700

    invoke-interface {v6, v0}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_15

    :cond_186
    move v0, v5

    goto/16 :goto_c9
.end method

.method a()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 647
    :goto_2
    iget v2, p0, LF/D;->I:I

    const/4 v3, 0x6

    if-ge v2, v3, :cond_1a

    .line 648
    iget v2, p0, LF/D;->I:I

    if-le v2, v1, :cond_1b

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-virtual {v2}, Lo/X;->d()F

    move-result v2

    iget v3, p0, LF/D;->p:F

    const/high16 v4, 0x4000

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1b

    .line 667
    :cond_1a
    :goto_1a
    return v0

    .line 654
    :cond_1b
    iget v2, p0, LF/D;->I:I

    const/4 v3, 0x3

    if-le v2, v3, :cond_2f

    iget-object v2, p0, LF/D;->n:Lo/X;

    invoke-virtual {v2}, Lo/X;->d()F

    move-result v2

    iget v3, p0, LF/D;->p:F

    const/high16 v4, 0x4040

    mul-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1a

    .line 660
    :cond_2f
    invoke-direct {p0}, LF/D;->e()Lo/X;

    move-result-object v2

    iput-object v2, p0, LF/D;->o:Lo/X;

    .line 661
    iget-object v2, p0, LF/D;->o:Lo/X;

    if-eqz v2, :cond_48

    .line 662
    iget v0, p0, LF/D;->C:F

    invoke-direct {p0, v0}, LF/D;->b(F)V

    .line 663
    iget-object v0, p0, LF/D;->o:Lo/X;

    invoke-static {v0}, LF/D;->a(Lo/X;)I

    move-result v0

    iput v0, p0, LF/D;->D:I

    move v0, v1

    .line 664
    goto :goto_1a

    .line 647
    :cond_48
    iget v2, p0, LF/D;->I:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LF/D;->I:I

    goto :goto_2
.end method

.method public a(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 634
    iget v0, p0, LF/D;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LF/D;->I:I

    .line 635
    invoke-virtual {p0}, LF/D;->a()Z

    move-result v0

    return v0
.end method

.method a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z
    .registers 20
    .parameter
    .parameter

    .prologue
    .line 917
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->b:Lo/aj;

    move-object/from16 v0, p2

    invoke-static {v1, v0}, LF/D;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v6

    .line 918
    move-object/from16 v0, p0

    iget-object v2, v0, LF/D;->b:Lo/aj;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->a:Lo/n;

    invoke-interface {v1}, Lo/n;->h()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_59

    const/4 v1, 0x1

    :goto_1a
    move-object/from16 v0, p2

    invoke-static {v2, v1, v0}, LF/D;->a(Lo/aj;ZLcom/google/android/maps/driveabout/vector/q;)I

    move-result v7

    .line 920
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->b:Lo/aj;

    if-eqz v1, :cond_5b

    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->b:Lo/aj;

    invoke-virtual {v1}, Lo/aj;->h()Lo/ao;

    move-result-object v4

    .line 921
    :goto_2e
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/D;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget v5, v0, LF/D;->B:F

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/D;->t:LD/b;

    .line 923
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->t:LD/b;

    if-nez v1, :cond_7c

    .line 925
    const/16 v1, 0x2710

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, LD/a;->a(I)Z

    move-result v1

    if-nez v1, :cond_5d

    .line 926
    const/4 v1, 0x0

    .line 955
    :goto_58
    return v1

    .line 918
    :cond_59
    const/4 v1, 0x0

    goto :goto_1a

    .line 920
    :cond_5b
    const/4 v4, 0x0

    goto :goto_2e

    .line 928
    :cond_5d
    move-object/from16 v0, p0

    iget-object v8, v0, LF/D;->s:Lcom/google/android/maps/driveabout/vector/aV;

    move-object/from16 v0, p0

    iget-object v10, v0, LF/D;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, LF/D;->m:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget v13, v0, LF/D;->B:F

    const/16 v16, 0x0

    move-object/from16 v9, p1

    move-object v12, v4

    move v14, v6

    move v15, v7

    invoke-virtual/range {v8 .. v16}, Lcom/google/android/maps/driveabout/vector/aV;->a(LD/a;Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FIII)LD/b;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/D;->t:LD/b;

    .line 931
    :cond_7c
    invoke-direct/range {p0 .. p1}, LF/D;->d(LD/a;)V

    .line 938
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->r:[LF/F;

    array-length v1, v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_b6

    .line 939
    move-object/from16 v0, p0

    iget-object v1, v0, LF/D;->r:[LF/F;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, v1, LF/F;->d:F

    .line 940
    const/4 v1, 0x1

    :goto_91
    move-object/from16 v0, p0

    iget-object v3, v0, LF/D;->r:[LF/F;

    array-length v3, v3

    if-ge v1, v3, :cond_b6

    .line 941
    move-object/from16 v0, p0

    iget-object v3, v0, LF/D;->r:[LF/F;

    aget-object v3, v3, v1

    iget v3, v3, LF/F;->d:F

    sub-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 942
    const/high16 v4, 0x41f0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_d0

    const/high16 v4, 0x43a5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_d0

    .line 944
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LF/D;->H:Z

    .line 950
    :cond_b6
    move-object/from16 v0, p0

    iget-boolean v1, v0, LF/D;->h:Z

    if-eqz v1, :cond_c9

    .line 951
    new-instance v1, Lh/e;

    const-wide/16 v2, 0x1f4

    sget-object v4, Lh/g;->a:Lh/g;

    invoke-direct {v1, v2, v3, v4}, Lh/e;-><init>(JLh/g;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LF/D;->z:Lh/e;

    .line 954
    :cond_c9
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LF/D;->J:Z

    .line 955
    const/4 v1, 0x1

    goto :goto_58

    .line 940
    :cond_d0
    add-int/lit8 v1, v1, 0x1

    goto :goto_91
.end method

.method public a(Lo/aS;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1394
    iget-object v1, p0, LF/D;->o:Lo/X;

    invoke-virtual {v1, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v1, p0, LF/D;->o:Lo/X;

    invoke-virtual {v1}, Lo/X;->c()Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1}, Lo/aS;->a(Lo/T;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v0, 0x1

    :cond_1a
    return v0
.end method

.method b()I
    .registers 4

    .prologue
    .line 1102
    iget v0, p0, LF/D;->E:I

    iget v1, p0, LF/D;->D:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 1103
    iget v1, p0, LF/D;->G:I

    int-to-float v1, v1

    int-to-float v0, v0

    const v2, 0x3c8efa35

    mul-float/2addr v0, v2

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 693
    invoke-super {p0, p1}, LF/m;->b(LD/a;)V

    .line 694
    iget-object v0, p0, LF/D;->u:LE/i;

    if-eqz v0, :cond_11

    .line 695
    iget-object v0, p0, LF/D;->u:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 696
    iget-object v0, p0, LF/D;->v:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 698
    :cond_11
    iget-object v0, p0, LF/D;->y:LE/o;

    if-eqz v0, :cond_1a

    .line 699
    iget-object v0, p0, LF/D;->y:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 701
    :cond_1a
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 709
    invoke-virtual {p1}, LC/a;->y()F

    move-result v0

    iget v3, p0, LF/D;->A:F

    div-float v3, v0, v3

    .line 711
    invoke-virtual {p0}, LF/D;->l()Z

    move-result v0

    if-eqz v0, :cond_39

    iget-boolean v0, p0, LF/D;->i:Z

    if-eqz v0, :cond_39

    .line 712
    const/high16 v0, 0x3e80

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_37

    const/high16 v0, 0x4000

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_37

    move v0, v1

    .line 714
    :goto_21
    invoke-static {v3}, LF/D;->a(F)I

    move-result v3

    iput v3, p0, LF/D;->k:I

    .line 722
    :goto_27
    if-eqz v0, :cond_4e

    .line 723
    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LF/D;->E:I

    .line 724
    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    iput v0, p0, LF/D;->F:F

    .line 727
    :goto_36
    return v1

    :cond_37
    move v0, v2

    .line 712
    goto :goto_21

    .line 717
    :cond_39
    const v0, 0x3f666666

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_4c

    const/high16 v0, 0x3fa0

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_4c

    move v0, v1

    .line 719
    :goto_47
    const/high16 v3, 0x1

    iput v3, p0, LF/D;->k:I

    goto :goto_27

    :cond_4c
    move v0, v2

    .line 717
    goto :goto_47

    :cond_4e
    move v1, v2

    .line 727
    goto :goto_36
.end method

.method public m()F
    .registers 2

    .prologue
    .line 624
    iget v0, p0, LF/D;->L:F

    return v0
.end method

.method public n()Lo/ae;
    .registers 2

    .prologue
    .line 672
    iget-object v0, p0, LF/D;->q:Lo/W;

    return-object v0
.end method

.method public t()I
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 732
    iget v2, p0, LF/D;->f:I

    iget v0, p0, LF/D;->I:I

    if-nez v0, :cond_13

    const/4 v0, 0x1

    :goto_8
    add-int/2addr v0, v2

    iget v2, p0, LF/D;->F:F

    const/high16 v3, 0x41f0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_15

    :goto_11
    add-int/2addr v0, v1

    return v0

    :cond_13
    move v0, v1

    goto :goto_8

    :cond_15
    invoke-virtual {p0}, LF/D;->b()I

    move-result v1

    goto :goto_11
.end method

.method public u()Ljava/lang/String;
    .registers 2

    .prologue
    .line 629
    iget-object v0, p0, LF/D;->K:Ljava/lang/String;

    return-object v0
.end method
