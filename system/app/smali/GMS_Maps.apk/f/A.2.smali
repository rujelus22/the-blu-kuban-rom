.class public LF/A;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lo/X;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/HashSet;

.field private final e:[Ljava/lang/String;

.field private f:Lo/o;


# direct methods
.method private constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LF/A;->b:Ljava/util/ArrayList;

    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LF/A;->c:Ljava/util/ArrayList;

    .line 282
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LF/A;->d:Ljava/util/HashSet;

    .line 295
    iput-object v2, p0, LF/A;->a:Lo/X;

    .line 296
    iput-object v2, p0, LF/A;->e:[Ljava/lang/String;

    .line 297
    return-void
.end method

.method public constructor <init>(Lo/K;[Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LF/A;->b:Ljava/util/ArrayList;

    .line 281
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LF/A;->c:Ljava/util/ArrayList;

    .line 282
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LF/A;->d:Ljava/util/HashSet;

    .line 289
    invoke-virtual {p1}, Lo/K;->b()Lo/X;

    move-result-object v0

    iput-object v0, p0, LF/A;->a:Lo/X;

    .line 290
    iput-object p2, p0, LF/A;->e:[Ljava/lang/String;

    .line 291
    invoke-virtual {p0, p1}, LF/A;->a(Lo/K;)V

    .line 292
    return-void
.end method

.method static a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 377
    mul-int v0, p0, p1

    invoke-static {p0, p1}, LF/A;->b(II)I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method static synthetic a(LF/A;)Lo/X;
    .registers 2
    .parameter

    .prologue
    .line 271
    iget-object v0, p0, LF/A;->a:Lo/X;

    return-object v0
.end method

.method static b(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 386
    if-lez p0, :cond_4

    if-gtz p1, :cond_c

    .line 387
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Greatest common divisor should be computed on numbers greater than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_c
    :goto_c
    if-eqz p1, :cond_13

    .line 394
    rem-int v0, p0, p1

    move p0, p1

    move p1, v0

    .line 395
    goto :goto_c

    .line 397
    :cond_13
    return p0
.end method


# virtual methods
.method public a()Lo/X;
    .registers 2

    .prologue
    .line 344
    iget-object v0, p0, LF/A;->a:Lo/X;

    return-object v0
.end method

.method public a(Lo/K;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 321
    invoke-virtual {p1}, Lo/K;->l()[I

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_8
    if-ge v0, v3, :cond_1f

    aget v4, v2, v0

    .line 322
    if-ltz v4, :cond_1c

    iget-object v5, p0, LF/A;->e:[Ljava/lang/String;

    array-length v5, v5

    if-ge v4, v5, :cond_1c

    .line 323
    iget-object v5, p0, LF/A;->d:Ljava/util/HashSet;

    iget-object v6, p0, LF/A;->e:[Ljava/lang/String;

    aget-object v4, v6, v4

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 321
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 326
    :cond_1f
    invoke-virtual {p1}, Lo/K;->e()Lo/aj;

    move-result-object v0

    .line 327
    invoke-virtual {p1}, Lo/K;->d()F

    move-result v2

    .line 331
    invoke-virtual {v0}, Lo/aj;->b()I

    move-result v3

    if-ne v3, v7, :cond_44

    .line 332
    iget-object v3, p0, LF/A;->c:Ljava/util/ArrayList;

    new-instance v4, LF/B;

    invoke-direct {v4, v2, v0, v1}, LF/B;-><init>(FLo/aj;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    :cond_37
    :goto_37
    invoke-virtual {p1}, Lo/K;->f()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 339
    invoke-virtual {p1}, Lo/K;->a()Lo/o;

    move-result-object v0

    iput-object v0, p0, LF/A;->f:Lo/o;

    .line 341
    :cond_43
    return-void

    .line 333
    :cond_44
    invoke-virtual {v0}, Lo/aj;->b()I

    move-result v3

    if-le v3, v7, :cond_37

    .line 334
    iget-object v3, p0, LF/A;->b:Ljava/util/ArrayList;

    new-instance v4, LF/B;

    invoke-direct {v4, v2, v0, v1}, LF/B;-><init>(FLo/aj;I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    iget-object v1, p0, LF/A;->c:Ljava/util/ArrayList;

    new-instance v3, LF/B;

    invoke-direct {v3, v2, v0, v7}, LF/B;-><init>(FLo/aj;I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_37
.end method

.method public b()Ljava/util/Set;
    .registers 2

    .prologue
    .line 348
    iget-object v0, p0, LF/A;->d:Ljava/util/HashSet;

    return-object v0
.end method

.method public c()Lo/o;
    .registers 2

    .prologue
    .line 352
    iget-object v0, p0, LF/A;->f:Lo/o;

    return-object v0
.end method

.method public d()F
    .registers 6

    .prologue
    const/high16 v4, 0x4000

    .line 357
    const/4 v0, 0x0

    .line 358
    iget-object v1, p0, LF/A;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/B;

    .line 359
    iget v3, v0, LF/B;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, v0, LF/B;->b:F

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    .line 360
    cmpl-float v3, v0, v1

    if-lez v3, :cond_4b

    :goto_24
    move v1, v0

    .line 363
    goto :goto_a

    .line 364
    :cond_26
    iget-object v0, p0, LF/A;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2c
    :goto_2c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/B;

    .line 365
    iget v3, v0, LF/B;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, v0, LF/B;->b:F

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    .line 366
    cmpl-float v3, v0, v1

    if-lez v3, :cond_2c

    move v1, v0

    .line 367
    goto :goto_2c

    .line 370
    :cond_48
    mul-float v0, v1, v4

    return v0

    :cond_4b
    move v0, v1

    goto :goto_24
.end method

.method public e()I
    .registers 13

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 407
    iget-object v0, p0, LF/A;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p0, LF/A;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int v7, v0, v2

    .line 410
    if-nez v7, :cond_13

    .line 447
    :cond_12
    return v1

    .line 414
    :cond_13
    new-array v8, v7, [I

    move v6, v4

    .line 417
    :goto_16
    if-ge v6, v7, :cond_5d

    .line 419
    iget-object v0, p0, LF/A;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_33

    .line 420
    iget-object v0, p0, LF/A;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/B;

    .line 426
    :goto_28
    iget-object v2, v0, LF/B;->d:[I

    if-nez v2, :cond_44

    move v0, v1

    .line 440
    :goto_2d
    aput v0, v8, v6

    .line 417
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_16

    .line 422
    :cond_33
    iget-object v0, p0, LF/A;->c:Ljava/util/ArrayList;

    iget-object v2, p0, LF/A;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v2, v6, v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/B;

    goto :goto_28

    .line 429
    :cond_44
    iget-object v9, v0, LF/B;->d:[I

    array-length v10, v9

    move v3, v4

    move v2, v4

    :goto_49
    if-ge v3, v10, :cond_53

    aget v5, v9, v3

    .line 430
    add-int/2addr v5, v2

    .line 429
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v5

    goto :goto_49

    .line 436
    :cond_53
    iget-object v0, v0, LF/B;->d:[I

    array-length v0, v0

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_6f

    .line 437
    mul-int/lit8 v0, v2, 0x2

    goto :goto_2d

    .line 443
    :cond_5d
    aget v0, v8, v4

    move v11, v1

    move v1, v0

    move v0, v11

    .line 444
    :goto_62
    if-ge v0, v7, :cond_12

    .line 445
    aget v2, v8, v0

    invoke-static {v1, v2}, LF/A;->a(II)I

    move-result v2

    .line 444
    add-int/lit8 v1, v0, 0x1

    move v0, v1

    move v1, v2

    goto :goto_62

    :cond_6f
    move v0, v2

    goto :goto_2d
.end method

.method public f()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 451
    iget-object v0, p0, LF/A;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public g()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 455
    iget-object v0, p0, LF/A;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public h()I
    .registers 4

    .prologue
    .line 463
    const/16 v0, 0xd0

    .line 464
    iget-object v1, p0, LF/A;->a:Lo/X;

    if-eqz v1, :cond_d

    .line 465
    iget-object v1, p0, LF/A;->a:Lo/X;

    invoke-virtual {v1}, Lo/X;->h()I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_d
    iget-object v1, p0, LF/A;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v2, p0, LF/A;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x18

    add-int/2addr v0, v1

    .line 468
    return v0
.end method
