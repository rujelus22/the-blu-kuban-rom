.class public LF/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field private final b:Lo/aq;

.field private final c:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lo/aq;[Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 476
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LF/y;->a:Ljava/util/ArrayList;

    .line 481
    iput-object p1, p0, LF/y;->b:Lo/aq;

    .line 482
    iput-object p2, p0, LF/y;->c:[Ljava/lang/String;

    .line 483
    return-void
.end method


# virtual methods
.method public a(Lo/aO;)V
    .registers 6
    .parameter

    .prologue
    .line 490
    const/4 v1, 0x0

    .line 495
    :goto_1
    invoke-interface {p1}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 496
    invoke-interface {p1}, Lo/aO;->b()Lo/n;

    move-result-object v0

    .line 497
    instance-of v2, v0, Lo/K;

    if-nez v2, :cond_10

    .line 511
    :cond_f
    return-void

    .line 500
    :cond_10
    check-cast v0, Lo/K;

    .line 501
    if-eqz v1, :cond_2b

    invoke-static {v1}, LF/A;->a(LF/A;)Lo/X;

    move-result-object v2

    invoke-virtual {v0}, Lo/K;->b()Lo/X;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/X;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 503
    invoke-virtual {v1, v0}, LF/A;->a(Lo/K;)V

    move-object v0, v1

    .line 509
    :goto_26
    invoke-interface {p1}, Lo/aO;->next()Ljava/lang/Object;

    move-object v1, v0

    .line 510
    goto :goto_1

    .line 506
    :cond_2b
    new-instance v1, LF/A;

    iget-object v2, p0, LF/y;->c:[Ljava/lang/String;

    invoke-direct {v1, v0, v2}, LF/A;-><init>(Lo/K;[Ljava/lang/String;)V

    .line 507
    iget-object v0, p0, LF/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_26
.end method

.method public a(Lo/n;)V
    .registers 5
    .parameter

    .prologue
    .line 517
    instance-of v0, p1, Lo/K;

    if-eqz v0, :cond_12

    .line 518
    iget-object v0, p0, LF/y;->a:Ljava/util/ArrayList;

    new-instance v1, LF/A;

    check-cast p1, Lo/K;

    iget-object v2, p0, LF/y;->c:[Ljava/lang/String;

    invoke-direct {v1, p1, v2}, LF/A;-><init>(Lo/K;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 520
    :cond_12
    return-void
.end method

.method public a(I)[LF/w;
    .registers 21
    .parameter

    .prologue
    .line 527
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 529
    move-object/from16 v0, p0

    iget-object v1, v0, LF/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 530
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v14

    .line 531
    new-instance v17, LF/z;

    invoke-direct/range {v17 .. v17}, LF/z;-><init>()V

    .line 532
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v13

    .line 533
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v12

    .line 534
    new-instance v18, LF/z;

    invoke-direct/range {v18 .. v18}, LF/z;-><init>()V

    .line 535
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v11

    .line 537
    const/4 v9, 0x0

    .line 554
    :goto_28
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_109

    .line 555
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, LF/A;

    .line 557
    invoke-virtual {v8}, LF/A;->c()Lo/o;

    move-result-object v1

    if-eqz v1, :cond_69

    .line 558
    new-instance v3, LF/z;

    invoke-direct {v3}, LF/z;-><init>()V

    .line 559
    invoke-virtual {v8}, LF/A;->a()Lo/X;

    move-result-object v1

    invoke-static {v1, v3}, LF/w;->a(Lo/X;LF/z;)Z

    .line 560
    new-instance v1, LF/w;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/y;->b:Lo/aq;

    invoke-static {v8}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v8}, LF/A;->b()Ljava/util/Set;

    move-result-object v5

    const/4 v7, 0x0

    move/from16 v6, p1

    invoke-direct/range {v1 .. v7}, LF/w;-><init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;ILF/x;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v9

    move-object v2, v11

    move-object v3, v12

    move-object v4, v13

    move-object v5, v14

    :goto_63
    move-object v9, v1

    move-object v11, v2

    move-object v12, v3

    move-object v13, v4

    move-object v14, v5

    .line 603
    goto :goto_28

    .line 570
    :cond_69
    invoke-virtual {v8}, LF/A;->e()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_c5

    .line 571
    new-instance v10, LF/C;

    invoke-direct {v10, v8}, LF/C;-><init>(LF/A;)V

    .line 574
    if-eqz v9, :cond_c3

    invoke-virtual {v10, v9}, LF/C;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c3

    const/4 v1, 0x1

    .line 577
    :goto_7e
    invoke-virtual {v8}, LF/A;->a()Lo/X;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-static {v2, v0}, LF/w;->a(Lo/X;LF/z;)Z

    move-result v2

    if-eqz v2, :cond_8c

    if-eqz v1, :cond_153

    .line 581
    :cond_8c
    new-instance v1, LF/w;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/y;->b:Lo/aq;

    const/4 v7, 0x0

    move-object/from16 v3, v18

    move-object v4, v12

    move-object v5, v11

    move/from16 v6, p1

    invoke-direct/range {v1 .. v7}, LF/w;-><init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;ILF/x;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 584
    invoke-virtual/range {v18 .. v18}, LF/z;->a()V

    .line 585
    invoke-virtual {v8}, LF/A;->a()Lo/X;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-static {v1, v0}, LF/w;->a(Lo/X;LF/z;)Z

    .line 586
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 588
    :goto_b3
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 589
    invoke-virtual {v8}, LF/A;->b()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object v3, v2

    move-object v4, v13

    move-object v5, v14

    move-object v2, v1

    move-object v1, v10

    .line 591
    goto :goto_63

    .line 574
    :cond_c3
    const/4 v1, 0x0

    goto :goto_7e

    .line 592
    :cond_c5
    invoke-virtual {v8}, LF/A;->a()Lo/X;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-static {v1, v0}, LF/w;->a(Lo/X;LF/z;)Z

    move-result v1

    if-nez v1, :cond_150

    .line 593
    new-instance v1, LF/w;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/y;->b:Lo/aq;

    const/4 v7, 0x0

    move-object/from16 v3, v17

    move-object v4, v14

    move-object v5, v13

    move/from16 v6, p1

    invoke-direct/range {v1 .. v7}, LF/w;-><init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;ILF/x;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 596
    invoke-virtual/range {v17 .. v17}, LF/z;->a()V

    .line 597
    invoke-virtual {v8}, LF/A;->a()Lo/X;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-static {v1, v0}, LF/w;->a(Lo/X;LF/z;)Z

    .line 598
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 600
    :goto_f8
    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601
    invoke-virtual {v8}, LF/A;->b()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    move-object v3, v12

    move-object v4, v1

    move-object v5, v2

    move-object v2, v11

    move-object v1, v9

    goto/16 :goto_63

    .line 604
    :cond_109
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_122

    .line 605
    new-instance v1, LF/w;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/y;->b:Lo/aq;

    const/4 v7, 0x0

    move-object/from16 v3, v17

    move-object v4, v14

    move-object v5, v13

    move/from16 v6, p1

    invoke-direct/range {v1 .. v7}, LF/w;-><init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;ILF/x;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    :cond_122
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13b

    .line 608
    new-instance v1, LF/w;

    move-object/from16 v0, p0

    iget-object v2, v0, LF/y;->b:Lo/aq;

    const/4 v7, 0x0

    move-object/from16 v3, v18

    move-object v4, v12

    move-object v5, v11

    move/from16 v6, p1

    invoke-direct/range {v1 .. v7}, LF/w;-><init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;ILF/x;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    :cond_13b
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_143

    .line 612
    const/4 v1, 0x0

    .line 614
    :goto_142
    return-object v1

    :cond_143
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/w;

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/w;

    goto :goto_142

    :cond_150
    move-object v1, v13

    move-object v2, v14

    goto :goto_f8

    :cond_153
    move-object v1, v11

    move-object v2, v12

    goto/16 :goto_b3
.end method
