.class public LF/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/h;


# instance fields
.field private a:LF/s;

.field private final b:LF/v;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:LE/q;

.field private final f:LE/k;

.field private final g:Lx/j;

.field private final h:LE/g;

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field private m:Z

.field private n:Z


# direct methods
.method constructor <init>(Ljava/util/ArrayList;LF/s;LF/v;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    iput-object p1, p0, LF/o;->c:Ljava/util/ArrayList;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    .line 177
    new-instance v0, Lx/j;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Lx/j;-><init>(IIZ)V

    iput-object v0, p0, LF/o;->g:Lx/j;

    .line 180
    new-instance v0, LE/g;

    iget-object v1, p0, LF/o;->g:Lx/j;

    invoke-virtual {v1}, Lx/j;->d()I

    move-result v1

    invoke-direct {v0, v1}, LE/g;-><init>(I)V

    iput-object v0, p0, LF/o;->h:LE/g;

    .line 181
    iget-object v0, p0, LF/o;->g:Lx/j;

    iput-object v0, p0, LF/o;->e:LE/q;

    .line 182
    iget-object v0, p0, LF/o;->g:Lx/j;

    iput-object v0, p0, LF/o;->f:LE/k;

    .line 183
    iput-object p2, p0, LF/o;->a:LF/s;

    .line 184
    iput-object p3, p0, LF/o;->b:LF/v;

    .line 185
    invoke-direct {p0}, LF/o;->d()V

    .line 186
    iput-boolean v3, p0, LF/o;->m:Z

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/o;->n:Z

    .line 188
    return-void
.end method

.method public static a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 202
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 204
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    const/4 v0, 0x0

    move v6, v0

    move-object v7, v1

    :goto_11
    invoke-virtual {p0}, Lo/H;->b()I

    move-result v0

    if-ge v6, v0, :cond_c8

    .line 206
    invoke-virtual {p0, v6}, Lo/H;->a(I)Lo/I;

    move-result-object v1

    .line 207
    invoke-virtual {v1}, Lo/I;->a()Z

    move-result v0

    if-eqz v0, :cond_59

    .line 208
    invoke-virtual {v1}, Lo/I;->j()Lo/aj;

    move-result-object v4

    .line 209
    invoke-virtual {v1}, Lo/I;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4, p5, v0, v4, p2}, LF/o;->a(Lcom/google/android/maps/driveabout/vector/aV;LG/a;Ljava/lang/String;Lo/aj;LC/a;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 210
    iget-object v5, p5, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    .line 211
    instance-of v0, p1, Lo/af;

    if-eqz v0, :cond_52

    .line 212
    iget-object v5, p5, LG/a;->a:Lcom/google/android/maps/driveabout/vector/aX;

    .line 216
    :cond_37
    :goto_37
    new-instance v0, LF/t;

    invoke-virtual {v1}, Lo/I;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, LC/a;->m()F

    move-result v1

    invoke-static {v4, p5, v1}, LF/L;->a(Lo/aj;LG/a;F)I

    move-result v3

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4c
    move-object v1, v7

    .line 205
    :goto_4d
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object v7, v1

    goto :goto_11

    .line 213
    :cond_52
    instance-of v0, p1, Lo/K;

    if-eqz v0, :cond_37

    .line 214
    iget-object v5, p5, LG/a;->h:Lcom/google/android/maps/driveabout/vector/aX;

    goto :goto_37

    .line 220
    :cond_59
    invoke-virtual {v1}, Lo/I;->b()Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 221
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v0

    invoke-virtual {v1}, Lo/I;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v8}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Lv/a;->b()Z

    move-result v2

    if-eqz v2, :cond_a2

    .line 224
    invoke-virtual {v0}, Lv/a;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 225
    invoke-virtual {v1}, Lo/I;->h()F

    move-result v0

    .line 226
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->x()I

    .line 228
    invoke-virtual {v1}, Lo/I;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/road_shields/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9e

    .line 232
    iget v1, p5, LG/a;->m:F

    mul-float/2addr v0, v1

    .line 237
    :goto_8f
    invoke-virtual {p2}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    .line 238
    new-instance v1, LF/q;

    invoke-direct {v1, v2, v0, p3}, LF/q;-><init>(Landroid/graphics/Bitmap;FLD/c;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    .line 243
    goto :goto_4d

    .line 235
    :cond_9e
    iget v1, p5, LG/a;->n:F

    mul-float/2addr v0, v1

    goto :goto_8f

    :cond_a2
    move-object v0, v8

    .line 250
    :goto_a3
    return-object v0

    .line 243
    :cond_a4
    invoke-virtual {v1}, Lo/I;->e()Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 244
    new-instance v0, LF/r;

    invoke-virtual {v1}, Lo/I;->k()F

    move-result v1

    invoke-direct {v0, v1}, LF/r;-><init>(F)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_4d

    .line 245
    :cond_b8
    invoke-virtual {v1}, Lo/I;->f()Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 246
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 247
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v7

    goto :goto_4d

    .line 250
    :cond_c8
    new-instance v0, LF/o;

    invoke-virtual {p0}, Lo/H;->c()Lo/b;

    move-result-object v1

    invoke-virtual {v1}, Lo/b;->a()I

    move-result v1

    invoke-static {v1}, LF/s;->a(I)LF/s;

    move-result-object v1

    invoke-virtual {p0}, Lo/H;->c()Lo/b;

    move-result-object v2

    invoke-virtual {v2}, Lo/b;->b()I

    move-result v2

    invoke-static {v2}, LF/v;->a(I)LF/v;

    move-result-object v2

    invoke-direct {v0, v9, v1, v2}, LF/o;-><init>(Ljava/util/ArrayList;LF/s;LF/v;)V

    goto :goto_a3

    :cond_e6
    move-object v1, v7

    goto/16 :goto_4d
.end method

.method static a(Lcom/google/android/maps/driveabout/vector/aV;LG/a;Ljava/lang/String;Lo/aj;LC/a;)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 583
    if-eqz p2, :cond_b

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_b

    if-nez p0, :cond_c

    .line 588
    :cond_b
    :goto_b
    return v0

    .line 586
    :cond_c
    invoke-virtual {p3}, Lo/aj;->h()Lo/ao;

    move-result-object v1

    invoke-virtual {v1}, Lo/ao;->d()I

    move-result v1

    .line 587
    invoke-virtual {p3}, Lo/aj;->h()Lo/ao;

    move-result-object v2

    invoke-virtual {v2}, Lo/ao;->f()I

    move-result v2

    .line 588
    if-lez v2, :cond_b

    const/high16 v2, -0x100

    and-int/2addr v1, v2

    if-eqz v1, :cond_b

    const/4 v0, 0x1

    goto :goto_b
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/q;)Z
    .registers 6
    .parameter

    .prologue
    .line 728
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 729
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 731
    :cond_10
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 732
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_26
    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 733
    instance-of v3, v0, LF/r;

    if-nez v3, :cond_26

    .line 734
    invoke-interface {v0, p1}, LF/u;->a(Lcom/google/android/maps/driveabout/vector/q;)LD/b;

    move-result-object v0

    .line 735
    if-nez v0, :cond_59

    .line 736
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_42
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_52

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 737
    invoke-virtual {v0}, LD/b;->g()V

    goto :goto_42

    .line 739
    :cond_52
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 740
    const/4 v0, 0x0

    .line 746
    :goto_58
    return v0

    .line 742
    :cond_59
    iget-object v3, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_26

    .line 746
    :cond_5f
    const/4 v0, 0x1

    goto :goto_58
.end method

.method private c(LD/a;)V
    .registers 15
    .parameter

    .prologue
    .line 760
    iget-object v0, p0, LF/o;->g:Lx/j;

    invoke-virtual {v0}, Lx/j;->f()V

    .line 761
    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->b(LD/a;)V

    .line 767
    iget-object v0, p0, LF/o;->g:Lx/j;

    iget-object v1, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    invoke-virtual {v0, v1}, Lx/j;->a(I)V

    .line 768
    const/4 v2, 0x0

    .line 769
    iget v0, p0, LF/o;->j:F

    iget v1, p0, LF/o;->k:F

    sub-float v1, v0, v1

    .line 770
    const/4 v0, 0x0

    move v3, v1

    move v4, v2

    move v2, v0

    :goto_22
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_111

    .line 771
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 774
    const/4 v5, 0x0

    .line 775
    const/4 v1, 0x0

    .line 776
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v5

    move v5, v1

    :goto_3a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_55

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/u;

    .line 777
    invoke-interface {v1}, LF/u;->e()F

    move-result v8

    .line 778
    invoke-static {v6, v8}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 779
    invoke-interface {v1}, LF/u;->a()F

    move-result v1

    add-float/2addr v1, v5

    move v5, v1

    .line 780
    goto :goto_3a

    .line 781
    :cond_55
    const/4 v1, 0x0

    .line 782
    iget-object v7, p0, LF/o;->a:LF/s;

    sget-object v8, LF/s;->a:LF/s;

    if-ne v7, v8, :cond_7f

    .line 784
    iget v1, p0, LF/o;->i:F

    sub-float/2addr v1, v5

    const/high16 v5, 0x4000

    div-float/2addr v1, v5

    .line 788
    :cond_62
    :goto_62
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v1

    :goto_67
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_109

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 789
    instance-of v1, v0, LF/r;

    if-eqz v1, :cond_89

    .line 791
    invoke-interface {v0}, LF/u;->a()F

    move-result v0

    add-float v1, v5, v0

    move v5, v1

    .line 793
    goto :goto_67

    .line 785
    :cond_7f
    iget-object v7, p0, LF/o;->a:LF/s;

    sget-object v8, LF/s;->c:LF/s;

    if-ne v7, v8, :cond_62

    .line 786
    iget v1, p0, LF/o;->i:F

    sub-float/2addr v1, v5

    goto :goto_62

    .line 796
    :cond_89
    invoke-interface {v0}, LF/u;->a()F

    move-result v7

    .line 797
    invoke-interface {v0}, LF/u;->b()F

    move-result v9

    .line 800
    iget-object v1, p0, LF/o;->b:LF/v;

    sget-object v10, LF/v;->a:LF/v;

    if-ne v1, v10, :cond_fa

    .line 802
    invoke-interface {v0}, LF/u;->e()F

    move-result v1

    sub-float v1, v6, v1

    const/high16 v10, 0x4000

    div-float/2addr v1, v10

    sub-float v1, v3, v1

    .line 807
    :goto_a2
    invoke-interface {v0}, LF/u;->c()F

    move-result v10

    add-float/2addr v1, v10

    .line 808
    iget-object v10, p0, LF/o;->e:LE/q;

    const/4 v11, 0x0

    sub-float v12, v1, v9

    invoke-interface {v10, v5, v11, v12}, LE/q;->a(FFF)V

    .line 809
    iget-object v10, p0, LF/o;->e:LE/q;

    add-float v11, v5, v7

    const/4 v12, 0x0

    sub-float v9, v1, v9

    invoke-interface {v10, v11, v12, v9}, LE/q;->a(FFF)V

    .line 810
    iget-object v9, p0, LF/o;->e:LE/q;

    add-float/2addr v7, v5

    const/4 v10, 0x0

    invoke-interface {v9, v7, v10, v1}, LE/q;->a(FFF)V

    .line 811
    iget-object v7, p0, LF/o;->e:LE/q;

    const/4 v9, 0x0

    invoke-interface {v7, v5, v9, v1}, LE/q;->a(FFF)V

    .line 813
    iget-object v1, p0, LF/o;->d:Ljava/util/ArrayList;

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LD/b;

    .line 814
    invoke-virtual {v1}, LD/b;->b()F

    move-result v4

    .line 815
    invoke-virtual {v1}, LD/b;->c()F

    move-result v1

    .line 816
    iget-object v9, p0, LF/o;->f:LE/k;

    const/4 v10, 0x0

    invoke-interface {v9, v10, v1}, LE/k;->a(FF)V

    .line 817
    iget-object v9, p0, LF/o;->f:LE/k;

    invoke-interface {v9, v4, v1}, LE/k;->a(FF)V

    .line 818
    iget-object v1, p0, LF/o;->f:LE/k;

    const/4 v9, 0x0

    invoke-interface {v1, v4, v9}, LE/k;->a(FF)V

    .line 819
    iget-object v1, p0, LF/o;->f:LE/k;

    const/4 v4, 0x0

    const/4 v9, 0x0

    invoke-interface {v1, v4, v9}, LE/k;->a(FF)V

    .line 822
    invoke-interface {v0}, LF/u;->a()F

    move-result v0

    add-float v1, v5, v0

    move v5, v1

    move v4, v7

    .line 823
    goto/16 :goto_67

    .line 803
    :cond_fa
    iget-object v1, p0, LF/o;->b:LF/v;

    sget-object v10, LF/v;->c:LF/v;

    if-ne v1, v10, :cond_125

    .line 804
    invoke-interface {v0}, LF/u;->e()F

    move-result v1

    sub-float v1, v6, v1

    sub-float v1, v3, v1

    goto :goto_a2

    .line 825
    :cond_109
    sub-float v1, v3, v6

    .line 770
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto/16 :goto_22

    .line 827
    :cond_111
    const/4 v0, 0x0

    iput-boolean v0, p0, LF/o;->n:Z

    .line 828
    iget-object v0, p0, LF/o;->g:Lx/j;

    invoke-virtual {v0}, Lx/j;->c()V

    .line 829
    iget-object v0, p0, LF/o;->h:LE/g;

    iget-object v1, p0, LF/o;->g:Lx/j;

    invoke-virtual {v1}, Lx/j;->e()Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v0, v1}, LE/g;->a(Ljava/nio/ByteBuffer;)V

    .line 830
    return-void

    :cond_125
    move v1, v3

    goto/16 :goto_a2
.end method

.method private d()V
    .registers 11

    .prologue
    const/4 v2, 0x0

    const/high16 v9, 0x4000

    const/4 v4, 0x0

    .line 256
    iput v4, p0, LF/o;->i:F

    move v1, v2

    move v3, v4

    .line 258
    :goto_8
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_46

    .line 259
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 262
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v5, v4

    move v6, v4

    :goto_1e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 263
    invoke-interface {v0}, LF/u;->a()F

    move-result v8

    add-float/2addr v6, v8

    .line 264
    invoke-interface {v0}, LF/u;->e()F

    move-result v0

    .line 265
    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v5, v0

    .line 266
    goto :goto_1e

    .line 267
    :cond_39
    iget v0, p0, LF/o;->i:F

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/o;->i:F

    .line 268
    add-float/2addr v3, v5

    .line 258
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    .line 272
    :cond_46
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 273
    iget-object v1, p0, LF/o;->c:Ljava/util/ArrayList;

    iget-object v2, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 274
    iput v4, p0, LF/o;->k:F

    .line 275
    iput v4, p0, LF/o;->l:F

    .line 278
    sget-object v2, LF/p;->a:[I

    iget-object v5, p0, LF/o;->b:LF/v;

    invoke-virtual {v5}, LF/v;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_166

    .line 330
    :cond_6f
    :goto_6f
    iget v0, p0, LF/o;->k:F

    add-float/2addr v0, v3

    iget v1, p0, LF/o;->l:F

    add-float/2addr v0, v1

    iput v0, p0, LF/o;->j:F

    .line 331
    return-void

    .line 281
    :pswitch_78
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 282
    iget v5, p0, LF/o;->k:F

    invoke-interface {v0}, LF/u;->c()F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/o;->k:F

    goto :goto_7c

    .line 285
    :cond_95
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_9a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 286
    invoke-interface {v0}, LF/u;->e()F

    move-result v5

    .line 287
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 288
    invoke-interface {v0}, LF/u;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_9a

    .line 290
    :cond_b8
    cmpl-float v0, v4, v1

    if-lez v0, :cond_6f

    .line 291
    sub-float v0, v4, v1

    iput v0, p0, LF/o;->l:F

    goto :goto_6f

    .line 296
    :pswitch_c1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v4

    :goto_c6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 297
    invoke-interface {v0}, LF/u;->e()F

    move-result v6

    .line 298
    invoke-static {v2, v6}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 299
    invoke-interface {v0}, LF/u;->c()F

    move-result v0

    add-float/2addr v0, v6

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_c6

    .line 301
    :cond_e4
    cmpl-float v0, v4, v2

    if-lez v0, :cond_ec

    .line 302
    sub-float v0, v4, v2

    iput v0, p0, LF/o;->k:F

    .line 305
    :cond_ec
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 306
    iget v2, p0, LF/o;->l:F

    invoke-interface {v0}, LF/u;->d()F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/o;->l:F

    goto :goto_f0

    .line 310
    :pswitch_109
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v4

    move v5, v4

    :goto_10f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 311
    invoke-interface {v0}, LF/u;->e()F

    move-result v7

    div-float/2addr v7, v9

    .line 312
    invoke-static {v5, v7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 313
    invoke-interface {v0}, LF/u;->c()F

    move-result v0

    add-float/2addr v0, v7

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v2, v0

    .line 314
    goto :goto_10f

    .line 315
    :cond_12f
    cmpl-float v0, v2, v5

    if-lez v0, :cond_137

    .line 316
    sub-float v0, v2, v5

    iput v0, p0, LF/o;->k:F

    .line 320
    :cond_137
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v4

    :goto_13c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 321
    invoke-interface {v0}, LF/u;->e()F

    move-result v5

    div-float/2addr v5, v9

    .line 322
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 323
    invoke-interface {v0}, LF/u;->d()F

    move-result v0

    add-float/2addr v0, v5

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_13c

    .line 325
    :cond_15b
    cmpl-float v0, v4, v1

    if-lez v0, :cond_6f

    .line 326
    sub-float v0, v4, v1

    iput v0, p0, LF/o;->l:F

    goto/16 :goto_6f

    .line 278
    nop

    :pswitch_data_166
    .packed-switch 0x1
        :pswitch_78
        :pswitch_c1
        :pswitch_109
    .end packed-switch
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 362
    iget v0, p0, LF/o;->i:F

    return v0
.end method

.method public a(LD/a;)V
    .registers 6
    .parameter

    .prologue
    .line 338
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 339
    invoke-virtual {v0}, LD/b;->g()V

    goto :goto_6

    .line 341
    :cond_16
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 342
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 343
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_31
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_41

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/u;

    .line 344
    invoke-interface {v1}, LF/u;->f()V

    goto :goto_31

    .line 346
    :cond_41
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_21

    .line 348
    :cond_45
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 349
    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->d(LD/a;)V

    .line 350
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 680
    iget-boolean v0, p0, LF/o;->m:Z

    if-nez v0, :cond_f

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LF/o;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 699
    :cond_e
    return-void

    .line 683
    :cond_f
    iget-boolean v0, p0, LF/o;->n:Z

    if-eqz v0, :cond_16

    .line 684
    invoke-direct {p0, p1}, LF/o;->c(LD/a;)V

    .line 687
    :cond_16
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    .line 691
    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0}, LE/g;->a()I

    move-result v0

    if-eqz v0, :cond_e

    .line 694
    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->a(LD/a;)V

    .line 695
    const/4 v0, 0x0

    move v1, v0

    :goto_29
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    .line 696
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    invoke-virtual {v0, v2}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 697
    const/4 v0, 0x6

    mul-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    invoke-interface {v2, v0, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 695
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_29
.end method

.method public a(LF/s;)V
    .registers 3
    .parameter

    .prologue
    .line 375
    iget-object v0, p0, LF/o;->a:LF/s;

    if-eq v0, p1, :cond_7

    .line 376
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/o;->n:Z

    .line 378
    :cond_7
    iput-object p1, p0, LF/o;->a:LF/s;

    .line 379
    return-void
.end method

.method a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 702
    invoke-direct {p0, p2}, LF/o;->a(Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_51

    .line 703
    const/16 v0, 0x2710

    invoke-virtual {p1, v0}, LD/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_11

    .line 704
    const/4 v0, 0x0

    .line 719
    :goto_10
    return v0

    .line 706
    :cond_11
    iget-object v0, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_21

    .line 707
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "this.textureArray should be empty on initialize."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 709
    :cond_21
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 710
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_37
    :goto_37
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/u;

    .line 711
    instance-of v4, v0, LF/r;

    if-nez v4, :cond_37

    .line 712
    iget-object v4, p0, LF/o;->d:Ljava/util/ArrayList;

    invoke-interface {v0, p1, p2}, LF/u;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)LD/b;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_37

    .line 717
    :cond_51
    invoke-direct {p0, p1}, LF/o;->c(LD/a;)V

    .line 718
    iput-boolean v1, p0, LF/o;->m:Z

    move v0, v1

    .line 719
    goto :goto_10
.end method

.method public b()F
    .registers 2

    .prologue
    .line 366
    iget v0, p0, LF/o;->j:F

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 357
    invoke-virtual {p0, p1}, LF/o;->a(LD/a;)V

    .line 358
    iget-object v0, p0, LF/o;->h:LE/g;

    invoke-virtual {v0, p1}, LE/g;->c(LD/a;)V

    .line 359
    return-void
.end method

.method public c()Z
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 371
    iget-object v0, p0, LF/o;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    invoke-virtual {p0}, LF/o;->a()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_19

    invoke-virtual {p0}, LF/o;->b()F

    move-result v0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1b

    :cond_19
    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method
