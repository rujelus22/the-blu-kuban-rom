.class public LF/L;
.super LF/m;
.source "SourceFile"


# static fields
.field private static final K:[LF/O;

.field private static final L:[LF/O;


# instance fields
.field private A:F

.field private B:Z

.field private final C:[LF/O;

.field private D:I

.field private E:Lh/e;

.field private F:Z

.field private G:F

.field private final H:F

.field private final I:[F

.field private J:Ljava/lang/String;

.field protected l:Lo/a;

.field protected m:LF/o;

.field protected n:LF/o;

.field protected o:LF/O;

.field protected p:F

.field protected q:F

.field protected r:F

.field protected s:F

.field private t:Lo/a;

.field private u:Lo/t;

.field private v:Lo/t;

.field private w:LE/o;

.field private final x:Ljava/lang/String;

.field private final y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x0

    .line 186
    new-array v0, v4, [LF/O;

    sput-object v0, LF/L;->K:[LF/O;

    .line 190
    const/4 v0, 0x4

    new-array v0, v0, [LF/O;

    new-instance v1, LF/O;

    sget-object v2, LF/N;->d:LF/N;

    sget-object v3, LF/s;->a:LF/s;

    invoke-direct {v1, v2, v3}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v1, v0, v4

    const/4 v1, 0x1

    new-instance v2, LF/O;

    sget-object v3, LF/N;->b:LF/N;

    sget-object v4, LF/s;->a:LF/s;

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, LF/O;

    sget-object v3, LF/N;->e:LF/N;

    sget-object v4, LF/s;->c:LF/s;

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, LF/O;

    sget-object v3, LF/N;->c:LF/N;

    sget-object v4, LF/s;->b:LF/s;

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v0, v1

    sput-object v0, LF/L;->L:[LF/O;

    return-void
.end method

.method constructor <init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V
    .registers 24
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 481
    invoke-interface {p1}, Lo/n;->e()Lo/aj;

    move-result-object v4

    invoke-interface {p1}, Lo/n;->i()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v8, p8

    move/from16 v9, p13

    invoke-direct/range {v1 .. v9}, LF/m;-><init>(Lo/n;Ly/b;Lo/aj;FFIZZ)V

    .line 104
    const/high16 v1, -0x4080

    iput v1, p0, LF/L;->A:F

    .line 134
    const/16 v1, 0x8

    new-array v1, v1, [F

    iput-object v1, p0, LF/L;->I:[F

    .line 483
    iput-object p3, p0, LF/L;->x:Ljava/lang/String;

    .line 484
    iput-object p4, p0, LF/L;->l:Lo/a;

    .line 485
    iput-object p5, p0, LF/L;->t:Lo/a;

    .line 486
    move-object/from16 v0, p10

    iput-object v0, p0, LF/L;->m:LF/o;

    .line 487
    move-object/from16 v0, p11

    iput-object v0, p0, LF/L;->n:LF/o;

    .line 488
    move/from16 v0, p9

    iput-boolean v0, p0, LF/L;->y:Z

    .line 491
    iget-object v1, p0, LF/L;->n:LF/o;

    if-nez v1, :cond_38

    sget-object p12, LF/L;->K:[LF/O;

    :cond_38
    move-object/from16 v0, p12

    iput-object v0, p0, LF/L;->C:[LF/O;

    .line 493
    const/4 v1, 0x0

    iput v1, p0, LF/L;->D:I

    .line 494
    iget-object v1, p0, LF/L;->n:LF/o;

    if-eqz v1, :cond_54

    .line 495
    iget-object v1, p0, LF/L;->C:[LF/O;

    iget v2, p0, LF/L;->D:I

    aget-object v1, v1, v2

    iput-object v1, p0, LF/L;->o:LF/O;

    .line 496
    iget-object v1, p0, LF/L;->n:LF/o;

    iget-object v2, p0, LF/L;->o:LF/O;

    iget-object v2, v2, LF/O;->b:LF/s;

    invoke-virtual {v1, v2}, LF/o;->a(LF/s;)V

    .line 499
    :cond_54
    const/4 v1, 0x0

    iput-boolean v1, p0, LF/L;->B:Z

    .line 501
    const/4 v1, 0x0

    .line 502
    if-eqz p10, :cond_64

    .line 503
    invoke-virtual/range {p10 .. p10}, LF/o;->a()F

    move-result v2

    invoke-virtual/range {p10 .. p10}, LF/o;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 505
    :cond_64
    if-eqz p11, :cond_70

    .line 506
    invoke-virtual/range {p11 .. p11}, LF/o;->a()F

    move-result v2

    invoke-virtual/range {p11 .. p11}, LF/o;->b()F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 508
    :cond_70
    iput v1, p0, LF/L;->H:F

    .line 509
    return-void
.end method

.method private a(LC/a;)F
    .registers 5
    .parameter

    .prologue
    const/high16 v2, 0x3f80

    .line 716
    const/4 v0, 0x1

    .line 717
    iget-boolean v1, p0, LF/L;->y:Z

    if-eqz v1, :cond_16

    .line 718
    iget-object v1, p0, LF/L;->l:Lo/a;

    invoke-virtual {v1}, Lo/a;->b()Lo/T;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, LC/a;->a(Lo/T;Z)F

    move-result v0

    invoke-virtual {p1, v2, v0}, LC/a;->a(FF)F

    move-result v0

    .line 721
    :goto_15
    return v0

    :cond_16
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    invoke-virtual {p1, v2, v0}, LC/a;->a(FF)F

    move-result v0

    goto :goto_15
.end method

.method static a(Lo/aj;LG/a;F)I
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 946
    iget v0, p1, LG/a;->e:F

    iget v1, p1, LG/a;->f:I

    iget v2, p1, LG/a;->g:I

    invoke-static {p0, v0, v1, v2, p2}, LF/m;->a(Lo/aj;FIIF)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static a(LF/T;Lcom/google/android/maps/driveabout/vector/aV;LC/a;)LF/L;
    .registers 23
    .parameter
    .parameter
    .parameter

    .prologue
    .line 416
    invoke-interface/range {p0 .. p0}, LF/T;->c()LA/c;

    move-result-object v7

    .line 417
    invoke-virtual {v7}, LA/c;->i()Z

    move-result v1

    if-nez v1, :cond_c

    .line 418
    const/4 v1, 0x0

    .line 464
    :goto_b
    return-object v1

    .line 421
    :cond_c
    invoke-interface/range {p0 .. p0}, LF/T;->b()Lo/aq;

    move-result-object v8

    .line 422
    invoke-virtual {v8}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-virtual {v7, v1}, LA/c;->a(Lo/ad;)Lo/T;

    move-result-object v9

    .line 423
    invoke-virtual {v7}, LA/c;->j()LF/O;

    move-result-object v10

    .line 424
    invoke-virtual {v7}, LA/c;->k()Lo/aj;

    move-result-object v5

    .line 426
    const/4 v1, 0x1

    new-array v14, v1, [LF/O;

    const/4 v1, 0x0

    aput-object v10, v14, v1

    .line 427
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 428
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 429
    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    sget-object v15, LG/a;->t:LG/a;

    .line 432
    new-instance v13, LF/r;

    const/high16 v1, 0x4080

    invoke-direct {v13, v1}, LF/r;-><init>(F)V

    .line 433
    invoke-virtual/range {p2 .. p2}, LC/a;->m()F

    move-result v1

    invoke-static {v5, v15, v1}, LF/L;->a(Lo/aj;LG/a;F)I

    move-result v4

    .line 435
    new-instance v1, LF/t;

    invoke-virtual {v8}, Lo/aq;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    new-instance v1, LF/t;

    invoke-virtual {v8}, Lo/aq;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    new-instance v1, LF/t;

    invoke-virtual {v8}, Lo/aq;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 445
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    new-instance v1, LF/t;

    invoke-virtual {v7}, LA/c;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    invoke-interface/range {p0 .. p0}, LF/T;->d()I

    move-result v1

    if-lez v1, :cond_c0

    .line 450
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 451
    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    new-instance v1, LF/t;

    invoke-interface/range {p0 .. p0}, LF/T;->d()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v6, v15, LG/a;->d:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v6}, LF/t;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Ljava/lang/String;ILo/aj;Lcom/google/android/maps/driveabout/vector/aX;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    :cond_c0
    new-instance v13, LF/o;

    iget-object v1, v10, LF/O;->b:LF/s;

    sget-object v2, LF/v;->c:LF/v;

    invoke-direct {v13, v11, v1, v2}, LF/o;-><init>(Ljava/util/ArrayList;LF/s;LF/v;)V

    .line 457
    const/4 v3, 0x1

    .line 458
    const/4 v4, 0x0

    .line 459
    new-instance v16, LF/L;

    new-instance v17, Lo/l;

    const/4 v1, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Lo/l;-><init>(Lo/aj;)V

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v1, Lo/a;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v1 .. v8}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    const/4 v7, 0x0

    const/high16 v8, -0x4080

    const/high16 v9, -0x4080

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-boolean v15, v15, LG/a;->q:Z

    move-object/from16 v2, v16

    move-object/from16 v3, v17

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    move-object v6, v1

    invoke-direct/range {v2 .. v15}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    .line 463
    const/4 v1, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v1}, LF/L;->b(Z)V

    move-object/from16 v1, v16

    .line 464
    goto/16 :goto_b
.end method

.method public static a(Lo/U;Ly/b;ZLC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;LZ/a;)LF/L;
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v1

    move-object v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, LF/o;->a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;

    move-result-object v11

    .line 226
    if-eqz v11, :cond_19

    invoke-virtual {v11}, LF/o;->c()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 227
    :cond_19
    const/4 v1, 0x0

    .line 305
    :cond_1a
    :goto_1a
    return-object v1

    .line 229
    :cond_1b
    const/4 v12, 0x0

    .line 230
    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v1

    if-eqz v1, :cond_3e

    .line 231
    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v1

    move-object v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, LF/o;->a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;

    move-result-object v12

    .line 233
    if-nez v12, :cond_37

    .line 234
    const/4 v1, 0x0

    goto :goto_1a

    .line 236
    :cond_37
    invoke-virtual {v12}, LF/o;->c()Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 237
    const/4 v12, 0x0

    .line 241
    :cond_3e
    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v1

    array-length v1, v1

    if-nez v1, :cond_cf

    if-eqz v12, :cond_cf

    .line 243
    sget-object v13, LF/L;->L:[LF/O;

    .line 254
    :cond_49
    if-eqz v12, :cond_4f

    array-length v1, v13

    const/4 v2, 0x1

    if-ne v1, v2, :cond_100

    :cond_4f
    const/4 v10, 0x1

    .line 258
    :goto_50
    invoke-virtual {p0}, Lo/U;->o()[Lo/a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v5, v1, v2

    .line 260
    invoke-virtual {v5}, Lo/a;->b()Lo/T;

    move-result-object v1

    invoke-static {p0, v1}, LF/L;->a(Lo/U;Lo/T;)Ljava/lang/String;

    move-result-object v4

    .line 262
    invoke-virtual {p0}, Lo/U;->t()Z

    move-result v1

    if-eqz v1, :cond_103

    invoke-virtual {p0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_103

    move-object/from16 v0, p6

    iget-boolean v1, v0, LG/a;->p:Z

    if-eqz v1, :cond_103

    const/4 v1, 0x1

    .line 264
    :goto_76
    if-eqz v1, :cond_106

    .line 265
    new-instance v1, LF/l;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lo/U;->k()F

    move-result v7

    invoke-virtual {p0}, Lo/U;->n()F

    move-result v8

    move-object/from16 v0, p6

    iget-boolean v14, v0, LG/a;->q:Z

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v9, p2

    invoke-direct/range {v1 .. v14}, LF/l;-><init>(Lo/U;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    .line 293
    :goto_8f
    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 294
    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v2

    invoke-virtual {v2}, Lo/H;->a()Ljava/lang/String;

    move-result-object v3

    .line 295
    if-eqz v12, :cond_125

    .line 296
    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v2

    invoke-virtual {v2}, Lo/H;->a()Ljava/lang/String;

    move-result-object v2

    .line 297
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_11f

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_11f

    .line 298
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 303
    :cond_ca
    :goto_ca
    invoke-direct {v1, v2}, LF/L;->a(Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 245
    :cond_cf
    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v1

    array-length v1, v1

    new-array v13, v1, [LF/O;

    .line 246
    const/4 v1, 0x0

    :goto_d7
    array-length v2, v13

    if-ge v1, v2, :cond_49

    .line 247
    new-instance v2, LF/O;

    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lo/c;->d()I

    move-result v3

    invoke-static {v3}, LF/N;->a(I)LF/N;

    move-result-object v3

    invoke-virtual {p0}, Lo/U;->r()[Lo/c;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lo/c;->a()I

    move-result v4

    invoke-static {v4}, LF/s;->a(I)LF/s;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LF/O;-><init>(LF/N;LF/s;)V

    aput-object v2, v13, v1

    .line 246
    add-int/lit8 v1, v1, 0x1

    goto :goto_d7

    .line 254
    :cond_100
    const/4 v10, 0x0

    goto/16 :goto_50

    .line 262
    :cond_103
    const/4 v1, 0x0

    goto/16 :goto_76

    .line 270
    :cond_106
    new-instance v1, LF/L;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lo/U;->k()F

    move-result v7

    invoke-virtual {p0}, Lo/U;->n()F

    move-result v8

    move-object/from16 v0, p6

    iget-boolean v14, v0, LG/a;->q:Z

    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v9, p2

    invoke-direct/range {v1 .. v14}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    goto/16 :goto_8f

    .line 299
    :cond_11f
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_ca

    :cond_125
    move-object v2, v3

    goto :goto_ca
.end method

.method public static a(Lo/af;ILy/b;Lo/T;Lo/T;ZLG/a;LC/a;LD/c;)LF/L;
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 372
    invoke-virtual/range {p0 .. p1}, Lo/af;->c(I)Lo/H;

    move-result-object v1

    .line 373
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 374
    const/4 v2, 0x0

    :goto_a
    invoke-virtual {v1}, Lo/H;->b()I

    move-result v3

    if-ge v2, v3, :cond_2c

    .line 375
    invoke-virtual {v1, v2}, Lo/H;->a(I)Lo/I;

    move-result-object v3

    .line 376
    invoke-virtual {v3}, Lo/I;->a()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 377
    const/4 v1, 0x0

    .line 391
    :goto_1b
    return-object v1

    .line 379
    :cond_1c
    invoke-virtual {v3}, Lo/I;->b()Z

    move-result v4

    if-eqz v4, :cond_29

    .line 380
    invoke-virtual {v3}, Lo/I;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    :cond_29
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 383
    :cond_2c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-nez v2, :cond_34

    .line 384
    const/4 v1, 0x0

    goto :goto_1b

    .line 386
    :cond_34
    const/4 v5, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p7

    move-object/from16 v4, p8

    move-object/from16 v6, p6

    invoke-static/range {v1 .. v6}, LF/o;->a(Lo/H;Lo/n;LC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;)LF/o;

    move-result-object v12

    .line 388
    if-nez v12, :cond_45

    .line 389
    const/4 v1, 0x0

    goto :goto_1b

    .line 391
    :cond_45
    new-instance v16, LF/L;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    new-instance v1, Lo/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v8}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    if-nez p4, :cond_77

    const/4 v7, 0x0

    :goto_5b
    const/high16 v8, -0x4080

    const/high16 v9, -0x4080

    const/4 v11, 0x0

    const/4 v13, 0x0

    sget-object v14, LF/L;->K:[LF/O;

    move-object/from16 v0, p6

    iget-boolean v15, v0, LG/a;->q:Z

    move-object/from16 v2, v16

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object v5, v10

    move-object v6, v1

    move/from16 v10, p5

    invoke-direct/range {v2 .. v15}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    move-object/from16 v1, v16

    goto :goto_1b

    :cond_77
    new-instance v2, Lo/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p4

    invoke-direct/range {v2 .. v9}, Lo/a;-><init>(Lo/T;IFLo/T;FFF)V

    move-object v7, v2

    goto :goto_5b
.end method

.method private static a(Lo/U;Lo/T;)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 317
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 318
    :goto_7
    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v3

    invoke-virtual {v3}, Lo/H;->b()I

    move-result v3

    if-ge v0, v3, :cond_29

    .line 319
    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v3

    invoke-virtual {v3, v0}, Lo/H;->a(I)Lo/I;

    move-result-object v3

    .line 320
    invoke-virtual {v3}, Lo/I;->b()Z

    move-result v4

    if-eqz v4, :cond_26

    .line 321
    invoke-virtual {v3}, Lo/I;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    :cond_26
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 324
    :cond_29
    :goto_29
    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v0

    invoke-virtual {v0}, Lo/H;->b()I

    move-result v0

    if-ge v1, v0, :cond_4b

    .line 325
    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v0

    invoke-virtual {v0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v0

    .line 326
    invoke-virtual {v0}, Lo/I;->b()Z

    move-result v3

    if-eqz v3, :cond_48

    .line 327
    invoke-virtual {v0}, Lo/I;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    :cond_48
    add-int/lit8 v1, v1, 0x1

    goto :goto_29

    .line 333
    :cond_4b
    invoke-virtual {p0}, Lo/U;->a()Lo/o;

    move-result-object v0

    if-eqz v0, :cond_5c

    .line 334
    invoke-virtual {p0}, Lo/U;->a()Lo/o;

    move-result-object v0

    invoke-virtual {v0}, Lo/o;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 338
    :cond_5c
    invoke-virtual {p0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_72

    .line 340
    invoke-virtual {p0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    :cond_6d
    :goto_6d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 341
    :cond_72
    invoke-virtual {p0}, Lo/U;->b()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0xd

    if-le v0, v1, :cond_6d

    .line 342
    invoke-virtual {p1}, Lo/T;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6d
.end method

.method private a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1076
    iput-object p1, p0, LF/L;->J:Ljava/lang/String;

    .line 1077
    return-void
.end method

.method private b(LC/a;)Lo/t;
    .registers 10
    .parameter

    .prologue
    const/high16 v4, 0x42b4

    const/high16 v3, 0x4387

    .line 987
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    if-nez v0, :cond_e

    .line 988
    const/4 v0, 0x0

    .line 1021
    :goto_d
    return-object v0

    .line 993
    :cond_e
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->c()F

    move-result v0

    .line 994
    cmpg-float v1, v0, v3

    if-gez v1, :cond_8b

    add-float/2addr v0, v4

    move v2, v0

    .line 995
    :goto_1a
    cmpg-float v0, v2, v3

    if-gez v0, :cond_8e

    add-float v0, v2, v4

    move v1, v0

    .line 997
    :goto_21
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, LC/a;->a(Lo/T;Z)F

    move-result v3

    .line 998
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    .line 999
    iget-object v4, v0, Lx/l;->a:Lo/T;

    .line 1000
    iget v5, p0, LF/L;->p:F

    invoke-virtual {p1, v5, v3}, LC/a;->a(FF)F

    move-result v5

    invoke-virtual {v4, v2, v5}, Lo/T;->b(FF)V

    .line 1002
    iget-object v5, v0, Lx/l;->b:Lo/T;

    .line 1003
    iget v6, p0, LF/L;->q:F

    invoke-virtual {p1, v6, v3}, LC/a;->a(FF)F

    move-result v6

    invoke-virtual {v5, v2, v6}, Lo/T;->b(FF)V

    .line 1005
    iget-object v2, v0, Lx/l;->c:Lo/T;

    .line 1006
    iget v6, p0, LF/L;->r:F

    invoke-virtual {p1, v6, v3}, LC/a;->a(FF)F

    move-result v6

    invoke-virtual {v2, v1, v6}, Lo/T;->b(FF)V

    .line 1008
    iget-object v6, v0, Lx/l;->d:Lo/T;

    .line 1009
    iget v7, p0, LF/L;->s:F

    invoke-virtual {p1, v7, v3}, LC/a;->a(FF)F

    move-result v3

    invoke-virtual {v6, v1, v3}, Lo/T;->b(FF)V

    .line 1012
    iget-object v1, v0, Lx/l;->e:Lo/T;

    .line 1013
    iget-object v3, p0, LF/L;->l:Lo/a;

    invoke-virtual {v3}, Lo/a;->b()Lo/T;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 1014
    iget-object v0, v0, Lx/l;->f:Lo/T;

    .line 1015
    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->b()Lo/T;

    move-result-object v2

    invoke-static {v2, v6, v0}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 1016
    invoke-virtual {v1, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v2

    .line 1017
    invoke-virtual {v1, v5}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    .line 1018
    invoke-virtual {v0, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v3

    .line 1019
    invoke-virtual {v0, v5}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    .line 1021
    invoke-static {v3, v0, v2, v1}, Lo/t;->a(Lo/T;Lo/T;Lo/T;Lo/T;)Lo/t;

    move-result-object v0

    goto :goto_d

    .line 994
    :cond_8b
    sub-float/2addr v0, v3

    move v2, v0

    goto :goto_1a

    .line 995
    :cond_8e
    sub-float v0, v2, v3

    move v1, v0

    goto :goto_21
.end method

.method private b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x43b4

    const/high16 v8, 0x4000

    const/4 v1, 0x0

    .line 777
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v6

    .line 778
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    .line 782
    invoke-virtual {p2}, LC/a;->j()Z

    move-result v2

    if-nez v2, :cond_57

    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->a()Z

    move-result v2

    if-eqz v2, :cond_2c

    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->c()F

    move-result v2

    invoke-virtual {p2}, LC/a;->p()F

    move-result v3

    sub-float/2addr v2, v3

    cmpl-float v2, v2, v1

    if-nez v2, :cond_57

    :cond_2c
    invoke-virtual {p2}, LC/a;->q()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_57

    .line 785
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    iget-object v2, p0, LF/L;->I:[F

    invoke-virtual {p2, v0, v2}, LC/a;->a(Lo/T;[F)V

    .line 786
    iget-object v0, p0, LF/L;->I:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LF/L;->I:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    .line 789
    :cond_57
    iget v2, p0, LF/L;->A:F

    invoke-static {p1, p2, v0, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 800
    iget-object v0, p0, LF/L;->E:Lh/e;

    if-eqz v0, :cond_11c

    .line 801
    iget-object v0, p0, LF/L;->E:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    .line 802
    const/high16 v2, 0x1

    if-ne v0, v2, :cond_6d

    .line 803
    const/4 v2, 0x0

    iput-object v2, p0, LF/L;->E:Lh/e;

    .line 809
    :cond_6d
    :goto_6d
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 811
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    if-eqz v0, :cond_120

    .line 816
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->c()F

    move-result v0

    invoke-virtual {p2}, LC/a;->p()F

    move-result v2

    sub-float/2addr v0, v2

    .line 817
    cmpg-float v2, v0, v1

    if-gez v2, :cond_8c

    .line 818
    add-float/2addr v0, v4

    .line 820
    :cond_8c
    iget-object v2, p0, LF/L;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->c()F

    move-result v2

    .line 824
    iget-boolean v3, p0, LF/L;->z:Z

    if-nez v3, :cond_1a4

    const/high16 v3, 0x42b4

    cmpl-float v3, v0, v3

    if-lez v3, :cond_1a4

    const/high16 v3, 0x4387

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1a4

    .line 825
    const/high16 v0, 0x4334

    add-float/2addr v0, v2

    .line 827
    :goto_a5
    cmpl-float v2, v0, v4

    if-ltz v2, :cond_aa

    .line 828
    sub-float/2addr v0, v4

    .line 835
    :cond_aa
    const/high16 v2, -0x4080

    invoke-interface {v6, v0, v1, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 839
    const/high16 v0, -0x3d4c

    const/high16 v2, 0x3f80

    invoke-interface {v6, v0, v2, v1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 848
    :goto_b6
    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_19e

    .line 850
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v4

    .line 851
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v3

    .line 852
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    div-float v2, v0, v8

    .line 853
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    div-float/2addr v0, v8

    .line 854
    neg-float v5, v2

    neg-float v7, v0

    invoke-interface {v6, v5, v1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 855
    iget-object v5, p0, LF/L;->m:LF/o;

    invoke-virtual {v5, p1, p2, p3}, LF/o;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 858
    :goto_df
    iget-object v5, p0, LF/L;->n:LF/o;

    if-eqz v5, :cond_11b

    .line 862
    sget-object v5, LF/M;->a:[I

    iget-object v7, p0, LF/L;->o:LF/O;

    iget-object v7, v7, LF/O;->a:LF/N;

    invoke-virtual {v7}, LF/N;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_1a8

    move v3, v1

    move v0, v1

    .line 907
    :goto_f4
    iget-object v5, p0, LF/L;->o:LF/O;

    iget-object v5, v5, LF/O;->a:LF/N;

    sget-object v7, LF/N;->d:LF/N;

    if-eq v5, v7, :cond_104

    iget-object v5, p0, LF/L;->o:LF/O;

    iget-object v5, v5, LF/O;->a:LF/N;

    sget-object v7, LF/N;->b:LF/N;

    if-ne v5, v7, :cond_113

    .line 912
    :cond_104
    sget-object v5, LF/M;->b:[I

    iget-object v7, p0, LF/L;->o:LF/O;

    iget-object v7, v7, LF/O;->b:LF/s;

    invoke-virtual {v7}, LF/s;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_1be

    .line 927
    :cond_113
    :goto_113
    invoke-interface {v6, v0, v1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 928
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0, p1, p2, p3}, LF/o;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 930
    :cond_11b
    return-void

    .line 807
    :cond_11c
    iget v0, p0, LF/L;->k:I

    goto/16 :goto_6d

    .line 841
    :cond_120
    invoke-static {v6, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    goto :goto_b6

    .line 866
    :pswitch_124
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->a()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v5, v2, v3

    .line 867
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v5

    .line 868
    goto :goto_f4

    .line 872
    :pswitch_138
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v4

    .line 873
    goto :goto_f4

    .line 878
    :pswitch_143
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->a()F

    move-result v3

    neg-float v5, v3

    .line 879
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    div-float/2addr v3, v8

    sub-float v3, v0, v3

    move v0, v5

    .line 880
    goto :goto_f4

    .line 883
    :pswitch_155
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v1

    .line 885
    goto :goto_f4

    :pswitch_15e
    move v0, v1

    .line 889
    goto :goto_f4

    .line 892
    :pswitch_160
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    neg-float v3, v0

    move v0, v4

    .line 893
    goto :goto_f4

    .line 895
    :pswitch_169
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    neg-float v0, v0

    .line 896
    iget-object v3, p0, LF/L;->n:LF/o;

    invoke-virtual {v3}, LF/o;->b()F

    move-result v3

    neg-float v3, v3

    .line 897
    goto/16 :goto_f4

    :pswitch_179
    move v0, v4

    .line 901
    goto/16 :goto_f4

    .line 903
    :pswitch_17c
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    neg-float v0, v0

    .line 904
    goto/16 :goto_f4

    .line 915
    :pswitch_185
    const/high16 v0, -0x3ee0

    .line 916
    goto :goto_113

    .line 919
    :pswitch_188
    const/high16 v0, 0x4120

    add-float/2addr v0, v4

    iget-object v2, p0, LF/L;->n:LF/o;

    invoke-virtual {v2}, LF/o;->a()F

    move-result v2

    sub-float/2addr v0, v2

    .line 921
    goto :goto_113

    .line 923
    :pswitch_193
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    div-float/2addr v0, v8

    sub-float v0, v2, v0

    goto/16 :goto_113

    :cond_19e
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_df

    :cond_1a4
    move v0, v2

    goto/16 :goto_a5

    .line 862
    nop

    :pswitch_data_1a8
    .packed-switch 0x1
        :pswitch_15e
        :pswitch_138
        :pswitch_155
        :pswitch_143
        :pswitch_160
        :pswitch_169
        :pswitch_179
        :pswitch_17c
        :pswitch_124
    .end packed-switch

    .line 912
    :pswitch_data_1be
    .packed-switch 0x1
        :pswitch_185
        :pswitch_188
        :pswitch_193
    .end packed-switch
.end method

.method private b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 773
    iput-boolean p1, p0, LF/L;->z:Z

    .line 774
    return-void
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 727
    invoke-super {p0, p1}, LF/m;->a(LD/a;)V

    .line 728
    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_c

    .line 729
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0, p1}, LF/o;->a(LD/a;)V

    .line 731
    :cond_c
    iget-object v0, p0, LF/L;->n:LF/o;

    if-eqz v0, :cond_15

    .line 732
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0, p1}, LF/o;->a(LD/a;)V

    .line 734
    :cond_15
    iget-object v0, p0, LF/L;->w:LE/o;

    if-eqz v0, :cond_1e

    .line 735
    iget-object v0, p0, LF/L;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 737
    :cond_1e
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 755
    iget-boolean v0, p0, LF/L;->B:Z

    if-nez v0, :cond_f

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LF/L;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 766
    :goto_e
    return-void

    .line 762
    :cond_f
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 763
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 764
    invoke-direct {p0, p1, p2, p3}, LF/L;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 765
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_e
.end method

.method public a(LC/a;LD/a;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 960
    iget v2, p0, LF/L;->D:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, LF/L;->C:[LF/O;

    array-length v3, v3

    if-ge v2, v3, :cond_27

    .line 961
    iget-object v1, p0, LF/L;->C:[LF/O;

    iget v2, p0, LF/L;->D:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LF/L;->D:I

    aget-object v1, v1, v2

    iput-object v1, p0, LF/L;->o:LF/O;

    .line 962
    iget-object v1, p0, LF/L;->n:LF/o;

    iget-object v2, p0, LF/L;->o:LF/O;

    iget-object v2, v2, LF/O;->b:LF/s;

    invoke-virtual {v1, v2}, LF/o;->a(LF/s;)V

    .line 963
    iput-object v4, p0, LF/L;->u:Lo/t;

    .line 964
    invoke-virtual {p0, p1, p2}, LF/L;->b(LC/a;LD/a;)Z

    .line 978
    :goto_26
    return v0

    .line 966
    :cond_27
    iget-object v2, p0, LF/L;->t:Lo/a;

    if-eqz v2, :cond_4d

    .line 967
    iget-object v2, p0, LF/L;->t:Lo/a;

    iput-object v2, p0, LF/L;->l:Lo/a;

    .line 968
    iput-object v4, p0, LF/L;->t:Lo/a;

    .line 969
    iget-object v2, p0, LF/L;->C:[LF/O;

    array-length v2, v2

    if-le v2, v0, :cond_47

    .line 970
    iput v1, p0, LF/L;->D:I

    .line 971
    iget-object v2, p0, LF/L;->C:[LF/O;

    aget-object v1, v2, v1

    iput-object v1, p0, LF/L;->o:LF/O;

    .line 972
    iget-object v1, p0, LF/L;->n:LF/o;

    iget-object v2, p0, LF/L;->o:LF/O;

    iget-object v2, v2, LF/O;->b:LF/s;

    invoke-virtual {v1, v2}, LF/o;->a(LF/s;)V

    .line 974
    :cond_47
    iput-object v4, p0, LF/L;->u:Lo/t;

    .line 975
    invoke-virtual {p0, p1, p2}, LF/L;->b(LC/a;LD/a;)Z

    goto :goto_26

    :cond_4d
    move v0, v1

    .line 978
    goto :goto_26
.end method

.method a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 933
    iget-boolean v0, p0, LF/L;->h:Z

    if-eqz v0, :cond_10

    .line 934
    new-instance v0, Lh/e;

    const-wide/16 v1, 0x1f4

    sget-object v3, Lh/g;->a:Lh/g;

    invoke-direct {v0, v1, v2, v3}, Lh/e;-><init>(JLh/g;)V

    iput-object v0, p0, LF/L;->E:Lh/e;

    .line 936
    :cond_10
    iput-boolean v4, p0, LF/L;->B:Z

    .line 937
    return v4
.end method

.method public a(Lo/aS;)Z
    .registers 3
    .parameter

    .prologue
    .line 953
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p1, v0}, Lo/aS;->a(Lo/T;)Z

    move-result v0

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 741
    invoke-super {p0, p1}, LF/m;->b(LD/a;)V

    .line 742
    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_c

    .line 743
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0, p1}, LF/o;->b(LD/a;)V

    .line 745
    :cond_c
    iget-object v0, p0, LF/L;->n:LF/o;

    if-eqz v0, :cond_15

    .line 746
    iget-object v0, p0, LF/L;->n:LF/o;

    invoke-virtual {v0, p1}, LF/o;->b(LD/a;)V

    .line 748
    :cond_15
    iget-object v0, p0, LF/L;->w:LE/o;

    if-eqz v0, :cond_1e

    .line 749
    iget-object v0, p0, LF/L;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 751
    :cond_1e
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 527
    invoke-virtual {p0}, LF/L;->l()Z

    move-result v0

    if-eqz v0, :cond_31

    iget-boolean v0, p0, LF/L;->i:Z

    if-eqz v0, :cond_31

    iget v0, p0, LF/L;->A:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_31

    .line 528
    invoke-direct {p0, p1}, LF/L;->a(LC/a;)F

    move-result v0

    iget v3, p0, LF/L;->A:F

    div-float/2addr v0, v3

    .line 529
    invoke-static {v0}, LF/L;->a(F)I

    move-result v3

    iput v3, p0, LF/L;->k:I

    .line 530
    const/high16 v3, 0x3e80

    cmpl-float v3, v0, v3

    if-ltz v3, :cond_2f

    const/high16 v3, 0x4000

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_2f

    move v0, v1

    :goto_2d
    move v1, v0

    .line 701
    :cond_2e
    :goto_2e
    return v1

    :cond_2f
    move v0, v2

    .line 530
    goto :goto_2d

    .line 533
    :cond_31
    const/high16 v0, 0x1

    iput v0, p0, LF/L;->k:I

    .line 538
    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_71

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_71

    move v0, v1

    .line 540
    :goto_46
    invoke-virtual {p1}, LC/a;->r()F

    move-result v3

    .line 541
    iget-object v4, p0, LF/L;->u:Lo/t;

    if-eqz v4, :cond_5a

    iget-boolean v4, p0, LF/L;->F:Z

    if-eqz v4, :cond_5a

    if-eqz v0, :cond_5a

    iget v4, p0, LF/L;->G:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_2e

    .line 548
    :cond_5a
    iget-object v4, p0, LF/L;->u:Lo/t;

    if-eqz v4, :cond_73

    iget-object v4, p0, LF/L;->l:Lo/a;

    invoke-virtual {v4}, Lo/a;->a()Z

    move-result v4

    if-eqz v4, :cond_73

    iget v4, p0, LF/L;->G:F

    cmpl-float v4, v3, v4

    if-nez v4, :cond_73

    .line 549
    iget-object v0, p0, LF/L;->u:Lo/t;

    iput-object v0, p0, LF/L;->v:Lo/t;

    goto :goto_2e

    :cond_71
    move v0, v2

    .line 538
    goto :goto_46

    .line 553
    :cond_73
    iput-boolean v0, p0, LF/L;->F:Z

    .line 554
    iput v3, p0, LF/L;->G:F

    .line 560
    iget-object v0, p0, LF/L;->m:LF/o;

    if-eqz v0, :cond_27a

    .line 561
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->a()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v3, v0, 0x1

    .line 562
    iget-object v0, p0, LF/L;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    float-to-int v0, v0

    shr-int/lit8 v0, v0, 0x1

    .line 564
    :goto_8d
    iget-object v4, p0, LF/L;->n:LF/o;

    if-nez v4, :cond_134

    .line 566
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    .line 567
    int-to-float v3, v3

    iput v3, p0, LF/L;->q:F

    .line 568
    neg-int v3, v0

    int-to-float v3, v3

    iput v3, p0, LF/L;->r:F

    .line 569
    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    .line 658
    :cond_9f
    :goto_9f
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx/l;

    iget-object v0, v0, Lx/l;->a:Lo/T;

    .line 659
    invoke-virtual {p1}, LC/a;->t()Lo/T;

    move-result-object v3

    iget-object v4, p0, LF/L;->l:Lo/a;

    invoke-virtual {v4}, Lo/a;->b()Lo/T;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 661
    invoke-direct {p0, p1}, LF/L;->a(LC/a;)F

    move-result v0

    iput v0, p0, LF/L;->A:F

    .line 662
    iget-boolean v0, p0, LF/L;->y:Z

    if-nez v0, :cond_e3

    .line 663
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    .line 666
    iget-object v3, p0, LF/L;->l:Lo/a;

    invoke-virtual {v3}, Lo/a;->b()Lo/T;

    move-result-object v3

    invoke-virtual {p1, v3, v1}, LC/a;->a(Lo/T;Z)F

    move-result v3

    div-float/2addr v0, v3

    .line 667
    iget v3, p0, LF/L;->p:F

    mul-float/2addr v3, v0

    iput v3, p0, LF/L;->p:F

    .line 668
    iget v3, p0, LF/L;->q:F

    mul-float/2addr v3, v0

    iput v3, p0, LF/L;->q:F

    .line 669
    iget v3, p0, LF/L;->r:F

    mul-float/2addr v3, v0

    iput v3, p0, LF/L;->r:F

    .line 670
    iget v3, p0, LF/L;->s:F

    mul-float/2addr v0, v3

    iput v0, p0, LF/L;->s:F

    .line 676
    :cond_e3
    iget-object v0, p0, LF/L;->c:Ly/b;

    if-eqz v0, :cond_11b

    iget-object v0, p0, LF/L;->c:Ly/b;

    instance-of v0, v0, Ly/a;

    if-eqz v0, :cond_11b

    .line 677
    iget-object v0, p0, LF/L;->c:Ly/b;

    check-cast v0, Ly/a;

    invoke-virtual {v0}, Ly/a;->a()Ljava/lang/Object;

    move-result-object v0

    .line 678
    if-eqz v0, :cond_11b

    instance-of v3, v0, Lo/r;

    if-eqz v3, :cond_11b

    .line 679
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v3

    check-cast v0, Lo/r;

    invoke-virtual {v3, v0}, Ln/q;->e(Lo/r;)Ln/k;

    move-result-object v0

    .line 681
    if-eqz v0, :cond_11b

    .line 682
    iget-object v3, p0, LF/L;->l:Lo/a;

    invoke-virtual {v3}, Lo/a;->b()Lo/T;

    move-result-object v3

    iget-object v4, p0, LF/L;->l:Lo/a;

    invoke-virtual {v4}, Lo/a;->b()Lo/T;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, Ln/k;->a(LC/a;Lo/T;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v3, v0}, Lo/T;->b(I)V

    .line 688
    :cond_11b
    iget-object v0, p0, LF/L;->u:Lo/t;

    iput-object v0, p0, LF/L;->v:Lo/t;

    .line 689
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    if-eqz v0, :cond_253

    .line 691
    invoke-direct {p0, p1}, LF/L;->b(LC/a;)Lo/t;

    move-result-object v0

    iput-object v0, p0, LF/L;->u:Lo/t;

    .line 701
    :goto_12d
    iget-object v0, p0, LF/L;->u:Lo/t;

    if-nez v0, :cond_2e

    move v1, v2

    goto/16 :goto_2e

    .line 572
    :cond_134
    iget-object v4, p0, LF/L;->n:LF/o;

    invoke-virtual {v4}, LF/o;->a()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v7, v4, 0x1

    .line 573
    iget-object v4, p0, LF/L;->n:LF/o;

    invoke-virtual {v4}, LF/o;->b()F

    move-result v4

    float-to-int v4, v4

    shr-int/lit8 v5, v4, 0x1

    .line 574
    if-le v3, v7, :cond_19f

    move v6, v3

    .line 575
    :goto_149
    if-le v0, v5, :cond_1a1

    move v4, v0

    .line 576
    :goto_14c
    sget-object v8, LF/M;->a:[I

    iget-object v9, p0, LF/L;->o:LF/O;

    iget-object v9, v9, LF/O;->a:LF/N;

    invoke-virtual {v9}, LF/N;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_27e

    .line 625
    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    .line 626
    int-to-float v0, v6

    iput v0, p0, LF/L;->q:F

    .line 627
    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->r:F

    .line 628
    int-to-float v0, v4

    iput v0, p0, LF/L;->s:F

    .line 631
    :goto_169
    iget-object v0, p0, LF/L;->o:LF/O;

    iget-object v0, v0, LF/O;->a:LF/N;

    sget-object v4, LF/N;->d:LF/N;

    if-eq v0, v4, :cond_179

    iget-object v0, p0, LF/L;->o:LF/O;

    iget-object v0, v0, LF/O;->a:LF/N;

    sget-object v4, LF/N;->b:LF/N;

    if-ne v0, v4, :cond_9f

    .line 636
    :cond_179
    sget-object v0, LF/M;->b:[I

    iget-object v4, p0, LF/L;->o:LF/O;

    iget-object v4, v4, LF/O;->b:LF/s;

    invoke-virtual {v4}, LF/s;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_292

    goto/16 :goto_9f

    .line 639
    :pswitch_18a
    neg-int v0, v3

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    .line 640
    iget v0, p0, LF/L;->p:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, LF/L;->q:F

    goto/16 :goto_9f

    :cond_19f
    move v6, v7

    .line 574
    goto :goto_149

    :cond_1a1
    move v4, v5

    .line 575
    goto :goto_14c

    .line 579
    :pswitch_1a3
    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    .line 580
    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto :goto_169

    .line 583
    :pswitch_1ae
    neg-int v0, v3

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    .line 584
    mul-int/lit8 v0, v7, 0x2

    add-int/2addr v0, v3

    int-to-float v0, v0

    iput v0, p0, LF/L;->q:F

    .line 585
    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->r:F

    .line 586
    int-to-float v0, v4

    iput v0, p0, LF/L;->s:F

    goto :goto_169

    .line 590
    :pswitch_1c0
    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    .line 591
    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto :goto_169

    .line 594
    :pswitch_1cb
    neg-int v0, v3

    mul-int/lit8 v5, v7, 0x2

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    .line 595
    int-to-float v0, v3

    iput v0, p0, LF/L;->q:F

    .line 596
    neg-int v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->r:F

    .line 597
    int-to-float v0, v4

    iput v0, p0, LF/L;->s:F

    goto :goto_169

    .line 600
    :pswitch_1dd
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    .line 601
    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->q:F

    .line 602
    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    .line 603
    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_169

    .line 606
    :pswitch_1f3
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    .line 607
    int-to-float v4, v3

    iput v4, p0, LF/L;->q:F

    .line 608
    neg-int v4, v0

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    .line 609
    mul-int/lit8 v4, v5, 0x2

    add-int/2addr v0, v4

    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_169

    .line 612
    :pswitch_209
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    .line 613
    mul-int/lit8 v4, v7, 0x2

    add-int/2addr v4, v3

    int-to-float v4, v4

    iput v4, p0, LF/L;->q:F

    .line 614
    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    .line 615
    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_169

    .line 618
    :pswitch_21f
    neg-int v4, v3

    mul-int/lit8 v8, v7, 0x2

    sub-int/2addr v4, v8

    int-to-float v4, v4

    iput v4, p0, LF/L;->p:F

    .line 619
    int-to-float v4, v3

    iput v4, p0, LF/L;->q:F

    .line 620
    neg-int v4, v0

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, p0, LF/L;->r:F

    .line 621
    int-to-float v0, v0

    iput v0, p0, LF/L;->s:F

    goto/16 :goto_169

    .line 645
    :pswitch_235
    add-int/lit8 v0, v3, 0xa

    int-to-float v0, v0

    iput v0, p0, LF/L;->q:F

    .line 646
    iget v0, p0, LF/L;->q:F

    mul-int/lit8 v4, v7, 0x2

    int-to-float v4, v4

    sub-float/2addr v0, v4

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, LF/L;->p:F

    goto/16 :goto_9f

    .line 650
    :pswitch_24a
    neg-int v0, v6

    int-to-float v0, v0

    iput v0, p0, LF/L;->p:F

    .line 651
    int-to-float v0, v6

    iput v0, p0, LF/L;->q:F

    goto/16 :goto_9f

    .line 695
    :cond_253
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    iget-object v3, p0, LF/L;->I:[F

    invoke-virtual {p1, v0, v3}, LC/a;->a(Lo/T;[F)V

    .line 696
    iget-object v0, p0, LF/L;->I:[F

    aget v0, v0, v2

    .line 697
    iget-object v3, p0, LF/L;->I:[F

    aget v3, v3, v1

    .line 698
    iget v4, p0, LF/L;->p:F

    add-float/2addr v4, v0

    iget v5, p0, LF/L;->q:F

    add-float/2addr v0, v5

    iget v5, p0, LF/L;->r:F

    add-float/2addr v5, v3

    iget v6, p0, LF/L;->s:F

    add-float/2addr v3, v6

    invoke-virtual {p1, v4, v0, v5, v3}, LC/a;->a(FFFF)Lo/t;

    move-result-object v0

    iput-object v0, p0, LF/L;->u:Lo/t;

    goto/16 :goto_12d

    :cond_27a
    move v0, v2

    move v3, v2

    goto/16 :goto_8d

    .line 576
    :pswitch_data_27e
    .packed-switch 0x1
        :pswitch_1a3
        :pswitch_1ae
        :pswitch_1c0
        :pswitch_1cb
        :pswitch_1dd
        :pswitch_1f3
        :pswitch_209
        :pswitch_21f
    .end packed-switch

    .line 636
    :pswitch_data_292
    .packed-switch 0x1
        :pswitch_18a
        :pswitch_235
        :pswitch_24a
    .end packed-switch
.end method

.method public m()F
    .registers 2

    .prologue
    .line 513
    iget v0, p0, LF/L;->H:F

    return v0
.end method

.method public n()Lo/ae;
    .registers 2

    .prologue
    .line 712
    iget-object v0, p0, LF/L;->u:Lo/t;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .registers 2

    .prologue
    .line 518
    iget-object v0, p0, LF/L;->x:Ljava/lang/String;

    return-object v0
.end method

.method public y()Z
    .registers 2

    .prologue
    .line 1047
    iget-object v0, p0, LF/L;->l:Lo/a;

    if-nez v0, :cond_6

    .line 1048
    const/4 v0, 0x0

    .line 1050
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, LF/L;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->a()Z

    move-result v0

    goto :goto_5
.end method
