.class public LF/Y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/T;


# static fields
.field private static L:[F

.field public static final a:LR/a;

.field private static final b:LF/d;

.field private static final c:LF/d;

.field private static final d:LF/d;

.field private static final e:LF/d;

.field private static final f:LF/d;

.field private static final g:LF/d;


# instance fields
.field private A:LA/c;

.field private B:I

.field private C:[F

.field private D:J

.field private volatile E:I

.field private F:Ly/b;

.field private G:Ljava/lang/Boolean;

.field private H:J

.field private I:J

.field private J:I

.field private K:J

.field private M:Z

.field private h:Lx/m;

.field private i:Lx/m;

.field private final j:[F

.field private k:[LF/P;

.field private l:[LF/i;

.field private m:[LF/w;

.field private n:[[LF/Q;

.field private o:[LF/i;

.field private p:[LF/w;

.field private q:[LF/e;

.field private r:[LF/V;

.field private s:LF/m;

.field private t:Ljava/util/ArrayList;

.field private u:Ljava/util/Set;

.field private final v:Lo/aq;

.field private final w:Lo/ad;

.field private final x:Ljava/util/HashSet;

.field private y:I

.field private z:Lcom/google/android/maps/driveabout/vector/bE;


# direct methods
.method static constructor <clinit>()V
    .registers 16

    .prologue
    const v15, -0x33000001

    const/high16 v11, -0x8000

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 139
    new-instance v0, LF/d;

    const/high16 v2, 0x42f0

    const/high16 v4, -0x3dc0

    const/high16 v5, 0x4240

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, LF/d;-><init>(FFZFFZFI)V

    sput-object v0, LF/Y;->b:LF/d;

    .line 144
    new-instance v0, LF/d;

    const/high16 v2, 0x4270

    const/high16 v4, -0x3e40

    const/high16 v5, 0x41c0

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, LF/d;-><init>(FFZFFZFI)V

    sput-object v0, LF/Y;->d:LF/d;

    .line 149
    new-instance v0, LF/d;

    const/high16 v2, 0x41f0

    const/high16 v4, -0x3f40

    const/high16 v5, 0x40c0

    const v8, 0x6fffffff

    move v7, v1

    invoke-direct/range {v0 .. v8}, LF/d;-><init>(FFZFFZFI)V

    sput-object v0, LF/Y;->f:LF/d;

    .line 154
    new-instance v7, LF/d;

    const/high16 v8, 0x4240

    const/high16 v9, 0x42f0

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, LF/d;-><init>(FFZFFZFI)V

    sput-object v7, LF/Y;->c:LF/d;

    .line 159
    new-instance v7, LF/d;

    const/high16 v8, 0x41c0

    const/high16 v9, 0x4270

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, LF/d;-><init>(FFZFFZFI)V

    sput-object v7, LF/Y;->e:LF/d;

    .line 164
    new-instance v7, LF/d;

    const/high16 v8, 0x4180

    const/high16 v9, 0x4220

    move v10, v6

    move v12, v1

    move v13, v3

    move v14, v1

    invoke-direct/range {v7 .. v15}, LF/d;-><init>(FFZFFZFI)V

    sput-object v7, LF/Y;->g:LF/d;

    .line 172
    sget-object v0, LR/a;->a:LR/a;

    sput-object v0, LF/Y;->a:LR/a;

    .line 254
    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, LF/Y;->L:[F

    return-void
.end method

.method private constructor <init>(Lo/aq;LD/a;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object v1, p0, LF/Y;->h:Lx/m;

    .line 112
    iput-object v1, p0, LF/Y;->i:Lx/m;

    .line 119
    new-array v0, v3, [F

    iput-object v0, p0, LF/Y;->j:[F

    .line 206
    new-array v0, v3, [F

    iput-object v0, p0, LF/Y;->C:[F

    .line 207
    iput-wide v6, p0, LF/Y;->D:J

    .line 220
    const/4 v0, -0x1

    iput v0, p0, LF/Y;->E:I

    .line 225
    iput-object v1, p0, LF/Y;->F:Ly/b;

    .line 231
    iput-object v1, p0, LF/Y;->G:Ljava/lang/Boolean;

    .line 236
    iput-wide v4, p0, LF/Y;->H:J

    .line 241
    iput-wide v4, p0, LF/Y;->I:J

    .line 246
    iput v2, p0, LF/Y;->J:I

    .line 249
    iput-wide v6, p0, LF/Y;->K:J

    .line 265
    iput-boolean v2, p0, LF/Y;->M:Z

    .line 473
    iput-object p1, p0, LF/Y;->v:Lo/aq;

    .line 474
    iget-object v0, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iput-object v0, p0, LF/Y;->w:Lo/ad;

    .line 475
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LF/Y;->x:Ljava/util/HashSet;

    .line 482
    return-void
.end method

.method public static a(Lo/ap;LR/a;LD/a;)LF/Y;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 277
    invoke-interface {p0}, Lo/ap;->d()Lo/aq;

    move-result-object v0

    .line 278
    new-instance v1, LF/Y;

    invoke-direct {v1, v0, p2}, LF/Y;-><init>(Lo/aq;LD/a;)V

    .line 279
    instance-of v0, p0, Lo/aL;

    if-eqz v0, :cond_19

    .line 280
    check-cast p0, Lo/aL;

    .line 281
    invoke-direct {v1, p0, p1, p2}, LF/Y;->a(Lo/aL;LR/a;LD/a;)V

    .line 282
    invoke-virtual {p0}, Lo/aL;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LF/Y;->b(J)V

    .line 284
    :cond_19
    return-object v1
.end method

.method private a(Lo/aL;)LF/d;
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x4194

    const/high16 v3, 0x4184

    const/4 v1, 0x0

    .line 507
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    sget-object v2, Lo/av;->c:Lo/av;

    invoke-virtual {v0, v2}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v0

    check-cast v0, Lo/E;

    .line 508
    if-nez v0, :cond_15

    move-object v0, v1

    .line 544
    :goto_14
    return-object v0

    .line 511
    :cond_15
    invoke-virtual {v0}, Lo/E;->b()Lo/r;

    move-result-object v0

    .line 515
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v2

    .line 516
    if-nez v2, :cond_21

    move-object v0, v1

    .line 517
    goto :goto_14

    .line 519
    :cond_21
    invoke-virtual {v2, v0}, Lr/n;->a(Lo/r;)Lo/y;

    move-result-object v2

    .line 520
    if-nez v2, :cond_29

    move-object v0, v1

    .line 521
    goto :goto_14

    .line 523
    :cond_29
    invoke-virtual {v2, v0}, Lo/y;->a(Lo/r;)Lo/z;

    move-result-object v0

    .line 524
    if-nez v0, :cond_31

    move-object v0, v1

    .line 525
    goto :goto_14

    .line 527
    :cond_31
    invoke-virtual {v0}, Lo/z;->e()I

    move-result v2

    if-lez v2, :cond_5a

    .line 528
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_47

    .line 529
    sget-object v0, LF/Y;->f:LF/d;

    goto :goto_14

    .line 530
    :cond_47
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_57

    .line 531
    sget-object v0, LF/Y;->d:LF/d;

    goto :goto_14

    .line 533
    :cond_57
    sget-object v0, LF/Y;->b:LF/d;

    goto :goto_14

    .line 535
    :cond_5a
    invoke-virtual {v0}, Lo/z;->e()I

    move-result v0

    if-gez v0, :cond_83

    .line 536
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_70

    .line 537
    sget-object v0, LF/Y;->g:LF/d;

    goto :goto_14

    .line 538
    :cond_70
    invoke-virtual {p1}, Lo/aL;->d()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_80

    .line 539
    sget-object v0, LF/Y;->e:LF/d;

    goto :goto_14

    .line 541
    :cond_80
    sget-object v0, LF/Y;->c:LF/d;

    goto :goto_14

    :cond_83
    move-object v0, v1

    .line 544
    goto :goto_14
.end method

.method private a(LD/a;F)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    const/4 v3, 0x0

    .line 1617
    invoke-virtual {p1}, LD/a;->r()V

    .line 1618
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 1619
    sub-float v0, v4, p2

    .line 1620
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    mul-float v2, v4, v0

    invoke-interface {v1, v3, v3, v2, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 1621
    iget-object v0, p1, LD/a;->f:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 1622
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 1623
    invoke-virtual {p1}, LD/a;->s()V

    .line 1624
    return-void
.end method

.method private a(Lo/aL;LR/a;LD/a;)V
    .registers 33
    .parameter
    .parameter
    .parameter

    .prologue
    .line 556
    invoke-virtual/range {p1 .. p1}, Lo/aL;->p()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, LF/Y;->y:I

    .line 557
    invoke-virtual/range {p1 .. p1}, Lo/aL;->c()[Ljava/lang/String;

    move-result-object v2

    .line 558
    array-length v3, v2

    const/4 v1, 0x0

    :goto_e
    if-ge v1, v3, :cond_1c

    aget-object v4, v2, v1

    .line 559
    move-object/from16 v0, p0

    iget-object v5, v0, LF/Y;->x:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 558
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 561
    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lo/aL;->g()LA/c;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->A:LA/c;

    .line 562
    invoke-virtual/range {p1 .. p1}, Lo/aL;->h()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, LF/Y;->B:I

    .line 563
    invoke-virtual/range {p1 .. p1}, Lo/aL;->f()[Ljava/lang/String;

    move-result-object v2

    .line 566
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 567
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 568
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 569
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 570
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 571
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 572
    new-instance v28, Ljava/util/ArrayList;

    invoke-direct/range {v28 .. v28}, Ljava/util/ArrayList;-><init>()V

    .line 574
    new-instance v10, LF/y;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-direct {v10, v1, v2}, LF/y;-><init>(Lo/aq;[Ljava/lang/String;)V

    .line 575
    new-instance v15, LF/y;

    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-direct {v15, v1, v2}, LF/y;-><init>(Lo/aq;[Ljava/lang/String;)V

    .line 577
    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    invoke-direct/range {p0 .. p1}, LF/Y;->a(Lo/aL;)LF/d;

    move-result-object v11

    .line 581
    const/4 v6, 0x0

    .line 586
    invoke-virtual/range {p1 .. p1}, Lo/aL;->k()Lo/aO;

    move-result-object v3

    .line 587
    const/4 v1, 0x1

    .line 588
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-wide/from16 v19, v4

    move/from16 v21, v1

    .line 589
    :goto_7a
    invoke-interface {v3}, Lo/aO;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a9

    .line 591
    invoke-interface {v3}, Lo/aO;->b()Lo/n;

    move-result-object v1

    .line 592
    invoke-interface {v1}, Lo/n;->h()I

    move-result v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, LR/a;->a(I)Z

    move-result v4

    if-nez v4, :cond_94

    .line 593
    invoke-interface {v3}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_7a

    .line 596
    :cond_94
    invoke-interface {v1}, Lo/n;->h()I

    move-result v4

    packed-switch v4, :pswitch_data_30c

    .line 661
    :pswitch_9b
    invoke-interface {v3}, Lo/aO;->next()Ljava/lang/Object;

    move/from16 v1, v21

    .line 666
    :goto_a0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 667
    sub-long v7, v4, v19

    const-wide/16 v12, 0xa

    cmp-long v7, v7, v12

    if-lez v7, :cond_307

    .line 668
    invoke-static {}, Ljava/lang/Thread;->yield()V

    :goto_af
    move-wide/from16 v19, v4

    move/from16 v21, v1

    .line 671
    goto :goto_7a

    .line 598
    :pswitch_b4
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    move-object/from16 v0, p0

    iget-object v4, v0, LF/Y;->i:Lx/m;

    move-object/from16 v0, p0

    iget-object v5, v0, LF/Y;->h:Lx/m;

    move-object/from16 v7, p3

    invoke-static/range {v1 .. v7}, LF/Q;->a(Lo/aq;[Ljava/lang/String;Lo/aO;Lz/D;Lz/D;Lx/n;LD/a;)LF/Q;

    move-result-object v1

    .line 603
    if-eqz v21, :cond_d0

    invoke-virtual {v1}, LF/Q;->c()Z

    move-result v4

    if-eqz v4, :cond_d0

    .line 604
    const/16 v21, 0x0

    .line 606
    :cond_d0
    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    .line 614
    goto :goto_a0

    .line 616
    :pswitch_d8
    if-eqz v21, :cond_ef

    .line 617
    move-object/from16 v0, p0

    iget-object v7, v0, LF/Y;->v:Lo/aq;

    const/4 v12, 0x2

    move-object v8, v2

    move-object v9, v3

    move-object/from16 v13, p3

    invoke-static/range {v7 .. v13}, LF/a;->a(Lo/aq;[Ljava/lang/String;Lo/aO;LF/y;LF/d;ILD/a;)LF/a;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto :goto_a0

    .line 620
    :cond_ef
    move-object/from16 v0, p0

    iget-object v12, v0, LF/Y;->v:Lo/aq;

    const/16 v17, 0x9

    move-object v13, v2

    move-object v14, v3

    move-object/from16 v16, v11

    move-object/from16 v18, p3

    invoke-static/range {v12 .. v18}, LF/a;->a(Lo/aq;[Ljava/lang/String;Lo/aO;LF/y;LF/d;ILD/a;)LF/a;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    .line 623
    goto :goto_a0

    .line 625
    :pswitch_107
    if-eqz v21, :cond_11c

    .line 626
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto :goto_a0

    .line 629
    :cond_11c
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/16 v4, 0xa

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    .line 632
    goto/16 :goto_a0

    .line 634
    :pswitch_131
    check-cast v1, Lo/K;

    invoke-static {v1}, LF/G;->a(Lo/K;)Z

    move-result v1

    if-eqz v1, :cond_164

    .line 635
    if-eqz v21, :cond_14f

    .line 636
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_a0

    .line 639
    :cond_14f
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    const/16 v4, 0xa

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v4, v0}, LF/G;->a(Lo/aq;[Ljava/lang/String;Lo/aO;ILD/a;)LF/G;

    move-result-object v1

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    goto/16 :goto_a0

    .line 642
    :cond_164
    if-eqz v21, :cond_16d

    .line 643
    invoke-virtual {v10, v3}, LF/y;->a(Lo/aO;)V

    move/from16 v1, v21

    goto/16 :goto_a0

    .line 645
    :cond_16d
    invoke-virtual {v15, v3}, LF/y;->a(Lo/aO;)V

    move/from16 v1, v21

    .line 647
    goto/16 :goto_a0

    .line 649
    :pswitch_174
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-static {v1, v2, v3}, LF/e;->a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/e;

    move-result-object v1

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    .line 650
    goto/16 :goto_a0

    .line 652
    :pswitch_185
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    move-object/from16 v0, p3

    invoke-static {v1, v2, v3, v0}, LF/P;->a(Lo/aq;[Ljava/lang/String;Lo/aO;LD/a;)LF/P;

    move-result-object v1

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    .line 653
    goto/16 :goto_a0

    .line 655
    :pswitch_198
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->v:Lo/aq;

    invoke-static {v1, v2, v3}, LF/V;->a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/V;

    move-result-object v1

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v1, v21

    .line 657
    goto/16 :goto_a0

    .line 689
    :cond_1a9
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1c1

    .line 690
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/P;

    move-object/from16 v0, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/P;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->k:[LF/P;

    .line 698
    :cond_1c1
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1d9

    .line 699
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/i;

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/i;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->l:[LF/i;

    .line 708
    :cond_1d9
    const/4 v1, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_219

    .line 709
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [[LF/Q;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->n:[[LF/Q;

    .line 710
    const/4 v1, 0x0

    move v3, v1

    :goto_1f4
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->n:[[LF/Q;

    array-length v1, v1

    if-ge v3, v1, :cond_219

    .line 711
    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 712
    move-object/from16 v0, p0

    iget-object v4, v0, LF/Y;->n:[[LF/Q;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [LF/Q;

    invoke-interface {v1, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/Q;

    aput-object v1, v4, v3

    .line 710
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1f4

    .line 723
    :cond_219
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_231

    .line 724
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/i;

    move-object/from16 v0, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/i;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->o:[LF/i;

    .line 732
    :cond_231
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_249

    .line 733
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/e;

    move-object/from16 v0, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/e;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->q:[LF/e;

    .line 740
    :cond_249
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_261

    .line 741
    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [LF/V;

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LF/V;

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->r:[LF/V;

    .line 749
    :cond_261
    const/4 v1, 0x3

    invoke-virtual {v10, v1}, LF/y;->a(I)[LF/w;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->m:[LF/w;

    .line 750
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_270

    .line 757
    :cond_270
    const/16 v1, 0xa

    invoke-virtual {v15, v1}, LF/y;->a(I)[LF/w;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->p:[LF/w;

    .line 758
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_280

    .line 767
    :cond_280
    invoke-virtual/range {p1 .. p1}, Lo/aL;->j()I

    move-result v5

    .line 768
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->t:Ljava/util/ArrayList;

    .line 769
    const/4 v1, 0x0

    move v4, v1

    :goto_28f
    if-ge v4, v5, :cond_2e8

    .line 770
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lo/aL;->a(I)Lo/n;

    move-result-object v3

    .line 771
    invoke-interface {v3}, Lo/n;->h()I

    move-result v1

    sparse-switch v1, :sswitch_data_320

    .line 769
    :cond_29e
    :goto_29e
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_28f

    .line 773
    :sswitch_2a2
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LF/Y;->a(Lo/n;)V

    move-object v1, v3

    .line 774
    check-cast v1, Lo/U;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LF/Y;->a(Lo/U;)V

    .line 777
    invoke-interface {v3}, Lo/n;->l()[I

    move-result-object v3

    array-length v6, v3

    const/4 v1, 0x0

    :goto_2b5
    if-ge v1, v6, :cond_29e

    aget v7, v3, v1

    .line 778
    if-ltz v7, :cond_2c7

    array-length v8, v2

    if-ge v7, v8, :cond_2c7

    .line 779
    move-object/from16 v0, p0

    iget-object v8, v0, LF/Y;->x:Ljava/util/HashSet;

    aget-object v7, v2, v7

    invoke-virtual {v8, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 777
    :cond_2c7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2b5

    :sswitch_2ca
    move-object v1, v3

    .line 787
    check-cast v1, Lo/K;

    .line 789
    invoke-virtual {v1}, Lo/K;->c()I

    move-result v1

    if-lez v1, :cond_29e

    .line 790
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LF/Y;->a(Lo/n;)V

    goto :goto_29e

    :sswitch_2d9
    move-object v1, v3

    .line 794
    check-cast v1, Lo/af;

    .line 796
    invoke-virtual {v1}, Lo/af;->d()I

    move-result v1

    if-lez v1, :cond_29e

    .line 797
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, LF/Y;->a(Lo/n;)V

    goto :goto_29e

    .line 804
    :cond_2e8
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Y;->t:Ljava/util/ArrayList;

    new-instance v2, LF/Z;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LF/Z;-><init>(LF/Y;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 811
    move-object/from16 v0, p1

    instance-of v1, v0, Lo/P;

    if-eqz v1, :cond_306

    .line 812
    check-cast p1, Lo/P;

    invoke-virtual/range {p1 .. p1}, Lo/P;->l()Ljava/util/Set;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, LF/Y;->u:Ljava/util/Set;

    .line 820
    :cond_306
    return-void

    :cond_307
    move-wide/from16 v4, v19

    goto/16 :goto_af

    .line 596
    nop

    :pswitch_data_30c
    .packed-switch 0x2
        :pswitch_b4
        :pswitch_d8
        :pswitch_174
        :pswitch_107
        :pswitch_185
        :pswitch_9b
        :pswitch_131
        :pswitch_198
    .end packed-switch

    .line 771
    :sswitch_data_320
    .sparse-switch
        0x2 -> :sswitch_2d9
        0x7 -> :sswitch_2a2
        0x8 -> :sswitch_2ca
        0xb -> :sswitch_2ca
    .end sparse-switch
.end method

.method private a(Lo/n;)V
    .registers 5
    .parameter

    .prologue
    .line 897
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    new-instance v1, Ly/c;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Ly/c;-><init>(Lo/n;Ly/b;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 898
    return-void
.end method

.method private k()I
    .registers 4

    .prologue
    .line 1630
    const/16 v0, 0x18

    .line 1631
    iget-object v1, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/c;

    .line 1632
    invoke-virtual {v0}, Ly/c;->d()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_9

    .line 1634
    :cond_1c
    return v1
.end method


# virtual methods
.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 915
    const/4 v0, 0x0

    .line 921
    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_6

    .line 922
    const/4 v0, 0x2

    .line 924
    :cond_6
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_c

    .line 925
    or-int/lit8 v0, v0, 0x4

    .line 927
    :cond_c
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_12

    .line 928
    or-int/lit8 v0, v0, 0x8

    .line 930
    :cond_12
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_1b

    .line 931
    invoke-static {p1, p2}, LF/Q;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v1

    or-int/2addr v0, v1

    .line 933
    :cond_1b
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_21

    .line 934
    or-int/lit16 v0, v0, 0x200

    .line 936
    :cond_21
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_27

    .line 937
    or-int/lit16 v0, v0, 0x400

    .line 939
    :cond_27
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_38

    .line 940
    invoke-virtual {p1}, LC/a;->q()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_36

    .line 942
    or-int/lit16 v0, v0, 0x800

    .line 944
    :cond_36
    or-int/lit16 v0, v0, 0x1000

    .line 946
    :cond_38
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_3e

    .line 947
    or-int/lit16 v0, v0, 0x2000

    .line 953
    :cond_3e
    sget-boolean v1, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    if-eqz v1, :cond_46

    .line 954
    const v1, 0x8000

    or-int/2addr v0, v1

    .line 957
    :cond_46
    return v0
.end method

.method public a(J)V
    .registers 7
    .parameter

    .prologue
    .line 1428
    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_12

    .line 1429
    iget-object v1, p0, LF/Y;->k:[LF/P;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    .line 1430
    invoke-virtual {v3, p1, p2}, LF/P;->a(J)V

    .line 1429
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1433
    :cond_12
    return-void
.end method

.method public a(LC/a;ILjava/util/Collection;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1437
    iget-object v1, p0, LF/Y;->x:Ljava/util/HashSet;

    invoke-interface {p3, v1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1439
    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_20

    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_20

    .line 1440
    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_12
    if-ge v1, v3, :cond_20

    aget-object v4, v2, v1

    .line 1441
    invoke-virtual {v4}, LF/P;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1440
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .line 1444
    :cond_20
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_3a

    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_3a

    .line 1446
    iget-object v2, p0, LF/Y;->l:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_2c
    if-ge v1, v3, :cond_3a

    aget-object v4, v2, v1

    .line 1447
    invoke-virtual {v4}, LF/i;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1446
    add-int/lit8 v1, v1, 0x1

    goto :goto_2c

    .line 1450
    :cond_3a
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_54

    and-int/lit8 v1, p2, 0x8

    if-eqz v1, :cond_54

    .line 1451
    iget-object v2, p0, LF/Y;->m:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_46
    if-ge v1, v3, :cond_54

    aget-object v4, v2, v1

    .line 1452
    invoke-virtual {v4}, LF/w;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1451
    add-int/lit8 v1, v1, 0x1

    goto :goto_46

    .line 1459
    :cond_54
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_78

    and-int/lit16 v1, p2, 0x1f0

    if-eqz v1, :cond_78

    .line 1460
    iget-object v3, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v3

    move v2, v0

    :goto_60
    if-ge v2, v4, :cond_78

    aget-object v5, v3, v2

    .line 1461
    array-length v6, v5

    move v1, v0

    :goto_66
    if-ge v1, v6, :cond_74

    aget-object v7, v5, v1

    .line 1462
    invoke-virtual {v7}, LF/Q;->d()Ljava/util/Set;

    move-result-object v7

    invoke-interface {p3, v7}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1461
    add-int/lit8 v1, v1, 0x1

    goto :goto_66

    .line 1460
    :cond_74
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_60

    .line 1466
    :cond_78
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_92

    and-int/lit16 v1, p2, 0x200

    if-eqz v1, :cond_92

    .line 1468
    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_84
    if-ge v1, v3, :cond_92

    aget-object v4, v2, v1

    .line 1469
    invoke-virtual {v4}, LF/i;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1468
    add-int/lit8 v1, v1, 0x1

    goto :goto_84

    .line 1472
    :cond_92
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_ac

    and-int/lit16 v1, p2, 0x400

    if-eqz v1, :cond_ac

    .line 1473
    iget-object v2, p0, LF/Y;->p:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_9e
    if-ge v1, v3, :cond_ac

    aget-object v4, v2, v1

    .line 1474
    invoke-virtual {v4}, LF/w;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1473
    add-int/lit8 v1, v1, 0x1

    goto :goto_9e

    .line 1477
    :cond_ac
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_c6

    and-int/lit16 v1, p2, 0x1800

    if-eqz v1, :cond_c6

    .line 1479
    iget-object v2, p0, LF/Y;->q:[LF/e;

    array-length v3, v2

    move v1, v0

    :goto_b8
    if-ge v1, v3, :cond_c6

    aget-object v4, v2, v1

    .line 1480
    invoke-virtual {v4}, LF/e;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {p3, v4}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1479
    add-int/lit8 v1, v1, 0x1

    goto :goto_b8

    .line 1483
    :cond_c6
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_df

    and-int/lit16 v1, p2, 0x2000

    if-eqz v1, :cond_df

    .line 1484
    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    :goto_d1
    if-ge v0, v2, :cond_df

    aget-object v3, v1, v0

    .line 1485
    invoke-virtual {v3}, LF/V;->d()Ljava/util/Set;

    move-result-object v3

    invoke-interface {p3, v3}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 1484
    add-int/lit8 v0, v0, 0x1

    goto :goto_d1

    .line 1488
    :cond_df
    return-void
.end method

.method public a(LC/a;LD/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1030
    return-void
.end method

.method public a(LC/a;Ljava/util/Collection;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1492
    return-void
.end method

.method public a(LD/a;)V
    .registers 10
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1268
    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_13

    .line 1269
    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_9
    if-ge v1, v3, :cond_13

    aget-object v4, v2, v1

    .line 1270
    invoke-virtual {v4, p1}, LF/P;->a(LD/a;)V

    .line 1269
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 1273
    :cond_13
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_25

    .line 1274
    iget-object v2, p0, LF/Y;->l:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_1b
    if-ge v1, v3, :cond_25

    aget-object v4, v2, v1

    .line 1275
    invoke-interface {v4, p1}, LF/h;->a(LD/a;)V

    .line 1274
    add-int/lit8 v1, v1, 0x1

    goto :goto_1b

    .line 1278
    :cond_25
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_37

    .line 1279
    iget-object v2, p0, LF/Y;->m:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_2d
    if-ge v1, v3, :cond_37

    aget-object v4, v2, v1

    .line 1280
    invoke-virtual {v4, p1}, LF/w;->a(LD/a;)V

    .line 1279
    add-int/lit8 v1, v1, 0x1

    goto :goto_2d

    .line 1283
    :cond_37
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_53

    .line 1284
    iget-object v3, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v3

    move v2, v0

    :goto_3f
    if-ge v2, v4, :cond_53

    aget-object v5, v3, v2

    .line 1285
    array-length v6, v5

    move v1, v0

    :goto_45
    if-ge v1, v6, :cond_4f

    aget-object v7, v5, v1

    .line 1286
    invoke-virtual {v7, p1}, LF/Q;->a(LD/a;)V

    .line 1285
    add-int/lit8 v1, v1, 0x1

    goto :goto_45

    .line 1284
    :cond_4f
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3f

    .line 1290
    :cond_53
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_65

    .line 1291
    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_5b
    if-ge v1, v3, :cond_65

    aget-object v4, v2, v1

    .line 1292
    invoke-interface {v4, p1}, LF/h;->a(LD/a;)V

    .line 1291
    add-int/lit8 v1, v1, 0x1

    goto :goto_5b

    .line 1295
    :cond_65
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_77

    .line 1296
    iget-object v2, p0, LF/Y;->p:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_6d
    if-ge v1, v3, :cond_77

    aget-object v4, v2, v1

    .line 1297
    invoke-virtual {v4, p1}, LF/w;->a(LD/a;)V

    .line 1296
    add-int/lit8 v1, v1, 0x1

    goto :goto_6d

    .line 1300
    :cond_77
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_89

    .line 1301
    iget-object v2, p0, LF/Y;->q:[LF/e;

    array-length v3, v2

    move v1, v0

    :goto_7f
    if-ge v1, v3, :cond_89

    aget-object v4, v2, v1

    .line 1302
    invoke-virtual {v4, p1}, LF/e;->a(LD/a;)V

    .line 1301
    add-int/lit8 v1, v1, 0x1

    goto :goto_7f

    .line 1305
    :cond_89
    iget-object v1, p0, LF/Y;->s:LF/m;

    if-eqz v1, :cond_95

    .line 1306
    iget-object v1, p0, LF/Y;->s:LF/m;

    invoke-virtual {v1, p1}, LF/m;->a(LD/a;)V

    .line 1307
    const/4 v1, 0x0

    iput-object v1, p0, LF/Y;->s:LF/m;

    .line 1310
    :cond_95
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_a6

    .line 1311
    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    :goto_9c
    if-ge v0, v2, :cond_a6

    aget-object v3, v1, v0

    .line 1312
    invoke-virtual {v3, p1}, LF/V;->a(LD/a;)V

    .line 1311
    add-int/lit8 v0, v0, 0x1

    goto :goto_9c

    .line 1315
    :cond_a6
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1040
    const/high16 v2, 0x3f80

    .line 1053
    invoke-virtual {p1}, LD/a;->I()I

    move-result v0

    if-lez v0, :cond_bc

    invoke-virtual {p1}, LD/a;->J()Z

    move-result v0

    if-eqz v0, :cond_bc

    const/4 v0, 0x1

    .line 1056
    :goto_f
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v3

    .line 1057
    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 1058
    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v4

    iget-wide v6, p0, LF/Y;->D:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_7b

    .line 1059
    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v4

    iput-wide v4, p0, LF/Y;->D:J

    .line 1060
    iget-object v1, p0, LF/Y;->w:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    .line 1065
    invoke-virtual {p2}, LC/a;->j()Z

    move-result v4

    if-nez v4, :cond_6f

    invoke-virtual {p2}, LC/a;->q()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_6f

    invoke-virtual {p2}, LC/a;->p()F

    move-result v4

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_6f

    invoke-virtual {p2}, LC/a;->r()F

    move-result v4

    invoke-virtual {p2}, LC/a;->r()F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-nez v4, :cond_6f

    .line 1068
    sget-object v4, LF/Y;->L:[F

    invoke-virtual {p2, v1, v4}, LC/a;->a(Lo/T;[F)V

    .line 1069
    sget-object v1, LF/Y;->L:[F

    const/4 v4, 0x0

    aget v1, v1, v4

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    sget-object v4, LF/Y;->L:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2, v1, v4}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    .line 1072
    :cond_6f
    iget-object v4, p0, LF/Y;->w:Lo/ad;

    invoke-virtual {v4}, Lo/ad;->g()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, LF/Y;->C:[F

    invoke-static {p1, p2, v1, v4, v5}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F[F)V

    .line 1075
    :cond_7b
    iget-object v1, p0, LF/Y;->C:[F

    invoke-static {v3, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    .line 1077
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_268

    .line 1229
    :cond_87
    :goto_87
    invoke-interface {v3}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 1234
    iget-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    if-nez v0, :cond_bb

    .line 1235
    invoke-virtual {p1}, LD/a;->H()Z

    move-result v0

    if-eqz v0, :cond_262

    sget-boolean v0, Lcom/google/googlenav/android/E;->a:Z

    if-nez v0, :cond_262

    .line 1238
    iget-object v0, p0, LF/Y;->l:[LF/i;

    if-nez v0, :cond_25f

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-nez v0, :cond_25f

    iget-object v0, p0, LF/Y;->m:[LF/w;

    if-nez v0, :cond_25f

    iget-object v0, p0, LF/Y;->o:[LF/i;

    if-nez v0, :cond_25f

    iget-object v0, p0, LF/Y;->p:[LF/w;

    if-nez v0, :cond_25f

    iget-object v0, p0, LF/Y;->q:[LF/e;

    if-nez v0, :cond_25f

    iget-object v0, p0, LF/Y;->r:[LF/V;

    if-nez v0, :cond_25f

    const/4 v0, 0x1

    :goto_b5
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    .line 1248
    :cond_bb
    :goto_bb
    return-void

    .line 1053
    :cond_bc
    const/4 v0, 0x0

    goto/16 :goto_f

    .line 1079
    :pswitch_bf
    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_87

    .line 1080
    iget-object v1, p0, LF/Y;->k:[LF/P;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_c7
    if-ge v0, v2, :cond_87

    aget-object v4, v1, v0

    .line 1081
    invoke-virtual {v4, p1, p2, p3}, LF/P;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1080
    add-int/lit8 v0, v0, 0x1

    goto :goto_c7

    .line 1086
    :pswitch_d1
    iget-object v0, p0, LF/Y;->l:[LF/i;

    if-eqz v0, :cond_e3

    .line 1087
    iget-object v1, p0, LF/Y;->l:[LF/i;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_d9
    if-ge v0, v2, :cond_e3

    aget-object v4, v1, v0

    .line 1088
    invoke-interface {v4, p1, p2, p3}, LF/h;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1087
    add-int/lit8 v0, v0, 0x1

    goto :goto_d9

    .line 1091
    :cond_e3
    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_87

    .line 1092
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_ec
    if-ge v1, v4, :cond_87

    aget-object v5, v2, v1

    .line 1093
    array-length v6, v5

    const/4 v0, 0x0

    :goto_f2
    if-ge v0, v6, :cond_fc

    aget-object v7, v5, v0

    .line 1094
    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1093
    add-int/lit8 v0, v0, 0x1

    goto :goto_f2

    .line 1092
    :cond_fc
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_ec

    .line 1100
    :pswitch_100
    iget-object v0, p0, LF/Y;->m:[LF/w;

    if-eqz v0, :cond_87

    .line 1101
    iget-object v1, p0, LF/Y;->m:[LF/w;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_108
    if-ge v0, v2, :cond_87

    aget-object v4, v1, v0

    .line 1102
    invoke-virtual {v4, p1, p2, p3}, LF/w;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1101
    add-int/lit8 v0, v0, 0x1

    goto :goto_108

    .line 1107
    :pswitch_112
    if-nez v0, :cond_87

    .line 1108
    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_87

    .line 1109
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_11d
    if-ge v1, v4, :cond_87

    aget-object v5, v2, v1

    .line 1110
    array-length v6, v5

    const/4 v0, 0x0

    :goto_123
    if-ge v0, v6, :cond_12d

    aget-object v7, v5, v0

    .line 1111
    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1110
    add-int/lit8 v0, v0, 0x1

    goto :goto_123

    .line 1109
    :cond_12d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11d

    .line 1118
    :pswitch_131
    if-eqz v0, :cond_1aa

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_1aa

    .line 1122
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    .line 1123
    invoke-static {p2, v0}, LF/Q;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v2

    .line 1124
    new-instance v4, Lcom/google/android/maps/driveabout/vector/aT;

    const/4 v1, 0x4

    invoke-direct {v4, v0, v1}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/q;I)V

    .line 1125
    new-instance v5, Lcom/google/android/maps/driveabout/vector/aT;

    const/4 v1, 0x6

    invoke-direct {v5, v0, v1}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/q;I)V

    .line 1127
    iget-object v6, p0, LF/Y;->n:[[LF/Q;

    array-length v7, v6

    const/4 v0, 0x0

    move v1, v0

    :goto_150
    if-ge v1, v7, :cond_87

    aget-object v8, v6, v1

    .line 1128
    and-int/lit8 v0, v2, 0x10

    if-eqz v0, :cond_167

    .line 1129
    invoke-virtual {p0, p1, p2, v4}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1130
    array-length v9, v8

    const/4 v0, 0x0

    :goto_15d
    if-ge v0, v9, :cond_167

    aget-object v10, v8, v0

    .line 1131
    invoke-virtual {v10, p1, p2, v4}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1130
    add-int/lit8 v0, v0, 0x1

    goto :goto_15d

    .line 1135
    :cond_167
    and-int/lit8 v0, v2, 0x20

    if-eqz v0, :cond_17a

    .line 1136
    invoke-virtual {p0, p1, p2, p3}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1137
    array-length v9, v8

    const/4 v0, 0x0

    :goto_170
    if-ge v0, v9, :cond_17a

    aget-object v10, v8, v0

    .line 1138
    invoke-virtual {v10, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1137
    add-int/lit8 v0, v0, 0x1

    goto :goto_170

    .line 1142
    :cond_17a
    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_190

    .line 1145
    invoke-virtual {p1}, LD/a;->o()V

    .line 1146
    invoke-virtual {p0, p1, p2, v5}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1147
    array-length v9, v8

    const/4 v0, 0x0

    :goto_186
    if-ge v0, v9, :cond_190

    aget-object v10, v8, v0

    .line 1148
    invoke-virtual {v10, p1, p2, v5}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1147
    add-int/lit8 v0, v0, 0x1

    goto :goto_186

    .line 1152
    :cond_190
    and-int/lit16 v0, v2, 0x180

    if-eqz v0, :cond_1a6

    .line 1156
    invoke-virtual {p1}, LD/a;->n()V

    .line 1157
    invoke-virtual {p0, p1, p2, p3}, LF/Y;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1158
    array-length v9, v8

    const/4 v0, 0x0

    :goto_19c
    if-ge v0, v9, :cond_1a6

    aget-object v10, v8, v0

    .line 1159
    invoke-virtual {v10, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1158
    add-int/lit8 v0, v0, 0x1

    goto :goto_19c

    .line 1127
    :cond_1a6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_150

    .line 1163
    :cond_1aa
    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_87

    .line 1164
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1b3
    if-ge v1, v4, :cond_87

    aget-object v5, v2, v1

    .line 1165
    array-length v6, v5

    const/4 v0, 0x0

    :goto_1b9
    if-ge v0, v6, :cond_1c3

    aget-object v7, v5, v0

    .line 1166
    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1165
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b9

    .line 1164
    :cond_1c3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1b3

    .line 1172
    :pswitch_1c7
    if-nez v0, :cond_87

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_87

    .line 1173
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1d2
    if-ge v1, v4, :cond_87

    aget-object v5, v2, v1

    .line 1174
    array-length v6, v5

    const/4 v0, 0x0

    :goto_1d8
    if-ge v0, v6, :cond_1e2

    aget-object v7, v5, v0

    .line 1175
    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1174
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d8

    .line 1173
    :cond_1e2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1d2

    .line 1182
    :pswitch_1e6
    if-nez v0, :cond_87

    iget-object v0, p0, LF/Y;->n:[[LF/Q;

    if-eqz v0, :cond_87

    .line 1183
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1f1
    if-ge v1, v4, :cond_87

    aget-object v5, v2, v1

    .line 1184
    array-length v6, v5

    const/4 v0, 0x0

    :goto_1f7
    if-ge v0, v6, :cond_201

    aget-object v7, v5, v0

    .line 1185
    invoke-virtual {v7, p1, p2, p3}, LF/Q;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1184
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f7

    .line 1183
    :cond_201
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1f1

    .line 1191
    :pswitch_205
    iget-object v0, p0, LF/Y;->o:[LF/i;

    if-eqz v0, :cond_87

    .line 1192
    iget-object v1, p0, LF/Y;->o:[LF/i;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_20d
    if-ge v0, v2, :cond_87

    aget-object v4, v1, v0

    .line 1193
    invoke-interface {v4, p1, p2, p3}, LF/h;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1192
    add-int/lit8 v0, v0, 0x1

    goto :goto_20d

    .line 1198
    :pswitch_217
    iget-object v0, p0, LF/Y;->p:[LF/w;

    if-eqz v0, :cond_87

    .line 1199
    iget-object v1, p0, LF/Y;->p:[LF/w;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_21f
    if-ge v0, v2, :cond_87

    aget-object v4, v1, v0

    .line 1200
    invoke-virtual {v4, p1, p2, p3}, LF/w;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1199
    add-int/lit8 v0, v0, 0x1

    goto :goto_21f

    .line 1206
    :pswitch_229
    iget-object v0, p0, LF/Y;->q:[LF/e;

    if-eqz v0, :cond_87

    .line 1207
    iget-object v1, p0, LF/Y;->q:[LF/e;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_231
    if-ge v0, v2, :cond_87

    aget-object v4, v1, v0

    .line 1208
    invoke-virtual {v4, p1, p2, p3}, LF/e;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1207
    add-int/lit8 v0, v0, 0x1

    goto :goto_231

    .line 1213
    :pswitch_23b
    iget-object v0, p0, LF/Y;->r:[LF/V;

    if-eqz v0, :cond_87

    .line 1214
    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_243
    if-ge v0, v2, :cond_87

    aget-object v4, v1, v0

    .line 1215
    invoke-virtual {v4, p1, p2, p3}, LF/V;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1214
    add-int/lit8 v0, v0, 0x1

    goto :goto_243

    .line 1221
    :pswitch_24d
    const/high16 v0, 0x3f80

    cmpl-float v0, v2, v0

    if-eqz v0, :cond_87

    .line 1222
    invoke-direct {p0, p1, v2}, LF/Y;->a(LD/a;F)V

    goto/16 :goto_87

    .line 1226
    :pswitch_258
    sget-object v0, LF/U;->a:LF/U;

    invoke-virtual {v0, p1, p2, p3}, LF/U;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto/16 :goto_87

    .line 1238
    :cond_25f
    const/4 v0, 0x0

    goto/16 :goto_b5

    .line 1245
    :cond_262
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    goto/16 :goto_bb

    .line 1077
    :pswitch_data_268
    .packed-switch 0x1
        :pswitch_bf
        :pswitch_d1
        :pswitch_100
        :pswitch_112
        :pswitch_131
        :pswitch_1c7
        :pswitch_1e6
        :pswitch_1e6
        :pswitch_205
        :pswitch_217
        :pswitch_229
        :pswitch_229
        :pswitch_23b
        :pswitch_24d
        :pswitch_258
    .end packed-switch
.end method

.method public a(LF/m;)V
    .registers 2
    .parameter

    .prologue
    .line 1383
    iput-object p1, p0, LF/Y;->s:LF/m;

    .line 1384
    return-void
.end method

.method public a(Lo/U;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 842
    iget-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    if-nez v0, :cond_17

    .line 843
    invoke-static {}, LR/o;->d()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v0

    iget-object v1, p0, LF/Y;->w:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    iget-object v2, p0, LF/Y;->A:LA/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v0

    iput-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    .line 846
    :cond_17
    iget-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    invoke-virtual {p1}, Lo/U;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bE;->d(I)F

    move-result v0

    .line 847
    cmpg-float v1, v0, v3

    if-gez v1, :cond_2a

    .line 848
    invoke-virtual {p1}, Lo/U;->g()I

    move-result v0

    int-to-float v0, v0

    .line 850
    :cond_2a
    invoke-virtual {p1, v0}, Lo/U;->a(F)V

    .line 851
    iget-object v0, p0, LF/Y;->z:Lcom/google/android/maps/driveabout/vector/bE;

    invoke-virtual {p1}, Lo/U;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bE;->e(I)F

    move-result v0

    .line 852
    cmpg-float v1, v0, v3

    if-gez v1, :cond_48

    .line 861
    const/16 v0, 0x16

    invoke-virtual {p1}, Lo/U;->j()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    .line 863
    :cond_48
    invoke-virtual {p1, v0}, Lo/U;->b(F)V

    .line 864
    return-void
.end method

.method public a(Ly/b;)V
    .registers 6
    .parameter

    .prologue
    .line 902
    iget-object v0, p0, LF/Y;->F:Ly/b;

    invoke-static {p1, v0}, Ly/b;->a(Ly/b;Ly/b;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 911
    :goto_8
    return-void

    .line 905
    :cond_9
    iput-object p1, p0, LF/Y;->F:Ly/b;

    .line 906
    const/4 v0, 0x0

    move v1, v0

    :goto_d
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2f

    .line 907
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/c;

    invoke-virtual {v0}, Ly/c;->a()Lo/n;

    move-result-object v0

    .line 908
    iget-object v2, p0, LF/Y;->t:Ljava/util/ArrayList;

    new-instance v3, Ly/c;

    invoke-direct {v3, v0, p1}, Ly/c;-><init>(Lo/n;Ly/b;)V

    invoke-virtual {v2, v1, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 906
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 910
    :cond_2f
    const/4 v0, -0x1

    iput v0, p0, LF/Y;->E:I

    goto :goto_8
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 486
    iget-boolean v0, p0, LF/Y;->M:Z

    if-ne v0, p1, :cond_5

    .line 494
    :goto_4
    return-void

    .line 489
    :cond_5
    iput-boolean p1, p0, LF/Y;->M:Z

    goto :goto_4
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 831
    iget-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    iget-object v0, p0, LF/Y;->G:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aF;)Z
    .registers 3
    .parameter

    .prologue
    .line 872
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 873
    const/4 v0, 0x0

    .line 876
    :goto_9
    return v0

    .line 875
    :cond_a
    iget-object v0, p0, LF/Y;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/aF;->a(Ljava/util/Iterator;)V

    .line 876
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .registers 6
    .parameter

    .prologue
    .line 1373
    iget-wide v0, p0, LF/Y;->H:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_14

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, LF/Y;->H:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public a(Ljava/util/Set;)Z
    .registers 3
    .parameter

    .prologue
    .line 880
    iget-object v0, p0, LF/Y;->u:Ljava/util/Set;

    if-eqz v0, :cond_e

    .line 881
    iget-object v0, p0, LF/Y;->u:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 884
    const/4 v0, 0x0

    iput-object v0, p0, LF/Y;->u:Ljava/util/Set;

    .line 885
    const/4 v0, 0x1

    .line 887
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public b()Lo/aq;
    .registers 2

    .prologue
    .line 1252
    iget-object v0, p0, LF/Y;->v:Lo/aq;

    return-object v0
.end method

.method public b(J)V
    .registers 7
    .parameter

    .prologue
    const-wide/16 v2, 0x0

    .line 1396
    cmp-long v0, p1, v2

    if-gez v0, :cond_7

    .line 1403
    :cond_6
    :goto_6
    return-void

    .line 1399
    :cond_7
    iget-wide v0, p0, LF/Y;->H:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_13

    iget-wide v0, p0, LF/Y;->H:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_6

    .line 1400
    :cond_13
    iput-wide p1, p0, LF/Y;->H:J

    .line 1401
    const-wide/32 v0, 0xea60

    add-long/2addr v0, p1

    iput-wide v0, p0, LF/Y;->I:J

    goto :goto_6
.end method

.method public b(LD/a;)V
    .registers 10
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1319
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, p0, LF/Y;->G:Ljava/lang/Boolean;

    .line 1322
    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_17

    .line 1323
    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_d
    if-ge v1, v3, :cond_17

    aget-object v4, v2, v1

    .line 1324
    invoke-virtual {v4, p1}, LF/P;->b(LD/a;)V

    .line 1323
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 1327
    :cond_17
    iget-object v1, p0, LF/Y;->l:[LF/i;

    if-eqz v1, :cond_29

    .line 1328
    iget-object v2, p0, LF/Y;->l:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_1f
    if-ge v1, v3, :cond_29

    aget-object v4, v2, v1

    .line 1329
    invoke-interface {v4, p1}, LF/h;->b(LD/a;)V

    .line 1328
    add-int/lit8 v1, v1, 0x1

    goto :goto_1f

    .line 1332
    :cond_29
    iget-object v1, p0, LF/Y;->m:[LF/w;

    if-eqz v1, :cond_3b

    .line 1333
    iget-object v2, p0, LF/Y;->m:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_31
    if-ge v1, v3, :cond_3b

    aget-object v4, v2, v1

    .line 1334
    invoke-virtual {v4, p1}, LF/w;->b(LD/a;)V

    .line 1333
    add-int/lit8 v1, v1, 0x1

    goto :goto_31

    .line 1337
    :cond_3b
    iget-object v1, p0, LF/Y;->n:[[LF/Q;

    if-eqz v1, :cond_57

    .line 1338
    iget-object v3, p0, LF/Y;->n:[[LF/Q;

    array-length v4, v3

    move v2, v0

    :goto_43
    if-ge v2, v4, :cond_57

    aget-object v5, v3, v2

    .line 1339
    array-length v6, v5

    move v1, v0

    :goto_49
    if-ge v1, v6, :cond_53

    aget-object v7, v5, v1

    .line 1340
    invoke-virtual {v7, p1}, LF/Q;->b(LD/a;)V

    .line 1339
    add-int/lit8 v1, v1, 0x1

    goto :goto_49

    .line 1338
    :cond_53
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_43

    .line 1344
    :cond_57
    iget-object v1, p0, LF/Y;->o:[LF/i;

    if-eqz v1, :cond_69

    .line 1345
    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    move v1, v0

    :goto_5f
    if-ge v1, v3, :cond_69

    aget-object v4, v2, v1

    .line 1346
    invoke-interface {v4, p1}, LF/h;->b(LD/a;)V

    .line 1345
    add-int/lit8 v1, v1, 0x1

    goto :goto_5f

    .line 1349
    :cond_69
    iget-object v1, p0, LF/Y;->p:[LF/w;

    if-eqz v1, :cond_7b

    .line 1350
    iget-object v2, p0, LF/Y;->p:[LF/w;

    array-length v3, v2

    move v1, v0

    :goto_71
    if-ge v1, v3, :cond_7b

    aget-object v4, v2, v1

    .line 1351
    invoke-virtual {v4, p1}, LF/w;->b(LD/a;)V

    .line 1350
    add-int/lit8 v1, v1, 0x1

    goto :goto_71

    .line 1354
    :cond_7b
    iget-object v1, p0, LF/Y;->q:[LF/e;

    if-eqz v1, :cond_8d

    .line 1355
    iget-object v2, p0, LF/Y;->q:[LF/e;

    array-length v3, v2

    move v1, v0

    :goto_83
    if-ge v1, v3, :cond_8d

    aget-object v4, v2, v1

    .line 1356
    invoke-virtual {v4, p1}, LF/e;->b(LD/a;)V

    .line 1355
    add-int/lit8 v1, v1, 0x1

    goto :goto_83

    .line 1359
    :cond_8d
    iget-object v1, p0, LF/Y;->s:LF/m;

    if-eqz v1, :cond_99

    .line 1360
    iget-object v1, p0, LF/Y;->s:LF/m;

    invoke-virtual {v1, p1}, LF/m;->b(LD/a;)V

    .line 1361
    const/4 v1, 0x0

    iput-object v1, p0, LF/Y;->s:LF/m;

    .line 1364
    :cond_99
    iget-object v1, p0, LF/Y;->r:[LF/V;

    if-eqz v1, :cond_aa

    .line 1365
    iget-object v1, p0, LF/Y;->r:[LF/V;

    array-length v2, v1

    :goto_a0
    if-ge v0, v2, :cond_aa

    aget-object v3, v1, v0

    .line 1366
    invoke-virtual {v3, p1}, LF/V;->b(LD/a;)V

    .line 1365
    add-int/lit8 v0, v0, 0x1

    goto :goto_a0

    .line 1369
    :cond_aa
    return-void
.end method

.method public b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 969
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    .line 970
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    .line 971
    packed-switch v1, :pswitch_data_6a

    .line 1019
    :goto_b
    :pswitch_b
    return-void

    .line 973
    :pswitch_c
    invoke-static {p1, v0}, LF/P;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 976
    :pswitch_10
    invoke-static {p1, p3}, LF/a;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_b

    .line 979
    :pswitch_14
    invoke-static {p1, v0}, LF/w;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 982
    :pswitch_18
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    iget-object v2, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, LF/Q;->a(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 985
    :pswitch_26
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    iget-object v2, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, LF/Q;->b(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 988
    :pswitch_34
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    invoke-static {p1, v1, v0}, LF/Q;->a(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 992
    :pswitch_3c
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    iget-object v2, p0, LF/Y;->v:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->b()I

    move-result v2

    invoke-static {p1, v1, v2, v0}, LF/Q;->c(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 997
    :pswitch_4a
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    invoke-static {p1, v1, v0}, LF/Q;->b(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 1001
    :pswitch_52
    invoke-static {p1, p3}, LF/a;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_b

    .line 1004
    :pswitch_56
    invoke-static {p1, v0}, LF/w;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 1007
    :pswitch_5a
    invoke-static {p1, v1}, LF/e;->a(LD/a;I)V

    goto :goto_b

    .line 1010
    :pswitch_5e
    invoke-static {p1, v1}, LF/e;->a(LD/a;I)V

    goto :goto_b

    .line 1013
    :pswitch_62
    invoke-static {p1, v0}, LF/V;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    goto :goto_b

    .line 1016
    :pswitch_66
    invoke-static {p1, p3}, LF/U;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_b

    .line 971
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_c
        :pswitch_10
        :pswitch_14
        :pswitch_18
        :pswitch_26
        :pswitch_34
        :pswitch_3c
        :pswitch_4a
        :pswitch_52
        :pswitch_56
        :pswitch_5e
        :pswitch_5a
        :pswitch_62
        :pswitch_b
        :pswitch_66
    .end packed-switch
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .registers 6
    .parameter

    .prologue
    .line 1378
    iget-wide v0, p0, LF/Y;->I:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_14

    invoke-interface {p1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-wide v2, p0, LF/Y;->I:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public c()LA/c;
    .registers 2

    .prologue
    .line 1262
    iget-object v0, p0, LF/Y;->A:LA/c;

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 1257
    iget v0, p0, LF/Y;->B:I

    return v0
.end method

.method public e()LF/m;
    .registers 2

    .prologue
    .line 1388
    iget-object v0, p0, LF/Y;->s:LF/m;

    return-object v0
.end method

.method public f()Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1407
    iget-object v1, p0, LF/Y;->k:[LF/P;

    if-eqz v1, :cond_14

    .line 1408
    iget-object v2, p0, LF/Y;->k:[LF/P;

    array-length v3, v2

    move v1, v0

    :goto_9
    if-ge v1, v3, :cond_14

    aget-object v4, v2, v1

    .line 1409
    invoke-virtual {v4}, LF/P;->c()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1410
    const/4 v0, 0x1

    .line 1414
    :cond_14
    return v0

    .line 1408
    :cond_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_9
.end method

.method public g()V
    .registers 5

    .prologue
    .line 1419
    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_12

    .line 1420
    iget-object v1, p0, LF/Y;->k:[LF/P;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    .line 1421
    invoke-virtual {v3}, LF/P;->e()V

    .line 1420
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1424
    :cond_12
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 1496
    iget v0, p0, LF/Y;->y:I

    return v0
.end method

.method public i()I
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 1501
    .line 1502
    iget-object v0, p0, LF/Y;->k:[LF/P;

    if-eqz v0, :cond_18

    .line 1503
    iget-object v4, p0, LF/Y;->k:[LF/P;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_a
    if-ge v2, v5, :cond_19

    aget-object v3, v4, v2

    .line 1504
    invoke-virtual {v3}, LF/P;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 1503
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_a

    :cond_18
    move v0, v1

    .line 1507
    :cond_19
    iget-object v2, p0, LF/Y;->m:[LF/w;

    if-eqz v2, :cond_2f

    .line 1508
    iget-object v4, p0, LF/Y;->m:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_21
    if-ge v2, v5, :cond_2f

    aget-object v3, v4, v2

    .line 1509
    invoke-virtual {v3}, LF/w;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 1508
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_21

    .line 1512
    :cond_2f
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    if-eqz v2, :cond_4f

    .line 1513
    iget-object v4, p0, LF/Y;->n:[[LF/Q;

    array-length v5, v4

    move v3, v1

    :goto_37
    if-ge v3, v5, :cond_4f

    aget-object v6, v4, v3

    .line 1514
    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_3e
    if-ge v0, v7, :cond_4a

    aget-object v8, v6, v0

    .line 1515
    invoke-virtual {v8}, LF/Q;->a()I

    move-result v8

    add-int/2addr v2, v8

    .line 1514
    add-int/lit8 v0, v0, 0x1

    goto :goto_3e

    .line 1513
    :cond_4a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_37

    .line 1519
    :cond_4f
    iget-object v2, p0, LF/Y;->p:[LF/w;

    if-eqz v2, :cond_65

    .line 1520
    iget-object v4, p0, LF/Y;->p:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_57
    if-ge v2, v5, :cond_65

    aget-object v3, v4, v2

    .line 1521
    invoke-virtual {v3}, LF/w;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 1520
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_57

    .line 1524
    :cond_65
    iget-object v2, p0, LF/Y;->q:[LF/e;

    if-eqz v2, :cond_7b

    .line 1525
    iget-object v4, p0, LF/Y;->q:[LF/e;

    array-length v5, v4

    move v2, v1

    :goto_6d
    if-ge v2, v5, :cond_7b

    aget-object v3, v4, v2

    .line 1526
    invoke-virtual {v3}, LF/e;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 1525
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_6d

    .line 1529
    :cond_7b
    iget-object v2, p0, LF/Y;->r:[LF/V;

    if-eqz v2, :cond_91

    .line 1530
    iget-object v4, p0, LF/Y;->r:[LF/V;

    array-length v5, v4

    move v2, v1

    :goto_83
    if-ge v2, v5, :cond_91

    aget-object v3, v4, v2

    .line 1531
    invoke-virtual {v3}, LF/V;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 1530
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_83

    .line 1534
    :cond_91
    iget-object v2, p0, LF/Y;->l:[LF/i;

    if-eqz v2, :cond_a7

    .line 1535
    iget-object v4, p0, LF/Y;->l:[LF/i;

    array-length v5, v4

    move v2, v1

    :goto_99
    if-ge v2, v5, :cond_a7

    aget-object v3, v4, v2

    .line 1536
    invoke-virtual {v3}, LF/i;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 1535
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_99

    .line 1539
    :cond_a7
    iget-object v2, p0, LF/Y;->o:[LF/i;

    if-eqz v2, :cond_ba

    .line 1540
    iget-object v2, p0, LF/Y;->o:[LF/i;

    array-length v3, v2

    :goto_ae
    if-ge v1, v3, :cond_ba

    aget-object v4, v2, v1

    .line 1541
    invoke-virtual {v4}, LF/i;->a()I

    move-result v4

    add-int/2addr v0, v4

    .line 1540
    add-int/lit8 v1, v1, 0x1

    goto :goto_ae

    .line 1545
    :cond_ba
    return v0
.end method

.method public j()I
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 1550
    const/16 v0, 0x100

    .line 1551
    iget-object v2, p0, LF/Y;->k:[LF/P;

    if-eqz v2, :cond_1b

    .line 1552
    const/16 v0, 0x110

    .line 1553
    iget-object v4, p0, LF/Y;->k:[LF/P;

    array-length v5, v4

    move v2, v1

    :goto_d
    if-ge v2, v5, :cond_1b

    aget-object v3, v4, v2

    .line 1554
    invoke-virtual {v3}, LF/P;->b()I

    move-result v3

    add-int/2addr v3, v0

    .line 1553
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_d

    .line 1557
    :cond_1b
    iget-object v2, p0, LF/Y;->m:[LF/w;

    if-eqz v2, :cond_33

    .line 1558
    add-int/lit8 v0, v0, 0x10

    .line 1559
    iget-object v4, p0, LF/Y;->m:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_25
    if-ge v2, v5, :cond_33

    aget-object v3, v4, v2

    .line 1560
    invoke-virtual {v3}, LF/w;->b()I

    move-result v3

    add-int/2addr v3, v0

    .line 1559
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_25

    .line 1563
    :cond_33
    iget-object v2, p0, LF/Y;->n:[[LF/Q;

    if-eqz v2, :cond_55

    .line 1564
    add-int/lit8 v0, v0, 0x10

    .line 1565
    iget-object v4, p0, LF/Y;->n:[[LF/Q;

    array-length v5, v4

    move v3, v1

    :goto_3d
    if-ge v3, v5, :cond_55

    aget-object v6, v4, v3

    .line 1566
    array-length v7, v6

    move v2, v0

    move v0, v1

    :goto_44
    if-ge v0, v7, :cond_50

    aget-object v8, v6, v0

    .line 1567
    invoke-virtual {v8}, LF/Q;->b()I

    move-result v8

    add-int/2addr v2, v8

    .line 1566
    add-int/lit8 v0, v0, 0x1

    goto :goto_44

    .line 1565
    :cond_50
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_3d

    .line 1571
    :cond_55
    iget-object v2, p0, LF/Y;->p:[LF/w;

    if-eqz v2, :cond_6d

    .line 1572
    add-int/lit8 v0, v0, 0x10

    .line 1573
    iget-object v4, p0, LF/Y;->p:[LF/w;

    array-length v5, v4

    move v2, v1

    :goto_5f
    if-ge v2, v5, :cond_6d

    aget-object v3, v4, v2

    .line 1574
    invoke-virtual {v3}, LF/w;->b()I

    move-result v3

    add-int/2addr v3, v0

    .line 1573
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_5f

    .line 1577
    :cond_6d
    iget-object v2, p0, LF/Y;->q:[LF/e;

    if-eqz v2, :cond_85

    .line 1578
    add-int/lit8 v0, v0, 0x10

    .line 1579
    iget-object v4, p0, LF/Y;->q:[LF/e;

    array-length v5, v4

    move v2, v1

    :goto_77
    if-ge v2, v5, :cond_85

    aget-object v3, v4, v2

    .line 1580
    invoke-virtual {v3}, LF/e;->b()I

    move-result v3

    add-int/2addr v3, v0

    .line 1579
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_77

    .line 1583
    :cond_85
    iget-object v2, p0, LF/Y;->r:[LF/V;

    if-eqz v2, :cond_9d

    .line 1584
    add-int/lit8 v0, v0, 0x10

    .line 1585
    iget-object v4, p0, LF/Y;->r:[LF/V;

    array-length v5, v4

    move v2, v1

    :goto_8f
    if-ge v2, v5, :cond_9d

    aget-object v3, v4, v2

    .line 1586
    invoke-virtual {v3}, LF/V;->b()I

    move-result v3

    add-int/2addr v3, v0

    .line 1585
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_8f

    .line 1589
    :cond_9d
    iget-object v2, p0, LF/Y;->l:[LF/i;

    if-eqz v2, :cond_b5

    .line 1590
    add-int/lit8 v0, v0, 0x10

    .line 1591
    iget-object v4, p0, LF/Y;->l:[LF/i;

    array-length v5, v4

    move v2, v1

    :goto_a7
    if-ge v2, v5, :cond_b5

    aget-object v3, v4, v2

    .line 1592
    invoke-virtual {v3}, LF/i;->b()I

    move-result v3

    add-int/2addr v3, v0

    .line 1591
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_a7

    .line 1595
    :cond_b5
    iget-object v2, p0, LF/Y;->o:[LF/i;

    if-eqz v2, :cond_cc

    .line 1596
    add-int/lit8 v0, v0, 0x10

    .line 1597
    iget-object v3, p0, LF/Y;->o:[LF/i;

    array-length v4, v3

    :goto_be
    if-ge v1, v4, :cond_cc

    aget-object v2, v3, v1

    .line 1598
    invoke-virtual {v2}, LF/i;->b()I

    move-result v2

    add-int/2addr v2, v0

    .line 1597
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_be

    .line 1602
    :cond_cc
    iget v1, p0, LF/Y;->E:I

    .line 1603
    const/4 v2, -0x1

    if-ne v1, v2, :cond_d7

    .line 1604
    invoke-direct {p0}, LF/Y;->k()I

    move-result v1

    .line 1605
    iput v1, p0, LF/Y;->E:I

    .line 1607
    :cond_d7
    add-int/2addr v0, v1

    .line 1610
    return v0
.end method
