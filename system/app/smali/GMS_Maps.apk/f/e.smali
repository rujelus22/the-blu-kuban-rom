.class public LF/e;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final b:Lo/T;

.field private static final c:Ljava/util/Comparator;


# instance fields
.field private final d:LE/o;

.field private final e:LE/a;

.field private f:Lh/p;

.field private final g:LE/d;

.field private final h:LE/d;

.field private final i:LE/e;

.field private final j:Lo/T;

.field private final k:Lo/T;

.field private final l:Lo/T;

.field private final m:Lo/T;

.field private final n:Lo/T;

.field private final o:Lo/T;

.field private final p:Lo/T;

.field private final q:Lo/T;

.field private final r:Lo/T;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const v2, 0xb504

    .line 65
    new-instance v0, Lo/T;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lo/T;-><init>(III)V

    sput-object v0, LF/e;->b:Lo/T;

    .line 101
    new-instance v0, LF/f;

    invoke-direct {v0}, LF/f;-><init>()V

    sput-object v0, LF/e;->c:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(LF/g;Ljava/util/Set;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 245
    invoke-direct {p0, p2}, LF/i;-><init>(Ljava/util/Set;)V

    .line 120
    new-instance v0, Lh/p;

    invoke-direct {v0}, Lh/p;-><init>()V

    iput-object v0, p0, LF/e;->f:Lh/p;

    .line 258
    new-instance v0, LE/r;

    iget v1, p1, LF/g;->a:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/e;->d:LE/o;

    .line 259
    new-instance v0, LE/c;

    iget v1, p1, LF/g;->a:I

    invoke-direct {v0, v1}, LE/c;-><init>(I)V

    iput-object v0, p0, LF/e;->e:LE/a;

    .line 260
    new-instance v0, LE/f;

    iget v1, p1, LF/g;->c:I

    iget v2, p1, LF/g;->b:I

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/e;->g:LE/d;

    .line 261
    new-instance v0, LE/f;

    iget v1, p1, LF/g;->d:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/e;->h:LE/d;

    .line 262
    new-instance v0, LE/d;

    iget v1, p1, LF/g;->c:I

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    iput-object v0, p0, LF/e;->i:LE/e;

    .line 264
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->j:Lo/T;

    .line 265
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->k:Lo/T;

    .line 266
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->l:Lo/T;

    .line 267
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->m:Lo/T;

    .line 268
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->n:Lo/T;

    .line 269
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->o:Lo/T;

    .line 270
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->p:Lo/T;

    .line 271
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->q:Lo/T;

    .line 272
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/e;->r:Lo/T;

    .line 273
    return-void
.end method

.method private static final a(I)I
    .registers 5
    .parameter

    .prologue
    .line 544
    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    .line 545
    shr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    .line 546
    and-int/lit16 v2, p0, 0xff

    .line 547
    add-int/lit16 v0, v0, 0x2fd

    shr-int/lit8 v0, v0, 0x2

    .line 548
    add-int/lit16 v1, v1, 0x2fd

    shr-int/lit8 v1, v1, 0x2

    .line 549
    add-int/lit16 v2, v2, 0x2fd

    shr-int/lit8 v2, v2, 0x2

    .line 550
    const/high16 v3, -0x100

    and-int/2addr v3, p0

    .line 551
    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    return v0
.end method

.method private static a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 623
    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    .line 624
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 625
    const v1, 0xffffff

    and-int/2addr v1, p0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method private static a(ILo/T;)I
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 603
    const/high16 v0, -0x100

    and-int v1, p0, v0

    .line 604
    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v2, v0, 0xff

    .line 605
    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 v3, v0, 0xff

    .line 606
    and-int/lit16 v4, p0, 0xff

    .line 607
    sget-object v0, LF/e;->b:Lo/T;

    invoke-static {p1, v0}, Lo/T;->b(Lo/T;Lo/T;)F

    move-result v0

    invoke-virtual {p1}, Lo/T;->i()F

    move-result v5

    div-float/2addr v0, v5

    float-to-int v0, v0

    .line 608
    if-gez v0, :cond_1d

    .line 613
    neg-int v0, v0

    .line 615
    :cond_1d
    mul-int/lit16 v0, v0, 0x4ccc

    shr-int/lit8 v0, v0, 0x10

    const v5, 0xb333

    add-int/2addr v0, v5

    .line 616
    mul-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    .line 617
    mul-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x10

    .line 618
    mul-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x10

    .line 619
    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, v3, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;)LF/e;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 156
    const/4 v0, 0x0

    .line 157
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->b()Z

    move-result v1

    if-eqz v1, :cond_a4

    .line 158
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    invoke-virtual {v0}, Ln/q;->j()Ln/e;

    move-result-object v0

    move-object v2, v0

    .line 162
    :goto_11
    new-instance v4, Ljava/util/ArrayList;

    const/16 v0, 0x80

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 163
    new-instance v5, LF/g;

    invoke-direct {v5}, LF/g;-><init>()V

    .line 164
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 165
    :goto_22
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 166
    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v1

    .line 167
    instance-of v0, v1, Lo/g;

    if-eqz v0, :cond_39

    move-object v0, v1

    .line 168
    check-cast v0, Lo/g;

    .line 169
    invoke-static {v0, v5}, LF/e;->a(Lo/g;LF/g;)Z

    move-result v7

    if-nez v7, :cond_68

    .line 197
    :cond_39
    sget-object v0, LF/e;->c:Ljava/util/Comparator;

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 200
    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->b()D

    move-result-wide v2

    invoke-static {v2, v3}, Lo/T;->a(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 202
    new-instance v3, LF/e;

    invoke-direct {v3, v5, v6}, LF/e;-><init>(LF/g;Ljava/util/Set;)V

    .line 203
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_58
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/g;

    .line 204
    invoke-direct {v3, p0, v1, v0, v2}, LF/e;->a(Lo/aq;Lo/ad;Lo/g;F)V

    goto :goto_58

    .line 172
    :cond_68
    invoke-interface {v1}, Lo/n;->l()[I

    move-result-object v7

    array-length v8, v7

    move v1, v3

    :goto_6e
    if-ge v1, v8, :cond_7f

    aget v9, v7, v1

    .line 173
    if-ltz v9, :cond_7c

    array-length v10, p1

    if-ge v9, v10, :cond_7c

    .line 174
    aget-object v9, p1, v9

    invoke-virtual {v6, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_7c
    add-int/lit8 v1, v1, 0x1

    goto :goto_6e

    .line 181
    :cond_7f
    invoke-virtual {v0}, Lo/g;->d()Z

    move-result v1

    if-nez v1, :cond_91

    if-eqz v2, :cond_9b

    invoke-virtual {v0}, Lo/g;->a()Lo/o;

    move-result-object v1

    invoke-interface {v2, v1}, Ln/e;->a(Lo/o;)Z

    move-result v1

    if-eqz v1, :cond_9b

    :cond_91
    const/4 v1, 0x1

    .line 185
    :goto_92
    if-nez v1, :cond_97

    .line 186
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_97
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto :goto_22

    :cond_9b
    move v1, v3

    .line 181
    goto :goto_92

    .line 206
    :cond_9d
    invoke-direct {v3}, LF/e;->c()V

    .line 207
    invoke-virtual {v3, p0}, LF/e;->a(Lo/aq;)V

    .line 208
    return-object v3

    :cond_a4
    move-object v2, v0

    goto/16 :goto_11
.end method

.method public static a(LD/a;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x1

    .line 324
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 326
    invoke-virtual {p0}, LD/a;->u()V

    .line 327
    invoke-virtual {p0}, LD/a;->v()V

    .line 328
    invoke-interface {v0, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glPolygonOffsetx(II)V

    .line 330
    const/16 v1, 0xb

    if-ne p1, v1, :cond_1e

    .line 333
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 334
    const/16 v1, 0x201

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    .line 343
    :cond_1d
    :goto_1d
    return-void

    .line 335
    :cond_1e
    const/16 v1, 0xc

    if-ne p1, v1, :cond_1d

    .line 338
    invoke-virtual {p0}, LD/a;->n()V

    .line 339
    const/16 v1, 0x203

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDepthFunc(I)V

    .line 340
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 341
    invoke-interface {v0, v3}, Ljavax/microedition/khronos/opengles/GL10;->glLineWidthx(I)V

    goto :goto_1d
.end method

.method private a(Lo/T;Lo/T;Lo/T;Lo/T;II)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 511
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    .line 526
    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p1, p5}, LE/o;->a(Lo/T;I)V

    .line 527
    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p3, p5}, LE/o;->a(Lo/T;I)V

    .line 528
    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p2, p5}, LE/o;->a(Lo/T;I)V

    .line 529
    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1, p4, p5}, LE/o;->a(Lo/T;I)V

    .line 530
    iget-object v1, p0, LF/e;->i:LE/e;

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v4, v0, 0x2

    invoke-interface {v1, v2, v0, v3, v4}, LE/e;->a(IIII)V

    .line 531
    iget-object v1, p0, LF/e;->h:LE/d;

    add-int/lit8 v2, v0, 0x1

    int-to-short v2, v2

    add-int/lit8 v0, v0, 0x3

    int-to-short v0, v0

    invoke-virtual {v1, v2, v0}, LE/d;->a(SS)V

    .line 534
    iget-object v0, p0, LF/e;->r:Lo/T;

    invoke-static {p2, p1, v0}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 535
    iget-object v0, p0, LF/e;->e:LE/a;

    iget-object v1, p0, LF/e;->r:Lo/T;

    invoke-static {p6, v1}, LF/e;->a(ILo/T;)I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, LE/a;->b(II)V

    .line 537
    return-void
.end method

.method private a(Lo/aq;Lo/ad;Lo/g;F)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 398
    invoke-virtual/range {p3 .. p3}, Lo/g;->e()Lo/aj;

    move-result-object v1

    .line 399
    invoke-virtual/range {p3 .. p3}, Lo/g;->b()Lo/aF;

    move-result-object v10

    .line 400
    invoke-virtual {v10}, Lo/aF;->a()I

    move-result v11

    .line 401
    invoke-virtual {v1}, Lo/aj;->c()I

    move-result v2

    .line 402
    if-eqz v11, :cond_14

    if-nez v2, :cond_15

    .line 480
    :cond_14
    return-void

    .line 409
    :cond_15
    invoke-virtual/range {p2 .. p2}, Lo/ad;->d()Lo/T;

    move-result-object v12

    .line 410
    invoke-virtual/range {p2 .. p2}, Lo/ad;->g()I

    move-result v6

    .line 413
    invoke-virtual/range {p3 .. p3}, Lo/g;->f()I

    move-result v3

    .line 414
    invoke-virtual/range {p3 .. p3}, Lo/g;->g()I

    move-result v13

    .line 415
    iget-object v4, p0, LF/e;->p:Lo/T;

    const/4 v5, 0x0

    const/4 v7, 0x0

    int-to-float v3, v3

    mul-float v3, v3, p4

    float-to-int v3, v3

    invoke-virtual {v4, v5, v7, v3}, Lo/T;->a(III)V

    .line 417
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lo/aj;->a(I)I

    move-result v3

    const/16 v4, 0xa0

    invoke-static {v3, v4}, LF/e;->a(II)I

    move-result v7

    .line 418
    const/4 v3, 0x1

    if-le v2, v3, :cond_120

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lo/aj;->a(I)I

    move-result v1

    const/16 v2, 0xa0

    invoke-static {v1, v2}, LF/e;->a(II)I

    move-result v1

    move v8, v1

    .line 421
    :goto_4a
    const/4 v1, 0x0

    move v9, v1

    :goto_4c
    if-ge v9, v11, :cond_14

    .line 422
    iget-object v1, p0, LF/e;->d:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    .line 427
    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->k:Lo/T;

    iget-object v4, p0, LF/e;->l:Lo/T;

    invoke-virtual {v10, v9, v2, v3, v4}, Lo/aF;->a(ILo/T;Lo/T;Lo/T;)V

    .line 428
    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->j:Lo/T;

    invoke-static {v2, v12, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 429
    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->k:Lo/T;

    invoke-static {v2, v12, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 430
    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->l:Lo/T;

    invoke-static {v2, v12, v3}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 431
    if-eqz v13, :cond_9a

    .line 433
    iget-object v2, p0, LF/e;->q:Lo/T;

    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v5, v13

    mul-float v5, v5, p4

    float-to-int v5, v5

    invoke-virtual {v2, v3, v4, v5}, Lo/T;->a(III)V

    .line 434
    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->q:Lo/T;

    iget-object v4, p0, LF/e;->j:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 435
    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->q:Lo/T;

    iget-object v4, p0, LF/e;->k:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 436
    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->q:Lo/T;

    iget-object v4, p0, LF/e;->l:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 440
    :cond_9a
    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->p:Lo/T;

    iget-object v4, p0, LF/e;->m:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 441
    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->p:Lo/T;

    iget-object v4, p0, LF/e;->n:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 442
    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->p:Lo/T;

    iget-object v4, p0, LF/e;->o:Lo/T;

    invoke-static {v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 454
    iget-object v2, p0, LF/e;->d:LE/o;

    iget-object v3, p0, LF/e;->m:Lo/T;

    invoke-virtual {v2, v3, v6}, LE/o;->a(Lo/T;I)V

    .line 455
    iget-object v2, p0, LF/e;->d:LE/o;

    iget-object v3, p0, LF/e;->n:Lo/T;

    invoke-virtual {v2, v3, v6}, LE/o;->a(Lo/T;I)V

    .line 456
    iget-object v2, p0, LF/e;->d:LE/o;

    iget-object v3, p0, LF/e;->o:Lo/T;

    invoke-virtual {v2, v3, v6}, LE/o;->a(Lo/T;I)V

    .line 457
    iget-object v2, p0, LF/e;->e:LE/a;

    const/4 v3, 0x3

    invoke-virtual {v2, v8, v3}, LE/a;->b(II)V

    .line 458
    iget-object v2, p0, LF/e;->g:LE/d;

    int-to-short v3, v1

    add-int/lit8 v4, v1, 0x1

    int-to-short v4, v4

    add-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v2, v3, v4, v1}, LE/d;->a(SSS)V

    .line 467
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lo/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_f1

    .line 468
    iget-object v2, p0, LF/e;->j:Lo/T;

    iget-object v3, p0, LF/e;->k:Lo/T;

    iget-object v4, p0, LF/e;->m:Lo/T;

    iget-object v5, p0, LF/e;->n:Lo/T;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LF/e;->a(Lo/T;Lo/T;Lo/T;Lo/T;II)V

    .line 471
    :cond_f1
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lo/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_106

    .line 472
    iget-object v2, p0, LF/e;->k:Lo/T;

    iget-object v3, p0, LF/e;->l:Lo/T;

    iget-object v4, p0, LF/e;->n:Lo/T;

    iget-object v5, p0, LF/e;->o:Lo/T;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LF/e;->a(Lo/T;Lo/T;Lo/T;Lo/T;II)V

    .line 475
    :cond_106
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v9, v1}, Lo/g;->a(II)Z

    move-result v1

    if-eqz v1, :cond_11b

    .line 476
    iget-object v2, p0, LF/e;->l:Lo/T;

    iget-object v3, p0, LF/e;->j:Lo/T;

    iget-object v4, p0, LF/e;->o:Lo/T;

    iget-object v5, p0, LF/e;->m:Lo/T;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, LF/e;->a(Lo/T;Lo/T;Lo/T;Lo/T;II)V

    .line 421
    :cond_11b
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto/16 :goto_4c

    .line 418
    :cond_120
    invoke-static {v7}, LF/e;->a(I)I

    move-result v1

    move v8, v1

    goto/16 :goto_4a
.end method

.method static a(Lo/g;LF/g;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 228
    invoke-virtual {p0}, Lo/g;->b()Lo/aF;

    move-result-object v0

    invoke-virtual {v0}, Lo/aF;->a()I

    move-result v0

    .line 229
    invoke-virtual {p0}, Lo/g;->c()I

    move-result v1

    .line 231
    mul-int/lit8 v0, v0, 0x3

    .line 232
    mul-int/lit8 v2, v1, 0x4

    .line 233
    iget v3, p1, LF/g;->a:I

    add-int/2addr v3, v0

    add-int/2addr v2, v3

    .line 234
    const/16 v3, 0x4000

    if-le v2, v3, :cond_1e

    iget v3, p1, LF/g;->a:I

    if-lez v3, :cond_1e

    .line 235
    const/4 v0, 0x0

    .line 241
    :goto_1d
    return v0

    .line 237
    :cond_1e
    iput v2, p1, LF/g;->a:I

    .line 238
    iget v2, p1, LF/g;->b:I

    add-int/2addr v0, v2

    iput v0, p1, LF/g;->b:I

    .line 239
    iget v0, p1, LF/g;->c:I

    mul-int/lit8 v2, v1, 0x6

    add-int/2addr v0, v2

    iput v0, p1, LF/g;->c:I

    .line 240
    iget v0, p1, LF/g;->d:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p1, LF/g;->d:I

    .line 241
    const/4 v0, 0x1

    goto :goto_1d
.end method

.method private c()V
    .registers 5

    .prologue
    .line 282
    iget-object v1, p0, LF/e;->g:LE/d;

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    const/4 v2, 0x0

    iget-object v3, p0, LF/e;->i:LE/e;

    invoke-interface {v3}, LE/e;->b()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LE/d;->a(LE/d;II)V

    .line 285
    const/4 v1, 0x0

    .line 286
    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    invoke-virtual {v0, v1}, LE/d;->a(LD/a;)V

    .line 288
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 579
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/e;->e:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->g:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->h:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    invoke-virtual {v0}, LE/d;->c()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 298
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 299
    iget-object v0, p0, LF/e;->e:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    .line 300
    iget-object v0, p0, LF/e;->g:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 301
    iget-object v0, p0, LF/e;->h:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 303
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x4

    const/high16 v1, 0x1

    .line 363
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-nez v0, :cond_c

    .line 390
    :cond_b
    :goto_b
    return-void

    .line 366
    :cond_c
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 367
    iget-object v0, p0, LF/e;->e:LE/a;

    invoke-virtual {v0, p1}, LE/a;->c(LD/a;)V

    .line 371
    iget-object v0, p0, LF/e;->f:Lh/p;

    if-eqz v0, :cond_3b

    .line 372
    iget-object v0, p0, LF/e;->f:Lh/p;

    invoke-virtual {v0, p1}, Lh/p;->a(LD/a;)I

    move-result v0

    .line 373
    if-ne v0, v1, :cond_33

    .line 374
    const/4 v2, 0x0

    iput-object v2, p0, LF/e;->f:Lh/p;

    .line 382
    :goto_25
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_3d

    .line 383
    iget-object v0, p0, LF/e;->g:LE/d;

    invoke-virtual {v0, p1, v4}, LE/d;->a(LD/a;I)V

    goto :goto_b

    .line 376
    :cond_33
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    goto :goto_25

    :cond_3b
    move v0, v1

    .line 379
    goto :goto_25

    .line 384
    :cond_3d
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v2

    const/16 v3, 0xc

    if-ne v2, v3, :cond_b

    .line 385
    iget-object v2, p0, LF/e;->g:LE/d;

    invoke-virtual {v2, p1, v4}, LE/d;->a(LD/a;I)V

    .line 386
    if-ne v0, v1, :cond_b

    .line 387
    iget-object v0, p0, LF/e;->h:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    goto :goto_b
.end method

.method a(Lo/aq;)V
    .registers 2
    .parameter

    .prologue
    .line 484
    return-void
.end method

.method public b()I
    .registers 3

    .prologue
    .line 590
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x160

    iget-object v1, p0, LF/e;->e:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->g:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/e;->h:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, p0, LF/e;->i:LE/e;

    check-cast v0, LE/d;

    invoke-virtual {v0}, LE/d;->d()I

    move-result v0

    add-int/2addr v0, v1

    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 312
    iget-object v0, p0, LF/e;->d:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 313
    iget-object v0, p0, LF/e;->e:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    .line 314
    iget-object v0, p0, LF/e;->g:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 315
    iget-object v0, p0, LF/e;->h:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 317
    return-void
.end method
