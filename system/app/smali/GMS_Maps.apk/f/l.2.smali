.class public LF/l;
.super LF/L;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/c;
.implements Lo/G;


# instance fields
.field private final t:Lo/U;

.field private u:Z


# direct methods
.method constructor <init>(Lo/U;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct/range {p0 .. p13}, LF/L;-><init>(Lo/n;Ly/b;Ljava/lang/String;Lo/a;Lo/a;FFZZLF/o;LF/o;[LF/O;Z)V

    .line 46
    iput-object p1, p0, LF/l;->t:Lo/U;

    .line 47
    return-void
.end method


# virtual methods
.method public a(FFLo/T;LC/a;)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, LF/l;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    invoke-virtual {p4, v0}, LC/a;->b(Lo/T;)[I

    move-result-object v0

    .line 147
    const/4 v1, 0x0

    aget v1, v0, v1

    int-to-float v1, v1

    sub-float v1, p1, v1

    .line 148
    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-float v0, v0

    sub-float v0, p2, v0

    .line 149
    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public a()Lo/U;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, LF/l;->t:Lo/U;

    return-object v0
.end method

.method public a(LC/a;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 100
    iget-object v2, p0, LF/l;->l:Lo/a;

    invoke-virtual {v2}, Lo/a;->b()Lo/T;

    move-result-object v2

    invoke-virtual {p1, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    .line 101
    aget v3, v2, v1

    int-to-float v3, v3

    .line 102
    aget v2, v2, v0

    int-to-float v2, v2

    .line 103
    iget v4, p0, LF/l;->p:F

    add-float/2addr v4, v3

    invoke-virtual {p1}, LC/a;->k()I

    move-result v5

    int-to-float v5, v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3a

    iget v4, p0, LF/l;->q:F

    add-float/2addr v3, v4

    cmpl-float v3, v3, v6

    if-ltz v3, :cond_3a

    iget v3, p0, LF/l;->r:F

    add-float/2addr v3, v2

    invoke-virtual {p1}, LC/a;->l()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3a

    iget v3, p0, LF/l;->s:F

    add-float/2addr v2, v3

    cmpl-float v2, v2, v6

    if-ltz v2, :cond_3a

    :goto_39
    return v0

    :cond_3a
    move v0, v1

    goto :goto_39
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    const-string v0, "s"

    return-object v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, LF/l;->u:Z

    .line 71
    return-void
.end method

.method public e()Lo/T;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, LF/l;->l:Lo/a;

    invoke-virtual {v0}, Lo/a;->b()Lo/T;

    move-result-object v0

    return-object v0
.end method

.method public f()Lo/D;
    .registers 2

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method public g_()V
    .registers 2

    .prologue
    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/l;->u:Z

    .line 66
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public h_()I
    .registers 4

    .prologue
    .line 111
    iget-object v0, p0, LF/l;->m:LF/o;

    invoke-virtual {v0}, LF/o;->b()F

    move-result v0

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    .line 112
    iget-object v1, p0, LF/l;->n:LF/o;

    if-eqz v1, :cond_1c

    iget-object v1, p0, LF/l;->o:LF/O;

    iget-object v1, v1, LF/O;->a:LF/N;

    sget-object v2, LF/N;->b:LF/N;

    if-ne v1, v2, :cond_1c

    .line 114
    iget-object v1, p0, LF/l;->n:LF/o;

    invoke-virtual {v1}, LF/o;->b()F

    move-result v1

    add-float/2addr v0, v1

    .line 116
    :cond_1c
    float-to-int v0, v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 126
    iget-object v0, p0, LF/l;->t:Lo/U;

    invoke-virtual {v0}, Lo/U;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 131
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 136
    const/4 v0, 0x1

    return v0
.end method
