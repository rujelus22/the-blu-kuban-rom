.class public LF/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/T;


# instance fields
.field private final a:Lo/aq;

.field private final b:Lo/ad;

.field private final c:LE/i;


# direct methods
.method public constructor <init>(Lo/aq;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, LE/i;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, LF/j;->c:LE/i;

    .line 45
    iput-object p1, p0, LF/j;->a:Lo/aq;

    .line 46
    iget-object v0, p0, LF/j;->a:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iput-object v0, p0, LF/j;->b:Lo/ad;

    .line 49
    const/high16 v0, 0x1

    mul-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x20

    .line 51
    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v2, v2}, LE/i;->a(II)V

    .line 52
    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v2, v0}, LE/i;->a(II)V

    .line 53
    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v0, v2}, LE/i;->a(II)V

    .line 54
    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, v0, v0}, LE/i;->a(II)V

    .line 58
    return-void
.end method


# virtual methods
.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 181
    return-void
.end method

.method public a(LC/a;ILjava/util/Collection;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    return-void
.end method

.method public a(LC/a;LD/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 158
    return-void
.end method

.method public a(LC/a;Ljava/util/Collection;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 189
    return-void
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 119
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 95
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 96
    iget-object v1, p0, LF/j;->b:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, LF/j;->b:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 97
    iget-object v1, p0, LF/j;->c:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 98
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 99
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 100
    return-void
.end method

.method public a(LF/m;)V
    .registers 2
    .parameter

    .prologue
    .line 148
    return-void
.end method

.method public a(Ly/b;)V
    .registers 2
    .parameter

    .prologue
    .line 115
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 153
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aF;)Z
    .registers 3
    .parameter

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 162
    const/4 v0, 0x0

    return v0
.end method

.method public b()Lo/aq;
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, LF/j;->a:Lo/aq;

    return-object v0
.end method

.method public b(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 123
    invoke-virtual {p0, p1}, LF/j;->a(LD/a;)V

    .line 124
    return-void
.end method

.method public b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 74
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-le v0, v2, :cond_8

    .line 84
    :goto_7
    return-void

    .line 77
    :cond_8
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 78
    const/16 v1, 0x303

    invoke-interface {v0, v2, v1}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 79
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 80
    invoke-virtual {p1}, LD/a;->p()V

    .line 82
    iget-object v1, p1, LD/a;->f:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 83
    const/16 v1, 0x14

    invoke-static {p1, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_7
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public c()LA/c;
    .registers 2

    .prologue
    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 138
    const/4 v0, -0x1

    return v0
.end method

.method public e()LF/m;
    .registers 2

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .registers 1

    .prologue
    .line 177
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 193
    const/4 v0, -0x1

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, LF/j;->c:LE/i;

    invoke-virtual {v0}, LE/i;->b()I

    move-result v0

    return v0
.end method

.method public j()I
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, LF/j;->c:LE/i;

    invoke-virtual {v0}, LE/i;->c()I

    move-result v0

    return v0
.end method
