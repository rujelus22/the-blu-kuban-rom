.class public final enum LF/v;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LF/v;

.field public static final enum b:LF/v;

.field public static final enum c:LF/v;

.field private static final synthetic d:[LF/v;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 47
    new-instance v0, LF/v;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, LF/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/v;->a:LF/v;

    .line 48
    new-instance v0, LF/v;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, LF/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/v;->b:LF/v;

    .line 49
    new-instance v0, LF/v;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v4}, LF/v;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/v;->c:LF/v;

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [LF/v;

    sget-object v1, LF/v;->a:LF/v;

    aput-object v1, v0, v2

    sget-object v1, LF/v;->b:LF/v;

    aput-object v1, v0, v3

    sget-object v1, LF/v;->c:LF/v;

    aput-object v1, v0, v4

    sput-object v0, LF/v;->d:[LF/v;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)LF/v;
    .registers 3
    .parameter

    .prologue
    .line 52
    packed-switch p0, :pswitch_data_14

    .line 57
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown alignment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :pswitch_b
    sget-object v0, LF/v;->a:LF/v;

    .line 55
    :goto_d
    return-object v0

    .line 54
    :pswitch_e
    sget-object v0, LF/v;->b:LF/v;

    goto :goto_d

    .line 55
    :pswitch_11
    sget-object v0, LF/v;->c:LF/v;

    goto :goto_d

    .line 52
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_b
        :pswitch_e
        :pswitch_11
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LF/v;
    .registers 2
    .parameter

    .prologue
    .line 46
    const-class v0, LF/v;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LF/v;

    return-object v0
.end method

.method public static values()[LF/v;
    .registers 1

    .prologue
    .line 46
    sget-object v0, LF/v;->d:[LF/v;

    invoke-virtual {v0}, [LF/v;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LF/v;

    return-object v0
.end method
