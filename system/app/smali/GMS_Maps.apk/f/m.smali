.class public abstract LF/m;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field protected final a:Lo/n;

.field protected final b:Lo/aj;

.field protected final c:Ly/b;

.field protected final d:F

.field protected final e:F

.field protected final f:I

.field protected final g:Z

.field protected h:Z

.field protected i:Z

.field protected j:Ljava/util/List;

.field protected k:I

.field private l:Z

.field private m:Z


# direct methods
.method protected constructor <init>(Lo/n;Ly/b;Lo/aj;FFIZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 47
    iput-boolean v0, p0, LF/m;->m:Z

    .line 48
    iput-boolean v0, p0, LF/m;->i:Z

    .line 54
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LF/m;->j:Ljava/util/List;

    .line 57
    const/high16 v0, 0x1

    iput v0, p0, LF/m;->k:I

    .line 83
    iput-object p1, p0, LF/m;->a:Lo/n;

    .line 84
    iput-object p3, p0, LF/m;->b:Lo/aj;

    .line 85
    iput-object p2, p0, LF/m;->c:Ly/b;

    .line 86
    iput p4, p0, LF/m;->d:F

    .line 87
    iput p5, p0, LF/m;->e:F

    .line 88
    iput p6, p0, LF/m;->f:I

    .line 89
    iput-boolean p7, p0, LF/m;->g:Z

    .line 90
    iput-boolean p8, p0, LF/m;->m:Z

    .line 91
    return-void
.end method

.method public static a(Lo/aj;FIIF)F
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 372
    invoke-virtual {p0}, Lo/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lo/aj;->h()Lo/ao;

    move-result-object v0

    invoke-virtual {v0}, Lo/ao;->f()I

    move-result v0

    .line 373
    :goto_e
    int-to-float v0, v0

    mul-float/2addr v0, p1

    .line 374
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 375
    mul-float/2addr v0, p4

    return v0

    .line 372
    :cond_1c
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static a(F)I
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3e80

    .line 114
    const/high16 v0, 0x3f80

    cmpg-float v0, p0, v0

    if-gez v0, :cond_1e

    cmpl-float v0, p0, v4

    if-ltz v0, :cond_1e

    .line 115
    const-wide/high16 v0, 0x40f0

    const-wide v2, 0x3ff5555560000000L

    sub-float v4, p0, v4

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 118
    :goto_1d
    return v0

    :cond_1e
    const/high16 v0, 0x1

    goto :goto_1d
.end method

.method private static a(II)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 344
    ushr-int/lit8 v0, p0, 0x18

    mul-int/2addr v0, p1

    div-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public static a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    const/high16 v0, -0x100

    .line 255
    sget-object v1, LF/n;->a:[I

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_28

    .line 265
    invoke-virtual {p0}, Lo/aj;->e()Z

    move-result v1

    if-eqz v1, :cond_24

    invoke-virtual {p0}, Lo/aj;->h()Lo/ao;

    move-result-object v1

    invoke-virtual {v1}, Lo/ao;->d()I

    move-result v1

    .line 272
    :goto_1b
    if-nez v1, :cond_26

    .line 275
    :goto_1d
    return v0

    .line 260
    :pswitch_1e
    const/4 v0, -0x1

    goto :goto_1d

    .line 262
    :pswitch_20
    const v0, -0x3f3f40

    goto :goto_1d

    :cond_24
    move v1, v0

    .line 265
    goto :goto_1b

    :cond_26
    move v0, v1

    .line 275
    goto :goto_1d

    .line 255
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_20
    .end packed-switch
.end method

.method static b(I)I
    .registers 4
    .parameter

    .prologue
    .line 307
    const/16 v0, 0xa0

    invoke-static {p0, v0}, LF/m;->a(II)I

    move-result v0

    .line 310
    invoke-static {p0}, LF/m;->d(I)I

    move-result v1

    .line 315
    const/16 v2, 0xc0

    if-lt v1, v2, :cond_13

    .line 316
    const v1, 0x808080

    or-int/2addr v0, v1

    .line 318
    :goto_12
    return v0

    :cond_13
    const v1, 0xffffff

    or-int/2addr v0, v1

    goto :goto_12
.end method

.method public static b(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 283
    sget-object v0, LF/n;->a:[I

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2a

    .line 292
    invoke-virtual {p0}, Lo/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-virtual {p0}, Lo/aj;->h()Lo/ao;

    move-result-object v0

    invoke-virtual {v0}, Lo/ao;->e()I

    move-result v0

    :goto_19
    return v0

    .line 288
    :pswitch_1a
    const/high16 v0, -0x6000

    goto :goto_19

    .line 290
    :pswitch_1d
    const/high16 v0, -0x8000

    goto :goto_19

    .line 292
    :cond_20
    invoke-static {p0, p1}, LF/m;->a(Lo/aj;Lcom/google/android/maps/driveabout/vector/q;)I

    move-result v0

    invoke-static {v0}, LF/m;->b(I)I

    move-result v0

    goto :goto_19

    .line 283
    nop

    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_1d
    .end packed-switch
.end method

.method static c(I)I
    .registers 4
    .parameter

    .prologue
    .line 326
    const/16 v0, 0xff

    invoke-static {p0, v0}, LF/m;->a(II)I

    move-result v0

    .line 329
    invoke-static {p0}, LF/m;->d(I)I

    move-result v1

    .line 333
    const/16 v2, 0xc0

    if-lt v1, v2, :cond_f

    .line 336
    :goto_e
    return v0

    :cond_f
    const v1, 0xffffff

    or-int/2addr v0, v1

    goto :goto_e
.end method

.method private static d(I)I
    .registers 3
    .parameter

    .prologue
    .line 353
    ushr-int/lit8 v0, p0, 0x10

    and-int/lit16 v0, v0, 0xff

    mul-int/lit8 v0, v0, 0x4d

    ushr-int/lit8 v1, p0, 0x8

    and-int/lit16 v1, v1, 0xff

    mul-int/lit16 v1, v1, 0x97

    add-int/2addr v0, v1

    and-int/lit16 v1, p0, 0xff

    mul-int/lit8 v1, v1, 0x1c

    add-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    .line 357
    return v0
.end method


# virtual methods
.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 105
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, LF/m;->i:Z

    .line 106
    return-void

    .line 105
    :cond_8
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public a(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;)V

    .line 185
    return-void
.end method

.method public a(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 163
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lo/aS;)Z
    .registers 4
    .parameter

    .prologue
    .line 249
    invoke-virtual {p1}, Lo/aS;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {p0}, LF/m;->o()Lo/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aR;->a(Lo/ae;)Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, LF/m;->n()Lo/ae;

    move-result-object v0

    invoke-virtual {p1, v0}, Lo/aS;->a(Lo/ae;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public a_(Z)V
    .registers 2
    .parameter

    .prologue
    .line 383
    iput-boolean p1, p0, LF/m;->h:Z

    .line 384
    return-void
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/D;->b(LD/a;)V

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/m;->l:Z

    .line 175
    return-void
.end method

.method protected l()Z
    .registers 2

    .prologue
    .line 100
    iget-boolean v0, p0, LF/m;->m:Z

    return v0
.end method

.method public abstract m()F
.end method

.method public abstract n()Lo/ae;
.end method

.method public o()Lo/ad;
    .registers 2

    .prologue
    .line 136
    invoke-virtual {p0}, LF/m;->n()Lo/ae;

    move-result-object v0

    invoke-virtual {v0}, Lo/ae;->a()Lo/ad;

    move-result-object v0

    return-object v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->l:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method

.method public q()Ly/b;
    .registers 2

    .prologue
    .line 191
    iget-object v0, p0, LF/m;->c:Ly/b;

    return-object v0
.end method

.method public r()F
    .registers 2

    .prologue
    .line 196
    iget v0, p0, LF/m;->d:F

    return v0
.end method

.method public s()F
    .registers 2

    .prologue
    .line 201
    iget v0, p0, LF/m;->e:F

    return v0
.end method

.method public t()I
    .registers 2

    .prologue
    .line 208
    iget v0, p0, LF/m;->f:I

    return v0
.end method

.method public abstract u()Ljava/lang/String;
.end method

.method public final v()Lo/n;
    .registers 2

    .prologue
    .line 223
    iget-object v0, p0, LF/m;->a:Lo/n;

    return-object v0
.end method

.method public w()Z
    .registers 2

    .prologue
    .line 239
    iget-boolean v0, p0, LF/m;->g:Z

    return v0
.end method

.method public x()Ljava/lang/Iterable;
    .registers 2

    .prologue
    .line 406
    iget-object v0, p0, LF/m;->j:Ljava/util/List;

    return-object v0
.end method
