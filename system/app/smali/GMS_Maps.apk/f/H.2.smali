.class public LF/H;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/c;
.implements Ljava/lang/Comparable;


# static fields
.field private static E:[F


# instance fields
.field private final A:I

.field private B:Z

.field private C:Lo/aQ;

.field private D:LF/I;

.field private F:Lh/p;

.field private G:Lz/r;

.field private H:Lz/r;

.field protected final a:Landroid/graphics/Bitmap;

.field protected final b:I

.field protected final c:I

.field private d:Lo/T;

.field private final e:Landroid/graphics/Bitmap;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Lcom/google/android/maps/driveabout/vector/A;

.field private i:LD/b;

.field private j:LD/b;

.field private k:LE/i;

.field private l:LE/i;

.field private m:F

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private final t:Z

.field private final u:F

.field private final v:I

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 192
    const/16 v0, 0x8

    new-array v0, v0, [F

    sput-object v0, LF/H;->E:[F

    return-void
.end method

.method public constructor <init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-boolean v0, p0, LF/H;->s:Z

    .line 234
    iput-object p1, p0, LF/H;->d:Lo/T;

    .line 235
    iput-object p2, p0, LF/H;->a:Landroid/graphics/Bitmap;

    .line 236
    iput-object p3, p0, LF/H;->e:Landroid/graphics/Bitmap;

    .line 237
    iput p4, p0, LF/H;->b:I

    .line 238
    iput p5, p0, LF/H;->c:I

    .line 239
    iput-object p6, p0, LF/H;->f:Ljava/lang/String;

    .line 240
    iput-object p7, p0, LF/H;->g:Ljava/lang/String;

    .line 241
    iput-boolean v0, p0, LF/H;->q:Z

    .line 242
    iput-boolean p8, p0, LF/H;->t:Z

    .line 248
    iget-boolean v0, p0, LF/H;->t:Z

    if-eqz v0, :cond_7e

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->y()Z

    move-result v0

    if-nez v0, :cond_7e

    .line 249
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->s()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x4370

    div-float/2addr v0, v1

    iput v0, p0, LF/H;->u:F

    .line 254
    :goto_35
    iget v0, p0, LF/H;->b:I

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->v:I

    .line 255
    iget v0, p0, LF/H;->c:I

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->w:I

    .line 256
    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_83

    .line 257
    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->x:I

    .line 259
    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->y:I

    .line 265
    :goto_61
    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_88

    .line 266
    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->z:I

    .line 268
    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, LF/H;->a(I)I

    move-result v0

    iput v0, p0, LF/H;->A:I

    .line 274
    :goto_7d
    return-void

    .line 252
    :cond_7e
    const/high16 v0, 0x3f80

    iput v0, p0, LF/H;->u:F

    goto :goto_35

    .line 262
    :cond_83
    iput v2, p0, LF/H;->x:I

    .line 263
    iput v2, p0, LF/H;->y:I

    goto :goto_61

    .line 271
    :cond_88
    iput v2, p0, LF/H;->z:I

    .line 272
    iput v2, p0, LF/H;->A:I

    goto :goto_7d
.end method

.method private a(LD/b;)LE/i;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 706
    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    .line 708
    invoke-virtual {p1}, LD/b;->b()F

    move-result v1

    .line 709
    invoke-virtual {p1}, LD/b;->c()F

    move-result v2

    .line 711
    invoke-virtual {v0, v3, v3}, LE/i;->a(FF)V

    .line 712
    invoke-virtual {v0, v3, v2}, LE/i;->a(FF)V

    .line 713
    invoke-virtual {v0, v1, v3}, LE/i;->a(FF)V

    .line 714
    invoke-virtual {v0, v1, v2}, LE/i;->a(FF)V

    .line 716
    return-object v0
.end method

.method private a(LD/a;Lz/r;II)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x1

    .line 820
    iget-object v0, p0, LF/H;->F:Lh/p;

    if-eqz v0, :cond_20

    .line 821
    iget-object v0, p0, LF/H;->F:Lh/p;

    invoke-virtual {v0, p1}, Lh/p;->a(LD/a;)I

    move-result v0

    .line 822
    if-ne v0, v2, :cond_11

    .line 823
    const/4 v1, 0x0

    iput-object v1, p0, LF/H;->F:Lh/p;

    .line 825
    :cond_11
    mul-int v1, p3, v0

    div-int/2addr v1, v2

    int-to-float v1, v1

    .line 826
    mul-int/2addr v0, p4

    div-int/2addr v0, v2

    int-to-float v0, v0

    .line 832
    :goto_18
    iget-object v2, p0, LF/H;->d:Lo/T;

    const/high16 v3, 0x3f80

    invoke-virtual {p2, v2, v1, v0, v3}, Lz/r;->a(Lo/T;FFF)V

    .line 833
    return-void

    .line 828
    :cond_20
    int-to-float v1, p3

    .line 829
    int-to-float v0, p4

    goto :goto_18
.end method

.method private b(LD/a;Landroid/graphics/Bitmap;)LD/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 690
    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/A;->h()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 691
    if-eqz v0, :cond_12

    .line 692
    invoke-virtual {v0}, LD/b;->f()V

    .line 699
    :goto_11
    return-object v0

    .line 694
    :cond_12
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    .line 695
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    .line 696
    invoke-virtual {v0, p2}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 697
    iget-object v1, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/A;->h()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_11
.end method


# virtual methods
.method public declared-synchronized a(FFLo/T;LC/a;)I
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 623
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LF/H;->d:Lo/T;

    invoke-virtual {p4, v0}, LC/a;->b(Lo/T;)[I

    move-result-object v0

    .line 624
    const/4 v1, 0x0

    aget v1, v0, v1

    iget v2, p0, LF/H;->v:I

    sub-int/2addr v1, v2

    iget v2, p0, LF/H;->x:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 625
    const/4 v2, 0x1

    aget v0, v0, v2

    iget v2, p0, LF/H;->w:I

    sub-int/2addr v0, v2

    iget v2, p0, LF/H;->y:I

    div-int/lit8 v2, v2, 0x2
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_2f

    add-int/2addr v0, v2

    .line 626
    int-to-float v2, v1

    sub-float v2, p1, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    mul-float/2addr v1, v2

    int-to-float v2, v0

    sub-float v2, p2, v2

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    monitor-exit p0

    return v0

    .line 623
    :catchall_2f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(I)I
    .registers 4
    .parameter

    .prologue
    .line 281
    iget-boolean v0, p0, LF/H;->t:Z

    if-eqz v0, :cond_16

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->y()Z

    move-result v0

    if-nez v0, :cond_16

    int-to-float v0, p1

    iget v1, p0, LF/H;->u:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result p1

    :cond_16
    return p1
.end method

.method public a(LF/H;)I
    .registers 4
    .parameter

    .prologue
    .line 353
    iget v0, p0, LF/H;->n:I

    iget v1, p1, LF/H;->n:I

    if-ne v0, v1, :cond_10

    .line 356
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    sub-int/2addr v0, v1

    .line 358
    :goto_f
    return v0

    :cond_10
    iget v0, p0, LF/H;->n:I

    iget v1, p1, LF/H;->n:I

    sub-int/2addr v0, v1

    goto :goto_f
.end method

.method public a(LD/a;Landroid/graphics/Bitmap;)LD/b;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 730
    new-instance v0, LD/b;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, LD/b;-><init>(LD/a;Z)V

    .line 731
    invoke-virtual {v0, p2}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 732
    return-object v0
.end method

.method public a(LD/a;)V
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/high16 v6, 0x3f80

    const/4 v5, 0x0

    .line 739
    iget-object v0, p0, LF/H;->G:Lz/r;

    if-nez v0, :cond_d1

    .line 740
    iget v0, p0, LF/H;->v:I

    int-to-float v0, v0

    iget v1, p0, LF/H;->x:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 741
    iget v1, p0, LF/H;->w:I

    int-to-float v1, v1

    iget v2, p0, LF/H;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 745
    const/16 v2, 0x14

    new-array v2, v2, [F

    const/4 v3, 0x0

    neg-float v4, v0

    aput v4, v2, v3

    aput v1, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v9

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    .line 752
    new-instance v0, Lz/r;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lz/r;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    iput-object v0, p0, LF/H;->G:Lz/r;

    .line 753
    iget-object v0, p0, LF/H;->G:Lz/r;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Marker for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, LF/H;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lz/r;->a(Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, LF/H;->i:LD/b;

    if-nez v0, :cond_99

    .line 755
    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, LF/H;->a(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->i:LD/b;

    .line 757
    :cond_99
    iget-object v0, p0, LF/H;->G:Lz/r;

    iget-object v1, p0, LF/H;->i:LD/b;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    .line 758
    iget-object v0, p0, LF/H;->G:Lz/r;

    new-instance v1, Lz/d;

    const/16 v3, 0x303

    invoke-direct {v1, v7, v3}, Lz/d;-><init>(II)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    .line 760
    new-array v0, v9, [F

    fill-array-data v0, :array_1a8

    .line 761
    new-instance v1, Lz/E;

    invoke-direct {v1, v8, v0}, Lz/E;-><init>(I[F)V

    .line 762
    iget-object v0, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    .line 763
    iget-object v0, p0, LF/H;->G:Lz/r;

    new-instance v1, Lz/v;

    const/16 v3, 0x9

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lz/v;-><init>([FII)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/M;)V

    .line 768
    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->a(Lz/i;)V

    .line 771
    :cond_d1
    iget-object v0, p0, LF/H;->H:Lz/r;

    if-nez v0, :cond_1a7

    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1a7

    .line 772
    iget v0, p0, LF/H;->v:I

    int-to-float v0, v0

    iget v1, p0, LF/H;->z:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 773
    iget v1, p0, LF/H;->w:I

    int-to-float v1, v1

    iget v2, p0, LF/H;->A:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 777
    const/16 v2, 0x14

    new-array v2, v2, [F

    const/4 v3, 0x0

    neg-float v4, v0

    aput v4, v2, v3

    aput v1, v2, v7

    aput v5, v2, v8

    const/4 v3, 0x3

    aput v5, v2, v3

    aput v5, v2, v9

    const/4 v3, 0x5

    neg-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x6

    sub-float v4, v1, v6

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v5, v2, v3

    const/16 v3, 0x8

    aput v5, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    const/16 v3, 0xa

    sub-float v4, v6, v0

    aput v4, v2, v3

    const/16 v3, 0xb

    aput v1, v2, v3

    const/16 v3, 0xc

    aput v5, v2, v3

    const/16 v3, 0xd

    aput v6, v2, v3

    const/16 v3, 0xe

    aput v5, v2, v3

    const/16 v3, 0xf

    sub-float v0, v6, v0

    aput v0, v2, v3

    const/16 v0, 0x10

    sub-float/2addr v1, v6

    aput v1, v2, v0

    const/16 v0, 0x11

    aput v5, v2, v0

    const/16 v0, 0x12

    aput v6, v2, v0

    const/16 v0, 0x13

    aput v6, v2, v0

    .line 784
    new-instance v0, Lz/r;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->v:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lz/r;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    iput-object v0, p0, LF/H;->H:Lz/r;

    .line 785
    iget-object v0, p0, LF/H;->H:Lz/r;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Marker shadow for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, LF/H;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lz/r;->a(Ljava/lang/String;)V

    .line 786
    iget-object v0, p0, LF/H;->j:LD/b;

    if-nez v0, :cond_168

    .line 787
    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, v0}, LF/H;->a(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->j:LD/b;

    .line 789
    :cond_168
    iget-object v0, p0, LF/H;->H:Lz/r;

    iget-object v1, p0, LF/H;->j:LD/b;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    .line 790
    iget-object v0, p0, LF/H;->H:Lz/r;

    new-instance v1, Lz/d;

    const/16 v3, 0x303

    invoke-direct {v1, v7, v3}, Lz/d;-><init>(II)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    .line 792
    new-array v0, v9, [F

    fill-array-data v0, :array_1b4

    .line 793
    new-instance v1, Lz/E;

    invoke-direct {v1, v8, v0}, Lz/E;-><init>(I[F)V

    .line 794
    iget-object v0, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/o;)V

    .line 795
    iget-object v0, p0, LF/H;->H:Lz/r;

    new-instance v1, Lz/v;

    const/16 v3, 0x9

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4}, Lz/v;-><init>([FII)V

    invoke-virtual {v0, v1}, Lz/r;->a(Lz/M;)V

    .line 800
    iget-object v0, p0, LF/H;->H:Lz/r;

    const/high16 v1, -0x3dcc

    invoke-virtual {v0, v1}, Lz/r;->a(F)V

    .line 801
    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->a(Lz/i;)V

    .line 803
    :cond_1a7
    return-void

    .line 760
    :array_1a8
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    .line 792
    :array_1b4
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v6, 0x303

    const/4 v5, 0x2

    const/4 v8, 0x0

    const/high16 v7, 0x1

    .line 537
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gt v0, v5, :cond_1f

    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    if-eqz v0, :cond_1f

    iget v0, p0, LF/H;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1f

    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, LF/H;->s:Z

    if-nez v0, :cond_20

    .line 619
    :cond_1f
    :goto_1f
    return-void

    .line 541
    :cond_20
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    .line 544
    iget-object v2, p0, LF/H;->e:Landroid/graphics/Bitmap;

    .line 545
    iget-object v3, p0, LF/H;->a:Landroid/graphics/Bitmap;

    .line 546
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_a4

    .line 547
    iget-object v0, p0, LF/H;->j:LD/b;

    if-nez v0, :cond_42

    .line 548
    if-eqz v2, :cond_1f

    .line 551
    invoke-direct {p0, p1, v2}, LF/H;->b(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->j:LD/b;

    .line 552
    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-direct {p0, v0}, LF/H;->a(LD/b;)LE/i;

    move-result-object v0

    iput-object v0, p0, LF/H;->l:LE/i;

    .line 562
    :cond_42
    :goto_42
    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 563
    monitor-enter p0

    .line 564
    :try_start_46
    iget-object v0, p0, LF/H;->d:Lo/T;

    iget v1, p0, LF/H;->m:F

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 565
    monitor-exit p0
    :try_end_4e
    .catchall {:try_start_46 .. :try_end_4e} :catchall_b7

    .line 569
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_ba

    .line 571
    const/high16 v0, -0x2d

    invoke-interface {v4, v0, v8, v8, v7}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    .line 572
    const/high16 v0, -0x5a

    invoke-interface {v4, v0, v7, v8, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatex(IIII)V

    .line 573
    iget-object v0, p0, LF/H;->l:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 574
    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-virtual {v0, v4}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 576
    iget v1, p0, LF/H;->z:I

    .line 577
    iget v0, p0, LF/H;->A:I

    .line 598
    :goto_6c
    iget-object v3, p0, LF/H;->F:Lh/p;

    if-eqz v3, :cond_ed

    .line 599
    iget-object v3, p0, LF/H;->F:Lh/p;

    invoke-virtual {v3, p1}, Lh/p;->a(LD/a;)I

    move-result v3

    .line 600
    if-ne v3, v7, :cond_7b

    .line 601
    const/4 v5, 0x0

    iput-object v5, p0, LF/H;->F:Lh/p;

    .line 603
    :cond_7b
    mul-int/2addr v1, v3

    .line 604
    mul-int/2addr v0, v3

    .line 612
    :goto_7d
    iget v3, p0, LF/H;->b:I

    shl-int/lit8 v3, v3, 0x10

    neg-int v3, v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/2addr v3, v5

    .line 613
    const/high16 v5, -0x1

    iget v6, p0, LF/H;->c:I

    shl-int/lit8 v6, v6, 0x10

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    div-int v2, v6, v2

    add-int/2addr v2, v5

    .line 614
    invoke-interface {v4, v1, v7, v0}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    .line 615
    invoke-interface {v4, v3, v8, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatex(III)V

    .line 616
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-interface {v4, v0, v8, v1}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 618
    invoke-interface {v4}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_1f

    .line 555
    :cond_a4
    iget-object v0, p0, LF/H;->i:LD/b;

    if-nez v0, :cond_42

    .line 556
    invoke-direct {p0, p1, v3}, LF/H;->b(LD/a;Landroid/graphics/Bitmap;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/H;->i:LD/b;

    .line 557
    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-direct {p0, v0}, LF/H;->a(LD/b;)LE/i;

    move-result-object v0

    iput-object v0, p0, LF/H;->k:LE/i;

    goto :goto_42

    .line 565
    :catchall_b7
    move-exception v0

    :try_start_b8
    monitor-exit p0
    :try_end_b9
    .catchall {:try_start_b8 .. :try_end_b9} :catchall_b7

    throw v0

    .line 579
    :cond_ba
    invoke-static {v4, p2}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;LC/a;)V

    .line 580
    iget-object v0, p0, LF/H;->k:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 581
    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-virtual {v0, v4}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 583
    iget v1, p0, LF/H;->x:I

    .line 584
    iget v0, p0, LF/H;->y:I

    .line 585
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v2

    if-ne v2, v5, :cond_e7

    .line 586
    const/16 v2, 0x302

    invoke-interface {v4, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 587
    const/16 v2, 0x2300

    const/16 v5, 0x2200

    const/16 v6, 0x2100

    invoke-interface {v4, v2, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 588
    const v2, 0xcccc

    invoke-interface {v4, v7, v7, v7, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    move-object v2, v3

    goto :goto_6c

    .line 591
    :cond_e7
    const/4 v2, 0x1

    invoke-interface {v4, v2, v6}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    move-object v2, v3

    goto :goto_6c

    .line 606
    :cond_ed
    shl-int/lit8 v1, v1, 0x10

    .line 607
    shl-int/lit8 v0, v0, 0x10

    goto :goto_7d
.end method

.method public a(LF/I;)V
    .registers 2
    .parameter

    .prologue
    .line 320
    iput-object p1, p0, LF/H;->D:LF/I;

    .line 321
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/A;)V
    .registers 2
    .parameter

    .prologue
    .line 366
    iput-object p1, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    .line 367
    return-void
.end method

.method public declared-synchronized a(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 329
    monitor-enter p0

    if-eqz p1, :cond_5

    .line 330
    :try_start_3
    iput-object p1, p0, LF/H;->d:Lo/T;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    .line 332
    :cond_5
    monitor-exit p0

    return-void

    .line 329
    :catchall_7
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 287
    iput-boolean p1, p0, LF/H;->q:Z

    .line 288
    return-void
.end method

.method public declared-synchronized a(LC/a;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 651
    monitor-enter p0

    :try_start_3
    iget-object v2, p0, LF/H;->a:Landroid/graphics/Bitmap;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_60

    if-nez v2, :cond_9

    .line 680
    :goto_7
    monitor-exit p0

    return v0

    .line 656
    :cond_9
    :try_start_9
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    iget-object v3, p0, LF/H;->C:Lo/aQ;

    invoke-virtual {v2, v3}, Lo/aQ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 657
    iget-boolean v0, p0, LF/H;->B:Z

    goto :goto_7

    .line 661
    :cond_18
    iget-object v2, p0, LF/H;->d:Lo/T;

    invoke-virtual {p1, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    .line 662
    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, LF/H;->v:I

    sub-int v4, v3, v4

    .line 663
    iget v3, p0, LF/H;->x:I

    add-int/2addr v3, v4

    .line 664
    const/4 v5, 0x1

    aget v2, v2, v5

    iget v5, p0, LF/H;->w:I

    sub-int v5, v2, v5

    .line 665
    iget v2, p0, LF/H;->y:I

    add-int/2addr v2, v5

    .line 668
    iget-object v6, p0, LF/H;->e:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_44

    .line 669
    iget v6, p0, LF/H;->z:I

    add-int/2addr v6, v4

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 670
    iget v6, p0, LF/H;->A:I

    add-int/2addr v6, v5

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 674
    :cond_44
    invoke-virtual {p1}, LC/a;->k()I

    move-result v6

    if-ge v4, v6, :cond_55

    if-ltz v3, :cond_55

    invoke-virtual {p1}, LC/a;->l()I

    move-result v3

    if-ge v5, v3, :cond_55

    if-ltz v2, :cond_55

    move v0, v1

    :cond_55
    iput-boolean v0, p0, LF/H;->B:Z

    .line 678
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v0

    iput-object v0, p0, LF/H;->C:Lo/aQ;

    .line 680
    iget-boolean v0, p0, LF/H;->B:Z
    :try_end_5f
    .catchall {:try_start_9 .. :try_end_5f} :catchall_60

    goto :goto_7

    .line 651
    :catchall_60
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 839
    iget-object v0, p0, LF/H;->G:Lz/r;

    if-eqz v0, :cond_9

    .line 840
    iget-object v0, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, p1}, Lz/r;->a(I)V

    .line 842
    :cond_9
    iget-object v0, p0, LF/H;->H:Lz/r;

    if-eqz v0, :cond_12

    .line 843
    iget-object v0, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, p1}, Lz/r;->a(I)V

    .line 845
    :cond_12
    return-void
.end method

.method public b(LD/a;)V
    .registers 5
    .parameter

    .prologue
    .line 809
    iget-object v0, p0, LF/H;->G:Lz/r;

    iget v1, p0, LF/H;->x:I

    iget v2, p0, LF/H;->y:I

    invoke-direct {p0, p1, v0, v1, v2}, LF/H;->a(LD/a;Lz/r;II)V

    .line 810
    iget-object v0, p0, LF/H;->H:Lz/r;

    iget v1, p0, LF/H;->z:I

    iget v2, p0, LF/H;->A:I

    invoke-direct {p0, p1, v0, v1, v2}, LF/H;->a(LD/a;Lz/r;II)V

    .line 811
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 296
    iput-boolean p1, p0, LF/H;->r:Z

    .line 297
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 300
    iget-boolean v0, p0, LF/H;->r:Z

    return v0
.end method

.method public declared-synchronized b(FFLo/T;LC/a;)Z
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 631
    monitor-enter p0

    :try_start_3
    iget-object v2, p0, LF/H;->a:Landroid/graphics/Bitmap;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_47

    if-nez v2, :cond_9

    .line 646
    :cond_7
    :goto_7
    monitor-exit p0

    return v1

    .line 636
    :cond_9
    :try_start_9
    iget-object v2, p0, LF/H;->d:Lo/T;

    invoke-virtual {p4, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    .line 637
    const/4 v3, 0x0

    aget v3, v2, v3

    iget v4, p0, LF/H;->v:I

    sub-int/2addr v3, v4

    .line 638
    iget v4, p0, LF/H;->x:I

    add-int/2addr v4, v3

    .line 639
    const/high16 v5, 0x4140

    invoke-virtual {p4}, LC/a;->m()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    .line 640
    sub-int/2addr v3, v5

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-ltz v3, :cond_7

    add-int v3, v4, v5

    int-to-float v3, v3

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_7

    .line 644
    const/4 v3, 0x1

    aget v2, v2, v3

    iget v3, p0, LF/H;->w:I

    sub-int/2addr v2, v3

    .line 645
    iget v3, p0, LF/H;->y:I
    :try_end_35
    .catchall {:try_start_9 .. :try_end_35} :catchall_47

    add-int/2addr v3, v2

    .line 646
    sub-int/2addr v2, v5

    int-to-float v2, v2

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_45

    add-int v2, v3, v5

    int-to-float v2, v2

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_45

    :goto_43
    move v1, v0

    goto :goto_7

    :cond_45
    move v0, v1

    goto :goto_43

    .line 631
    :catchall_47
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(LC/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 510
    monitor-enter p0

    :try_start_2
    iget-boolean v0, p0, LF/H;->s:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_27

    if-nez v0, :cond_8

    .line 519
    :goto_6
    monitor-exit p0

    return v2

    .line 516
    :cond_8
    const/high16 v0, 0x3f80

    :try_start_a
    invoke-virtual {p1}, LC/a;->o()F

    move-result v1

    invoke-virtual {p1, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iput v0, p0, LF/H;->m:F

    .line 517
    iget-object v0, p0, LF/H;->d:Lo/T;

    sget-object v1, LF/H;->E:[F

    invoke-virtual {p1, v0, v1}, LC/a;->a(Lo/T;[F)V

    .line 518
    sget-object v0, LF/H;->E:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    const/high16 v1, 0x4780

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LF/H;->n:I
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_27

    goto :goto_6

    .line 510
    :catchall_27
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    .line 851
    iget-object v0, p0, LF/H;->G:Lz/r;

    if-eqz v0, :cond_d

    .line 852
    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->G:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->b(Lz/i;)V

    .line 854
    :cond_d
    iget-object v0, p0, LF/H;->H:Lz/r;

    if-eqz v0, :cond_1a

    .line 855
    invoke-virtual {p1}, LD/a;->l()Lz/k;

    move-result-object v0

    iget-object v1, p0, LF/H;->H:Lz/r;

    invoke-virtual {v0, v1}, Lz/k;->b(Lz/i;)V

    .line 857
    :cond_1a
    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 336
    if-eqz p1, :cond_e

    iget-object v0, p0, LF/H;->F:Lh/p;

    if-nez v0, :cond_e

    .line 337
    new-instance v0, Lh/p;

    invoke-direct {v0}, Lh/p;-><init>()V

    iput-object v0, p0, LF/H;->F:Lh/p;

    .line 341
    :cond_d
    :goto_d
    return-void

    .line 338
    :cond_e
    if-nez p1, :cond_d

    .line 339
    const/4 v0, 0x0

    iput-object v0, p0, LF/H;->F:Lh/p;

    goto :goto_d
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 40
    check-cast p1, LF/H;

    invoke-virtual {p0, p1}, LF/H;->a(LF/H;)I

    move-result v0

    return v0
.end method

.method public d()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 409
    iput-boolean v1, p0, LF/H;->o:Z

    .line 410
    iget-boolean v0, p0, LF/H;->p:Z

    if-eqz v0, :cond_e

    .line 411
    iput-boolean v1, p0, LF/H;->p:Z

    .line 412
    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/vector/A;->c(LF/H;)V

    .line 414
    :cond_e
    return-void
.end method

.method public d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 469
    iput-boolean p1, p0, LF/H;->p:Z

    .line 470
    return-void
.end method

.method public declared-synchronized e()Lo/T;
    .registers 2

    .prologue
    .line 436
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LF/H;->d:Lo/T;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Lo/D;
    .registers 2

    .prologue
    .line 441
    const/4 v0, 0x0

    return-object v0
.end method

.method public g_()V
    .registers 2

    .prologue
    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, LF/H;->o:Z

    .line 423
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 451
    const/4 v0, 0x0

    return v0
.end method

.method public h_()I
    .registers 2

    .prologue
    .line 446
    iget v0, p0, LF/H;->w:I

    return v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 456
    iget-object v0, p0, LF/H;->f:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 461
    iget-object v0, p0, LF/H;->g:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 292
    iget-boolean v0, p0, LF/H;->q:Z

    return v0
.end method

.method public l()LF/I;
    .registers 2

    .prologue
    .line 316
    iget-object v0, p0, LF/H;->D:LF/I;

    return-object v0
.end method

.method public m()Lcom/google/android/maps/driveabout/vector/A;
    .registers 2

    .prologue
    .line 374
    iget-object v0, p0, LF/H;->h:Lcom/google/android/maps/driveabout/vector/A;

    return-object v0
.end method

.method public n()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 382
    iget-object v0, p0, LF/H;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public o()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 390
    iget-object v0, p0, LF/H;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public p()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 398
    return-object p0
.end method

.method public q()Z
    .registers 2

    .prologue
    .line 431
    iget-boolean v0, p0, LF/H;->o:Z

    return v0
.end method

.method public r()I
    .registers 3

    .prologue
    .line 479
    const/4 v0, 0x0

    .line 480
    iget-object v1, p0, LF/H;->i:LD/b;

    if-eqz v1, :cond_13

    .line 481
    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 482
    iget-object v0, p0, LF/H;->i:LD/b;

    invoke-virtual {v0}, LD/b;->h()I

    move-result v0

    .line 483
    const/4 v1, 0x0

    iput-object v1, p0, LF/H;->i:LD/b;

    .line 485
    :cond_13
    return v0
.end method

.method public s()I
    .registers 3

    .prologue
    .line 495
    const/4 v0, 0x0

    .line 496
    iget-object v1, p0, LF/H;->j:LD/b;

    if-eqz v1, :cond_13

    .line 497
    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 498
    iget-object v0, p0, LF/H;->j:LD/b;

    invoke-virtual {v0}, LD/b;->h()I

    move-result v0

    .line 499
    const/4 v1, 0x0

    iput-object v1, p0, LF/H;->j:LD/b;

    .line 501
    :cond_13
    return v0
.end method
