.class public final enum LF/s;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LF/s;

.field public static final enum b:LF/s;

.field public static final enum c:LF/s;

.field private static final synthetic d:[LF/s;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, LF/s;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, LF/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/s;->a:LF/s;

    .line 64
    new-instance v0, LF/s;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, LF/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/s;->b:LF/s;

    .line 65
    new-instance v0, LF/s;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LF/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, LF/s;->c:LF/s;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [LF/s;

    sget-object v1, LF/s;->a:LF/s;

    aput-object v1, v0, v2

    sget-object v1, LF/s;->b:LF/s;

    aput-object v1, v0, v3

    sget-object v1, LF/s;->c:LF/s;

    aput-object v1, v0, v4

    sput-object v0, LF/s;->d:[LF/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)LF/s;
    .registers 3
    .parameter

    .prologue
    .line 68
    packed-switch p0, :pswitch_data_14

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown justification"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :pswitch_b
    sget-object v0, LF/s;->a:LF/s;

    .line 71
    :goto_d
    return-object v0

    .line 70
    :pswitch_e
    sget-object v0, LF/s;->b:LF/s;

    goto :goto_d

    .line 71
    :pswitch_11
    sget-object v0, LF/s;->c:LF/s;

    goto :goto_d

    .line 68
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_b
        :pswitch_e
        :pswitch_11
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LF/s;
    .registers 2
    .parameter

    .prologue
    .line 62
    const-class v0, LF/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LF/s;

    return-object v0
.end method

.method public static values()[LF/s;
    .registers 1

    .prologue
    .line 62
    sget-object v0, LF/s;->d:[LF/s;

    invoke-virtual {v0}, [LF/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LF/s;

    return-object v0
.end method
