.class public LF/w;
.super LF/i;
.source "SourceFile"


# static fields
.field static b:F

.field private static final p:Ljava/util/Map;


# instance fields
.field private final c:Lo/aq;

.field private final d:Ljava/util/List;

.field private final e:LE/o;

.field private final f:LE/d;

.field private final g:LE/i;

.field private h:LD/b;

.field private final i:I

.field private final j:I

.field private final k:Z

.field private final l:F

.field private m:I

.field private n:Lo/o;

.field private final o:LF/C;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 107
    const/high16 v0, 0x3f80

    sput v0, LF/w;->b:F

    .line 154
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, LF/w;->p:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;I)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 653
    invoke-direct {p0, p4}, LF/i;-><init>(Ljava/util/Set;)V

    .line 654
    iput-object p1, p0, LF/w;->c:Lo/aq;

    .line 666
    new-instance v0, LE/r;

    iget v3, p2, LF/z;->a:I

    invoke-direct {v0, v3, v1}, LE/r;-><init>(IZ)V

    iput-object v0, p0, LF/w;->e:LE/o;

    .line 667
    new-instance v0, LE/f;

    iget v3, p2, LF/z;->b:I

    invoke-direct {v0, v3, v1}, LE/f;-><init>(IZ)V

    iput-object v0, p0, LF/w;->f:LE/d;

    .line 668
    new-instance v0, LE/l;

    iget v3, p2, LF/z;->a:I

    invoke-direct {v0, v3, v1}, LE/l;-><init>(IZ)V

    iput-object v0, p0, LF/w;->g:LE/i;

    .line 671
    iput-object p3, p0, LF/w;->d:Ljava/util/List;

    .line 676
    const/high16 v3, 0x3f80

    .line 677
    iget-object v0, p0, LF/w;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    invoke-virtual {v0}, LF/A;->e()I

    move-result v4

    .line 678
    if-le v4, v1, :cond_5e

    move v0, v1

    :goto_35
    iput-boolean v0, p0, LF/w;->k:Z

    .line 679
    iget-boolean v0, p0, LF/w;->k:Z

    if-eqz v0, :cond_60

    .line 682
    div-int/lit8 v0, v4, 0x10

    int-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 683
    mul-int/lit8 v4, v0, 0x2

    .line 684
    iget-object v0, p0, LF/w;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    invoke-virtual {v0}, LF/A;->d()F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 685
    add-int/2addr v4, v2

    move v0, v2

    .line 694
    :goto_57
    shl-int v5, v1, v0

    if-ge v5, v4, :cond_84

    .line 695
    add-int/lit8 v0, v0, 0x1

    goto :goto_57

    :cond_5e
    move v0, v2

    .line 678
    goto :goto_35

    .line 687
    :cond_60
    iget-object v0, p0, LF/w;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v2

    :goto_67
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    .line 688
    invoke-virtual {v0}, LF/A;->e()I

    move-result v6

    .line 689
    invoke-virtual {v0}, LF/A;->d()F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 690
    add-int v3, v4, v6

    move v4, v3

    move v3, v0

    goto :goto_67

    .line 697
    :cond_84
    iput v0, p0, LF/w;->i:I

    .line 701
    sget v0, LF/w;->b:F

    mul-float/2addr v0, v3

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    const/high16 v1, 0x4000

    add-float/2addr v0, v1

    .line 702
    float-to-int v1, v0

    const/16 v3, 0x8

    invoke-static {v1, v3}, LD/b;->c(II)I

    move-result v1

    iput v1, p0, LF/w;->j:I

    .line 703
    iget v1, p0, LF/w;->j:I

    int-to-float v1, v1

    div-float/2addr v1, v0

    iput v1, p0, LF/w;->l:F

    .line 705
    new-instance v1, LF/C;

    iget-object v3, p0, LF/w;->d:Ljava/util/List;

    iget v4, p0, LF/w;->l:F

    iget v5, p0, LF/w;->i:I

    iget-boolean v6, p0, LF/w;->k:Z

    invoke-direct {v1, v3, v4, v5, v6}, LF/C;-><init>(Ljava/util/List;FIZ)V

    iput-object v1, p0, LF/w;->o:LF/C;

    .line 707
    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-virtual {v1}, Lo/ad;->g()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x4380

    div-float/2addr v0, v1

    const/high16 v1, 0x3f00

    mul-float v4, v0, v1

    .line 709
    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v5

    move v3, v2

    .line 710
    :goto_c4
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_db

    .line 711
    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v1

    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LF/A;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LF/w;->a(Lo/ad;LF/A;IFLx/h;)V

    .line 710
    add-int/lit8 v3, v3, 0x1

    goto :goto_c4

    .line 713
    :cond_db
    iget-object v0, p0, LF/w;->c:Lo/aq;

    invoke-direct {p0, p5, v0}, LF/w;->a(ILo/aq;)V

    .line 714
    return-void

    :cond_e1
    move v0, v2

    goto/16 :goto_57
.end method

.method synthetic constructor <init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;ILF/x;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct/range {p0 .. p5}, LF/w;-><init>(Lo/aq;LF/z;Ljava/util/List;Ljava/util/Set;I)V

    return-void
.end method

.method private static declared-synchronized a(LD/a;LF/C;)LD/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 625
    const-class v1, LF/w;

    monitor-enter v1

    :try_start_3
    sget-object v0, LF/w;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_17

    .line 626
    if-nez v0, :cond_10

    .line 627
    const/4 v0, 0x0

    .line 629
    :goto_e
    monitor-exit v1

    return-object v0

    :cond_10
    :try_start_10
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;
    :try_end_16
    .catchall {:try_start_10 .. :try_end_16} :catchall_17

    goto :goto_e

    .line 625
    :catchall_17
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(F)V
    .registers 1
    .parameter

    .prologue
    .line 1078
    sput p0, LF/w;->b:F

    .line 1079
    return-void
.end method

.method private a(ILo/aq;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1054
    return-void
.end method

.method private static declared-synchronized a(LD/a;LF/C;LD/b;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 633
    const-class v1, LF/w;

    monitor-enter v1

    :try_start_3
    sget-object v0, LF/w;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 634
    if-nez v0, :cond_16

    .line 635
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 636
    sget-object v2, LF/w;->p:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 638
    :cond_16
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 639
    monitor-exit v1

    return-void

    .line 633
    :catchall_1b
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x1

    .line 718
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 719
    invoke-virtual {p0}, LD/a;->p()V

    .line 720
    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 724
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 725
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 727
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 895
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move/from16 v0, p6

    int-to-float v2, v0

    div-float/2addr v1, v2

    const/high16 v2, 0x4180

    mul-float v12, v1, v2

    .line 897
    const/4 v1, 0x0

    move v7, v1

    :goto_f
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v7, v1, :cond_b0

    .line 898
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/B;

    .line 899
    iget v2, v1, LF/B;->a:F

    mul-float v2, v2, p5

    add-float v13, p3, v2

    .line 900
    iget v2, v1, LF/B;->b:F

    mul-float v2, v2, p5

    const/high16 v3, 0x3f00

    mul-float v14, v2, v3

    .line 901
    iget v2, v1, LF/B;->c:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 902
    iget-object v15, v1, LF/B;->d:[I

    .line 903
    if-nez v15, :cond_64

    .line 904
    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_52

    .line 905
    sub-float v2, v13, v14

    const/high16 v1, 0x3f00

    add-float v3, p4, v1

    add-float v4, v13, v14

    const/high16 v1, 0x3f00

    add-float v5, p4, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 897
    :cond_4e
    :goto_4e
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_f

    .line 908
    :cond_52
    sub-float v2, v13, v14

    const/4 v3, 0x0

    add-float v4, v13, v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v5, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_4e

    .line 913
    :cond_64
    const/4 v3, 0x0

    .line 914
    const/4 v2, 0x1

    move/from16 v4, p4

    .line 916
    :goto_68
    move/from16 v0, p6

    if-ge v3, v0, :cond_4e

    .line 918
    const/4 v1, 0x0

    move v8, v1

    move v1, v2

    move v2, v3

    move v3, v4

    :goto_71
    array-length v4, v15

    rem-int/lit8 v4, v4, 0x2

    if-gt v8, v4, :cond_ac

    .line 919
    array-length v0, v15

    move/from16 v16, v0

    const/4 v4, 0x0

    move v11, v4

    move v9, v1

    move v10, v2

    :goto_7d
    move/from16 v0, v16

    if-ge v11, v0, :cond_a6

    aget v17, v15, v11

    .line 920
    move/from16 v0, v17

    int-to-float v1, v0

    const/high16 v2, 0x4180

    div-float/2addr v1, v2

    mul-float/2addr v1, v12

    add-float v5, v3, v1

    .line 922
    if-eqz v9, :cond_99

    .line 923
    sub-float v2, v13, v14

    add-float v4, v13, v14

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 927
    :cond_99
    if-nez v9, :cond_a4

    const/4 v1, 0x1

    .line 928
    :goto_9c
    add-int v10, v10, v17

    .line 919
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v9, v1

    move v3, v5

    goto :goto_7d

    .line 927
    :cond_a4
    const/4 v1, 0x0

    goto :goto_9c

    .line 918
    :cond_a6
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v2, v10

    move v1, v9

    goto :goto_71

    :cond_ac
    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_68

    .line 934
    :cond_b0
    return-void
.end method

.method static a(Ljava/util/List;Landroid/graphics/Bitmap;FZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 871
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 872
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 873
    const/high16 v3, 0x3f80

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 874
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 875
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f00

    mul-float/2addr v3, v4

    .line 879
    if-eqz p3, :cond_42

    move v8, v0

    .line 880
    :goto_1e
    const/4 v0, 0x0

    move v9, v0

    :goto_20
    if-ge v9, v8, :cond_48

    .line 881
    invoke-interface {p0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LF/A;

    .line 882
    invoke-virtual {v7}, LF/A;->e()I

    move-result v6

    .line 883
    int-to-float v4, v9

    .line 884
    invoke-virtual {v7}, LF/A;->f()Ljava/util/ArrayList;

    move-result-object v0

    move v5, p2

    invoke-static/range {v0 .. v6}, LF/w;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    .line 886
    invoke-virtual {v7}, LF/A;->g()Ljava/util/ArrayList;

    move-result-object v0

    move v5, p2

    invoke-static/range {v0 .. v6}, LF/w;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    .line 880
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_20

    .line 879
    :cond_42
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    move v8, v0

    goto :goto_1e

    .line 889
    :cond_48
    return-void
.end method

.method private a(Lo/ad;LF/A;IFLx/h;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 979
    invoke-virtual {p2}, LF/A;->a()Lo/X;

    move-result-object v1

    .line 980
    invoke-virtual {v1}, Lo/X;->b()I

    move-result v10

    .line 983
    const/4 v0, 0x2

    if-ge v10, v0, :cond_c

    .line 1050
    :goto_b
    return-void

    .line 987
    :cond_c
    invoke-virtual {p2}, LF/A;->c()Lo/o;

    move-result-object v0

    iput-object v0, p0, LF/w;->n:Lo/o;

    .line 992
    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v4

    .line 993
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v5

    .line 1025
    iget-boolean v0, p0, LF/w;->k:Z

    if-nez v0, :cond_3e

    .line 1026
    const/4 v3, 0x1

    const/high16 v6, 0x3f80

    iget-object v7, p0, LF/w;->e:LE/o;

    iget-object v8, p0, LF/w;->f:LE/d;

    const/4 v9, 0x0

    move-object/from16 v0, p5

    move v2, p4

    invoke-virtual/range {v0 .. v9}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    .line 1028
    const/4 v2, 0x1

    iget v3, p0, LF/w;->i:I

    const/4 v0, 0x1

    new-array v4, v0, [I

    const/4 v0, 0x0

    aput p3, v4, v0

    iget-object v5, p0, LF/w;->g:LE/i;

    move-object/from16 v0, p5

    move v1, v10

    invoke-virtual/range {v0 .. v5}, Lx/h;->a(IZI[ILE/k;)V

    goto :goto_b

    .line 1043
    :cond_3e
    invoke-virtual {p2}, LF/A;->e()I

    move-result v0

    .line 1044
    const/high16 v2, 0x4500

    int-to-float v0, v0

    div-float v6, v2, v0

    .line 1046
    const/4 v3, 0x1

    iget-object v7, p0, LF/w;->e:LE/o;

    iget-object v8, p0, LF/w;->f:LE/d;

    iget-object v9, p0, LF/w;->g:LE/i;

    move-object/from16 v0, p5

    move v2, p4

    invoke-virtual/range {v0 .. v9}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    goto :goto_b
.end method

.method static a(Lo/X;LF/z;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 958
    invoke-virtual {p0}, Lo/X;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 959
    if-ge v2, v1, :cond_c

    move v0, v1

    .line 969
    :cond_b
    :goto_b
    return v0

    .line 963
    :cond_c
    mul-int/lit8 v3, v2, 0x5

    .line 964
    iget v4, p1, LF/z;->a:I

    if-lez v4, :cond_19

    iget v4, p1, LF/z;->a:I

    add-int/2addr v4, v3

    const/16 v5, 0x4000

    if-gt v4, v5, :cond_b

    .line 967
    :cond_19
    iget v4, p1, LF/z;->a:I

    add-int/2addr v3, v4

    iput v3, p1, LF/z;->a:I

    .line 968
    iget v3, p1, LF/z;->b:I

    mul-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    invoke-virtual {p0}, Lo/X;->e()Z

    move-result v4

    if-eqz v4, :cond_2d

    const/4 v0, 0x3

    :cond_2d
    add-int/2addr v0, v2

    add-int/2addr v0, v3

    iput v0, p1, LF/z;->b:I

    move v0, v1

    .line 969
    goto :goto_b
.end method

.method private static declared-synchronized b(LD/a;LF/C;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 642
    const-class v1, LF/w;

    monitor-enter v1

    :try_start_3
    sget-object v0, LF/w;->p:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 643
    if-eqz v0, :cond_10

    .line 644
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    .line 649
    :cond_10
    monitor-exit v1

    return-void

    .line 642
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c()Landroid/graphics/Bitmap;
    .registers 5

    .prologue
    .line 817
    iget v0, p0, LF/w;->j:I

    const/4 v1, 0x1

    iget v2, p0, LF/w;->i:I

    shl-int/2addr v1, v2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 819
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 821
    iget-object v1, p0, LF/w;->d:Ljava/util/List;

    iget v2, p0, LF/w;->l:F

    iget-boolean v3, p0, LF/w;->k:Z

    invoke-static {v1, v0, v2, v3}, LF/w;->a(Ljava/util/List;Landroid/graphics/Bitmap;FZ)V

    .line 823
    return-object v0
.end method

.method private c(LD/a;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 788
    iget-object v0, p0, LF/w;->o:LF/C;

    invoke-static {p1, v0}, LF/w;->a(LD/a;LF/C;)LD/b;

    move-result-object v0

    iput-object v0, p0, LF/w;->h:LD/b;

    .line 790
    iget-object v0, p0, LF/w;->h:LD/b;

    if-nez v0, :cond_32

    .line 800
    invoke-direct {p0}, LF/w;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 802
    new-instance v1, LD/b;

    invoke-direct {v1, p1}, LD/b;-><init>(LD/a;)V

    iput-object v1, p0, LF/w;->h:LD/b;

    .line 803
    iget-object v1, p0, LF/w;->o:LF/C;

    iget-object v2, p0, LF/w;->h:LD/b;

    invoke-static {p1, v1, v2}, LF/w;->a(LD/a;LF/C;LD/b;)V

    .line 804
    iget-object v1, p0, LF/w;->h:LD/b;

    invoke-virtual {v1, v3}, LD/b;->c(Z)V

    .line 805
    iget-object v1, p0, LF/w;->h:LD/b;

    invoke-virtual {v1, v3}, LD/b;->b(Z)V

    .line 806
    iget-object v1, p0, LF/w;->h:LD/b;

    invoke-virtual {v1, v0}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 807
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 811
    :goto_31
    return-void

    .line 809
    :cond_32
    iget-object v0, p0, LF/w;->h:LD/b;

    invoke-virtual {v0}, LD/b;->f()V

    goto :goto_31
.end method

.method private d(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 827
    iget-object v0, p0, LF/w;->h:LD/b;

    if-eqz v0, :cond_19

    .line 828
    iget-object v0, p0, LF/w;->h:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 829
    iget-object v0, p0, LF/w;->h:LD/b;

    invoke-virtual {v0}, LD/b;->h()I

    move-result v0

    if-nez v0, :cond_16

    .line 830
    iget-object v0, p0, LF/w;->o:LF/C;

    invoke-static {p1, v0}, LF/w;->b(LD/a;LF/C;)V

    .line 832
    :cond_16
    const/4 v0, 0x0

    iput-object v0, p0, LF/w;->h:LD/b;

    .line 834
    :cond_19
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 1087
    iget-object v0, p0, LF/w;->e:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/w;->f:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/w;->g:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1090
    iget-object v1, p0, LF/w;->h:LD/b;

    if-eqz v1, :cond_1f

    .line 1091
    iget-object v1, p0, LF/w;->h:LD/b;

    invoke-virtual {v1}, LD/b;->i()I

    move-result v1

    add-int/2addr v0, v1

    .line 1093
    :cond_1f
    return v0
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 838
    invoke-direct {p0, p1}, LF/w;->d(LD/a;)V

    .line 845
    iget-object v0, p0, LF/w;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 846
    iget-object v0, p0, LF/w;->f:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 847
    iget-object v0, p0, LF/w;->g:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 849
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v7, 0x1700

    const/high16 v6, -0x4100

    const/high16 v0, 0x3f80

    const/high16 v4, 0x3f00

    const/4 v5, 0x0

    .line 736
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->c()Z

    move-result v1

    if-nez v1, :cond_14

    .line 780
    :cond_13
    :goto_13
    return-void

    .line 745
    :cond_14
    iget-object v1, p0, LF/w;->n:Lo/o;

    if-eqz v1, :cond_24

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    iget-object v2, p0, LF/w;->n:Lo/o;

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aH;->b(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 750
    :cond_24
    const/4 v1, 0x0

    invoke-virtual {p2}, LC/a;->r()F

    move-result v2

    iget-object v3, p0, LF/w;->c:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->b()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 753
    iget-object v2, p0, LF/w;->h:LD/b;

    if-nez v2, :cond_41

    .line 754
    invoke-direct {p0, p1}, LF/w;->c(LD/a;)V

    .line 756
    :cond_41
    iput v1, p0, LF/w;->m:I

    .line 758
    if-eqz v1, :cond_79

    .line 760
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    const/16 v3, 0x1702

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 761
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 762
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-interface {v2, v4, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 764
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    int-to-float v3, v1

    add-float/2addr v3, v0

    iget-boolean v4, p0, LF/w;->k:Z

    if-eqz v4, :cond_68

    int-to-float v4, v1

    add-float/2addr v0, v4

    :cond_68
    invoke-interface {v2, v3, v0, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 765
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v6, v6, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 766
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 769
    :cond_79
    iget-object v0, p0, LF/w;->h:LD/b;

    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    invoke-virtual {v0, v2}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 770
    iget-object v0, p0, LF/w;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 771
    iget-object v0, p0, LF/w;->g:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 772
    iget-object v0, p0, LF/w;->f:LE/d;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2}, LE/d;->a(LD/a;I)V

    .line 774
    if-eqz v1, :cond_13

    .line 776
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x1702

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 777
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 778
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0, v7}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto/16 :goto_13
.end method

.method public b()I
    .registers 4

    .prologue
    .line 1098
    const/16 v0, 0x260

    .line 1099
    iget-object v1, p0, LF/w;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/A;

    .line 1100
    invoke-virtual {v0}, LF/A;->h()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_9

    .line 1105
    :cond_1c
    iget-object v0, p0, LF/w;->e:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    iget-object v2, p0, LF/w;->f:LE/d;

    invoke-virtual {v2}, LE/d;->d()I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, LF/w;->g:LE/i;

    invoke-virtual {v2}, LE/i;->c()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 1108
    return v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 853
    invoke-direct {p0, p1}, LF/w;->d(LD/a;)V

    .line 860
    iget-object v0, p0, LF/w;->e:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 861
    iget-object v0, p0, LF/w;->f:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 862
    iget-object v0, p0, LF/w;->g:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 864
    return-void
.end method
