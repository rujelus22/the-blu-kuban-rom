.class public LF/Q;
.super LF/i;
.source "SourceFile"


# static fields
.field private static final c:[F

.field private static final d:F


# instance fields
.field private final A:Lo/T;

.field private final B:Ljava/util/ArrayList;

.field private final C:Ljava/util/ArrayList;

.field private D:Z

.field private E:F

.field b:F

.field private final e:Lo/aq;

.field private final f:[F

.field private final g:Lx/j;

.field private final h:Lx/j;

.field private final i:LE/o;

.field private final j:LE/i;

.field private final k:LE/a;

.field private final l:LE/a;

.field private final m:LE/d;

.field private final n:LE/a;

.field private final o:LE/o;

.field private final p:LE/i;

.field private final q:LE/d;

.field private final r:LE/a;

.field private final s:LE/o;

.field private final t:Lx/c;

.field private final u:LE/d;

.field private final v:LE/d;

.field private final w:LE/o;

.field private final x:LE/i;

.field private final y:LE/d;

.field private z:Lh/e;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 67
    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_16

    sput-object v0, LF/Q;->c:[F

    .line 103
    const-wide/high16 v0, 0x3ff0

    const-wide/high16 v2, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, LF/Q;->d:F

    return-void

    .line 67
    nop

    :array_16
    .array-data 0x4
        0xb8t 0xb7t 0x37t 0x3ft
        0xb8t 0xb7t 0x37t 0x3ft
        0xe6t 0xe5t 0x65t 0x3ft
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method private constructor <init>(Lo/aq;LF/R;Ljava/util/HashSet;LD/a;I)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/high16 v5, -0x4080

    const/high16 v4, 0x437f

    const/4 v3, 0x0

    .line 300
    invoke-direct {p0, p3}, LF/i;-><init>(Ljava/util/Set;)V

    .line 198
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LF/Q;->A:Lo/T;

    .line 215
    iput-boolean v2, p0, LF/Q;->D:Z

    .line 301
    iput-object p1, p0, LF/Q;->e:Lo/aq;

    .line 304
    const/4 v0, 0x4

    new-array v0, v0, [F

    invoke-static {p5}, Lx/d;->d(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v2

    invoke-static {p5}, Lx/d;->e(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v6

    const/4 v1, 0x2

    invoke-static {p5}, Lx/d;->f(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Lx/d;->g(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    aput v2, v0, v1

    iput-object v0, p0, LF/Q;->f:[F

    .line 321
    iput-object v3, p0, LF/Q;->g:Lx/j;

    .line 322
    iput-object v3, p0, LF/Q;->h:Lx/j;

    .line 324
    new-instance v0, LE/r;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/r;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->i:LE/o;

    .line 325
    new-instance v0, LE/l;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/l;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->j:LE/i;

    .line 326
    new-instance v0, LE/c;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/c;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->l:LE/a;

    .line 327
    new-instance v0, LE/c;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1, v6}, LE/c;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->k:LE/a;

    .line 328
    new-instance v0, LE/f;

    iget v1, p2, LF/R;->b:I

    invoke-direct {v0, v1, v6}, LE/f;-><init>(IZ)V

    iput-object v0, p0, LF/Q;->m:LE/d;

    .line 332
    iget-object v0, p2, LF/R;->i:Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne v0, v1, :cond_102

    .line 333
    new-instance v0, LE/c;

    iget v1, p2, LF/R;->a:I

    invoke-direct {v0, v1}, LE/c;-><init>(I)V

    iput-object v0, p0, LF/Q;->n:LE/a;

    .line 339
    :goto_7c
    iget v0, p2, LF/R;->c:I

    if-lez v0, :cond_106

    .line 340
    new-instance v0, LE/r;

    iget v1, p2, LF/R;->c:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/Q;->s:LE/o;

    .line 341
    new-instance v0, Lx/c;

    iget v1, p2, LF/R;->c:I

    invoke-virtual {p4}, LD/a;->G()Lx/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lx/c;-><init>(ILx/a;)V

    iput-object v0, p0, LF/Q;->t:Lx/c;

    .line 343
    new-instance v0, LE/f;

    iget v1, p2, LF/R;->d:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->u:LE/d;

    .line 347
    new-instance v0, LE/f;

    iget v1, p2, LF/R;->b:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->v:LE/d;

    .line 355
    :goto_a8
    iget v0, p2, LF/R;->e:I

    if-lez v0, :cond_10f

    .line 356
    new-instance v0, LE/r;

    iget v1, p2, LF/R;->e:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/Q;->w:LE/o;

    .line 357
    new-instance v0, LE/l;

    iget v1, p2, LF/R;->e:I

    invoke-direct {v0, v1}, LE/l;-><init>(I)V

    iput-object v0, p0, LF/Q;->x:LE/i;

    .line 358
    new-instance v0, LE/f;

    iget v1, p2, LF/R;->f:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->y:LE/d;

    .line 364
    :goto_c7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LF/Q;->B:Ljava/util/ArrayList;

    .line 365
    iput v5, p0, LF/Q;->b:F

    .line 367
    iget v0, p2, LF/R;->g:I

    if-lez v0, :cond_116

    .line 368
    new-instance v0, LE/r;

    iget v1, p2, LF/R;->g:I

    invoke-direct {v0, v1}, LE/r;-><init>(I)V

    iput-object v0, p0, LF/Q;->o:LE/o;

    .line 369
    new-instance v0, LE/l;

    iget v1, p2, LF/R;->g:I

    invoke-direct {v0, v1}, LE/l;-><init>(I)V

    iput-object v0, p0, LF/Q;->p:LE/i;

    .line 370
    new-instance v0, LE/f;

    iget v1, p2, LF/R;->h:I

    invoke-direct {v0, v1}, LE/f;-><init>(I)V

    iput-object v0, p0, LF/Q;->q:LE/d;

    .line 371
    new-instance v0, LE/c;

    iget v1, p2, LF/R;->g:I

    invoke-direct {v0, v1}, LE/c;-><init>(I)V

    iput-object v0, p0, LF/Q;->r:LE/a;

    .line 378
    :goto_f8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LF/Q;->C:Ljava/util/ArrayList;

    .line 379
    iput v5, p0, LF/Q;->E:F

    .line 380
    return-void

    .line 335
    :cond_102
    iput-object v3, p0, LF/Q;->n:LE/a;

    goto/16 :goto_7c

    .line 349
    :cond_106
    iput-object v3, p0, LF/Q;->s:LE/o;

    .line 350
    iput-object v3, p0, LF/Q;->t:Lx/c;

    .line 351
    iput-object v3, p0, LF/Q;->u:LE/d;

    .line 352
    iput-object v3, p0, LF/Q;->v:LE/d;

    goto :goto_a8

    .line 360
    :cond_10f
    iput-object v3, p0, LF/Q;->w:LE/o;

    .line 361
    iput-object v3, p0, LF/Q;->x:LE/i;

    .line 362
    iput-object v3, p0, LF/Q;->y:LE/d;

    goto :goto_c7

    .line 373
    :cond_116
    iput-object v3, p0, LF/Q;->o:LE/o;

    .line 374
    iput-object v3, p0, LF/Q;->p:LE/i;

    .line 375
    iput-object v3, p0, LF/Q;->q:LE/d;

    .line 376
    iput-object v3, p0, LF/Q;->r:LE/a;

    goto :goto_f8
.end method

.method private static a(F)F
    .registers 4
    .parameter

    .prologue
    .line 927
    float-to-int v1, p0

    .line 928
    int-to-float v0, v1

    sub-float v0, p0, v0

    const/high16 v2, 0x3f00

    cmpl-float v0, v0, v2

    if-lez v0, :cond_17

    sget v0, LF/Q;->d:F

    .line 929
    :goto_c
    const/4 v2, 0x1

    rsub-int/lit8 v1, v1, 0x1e

    shl-int v1, v2, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x4380

    div-float/2addr v0, v1

    return v0

    .line 928
    :cond_17
    const/high16 v0, 0x3f80

    goto :goto_c
.end method

.method private a(LC/a;)F
    .registers 4
    .parameter

    .prologue
    .line 910
    const/high16 v0, 0x40c0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    invoke-static {v1}, LF/Q;->a(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static a(Lo/aj;)F
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 936
    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v0

    if-nez v0, :cond_8

    .line 952
    :cond_7
    return v1

    .line 949
    :cond_8
    const/4 v0, 0x0

    :goto_9
    invoke-virtual {p0}, Lo/aj;->b()I

    move-result v2

    if-ge v0, v2, :cond_7

    .line 950
    invoke-virtual {p0, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->c()F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 949
    add-int/lit8 v0, v0, 0x1

    goto :goto_9
.end method

.method private static a(FI)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 836
    int-to-float v0, p1

    sub-float v0, p0, v0

    .line 837
    const/high16 v1, 0x4080

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_b

    .line 838
    const/4 v0, 0x2

    .line 848
    :goto_a
    return v0

    .line 839
    :cond_b
    const/high16 v1, 0x4040

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_13

    .line 840
    const/4 v0, 0x3

    goto :goto_a

    .line 841
    :cond_13
    const/high16 v1, 0x4010

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1b

    .line 842
    const/4 v0, 0x4

    goto :goto_a

    .line 843
    :cond_1b
    const/high16 v1, 0x3f80

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_27

    const/16 v0, 0x11

    if-lt p1, v0, :cond_27

    .line 846
    const/4 v0, 0x5

    goto :goto_a

    .line 848
    :cond_27
    const/16 v0, 0x16

    goto :goto_a
.end method

.method private static a(FILcom/google/android/maps/driveabout/vector/q;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 858
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p2, v0, :cond_f

    .line 859
    const/high16 v0, 0x4180

    cmpg-float v0, p0, v0

    if-gez v0, :cond_c

    .line 860
    const/4 v0, 0x7

    .line 878
    :goto_b
    return v0

    .line 862
    :cond_c
    const/16 v0, 0x15

    goto :goto_b

    .line 866
    :cond_f
    int-to-float v0, p1

    sub-float v0, p0, v0

    .line 867
    const/high16 v1, 0x4080

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1a

    .line 868
    const/4 v0, 0x1

    goto :goto_b

    .line 869
    :cond_1a
    const/high16 v1, 0x4040

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_22

    .line 870
    const/4 v0, 0x2

    goto :goto_b

    .line 871
    :cond_22
    const/high16 v1, 0x4010

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2a

    .line 872
    const/4 v0, 0x3

    goto :goto_b

    .line 873
    :cond_2a
    const/high16 v1, 0x3f80

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_36

    const/16 v0, 0x11

    if-lt p1, v0, :cond_36

    .line 876
    const/4 v0, 0x4

    goto :goto_b

    .line 878
    :cond_36
    const/4 v0, 0x6

    goto :goto_b
.end method

.method public static a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 384
    const/4 v0, 0x0

    .line 385
    invoke-virtual {p0}, LC/a;->r()F

    move-result v1

    .line 386
    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->b:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v2, :cond_f

    const/high16 v2, 0x418c

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_11

    .line 387
    :cond_f
    const/16 v0, 0x20

    .line 389
    :cond_11
    sget-object v2, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v2, :cond_17

    .line 394
    or-int/lit8 v0, v0, 0x14

    .line 397
    :cond_17
    const/high16 v2, 0x4178

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_27

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p1, v1, :cond_25

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    if-ne p1, v1, :cond_27

    .line 399
    :cond_25
    or-int/lit8 v0, v0, 0x40

    .line 402
    :cond_27
    or-int/lit16 v0, v0, 0x180

    .line 404
    return v0
.end method

.method public static a(Lo/aq;[Ljava/lang/String;Lo/aO;Lz/D;Lz/D;Lx/n;LD/a;)LF/Q;
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 238
    invoke-virtual {p0}, Lo/aq;->i()Lo/ad;

    move-result-object v6

    .line 241
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 243
    new-instance v7, Ljava/util/ArrayList;

    const/16 v0, 0x200

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 244
    invoke-virtual {p0}, Lo/aq;->b()I

    move-result v5

    .line 245
    new-instance v2, LF/R;

    invoke-direct {v2}, LF/R;-><init>()V

    .line 246
    new-instance v8, LF/S;

    invoke-direct {v8}, LF/S;-><init>()V

    .line 248
    const/4 v0, -0x1

    move v4, v0

    .line 250
    :goto_20
    invoke-interface {p2}, Lo/aO;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c3

    .line 251
    invoke-interface {p2}, Lo/aO;->b()Lo/n;

    move-result-object v1

    .line 252
    instance-of v0, v1, Lo/af;

    if-nez v0, :cond_56

    move v5, v4

    .line 287
    :goto_2f
    new-instance v0, LF/Q;

    move-object v1, p0

    move-object/from16 v4, p6

    invoke-direct/range {v0 .. v5}, LF/Q;-><init>(Lo/aq;LF/R;Ljava/util/HashSet;LD/a;I)V

    .line 288
    invoke-static {}, Lx/h;->a()Lx/h;

    move-result-object v4

    .line 289
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3f
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/af;

    .line 290
    invoke-static {v2, v8}, LF/Q;->a(Lo/af;LF/S;)V

    move-object v1, v6

    move-object v3, v8

    move-object/from16 v5, p5

    .line 291
    invoke-direct/range {v0 .. v5}, LF/Q;->a(Lo/ad;Lo/af;LF/S;Lx/h;Lx/n;)V

    goto :goto_3f

    :cond_56
    move-object v0, v1

    .line 255
    check-cast v0, Lo/af;

    .line 256
    invoke-virtual {v0}, Lo/af;->o()Z

    move-result v9

    if-eqz v9, :cond_6c

    .line 257
    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v9

    invoke-virtual {v9}, Lo/aj;->j()Lo/ai;

    move-result-object v9

    if-nez v9, :cond_77

    .line 259
    const v4, -0x48481b

    .line 267
    :cond_6c
    :goto_6c
    invoke-static {v0, v8}, LF/Q;->a(Lo/af;LF/S;)V

    .line 268
    invoke-static {v5, v0, v8, v2}, LF/Q;->a(ILo/af;LF/S;LF/R;)Z

    move-result v9

    if-nez v9, :cond_97

    move v5, v4

    .line 269
    goto :goto_2f

    .line 260
    :cond_77
    const/4 v9, -0x1

    if-ne v4, v9, :cond_87

    .line 261
    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v4

    invoke-virtual {v4}, Lo/aj;->j()Lo/ai;

    move-result-object v4

    invoke-virtual {v4}, Lo/ai;->b()I

    move-result v4

    goto :goto_6c

    .line 262
    :cond_87
    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v9

    invoke-virtual {v9}, Lo/aj;->j()Lo/ai;

    move-result-object v9

    invoke-virtual {v9}, Lo/ai;->b()I

    move-result v9

    if-eq v4, v9, :cond_6c

    move v5, v4

    .line 264
    goto :goto_2f

    .line 271
    :cond_97
    invoke-interface {v1}, Lo/n;->l()[I

    move-result-object v9

    array-length v10, v9

    const/4 v1, 0x0

    :goto_9d
    if-ge v1, v10, :cond_ae

    aget v11, v9, v1

    .line 272
    if-ltz v11, :cond_ab

    array-length v12, p1

    if-ge v11, v12, :cond_ab

    .line 273
    aget-object v11, p1, v11

    invoke-virtual {v3, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_ab
    add-int/lit8 v1, v1, 0x1

    goto :goto_9d

    .line 276
    :cond_ae
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    invoke-interface {p2}, Lo/aO;->next()Ljava/lang/Object;

    goto/16 :goto_20

    :cond_b6
    move-object v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    .line 293
    invoke-direct/range {v0 .. v5}, LF/Q;->a(Lo/aq;Lz/D;Lz/D;Lx/n;LD/a;)V

    .line 294
    return-object v0

    :cond_c3
    move v5, v4

    goto/16 :goto_2f
.end method

.method public static a(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 413
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 414
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 418
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 420
    invoke-virtual {p0}, LD/a;->n()V

    .line 421
    invoke-virtual {p0}, LD/a;->p()V

    .line 422
    invoke-static {p1, p2}, LF/Q;->a(FI)I

    move-result v1

    .line 423
    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 424
    return-void
.end method

.method public static a(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 450
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 451
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 452
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 453
    invoke-virtual {p0}, LD/a;->p()V

    .line 454
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 455
    return-void
.end method

.method private a(Lo/X;Lo/T;II)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1209
    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v1

    .line 1210
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v2

    .line 1211
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v2, :cond_32

    .line 1212
    iget-object v3, p0, LF/Q;->A:Lo/T;

    invoke-virtual {p1, v0, v3}, Lo/X;->a(ILo/T;)V

    .line 1213
    iget-object v3, p0, LF/Q;->A:Lo/T;

    iget-object v4, p0, LF/Q;->A:Lo/T;

    invoke-static {v3, p2, v4}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 1214
    iget-object v3, p0, LF/Q;->s:LE/o;

    iget-object v4, p0, LF/Q;->A:Lo/T;

    invoke-virtual {v3, v4, p3}, LE/o;->a(Lo/T;I)V

    .line 1215
    if-lez v0, :cond_2f

    .line 1216
    iget-object v3, p0, LF/Q;->u:LE/d;

    add-int v4, v1, v0

    add-int/lit8 v4, v4, -0x1

    int-to-short v4, v4

    add-int v5, v1, v0

    int-to-short v5, v5

    invoke-virtual {v3, v4, v5}, LE/d;->a(SS)V

    .line 1211
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 1220
    :cond_32
    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p4, v2}, Lx/c;->a(II)V

    .line 1221
    return-void
.end method

.method private a(Lo/ad;Lo/af;LF/S;Lx/h;Lx/n;)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1056
    move-object/from16 v0, p3

    iget-boolean v1, v0, LF/S;->a:Z

    if-nez v1, :cond_7

    .line 1149
    :goto_6
    return-void

    .line 1060
    :cond_7
    const/4 v9, 0x0

    .line 1061
    const/4 v10, 0x0

    .line 1064
    invoke-virtual/range {p2 .. p2}, Lo/af;->e()Lo/aj;

    move-result-object v1

    .line 1065
    invoke-virtual {v1}, Lo/aj;->b()I

    move-result v2

    if-lez v2, :cond_22

    .line 1066
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v1

    .line 1067
    if-eqz v1, :cond_22

    .line 1068
    invoke-virtual {v1}, Lo/ai;->f()Z

    move-result v9

    .line 1069
    invoke-virtual {v1}, Lo/ai;->g()Z

    move-result v10

    .line 1076
    :cond_22
    invoke-virtual/range {p1 .. p1}, Lo/ad;->d()Lo/T;

    move-result-object v4

    .line 1077
    invoke-virtual/range {p1 .. p1}, Lo/ad;->g()I

    move-result v5

    .line 1080
    invoke-virtual/range {p2 .. p2}, Lo/af;->b()Lo/X;

    move-result-object v2

    .line 1081
    move-object/from16 v0, p3

    iget v1, v0, LF/S;->c:F

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v5}, LF/Q;->c(FI)F

    move-result v3

    .line 1082
    move-object/from16 v0, p3

    iget v1, v0, LF/S;->e:I

    if-nez v1, :cond_4a

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->d:I

    if-nez v1, :cond_4a

    move-object/from16 v0, p3

    iget v1, v0, LF/S;->f:I

    if-eqz v1, :cond_b7

    .line 1091
    :cond_4a
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->i:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v12

    .line 1092
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->m:LE/d;

    invoke-virtual {v1}, LE/d;->b()I

    move-result v13

    .line 1093
    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->i:LE/o;

    move-object/from16 v0, p0

    iget-object v7, v0, LF/Q;->j:LE/i;

    move-object/from16 v0, p0

    iget-object v8, v0, LF/Q;->m:LE/d;

    const/4 v11, 0x0

    move-object/from16 v1, p4

    invoke-virtual/range {v1 .. v11}, Lx/h;->a(Lo/X;FLo/T;ILE/q;LE/k;LE/e;ZZB)I

    .line 1095
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->i:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    sub-int/2addr v1, v12

    .line 1096
    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->k:LE/a;

    move-object/from16 v0, p3

    iget v7, v0, LF/S;->e:I

    invoke-virtual {v6, v7, v1}, LE/a;->b(II)V

    .line 1097
    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->l:LE/a;

    move-object/from16 v0, p3

    iget v7, v0, LF/S;->d:I

    invoke-virtual {v6, v7, v1}, LE/a;->b(II)V

    .line 1098
    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->n:LE/a;

    if-eqz v6, :cond_a2

    move-object/from16 v0, p3

    iget v6, v0, LF/S;->f:I

    if-eqz v6, :cond_a2

    .line 1100
    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->n:LE/a;

    move-object/from16 v0, p3

    iget v7, v0, LF/S;->f:I

    invoke-virtual {v6, v7, v1}, LE/a;->b(II)V

    .line 1103
    :cond_a2
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_b7

    .line 1104
    move-object/from16 v0, p3

    iget-boolean v1, v0, LF/S;->b:Z

    if-eqz v1, :cond_130

    .line 1107
    move-object/from16 v0, p3

    iget v1, v0, LF/S;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5, v1}, LF/Q;->a(Lo/X;Lo/T;II)V

    .line 1122
    :cond_b7
    :goto_b7
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->e:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    move-object/from16 v0, p2

    invoke-static {v1, v0}, LF/Q;->a(ILo/af;)Z

    move-result v1

    if-eqz v1, :cond_e5

    .line 1130
    const/4 v9, 0x0

    const/high16 v12, 0x3f80

    move-object/from16 v0, p0

    iget-object v13, v0, LF/Q;->w:LE/o;

    move-object/from16 v0, p0

    iget-object v14, v0, LF/Q;->y:LE/d;

    const/4 v15, 0x0

    move-object/from16 v6, p4

    move-object v7, v2

    move v8, v3

    move-object v10, v4

    move v11, v5

    invoke-virtual/range {v6 .. v15}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    .line 1133
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->B:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1138
    :cond_e5
    move-object/from16 v0, p3

    iget v1, v0, LF/S;->g:I

    if-eqz v1, :cond_126

    .line 1139
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->o:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    .line 1140
    const/4 v9, 0x0

    const/high16 v12, 0x3f80

    move-object/from16 v0, p0

    iget-object v13, v0, LF/Q;->o:LE/o;

    move-object/from16 v0, p0

    iget-object v14, v0, LF/Q;->q:LE/d;

    const/4 v15, 0x0

    move-object/from16 v6, p4

    move-object v7, v2

    move v8, v3

    move-object v10, v4

    move v11, v5

    invoke-virtual/range {v6 .. v15}, Lx/h;->a(Lo/X;FZLo/T;IFLE/q;LE/e;LE/k;)V

    .line 1142
    move-object/from16 v0, p0

    iget-object v2, v0, LF/Q;->o:LE/o;

    invoke-virtual {v2}, LE/o;->a()I

    move-result v2

    sub-int v1, v2, v1

    .line 1143
    move-object/from16 v0, p0

    iget-object v2, v0, LF/Q;->r:LE/a;

    move-object/from16 v0, p3

    iget v3, v0, LF/S;->g:I

    invoke-virtual {v2, v3, v1}, LE/a;->b(II)V

    .line 1145
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->C:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1148
    :cond_126
    invoke-virtual/range {p2 .. p2}, Lo/af;->r()Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, LF/Q;->D:Z

    goto/16 :goto_6

    .line 1111
    :cond_130
    move-object/from16 v0, p0

    iget-object v1, v0, LF/Q;->v:LE/d;

    move-object/from16 v0, p0

    iget-object v6, v0, LF/Q;->m:LE/d;

    move-object/from16 v0, p0

    iget-object v7, v0, LF/Q;->m:LE/d;

    invoke-virtual {v7}, LE/d;->b()I

    move-result v7

    sub-int/2addr v7, v13

    invoke-virtual {v1, v6, v13, v7}, LE/d;->a(LE/d;II)V

    goto/16 :goto_b7
.end method

.method private a(Lo/af;LE/k;F)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 734
    const/high16 v0, 0x4180

    mul-float/2addr v0, p3

    .line 735
    const/high16 v1, 0x3f80

    div-float v4, v1, p3

    .line 736
    const/high16 v1, 0x3f80

    div-float v5, v1, v0

    .line 738
    iget-object v0, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v0

    .line 739
    invoke-virtual {p1}, Lo/af;->b()Lo/X;

    move-result-object v6

    .line 740
    invoke-virtual {v6}, Lo/X;->b()I

    move-result v1

    add-int/lit8 v7, v1, -0x1

    .line 741
    invoke-virtual {p1}, Lo/af;->e()Lo/aj;

    move-result-object v1

    invoke-static {v1}, LF/Q;->a(Lo/aj;)F

    move-result v1

    .line 742
    const/high16 v2, 0x4000

    invoke-direct {p0, v1, v0}, LF/Q;->c(FI)F

    move-result v0

    mul-float v8, v2, v0

    .line 745
    const/4 v0, 0x0

    move v3, v0

    :goto_31
    if-ge v3, v7, :cond_92

    .line 746
    invoke-virtual {v6, v3}, Lo/X;->b(I)F

    move-result v9

    .line 747
    const/4 v2, 0x0

    .line 748
    const/4 v1, 0x0

    .line 749
    const/4 v0, 0x0

    .line 753
    mul-float/2addr v9, v5

    .line 754
    const/high16 v10, 0x3f00

    cmpl-float v10, v9, v10

    if-lez v10, :cond_7e

    .line 755
    mul-float v1, v8, v4

    const/high16 v2, 0x4700

    mul-float/2addr v1, v2

    float-to-int v2, v1

    .line 757
    const/high16 v1, 0x4780

    mul-float/2addr v1, v9

    float-to-int v1, v1

    .line 758
    float-to-int v10, v9

    int-to-float v10, v10

    sub-float/2addr v9, v10

    .line 761
    const/high16 v10, 0x3e00

    cmpl-float v10, v9, v10

    if-lez v10, :cond_5d

    const/high16 v10, 0x3ec0

    cmpg-float v9, v9, v10

    if-gez v9, :cond_5d

    .line 762
    const v0, 0xa000

    .line 772
    :cond_5d
    :goto_5d
    const v9, 0x8000

    sub-int/2addr v9, v2

    .line 773
    const v10, 0x8000

    add-int/2addr v2, v10

    .line 774
    invoke-virtual {p1}, Lo/af;->k()Z

    move-result v10

    if-eqz v10, :cond_82

    .line 775
    add-int v10, v1, v0

    invoke-interface {p2, v2, v10}, LE/k;->a(II)V

    .line 776
    add-int/2addr v1, v0

    invoke-interface {p2, v9, v1}, LE/k;->a(II)V

    .line 777
    invoke-interface {p2, v9, v0}, LE/k;->a(II)V

    .line 778
    invoke-interface {p2, v2, v0}, LE/k;->a(II)V

    .line 745
    :goto_7a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_31

    .line 768
    :cond_7e
    const v0, 0xc000

    goto :goto_5d

    .line 780
    :cond_82
    invoke-interface {p2, v9, v0}, LE/k;->a(II)V

    .line 781
    invoke-interface {p2, v2, v0}, LE/k;->a(II)V

    .line 782
    add-int v10, v1, v0

    invoke-interface {p2, v2, v10}, LE/k;->a(II)V

    .line 783
    add-int/2addr v0, v1

    invoke-interface {p2, v9, v0}, LE/k;->a(II)V

    goto :goto_7a

    .line 786
    :cond_92
    return-void
.end method

.method static a(Lo/af;LF/S;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x2

    const/4 v5, 0x0

    .line 1396
    invoke-virtual {p0}, Lo/af;->e()Lo/aj;

    move-result-object v6

    .line 1397
    invoke-static {v6}, LF/Q;->a(Lo/aj;)F

    move-result v0

    iput v0, p1, LF/S;->c:F

    .line 1398
    iput v5, p1, LF/S;->d:I

    .line 1399
    iput v5, p1, LF/S;->e:I

    .line 1400
    iput v5, p1, LF/S;->f:I

    .line 1401
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-lt v0, v1, :cond_83

    .line 1402
    invoke-virtual {v6, v5}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->d:I

    .line 1403
    invoke-virtual {v6, v4}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->e:I

    .line 1404
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-le v0, v1, :cond_50

    move v0, v1

    .line 1409
    :goto_35
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v2

    if-ge v0, v2, :cond_111

    .line 1410
    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->d()[I

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_80

    .line 1415
    :goto_46
    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->f:I

    .line 1424
    :cond_50
    :goto_50
    iput v5, p1, LF/S;->g:I

    .line 1425
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-le v0, v1, :cond_a7

    move v0, v1

    move v2, v3

    .line 1426
    :goto_5a
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v7

    if-ge v0, v7, :cond_a7

    .line 1427
    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v7

    invoke-virtual {v7}, Lo/ai;->d()[I

    move-result-object v7

    array-length v7, v7

    if-lez v7, :cond_94

    .line 1428
    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v2

    invoke-virtual {v2}, Lo/ai;->c()F

    move-result v2

    .line 1429
    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v7

    invoke-virtual {v7}, Lo/ai;->b()I

    move-result v7

    iput v7, p1, LF/S;->g:I

    .line 1426
    :cond_7d
    :goto_7d
    add-int/lit8 v0, v0, 0x1

    goto :goto_5a

    .line 1409
    :cond_80
    add-int/lit8 v0, v0, 0x1

    goto :goto_35

    .line 1417
    :cond_83
    invoke-virtual {v6}, Lo/aj;->b()I

    move-result v0

    if-lt v0, v4, :cond_50

    .line 1418
    invoke-virtual {v6, v5}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->b()I

    move-result v0

    iput v0, p1, LF/S;->e:I

    goto :goto_50

    .line 1430
    :cond_94
    iget v7, p1, LF/S;->g:I

    if-eqz v7, :cond_7d

    invoke-virtual {v6, v0}, Lo/aj;->b(I)Lo/ai;

    move-result-object v7

    invoke-virtual {v7}, Lo/ai;->c()F

    move-result v7

    cmpl-float v7, v7, v2

    if-lez v7, :cond_7d

    .line 1434
    iput v5, p1, LF/S;->g:I

    goto :goto_7d

    .line 1441
    :cond_a7
    iget v0, p1, LF/S;->g:I

    if-eqz v0, :cond_ad

    .line 1442
    iput v5, p1, LF/S;->f:I

    .line 1448
    :cond_ad
    iget v0, p1, LF/S;->f:I

    if-eqz v0, :cond_cb

    iget v0, p1, LF/S;->e:I

    invoke-static {v0}, Lx/d;->a(I)I

    move-result v0

    iget v2, p1, LF/S;->d:I

    invoke-static {v2}, Lx/d;->a(I)I

    move-result v2

    if-lt v0, v2, :cond_c7

    iget v0, p1, LF/S;->c:F

    const/high16 v2, 0x4110

    cmpg-float v0, v0, v2

    if-gez v0, :cond_cb

    .line 1452
    :cond_c7
    iget v0, p1, LF/S;->f:I

    iput v0, p1, LF/S;->e:I

    .line 1459
    :cond_cb
    invoke-virtual {p0}, Lo/af;->b()Lo/X;

    move-result-object v0

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-lt v0, v1, :cond_10d

    iget v0, p1, LF/S;->c:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_10d

    iget v0, p1, LF/S;->e:I

    if-nez v0, :cond_f1

    iget v0, p1, LF/S;->d:I

    if-nez v0, :cond_f1

    iget v0, p1, LF/S;->f:I

    if-nez v0, :cond_f1

    iget v0, p1, LF/S;->g:I

    if-nez v0, :cond_f1

    invoke-virtual {p0}, Lo/af;->p()Z

    move-result v0

    if-eqz v0, :cond_10d

    :cond_f1
    move v0, v4

    :goto_f2
    iput-boolean v0, p1, LF/S;->a:Z

    .line 1469
    invoke-virtual {p0}, Lo/af;->q()Z

    move-result v0

    if-eqz v0, :cond_10f

    iget v0, p1, LF/S;->g:I

    if-nez v0, :cond_10f

    iget v0, p1, LF/S;->f:I

    if-nez v0, :cond_10f

    iget v0, p1, LF/S;->e:I

    invoke-static {v0}, Lx/d;->c(I)Z

    move-result v0

    if-nez v0, :cond_10f

    :goto_10a
    iput-boolean v4, p1, LF/S;->b:Z

    .line 1474
    return-void

    :cond_10d
    move v0, v5

    .line 1459
    goto :goto_f2

    :cond_10f
    move v4, v5

    .line 1469
    goto :goto_10a

    :cond_111
    move v0, v1

    goto/16 :goto_46
.end method

.method private a(Lo/aq;Lz/D;Lz/D;Lx/n;LD/a;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1154
    return-void
.end method

.method private static a(ILo/af;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1225
    const/16 v0, 0xe

    if-lt p0, v0, :cond_c

    invoke-virtual {p1}, Lo/af;->o()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method static a(ILo/af;LF/S;LF/R;)Z
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 994
    invoke-virtual {p1}, Lo/af;->b()Lo/X;

    move-result-object v3

    .line 995
    invoke-virtual {v3}, Lo/X;->b()I

    move-result v4

    .line 996
    add-int/lit8 v5, v4, -0x1

    .line 997
    iget-boolean v0, p2, LF/S;->a:Z

    if-nez v0, :cond_11

    .line 1043
    :cond_10
    :goto_10
    return v1

    .line 1001
    :cond_11
    invoke-static {v3}, Lx/h;->a(Lo/X;)I

    move-result v6

    .line 1002
    iget v0, p3, LF/R;->a:I

    if-lez v0, :cond_22

    iget v0, p3, LF/R;->a:I

    add-int/2addr v0, v6

    const/16 v7, 0x4000

    if-le v0, v7, :cond_22

    move v1, v2

    .line 1004
    goto :goto_10

    .line 1006
    :cond_22
    invoke-virtual {p1}, Lo/af;->e()Lo/aj;

    move-result-object v7

    .line 1009
    iget v0, p2, LF/S;->f:I

    if-eqz v0, :cond_5d

    move v0, v1

    .line 1010
    :goto_2b
    iget-object v8, p3, LF/R;->i:Ljava/lang/Boolean;

    if-nez v8, :cond_5f

    .line 1011
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p3, LF/R;->i:Ljava/lang/Boolean;

    .line 1018
    :cond_35
    iget v0, p2, LF/S;->g:I

    if-eqz v0, :cond_69

    .line 1019
    :goto_39
    invoke-virtual {v7}, Lo/aj;->b()I

    move-result v0

    if-ge v2, v0, :cond_69

    .line 1020
    invoke-virtual {v7, v2}, Lo/aj;->b(I)Lo/ai;

    move-result-object v0

    invoke-virtual {v0}, Lo/ai;->d()[I

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_5a

    .line 1021
    mul-int/lit8 v0, v5, 0x4

    .line 1022
    mul-int/lit8 v8, v5, 0x2

    .line 1023
    iget v9, p3, LF/R;->g:I

    add-int/2addr v0, v9

    iput v0, p3, LF/R;->g:I

    .line 1024
    iget v0, p3, LF/R;->h:I

    mul-int/lit8 v8, v8, 0x3

    add-int/2addr v0, v8

    iput v0, p3, LF/R;->h:I

    .line 1019
    :cond_5a
    add-int/lit8 v2, v2, 0x1

    goto :goto_39

    :cond_5d
    move v0, v2

    .line 1009
    goto :goto_2b

    .line 1012
    :cond_5f
    iget-object v8, p3, LF/R;->i:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eq v8, v0, :cond_35

    move v1, v2

    .line 1015
    goto :goto_10

    .line 1029
    :cond_69
    iget v0, p3, LF/R;->a:I

    add-int/2addr v0, v6

    iput v0, p3, LF/R;->a:I

    .line 1030
    iget v0, p3, LF/R;->b:I

    invoke-static {v3}, Lx/h;->b(Lo/X;)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p3, LF/R;->b:I

    .line 1032
    iget-boolean v0, p2, LF/S;->b:Z

    if-eqz v0, :cond_87

    .line 1033
    iget v0, p3, LF/R;->c:I

    add-int/2addr v0, v4

    iput v0, p3, LF/R;->c:I

    .line 1034
    iget v0, p3, LF/R;->d:I

    mul-int/lit8 v2, v5, 0x2

    add-int/2addr v0, v2

    iput v0, p3, LF/R;->d:I

    .line 1037
    :cond_87
    invoke-static {p0, p1}, LF/Q;->a(ILo/af;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1038
    mul-int/lit8 v0, v5, 0x4

    .line 1039
    mul-int/lit8 v2, v5, 0x2

    .line 1040
    iget v3, p3, LF/R;->e:I

    add-int/2addr v0, v3

    iput v0, p3, LF/R;->e:I

    .line 1041
    iget v0, p3, LF/R;->f:I

    mul-int/lit8 v2, v2, 0x3

    add-int/2addr v0, v2

    iput v0, p3, LF/R;->f:I

    goto/16 :goto_10
.end method

.method private b(LC/a;)F
    .registers 4
    .parameter

    .prologue
    .line 918
    const/high16 v0, 0x40c0

    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    invoke-static {v1}, LF/Q;->a(F)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static b(FI)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 888
    int-to-float v0, p1

    sub-float v0, p0, v0

    .line 889
    const/high16 v1, 0x4080

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_c

    .line 890
    const/16 v0, 0x19

    .line 900
    :goto_b
    return v0

    .line 891
    :cond_c
    const/high16 v1, 0x4040

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_15

    .line 892
    const/16 v0, 0x1a

    goto :goto_b

    .line 893
    :cond_15
    const/high16 v1, 0x4010

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1e

    .line 894
    const/16 v0, 0x1b

    goto :goto_b

    .line 895
    :cond_1e
    const/high16 v1, 0x3f80

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2b

    const/16 v0, 0x11

    if-lt p1, v0, :cond_2b

    .line 898
    const/16 v0, 0x1c

    goto :goto_b

    .line 900
    :cond_2b
    const/16 v0, 0x1d

    goto :goto_b
.end method

.method public static b(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 432
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 433
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 437
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 439
    invoke-virtual {p0}, LD/a;->n()V

    .line 440
    invoke-virtual {p0}, LD/a;->p()V

    .line 441
    invoke-static {p1, p2, p3}, LF/Q;->a(FILcom/google/android/maps/driveabout/vector/q;)I

    move-result v1

    .line 442
    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 443
    return-void
.end method

.method public static b(LD/a;FLcom/google/android/maps/driveabout/vector/q;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 472
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 473
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 474
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 475
    invoke-virtual {p0}, LD/a;->n()V

    .line 476
    invoke-virtual {p0}, LD/a;->p()V

    .line 477
    const/16 v1, 0x1e

    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 478
    return-void
.end method

.method private c(FI)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 968
    iget-object v0, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    const/16 v1, 0xe

    if-le v0, v1, :cond_13

    const/high16 v0, 0x3f00

    .line 969
    :goto_c
    int-to-float v1, p2

    mul-float/2addr v1, p1

    const/high16 v2, 0x4380

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    return v0

    .line 968
    :cond_13
    const v0, 0x3e99999a

    goto :goto_c
.end method

.method public static c(LD/a;FILcom/google/android/maps/driveabout/vector/q;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 460
    invoke-virtual {p0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 461
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 462
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 463
    invoke-virtual {p0}, LD/a;->n()V

    .line 464
    invoke-virtual {p0}, LD/a;->p()V

    .line 465
    invoke-static {p1, p2}, LF/Q;->b(FI)I

    move-result v1

    .line 466
    invoke-static {p0, v1}, Lx/o;->a(LD/a;I)LD/b;

    move-result-object v1

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 467
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 1234
    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0}, LE/o;->c()I

    move-result v0

    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->k:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->l:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->m:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1237
    iget-object v1, p0, LF/Q;->n:LE/a;

    if-eqz v1, :cond_2d

    .line 1238
    iget-object v1, p0, LF/Q;->n:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1240
    :cond_2d
    iget-object v1, p0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_38

    .line 1241
    iget-object v1, p0, LF/Q;->s:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1243
    :cond_38
    iget-object v1, p0, LF/Q;->t:Lx/c;

    if-eqz v1, :cond_43

    .line 1244
    iget-object v1, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v1}, Lx/c;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 1246
    :cond_43
    iget-object v1, p0, LF/Q;->u:LE/d;

    if-eqz v1, :cond_4e

    .line 1247
    iget-object v1, p0, LF/Q;->u:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1249
    :cond_4e
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_59

    .line 1250
    iget-object v1, p0, LF/Q;->v:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1252
    :cond_59
    iget-object v1, p0, LF/Q;->w:LE/o;

    if-eqz v1, :cond_64

    .line 1253
    iget-object v1, p0, LF/Q;->w:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1255
    :cond_64
    iget-object v1, p0, LF/Q;->x:LE/i;

    if-eqz v1, :cond_6f

    .line 1256
    iget-object v1, p0, LF/Q;->x:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1258
    :cond_6f
    iget-object v1, p0, LF/Q;->y:LE/d;

    if-eqz v1, :cond_7a

    .line 1259
    iget-object v1, p0, LF/Q;->y:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1262
    :cond_7a
    iget-object v1, p0, LF/Q;->o:LE/o;

    if-eqz v1, :cond_85

    .line 1263
    iget-object v1, p0, LF/Q;->o:LE/o;

    invoke-virtual {v1}, LE/o;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1265
    :cond_85
    iget-object v1, p0, LF/Q;->p:LE/i;

    if-eqz v1, :cond_90

    .line 1266
    iget-object v1, p0, LF/Q;->p:LE/i;

    invoke-virtual {v1}, LE/i;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1268
    :cond_90
    iget-object v1, p0, LF/Q;->q:LE/d;

    if-eqz v1, :cond_9b

    .line 1269
    iget-object v1, p0, LF/Q;->q:LE/d;

    invoke-virtual {v1}, LE/d;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1271
    :cond_9b
    iget-object v1, p0, LF/Q;->r:LE/a;

    if-eqz v1, :cond_a6

    .line 1272
    iget-object v1, p0, LF/Q;->r:LE/a;

    invoke-virtual {v1}, LE/a;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1275
    :cond_a6
    return v0
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 612
    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 613
    iget-object v0, p0, LF/Q;->j:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 614
    iget-object v0, p0, LF/Q;->l:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    .line 615
    iget-object v0, p0, LF/Q;->k:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    .line 616
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 618
    iget-object v0, p0, LF/Q;->n:LE/a;

    if-eqz v0, :cond_22

    .line 619
    iget-object v0, p0, LF/Q;->n:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    .line 622
    :cond_22
    iget-object v0, p0, LF/Q;->s:LE/o;

    if-eqz v0, :cond_3a

    .line 623
    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 624
    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->b(LD/a;)V

    .line 625
    iget-object v0, p0, LF/Q;->u:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 626
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 629
    :cond_3a
    iget-object v0, p0, LF/Q;->w:LE/o;

    if-eqz v0, :cond_4d

    .line 630
    iget-object v0, p0, LF/Q;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 631
    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 632
    iget-object v0, p0, LF/Q;->y:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 636
    :cond_4d
    iget-object v0, p0, LF/Q;->o:LE/o;

    if-eqz v0, :cond_65

    .line 637
    iget-object v0, p0, LF/Q;->o:LE/o;

    invoke-virtual {v0, p1}, LE/o;->b(LD/a;)V

    .line 638
    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->b(LD/a;)V

    .line 639
    iget-object v0, p0, LF/Q;->q:LE/d;

    invoke-virtual {v0, p1}, LE/d;->b(LD/a;)V

    .line 640
    iget-object v0, p0, LF/Q;->r:LE/a;

    invoke-virtual {v0, p1}, LE/a;->a(LD/a;)V

    .line 642
    :cond_65
    return-void
.end method

.method a(LD/a;F)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 703
    iget v0, p0, LF/Q;->b:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_7

    .line 729
    :goto_6
    return-void

    .line 706
    :cond_7
    iput p2, p0, LF/Q;->b:F

    .line 710
    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 711
    iget-object v1, p0, LF/Q;->x:LE/i;

    .line 715
    iget-object v0, p0, LF/Q;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 716
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 717
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    .line 718
    invoke-direct {p0, v0, v1, p2}, LF/Q;->a(Lo/af;LE/k;F)V

    goto :goto_16

    .line 724
    :cond_26
    new-instance v0, Lh/e;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x1f4

    sget-object v5, Lh/g;->c:Lh/g;

    const/4 v6, 0x0

    const/16 v7, 0x64

    invoke-direct/range {v0 .. v7}, Lh/e;-><init>(JJLh/g;II)V

    iput-object v0, p0, LF/Q;->z:Lh/e;

    goto :goto_6
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const-wide/16 v3, 0x0

    const/high16 v2, 0x3e80

    const/4 v7, 0x4

    .line 490
    invoke-virtual {p2}, LC/a;->r()F

    move-result v0

    iget-object v1, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 491
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_124

    .line 602
    :cond_19
    :goto_19
    :pswitch_19
    return-void

    .line 493
    :pswitch_1a
    iget-object v1, p0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_19

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_19

    .line 497
    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 498
    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->a(LD/a;)V

    .line 499
    invoke-static {p1}, Lx/a;->c(LD/a;)V

    .line 500
    iget-object v0, p0, LF/Q;->u:LE/d;

    invoke-virtual {v0, p1, v5}, LE/d;->a(LD/a;I)V

    .line 501
    invoke-static {p1}, Lx/a;->d(LD/a;)V

    goto :goto_19

    .line 505
    :pswitch_38
    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 506
    iget-object v1, p0, LF/Q;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 507
    iget-object v1, p0, LF/Q;->l:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    .line 508
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_4f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_55

    .line 510
    :cond_4f
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_19

    .line 512
    :cond_55
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_19

    .line 517
    :pswitch_5b
    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 518
    iget-object v1, p0, LF/Q;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 519
    iget-object v1, p0, LF/Q;->k:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    .line 520
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_72

    cmpl-float v0, v0, v2

    if-lez v0, :cond_78

    .line 522
    :cond_72
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_19

    .line 524
    :cond_78
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto :goto_19

    .line 529
    :pswitch_7e
    iget-object v0, p0, LF/Q;->w:LE/o;

    if-eqz v0, :cond_19

    .line 532
    invoke-direct {p0, p2}, LF/Q;->a(LC/a;)F

    move-result v0

    .line 533
    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_19

    .line 537
    invoke-virtual {p0, p1, v0}, LF/Q;->a(LD/a;F)V

    .line 539
    const/high16 v0, 0x3f80

    .line 540
    iget-object v1, p0, LF/Q;->z:Lh/e;

    if-eqz v1, :cond_a8

    .line 542
    iget-object v0, p0, LF/Q;->z:Lh/e;

    invoke-virtual {v0, p1}, Lh/e;->a(LD/a;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c8

    div-float/2addr v0, v1

    .line 543
    const v1, 0x3f7d70a4

    cmpl-float v1, v0, v1

    if-lez v1, :cond_a8

    .line 544
    const/4 v1, 0x0

    iput-object v1, p0, LF/Q;->z:Lh/e;

    .line 554
    :cond_a8
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    iget-object v2, p0, LF/Q;->f:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p0, LF/Q;->f:[F

    aget v3, v3, v5

    iget-object v4, p0, LF/Q;->f:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p0, LF/Q;->f:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    mul-float/2addr v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 560
    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 561
    iget-object v0, p0, LF/Q;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 562
    iget-object v0, p0, LF/Q;->y:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_19

    .line 569
    :pswitch_d4
    iget-object v1, p0, LF/Q;->n:LE/a;

    if-eqz v1, :cond_19

    .line 572
    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 573
    iget-object v1, p0, LF/Q;->i:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 574
    iget-object v1, p0, LF/Q;->n:LE/a;

    invoke-virtual {v1, p1}, LE/a;->c(LD/a;)V

    .line 575
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_ef

    cmpl-float v0, v0, v2

    if-lez v0, :cond_f6

    .line 577
    :cond_ef
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_19

    .line 579
    :cond_f6
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_19

    .line 587
    :pswitch_fd
    iget-object v0, p0, LF/Q;->o:LE/o;

    if-eqz v0, :cond_19

    .line 590
    invoke-direct {p0, p2}, LF/Q;->b(LC/a;)F

    move-result v0

    .line 591
    float-to-double v1, v0

    cmpg-double v1, v1, v3

    if-lez v1, :cond_19

    .line 595
    invoke-virtual {p0, p1, v0}, LF/Q;->b(LD/a;F)V

    .line 596
    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 597
    iget-object v0, p0, LF/Q;->o:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 598
    iget-object v0, p0, LF/Q;->r:LE/a;

    invoke-virtual {v0, p1}, LE/a;->c(LD/a;)V

    .line 599
    iget-object v0, p0, LF/Q;->q:LE/d;

    invoke-virtual {v0, p1, v7}, LE/d;->a(LD/a;I)V

    goto/16 :goto_19

    .line 491
    nop

    :pswitch_data_124
    .packed-switch 0x2
        :pswitch_1a
        :pswitch_19
        :pswitch_38
        :pswitch_5b
        :pswitch_7e
        :pswitch_d4
        :pswitch_fd
    .end packed-switch
.end method

.method public b()I
    .registers 4

    .prologue
    .line 1285
    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0}, LE/o;->d()I

    move-result v0

    add-int/lit16 v0, v0, 0x1d0

    iget-object v1, p0, LF/Q;->j:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->k:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->l:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LF/Q;->m:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1290
    iget-object v1, p0, LF/Q;->n:LE/a;

    if-eqz v1, :cond_2f

    .line 1291
    iget-object v1, p0, LF/Q;->n:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1293
    :cond_2f
    iget-object v1, p0, LF/Q;->s:LE/o;

    if-eqz v1, :cond_3a

    .line 1294
    iget-object v1, p0, LF/Q;->s:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1296
    :cond_3a
    iget-object v1, p0, LF/Q;->t:Lx/c;

    if-eqz v1, :cond_45

    .line 1297
    iget-object v1, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v1}, Lx/c;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 1299
    :cond_45
    iget-object v1, p0, LF/Q;->u:LE/d;

    if-eqz v1, :cond_50

    .line 1300
    iget-object v1, p0, LF/Q;->u:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1302
    :cond_50
    iget-object v1, p0, LF/Q;->v:LE/d;

    if-eqz v1, :cond_5b

    .line 1303
    iget-object v1, p0, LF/Q;->v:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1305
    :cond_5b
    iget-object v1, p0, LF/Q;->w:LE/o;

    if-eqz v1, :cond_66

    .line 1306
    iget-object v1, p0, LF/Q;->w:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1308
    :cond_66
    iget-object v1, p0, LF/Q;->x:LE/i;

    if-eqz v1, :cond_71

    .line 1309
    iget-object v1, p0, LF/Q;->x:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1311
    :cond_71
    iget-object v1, p0, LF/Q;->y:LE/d;

    if-eqz v1, :cond_7c

    .line 1312
    iget-object v1, p0, LF/Q;->y:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1315
    :cond_7c
    iget-object v1, p0, LF/Q;->o:LE/o;

    if-eqz v1, :cond_87

    .line 1316
    iget-object v1, p0, LF/Q;->o:LE/o;

    invoke-virtual {v1}, LE/o;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1318
    :cond_87
    iget-object v1, p0, LF/Q;->p:LE/i;

    if-eqz v1, :cond_92

    .line 1319
    iget-object v1, p0, LF/Q;->p:LE/i;

    invoke-virtual {v1}, LE/i;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1321
    :cond_92
    iget-object v1, p0, LF/Q;->q:LE/d;

    if-eqz v1, :cond_9d

    .line 1322
    iget-object v1, p0, LF/Q;->q:LE/d;

    invoke-virtual {v1}, LE/d;->d()I

    move-result v1

    add-int/2addr v0, v1

    .line 1324
    :cond_9d
    iget-object v1, p0, LF/Q;->r:LE/a;

    if-eqz v1, :cond_a8

    .line 1325
    iget-object v1, p0, LF/Q;->r:LE/a;

    invoke-virtual {v1}, LE/a;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1332
    :cond_a8
    add-int/lit8 v0, v0, 0x18

    .line 1333
    iget-object v1, p0, LF/Q;->B:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_b1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    .line 1334
    invoke-virtual {v0}, Lo/af;->m()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_b1

    .line 1341
    :cond_c4
    add-int/lit8 v0, v1, 0x18

    .line 1342
    iget-object v1, p0, LF/Q;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_cd
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    .line 1343
    invoke-virtual {v0}, Lo/af;->m()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_cd

    .line 1347
    :cond_e0
    return v1
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 650
    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 651
    iget-object v0, p0, LF/Q;->j:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 652
    iget-object v0, p0, LF/Q;->l:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    .line 653
    iget-object v0, p0, LF/Q;->k:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    .line 654
    iget-object v0, p0, LF/Q;->m:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 656
    iget-object v0, p0, LF/Q;->n:LE/a;

    if-eqz v0, :cond_22

    .line 657
    iget-object v0, p0, LF/Q;->n:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    .line 660
    :cond_22
    iget-object v0, p0, LF/Q;->s:LE/o;

    if-eqz v0, :cond_3a

    .line 661
    iget-object v0, p0, LF/Q;->s:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 662
    iget-object v0, p0, LF/Q;->t:Lx/c;

    invoke-virtual {v0, p1}, Lx/c;->c(LD/a;)V

    .line 663
    iget-object v0, p0, LF/Q;->u:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 664
    iget-object v0, p0, LF/Q;->v:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 667
    :cond_3a
    iget-object v0, p0, LF/Q;->w:LE/o;

    if-eqz v0, :cond_4d

    .line 668
    iget-object v0, p0, LF/Q;->w:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 669
    iget-object v0, p0, LF/Q;->x:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 670
    iget-object v0, p0, LF/Q;->y:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 674
    :cond_4d
    iget-object v0, p0, LF/Q;->o:LE/o;

    if-eqz v0, :cond_65

    .line 675
    iget-object v0, p0, LF/Q;->o:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 676
    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->c(LD/a;)V

    .line 677
    iget-object v0, p0, LF/Q;->q:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 678
    iget-object v0, p0, LF/Q;->r:LE/a;

    invoke-virtual {v0, p1}, LE/a;->b(LD/a;)V

    .line 680
    :cond_65
    return-void
.end method

.method b(LD/a;F)V
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 794
    iget v0, p0, LF/Q;->E:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_7

    .line 828
    :cond_6
    return-void

    .line 797
    :cond_7
    iput p2, p0, LF/Q;->E:F

    .line 800
    const/high16 v0, 0x4180

    mul-float/2addr v0, p2

    .line 801
    const/high16 v1, 0x3f80

    div-float/2addr v1, p2

    .line 802
    const/high16 v2, 0x3f80

    div-float/2addr v2, v0

    .line 804
    iget-object v0, p0, LF/Q;->p:LE/i;

    invoke-virtual {v0, p1}, LE/i;->a(LD/a;)V

    .line 805
    iget-object v0, p0, LF/Q;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 806
    iget-object v0, p0, LF/Q;->e:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    invoke-virtual {v0}, Lo/ad;->g()I

    move-result v4

    .line 807
    :cond_27
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 808
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/af;

    .line 809
    invoke-virtual {v0}, Lo/af;->b()Lo/X;

    move-result-object v5

    .line 810
    invoke-virtual {v5}, Lo/X;->b()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    .line 811
    invoke-virtual {v0}, Lo/af;->e()Lo/aj;

    move-result-object v0

    invoke-static {v0}, LF/Q;->a(Lo/aj;)F

    move-result v0

    .line 812
    const/high16 v7, 0x4000

    invoke-direct {p0, v0, v4}, LF/Q;->c(FI)F

    move-result v0

    mul-float/2addr v7, v0

    .line 815
    const/4 v0, 0x0

    :goto_4d
    if-ge v0, v6, :cond_27

    .line 816
    invoke-virtual {v5, v0}, Lo/X;->b(I)F

    move-result v8

    .line 817
    mul-float/2addr v8, v2

    .line 818
    mul-float v9, v7, v1

    const/high16 v10, 0x4700

    mul-float/2addr v9, v10

    float-to-int v9, v9

    .line 819
    const/high16 v10, 0x4780

    mul-float/2addr v8, v10

    float-to-int v8, v8

    .line 820
    const v10, 0x8000

    sub-int/2addr v10, v9

    .line 821
    const v11, 0x8000

    add-int/2addr v9, v11

    .line 822
    iget-object v11, p0, LF/Q;->p:LE/i;

    invoke-virtual {v11, v9, v8}, LE/i;->a(II)V

    .line 823
    iget-object v11, p0, LF/Q;->p:LE/i;

    invoke-virtual {v11, v10, v8}, LE/i;->a(II)V

    .line 824
    iget-object v8, p0, LF/Q;->p:LE/i;

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, LE/i;->a(II)V

    .line 825
    iget-object v8, p0, LF/Q;->p:LE/i;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, LE/i;->a(II)V

    .line 815
    add-int/lit8 v0, v0, 0x1

    goto :goto_4d
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 693
    iget-object v0, p0, LF/Q;->i:LE/o;

    invoke-virtual {v0}, LE/o;->a()I

    move-result v0

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method
