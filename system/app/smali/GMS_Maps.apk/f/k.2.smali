.class public LF/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LF/T;


# instance fields
.field private final a:Lo/ad;

.field private final b:Lo/aq;

.field private final c:LA/c;

.field private d:[Ljava/lang/String;

.field private e:[Ljava/lang/String;

.field private f:I

.field private g:LF/P;

.field private final h:[F

.field private i:J

.field private j:LF/m;


# direct methods
.method private constructor <init>(Lo/aq;LA/c;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, LF/k;->h:[F

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LF/k;->i:J

    .line 68
    iput-object p1, p0, LF/k;->b:Lo/aq;

    .line 69
    iput-object p2, p0, LF/k;->c:LA/c;

    .line 70
    invoke-virtual {p1}, Lo/aq;->i()Lo/ad;

    move-result-object v0

    iput-object v0, p0, LF/k;->a:Lo/ad;

    .line 71
    return-void
.end method

.method public static a(Lo/ap;LD/a;)LF/k;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 57
    new-instance v0, LF/k;

    invoke-interface {p0}, Lo/ap;->d()Lo/aq;

    move-result-object v1

    invoke-interface {p0}, Lo/ap;->g()LA/c;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LF/k;-><init>(Lo/aq;LA/c;)V

    .line 58
    instance-of v1, p0, Lo/x;

    if-eqz v1, :cond_17

    .line 59
    check-cast p0, Lo/x;

    .line 60
    invoke-direct {v0, p0, p1}, LF/k;->a(Lo/x;LD/a;)V

    .line 64
    :goto_16
    return-object v0

    .line 62
    :cond_17
    invoke-direct {v0}, LF/k;->k()V

    goto :goto_16
.end method

.method private a(Lo/x;LD/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-virtual {p1}, Lo/x;->f()[B

    move-result-object v0

    iget-object v1, p0, LF/k;->b:Lo/aq;

    invoke-static {v0, v1, p2}, LF/P;->a([BLo/aq;LD/a;)LF/P;

    move-result-object v0

    iput-object v0, p0, LF/k;->g:LF/P;

    .line 81
    invoke-virtual {p1}, Lo/x;->a()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LF/k;->d:[Ljava/lang/String;

    .line 82
    invoke-virtual {p1}, Lo/x;->b()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LF/k;->e:[Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Lo/x;->c()I

    move-result v0

    iput v0, p0, LF/k;->f:I

    .line 84
    return-void
.end method

.method private k()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 74
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, LF/k;->d:[Ljava/lang/String;

    .line 75
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, LF/k;->e:[Ljava/lang/String;

    .line 76
    const/4 v0, -0x1

    iput v0, p0, LF/k;->f:I

    .line 77
    return-void
.end method


# virtual methods
.method public a(LC/a;Lcom/google/android/maps/driveabout/vector/q;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 126
    sget-boolean v1, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    if-eqz v1, :cond_8

    .line 127
    const v0, 0x8000

    .line 129
    :cond_8
    iget-object v1, p0, LF/k;->g:LF/P;

    if-eqz v1, :cond_e

    .line 130
    or-int/lit8 v0, v0, 0x2

    .line 132
    :cond_e
    return v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 249
    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_9

    .line 250
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1, p2}, LF/P;->a(J)V

    .line 252
    :cond_9
    return-void
.end method

.method public a(LC/a;ILjava/util/Collection;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 256
    iget-object v1, p0, LF/k;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 257
    invoke-interface {p3, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 259
    :cond_e
    return-void
.end method

.method public a(LC/a;LD/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 223
    return-void
.end method

.method public a(LC/a;Ljava/util/Collection;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 263
    iget-object v1, p0, LF/k;->e:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 264
    invoke-interface {p2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 266
    :cond_e
    return-void
.end method

.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_9

    .line 89
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1}, LF/P;->a(LD/a;)V

    .line 91
    :cond_9
    iget-object v0, p0, LF/k;->j:LF/m;

    if-eqz v0, :cond_15

    .line 92
    iget-object v0, p0, LF/k;->j:LF/m;

    invoke-virtual {v0, p1}, LF/m;->a(LD/a;)V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, LF/k;->j:LF/m;

    .line 95
    :cond_15
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 152
    iget-object v0, p0, LF/k;->g:LF/P;

    if-nez v0, :cond_d

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-ne v0, v6, :cond_d

    .line 184
    :goto_c
    return-void

    .line 157
    :cond_d
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 158
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 159
    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v2

    iget-wide v4, p0, LF/k;->i:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_76

    .line 160
    invoke-virtual {p2}, LC/a;->g()J

    move-result-wide v2

    iput-wide v2, p0, LF/k;->i:J

    .line 161
    iget-object v0, p0, LF/k;->a:Lo/ad;

    invoke-virtual {v0}, Lo/ad;->d()Lo/T;

    move-result-object v0

    .line 165
    invoke-virtual {p2}, LC/a;->j()Z

    move-result v2

    if-nez v2, :cond_6a

    invoke-virtual {p2}, LC/a;->q()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_6a

    invoke-virtual {p2}, LC/a;->p()F

    move-result v2

    cmpl-float v2, v2, v7

    if-nez v2, :cond_6a

    invoke-virtual {p2}, LC/a;->r()F

    move-result v2

    invoke-virtual {p2}, LC/a;->r()F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-nez v2, :cond_6a

    .line 168
    iget-object v2, p1, LD/a;->k:[F

    invoke-virtual {p2, v0, v2}, LC/a;->a(Lo/T;[F)V

    .line 169
    iget-object v0, p1, LD/a;->k:[F

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p1, LD/a;->k:[F

    aget v2, v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    .line 173
    :cond_6a
    iget-object v2, p0, LF/k;->a:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LF/k;->h:[F

    invoke-static {p1, p2, v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F[F)V

    .line 176
    :cond_76
    iget-object v0, p0, LF/k;->h:[F

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/vector/be;->a(Ljavax/microedition/khronos/opengles/GL10;[F)V

    .line 178
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-ne v0, v6, :cond_8a

    .line 179
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1, p2, p3}, LF/P;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 183
    :cond_86
    :goto_86
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_c

    .line 180
    :cond_8a
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    const/16 v2, 0xf

    if-ne v0, v2, :cond_86

    .line 181
    sget-object v0, LF/U;->a:LF/U;

    invoke-virtual {v0, p1, p2, p3}, LF/U;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_86
.end method

.method public a(LF/m;)V
    .registers 2
    .parameter

    .prologue
    .line 212
    iput-object p1, p0, LF/k;->j:LF/m;

    .line 213
    return-void
.end method

.method public a(Ly/b;)V
    .registers 2
    .parameter

    .prologue
    .line 121
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 218
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 110
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aF;)Z
    .registers 3
    .parameter

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/googlenav/common/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 227
    const/4 v0, 0x0

    return v0
.end method

.method public b()Lo/aq;
    .registers 2

    .prologue
    .line 188
    iget-object v0, p0, LF/k;->b:Lo/aq;

    return-object v0
.end method

.method public b(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_9

    .line 100
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0, p1}, LF/P;->b(LD/a;)V

    .line 102
    :cond_9
    iget-object v0, p0, LF/k;->j:LF/m;

    if-eqz v0, :cond_15

    .line 103
    iget-object v0, p0, LF/k;->j:LF/m;

    invoke-virtual {v0, p1}, LF/m;->b(LD/a;)V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, LF/k;->j:LF/m;

    .line 106
    :cond_15
    return-void
.end method

.method public b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 138
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    .line 139
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    .line 141
    invoke-static {p1, v0}, LF/P;->a(LD/a;Lcom/google/android/maps/driveabout/vector/q;)V

    .line 145
    return-void
.end method

.method public b(Lcom/google/googlenav/common/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public c()LA/c;
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, LF/k;->c:LA/c;

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 193
    const/4 v0, -0x1

    return v0
.end method

.method public e()LF/m;
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, LF/k;->j:LF/m;

    return-object v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_e

    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0}, LF/P;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public g()V
    .registers 2

    .prologue
    .line 242
    iget-object v0, p0, LF/k;->g:LF/P;

    if-eqz v0, :cond_9

    .line 243
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0}, LF/P;->e()V

    .line 245
    :cond_9
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 270
    iget v0, p0, LF/k;->f:I

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 275
    iget-object v0, p0, LF/k;->g:LF/P;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, LF/k;->g:LF/P;

    invoke-virtual {v0}, LF/P;->a()I

    move-result v0

    goto :goto_5
.end method

.method public j()I
    .registers 3

    .prologue
    .line 280
    const/16 v0, 0x88

    .line 281
    iget-object v1, p0, LF/k;->g:LF/P;

    if-eqz v1, :cond_d

    .line 282
    iget-object v1, p0, LF/k;->g:LF/P;

    invoke-virtual {v1}, LF/P;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_d
    return v0
.end method
