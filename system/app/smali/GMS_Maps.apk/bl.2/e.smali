.class public final enum Lbl/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbl/e;

.field public static final enum b:Lbl/e;

.field public static final enum c:Lbl/e;

.field public static final enum d:Lbl/e;

.field public static final enum e:Lbl/e;

.field public static final enum f:Lbl/e;

.field private static final synthetic h:[Lbl/e;


# instance fields
.field g:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 37
    new-instance v0, Lbl/e;

    const-string v1, "IS_CHECKED_IN"

    invoke-direct {v0, v1, v4, v3}, Lbl/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbl/e;->a:Lbl/e;

    .line 38
    new-instance v0, Lbl/e;

    const-string v1, "JUST_REVIEWED"

    invoke-direct {v0, v1, v3, v3}, Lbl/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbl/e;->b:Lbl/e;

    .line 39
    new-instance v0, Lbl/e;

    const-string v1, "JUST_CALLED"

    invoke-direct {v0, v1, v5, v3}, Lbl/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbl/e;->c:Lbl/e;

    .line 42
    new-instance v0, Lbl/e;

    const-string v1, "DISTANCE_THRESHOLD"

    invoke-direct {v0, v1, v6, v4}, Lbl/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbl/e;->d:Lbl/e;

    .line 43
    new-instance v0, Lbl/e;

    const-string v1, "HOMEPAGE_CLICK"

    invoke-direct {v0, v1, v7, v3}, Lbl/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbl/e;->e:Lbl/e;

    .line 44
    new-instance v0, Lbl/e;

    const-string v1, "DIRECTIONS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Lbl/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lbl/e;->f:Lbl/e;

    .line 36
    const/4 v0, 0x6

    new-array v0, v0, [Lbl/e;

    sget-object v1, Lbl/e;->a:Lbl/e;

    aput-object v1, v0, v4

    sget-object v1, Lbl/e;->b:Lbl/e;

    aput-object v1, v0, v3

    sget-object v1, Lbl/e;->c:Lbl/e;

    aput-object v1, v0, v5

    sget-object v1, Lbl/e;->d:Lbl/e;

    aput-object v1, v0, v6

    sget-object v1, Lbl/e;->e:Lbl/e;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lbl/e;->f:Lbl/e;

    aput-object v2, v0, v1

    sput-object v0, Lbl/e;->h:[Lbl/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-boolean p3, p0, Lbl/e;->g:Z

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbl/e;
    .registers 2
    .parameter

    .prologue
    .line 36
    const-class v0, Lbl/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbl/e;

    return-object v0
.end method

.method public static values()[Lbl/e;
    .registers 1

    .prologue
    .line 36
    sget-object v0, Lbl/e;->h:[Lbl/e;

    invoke-virtual {v0}, [Lbl/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbl/e;

    return-object v0
.end method
