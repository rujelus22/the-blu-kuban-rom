.class public Lbl/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private h:Lbl/g;

.field private i:Lbl/f;

.field private j:Lbl/i;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lbl/h;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbl/h;->f:Ljava/util/List;

    .line 227
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbl/h;->g:Ljava/util/List;

    .line 228
    sget-object v0, Lbl/i;->a:Lbl/i;

    iput-object v0, p0, Lbl/h;->j:Lbl/i;

    .line 229
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/h;
    .registers 9
    .parameter

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 338
    if-nez p0, :cond_8

    .line 339
    const/4 v0, 0x0

    .line 398
    :goto_7
    return-object v0

    .line 341
    :cond_8
    new-instance v0, Lbl/h;

    invoke-direct {v0}, Lbl/h;-><init>()V

    .line 343
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->a:Ljava/lang/String;

    .line 345
    invoke-static {p0, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->b:Ljava/lang/String;

    .line 347
    const/4 v2, 0x4

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->e:Ljava/lang/String;

    .line 349
    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v2

    iput v2, v0, Lbl/h;->c:I

    .line 352
    sget-object v2, Lbl/i;->a:Lbl/i;

    iput-object v2, v0, Lbl/h;->j:Lbl/i;

    .line 353
    const/16 v2, 0x8

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v2

    .line 355
    if-ne v2, v3, :cond_38

    .line 356
    sget-object v2, Lbl/i;->b:Lbl/i;

    iput-object v2, v0, Lbl/h;->j:Lbl/i;

    .line 359
    :cond_38
    iget-object v2, v0, Lbl/h;->e:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_44

    .line 360
    const-string v2, "uddq_experiment"

    iput-object v2, v0, Lbl/h;->e:Ljava/lang/String;

    .line 363
    :cond_44
    const/4 v2, 0x7

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lbl/h;->d:Ljava/lang/String;

    .line 365
    iget-object v2, v0, Lbl/h;->d:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_57

    .line 368
    const-string v2, "Thank you for your feedback."

    iput-object v2, v0, Lbl/h;->d:Ljava/lang/String;

    .line 371
    :cond_57
    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    move v2, v1

    .line 372
    :goto_5c
    if-ge v2, v3, :cond_6e

    .line 373
    invoke-virtual {p0, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, Lbl/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/c;

    move-result-object v4

    .line 375
    iget-object v5, v0, Lbl/h;->f:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    add-int/lit8 v2, v2, 0x1

    goto :goto_5c

    .line 378
    :cond_6e
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 379
    :goto_72
    if-ge v1, v2, :cond_86

    .line 380
    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lbl/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/d;

    move-result-object v3

    .line 382
    if-eqz v3, :cond_83

    .line 383
    iget-object v4, v0, Lbl/h;->g:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    :cond_83
    add-int/lit8 v1, v1, 0x1

    goto :goto_72

    .line 387
    :cond_86
    const/16 v1, 0x9

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->k:Ljava/lang/String;

    .line 389
    const/16 v1, 0xa

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->l:Ljava/lang/String;

    .line 391
    const/16 v1, 0xb

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->m:Ljava/lang/String;

    .line 393
    const/16 v1, 0xc

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lbl/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lbl/h;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->n:Lbl/h;

    .line 396
    const/16 v1, 0xd

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbl/h;->o:Ljava/lang/String;

    goto/16 :goto_7
.end method

.method static synthetic a(Lbl/h;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lbl/h;->g:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lbl/h;->e:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lbl/f;)V
    .registers 2
    .parameter

    .prologue
    .line 293
    iput-object p1, p0, Lbl/h;->i:Lbl/f;

    .line 294
    return-void
.end method

.method public a(Lbl/g;)V
    .registers 2
    .parameter

    .prologue
    .line 281
    iput-object p1, p0, Lbl/h;->h:Lbl/g;

    .line 282
    return-void
.end method

.method public a(Lbl/e;)Z
    .registers 4
    .parameter

    .prologue
    .line 264
    iget-object v0, p0, Lbl/h;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbl/d;

    .line 265
    iget-object v0, v0, Lbl/d;->a:Lbl/e;

    if-ne v0, p1, :cond_6

    .line 266
    const/4 v0, 0x1

    .line 269
    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 232
    iget-object v0, p0, Lbl/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 236
    iget-object v0, p0, Lbl/h;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .registers 2

    .prologue
    .line 244
    iget-object v0, p0, Lbl/h;->f:Ljava/util/List;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Lbl/h;->g:Ljava/util/List;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .registers 2

    .prologue
    .line 252
    iget-object v0, p0, Lbl/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Lbl/h;->h:Lbl/g;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public h()Lbl/g;
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lbl/h;->h:Lbl/g;

    return-object v0
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 297
    sget-object v0, Lbl/i;->b:Lbl/i;

    iget-object v1, p0, Lbl/h;->j:Lbl/i;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public j()Z
    .registers 2

    .prologue
    .line 301
    iget-object v0, p0, Lbl/h;->k:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public k()Ljava/lang/String;
    .registers 2

    .prologue
    .line 308
    iget-object v0, p0, Lbl/h;->k:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .registers 2

    .prologue
    .line 315
    iget-object v0, p0, Lbl/h;->l:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 322
    iget-object v0, p0, Lbl/h;->m:Ljava/lang/String;

    return-object v0
.end method

.method public n()Lbl/h;
    .registers 2

    .prologue
    .line 330
    iget-object v0, p0, Lbl/h;->n:Lbl/h;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .registers 2

    .prologue
    .line 334
    iget-object v0, p0, Lbl/h;->o:Ljava/lang/String;

    return-object v0
.end method
