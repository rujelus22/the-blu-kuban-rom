.class public final enum Lah/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lah/c;

.field public static final enum b:Lah/c;

.field public static final enum c:Lah/c;

.field private static final synthetic d:[Lah/c;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v0, Lah/c;

    const-string v1, "POI"

    invoke-direct {v0, v1, v2}, Lah/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lah/c;->a:Lah/c;

    .line 69
    new-instance v0, Lah/c;

    const-string v1, "PLACEMARK"

    invoke-direct {v0, v1, v3}, Lah/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lah/c;->b:Lah/c;

    .line 70
    new-instance v0, Lah/c;

    const-string v1, "MY_LOCATION"

    invoke-direct {v0, v1, v4}, Lah/c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lah/c;->c:Lah/c;

    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [Lah/c;

    sget-object v1, Lah/c;->a:Lah/c;

    aput-object v1, v0, v2

    sget-object v1, Lah/c;->b:Lah/c;

    aput-object v1, v0, v3

    sget-object v1, Lah/c;->c:Lah/c;

    aput-object v1, v0, v4

    sput-object v0, Lah/c;->d:[Lah/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lah/c;
    .registers 2
    .parameter

    .prologue
    .line 67
    const-class v0, Lah/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lah/c;

    return-object v0
.end method

.method public static values()[Lah/c;
    .registers 1

    .prologue
    .line 67
    sget-object v0, Lah/c;->d:[Lah/c;

    invoke-virtual {v0}, [Lah/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lah/c;

    return-object v0
.end method
