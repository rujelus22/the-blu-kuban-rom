.class public Lo/E;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/at;


# instance fields
.field private final a:Lo/D;


# direct methods
.method protected constructor <init>(Lo/D;)V
    .registers 2
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lo/E;->a:Lo/D;

    .line 22
    return-void
.end method

.method public static a(Lo/D;)Lo/E;
    .registers 2
    .parameter

    .prologue
    .line 104
    new-instance v0, Lo/F;

    invoke-direct {v0}, Lo/F;-><init>()V

    invoke-virtual {v0, p0}, Lo/F;->a(Lo/D;)Lo/F;

    move-result-object v0

    invoke-virtual {v0}, Lo/F;->a()Lo/E;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lo/av;
    .registers 2

    .prologue
    .line 26
    sget-object v0, Lo/av;->c:Lo/av;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 79
    const/4 v0, 0x6

    invoke-virtual {p0}, Lo/E;->b()Lo/r;

    move-result-object v1

    invoke-virtual {v1}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 80
    return-void
.end method

.method public a(LA/c;)Z
    .registers 3
    .parameter

    .prologue
    .line 74
    sget-object v0, LA/c;->n:LA/c;

    if-ne p1, v0, :cond_a

    iget-object v0, p0, Lo/E;->a:Lo/D;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public a(Lo/at;)Z
    .registers 3
    .parameter

    .prologue
    .line 37
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lo/at;)I
    .registers 4
    .parameter

    .prologue
    .line 55
    if-nez p1, :cond_4

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0}, Lo/E;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_3
.end method

.method public b()Lo/r;
    .registers 2

    .prologue
    .line 30
    iget-object v0, p0, Lo/E;->a:Lo/D;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    return-object v0
.end method

.method public c()Lo/D;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lo/E;->a:Lo/D;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 16
    check-cast p1, Lo/at;

    invoke-virtual {p0, p1}, Lo/E;->b(Lo/at;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    if-ne p0, p1, :cond_5

    .line 69
    :cond_4
    :goto_4
    return v0

    .line 63
    :cond_5
    if-nez p1, :cond_d

    .line 64
    iget-object v2, p0, Lo/E;->a:Lo/D;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_4

    .line 66
    :cond_d
    instance-of v0, p1, Lo/E;

    if-nez v0, :cond_13

    move v0, v1

    .line 67
    goto :goto_4

    .line 69
    :cond_13
    iget-object v0, p0, Lo/E;->a:Lo/D;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    check-cast p1, Lo/E;

    iget-object v1, p1, Lo/E;->a:Lo/D;

    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 47
    .line 49
    iget-object v0, p0, Lo/E;->a:Lo/D;

    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_5
    add-int/lit8 v0, v0, 0x1f

    .line 50
    return v0

    .line 49
    :cond_8
    iget-object v0, p0, Lo/E;->a:Lo/D;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->hashCode()I

    move-result v0

    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lo/E;->b()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
