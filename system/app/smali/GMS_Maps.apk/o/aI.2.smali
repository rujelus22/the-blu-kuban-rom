.class public Lo/aI;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lo/o;

.field protected final b:Lo/n;

.field protected final c:I

.field protected final d:I


# direct methods
.method protected constructor <init>(Lo/o;Lo/n;II)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lo/aI;->a:Lo/o;

    .line 32
    iput-object p2, p0, Lo/aI;->b:Lo/n;

    .line 33
    iput p3, p0, Lo/aI;->c:I

    .line 34
    iput p4, p0, Lo/aI;->d:I

    .line 35
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/aI;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    .line 47
    const/4 v0, -0x1

    .line 50
    invoke-static {v2}, Lo/aI;->c(I)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 52
    invoke-static {p0, p1}, Lo/aL;->a(Ljava/io/DataInput;Lo/as;)Lo/n;

    move-result-object v3

    .line 54
    invoke-static {v2}, Lo/aI;->b(I)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 57
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    .line 59
    :cond_19
    new-instance v1, Lo/aJ;

    invoke-direct {v1, v3, v2, v0}, Lo/aJ;-><init>(Lo/n;II)V

    move-object v0, v1

    .line 66
    :goto_1f
    return-object v0

    .line 62
    :cond_20
    const/4 v0, 0x2

    invoke-static {v2, v0}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v0

    .line 66
    :goto_2b
    new-instance v1, Lo/aK;

    invoke-direct {v1, v0}, Lo/aK;-><init>(Lo/o;)V

    move-object v0, v1

    goto :goto_1f

    .line 62
    :cond_32
    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v0

    goto :goto_2b
.end method

.method static synthetic a(I)Z
    .registers 2
    .parameter

    .prologue
    .line 16
    invoke-static {p0}, Lo/aI;->b(I)Z

    move-result v0

    return v0
.end method

.method private static b(I)Z
    .registers 2
    .parameter

    .prologue
    .line 71
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .registers 2
    .parameter

    .prologue
    .line 75
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method
