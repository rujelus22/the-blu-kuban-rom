.class public abstract enum Lo/av;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lo/av;

.field public static final enum b:Lo/av;

.field public static final enum c:Lo/av;

.field public static final enum d:Lo/av;

.field public static final enum e:Lo/av;

.field private static final synthetic f:[Lo/av;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lo/aw;

    const-string v1, "SPOTLIGHT"

    invoke-direct {v0, v1, v2}, Lo/aw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo/av;->a:Lo/av;

    .line 41
    new-instance v0, Lo/ax;

    const-string v1, "HIGHLIGHT"

    invoke-direct {v0, v1, v3}, Lo/ax;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo/av;->b:Lo/av;

    .line 52
    new-instance v0, Lo/ay;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1, v4}, Lo/ay;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo/av;->c:Lo/av;

    .line 65
    new-instance v0, Lo/az;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v5}, Lo/az;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo/av;->d:Lo/av;

    .line 85
    new-instance v0, Lo/aA;

    const-string v1, "ALTERNATE_PAINTFE"

    invoke-direct {v0, v1, v6}, Lo/aA;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo/av;->e:Lo/av;

    .line 28
    const/4 v0, 0x5

    new-array v0, v0, [Lo/av;

    sget-object v1, Lo/av;->a:Lo/av;

    aput-object v1, v0, v2

    sget-object v1, Lo/av;->b:Lo/av;

    aput-object v1, v0, v3

    sget-object v1, Lo/av;->c:Lo/av;

    aput-object v1, v0, v4

    sget-object v1, Lo/av;->d:Lo/av;

    aput-object v1, v0, v5

    sget-object v1, Lo/av;->e:Lo/av;

    aput-object v1, v0, v6

    sput-object v0, Lo/av;->f:[Lo/av;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILo/au;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lo/av;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lo/av;
    .registers 2
    .parameter

    .prologue
    .line 28
    const-class v0, Lo/av;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lo/av;

    return-object v0
.end method

.method public static values()[Lo/av;
    .registers 1

    .prologue
    .line 28
    sget-object v0, Lo/av;->f:[Lo/av;

    invoke-virtual {v0}, [Lo/av;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lo/av;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/at;
.end method
