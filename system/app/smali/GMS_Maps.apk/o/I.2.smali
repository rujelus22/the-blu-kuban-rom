.class public Lo/I;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:F

.field private final e:Ljava/lang/String;

.field private final f:Lo/aj;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:F


# direct methods
.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    iput p1, p0, Lo/I;->a:I

    .line 267
    iput-object p2, p0, Lo/I;->b:Ljava/lang/String;

    .line 268
    iput p3, p0, Lo/I;->c:I

    .line 269
    const/high16 v0, 0x3f80

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p0, Lo/I;->d:F

    .line 270
    iput-object p4, p0, Lo/I;->e:Ljava/lang/String;

    .line 271
    iput-object p5, p0, Lo/I;->f:Lo/aj;

    .line 272
    iput p6, p0, Lo/I;->g:I

    .line 273
    iput-object p7, p0, Lo/I;->h:Ljava/lang/String;

    .line 274
    iput p8, p0, Lo/I;->i:F

    .line 275
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;Lo/ak;Ljava/util/List;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x1

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 303
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v1

    .line 306
    invoke-static {v1}, Lo/I;->b(I)Z

    move-result v0

    if-eqz v0, :cond_97

    .line 307
    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    .line 308
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    .line 312
    :goto_1a
    invoke-static {v1}, Lo/I;->c(I)Z

    move-result v0

    if-eqz v0, :cond_95

    .line 314
    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Li/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    .line 319
    :goto_2c
    invoke-static {v1}, Lo/I;->d(I)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 320
    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object p2

    .line 328
    :cond_36
    :goto_36
    const/4 v8, 0x0

    .line 329
    invoke-static {v1}, Lo/I;->e(I)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 330
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-static {v0}, Lo/O;->a(I)F

    move-result v8

    .line 335
    :cond_45
    invoke-static {v1, v11}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_80

    if-eq v1, v11, :cond_80

    .line 337
    new-instance v0, Lo/I;

    xor-int/lit8 v1, v1, 0x8

    invoke-virtual {p2}, Lo/ak;->a()Lo/aj;

    move-result-object v5

    invoke-virtual {p2}, Lo/ak;->c()I

    move-result v6

    invoke-virtual {p2}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    new-instance v0, Lo/I;

    const/4 v6, -0x1

    const/high16 v8, -0x4080

    move v1, v11

    move-object v2, v10

    move v3, v9

    move-object v4, v10

    move-object v5, v10

    move-object v7, v10

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 359
    :goto_74
    return-void

    .line 321
    :cond_75
    invoke-static {v1}, Lo/I;->a(I)Z

    move-result v0

    if-nez v0, :cond_36

    .line 324
    invoke-static {}, Lo/H;->e()Lo/ak;

    move-result-object p2

    goto :goto_36

    .line 349
    :cond_80
    new-instance v0, Lo/I;

    invoke-virtual {p2}, Lo/ak;->a()Lo/aj;

    move-result-object v5

    invoke-virtual {p2}, Lo/ak;->c()I

    move-result v6

    invoke-virtual {p2}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v8}, Lo/I;-><init>(ILjava/lang/String;ILjava/lang/String;Lo/aj;ILjava/lang/String;F)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_74

    :cond_95
    move-object v4, v10

    goto :goto_2c

    :cond_97
    move v3, v9

    move-object v2, v10

    goto :goto_1a
.end method

.method private static a(I)Z
    .registers 2
    .parameter

    .prologue
    .line 367
    invoke-static {p0}, Lo/I;->c(I)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {p0}, Lo/I;->b(I)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private static b(I)Z
    .registers 2
    .parameter

    .prologue
    .line 375
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static c(I)Z
    .registers 2
    .parameter

    .prologue
    .line 384
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static d(I)Z
    .registers 2
    .parameter

    .prologue
    .line 392
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static e(I)Z
    .registers 2
    .parameter

    .prologue
    .line 400
    const/16 v0, 0x10

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method private static f(I)Z
    .registers 2
    .parameter

    .prologue
    .line 408
    const/16 v0, 0x8

    invoke-static {p0, v0}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Z
    .registers 2

    .prologue
    .line 362
    iget v0, p0, Lo/I;->a:I

    invoke-static {v0}, Lo/I;->a(I)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 371
    iget v0, p0, Lo/I;->a:I

    invoke-static {v0}, Lo/I;->b(I)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 380
    iget v0, p0, Lo/I;->a:I

    invoke-static {v0}, Lo/I;->c(I)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 388
    iget v0, p0, Lo/I;->a:I

    invoke-static {v0}, Lo/I;->d(I)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 396
    iget v0, p0, Lo/I;->a:I

    invoke-static {v0}, Lo/I;->e(I)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 472
    if-ne p0, p1, :cond_6

    move v1, v0

    .line 516
    :cond_5
    :goto_5
    return v1

    .line 475
    :cond_6
    if-eqz p1, :cond_5

    .line 478
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_5

    .line 481
    check-cast p1, Lo/I;

    .line 482
    iget v2, p0, Lo/I;->a:I

    iget v3, p1, Lo/I;->a:I

    if-ne v2, v3, :cond_5

    .line 485
    iget v2, p0, Lo/I;->i:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/I;->i:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 489
    iget-object v2, p0, Lo/I;->b:Ljava/lang/String;

    if-nez v2, :cond_5a

    .line 490
    iget-object v2, p1, Lo/I;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 496
    :cond_30
    iget-object v2, p0, Lo/I;->f:Lo/aj;

    if-nez v2, :cond_65

    .line 497
    iget-object v2, p1, Lo/I;->f:Lo/aj;

    if-nez v2, :cond_5

    .line 503
    :cond_38
    iget-object v2, p0, Lo/I;->h:Ljava/lang/String;

    if-nez v2, :cond_70

    .line 504
    iget-object v2, p1, Lo/I;->h:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 512
    :cond_40
    iget v2, p0, Lo/I;->c:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lo/I;->c:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 516
    iget-object v2, p0, Lo/I;->e:Ljava/lang/String;

    if-nez v2, :cond_7d

    iget-object v2, p1, Lo/I;->e:Ljava/lang/String;

    if-nez v2, :cond_7b

    :goto_58
    move v1, v0

    goto :goto_5

    .line 493
    :cond_5a
    iget-object v2, p0, Lo/I;->b:Ljava/lang/String;

    iget-object v3, p1, Lo/I;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_30

    goto :goto_5

    .line 500
    :cond_65
    iget-object v2, p0, Lo/I;->f:Lo/aj;

    iget-object v3, p1, Lo/I;->f:Lo/aj;

    invoke-virtual {v2, v3}, Lo/aj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_38

    goto :goto_5

    .line 507
    :cond_70
    iget-object v2, p0, Lo/I;->h:Ljava/lang/String;

    iget-object v3, p1, Lo/I;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_40

    goto :goto_5

    :cond_7b
    move v0, v1

    .line 516
    goto :goto_58

    :cond_7d
    iget-object v0, p0, Lo/I;->e:Ljava/lang/String;

    iget-object v1, p1, Lo/I;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_58
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 404
    iget v0, p0, Lo/I;->a:I

    invoke-static {v0}, Lo/I;->f(I)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 417
    iget-object v0, p0, Lo/I;->b:Ljava/lang/String;

    return-object v0
.end method

.method public h()F
    .registers 2

    .prologue
    .line 427
    iget v0, p0, Lo/I;->d:F

    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 458
    .line 460
    iget v0, p0, Lo/I;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 461
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/I;->i:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 462
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lo/I;->b:Ljava/lang/String;

    if-nez v0, :cond_38

    move v0, v1

    :goto_15
    add-int/2addr v0, v2

    .line 463
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lo/I;->f:Lo/aj;

    if-nez v0, :cond_3f

    move v0, v1

    :goto_1d
    add-int/2addr v0, v2

    .line 464
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lo/I;->h:Ljava/lang/String;

    if-nez v0, :cond_46

    move v0, v1

    :goto_25
    add-int/2addr v0, v2

    .line 465
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lo/I;->c:I

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 466
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lo/I;->e:Ljava/lang/String;

    if-nez v2, :cond_4d

    :goto_36
    add-int/2addr v0, v1

    .line 467
    return v0

    .line 462
    :cond_38
    iget-object v0, p0, Lo/I;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_15

    .line 463
    :cond_3f
    iget-object v0, p0, Lo/I;->f:Lo/aj;

    invoke-virtual {v0}, Lo/aj;->hashCode()I

    move-result v0

    goto :goto_1d

    .line 464
    :cond_46
    iget-object v0, p0, Lo/I;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_25

    .line 466
    :cond_4d
    iget-object v1, p0, Lo/I;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_36
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 431
    iget-object v0, p0, Lo/I;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()Lo/aj;
    .registers 2

    .prologue
    .line 435
    iget-object v0, p0, Lo/I;->f:Lo/aj;

    return-object v0
.end method

.method public k()F
    .registers 2

    .prologue
    .line 452
    iget v0, p0, Lo/I;->i:F

    return v0
.end method

.method public l()I
    .registers 3

    .prologue
    .line 520
    iget-object v0, p0, Lo/I;->b:Ljava/lang/String;

    invoke-static {v0}, Lo/O;->a(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x30

    iget-object v1, p0, Lo/I;->e:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/I;->h:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/I;->f:Lo/aj;

    invoke-static {v1}, Lo/O;->a(Lo/aj;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
