.class public Lo/K;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/X;

.field private final c:[Lo/H;

.field private final d:Ljava/lang/String;

.field private final e:Lo/aj;

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:F

.field private final j:I

.field private final k:Z

.field private final l:[I


# direct methods
.method public constructor <init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[I)V
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lo/K;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[IZ)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[IZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lo/K;->a:Lo/o;

    .line 91
    iput-object p2, p0, Lo/K;->b:Lo/X;

    .line 92
    iput-object p3, p0, Lo/K;->c:[Lo/H;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lo/K;->d:Ljava/lang/String;

    .line 94
    iput-object p4, p0, Lo/K;->e:Lo/aj;

    .line 95
    iput p5, p0, Lo/K;->f:I

    .line 96
    iput-object p6, p0, Lo/K;->g:Ljava/lang/String;

    .line 97
    iput p7, p0, Lo/K;->h:I

    .line 98
    iput p8, p0, Lo/K;->i:F

    .line 99
    iput p9, p0, Lo/K;->j:I

    .line 100
    iput-object p10, p0, Lo/K;->l:[I

    .line 101
    iput-boolean p11, p0, Lo/K;->k:Z

    .line 102
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/K;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lo/K;->a(Ljava/io/DataInput;Lo/as;Z)Lo/K;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/io/DataInput;Lo/as;Z)Lo/K;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 133
    .line 136
    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v1

    invoke-static {p0, v1}, Lo/X;->a(Ljava/io/DataInput;Lo/aq;)Lo/X;

    move-result-object v2

    .line 139
    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v6

    .line 142
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 143
    new-array v3, v4, [Lo/H;

    move v1, v0

    .line 144
    :goto_14
    if-ge v1, v4, :cond_1f

    .line 145
    invoke-static {p0, p1, v6}, Lo/H;->a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;

    move-result-object v5

    aput-object v5, v3, v1

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 149
    :cond_1f
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v7

    .line 152
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    invoke-static {v1}, Lo/O;->c(I)F

    move-result v8

    .line 156
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v9

    .line 159
    const/4 v1, 0x0

    .line 160
    const/4 v4, 0x1

    invoke-static {v4, v9}, Lo/O;->a(II)Z

    move-result v4

    if-eqz v4, :cond_4c

    .line 161
    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    .line 167
    :cond_3b
    :goto_3b
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 168
    new-array v10, v4, [I

    .line 169
    :goto_41
    if-ge v0, v4, :cond_58

    .line 170
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v10, v0

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_41

    .line 162
    :cond_4c
    const/4 v4, 0x2

    invoke-static {v4, v9}, Lo/O;->a(II)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 163
    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_3b

    .line 173
    :cond_58
    if-eqz p2, :cond_6c

    .line 174
    new-instance v0, Lo/L;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v10}, Lo/L;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[I)V

    .line 186
    :goto_6b
    return-object v0

    :cond_6c
    new-instance v0, Lo/K;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v10}, Lo/K;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;IFI[I)V

    goto :goto_6b
.end method


# virtual methods
.method public a(I)Lo/H;
    .registers 3
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lo/K;->c:[Lo/H;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a()Lo/o;
    .registers 2

    .prologue
    .line 202
    iget-object v0, p0, Lo/K;->a:Lo/o;

    return-object v0
.end method

.method public b()Lo/X;
    .registers 2

    .prologue
    .line 206
    iget-object v0, p0, Lo/K;->b:Lo/X;

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 214
    iget-object v0, p0, Lo/K;->c:[Lo/H;

    array-length v0, v0

    return v0
.end method

.method public d()F
    .registers 2

    .prologue
    .line 257
    iget v0, p0, Lo/K;->i:F

    return v0
.end method

.method public e()Lo/aj;
    .registers 2

    .prologue
    .line 228
    iget-object v0, p0, Lo/K;->e:Lo/aj;

    return-object v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 264
    iget-boolean v0, p0, Lo/K;->k:Z

    return v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 243
    const/16 v0, 0x8

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 248
    iget v0, p0, Lo/K;->h:I

    return v0
.end method

.method public l()[I
    .registers 2

    .prologue
    .line 282
    iget-object v0, p0, Lo/K;->l:[I

    return-object v0
.end method

.method public m()I
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 287
    iget-object v1, p0, Lo/K;->b:Lo/X;

    invoke-virtual {v1}, Lo/X;->h()I

    move-result v3

    .line 289
    iget-object v1, p0, Lo/K;->c:[Lo/H;

    if-eqz v1, :cond_1d

    .line 290
    iget-object v4, p0, Lo/K;->c:[Lo/H;

    array-length v5, v4

    move v1, v0

    :goto_f
    if-ge v1, v5, :cond_1d

    aget-object v2, v4, v1

    .line 291
    invoke-virtual {v2}, Lo/H;->d()I

    move-result v2

    add-int/2addr v2, v0

    .line 290
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_f

    .line 294
    :cond_1d
    iget-object v1, p0, Lo/K;->a:Lo/o;

    invoke-static {v1}, Lo/O;->a(Lo/o;)I

    move-result v1

    add-int/lit8 v1, v1, 0x38

    iget-object v2, p0, Lo/K;->g:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/K;->e:Lo/aj;

    invoke-static {v2}, Lo/O;->a(Lo/aj;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    .line 299
    return v0
.end method
