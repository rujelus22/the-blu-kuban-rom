.class public Lo/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static g:Lo/ao;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:F

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 40
    new-instance v0, Lo/ao;

    const/16 v3, 0xc

    move v2, v1

    move v5, v4

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lo/ao;-><init>(IIIFFI)V

    sput-object v0, Lo/ao;->g:Lo/ao;

    return-void
.end method

.method public constructor <init>(IIIFFI)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lo/ao;->a:I

    .line 55
    iput p2, p0, Lo/ao;->b:I

    .line 56
    iput p3, p0, Lo/ao;->c:I

    .line 57
    iput p4, p0, Lo/ao;->d:F

    .line 58
    iput p5, p0, Lo/ao;->e:F

    .line 59
    iput p6, p0, Lo/ao;->f:I

    .line 60
    return-void
.end method

.method public static a()Lo/ao;
    .registers 1

    .prologue
    .line 85
    sget-object v0, Lo/ao;->g:Lo/ao;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;I)Lo/ao;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 67
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 70
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    .line 73
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v0

    invoke-static {v0}, Lo/O;->b(I)F

    move-result v4

    .line 76
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v0

    invoke-static {v0}, Lo/O;->b(I)F

    move-result v5

    .line 79
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v6

    .line 81
    new-instance v0, Lo/ao;

    invoke-direct/range {v0 .. v6}, Lo/ao;-><init>(IIIFFI)V

    return-object v0
.end method


# virtual methods
.method public b()Z
    .registers 3

    .prologue
    .line 89
    const/4 v0, 0x1

    iget v1, p0, Lo/ao;->f:I

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 93
    const/4 v0, 0x2

    iget v1, p0, Lo/ao;->f:I

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 101
    iget v0, p0, Lo/ao;->a:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 105
    iget v0, p0, Lo/ao;->b:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 140
    if-ne p0, p1, :cond_6

    move v1, v0

    .line 165
    :cond_5
    :goto_5
    return v1

    .line 143
    :cond_6
    if-eqz p1, :cond_5

    .line 146
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_5

    .line 149
    check-cast p1, Lo/ao;

    .line 150
    iget v2, p0, Lo/ao;->f:I

    iget v3, p1, Lo/ao;->f:I

    if-ne v2, v3, :cond_5

    .line 153
    iget v2, p0, Lo/ao;->a:I

    iget v3, p1, Lo/ao;->a:I

    if-ne v2, v3, :cond_5

    .line 156
    iget v2, p0, Lo/ao;->d:F

    iget v3, p1, Lo/ao;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_5

    .line 159
    iget v2, p0, Lo/ao;->b:I

    iget v3, p1, Lo/ao;->b:I

    if-ne v2, v3, :cond_5

    .line 162
    iget v2, p0, Lo/ao;->c:I

    iget v3, p1, Lo/ao;->c:I

    if-ne v2, v3, :cond_5

    .line 165
    iget v2, p0, Lo/ao;->e:F

    iget v3, p1, Lo/ao;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3e

    :goto_3c
    move v1, v0

    goto :goto_5

    :cond_3e
    move v0, v1

    goto :goto_3c
.end method

.method public f()I
    .registers 2

    .prologue
    .line 109
    iget v0, p0, Lo/ao;->c:I

    return v0
.end method

.method public g()F
    .registers 2

    .prologue
    .line 113
    iget v0, p0, Lo/ao;->d:F

    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 127
    .line 129
    iget v0, p0, Lo/ao;->f:I

    add-int/lit8 v0, v0, 0x1f

    .line 130
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ao;->a:I

    add-int/2addr v0, v1

    .line 131
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ao;->d:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ao;->b:I

    add-int/2addr v0, v1

    .line 133
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ao;->c:I

    add-int/2addr v0, v1

    .line 134
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lo/ao;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    const-string v1, "TextStyle{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "color="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ao;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", outlineColor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ao;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ao;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", leadingRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ao;->d:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", trackingRatio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ao;->e:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attributes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lo/ao;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
