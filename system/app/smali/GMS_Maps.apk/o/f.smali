.class public Lo/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/aF;

.field private final c:[B

.field private final d:Lo/aj;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:[I


# direct methods
.method public constructor <init>(Lo/o;Lo/aF;[BLo/aj;ILjava/lang/String;II[I)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lo/f;->a:Lo/o;

    .line 65
    iput-object p2, p0, Lo/f;->b:Lo/aF;

    .line 66
    iput-object p3, p0, Lo/f;->c:[B

    .line 67
    iput-object p4, p0, Lo/f;->d:Lo/aj;

    .line 68
    iput p5, p0, Lo/f;->e:I

    .line 69
    iput-object p6, p0, Lo/f;->f:Ljava/lang/String;

    .line 70
    iput p7, p0, Lo/f;->g:I

    .line 71
    iput p8, p0, Lo/f;->h:I

    .line 72
    iput-object p9, p0, Lo/f;->i:[I

    .line 73
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/f;
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v0

    invoke-static {p0, v0}, Lo/aF;->a(Ljava/io/DataInput;Lo/aq;)Lo/aF;

    move-result-object v2

    .line 93
    invoke-virtual {v2}, Lo/aF;->a()I

    move-result v0

    new-array v3, v0, [B

    .line 94
    invoke-interface {p0, v3}, Ljava/io/DataInput;->readFully([B)V

    .line 101
    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v6

    .line 104
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v7

    .line 106
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v8

    .line 109
    const/4 v1, 0x0

    .line 110
    const/4 v0, 0x1

    invoke-static {v0, v8}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 111
    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    .line 117
    :cond_29
    :goto_29
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 118
    new-array v9, v4, [I

    .line 119
    const/4 v0, 0x0

    :goto_30
    if-ge v0, v4, :cond_47

    .line 120
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v9, v0

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_30

    .line 112
    :cond_3b
    const/4 v0, 0x2

    invoke-static {v0, v8}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 113
    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_29

    .line 123
    :cond_47
    new-instance v0, Lo/f;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v9}, Lo/f;-><init>(Lo/o;Lo/aF;[BLo/aj;ILjava/lang/String;II[I)V

    return-object v0
.end method


# virtual methods
.method public a()Lo/o;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lo/f;->a:Lo/o;

    return-object v0
.end method

.method public b()Lo/aF;
    .registers 2

    .prologue
    .line 140
    iget-object v0, p0, Lo/f;->b:Lo/aF;

    return-object v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 144
    iget-object v0, p0, Lo/f;->c:[B

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public d()[B
    .registers 2

    .prologue
    .line 148
    iget-object v0, p0, Lo/f;->c:[B

    return-object v0
.end method

.method public e()Lo/aj;
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lo/f;->d:Lo/aj;

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 161
    iget v0, p0, Lo/f;->e:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lo/f;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 171
    const/4 v0, 0x3

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 176
    iget v0, p0, Lo/f;->g:I

    return v0
.end method

.method public j()Z
    .registers 3

    .prologue
    .line 184
    iget v0, p0, Lo/f;->h:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .registers 3

    .prologue
    .line 191
    iget v0, p0, Lo/f;->h:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public l()[I
    .registers 2

    .prologue
    .line 202
    iget-object v0, p0, Lo/f;->i:[I

    return-object v0
.end method

.method public m()I
    .registers 4

    .prologue
    .line 208
    iget-object v0, p0, Lo/f;->b:Lo/aF;

    invoke-virtual {v0}, Lo/aF;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x38

    iget-object v1, p0, Lo/f;->c:[B

    array-length v1, v1

    add-int/2addr v0, v1

    .line 209
    iget-object v1, p0, Lo/f;->a:Lo/o;

    invoke-static {v1}, Lo/O;->a(Lo/o;)I

    move-result v1

    iget-object v2, p0, Lo/f;->f:Ljava/lang/String;

    invoke-static {v2}, Lo/O;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lo/f;->d:Lo/aj;

    invoke-static {v2}, Lo/O;->a(Lo/aj;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 212
    return v0
.end method
