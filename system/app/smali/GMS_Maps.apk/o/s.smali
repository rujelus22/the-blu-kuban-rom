.class public Lo/s;
.super Lo/o;
.source "SourceFile"


# instance fields
.field protected final b:Lo/u;

.field protected final c:I


# direct methods
.method constructor <init>(Lo/u;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 397
    invoke-direct {p0}, Lo/o;-><init>()V

    .line 398
    iput-object p1, p0, Lo/s;->b:Lo/u;

    .line 399
    iput p2, p0, Lo/s;->c:I

    .line 400
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 408
    invoke-virtual {p0}, Lo/s;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 446
    invoke-virtual {p0, p1}, Lo/s;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lo/s;->b:Lo/u;

    invoke-virtual {v1}, Lo/u;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lo/s;->b:Lo/u;

    invoke-virtual {v1}, Lo/u;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lo/s;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 434
    instance-of v1, p1, Lo/s;

    if-eqz v1, :cond_18

    .line 435
    check-cast p1, Lo/s;

    .line 436
    iget-object v1, p1, Lo/s;->b:Lo/u;

    iget-object v2, p0, Lo/s;->b:Lo/u;

    invoke-virtual {v1, v2}, Lo/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    iget v1, p1, Lo/s;->c:I

    iget v2, p0, Lo/s;->c:I

    if-ne v1, v2, :cond_18

    const/4 v0, 0x1

    .line 438
    :cond_18
    return v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 425
    invoke-virtual {p0}, Lo/s;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
