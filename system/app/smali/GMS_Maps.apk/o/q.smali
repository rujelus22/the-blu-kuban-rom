.class public Lo/q;
.super Lo/o;
.source "SourceFile"


# instance fields
.field protected final b:J


# direct methods
.method constructor <init>(J)V
    .registers 3
    .parameter

    .prologue
    .line 148
    invoke-direct {p0}, Lo/o;-><init>()V

    .line 149
    iput-wide p1, p0, Lo/q;->b:J

    .line 150
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 164
    const-string v0, ""

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 203
    instance-of v1, p1, Lo/q;

    if-eqz v1, :cond_10

    .line 204
    check-cast p1, Lo/q;

    .line 205
    iget-wide v1, p1, Lo/q;->b:J

    iget-wide v3, p0, Lo/q;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_10

    const/4 v0, 0x1

    .line 207
    :cond_10
    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 172
    const-string v0, ""

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    .line 194
    instance-of v0, p1, Lo/p;

    if-eqz v0, :cond_12

    .line 195
    check-cast p1, Lo/p;

    .line 196
    iget-wide v0, p1, Lo/p;->b:J

    iget-wide v2, p0, Lo/q;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_10

    const/4 v0, 0x1

    .line 198
    :goto_f
    return v0

    .line 196
    :cond_10
    const/4 v0, 0x0

    goto :goto_f

    .line 198
    :cond_12
    invoke-virtual {p0, p1}, Lo/q;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_f
.end method

.method public hashCode()I
    .registers 6

    .prologue
    .line 185
    iget-wide v0, p0, Lo/q;->b:J

    iget-wide v2, p0, Lo/q;->b:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[hash:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lo/q;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
