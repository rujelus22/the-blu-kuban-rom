.class public Lo/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/n;


# instance fields
.field private final a:Lo/o;

.field private final b:Lo/X;

.field private final c:[Lo/H;

.field private final d:Lo/aj;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:[I


# direct methods
.method public constructor <init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lo/af;->a:Lo/o;

    .line 133
    iput-object p2, p0, Lo/af;->b:Lo/X;

    .line 134
    if-nez p3, :cond_c

    const/4 v0, 0x0

    new-array p3, v0, [Lo/H;

    :cond_c
    iput-object p3, p0, Lo/af;->c:[Lo/H;

    .line 135
    iput-object p4, p0, Lo/af;->d:Lo/aj;

    .line 136
    iput p5, p0, Lo/af;->e:I

    .line 137
    iput-object p6, p0, Lo/af;->f:Ljava/lang/String;

    .line 138
    iput p7, p0, Lo/af;->g:I

    .line 139
    iput p8, p0, Lo/af;->h:I

    .line 140
    iput p9, p0, Lo/af;->i:I

    .line 141
    iput-object p10, p0, Lo/af;->j:[I

    .line 142
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lo/as;)Lo/af;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 159
    invoke-virtual {p1}, Lo/as;->b()Lo/aq;

    move-result-object v1

    invoke-static {p0, v1}, Lo/X;->a(Ljava/io/DataInput;Lo/aq;)Lo/X;

    move-result-object v2

    .line 162
    invoke-static {p0, p1}, Lo/ak;->a(Ljava/io/DataInput;Lo/as;)Lo/ak;

    move-result-object v6

    .line 165
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 166
    new-array v3, v4, [Lo/H;

    move v1, v0

    .line 167
    :goto_14
    if-ge v1, v4, :cond_1f

    .line 168
    invoke-static {p0, p1, v6}, Lo/H;->a(Ljava/io/DataInput;Lo/as;Lo/ak;)Lo/H;

    move-result-object v5

    aput-object v5, v3, v1

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 172
    :cond_1f
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v7

    .line 175
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v8

    .line 178
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    .line 181
    const/4 v1, 0x0

    .line 182
    const/4 v5, 0x1

    invoke-static {v5, v4}, Lo/O;->a(II)Z

    move-result v5

    if-eqz v5, :cond_4a

    .line 183
    invoke-static {p0}, Lo/o;->a(Ljava/io/DataInput;)Lo/p;

    move-result-object v1

    .line 189
    :cond_37
    :goto_37
    ushr-int/lit8 v9, v4, 0x2

    .line 192
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 193
    new-array v10, v4, [I

    .line 194
    :goto_3f
    if-ge v0, v4, :cond_56

    .line 195
    invoke-static {p0}, Lo/aG;->a(Ljava/io/DataInput;)I

    move-result v5

    aput v5, v10, v0

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_3f

    .line 184
    :cond_4a
    const/4 v5, 0x2

    invoke-static {v5, v4}, Lo/O;->a(II)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 185
    invoke-static {p0}, Lo/o;->b(Ljava/io/DataInput;)Lo/q;

    move-result-object v1

    goto :goto_37

    .line 198
    :cond_56
    new-instance v0, Lo/af;

    invoke-virtual {v6}, Lo/ak;->a()Lo/aj;

    move-result-object v4

    invoke-virtual {v6}, Lo/ak;->c()I

    move-result v5

    invoke-virtual {v6}, Lo/ak;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v10}, Lo/af;-><init>(Lo/o;Lo/X;[Lo/H;Lo/aj;ILjava/lang/String;III[I)V

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 226
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lo/H;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Lo/o;
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lo/af;->a:Lo/o;

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lo/H;->b()I

    move-result v0

    if-lez v0, :cond_18

    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v0

    invoke-virtual {v0}, Lo/I;->g()Ljava/lang/String;

    move-result-object v0

    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public b()Lo/X;
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, Lo/af;->b:Lo/X;

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    array-length v0, v0

    goto :goto_5
.end method

.method public c(I)Lo/H;
    .registers 3
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 240
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lo/af;->c:[Lo/H;

    array-length v0, v0

    goto :goto_5
.end method

.method public d(I)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 250
    iget-object v1, p0, Lo/af;->c:[Lo/H;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lo/H;->b()I

    move-result v1

    if-lez v1, :cond_1a

    iget-object v1, p0, Lo/af;->c:[Lo/H;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Lo/H;->a(I)Lo/I;

    move-result-object v1

    invoke-virtual {v1}, Lo/I;->b()Z

    move-result v1

    if-eqz v1, :cond_1a

    const/4 v0, 0x1

    :cond_1a
    return v0
.end method

.method public e()Lo/aj;
    .registers 2

    .prologue
    .line 256
    iget-object v0, p0, Lo/af;->d:Lo/aj;

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 270
    iget v0, p0, Lo/af;->g:I

    return v0
.end method

.method public g()Z
    .registers 3

    .prologue
    .line 287
    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 280
    const/4 v0, 0x2

    return v0
.end method

.method public i()I
    .registers 2

    .prologue
    .line 275
    iget v0, p0, Lo/af;->h:I

    return v0
.end method

.method public j()Z
    .registers 3

    .prologue
    .line 294
    iget v0, p0, Lo/af;->i:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public k()Z
    .registers 3

    .prologue
    .line 302
    iget v0, p0, Lo/af;->i:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public l()[I
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, Lo/af;->j:[I

    return-object v0
.end method

.method public m()I
    .registers 7

    .prologue
    const/4 v0, 0x0

    .line 364
    iget-object v1, p0, Lo/af;->b:Lo/X;

    invoke-virtual {v1}, Lo/X;->h()I

    move-result v1

    add-int/lit8 v3, v1, 0x38

    .line 366
    iget-object v1, p0, Lo/af;->c:[Lo/H;

    if-eqz v1, :cond_1f

    .line 367
    iget-object v4, p0, Lo/af;->c:[Lo/H;

    array-length v5, v4

    move v1, v0

    :goto_11
    if-ge v1, v5, :cond_1f

    aget-object v2, v4, v1

    .line 368
    invoke-virtual {v2}, Lo/H;->d()I

    move-result v2

    add-int/2addr v2, v0

    .line 367
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_11

    .line 371
    :cond_1f
    iget-object v1, p0, Lo/af;->a:Lo/o;

    invoke-static {v1}, Lo/O;->a(Lo/o;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/af;->d:Lo/aj;

    invoke-static {v1}, Lo/O;->a(Lo/aj;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lo/af;->f:Ljava/lang/String;

    invoke-static {v1}, Lo/O;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, v3

    .line 376
    return v0
.end method

.method public n()Z
    .registers 3

    .prologue
    .line 310
    iget v0, p0, Lo/af;->i:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public o()Z
    .registers 3

    .prologue
    .line 320
    invoke-virtual {p0}, Lo/af;->e()Lo/aj;

    move-result-object v0

    invoke-virtual {v0}, Lo/aj;->g()Z

    move-result v0

    .line 321
    if-nez v0, :cond_10

    invoke-virtual {p0}, Lo/af;->p()Z

    move-result v0

    if-eqz v0, :cond_2e

    :cond_10
    invoke-virtual {p0}, Lo/af;->k()Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-virtual {p0}, Lo/af;->n()Z

    move-result v0

    if-eqz v0, :cond_2e

    :cond_1c
    iget v0, p0, Lo/af;->g:I

    const/16 v1, 0x80

    if-ge v0, v1, :cond_2e

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    if-nez v0, :cond_2e

    const/4 v0, 0x1

    :goto_2d
    return v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method

.method public p()Z
    .registers 3

    .prologue
    .line 332
    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x20

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    if-eqz v0, :cond_16

    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public q()Z
    .registers 3

    .prologue
    .line 341
    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x40

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method

.method public r()Z
    .registers 3

    .prologue
    .line 348
    iget v0, p0, Lo/af;->i:I

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lo/O;->a(II)Z

    move-result v0

    return v0
.end method
