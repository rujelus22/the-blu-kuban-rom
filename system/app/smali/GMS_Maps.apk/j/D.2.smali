.class public Lj/D;
.super Lj/E;
.source "SourceFile"


# static fields
.field private static a:Lj/D;


# instance fields
.field private b:Lj/aa;


# direct methods
.method constructor <init>(Lad/p;Landroid/content/Context;Lj/aa;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lj/E;-><init>(Lad/p;Landroid/content/Context;)V

    .line 55
    iput-object p3, p0, Lj/D;->b:Lj/aa;

    .line 56
    return-void
.end method

.method public static a(Lad/p;Landroid/content/Context;Lj/aa;)Lj/D;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    sget-object v0, Lj/D;->a:Lj/D;

    if-nez v0, :cond_b

    .line 68
    new-instance v0, Lj/D;

    invoke-direct {v0, p0, p1, p2}, Lj/D;-><init>(Lad/p;Landroid/content/Context;Lj/aa;)V

    sput-object v0, Lj/D;->a:Lj/D;

    .line 70
    :cond_b
    sget-object v0, Lj/D;->a:Lj/D;

    return-object v0
.end method


# virtual methods
.method protected a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 48
    const-string v0, "/cannedtts/"

    return-object v0
.end method

.method protected a(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 103
    packed-switch p1, :pswitch_data_c

    .line 110
    const-string v0, "voice_instructions.xml"

    :goto_5
    return-object v0

    .line 105
    :pswitch_6
    const-string v0, "voice_instructions_imperial.xml"

    goto :goto_5

    .line 107
    :pswitch_9
    const-string v0, "voice_instructions_yards.xml"

    goto :goto_5

    .line 103
    :pswitch_data_c
    .packed-switch 0x2
        :pswitch_6
        :pswitch_9
    .end packed-switch
.end method

.method a(Lj/v;)V
    .registers 8
    .parameter

    .prologue
    .line 91
    invoke-virtual {p1}, Lj/v;->a()Ljava/util/List;

    move-result-object v0

    .line 92
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj/A;

    .line 93
    invoke-virtual {v0}, Lj/A;->a()Ljava/lang/String;

    move-result-object v2

    .line 94
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_8

    .line 95
    const-string v3, "g"

    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/dh;->a(Ljava/lang/String;)V

    .line 96
    iget-object v3, p0, Lj/D;->b:Lj/aa;

    invoke-virtual {v0}, Lj/A;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-interface {v3, v0, v4, v2, v5}, Lj/aa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    goto :goto_8

    .line 99
    :cond_34
    return-void
.end method

.method protected b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 81
    const-string v0, "TtsVoiceBundles"

    return-object v0
.end method
