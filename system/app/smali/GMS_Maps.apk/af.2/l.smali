.class public final Laf/l;
.super LbH/k;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 175
    invoke-direct {p0}, LbH/k;-><init>()V

    .line 176
    invoke-direct {p0}, Laf/l;->h()V

    .line 177
    return-void
.end method

.method static synthetic g()Laf/l;
    .registers 1

    .prologue
    .line 170
    invoke-static {}, Laf/l;->i()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .registers 1

    .prologue
    .line 180
    return-void
.end method

.method private static i()Laf/l;
    .registers 1

    .prologue
    .line 182
    new-instance v0, Laf/l;

    invoke-direct {v0}, Laf/l;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Laf/l;
    .registers 3

    .prologue
    .line 191
    invoke-static {}, Laf/l;->i()Laf/l;

    move-result-object v0

    invoke-virtual {p0}, Laf/l;->c()Laf/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/l;->a(Laf/j;)Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public a(Laf/j;)Laf/l;
    .registers 3
    .parameter

    .prologue
    .line 212
    invoke-static {}, Laf/j;->a()Laf/j;

    move-result-object v0

    if-ne p1, v0, :cond_6

    .line 213
    :cond_6
    return-object p0
.end method

.method public a(LbH/f;LbH/i;)Laf/l;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 224
    const/4 v2, 0x0

    .line 226
    :try_start_1
    sget-object v0, Laf/j;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/j;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch LbH/l; {:try_start_1 .. :try_end_9} :catch_f

    .line 231
    if-eqz v0, :cond_e

    .line 232
    invoke-virtual {p0, v0}, Laf/l;->a(Laf/j;)Laf/l;

    .line 235
    :cond_e
    return-object p0

    .line 227
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 228
    :try_start_11
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/j;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 229
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 231
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 232
    invoke-virtual {p0, v1}, Laf/l;->a(Laf/j;)Laf/l;

    :cond_21
    throw v0

    .line 231
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method public b()Laf/j;
    .registers 3

    .prologue
    .line 199
    invoke-virtual {p0}, Laf/l;->c()Laf/j;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Laf/j;->b()Z

    move-result v1

    if-nez v1, :cond_f

    .line 201
    invoke-static {v0}, Laf/l;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    .line 203
    :cond_f
    return-object v0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 170
    invoke-virtual {p0, p1, p2}, Laf/l;->a(LbH/f;LbH/i;)Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/j;
    .registers 3

    .prologue
    .line 207
    new-instance v0, Laf/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laf/j;-><init>(LbH/k;Laf/k;)V

    .line 208
    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 170
    invoke-virtual {p0}, Laf/l;->a()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .registers 2

    .prologue
    .line 170
    invoke-virtual {p0}, Laf/l;->a()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .registers 2

    .prologue
    .line 170
    invoke-virtual {p0}, Laf/l;->a()Laf/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .registers 2

    .prologue
    .line 170
    invoke-virtual {p0}, Laf/l;->b()Laf/j;

    move-result-object v0

    return-object v0
.end method
