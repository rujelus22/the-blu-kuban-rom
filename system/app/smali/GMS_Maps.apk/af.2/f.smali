.class public final Laf/f;
.super LbH/k;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/util/List;

.field private c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 288
    invoke-direct {p0}, LbH/k;-><init>()V

    .line 384
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laf/f;->b:Ljava/util/List;

    .line 509
    const-string v0, ""

    iput-object v0, p0, Laf/f;->c:Ljava/lang/Object;

    .line 289
    invoke-direct {p0}, Laf/f;->h()V

    .line 290
    return-void
.end method

.method static synthetic g()Laf/f;
    .registers 1

    .prologue
    .line 283
    invoke-static {}, Laf/f;->i()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .registers 1

    .prologue
    .line 293
    return-void
.end method

.method private static i()Laf/f;
    .registers 1

    .prologue
    .line 295
    new-instance v0, Laf/f;

    invoke-direct {v0}, Laf/f;-><init>()V

    return-object v0
.end method

.method private j()V
    .registers 3

    .prologue
    .line 387
    iget v0, p0, Laf/f;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_16

    .line 388
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Laf/f;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Laf/f;->b:Ljava/util/List;

    .line 389
    iget v0, p0, Laf/f;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laf/f;->a:I

    .line 391
    :cond_16
    return-void
.end method


# virtual methods
.method public a()Laf/f;
    .registers 3

    .prologue
    .line 308
    invoke-static {}, Laf/f;->i()Laf/f;

    move-result-object v0

    invoke-virtual {p0}, Laf/f;->c()Laf/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Laf/f;->a(Laf/d;)Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public a(Laf/c;)Laf/f;
    .registers 4
    .parameter

    .prologue
    .line 464
    invoke-direct {p0}, Laf/f;->j()V

    .line 465
    iget-object v0, p0, Laf/f;->b:Ljava/util/List;

    invoke-virtual {p1}, Laf/c;->b()Laf/a;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    return-object p0
.end method

.method public a(Laf/d;)Laf/f;
    .registers 4
    .parameter

    .prologue
    .line 341
    invoke-static {}, Laf/d;->a()Laf/d;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 357
    :cond_6
    :goto_6
    return-object p0

    .line 342
    :cond_7
    invoke-static {p1}, Laf/d;->b(Laf/d;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_25

    .line 343
    iget-object v0, p0, Laf/f;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 344
    invoke-static {p1}, Laf/d;->b(Laf/d;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Laf/f;->b:Ljava/util/List;

    .line 345
    iget v0, p0, Laf/f;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Laf/f;->a:I

    .line 352
    :cond_25
    :goto_25
    invoke-virtual {p1}, Laf/d;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 353
    iget v0, p0, Laf/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/f;->a:I

    .line 354
    invoke-static {p1}, Laf/d;->c(Laf/d;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Laf/f;->c:Ljava/lang/Object;

    goto :goto_6

    .line 347
    :cond_38
    invoke-direct {p0}, Laf/f;->j()V

    .line 348
    iget-object v0, p0, Laf/f;->b:Ljava/util/List;

    invoke-static {p1}, Laf/d;->b(Laf/d;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_25
.end method

.method public a(LbH/f;LbH/i;)Laf/f;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 368
    const/4 v2, 0x0

    .line 370
    :try_start_1
    sget-object v0, Laf/d;->a:LbH/r;

    invoke-interface {v0, p1, p2}, LbH/r;->b(LbH/f;LbH/i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laf/d;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_22
    .catch LbH/l; {:try_start_1 .. :try_end_9} :catch_f

    .line 375
    if-eqz v0, :cond_e

    .line 376
    invoke-virtual {p0, v0}, Laf/f;->a(Laf/d;)Laf/f;

    .line 379
    :cond_e
    return-object p0

    .line 371
    :catch_f
    move-exception v0

    move-object v1, v0

    .line 372
    :try_start_11
    invoke-virtual {v1}, LbH/l;->a()LbH/p;

    move-result-object v0

    check-cast v0, Laf/d;
    :try_end_17
    .catchall {:try_start_11 .. :try_end_17} :catchall_22

    .line 373
    :try_start_17
    throw v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_18

    .line 375
    :catchall_18
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1c
    if-eqz v1, :cond_21

    .line 376
    invoke-virtual {p0, v1}, Laf/f;->a(Laf/d;)Laf/f;

    :cond_21
    throw v0

    .line 375
    :catchall_22
    move-exception v0

    move-object v1, v2

    goto :goto_1c
.end method

.method public a(Ljava/lang/String;)Laf/f;
    .registers 3
    .parameter

    .prologue
    .line 551
    if-nez p1, :cond_8

    .line 552
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 554
    :cond_8
    iget v0, p0, Laf/f;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Laf/f;->a:I

    .line 555
    iput-object p1, p0, Laf/f;->c:Ljava/lang/Object;

    .line 557
    return-object p0
.end method

.method public b()Laf/d;
    .registers 3

    .prologue
    .line 316
    invoke-virtual {p0}, Laf/f;->c()Laf/d;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Laf/d;->d()Z

    move-result v1

    if-nez v1, :cond_f

    .line 318
    invoke-static {v0}, Laf/f;->a(LbH/p;)LbH/x;

    move-result-object v0

    throw v0

    .line 320
    :cond_f
    return-object v0
.end method

.method public synthetic b(LbH/f;LbH/i;)LbH/b;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 283
    invoke-virtual {p0, p1, p2}, Laf/f;->a(LbH/f;LbH/i;)Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public c()Laf/d;
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 324
    new-instance v2, Laf/d;

    const/4 v1, 0x0

    invoke-direct {v2, p0, v1}, Laf/d;-><init>(LbH/k;Laf/e;)V

    .line 325
    iget v3, p0, Laf/f;->a:I

    .line 326
    const/4 v1, 0x0

    .line 327
    iget v4, p0, Laf/f;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v0, :cond_1e

    .line 328
    iget-object v4, p0, Laf/f;->b:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Laf/f;->b:Ljava/util/List;

    .line 329
    iget v4, p0, Laf/f;->a:I

    and-int/lit8 v4, v4, -0x2

    iput v4, p0, Laf/f;->a:I

    .line 331
    :cond_1e
    iget-object v4, p0, Laf/f;->b:Ljava/util/List;

    invoke-static {v2, v4}, Laf/d;->a(Laf/d;Ljava/util/List;)Ljava/util/List;

    .line 332
    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_31

    .line 335
    :goto_28
    iget-object v1, p0, Laf/f;->c:Ljava/lang/Object;

    invoke-static {v2, v1}, Laf/d;->a(Laf/d;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    invoke-static {v2, v0}, Laf/d;->a(Laf/d;I)I

    .line 337
    return-object v2

    :cond_31
    move v0, v1

    goto :goto_28
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 283
    invoke-virtual {p0}, Laf/f;->a()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()LbH/k;
    .registers 2

    .prologue
    .line 283
    invoke-virtual {p0}, Laf/f;->a()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()LbH/b;
    .registers 2

    .prologue
    .line 283
    invoke-virtual {p0}, Laf/f;->a()Laf/f;

    move-result-object v0

    return-object v0
.end method

.method public synthetic f()LbH/p;
    .registers 2

    .prologue
    .line 283
    invoke-virtual {p0}, Laf/f;->b()Laf/d;

    move-result-object v0

    return-object v0
.end method
