.class Lv/f;
.super Law/a;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field b:Lv/a;

.field final synthetic c:Lv/d;


# direct methods
.method private constructor <init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 335
    iput-object p1, p0, Lv/f;->c:Lv/d;

    invoke-direct {p0}, Law/a;-><init>()V

    .line 336
    iput-object p2, p0, Lv/f;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 337
    iput-object p3, p0, Lv/f;->b:Lv/a;

    .line 338
    return-void
.end method

.method synthetic constructor <init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;Lv/e;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 328
    invoke-direct {p0, p1, p2, p3}, Lv/f;-><init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 341
    iget-object v0, p0, Lv/f;->b:Lv/a;

    invoke-virtual {v0}, Lv/a;->i()V

    .line 342
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 4
    .parameter

    .prologue
    .line 376
    iget-object v0, p0, Lv/f;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v0

    .line 377
    array-length v1, v0

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 378
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    .line 379
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 357
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/fj;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 360
    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-nez v3, :cond_f

    .line 371
    :goto_e
    return v0

    .line 366
    :cond_f
    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 367
    iget-object v2, p0, Lv/f;->b:Lv/a;

    invoke-virtual {v2, v0}, Lv/a;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    .line 368
    iget-object v3, p0, Lv/f;->c:Lv/d;

    invoke-static {v3}, Lv/d;->a(Lv/d;)Lt/e;

    move-result-object v3

    if-eqz v3, :cond_34

    if-eqz v2, :cond_34

    iget-object v2, p0, Lv/f;->b:Lv/a;

    invoke-virtual {v2}, Lv/a;->a()Z

    move-result v2

    if-eqz v2, :cond_34

    .line 369
    iget-object v2, p0, Lv/f;->c:Lv/d;

    invoke-static {v2}, Lv/d;->a(Lv/d;)Lt/e;

    move-result-object v2

    invoke-virtual {v2, v0}, Lt/e;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_34
    move v0, v1

    .line 371
    goto :goto_e
.end method

.method public a_()Z
    .registers 2

    .prologue
    .line 347
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 352
    const/16 v0, 0x27

    return v0
.end method
