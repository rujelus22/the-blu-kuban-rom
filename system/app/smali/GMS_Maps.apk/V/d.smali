.class public Lv/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# static fields
.field private static a:Lv/d;


# instance fields
.field private final b:Law/h;

.field private final c:Lcom/google/googlenav/common/a;

.field private final d:LR/h;

.field private final e:LR/h;

.field private volatile f:Lt/e;

.field private final g:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object v0, p0, Lv/d;->b:Law/h;

    .line 77
    iput-object v0, p0, Lv/d;->c:Lcom/google/googlenav/common/a;

    .line 78
    iput-object v0, p0, Lv/d;->d:LR/h;

    .line 79
    iput-object v0, p0, Lv/d;->e:LR/h;

    .line 80
    iput-object v0, p0, Lv/d;->f:Lt/e;

    .line 81
    iput-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    .line 82
    return-void
.end method

.method private constructor <init>(Law/h;)V
    .registers 4
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lv/d;->b:Law/h;

    .line 61
    iget-object v0, p0, Lv/d;->b:Law/h;

    if-eqz v0, :cond_e

    .line 62
    iget-object v0, p0, Lv/d;->b:Law/h;

    invoke-virtual {v0, p0}, Law/h;->a(Law/q;)V

    .line 64
    :cond_e
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lv/d;->c:Lcom/google/googlenav/common/a;

    .line 65
    new-instance v0, LR/h;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lv/d;->d:LR/h;

    .line 66
    new-instance v0, LR/h;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lv/d;->e:LR/h;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lv/d;->f:Lt/e;

    .line 68
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    .line 69
    return-void
.end method

.method static synthetic a(Lv/d;)Lt/e;
    .registers 2
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lv/d;->f:Lt/e;

    return-object v0
.end method

.method public static a(Law/h;Ljava/io/File;)Lv/d;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 247
    sget-object v0, Lv/d;->a:Lv/d;

    if-nez v0, :cond_b

    .line 248
    new-instance v0, Lv/d;

    invoke-direct {v0, p0}, Lv/d;-><init>(Law/h;)V

    sput-object v0, Lv/d;->a:Lv/d;

    .line 253
    :cond_b
    new-instance v0, Lv/e;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lv/e;-><init>(Las/c;Ljava/io/File;)V

    invoke-virtual {v0}, Lv/e;->g()V

    .line 260
    sget-object v0, Lv/d;->a:Lv/d;

    return-object v0
.end method

.method private a(Ljava/io/File;)V
    .registers 3
    .parameter

    .prologue
    .line 270
    invoke-static {p1}, Lt/e;->a(Ljava/io/File;)Lt/e;

    move-result-object v0

    iput-object v0, p0, Lv/d;->f:Lt/e;

    .line 271
    invoke-direct {p0}, Lv/d;->e()V

    .line 272
    return-void
.end method

.method private a(Ljava/lang/String;Lv/a;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 204
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/fj;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 205
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 206
    invoke-virtual {p2}, Lv/a;->b()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 207
    const/4 v1, 0x2

    invoke-virtual {p2}, Lv/a;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 209
    :cond_19
    new-instance v1, Lv/f;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, p2, v2}, Lv/f;-><init>(Lv/d;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lv/a;Lv/e;)V

    .line 210
    new-instance v0, Ll/g;

    const-string v2, "addRequest"

    invoke-direct {v0, v2, v1}, Ll/g;-><init>(Ljava/lang/String;Law/g;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    .line 212
    iget-object v0, p0, Lv/d;->b:Law/h;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 213
    return-void
.end method

.method static synthetic a(Lv/d;Ljava/io/File;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lv/d;->a(Ljava/io/File;)V

    return-void
.end method

.method public static c()Lv/d;
    .registers 1

    .prologue
    .line 298
    sget-object v0, Lv/d;->a:Lv/d;

    return-object v0
.end method

.method public static d()V
    .registers 1

    .prologue
    .line 303
    sget-object v0, Lv/d;->a:Lv/d;

    if-eqz v0, :cond_c

    .line 304
    sget-object v0, Lv/d;->a:Lv/d;

    invoke-virtual {v0}, Lv/d;->a()V

    .line 305
    const/4 v0, 0x0

    sput-object v0, Lv/d;->a:Lv/d;

    .line 307
    :cond_c
    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 279
    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 287
    :goto_0
    iget-object v0, p0, Lv/d;->f:Lt/e;

    if-nez v0, :cond_c

    .line 289
    :try_start_4
    iget-object v0, p0, Lv/d;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_9} :catch_a

    goto :goto_0

    .line 290
    :catch_a
    move-exception v0

    goto :goto_0

    .line 294
    :cond_c
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lv/c;)Lv/a;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lv/d;->a(Ljava/lang/String;Lv/c;Z)Lv/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lv/c;Z)Lv/a;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    const/4 v1, 0x0

    .line 127
    if-eqz p3, :cond_55

    .line 128
    iget-object v2, p0, Lv/d;->e:LR/h;

    monitor-enter v2

    .line 129
    :try_start_6
    iget-object v0, p0, Lv/d;->e:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 130
    if-eqz v0, :cond_84

    .line 131
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lv/a;

    .line 133
    :goto_16
    if-nez v0, :cond_2b

    .line 134
    new-instance v0, Lv/a;

    invoke-direct {v0}, Lv/a;-><init>()V

    .line 138
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lv/a;->a(Z)V

    .line 139
    iget-object v1, p0, Lv/d;->e:LR/h;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v3}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 141
    :cond_2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_6 .. :try_end_2c} :catchall_52

    .line 160
    :goto_2c
    monitor-enter v0

    .line 161
    :try_start_2d
    iget-object v1, p0, Lv/d;->c:Lcom/google/googlenav/common/a;

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    .line 162
    invoke-virtual {v0}, Lv/a;->h()J

    move-result-wide v3

    const-wide/32 v5, 0x5265c00

    add-long/2addr v3, v5

    cmp-long v3, v3, v1

    if-gez v3, :cond_45

    .line 163
    invoke-direct {p0, p1, v0}, Lv/d;->a(Ljava/lang/String;Lv/a;)V

    .line 164
    invoke-virtual {v0, v1, v2}, Lv/a;->a(J)V

    .line 166
    :cond_45
    monitor-exit v0
    :try_end_46
    .catchall {:try_start_2d .. :try_end_46} :catchall_81

    .line 168
    if-eqz p2, :cond_51

    invoke-virtual {v0}, Lv/a;->b()Z

    move-result v1

    if-nez v1, :cond_51

    .line 169
    invoke-virtual {v0, p2}, Lv/a;->a(Lv/c;)V

    .line 171
    :cond_51
    return-object v0

    .line 141
    :catchall_52
    move-exception v0

    :try_start_53
    monitor-exit v2
    :try_end_54
    .catchall {:try_start_53 .. :try_end_54} :catchall_52

    throw v0

    .line 143
    :cond_55
    iget-object v1, p0, Lv/d;->d:LR/h;

    monitor-enter v1

    .line 144
    :try_start_58
    iget-object v0, p0, Lv/d;->d:LR/h;

    invoke-virtual {v0, p1}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lv/a;

    .line 145
    if-nez v0, :cond_6c

    .line 147
    iget-object v2, p0, Lv/d;->f:Lt/e;

    if-eqz v2, :cond_6c

    .line 148
    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0, p1}, Lt/e;->a(Ljava/lang/String;)Lv/a;

    move-result-object v0

    .line 151
    :cond_6c
    if-nez v0, :cond_77

    .line 153
    new-instance v0, Lv/a;

    invoke-direct {v0}, Lv/a;-><init>()V

    .line 154
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lv/a;->a(Z)V

    .line 156
    :cond_77
    iget-object v2, p0, Lv/d;->d:LR/h;

    invoke-virtual {v2, p1, v0}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 157
    monitor-exit v1

    goto :goto_2c

    :catchall_7e
    move-exception v0

    monitor-exit v1
    :try_end_80
    .catchall {:try_start_58 .. :try_end_80} :catchall_7e

    throw v0

    .line 166
    :catchall_81
    move-exception v1

    :try_start_82
    monitor-exit v0
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_81

    throw v1

    :cond_84
    move-object v0, v1

    goto :goto_16
.end method

.method a()V
    .registers 2

    .prologue
    .line 85
    invoke-direct {p0}, Lv/d;->f()V

    .line 86
    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0}, Lt/e;->a()V

    .line 87
    return-void
.end method

.method public a(IZLjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 230
    return-void
.end method

.method public a(Law/g;)V
    .registers 3
    .parameter

    .prologue
    .line 221
    instance-of v0, p1, Lv/f;

    if-eqz v0, :cond_9

    .line 222
    check-cast p1, Lv/f;

    .line 223
    invoke-virtual {p1}, Lv/f;->a()V

    .line 225
    :cond_9
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 179
    iget-object v1, p0, Lv/d;->e:LR/h;

    monitor-enter v1

    .line 180
    :try_start_3
    iget-object v0, p0, Lv/d;->e:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 181
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_1d

    .line 182
    iget-object v1, p0, Lv/d;->d:LR/h;

    monitor-enter v1

    .line 183
    :try_start_c
    iget-object v0, p0, Lv/d;->d:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 184
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_20

    .line 185
    if-eqz p1, :cond_1c

    .line 186
    invoke-direct {p0}, Lv/d;->f()V

    .line 187
    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0}, Lt/e;->c()V

    .line 189
    :cond_1c
    return-void

    .line 181
    :catchall_1d
    move-exception v0

    :try_start_1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1d

    throw v0

    .line 184
    :catchall_20
    move-exception v0

    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    throw v0
.end method

.method public b()J
    .registers 3

    .prologue
    .line 196
    iget-object v0, p0, Lv/d;->f:Lt/e;

    if-eqz v0, :cond_b

    .line 197
    iget-object v0, p0, Lv/d;->f:Lt/e;

    invoke-virtual {v0}, Lt/e;->b()J

    move-result-wide v0

    .line 200
    :goto_a
    return-wide v0

    :cond_b
    const-wide/16 v0, 0x0

    goto :goto_a
.end method

.method public b(Law/g;)V
    .registers 2
    .parameter

    .prologue
    .line 238
    return-void
.end method

.method public k()V
    .registers 1

    .prologue
    .line 234
    return-void
.end method
