.class public Lbi/j;
.super Lbi/s;
.source "SourceFile"


# instance fields
.field private final e:Lax/t;


# direct methods
.method public constructor <init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 652
    sget-object v1, Lbi/q;->b:Lbi/q;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lbi/s;-><init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V

    .line 653
    iput-object p2, p0, Lbi/j;->e:Lax/t;

    .line 654
    invoke-virtual {p3}, Lax/t;->a()Lax/v;

    move-result-object v0

    iput-object v0, p0, Lbi/j;->d:Lax/v;

    .line 655
    invoke-virtual {p0}, Lbi/j;->C()V

    .line 656
    return-void
.end method


# virtual methods
.method protected C()V
    .registers 2

    .prologue
    .line 695
    invoke-virtual {p0}, Lbi/j;->s()V

    .line 696
    invoke-virtual {p0}, Lbi/j;->t()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 697
    invoke-virtual {p0}, Lbi/j;->y()F

    move-result v0

    invoke-virtual {p0, v0}, Lbi/j;->a(F)V

    .line 699
    :cond_10
    return-void
.end method

.method public D()Ljava/lang/String;
    .registers 2

    .prologue
    .line 703
    invoke-virtual {p0}, Lbi/j;->N()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/util/Date;
    .registers 2

    .prologue
    .line 680
    iget-object v0, p0, Lbi/j;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lbi/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/Date;
    .registers 3

    .prologue
    .line 685
    iget-object v0, p0, Lbi/j;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->Q()[Lax/u;

    move-result-object v0

    .line 686
    array-length v1, v0

    if-lez v1, :cond_11

    .line 687
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lax/u;->b()Ljava/util/Date;

    move-result-object v0

    .line 689
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Lbi/j;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->j()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lbi/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    goto :goto_10
.end method

.method public u()LaN/B;
    .registers 2

    .prologue
    .line 665
    iget-object v0, p0, Lbi/j;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->q()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public v()LaN/B;
    .registers 3

    .prologue
    .line 670
    iget-object v0, p0, Lbi/j;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->Q()[Lax/u;

    move-result-object v0

    .line 671
    array-length v1, v0

    if-lez v1, :cond_11

    .line 672
    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lax/u;->d()LaN/B;

    move-result-object v0

    .line 674
    :goto_10
    return-object v0

    :cond_11
    iget-object v0, p0, Lbi/j;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->k()LaN/B;

    move-result-object v0

    goto :goto_10
.end method
