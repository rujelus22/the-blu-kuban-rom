.class Lbf/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Z

.field final synthetic c:Ljava/io/DataInput;

.field final synthetic d:I

.field final synthetic e:Z

.field final synthetic f:Z

.field final synthetic g:Lbf/am;


# direct methods
.method constructor <init>(Lbf/am;IZLjava/io/DataInput;IZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3393
    iput-object p1, p0, Lbf/ar;->g:Lbf/am;

    iput p2, p0, Lbf/ar;->a:I

    iput-boolean p3, p0, Lbf/ar;->b:Z

    iput-object p4, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iput p5, p0, Lbf/ar;->d:I

    iput-boolean p6, p0, Lbf/ar;->e:Z

    iput-boolean p7, p0, Lbf/ar;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 3397
    :try_start_0
    iget v0, p0, Lbf/ar;->a:I

    packed-switch v0, :pswitch_data_94

    .line 3447
    :cond_5
    :goto_5
    :pswitch_5
    return-void

    .line 3399
    :pswitch_6
    iget-boolean v0, p0, Lbf/ar;->b:Z

    if-eqz v0, :cond_5

    .line 3400
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->a(Lbf/am;Ljava/io/DataInput;IZZ)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_17} :catch_18

    goto :goto_5

    .line 3435
    :catch_18
    move-exception v0

    .line 3442
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_MANAGER-LayersManager load ex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lbf/ar;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3444
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    invoke-static {v0}, Lbf/am;->a(Lbf/am;)V

    goto :goto_5

    .line 3404
    :pswitch_37
    :try_start_37
    iget-boolean v0, p0, Lbf/ar;->b:Z

    if-eqz v0, :cond_5

    .line 3405
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->b(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_5

    .line 3409
    :pswitch_49
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->c(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_5

    .line 3412
    :pswitch_57
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->d(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_5

    .line 3415
    :pswitch_65
    iget-boolean v0, p0, Lbf/ar;->b:Z

    if-eqz v0, :cond_5

    .line 3416
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->e(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_5

    .line 3420
    :pswitch_77
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->f(Lbf/am;Ljava/io/DataInput;IZZ)V

    goto :goto_5

    .line 3423
    :pswitch_85
    iget-object v0, p0, Lbf/ar;->g:Lbf/am;

    iget-object v1, p0, Lbf/ar;->c:Ljava/io/DataInput;

    iget v2, p0, Lbf/ar;->d:I

    iget-boolean v3, p0, Lbf/ar;->e:Z

    iget-boolean v4, p0, Lbf/ar;->f:Z

    invoke-static {v0, v1, v2, v3, v4}, Lbf/am;->g(Lbf/am;Ljava/io/DataInput;IZZ)V
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_92} :catch_18

    goto/16 :goto_5

    .line 3397
    :pswitch_data_94
    .packed-switch 0x0
        :pswitch_6
        :pswitch_65
        :pswitch_5
        :pswitch_49
        :pswitch_5
        :pswitch_5
        :pswitch_57
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_77
        :pswitch_5
        :pswitch_85
        :pswitch_5
        :pswitch_5
        :pswitch_37
    .end packed-switch
.end method
