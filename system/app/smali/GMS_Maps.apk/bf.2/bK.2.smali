.class public Lbf/bK;
.super Lbf/F;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ui/view/d;

.field private B:LaS/a;

.field private C:Lcom/google/googlenav/F;

.field private D:Z

.field private E:Lbm/j;

.field private final F:Lbf/bS;

.field private v:Z

.field private w:Z

.field private x:Lcom/google/googlenav/ui/view/r;

.field private y:Lcom/google/googlenav/ui/view/android/bF;

.field private z:Lcom/google/googlenav/ui/view/d;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 163
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/bK;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 164
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 154
    invoke-direct/range {p0 .. p5}, Lbf/F;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 147
    invoke-direct {p0}, Lbf/bK;->be()Lbf/bS;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->F:Lbf/bS;

    .line 155
    return-void
.end method

.method static synthetic a(Lbf/bK;Lcom/google/googlenav/ui/view/d;)Lcom/google/googlenav/ui/view/d;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lbf/bK;->z:Lcom/google/googlenav/ui/view/d;

    return-object p1
.end method

.method static synthetic a(Lbf/bK;)V
    .registers 1
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lbf/bK;->bl()V

    return-void
.end method

.method static synthetic a(Lbf/bK;Landroid/content/res/Configuration;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lbf/bK;->b(Landroid/content/res/Configuration;)V

    return-void
.end method

.method private b(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->e()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 222
    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/r;->a(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 223
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/r;->k()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(II)V

    .line 225
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    iget-object v1, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/r;->h()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/i;)V

    .line 226
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->b()V

    .line 229
    :cond_2e
    return-void
.end method

.method static synthetic b(Lbf/bK;)V
    .registers 1
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lbf/bK;->bf()V

    return-void
.end method

.method private be()Lbf/bS;
    .registers 2

    .prologue
    .line 167
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 168
    new-instance v0, Lbf/bT;

    invoke-direct {v0, p0}, Lbf/bT;-><init>(Lbf/bK;)V

    .line 172
    :goto_f
    return-object v0

    .line 169
    :cond_10
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 170
    new-instance v0, Lbf/bQ;

    invoke-direct {v0, p0}, Lbf/bQ;-><init>(Lbf/bK;)V

    goto :goto_f

    .line 172
    :cond_1c
    new-instance v0, Lbf/bO;

    invoke-direct {v0, p0}, Lbf/bO;-><init>(Lbf/bK;)V

    goto :goto_f
.end method

.method private bf()V
    .registers 4

    .prologue
    .line 207
    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->i()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 208
    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->a()V

    .line 210
    :cond_d
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->i()Z

    .line 211
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/r;->k()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(II)V

    .line 212
    invoke-direct {p0}, Lbf/bK;->bo()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/bK;->b(Landroid/content/res/Configuration;)V

    .line 213
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    iget-object v1, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/r;->h()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/i;)V

    .line 214
    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->c()V

    .line 215
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    iget-object v1, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/r;->g()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/android/maps/rideabout/view/h;)V

    .line 216
    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->e()V

    .line 217
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->b()V

    .line 218
    return-void
.end method

.method private bg()Z
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 288
    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 289
    invoke-virtual {p0}, Lbf/bK;->f()V

    .line 295
    :goto_a
    return v1

    .line 293
    :cond_b
    invoke-virtual {p0}, Lbf/bK;->l()V

    .line 294
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->o()V

    goto :goto_a
.end method

.method private bh()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 314
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->s()V

    .line 315
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->l()V

    .line 316
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->d()V

    .line 317
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->f()V

    .line 318
    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->b()V

    .line 320
    iput-boolean v1, p0, Lbf/bK;->v:Z

    .line 321
    iput-boolean v1, p0, Lbf/bK;->w:Z

    .line 322
    return-void
.end method

.method private bi()Lax/w;
    .registers 2

    .prologue
    .line 351
    iget-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    return-object v0
.end method

.method private bj()V
    .registers 1

    .prologue
    .line 491
    return-void
.end method

.method private bk()Z
    .registers 3

    .prologue
    .line 499
    iget-object v0, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    .line 500
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ad()Z

    move-result v1

    if-nez v1, :cond_16

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ae()Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private bl()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 595
    iput-boolean v3, p0, Lbf/bK;->D:Z

    .line 597
    new-instance v0, Lbf/bM;

    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    iget-object v2, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, v3}, Lbf/bM;-><init>(Lbf/bK;Las/c;Lcom/google/googlenav/android/aa;Z)V

    iput-object v0, p0, Lbf/bK;->E:Lbm/j;

    .line 604
    iget-object v0, p0, Lbf/bK;->E:Lbm/j;

    invoke-virtual {v0}, Lbm/j;->g()V

    .line 605
    return-void
.end method

.method private bm()V
    .registers 5

    .prologue
    .line 793
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/16 v1, 0x14

    new-instance v2, Lbf/bN;

    invoke-direct {v2, p0}, Lbf/bN;-><init>(Lbf/bK;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->A:Lcom/google/googlenav/ui/view/d;

    .line 809
    return-void
.end method

.method private bn()V
    .registers 2

    .prologue
    .line 812
    iget-object v0, p0, Lbf/bK;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_9

    .line 813
    iget-object v0, p0, Lbf/bK;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 815
    :cond_9
    return-void
.end method

.method private bo()Landroid/content/res/Configuration;
    .registers 2

    .prologue
    .line 829
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lbf/bK;)V
    .registers 1
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lbf/bK;->bm()V

    return-void
.end method

.method static synthetic d(Lbf/bK;)Lcom/google/googlenav/ui/view/android/bF;
    .registers 2
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    return-object v0
.end method

.method static synthetic e(Lbf/bK;)Lcom/google/googlenav/ui/view/r;
    .registers 2
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    return-object v0
.end method

.method static synthetic f(Lbf/bK;)Lcom/google/googlenav/ui/view/d;
    .registers 2
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lbf/bK;->z:Lcom/google/googlenav/ui/view/d;

    return-object v0
.end method

.method static synthetic g(Lbf/bK;)V
    .registers 1
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lbf/bK;->bn()V

    return-void
.end method

.method static synthetic h(Lbf/bK;)Landroid/content/res/Configuration;
    .registers 2
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Lbf/bK;->bo()Landroid/content/res/Configuration;

    move-result-object v0

    return-object v0
.end method

.method private h(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 389
    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/bK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    if-ltz p1, :cond_27

    iget-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_27

    .line 391
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, Lbf/bK;->a(IZZ)V

    .line 395
    :goto_18
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_26

    .line 396
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lbf/bK;->b(ILjava/lang/Object;)V

    .line 398
    :cond_26
    return-void

    .line 393
    :cond_27
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/bK;->b(I)V

    goto :goto_18
.end method


# virtual methods
.method protected O()Z
    .registers 2

    .prologue
    .line 611
    const/4 v0, 0x1

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 618
    const/4 v0, 0x1

    return v0
.end method

.method protected T()Ljava/lang/String;
    .registers 2

    .prologue
    .line 198
    const/16 v0, 0x4d1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .registers 2

    .prologue
    .line 203
    const/16 v0, 0x12

    return v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 819
    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->e()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 820
    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 821
    iget-object v0, p0, Lbf/bK;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LT/a;

    invoke-virtual {v0, p1}, LT/a;->a(Landroid/content/res/Configuration;)V

    .line 826
    :cond_15
    :goto_15
    return-void

    .line 823
    :cond_16
    invoke-direct {p0, p1}, Lbf/bK;->b(Landroid/content/res/Configuration;)V

    goto :goto_15
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 4
    .parameter

    .prologue
    .line 341
    iput-object p1, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    .line 342
    invoke-interface {p1}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 344
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-lez v0, :cond_1d

    .line 345
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bK;->b(I)V

    .line 348
    :cond_1d
    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .registers 3
    .parameter

    .prologue
    .line 553
    invoke-virtual {p0}, Lbf/bK;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lax/b;->b(Ljava/io/DataOutput;)V

    .line 554
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 845
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 429
    sparse-switch p1, :sswitch_data_56

    move v0, v1

    .line 468
    :cond_6
    :goto_6
    return v0

    .line 432
    :sswitch_7
    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-direct {p0}, Lbf/bK;->bk()Z

    move-result v2

    if-nez v2, :cond_6

    .line 433
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bK;->c(ILjava/lang/Object;)V

    goto :goto_6

    .line 437
    :sswitch_18
    invoke-direct {p0}, Lbf/bK;->bk()Z

    move-result v1

    if-nez v1, :cond_2c

    .line 438
    const-string v1, "sb"

    invoke-static {v1}, Lbf/O;->b(Ljava/lang/String;)V

    .line 440
    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/ak;->a(Z)V

    .line 444
    :cond_2c
    :sswitch_2c
    invoke-direct {p0}, Lbf/bK;->bk()Z

    move-result v1

    if-nez v1, :cond_6

    .line 445
    invoke-direct {p0, p2}, Lbf/bK;->h(I)V

    goto :goto_6

    .line 449
    :sswitch_36
    invoke-virtual {p0}, Lbf/bK;->W()V

    goto :goto_6

    .line 453
    :sswitch_3a
    invoke-virtual {p0}, Lbf/bK;->d()V

    goto :goto_6

    .line 456
    :sswitch_3e
    invoke-direct {p0}, Lbf/bK;->bj()V

    goto :goto_6

    .line 459
    :sswitch_42
    iget-object v1, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v1}, LaS/a;->r()V

    goto :goto_6

    .line 462
    :sswitch_48
    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->h()V

    goto :goto_6

    .line 465
    :sswitch_52
    invoke-virtual {p0}, Lbf/bK;->f()V

    goto :goto_6

    .line 429
    :sswitch_data_56
    .sparse-switch
        0x1 -> :sswitch_18
        0xc8 -> :sswitch_2c
        0xf1 -> :sswitch_7
        0x3f9 -> :sswitch_36
        0xb54 -> :sswitch_3a
        0xb55 -> :sswitch_52
        0xb56 -> :sswitch_3e
        0xb57 -> :sswitch_42
        0xb58 -> :sswitch_48
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 329
    const/16 v0, 0x1b

    invoke-super {p0, p1, v0}, Lbf/F;->a(Lcom/google/googlenav/ui/view/t;I)Z

    move-result v0

    return v0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .registers 3
    .parameter

    .prologue
    .line 543
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    .line 546
    invoke-static {p1, v0}, Lax/b;->a(Ljava/io/DataInput;Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    .line 547
    iput-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    .line 548
    const/4 v0, 0x1

    return v0
.end method

.method public aF()I
    .registers 2

    .prologue
    .line 623
    const v0, 0x7f110022

    return v0
.end method

.method public aK()Ljava/lang/String;
    .registers 5

    .prologue
    .line 334
    invoke-virtual {p0}, Lbf/bK;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->as()Lax/y;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bv;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    .line 335
    const/16 v1, 0x4a5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .registers 2

    .prologue
    .line 538
    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .registers 3

    .prologue
    .line 178
    iget-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    iput-object v0, p0, Lbf/bK;->C:Lcom/google/googlenav/F;

    .line 181
    invoke-virtual {p0}, Lbf/bK;->a()Lax/b;

    move-result-object v0

    invoke-virtual {v0}, Lax/b;->aJ()Lax/b;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    .line 184
    iget-object v0, p0, Lbf/bK;->C:Lcom/google/googlenav/F;

    check-cast v0, Lax/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lax/w;->c(Z)V

    .line 186
    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    .line 187
    invoke-static {}, Lcom/google/googlenav/ui/view/r;->l()Lcom/google/googlenav/ui/view/r;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->x:Lcom/google/googlenav/ui/view/r;

    .line 188
    invoke-static {}, Lcom/google/googlenav/ui/view/android/bF;->a()Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    .line 189
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    iput-object v0, p0, Lbf/bK;->B:LaS/a;

    .line 190
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->s()V

    .line 191
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-direct {p0}, Lbf/bK;->bi()Lax/w;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, LaS/a;->a(Lax/w;Lcom/google/googlenav/ui/e;)V

    .line 192
    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->a()V

    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .registers 1

    .prologue
    .line 309
    invoke-direct {p0}, Lbf/bK;->bh()V

    .line 310
    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    .line 311
    return-void
.end method

.method public aV()V
    .registers 2

    .prologue
    .line 264
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->l()V

    .line 265
    return-void
.end method

.method public aW()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 238
    invoke-super {p0}, Lbf/F;->aW()V

    .line 239
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/view/android/bF;->a(Lcom/google/googlenav/ui/e;)V

    .line 241
    iget-boolean v0, p0, Lbf/bK;->v:Z

    if-nez v0, :cond_19

    .line 242
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->j()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 245
    iput-boolean v1, p0, Lbf/bK;->v:Z

    .line 246
    iput-boolean v1, p0, Lbf/bK;->w:Z

    .line 252
    :cond_19
    :goto_19
    return-void

    .line 248
    :cond_1a
    iget-object v0, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v0}, LaS/a;->k()V

    .line 249
    iput-boolean v1, p0, Lbf/bK;->v:Z

    goto :goto_19
.end method

.method public aX()V
    .registers 2

    .prologue
    .line 258
    iget-object v0, p0, Lbf/bK;->y:Lcom/google/googlenav/ui/view/android/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->m()V

    .line 259
    invoke-super {p0}, Lbf/F;->aX()V

    .line 260
    return-void
.end method

.method protected ap()V
    .registers 1

    .prologue
    .line 835
    return-void
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 839
    new-instance v0, LT/a;

    invoke-direct {v0, p0}, LT/a;-><init>(Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lbf/bK;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 840
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 272
    const/16 v0, 0x15

    return v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 232
    iget-boolean v0, p0, Lbf/bK;->w:Z

    iget-object v1, p0, Lbf/bK;->B:LaS/a;

    invoke-virtual {v1}, LaS/a;->h()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lbf/bK;->w:Z

    .line 233
    iget-boolean v0, p0, Lbf/bK;->w:Z

    return v0
.end method

.method public d()V
    .registers 4

    .prologue
    .line 300
    iget-object v0, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    .line 303
    iget-object v1, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    iget-object v0, p0, Lbf/bK;->C:Lcom/google/googlenav/F;

    check-cast v0, Lax/b;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/b;B)V

    .line 305
    return-void
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .registers 8

    .prologue
    const/4 v0, 0x0

    .line 509
    invoke-direct {p0}, Lbf/bK;->bi()Lax/w;

    move-result-object v3

    .line 510
    if-nez v3, :cond_8

    .line 530
    :cond_7
    :goto_7
    return-object v0

    .line 513
    :cond_8
    invoke-virtual {v3}, Lax/w;->ae()I

    move-result v4

    .line 518
    const/4 v1, 0x1

    if-lt v4, v1, :cond_7

    .line 521
    add-int/lit8 v0, v4, -0x1

    new-array v1, v0, [Lcom/google/googlenav/ui/aH;

    .line 522
    const/4 v0, 0x0

    move v2, v0

    :goto_15
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_3d

    .line 523
    new-instance v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->b(Z)I

    move-result v5

    add-int/lit8 v6, v2, 0x1

    invoke-direct {v0, v3, v5, v2, v6}, Lcom/google/googlenav/ui/aJ;-><init>(Lax/w;III)V

    aput-object v0, v1, v2

    .line 526
    aget-object v0, v1, v2

    check-cast v0, Lcom/google/googlenav/ui/aJ;

    invoke-virtual {v3, v2}, Lax/w;->q(I)Z

    move-result v5

    invoke-static {v5}, Lcom/google/googlenav/ui/aJ;->a(Z)I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/aJ;->a(I)V

    .line 522
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_15

    :cond_3d
    move-object v0, v1

    .line 530
    goto :goto_7
.end method

.method public f()V
    .registers 9

    .prologue
    .line 560
    iget-boolean v0, p0, Lbf/bK;->D:Z

    if-eqz v0, :cond_5

    .line 592
    :goto_4
    return-void

    .line 563
    :cond_5
    iget-object v0, p0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x492

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x491

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-instance v7, Lbf/bL;

    invoke-direct {v7, p0}, Lbf/bL;-><init>(Lbf/bK;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_4
.end method

.method protected f(Lat/a;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 359
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_2d

    .line 360
    invoke-virtual {p0}, Lbf/bK;->ae()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 361
    const-string v1, "m"

    const-string v2, "#"

    invoke-virtual {p0, v1, v2}, Lbf/bK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v1, p0, Lbf/bK;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-direct {p0, v1}, Lbf/bK;->h(I)V

    .line 385
    :cond_20
    :goto_20
    return v0

    .line 364
    :cond_21
    const-string v2, "l"

    const-string v3, "#"

    invoke-virtual {p0, v2, v3}, Lbf/bK;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bK;->c(ILjava/lang/Object;)V

    goto :goto_20

    .line 371
    :cond_2d
    invoke-virtual {p0}, Lbf/bK;->ai()Z

    move-result v2

    if-nez v2, :cond_53

    .line 372
    iget-object v2, p0, Lbf/bK;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    invoke-virtual {p0, v2}, Lbf/bK;->b(LaN/B;)I

    move-result v2

    if-ltz v2, :cond_46

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    const/4 v3, 0x7

    if-eq v2, v3, :cond_20

    .line 378
    :cond_46
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_53

    .line 379
    invoke-direct {p0}, Lbf/bK;->bg()Z

    move-result v0

    goto :goto_20

    .line 382
    :cond_53
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v2

    invoke-virtual {p1}, Lat/a;->a()I

    move-result v3

    invoke-virtual {v2, v3}, LaS/a;->a(I)Z

    move-result v2

    if-nez v2, :cond_20

    move v0, v1

    .line 385
    goto :goto_20
.end method

.method public h()V
    .registers 1

    .prologue
    .line 277
    invoke-direct {p0}, Lbf/bK;->bg()Z

    .line 278
    return-void
.end method

.method protected l()V
    .registers 2

    .prologue
    .line 413
    invoke-super {p0}, Lbf/F;->l()V

    .line 415
    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->c()V

    .line 416
    return-void
.end method

.method protected n()V
    .registers 2

    .prologue
    .line 407
    iget-object v0, p0, Lbf/bK;->F:Lbf/bS;

    invoke-interface {v0}, Lbf/bS;->d()V

    .line 408
    invoke-super {p0}, Lbf/F;->n()V

    .line 409
    return-void
.end method

.method public q()I
    .registers 2

    .prologue
    .line 402
    invoke-static {}, Lcom/google/googlenav/ui/view/r;->l()Lcom/google/googlenav/ui/view/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->k()I

    move-result v0

    return v0
.end method
