.class public Lbf/C;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements Lba/e;
.implements Lcom/google/googlenav/friend/bf;


# instance fields
.field private B:Z

.field private C:Z

.field private D:[Ljava/lang/String;

.field private E:Z

.field private F:Lcom/google/googlenav/friend/be;

.field private G:Lba/d;

.field private H:Z

.field private I:Z

.field private J:[Ljava/lang/String;

.field private K:Z

.field private L:Ljava/lang/String;

.field private M:Z

.field private N:Z

.field private O:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final P:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;Z[Ljava/lang/String;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 171
    invoke-direct/range {p0 .. p5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 165
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbf/C;->P:Ljava/lang/Object;

    .line 173
    iput-boolean p6, p0, Lbf/C;->I:Z

    .line 174
    iput-object p7, p0, Lbf/C;->J:[Ljava/lang/String;

    .line 175
    iput-boolean p8, p0, Lbf/C;->M:Z

    .line 176
    iput-object p9, p0, Lbf/C;->O:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 177
    if-eqz p7, :cond_22

    array-length v0, p7

    if-lez v0, :cond_22

    const/4 v0, 0x1

    :goto_18
    iput-boolean v0, p0, Lbf/C;->K:Z

    .line 178
    new-instance v0, Lbf/B;

    invoke-direct {v0, p0}, Lbf/B;-><init>(Lbf/C;)V

    iput-object v0, p0, Lbf/C;->A:Lbf/be;

    .line 179
    return-void

    .line 177
    :cond_22
    const/4 v0, 0x0

    goto :goto_18
.end method

.method private a(Ljava/util/List;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 613
    if-eqz p1, :cond_49

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_49

    .line 616
    invoke-virtual {p0}, Lbf/C;->bu()Lcom/google/googlenav/ai;

    move-result-object v3

    .line 617
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/googlenav/ai;

    move v1, v2

    .line 618
    :goto_15
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2b

    .line 619
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;B)Lcom/google/googlenav/ai;

    move-result-object v0

    aput-object v0, v4, v1

    .line 618
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    .line 622
    :cond_2b
    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->a([Lcom/google/googlenav/ai;)V

    .line 624
    iget-boolean v0, p0, Lbf/C;->M:Z

    if-nez v0, :cond_49

    .line 629
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v1, v1, v2

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lbf/C;->bm()Z

    move-result v1

    invoke-static {v3, v1}, Lbf/aS;->c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iput-object v0, p0, Lbf/C;->J:[Ljava/lang/String;

    .line 632
    iput-boolean v5, p0, Lbf/C;->K:Z

    .line 635
    :cond_49
    return-void
.end method

.method private static b(Ljava/lang/String;)LaN/B;
    .registers 6
    .parameter

    .prologue
    const v4, 0x49742400

    const/4 v0, 0x0

    .line 841
    if-nez p0, :cond_7

    .line 859
    :cond_6
    :goto_6
    return-object v0

    .line 844
    :cond_7
    const-string v1, "http://maps.google.com/?q="

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 845
    const-string v1, "http://maps.google.com/?q="

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 846
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 847
    if-lez v2, :cond_6

    .line 848
    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 849
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 851
    :try_start_2c
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 852
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 853
    new-instance v1, LaN/B;

    mul-float/2addr v2, v4

    float-to-int v2, v2

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, LaN/B;-><init>(II)V
    :try_end_3d
    .catch Ljava/lang/NumberFormatException; {:try_start_2c .. :try_end_3d} :catch_3f

    move-object v0, v1

    goto :goto_6

    .line 854
    :catch_3f
    move-exception v1

    goto :goto_6
.end method

.method private bO()V
    .registers 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 199
    invoke-virtual {p0, v0}, Lbf/C;->b(B)V

    .line 200
    iput-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    .line 201
    iput-boolean v1, p0, Lbf/C;->E:Z

    .line 202
    iput-object v2, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    .line 203
    iput-object v2, p0, Lbf/C;->G:Lba/d;

    .line 205
    iget-boolean v2, p0, Lbf/C;->K:Z

    if-nez v2, :cond_15

    :goto_12
    iput-boolean v0, p0, Lbf/C;->H:Z

    .line 206
    return-void

    :cond_15
    move v0, v1

    .line 205
    goto :goto_12
.end method

.method private bP()Lo/D;
    .registers 3

    .prologue
    .line 281
    iget-object v0, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_1a

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1a

    .line 284
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 287
    :goto_19
    return-object v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private bQ()V
    .registers 2

    .prologue
    .line 488
    invoke-virtual {p0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 489
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->A()V

    .line 490
    iget-object v0, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    if-nez v0, :cond_16

    .line 493
    invoke-direct {p0}, Lbf/C;->bR()V

    .line 496
    :cond_16
    invoke-virtual {p0}, Lbf/C;->m()V

    .line 497
    return-void
.end method

.method private bR()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 504
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 505
    iput-object v1, p0, Lbf/C;->G:Lba/d;

    .line 506
    iput-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    .line 507
    iput-boolean v4, p0, Lbf/C;->B:Z

    .line 509
    iput-boolean v5, p0, Lbf/C;->C:Z

    .line 511
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p0, p0, v1}, Lbf/C;->a(Lba/e;LaN/B;)Lba/d;

    move-result-object v1

    iput-object v1, p0, Lbf/C;->G:Lba/d;

    .line 512
    iget-boolean v1, p0, Lbf/C;->H:Z

    if-eqz v1, :cond_26

    .line 514
    iget-object v1, p0, Lbf/C;->G:Lba/d;

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    sget v3, Lcom/google/googlenav/ui/bi;->by:I

    invoke-virtual {v1, v2, v3}, Lba/d;->a(II)V

    .line 519
    :cond_26
    iput-boolean v5, p0, Lbf/C;->B:Z

    .line 522
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbf/C;->d(LaN/B;)Lcom/google/googlenav/friend/be;

    move-result-object v1

    iput-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    .line 524
    iget-object v1, p0, Lbf/C;->G:Lba/d;

    if-eqz v1, :cond_40

    .line 528
    iget-object v1, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v1, v4}, Lba/d;->a(Z)V

    .line 529
    iget-object v1, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v1}, Lba/d;->o()V

    .line 531
    :cond_40
    iget-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    if-eqz v1, :cond_49

    .line 532
    iget-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 534
    :cond_49
    return-void
.end method

.method private bS()V
    .registers 3

    .prologue
    .line 674
    iget-object v1, p0, Lbf/C;->P:Ljava/lang/Object;

    monitor-enter v1

    .line 677
    :try_start_3
    iget-boolean v0, p0, Lbf/C;->C:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lbf/C;->B:Z

    if-eqz v0, :cond_d

    .line 678
    :cond_b
    monitor-exit v1

    .line 693
    :goto_c
    return-void

    .line 681
    :cond_d
    invoke-virtual {p0}, Lbf/C;->ag()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 685
    invoke-virtual {p0}, Lbf/C;->an()Z

    .line 692
    :cond_16
    :goto_16
    monitor-exit v1

    goto :goto_c

    :catchall_18
    move-exception v0

    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_3 .. :try_end_1a} :catchall_18

    throw v0

    .line 686
    :cond_1b
    :try_start_1b
    invoke-virtual {p0}, Lbf/C;->af()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 690
    invoke-virtual {p0}, Lbf/C;->bp()V
    :try_end_24
    .catchall {:try_start_1b .. :try_end_24} :catchall_18

    goto :goto_16
.end method

.method private bT()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v1, 0x0

    const v2, 0x49742400

    .line 773
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 774
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 779
    :goto_16
    return-object v0

    .line 776
    :cond_17
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    .line 777
    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    .line 778
    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 779
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_16
.end method


# virtual methods
.method protected O()Z
    .registers 2

    .prologue
    .line 321
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .registers 2

    .prologue
    .line 365
    iget-boolean v0, p0, Lbf/C;->I:Z

    if-eqz v0, :cond_c

    .line 366
    invoke-super {p0}, Lbf/m;->Z()V

    .line 367
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    .line 369
    :cond_c
    return-void
.end method

.method public a()LaN/B;
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lba/e;LaN/B;)Lba/d;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 720
    new-instance v0, Lba/d;

    invoke-direct {v0, p1, p2}, Lba/d;-><init>(Lba/e;LaN/B;)V

    return-object v0
.end method

.method public a(J)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 641
    iput-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    .line 642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/C;->B:Z

    .line 643
    iput-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    .line 647
    invoke-direct {p0}, Lbf/C;->bS()V

    .line 648
    return-void
.end method

.method public a(Lba/d;)V
    .registers 3
    .parameter

    .prologue
    .line 657
    invoke-virtual {p0}, Lbf/C;->ax()Z

    move-result v0

    if-nez v0, :cond_7

    .line 667
    :cond_6
    :goto_6
    return-void

    .line 660
    :cond_7
    if-eqz p1, :cond_6

    iget-object v0, p0, Lbf/C;->G:Lba/d;

    if-ne p1, v0, :cond_6

    .line 664
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/C;->C:Z

    .line 665
    invoke-virtual {p1}, Lba/d;->k()Z

    move-result v0

    iput-boolean v0, p0, Lbf/C;->E:Z

    .line 666
    invoke-direct {p0}, Lbf/C;->bS()V

    goto :goto_6
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 183
    iput-object p1, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    .line 184
    invoke-direct {p0}, Lbf/C;->bO()V

    .line 188
    invoke-virtual {p0}, Lbf/C;->bI()Z

    move-result v0

    if-nez v0, :cond_e

    .line 189
    invoke-direct {p0}, Lbf/C;->bR()V

    .line 193
    :cond_e
    invoke-virtual {p0}, Lbf/C;->ax()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 194
    invoke-virtual {p0}, Lbf/C;->y()V

    .line 196
    :cond_17
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 552
    invoke-virtual {p0}, Lbf/C;->ax()Z

    move-result v1

    if-nez v1, :cond_b

    .line 602
    :cond_a
    :goto_a
    return-void

    .line 556
    :cond_b
    iget-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    if-eqz v1, :cond_a

    .line 564
    iput-boolean v3, p0, Lbf/C;->B:Z

    .line 569
    if-eqz p1, :cond_7d

    .line 570
    invoke-static {p1, v3}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;B)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 574
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_27

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2e

    .line 575
    :cond_27
    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(Lo/D;)V

    .line 577
    :cond_2e
    iget-object v1, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    .line 578
    new-instance v2, Lcom/google/googlenav/bl;

    invoke-direct {v2, v0}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v2, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    .line 579
    invoke-virtual {p0, v1}, Lbf/C;->b(B)V

    .line 580
    invoke-virtual {p0}, Lbf/C;->R()V

    .line 584
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    .line 585
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    .line 588
    :goto_49
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_74

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_74

    .line 591
    new-array v0, v5, [Ljava/lang/String;

    const/16 v1, 0x5e6

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, ""

    aput-object v1, v0, v4

    iput-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    .line 597
    :goto_65
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    .line 599
    invoke-direct {p0, p2}, Lbf/C;->a(Ljava/util/List;)V

    .line 601
    invoke-direct {p0}, Lbf/C;->bS()V

    goto :goto_a

    .line 593
    :cond_74
    new-array v2, v5, [Ljava/lang/String;

    aput-object v1, v2, v3

    aput-object v0, v2, v4

    iput-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    goto :goto_65

    :cond_7d
    move-object v1, v0

    goto :goto_49
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 297
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 373
    .line 374
    sparse-switch p1, :sswitch_data_1be

    move v0, v6

    .line 464
    :goto_8
    const/4 v1, -0x1

    if-eq p1, v1, :cond_33

    if-eqz v0, :cond_33

    .line 465
    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "a=s"

    aput-object v2, v1, v6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 467
    const/16 v2, 0x10

    const-string v3, "c"

    invoke-static {v2, v3, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 471
    :cond_33
    if-nez v0, :cond_39

    .line 472
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    .line 475
    :cond_39
    return v0

    .line 376
    :sswitch_3a
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, Lbf/C;->b(ILjava/lang/Object;)V

    .line 377
    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_5b

    .line 378
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v5, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v8

    .line 381
    goto :goto_8

    .line 382
    :cond_5b
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x18

    iget-object v3, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 387
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-nez v0, :cond_8b

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 391
    :goto_7d
    iget-object v1, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v5, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v8

    .line 394
    goto/16 :goto_8

    .line 387
    :cond_8b
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v1, v1, v6

    iget-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v2, v2, v8

    invoke-static {v1, v2}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lax/y;->a(LaN/B;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    goto :goto_7d

    .line 399
    :sswitch_a4
    const/16 v0, 0x25d

    if-ne p1, v0, :cond_cf

    move v0, v7

    .line 405
    :goto_a9
    const/16 v1, 0x45

    const-string v2, "n"

    const-string v3, "c"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-virtual {p0}, Lbf/C;->n()V

    .line 409
    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    if-nez v1, :cond_d5

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v2

    invoke-static {v1, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v1

    .line 413
    :goto_c5
    iget-object v2, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    const-string v3, "c"

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/googlenav/ui/s;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    move v0, v8

    .line 415
    goto/16 :goto_8

    .line 401
    :cond_cf
    const/16 v0, 0x25e

    if-ne p1, v0, :cond_1bb

    move v0, v8

    .line 402
    goto :goto_a9

    .line 409
    :cond_d5
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v2, v2, v6

    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v2, v3}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v3

    invoke-static {v1, v5, v2, v3}, Lax/y;->a(LaN/B;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v1

    goto :goto_c5

    .line 417
    :sswitch_ee
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_f9

    .line 418
    const-string v0, "Street View"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 421
    :cond_f9
    invoke-virtual {p0}, Lbf/C;->n()V

    .line 422
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(LaN/B;)V

    .line 423
    invoke-static {}, Lba/c;->a()V

    move v0, v8

    .line 424
    goto/16 :goto_8

    .line 426
    :sswitch_10b
    invoke-virtual {p0}, Lbf/C;->n()V

    .line 427
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "9"

    invoke-virtual {p0}, Lbf/C;->s()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v5, v1, v2, v8}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v8

    .line 429
    goto/16 :goto_8

    .line 435
    :sswitch_124
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-eqz v0, :cond_1b8

    .line 436
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v6

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v1, v1, v8

    invoke-static {v0, v1}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 438
    :goto_134
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v2

    invoke-virtual {p0}, Lbf/C;->bN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lbf/C;->bI()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v8

    .line 440
    goto/16 :goto_8

    .line 442
    :sswitch_14c
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 443
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v9

    new-instance v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    iget-boolean v3, p0, Lbf/C;->M:Z

    if-eqz v3, :cond_189

    const/16 v3, 0x530

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v6

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_178
    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v6

    iget-object v4, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v4, v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-virtual {v9, v0}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ai;)V

    move v0, v8

    .line 448
    goto/16 :goto_8

    :cond_189
    move-object v2, v5

    .line 443
    goto :goto_178

    .line 450
    :sswitch_18b
    new-instance v1, LaR/D;

    iget-object v2, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-direct {p0}, Lbf/C;->bT()Ljava/lang/String;

    move-result-object v3

    move-object v4, v5

    invoke-direct/range {v1 .. v6}, LaR/D;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    .line 451
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v1, v0}, LaR/D;->a(LaN/B;)V

    .line 452
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    iget-object v2, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v2

    const-string v3, "p"

    invoke-interface {v0, v1, v5, v2, v3}, LaR/n;->a(LaR/D;LaR/H;Lbf/am;Ljava/lang/String;)LaR/D;

    .line 454
    invoke-virtual {p0}, Lbf/C;->bp()V

    move v0, v8

    .line 455
    goto/16 :goto_8

    :sswitch_1b5
    move v0, v6

    .line 459
    goto/16 :goto_8

    :cond_1b8
    move-object v3, v5

    goto/16 :goto_134

    :cond_1bb
    move v0, v6

    goto/16 :goto_a9

    .line 374
    :sswitch_data_1be
    .sparse-switch
        0x4 -> :sswitch_14c
        0xf -> :sswitch_1b5
        0x258 -> :sswitch_ee
        0x25a -> :sswitch_10b
        0x25b -> :sswitch_3a
        0x25c -> :sswitch_a4
        0x25d -> :sswitch_a4
        0x25e -> :sswitch_a4
        0x578 -> :sswitch_18b
        0x5dc -> :sswitch_124
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 480
    iget-object v0, p0, Lbf/C;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lbf/C;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_d

    .line 481
    invoke-direct {p0}, Lbf/C;->bQ()V

    .line 482
    const/4 v0, 0x1

    .line 484
    :goto_c
    return v0

    :cond_d
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_c
.end method

.method public aE()Z
    .registers 2

    .prologue
    .line 334
    const/4 v0, 0x0

    return v0
.end method

.method protected aq()V
    .registers 1

    .prologue
    .line 883
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 311
    const/16 v0, 0xd

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 338
    iget-boolean v0, p0, Lbf/C;->B:Z

    return v0
.end method

.method public b(LaN/g;)Z
    .registers 3
    .parameter

    .prologue
    .line 725
    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_6

    .line 726
    const/4 v0, 0x0

    .line 728
    :goto_5
    return v0

    :cond_6
    invoke-super {p0, p1}, Lbf/m;->b(LaN/g;)Z

    move-result v0

    goto :goto_5
.end method

.method public bG()Lam/f;
    .registers 2

    .prologue
    .line 354
    iget-object v0, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v0}, Lba/d;->m()Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public bH()Z
    .registers 2

    .prologue
    .line 358
    iget-boolean v0, p0, Lbf/C;->H:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Lbf/C;->C:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v0}, Lba/d;->l()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public bI()Z
    .registers 2

    .prologue
    .line 740
    iget-boolean v0, p0, Lbf/C;->M:Z

    return v0
.end method

.method public bJ()Z
    .registers 2

    .prologue
    .line 744
    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lbf/C;->N:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public bK()Z
    .registers 2

    .prologue
    .line 752
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->u()Z

    move-result v0

    return v0
.end method

.method public bL()Z
    .registers 2

    .prologue
    .line 756
    iget-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public bM()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 760
    invoke-virtual {p0}, Lbf/C;->bL()Z

    move-result v0

    if-nez v0, :cond_8

    .line 765
    :goto_7
    return v1

    .line 763
    :cond_8
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v2, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-interface {v0, v2}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 765
    if-eqz v0, :cond_25

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_25

    const/4 v0, 0x1

    :goto_23
    move v1, v0

    goto :goto_7

    :cond_25
    move v0, v1

    goto :goto_23
.end method

.method bN()Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 791
    iget-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-static {v0}, Lbf/C;->b(Ljava/lang/String;)LaN/B;

    move-result-object v2

    .line 792
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-eqz v0, :cond_f

    if-nez v2, :cond_12

    .line 796
    :cond_f
    iget-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    .line 830
    :goto_11
    return-object v0

    .line 801
    :cond_12
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b2

    .line 802
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 803
    const-string v3, " "

    const-string v4, "+"

    invoke-static {v3, v4, v0}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)I

    .line 806
    :goto_2c
    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_46

    .line 807
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 808
    const-string v3, " "

    const-string v4, "+"

    invoke-static {v3, v4, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)I

    .line 811
    :cond_46
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://maps.google.com/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 812
    const-string v4, "maps?f=q&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 813
    if-eqz v0, :cond_57

    .line 814
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 816
    :cond_57
    if-eqz v1, :cond_63

    .line 817
    if-eqz v0, :cond_60

    .line 818
    const-string v0, "+"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 820
    :cond_60
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 823
    :cond_63
    const-string v0, "@"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sspn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v1

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v1

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 830
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_11

    :cond_b2
    move-object v0, v1

    goto/16 :goto_2c
.end method

.method public bp()V
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Lbf/C;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lbf/C;->af()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lbf/C;->bA()Z

    move-result v0

    if-nez v0, :cond_11

    .line 230
    :cond_10
    :goto_10
    return-void

    .line 222
    :cond_11
    iget v0, p0, Lbf/C;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_19

    .line 223
    const/4 v0, 0x0

    iput v0, p0, Lbf/C;->v:I

    .line 227
    :cond_19
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/C;->f(Z)V

    .line 229
    iget-object v0, p0, Lbf/C;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/Y;->h()V

    goto :goto_10
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 257
    iget-boolean v1, p0, Lbf/C;->M:Z

    if-eqz v1, :cond_12

    .line 258
    invoke-static {}, Lcom/google/googlenav/ui/bi;->R()[Lam/f;

    move-result-object v1

    aget-object v0, v1, v0

    .line 261
    invoke-interface {v0}, Lam/f;->b()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x4

    .line 263
    :cond_12
    return v0
.end method

.method public c(LaN/g;)Z
    .registers 3
    .parameter

    .prologue
    .line 733
    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_6

    .line 734
    const/4 v0, 0x0

    .line 736
    :goto_5
    return v0

    :cond_6
    invoke-super {p0, p1}, Lbf/m;->c(LaN/g;)Z

    move-result v0

    goto :goto_5
.end method

.method public c()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 342
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    return-object v0
.end method

.method protected d(LaN/B;)Lcom/google/googlenav/friend/be;
    .registers 4
    .parameter

    .prologue
    .line 705
    new-instance v0, Lcom/google/googlenav/friend/bg;

    invoke-direct {v0}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->O:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .registers 2
    .parameter

    .prologue
    .line 274
    return-void
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 326
    iget-boolean v0, p0, Lbf/C;->C:Z

    if-eqz v0, :cond_6

    .line 327
    const/4 v0, 0x0

    .line 329
    :goto_5
    return v0

    :cond_6
    iget-boolean v0, p0, Lbf/C;->E:Z

    goto :goto_5
.end method

.method public d()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 346
    iget-object v0, p0, Lbf/C;->J:[Ljava/lang/String;

    return-object v0
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .registers 2
    .parameter

    .prologue
    .line 269
    return-void
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 350
    iget-boolean v0, p0, Lbf/C;->K:Z

    return v0
.end method

.method protected f(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 234
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_15

    invoke-virtual {p0}, Lbf/C;->ag()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 236
    iget-object v1, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    .line 247
    :cond_14
    :goto_14
    return v0

    .line 241
    :cond_15
    invoke-virtual {p0}, Lbf/C;->ah()Z

    move-result v1

    if-nez v1, :cond_14

    .line 242
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_14

    invoke-virtual {p0}, Lbf/C;->ag()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 243
    invoke-direct {p0}, Lbf/C;->bQ()V

    .line 244
    const/4 v0, 0x1

    goto :goto_14
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 301
    new-instance v0, Lbh/g;

    invoke-direct {v0, p0}, Lbh/g;-><init>(Lbf/i;)V

    return-object v0
.end method

.method public k(Z)V
    .registers 2
    .parameter

    .prologue
    .line 748
    iput-boolean p1, p0, Lbf/C;->N:Z

    .line 749
    return-void
.end method

.method protected l()V
    .registers 1

    .prologue
    .line 307
    return-void
.end method
