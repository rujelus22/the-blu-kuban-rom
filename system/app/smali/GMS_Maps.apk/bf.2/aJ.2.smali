.class public Lbf/aJ;
.super Lbf/bk;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private E:Lcom/google/googlenav/ui/view/dialog/bl;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    const/4 v7, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 75
    iput-object p7, p0, Lbf/aJ;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 76
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    const/4 v9, 0x6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 98
    move-object/from16 v0, p8

    iput-object v0, p0, Lbf/aJ;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 99
    return-void
.end method

.method static synthetic a(Lbf/aJ;)V
    .registers 1
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Lbf/aJ;->bW()V

    return-void
.end method

.method private bW()V
    .registers 5

    .prologue
    .line 187
    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 188
    iget-object v1, p0, Lbf/aJ;->y:LaB/s;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lbf/aL;

    invoke-direct {v3, p0}, Lbf/aL;-><init>(Lbf/aJ;)V

    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    .line 195
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .registers 3
    .parameter

    .prologue
    .line 258
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    .line 259
    invoke-virtual {p0}, Lbf/aJ;->h()V

    .line 260
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 199
    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    .line 200
    sparse-switch p1, :sswitch_data_28

    .line 214
    invoke-super {p0, p1, p2, p3}, Lbf/bk;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_b
    return v0

    .line 204
    :sswitch_c
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbf/aJ;->B:Z

    .line 205
    iput p2, p0, Lbf/aJ;->D:I

    .line 206
    const-string v1, "o"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Lbf/aJ;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0, p2}, Lbf/aJ;->j(I)V

    goto :goto_b

    .line 210
    :sswitch_1c
    iget-object v1, p0, Lbf/aJ;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    iget-object v2, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    goto :goto_b

    .line 200
    :sswitch_data_28
    .sparse-switch
        0x2bc -> :sswitch_c
        0x76f -> :sswitch_1c
    .end sparse-switch
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public aF()I
    .registers 3

    .prologue
    .line 232
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 235
    iget-object v0, p0, Lbf/aJ;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_17

    const v0, 0x7f110010

    .line 238
    :goto_16
    return v0

    .line 235
    :cond_17
    const v0, 0x7f110011

    goto :goto_16

    .line 238
    :cond_1b
    invoke-super {p0}, Lbf/bk;->aF()I

    move-result v0

    goto :goto_16
.end method

.method protected ap()V
    .registers 4

    .prologue
    .line 140
    invoke-virtual {p0}, Lbf/aJ;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-gtz v0, :cond_e

    .line 144
    invoke-super {p0}, Lbf/bk;->ap()V

    .line 151
    :goto_d
    return-void

    .line 147
    :cond_e
    iget-object v0, p0, Lbf/aJ;->E:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    .line 148
    invoke-virtual {p0}, Lbf/aJ;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    .line 149
    invoke-virtual {p0}, Lbf/aJ;->bL()V

    .line 150
    iput-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_d
.end method

.method protected aq()V
    .registers 2

    .prologue
    .line 155
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/aJ;->d(Z)V

    .line 157
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bo;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bo;-><init>(Lbf/aJ;)V

    iput-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    .line 165
    invoke-virtual {p0}, Lbf/aJ;->bG()V

    .line 166
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 103
    const/16 v0, 0x1a

    return v0
.end method

.method protected b(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 251
    invoke-super {p0, p1, p2}, Lbf/bk;->b(ILjava/lang/Object;)V

    .line 252
    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/common/offerslib/y;->b:Lcom/google/android/apps/common/offerslib/y;

    invoke-virtual {v1}, Lcom/google/android/apps/common/offerslib/y;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/offers/i;->a(Lcom/google/googlenav/aZ;Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 265
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    .line 266
    invoke-virtual {p0}, Lbf/aJ;->h()V

    .line 267
    const/4 v0, 0x1

    return v0
.end method

.method protected bG()V
    .registers 3

    .prologue
    .line 171
    iget-object v0, p0, Lbf/aJ;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    .line 172
    if-eqz v0, :cond_10

    .line 173
    new-instance v1, Lbf/aK;

    invoke-direct {v1, p0, v0}, Lbf/aK;-><init>(Lbf/aJ;Las/c;)V

    invoke-virtual {v1}, Lbf/aK;->g()V

    .line 180
    :cond_10
    return-void
.end method

.method protected bH()Lcom/google/googlenav/aZ;
    .registers 4

    .prologue
    .line 224
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->j(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {p0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->aq()I

    move-result v2

    invoke-virtual {v1, v0, v2, p0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/bf;ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public bp()V
    .registers 4

    .prologue
    .line 277
    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lbf/aJ;->af()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lbf/aJ;->bA()Z

    move-result v0

    if-nez v0, :cond_11

    .line 286
    :cond_10
    :goto_10
    return-void

    .line 280
    :cond_11
    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/bk;

    if-eqz v0, :cond_24

    .line 281
    iget-object v0, p0, Lbf/aJ;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bk;

    .line 282
    invoke-virtual {p0}, Lbf/aJ;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    goto :goto_10

    .line 284
    :cond_24
    invoke-super {p0}, Lbf/bk;->bp()V

    goto :goto_10
.end method

.method protected c(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 244
    invoke-super {p0, p1, p2}, Lbf/bk;->c(ILjava/lang/Object;)V

    .line 245
    iget-object v0, p0, Lbf/aJ;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->aA()Lcom/google/googlenav/offers/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->e()V

    .line 246
    invoke-static {}, Lcom/google/googlenav/offers/k;->h()Lcom/google/googlenav/offers/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/offers/k;->e()V

    .line 247
    return-void
.end method

.method protected c(Lcom/google/googlenav/aZ;)Z
    .registers 3
    .parameter

    .prologue
    .line 135
    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 108
    const/16 v0, 0xb

    return v0
.end method

.method protected f()Z
    .registers 2

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method protected k(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method
