.class public Lbf/aZ;
.super Lbf/h;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/googlenav/ui/bi;

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    .line 72
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    .line 73
    return-void
.end method

.method private a(I)I
    .registers 3
    .parameter

    .prologue
    .line 297
    sparse-switch p1, :sswitch_data_10

    .line 307
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 299
    :sswitch_5
    const/4 v0, 0x4

    goto :goto_4

    .line 301
    :sswitch_7
    const/16 v0, 0x45

    goto :goto_4

    .line 303
    :sswitch_a
    const/4 v0, 0x1

    goto :goto_4

    .line 305
    :sswitch_c
    const/16 v0, 0x39

    goto :goto_4

    .line 297
    nop

    :sswitch_data_10
    .sparse-switch
        0x10 -> :sswitch_a
        0x258 -> :sswitch_c
        0x25c -> :sswitch_7
        0x262 -> :sswitch_5
    .end sparse-switch
.end method

.method static synthetic a(Lbf/aZ;I)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lbf/aZ;->a(I)I

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/String;Lbf/i;)Lcom/google/googlenav/ui/aW;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 378
    new-instance v0, Lbf/bc;

    invoke-direct {v0, p1}, Lbf/bc;-><init>(Lbf/i;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->e:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 242
    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->d()I

    move-result v0

    .line 243
    sget-object v4, Lbf/bb;->a:[I

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_52

    .line 268
    const/4 v0, 0x0

    .line 270
    :goto_13
    return-object v0

    .line 247
    :pswitch_14
    iget-object v2, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lbf/i;->a(LaN/B;I)Z

    move-result v2

    .line 249
    if-nez v2, :cond_21

    move v0, v1

    .line 270
    :cond_21
    :goto_21
    :pswitch_21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_13

    .line 254
    :pswitch_26
    iget-object v4, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v4}, Lbf/i;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/aA;->e()Z

    move-result v4

    if-eqz v4, :cond_44

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_44

    .line 256
    :goto_40
    if-nez v2, :cond_21

    move v0, v1

    .line 257
    goto :goto_21

    :cond_44
    move v2, v3

    .line 254
    goto :goto_40

    .line 262
    :pswitch_46
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_50

    .line 263
    :goto_4c
    if-nez v2, :cond_21

    move v0, v1

    .line 264
    goto :goto_21

    :cond_50
    move v2, v3

    .line 262
    goto :goto_4c

    .line 243
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_21
        :pswitch_14
        :pswitch_26
        :pswitch_46
    .end packed-switch
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/bZ;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 385
    if-eqz p1, :cond_25

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->l()I

    move-result v0

    if-lez v0, :cond_25

    .line 386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Lcom/google/googlenav/ui/bi;->bm:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 388
    :cond_25
    return-object p0
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cm;)Ljava/lang/String;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v10, 0x308

    const/16 v9, 0x307

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 418
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->c()Z

    move-result v0

    if-eqz v0, :cond_77

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v0

    .line 421
    :goto_11
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->a()Z

    move-result v3

    .line 422
    if-eqz v3, :cond_8a

    invoke-virtual {p1, v7}, Lcom/google/googlenav/cm;->d(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 429
    :goto_1c
    if-eqz v3, :cond_90

    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_22
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    invoke-static {v1, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 432
    const/16 v4, 0xa

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v5, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 433
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_66

    .line 434
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "..."

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 437
    :cond_66
    if-eqz v3, :cond_95

    .line 438
    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    :goto_76
    return-object v0

    .line 418
    :cond_77
    const/16 v0, 0x4eb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    .line 422
    :cond_8a
    invoke-virtual {p1, v6}, Lcom/google/googlenav/cm;->c(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1c

    .line 429
    :cond_90
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_22

    .line 442
    :cond_95
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_76
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 280
    invoke-direct {p0, p1, p2}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    .line 281
    if-nez v0, :cond_8

    .line 282
    const/4 v0, 0x0

    .line 287
    :goto_7
    return-object v0

    .line 284
    :cond_8
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1a

    .line 285
    iget-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(I)Lam/f;

    move-result-object v0

    goto :goto_7

    .line 287
    :cond_1a
    iget-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(I)Lam/f;

    move-result-object v0

    goto :goto_7
.end method

.method private d()Landroid/view/LayoutInflater;
    .registers 3

    .prologue
    .line 525
    iget-object v0, p0, Lbf/aZ;->e:Landroid/view/LayoutInflater;

    if-nez v0, :cond_14

    .line 526
    iget-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lbf/aZ;->e:Landroid/view/LayoutInflater;

    .line 529
    :cond_14
    iget-object v0, p0, Lbf/aZ;->e:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;)Lbj/F;
    .registers 3
    .parameter

    .prologue
    .line 157
    invoke-virtual {p0}, Lbf/aZ;->c()Lbf/m;

    move-result-object v0

    invoke-virtual {v0}, Lbf/m;->bh()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v0

    if-nez v0, :cond_16

    .line 159
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bB;->a(Lcom/google/googlenav/ai;Z)Lcom/google/googlenav/ui/bB;

    move-result-object v0

    .line 161
    :goto_15
    return-object v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method protected a(Lcom/google/googlenav/ai;Landroid/view/View;)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v10, -0x1

    .line 319
    .line 321
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_87

    .line 322
    :cond_14
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v1

    .line 323
    invoke-direct {p0, p1, v1}, Lbf/aZ;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;

    move-result-object v2

    move-object v3, v2

    move-object v2, v0

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    .line 332
    :goto_25
    new-instance v7, Lbf/ba;

    invoke-direct {v7, p0}, Lbf/ba;-><init>(Lbf/aZ;)V

    .line 347
    if-eqz v3, :cond_57

    .line 348
    invoke-direct {p0, p1, v0}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 349
    const v0, 0x7f10001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    .line 350
    if-eq v4, v10, :cond_51

    .line 351
    new-instance v8, Lcom/google/googlenav/ui/view/a;

    iget-object v9, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v9}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/googlenav/F;->c()I

    move-result v9

    invoke-direct {v8, v4, v9}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v7, v8}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 354
    :cond_51
    if-eq v4, v10, :cond_97

    move v4, v5

    :goto_54
    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/android/BubbleButton;->setEnabled(Z)V

    .line 357
    :cond_57
    if-eqz v2, :cond_83

    .line 358
    invoke-direct {p0, p1, v1}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 359
    const v0, 0x7f10004e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    .line 360
    if-eq v1, v10, :cond_7e

    .line 361
    new-instance v4, Lcom/google/googlenav/ui/view/a;

    iget-object v8, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v8}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/googlenav/F;->c()I

    move-result v8

    invoke-direct {v4, v1, v8}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v7, v4}, Lcom/google/googlenav/ui/android/aF;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aF;

    .line 364
    :cond_7e
    if-eq v1, v10, :cond_99

    :goto_80
    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/android/BubbleButton;->setEnabled(Z)V

    .line 367
    :cond_83
    invoke-virtual {p0, p2, v3, v2}, Lbf/aZ;->a(Landroid/view/View;Lam/f;Lam/f;)V

    .line 368
    return-void

    .line 326
    :cond_87
    sget-object v1, Lcom/google/googlenav/settings/e;->a:Lcom/google/googlenav/settings/e;

    .line 327
    sget-object v0, Lcom/google/googlenav/settings/e;->c:Lcom/google/googlenav/settings/e;

    .line 328
    invoke-direct {p0, p1, v1}, Lbf/aZ;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;

    move-result-object v3

    .line 329
    invoke-direct {p0, p1, v0}, Lbf/aZ;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;

    move-result-object v2

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_25

    :cond_97
    move v4, v6

    .line 354
    goto :goto_54

    :cond_99
    move v5, v6

    .line 364
    goto :goto_80
.end method

.method a(Lcom/google/googlenav/ai;Ljava/util/List;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 397
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    .line 398
    if-nez v0, :cond_8

    .line 414
    :cond_7
    :goto_7
    return-void

    .line 404
    :cond_8
    invoke-virtual {v0, v1}, Lcom/google/googlenav/bZ;->c(Z)Lcom/google/googlenav/cm;

    move-result-object v0

    .line 405
    if-eqz v0, :cond_7

    .line 408
    invoke-virtual {v0, v1}, Lcom/google/googlenav/cm;->c(Z)Ljava/lang/String;

    move-result-object v1

    .line 409
    if-eqz v1, :cond_7

    .line 412
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbf/aZ;->a(Ljava/lang/String;Lcom/google/googlenav/cm;)Ljava/lang/String;

    move-result-object v0

    .line 413
    sget-object v1, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7
.end method

.method public b()Lcom/google/googlenav/ui/view/d;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 79
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    if-nez v2, :cond_10

    .line 130
    :goto_f
    return-object v1

    .line 83
    :cond_10
    invoke-virtual {p0}, Lbf/aZ;->a()Landroid/view/View;

    move-result-object v3

    .line 88
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-eqz v2, :cond_8b

    .line 92
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aI()Z

    move-result v2

    if-nez v2, :cond_7b

    .line 93
    const/16 v2, 0x575

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/ui/aV;->ad:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    .line 100
    :goto_30
    invoke-virtual {p0, v3, v1}, Lbf/aZ;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    .line 103
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 104
    if-eqz v2, :cond_3c

    .line 105
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_3c
    invoke-virtual {p0, v0}, Lbf/aZ;->b(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_57

    .line 110
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v4

    invoke-static {v2, v4}, Lbf/aZ;->a(Ljava/lang/String;Lcom/google/googlenav/bZ;)Ljava/lang/String;

    move-result-object v2

    .line 111
    sget-object v4, Lcom/google/googlenav/ui/aV;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    :cond_57
    invoke-virtual {p0, v3, v1}, Lbf/aZ;->a(Landroid/view/View;Ljava/util/List;)V

    .line 116
    invoke-virtual {p0, v0}, Lbf/aZ;->d(Lcom/google/googlenav/ai;)Ljava/util/List;

    move-result-object v1

    .line 117
    invoke-virtual {p0, v3, v1}, Lbf/aZ;->b(Landroid/view/View;Ljava/util/List;)V

    .line 120
    invoke-virtual {p0, v0}, Lbf/aZ;->e(Lcom/google/googlenav/ai;)Ljava/util/List;

    move-result-object v1

    .line 121
    invoke-virtual {p0, v3, v1}, Lbf/aZ;->c(Landroid/view/View;Ljava/util/List;)V

    .line 124
    invoke-virtual {p0, v0}, Lbf/aZ;->c(Lcom/google/googlenav/ai;)Landroid/view/View;

    move-result-object v1

    .line 125
    invoke-virtual {p0, v3, v1}, Lbf/aZ;->a(Landroid/view/View;Landroid/view/View;)V

    .line 128
    invoke-virtual {p0, v0, v3}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Landroid/view/View;)V

    .line 130
    sget-object v0, Lbf/aZ;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/aZ;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v1

    goto :goto_f

    .line 96
    :cond_7b
    const/16 v2, 0x34

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/googlenav/ui/aV;->ad:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v4}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    goto :goto_30

    :cond_8b
    move-object v2, v1

    goto :goto_30
.end method

.method protected b(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 172
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c(Lcom/google/googlenav/ai;)Landroid/view/View;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 140
    invoke-virtual {p0, p1}, Lbf/aZ;->a(Lcom/google/googlenav/ai;)Lbj/F;

    move-result-object v1

    .line 141
    if-eqz v1, :cond_1c

    .line 142
    invoke-direct {p0}, Lbf/aZ;->d()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-interface {v1}, Lbj/F;->b()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 143
    invoke-interface {v1, v0}, Lbj/F;->a(Landroid/view/View;)Lbj/bB;

    move-result-object v2

    .line 144
    iget-object v3, p0, Lbf/aZ;->c:Lbf/i;

    invoke-interface {v1, v3, v2}, Lbj/F;->a(Lcom/google/googlenav/ui/e;Lbj/bB;)V

    .line 147
    :cond_1c
    return-object v0
.end method

.method protected final c()Lbf/m;
    .registers 2

    .prologue
    .line 231
    iget-object v0, p0, Lbf/aZ;->c:Lbf/i;

    check-cast v0, Lbf/m;

    return-object v0
.end method

.method protected d(Lcom/google/googlenav/ai;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185
    invoke-virtual {p0}, Lbf/aZ;->c()Lbf/m;

    move-result-object v2

    .line 186
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_2c

    move-object v0, p1

    .line 187
    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    move-object v0, p1

    .line 188
    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lbf/aZ;->c:Lbf/i;

    invoke-static {v0, v3}, Lbf/aZ;->a(Ljava/lang/String;Lbf/i;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    :cond_2c
    invoke-virtual {v2}, Lbf/m;->bg()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 193
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 194
    const/16 v0, 0x205

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_47
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->by()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 199
    const/16 v0, 0x5f2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    :cond_5c
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bx()Z

    move-result v0

    if-eqz v0, :cond_71

    .line 204
    const/16 v0, 0x1e2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    :cond_71
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 210
    invoke-virtual {v2}, Lbf/m;->bm()Z

    move-result v0

    invoke-static {p1, v0}, Lbf/aS;->c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_88
    return-object v1
.end method

.method protected e(Lcom/google/googlenav/ai;)Ljava/util/List;
    .registers 5
    .parameter

    .prologue
    .line 219
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 221
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 222
    const/16 v1, 0x3bf

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_1a
    invoke-virtual {p0, p1, v0}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Ljava/util/List;)V

    .line 227
    return-object v0
.end method
