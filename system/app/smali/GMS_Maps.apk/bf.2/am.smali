.class public Lbf/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/layer/l;


# static fields
.field private static final I:I

.field private static final J:I

.field private static final K:I

.field private static final Q:I

.field public static a:I

.field public static b:I

.field protected static final j:I

.field private static final t:[I

.field private static final u:[Z

.field private static v:Z

.field private static volatile w:I


# instance fields
.field private A:Z

.field private B:I

.field private C:Ljava/lang/Object;

.field private D:Ljava/lang/String;

.field private final E:Landroid/graphics/Point;

.field private final F:Lcom/google/googlenav/layer/r;

.field private final G:Lbf/a;

.field private final H:Ljava/util/Map;

.field private final L:Ljava/util/Vector;

.field private M:Z

.field private N:LaN/B;

.field private O:I

.field private P:Lcom/google/googlenav/offers/j;

.field protected final c:Lcom/google/googlenav/ui/s;

.field protected final d:LaH/m;

.field protected final e:LaN/p;

.field protected final f:LaN/u;

.field protected final g:Lcom/google/googlenav/ui/X;

.field protected final h:LaN/k;

.field protected final i:Ljava/util/Vector;

.field private final k:Lcom/google/googlenav/friend/J;

.field private final l:Lcom/google/googlenav/friend/p;

.field private final m:Lcom/google/googlenav/friend/ag;

.field private final n:Lcom/google/googlenav/android/aa;

.field private final o:Lcom/google/googlenav/ui/wizard/jv;

.field private final p:Ljava/util/Vector;

.field private final q:Ljava/util/Vector;

.field private final r:Ljava/util/Vector;

.field private s:Z

.field private x:B

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x5

    .line 114
    sput v2, Lbf/am;->a:I

    .line 122
    const/16 v0, 0x14

    sput v0, Lbf/am;->b:I

    .line 194
    new-array v0, v2, [I

    fill-array-data v0, :array_4a

    sput-object v0, Lbf/am;->t:[I

    .line 203
    sget-object v0, Lbf/am;->t:[I

    array-length v0, v0

    new-array v0, v0, [Z

    sput-object v0, Lbf/am;->u:[Z

    .line 212
    sput-boolean v1, Lbf/am;->v:Z

    .line 245
    sput v1, Lbf/am;->w:I

    .line 295
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/am;->I:I

    .line 301
    sget v0, Lbf/am;->I:I

    sget v1, Lbf/am;->I:I

    mul-int/2addr v0, v1

    sput v0, Lbf/am;->J:I

    .line 319
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/am;->K:I

    .line 326
    sget v0, Lbf/am;->K:I

    sget v1, Lbf/am;->K:I

    mul-int/2addr v0, v1

    sput v0, Lbf/am;->j:I

    .line 366
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lbf/am;->Q:I

    return-void

    .line 194
    nop

    :array_4a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    .line 156
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    .line 167
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    .line 173
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->r:Ljava/util/Vector;

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->s:Z

    .line 252
    const/4 v0, -0x1

    iput-byte v0, p0, Lbf/am;->x:B

    .line 253
    const/16 v0, -0xa

    iput v0, p0, Lbf/am;->y:I

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->z:Z

    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->A:Z

    .line 258
    const/4 v0, -0x1

    iput v0, p0, Lbf/am;->B:I

    .line 271
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lbf/am;->E:Landroid/graphics/Point;

    .line 283
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    .line 334
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->M:Z

    .line 357
    const/4 v0, -0x1

    iput v0, p0, Lbf/am;->O:I

    .line 428
    iput-object p1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    .line 429
    iput-object p2, p0, Lbf/am;->e:LaN/p;

    .line 430
    iput-object p3, p0, Lbf/am;->f:LaN/u;

    .line 431
    iput-object p4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    .line 432
    iput-object p5, p0, Lbf/am;->d:LaH/m;

    .line 433
    iput-object p6, p0, Lbf/am;->k:Lcom/google/googlenav/friend/J;

    .line 434
    iput-object p7, p0, Lbf/am;->l:Lcom/google/googlenav/friend/p;

    .line 435
    iput-object p8, p0, Lbf/am;->m:Lcom/google/googlenav/friend/ag;

    .line 436
    iput-object p9, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    .line 437
    iput-object p10, p0, Lbf/am;->o:Lcom/google/googlenav/ui/wizard/jv;

    .line 438
    iput-object p11, p0, Lbf/am;->h:LaN/k;

    .line 439
    iput-object p12, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    .line 440
    iput-object p13, p0, Lbf/am;->P:Lcom/google/googlenav/offers/j;

    .line 441
    new-instance v0, Lbf/a;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    invoke-direct {v0, v1}, Lbf/a;-><init>(Las/c;)V

    iput-object v0, p0, Lbf/am;->G:Lbf/a;

    .line 442
    return-void
.end method

.method public static W()I
    .registers 3

    .prologue
    const/4 v1, 0x0

    move v0, v1

    .line 4102
    :goto_2
    sget-object v2, Lbf/am;->u:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_11

    .line 4103
    sget-object v2, Lbf/am;->u:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_12

    .line 4104
    sget-object v1, Lbf/am;->t:[I

    aget v1, v1, v0

    .line 4108
    :cond_11
    return v1

    .line 4102
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public static X()I
    .registers 2

    .prologue
    .line 4130
    sget v0, Lbf/am;->w:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbf/am;->w:I

    return v0
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3240
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 3242
    const/4 v1, 0x2

    invoke-virtual {p2}, Lbf/i;->av()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3243
    const/4 v1, 0x3

    invoke-virtual {p2}, Lbf/i;->aO()I

    move-result v2

    invoke-static {v2}, Lcom/google/googlenav/common/util/e;->a(I)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3245
    const/4 v1, 0x4

    invoke-virtual {p2}, Lbf/i;->ay()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3246
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3247
    return-object v0
.end method

.method public static a(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4118
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lbf/am;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_10

    .line 4119
    sget-object v1, Lbf/am;->t:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_11

    .line 4120
    sget-object v1, Lbf/am;->u:[Z

    aput-boolean p1, v1, v0

    .line 4124
    :cond_10
    return-void

    .line 4118
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic a(Lbf/am;)V
    .registers 1
    .parameter

    .prologue
    .line 108
    invoke-direct {p0}, Lbf/am;->ag()V

    return-void
.end method

.method static synthetic a(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->a(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private a(Lbf/i;)V
    .registers 5
    .parameter

    .prologue
    .line 744
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lbf/i;->aC()Z

    move-result v0

    if-nez v0, :cond_d

    .line 765
    :cond_c
    :goto_c
    return-void

    .line 750
    :cond_d
    iget-object v2, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v2

    .line 752
    :try_start_10
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 754
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    sget v1, Lbf/am;->b:I

    if-le v0, v1, :cond_43

    .line 757
    sget v0, Lbf/am;->b:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_25
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3c

    .line 758
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 759
    invoke-direct {p0, v0}, Lbf/am;->c(Lbf/i;)V

    .line 757
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_25

    .line 761
    :cond_3c
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    sget v1, Lbf/am;->b:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 763
    :cond_43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->A:Z

    .line 764
    monitor-exit v2

    goto :goto_c

    :catchall_48
    move-exception v0

    monitor-exit v2
    :try_end_4a
    .catchall {:try_start_10 .. :try_end_4a} :catchall_48

    throw v0
.end method

.method private a(Lbf/i;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x43

    .line 4190
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    packed-switch v0, :pswitch_data_26

    .line 4197
    invoke-static {p1}, Lbf/am;->p(Lbf/i;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 4200
    :goto_14
    invoke-direct {p0}, Lbf/am;->aj()V

    .line 4201
    return-void

    .line 4193
    :pswitch_18
    check-cast p1, Lbf/y;

    invoke-virtual {p1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_14

    .line 4190
    :pswitch_data_26
    .packed-switch 0x6
        :pswitch_18
        :pswitch_18
    .end packed-switch
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 4221
    invoke-static {p1}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 4222
    invoke-direct {p0}, Lbf/am;->aj()V

    .line 4223
    return-void
.end method

.method private a(Ljava/io/DataInput;IZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3516
    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v5, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    invoke-static {}, Lbf/am;->W()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 3518
    invoke-virtual {v0, p1}, Lbf/bk;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 3519
    invoke-virtual {v0, p2}, Lbf/bk;->e(I)V

    .line 3520
    invoke-virtual {v0, p3}, Lbf/bk;->h(Z)V

    .line 3521
    if-eqz p4, :cond_3a

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 3526
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->f()Z

    move-result v1

    .line 3527
    const/4 v2, 0x0

    .line 3528
    invoke-virtual {p0, v0, v1, v2}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3536
    :goto_39
    return-void

    .line 3530
    :cond_3a
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_39

    .line 3533
    :cond_3e
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_39
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3194
    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 3195
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDataSize()I

    move-result v1

    if-lez v1, :cond_17

    .line 3196
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    .line 3200
    :goto_15
    const/4 v0, 0x1

    .line 3211
    :goto_16
    return v0

    .line 3198
    :cond_17
    invoke-interface {v0, p2}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1a} :catch_1b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_1a} :catch_26

    goto :goto_15

    .line 3201
    :catch_1b
    move-exception v0

    .line 3202
    const-string v1, "LAYER_MANAGER-LayerManager Error saving layers"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3205
    invoke-direct {p0}, Lbf/am;->ag()V

    .line 3211
    :goto_24
    const/4 v0, 0x0

    goto :goto_16

    .line 3206
    :catch_26
    move-exception v0

    .line 3207
    const-string v1, "LAYER_MANAGER-LayerManager OOME saving layers"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3209
    invoke-direct {p0}, Lbf/am;->ag()V

    goto :goto_24
.end method

.method private ab()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1032
    const/16 v0, 0xd

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1033
    const/16 v0, 0x12

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1034
    const/16 v0, 0xf

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1035
    return-void
.end method

.method private ac()Lcom/google/googlenav/ui/ak;
    .registers 2

    .prologue
    .line 1242
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    return-object v0
.end method

.method private ad()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 2236
    move v1, v2

    :goto_2
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 2237
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    .line 2238
    invoke-virtual {v0, v2}, Lbf/ai;->a(Z)V

    .line 2236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2240
    :cond_19
    return-void
.end method

.method private ae()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    .line 3105
    new-instance v3, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 3109
    const/4 v1, 0x0

    .line 3114
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 3115
    iget-object v4, p0, Lbf/am;->i:Ljava/util/Vector;

    monitor-enter v4

    .line 3116
    const/4 v0, 0x0

    :goto_11
    :try_start_11
    iget-object v5, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v0, v5, :cond_25

    .line 3117
    iget-object v5, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3116
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 3119
    :cond_25
    monitor-exit v4
    :try_end_26
    .catchall {:try_start_11 .. :try_end_26} :catchall_50

    .line 3121
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 3122
    if-eqz v0, :cond_97

    invoke-virtual {v0}, Lbf/i;->aM()Z

    move-result v4

    if-eqz v4, :cond_97

    .line 3123
    invoke-direct {p0, v3, v0, v6}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v3, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 3126
    iget-object v1, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v1, :cond_4e

    .line 3127
    iget-object v1, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v1, v0}, Lcom/google/googlenav/layer/r;->a(Lbf/i;)V

    :cond_4e
    :goto_4e
    move-object v1, v0

    goto :goto_2a

    .line 3119
    :catchall_50
    move-exception v0

    :try_start_51
    monitor-exit v4
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_50

    throw v0

    .line 3133
    :cond_53
    const/4 v2, -0x1

    .line 3134
    const/16 v0, -0xa

    .line 3135
    if-eqz v1, :cond_95

    .line 3136
    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v2

    .line 3137
    const/16 v0, 0xa

    invoke-virtual {v3, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3139
    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    .line 3140
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3142
    const/16 v4, 0xe

    invoke-virtual {v1}, Lbf/i;->aA()I

    move-result v1

    invoke-virtual {v3, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move v1, v2

    .line 3147
    :goto_7c
    iget-boolean v2, p0, Lbf/am;->z:Z

    if-eqz v2, :cond_88

    iget-byte v2, p0, Lbf/am;->x:B

    if-ne v1, v2, :cond_88

    iget v2, p0, Lbf/am;->y:I

    if-eq v0, v2, :cond_94

    .line 3149
    :cond_88
    const-string v2, "PROTO_SAVED_LAYER_STATE"

    invoke-direct {p0, v3, v2}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lbf/am;->z:Z

    .line 3150
    iput-byte v1, p0, Lbf/am;->x:B

    .line 3151
    iput v0, p0, Lbf/am;->y:I

    .line 3153
    :cond_94
    return-void

    :cond_95
    move v1, v2

    goto :goto_7c

    :cond_97
    move-object v0, v1

    goto :goto_4e
.end method

.method private af()V
    .registers 7

    .prologue
    .line 3159
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 3164
    iget-object v3, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v3

    .line 3166
    :try_start_a
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_13
    if-ltz v1, :cond_39

    .line 3167
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 3168
    invoke-virtual {v0}, Lbf/i;->aM()Z

    move-result v4

    if-eqz v4, :cond_35

    .line 3169
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v2, v0, v5}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/i;Z)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 3171
    iget-object v4, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v4, :cond_35

    .line 3172
    iget-object v4, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v4, v0}, Lcom/google/googlenav/layer/r;->a(Lbf/i;)V

    .line 3166
    :cond_35
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_13

    .line 3176
    :cond_39
    monitor-exit v3
    :try_end_3a
    .catchall {:try_start_a .. :try_end_3a} :catchall_47

    .line 3179
    iget-boolean v0, p0, Lbf/am;->A:Z

    if-nez v0, :cond_46

    .line 3180
    const-string v0, "PROTO_SAVED_RECENT_LAYERS"

    invoke-direct {p0, v2, v0}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbf/am;->A:Z

    .line 3182
    :cond_46
    return-void

    .line 3176
    :catchall_47
    move-exception v0

    :try_start_48
    monitor-exit v3
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    throw v0
.end method

.method private ag()V
    .registers 3

    .prologue
    .line 3672
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 3675
    const-string v1, "PROTO_SAVED_LAYER_STATE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3676
    const-string v1, "PROTO_SAVED_RECENT_LAYERS"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3677
    const-string v1, "LAYER_"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    .line 3678
    return-void
.end method

.method private ah()V
    .registers 3

    .prologue
    .line 3684
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 3685
    const-string v1, "SAVED_SEARCH_1_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3686
    const-string v1, "SAVED_SEARCH_1"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3687
    const-string v1, "PROTO_SAVED_SEARCH_DB"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3688
    const-string v1, "SAVED_SEARCH_INFO"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    .line 3692
    invoke-static {v0}, Lbf/a;->a(Lcom/google/googlenav/common/io/j;)V

    .line 3693
    return-void
.end method

.method private ai()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 4039
    move v2, v3

    :goto_2
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_2c

    .line 4040
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 4041
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    if-nez v1, :cond_28

    move-object v1, v0

    .line 4042
    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 4043
    invoke-virtual {v0, v3}, Lbf/i;->h(Z)V

    .line 4039
    :cond_28
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 4047
    :cond_2c
    return-void
.end method

.method private aj()V
    .registers 4

    .prologue
    .line 4229
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 4230
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_22

    .line 4231
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-static {v0}, Lbf/am;->p(Lbf/i;)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4230
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 4233
    :cond_22
    const/16 v0, 0x43

    const-string v1, "v"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 4235
    return-void
.end method

.method private ak()V
    .registers 4

    .prologue
    .line 4241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 4242
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_22

    .line 4243
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-static {v0}, Lbf/am;->p(Lbf/i;)C

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 4242
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 4245
    :cond_22
    const/16 v0, 0x43

    const-string v1, "r"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 4247
    return-void
.end method

.method public static b(I)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    .line 1961
    packed-switch p0, :pswitch_data_3a

    .line 1998
    :pswitch_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown layer of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1e
    move v0, v1

    .line 1995
    :goto_1f
    :pswitch_1f
    return v0

    :pswitch_20
    move v0, v1

    .line 1967
    goto :goto_1f

    :pswitch_22
    move v0, v1

    .line 1969
    goto :goto_1f

    :pswitch_24
    move v0, v1

    .line 1971
    goto :goto_1f

    :pswitch_26
    move v0, v1

    .line 1973
    goto :goto_1f

    :pswitch_28
    move v0, v1

    .line 1977
    goto :goto_1f

    :pswitch_2a
    move v0, v1

    .line 1981
    goto :goto_1f

    :pswitch_2c
    move v0, v1

    .line 1983
    goto :goto_1f

    :pswitch_2e
    move v0, v1

    .line 1985
    goto :goto_1f

    :pswitch_30
    move v0, v1

    .line 1987
    goto :goto_1f

    :pswitch_32
    move v0, v1

    .line 1989
    goto :goto_1f

    :pswitch_34
    move v0, v1

    .line 1991
    goto :goto_1f

    :pswitch_36
    move v0, v1

    .line 1993
    goto :goto_1f

    :pswitch_38
    move v0, v1

    .line 1995
    goto :goto_1f

    .line 1961
    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_1f
        :pswitch_1e
        :pswitch_22
        :pswitch_24
        :pswitch_5
        :pswitch_26
        :pswitch_1f
        :pswitch_28
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1f
        :pswitch_5
        :pswitch_2a
        :pswitch_5
        :pswitch_2c
        :pswitch_2e
        :pswitch_5
        :pswitch_30
        :pswitch_32
        :pswitch_5
        :pswitch_20
        :pswitch_34
        :pswitch_36
        :pswitch_5
        :pswitch_5
        :pswitch_38
    .end packed-switch
.end method

.method private b(IZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 962
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 963
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2f

    .line 964
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 966
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v3

    if-ne v3, p1, :cond_2b

    invoke-virtual {v0}, Lbf/i;->ay()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 967
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 963
    :cond_2b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 970
    :cond_2f
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_33
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 971
    const/4 v2, 0x1

    invoke-virtual {p0, v0, p2, v2}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_33

    .line 973
    :cond_44
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 974
    return-void
.end method

.method static synthetic b(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->b(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private b(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 929
    new-instance v0, Lbf/an;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lbf/an;-><init>(Lbf/am;Las/c;Lbf/i;)V

    invoke-virtual {v0}, Lbf/an;->g()V

    .line 935
    return-void
.end method

.method private b(Lbf/i;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2321
    invoke-virtual {p1}, Lbf/i;->ah()Z

    move-result v0

    .line 2324
    invoke-virtual {p1, p2}, Lbf/i;->b(I)V

    .line 2326
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 2329
    invoke-virtual {p1}, Lbf/i;->n()V

    .line 2334
    if-eqz v0, :cond_20

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_20

    .line 2335
    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    .line 2347
    :cond_20
    :goto_20
    return-void

    .line 2341
    :cond_21
    invoke-virtual {p1}, Lbf/i;->al()V

    .line 2342
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lbf/i;->b(B)V

    .line 2345
    invoke-virtual {p0, p1}, Lbf/am;->e(Lbf/i;)V

    goto :goto_20
.end method

.method private b(Ljava/io/DataInput;IZZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 3540
    new-instance v0, Lbf/aJ;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v5, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    iget-object v7, p0, Lbf/am;->P:Lcom/google/googlenav/offers/j;

    invoke-direct/range {v0 .. v7}, Lbf/aJ;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V

    .line 3542
    invoke-virtual {v0, p1}, Lbf/aJ;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 3543
    invoke-virtual {v0, p2}, Lbf/aJ;->e(I)V

    .line 3544
    invoke-virtual {v0, p3}, Lbf/aJ;->h(Z)V

    .line 3545
    if-eqz p4, :cond_30

    invoke-virtual {v0}, Lbf/aJ;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_30

    .line 3546
    invoke-virtual {p0, v0, v8, v8}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3554
    :goto_2f
    return-void

    .line 3548
    :cond_30
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_2f

    .line 3551
    :cond_34
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_2f
.end method

.method static synthetic c(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->g(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private c(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 943
    new-instance v0, Lbf/ao;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lbf/ao;-><init>(Lbf/am;Las/c;Lbf/i;)V

    invoke-virtual {v0}, Lbf/ao;->g()V

    .line 950
    return-void
.end method

.method private c(Lbf/i;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2360
    invoke-virtual {p1, p2}, Lbf/i;->b(I)V

    .line 2361
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2363
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 2367
    invoke-virtual {p1}, Lbf/i;->ae()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2371
    invoke-virtual {p1}, Lbf/i;->l()V

    .line 2400
    :goto_16
    return-void

    .line 2372
    :cond_17
    invoke-virtual {p1}, Lbf/i;->af()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 2373
    invoke-virtual {p1}, Lbf/i;->m()V

    goto :goto_16

    .line 2375
    :cond_21
    invoke-virtual {p1}, Lbf/i;->n()V

    goto :goto_16

    .line 2381
    :cond_25
    invoke-virtual {p1}, Lbf/i;->al()V

    .line 2385
    if-eqz v0, :cond_41

    invoke-virtual {v0}, Lbf/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_41

    .line 2388
    invoke-virtual {p0, p1}, Lbf/am;->e(Lbf/i;)V

    .line 2389
    invoke-virtual {p1}, Lbf/i;->ad()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 2390
    invoke-virtual {p1}, Lbf/i;->m()V

    goto :goto_16

    .line 2392
    :cond_3d
    invoke-virtual {p1}, Lbf/i;->l()V

    goto :goto_16

    .line 2397
    :cond_41
    invoke-virtual {p0, p1}, Lbf/am;->e(Lbf/i;)V

    goto :goto_16
.end method

.method private c(Lcom/google/googlenav/aZ;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2855
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 2857
    invoke-virtual {p0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    .line 2861
    if-nez v0, :cond_2c

    sget-boolean v1, Lbf/am;->v:Z

    if-eqz v1, :cond_2c

    .line 2862
    invoke-virtual {p0, p1}, Lbf/am;->e(Lcom/google/googlenav/aZ;)Lbf/X;

    move-result-object v0

    .line 2873
    :cond_1e
    :goto_1e
    invoke-virtual {p0, p1}, Lbf/am;->f(Lcom/google/googlenav/aZ;)V

    .line 2878
    sget-boolean v1, Lbf/am;->v:Z

    if-eqz v1, :cond_2b

    if-nez p2, :cond_2b

    .line 2879
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lbf/X;->a(ZLcom/google/googlenav/aZ;)V

    .line 2882
    :cond_2b
    return-void

    .line 2866
    :cond_2c
    if-eqz v0, :cond_1e

    .line 2867
    invoke-virtual {p0, v0}, Lbf/am;->a(Lbf/X;)V

    goto :goto_1e
.end method

.method private c(Ljava/io/DataInput;IZZ)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 3558
    new-instance v0, Lbf/O;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->d:LaH/m;

    invoke-direct/range {v0 .. v5}, Lbf/O;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;)V

    .line 3560
    invoke-virtual {v0, p1}, Lbf/O;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 3561
    invoke-virtual {v0, p2}, Lbf/O;->e(I)V

    .line 3562
    invoke-virtual {v0, p3}, Lbf/O;->h(Z)V

    .line 3563
    if-eqz p4, :cond_26

    .line 3564
    invoke-virtual {p0, v0, v6, v6}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3565
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/O;->b(B)V

    .line 3573
    :goto_25
    return-void

    .line 3567
    :cond_26
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_25

    .line 3570
    :cond_2a
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_25
.end method

.method private c(LaN/B;)Z
    .registers 11
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 1316
    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    .line 1317
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v2

    .line 1318
    if-eqz v2, :cond_1e

    .line 1319
    invoke-static {}, Lcom/google/googlenav/ui/bi;->R()[Lam/f;

    move-result-object v0

    aget-object v0, v0, v6

    .line 1323
    if-nez v0, :cond_1f

    move v4, v6

    .line 1324
    :goto_14
    const/4 v3, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    move v7, v6

    invoke-virtual/range {v0 .. v8}, Lbf/am;->a(LaN/B;LaN/B;IIIIILam/e;)Z

    move-result v6

    .line 1327
    :cond_1e
    return v6

    .line 1323
    :cond_1f
    invoke-interface {v0}, Lam/f;->a()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    goto :goto_14
.end method

.method static synthetic d(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->f(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private d(Ljava/io/DataInput;IZZ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 3577
    new-instance v0, Lbf/bK;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v0, v1, v2, v3, v4}, Lbf/bK;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    .line 3579
    invoke-virtual {v0, p1}, Lbf/bK;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 3580
    invoke-virtual {v0, p2}, Lbf/bK;->e(I)V

    .line 3581
    invoke-virtual {v0, p3}, Lbf/bK;->h(Z)V

    .line 3582
    if-eqz p4, :cond_24

    .line 3583
    invoke-virtual {p0, v0, v5, v5}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3584
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/bK;->b(B)V

    .line 3592
    :goto_23
    return-void

    .line 3586
    :cond_24
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_23

    .line 3589
    :cond_28
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_23
.end method

.method private e(I)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1061
    move v1, v0

    move v2, v0

    .line 1062
    :goto_3
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1f

    .line 1063
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    if-ne v0, p1, :cond_1b

    .line 1064
    add-int/lit8 v2, v2, 0x1

    .line 1062
    :cond_1b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1067
    :cond_1f
    return v2
.end method

.method static synthetic e(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->c(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private e(Ljava/io/DataInput;IZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 3596
    new-instance v0, Lbf/bU;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    new-instance v6, Lcom/google/googlenav/Y;

    invoke-direct {v6}, Lcom/google/googlenav/Y;-><init>()V

    invoke-direct/range {v0 .. v7}, Lbf/bU;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/F;Z)V

    .line 3598
    invoke-virtual {v0, p1}, Lbf/bU;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 3599
    invoke-virtual {v0, p2}, Lbf/bU;->e(I)V

    .line 3600
    invoke-virtual {v0, p3}, Lbf/bU;->h(Z)V

    .line 3601
    if-eqz p4, :cond_27

    .line 3602
    invoke-virtual {p0, v0, v7, v7}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3610
    :goto_26
    return-void

    .line 3604
    :cond_27
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_26

    .line 3607
    :cond_2b
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_26
.end method

.method private f(I)Lbf/i;
    .registers 5
    .parameter

    .prologue
    .line 1834
    packed-switch p1, :pswitch_data_4a

    .line 1859
    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Layer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has MAX_ALLOWED_INSTANCES > 1."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1849
    :pswitch_22
    const/4 v0, 0x0

    move v1, v0

    :goto_24
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_47

    .line 1850
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    if-ne v0, p1, :cond_43

    .line 1851
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 1862
    :goto_42
    return-object v0

    .line 1849
    :cond_43
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_24

    .line 1862
    :cond_47
    const/4 v0, 0x0

    goto :goto_42

    .line 1834
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_22
        :pswitch_3
        :pswitch_3
        :pswitch_22
        :pswitch_3
        :pswitch_22
        :pswitch_22
        :pswitch_22
        :pswitch_3
        :pswitch_3
        :pswitch_22
    .end packed-switch
.end method

.method static synthetic f(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->d(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private f(Ljava/io/DataInput;IZZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 3614
    new-instance v0, Lbf/y;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v5}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;)V

    .line 3616
    invoke-virtual {v0, p1}, Lbf/y;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_61

    .line 3620
    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    .line 3621
    const-string v2, "msid:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 3622
    new-instance v7, Lbf/bD;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v5

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lbf/bD;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    move-object v0, v7

    .line 3629
    :cond_3b
    :goto_3b
    invoke-virtual {v0, p2}, Lbf/y;->e(I)V

    .line 3630
    invoke-virtual {v0, p3}, Lbf/y;->h(Z)V

    .line 3631
    if-eqz p4, :cond_5d

    .line 3632
    invoke-virtual {p0, v0, v8, v8}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3640
    :goto_46
    return-void

    .line 3624
    :cond_47
    const-string v2, "LayerTransit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3b

    .line 3625
    invoke-virtual {v0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 3626
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/m;->a(Ljava/lang/String;)V

    .line 3627
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v0

    goto :goto_3b

    .line 3634
    :cond_5d
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_46

    .line 3637
    :cond_61
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_46
.end method

.method private static g(I)C
    .registers 2
    .parameter

    .prologue
    .line 4257
    add-int/lit8 v0, p0, 0x61

    int-to-char v0, v0

    return v0
.end method

.method private g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;
    .registers 8
    .parameter

    .prologue
    .line 1211
    new-instance v0, Lcom/google/googlenav/n;

    new-instance v1, Lcom/google/googlenav/T;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v2

    iget-object v3, p0, Lbf/am;->h:LaN/k;

    iget-object v4, p0, Lbf/am;->e:LaN/p;

    iget-object v5, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    return-object v0
.end method

.method static synthetic g(Lbf/am;Ljava/io/DataInput;IZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/am;->e(Ljava/io/DataInput;IZZ)V

    return-void
.end method

.method private g(Ljava/io/DataInput;IZZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x1

    .line 3644
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-nez v0, :cond_c

    .line 3666
    :goto_b
    return-void

    .line 3650
    :cond_c
    new-instance v0, Lbf/X;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    iget-object v5, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->k:Lcom/google/googlenav/friend/J;

    iget-object v7, p0, Lbf/am;->l:Lcom/google/googlenav/friend/p;

    iget-object v8, p0, Lbf/am;->m:Lcom/google/googlenav/friend/ag;

    iget-object v9, p0, Lbf/am;->G:Lbf/a;

    invoke-direct/range {v0 .. v9}, Lbf/X;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lbf/a;)V

    .line 3653
    invoke-virtual {v0, p1}, Lbf/X;->a(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 3654
    invoke-virtual {v0, p2}, Lbf/X;->e(I)V

    .line 3655
    invoke-virtual {v0, p3}, Lbf/X;->h(Z)V

    .line 3656
    if-eqz p4, :cond_38

    .line 3657
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v10, v1}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 3658
    sput-boolean v10, Lbf/am;->v:Z

    goto :goto_b

    .line 3660
    :cond_38
    invoke-direct {p0, v0}, Lbf/am;->a(Lbf/i;)V

    goto :goto_b

    .line 3663
    :cond_3c
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LAYER_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    goto :goto_b
.end method

.method private g(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1733
    invoke-virtual {p0, p1}, Lbf/am;->b(Ljava/lang/String;)Lbf/y;

    move-result-object v0

    .line 1734
    if-eqz v0, :cond_9

    .line 1735
    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    .line 1737
    :cond_9
    return-void
.end method

.method private g(Z)V
    .registers 5
    .parameter

    .prologue
    .line 2105
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2106
    const/4 v0, 0x0

    :goto_5
    iget-object v2, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_19

    .line 2107
    iget-object v2, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2106
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 2109
    :cond_19
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2110
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, p1}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_1d

    .line 2112
    :cond_2e
    return-void
.end method

.method private h(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 2836
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aJ;

    .line 2837
    if-nez v0, :cond_17

    .line 2838
    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v0

    .line 2842
    :cond_e
    :goto_e
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/aJ;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2843
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/am;->e(Z)V

    .line 2844
    return-void

    .line 2839
    :cond_17
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v1

    if-nez v1, :cond_e

    .line 2840
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/aJ;->c(Lcom/google/googlenav/F;)V

    goto :goto_e
.end method

.method private h(Ljava/lang/String;)V
    .registers 15
    .parameter

    .prologue
    .line 3329
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v9

    .line 3330
    invoke-interface {v9, p1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v0

    .line 3331
    if-eqz v0, :cond_11

    array-length v1, v0

    if-nez v1, :cond_12

    .line 3512
    :cond_11
    :goto_11
    return-void

    .line 3336
    :cond_12
    invoke-virtual {p0, p1}, Lbf/am;->c(Ljava/lang/String;)Z

    move-result v3

    .line 3338
    :try_start_16
    new-instance v10, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->o:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v10, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 3339
    invoke-virtual {v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 3341
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v11

    .line 3345
    const/4 v0, 0x0

    move v8, v0

    :goto_27
    if-ge v8, v11, :cond_61

    .line 3346
    const/4 v0, 0x1

    invoke-virtual {v10, v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 3348
    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v2

    .line 3350
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/common/util/e;->a([B)I

    move-result v5

    .line 3355
    sget v1, Lbf/am;->w:I

    if-lt v5, v1, :cond_45

    .line 3356
    add-int/lit8 v1, v5, 0x1

    sput v1, Lbf/am;->w:I

    .line 3359
    :cond_45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LAYER_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3360
    invoke-static {v9, v1}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v4

    .line 3361
    if-nez v4, :cond_7b

    .line 3366
    invoke-direct {p0}, Lbf/am;->ag()V

    .line 3451
    :cond_61
    iget-object v0, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/as;

    move-object v2, p0

    move-object v4, p1

    move-object v5, v10

    move v6, v11

    invoke-direct/range {v1 .. v6}, Lbf/as;-><init>(Lbf/am;ZLjava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_70} :catch_71

    goto :goto_11

    .line 3497
    :catch_71
    move-exception v0

    .line 3508
    const-string v1, "LAYER_MANAGER-LayersManager load"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3510
    invoke-direct {p0}, Lbf/am;->ag()V

    goto :goto_11

    .line 3371
    :cond_7b
    const/4 v1, 0x4

    :try_start_7c
    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v6

    .line 3375
    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v7

    .line 3385
    if-eqz v7, :cond_95

    invoke-direct {p0, v2}, Lbf/am;->e(I)I

    move-result v0

    invoke-static {v2}, Lbf/am;->b(I)I

    move-result v1

    if-lt v0, v1, :cond_95

    .line 3345
    :goto_91
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_27

    .line 3393
    :cond_95
    iget-object v12, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v0, Lbf/ar;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lbf/ar;-><init>(Lbf/am;IZLjava/io/DataInput;IZZ)V

    const/4 v1, 0x1

    invoke-virtual {v12, v0, v1}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_a1
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_a1} :catch_71

    goto :goto_91
.end method

.method private i(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 4209
    const/16 v0, 0x43

    const/16 v1, 0xd

    invoke-static {v1}, Lbf/am;->g(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method private i(Lcom/google/googlenav/aZ;)V
    .registers 4
    .parameter

    .prologue
    .line 3040
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v1

    .line 3041
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 3042
    if-eqz v0, :cond_10

    .line 3043
    invoke-interface {v1, v0, p1}, Lcom/google/googlenav/ba;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/aZ;)V

    .line 3045
    :cond_10
    return-void
.end method

.method private j(Lcom/google/googlenav/aZ;)V
    .registers 10
    .parameter

    .prologue
    .line 3052
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 3053
    new-instance v0, Lbf/cg;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    invoke-static {}, Lbf/am;->W()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lbf/cg;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;LaN/k;I)V

    .line 3057
    invoke-virtual {v0}, Lbf/cg;->av()I

    move-result v1

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    .line 3060
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;Z)V

    .line 3061
    invoke-virtual {v0}, Lbf/cg;->l()V

    .line 3062
    return-void
.end method

.method public static m(Lbf/i;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 3098
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LAYER_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/i;->aO()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o(Lbf/i;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1013
    iget-object v2, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v2

    .line 1014
    :try_start_4
    iget-object v1, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    move v1, v0

    :goto_b
    if-ge v1, v3, :cond_2c

    .line 1015
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 1016
    invoke-virtual {v0, p1}, Lbf/i;->a(Lbf/i;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 1017
    iget-object v1, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1018
    invoke-direct {p0, v0}, Lbf/am;->c(Lbf/i;)V

    .line 1019
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->A:Z

    .line 1020
    monitor-exit v2

    .line 1024
    :goto_27
    return-void

    .line 1014
    :cond_28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1023
    :cond_2c
    monitor-exit v2

    goto :goto_27

    :catchall_2e
    move-exception v0

    monitor-exit v2
    :try_end_30
    .catchall {:try_start_4 .. :try_end_30} :catchall_2e

    throw v0
.end method

.method private static p(Lbf/i;)C
    .registers 4
    .parameter

    .prologue
    .line 4262
    invoke-virtual {p0}, Lbf/i;->av()I

    move-result v0

    .line 4263
    const/4 v1, 0x6

    if-ne v0, v1, :cond_27

    .line 4264
    check-cast p0, Lbf/y;

    invoke-virtual {p0}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    .line 4265
    const-string v2, "LayerTransit"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 4266
    const/16 v0, 0x54

    .line 4271
    :goto_1b
    return v0

    .line 4267
    :cond_1c
    const-string v2, "LayerWikipedia"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 4268
    const/16 v0, 0x57

    goto :goto_1b

    .line 4271
    :cond_27
    invoke-static {v0}, Lbf/am;->g(I)C

    move-result v0

    goto :goto_1b
.end method


# virtual methods
.method public A()Lbf/bx;
    .registers 2

    .prologue
    .line 1928
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bx;

    return-object v0
.end method

.method public B()Lbf/X;
    .registers 2

    .prologue
    .line 1937
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/X;

    return-object v0
.end method

.method public C()Lbf/aJ;
    .registers 2

    .prologue
    .line 1951
    const/16 v0, 0x1a

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aJ;

    return-object v0
.end method

.method protected D()I
    .registers 12

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 2251
    iget-object v1, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_c

    .line 2288
    :goto_b
    return v5

    .line 2256
    :cond_c
    iget-object v1, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ne v1, v8, :cond_16

    move v5, v0

    .line 2257
    goto :goto_b

    .line 2262
    :cond_16
    const-wide v1, 0x7fffffffffffffffL

    move v4, v5

    move-wide v9, v1

    move-wide v2, v9

    move v1, v0

    .line 2263
    :goto_1f
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_47

    .line 2264
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    .line 2265
    invoke-virtual {v0}, Lbf/ai;->e()Z

    move-result v6

    if-eqz v6, :cond_39

    .line 2263
    :cond_35
    :goto_35
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1f

    .line 2268
    :cond_39
    invoke-virtual {v0}, Lbf/ai;->d()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-gez v6, :cond_35

    .line 2270
    invoke-virtual {v0}, Lbf/ai;->d()J

    move-result-wide v2

    move v4, v1

    goto :goto_35

    .line 2276
    :cond_47
    if-ne v4, v5, :cond_62

    .line 2277
    invoke-direct {p0}, Lbf/am;->ad()V

    .line 2281
    iget v0, p0, Lbf/am;->O:I

    if-eq v0, v5, :cond_5d

    .line 2282
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    iget v1, p0, Lbf/am;->O:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    invoke-virtual {v0, v8}, Lbf/ai;->a(Z)V

    .line 2285
    :cond_5d
    invoke-virtual {p0}, Lbf/am;->D()I

    move-result v5

    goto :goto_b

    :cond_62
    move v5, v4

    .line 2288
    goto :goto_b
.end method

.method public E()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 2295
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2301
    if-eqz v0, :cond_2b

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v2

    if-nez v2, :cond_2b

    .line 2302
    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2304
    invoke-static {v0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    move-object v0, v1

    .line 2308
    :cond_22
    :goto_22
    iget-object v2, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    const-string v3, "22"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v1, v4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2310
    return-void

    :cond_2b
    move-object v0, v1

    goto :goto_22
.end method

.method public F()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 2659
    move v1, v0

    move v2, v0

    .line 2660
    :goto_3
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1c

    .line 2661
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->X()Z

    move-result v0

    or-int/2addr v2, v0

    .line 2660
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2663
    :cond_1c
    if-eqz v2, :cond_22

    .line 2664
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/am;->d(Z)V

    .line 2666
    :cond_22
    return v2
.end method

.method public G()V
    .registers 1

    .prologue
    .line 2675
    return-void
.end method

.method public H()Lbf/i;
    .registers 2

    .prologue
    .line 2758
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    goto :goto_9
.end method

.method public I()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 2762
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    return-object v0
.end method

.method public J()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 2766
    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    return-object v0
.end method

.method public K()Ljava/util/Vector;
    .registers 2

    .prologue
    .line 2770
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    return-object v0
.end method

.method public L()V
    .registers 2

    .prologue
    .line 3221
    iget-object v0, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    if-eqz v0, :cond_9

    .line 3222
    iget-object v0, p0, Lbf/am;->F:Lcom/google/googlenav/layer/r;

    invoke-interface {v0}, Lcom/google/googlenav/layer/r;->a()V

    .line 3228
    :cond_9
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 3230
    invoke-direct {p0}, Lbf/am;->ae()V

    .line 3231
    invoke-direct {p0}, Lbf/am;->af()V

    .line 3233
    :cond_17
    return-void
.end method

.method public M()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 3254
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->L()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3255
    invoke-direct {p0}, Lbf/am;->ah()V

    .line 3263
    :cond_e
    sget-object v0, LaE/g;->a:LaE/g;

    invoke-virtual {v0}, LaE/g;->e()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 3264
    iget-object v0, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/ap;

    invoke-direct {v1, p0}, Lbf/ap;-><init>(Lbf/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 3273
    :cond_20
    const-string v0, "PROTO_SAVED_LAYER_STATE"

    invoke-direct {p0, v0}, Lbf/am;->h(Ljava/lang/String;)V

    .line 3279
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 3282
    iget-object v0, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    new-instance v1, Lbf/aq;

    invoke-direct {v1, p0}, Lbf/aq;-><init>(Lbf/am;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 3288
    return-void
.end method

.method public N()V
    .registers 2

    .prologue
    .line 3296
    const-string v0, "PROTO_SAVED_RECENT_LAYERS"

    invoke-direct {p0, v0}, Lbf/am;->h(Ljava/lang/String;)V

    .line 3297
    invoke-direct {p0}, Lbf/am;->ak()V

    .line 3298
    return-void
.end method

.method public O()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 3700
    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aA;->a()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 3706
    :cond_11
    :goto_11
    return v0

    :cond_12
    iget-object v1, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gtz v1, :cond_11

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->b()I

    move-result v1

    if-nez v1, :cond_11

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->c()Z

    move-result v1

    if-nez v1, :cond_11

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_3a

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->ah()Z

    move-result v1

    if-eqz v1, :cond_11

    :cond_3a
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_4c

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v1

    const/16 v2, 0x16

    if-eq v1, v2, :cond_11

    :cond_4c
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public P()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3722
    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ne v0, v1, :cond_24

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_24

    move v0, v1

    .line 3724
    :goto_1a
    if-nez v0, :cond_26

    invoke-virtual {p0}, Lbf/am;->O()Z

    move-result v0

    if-eqz v0, :cond_26

    move v0, v1

    :goto_23
    return v0

    :cond_24
    move v0, v2

    .line 3722
    goto :goto_1a

    :cond_26
    move v0, v2

    .line 3724
    goto :goto_23
.end method

.method public Q()V
    .registers 2

    .prologue
    .line 3923
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/am;->D:Ljava/lang/String;

    .line 3925
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-nez v0, :cond_a

    .line 3943
    :goto_9
    return-void

    .line 3933
    :cond_a
    invoke-virtual {p0}, Lbf/am;->A()Lbf/bx;

    move-result-object v0

    .line 3934
    if-eqz v0, :cond_13

    .line 3935
    invoke-virtual {v0}, Lbf/bx;->Y()V

    .line 3939
    :cond_13
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 3940
    invoke-virtual {v0}, Lbf/i;->Y()V

    .line 3942
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->V()V

    goto :goto_9
.end method

.method public R()Z
    .registers 2

    .prologue
    .line 3949
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 3950
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lbf/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public S()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 3997
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 4000
    invoke-direct {p0, v5, v2}, Lbf/am;->b(IZ)V

    .line 4003
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v2}, Lbf/am;->b(IZ)V

    .line 4006
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 4007
    :goto_12
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3d

    .line 4008
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 4009
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    if-nez v4, :cond_39

    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bl()Z

    move-result v0

    if-nez v0, :cond_39

    .line 4010
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4007
    :cond_39
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_12

    .line 4013
    :cond_3d
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_41
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 4014
    invoke-virtual {p0, v0, v2, v5}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_41

    .line 4016
    :cond_51
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 4019
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->e(Lbf/i;)V

    .line 4020
    return-void
.end method

.method protected T()V
    .registers 2

    .prologue
    .line 4024
    const/4 v0, 0x1

    .line 4025
    invoke-virtual {p0, v0}, Lbf/am;->a(Z)Lbf/by;

    .line 4026
    invoke-virtual {p0}, Lbf/am;->s()V

    .line 4027
    invoke-virtual {p0}, Lbf/am;->t()Lbf/bx;

    .line 4029
    sget-object v0, LaE/g;->a:LaE/g;

    invoke-virtual {v0}, LaE/g;->e()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 4030
    invoke-virtual {p0}, Lbf/am;->m()Lbf/aA;

    .line 4032
    :cond_15
    return-void
.end method

.method public U()V
    .registers 3

    .prologue
    .line 4051
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 4052
    if-eqz v0, :cond_26

    .line 4053
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 4059
    invoke-virtual {v0}, Lbf/i;->al()V

    .line 4065
    :goto_13
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_26

    invoke-virtual {p0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 4067
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->H()V

    .line 4070
    :cond_26
    return-void

    .line 4061
    :cond_27
    invoke-virtual {v0}, Lbf/i;->Z()V

    goto :goto_13
.end method

.method public V()Lcom/google/googlenav/E;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 4078
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    .line 4081
    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_10

    :cond_e
    move-object v0, v2

    .line 4096
    :goto_f
    return-object v0

    .line 4084
    :cond_10
    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v0

    const/16 v3, 0xd

    if-ne v0, v3, :cond_23

    move-object v0, v1

    check-cast v0, Lbf/C;

    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_23

    move-object v0, v2

    .line 4086
    goto :goto_f

    .line 4091
    :cond_23
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_48

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_48

    .line 4094
    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_f

    .line 4096
    :cond_48
    invoke-virtual {v1}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    goto :goto_f
.end method

.method public Y()V
    .registers 2

    .prologue
    .line 4155
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 4156
    invoke-virtual {p0, v0}, Lbf/am;->e(Lbf/i;)V

    .line 4157
    if-eqz v0, :cond_c

    .line 4158
    invoke-virtual {v0}, Lbf/i;->aV()V

    .line 4160
    :cond_c
    return-void
.end method

.method public Z()I
    .registers 2

    .prologue
    .line 4185
    iget v0, p0, Lbf/am;->B:I

    return v0
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1229
    new-instance v0, Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p3, v1}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    .line 1230
    invoke-virtual {v0, p2}, Lcom/google/googlenav/ai;->a(Lo/D;)V

    .line 1232
    new-instance v1, Lcom/google/googlenav/bl;

    invoke-direct {v1, v0}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    .line 1235
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, p4, p5}, Lbf/am;->a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)Lbf/C;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)Lbf/C;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1257
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    .line 1259
    invoke-virtual {p1}, Lcom/google/googlenav/bl;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    .line 1260
    invoke-direct {p0, v0}, Lbf/am;->c(LaN/B;)Z

    move-result v8

    .line 1261
    if-eqz v8, :cond_44

    .line 1264
    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    .line 1265
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    .line 1268
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->g()[Ljava/lang/String;

    move-result-object v7

    .line 1269
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->z()V

    .line 1273
    :goto_21
    if-nez v8, :cond_27

    .line 1274
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/am;->d(Z)V

    .line 1279
    :cond_27
    const-string v0, "s"

    invoke-direct {p0, v0}, Lbf/am;->i(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v9

    .line 1281
    new-instance v0, Lbf/C;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    move v6, p3

    invoke-direct/range {v0 .. v9}, Lbf/C;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;Z[Ljava/lang/String;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1285
    const/4 v1, 0x0

    .line 1286
    invoke-virtual {p0, v0, v1, v9}, Lbf/am;->a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1287
    invoke-virtual {v0, p2}, Lbf/C;->a(B)V

    .line 1289
    return-object v0

    :cond_44
    move-object v7, p4

    goto :goto_21
.end method

.method public a(Lax/b;)Lbf/O;
    .registers 9
    .parameter

    .prologue
    .line 1361
    invoke-virtual {p0}, Lbf/am;->i()V

    .line 1363
    new-instance v0, Lbf/O;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->d:LaH/m;

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lbf/O;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/F;)V

    .line 1365
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1366
    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;)Lbf/aJ;
    .registers 4
    .parameter

    .prologue
    .line 1119
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1120
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1123
    :cond_f
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1125
    invoke-virtual {p0, p1}, Lbf/am;->d(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v0

    .line 1126
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1127
    return-object v0
.end method

.method public a(LaN/B;LaN/Y;Ljava/lang/String;)Lbf/aM;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1549
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LaN/u;->a(I)V

    .line 1550
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v0, p1, p2}, LaN/u;->e(LaN/B;LaN/Y;)V

    .line 1551
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aM;

    .line 1553
    if-eqz v0, :cond_19

    .line 1554
    invoke-virtual {v0}, Lbf/aM;->aW()V

    .line 1557
    :goto_18
    return-object v0

    :cond_19
    invoke-virtual {p0, p3}, Lbf/am;->a(Ljava/lang/String;)Lbf/aM;

    move-result-object v0

    goto :goto_18
.end method

.method public a(Ljava/lang/String;)Lbf/aM;
    .registers 8
    .parameter

    .prologue
    .line 1540
    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1541
    new-instance v0, Lbf/aM;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/aM;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Ljava/lang/String;)V

    .line 1543
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1544
    return-object v0
.end method

.method public a(Lcom/google/googlenav/F;)Lbf/aU;
    .registers 9
    .parameter

    .prologue
    .line 1595
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    .line 1596
    new-instance v0, Lbf/aU;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-static {}, Lbf/am;->W()I

    move-result v6

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/aU;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;I)V

    .line 1599
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1600
    return-object v0
.end method

.method public a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1790
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    .line 1792
    new-instance v0, Lbf/ak;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    move-object v6, p1

    move v7, p2

    move v8, p3

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lbf/ak;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/ai;ZBZ)V

    .line 1794
    invoke-virtual {p0, v0, p5}, Lbf/am;->a(Lbf/i;Z)V

    .line 1795
    return-object v0
.end method

.method protected a(Lcom/google/googlenav/layer/m;)Lbf/bE;
    .registers 9
    .parameter

    .prologue
    .line 1632
    new-instance v0, Lbf/bE;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    new-instance v6, LaN/k;

    invoke-direct {v6}, LaN/k;-><init>()V

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/bE;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    .line 1634
    return-object v0
.end method

.method public a(Lcom/google/googlenav/bN;)Lbf/bH;
    .registers 8
    .parameter

    .prologue
    .line 1440
    invoke-virtual {p0}, Lbf/am;->k()V

    .line 1442
    new-instance v0, Lbf/bH;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/bH;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 1444
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1445
    return-object v0
.end method

.method public a(Lax/w;)Lbf/bK;
    .registers 8
    .parameter

    .prologue
    .line 1391
    invoke-virtual {p0}, Lbf/am;->j()V

    .line 1393
    new-instance v0, Lbf/bK;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lbf/bK;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    .line 1395
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1396
    return-object v0
.end method

.method public a(Lcom/google/googlenav/Y;Z)Lbf/bU;
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 1428
    invoke-virtual {p0}, Lbf/am;->l()V

    .line 1430
    new-instance v0, Lbf/bU;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v5, p0, Lbf/am;->h:LaN/k;

    move-object v6, p1

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lbf/bU;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/F;Z)V

    .line 1432
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1433
    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;Z)Lbf/bk;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1085
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    .line 1086
    if-nez p2, :cond_d

    if-eqz v0, :cond_10

    .line 1087
    :cond_d
    invoke-direct {p0, v1, v1}, Lbf/am;->b(IZ)V

    .line 1093
    :cond_10
    if-eqz v0, :cond_16

    .line 1094
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1097
    :cond_16
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1099
    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    .line 1101
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 1102
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v8, 0x5

    .line 1103
    :goto_27
    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 1105
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1106
    return-object v0

    .line 1102
    :cond_3e
    invoke-static {}, Lbf/am;->W()I

    move-result v8

    goto :goto_27
.end method

.method public a(Z)Lbf/by;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1569
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    .line 1570
    new-instance v1, Lbf/by;

    iget-object v2, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v3, p0, Lbf/am;->e:LaN/p;

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    iget-object v5, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v1, v2, v3, v4, v5}, Lbf/by;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    .line 1575
    sget-object v2, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v2}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1577
    invoke-virtual {p0, v1, v0, v0}, Lbf/am;->a(Lbf/i;ZZ)Z

    .line 1581
    :goto_1d
    return-object v1

    .line 1579
    :cond_1e
    if-nez p1, :cond_21

    const/4 v0, 0x1

    :cond_21
    invoke-virtual {p0, v1, v0}, Lbf/am;->a(Lbf/i;Z)V

    goto :goto_1d
.end method

.method protected a(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 1649
    new-instance v0, Lbf/y;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    .line 1651
    return-object v0
.end method

.method public a(Lcom/google/googlenav/layer/m;ZZZ)Lbf/y;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1674
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1675
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbf/am;->a(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v3

    :goto_11
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    .line 1690
    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZLbf/y;ZZ)V

    .line 1692
    return-object v3

    .line 1681
    :cond_1a
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->i()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_39

    .line 1682
    new-instance v6, LaN/k;

    invoke-direct {v6}, LaN/k;-><init>()V

    .line 1686
    :goto_29
    new-instance v0, Lbf/y;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    move-object v3, v0

    goto :goto_11

    .line 1684
    :cond_39
    iget-object v6, p0, Lbf/am;->h:LaN/k;

    goto :goto_29
.end method

.method public a()V
    .registers 3

    .prologue
    .line 4311
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lbf/am;->r:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 4312
    iget-object v0, p0, Lbf/am;->r:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/layer/l;

    invoke-interface {v0}, Lcom/google/googlenav/layer/l;->a()V

    .line 4311
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 4314
    :cond_19
    return-void
.end method

.method protected a(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 984
    invoke-static {p1}, Lbf/am;->b(I)I

    move-result v0

    .line 986
    invoke-direct {p0, p1}, Lbf/am;->e(I)I

    move-result v1

    if-lt v1, v0, :cond_26

    move v1, v2

    .line 987
    :goto_c
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_26

    .line 988
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 989
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v3

    if-ne v3, p1, :cond_27

    .line 992
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lbf/am;->b(Lbf/i;ZZ)V

    .line 999
    :cond_26
    return-void

    .line 987
    :cond_27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c
.end method

.method public a(ILjava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4176
    iput p1, p0, Lbf/am;->B:I

    .line 4177
    iput-object p2, p0, Lbf/am;->C:Ljava/lang/Object;

    .line 4178
    return-void
.end method

.method protected a(LaN/B;)V
    .registers 6
    .parameter

    .prologue
    .line 2162
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 2165
    iput-object p1, p0, Lbf/am;->N:LaN/B;

    .line 2168
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_10
    if-ltz v1, :cond_25

    .line 2169
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2170
    iget-object v2, p0, Lbf/am;->L:Ljava/util/Vector;

    sget v3, Lbf/am;->J:I

    invoke-virtual {v0, v2, p1, v3}, Lbf/i;->a(Ljava/util/Vector;LaN/B;I)V

    .line 2168
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_10

    .line 2174
    :cond_25
    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    .line 2175
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v0

    .line 2176
    if-eqz v0, :cond_49

    .line 2177
    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v0

    .line 2178
    sget v2, Lbf/am;->J:I

    int-to-long v2, v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_49

    .line 2179
    iget-object v2, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-static {v0, v1}, Lbf/ai;->a(J)Lbf/ai;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 2182
    :cond_49
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 3086
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 3087
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->a(Landroid/content/res/Configuration;)V

    .line 3086
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 3089
    :cond_19
    return-void
.end method

.method public a(Lbb/w;)V
    .registers 13
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2996
    invoke-direct {p0, v3, v3}, Lbf/am;->b(IZ)V

    .line 3001
    invoke-direct {p0, v4, v3}, Lbf/am;->b(IZ)V

    .line 3003
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 3005
    invoke-virtual {p1}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v0

    .line 3006
    invoke-virtual {p1}, Lbb/w;->h()LaN/B;

    move-result-object v9

    .line 3007
    invoke-virtual {p1}, Lbb/w;->i()LaN/Y;

    move-result-object v10

    .line 3009
    new-instance v1, Lcom/google/googlenav/ai;

    const/16 v2, 0xa

    invoke-direct {v1, v9, v0, v2}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    .line 3010
    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    .line 3015
    new-array v2, v4, [Lcom/google/googlenav/ai;

    aput-object v1, v2, v3

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v3}, LaN/u;->a()I

    move-result v3

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v4}, LaN/u;->b()I

    move-result v4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3018
    new-instance v1, Lcom/google/googlenav/T;

    sget-object v2, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v3, p0, Lbf/am;->h:LaN/k;

    iget-object v4, p0, Lbf/am;->e:LaN/p;

    iget-object v5, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    .line 3020
    new-instance v5, Lcom/google/googlenav/n;

    invoke-direct {v5, v0, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    .line 3021
    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v6, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-static {}, Lbf/am;->W()I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 3023
    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1, v9, v10}, LaN/u;->d(LaN/B;LaN/Y;)V

    .line 3024
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 3025
    return-void
.end method

.method public a(Lbf/X;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 2892
    iget v0, p0, Lbf/am;->B:I

    iget-object v1, p0, Lbf/am;->C:Ljava/lang/Object;

    invoke-virtual {p1, v0, v2, v1}, Lbf/X;->a(IILjava/lang/Object;)Z

    .line 2896
    iput v2, p0, Lbf/am;->B:I

    .line 2897
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/am;->C:Ljava/lang/Object;

    .line 2898
    return-void
.end method

.method public a(Lbf/aU;)V
    .registers 4
    .parameter

    .prologue
    .line 3850
    invoke-virtual {p1}, Lbf/aU;->a()I

    move-result v0

    if-nez v0, :cond_18

    invoke-virtual {p1}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 3852
    invoke-virtual {p1}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->b(Lcom/google/googlenav/aZ;Z)V

    .line 3854
    :cond_18
    return-void
.end method

.method public a(Lbf/au;)V
    .registers 4
    .parameter

    .prologue
    .line 880
    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v1

    .line 881
    :try_start_3
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 882
    monitor-exit v1

    .line 883
    return-void

    .line 882
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public a(Lbf/au;LR/a;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 871
    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v1

    .line 872
    :try_start_3
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 873
    monitor-exit v1

    .line 874
    return-void

    .line 873
    :catchall_a
    move-exception v0

    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    throw v0
.end method

.method protected a(Lbf/i;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2412
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 2413
    invoke-direct {p0, p1, p2}, Lbf/am;->c(Lbf/i;I)V

    .line 2421
    :goto_d
    invoke-virtual {p1}, Lbf/i;->ag()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 2424
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lbf/i;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v0

    .line 2426
    iget-object v1, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->b(LaN/B;)V

    .line 2428
    :cond_28
    return-void

    .line 2415
    :cond_29
    invoke-direct {p0, p1, p2}, Lbf/am;->b(Lbf/i;I)V

    goto :goto_d
.end method

.method public a(Lbf/i;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 540
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lbf/am;->a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 541
    return-void
.end method

.method public a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 565
    if-nez p2, :cond_f

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 566
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->aX()V

    .line 569
    :cond_f
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lbf/am;->a(Lbf/i;ZZ)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 571
    invoke-virtual {p1}, Lbf/i;->aW()V

    .line 574
    invoke-virtual {p1}, Lbf/i;->aM()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 575
    invoke-static {}, Lbf/am;->X()I

    move-result v0

    invoke-virtual {p1, v0}, Lbf/i;->e(I)V

    .line 576
    invoke-direct {p0, p1}, Lbf/am;->b(Lbf/i;)V

    .line 580
    :cond_29
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 583
    :cond_2c
    if-eqz p3, :cond_32

    .line 584
    invoke-direct {p0, p3}, Lbf/am;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 588
    :goto_31
    return-void

    .line 586
    :cond_32
    const-string v0, "s"

    invoke-direct {p0, p1, v0}, Lbf/am;->a(Lbf/i;Ljava/lang/String;)V

    goto :goto_31
.end method

.method public a(Lcom/google/googlenav/J;Lbf/aU;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    .line 3760
    invoke-virtual {p2}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 3769
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->z()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->P()LaN/M;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(LaN/M;)Lcom/google/googlenav/bg;

    move-result-object v0

    iget-object v2, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    .line 3776
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 3786
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lbf/aU;->be()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3788
    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 3789
    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 3819
    :cond_4c
    :goto_4c
    return-void

    .line 3790
    :cond_4d
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 3793
    invoke-virtual {p2}, Lbf/aU;->be()Ljava/lang/String;

    move-result-object v0

    .line 3794
    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 3797
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    .line 3798
    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_4c

    .line 3799
    :cond_6e
    invoke-virtual {p2}, Lbf/aU;->b()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 3800
    invoke-virtual {p2}, Lbf/aU;->bf()Lcom/google/googlenav/ai;

    move-result-object v3

    .line 3801
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    .line 3802
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_86

    .line 3803
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    .line 3805
    :cond_86
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->E()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3807
    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    .line 3810
    invoke-virtual {p2}, Lbf/aU;->bf()Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->am()[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/bg;

    .line 3815
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    .line 3817
    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    goto :goto_4c
.end method

.method public a(Lcom/google/googlenav/aZ;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3035
    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/F;)Lbf/aU;

    move-result-object v0

    .line 3036
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Lbf/aU;->a(Lcom/google/googlenav/aZ;ZI)V

    .line 3037
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;ILcom/google/googlenav/ba;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3029
    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/F;)Lbf/aU;

    move-result-object v0

    .line 3031
    invoke-virtual {v0, p1, p3, p2}, Lbf/aU;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/ba;I)V

    .line 3032
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2777
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 2778
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aC()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v3, :cond_18

    .line 2780
    invoke-direct {p0, p1}, Lbf/am;->i(Lcom/google/googlenav/aZ;)V

    .line 2832
    :goto_17
    return-void

    .line 2782
    :cond_18
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->k()Lcom/google/googlenav/ba;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lbf/am;->a(Lcom/google/googlenav/aZ;ILcom/google/googlenav/ba;)V

    goto :goto_17

    .line 2787
    :cond_21
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->r()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 2788
    invoke-virtual {p0, p1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    .line 2825
    :cond_2a
    :goto_2a
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-eqz v0, :cond_39

    .line 2827
    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v0}, Lbf/am;->a(Lcom/google/googlenav/ai;)V

    .line 2831
    :cond_39
    invoke-virtual {p2}, Lcom/google/googlenav/A;->d()V

    goto :goto_17

    .line 2789
    :cond_3d
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/googlenav/bf;->H:Z

    if-eqz v0, :cond_4f

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-nez v0, :cond_4f

    .line 2793
    invoke-direct {p0, p1}, Lbf/am;->j(Lcom/google/googlenav/aZ;)V

    goto :goto_2a

    .line 2794
    :cond_4f
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ne v0, v3, :cond_68

    invoke-virtual {p2}, Lcom/google/googlenav/A;->a()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 2799
    invoke-virtual {p0, p1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    .line 2800
    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p2, v0}, Lcom/google/googlenav/A;->a(Lcom/google/googlenav/ai;)V

    goto :goto_2a

    .line 2801
    :cond_68
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-gtz v0, :cond_7a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-nez v0, :cond_7a

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 2807
    :cond_7a
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_85

    .line 2808
    invoke-direct {p0, p1, p3}, Lbf/am;->c(Lcom/google/googlenav/aZ;Z)V

    goto :goto_2a

    .line 2809
    :cond_85
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 2810
    invoke-direct {p0, p1}, Lbf/am;->h(Lcom/google/googlenav/aZ;)V

    goto :goto_2a

    .line 2812
    :cond_8f
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->w()Z

    move-result v0

    if-nez v0, :cond_2a

    .line 2813
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->F()Z

    move-result v0

    if-eqz v0, :cond_ab

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->K()Z

    move-result v0

    if-nez v0, :cond_ab

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->N()Z

    move-result v0

    if-nez v0, :cond_ab

    .line 2818
    invoke-virtual {p0, p1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    goto :goto_2a

    .line 2820
    :cond_ab
    invoke-virtual {p0, p1, v3}, Lbf/am;->b(Lcom/google/googlenav/aZ;Z)V

    goto/16 :goto_2a
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 3891
    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    .line 3893
    invoke-static {p1}, Lax/y;->b(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/z;->a(Lax/y;)Z

    .line 3894
    return-void
.end method

.method protected a(Lcom/google/googlenav/layer/m;ZLbf/y;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1697
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZLbf/y;ZZ)V

    .line 1698
    return-void
.end method

.method protected a(Lcom/google/googlenav/layer/m;ZLbf/y;ZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x6

    .line 1716
    if-eqz p2, :cond_7

    .line 1717
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lbf/am;->b(IZ)V

    .line 1720
    :cond_7
    if-eqz p5, :cond_c

    .line 1721
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1724
    :cond_c
    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    .line 1727
    invoke-virtual {p1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbf/am;->g(Ljava/lang/String;)V

    .line 1729
    invoke-virtual {p0, p3, p4}, Lbf/am;->a(Lbf/i;Z)V

    .line 1730
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2700
    move v1, v2

    :goto_2
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 2701
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->b(Lcom/google/googlenav/ui/r;)V

    .line 2700
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2705
    :cond_19
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/i;->e(Lcom/google/googlenav/ui/r;)V

    .line 2708
    :goto_20
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_36

    .line 2709
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0, p1}, Lbf/i;->d(Lcom/google/googlenav/ui/r;)V

    .line 2708
    add-int/lit8 v2, v2, 0x1

    goto :goto_20

    .line 2711
    :cond_36
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/jv;Lbf/aU;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3862
    invoke-virtual {p2}, Lbf/aU;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_30

    .line 3883
    :goto_7
    :pswitch_7
    return-void

    .line 3864
    :pswitch_8
    const-string v0, "100"

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    goto :goto_7

    .line 3867
    :pswitch_e
    invoke-virtual {p2}, Lbf/aU;->bg()Lax/b;

    move-result-object v0

    .line 3870
    invoke-virtual {v0}, Lax/b;->y()I

    move-result v1

    if-nez v1, :cond_1c

    .line 3872
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/b;)V

    goto :goto_7

    .line 3874
    :cond_1c
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    goto :goto_7

    .line 3878
    :pswitch_20
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v0

    .line 3880
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    .line 3881
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/dg;->h()V

    goto :goto_7

    .line 3862
    nop

    :pswitch_data_30
    .packed-switch 0x0
        :pswitch_8
        :pswitch_e
        :pswitch_7
        :pswitch_7
        :pswitch_20
    .end packed-switch
.end method

.method public a(Lo/S;)V
    .registers 7
    .parameter

    .prologue
    .line 2534
    invoke-virtual {p1}, Lo/S;->a()Lo/T;

    move-result-object v1

    .line 2535
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_2d

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v2, 0xd

    if-ne v0, v2, :cond_2d

    .line 2537
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/C;

    .line 2538
    invoke-virtual {v0}, Lbf/C;->bI()Z

    move-result v2

    if-eqz v2, :cond_2d

    invoke-virtual {v0}, Lbf/C;->aa()Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 2540
    if-nez v1, :cond_2e

    .line 2541
    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    .line 2553
    :cond_2d
    :goto_2d
    return-void

    .line 2542
    :cond_2e
    invoke-virtual {v0}, Lbf/C;->bJ()Z

    move-result v2

    if-nez v2, :cond_2d

    .line 2543
    iget-object v2, p0, Lbf/am;->f:LaN/u;

    invoke-static {v1}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lbf/C;->a()LaN/B;

    move-result-object v3

    sget v4, Lbf/am;->Q:I

    invoke-virtual {v2, v1, v3, v4}, LaN/u;->a(LaN/B;LaN/B;I)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 2548
    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    goto :goto_2d
.end method

.method public a(ZZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 3964
    invoke-direct {p0, p2}, Lbf/am;->g(Z)V

    .line 3967
    if-eqz p1, :cond_f

    .line 3970
    iget-object v2, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v2

    .line 3971
    :try_start_9
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    .line 3972
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_2a

    .line 3976
    :cond_f
    if-eqz p3, :cond_14

    .line 3977
    invoke-virtual {p0}, Lbf/am;->T()V

    .line 3982
    :cond_14
    iget-object v0, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->l()V

    move v0, v1

    .line 3985
    :goto_1e
    sget-object v2, Lbf/am;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2d

    .line 3986
    sget-object v2, Lbf/am;->u:[Z

    aput-boolean v1, v2, v0

    .line 3985
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 3972
    :catchall_2a
    move-exception v0

    :try_start_2b
    monitor-exit v2
    :try_end_2c
    .catchall {:try_start_2b .. :try_end_2c} :catchall_2a

    throw v0

    .line 3988
    :cond_2d
    return-void
.end method

.method public a(LaN/B;LaN/B;IIIIILam/e;)Z
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2573
    if-nez p2, :cond_4

    .line 2649
    :goto_3
    return v2

    .line 2576
    :cond_4
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    .line 2577
    invoke-virtual {p1, v0}, LaN/B;->a(LaN/Y;)I

    move-result v1

    invoke-virtual {p2, v0}, LaN/B;->a(LaN/Y;)I

    move-result v3

    sub-int v4, v1, v3

    .line 2578
    invoke-virtual {p1, v0}, LaN/B;->b(LaN/Y;)I

    move-result v1

    invoke-virtual {p2, v0}, LaN/B;->b(LaN/Y;)I

    move-result v0

    sub-int v5, v1, v0

    .line 2585
    packed-switch p3, :pswitch_data_74

    .line 2607
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Feature.ANCHOR_* type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2587
    :pswitch_3a
    div-int/lit8 v0, p4, 0x2

    neg-int v1, v0

    .line 2588
    neg-int v0, p5

    .line 2622
    :goto_3e
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->i()I

    move-result v3

    .line 2626
    if-ge p4, v3, :cond_4e

    .line 2627
    sub-int v6, v3, p4

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v6

    move p4, v3

    .line 2631
    :cond_4e
    if-ge p5, v3, :cond_56

    .line 2632
    sub-int v6, v3, p5

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v0, v6

    move p5, v3

    .line 2644
    :cond_56
    add-int v3, v1, p4

    .line 2645
    add-int v6, v0, p5

    .line 2649
    if-lt v4, v1, :cond_72

    if-gt v4, v3, :cond_72

    if-lt v5, v0, :cond_72

    if-gt v5, v6, :cond_72

    const/4 v0, 0x1

    :goto_63
    move v2, v0

    goto :goto_3

    .line 2592
    :pswitch_65
    div-int/lit8 v0, p4, 0x2

    neg-int v1, v0

    .line 2593
    div-int/lit8 v0, p5, 0x2

    neg-int v0, v0

    .line 2594
    goto :goto_3e

    .line 2597
    :pswitch_6c
    neg-int v1, p4

    .line 2598
    neg-int v0, p5

    .line 2599
    goto :goto_3e

    .line 2602
    :pswitch_6f
    neg-int v1, p6

    .line 2603
    neg-int v0, p7

    .line 2604
    goto :goto_3e

    :cond_72
    move v0, v2

    .line 2649
    goto :goto_63

    .line 2585
    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_3a
        :pswitch_65
        :pswitch_6c
        :pswitch_6f
    .end packed-switch
.end method

.method public a(Lat/a;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2116
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    .line 2120
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lbf/i;->aa()Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 2122
    :cond_e
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v3

    const/16 v4, 0x32

    if-ne v3, v4, :cond_1c

    .line 2123
    iget-object v0, p0, Lbf/am;->o:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;Z)V

    .line 2139
    :goto_1b
    return v1

    .line 2125
    :cond_1c
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v3

    const/16 v4, 0x2a

    if-ne v3, v4, :cond_2f

    .line 2126
    iget-object v2, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->j()Lbf/by;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lbf/by;->e(ILjava/lang/Object;)V

    goto :goto_1b

    .line 2131
    :cond_2f
    if-eqz v2, :cond_38

    invoke-virtual {v2, p1}, Lbf/i;->a(Lat/a;)Z

    move-result v2

    if-eqz v2, :cond_38

    move v0, v1

    .line 2133
    :cond_38
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4e

    if-nez v0, :cond_4e

    invoke-virtual {p0}, Lbf/am;->w()Lbf/bK;

    move-result-object v2

    if-eqz v2, :cond_4e

    .line 2136
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->H()V

    move v0, v1

    :cond_4e
    move v1, v0

    .line 2139
    goto :goto_1b
.end method

.method public a(Lat/b;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2469
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 2472
    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 2473
    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/aA;->a(Lat/b;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2516
    :cond_1a
    :goto_1a
    return v2

    .line 2483
    :cond_1b
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v0

    if-nez v0, :cond_27

    invoke-virtual {p1}, Lat/b;->f()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 2485
    :cond_27
    iget-object v0, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v3

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v4

    invoke-virtual {v0, v3, v4}, LaN/u;->b(II)LaN/B;

    move-result-object v3

    .line 2489
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2490
    iget-boolean v4, p0, Lbf/am;->M:Z

    if-nez v4, :cond_45

    if-eqz v0, :cond_7a

    invoke-virtual {v0}, Lbf/i;->aj()Z

    move-result v0

    if-nez v0, :cond_7a

    :cond_45
    move v0, v2

    .line 2495
    :goto_46
    if-nez v0, :cond_5f

    iget-object v0, p0, Lbf/am;->N:LaN/B;

    if-eqz v0, :cond_5f

    iget-object v0, p0, Lbf/am;->N:LaN/B;

    iget-object v4, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v4}, LaN/p;->c()LaN/Y;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v4

    sget v0, Lbf/am;->J:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-lez v0, :cond_7c

    .line 2498
    :cond_5f
    invoke-virtual {p0, v1}, Lbf/am;->d(Z)V

    .line 2499
    invoke-virtual {p0, v3}, Lbf/am;->a(LaN/B;)V

    move v0, v2

    .line 2506
    :goto_66
    invoke-virtual {p0, v0}, Lbf/am;->c(Z)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 2515
    :cond_6c
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2516
    if-eqz v0, :cond_78

    invoke-virtual {v0, p1}, Lbf/i;->a(Lat/b;)Z

    move-result v0

    if-nez v0, :cond_1a

    :cond_78
    move v2, v1

    goto :goto_1a

    :cond_7a
    move v0, v1

    .line 2490
    goto :goto_46

    .line 2502
    :cond_7c
    invoke-virtual {p0, v3}, Lbf/am;->b(LaN/B;)V

    move v0, v1

    goto :goto_66
.end method

.method protected a(Lbf/i;ZZ)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 600
    invoke-virtual {p1}, Lbf/i;->aS()Z

    move-result v0

    if-nez v0, :cond_9

    .line 660
    :goto_8
    return v2

    .line 608
    :cond_9
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_33

    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    sget v1, Lbf/am;->a:I

    if-lt v0, v1, :cond_33

    move v1, v2

    .line 610
    :goto_1a
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_33

    .line 611
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 612
    invoke-virtual {v0}, Lbf/i;->aB()Z

    move-result v4

    if-eqz v4, :cond_69

    .line 613
    invoke-virtual {p0, v0, v2, v3}, Lbf/am;->b(Lbf/i;ZZ)V

    .line 619
    :cond_33
    if-eqz p2, :cond_6d

    .line 620
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 629
    :goto_3a
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 630
    if-eqz p2, :cond_76

    .line 631
    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, p1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 640
    :cond_47
    :goto_47
    invoke-virtual {p1}, Lbf/i;->aB()Z

    move-result v0

    if-eqz v0, :cond_50

    .line 641
    invoke-direct {p0, p1}, Lbf/am;->o(Lbf/i;)V

    .line 644
    :cond_50
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->a(Lcom/google/googlenav/ui/Y;)V

    .line 647
    invoke-virtual {p0, p1}, Lbf/am;->j(Lbf/i;)V

    .line 650
    if-eqz p3, :cond_67

    .line 651
    invoke-virtual {p1, v3}, Lbf/i;->f(I)V

    .line 655
    invoke-virtual {p1}, Lbf/i;->at()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 656
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lbf/i;->f(I)V

    :cond_67
    move v2, v3

    .line 660
    goto :goto_8

    .line 610
    :cond_69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    .line 622
    :cond_6d
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 625
    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    goto :goto_3a

    .line 633
    :cond_76
    iget-object v0, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_47
.end method

.method public aa()Lbf/a;
    .registers 2

    .prologue
    .line 4278
    iget-object v0, p0, Lbf/am;->G:Lbf/a;

    return-object v0
.end method

.method public b(Lax/b;)Lbf/O;
    .registers 5
    .parameter

    .prologue
    .line 1452
    invoke-virtual {p0}, Lbf/am;->v()Lbf/O;

    move-result-object v0

    .line 1453
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lbf/O;->ae()Z

    move-result v1

    if-nez v1, :cond_11

    .line 1454
    :cond_c
    invoke-virtual {p0, p1}, Lbf/am;->a(Lax/b;)Lbf/O;

    move-result-object v0

    .line 1477
    :cond_10
    :goto_10
    return-object v0

    .line 1460
    :cond_11
    iget-object v1, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1461
    iget-object v1, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1462
    invoke-virtual {p0, v0}, Lbf/am;->i(Lbf/i;)V

    .line 1463
    invoke-virtual {v0, p1}, Lbf/O;->b(Lcom/google/googlenav/F;)V

    .line 1464
    invoke-virtual {v0}, Lbf/O;->J()V

    .line 1465
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lbf/am;->a(Lbf/i;ZZ)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1468
    invoke-virtual {v0}, Lbf/O;->aM()Z

    move-result v1

    if-eqz v1, :cond_3c

    .line 1469
    invoke-static {}, Lbf/am;->X()I

    move-result v1

    invoke-virtual {v0, v1}, Lbf/O;->e(I)V

    .line 1470
    invoke-direct {p0, v0}, Lbf/am;->b(Lbf/i;)V

    .line 1474
    :cond_3c
    invoke-virtual {p0}, Lbf/am;->d()V

    goto :goto_10
.end method

.method public b(Lcom/google/googlenav/aZ;)Lbf/x;
    .registers 11
    .parameter

    .prologue
    const/16 v4, 0x28

    const/16 v3, 0x24

    .line 1137
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 1138
    if-eqz v0, :cond_7c

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_7c

    const/4 v0, 0x1

    .line 1140
    :goto_13
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    .line 1145
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    if-eq v2, v3, :cond_2e

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v2

    if-eq v2, v4, :cond_2e

    .line 1152
    if-nez v0, :cond_7e

    .line 1153
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1162
    :cond_2e
    :goto_2e
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v2, 0x11

    if-eq v0, v2, :cond_42

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    if-eq v0, v3, :cond_42

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    if-ne v0, v4, :cond_51

    .line 1165
    :cond_42
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 1166
    if-eqz v0, :cond_51

    invoke-virtual {v0}, Lbf/i;->ah()Z

    move-result v1

    if-nez v1, :cond_51

    .line 1168
    invoke-virtual {v0}, Lbf/i;->aQ()V

    .line 1172
    :cond_51
    invoke-static {}, Lbf/am;->W()I

    move-result v8

    .line 1173
    new-instance v5, Lcom/google/googlenav/n;

    new-instance v0, Lcom/google/googlenav/T;

    sget-object v1, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v2, p0, Lbf/am;->h:LaN/k;

    iget-object v3, p0, Lbf/am;->e:LaN/p;

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-direct {v5, p1, v0}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    .line 1175
    new-instance v0, Lbf/x;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    sget-object v6, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v8}, Lbf/x;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 1178
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1179
    return-object v0

    .line 1138
    :cond_7c
    const/4 v0, 0x0

    goto :goto_13

    .line 1155
    :cond_7e
    invoke-virtual {p0}, Lbf/am;->e()V

    goto :goto_2e
.end method

.method public b(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1655
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZZZ)Lbf/y;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lbf/y;
    .registers 7
    .parameter

    .prologue
    .line 1746
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 1747
    const/4 v0, 0x0

    move v2, v0

    :goto_a
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_41

    .line 1748
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 1749
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    const/4 v4, 0x6

    if-ne v1, v4, :cond_3d

    move-object v1, v0

    .line 1750
    check-cast v1, Lbf/y;

    invoke-virtual {v1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1751
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 1752
    check-cast v0, Lbf/y;

    .line 1756
    :goto_3c
    return-object v0

    .line 1747
    :cond_3d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_a

    .line 1756
    :cond_41
    const/4 v0, 0x0

    goto :goto_3c
.end method

.method public b()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 452
    invoke-virtual {p0}, Lbf/am;->T()V

    .line 456
    iget-object v0, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->l()V

    move v0, v1

    .line 459
    :goto_e
    sget-object v2, Lbf/am;->t:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1a

    .line 460
    sget-object v2, Lbf/am;->u:[Z

    aput-boolean v1, v2, v0

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 462
    :cond_1a
    return-void
.end method

.method protected b(LaN/B;)V
    .registers 9
    .parameter

    .prologue
    .line 2192
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_6f

    .line 2193
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    .line 2194
    invoke-virtual {v0}, Lbf/ai;->f()Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 2195
    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v2

    .line 2196
    invoke-virtual {v2}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v2

    .line 2197
    if-eqz v2, :cond_33

    .line 2198
    iget-object v3, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lbf/ai;->b(J)V

    .line 2192
    :goto_2f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2201
    :cond_33
    const-wide v2, 0x7fffffffffffffffL

    invoke-virtual {v0, v2, v3}, Lbf/ai;->b(J)V

    goto :goto_2f

    .line 2204
    :cond_3c
    invoke-virtual {v0}, Lbf/ai;->a()Lbf/i;

    move-result-object v4

    .line 2205
    invoke-virtual {v0}, Lbf/ai;->b()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {v4, v2, p1}, Lbf/i;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v2

    .line 2208
    invoke-virtual {v4}, Lbf/i;->ax()Z

    move-result v5

    if-nez v5, :cond_52

    .line 2209
    sget v5, Lbf/am;->j:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    .line 2214
    :cond_52
    invoke-virtual {v4}, Lbf/i;->av()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5d

    .line 2215
    sget v5, Lbf/am;->j:I

    int-to-long v5, v5

    add-long/2addr v2, v5

    .line 2221
    :cond_5d
    invoke-virtual {v4}, Lbf/i;->av()I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_6b

    .line 2222
    sget v4, Lbf/am;->j:I

    mul-int/lit8 v4, v4, 0x2

    int-to-long v4, v4

    add-long/2addr v2, v4

    .line 2225
    :cond_6b
    invoke-virtual {v0, v2, v3}, Lbf/ai;->b(J)V

    goto :goto_2f

    .line 2228
    :cond_6f
    return-void
.end method

.method protected b(Lbf/i;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 809
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 812
    invoke-virtual {p0, p1}, Lbf/am;->k(Lbf/i;)V

    .line 815
    invoke-virtual {p1, v2}, Lbf/i;->h(Z)V

    .line 818
    invoke-virtual {p1}, Lbf/i;->aX()V

    .line 821
    invoke-virtual {p1}, Lbf/i;->aU()V

    .line 823
    iget-object v1, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 824
    iget-object v1, p0, Lbf/am;->p:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 825
    invoke-virtual {p0, p1}, Lbf/am;->i(Lbf/i;)V

    .line 828
    if-eqz v0, :cond_33

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_33

    if-eqz p2, :cond_33

    .line 829
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->aW()V

    .line 833
    :cond_33
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3d

    .line 834
    invoke-virtual {p0, v2}, Lbf/am;->f(Z)V

    .line 837
    :cond_3d
    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    .line 839
    invoke-direct {p0, p1}, Lbf/am;->a(Lbf/i;)V

    .line 844
    invoke-virtual {p1}, Lbf/i;->aM()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 845
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 846
    if-eqz p3, :cond_5c

    invoke-virtual {p1}, Lbf/i;->aN()Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 847
    invoke-direct {p0, p1}, Lbf/am;->b(Lbf/i;)V

    .line 856
    :cond_5c
    :goto_5c
    if-eqz p3, :cond_62

    .line 857
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lbf/i;->f(I)V

    .line 859
    :cond_62
    return-void

    .line 850
    :cond_63
    invoke-direct {p0, p1}, Lbf/am;->c(Lbf/i;)V

    goto :goto_5c
.end method

.method public b(Lcom/google/googlenav/J;Lbf/aU;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3828
    invoke-virtual {p2}, Lbf/aU;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_1e

    .line 3839
    :goto_7
    :pswitch_7
    return-void

    .line 3832
    :pswitch_8
    invoke-virtual {p0, p1, p2}, Lbf/am;->a(Lcom/google/googlenav/J;Lbf/aU;)V

    goto :goto_7

    .line 3835
    :pswitch_c
    iget-object v0, p0, Lbf/am;->o:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {p2}, Lbf/aU;->bg()Lax/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/ca;->c(Lax/b;)V

    goto :goto_7

    .line 3828
    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_8
        :pswitch_c
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public b(Lcom/google/googlenav/aZ;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2905
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2906
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    check-cast v0, Lbf/bk;

    .line 2909
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Lbf/bk;->ax()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 2912
    invoke-virtual {v0}, Lbf/bk;->ae()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 2913
    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2988
    :cond_1f
    :goto_1f
    return-void

    .line 2922
    :cond_20
    const-string v0, "20"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_73

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_73

    .line 2924
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    instance-of v0, v0, Lbf/bk;

    if-eqz v0, :cond_73

    .line 2925
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->l()Lcom/google/googlenav/bb;

    move-result-object v0

    check-cast v0, Lbf/bk;

    .line 2926
    invoke-virtual {v0}, Lbf/bk;->ax()Z

    move-result v1

    if-eqz v1, :cond_73

    .line 2927
    invoke-virtual {v0}, Lbf/bk;->ae()Z

    move-result v1

    if-eqz v1, :cond_57

    .line 2928
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/bk;->c(Lcom/google/googlenav/F;)V

    .line 2929
    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    goto :goto_1f

    .line 2931
    :cond_57
    invoke-virtual {v0}, Lbf/bk;->af()Z

    move-result v1

    if-eqz v1, :cond_73

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->W()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_73

    .line 2934
    invoke-virtual {v0}, Lbf/bk;->h()V

    .line 2935
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/bk;->c(Lcom/google/googlenav/F;)V

    .line 2936
    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    goto :goto_1f

    :cond_73
    move v2, v3

    .line 2945
    :goto_74
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_c4

    .line 2946
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2947
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    if-nez v1, :cond_129

    .line 2948
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 2949
    invoke-virtual {v0}, Lbf/i;->aK()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c1

    move-object v1, v0

    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_129

    .line 2951
    :cond_c1
    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    :cond_c4
    move v2, v3

    .line 2961
    :goto_c5
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_ed

    .line 2962
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2963
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    if-nez v1, :cond_12e

    move-object v1, v0

    check-cast v1, Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ae()Z

    move-result v1

    if-eqz v1, :cond_12e

    .line 2965
    check-cast v0, Lbf/bk;

    invoke-virtual {v0, v3}, Lbf/bk;->k(I)V

    .line 2970
    :cond_ed
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-eqz v0, :cond_f6

    .line 2971
    invoke-direct {p0}, Lbf/am;->ai()V

    .line 2974
    :cond_f6
    iget-boolean v0, p0, Lbf/am;->s:Z

    if-nez v0, :cond_132

    move v0, v4

    :goto_fb
    invoke-virtual {p0, p1, v0}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    .line 2975
    invoke-virtual {v0, p1, p2}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    .line 2978
    iget-boolean v1, p0, Lbf/am;->s:Z

    if-nez v1, :cond_10c

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ae()Z

    move-result v1

    if-eqz v1, :cond_10f

    .line 2979
    :cond_10c
    invoke-virtual {v0, v4}, Lbf/bk;->h(Z)V

    .line 2981
    :cond_10f
    invoke-virtual {p0, v3}, Lbf/am;->e(Z)V

    .line 2985
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v0

    if-nez v0, :cond_122

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 2986
    :cond_122
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->i()V

    goto/16 :goto_1f

    .line 2945
    :cond_129
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_74

    .line 2961
    :cond_12e
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c5

    :cond_132
    move v0, v3

    .line 2974
    goto :goto_fb
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .registers 4
    .parameter

    .prologue
    .line 2725
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2726
    if-eqz v0, :cond_26

    .line 2727
    invoke-virtual {v0, p1}, Lbf/i;->a(Lcom/google/googlenav/ui/r;)V

    .line 2731
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->B()Z

    move-result v1

    if-eqz v1, :cond_26

    invoke-virtual {v0}, Lbf/i;->ai()Z

    move-result v0

    if-nez v0, :cond_26

    .line 2733
    invoke-virtual {p0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    .line 2734
    if-eqz v0, :cond_26

    iget-object v1, p0, Lbf/am;->D:Ljava/lang/String;

    if-eqz v1, :cond_26

    .line 2735
    iget-object v1, p0, Lbf/am;->D:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Lcom/google/googlenav/ui/r;Ljava/lang/String;)V

    .line 2739
    :cond_26
    return-void
.end method

.method public b(Z)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2010
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->a(I)Z

    .line 2013
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->c_(Z)V

    .line 2014
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, v2, v2}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    .line 2021
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 2022
    :goto_16
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_59

    .line 2023
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2024
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3a

    .line 2025
    if-eqz p1, :cond_36

    .line 2026
    invoke-virtual {p0}, Lbf/am;->U()V

    .line 2022
    :cond_32
    :goto_32
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 2028
    :cond_36
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 2032
    :cond_3a
    invoke-virtual {v0}, Lbf/i;->aB()Z

    move-result v4

    if-eqz v4, :cond_44

    .line 2033
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 2034
    :cond_44
    invoke-virtual {v0}, Lbf/i;->aj()Z

    move-result v4

    if-eqz v4, :cond_32

    .line 2035
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_55

    .line 2037
    invoke-virtual {v0}, Lbf/i;->al()V

    goto :goto_32

    .line 2041
    :cond_55
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_32

    .line 2045
    :cond_59
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2046
    invoke-virtual {p0, v0, v6, v6}, Lbf/am;->b(Lbf/i;ZZ)V

    goto :goto_5d

    .line 2051
    :cond_6d
    sget-object v0, LaE/g;->a:LaE/g;

    invoke-virtual {v0}, LaE/g;->e()Z

    move-result v0

    if-eqz v0, :cond_82

    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    if-eqz v0, :cond_82

    .line 2052
    invoke-virtual {p0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbf/aA;->k(Z)V

    .line 2057
    :cond_82
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_97

    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_97

    .line 2059
    invoke-virtual {p0}, Lbf/am;->p()V

    .line 2063
    :cond_97
    iget-object v0, p0, Lbf/am;->e:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    invoke-virtual {v0}, LaN/D;->l()V

    move v0, v2

    .line 2066
    :goto_a1
    sget-object v1, Lbf/am;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_ad

    .line 2067
    sget-object v1, Lbf/am;->u:[Z

    aput-boolean v2, v1, v0

    .line 2066
    add-int/lit8 v0, v0, 0x1

    goto :goto_a1

    .line 2070
    :cond_ad
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 2074
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/am;->d(Lbf/i;)V

    .line 2075
    return-void
.end method

.method public b(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 2144
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 2145
    if-eqz v0, :cond_e

    invoke-virtual {v0, p1}, Lbf/i;->b(Lat/a;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public c(Lcom/google/googlenav/aZ;)Lbf/bk;
    .registers 11
    .parameter

    .prologue
    .line 1187
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 1188
    invoke-static {}, Lbf/am;->W()I

    move-result v8

    .line 1189
    new-instance v0, Lbf/bk;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v8}, Lbf/bk;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V

    .line 1191
    return-object v0
.end method

.method public c(Lcom/google/googlenav/layer/m;Z)Lbf/y;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 1770
    new-instance v0, Lbf/bD;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lbf/bD;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    .line 1772
    invoke-virtual {p0, p1, p2, v0}, Lbf/am;->a(Lcom/google/googlenav/layer/m;ZLbf/y;)V

    .line 1773
    return-object v0
.end method

.method public c()V
    .registers 1

    .prologue
    .line 466
    return-void
.end method

.method public c(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2082
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v1, v2

    .line 2083
    :goto_6
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_29

    .line 2084
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2086
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v4

    if-ne v4, p1, :cond_25

    invoke-virtual {v0}, Lbf/i;->aD()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 2087
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2083
    :cond_25
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2090
    :cond_29
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 2091
    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lbf/am;->b(Lbf/i;ZZ)V

    .line 2092
    invoke-direct {p0, v0}, Lbf/am;->o(Lbf/i;)V

    goto :goto_2d

    .line 2094
    :cond_41
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 2095
    return-void
.end method

.method public c(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    .line 3073
    invoke-virtual {p0, p1}, Lbf/am;->a(Lcom/google/googlenav/F;)Lbf/aU;

    move-result-object v0

    .line 3075
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/aU;->a(Lax/b;I)V

    .line 3077
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 3317
    const-string v0, "PROTO_SAVED_LAYER_STATE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 3318
    if-eqz v0, :cond_10

    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->as()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method protected c(Z)Z
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2438
    iget v0, p0, Lbf/am;->O:I

    .line 2439
    invoke-virtual {p0}, Lbf/am;->D()I

    move-result v3

    iput v3, p0, Lbf/am;->O:I

    .line 2440
    iget v3, p0, Lbf/am;->O:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_49

    .line 2441
    if-nez p1, :cond_15

    iget v3, p0, Lbf/am;->O:I

    if-eq v0, v3, :cond_3b

    .line 2442
    :cond_15
    iget-object v0, p0, Lbf/am;->L:Ljava/util/Vector;

    iget v3, p0, Lbf/am;->O:I

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/ai;

    .line 2443
    invoke-virtual {v0, v2}, Lbf/ai;->a(Z)V

    .line 2445
    invoke-virtual {v0}, Lbf/ai;->f()Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 2448
    invoke-virtual {p0}, Lbf/am;->h()Lbf/C;

    move-result-object v0

    .line 2449
    if-nez v0, :cond_30

    move v0, v1

    .line 2460
    :goto_2f
    return v0

    .line 2452
    :cond_30
    invoke-virtual {v0}, Lbf/C;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;I)V

    :cond_3b
    :goto_3b
    move v0, v2

    .line 2458
    goto :goto_2f

    .line 2455
    :cond_3d
    invoke-virtual {v0}, Lbf/ai;->a()Lbf/i;

    move-result-object v1

    invoke-virtual {v0}, Lbf/ai;->c()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lbf/am;->a(Lbf/i;I)V

    goto :goto_3b

    :cond_49
    move v0, v1

    .line 2460
    goto :goto_2f
.end method

.method public d(Lcom/google/googlenav/aZ;)Lbf/aJ;
    .registers 11
    .parameter

    .prologue
    .line 1199
    invoke-direct {p0, p1}, Lbf/am;->g(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/n;

    move-result-object v5

    .line 1200
    new-instance v0, Lbf/aJ;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v6

    iget-object v7, p0, Lbf/am;->h:LaN/k;

    iget-object v8, p0, Lbf/am;->P:Lcom/google/googlenav/offers/j;

    invoke-direct/range {v0 .. v8}, Lbf/aJ;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;Lcom/google/googlenav/ui/view/dialog/bl;)V

    .line 1202
    return-object v0
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 773
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/am;->z:Z

    .line 774
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->e()V

    .line 775
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->V()V

    .line 776
    return-void
.end method

.method public d(I)V
    .registers 3
    .parameter

    .prologue
    .line 4181
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbf/am;->a(ILjava/lang/Object;)V

    .line 4182
    return-void
.end method

.method d(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 469
    iget-object v0, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 470
    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lbf/i;->aE()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 476
    :cond_10
    if-eqz p1, :cond_23

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_23

    const/4 v0, 0x1

    .line 478
    :goto_19
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/as;->a(Z)V

    .line 481
    :cond_22
    return-void

    .line 476
    :cond_23
    const/4 v0, 0x0

    goto :goto_19
.end method

.method protected d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2686
    iput-boolean p1, p0, Lbf/am;->M:Z

    .line 2687
    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 3734
    invoke-virtual {p0, p1}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/y;

    .line 3735
    if-eqz v0, :cond_d

    .line 3736
    invoke-virtual {v0}, Lbf/y;->bH()Z

    move-result v0

    .line 3738
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public e(Lcom/google/googlenav/aZ;)Lbf/X;
    .registers 13
    .parameter

    .prologue
    .line 1487
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    .line 1491
    new-instance v0, Lbf/X;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->n:Lcom/google/googlenav/android/aa;

    iget-object v5, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->k:Lcom/google/googlenav/friend/J;

    iget-object v7, p0, Lbf/am;->l:Lcom/google/googlenav/friend/p;

    iget-object v8, p0, Lbf/am;->m:Lcom/google/googlenav/friend/ag;

    iget-object v10, p0, Lbf/am;->G:Lbf/a;

    move-object v9, p1

    invoke-direct/range {v0 .. v10}, Lbf/X;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/F;Lbf/a;)V

    .line 1494
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1495
    invoke-virtual {p0, v0}, Lbf/am;->a(Lbf/X;)V

    .line 1496
    return-object v0
.end method

.method public e(Ljava/lang/String;)Lbf/i;
    .registers 6
    .parameter

    .prologue
    .line 3747
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_2f

    .line 3748
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 3749
    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v1

    const/4 v3, 0x6

    if-ne v1, v3, :cond_2b

    move-object v1, v0

    check-cast v1, Lbf/y;

    invoke-virtual {v1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 3754
    :goto_2a
    return-object v0

    .line 3747
    :cond_2b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3754
    :cond_2f
    const/4 v0, 0x0

    goto :goto_2a
.end method

.method public e()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1042
    const/16 v0, 0x12

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1043
    const/16 v0, 0xf

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1044
    return-void
.end method

.method public e(Lbf/i;)V
    .registers 5
    .parameter

    .prologue
    .line 488
    invoke-virtual {p0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 489
    if-eqz v0, :cond_2c

    .line 490
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 491
    invoke-virtual {v0}, Lbf/i;->aX()V

    .line 492
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 493
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->b(Lcom/google/googlenav/ui/Y;)V

    .line 494
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 495
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->a(Lcom/google/googlenav/ui/Y;)V

    .line 496
    invoke-virtual {p1}, Lbf/i;->aW()V

    .line 497
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 499
    invoke-virtual {p0, p1}, Lbf/am;->d(Lbf/i;)V

    .line 509
    :cond_2c
    :goto_2c
    return-void

    .line 500
    :cond_2d
    invoke-virtual {v0}, Lbf/i;->ax()Z

    move-result v1

    if-nez v1, :cond_37

    .line 501
    invoke-virtual {v0}, Lbf/i;->aW()V

    goto :goto_2c

    .line 502
    :cond_37
    invoke-virtual {v0}, Lbf/i;->k()Lcom/google/googlenav/settings/e;

    move-result-object v1

    if-eqz v1, :cond_2c

    invoke-virtual {v0}, Lbf/i;->k()Lcom/google/googlenav/settings/e;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v2

    if-eq v1, v2, :cond_2c

    .line 505
    const/4 v1, 0x1

    .line 506
    invoke-virtual {v0, v1}, Lbf/i;->g(Z)Z

    goto :goto_2c
.end method

.method public e(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3908
    iput-boolean p1, p0, Lbf/am;->s:Z

    .line 3909
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 1053
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1054
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1055
    return-void
.end method

.method public f(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 523
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lbf/am;->a(Lbf/i;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 524
    return-void
.end method

.method public f(Lcom/google/googlenav/aZ;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1504
    iget-object v0, p0, Lbf/am;->G:Lbf/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, v2, v2}, Lbf/a;->a(ILcom/google/googlenav/F;Lcom/google/googlenav/android/aa;Lbf/g;)V

    .line 1506
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 4306
    iput-object p1, p0, Lbf/am;->D:Ljava/lang/String;

    .line 4307
    return-void
.end method

.method public f(Z)V
    .registers 2
    .parameter

    .prologue
    .line 3912
    sput-boolean p1, Lbf/am;->v:Z

    .line 3913
    return-void
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 4323
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 4326
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    .line 4327
    :goto_a
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_25

    .line 4328
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 4329
    invoke-virtual {v0}, Lbf/i;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4327
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 4331
    :cond_25
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "visible layers"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4334
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 4335
    iget-object v5, p0, Lbf/am;->q:Ljava/util/Vector;

    monitor-enter v5

    move v1, v2

    .line 4336
    :goto_37
    :try_start_37
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_52

    .line 4337
    iget-object v0, p0, Lbf/am;->q:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 4338
    invoke-virtual {v0}, Lbf/i;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4336
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_37

    .line 4340
    :cond_52
    monitor-exit v5
    :try_end_53
    .catchall {:try_start_37 .. :try_end_53} :catchall_65

    .line 4341
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "recent layers"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4344
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v1, "LayerManager"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0

    .line 4340
    :catchall_65
    move-exception v0

    :try_start_66
    monitor-exit v5
    :try_end_67
    .catchall {:try_start_66 .. :try_end_67} :catchall_65

    throw v0
.end method

.method public g(Lbf/i;)V
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 673
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v1

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    .line 676
    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v1

    sparse-switch v1, :sswitch_data_b8

    .line 731
    :goto_10
    return-void

    .line 678
    :sswitch_11
    invoke-virtual {p0, v5}, Lbf/am;->e(Z)V

    .line 679
    check-cast p1, Lbf/bk;

    invoke-virtual {p1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    .line 680
    new-instance v2, Lcom/google/googlenav/aZ;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-direct {v3}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->z()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->W()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v3

    const-string v4, "13"

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v3

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-virtual {v4}, LaN/u;->f()LaN/H;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v3

    iget-object v4, p0, Lbf/am;->f:LaN/u;

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    .line 689
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->N()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aZ;->a(Z)V

    .line 690
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aN()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/aZ;->c(Z)V

    .line 692
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v3

    if-eqz v3, :cond_7b

    .line 695
    iget-object v3, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 699
    :cond_7b
    iget-object v3, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v1

    if-eqz v1, :cond_87

    :goto_83
    invoke-virtual {v3, v2, v0, v5}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_10

    :cond_87
    const/4 v0, 0x6

    goto :goto_83

    .line 708
    :sswitch_89
    invoke-virtual {p0, p1}, Lbf/am;->f(Lbf/i;)V

    .line 710
    invoke-virtual {p1, v5}, Lbf/i;->h(Z)V

    goto :goto_10

    .line 713
    :sswitch_90
    check-cast p1, Lbf/y;

    invoke-virtual {p1}, Lbf/y;->b()Lcom/google/googlenav/layer/m;

    move-result-object v1

    .line 715
    invoke-virtual {v1}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "msid:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_ab

    .line 716
    invoke-virtual {p0, v1, v0}, Lbf/am;->c(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v0

    .line 721
    :goto_a6
    invoke-virtual {v0, v5}, Lbf/y;->h(Z)V

    goto/16 :goto_10

    .line 718
    :cond_ab
    invoke-virtual {p0, v1, v0}, Lbf/am;->b(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    move-result-object v0

    goto :goto_a6

    .line 724
    :sswitch_b0
    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    goto/16 :goto_10

    .line 676
    nop

    :sswitch_data_b8
    .sparse-switch
        0x0 -> :sswitch_11
        0x1 -> :sswitch_89
        0x3 -> :sswitch_89
        0x6 -> :sswitch_90
        0x7 -> :sswitch_b0
        0x15 -> :sswitch_89
    .end sparse-switch
.end method

.method protected h()Lbf/C;
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 1299
    invoke-direct {p0}, Lbf/am;->ac()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    .line 1300
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    .line 1301
    if-eqz v1, :cond_1d

    .line 1302
    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->i()LaH/h;

    move-result-object v2

    invoke-static {v2}, LaH/h;->d(Landroid/location/Location;)Lo/D;

    move-result-object v2

    .line 1303
    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->g()[Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;

    move-result-object v3

    .line 1306
    :cond_1d
    return-object v3
.end method

.method public h(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 782
    if-nez p1, :cond_4

    .line 788
    :goto_3
    return-void

    .line 785
    :cond_4
    invoke-virtual {p0, p1, v0, v0}, Lbf/am;->b(Lbf/i;ZZ)V

    .line 786
    invoke-virtual {p0}, Lbf/am;->d()V

    .line 787
    const-string v0, "h"

    invoke-direct {p0, p1, v0}, Lbf/am;->a(Lbf/i;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public i()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1335
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1345
    invoke-virtual {p0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    .line 1346
    invoke-direct {p0, v2, v1}, Lbf/am;->b(IZ)V

    .line 1349
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1351
    invoke-virtual {p0, v2}, Lbf/am;->a(I)V

    .line 1352
    return-void
.end method

.method protected i(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 794
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/X;->b(Lcom/google/googlenav/ui/Y;)V

    .line 795
    return-void
.end method

.method public j()V
    .registers 4

    .prologue
    const/16 v2, 0x15

    const/4 v1, 0x0

    .line 1374
    const/16 v0, 0x1a

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1375
    invoke-direct {p0, v1, v1}, Lbf/am;->b(IZ)V

    .line 1376
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1377
    invoke-direct {p0, v2, v1}, Lbf/am;->b(IZ)V

    .line 1380
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1382
    invoke-virtual {p0, v2}, Lbf/am;->a(I)V

    .line 1383
    return-void
.end method

.method protected j(Lbf/i;)V
    .registers 7
    .parameter

    .prologue
    .line 889
    iget-object v2, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v2

    .line 890
    :try_start_3
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/au;

    .line 891
    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LR/a;

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, LR/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 892
    invoke-interface {v0, p1}, Lbf/au;->a(Lbf/i;)V

    goto :goto_d

    .line 895
    :catchall_2f
    move-exception v0

    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0

    :cond_32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_2f

    .line 896
    return-void
.end method

.method public k()V
    .registers 3

    .prologue
    .line 1404
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1405
    const/16 v0, 0x17

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1406
    return-void
.end method

.method protected k(Lbf/i;)V
    .registers 7
    .parameter

    .prologue
    .line 902
    iget-object v2, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v2

    .line 903
    :try_start_3
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/au;

    .line 904
    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LR/a;

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, LR/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 905
    invoke-interface {v0, p1}, Lbf/au;->b(Lbf/i;)V

    goto :goto_d

    .line 908
    :catchall_2f
    move-exception v0

    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0

    :cond_32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_2f

    .line 909
    return-void
.end method

.method public l()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1413
    invoke-direct {p0}, Lbf/am;->ab()V

    .line 1414
    const/16 v0, 0x18

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1415
    const/16 v0, 0x17

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1416
    return-void
.end method

.method protected l(Lbf/i;)V
    .registers 7
    .parameter

    .prologue
    .line 915
    iget-object v2, p0, Lbf/am;->H:Ljava/util/Map;

    monitor-enter v2

    .line 916
    :try_start_3
    iget-object v0, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/au;

    .line 917
    iget-object v1, p0, Lbf/am;->H:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LR/a;

    invoke-virtual {p1}, Lbf/i;->av()I

    move-result v4

    invoke-virtual {v1, v4}, LR/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 918
    invoke-interface {v0, p1}, Lbf/au;->c(Lbf/i;)V

    goto :goto_d

    .line 921
    :catchall_2f
    move-exception v0

    monitor-exit v2
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0

    :cond_32
    :try_start_32
    monitor-exit v2
    :try_end_33
    .catchall {:try_start_32 .. :try_end_33} :catchall_2f

    .line 922
    return-void
.end method

.method public m()Lbf/aA;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1510
    const/16 v0, 0x13

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1511
    const/16 v0, 0xd

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1513
    new-instance v0, Lbf/aA;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v0, v1, v2, v3, v4}, Lbf/aA;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    .line 1515
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1516
    return-object v0
.end method

.method public n()Lbf/aA;
    .registers 2

    .prologue
    .line 1525
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/aA;

    return-object v0
.end method

.method public n(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 4318
    iget-object v0, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->e()V

    .line 4319
    return-void
.end method

.method public o()V
    .registers 3

    .prologue
    .line 1530
    const/16 v0, 0x13

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1531
    return-void
.end method

.method public p()V
    .registers 3

    .prologue
    .line 1535
    const/16 v0, 0x16

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1536
    return-void
.end method

.method public q()Lbf/bE;
    .registers 3

    .prologue
    .line 1609
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 1615
    if-nez v0, :cond_11

    .line 1616
    new-instance v0, Lcom/google/googlenav/layer/m;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/googlenav/layer/m;-><init>(Ljava/lang/String;)V

    .line 1619
    :cond_11
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lbf/am;->a(I)V

    .line 1621
    invoke-virtual {p0, v0}, Lbf/am;->a(Lcom/google/googlenav/layer/m;)Lbf/bE;

    move-result-object v0

    .line 1626
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;Z)V

    .line 1627
    return-object v0
.end method

.method public r()V
    .registers 2

    .prologue
    .line 1639
    invoke-virtual {p0}, Lbf/am;->z()Lbf/bE;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/am;->h(Lbf/i;)V

    .line 1640
    return-void
.end method

.method public s()V
    .registers 9

    .prologue
    .line 1802
    const/16 v0, 0xb

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbf/am;->b(IZ)V

    .line 1804
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/f;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_12
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/googlenav/layer/m;

    .line 1805
    new-instance v0, Lbf/D;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    iget-object v6, p0, Lbf/am;->h:LaN/k;

    invoke-direct/range {v0 .. v6}, Lbf/D;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    .line 1807
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/am;->a(Lbf/i;Z)V

    goto :goto_12

    .line 1809
    :cond_32
    return-void
.end method

.method public t()Lbf/bx;
    .registers 6

    .prologue
    .line 1817
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lbf/am;->a(I)V

    .line 1819
    new-instance v0, Lbf/bx;

    iget-object v1, p0, Lbf/am;->c:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/am;->e:LaN/p;

    iget-object v3, p0, Lbf/am;->f:LaN/u;

    iget-object v4, p0, Lbf/am;->g:Lcom/google/googlenav/ui/X;

    invoke-direct {v0, v1, v2, v3, v4}, Lbf/bx;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    .line 1821
    invoke-virtual {p0, v0}, Lbf/am;->f(Lbf/i;)V

    .line 1822
    return-object v0
.end method

.method public u()Lbf/bk;
    .registers 3

    .prologue
    .line 1871
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_26

    .line 1872
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    if-nez v0, :cond_22

    .line 1873
    iget-object v0, p0, Lbf/am;->i:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/bk;

    .line 1876
    :goto_21
    return-object v0

    .line 1871
    :cond_22
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    .line 1876
    :cond_26
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public v()Lbf/O;
    .registers 2

    .prologue
    .line 1885
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/O;

    return-object v0
.end method

.method public w()Lbf/bK;
    .registers 2

    .prologue
    .line 1894
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bK;

    return-object v0
.end method

.method public x()Lbf/bU;
    .registers 2

    .prologue
    .line 1903
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bU;

    return-object v0
.end method

.method public y()Lbf/by;
    .registers 2

    .prologue
    .line 1910
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/by;

    return-object v0
.end method

.method public z()Lbf/bE;
    .registers 2

    .prologue
    .line 1919
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lbf/am;->f(I)Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bE;

    return-object v0
.end method
