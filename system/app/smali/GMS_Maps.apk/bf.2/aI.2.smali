.class public Lbf/aI;
.super Lbf/aZ;
.source "SourceFile"


# instance fields
.field protected final d:LaB/s;


# direct methods
.method public constructor <init>(Lbf/i;LaB/s;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lbf/aZ;-><init>(Lbf/i;)V

    .line 22
    iput-object p2, p0, Lbf/aI;->d:LaB/s;

    .line 23
    return-void
.end method

.method private f(Lcom/google/googlenav/ai;)Z
    .registers 3
    .parameter

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;)Lbj/F;
    .registers 5
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lbf/aI;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 29
    new-instance v0, Lcom/google/googlenav/ui/ay;

    invoke-virtual {p0}, Lbf/aI;->c()Lbf/m;

    move-result-object v1

    iget-object v2, p0, Lbf/aI;->d:LaB/s;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/googlenav/ui/ay;-><init>(Lcom/google/googlenav/ai;Lbf/m;LaB/s;)V

    .line 34
    :goto_11
    return-object v0

    :cond_12
    invoke-super {p0, p1}, Lbf/aZ;->a(Lcom/google/googlenav/ai;)Lbj/F;

    move-result-object v0

    goto :goto_11
.end method

.method protected a(Lcom/google/googlenav/ai;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0, p1}, Lbf/aI;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 60
    invoke-virtual {p0, p2, v1, v1}, Lbf/aI;->a(Landroid/view/View;Lam/f;Lam/f;)V

    .line 64
    :goto_a
    return-void

    .line 62
    :cond_b
    invoke-super {p0, p1, p2}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Landroid/view/View;)V

    goto :goto_a
.end method

.method protected b(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lbf/aI;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 52
    const/4 v0, 0x0

    .line 54
    :goto_7
    return-object v0

    :cond_8
    invoke-super {p0, p1}, Lbf/aZ;->b(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method
