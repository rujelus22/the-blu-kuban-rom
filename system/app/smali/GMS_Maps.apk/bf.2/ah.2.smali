.class public Lbf/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/au;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private final b:Lcom/google/android/maps/driveabout/vector/aZ;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lcom/google/android/maps/driveabout/vector/aZ;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-boolean v0, p0, Lbf/ah;->c:Z

    .line 21
    iput-boolean v0, p0, Lbf/ah;->d:Z

    .line 24
    iput-object p1, p0, Lbf/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 25
    iput-object p2, p0, Lbf/ah;->b:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 26
    return-void
.end method

.method private declared-synchronized a()V
    .registers 4

    .prologue
    .line 97
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lbf/ah;->c:Z

    if-eqz v0, :cond_f

    .line 98
    iget-object v0, p0, Lbf/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/ah;->c:Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 101
    :cond_f
    monitor-exit p0

    return-void

    .line 97
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 39
    return-void
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 32
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lbf/ah;->c:Z

    iget-object v1, p0, Lbf/ah;->b:Lcom/google/android/maps/driveabout/vector/aZ;

    new-instance v2, Lo/w;

    invoke-direct {v2}, Lo/w;-><init>()V

    invoke-virtual {v2, p1}, Lo/w;->a(Ljava/lang/String;)Lo/w;

    move-result-object v2

    invoke-virtual {v2}, Lo/w;->a()Lo/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lo/at;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lbf/ah;->c:Z
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    .line 34
    monitor-exit p0

    return-void

    .line 32
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 62
    iput-boolean p1, p0, Lbf/ah;->d:Z

    .line 63
    return-void
.end method

.method public b(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    iget-boolean v0, p0, Lbf/ah;->d:Z

    if-eqz v0, :cond_5

    .line 55
    :cond_4
    :goto_4
    return-void

    .line 48
    :cond_5
    instance-of v0, p1, Lbf/bk;

    if-eqz v0, :cond_4

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/ah;->a(Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lbf/ah;->a()V

    goto :goto_4
.end method

.method public c(Lbf/i;)V
    .registers 5
    .parameter

    .prologue
    .line 68
    instance-of v0, p1, Lbf/bk;

    if-nez v0, :cond_5

    .line 93
    :cond_4
    :goto_4
    return-void

    .line 74
    :cond_5
    iget-object v0, p0, Lbf/ah;->a:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->z()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v0

    sget-object v1, LA/c;->a:LA/c;

    if-ne v0, v1, :cond_4

    .line 78
    check-cast p1, Lbf/bk;

    invoke-virtual {p1}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 82
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 83
    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ap()I

    move-result v1

    .line 84
    invoke-virtual {v0, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 85
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ch()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    invoke-virtual {p0, v1}, Lbf/ah;->a(Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Lbf/ah;->a()V

    goto :goto_4
.end method
