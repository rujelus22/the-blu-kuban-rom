.class public Lbf/by;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaM/h;
.implements Lbf/k;


# instance fields
.field protected B:Ljava/lang/String;

.field private C:Lcom/google/googlenav/ui/wizard/dy;

.field private D:I


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 98
    invoke-direct {p0, p1, p2, p3, p4}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/by;->C:Lcom/google/googlenav/ui/wizard/dy;

    .line 70
    iput v3, p0, Lbf/by;->D:I

    .line 99
    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    .line 101
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->b()LaR/aa;

    move-result-object v1

    .line 102
    new-instance v2, Lcom/google/googlenav/bz;

    invoke-direct {v2, v0, v1, p0}, Lcom/google/googlenav/bz;-><init>(LaR/u;LaR/aa;Lbf/k;)V

    iput-object v2, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    .line 107
    invoke-virtual {p0, v3}, Lbf/by;->i(Z)V

    .line 108
    return-void
.end method

.method private bK()V
    .registers 4

    .prologue
    .line 529
    const/4 v0, 0x1

    .line 530
    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lbf/bC;

    invoke-direct {v2, p0}, Lbf/bC;-><init>(Lbf/by;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 540
    return-void
.end method


# virtual methods
.method public C_()V
    .registers 1

    .prologue
    .line 551
    invoke-direct {p0}, Lbf/by;->bK()V

    .line 552
    return-void
.end method

.method public D_()V
    .registers 1

    .prologue
    .line 556
    invoke-direct {p0}, Lbf/by;->bK()V

    .line 557
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 567
    return-void
.end method

.method public F()I
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 594
    invoke-virtual {p0}, Lbf/by;->az()Z

    move-result v1

    if-eqz v1, :cond_b

    iget v1, p0, Lbf/by;->D:I

    if-gez v1, :cond_c

    .line 598
    :cond_b
    :goto_b
    return v0

    .line 597
    :cond_c
    invoke-virtual {p0, v0}, Lbf/by;->i(Z)V

    .line 598
    iget v0, p0, Lbf/by;->D:I

    goto :goto_b
.end method

.method public M_()V
    .registers 1

    .prologue
    .line 546
    invoke-direct {p0}, Lbf/by;->bK()V

    .line 547
    return-void
.end method

.method public N_()V
    .registers 1

    .prologue
    .line 562
    return-void
.end method

.method protected O()Z
    .registers 2

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .registers 2

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 345
    invoke-super {p0}, Lbf/m;->X()Z

    move-result v1

    .line 350
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bz;->a(Z)Z

    move-result v2

    .line 352
    if-nez v1, :cond_11

    if-eqz v2, :cond_12

    :cond_11
    const/4 v0, 0x1

    :cond_12
    return v0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .registers 3
    .parameter

    .prologue
    .line 189
    iput-object p1, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    .line 190
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/by;->b(I)V

    .line 191
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 496
    invoke-virtual {p0, p2}, Lbf/by;->b(Ljava/lang/String;)I

    move-result v0

    .line 497
    invoke-virtual {p0}, Lbf/by;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    .line 498
    if-eqz v1, :cond_28

    .line 499
    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    .line 500
    iget-object v2, p0, Lbf/by;->c:LaN/p;

    iget-object v3, p0, Lbf/by;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 501
    invoke-virtual {p0, v0}, Lbf/by;->c(I)Lcom/google/googlenav/e;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lbf/by;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v0, p1, v1, v2}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/r;II)V

    .line 504
    :cond_28
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 306
    invoke-virtual {p0}, Lbf/by;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bz;

    .line 307
    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 308
    const/4 v2, -0x1

    if-eq v0, v2, :cond_30

    .line 309
    invoke-virtual {p0, v0}, Lbf/by;->b(I)V

    .line 310
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->e(Lbf/i;)V

    .line 311
    invoke-virtual {p0}, Lbf/by;->bH()V

    .line 317
    iput v1, p0, Lbf/by;->D:I

    .line 319
    if-eqz p2, :cond_31

    .line 320
    const/16 v0, 0xe

    .line 322
    :goto_23
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/by;->a(ILjava/lang/Object;)V

    .line 325
    invoke-virtual {p0}, Lbf/by;->bJ()LaR/n;

    move-result-object v0

    const-string v1, "s"

    invoke-interface {v0, v1, p1}, LaR/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_30
    return-void

    :cond_31
    move v0, v1

    goto :goto_23
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 217
    sparse-switch p1, :sswitch_data_2c

    .line 230
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_8
    return v0

    .line 219
    :sswitch_9
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 220
    invoke-virtual {p0}, Lbf/by;->an()Z

    .line 221
    invoke-virtual {p0, v0}, Lbf/by;->b(Z)V

    goto :goto_8

    .line 223
    :cond_1a
    iget-object v1, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbf/by;->a(Ljava/lang/Object;)V

    goto :goto_8

    .line 227
    :sswitch_28
    invoke-virtual {p0}, Lbf/by;->W()V

    goto :goto_8

    .line 217
    :sswitch_data_2c
    .sparse-switch
        0x1 -> :sswitch_9
        0x3f9 -> :sswitch_28
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 4
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, Lbf/by;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbf/by;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_10

    .line 236
    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/by;->a(ILjava/lang/Object;)V

    .line 240
    const/4 v0, 0x1

    :goto_f
    return v0

    .line 238
    :cond_10
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_f
.end method

.method public aE()Z
    .registers 2

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public aU()V
    .registers 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bz;->a()V

    .line 113
    invoke-super {p0}, Lbf/m;->aU()V

    .line 114
    return-void
.end method

.method public aX()V
    .registers 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lbf/by;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 120
    if-eqz v0, :cond_e

    .line 121
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/by;->B:Ljava/lang/String;

    .line 123
    :cond_e
    invoke-super {p0}, Lbf/m;->aX()V

    .line 124
    return-void
.end method

.method protected am()V
    .registers 4

    .prologue
    .line 359
    const/4 v0, 0x0

    .line 360
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bz;->a(Ljava/lang/Runnable;)V

    .line 362
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LaR/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    return-void
.end method

.method protected aq()V
    .registers 5

    .prologue
    .line 430
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "stars"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    .line 432
    return-void
.end method

.method public av()I
    .registers 2

    .prologue
    .line 464
    const/4 v0, 0x2

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 447
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;)I
    .registers 3
    .parameter

    .prologue
    .line 492
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected bC()I
    .registers 3

    .prologue
    .line 508
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 509
    if-gez v0, :cond_d

    .line 511
    const/4 v0, -0x2

    .line 513
    :cond_d
    return v0
.end method

.method protected bG()V
    .registers 2

    .prologue
    .line 340
    invoke-virtual {p0}, Lbf/by;->bC()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/by;->j(I)V

    .line 341
    return-void
.end method

.method public bH()V
    .registers 3

    .prologue
    .line 481
    invoke-virtual {p0}, Lbf/by;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 482
    if-eqz v0, :cond_18

    .line 483
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    .line 484
    if-eqz v0, :cond_18

    .line 485
    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->R()V

    .line 486
    iget-object v1, p0, Lbf/by;->d:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    .line 489
    :cond_18
    return-void
.end method

.method public bI()Lcom/google/googlenav/bz;
    .registers 2

    .prologue
    .line 517
    invoke-virtual {p0}, Lbf/by;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bz;

    return-object v0
.end method

.method public bJ()LaR/n;
    .registers 2

    .prologue
    .line 521
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    return-object v0
.end method

.method public bj()Z
    .registers 2

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public bk()Z
    .registers 2

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method protected bv()Z
    .registers 2

    .prologue
    .line 459
    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .registers 3
    .parameter

    .prologue
    .line 453
    invoke-virtual {p0, p1}, Lbf/by;->f(Lcom/google/googlenav/E;)I

    move-result v0

    .line 454
    if-eqz v0, :cond_b

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x2

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public c(Lbf/i;)V
    .registers 5
    .parameter

    .prologue
    .line 157
    const/4 v0, 0x1

    .line 158
    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lbf/bz;

    invoke-direct {v2, p0}, Lbf/bz;-><init>(Lbf/by;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 167
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 575
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    .line 576
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1d

    .line 577
    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1, p0}, Lbf/am;->e(Lbf/i;)V

    .line 578
    invoke-virtual {p0, v0}, Lbf/by;->b(I)V

    .line 579
    invoke-virtual {p0}, Lbf/by;->an()Z

    .line 580
    invoke-virtual {p0}, Lbf/by;->bH()V

    .line 584
    :cond_1d
    return-void
.end method

.method public e(ILjava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 440
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "stars"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    .line 442
    return-void
.end method

.method protected f(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 200
    invoke-virtual {p0}, Lbf/by;->ah()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 201
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1b

    invoke-virtual {p0}, Lbf/by;->ag()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 202
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/by;->a(ILjava/lang/Object;)V

    .line 212
    :goto_1a
    return v0

    .line 207
    :cond_1b
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_33

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 209
    invoke-virtual {p0}, Lbf/by;->h()V

    goto :goto_1a

    .line 212
    :cond_33
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public g(I)Z
    .registers 3
    .parameter

    .prologue
    .line 469
    packed-switch p1, :pswitch_data_a

    .line 476
    invoke-super {p0, p1}, Lbf/m;->g(I)Z

    move-result v0

    :goto_7
    return v0

    .line 473
    :pswitch_8
    const/4 v0, 0x0

    goto :goto_7

    .line 469
    :pswitch_data_a
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 250
    invoke-virtual {p0}, Lbf/by;->bz()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 293
    :cond_8
    :goto_8
    return-void

    .line 255
    :cond_9
    iput-object v3, p0, Lbf/by;->C:Lcom/google/googlenav/ui/wizard/dy;

    .line 257
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_8c

    .line 291
    invoke-virtual {p0}, Lbf/by;->n()V

    goto :goto_8

    .line 260
    :sswitch_20
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lbf/by;->b(ILjava/lang/Object;)V

    goto :goto_8

    .line 264
    :sswitch_2e
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 265
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/by;->j(I)V

    goto :goto_8

    .line 269
    :sswitch_4a
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/by;->i(Z)V

    .line 270
    invoke-virtual {p0}, Lbf/by;->l()V

    goto :goto_8

    .line 273
    :sswitch_52
    invoke-virtual {p0}, Lbf/by;->n()V

    .line 274
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->j()V

    goto :goto_8

    .line 277
    :sswitch_5f
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v0

    const/16 v1, 0x32c

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(IILjava/lang/Object;)Z

    .line 279
    invoke-virtual {p0}, Lbf/by;->n()V

    goto :goto_8

    .line 282
    :sswitch_76
    invoke-virtual {p0}, Lbf/by;->Z()V

    .line 283
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->i(Ljava/lang/String;)V

    goto :goto_8

    .line 286
    :sswitch_81
    invoke-virtual {p0}, Lbf/by;->Z()V

    .line 288
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->y()V

    goto/16 :goto_8

    .line 258
    nop

    :sswitch_data_8c
    .sparse-switch
        0x6 -> :sswitch_76
        0x7 -> :sswitch_2e
        0x9 -> :sswitch_20
        0xe -> :sswitch_4a
        0x10 -> :sswitch_52
        0x16 -> :sswitch_5f
        0x1c -> :sswitch_81
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 369
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v1

    .line 370
    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/google/googlenav/by;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    .line 398
    :cond_19
    :goto_19
    return v0

    .line 374
    :cond_1a
    invoke-virtual {v1}, Lcom/google/googlenav/by;->i()Z

    move-result v1

    if-nez v1, :cond_19

    .line 380
    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    new-instance v1, Lbf/bA;

    invoke-direct {v1, p0}, Lbf/bA;-><init>(Lbf/by;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bz;->a(Ljava/lang/Runnable;)V

    .line 398
    const/4 v0, 0x1

    goto :goto_19
.end method

.method protected i()Lbh/a;
    .registers 2

    .prologue
    .line 245
    new-instance v0, Lbh/j;

    invoke-direct {v0, p0}, Lbh/j;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected j(I)V
    .registers 4
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_18

    .line 332
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->a(I)V

    .line 333
    invoke-virtual {p0}, Lbf/by;->an()Z

    .line 334
    invoke-virtual {p0}, Lbf/by;->bH()V

    .line 335
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/by;->a(ILjava/lang/Object;)V

    .line 337
    :cond_18
    return-void
.end method

.method protected m()V
    .registers 4

    .prologue
    .line 403
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    .line 406
    if-nez v0, :cond_b

    .line 415
    :goto_a
    return-void

    .line 410
    :cond_b
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    .line 411
    iget-object v2, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v2

    invoke-interface {v2, v1}, LaR/n;->a(Ljava/lang/String;)V

    .line 412
    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v1

    const-string v2, "o"

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LaR/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-super {p0}, Lbf/m;->m()V

    goto :goto_a
.end method

.method protected n()V
    .registers 2

    .prologue
    .line 419
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ltz v0, :cond_c

    .line 420
    invoke-super {p0}, Lbf/m;->n()V

    .line 424
    :goto_b
    return-void

    .line 422
    :cond_c
    invoke-virtual {p0}, Lbf/by;->Z()V

    goto :goto_b
.end method

.method public s()Lcom/google/googlenav/E;
    .registers 2

    .prologue
    .line 195
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method
