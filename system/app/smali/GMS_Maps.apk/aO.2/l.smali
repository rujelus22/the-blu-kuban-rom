.class LaO/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/f;


# instance fields
.field final synthetic a:LaO/b;


# direct methods
.method private constructor <init>(LaO/b;)V
    .registers 2
    .parameter

    .prologue
    .line 1249
    iput-object p1, p0, LaO/l;->a:LaO/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LaO/b;LaO/c;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1249
    invoke-direct {p0, p1}, LaO/l;-><init>(LaO/b;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/e;Lcom/google/android/maps/driveabout/vector/d;)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 1252
    instance-of v0, p2, Lcom/google/android/maps/driveabout/vector/ac;

    if-eqz v0, :cond_d7

    .line 1253
    iget-object v0, p0, LaO/l;->a:LaO/b;

    invoke-static {v0}, LaO/b;->i(LaO/b;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1254
    :try_start_d
    iget-object v0, p0, LaO/l;->a:LaO/b;

    invoke-static {v0, p2}, LaO/b;->a(LaO/b;Lcom/google/android/maps/driveabout/vector/d;)Lcom/google/android/maps/driveabout/vector/d;

    .line 1255
    check-cast p2, Lcom/google/android/maps/driveabout/vector/ac;

    .line 1256
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v0

    invoke-virtual {v0}, Lo/R;->a()Lo/m;

    move-result-object v0

    invoke-virtual {v0}, Lo/m;->a()Ljava/lang/String;

    move-result-object v0

    .line 1257
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v1

    invoke-virtual {v1}, Lo/R;->b()Lo/am;

    move-result-object v1

    invoke-virtual {v1}, Lo/am;->k()Lo/av;

    move-result-object v2

    .line 1259
    sget-object v1, Lo/aq;->c:Lo/aq;

    invoke-virtual {v2, v1}, Lo/av;->b(Lo/aq;)Z

    move-result v4

    .line 1263
    new-instance v1, Lcom/google/googlenav/W;

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->n()Ljava/lang/String;

    move-result-object v5

    if-eqz v4, :cond_44

    const-string v8, ":"

    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_44

    const-string v0, ""

    :cond_44
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v8

    invoke-virtual {v8}, Lo/R;->c()Lo/Q;

    move-result-object v8

    invoke-static {v8}, Lu/e;->b(Lo/Q;)Lau/B;

    move-result-object v8

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v9

    invoke-virtual {v9}, Lo/R;->d()Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0xa

    const/16 v11, 0x20

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v5, v0, v8, v9}, Lcom/google/googlenav/W;-><init>(Ljava/lang/String;Ljava/lang/String;Lau/B;Ljava/lang/String;)V

    .line 1267
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v0

    invoke-virtual {v0}, Lo/R;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_78

    .line 1269
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v0

    invoke-virtual {v0}, Lo/R;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(Ljava/lang/String;)V

    .line 1274
    :cond_78
    if-eqz v4, :cond_89

    .line 1275
    sget-object v0, Lo/aq;->c:Lo/aq;

    invoke-virtual {v2, v0}, Lo/av;->a(Lo/aq;)Lo/ao;

    move-result-object v0

    check-cast v0, Lo/C;

    .line 1276
    invoke-virtual {v0}, Lo/C;->c()Lo/B;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(Lo/B;)V

    .line 1279
    :cond_89
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v0

    invoke-virtual {v0}, Lo/R;->u()Z

    move-result v0

    if-eqz v0, :cond_97

    .line 1280
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->b(Z)V

    .line 1283
    :cond_97
    const/16 v0, 0x14

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->a(B)V

    .line 1288
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v0

    invoke-virtual {v0}, Lo/R;->g()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->f(I)V

    .line 1290
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/ac;->m()Lo/R;

    move-result-object v0

    invoke-virtual {v0}, Lo/R;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/W;->c(Ljava/lang/String;)V

    .line 1291
    const/4 v2, 0x1

    .line 1292
    const/4 v5, 0x0

    .line 1293
    const/4 v4, 0x0

    .line 1294
    iget-object v0, p0, LaO/l;->a:LaO/b;

    invoke-virtual {v0}, LaO/b;->J()LaN/i;

    move-result-object v0

    invoke-virtual {v0}, LaN/i;->ai()Z

    move-result v0

    if-eqz v0, :cond_ce

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_ce

    move v6, v3

    .line 1299
    :cond_ce
    iget-object v0, p0, LaO/l;->a:LaO/b;

    if-eqz v6, :cond_d3

    const/4 v3, 0x2

    :cond_d3
    invoke-virtual/range {v0 .. v5}, LaO/b;->a(Lcom/google/googlenav/ai;ZBZZ)LaN/ak;

    .line 1311
    monitor-exit v7

    .line 1313
    :cond_d7
    return-void

    .line 1311
    :catchall_d8
    move-exception v0

    monitor-exit v7
    :try_end_da
    .catchall {:try_start_d .. :try_end_da} :catchall_d8

    throw v0
.end method
