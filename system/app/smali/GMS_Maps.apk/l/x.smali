.class public LL/x;
.super LL/q;
.source "SourceFile"


# instance fields
.field private final b:LM/C;


# direct methods
.method public constructor <init>(LM/C;)V
    .registers 3
    .parameter

    .prologue
    .line 247
    invoke-virtual {p1}, LM/C;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LL/q;-><init>(Ljava/lang/String;)V

    .line 248
    iput-object p1, p0, LL/x;->b:LM/C;

    .line 249
    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Node;)V
    .registers 9
    .parameter

    .prologue
    .line 257
    invoke-direct {p0, p1}, LL/q;-><init>(Lorg/w3c/dom/Node;)V

    .line 258
    new-instance v0, LM/C;

    const-string v1, "provider"

    invoke-static {p1, v1}, LL/x;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "time"

    invoke-static {p1, v2}, LL/x;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "orientation"

    invoke-static {p1, v4}, LL/x;->a(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    const-string v5, "pitch"

    const-string v6, "45"

    invoke-static {p1, v5, v6}, LL/x;->a(Lorg/w3c/dom/Node;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v5

    invoke-direct/range {v0 .. v5}, LM/C;-><init>(Ljava/lang/String;JFF)V

    iput-object v0, p0, LL/x;->b:LM/C;

    .line 262
    return-void
.end method


# virtual methods
.method protected a(Ljava/io/Writer;)V
    .registers 5
    .parameter

    .prologue
    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<orientation provider=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/x;->a:Ljava/lang/String;

    invoke-static {v1}, LL/k;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' time=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/x;->b:LM/C;

    invoke-virtual {v1}, LM/C;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' orientation=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/x;->b:LM/C;

    invoke-virtual {v1}, LM/C;->c()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' pitch=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/x;->b:LM/C;

    invoke-virtual {v1}, LM/C;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 269
    return-void
.end method

.method public f()LM/C;
    .registers 2

    .prologue
    .line 272
    iget-object v0, p0, LL/x;->b:LM/C;

    return-object v0
.end method
