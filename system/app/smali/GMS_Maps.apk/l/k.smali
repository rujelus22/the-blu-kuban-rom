.class public abstract LL/k;
.super Ll/f;
.source "SourceFile"


# instance fields
.field protected b:Ljava/lang/String;

.field private c:LM/b;

.field private d:Landroid/location/Location;

.field private e:Landroid/location/Location;

.field private f:Landroid/location/Location;

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 78
    invoke-direct {p0, p1}, Ll/f;-><init>(Landroid/content/Context;)V

    .line 73
    const/16 v0, 0x3e8

    iput v0, p0, LL/k;->g:I

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LL/k;->h:J

    .line 79
    return-void
.end method

.method static synthetic a(LL/k;)J
    .registers 3
    .parameter

    .prologue
    .line 57
    iget-wide v0, p0, LL/k;->h:J

    return-wide v0
.end method

.method static synthetic a(LL/k;J)J
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 57
    iput-wide p1, p0, LL/k;->h:J

    return-wide p1
.end method

.method static synthetic a(LL/k;Landroid/location/Location;)Landroid/location/Location;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, LL/k;->d:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic b(LL/k;)I
    .registers 2
    .parameter

    .prologue
    .line 57
    iget v0, p0, LL/k;->g:I

    return v0
.end method

.method static synthetic c(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()Landroid/location/Location;
    .registers 4

    .prologue
    .line 1228
    invoke-virtual {p0}, LL/k;->a()Ljava/util/List;

    move-result-object v0

    .line 1229
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll/j;

    .line 1230
    instance-of v2, v0, LL/p;

    if-eqz v2, :cond_8

    .line 1231
    check-cast v0, LL/p;

    .line 1232
    invoke-virtual {v0}, LL/p;->f()Landroid/location/Location;

    move-result-object v0

    .line 1235
    :goto_1e
    return-object v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method static synthetic i(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j()I
    .registers 4

    .prologue
    .line 1243
    invoke-virtual {p0}, LL/k;->a()Ljava/util/List;

    move-result-object v0

    .line 1244
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll/j;

    .line 1245
    instance-of v2, v0, LL/A;

    if-eqz v2, :cond_8

    .line 1246
    new-instance v2, Lcom/google/android/maps/driveabout/app/bn;

    check-cast v0, LL/A;

    invoke-virtual {v0}, LL/A;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/maps/driveabout/app/bn;-><init>(Ljava/lang/String;)V

    .line 1247
    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bn;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1248
    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/bn;->e()I

    move-result v0

    .line 1252
    :goto_2d
    return v0

    :cond_2e
    const/4 v0, 0x0

    goto :goto_2d
.end method

.method static synthetic j(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-static {p0}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/File;)V
    .registers 3
    .parameter

    .prologue
    .line 1174
    iget-object v0, p0, LL/k;->d:Landroid/location/Location;

    iput-object v0, p0, LL/k;->f:Landroid/location/Location;

    .line 1175
    invoke-direct {p0}, LL/k;->i()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, LL/k;->e:Landroid/location/Location;

    .line 1176
    invoke-super {p0, p1}, Ll/f;->a(Ljava/io/File;)V

    .line 1177
    return-void
.end method

.method protected b(Ljava/io/BufferedWriter;)V
    .registers 4
    .parameter

    .prologue
    .line 1158
    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_26

    .line 1159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<destination uri=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LL/k;->b:Ljava/lang/String;

    invoke-static {v1}, LL/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1161
    :cond_26
    const-string v0, "</event-log>\n"

    invoke-virtual {p1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1162
    return-void
.end method

.method public b(Ljava/io/File;)V
    .registers 9
    .parameter

    .prologue
    const-wide v3, 0x3eb0c6f7a0b5ed8dL

    .line 1189
    const/4 v5, 0x0

    .line 1190
    invoke-virtual {p0}, LL/k;->h()LO/U;

    move-result-object v0

    .line 1191
    if-eqz v0, :cond_33

    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v1

    if-eqz v1, :cond_33

    .line 1192
    new-instance v5, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v5, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1193
    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v1

    invoke-virtual {v1}, Lo/u;->a()I

    move-result v1

    int-to-double v1, v1

    mul-double/2addr v1, v3

    invoke-virtual {v5, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 1194
    invoke-virtual {v0}, LO/U;->c()Lo/u;

    move-result-object v0

    invoke-virtual {v0}, Lo/u;->b()I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v3

    invoke-virtual {v5, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 1197
    :cond_33
    new-instance v0, Ll/b;

    iget-object v1, p0, LL/k;->a:Landroid/content/Context;

    iget-object v3, p0, LL/k;->e:Landroid/location/Location;

    iget-object v4, p0, LL/k;->f:Landroid/location/Location;

    invoke-direct {p0}, LL/k;->j()I

    move-result v6

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Ll/b;-><init>(Landroid/content/Context;Ljava/io/File;Landroid/location/Location;Landroid/location/Location;Landroid/location/Location;I)V

    .line 1199
    invoke-virtual {v0}, Ll/b;->a()V

    .line 1200
    return-void
.end method

.method public f()LM/b;
    .registers 2

    .prologue
    .line 1115
    iget-object v0, p0, LL/k;->c:LM/b;

    if-nez v0, :cond_b

    .line 1116
    new-instance v0, LL/l;

    invoke-direct {v0, p0}, LL/l;-><init>(LL/k;)V

    iput-object v0, p0, LL/k;->c:LM/b;

    .line 1152
    :cond_b
    iget-object v0, p0, LL/k;->c:LM/b;

    return-object v0
.end method

.method public g()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 1204
    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1205
    const/4 v0, 0x0

    .line 1207
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_5
.end method

.method public h()LO/U;
    .registers 3

    .prologue
    .line 1212
    iget-object v0, p0, LL/k;->b:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 1213
    const/4 v0, 0x0

    .line 1216
    :goto_5
    return-object v0

    .line 1215
    :cond_6
    new-instance v0, Lcom/google/android/maps/driveabout/app/bn;

    iget-object v1, p0, LL/k;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/bn;-><init>(Ljava/lang/String;)V

    .line 1216
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bn;->b()LO/U;

    move-result-object v0

    goto :goto_5
.end method
