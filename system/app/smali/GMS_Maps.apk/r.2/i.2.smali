.class Lr/i;
.super LR/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lr/d;

.field private volatile b:Z

.field private volatile c:Z


# direct methods
.method constructor <init>(Lr/d;)V
    .registers 4
    .parameter

    .prologue
    .line 1019
    iput-object p1, p0, Lr/i;->a:Lr/d;

    .line 1020
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheCommitter:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lr/d;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 1021
    invoke-static {p1}, Lr/d;->d(Lr/d;)I

    move-result v0

    if-gez v0, :cond_26

    .line 1022
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/i;->c:Z

    .line 1026
    :goto_25
    return-void

    .line 1025
    :cond_26
    invoke-virtual {p0}, Lr/i;->start()V

    goto :goto_25
.end method


# virtual methods
.method a()V
    .registers 2

    .prologue
    .line 1075
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/i;->b:Z

    .line 1076
    return-void
.end method

.method b()Z
    .registers 2

    .prologue
    .line 1079
    iget-boolean v0, p0, Lr/i;->c:Z

    return v0
.end method

.method public l()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 1033
    :try_start_1
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_a
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_a} :catch_15

    .line 1037
    :goto_a
    iget-object v0, p0, Lr/i;->a:Lr/d;

    iget-object v0, v0, Lr/d;->b:Lr/B;

    invoke-virtual {v0}, Lr/B;->b()Lt/f;

    move-result-object v1

    .line 1038
    if-nez v1, :cond_31

    .line 1072
    :goto_14
    return-void

    .line 1034
    :catch_15
    move-exception v0

    .line 1035
    invoke-virtual {p0}, Lr/i;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 1042
    :cond_31
    iget-boolean v0, p0, Lr/i;->b:Z

    if-nez v0, :cond_4e

    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0}, Lr/d;->e(Lr/d;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 1046
    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0, v4}, Lr/d;->a(Lr/d;Z)Z

    .line 1047
    invoke-interface {v1}, Lt/f;->f_()V

    .line 1070
    :goto_45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lr/i;->c:Z

    .line 1071
    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0}, Lr/d;->f(Lr/d;)V

    goto :goto_14

    .line 1050
    :cond_4e
    iput-boolean v4, p0, Lr/i;->b:Z

    .line 1052
    :try_start_50
    iget-object v0, p0, Lr/i;->a:Lr/d;

    invoke-static {v0}, Lr/d;->d(Lr/d;)I

    move-result v0

    .line 1053
    :goto_56
    if-lez v0, :cond_65

    .line 1054
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Lr/i;->sleep(J)V

    .line 1055
    iget-object v2, p0, Lr/i;->a:Lr/d;

    invoke-static {v2}, Lr/d;->e(Lr/d;)Z

    move-result v2

    if-eqz v2, :cond_75

    .line 1060
    :cond_65
    iget-boolean v0, p0, Lr/i;->b:Z

    if-nez v0, :cond_4e

    .line 1061
    iget-object v0, p0, Lr/i;->a:Lr/d;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lr/d;->a(Lr/d;Z)Z

    .line 1062
    invoke-interface {v1}, Lt/f;->f_()V
    :try_end_72
    .catch Ljava/lang/InterruptedException; {:try_start_50 .. :try_end_72} :catch_73

    goto :goto_45

    .line 1065
    :catch_73
    move-exception v0

    goto :goto_14

    .line 1058
    :cond_75
    add-int/lit16 v0, v0, -0x3e8

    goto :goto_56
.end method
