.class public final enum Lr/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lr/c;

.field public static final enum b:Lr/c;

.field public static final enum c:Lr/c;

.field public static final enum d:Lr/c;

.field public static final enum e:Lr/c;

.field private static final synthetic g:[Lr/c;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x1

    .line 123
    new-instance v0, Lr/c;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, Lr/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lr/c;->a:Lr/c;

    .line 129
    new-instance v0, Lr/c;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3, v3}, Lr/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lr/c;->b:Lr/c;

    .line 136
    new-instance v0, Lr/c;

    const-string v1, "PREFETCH_OFFLINE_MAP"

    invoke-direct {v0, v1, v6, v4}, Lr/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lr/c;->c:Lr/c;

    .line 143
    new-instance v0, Lr/c;

    const-string v1, "PREFETCH_ROUTE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v7, v2}, Lr/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lr/c;->d:Lr/c;

    .line 152
    new-instance v0, Lr/c;

    const-string v1, "PREFETCH_AREA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, Lr/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lr/c;->e:Lr/c;

    .line 122
    const/4 v0, 0x5

    new-array v0, v0, [Lr/c;

    sget-object v1, Lr/c;->a:Lr/c;

    aput-object v1, v0, v5

    sget-object v1, Lr/c;->b:Lr/c;

    aput-object v1, v0, v3

    sget-object v1, Lr/c;->c:Lr/c;

    aput-object v1, v0, v6

    sget-object v1, Lr/c;->d:Lr/c;

    aput-object v1, v0, v7

    sget-object v1, Lr/c;->e:Lr/c;

    aput-object v1, v0, v4

    sput-object v0, Lr/c;->g:[Lr/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 157
    iput p3, p0, Lr/c;->f:I

    .line 158
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lr/c;
    .registers 2
    .parameter

    .prologue
    .line 122
    const-class v0, Lr/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lr/c;

    return-object v0
.end method

.method public static values()[Lr/c;
    .registers 1

    .prologue
    .line 122
    sget-object v0, Lr/c;->g:[Lr/c;

    invoke-virtual {v0}, [Lr/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lr/c;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 161
    iget v0, p0, Lr/c;->f:I

    return v0
.end method
