.class Lr/m;
.super Lr/b;
.source "SourceFile"


# instance fields
.field final synthetic d:Lr/l;


# direct methods
.method constructor <init>(Lr/l;)V
    .registers 2
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lr/m;->d:Lr/l;

    invoke-direct {p0, p1}, Lr/b;-><init>(Lr/a;)V

    return-void
.end method


# virtual methods
.method protected b(I)Lo/ap;
    .registers 9
    .parameter

    .prologue
    const/16 v3, 0x100

    .line 63
    iget-object v0, p0, Lr/m;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_a

    .line 64
    const/4 v0, 0x0

    .line 66
    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Lo/x;

    invoke-virtual {p0, p1}, Lr/m;->a(I)Lr/k;

    move-result-object v1

    invoke-virtual {v1}, Lr/k;->c()Lo/aq;

    move-result-object v1

    invoke-virtual {p0}, Lr/m;->a()I

    move-result v2

    iget-object v4, p0, Lr/m;->b:[[B

    aget-object v5, v4, p1

    iget-object v4, p0, Lr/m;->d:Lr/l;

    iget-object v6, v4, Lr/l;->a:LA/c;

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lo/x;-><init>(Lo/aq;III[BLA/c;)V

    goto :goto_9
.end method

.method protected c(I)[B
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 78
    iget-object v0, p0, Lr/m;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_9

    move-object v0, v6

    .line 90
    :goto_8
    return-object v0

    .line 81
    :cond_9
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    iget-object v0, p0, Lr/m;->b:[[B

    aget-object v0, v0, p1

    array-length v0, v0

    add-int/lit8 v0, v0, 0x20

    invoke-direct {v7, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 84
    :try_start_15
    invoke-virtual {p0, p1}, Lr/m;->a(I)Lr/k;

    move-result-object v0

    invoke-virtual {v0}, Lr/k;->c()Lo/aq;

    move-result-object v0

    invoke-virtual {p0}, Lr/m;->a()I

    move-result v1

    const/16 v2, 0x100

    const/16 v3, 0x100

    iget-object v4, p0, Lr/m;->b:[[B

    aget-object v4, v4, p1

    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v7}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static/range {v0 .. v5}, Lo/x;->a(Lo/aq;III[BLjava/io/DataOutput;)V

    .line 87
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_34} :catch_36

    move-result-object v0

    goto :goto_8

    .line 88
    :catch_36
    move-exception v0

    move-object v0, v6

    .line 90
    goto :goto_8
.end method
