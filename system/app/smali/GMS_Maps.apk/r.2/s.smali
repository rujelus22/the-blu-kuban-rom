.class public Lr/s;
.super Lo/aq;
.source "SourceFile"


# instance fields
.field private final d:Lo/J;


# direct methods
.method public constructor <init>(Lo/aq;Lo/J;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v1

    invoke-virtual {p1}, Lo/aq;->d()I

    move-result v2

    invoke-virtual {p1}, Lo/aq;->k()Lo/aB;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lo/aq;-><init>(IIILo/aB;)V

    .line 72
    iput-object p2, p0, Lr/s;->d:Lo/J;

    .line 73
    return-void
.end method


# virtual methods
.method public a(Lo/aB;)Lo/aq;
    .registers 5
    .parameter

    .prologue
    .line 77
    new-instance v0, Lr/s;

    invoke-super {p0, p1}, Lo/aq;->a(Lo/aB;)Lo/aq;

    move-result-object v1

    iget-object v2, p0, Lr/s;->d:Lo/J;

    invoke-direct {v0, v1, v2}, Lr/s;-><init>(Lo/aq;Lo/J;)V

    return-object v0
.end method

.method public a(Lr/s;)Z
    .registers 4
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lr/s;->d:Lo/J;

    iget-object v1, p1, Lr/s;->d:Lo/J;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 91
    if-ne p1, p0, :cond_4

    .line 94
    :cond_3
    :goto_3
    return v0

    :cond_4
    instance-of v1, p1, Lr/s;

    if-eqz v1, :cond_16

    invoke-super {p0, p1}, Lo/aq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    check-cast p1, Lr/s;

    invoke-virtual {p0, p1}, Lr/s;->a(Lr/s;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_16
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 86
    invoke-super {p0}, Lo/aq;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    iget-object v1, p0, Lr/s;->d:Lo/J;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public l()Lo/J;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lr/s;->d:Lo/J;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[layer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lr/s;->d:Lo/J;

    invoke-virtual {v1}, Lo/J;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 108
    const-string v0, " params: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget-object v0, p0, Lr/s;->d:Lo/J;

    invoke-virtual {v0}, Lo/J;->d()[Ljava/lang/String;

    move-result-object v2

    .line 110
    const/4 v0, 0x0

    :goto_1d
    array-length v3, v2

    if-ge v0, v3, :cond_36

    .line 111
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    add-int/lit8 v0, v0, 0x2

    goto :goto_1d

    .line 113
    :cond_36
    const-string v0, " coords: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lo/aq;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 114
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
