.class Lr/g;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lr/d;


# direct methods
.method constructor <init>(Lr/d;)V
    .registers 2
    .parameter

    .prologue
    .line 578
    iput-object p1, p0, Lr/g;->a:Lr/d;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter

    .prologue
    .line 581
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_60

    .line 614
    :goto_5
    return-void

    .line 583
    :pswitch_6
    iget-object v1, p0, Lr/g;->a:Lr/d;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lr/k;

    invoke-static {v1, v0}, Lr/d;->a(Lr/d;Lr/k;)V

    goto :goto_5

    .line 586
    :pswitch_10
    iget-object v0, p0, Lr/g;->a:Lr/d;

    invoke-static {v0}, Lr/d;->a(Lr/d;)V

    goto :goto_5

    .line 589
    :pswitch_16
    iget-object v1, p0, Lr/g;->a:Lr/d;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lr/h;

    invoke-static {v1, v0}, Lr/d;->a(Lr/d;Lr/h;)V

    goto :goto_5

    .line 592
    :pswitch_20
    iget-object v0, p0, Lr/g;->a:Lr/d;

    invoke-static {v0}, Lr/d;->b(Lr/d;)V

    goto :goto_5

    .line 595
    :pswitch_26
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    monitor-enter v1

    .line 596
    :try_start_29
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 597
    monitor-exit v1
    :try_end_2f
    .catchall {:try_start_29 .. :try_end_2f} :catchall_35

    .line 598
    iget-object v0, p0, Lr/g;->a:Lr/d;

    invoke-static {v0}, Lr/d;->c(Lr/d;)V

    goto :goto_5

    .line 597
    :catchall_35
    move-exception v0

    :try_start_36
    monitor-exit v1
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    throw v0

    .line 602
    :pswitch_38
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 605
    iget-object v3, p0, Lr/g;->a:Lr/d;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lo/ar;

    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lr/c;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ls/e;

    invoke-static {v3, v1, v2, v0}, Lr/d;->a(Lr/d;Lo/ar;Lr/c;Ls/e;)V

    goto :goto_5

    .line 609
    :pswitch_56
    iget-object v1, p0, Lr/g;->a:Lr/d;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v1, v0}, Lr/d;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_5

    .line 581
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_6
        :pswitch_10
        :pswitch_16
        :pswitch_20
        :pswitch_26
        :pswitch_38
        :pswitch_56
    .end packed-switch
.end method
