.class public Lay/R;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lay/Q;
.implements Lay/Z;


# instance fields
.field private a:Lay/X;

.field private b:Lay/ab;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jt;)V
    .registers 4
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lay/X;

    invoke-direct {v0, p0, p1}, Lay/X;-><init>(Lay/Z;Lcom/google/googlenav/ui/wizard/jt;)V

    iput-object v0, p0, Lay/R;->a:Lay/X;

    .line 35
    new-instance v0, Lay/ab;

    iget-object v1, p0, Lay/R;->a:Lay/X;

    invoke-direct {v0, v1}, Lay/ab;-><init>(Lay/W;)V

    iput-object v0, p0, Lay/R;->b:Lay/ab;

    .line 36
    invoke-virtual {p0}, Lay/R;->h()V

    .line 37
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILay/N;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lay/R;->b:Lay/ab;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->h()Z

    move-result v0

    if-nez v0, :cond_d

    .line 113
    :cond_c
    :goto_c
    return-void

    .line 102
    :cond_d
    :try_start_d
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 103
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 104
    if-eqz v1, :cond_c

    .line 108
    invoke-virtual {p0, v0, p2, p3}, Lay/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILay/N;)V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_1b} :catch_1c

    goto :goto_c

    .line 109
    :catch_1c
    move-exception v0

    goto :goto_c
.end method


# virtual methods
.method public D_()V
    .registers 2

    .prologue
    .line 171
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->f()V

    .line 172
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 178
    return-void
.end method

.method public F_()V
    .registers 1

    .prologue
    .line 196
    return-void
.end method

.method public L_()V
    .registers 2

    .prologue
    .line 183
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->g()V

    .line 184
    return-void
.end method

.method public M_()V
    .registers 1

    .prologue
    .line 190
    return-void
.end method

.method public a(I)Lay/S;
    .registers 3
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0, p1}, Lay/X;->d(I)Lay/S;

    move-result-object v0

    return-object v0
.end method

.method public a(ILay/N;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0, p1}, Lay/X;->e(I)V

    .line 59
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0, p1}, Lay/X;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lay/R;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILay/N;)V

    .line 61
    return-void
.end method

.method public a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lay/o;Lay/N;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lay/R;->b:Lay/ab;

    invoke-virtual {v0, p1, p2, p3, p4}, Lay/ab;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lay/o;Lay/N;)V

    .line 148
    return-void
.end method

.method public a(Lay/aa;)V
    .registers 3
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lay/R;->b:Lay/ab;

    invoke-virtual {v0, p1}, Lay/ab;->a(Lay/aa;)V

    .line 73
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILay/N;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lay/R;->b:Lay/ab;

    invoke-virtual {v0, p1, p2, p3}, Lay/ab;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILay/N;)V

    .line 140
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0, p1}, Lay/X;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 141
    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/jt;)V
    .registers 3
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0, p1}, Lay/X;->a(Lcom/google/googlenav/ui/wizard/jt;)V

    .line 54
    return-void
.end method

.method public a(ZLay/N;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->b()V

    .line 86
    if-eqz p1, :cond_c

    .line 87
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->c()V

    .line 89
    :cond_c
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p2}, Lay/R;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILay/N;)V

    .line 90
    return-void
.end method

.method public b(Lay/aa;)V
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lay/R;->b:Lay/ab;

    invoke-virtual {v0, p1}, Lay/ab;->b(Lay/aa;)V

    .line 78
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lay/R;->b:Lay/ab;

    invoke-virtual {v0}, Lay/ab;->d()V

    .line 154
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->d()V

    .line 160
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lay/R;->a:Lay/X;

    invoke-virtual {v0}, Lay/X;->e()V

    .line 166
    return-void
.end method
