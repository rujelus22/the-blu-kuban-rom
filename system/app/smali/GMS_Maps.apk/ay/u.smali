.class public abstract Lay/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lay/V;
.implements Lay/t;


# static fields
.field private static final f:Ljava/util/Comparator;

.field private static final g:Ljava/util/Comparator;


# instance fields
.field protected final a:I

.field protected final b:Lay/S;

.field private final c:Ljava/util/List;

.field private d:Lay/H;

.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 55
    new-instance v0, Lay/v;

    invoke-direct {v0}, Lay/v;-><init>()V

    sput-object v0, Lay/u;->f:Ljava/util/Comparator;

    .line 74
    new-instance v0, Lay/w;

    invoke-direct {v0}, Lay/w;-><init>()V

    sput-object v0, Lay/u;->g:Ljava/util/Comparator;

    return-void
.end method

.method constructor <init>(Lay/S;Lay/H;IZLay/T;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput-object p1, p0, Lay/u;->b:Lay/S;

    .line 152
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lay/u;->c:Ljava/util/List;

    .line 153
    invoke-interface {p1, p0}, Lay/S;->a(Lay/V;)V

    .line 154
    invoke-interface {p1, p3, p5}, Lay/S;->a(ILay/T;)V

    .line 155
    iput-object p2, p0, Lay/u;->d:Lay/H;

    .line 156
    iput p3, p0, Lay/u;->a:I

    .line 157
    iput-boolean p4, p0, Lay/u;->e:Z

    .line 158
    return-void
.end method

.method public static a(ILay/S;Lay/H;)Lay/u;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    packed-switch p0, :pswitch_data_4a

    .line 144
    :pswitch_3
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 93
    :pswitch_5
    new-instance v0, Lay/L;

    invoke-direct {v0, p1, p2}, Lay/L;-><init>(Lay/S;Lay/H;)V

    goto :goto_4

    .line 95
    :pswitch_b
    new-instance v0, Lay/k;

    new-instance v1, Lay/x;

    invoke-direct {v1}, Lay/x;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, Lay/k;-><init>(Lay/S;Lay/H;ILay/T;)V

    goto :goto_4

    .line 106
    :pswitch_16
    new-instance v0, Lay/k;

    new-instance v1, Lay/y;

    invoke-direct {v1}, Lay/y;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, Lay/k;-><init>(Lay/S;Lay/H;ILay/T;)V

    goto :goto_4

    .line 119
    :pswitch_21
    new-instance v0, Lay/k;

    new-instance v1, Lay/z;

    invoke-direct {v1}, Lay/z;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, Lay/k;-><init>(Lay/S;Lay/H;ILay/T;)V

    goto :goto_4

    .line 129
    :pswitch_2c
    new-instance v0, Lay/k;

    new-instance v1, Lay/A;

    invoke-direct {v1}, Lay/A;-><init>()V

    invoke-direct {v0, p1, p2, p0, v1}, Lay/k;-><init>(Lay/S;Lay/H;ILay/T;)V

    goto :goto_4

    .line 138
    :pswitch_37
    new-instance v0, Lay/D;

    invoke-direct {v0, p1, p2}, Lay/D;-><init>(Lay/S;Lay/H;)V

    goto :goto_4

    .line 140
    :pswitch_3d
    new-instance v0, Lay/i;

    invoke-direct {v0, p1, p2}, Lay/i;-><init>(Lay/S;Lay/H;)V

    goto :goto_4

    .line 142
    :pswitch_43
    new-instance v0, Lay/I;

    invoke-direct {v0, p1, p2}, Lay/I;-><init>(Lay/S;Lay/H;)V

    goto :goto_4

    .line 91
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_b
        :pswitch_16
        :pswitch_21
        :pswitch_3d
        :pswitch_2c
        :pswitch_37
        :pswitch_43
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lay/G;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 279
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 296
    :goto_6
    return-void

    .line 287
    :cond_7
    :try_start_7
    invoke-virtual {p2}, Lay/G;->d()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_e} :catch_21

    move-result-object v0

    .line 293
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 294
    const/4 v1, 0x4

    invoke-virtual {p2}, Lay/G;->c()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 295
    iget-object v1, p0, Lay/u;->d:Lay/H;

    invoke-virtual {v1, p1, v0}, Lay/H;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_6

    .line 288
    :catch_21
    move-exception v0

    goto :goto_6
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x2

    .line 214
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hc;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 215
    invoke-virtual {v0, v4, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 216
    invoke-virtual {p1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 219
    iget-object v2, p0, Lay/u;->b:Lay/S;

    iget v3, p0, Lay/u;->a:I

    invoke-interface {v2, v3, v1}, Lay/S;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 220
    if-eqz v1, :cond_23

    .line 224
    :try_start_19
    invoke-static {v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_1c} :catch_30

    move-result-object v1

    .line 228
    invoke-virtual {p0, v1, p1}, Lay/u;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 229
    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 233
    :cond_23
    iget-boolean v1, p0, Lay/u;->e:Z

    if-eqz v1, :cond_33

    .line 234
    iget-object v1, p0, Lay/u;->b:Lay/S;

    iget v2, p0, Lay/u;->a:I

    invoke-interface {v1, v2, v0}, Lay/S;->a(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    .line 236
    :goto_2f
    return v0

    .line 225
    :catch_30
    move-exception v0

    .line 226
    const/4 v0, 0x0

    goto :goto_2f

    .line 236
    :cond_33
    iget-object v1, p0, Lay/u;->b:Lay/S;

    iget v2, p0, Lay/u;->a:I

    invoke-interface {v1, v2, v0}, Lay/S;->b(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    goto :goto_2f
.end method

.method private f(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 342
    iget-object v0, p0, Lay/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/B;

    .line 343
    invoke-interface {v0, p1}, Lay/B;->b(Ljava/lang/String;)V

    goto :goto_6

    .line 345
    :cond_16
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lay/s;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 174
    :cond_7
    :goto_7
    return-object v0

    .line 170
    :cond_8
    iget-object v1, p0, Lay/u;->b:Lay/S;

    iget v2, p0, Lay/u;->a:I

    invoke-interface {v1, v2, p1}, Lay/S;->a(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 171
    if-eqz v1, :cond_7

    .line 172
    invoke-virtual {p0, v1}, Lay/u;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/s;

    move-result-object v0

    goto :goto_7
.end method

.method public a()Ljava/util/List;
    .registers 4

    .prologue
    .line 205
    iget-object v0, p0, Lay/u;->b:Lay/S;

    iget v1, p0, Lay/u;->a:I

    invoke-interface {v0, v1}, Lay/S;->b(I)Ljava/util/List;

    move-result-object v0

    .line 206
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/common/collect/cx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 207
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 208
    invoke-virtual {p0, v0}, Lay/u;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/s;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 210
    :cond_28
    return-object v1
.end method

.method public a(Lay/B;)V
    .registers 3
    .parameter

    .prologue
    .line 309
    iget-object v0, p0, Lay/u;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    return-void
.end method

.method public a(Lay/G;)V
    .registers 3
    .parameter

    .prologue
    .line 273
    invoke-virtual {p1}, Lay/G;->a()Ljava/lang/String;

    move-result-object v0

    .line 274
    invoke-direct {p0, v0, p1}, Lay/u;->a(Ljava/lang/String;Lay/G;)V

    .line 275
    invoke-direct {p0, v0}, Lay/u;->f(Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method protected abstract a(Lay/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method protected abstract a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
.end method

.method public a(Lay/s;)Z
    .registers 5
    .parameter

    .prologue
    .line 248
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/hc;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 249
    invoke-virtual {p0, p1, v0}, Lay/u;->a(Lay/s;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 250
    const/4 v1, 0x2

    invoke-virtual {p1}, Lay/s;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 251
    invoke-direct {p0, v0}, Lay/u;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    return v0
.end method

.method public a_(ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 320
    iget v0, p0, Lay/u;->a:I

    if-eq p1, v0, :cond_5

    .line 328
    :cond_4
    :goto_4
    return-void

    .line 323
    :cond_5
    invoke-static {p2}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 327
    invoke-direct {p0, p2}, Lay/u;->f(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public b(Ljava/lang/String;)Lay/G;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 186
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 190
    :cond_7
    :goto_7
    return-object v0

    .line 189
    :cond_8
    iget-object v1, p0, Lay/u;->d:Lay/H;

    invoke-virtual {v1, p1}, Lay/H;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 190
    if-eqz v1, :cond_7

    new-instance v0, Lay/G;

    invoke-direct {v0, v1}, Lay/G;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_7
.end method

.method protected abstract b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lay/s;
.end method

.method public b()Ljava/util/List;
    .registers 2

    .prologue
    .line 365
    invoke-virtual {p0}, Lay/u;->d()Ljava/util/List;

    move-result-object v0

    .line 366
    return-object v0
.end method

.method public b(Lay/B;)V
    .registers 3
    .parameter

    .prologue
    .line 315
    iget-object v0, p0, Lay/u;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 316
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 371
    iget-object v0, p0, Lay/u;->b:Lay/S;

    iget v1, p0, Lay/u;->a:I

    invoke-interface {v0, v1}, Lay/S;->c(I)V

    .line 372
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lay/u;->f(Ljava/lang/String;)V

    .line 373
    return-void
.end method

.method public c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 195
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lay/u;->d:Lay/H;

    invoke-virtual {v0, p1}, Lay/H;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public d()Ljava/util/List;
    .registers 6

    .prologue
    .line 349
    invoke-virtual {p0}, Lay/u;->a()Ljava/util/List;

    move-result-object v0

    .line 351
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lay/s;

    .line 352
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 353
    sget-object v2, Lay/u;->g:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 355
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/cx;->b(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 356
    array-length v3, v1

    const/4 v0, 0x0

    :goto_1c
    if-ge v0, v3, :cond_2a

    aget-object v4, v1, v0

    .line 357
    invoke-virtual {v4}, Lay/s;->h()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 360
    :cond_2a
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 200
    invoke-static {p1}, Lab/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lay/u;->d:Lay/H;

    invoke-virtual {v0, p1}, Lay/H;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public e(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 300
    iget-object v0, p0, Lay/u;->b:Lay/S;

    iget v1, p0, Lay/u;->a:I

    invoke-interface {v0, v1, p1}, Lay/S;->b(ILjava/lang/String;)V

    .line 301
    invoke-direct {p0, p1}, Lay/u;->f(Ljava/lang/String;)V

    .line 302
    return-void
.end method
