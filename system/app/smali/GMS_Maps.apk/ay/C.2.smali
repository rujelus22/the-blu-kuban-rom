.class public Lay/C;
.super Lay/s;
.source "SourceFile"


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lau/B;

.field private g:Ljava/util/List;

.field private h:I

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:J


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 76
    invoke-direct {p0}, Lay/s;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lay/C;->h:I

    .line 61
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lay/C;->o:J

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter

    .prologue
    .line 126
    invoke-direct {p0}, Lay/s;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lay/C;->h:I

    .line 61
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lay/C;->o:J

    .line 127
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v0

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/go;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-ne v0, v1, :cond_19

    .line 128
    invoke-direct {p0, p1}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 133
    :goto_18
    return-void

    .line 131
    :cond_19
    invoke-direct {p0, p1}, Lay/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_18
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Lay/s;-><init>()V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lay/C;->h:I

    .line 61
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lay/C;->o:J

    .line 80
    iput-object p1, p0, Lay/C;->a:Ljava/lang/String;

    .line 81
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->i:Ljava/lang/Boolean;

    .line 82
    iput-object p2, p0, Lay/C;->d:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lay/C;->e:Ljava/lang/String;

    .line 84
    invoke-static {p4}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v0

    iput-object v0, p0, Lay/C;->f:Lau/B;

    .line 85
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lay/C;->g:Ljava/util/List;

    .line 86
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 65
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 67
    :goto_e
    return-object v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x5

    .line 89
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lay/C;->a:Ljava/lang/String;

    .line 90
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->i:Ljava/lang/Boolean;

    .line 91
    const/4 v0, 0x3

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lay/C;->d:Ljava/lang/String;

    .line 92
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lay/C;->e:Ljava/lang/String;

    .line 93
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 94
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v0

    iput-object v0, p0, Lay/C;->f:Lau/B;

    .line 96
    :cond_2d
    const/16 v0, 0x8

    const-wide/16 v1, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lay/C;->b:J

    .line 98
    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 99
    invoke-static {v0}, Lcom/google/common/collect/cx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lo/B;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lay/C;->g:Ljava/util/List;

    .line 100
    const/16 v0, 0x9

    const-wide/16 v1, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lay/C;->c:J

    .line 102
    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    if-eqz p2, :cond_9

    .line 72
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 74
    :cond_9
    return-void
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x3

    .line 105
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lay/C;->a:Ljava/lang/String;

    .line 106
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lay/C;->d:Ljava/lang/String;

    .line 107
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 108
    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lau/C;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lau/B;

    move-result-object v0

    iput-object v0, p0, Lay/C;->f:Lau/B;

    .line 110
    :cond_1f
    const/4 v0, 0x4

    const-wide/16 v1, -0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lay/C;->b:J

    .line 112
    const/4 v0, 0x5

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    iput v0, p0, Lay/C;->h:I

    .line 114
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->i:Ljava/lang/Boolean;

    .line 115
    const/4 v0, 0x7

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->j:Ljava/lang/Boolean;

    .line 116
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->k:Ljava/lang/Boolean;

    .line 117
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 118
    invoke-static {v0}, Lcom/google/common/collect/cx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lo/B;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lay/C;->g:Ljava/util/List;

    .line 119
    const/16 v0, 0xd

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->l:Ljava/lang/Boolean;

    .line 120
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->m:Ljava/lang/Boolean;

    .line 121
    const/16 v0, 0xb

    invoke-static {p1, v0}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->n:Ljava/lang/Boolean;

    .line 122
    const/16 v0, 0xc

    const-wide/16 v1, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;IJ)J

    move-result-wide v0

    iput-wide v0, p0, Lay/C;->c:J

    .line 124
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 141
    iget-object v0, p0, Lay/C;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 201
    iput p1, p0, Lay/C;->h:I

    .line 202
    return-void
.end method

.method public a(Lau/B;)V
    .registers 2
    .parameter

    .prologue
    .line 193
    iput-object p1, p0, Lay/C;->f:Lau/B;

    .line 194
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 2
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lay/C;->g:Ljava/util/List;

    .line 198
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 205
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lay/C;->i:Ljava/lang/Boolean;

    .line 206
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 137
    iget-object v0, p0, Lay/C;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 185
    iput-object p1, p0, Lay/C;->d:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lay/C;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lau/B;
    .registers 2

    .prologue
    .line 149
    iget-object v0, p0, Lay/C;->f:Lau/B;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    return-object v0
.end method

.method public f()I
    .registers 2

    .prologue
    .line 157
    iget v0, p0, Lay/C;->h:I

    return v0
.end method

.method public g()Z
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Lay/C;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lay/C;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public l()Z
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lay/C;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lay/C;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 169
    iget-object v0, p0, Lay/C;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lay/C;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 177
    iget-object v0, p0, Lay/C;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lay/C;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 181
    iget-object v0, p0, Lay/C;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lay/C;->n:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7

    .prologue
    .line 229
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/go;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 230
    const/4 v0, 0x1

    iget-object v2, p0, Lay/C;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 231
    const/4 v0, 0x2

    iget-object v2, p0, Lay/C;->i:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 232
    iget-object v0, p0, Lay/C;->d:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 233
    const/4 v0, 0x3

    iget-object v2, p0, Lay/C;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 235
    :cond_1d
    iget-object v0, p0, Lay/C;->e:Ljava/lang/String;

    if-eqz v0, :cond_27

    .line 236
    const/4 v0, 0x4

    iget-object v2, p0, Lay/C;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 238
    :cond_27
    iget-object v0, p0, Lay/C;->f:Lau/B;

    if-eqz v0, :cond_35

    .line 239
    const/4 v0, 0x5

    iget-object v2, p0, Lay/C;->f:Lau/B;

    invoke-static {v2}, Lau/C;->d(Lau/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 242
    :cond_35
    iget-wide v2, p0, Lay/C;->b:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_44

    .line 243
    const/16 v0, 0x8

    iget-wide v2, p0, Lay/C;->b:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 245
    :cond_44
    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    if-eqz v0, :cond_6b

    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6b

    .line 246
    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_56
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/B;

    .line 247
    const/4 v3, 0x7

    invoke-virtual {v0}, Lo/B;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_56

    .line 250
    :cond_6b
    iget-wide v2, p0, Lay/C;->c:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_7a

    .line 251
    const/16 v0, 0x9

    iget-wide v2, p0, Lay/C;->c:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 253
    :cond_7a
    return-object v1
.end method

.method public q()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5

    .prologue
    .line 258
    invoke-super {p0}, Lay/s;->k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 260
    iget-object v0, p0, Lay/C;->d:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 261
    const/4 v0, 0x2

    iget-object v2, p0, Lay/C;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 263
    :cond_e
    iget-object v0, p0, Lay/C;->f:Lau/B;

    if-eqz v0, :cond_1c

    .line 264
    const/4 v0, 0x3

    iget-object v2, p0, Lay/C;->f:Lau/B;

    invoke-static {v2}, Lau/C;->d(Lau/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 267
    :cond_1c
    iget v0, p0, Lay/C;->h:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_27

    .line 268
    const/4 v0, 0x5

    iget v2, p0, Lay/C;->h:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 270
    :cond_27
    const/4 v0, 0x6

    iget-object v2, p0, Lay/C;->i:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 271
    const/4 v0, 0x7

    iget-object v2, p0, Lay/C;->j:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 273
    iget-object v0, p0, Lay/C;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_42

    .line 274
    const/16 v0, 0xd

    iget-object v2, p0, Lay/C;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 276
    :cond_42
    const/16 v0, 0x9

    iget-object v2, p0, Lay/C;->k:Ljava/lang/Boolean;

    invoke-static {v1, v0, v2}, Lay/C;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;ILjava/lang/Boolean;)V

    .line 277
    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    if-eqz v0, :cond_71

    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_71

    .line 278
    iget-object v0, p0, Lay/C;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/B;

    .line 279
    const/16 v3, 0xa

    invoke-virtual {v0}, Lo/B;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_5b

    .line 282
    :cond_71
    iget-object v0, p0, Lay/C;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_80

    .line 283
    const/16 v0, 0x10

    iget-object v2, p0, Lay/C;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 285
    :cond_80
    iget-object v0, p0, Lay/C;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_8f

    .line 286
    const/16 v0, 0xb

    iget-object v2, p0, Lay/C;->n:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 290
    :cond_8f
    return-object v1
.end method

.method public r()J
    .registers 3

    .prologue
    .line 299
    iget-wide v0, p0, Lay/C;->o:J

    return-wide v0
.end method
