.class public final enum Lay/N;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lay/N;

.field public static final enum b:Lay/N;

.field public static final enum c:Lay/N;

.field public static final enum d:Lay/N;

.field public static final enum e:Lay/N;

.field public static final enum f:Lay/N;

.field public static final enum g:Lay/N;

.field public static final enum h:Lay/N;

.field private static final synthetic j:[Lay/N;


# instance fields
.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 11
    new-instance v0, Lay/N;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->a:Lay/N;

    .line 13
    new-instance v0, Lay/N;

    const-string v1, "ACTIVATE_WIZARD"

    invoke-direct {v0, v1, v5, v5}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->b:Lay/N;

    .line 15
    new-instance v0, Lay/N;

    const-string v1, "REFRESH"

    invoke-direct {v0, v1, v6, v6}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->c:Lay/N;

    .line 17
    new-instance v0, Lay/N;

    const-string v1, "UPDATE_ITEM"

    invoke-direct {v0, v1, v7, v7}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->d:Lay/N;

    .line 19
    new-instance v0, Lay/N;

    const-string v1, "ON_RESUME"

    invoke-direct {v0, v1, v8, v8}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->e:Lay/N;

    .line 21
    new-instance v0, Lay/N;

    const-string v1, "ON_SIGN_IN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->f:Lay/N;

    .line 23
    new-instance v0, Lay/N;

    const-string v1, "CONTENT_PROVIDER"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->g:Lay/N;

    .line 25
    new-instance v0, Lay/N;

    const-string v1, "ACTIVATE_PLACES_WIZARD"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lay/N;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lay/N;->h:Lay/N;

    .line 9
    const/16 v0, 0x8

    new-array v0, v0, [Lay/N;

    sget-object v1, Lay/N;->a:Lay/N;

    aput-object v1, v0, v4

    sget-object v1, Lay/N;->b:Lay/N;

    aput-object v1, v0, v5

    sget-object v1, Lay/N;->c:Lay/N;

    aput-object v1, v0, v6

    sget-object v1, Lay/N;->d:Lay/N;

    aput-object v1, v0, v7

    sget-object v1, Lay/N;->e:Lay/N;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lay/N;->f:Lay/N;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lay/N;->g:Lay/N;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lay/N;->h:Lay/N;

    aput-object v2, v0, v1

    sput-object v0, Lay/N;->j:[Lay/N;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lay/N;->i:I

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lay/N;
    .registers 2
    .parameter

    .prologue
    .line 9
    const-class v0, Lay/N;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lay/N;

    return-object v0
.end method

.method public static values()[Lay/N;
    .registers 1

    .prologue
    .line 9
    sget-object v0, Lay/N;->j:[Lay/N;

    invoke-virtual {v0}, [Lay/N;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lay/N;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 34
    iget v0, p0, Lay/N;->i:I

    return v0
.end method
