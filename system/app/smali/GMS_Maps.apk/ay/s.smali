.class public abstract Lay/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:J

.field protected c:J


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lay/s;->a:Ljava/lang/String;

    .line 22
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lay/s;->b:J

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lay/s;->c:J

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 29
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lay/s;-><init>(Ljava/lang/String;J)V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lay/s;->a:Ljava/lang/String;

    .line 22
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lay/s;->b:J

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lay/s;->c:J

    .line 33
    iput-object p1, p0, Lay/s;->a:Ljava/lang/String;

    .line 34
    iput-wide p2, p0, Lay/s;->b:J

    .line 35
    return-void
.end method


# virtual methods
.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 54
    iput-wide p1, p0, Lay/s;->b:J

    .line 55
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lay/s;->a:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public b(J)V
    .registers 3
    .parameter

    .prologue
    .line 58
    iput-wide p1, p0, Lay/s;->c:J

    .line 59
    return-void
.end method

.method public h()Ljava/lang/String;
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lay/s;->a:Ljava/lang/String;

    return-object v0
.end method

.method public i()J
    .registers 3

    .prologue
    .line 42
    iget-wide v0, p0, Lay/s;->b:J

    return-wide v0
.end method

.method public j()J
    .registers 3

    .prologue
    .line 46
    iget-wide v0, p0, Lay/s;->c:J

    return-wide v0
.end method

.method protected final k()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6

    .prologue
    .line 62
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eq;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 63
    const/4 v1, 0x1

    iget-object v2, p0, Lay/s;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 64
    iget-wide v1, p0, Lay/s;->b:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1b

    .line 65
    const/4 v1, 0x4

    iget-wide v2, p0, Lay/s;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 67
    :cond_1b
    iget-wide v1, p0, Lay/s;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2a

    .line 68
    const/16 v1, 0xc

    iget-wide v2, p0, Lay/s;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 70
    :cond_2a
    return-object v0
.end method
