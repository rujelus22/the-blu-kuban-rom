.class public LaY/c;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# static fields
.field private static final n:Lcom/google/common/collect/ax;

.field private static final o:Lcom/google/common/collect/ax;

.field private static final p:Lcom/google/common/collect/ax;

.field private static final q:Lcom/google/common/collect/ax;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/EditText;

.field private C:Landroid/widget/EditText;

.field private D:Landroid/widget/TextView;

.field private E:Ljava/util/Map;

.field private F:Landroid/view/MenuItem;

.field private G:Landroid/widget/Button;

.field private H:Landroid/view/MenuItem;

.field private I:Landroid/widget/Button;

.field private J:Lcom/google/googlenav/ui/br;

.field private K:Ljava/util/List;

.field private L:Ljava/lang/String;

.field private final M:Lcom/google/googlenav/ui/wizard/hb;

.field a:Landroid/view/MenuItem;

.field b:Landroid/widget/Button;

.field public c:Landroid/app/AlertDialog;

.field final d:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private final l:Lcom/google/googlenav/ui/wizard/hh;

.field private final m:Lcom/google/googlenav/ai;

.field private r:Landroid/view/View;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/ImageView;

.field private w:Landroid/view/ViewGroup;

.field private x:Landroid/view/ViewGroup;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const v7, 0x7f100393

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 81
    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    const-string v1, "0"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const-string v1, "1"

    const v2, 0x7f100394

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const-string v1, "2"

    const v2, 0x7f100395

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const-string v1, "3"

    const v2, 0x7f100396

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->n:Lcom/google/common/collect/ax;

    .line 89
    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1003a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->o:Lcom/google/common/collect/ax;

    .line 97
    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x419

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const v1, 0x7f100394

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x418

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const v1, 0x7f100395

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x41a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    const v1, 0x7f100396

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x417

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->p:Lcom/google/common/collect/ax;

    .line 105
    new-instance v0, Lcom/google/common/collect/ay;

    invoke-direct {v0}, Lcom/google/common/collect/ay;-><init>()V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0200a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f02009e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v0

    sput-object v0, LaY/c;->q:Lcom/google/common/collect/ax;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hh;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;Lcom/google/googlenav/ui/wizard/hb;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 163
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_24

    const v0, 0x7f0f001b

    :goto_d
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 146
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaY/c;->K:Ljava/util/List;

    .line 153
    new-instance v0, LaY/d;

    invoke-direct {v0, p0}, LaY/d;-><init>(LaY/c;)V

    iput-object v0, p0, LaY/c;->d:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 165
    iput-object p1, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    .line 166
    iput-object p2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    .line 167
    iput-object p4, p0, LaY/c;->M:Lcom/google/googlenav/ui/wizard/hb;

    .line 168
    return-void

    .line 163
    :cond_24
    const v0, 0x7f0f0018

    goto :goto_d
.end method

.method private A()V
    .registers 4

    .prologue
    .line 692
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v0

    .line 693
    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    .line 694
    if-nez v0, :cond_d

    .line 713
    :goto_c
    return-void

    .line 697
    :cond_d
    iget-object v1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    .line 700
    invoke-virtual {v1, v0}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 701
    new-instance v2, LaY/f;

    invoke-direct {v2, p0, v1, v0}, LaY/f;-><init>(LaY/c;LaB/s;Lcom/google/googlenav/ui/bs;)V

    .line 708
    invoke-virtual {v1, v0, v2}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto :goto_c

    .line 710
    :cond_22
    iget-object v1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 711
    iget-object v1, p0, LaY/c;->v:Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    goto :goto_c
.end method

.method private a(Landroid/view/View;Ljava/lang/String;)Landroid/widget/RadioButton;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 460
    sget-object v0, LaY/c;->n:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p2}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 461
    if-eqz v0, :cond_15

    .line 462
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 464
    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method static synthetic a(LaY/c;)Lcom/google/googlenav/ui/wizard/hh;
    .registers 2
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    return-object v0
.end method

.method static synthetic a(LaY/c;Landroid/widget/RadioGroup;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0, p1}, LaY/c;->a(Landroid/widget/RadioGroup;)V

    return-void
.end method

.method private a(Landroid/widget/RadioGroup;)V
    .registers 5
    .parameter

    .prologue
    .line 601
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 602
    const v1, 0x7f10039e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 603
    iget-object v1, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {p0, v1}, LaY/c;->a(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v1

    .line 604
    if-eqz v1, :cond_2a

    .line 605
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 606
    iget-object v2, p0, LaY/c;->E:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 610
    :goto_29
    return-void

    .line 608
    :cond_2a
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_29
.end method

.method private a(Lcom/google/googlenav/cy;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 255
    iget-boolean v0, p1, Lcom/google/googlenav/cy;->e:Z

    if-nez v0, :cond_5

    .line 280
    :goto_4
    return-void

    .line 258
    :cond_5
    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04014c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 261
    const v0, 0x7f100391

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 262
    iget-object v1, p1, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    const v0, 0x7f100393

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 266
    const v1, 0x7f100394

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 267
    const v2, 0x7f100395

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 268
    const v3, 0x7f100396

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 269
    const/16 v5, 0x419

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const/16 v0, 0x418

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 271
    const/16 v0, 0x41a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 272
    const/16 v0, 0x417

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v0, p1, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    invoke-direct {p0, v4, v0}, LaY/c;->a(Landroid/view/View;Ljava/lang/String;)Landroid/widget/RadioButton;

    move-result-object v0

    .line 276
    if-eqz v0, :cond_73

    .line 277
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 279
    :cond_73
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_4
.end method

.method private a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 340
    iget-object v1, p0, LaY/c;->r:Landroid/view/View;

    sget-object v0, LaY/c;->o:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 341
    if-eqz p2, :cond_21

    .line 342
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 346
    :goto_19
    invoke-virtual {p1, p3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 347
    return-void

    .line 344
    :cond_21
    sget-object v1, LaY/c;->q:Lcom/google/common/collect/ax;

    invoke-virtual {v1, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setBackgroundResource(I)V

    goto :goto_19
.end method

.method private a(Ljava/lang/String;)V
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 724
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 726
    const/16 v2, 0x55

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    if-lez v0, :cond_50

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "p="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2a
    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-object v4, p0, LaY/c;->L:Ljava/lang/String;

    if-eqz v4, :cond_46

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "v="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, LaY/c;->L:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_46
    aput-object v1, v3, v0

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, p1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 734
    return-void

    :cond_50
    move-object v0, v1

    .line 726
    goto :goto_2a
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 308
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->g()Ljava/lang/String;

    move-result-object v0

    .line 309
    iget-object v1, p0, LaY/c;->y:Landroid/widget/TextView;

    if-eqz v0, :cond_3d

    :goto_f
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    iget-object v0, p0, LaY/c;->z:Landroid/widget/TextView;

    const/16 v1, 0x3f1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v0, p0, LaY/c;->A:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    if-eqz p2, :cond_29

    .line 314
    iget-object v0, p0, LaY/c;->B:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 316
    :cond_29
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 318
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 319
    iget-object v0, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 320
    return-void

    .line 309
    :cond_3d
    const-string v0, ""

    goto :goto_f
.end method

.method private a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 324
    iget-object v0, p0, LaY/c;->y:Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v0, p0, LaY/c;->z:Landroid/widget/TextView;

    const/16 v1, 0x3f1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_23
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 327
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-direct {p0, v2, v0, p2}, LaY/c;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_23

    .line 329
    :cond_47
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 331
    iget-object v1, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f10039f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    .line 332
    iget-object v2, p0, LaY/c;->d:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 333
    invoke-direct {p0, v1}, LaY/c;->a(Landroid/widget/RadioGroup;)V

    .line 334
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 335
    iget-object v0, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 336
    return-void
.end method

.method private b(Lcom/google/googlenav/ay;)Landroid/view/View;
    .registers 8
    .parameter

    .prologue
    const v5, 0x7f0b00ac

    .line 670
    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040152

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 671
    const v0, 0x7f1003b9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 674
    invoke-virtual {p1}, Lcom/google/googlenav/ay;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_45

    .line 675
    invoke-virtual {p1}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v1

    .line 680
    :goto_22
    invoke-virtual {p0}, LaY/c;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 681
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 682
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 684
    check-cast v1, Lan/f;

    invoke-virtual {v1}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lbm/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 686
    const/4 v5, 0x0

    invoke-static {v1, v4, v3, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 688
    return-object v2

    .line 677
    :cond_45
    iget-object v1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    invoke-virtual {p1}, Lcom/google/googlenav/ay;->b()Lcom/google/googlenav/ui/bs;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v1

    goto :goto_22
.end method

.method static synthetic b(LaY/c;)Lcom/google/googlenav/ui/wizard/hb;
    .registers 2
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LaY/c;->M:Lcom/google/googlenav/ui/wizard/hb;

    return-object v0
.end method

.method private b(Landroid/view/View;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 500
    sget-object v0, LaY/c;->n:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->x_()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 501
    sget-object v1, LaY/c;->n:Lcom/google/common/collect/ax;

    invoke-virtual {v1, v0}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 502
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 503
    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 507
    :goto_2e
    return-object v0

    :cond_2f
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method private b(Landroid/view/View;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 574
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 575
    const v0, 0x7f10004a

    const v1, 0x7f0201a7

    invoke-virtual {p0, p2, v0, v1}, LaY/c;->a(Ljava/lang/CharSequence;II)V

    .line 591
    :cond_14
    :goto_14
    return-void

    .line 576
    :cond_15
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 577
    iget-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 578
    iget-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    const v1, 0x7f1003a4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 579
    const/16 v1, 0x420

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 580
    iget-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    const v1, 0x7f1000fb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 581
    if-eqz v0, :cond_14

    .line 582
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 583
    new-instance v1, LaY/m;

    invoke-direct {v1, p0}, LaY/m;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_14
.end method

.method private c(Landroid/view/View;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 511
    sget-object v0, LaY/c;->p:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->x_()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 512
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 513
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 514
    sget-object v0, LaY/c;->p:Lcom/google/common/collect/ax;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 517
    :goto_32
    return-object v0

    :cond_33
    const/4 v0, 0x0

    goto :goto_32
.end method

.method static synthetic c(LaY/c;)V
    .registers 1
    .parameter

    .prologue
    .line 73
    invoke-direct {p0}, LaY/c;->w()V

    return-void
.end method

.method static synthetic d(LaY/c;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LaY/c;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic e(LaY/c;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LaY/c;->v:Landroid/widget/ImageView;

    return-object v0
.end method

.method private l()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 244
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    .line 245
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 246
    iget-object v0, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 247
    :goto_1a
    if-ge v1, v4, :cond_30

    .line 248
    iget-object v0, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cy;

    .line 249
    add-int/lit8 v3, v1, 0x1

    .line 250
    if-ne v3, v4, :cond_2e

    const/4 v1, 0x1

    :goto_29
    invoke-direct {p0, v0, v1}, LaY/c;->a(Lcom/google/googlenav/cy;Z)V

    move v1, v3

    .line 251
    goto :goto_1a

    :cond_2e
    move v1, v2

    .line 250
    goto :goto_29

    .line 252
    :cond_30
    return-void
.end method

.method private m()V
    .registers 5

    .prologue
    .line 283
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f100397

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaY/c;->x:Landroid/view/ViewGroup;

    .line 285
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f100398

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->y:Landroid/widget/TextView;

    .line 286
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f100399

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->z:Landroid/widget/TextView;

    .line 288
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->A:Landroid/widget/TextView;

    .line 289
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10039c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LaY/c;->B:Landroid/widget/EditText;

    .line 291
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 292
    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->e()Ljava/lang/String;

    move-result-object v1

    .line 293
    iget-object v2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->f()Ljava/lang/String;

    move-result-object v2

    .line 294
    iget-object v3, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/cu;->i()Ljava/lang/Integer;

    move-result-object v3

    .line 297
    if-eqz v1, :cond_6f

    .line 298
    invoke-direct {p0, v1, v2, v0}, LaY/c;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 305
    :cond_6e
    :goto_6e
    return-void

    .line 300
    :cond_6f
    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->h()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, LaY/c;->E:Ljava/util/Map;

    .line 301
    iget-object v1, p0, LaY/c;->E:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6e

    .line 302
    iget-object v1, p0, LaY/c;->E:Ljava/util/Map;

    invoke-direct {p0, v1, v3, v0}, LaY/c;->a(Ljava/util/Map;Ljava/lang/Integer;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_6e
.end method

.method private n()V
    .registers 3

    .prologue
    .line 350
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 351
    const/16 v1, 0x420

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LaY/c;->C:Landroid/widget/EditText;

    .line 353
    iget-object v0, p0, LaY/c;->C:Landroid/widget/EditText;

    const/16 v1, 0x41f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, LaY/c;->C:Landroid/widget/EditText;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 355
    return-void
.end method

.method private o()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 358
    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040150

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 359
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003ab

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    .line 360
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_72

    .line 361
    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 362
    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    new-instance v2, LaY/i;

    invoke-direct {v2, p0}, LaY/i;-><init>(LaY/c;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    iget-object v2, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v0

    if-nez v0, :cond_70

    const/4 v0, 0x1

    :goto_3e
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 373
    :goto_41
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LaY/c;->b:Landroid/widget/Button;

    .line 374
    iget-object v0, p0, LaY/c;->b:Landroid/widget/Button;

    new-instance v1, LaY/j;

    invoke-direct {v1, p0}, LaY/j;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    .line 382
    iget-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    new-instance v1, LaY/k;

    invoke-direct {v1, p0}, LaY/k;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    return-void

    :cond_70
    move v0, v1

    .line 368
    goto :goto_3e

    .line 370
    :cond_72
    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_41
.end method

.method private v()Z
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 469
    move v1, v2

    :goto_2
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_25

    .line 470
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 471
    const v3, 0x7f100392

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 472
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_21

    .line 476
    :goto_20
    return v2

    .line 469
    :cond_21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 476
    :cond_25
    const/4 v2, 0x1

    goto :goto_20
.end method

.method private w()V
    .registers 6

    .prologue
    .line 531
    invoke-direct {p0}, LaY/c;->v()Z

    move-result v0

    if-nez v0, :cond_a

    .line 532
    invoke-direct {p0}, LaY/c;->x()V

    .line 542
    :goto_9
    return-void

    .line 535
    :cond_a
    const-string v0, "s"

    invoke-direct {p0, v0}, LaY/c;->a(Ljava/lang/String;)V

    .line 536
    invoke-virtual {p0}, LaY/c;->h()Lcom/google/googlenav/cx;

    move-result-object v0

    .line 537
    iget-object v1, p0, LaY/c;->C:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 538
    iget-object v2, p0, LaY/c;->B:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 539
    iget-object v3, p0, LaY/c;->x:Landroid/view/ViewGroup;

    invoke-virtual {p0, v3}, LaY/c;->a(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v3

    .line 541
    iget-object v4, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_9
.end method

.method private x()V
    .registers 5

    .prologue
    .line 545
    const/16 v0, 0x416

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 546
    const/16 v1, 0x35c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 548
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    .line 550
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, LaY/l;

    invoke-direct {v2, p0}, LaY/l;-><init>(LaY/c;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, LaY/c;->c:Landroid/app/AlertDialog;

    .line 558
    iget-object v0, p0, LaY/c;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 559
    return-void
.end method

.method private y()V
    .registers 11

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 613
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 614
    iget-object v1, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003bf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 615
    iget-object v2, p0, LaY/c;->r:Landroid/view/View;

    const v3, 0x7f1003be

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/HorizontalScrollView;

    .line 618
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    invoke-virtual {v1, v5, v3}, Landroid/widget/LinearLayout;->removeViews(II)V

    move v4, v5

    .line 619
    :goto_2b
    iget-object v3, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_6d

    .line 620
    iget-object v3, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/googlenav/ay;

    .line 622
    invoke-direct {p0, v3}, LaY/c;->b(Lcom/google/googlenav/ay;)Landroid/view/View;

    move-result-object v7

    .line 623
    new-instance v8, LaY/n;

    invoke-direct {v8, p0, v3}, LaY/n;-><init>(LaY/c;Lcom/google/googlenav/ay;)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 631
    const v8, 0x7f1003ba

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 632
    new-instance v9, LaY/e;

    invoke-direct {v9, p0, v3}, LaY/e;-><init>(LaY/c;Lcom/google/googlenav/ay;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 639
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 619
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2b

    .line 642
    :cond_6d
    const/16 v1, 0x42

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    .line 644
    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_b9

    .line 645
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 650
    :goto_85
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_9f

    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    if-eqz v0, :cond_9f

    .line 651
    iget-object v1, p0, LaY/c;->F:Landroid/view/MenuItem;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v0

    if-nez v0, :cond_bf

    move v0, v6

    :goto_9c
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 653
    :cond_9f
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_b8

    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    if-eqz v0, :cond_b8

    .line 654
    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v1

    if-nez v1, :cond_c1

    :goto_b5
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 656
    :cond_b8
    return-void

    .line 647
    :cond_b9
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_85

    :cond_bf
    move v0, v5

    .line 651
    goto :goto_9c

    :cond_c1
    move v6, v5

    .line 654
    goto :goto_b5
.end method

.method private z()Z
    .registers 3

    .prologue
    .line 659
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method


# virtual methods
.method public J_()V
    .registers 1

    .prologue
    .line 570
    invoke-direct {p0}, LaY/c;->y()V

    .line 571
    return-void
.end method

.method protected a(Landroid/view/View;)Ljava/lang/Integer;
    .registers 5
    .parameter

    .prologue
    .line 521
    sget-object v0, LaY/c;->o:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 522
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 523
    invoke-virtual {v1}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 524
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 527
    :goto_32
    return-object v0

    :cond_33
    const/4 v0, 0x0

    goto :goto_32
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 3
    .parameter

    .prologue
    .line 596
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 597
    const/16 v0, 0x402

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 598
    return-void
.end method

.method public a(Lcom/google/googlenav/ay;)V
    .registers 2
    .parameter

    .prologue
    .line 566
    invoke-direct {p0}, LaY/c;->y()V

    .line 567
    return-void
.end method

.method public a(Lcom/google/googlenav/cx;)V
    .registers 3
    .parameter

    .prologue
    .line 238
    iget-object v0, p1, Lcom/google/googlenav/cx;->b:Lcom/google/common/collect/ImmutableList;

    iput-object v0, p0, LaY/c;->K:Ljava/util/List;

    .line 239
    iget-object v0, p1, Lcom/google/googlenav/cx;->d:Ljava/lang/String;

    iput-object v0, p0, LaY/c;->L:Ljava/lang/String;

    .line 240
    invoke-direct {p0}, LaY/c;->l()V

    .line 241
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/br;)V
    .registers 2
    .parameter

    .prologue
    .line 717
    iput-object p1, p0, LaY/c;->J:Lcom/google/googlenav/ui/br;

    .line 718
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 446
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1003ac

    if-ne v1, v2, :cond_e

    .line 447
    invoke-direct {p0}, LaY/c;->w()V

    .line 456
    :goto_d
    return v0

    .line 449
    :cond_e
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1003ab

    if-ne v1, v2, :cond_1d

    .line 450
    iget-object v1, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/hh;->e()V

    goto :goto_d

    .line 452
    :cond_1d
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f1003ad

    if-ne v1, v2, :cond_2c

    .line 453
    iget-object v1, p0, LaY/c;->l:Lcom/google/googlenav/ui/wizard/hh;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/hh;->f()V

    goto :goto_d

    .line 456
    :cond_2c
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 435
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    if-eqz v0, :cond_1b

    .line 436
    iget-object v2, p0, LaY/c;->F:Landroid/view/MenuItem;

    invoke-direct {p0}, LaY/c;->z()Z

    move-result v0

    if-nez v0, :cond_2f

    move v0, v1

    :goto_18
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 438
    :cond_1b
    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    if-eqz v0, :cond_2e

    .line 439
    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    iget-object v2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->b()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 441
    :cond_2e
    return v1

    .line 436
    :cond_2f
    const/4 v0, 0x0

    goto :goto_18
.end method

.method protected c()Landroid/view/View;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 172
    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04014e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LaY/c;->r:Landroid/view/View;

    .line 173
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1001fd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LaY/c;->s:Landroid/view/ViewGroup;

    .line 175
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 176
    new-instance v1, LaY/g;

    invoke-direct {v1, p0}, LaY/g;-><init>(LaY/c;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10037d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->t:Landroid/widget/TextView;

    .line 184
    iget-object v0, p0, LaY/c;->t:Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10037e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->u:Landroid/widget/TextView;

    .line 187
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_65

    .line 188
    iget-object v0, p0, LaY/c;->u:Landroid/widget/TextView;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_65
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f10037c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LaY/c;->v:Landroid/widget/ImageView;

    .line 192
    invoke-direct {p0}, LaY/c;->A()V

    .line 194
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    .line 197
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v2, 0x7f1003a7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 199
    iget-object v2, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/cu;->b()Z

    move-result v2

    .line 200
    if-nez v2, :cond_112

    iget-object v2, p0, LaY/c;->M:Lcom/google/googlenav/ui/wizard/hb;

    if-eqz v2, :cond_112

    .line 201
    const/16 v2, 0x421

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 203
    new-instance v2, LaY/h;

    invoke-direct {v2, p0}, LaY/h;-><init>(LaY/c;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 214
    :goto_aa
    invoke-virtual {v1}, Lcom/google/googlenav/cu;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LaY/c;->K:Ljava/util/List;

    .line 215
    invoke-direct {p0}, LaY/c;->l()V

    .line 216
    invoke-direct {p0}, LaY/c;->m()V

    .line 217
    invoke-direct {p0}, LaY/c;->n()V

    .line 219
    invoke-virtual {p0}, LaY/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0401cd

    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 220
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const v1, 0x7f1003aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LaY/c;->D:Landroid/widget/TextView;

    .line 222
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_e1

    .line 223
    invoke-direct {p0}, LaY/c;->o()V

    .line 226
    :cond_e1
    invoke-virtual {p0}, LaY/c;->J_()V

    .line 228
    invoke-static {}, Lcom/google/googlenav/bm;->j()Ljava/lang/String;

    move-result-object v0

    .line 229
    iget-object v1, p0, LaY/c;->D:Landroid/widget/TextView;

    if-eqz v0, :cond_118

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_118

    const/16 v2, 0x40a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_101
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    const/16 v1, 0x402

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LaY/c;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 234
    iget-object v0, p0, LaY/c;->r:Landroid/view/View;

    return-object v0

    .line 210
    :cond_112
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_aa

    .line 229
    :cond_118
    const/16 v0, 0x40b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_101
.end method

.method protected d()V
    .registers 4

    .prologue
    .line 392
    iget-object v0, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v1

    .line 393
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_36

    .line 396
    iget-object v2, p0, LaY/c;->b:Landroid/widget/Button;

    if-eqz v1, :cond_54

    const/16 v0, 0x41b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_1e
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 398
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 399
    iget-object v0, p0, LaY/c;->G:Landroid/widget/Button;

    const/16 v2, 0x41d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 403
    :cond_36
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_53

    if-eqz v1, :cond_53

    .line 404
    iget-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 405
    iget-object v0, p0, LaY/c;->I:Landroid/widget/Button;

    const/16 v1, 0x3f3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 407
    :cond_53
    return-void

    .line 396
    :cond_54
    const/16 v0, 0x409

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1e
.end method

.method h()Lcom/google/googlenav/cx;
    .registers 11

    .prologue
    const/4 v9, 0x0

    .line 481
    const/4 v0, 0x0

    .line 482
    new-instance v7, Ljava/util/ArrayList;

    iget-object v1, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 483
    iget-object v1, p0, LaY/c;->K:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v0

    :cond_14
    :goto_14
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/googlenav/cy;

    .line 484
    iget-boolean v0, v2, Lcom/google/googlenav/cy;->e:Z

    if-eqz v0, :cond_14

    .line 487
    iget-object v0, p0, LaY/c;->w:Landroid/view/ViewGroup;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 488
    invoke-direct {p0, v0}, LaY/c;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    .line 489
    invoke-direct {p0, v0}, LaY/c;->c(Landroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 490
    if-eqz v3, :cond_44

    .line 491
    new-instance v0, Lcom/google/googlenav/cy;

    iget-object v1, v2, Lcom/google/googlenav/cy;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/googlenav/cy;->b:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/cy;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_44
    move v1, v6

    .line 494
    goto :goto_14

    .line 495
    :cond_46
    new-instance v0, Lcom/google/googlenav/cx;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v7, v9, v9}, Lcom/google/googlenav/cx;-><init>(ILjava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public j()V
    .registers 1

    .prologue
    .line 562
    invoke-direct {p0}, LaY/c;->y()V

    .line 563
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 411
    sget-object v0, LaY/c;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001f

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 415
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 416
    const v0, 0x7f1003ab

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    .line 417
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 418
    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    const/16 v1, 0x41d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 423
    :goto_34
    const v0, 0x7f1003ac

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LaY/c;->a:Landroid/view/MenuItem;

    .line 424
    iget-object v0, p0, LaY/c;->a:Landroid/view/MenuItem;

    const/16 v1, 0x410

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 427
    :cond_48
    const v0, 0x7f1003ad

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    .line 428
    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    const/16 v1, 0x3f3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 429
    iget-object v0, p0, LaY/c;->H:Landroid/view/MenuItem;

    iget-object v1, p0, LaY/c;->m:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/cu;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 430
    const/4 v0, 0x1

    return v0

    .line 420
    :cond_6d
    iget-object v0, p0, LaY/c;->F:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_34
.end method
