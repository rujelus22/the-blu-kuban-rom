.class public final Lae/j;
.super Ljava/util/concurrent/FutureTask;
.source "SourceFile"

# interfaces
.implements Lae/i;


# instance fields
.field private final a:Lae/e;


# direct methods
.method private constructor <init>(Ljava/lang/Runnable;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 38
    new-instance v0, Lae/e;

    invoke-direct {v0}, Lae/e;-><init>()V

    iput-object v0, p0, Lae/j;->a:Lae/e;

    .line 74
    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Callable;)V
    .registers 3
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 38
    new-instance v0, Lae/e;

    invoke-direct {v0}, Lae/e;-><init>()V

    iput-object v0, p0, Lae/j;->a:Lae/e;

    .line 70
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Ljava/lang/Object;)Lae/j;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    new-instance v0, Lae/j;

    invoke-direct {v0, p0, p1}, Lae/j;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Lae/j;
    .registers 2
    .parameter

    .prologue
    .line 48
    new-instance v0, Lae/j;

    invoke-direct {v0, p0}, Lae/j;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lae/j;->a:Lae/e;

    invoke-virtual {v0, p1, p2}, Lae/e;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 79
    return-void
.end method

.method protected done()V
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, Lae/j;->a:Lae/e;

    invoke-virtual {v0}, Lae/e;->a()V

    .line 87
    return-void
.end method
