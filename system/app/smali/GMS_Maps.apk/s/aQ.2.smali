.class Ls/aQ;
.super Ls/d;
.source "SourceFile"


# instance fields
.field final synthetic d:Ls/aP;


# direct methods
.method constructor <init>(Ls/aP;)V
    .registers 2
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Ls/aQ;->d:Ls/aP;

    invoke-direct {p0, p1}, Ls/d;-><init>(Ls/c;)V

    return-void
.end method


# virtual methods
.method protected a(II)[B
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 141
    invoke-static {}, Lo/aF;->u()I

    move-result v0

    add-int/2addr v0, p1

    new-array v0, v0, [B

    .line 142
    iget v1, p0, Ls/aQ;->a:I

    invoke-static {v1, p2, v0}, Lo/aF;->a(II[B)V

    .line 143
    return-object v0
.end method

.method protected b(I)Lo/al;
    .registers 10
    .parameter

    .prologue
    const-wide/16 v6, -0x1

    .line 117
    iget-object v0, p0, Ls/aQ;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_a

    .line 118
    const/4 v0, 0x0

    .line 131
    :goto_9
    return-object v0

    .line 122
    :cond_a
    iget-object v0, p0, Ls/aQ;->d:Ls/aP;

    iget-wide v0, v0, Ls/aP;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_43

    .line 123
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->u()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iget-object v2, p0, Ls/aQ;->d:Ls/aP;

    iget-wide v2, v2, Ls/aP;->i:J

    add-long v4, v0, v2

    .line 126
    :goto_26
    invoke-virtual {p0, p1}, Ls/aQ;->a(I)Ls/m;

    move-result-object v0

    invoke-virtual {v0}, Ls/m;->c()Lo/am;

    move-result-object v0

    iget-object v1, p0, Ls/aQ;->b:[[B

    aget-object v1, v1, p1

    const/4 v2, 0x0

    iget-object v3, p0, Ls/aQ;->d:Ls/aP;

    iget-object v3, v3, Ls/aP;->a:Lcom/google/android/maps/driveabout/vector/di;

    invoke-static/range {v0 .. v7}, Lo/aF;->a(Lo/am;[BILcom/google/android/maps/driveabout/vector/di;JJ)Lo/aF;

    move-result-object v0

    .line 130
    iget-object v1, p0, Ls/aQ;->d:Ls/aP;

    iget-object v1, v1, Ls/aP;->d:Lcom/google/googlenav/common/a;

    invoke-interface {v0, v1}, Lo/al;->c(Lcom/google/googlenav/common/a;)V

    goto :goto_9

    :cond_43
    move-wide v4, v6

    goto :goto_26
.end method

.method protected c(I)[B
    .registers 3
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Ls/aQ;->b:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
