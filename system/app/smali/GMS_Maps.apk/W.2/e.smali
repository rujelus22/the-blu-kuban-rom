.class public LW/e;
.super LW/k;
.source "SourceFile"


# static fields
.field public static final a:LW/e;


# instance fields
.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 18
    new-instance v0, LW/e;

    invoke-direct {v0}, LW/e;-><init>()V

    sput-object v0, LW/e;->a:LW/e;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, LW/k;-><init>()V

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, LW/e;->h:Z

    .line 25
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 2

    .prologue
    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, LW/e;->h:Z

    .line 30
    return-void
.end method

.method public a(LU/z;)V
    .registers 4
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, LW/e;->b:LW/h;

    sget-object v1, LW/n;->a:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    move-result-object v0

    invoke-virtual {v0, p1}, LW/k;->a(LU/z;)V

    .line 60
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 69
    invoke-virtual {p0}, LW/e;->l()Lcom/google/android/maps/rideabout/view/i;

    move-result-object v0

    iget-object v1, p0, LW/e;->d:LW/g;

    invoke-virtual {v1}, LW/g;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/rideabout/view/i;->a(Z)V

    .line 70
    invoke-virtual {p0}, LW/e;->m()Lcom/google/android/maps/rideabout/view/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/rideabout/view/h;->b()V

    .line 71
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 34
    invoke-super {p0}, LW/k;->b()V

    .line 35
    iget-boolean v0, p0, LW/e;->h:Z

    if-eqz v0, :cond_15

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, LW/e;->h:Z

    .line 37
    iget-object v0, p0, LW/e;->g:Lbi/d;

    invoke-virtual {v0}, Lbi/d;->b()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 38
    invoke-virtual {p0}, LW/e;->n()V

    .line 43
    :cond_15
    :goto_15
    return-void

    .line 40
    :cond_16
    invoke-virtual {p0}, LW/e;->o()V

    goto :goto_15
.end method

.method public b(LU/z;)V
    .registers 4
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, LW/e;->b:LW/h;

    sget-object v1, LW/n;->c:LW/n;

    invoke-virtual {v0, v1}, LW/h;->a(LW/n;)LW/k;

    .line 65
    return-void
.end method

.method protected e()V
    .registers 3

    .prologue
    .line 47
    invoke-virtual {p0}, LW/e;->k()Lcom/google/android/maps/rideabout/view/j;

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LW/e;->a(Z)V

    .line 49
    iget-object v0, p0, LW/e;->c:LW/j;

    invoke-virtual {v0}, LW/j;->a()LU/z;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_14

    .line 51
    iget-object v1, p0, LW/e;->c:LW/j;

    invoke-virtual {v1, v0}, LW/j;->c(LU/z;)V

    .line 53
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LW/e;->b(Z)V

    .line 54
    return-void
.end method
