.class public Ly/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ly/b;


# instance fields
.field private A:Ljava/util/Set;

.field private final B:Ljava/util/List;

.field private C:Ljava/util/Iterator;

.field private final D:Ljava/util/Comparator;

.field private E:LZ/a;

.field private F:I

.field private final G:Ljava/util/Map;

.field private final b:Lcom/google/android/maps/driveabout/vector/aV;

.field private volatile c:LG/a;

.field private final d:LD/c;

.field private e:Lo/h;

.field private f:LC/a;

.field private final g:LD/a;

.field private h:F

.field private i:Lo/aS;

.field private j:Lcom/google/android/maps/driveabout/vector/aF;

.field private k:Ljava/util/Iterator;

.field private l:Ljava/util/ArrayList;

.field private m:I

.field private n:Ljava/util/ArrayList;

.field private o:I

.field private final p:Ljava/util/Map;

.field private q:I

.field private r:F

.field private s:I

.field private t:I

.field private u:Z

.field private v:Z

.field private volatile w:Z

.field private x:Z

.field private y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 101
    new-instance v0, Ly/b;

    invoke-direct {v0}, Ly/b;-><init>()V

    sput-object v0, Ly/d;->a:Ly/b;

    return-void
.end method

.method public constructor <init>(LG/a;LD/a;Landroid/content/res/Resources;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ly/d;->p:Ljava/util/Map;

    .line 251
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ly/d;->B:Ljava/util/List;

    .line 264
    new-instance v0, Ly/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ly/f;-><init>(Ly/e;)V

    iput-object v0, p0, Ly/d;->D:Ljava/util/Comparator;

    .line 272
    const/4 v0, 0x0

    iput v0, p0, Ly/d;->F:I

    .line 281
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Ly/d;->G:Ljava/util/Map;

    .line 284
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    .line 285
    iput-object p1, p0, Ly/d;->c:LG/a;

    .line 286
    iput-object p2, p0, Ly/d;->g:LD/a;

    .line 287
    new-instance v0, LD/c;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, LD/c;-><init>(I)V

    iput-object v0, p0, Ly/d;->d:LD/c;

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    .line 289
    iput v2, p0, Ly/d;->m:I

    .line 290
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    .line 291
    iput v2, p0, Ly/d;->o:I

    .line 292
    return-void
.end method

.method static final a(LC/a;)I
    .registers 5
    .parameter

    .prologue
    const v3, 0x48435000

    .line 311
    invoke-virtual {p0}, LC/a;->m()F

    move-result v0

    .line 312
    invoke-virtual {p0}, LC/a;->l()I

    move-result v1

    invoke-virtual {p0}, LC/a;->k()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v0

    div-float v0, v1, v0

    .line 314
    cmpl-float v1, v0, v3

    if-lez v1, :cond_22

    .line 315
    sub-float/2addr v0, v3

    const v1, 0x38d1b717

    mul-float/2addr v0, v1

    const/high16 v1, 0x4230

    add-float/2addr v0, v1

    .line 320
    :goto_20
    float-to-int v0, v0

    return v0

    .line 318
    :cond_22
    const v1, 0x3966afcd

    mul-float/2addr v0, v1

    goto :goto_20
.end method

.method static a(Lo/H;Lo/aj;)I
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 774
    if-nez p0, :cond_4

    .line 801
    :goto_3
    return v1

    .line 782
    :cond_4
    if-eqz p1, :cond_63

    invoke-virtual {p1}, Lo/aj;->e()Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-virtual {p1}, Lo/aj;->h()Lo/ao;

    move-result-object v0

    invoke-virtual {v0}, Lo/ao;->f()I

    move-result v0

    :goto_14
    move v2, v1

    .line 784
    :goto_15
    invoke-virtual {p0}, Lo/H;->b()I

    move-result v3

    if-ge v1, v3, :cond_66

    .line 785
    invoke-virtual {p0, v1}, Lo/H;->a(I)Lo/I;

    move-result-object v4

    .line 786
    invoke-virtual {v4}, Lo/I;->c()Z

    move-result v3

    if-eqz v3, :cond_4b

    .line 788
    invoke-virtual {v4}, Lo/I;->d()Z

    move-result v3

    if-eqz v3, :cond_68

    invoke-virtual {v4}, Lo/I;->j()Lo/aj;

    move-result-object v3

    invoke-virtual {v3}, Lo/aj;->e()Z

    move-result v3

    if-eqz v3, :cond_68

    .line 789
    invoke-virtual {v4}, Lo/I;->j()Lo/aj;

    move-result-object v3

    invoke-virtual {v3}, Lo/aj;->h()Lo/ao;

    move-result-object v3

    invoke-virtual {v3}, Lo/ao;->f()I

    move-result v3

    .line 791
    :goto_41
    invoke-virtual {v4}, Lo/I;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    mul-int/2addr v3, v5

    add-int/2addr v2, v3

    .line 793
    :cond_4b
    invoke-virtual {v4}, Lo/I;->b()Z

    move-result v3

    if-eqz v3, :cond_53

    .line 794
    add-int/lit8 v2, v2, 0x8

    .line 796
    :cond_53
    invoke-virtual {v4}, Lo/I;->e()Z

    move-result v3

    if-eqz v3, :cond_60

    .line 797
    int-to-float v2, v2

    invoke-virtual {v4}, Lo/I;->k()F

    move-result v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 784
    :cond_60
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 782
    :cond_63
    const/16 v0, 0xa

    goto :goto_14

    :cond_66
    move v1, v2

    .line 801
    goto :goto_3

    :cond_68
    move v3, v0

    goto :goto_41
.end method

.method static a(Lo/n;)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 723
    invoke-interface {p0}, Lo/n;->h()I

    move-result v1

    packed-switch v1, :pswitch_data_60

    :pswitch_8
    move v1, v0

    .line 764
    :cond_9
    :goto_9
    return v1

    .line 727
    :pswitch_a
    check-cast p0, Lo/af;

    move v1, v0

    .line 728
    :goto_d
    invoke-virtual {p0}, Lo/af;->d()I

    move-result v2

    if-ge v0, v2, :cond_9

    .line 729
    invoke-virtual {p0, v0}, Lo/af;->c(I)Lo/H;

    move-result-object v2

    invoke-virtual {p0}, Lo/af;->e()Lo/aj;

    move-result-object v3

    invoke-static {v2, v3}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 728
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 736
    :pswitch_26
    check-cast p0, Lo/U;

    .line 737
    invoke-virtual {p0}, Lo/U;->p()Lo/H;

    move-result-object v0

    invoke-virtual {p0}, Lo/U;->e()Lo/aj;

    move-result-object v1

    invoke-static {v0, v1}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v0

    invoke-virtual {p0}, Lo/U;->q()Lo/H;

    move-result-object v1

    invoke-virtual {p0}, Lo/U;->e()Lo/aj;

    move-result-object v2

    invoke-static {v1, v2}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v1

    add-int/2addr v1, v0

    goto :goto_9

    .line 745
    :pswitch_42
    check-cast p0, Lo/K;

    move v1, v0

    .line 746
    :goto_45
    invoke-virtual {p0}, Lo/K;->c()I

    move-result v2

    if-ge v0, v2, :cond_9

    .line 747
    invoke-virtual {p0, v0}, Lo/K;->a(I)Lo/H;

    move-result-object v2

    invoke-virtual {p0}, Lo/K;->e()Lo/aj;

    move-result-object v3

    invoke-static {v2, v3}, Ly/d;->a(Lo/H;Lo/aj;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 746
    add-int/lit8 v0, v0, 0x1

    goto :goto_45

    :pswitch_5e
    move v1, v0

    .line 756
    goto :goto_9

    .line 723
    :pswitch_data_60
    .packed-switch 0x2
        :pswitch_a
        :pswitch_5e
        :pswitch_5e
        :pswitch_5e
        :pswitch_8
        :pswitch_26
        :pswitch_42
        :pswitch_8
        :pswitch_8
        :pswitch_42
    .end packed-switch
.end method

.method private a(Lo/K;Ly/b;Z)LF/m;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1079
    invoke-virtual {p1}, Lo/K;->c()I

    move-result v1

    if-nez v1, :cond_8

    .line 1105
    :cond_7
    :goto_7
    return-object v0

    .line 1082
    :cond_8
    invoke-virtual {p1}, Lo/K;->b()Lo/X;

    move-result-object v1

    .line 1083
    invoke-virtual {v1}, Lo/X;->a()Lo/ad;

    move-result-object v2

    .line 1085
    iget-object v3, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {v3, v2}, Lo/aS;->b(Lo/ae;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1088
    invoke-direct {p0, v1}, Ly/d;->a(Lo/X;)Lo/X;

    move-result-object v3

    .line 1089
    if-eqz v3, :cond_7

    .line 1090
    invoke-virtual {p1}, Lo/K;->e()Lo/aj;

    move-result-object v0

    iget-object v1, p0, Ly/d;->c:LG/a;

    iget v1, v1, LG/a;->i:F

    iget-object v2, p0, Ly/d;->c:LG/a;

    iget v2, v2, LG/a;->j:I

    iget-object v4, p0, Ly/d;->c:LG/a;

    iget v4, v4, LG/a;->k:I

    iget-object v5, p0, Ly/d;->f:LC/a;

    invoke-virtual {v5}, LC/a;->m()F

    move-result v5

    invoke-static {v0, v1, v2, v4, v5}, LF/m;->a(Lo/aj;FIIF)F

    move-result v5

    .line 1098
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lo/K;->a(I)Lo/H;

    move-result-object v2

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget-object v6, v0, LG/a;->h:Lcom/google/android/maps/driveabout/vector/aX;

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget v7, v0, LG/a;->l:F

    iget-object v8, p0, Ly/d;->f:LC/a;

    iget-object v9, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v0, p0, Ly/d;->c:LG/a;

    iget-boolean v10, v0, LG/a;->q:Z

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    invoke-static/range {v0 .. v10}, LF/D;->a(Lo/K;Ly/b;Lo/H;Lo/X;ZFLcom/google/android/maps/driveabout/vector/aX;FLC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v0

    goto :goto_7
.end method

.method private a(Lo/U;Ly/b;Z)LF/m;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 969
    invoke-virtual {p1}, Lo/U;->o()[Lo/a;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lo/a;->b()Lo/T;

    move-result-object v1

    .line 970
    invoke-direct {p0, v1, p2}, Ly/d;->a(Lo/T;Ly/b;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 973
    invoke-virtual {p1}, Lo/U;->k()F

    move-result v1

    iget v2, p0, Ly/d;->h:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_36

    invoke-virtual {p1}, Lo/U;->n()F

    move-result v1

    const/high16 v2, -0x4080

    cmpl-float v1, v1, v2

    if-lez v1, :cond_37

    invoke-virtual {p1}, Lo/U;->n()F

    move-result v1

    iget v2, p0, Ly/d;->h:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_37

    invoke-direct {p0, p1}, Ly/d;->a(Lo/U;)Z

    move-result v1

    if-nez v1, :cond_37

    .line 991
    :cond_36
    :goto_36
    return-object v0

    .line 981
    :cond_37
    iget-object v3, p0, Ly/d;->f:LC/a;

    iget-object v4, p0, Ly/d;->d:LD/c;

    iget-object v5, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    iget-object v6, p0, Ly/d;->c:LG/a;

    iget-object v7, p0, Ly/d;->E:LZ/a;

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v7}, LF/L;->a(Lo/U;Ly/b;ZLC/a;LD/c;Lcom/google/android/maps/driveabout/vector/aV;LG/a;LZ/a;)LF/L;

    move-result-object v0

    goto :goto_36
.end method

.method private a(Lo/X;)Lo/X;
    .registers 10
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1113
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1114
    iget-object v0, p0, Ly/d;->e:Lo/h;

    invoke-virtual {v0, p1, v5}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    .line 1115
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1116
    if-nez v6, :cond_14

    .line 1117
    const/4 v0, 0x0

    .line 1130
    :goto_13
    return-object v0

    .line 1118
    :cond_14
    if-ne v6, v1, :cond_1d

    .line 1119
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    goto :goto_13

    .line 1121
    :cond_1d
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 1122
    invoke-virtual {v0}, Lo/X;->d()F

    move-result v2

    move v4, v1

    move-object v3, v0

    .line 1123
    :goto_29
    if-ge v4, v6, :cond_48

    .line 1124
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->d()F

    move-result v1

    .line 1125
    cmpl-float v0, v1, v2

    if-lez v0, :cond_4a

    .line 1127
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    move v7, v1

    move-object v1, v0

    move v0, v7

    .line 1123
    :goto_42
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_29

    :cond_48
    move-object v0, v3

    .line 1130
    goto :goto_13

    :cond_4a
    move v0, v2

    move-object v1, v3

    goto :goto_42
.end method

.method static a(Ljava/util/Map;Ljava/util/Map;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 593
    invoke-interface {p1}, Ljava/util/Map;->clear()V

    .line 594
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 614
    :cond_9
    return-void

    .line 597
    :cond_a
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v1

    .line 598
    sget-object v0, Ly/d;->a:Ly/b;

    new-instance v2, Lo/k;

    invoke-direct {v2, v1}, Lo/k;-><init>(I)V

    invoke-interface {p1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/b;

    .line 600
    new-instance v3, Lo/k;

    add-int/lit8 v4, v1, -0x1

    invoke-direct {v3, v4}, Lo/k;-><init>(I)V

    invoke-interface {p1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_20

    .line 603
    :cond_37
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/b;

    .line 604
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/j;

    .line 605
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_59
    :goto_59
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ly/b;

    .line 606
    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_59

    .line 609
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lo/k;

    .line 611
    invoke-virtual {v2, v1}, Lo/k;->a(Lo/j;)V

    goto :goto_59
.end method

.method private a(Lo/K;Ly/b;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1065
    invoke-direct {p0, p1, p2, p3}, Ly/d;->a(Lo/K;Ly/b;Z)LF/m;

    move-result-object v0

    .line 1066
    invoke-direct {p0, v0}, Ly/d;->c(LF/m;)Z

    move-result v1

    if-eqz v1, :cond_10

    if-eqz p4, :cond_10

    .line 1067
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LF/m;->a_(Z)V

    .line 1069
    :cond_10
    return-void
.end method

.method private a(Lo/U;Ly/b;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 954
    invoke-direct {p0, p1, p2, p3}, Ly/d;->a(Lo/U;Ly/b;Z)LF/m;

    move-result-object v0

    .line 955
    invoke-direct {p0, v0}, Ly/d;->c(LF/m;)Z

    move-result v1

    if-eqz v1, :cond_10

    if-eqz p4, :cond_10

    .line 956
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LF/m;->a_(Z)V

    .line 958
    :cond_10
    return-void
.end method

.method private a(Lo/af;Ly/b;ZZ)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1000
    invoke-virtual/range {p1 .. p1}, Lo/af;->b()Lo/X;

    move-result-object v1

    .line 1001
    invoke-virtual {v1}, Lo/X;->a()Lo/ad;

    move-result-object v2

    .line 1003
    move-object/from16 v0, p0

    iget-object v3, v0, Ly/d;->i:Lo/aS;

    invoke-virtual {v3, v2}, Lo/aS;->b(Lo/ae;)Z

    move-result v2

    if-eqz v2, :cond_af

    .line 1004
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Ly/d;->a(Lo/X;)Lo/X;

    move-result-object v15

    .line 1005
    if-eqz v15, :cond_af

    invoke-virtual {v15}, Lo/X;->d()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Ly/d;->f:LC/a;

    invoke-virtual {v2}, LC/a;->y()F

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ly/d;->f:LC/a;

    invoke-virtual {v3}, LC/a;->m()F

    move-result v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x4220

    mul-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_af

    .line 1008
    const/4 v2, 0x0

    :goto_37
    invoke-virtual/range {p1 .. p1}, Lo/af;->d()I

    move-result v1

    if-ge v2, v1, :cond_af

    .line 1010
    const v1, 0x3f333333

    invoke-virtual {v15, v1}, Lo/X;->a(F)Lo/T;

    move-result-object v4

    .line 1011
    const v1, 0x3e99999a

    invoke-virtual {v15, v1}, Lo/X;->a(F)Lo/T;

    move-result-object v5

    .line 1012
    move-object/from16 v0, p0

    iget-object v7, v0, Ly/d;->c:LG/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Ly/d;->f:LC/a;

    move-object/from16 v0, p0

    iget-object v9, v0, Ly/d;->d:LD/c;

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move/from16 v6, p3

    invoke-static/range {v1 .. v9}, LF/L;->a(Lo/af;ILy/b;Lo/T;Lo/T;ZLG/a;LC/a;LD/c;)LF/L;

    move-result-object v1

    .line 1016
    if-nez v1, :cond_9c

    .line 1017
    invoke-virtual/range {p0 .. p0}, Ly/d;->e()F

    move-result v9

    .line 1018
    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget-boolean v1, v1, LG/a;->o:Z

    if-eqz v1, :cond_ad

    const/16 v7, 0xa

    .line 1020
    :goto_71
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lo/af;->c(I)Lo/H;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget v10, v1, LG/a;->l:F

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget-object v11, v1, LG/a;->a:Lcom/google/android/maps/driveabout/vector/aX;

    move-object/from16 v0, p0

    iget-object v12, v0, Ly/d;->f:LC/a;

    move-object/from16 v0, p0

    iget-object v13, v0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    move-object/from16 v0, p0

    iget-object v1, v0, Ly/d;->c:LG/a;

    iget-boolean v14, v1, LG/a;->q:Z

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object v6, v15

    move/from16 v8, p3

    invoke-static/range {v3 .. v14}, LF/D;->a(Lo/af;Ly/b;Lo/H;Lo/X;IZFFLcom/google/android/maps/driveabout/vector/aX;LC/a;Lcom/google/android/maps/driveabout/vector/aV;Z)LF/D;

    move-result-object v1

    .line 1026
    :cond_9c
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Ly/d;->c(LF/m;)Z

    move-result v3

    if-eqz v3, :cond_aa

    if-eqz p4, :cond_aa

    .line 1027
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, LF/m;->a_(Z)V

    .line 1008
    :cond_aa
    add-int/lit8 v2, v2, 0x1

    goto :goto_37

    .line 1018
    :cond_ad
    const/4 v7, 0x0

    goto :goto_71

    .line 1032
    :cond_af
    return-void
.end method

.method private a(Lo/n;Ly/b;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 937
    invoke-direct {p0, p2}, Ly/d;->a(Ly/b;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 947
    :cond_6
    :goto_6
    return-void

    .line 940
    :cond_7
    instance-of v0, p1, Lo/af;

    if-eqz v0, :cond_11

    .line 941
    check-cast p1, Lo/af;

    invoke-direct {p0, p1, p2, p3, p4}, Ly/d;->a(Lo/af;Ly/b;ZZ)V

    goto :goto_6

    .line 942
    :cond_11
    instance-of v0, p1, Lo/K;

    if-eqz v0, :cond_1b

    .line 943
    check-cast p1, Lo/K;

    invoke-direct {p0, p1, p2, p3, p4}, Ly/d;->a(Lo/K;Ly/b;ZZ)V

    goto :goto_6

    .line 944
    :cond_1b
    instance-of v0, p1, Lo/U;

    if-eqz v0, :cond_6

    .line 945
    check-cast p1, Lo/U;

    invoke-direct {p0, p1, p2, p3, p4}, Ly/d;->a(Lo/U;Ly/b;ZZ)V

    goto :goto_6
.end method

.method private a(J)Z
    .registers 11
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 818
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1e

    .line 819
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot run labeler loop until all previous labels have either been copied into new label table or destroyed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 831
    :cond_13
    iget-object v0, p0, Ly/d;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/n;

    invoke-direct {p0, v0, v7, v4, v4}, Ly/d;->a(Lo/n;Ly/b;ZZ)V

    .line 826
    :cond_1e
    iget-object v0, p0, Ly/d;->k:Ljava/util/Iterator;

    if-eqz v0, :cond_38

    iget-object v0, p0, Ly/d;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    iget v0, p0, Ly/d;->s:I

    iget v1, p0, Ly/d;->q:I

    if-ge v0, v1, :cond_38

    .line 827
    iget-boolean v0, p0, Ly/d;->z:Z

    if-eqz v0, :cond_13

    .line 828
    iput-boolean v2, p0, Ly/d;->z:Z

    move v2, v4

    .line 906
    :cond_37
    :goto_37
    return v2

    :cond_38
    move v1, v2

    .line 843
    :goto_39
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    if-nez v0, :cond_45

    iget-object v0, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    :cond_45
    iget v0, p0, Ly/d;->s:I

    iget v3, p0, Ly/d;->q:I

    if-lt v0, v3, :cond_6d

    iget-object v0, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v3, p0, Ly/d;->m:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    invoke-virtual {v0}, LF/m;->t()I

    move-result v0

    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->b()Ly/c;

    move-result-object v3

    invoke-virtual {v3}, Ly/c;->b()I

    move-result v3

    if-gt v0, v3, :cond_37

    .line 845
    :cond_6d
    iget-boolean v0, p0, Ly/d;->z:Z

    if-eqz v0, :cond_75

    .line 846
    iput-boolean v2, p0, Ly/d;->z:Z

    move v2, v4

    .line 847
    goto :goto_37

    .line 849
    :cond_75
    if-lez v1, :cond_81

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    cmp-long v0, v5, p1

    if-ltz v0, :cond_81

    move v2, v4

    .line 850
    goto :goto_37

    .line 855
    :cond_81
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    if-nez v0, :cond_c3

    .line 861
    iget-object v0, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v0

    .line 863
    iget-object v3, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 866
    :goto_90
    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b4

    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->b()Ly/c;

    move-result-object v3

    invoke-virtual {v3}, Ly/c;->b()I

    move-result v3

    invoke-virtual {v0}, Ly/c;->b()I

    move-result v5

    if-ne v3, v5, :cond_b4

    .line 867
    iget-object v3, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v3

    .line 868
    iget-object v5, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_90

    .line 872
    :cond_b4
    iget-object v0, p0, Ly/d;->B:Ljava/util/List;

    iget-object v3, p0, Ly/d;->D:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 873
    iget-object v0, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    .line 877
    :cond_c3
    :goto_c3
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_128

    .line 879
    add-int/lit8 v3, v1, 0x1

    if-lez v1, :cond_da

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-ltz v0, :cond_da

    move v2, v4

    .line 880
    goto/16 :goto_37

    .line 882
    :cond_da
    iget-boolean v0, p0, Ly/d;->z:Z

    if-eqz v0, :cond_e3

    .line 883
    iput-boolean v2, p0, Ly/d;->z:Z

    move v2, v4

    .line 884
    goto/16 :goto_37

    .line 887
    :cond_e3
    iget-object v0, p0, Ly/d;->C:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ly/c;

    .line 888
    iget v1, p0, Ly/d;->s:I

    iget v5, p0, Ly/d;->q:I

    if-lt v1, v5, :cond_110

    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v5, p0, Ly/d;->m:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/m;

    invoke-virtual {v1}, LF/m;->t()I

    move-result v1

    invoke-virtual {v0}, Ly/c;->b()I

    move-result v5

    if-lt v1, v5, :cond_110

    move v0, v3

    .line 903
    :goto_106
    iget-object v1, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 904
    iput-object v7, p0, Ly/d;->C:Ljava/util/Iterator;

    move v1, v0

    goto/16 :goto_39

    .line 897
    :cond_110
    invoke-virtual {v0}, Ly/c;->a()Lo/n;

    move-result-object v1

    invoke-virtual {v0}, Ly/c;->c()Ly/b;

    move-result-object v0

    invoke-direct {p0, v1, v0, v2, v4}, Ly/d;->a(Lo/n;Ly/b;ZZ)V

    .line 899
    iget v0, p0, Ly/d;->s:I

    iget v1, p0, Ly/d;->q:I

    if-le v0, v1, :cond_126

    .line 900
    iget v0, p0, Ly/d;->m:I

    invoke-direct {p0, v0}, Ly/d;->c(I)V

    :cond_126
    move v1, v3

    .line 902
    goto :goto_c3

    :cond_128
    move v0, v1

    goto :goto_106
.end method

.method private a(LF/m;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 408
    iget v0, p0, Ly/d;->h:F

    invoke-virtual {p1}, LF/m;->r()F

    move-result v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_e

    move v0, v1

    .line 425
    :goto_d
    return v0

    .line 411
    :cond_e
    invoke-virtual {p1}, LF/m;->q()Ly/b;

    move-result-object v0

    invoke-direct {p0, v0}, Ly/d;->a(Ly/b;)Z

    move-result v0

    if-nez v0, :cond_1a

    move v0, v1

    .line 412
    goto :goto_d

    .line 418
    :cond_1a
    invoke-virtual {p1}, LF/m;->v()Lo/n;

    move-result-object v0

    instance-of v0, v0, Lo/U;

    if-eqz v0, :cond_30

    invoke-virtual {p1}, LF/m;->v()Lo/n;

    move-result-object v0

    check-cast v0, Lo/U;

    invoke-direct {p0, v0}, Ly/d;->a(Lo/U;)Z

    move-result v0

    if-eqz v0, :cond_30

    move v0, v2

    .line 420
    goto :goto_d

    .line 422
    :cond_30
    invoke-virtual {p1}, LF/m;->s()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_45

    iget v0, p0, Ly/d;->h:F

    invoke-virtual {p1}, LF/m;->s()F

    move-result v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_45

    move v0, v1

    .line 423
    goto :goto_d

    :cond_45
    move v0, v2

    .line 425
    goto :goto_d
.end method

.method private static a(LF/m;LF/m;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1258
    invoke-virtual {p0}, LF/m;->v()Lo/n;

    move-result-object v0

    invoke-interface {v0}, Lo/n;->a()Lo/o;

    move-result-object v0

    .line 1259
    invoke-virtual {p1}, LF/m;->v()Lo/n;

    move-result-object v1

    invoke-interface {v1}, Lo/n;->a()Lo/o;

    move-result-object v1

    .line 1260
    if-eqz v0, :cond_2c

    if-eqz v1, :cond_2c

    instance-of v2, v0, Lo/p;

    if-eqz v2, :cond_2c

    instance-of v2, v1, Lo/p;

    if-eqz v2, :cond_2c

    sget-object v2, Lo/o;->a:Lo/o;

    invoke-virtual {v2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    const/4 v0, 0x1

    :goto_2b
    return v0

    :cond_2c
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method private a(Lo/T;Ly/b;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1392
    iget-object v0, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {v0, p1}, Lo/aS;->a(Lo/T;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-direct {p0, p1, p2}, Ly/d;->b(Lo/T;Ly/b;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private a(Lo/U;)Z
    .registers 4
    .parameter

    .prologue
    .line 401
    invoke-virtual {p1}, Lo/U;->b()Lo/aq;

    move-result-object v0

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    iget v1, p0, Ly/d;->t:I

    if-ne v0, v1, :cond_12

    iget-boolean v0, p0, Ly/d;->u:Z

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private a(Ly/b;)Z
    .registers 3
    .parameter

    .prologue
    .line 1383
    if-eqz p1, :cond_a

    iget-object v0, p0, Ly/d;->A:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private b(Ly/b;)Lo/j;
    .registers 4
    .parameter

    .prologue
    .line 1428
    iget-object v0, p0, Ly/d;->G:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1429
    const/4 v0, 0x0

    .line 1438
    :cond_9
    :goto_9
    return-object v0

    .line 1431
    :cond_a
    if-nez p1, :cond_e

    .line 1432
    sget-object p1, Ly/d;->a:Ly/b;

    .line 1434
    :cond_e
    iget-object v0, p0, Ly/d;->G:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    .line 1435
    if-nez v0, :cond_9

    .line 1436
    iget-object v0, p0, Ly/d;->G:Ljava/util/Map;

    sget-object v1, Ly/d;->a:Ly/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/j;

    goto :goto_9
.end method

.method private b(Z)V
    .registers 6
    .parameter

    .prologue
    .line 1180
    invoke-direct {p0}, Ly/d;->g()V

    .line 1181
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1182
    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v2, :cond_3a

    .line 1183
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 1184
    if-eqz v0, :cond_30

    .line 1185
    if-eqz p1, :cond_27

    iget-object v3, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {v0, v3}, LF/m;->a(Lo/aS;)Z

    move-result v3

    if-eqz v3, :cond_34

    invoke-direct {p0, v0}, Ly/d;->g(LF/m;)Z

    move-result v3

    if-nez v3, :cond_34

    :cond_27
    invoke-direct {p0, v0}, Ly/d;->d(LF/m;)Z

    move-result v3

    if-nez v3, :cond_34

    .line 1187
    invoke-direct {p0, v0}, Ly/d;->e(LF/m;)V

    .line 1182
    :cond_30
    :goto_30
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1189
    :cond_34
    iget-object v3, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v3}, LF/m;->b(LD/a;)V

    goto :goto_30

    .line 1193
    :cond_3a
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1194
    const/4 v0, -0x1

    iput v0, p0, Ly/d;->o:I

    .line 1195
    return-void
.end method

.method private b(J)Z
    .registers 8
    .parameter

    .prologue
    .line 917
    :try_start_0
    invoke-direct {p0, p1, p2}, Ly/d;->a(J)Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_3} :catch_5

    move-result v0

    .line 928
    :goto_4
    return v0

    .line 918
    :catch_5
    move-exception v0

    .line 919
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-nez v1, :cond_12

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 920
    :cond_12
    throw v0

    .line 922
    :cond_13
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x100

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 923
    const-string v2, "#:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Ly/d;->F:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Ly/d;->F:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " T:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " E:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " numL:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 927
    const-string v0, "Labeler.runLabeler"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private b(LF/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 573
    iget-object v0, p0, Ly/d;->c:LG/a;

    iget-boolean v0, v0, LG/a;->r:Z

    if-eqz v0, :cond_11

    .line 574
    iget-object v0, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {p1}, LF/m;->o()Lo/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/aS;->b(Lo/ae;)Z

    move-result v0

    .line 576
    :goto_10
    return v0

    :cond_11
    iget-object v0, p0, Ly/d;->i:Lo/aS;

    invoke-virtual {p1, v0}, LF/m;->a(Lo/aS;)Z

    move-result v0

    goto :goto_10
.end method

.method private static b(LF/m;LF/m;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1275
    instance-of v0, p0, LF/L;

    if-eqz v0, :cond_1a

    instance-of v0, p1, LF/L;

    if-eqz v0, :cond_1a

    check-cast p1, LF/L;

    invoke-virtual {p1}, LF/L;->y()Z

    move-result v0

    if-eqz v0, :cond_1a

    check-cast p0, LF/L;

    invoke-virtual {p0}, LF/L;->y()Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private b(Lo/T;Ly/b;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1402
    invoke-direct {p0, p2}, Ly/d;->b(Ly/b;)Lo/j;

    move-result-object v0

    .line 1403
    if-eqz v0, :cond_b

    .line 1404
    invoke-interface {v0, p1}, Lo/j;->a(Lo/T;)Z

    move-result v0

    .line 1406
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private c(I)V
    .registers 5
    .parameter

    .prologue
    .line 1304
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 1305
    invoke-direct {p0, v0}, Ly/d;->f(LF/m;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1306
    iget v1, p0, Ly/d;->s:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ly/d;->s:I

    .line 1308
    :cond_14
    iget-object v1, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v1}, LF/m;->b(LD/a;)V

    .line 1309
    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1310
    iget-object v1, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {v0}, LF/m;->u()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1311
    iget v0, p0, Ly/d;->m:I

    if-ne p1, v0, :cond_2f

    .line 1312
    invoke-direct {p0}, Ly/d;->f()V

    .line 1314
    :cond_2f
    return-void
.end method

.method private c(LF/m;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1141
    if-eqz p1, :cond_66

    .line 1142
    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, LF/m;->u()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 1147
    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, LF/m;->u()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1148
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 1153
    if-eqz v0, :cond_61

    invoke-static {v0, p1}, Ly/d;->b(LF/m;LF/m;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 1154
    invoke-direct {p0, v3}, Ly/d;->c(I)V

    .line 1160
    :cond_33
    iget-object v0, p0, Ly/d;->f:LC/a;

    iget-object v3, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v0, v3}, LF/m;->b(LC/a;LD/a;)Z

    .line 1161
    invoke-direct {p0, p1}, Ly/d;->g(LF/m;)Z

    move-result v0

    if-nez v0, :cond_46

    invoke-direct {p0, p1}, Ly/d;->d(LF/m;)Z

    move-result v0

    if-eqz v0, :cond_67

    :cond_46
    move v0, v2

    .line 1162
    :goto_47
    if-eqz v0, :cond_6b

    iget-object v3, p0, Ly/d;->f:LC/a;

    iget-object v4, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v3, v4}, LF/m;->a(LC/a;LD/a;)Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 1163
    invoke-direct {p0, p1}, Ly/d;->g(LF/m;)Z

    move-result v0

    if-nez v0, :cond_5f

    invoke-direct {p0, p1}, Ly/d;->d(LF/m;)Z

    move-result v0

    if-eqz v0, :cond_69

    :cond_5f
    move v0, v2

    goto :goto_47

    .line 1156
    :cond_61
    iget-object v0, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v0}, LF/m;->b(LD/a;)V

    .line 1172
    :cond_66
    :goto_66
    return v1

    :cond_67
    move v0, v1

    .line 1161
    goto :goto_47

    :cond_69
    move v0, v1

    .line 1163
    goto :goto_47

    .line 1165
    :cond_6b
    if-eqz v0, :cond_73

    .line 1166
    iget-object v0, p0, Ly/d;->g:LD/a;

    invoke-virtual {p1, v0}, LF/m;->b(LD/a;)V

    goto :goto_66

    .line 1168
    :cond_73
    invoke-direct {p0, p1}, Ly/d;->e(LF/m;)V

    move v1, v2

    .line 1169
    goto :goto_66
.end method

.method private d(LF/m;)Z
    .registers 13
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1205
    invoke-virtual {p1}, LF/m;->t()I

    move-result v6

    .line 1206
    invoke-virtual {p1}, LF/m;->w()Z

    move-result v0

    if-eqz v0, :cond_41

    move v1, v2

    .line 1207
    :goto_d
    invoke-virtual {p1}, LF/m;->n()Lo/ae;

    move-result-object v7

    .line 1208
    invoke-virtual {v7}, Lo/ae;->a()Lo/ad;

    move-result-object v8

    .line 1209
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v5, v3

    .line 1210
    :goto_1c
    if-ge v5, v9, :cond_7b

    .line 1211
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 1212
    if-eqz v0, :cond_3d

    .line 1220
    invoke-static {p1, v0}, Ly/d;->a(LF/m;LF/m;)Z

    move-result v4

    if-eqz v4, :cond_50

    .line 1221
    invoke-virtual {p1}, LF/m;->r()F

    move-result v4

    invoke-virtual {v0}, LF/m;->r()F

    move-result v10

    cmpl-float v4, v4, v10

    if-lez v4, :cond_43

    .line 1222
    invoke-direct {p0, v5}, Ly/d;->c(I)V

    .line 1210
    :cond_3d
    :goto_3d
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1c

    :cond_41
    move v1, v3

    .line 1206
    goto :goto_d

    .line 1224
    :cond_43
    invoke-virtual {p1}, LF/m;->r()F

    move-result v4

    invoke-virtual {v0}, LF/m;->r()F

    move-result v10

    cmpg-float v4, v4, v10

    if-gez v4, :cond_50

    .line 1251
    :cond_4f
    :goto_4f
    return v2

    .line 1231
    :cond_50
    invoke-virtual {v0}, LF/m;->n()Lo/ae;

    move-result-object v4

    .line 1232
    invoke-virtual {v4}, Lo/ae;->a()Lo/ad;

    move-result-object v10

    .line 1235
    invoke-virtual {v10, v8}, Lo/ad;->a(Lo/ae;)Z

    move-result v10

    if-eqz v10, :cond_3d

    invoke-virtual {v4, v7}, Lo/ae;->a(Lo/ae;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 1237
    invoke-virtual {v0}, LF/m;->w()Z

    move-result v4

    if-eqz v4, :cond_79

    move v4, v2

    .line 1238
    :goto_6b
    if-gt v1, v4, :cond_75

    if-ne v1, v4, :cond_4f

    invoke-virtual {v0}, LF/m;->t()I

    move-result v0

    if-le v6, v0, :cond_4f

    .line 1244
    :cond_75
    invoke-direct {p0, v5}, Ly/d;->c(I)V

    goto :goto_3d

    :cond_79
    move v4, v3

    .line 1237
    goto :goto_6b

    :cond_7b
    move v2, v3

    .line 1251
    goto :goto_4f
.end method

.method private e(LF/m;)V
    .registers 5
    .parameter

    .prologue
    .line 1284
    invoke-direct {p0, p1}, Ly/d;->f(LF/m;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1285
    iget v0, p0, Ly/d;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ly/d;->s:I

    .line 1286
    iget v0, p0, Ly/d;->m:I

    if-ltz v0, :cond_24

    invoke-virtual {p1}, LF/m;->t()I

    move-result v1

    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    iget v2, p0, Ly/d;->m:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    invoke-virtual {v0}, LF/m;->t()I

    move-result v0

    if-ge v1, v0, :cond_2c

    .line 1288
    :cond_24
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Ly/d;->m:I

    .line 1291
    :cond_2c
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1293
    invoke-virtual {p1}, LF/m;->x()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_39
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/i;

    .line 1294
    iget-object v2, p0, Ly/d;->g:LD/a;

    invoke-virtual {v2}, LD/a;->l()Lz/k;

    move-result-object v2

    invoke-virtual {v2, v0}, Lz/k;->a(Lz/i;)V

    goto :goto_39

    .line 1296
    :cond_4f
    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-virtual {p1}, LF/m;->u()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1297
    return-void
.end method

.method private f()V
    .registers 5

    .prologue
    .line 1336
    const v1, 0x7fffffff

    .line 1337
    const/4 v0, -0x1

    iput v0, p0, Ly/d;->m:I

    .line 1338
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_9
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_31

    .line 1339
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 1340
    if-eqz v0, :cond_2d

    invoke-virtual {v0}, LF/m;->t()I

    move-result v3

    if-ge v3, v2, :cond_2d

    invoke-direct {p0, v0}, Ly/d;->f(LF/m;)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 1342
    invoke-virtual {v0}, LF/m;->t()I

    move-result v2

    .line 1343
    iput v1, p0, Ly/d;->m:I

    .line 1338
    :cond_2d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 1346
    :cond_31
    return-void
.end method

.method private f(LF/m;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1321
    invoke-virtual {p1}, LF/m;->m()F

    move-result v1

    iget v2, p0, Ly/d;->r:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_c

    .line 1328
    :cond_b
    :goto_b
    return v0

    .line 1324
    :cond_c
    invoke-virtual {p1}, LF/m;->q()Ly/b;

    move-result-object v1

    .line 1325
    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ly/b;->b()Z

    move-result v1

    if-nez v1, :cond_b

    .line 1328
    :cond_18
    const/4 v0, 0x1

    goto :goto_b
.end method

.method private g()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 1353
    iget-boolean v0, p0, Ly/d;->w:Z

    if-eqz v0, :cond_2f

    .line 1354
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 1355
    :goto_c
    if-ge v1, v3, :cond_21

    .line 1356
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 1357
    if-eqz v0, :cond_1d

    .line 1358
    iget-object v4, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v4}, LF/m;->b(LD/a;)V

    .line 1355
    :cond_1d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c

    .line 1361
    :cond_21
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1362
    const/4 v0, -0x1

    iput v0, p0, Ly/d;->m:I

    .line 1363
    iput-boolean v2, p0, Ly/d;->w:Z

    .line 1364
    iput-boolean v2, p0, Ly/d;->x:Z

    .line 1365
    iput-boolean v2, p0, Ly/d;->y:Z

    .line 1368
    :cond_2f
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    .line 1369
    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    iput-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    .line 1370
    iput-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    .line 1371
    iget v0, p0, Ly/d;->o:I

    .line 1372
    iget v1, p0, Ly/d;->m:I

    iput v1, p0, Ly/d;->o:I

    .line 1373
    iput v0, p0, Ly/d;->m:I

    .line 1374
    iput v2, p0, Ly/d;->s:I

    .line 1375
    iget-object v0, p0, Ly/d;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1376
    return-void
.end method

.method private g(LF/m;)Z
    .registers 4
    .parameter

    .prologue
    .line 1415
    invoke-virtual {p1}, LF/m;->q()Ly/b;

    move-result-object v0

    invoke-direct {p0, v0}, Ly/d;->b(Ly/b;)Lo/j;

    move-result-object v0

    .line 1416
    if-eqz v0, :cond_13

    .line 1417
    invoke-virtual {p1}, LF/m;->n()Lo/ae;

    move-result-object v1

    invoke-interface {v0, v1}, Lo/j;->a(Lo/ae;)Z

    move-result v0

    .line 1419
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method


# virtual methods
.method public a(LF/T;LC/a;)LF/m;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 654
    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-static {p1, v0, p2}, LF/L;->a(LF/T;Lcom/google/android/maps/driveabout/vector/aV;LC/a;)LF/L;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 4

    .prologue
    .line 327
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_25

    .line 328
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 329
    if-eqz v0, :cond_21

    .line 330
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    iget-object v2, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v2}, LF/m;->b(LD/a;)V

    .line 327
    :cond_21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 333
    :cond_25
    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    .line 334
    iget-object v0, p0, Ly/d;->d:LD/c;

    invoke-virtual {v0}, LD/c;->e()V

    .line 335
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 429
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    if-eqz v0, :cond_1c

    .line 430
    iget-object v0, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 431
    if-eqz v0, :cond_a

    .line 432
    invoke-virtual {v0, p1}, LF/m;->a(I)V

    goto :goto_a

    .line 436
    :cond_1c
    return-void
.end method

.method public a(LC/a;Lo/aS;ILjava/util/Iterator;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILA/c;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 479
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    move/from16 v0, p9

    int-to-long v3, v0

    add-long/2addr v3, v1

    .line 481
    iput-object p1, p0, Ly/d;->f:LC/a;

    .line 482
    iput-object p4, p0, Ly/d;->k:Ljava/util/Iterator;

    .line 483
    iput-object p5, p0, Ly/d;->j:Lcom/google/android/maps/driveabout/vector/aF;

    .line 484
    iput-object p2, p0, Ly/d;->i:Lo/aS;

    .line 485
    iput p3, p0, Ly/d;->t:I

    .line 486
    move-object/from16 v0, p7

    iput-object v0, p0, Ly/d;->A:Ljava/util/Set;

    .line 490
    new-instance v1, Lo/h;

    invoke-virtual {p2}, Lo/aS;->c()Lo/ae;

    move-result-object v2

    invoke-direct {v1, v2}, Lo/h;-><init>(Lo/ae;)V

    iput-object v1, p0, Ly/d;->e:Lo/h;

    .line 493
    iget-object v1, p0, Ly/d;->G:Ljava/util/Map;

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Ly/d;->a(Ljava/util/Map;Ljava/util/Map;)V

    .line 495
    invoke-static {p1}, Ly/d;->a(LC/a;)I

    move-result v1

    .line 496
    iget v2, p0, Ly/d;->q:I

    if-eq v1, v2, :cond_39

    .line 497
    iput v1, p0, Ly/d;->q:I

    .line 498
    iget-object v2, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/vector/aV;->a(I)V

    .line 500
    :cond_39
    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->m()F

    move-result v1

    iget-object v2, p0, Ly/d;->f:LC/a;

    invoke-virtual {v2}, LC/a;->m()F

    move-result v2

    mul-float/2addr v1, v2

    .line 501
    const/high16 v2, 0x4348

    mul-float/2addr v1, v2

    iput v1, p0, Ly/d;->r:F

    .line 505
    invoke-direct {p0}, Ly/d;->g()V

    .line 508
    iget-object v1, p0, Ly/d;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 509
    const/4 v1, 0x0

    iput-object v1, p0, Ly/d;->C:Ljava/util/Iterator;

    .line 514
    iget-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 515
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 517
    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->s()F

    move-result v1

    iput v1, p0, Ly/d;->h:F

    .line 519
    iget v1, p0, Ly/d;->t:I

    invoke-static {}, LR/o;->d()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v2

    iget-object v7, p0, Ly/d;->f:LC/a;

    invoke-virtual {v7}, LC/a;->h()Lo/T;

    move-result-object v7

    move-object/from16 v0, p10

    invoke-virtual {v2, v7, v0}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lo/T;LA/c;)Lcom/google/android/maps/driveabout/vector/bE;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/bE;->b()I

    move-result v2

    if-ge v1, v2, :cond_b1

    const/4 v1, 0x1

    :goto_82
    iput-boolean v1, p0, Ly/d;->u:Z

    .line 521
    const/4 v1, 0x0

    move v2, v1

    :goto_86
    if-ge v2, v5, :cond_e2

    .line 522
    iget-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/m;

    .line 523
    if-eqz v1, :cond_ad

    .line 532
    invoke-virtual {v1}, LF/m;->v()Lo/n;

    move-result-object v7

    invoke-interface {v7}, Lo/n;->a()Lo/o;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-interface {v0, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a8

    invoke-direct {p0, v1}, Ly/d;->a(LF/m;)Z

    move-result v7

    if-nez v7, :cond_b3

    .line 534
    :cond_a8
    iget-object v7, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7}, LF/m;->b(LD/a;)V

    .line 521
    :cond_ad
    :goto_ad
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_86

    .line 519
    :cond_b1
    const/4 v1, 0x0

    goto :goto_82

    .line 540
    :cond_b3
    iget-object v7, p0, Ly/d;->f:LC/a;

    iget-object v8, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7, v8}, LF/m;->b(LC/a;LD/a;)Z

    move-result v7

    if-eqz v7, :cond_d9

    invoke-direct {p0, v1}, Ly/d;->b(LF/m;)Z

    move-result v7

    if-eqz v7, :cond_d9

    invoke-direct {p0, v1}, Ly/d;->g(LF/m;)Z

    move-result v7

    if-nez v7, :cond_d9

    .line 543
    invoke-direct {p0, v1}, Ly/d;->d(LF/m;)Z

    move-result v7

    if-nez v7, :cond_d3

    .line 544
    invoke-direct {p0, v1}, Ly/d;->e(LF/m;)V

    goto :goto_ad

    .line 546
    :cond_d3
    iget-object v7, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7}, LF/m;->b(LD/a;)V

    goto :goto_ad

    .line 549
    :cond_d9
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 550
    iget-object v7, p0, Ly/d;->g:LD/a;

    invoke-virtual {v1, v7}, LF/m;->b(LD/a;)V

    goto :goto_ad

    .line 556
    :cond_e2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 557
    const/4 v1, 0x0

    move v2, v1

    :goto_e8
    if-ge v2, v5, :cond_104

    .line 560
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LF/m;

    .line 561
    invoke-virtual {v1}, LF/m;->v()Lo/n;

    move-result-object v7

    invoke-virtual {v1}, LF/m;->q()Ly/b;

    move-result-object v8

    invoke-virtual {v1}, LF/m;->w()Z

    move-result v1

    const/4 v9, 0x0

    invoke-direct {p0, v7, v8, v1, v9}, Ly/d;->a(Lo/n;Ly/b;ZZ)V

    .line 557
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_e8

    .line 565
    :cond_104
    iget-object v1, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 566
    const/4 v1, -0x1

    iput v1, p0, Ly/d;->o:I

    .line 567
    const/4 v1, 0x0

    iput-boolean v1, p0, Ly/d;->x:Z

    .line 568
    const/4 v1, 0x0

    iput-boolean v1, p0, Ly/d;->y:Z

    .line 569
    invoke-direct {p0, v3, v4}, Ly/d;->b(J)Z

    move-result v1

    iput-boolean v1, p0, Ly/d;->v:Z

    .line 570
    return-void
.end method

.method public a(LG/a;)V
    .registers 3
    .parameter

    .prologue
    .line 346
    iget-object v0, p0, Ly/d;->c:LG/a;

    if-eq p1, v0, :cond_9

    .line 347
    iput-object p1, p0, Ly/d;->c:LG/a;

    .line 348
    invoke-virtual {p0}, Ly/d;->b()V

    .line 350
    :cond_9
    return-void
.end method

.method public a(LZ/a;)V
    .registers 2
    .parameter

    .prologue
    .line 1454
    iput-object p1, p0, Ly/d;->E:LZ/a;

    .line 1455
    return-void
.end method

.method public a(Lo/aS;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 372
    invoke-direct {p0}, Ly/d;->g()V

    .line 374
    invoke-virtual {p1}, Lo/aS;->a()Lo/aR;

    move-result-object v3

    .line 375
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    .line 376
    :goto_10
    if-ge v1, v4, :cond_3d

    .line 377
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 378
    if-eqz v0, :cond_33

    .line 379
    iget-object v5, p0, Ly/d;->f:LC/a;

    iget-object v6, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v5, v6}, LF/m;->b(LC/a;LD/a;)Z

    move-result v5

    if-eqz v5, :cond_37

    invoke-virtual {v0}, LF/m;->o()Lo/ad;

    move-result-object v5

    invoke-virtual {v3, v5}, Lo/aR;->b(Lo/ae;)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 380
    invoke-direct {p0, v0}, Ly/d;->e(LF/m;)V

    .line 376
    :cond_33
    :goto_33
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 382
    :cond_37
    iget-object v5, p0, Ly/d;->g:LD/a;

    invoke-virtual {v0, v5}, LF/m;->b(LD/a;)V

    goto :goto_33

    .line 386
    :cond_3d
    iget-object v0, p0, Ly/d;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 387
    const/4 v0, -0x1

    iput v0, p0, Ly/d;->o:I

    .line 391
    iput-boolean v7, p0, Ly/d;->x:Z

    .line 392
    iput-boolean v7, p0, Ly/d;->y:Z

    .line 393
    iput-boolean v2, p0, Ly/d;->v:Z

    .line 394
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 360
    if-eqz p1, :cond_d

    .line 361
    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->a()V

    .line 362
    iget-object v0, p0, Ly/d;->d:LD/c;

    invoke-virtual {v0}, LD/c;->e()V

    .line 366
    :goto_c
    return-void

    .line 364
    :cond_d
    iget-object v0, p0, Ly/d;->b:Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aV;->b()V

    goto :goto_c
.end method

.method public b()V
    .registers 2

    .prologue
    .line 341
    const/4 v0, 0x1

    iput-boolean v0, p0, Ly/d;->w:Z

    .line 342
    return-void
.end method

.method public b(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 624
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    .line 625
    iget-boolean v2, p0, Ly/d;->x:Z

    if-nez v2, :cond_f

    iget-boolean v2, p0, Ly/d;->y:Z

    if-eqz v2, :cond_18

    .line 626
    :cond_f
    iget-boolean v2, p0, Ly/d;->y:Z

    invoke-direct {p0, v2}, Ly/d;->b(Z)V

    .line 627
    iput-boolean v4, p0, Ly/d;->x:Z

    .line 628
    iput-boolean v4, p0, Ly/d;->y:Z

    .line 630
    :cond_18
    invoke-direct {p0, v0, v1}, Ly/d;->b(J)Z

    move-result v0

    iput-boolean v0, p0, Ly/d;->v:Z

    .line 631
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 637
    iget-boolean v0, p0, Ly/d;->v:Z

    return v0
.end method

.method public d()Ly/g;
    .registers 3

    .prologue
    .line 808
    new-instance v0, Ly/g;

    iget-object v1, p0, Ly/d;->l:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ly/g;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method

.method e()F
    .registers 4

    .prologue
    .line 1040
    iget-object v0, p0, Ly/d;->c:LG/a;

    iget v0, v0, LG/a;->c:I

    int-to-float v0, v0

    .line 1041
    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    const/high16 v2, 0x4168

    sub-float/2addr v1, v2

    .line 1042
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1d

    .line 1044
    iget-object v0, p0, Ly/d;->c:LG/a;

    iget v0, v0, LG/a;->b:I

    int-to-float v0, v0

    .line 1052
    iget-boolean v2, p0, Ly/d;->u:Z

    if-nez v2, :cond_1d

    .line 1053
    add-float/2addr v0, v1

    .line 1056
    :cond_1d
    iget-object v1, p0, Ly/d;->f:LC/a;

    invoke-virtual {v1}, LC/a;->m()F

    move-result v1

    mul-float/2addr v0, v1

    .line 1057
    return v0
.end method
