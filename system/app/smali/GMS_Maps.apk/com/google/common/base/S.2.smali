.class public final Lcom/google/common/base/S;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/common/base/e;

.field private final b:Z

.field private final c:Lcom/google/common/base/X;

.field private final d:I


# direct methods
.method private constructor <init>(Lcom/google/common/base/X;)V
    .registers 5
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x0

    sget-object v1, Lcom/google/common/base/e;->n:Lcom/google/common/base/e;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/common/base/S;-><init>(Lcom/google/common/base/X;ZLcom/google/common/base/e;I)V

    .line 106
    return-void
.end method

.method private constructor <init>(Lcom/google/common/base/X;ZLcom/google/common/base/e;I)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/google/common/base/S;->c:Lcom/google/common/base/X;

    .line 111
    iput-boolean p2, p0, Lcom/google/common/base/S;->b:Z

    .line 112
    iput-object p3, p0, Lcom/google/common/base/S;->a:Lcom/google/common/base/e;

    .line 113
    iput p4, p0, Lcom/google/common/base/S;->d:I

    .line 114
    return-void
.end method

.method public static a(C)Lcom/google/common/base/S;
    .registers 2
    .parameter

    .prologue
    .line 125
    invoke-static {p0}, Lcom/google/common/base/e;->a(C)Lcom/google/common/base/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/S;->a(Lcom/google/common/base/e;)Lcom/google/common/base/S;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/common/base/e;)Lcom/google/common/base/S;
    .registers 3
    .parameter

    .prologue
    .line 139
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    new-instance v0, Lcom/google/common/base/S;

    new-instance v1, Lcom/google/common/base/T;

    invoke-direct {v1, p0}, Lcom/google/common/base/T;-><init>(Lcom/google/common/base/e;)V

    invoke-direct {v0, v1}, Lcom/google/common/base/S;-><init>(Lcom/google/common/base/X;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/base/S;)Lcom/google/common/base/e;
    .registers 2
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/common/base/S;->a:Lcom/google/common/base/e;

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/base/S;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/google/common/base/S;->b(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .registers 3
    .parameter

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/common/base/S;->c:Lcom/google/common/base/X;

    invoke-interface {v0, p0, p1}, Lcom/google/common/base/X;->b(Lcom/google/common/base/S;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/common/base/S;)Z
    .registers 2
    .parameter

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/common/base/S;->b:Z

    return v0
.end method

.method static synthetic c(Lcom/google/common/base/S;)I
    .registers 2
    .parameter

    .prologue
    .line 98
    iget v0, p0, Lcom/google/common/base/S;->d:I

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .registers 3
    .parameter

    .prologue
    .line 371
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    new-instance v0, Lcom/google/common/base/V;

    invoke-direct {v0, p0, p1}, Lcom/google/common/base/V;-><init>(Lcom/google/common/base/S;Ljava/lang/CharSequence;)V

    return-object v0
.end method
