.class public abstract Lcom/google/common/base/t;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 102
    if-nez p1, :cond_4

    .line 103
    const/4 v0, 0x0

    .line 105
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/common/base/t;->b(Ljava/lang/Object;)I

    move-result v0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 66
    if-ne p1, p2, :cond_4

    .line 67
    const/4 v0, 0x1

    .line 72
    :goto_3
    return v0

    .line 69
    :cond_4
    if-eqz p1, :cond_8

    if-nez p2, :cond_a

    .line 70
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 72
    :cond_a
    invoke-virtual {p0, p1, p2}, Lcom/google/common/base/t;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method protected abstract b(Ljava/lang/Object;)I
.end method

.method protected abstract b(Ljava/lang/Object;Ljava/lang/Object;)Z
.end method
