.class public abstract Lcom/google/common/collect/dh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Comparator;)Lcom/google/common/collect/dh;
    .registers 2
    .parameter

    .prologue
    .line 95
    instance-of v0, p0, Lcom/google/common/collect/dh;

    if-eqz v0, :cond_7

    check-cast p0, Lcom/google/common/collect/dh;

    :goto_6
    return-object p0

    :cond_7
    new-instance v0, Lcom/google/common/collect/X;

    invoke-direct {v0, p0}, Lcom/google/common/collect/X;-><init>(Ljava/util/Comparator;)V

    move-object p0, v0

    goto :goto_6
.end method

.method public static b()Lcom/google/common/collect/dh;
    .registers 1

    .prologue
    .line 82
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/common/collect/dh;
    .registers 2

    .prologue
    .line 321
    new-instance v0, Lcom/google/common/collect/dy;

    invoke-direct {v0, p0}, Lcom/google/common/collect/dy;-><init>(Lcom/google/common/collect/dh;)V

    return-object v0
.end method

.method public a(Lcom/google/common/base/x;)Lcom/google/common/collect/dh;
    .registers 3
    .parameter

    .prologue
    .line 335
    new-instance v0, Lcom/google/common/collect/R;

    invoke-direct {v0, p1, p0}, Lcom/google/common/collect/R;-><init>(Lcom/google/common/base/x;Lcom/google/common/collect/dh;)V

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 614
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/common/collect/dh;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 653
    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/dh;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_7

    :goto_6
    return-object p1

    :cond_7
    move-object p1, p2

    goto :goto_6
.end method

.method public a(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 595
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 597
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 598
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/common/collect/dh;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_4

    .line 601
    :cond_13
    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 691
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/common/collect/dh;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 730
    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/dh;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_7

    :goto_6
    return-object p1

    :cond_7
    move-object p1, p2

    goto :goto_6
.end method

.method public b(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 672
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 674
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 675
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/common/collect/dh;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_4

    .line 678
    :cond_13
    return-object v0
.end method

.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
.end method
