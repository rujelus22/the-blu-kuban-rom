.class final Lcom/google/common/collect/ch;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/common/collect/bP;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bP;)V
    .registers 2
    .parameter

    .prologue
    .line 3885
    iput-object p1, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    .prologue
    .line 3929
    iget-object v0, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Lcom/google/common/collect/bP;->clear()V

    .line 3930
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 3894
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 3904
    :cond_5
    :goto_5
    return v0

    .line 3897
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 3898
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3899
    if-eqz v1, :cond_5

    .line 3902
    iget-object v2, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v2, v1}, Lcom/google/common/collect/bP;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 3904
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    iget-object v2, v2, Lcom/google/common/collect/bP;->f:Lcom/google/common/base/t;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 3924
    iget-object v0, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Lcom/google/common/collect/bP;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 3889
    new-instance v0, Lcom/google/common/collect/cg;

    iget-object v1, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-direct {v0, v1}, Lcom/google/common/collect/cg;-><init>(Lcom/google/common/collect/bP;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 3909
    instance-of v1, p1, Ljava/util/Map$Entry;

    if-nez v1, :cond_6

    .line 3914
    :cond_5
    :goto_5
    return v0

    .line 3912
    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    .line 3913
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 3914
    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/google/common/collect/bP;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public size()I
    .registers 2

    .prologue
    .line 3919
    iget-object v0, p0, Lcom/google/common/collect/ch;->a:Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Lcom/google/common/collect/bP;->size()I

    move-result v0

    return v0
.end method
