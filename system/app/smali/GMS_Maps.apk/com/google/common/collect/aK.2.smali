.class public Lcom/google/common/collect/aK;
.super Lcom/google/common/collect/aQ;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# static fields
.field private static final b:Ljava/util/Comparator;

.field private static final c:Lcom/google/common/collect/aK;

.field private static final serialVersionUID:J


# instance fields
.field final transient a:Lcom/google/common/collect/ImmutableList;

.field private final transient d:Ljava/util/Comparator;

.field private transient e:Lcom/google/common/collect/ImmutableSet;

.field private transient f:Lcom/google/common/collect/aR;

.field private transient g:Lcom/google/common/collect/ar;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 71
    invoke-static {}, Lcom/google/common/collect/dh;->b()Lcom/google/common/collect/dh;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/aK;->b:Ljava/util/Comparator;

    .line 75
    new-instance v0, Lcom/google/common/collect/aK;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    sget-object v2, Lcom/google/common/collect/aK;->b:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/aK;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    sput-object v0, Lcom/google/common/collect/aK;->c:Lcom/google/common/collect/aK;

    return-void
.end method

.method constructor <init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 417
    invoke-direct {p0}, Lcom/google/common/collect/aQ;-><init>()V

    .line 418
    iput-object p1, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    .line 419
    iput-object p2, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    .line 420
    return-void
.end method

.method private a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 726
    invoke-direct {p0}, Lcom/google/common/collect/aK;->l()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->e()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p3}, Lcom/google/common/collect/dJ;->a(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    return v0
.end method

.method private a(II)Lcom/google/common/collect/aK;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 732
    if-ge p1, p2, :cond_10

    .line 733
    new-instance v0, Lcom/google/common/collect/aK;

    iget-object v1, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1, p1, p2}, Lcom/google/common/collect/ImmutableList;->a(II)Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/aK;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    .line 736
    :goto_f
    return-object v0

    :cond_10
    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/google/common/collect/aK;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aK;

    move-result-object v0

    goto :goto_f
.end method

.method private static a(Ljava/util/Comparator;)Lcom/google/common/collect/aK;
    .registers 3
    .parameter

    .prologue
    .line 92
    sget-object v0, Lcom/google/common/collect/aK;->b:Ljava/util/Comparator;

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 93
    sget-object v0, Lcom/google/common/collect/aK;->c:Lcom/google/common/collect/aK;

    .line 95
    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/google/common/collect/aK;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/common/collect/aK;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    goto :goto_a
.end method

.method private j()Lcom/google/common/collect/ImmutableSet;
    .registers 2

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    new-instance v0, Lcom/google/common/collect/aO;

    invoke-direct {v0, p0}, Lcom/google/common/collect/aO;-><init>(Lcom/google/common/collect/aK;)V

    goto :goto_a
.end method

.method private k()Lcom/google/common/collect/aR;
    .registers 4

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 534
    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-static {v0}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;

    move-result-object v0

    .line 537
    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lcom/google/common/collect/dw;

    new-instance v1, Lcom/google/common/collect/aL;

    iget-object v2, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-direct {v1, p0, v2}, Lcom/google/common/collect/aL;-><init>(Lcom/google/common/collect/aK;Lcom/google/common/collect/ImmutableList;)V

    iget-object v2, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/dw;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/util/Comparator;)V

    goto :goto_c
.end method

.method private l()Lcom/google/common/collect/ImmutableList;
    .registers 3

    .prologue
    .line 716
    new-instance v0, Lcom/google/common/collect/aN;

    iget-object v1, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/aN;-><init>(Lcom/google/common/collect/aK;Lcom/google/common/collect/ImmutableList;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/common/collect/ImmutableSet;
    .registers 2

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/common/collect/aK;->e:Lcom/google/common/collect/ImmutableSet;

    .line 467
    if-nez v0, :cond_a

    invoke-direct {p0}, Lcom/google/common/collect/aK;->j()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/aK;->e:Lcom/google/common/collect/ImmutableSet;

    :cond_a
    return-object v0
.end method

.method public a(Ljava/lang/Object;)Lcom/google/common/collect/aK;
    .registers 3
    .parameter

    .prologue
    .line 651
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 656
    if-eqz p2, :cond_12

    .line 657
    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->a:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 661
    :goto_c
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/common/collect/aK;->a(II)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0

    .line 659
    :cond_12
    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->b:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    goto :goto_c
.end method

.method a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aK;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 684
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 685
    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 686
    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    invoke-interface {v0, p1, p3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_1b

    const/4 v0, 0x1

    :goto_f
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    .line 687
    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0

    .line 686
    :cond_1b
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public b(Ljava/lang/Object;)Lcom/google/common/collect/aK;
    .registers 3
    .parameter

    .prologue
    .line 702
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aK;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 679
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;ZLjava/lang/Object;Z)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method b(Ljava/lang/Object;Z)Lcom/google/common/collect/aK;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 707
    if-eqz p2, :cond_13

    .line 708
    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->b:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    .line 712
    :goto_a
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/common/collect/aK;->a(II)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0

    .line 710
    :cond_13
    sget-object v0, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v1, Lcom/google/common/collect/dL;->a:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_a
.end method

.method public c()Lcom/google/common/collect/ar;
    .registers 2

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/common/collect/aK;->g:Lcom/google/common/collect/ar;

    .line 554
    if-nez v0, :cond_b

    new-instance v0, Lcom/google/common/collect/aP;

    invoke-direct {v0, p0}, Lcom/google/common/collect/aP;-><init>(Lcom/google/common/collect/aK;)V

    iput-object v0, p0, Lcom/google/common/collect/aK;->g:Lcom/google/common/collect/ar;

    :cond_b
    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    return-object v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 449
    if-nez p1, :cond_4

    .line 450
    const/4 v0, 0x0

    .line 452
    :goto_3
    return v0

    :cond_4
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->g()Lcom/google/common/collect/dY;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/collect/aZ;->a(Ljava/util/Iterator;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method d()Z
    .registers 2

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->a()Z

    move-result v0

    return v0
.end method

.method e()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/common/collect/aK;->d:Ljava/util/Comparator;

    return-object v0
.end method

.method public synthetic entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/google/common/collect/aR;
    .registers 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/common/collect/aK;->f:Lcom/google/common/collect/aR;

    .line 528
    if-nez v0, :cond_a

    invoke-direct {p0}, Lcom/google/common/collect/aK;->k()Lcom/google/common/collect/aR;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/aK;->f:Lcom/google/common/collect/aR;

    :cond_a
    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 625
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 626
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 628
    :cond_c
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method g()Lcom/google/common/collect/dY;
    .registers 3

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    .line 559
    new-instance v1, Lcom/google/common/collect/aM;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/aM;-><init>(Lcom/google/common/collect/aK;Lcom/google/common/collect/dY;)V

    return-object v1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 436
    if-nez p1, :cond_4

    .line 445
    :cond_3
    :goto_3
    return-object v0

    .line 441
    :cond_4
    :try_start_4
    sget-object v1, Lcom/google/common/collect/dP;->a:Lcom/google/common/collect/dP;

    sget-object v2, Lcom/google/common/collect/dL;->c:Lcom/google/common/collect/dL;

    invoke-direct {p0, p1, v1, v2}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;Lcom/google/common/collect/dP;Lcom/google/common/collect/dL;)I
    :try_end_b
    .catch Ljava/lang/ClassCastException; {:try_start_4 .. :try_end_b} :catch_1b

    move-result v1

    .line 445
    if-ltz v1, :cond_3

    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto :goto_3

    .line 442
    :catch_1b
    move-exception v1

    goto :goto_3
.end method

.method public synthetic headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3
    .parameter

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aK;->a(Ljava/lang/Object;)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->f()Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 3

    .prologue
    .line 633
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 634
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 636
    :cond_c
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {p0}, Lcom/google/common/collect/aK;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/common/collect/aK;->a:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public synthetic tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 3
    .parameter

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aK;->b(Ljava/lang/Object;)Lcom/google/common/collect/aK;

    move-result-object v0

    return-object v0
.end method

.method public synthetic values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->c()Lcom/google/common/collect/ar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic x_()Lcom/google/common/collect/ImmutableSet;
    .registers 2

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/common/collect/aK;->f()Lcom/google/common/collect/aR;

    move-result-object v0

    return-object v0
.end method
