.class Lcom/google/common/collect/bl;
.super Ljava/util/AbstractSet;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/common/collect/bj;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bj;)V
    .registers 2
    .parameter

    .prologue
    .line 680
    iput-object p1, p0, Lcom/google/common/collect/bl;->a:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/common/collect/bl;->a:Lcom/google/common/collect/bj;

    invoke-static {v0}, Lcom/google/common/collect/bj;->d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/common/collect/cW;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 4

    .prologue
    .line 685
    new-instance v0, Lcom/google/common/collect/bs;

    iget-object v1, p0, Lcom/google/common/collect/bl;->a:Lcom/google/common/collect/bj;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bs;-><init>(Lcom/google/common/collect/bj;Lcom/google/common/collect/bk;)V

    return-object v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 3
    .parameter

    .prologue
    .line 691
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    invoke-super {p0, p1}, Ljava/util/AbstractSet;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/common/collect/bl;->a:Lcom/google/common/collect/bj;

    invoke-static {v0}, Lcom/google/common/collect/bj;->d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method
