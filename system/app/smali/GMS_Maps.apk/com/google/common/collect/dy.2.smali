.class final Lcom/google/common/collect/dy;
.super Lcom/google/common/collect/dh;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final a:Lcom/google/common/collect/dh;


# direct methods
.method constructor <init>(Lcom/google/common/collect/dh;)V
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/common/collect/dh;-><init>()V

    .line 34
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/dh;

    iput-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    .line 35
    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/dh;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    return-object v0
.end method

.method public a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/dh;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p1, p2}, Lcom/google/common/collect/dh;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/dh;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/dh;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p1, p2}, Lcom/google/common/collect/dh;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/dh;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, p2, p1}, Lcom/google/common/collect/dh;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 85
    if-ne p1, p0, :cond_4

    .line 86
    const/4 v0, 0x1

    .line 92
    :goto_3
    return v0

    .line 88
    :cond_4
    instance-of v0, p1, Lcom/google/common/collect/dy;

    if-eqz v0, :cond_13

    .line 89
    check-cast p1, Lcom/google/common/collect/dy;

    .line 90
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    iget-object v1, p1, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 92
    :cond_13
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/common/collect/dy;->a:Lcom/google/common/collect/dh;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".reverse()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
