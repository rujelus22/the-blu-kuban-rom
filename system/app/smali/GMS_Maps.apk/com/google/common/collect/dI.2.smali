.class final Lcom/google/common/collect/dI;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/util/Comparator;[Ljava/lang/Object;)Ljava/util/Collection;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 102
    array-length v1, p1

    if-nez v1, :cond_9

    .line 103
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 115
    :goto_8
    return-object v0

    :cond_9
    move v1, v0

    .line 106
    :goto_a
    array-length v2, p1

    if-ge v0, v2, :cond_23

    .line 107
    aget-object v2, p1, v0

    add-int/lit8 v3, v1, -0x1

    aget-object v3, p1, v3

    invoke-interface {p0, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    .line 108
    if-eqz v2, :cond_20

    .line 109
    add-int/lit8 v2, v1, 0x1

    aget-object v3, p1, v0

    aput-object v3, p1, v1

    move v1, v2

    .line 106
    :cond_20
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 112
    :cond_23
    array-length v0, p1

    if-ge v1, v0, :cond_2a

    .line 113
    invoke-static {p1, v1}, Lcom/google/common/collect/dg;->b([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    .line 115
    :cond_2a
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_8
.end method

.method public static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    instance-of v0, p1, Ljava/util/SortedSet;

    if-eqz v0, :cond_1b

    .line 50
    check-cast p1, Ljava/util/SortedSet;

    .line 51
    invoke-interface {p1}, Ljava/util/SortedSet;->comparator()Ljava/util/Comparator;

    move-result-object v0

    .line 52
    if-nez v0, :cond_16

    .line 53
    invoke-static {}, Lcom/google/common/collect/dh;->b()Lcom/google/common/collect/dh;

    move-result-object v0

    .line 60
    :cond_16
    :goto_16
    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 55
    :cond_1b
    instance-of v0, p1, Lcom/google/common/collect/dH;

    if-eqz v0, :cond_26

    .line 56
    check-cast p1, Lcom/google/common/collect/dH;

    invoke-interface {p1}, Lcom/google/common/collect/dH;->comparator()Ljava/util/Comparator;

    move-result-object v0

    goto :goto_16

    .line 58
    :cond_26
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public static b(Ljava/util/Comparator;Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 82
    instance-of v0, p1, Lcom/google/common/collect/cW;

    if-eqz v0, :cond_33

    .line 83
    check-cast p1, Lcom/google/common/collect/cW;

    invoke-interface {p1}, Lcom/google/common/collect/cW;->e()Ljava/util/Set;

    move-result-object v0

    .line 85
    :goto_a
    instance-of v1, v0, Ljava/util/Set;

    if-eqz v1, :cond_1f

    .line 86
    invoke-static {p0, v0}, Lcom/google/common/collect/dI;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 87
    check-cast v0, Ljava/util/Set;

    .line 97
    :goto_16
    return-object v0

    .line 89
    :cond_17
    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 90
    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_16

    .line 93
    :cond_1f
    invoke-static {v0}, Lcom/google/common/collect/aT;->d(Ljava/lang/Iterable;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    .line 94
    invoke-static {p0, v0}, Lcom/google/common/collect/dI;->a(Ljava/util/Comparator;Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 95
    invoke-static {v1, p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 97
    :cond_2e
    invoke-static {p0, v1}, Lcom/google/common/collect/dI;->a(Ljava/util/Comparator;[Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    goto :goto_16

    :cond_33
    move-object v0, p1

    goto :goto_a
.end method
