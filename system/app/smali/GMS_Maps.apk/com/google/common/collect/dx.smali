.class final Lcom/google/common/collect/dx;
.super Lcom/google/common/collect/dh;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final a:Lcom/google/common/collect/dx;

.field private static final serialVersionUID:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/common/collect/dx;

    invoke-direct {v0}, Lcom/google/common/collect/dx;-><init>()V

    sput-object v0, Lcom/google/common/collect/dx;->a:Lcom/google/common/collect/dx;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/common/collect/dh;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Comparable;Ljava/lang/Comparable;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    if-ne p1, p2, :cond_7

    .line 36
    const/4 v0, 0x0

    .line 39
    :goto_6
    return v0

    :cond_7
    invoke-interface {p2, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_6
.end method

.method public a()Lcom/google/common/collect/dh;
    .registers 2

    .prologue
    .line 43
    invoke-static {}, Lcom/google/common/collect/dh;->b()Lcom/google/common/collect/dh;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/google/common/collect/dx;->d(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/dx;->c(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/google/common/collect/dx;->d(Ljava/util/Iterator;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 49
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    invoke-virtual {v0, p1, p2}, Lcom/google/common/collect/df;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/google/common/collect/dx;->c(Ljava/lang/Iterable;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/dx;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/util/Iterator;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/google/common/collect/dx;->c(Ljava/util/Iterator;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 65
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    invoke-virtual {v0, p1, p2}, Lcom/google/common/collect/df;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public c(Ljava/lang/Iterable;)Ljava/lang/Comparable;
    .registers 3
    .parameter

    .prologue
    .line 61
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/df;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public c(Ljava/util/Iterator;)Ljava/lang/Comparable;
    .registers 3
    .parameter

    .prologue
    .line 57
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/df;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 27
    check-cast p1, Ljava/lang/Comparable;

    check-cast p2, Ljava/lang/Comparable;

    invoke-virtual {p0, p1, p2}, Lcom/google/common/collect/dx;->a(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/Iterable;)Ljava/lang/Comparable;
    .registers 3
    .parameter

    .prologue
    .line 77
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/df;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public d(Ljava/util/Iterator;)Ljava/lang/Comparable;
    .registers 3
    .parameter

    .prologue
    .line 73
    sget-object v0, Lcom/google/common/collect/df;->a:Lcom/google/common/collect/df;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/df;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 86
    const-string v0, "Ordering.natural().reverse()"

    return-object v0
.end method
