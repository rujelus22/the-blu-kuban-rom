.class Lcom/google/common/collect/F;
.super Lcom/google/common/collect/B;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedSet;


# instance fields
.field final synthetic c:Lcom/google/common/collect/x;


# direct methods
.method constructor <init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 951
    iput-object p1, p0, Lcom/google/common/collect/F;->c:Lcom/google/common/collect/x;

    .line 952
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/B;-><init>(Lcom/google/common/collect/x;Ljava/util/Map;)V

    .line 953
    return-void
.end method


# virtual methods
.method b()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/common/collect/F;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 961
    invoke-virtual {p0}, Lcom/google/common/collect/F;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public first()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 966
    invoke-virtual {p0}, Lcom/google/common/collect/F;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5
    .parameter

    .prologue
    .line 971
    new-instance v0, Lcom/google/common/collect/F;

    iget-object v1, p0, Lcom/google/common/collect/F;->c:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/F;->b()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/F;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public last()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 976
    invoke-virtual {p0}, Lcom/google/common/collect/F;->b()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 981
    new-instance v0, Lcom/google/common/collect/F;

    iget-object v1, p0, Lcom/google/common/collect/F;->c:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/F;->b()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/F;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
    .registers 5
    .parameter

    .prologue
    .line 986
    new-instance v0, Lcom/google/common/collect/F;

    iget-object v1, p0, Lcom/google/common/collect/F;->c:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/F;->b()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/F;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    return-object v0
.end method
