.class Lcom/google/common/collect/U;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Collection;


# instance fields
.field final a:Ljava/util/Collection;

.field final b:Lcom/google/common/base/K;


# direct methods
.method constructor <init>(Ljava/util/Collection;Lcom/google/common/base/K;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    .line 109
    iput-object p2, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    .line 110
    return-void
.end method


# virtual methods
.method a(Lcom/google/common/base/K;)Lcom/google/common/collect/U;
    .registers 5
    .parameter

    .prologue
    .line 113
    new-instance v0, Lcom/google/common/collect/U;

    iget-object v1, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    iget-object v2, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-static {v2, p1}, Lcom/google/common/base/Predicates;->a(Lcom/google/common/base/K;Lcom/google/common/base/K;)Lcom/google/common/base/K;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/U;-><init>(Ljava/util/Collection;Lcom/google/common/base/K;)V

    return-object v0
.end method

.method public add(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-interface {v0, p1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    .line 121
    iget-object v0, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .registers 5
    .parameter

    .prologue
    .line 126
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 127
    iget-object v2, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-interface {v2, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/common/base/J;->a(Z)V

    goto :goto_4

    .line 129
    :cond_18
    iget-object v0, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    move-result v0

    return v0
.end method

.method public clear()V
    .registers 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    iget-object v1, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-static {v0, v1}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/K;)Z

    .line 135
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 143
    .line 150
    :try_start_1
    iget-object v1, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-interface {v1, p1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z
    :try_end_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_e} :catch_15
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_e} :catch_13

    move-result v1

    if-eqz v1, :cond_12

    const/4 v0, 0x1

    .line 154
    :cond_12
    :goto_12
    return v0

    .line 153
    :catch_13
    move-exception v1

    goto :goto_12

    .line 151
    :catch_15
    move-exception v1

    goto :goto_12
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .registers 4
    .parameter

    .prologue
    .line 160
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 161
    invoke-virtual {p0, v1}, Lcom/google/common/collect/U;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 162
    const/4 v0, 0x0

    .line 165
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x1

    goto :goto_15
.end method

.method public isEmpty()Z
    .registers 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-static {v0, v1}, Lcom/google/common/collect/aZ;->c(Ljava/util/Iterator;Lcom/google/common/base/K;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-static {v0, v1}, Lcom/google/common/collect/aZ;->b(Ljava/util/Iterator;Lcom/google/common/base/K;)Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 184
    .line 187
    :try_start_1
    iget-object v1, p0, Lcom/google/common/collect/U;->b:Lcom/google/common/base/K;

    invoke-interface {v1, p1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-interface {v1, p1}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z
    :try_end_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_e} :catch_15
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_e} :catch_13

    move-result v1

    if-eqz v1, :cond_12

    const/4 v0, 0x1

    .line 191
    :cond_12
    :goto_12
    return v0

    .line 190
    :catch_13
    move-exception v1

    goto :goto_12

    .line 188
    :catch_15
    move-exception v1

    goto :goto_12
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .registers 4
    .parameter

    .prologue
    .line 197
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    new-instance v0, Lcom/google/common/collect/V;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/V;-><init>(Lcom/google/common/collect/U;Ljava/util/Collection;)V

    .line 204
    iget-object v1, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-static {v1, v0}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/K;)Z

    move-result v0

    return v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .registers 4
    .parameter

    .prologue
    .line 209
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    new-instance v0, Lcom/google/common/collect/W;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/W;-><init>(Lcom/google/common/collect/U;Ljava/util/Collection;)V

    .line 217
    iget-object v1, p0, Lcom/google/common/collect/U;->a:Ljava/util/Collection;

    invoke-static {v1, v0}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/K;)Z

    move-result v0

    return v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 222
    invoke-virtual {p0}, Lcom/google/common/collect/U;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/aZ;->a(Ljava/util/Iterator;)I

    move-result v0

    return v0
.end method

.method public toArray()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/common/collect/U;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/google/common/collect/U;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/common/collect/U;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/aZ;->b(Ljava/util/Iterator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
