.class Lcom/google/common/collect/cD;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cs;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:I

.field final c:Lcom/google/common/collect/cs;

.field volatile d:Lcom/google/common/collect/cJ;


# direct methods
.method constructor <init>(Ljava/lang/Object;ILcom/google/common/collect/cs;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038
    invoke-static {}, Lcom/google/common/collect/bP;->g()Lcom/google/common/collect/cJ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    .line 970
    iput-object p1, p0, Lcom/google/common/collect/cD;->a:Ljava/lang/Object;

    .line 971
    iput p2, p0, Lcom/google/common/collect/cD;->b:I

    .line 972
    iput-object p3, p0, Lcom/google/common/collect/cD;->c:Lcom/google/common/collect/cs;

    .line 973
    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/cJ;
    .registers 2

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 989
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/common/collect/cJ;)V
    .registers 3
    .parameter

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    .line 1048
    iput-object p1, p0, Lcom/google/common/collect/cD;->d:Lcom/google/common/collect/cJ;

    .line 1049
    invoke-interface {v0, p1}, Lcom/google/common/collect/cJ;->a(Lcom/google/common/collect/cJ;)V

    .line 1050
    return-void
.end method

.method public a(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 999
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/google/common/collect/cD;->c:Lcom/google/common/collect/cs;

    return-object v0
.end method

.method public b(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1009
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1054
    iget v0, p0, Lcom/google/common/collect/cD;->b:I

    return v0
.end method

.method public c(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1021
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 977
    iget-object v0, p0, Lcom/google/common/collect/cD;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public d(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1031
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 984
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 994
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1004
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1016
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
