.class abstract enum Lcom/google/common/collect/bT;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/common/collect/bT;

.field public static final enum b:Lcom/google/common/collect/bT;

.field public static final enum c:Lcom/google/common/collect/bT;

.field public static final enum d:Lcom/google/common/collect/bT;

.field public static final enum e:Lcom/google/common/collect/bT;

.field public static final enum f:Lcom/google/common/collect/bT;

.field public static final enum g:Lcom/google/common/collect/bT;

.field public static final enum h:Lcom/google/common/collect/bT;

.field public static final enum i:Lcom/google/common/collect/bT;

.field public static final enum j:Lcom/google/common/collect/bT;

.field public static final enum k:Lcom/google/common/collect/bT;

.field public static final enum l:Lcom/google/common/collect/bT;

.field static final m:[[Lcom/google/common/collect/bT;

.field private static final synthetic n:[Lcom/google/common/collect/bT;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 353
    new-instance v0, Lcom/google/common/collect/bU;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v3}, Lcom/google/common/collect/bU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->a:Lcom/google/common/collect/bT;

    .line 360
    new-instance v0, Lcom/google/common/collect/bY;

    const-string v1, "STRONG_EXPIRABLE"

    invoke-direct {v0, v1, v4}, Lcom/google/common/collect/bY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->b:Lcom/google/common/collect/bT;

    .line 375
    new-instance v0, Lcom/google/common/collect/bZ;

    const-string v1, "STRONG_EVICTABLE"

    invoke-direct {v0, v1, v5}, Lcom/google/common/collect/bZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->c:Lcom/google/common/collect/bT;

    .line 390
    new-instance v0, Lcom/google/common/collect/ca;

    const-string v1, "STRONG_EXPIRABLE_EVICTABLE"

    invoke-direct {v0, v1, v6}, Lcom/google/common/collect/ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->d:Lcom/google/common/collect/bT;

    .line 407
    new-instance v0, Lcom/google/common/collect/cb;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v7}, Lcom/google/common/collect/cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->e:Lcom/google/common/collect/bT;

    .line 414
    new-instance v0, Lcom/google/common/collect/cc;

    const-string v1, "SOFT_EXPIRABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->f:Lcom/google/common/collect/bT;

    .line 429
    new-instance v0, Lcom/google/common/collect/cd;

    const-string v1, "SOFT_EVICTABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cd;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->g:Lcom/google/common/collect/bT;

    .line 444
    new-instance v0, Lcom/google/common/collect/ce;

    const-string v1, "SOFT_EXPIRABLE_EVICTABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/ce;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->h:Lcom/google/common/collect/bT;

    .line 461
    new-instance v0, Lcom/google/common/collect/cf;

    const-string v1, "WEAK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/cf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->i:Lcom/google/common/collect/bT;

    .line 468
    new-instance v0, Lcom/google/common/collect/bV;

    const-string v1, "WEAK_EXPIRABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bV;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->j:Lcom/google/common/collect/bT;

    .line 483
    new-instance v0, Lcom/google/common/collect/bW;

    const-string v1, "WEAK_EVICTABLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bW;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->k:Lcom/google/common/collect/bT;

    .line 498
    new-instance v0, Lcom/google/common/collect/bX;

    const-string v1, "WEAK_EXPIRABLE_EVICTABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/bX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/bT;->l:Lcom/google/common/collect/bT;

    .line 352
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/common/collect/bT;

    sget-object v1, Lcom/google/common/collect/bT;->a:Lcom/google/common/collect/bT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/collect/bT;->b:Lcom/google/common/collect/bT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/common/collect/bT;->c:Lcom/google/common/collect/bT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/common/collect/bT;->d:Lcom/google/common/collect/bT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/common/collect/bT;->e:Lcom/google/common/collect/bT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/common/collect/bT;->f:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/common/collect/bT;->g:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/common/collect/bT;->h:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/common/collect/bT;->i:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/common/collect/bT;->j:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/common/collect/bT;->k:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/common/collect/bT;->l:Lcom/google/common/collect/bT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/common/collect/bT;->n:[Lcom/google/common/collect/bT;

    .line 525
    new-array v0, v6, [[Lcom/google/common/collect/bT;

    new-array v1, v7, [Lcom/google/common/collect/bT;

    sget-object v2, Lcom/google/common/collect/bT;->a:Lcom/google/common/collect/bT;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/common/collect/bT;->b:Lcom/google/common/collect/bT;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/common/collect/bT;->c:Lcom/google/common/collect/bT;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/common/collect/bT;->d:Lcom/google/common/collect/bT;

    aput-object v2, v1, v6

    aput-object v1, v0, v3

    new-array v1, v7, [Lcom/google/common/collect/bT;

    sget-object v2, Lcom/google/common/collect/bT;->e:Lcom/google/common/collect/bT;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/common/collect/bT;->f:Lcom/google/common/collect/bT;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/common/collect/bT;->g:Lcom/google/common/collect/bT;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/common/collect/bT;->h:Lcom/google/common/collect/bT;

    aput-object v2, v1, v6

    aput-object v1, v0, v4

    new-array v1, v7, [Lcom/google/common/collect/bT;

    sget-object v2, Lcom/google/common/collect/bT;->i:Lcom/google/common/collect/bT;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/common/collect/bT;->j:Lcom/google/common/collect/bT;

    aput-object v2, v1, v4

    sget-object v2, Lcom/google/common/collect/bT;->k:Lcom/google/common/collect/bT;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/common/collect/bT;->l:Lcom/google/common/collect/bT;

    aput-object v2, v1, v6

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/common/collect/bT;->m:[[Lcom/google/common/collect/bT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/collect/bQ;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/bT;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(Lcom/google/common/collect/cz;ZZ)Lcom/google/common/collect/bT;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 533
    if-eqz p1, :cond_13

    const/4 v1, 0x1

    :goto_4
    if-eqz p2, :cond_7

    const/4 v0, 0x2

    :cond_7
    or-int/2addr v0, v1

    .line 534
    sget-object v1, Lcom/google/common/collect/bT;->m:[[Lcom/google/common/collect/bT;

    invoke-virtual {p0}, Lcom/google/common/collect/cz;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    aget-object v0, v1, v0

    return-object v0

    :cond_13
    move v1, v0

    .line 533
    goto :goto_4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/bT;
    .registers 2
    .parameter

    .prologue
    .line 352
    const-class v0, Lcom/google/common/collect/bT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bT;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/bT;
    .registers 1

    .prologue
    .line 352
    sget-object v0, Lcom/google/common/collect/bT;->n:[Lcom/google/common/collect/bT;

    invoke-virtual {v0}, [Lcom/google/common/collect/bT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/bT;

    return-object v0
.end method


# virtual methods
.method a(Lcom/google/common/collect/ct;Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)Lcom/google/common/collect/cs;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 557
    invoke-interface {p2}, Lcom/google/common/collect/cs;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/common/collect/cs;->c()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1, p3}, Lcom/google/common/collect/bT;->a(Lcom/google/common/collect/ct;Ljava/lang/Object;ILcom/google/common/collect/cs;)Lcom/google/common/collect/cs;

    move-result-object v0

    return-object v0
.end method

.method abstract a(Lcom/google/common/collect/ct;Ljava/lang/Object;ILcom/google/common/collect/cs;)Lcom/google/common/collect/cs;
.end method

.method a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 564
    invoke-interface {p1}, Lcom/google/common/collect/cs;->e()J

    move-result-wide v0

    invoke-interface {p2, v0, v1}, Lcom/google/common/collect/cs;->a(J)V

    .line 566
    invoke-interface {p1}, Lcom/google/common/collect/cs;->g()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/collect/bP;->a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    .line 567
    invoke-interface {p1}, Lcom/google/common/collect/cs;->f()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/collect/bP;->a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    .line 569
    invoke-static {p1}, Lcom/google/common/collect/bP;->d(Lcom/google/common/collect/cs;)V

    .line 570
    return-void
.end method

.method b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 576
    invoke-interface {p1}, Lcom/google/common/collect/cs;->i()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/common/collect/bP;->b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    .line 577
    invoke-interface {p1}, Lcom/google/common/collect/cs;->h()Lcom/google/common/collect/cs;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/common/collect/bP;->b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V

    .line 579
    invoke-static {p1}, Lcom/google/common/collect/bP;->e(Lcom/google/common/collect/cs;)V

    .line 580
    return-void
.end method
