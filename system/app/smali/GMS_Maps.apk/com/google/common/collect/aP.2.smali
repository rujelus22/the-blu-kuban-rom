.class Lcom/google/common/collect/aP;
.super Lcom/google/common/collect/ar;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/common/collect/aK;


# direct methods
.method constructor <init>(Lcom/google/common/collect/aK;)V
    .registers 2
    .parameter

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/google/common/collect/ar;-><init>()V

    .line 576
    iput-object p1, p0, Lcom/google/common/collect/aP;->a:Lcom/google/common/collect/aK;

    .line 577
    return-void
.end method


# virtual methods
.method a()Z
    .registers 2

    .prologue
    .line 593
    const/4 v0, 0x1

    return v0
.end method

.method public b()Lcom/google/common/collect/dY;
    .registers 2

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/common/collect/aP;->a:Lcom/google/common/collect/aK;

    invoke-virtual {v0}, Lcom/google/common/collect/aK;->g()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/common/collect/aP;->a:Lcom/google/common/collect/aK;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/aK;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/google/common/collect/aP;->b()Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .prologue
    .line 581
    iget-object v0, p0, Lcom/google/common/collect/aP;->a:Lcom/google/common/collect/aK;

    invoke-virtual {v0}, Lcom/google/common/collect/aK;->size()I

    move-result v0

    return v0
.end method
