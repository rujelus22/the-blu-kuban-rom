.class abstract Lcom/google/common/collect/q;
.super Lcom/google/common/collect/M;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1f3c5464cd7009c6L


# instance fields
.field private transient a:Ljava/util/Map;

.field private transient b:J


# direct methods
.method protected constructor <init>(Ljava/util/Map;)V
    .registers 4
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/common/collect/M;-><init>()V

    .line 63
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    .line 64
    invoke-super {p0}, Lcom/google/common/collect/M;->size()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/google/common/collect/q;Ljava/lang/Object;Ljava/util/Map;)I
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/q;->a(Ljava/lang/Object;Ljava/util/Map;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/Object;Ljava/util/Map;)I
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 307
    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    .line 308
    if-nez v0, :cond_b

    move v0, v1

    .line 313
    :goto_a
    return v0

    .line 311
    :cond_b
    invoke-virtual {v0, v1}, Lcom/google/common/collect/Y;->d(I)I

    move-result v0

    .line 312
    iget-wide v1, p0, Lcom/google/common/collect/q;->b:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/common/collect/q;->b:J

    goto :goto_a
.end method

.method static synthetic a(Lcom/google/common/collect/q;J)J
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/common/collect/q;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/common/collect/q;)J
    .registers 5
    .parameter

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/google/common/collect/q;->b:J

    return-wide v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 209
    :try_start_1
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    .line 210
    if-nez v0, :cond_e

    move v0, v1

    :goto_c
    move v1, v0

    .line 214
    :goto_d
    return v1

    .line 210
    :cond_e
    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I
    :try_end_11
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_11} :catch_15
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_11} :catch_13

    move-result v0

    goto :goto_c

    .line 213
    :catch_13
    move-exception v0

    goto :goto_d

    .line 211
    :catch_15
    move-exception v0

    goto :goto_d
.end method

.method public a(Ljava/lang/Object;I)I
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 228
    if-nez p2, :cond_9

    .line 229
    invoke-virtual {p0, p1}, Lcom/google/common/collect/q;->a(Ljava/lang/Object;)I

    move-result v2

    .line 246
    :goto_8
    return v2

    .line 231
    :cond_9
    if-lez p2, :cond_34

    move v0, v1

    :goto_c
    const-string v3, "occurrences cannot be negative: %s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    .line 235
    if-nez v0, :cond_36

    .line 237
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    new-instance v1, Lcom/google/common/collect/Y;

    invoke-direct {v1, p2}, Lcom/google/common/collect/Y;-><init>(I)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :goto_2d
    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    int-to-long v3, p2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    goto :goto_8

    :cond_34
    move v0, v2

    .line 231
    goto :goto_c

    .line 239
    :cond_36
    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I

    move-result v4

    .line 240
    int-to-long v5, v4

    int-to-long v7, p2

    add-long/2addr v5, v7

    .line 241
    const-wide/32 v7, 0x7fffffff

    cmp-long v3, v5, v7

    if-gtz v3, :cond_57

    move v3, v1

    :goto_45
    const-string v7, "too many occurrences: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-static {v3, v7, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 243
    invoke-virtual {v0, p2}, Lcom/google/common/collect/Y;->a(I)I

    move v2, v4

    goto :goto_2d

    :cond_57
    move v3, v2

    .line 241
    goto :goto_45
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/common/collect/M;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Object;I)I
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 250
    if-nez p2, :cond_9

    .line 251
    invoke-virtual {p0, p1}, Lcom/google/common/collect/q;->a(Ljava/lang/Object;)I

    move-result v2

    .line 272
    :cond_8
    :goto_8
    return v2

    .line 253
    :cond_9
    if-lez p2, :cond_35

    move v0, v1

    :goto_c
    const-string v3, "occurrences cannot be negative: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 255
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    .line 256
    if-eqz v0, :cond_8

    .line 260
    invoke-virtual {v0}, Lcom/google/common/collect/Y;->a()I

    move-result v1

    .line 263
    if-le v1, p2, :cond_37

    .line 270
    :goto_29
    neg-int v2, p2

    invoke-virtual {v0, v2}, Lcom/google/common/collect/Y;->b(I)I

    .line 271
    iget-wide v2, p0, Lcom/google/common/collect/q;->b:J

    int-to-long v4, p2

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/common/collect/q;->b:J

    move v2, v1

    .line 272
    goto :goto_8

    :cond_35
    move v0, v2

    .line 253
    goto :goto_c

    .line 267
    :cond_37
    iget-object v2, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move p2, v1

    goto :goto_29
.end method

.method b()Ljava/util/Iterator;
    .registers 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 94
    new-instance v1, Lcom/google/common/collect/r;

    invoke-direct {v1, p0, v0}, Lcom/google/common/collect/r;-><init>(Lcom/google/common/collect/q;Ljava/util/Iterator;)V

    return-object v1
.end method

.method c()I
    .registers 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public clear()V
    .registers 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/Y;

    .line 139
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/common/collect/Y;->c(I)V

    goto :goto_a

    .line 141
    :cond_1b
    iget-object v0, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/common/collect/q;->b:J

    .line 143
    return-void
.end method

.method d()Ljava/util/Set;
    .registers 3

    .prologue
    .line 319
    new-instance v0, Lcom/google/common/collect/t;

    iget-object v1, p0, Lcom/google/common/collect/q;->a:Ljava/util/Map;

    invoke-direct {v0, p0, v1}, Lcom/google/common/collect/t;-><init>(Lcom/google/common/collect/q;Ljava/util/Map;)V

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2

    .prologue
    .line 157
    new-instance v0, Lcom/google/common/collect/v;

    invoke-direct {v0, p0}, Lcom/google/common/collect/v;-><init>(Lcom/google/common/collect/q;)V

    return-object v0
.end method

.method public size()I
    .registers 3

    .prologue
    .line 153
    iget-wide v0, p0, Lcom/google/common/collect/q;->b:J

    invoke-static {v0, v1}, Lac/a;->a(J)I

    move-result v0

    return v0
.end method
