.class public abstract Lcom/google/common/collect/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cV;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field final transient b:Lcom/google/common/collect/ax;

.field final transient c:I


# direct methods
.method constructor <init>(Lcom/google/common/collect/ax;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    iput-object p1, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    .line 319
    iput p2, p0, Lcom/google/common/collect/az;->c:I

    .line 320
    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)Lcom/google/common/collect/ar;
.end method

.method a()Z
    .registers 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->d()Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 381
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .parameter

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/google/common/collect/az;->a(Ljava/lang/Object;)Lcom/google/common/collect/ar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/common/collect/az;->c()Lcom/google/common/collect/ax;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/google/common/collect/ax;
    .registers 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    return-object v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 443
    iget v0, p0, Lcom/google/common/collect/az;->c:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 452
    instance-of v0, p1, Lcom/google/common/collect/cV;

    if-eqz v0, :cond_11

    .line 453
    check-cast p1, Lcom/google/common/collect/cV;

    .line 454
    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-interface {p1}, Lcom/google/common/collect/cV;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/common/collect/ax;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 456
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/common/collect/az;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0}, Lcom/google/common/collect/ax;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
