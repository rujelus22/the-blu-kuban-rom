.class public abstract enum Lcom/google/common/collect/dL;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/common/collect/dL;

.field public static final enum b:Lcom/google/common/collect/dL;

.field public static final enum c:Lcom/google/common/collect/dL;

.field private static final synthetic d:[Lcom/google/common/collect/dL;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, Lcom/google/common/collect/dM;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/dM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dL;->a:Lcom/google/common/collect/dL;

    .line 154
    new-instance v0, Lcom/google/common/collect/dN;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, Lcom/google/common/collect/dN;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dL;->b:Lcom/google/common/collect/dL;

    .line 172
    new-instance v0, Lcom/google/common/collect/dO;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, Lcom/google/common/collect/dO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/common/collect/dL;->c:Lcom/google/common/collect/dL;

    .line 139
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/common/collect/dL;

    sget-object v1, Lcom/google/common/collect/dL;->a:Lcom/google/common/collect/dL;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/common/collect/dL;->b:Lcom/google/common/collect/dL;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/common/collect/dL;->c:Lcom/google/common/collect/dL;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/common/collect/dL;->d:[Lcom/google/common/collect/dL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/google/common/collect/dK;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/dL;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/common/collect/dL;
    .registers 2
    .parameter

    .prologue
    .line 139
    const-class v0, Lcom/google/common/collect/dL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/dL;

    return-object v0
.end method

.method public static values()[Lcom/google/common/collect/dL;
    .registers 1

    .prologue
    .line 139
    sget-object v0, Lcom/google/common/collect/dL;->d:[Lcom/google/common/collect/dL;

    invoke-virtual {v0}, [Lcom/google/common/collect/dL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/common/collect/dL;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
.end method
