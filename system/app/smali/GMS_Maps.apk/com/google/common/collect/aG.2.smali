.class public Lcom/google/common/collect/aG;
.super Lcom/google/common/collect/az;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/dz;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final transient a:Lcom/google/common/collect/aR;


# direct methods
.method constructor <init>(Lcom/google/common/collect/ax;ILjava/util/Comparator;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 343
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/az;-><init>(Lcom/google/common/collect/ax;I)V

    .line 344
    if-nez p3, :cond_9

    const/4 v0, 0x0

    :goto_6
    iput-object v0, p0, Lcom/google/common/collect/aG;->a:Lcom/google/common/collect/aR;

    .line 346
    return-void

    .line 344
    :cond_9
    invoke-static {p3}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;)Lcom/google/common/collect/aR;

    move-result-object v0

    goto :goto_6
.end method

.method static synthetic a(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/google/common/collect/aG;->b(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/google/common/collect/cV;Ljava/util/Comparator;)Lcom/google/common/collect/aG;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 304
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    invoke-interface {p0}, Lcom/google/common/collect/cV;->d()Z

    move-result v0

    if-eqz v0, :cond_10

    if-nez p1, :cond_10

    .line 306
    invoke-static {}, Lcom/google/common/collect/aG;->e()Lcom/google/common/collect/aG;

    move-result-object v0

    .line 334
    :cond_f
    :goto_f
    return-object v0

    .line 309
    :cond_10
    instance-of v0, p0, Lcom/google/common/collect/aG;

    if-eqz v0, :cond_1d

    move-object v0, p0

    .line 311
    check-cast v0, Lcom/google/common/collect/aG;

    .line 313
    invoke-virtual {v0}, Lcom/google/common/collect/aG;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 318
    :cond_1d
    invoke-static {}, Lcom/google/common/collect/ax;->i()Lcom/google/common/collect/ay;

    move-result-object v2

    .line 319
    const/4 v0, 0x0

    .line 322
    invoke-interface {p0}, Lcom/google/common/collect/cV;->b()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_60

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 323
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    .line 324
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 325
    if-nez p1, :cond_5b

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 328
    :goto_4b
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_6a

    .line 329
    invoke-virtual {v2, v4, v0}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    .line 330
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->size()I

    move-result v0

    add-int/2addr v0, v1

    :goto_59
    move v1, v0

    .line 332
    goto :goto_2f

    .line 325
    :cond_5b
    invoke-static {p1, v0}, Lcom/google/common/collect/aR;->a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/google/common/collect/aR;

    move-result-object v0

    goto :goto_4b

    .line 334
    :cond_60
    new-instance v0, Lcom/google/common/collect/aG;

    invoke-virtual {v2}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v2

    invoke-direct {v0, v2, v1, p1}, Lcom/google/common/collect/aG;-><init>(Lcom/google/common/collect/ax;ILjava/util/Comparator;)V

    goto :goto_f

    :cond_6a
    move v0, v1

    goto :goto_59
.end method

.method public static e()Lcom/google/common/collect/aG;
    .registers 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/common/collect/ad;->a:Lcom/google/common/collect/ad;

    return-object v0
.end method

.method public static f()Lcom/google/common/collect/aH;
    .registers 1

    .prologue
    .line 141
    new-instance v0, Lcom/google/common/collect/aH;

    invoke-direct {v0}, Lcom/google/common/collect/aH;-><init>()V

    return-object v0
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Lcom/google/common/collect/ar;
    .registers 3
    .parameter

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aG;->c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .parameter

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/common/collect/aG;->c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;
    .registers 3
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/common/collect/aG;->b:Lcom/google/common/collect/ax;

    invoke-virtual {v0, p1}, Lcom/google/common/collect/ax;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/ImmutableSet;

    .line 359
    if-eqz v0, :cond_b

    .line 364
    :goto_a
    return-object v0

    .line 361
    :cond_b
    iget-object v0, p0, Lcom/google/common/collect/aG;->a:Lcom/google/common/collect/aR;

    if-eqz v0, :cond_12

    .line 362
    iget-object v0, p0, Lcom/google/common/collect/aG;->a:Lcom/google/common/collect/aR;

    goto :goto_a

    .line 364
    :cond_12
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    goto :goto_a
.end method
