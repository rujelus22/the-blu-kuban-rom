.class public Lcom/google/common/collect/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/bw;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private transient a:Lcom/google/common/collect/bt;

.field private transient b:Lcom/google/common/collect/bt;

.field private transient c:Lcom/google/common/collect/cW;

.field private transient d:Ljava/util/Map;

.field private transient e:Ljava/util/Map;

.field private transient f:Ljava/util/Set;

.field private transient g:Ljava/util/List;

.field private transient h:Ljava/util/Map;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    invoke-static {}, Lcom/google/common/collect/bi;->g()Lcom/google/common/collect/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    .line 168
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    .line 169
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    .line 170
    return-void
.end method

.method public static a()Lcom/google/common/collect/bj;
    .registers 1

    .prologue
    .line 140
    new-instance v0, Lcom/google/common/collect/bj;

    invoke-direct {v0}, Lcom/google/common/collect/bj;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method static synthetic a(Lcom/google/common/collect/bj;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1, p2, p3}, Lcom/google/common/collect/bj;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    new-instance v1, Lcom/google/common/collect/bt;

    invoke-direct {v1, p1, p2}, Lcom/google/common/collect/bt;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 192
    iget-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    if-nez v0, :cond_1d

    .line 193
    iput-object v1, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    iput-object v1, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    .line 194
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :goto_17
    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    invoke-interface {v0, p1}, Lcom/google/common/collect/cW;->add(Ljava/lang/Object;)Z

    .line 227
    return-object v1

    .line 196
    :cond_1d
    if-nez p3, :cond_43

    .line 197
    iget-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    .line 198
    iget-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    .line 199
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bt;

    .line 200
    if-nez v0, :cond_3e

    .line 201
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    :goto_36
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iput-object v1, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    goto :goto_17

    .line 203
    :cond_3e
    iput-object v1, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    .line 204
    iput-object v0, v1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    goto :goto_36

    .line 209
    :cond_43
    iget-object v0, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    .line 210
    iget-object v0, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v0, v1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    .line 211
    iput-object p3, v1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    .line 212
    iput-object p3, v1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    .line 213
    iget-object v0, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    if-nez v0, :cond_63

    .line 214
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :goto_58
    iget-object v0, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    if-nez v0, :cond_68

    .line 219
    iput-object v1, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    .line 223
    :goto_5e
    iput-object v1, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    .line 224
    iput-object v1, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    goto :goto_17

    .line 216
    :cond_63
    iget-object v0, p3, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    goto :goto_58

    .line 221
    :cond_68
    iget-object v0, p3, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    goto :goto_5e
.end method

.method static synthetic a(Lcom/google/common/collect/bj;Lcom/google/common/collect/bt;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bt;)V

    return-void
.end method

.method static synthetic a(Lcom/google/common/collect/bj;Ljava/lang/Object;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->f(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/google/common/collect/bt;)V
    .registers 5
    .parameter

    .prologue
    .line 236
    iget-object v0, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_30

    .line 237
    iget-object v0, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    .line 241
    :goto_a
    iget-object v0, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_35

    .line 242
    iget-object v0, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    .line 246
    :goto_14
    iget-object v0, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_3a

    .line 247
    iget-object v0, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    .line 253
    :goto_1e
    iget-object v0, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_50

    .line 254
    iget-object v0, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    iget-object v1, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    iput-object v1, v0, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    .line 260
    :goto_28
    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/common/collect/cW;->remove(Ljava/lang/Object;)Z

    .line 261
    return-void

    .line 239
    :cond_30
    iget-object v0, p1, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    goto :goto_a

    .line 244
    :cond_35
    iget-object v0, p1, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    goto :goto_14

    .line 248
    :cond_3a
    iget-object v0, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_48

    .line 249
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/common/collect/bt;->e:Lcom/google/common/collect/bt;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1e

    .line 251
    :cond_48
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1e

    .line 255
    :cond_50
    iget-object v0, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_5e

    .line 256
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/common/collect/bt;->f:Lcom/google/common/collect/bt;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_28

    .line 258
    :cond_5e
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/common/collect/bt;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_28
.end method

.method static synthetic b(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/common/collect/bj;->b:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method static synthetic c(Lcom/google/common/collect/bj;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/google/common/collect/bj;)Lcom/google/common/collect/cW;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    return-object v0
.end method

.method static synthetic e(Lcom/google/common/collect/bj;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/common/collect/bj;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)V
    .registers 1
    .parameter

    .prologue
    .line 102
    invoke-static {p0}, Lcom/google/common/collect/bj;->g(Ljava/lang/Object;)V

    return-void
.end method

.method private f(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 265
    new-instance v0, Lcom/google/common/collect/bv;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/bv;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 266
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 267
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    .line 269
    :cond_12
    return-void
.end method

.method private static g(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 273
    if-nez p0, :cond_8

    .line 274
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 276
    :cond_8
    return-void
.end method

.method private h(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 620
    new-instance v0, Lcom/google/common/collect/bv;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/bv;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/common/collect/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 547
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/common/collect/bj;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/common/collect/bt;)Lcom/google/common/collect/bt;

    .line 548
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/util/Collection;
    .registers 3
    .parameter

    .prologue
    .line 101
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bj;->d(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/Map;
    .registers 2

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/google/common/collect/bj;->h:Ljava/util/Map;

    .line 1038
    if-nez v0, :cond_b

    .line 1039
    new-instance v0, Lcom/google/common/collect/bo;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bo;-><init>(Lcom/google/common/collect/bj;)V

    iput-object v0, p0, Lcom/google/common/collect/bj;->h:Ljava/util/Map;

    .line 1069
    :cond_b
    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/common/collect/bj;->c:Lcom/google/common/collect/cW;

    invoke-interface {v0}, Lcom/google/common/collect/cW;->size()I

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 631
    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->h(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 632
    invoke-direct {p0, p1}, Lcom/google/common/collect/bj;->f(Ljava/lang/Object;)V

    .line 633
    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/util/List;
    .registers 3
    .parameter

    .prologue
    .line 658
    new-instance v0, Lcom/google/common/collect/bk;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/bk;-><init>(Lcom/google/common/collect/bj;Ljava/lang/Object;)V

    return-object v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/common/collect/bj;->a:Lcom/google/common/collect/bt;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public e()Ljava/util/Set;
    .registers 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/common/collect/bj;->f:Ljava/util/Set;

    .line 679
    if-nez v0, :cond_b

    .line 680
    new-instance v0, Lcom/google/common/collect/bl;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bl;-><init>(Lcom/google/common/collect/bj;)V

    iput-object v0, p0, Lcom/google/common/collect/bj;->f:Ljava/util/Set;

    .line 696
    :cond_b
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 1082
    if-ne p1, p0, :cond_4

    .line 1083
    const/4 v0, 0x1

    .line 1089
    :goto_3
    return v0

    .line 1085
    :cond_4
    instance-of v0, p1, Lcom/google/common/collect/cV;

    if-eqz v0, :cond_17

    .line 1086
    check-cast p1, Lcom/google/common/collect/cV;

    .line 1087
    invoke-virtual {p0}, Lcom/google/common/collect/bj;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/common/collect/cV;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3

    .line 1089
    :cond_17
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public f()Ljava/util/List;
    .registers 2

    .prologue
    .line 844
    iget-object v0, p0, Lcom/google/common/collect/bj;->g:Ljava/util/List;

    .line 845
    if-nez v0, :cond_b

    .line 846
    new-instance v0, Lcom/google/common/collect/bm;

    invoke-direct {v0, p0}, Lcom/google/common/collect/bm;-><init>(Lcom/google/common/collect/bj;)V

    iput-object v0, p0, Lcom/google/common/collect/bj;->g:Ljava/util/List;

    .line 894
    :cond_b
    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .prologue
    .line 1099
    invoke-virtual {p0}, Lcom/google/common/collect/bj;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1109
    invoke-virtual {p0}, Lcom/google/common/collect/bj;->b()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
