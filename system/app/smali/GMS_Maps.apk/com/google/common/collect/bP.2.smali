.class Lcom/google/common/collect/bP;
.super Ljava/util/AbstractMap;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/concurrent/ConcurrentMap;


# static fields
.field static final p:Lcom/google/common/collect/cJ; = null

.field static final q:Ljava/util/Queue; = null

.field private static final serialVersionUID:J = 0x5L

.field private static final u:Ljava/util/logging/Logger;


# instance fields
.field final transient a:I

.field final transient b:I

.field final transient c:[Lcom/google/common/collect/ct;

.field final d:I

.field final e:Lcom/google/common/base/t;

.field final f:Lcom/google/common/base/t;

.field final g:Lcom/google/common/collect/cz;

.field final h:Lcom/google/common/collect/cz;

.field final i:I

.field final j:J

.field final k:J

.field final l:Ljava/util/Queue;

.field final m:Lcom/google/common/collect/bN;

.field final transient n:Lcom/google/common/collect/bT;

.field final o:Lcom/google/common/base/ae;

.field r:Ljava/util/Set;

.field s:Ljava/util/Collection;

.field t:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 136
    const-class v0, Lcom/google/common/collect/bP;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/bP;->u:Ljava/util/logging/Logger;

    .line 630
    new-instance v0, Lcom/google/common/collect/bQ;

    invoke-direct {v0}, Lcom/google/common/collect/bQ;-><init>()V

    sput-object v0, Lcom/google/common/collect/bP;->p:Lcom/google/common/collect/cJ;

    .line 920
    new-instance v0, Lcom/google/common/collect/bR;

    invoke-direct {v0}, Lcom/google/common/collect/bR;-><init>()V

    sput-object v0, Lcom/google/common/collect/bP;->q:Ljava/util/Queue;

    return-void
.end method

.method constructor <init>(Lcom/google/common/collect/bE;)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 196
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 197
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->e()I

    move-result v0

    const/high16 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/common/collect/bP;->d:I

    .line 199
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->f()Lcom/google/common/collect/cz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->g:Lcom/google/common/collect/cz;

    .line 200
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->g()Lcom/google/common/collect/cz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->h:Lcom/google/common/collect/cz;

    .line 202
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->b()Lcom/google/common/base/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->e:Lcom/google/common/base/t;

    .line 203
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->c()Lcom/google/common/base/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->f:Lcom/google/common/base/t;

    .line 205
    iget v0, p1, Lcom/google/common/collect/bE;->e:I

    iput v0, p0, Lcom/google/common/collect/bP;->i:I

    .line 206
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->i()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/common/collect/bP;->j:J

    .line 207
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/common/collect/bP;->k:J

    .line 209
    iget-object v0, p0, Lcom/google/common/collect/bP;->g:Lcom/google/common/collect/cz;

    invoke-virtual {p0}, Lcom/google/common/collect/bP;->b()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/common/collect/bP;->a()Z

    move-result v3

    invoke-static {v0, v1, v3}, Lcom/google/common/collect/bT;->a(Lcom/google/common/collect/cz;ZZ)Lcom/google/common/collect/bT;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->n:Lcom/google/common/collect/bT;

    .line 210
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->j()Lcom/google/common/base/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->o:Lcom/google/common/base/ae;

    .line 212
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->a()Lcom/google/common/collect/bN;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bP;->m:Lcom/google/common/collect/bN;

    .line 213
    iget-object v0, p0, Lcom/google/common/collect/bP;->m:Lcom/google/common/collect/bN;

    sget-object v1, Lcom/google/common/collect/al;->a:Lcom/google/common/collect/al;

    if-ne v0, v1, :cond_8e

    invoke-static {}, Lcom/google/common/collect/bP;->i()Ljava/util/Queue;

    move-result-object v0

    :goto_5f
    iput-object v0, p0, Lcom/google/common/collect/bP;->l:Ljava/util/Queue;

    .line 217
    invoke-virtual {p1}, Lcom/google/common/collect/bE;->d()I

    move-result v0

    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 218
    invoke-virtual {p0}, Lcom/google/common/collect/bP;->a()Z

    move-result v1

    if-eqz v1, :cond_77

    .line 219
    iget v1, p0, Lcom/google/common/collect/bP;->i:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_77
    move v1, v2

    move v3, v4

    .line 228
    :goto_79
    iget v5, p0, Lcom/google/common/collect/bP;->d:I

    if-ge v1, v5, :cond_94

    invoke-virtual {p0}, Lcom/google/common/collect/bP;->a()Z

    move-result v5

    if-eqz v5, :cond_89

    mul-int/lit8 v5, v1, 0x2

    iget v6, p0, Lcom/google/common/collect/bP;->i:I

    if-gt v5, v6, :cond_94

    .line 229
    :cond_89
    add-int/lit8 v3, v3, 0x1

    .line 230
    shl-int/lit8 v1, v1, 0x1

    goto :goto_79

    .line 213
    :cond_8e
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    goto :goto_5f

    .line 232
    :cond_94
    rsub-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/common/collect/bP;->b:I

    .line 233
    add-int/lit8 v3, v1, -0x1

    iput v3, p0, Lcom/google/common/collect/bP;->a:I

    .line 235
    invoke-virtual {p0, v1}, Lcom/google/common/collect/bP;->c(I)[Lcom/google/common/collect/ct;

    move-result-object v3

    iput-object v3, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    .line 237
    div-int v3, v0, v1

    .line 238
    mul-int v5, v3, v1

    if-ge v5, v0, :cond_e4

    .line 239
    add-int/lit8 v0, v3, 0x1

    .line 243
    :goto_aa
    if-ge v2, v0, :cond_af

    .line 244
    shl-int/lit8 v2, v2, 0x1

    goto :goto_aa

    .line 247
    :cond_af
    invoke-virtual {p0}, Lcom/google/common/collect/bP;->a()Z

    move-result v0

    if-eqz v0, :cond_d2

    .line 249
    iget v0, p0, Lcom/google/common/collect/bP;->i:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 250
    iget v3, p0, Lcom/google/common/collect/bP;->i:I

    rem-int v1, v3, v1

    .line 251
    :goto_be
    iget-object v3, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    array-length v3, v3

    if-ge v4, v3, :cond_e3

    .line 252
    if-ne v4, v1, :cond_c7

    .line 253
    add-int/lit8 v0, v0, -0x1

    .line 255
    :cond_c7
    iget-object v3, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    invoke-virtual {p0, v2, v0}, Lcom/google/common/collect/bP;->a(II)Lcom/google/common/collect/ct;

    move-result-object v5

    aput-object v5, v3, v4

    .line 251
    add-int/lit8 v4, v4, 0x1

    goto :goto_be

    .line 259
    :cond_d2
    :goto_d2
    iget-object v0, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    array-length v0, v0

    if-ge v4, v0, :cond_e3

    .line 260
    iget-object v0, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    const/4 v1, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/common/collect/bP;->a(II)Lcom/google/common/collect/ct;

    move-result-object v1

    aput-object v1, v0, v4

    .line 259
    add-int/lit8 v4, v4, 0x1

    goto :goto_d2

    .line 264
    :cond_e3
    return-void

    :cond_e4
    move v0, v3

    goto :goto_aa
.end method

.method static a(I)I
    .registers 4
    .parameter

    .prologue
    .line 1863
    shl-int/lit8 v0, p0, 0xf

    xor-int/lit16 v0, v0, -0x3283

    add-int/2addr v0, p0

    .line 1864
    ushr-int/lit8 v1, v0, 0xa

    xor-int/2addr v0, v1

    .line 1865
    shl-int/lit8 v1, v0, 0x3

    add-int/2addr v0, v1

    .line 1866
    ushr-int/lit8 v1, v0, 0x6

    xor-int/2addr v0, v1

    .line 1867
    shl-int/lit8 v1, v0, 0x2

    shl-int/lit8 v2, v0, 0xe

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1868
    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    return v0
.end method

.method static a(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1979
    invoke-interface {p0, p1}, Lcom/google/common/collect/cs;->a(Lcom/google/common/collect/cs;)V

    .line 1980
    invoke-interface {p1, p0}, Lcom/google/common/collect/cs;->b(Lcom/google/common/collect/cs;)V

    .line 1981
    return-void
.end method

.method static b(Lcom/google/common/collect/cs;Lcom/google/common/collect/cs;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 2011
    invoke-interface {p0, p1}, Lcom/google/common/collect/cs;->c(Lcom/google/common/collect/cs;)V

    .line 2012
    invoke-interface {p1, p0}, Lcom/google/common/collect/cs;->d(Lcom/google/common/collect/cs;)V

    .line 2013
    return-void
.end method

.method static d(Lcom/google/common/collect/cs;)V
    .registers 2
    .parameter

    .prologue
    .line 1985
    invoke-static {}, Lcom/google/common/collect/bP;->h()Lcom/google/common/collect/cs;

    move-result-object v0

    .line 1986
    invoke-interface {p0, v0}, Lcom/google/common/collect/cs;->a(Lcom/google/common/collect/cs;)V

    .line 1987
    invoke-interface {p0, v0}, Lcom/google/common/collect/cs;->b(Lcom/google/common/collect/cs;)V

    .line 1988
    return-void
.end method

.method static e(Lcom/google/common/collect/cs;)V
    .registers 2
    .parameter

    .prologue
    .line 2017
    invoke-static {}, Lcom/google/common/collect/bP;->h()Lcom/google/common/collect/cs;

    move-result-object v0

    .line 2018
    invoke-interface {p0, v0}, Lcom/google/common/collect/cs;->c(Lcom/google/common/collect/cs;)V

    .line 2019
    invoke-interface {p0, v0}, Lcom/google/common/collect/cs;->d(Lcom/google/common/collect/cs;)V

    .line 2020
    return-void
.end method

.method static g()Lcom/google/common/collect/cJ;
    .registers 1

    .prologue
    .line 666
    sget-object v0, Lcom/google/common/collect/bP;->p:Lcom/google/common/collect/cJ;

    return-object v0
.end method

.method static h()Lcom/google/common/collect/cs;
    .registers 1

    .prologue
    .line 917
    sget-object v0, Lcom/google/common/collect/cr;->a:Lcom/google/common/collect/cr;

    return-object v0
.end method

.method static i()Ljava/util/Queue;
    .registers 1

    .prologue
    .line 952
    sget-object v0, Lcom/google/common/collect/bP;->q:Ljava/util/Queue;

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 1901
    iget-object v0, p0, Lcom/google/common/collect/bP;->e:Lcom/google/common/base/t;

    invoke-virtual {v0, p1}, Lcom/google/common/base/t;->a(Ljava/lang/Object;)I

    move-result v0

    .line 1902
    invoke-static {v0}, Lcom/google/common/collect/bP;->a(I)I

    move-result v0

    return v0
.end method

.method a(II)Lcom/google/common/collect/ct;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1937
    new-instance v0, Lcom/google/common/collect/ct;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/common/collect/ct;-><init>(Lcom/google/common/collect/bP;II)V

    return-object v0
.end method

.method a(Lcom/google/common/collect/cJ;)V
    .registers 5
    .parameter

    .prologue
    .line 1906
    invoke-interface {p1}, Lcom/google/common/collect/cJ;->a()Lcom/google/common/collect/cs;

    move-result-object v0

    .line 1907
    invoke-interface {v0}, Lcom/google/common/collect/cs;->c()I

    move-result v1

    .line 1908
    invoke-virtual {p0, v1}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/common/collect/cs;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lcom/google/common/collect/ct;->a(Ljava/lang/Object;ILcom/google/common/collect/cJ;)Z

    .line 1909
    return-void
.end method

.method a(Lcom/google/common/collect/cs;)V
    .registers 4
    .parameter

    .prologue
    .line 1912
    invoke-interface {p1}, Lcom/google/common/collect/cs;->c()I

    move-result v0

    .line 1913
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/common/collect/ct;->a(Lcom/google/common/collect/cs;I)Z

    .line 1914
    return-void
.end method

.method a()Z
    .registers 3

    .prologue
    .line 267
    iget v0, p0, Lcom/google/common/collect/bP;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method a(Lcom/google/common/collect/cs;J)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 1974
    invoke-interface {p1}, Lcom/google/common/collect/cs;->e()J

    move-result-wide v0

    sub-long v0, p2, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method b(I)Lcom/google/common/collect/ct;
    .registers 5
    .parameter

    .prologue
    .line 1933
    iget-object v0, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    iget v1, p0, Lcom/google/common/collect/bP;->b:I

    ushr-int v1, p1, v1

    iget v2, p0, Lcom/google/common/collect/bP;->a:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method b(Lcom/google/common/collect/cs;)Ljava/lang/Object;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1946
    invoke-interface {p1}, Lcom/google/common/collect/cs;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_8

    .line 1957
    :cond_7
    :goto_7
    return-object v0

    .line 1949
    :cond_8
    invoke-interface {p1}, Lcom/google/common/collect/cs;->a()Lcom/google/common/collect/cJ;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/common/collect/cJ;->get()Ljava/lang/Object;

    move-result-object v1

    .line 1950
    if-eqz v1, :cond_7

    .line 1954
    invoke-virtual {p0}, Lcom/google/common/collect/bP;->b()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->c(Lcom/google/common/collect/cs;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_1e
    move-object v0, v1

    .line 1957
    goto :goto_7
.end method

.method b()Z
    .registers 2

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/common/collect/bP;->c()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lcom/google/common/collect/bP;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method c()Z
    .registers 5

    .prologue
    .line 275
    iget-wide v0, p0, Lcom/google/common/collect/bP;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method c(Lcom/google/common/collect/cs;)Z
    .registers 4
    .parameter

    .prologue
    .line 1966
    iget-object v0, p0, Lcom/google/common/collect/bP;->o:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/common/collect/bP;->a(Lcom/google/common/collect/cs;J)Z

    move-result v0

    return v0
.end method

.method final c(I)[Lcom/google/common/collect/ct;
    .registers 3
    .parameter

    .prologue
    .line 2024
    new-array v0, p1, [Lcom/google/common/collect/ct;

    return-object v0
.end method

.method public clear()V
    .registers 5

    .prologue
    .line 3615
    iget-object v1, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 3616
    invoke-virtual {v3}, Lcom/google/common/collect/ct;->m()V

    .line 3615
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 3618
    :cond_e
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 3507
    if-nez p1, :cond_4

    .line 3508
    const/4 v0, 0x0

    .line 3511
    :goto_3
    return v0

    .line 3510
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3511
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/common/collect/ct;->d(Ljava/lang/Object;I)Z

    move-result v0

    goto :goto_3
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .registers 16
    .parameter

    .prologue
    .line 3516
    if-nez p1, :cond_4

    .line 3517
    const/4 v0, 0x0

    .line 3550
    :goto_3
    return v0

    .line 3525
    :cond_4
    iget-object v8, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    .line 3526
    const-wide/16 v3, -0x1

    .line 3527
    const/4 v0, 0x0

    move v5, v0

    move-wide v6, v3

    :goto_b
    const/4 v0, 0x3

    if-ge v5, v0, :cond_51

    .line 3528
    const-wide/16 v1, 0x0

    .line 3529
    array-length v9, v8

    const/4 v0, 0x0

    move-wide v3, v1

    move v2, v0

    :goto_14
    if-ge v2, v9, :cond_4d

    aget-object v10, v8, v2

    .line 3532
    iget v0, v10, Lcom/google/common/collect/ct;->b:I

    .line 3534
    iget-object v11, v10, Lcom/google/common/collect/ct;->e:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 3535
    const/4 v0, 0x0

    move v1, v0

    :goto_1e
    invoke-virtual {v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_45

    .line 3536
    invoke-virtual {v11, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/cs;

    :goto_2a
    if-eqz v0, :cond_41

    .line 3537
    invoke-virtual {v10, v0}, Lcom/google/common/collect/ct;->f(Lcom/google/common/collect/cs;)Ljava/lang/Object;

    move-result-object v12

    .line 3538
    if-eqz v12, :cond_3c

    iget-object v13, p0, Lcom/google/common/collect/bP;->f:Lcom/google/common/base/t;

    invoke-virtual {v13, p1, v12}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3c

    .line 3539
    const/4 v0, 0x1

    goto :goto_3

    .line 3536
    :cond_3c
    invoke-interface {v0}, Lcom/google/common/collect/cs;->b()Lcom/google/common/collect/cs;

    move-result-object v0

    goto :goto_2a

    .line 3535
    :cond_41
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1e

    .line 3543
    :cond_45
    iget v0, v10, Lcom/google/common/collect/ct;->c:I

    int-to-long v0, v0

    add-long/2addr v3, v0

    .line 3529
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_14

    .line 3545
    :cond_4d
    cmp-long v0, v3, v6

    if-nez v0, :cond_53

    .line 3550
    :cond_51
    const/4 v0, 0x0

    goto :goto_3

    .line 3527
    :cond_53
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move-wide v6, v3

    goto :goto_b
.end method

.method d()Z
    .registers 5

    .prologue
    .line 279
    iget-wide v0, p0, Lcom/google/common/collect/bP;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method e()Z
    .registers 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/common/collect/bP;->g:Lcom/google/common/collect/cz;

    sget-object v1, Lcom/google/common/collect/cz;->a:Lcom/google/common/collect/cz;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public entrySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3640
    iget-object v0, p0, Lcom/google/common/collect/bP;->t:Ljava/util/Set;

    .line 3641
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/google/common/collect/ch;

    invoke-direct {v0, p0}, Lcom/google/common/collect/ch;-><init>(Lcom/google/common/collect/bP;)V

    iput-object v0, p0, Lcom/google/common/collect/bP;->t:Ljava/util/Set;

    goto :goto_4
.end method

.method f()Z
    .registers 3

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/common/collect/bP;->h:Lcom/google/common/collect/cz;

    sget-object v1, Lcom/google/common/collect/cz;->a:Lcom/google/common/collect/cz;

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 3475
    if-nez p1, :cond_4

    .line 3476
    const/4 v0, 0x0

    .line 3479
    :goto_3
    return-object v0

    .line 3478
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3479
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/common/collect/ct;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public isEmpty()Z
    .registers 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 3440
    .line 3441
    iget-object v6, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    move v0, v1

    move-wide v2, v4

    .line 3442
    :goto_7
    array-length v7, v6

    if-ge v0, v7, :cond_1a

    .line 3443
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/common/collect/ct;->b:I

    if-eqz v7, :cond_11

    .line 3460
    :cond_10
    :goto_10
    return v1

    .line 3446
    :cond_11
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/common/collect/ct;->c:I

    int-to-long v7, v7

    add-long/2addr v2, v7

    .line 3442
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 3449
    :cond_1a
    cmp-long v0, v2, v4

    if-eqz v0, :cond_35

    move v0, v1

    .line 3450
    :goto_1f
    array-length v7, v6

    if-ge v0, v7, :cond_31

    .line 3451
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/common/collect/ct;->b:I

    if-nez v7, :cond_10

    .line 3454
    aget-object v7, v6, v0

    iget v7, v7, Lcom/google/common/collect/ct;->c:I

    int-to-long v7, v7

    sub-long/2addr v2, v7

    .line 3450
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 3456
    :cond_31
    cmp-long v0, v2, v4

    if-nez v0, :cond_10

    .line 3460
    :cond_35
    const/4 v1, 0x1

    goto :goto_10
.end method

.method j()V
    .registers 5

    .prologue
    .line 1999
    :goto_0
    iget-object v0, p0, Lcom/google/common/collect/bP;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/collect/bO;

    if-eqz v0, :cond_1b

    .line 2001
    :try_start_a
    iget-object v1, p0, Lcom/google/common/collect/bP;->m:Lcom/google/common/collect/bN;

    invoke-interface {v1, v0}, Lcom/google/common/collect/bN;->a(Lcom/google/common/collect/bO;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_f} :catch_10

    goto :goto_0

    .line 2002
    :catch_10
    move-exception v0

    .line 2003
    sget-object v1, Lcom/google/common/collect/bP;->u:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Exception thrown by removal listener"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2006
    :cond_1b
    return-void
.end method

.method public keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 3624
    iget-object v0, p0, Lcom/google/common/collect/bP;->r:Ljava/util/Set;

    .line 3625
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/google/common/collect/cq;

    invoke-direct {v0, p0}, Lcom/google/common/collect/cq;-><init>(Lcom/google/common/collect/bP;)V

    iput-object v0, p0, Lcom/google/common/collect/bP;->r:Ljava/util/Set;

    goto :goto_4
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 3555
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3556
    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3557
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3558
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/common/collect/ct;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public putAll(Ljava/util/Map;)V
    .registers 5
    .parameter

    .prologue
    .line 3571
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3572
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/google/common/collect/bP;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 3574
    :cond_20
    return-void
.end method

.method public putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 3563
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3564
    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3565
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3566
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, p2, v2}, Lcom/google/common/collect/ct;->a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 3578
    if-nez p1, :cond_4

    .line 3579
    const/4 v0, 0x0

    .line 3582
    :goto_3
    return-object v0

    .line 3581
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3582
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/common/collect/ct;->e(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_3
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3587
    if-eqz p1, :cond_4

    if-nez p2, :cond_6

    .line 3588
    :cond_4
    const/4 v0, 0x0

    .line 3591
    :goto_5
    return v0

    .line 3590
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3591
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/common/collect/ct;->b(Ljava/lang/Object;ILjava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3607
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3608
    invoke-static {p2}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3609
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3610
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/common/collect/ct;->a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3596
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3597
    invoke-static {p3}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3598
    if-nez p2, :cond_a

    .line 3599
    const/4 v0, 0x0

    .line 3602
    :goto_9
    return v0

    .line 3601
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/common/collect/bP;->a(Ljava/lang/Object;)I

    move-result v0

    .line 3602
    invoke-virtual {p0, v0}, Lcom/google/common/collect/bP;->b(I)Lcom/google/common/collect/ct;

    move-result-object v1

    invoke-virtual {v1, p1, v0, p2, p3}, Lcom/google/common/collect/ct;->a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_9
.end method

.method public size()I
    .registers 7

    .prologue
    .line 3465
    iget-object v3, p0, Lcom/google/common/collect/bP;->c:[Lcom/google/common/collect/ct;

    .line 3466
    const-wide/16 v1, 0x0

    .line 3467
    const/4 v0, 0x0

    :goto_5
    array-length v4, v3

    if-ge v0, v4, :cond_11

    .line 3468
    aget-object v4, v3, v0

    iget v4, v4, Lcom/google/common/collect/ct;->b:I

    int-to-long v4, v4

    add-long/2addr v1, v4

    .line 3467
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 3470
    :cond_11
    invoke-static {v1, v2}, Lac/a;->a(J)I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .registers 2

    .prologue
    .line 3632
    iget-object v0, p0, Lcom/google/common/collect/bP;->s:Ljava/util/Collection;

    .line 3633
    if-eqz v0, :cond_5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Lcom/google/common/collect/cK;

    invoke-direct {v0, p0}, Lcom/google/common/collect/cK;-><init>(Lcom/google/common/collect/bP;)V

    iput-object v0, p0, Lcom/google/common/collect/bP;->s:Ljava/util/Collection;

    goto :goto_4
.end method
