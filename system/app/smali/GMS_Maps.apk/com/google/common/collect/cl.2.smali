.class Lcom/google/common/collect/cL;
.super Ljava/lang/ref/WeakReference;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cs;


# instance fields
.field final a:I

.field final b:Lcom/google/common/collect/cs;

.field volatile c:Lcom/google/common/collect/cJ;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/cs;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1482
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1550
    invoke-static {}, Lcom/google/common/collect/bP;->g()Lcom/google/common/collect/cJ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/cL;->c:Lcom/google/common/collect/cJ;

    .line 1483
    iput p3, p0, Lcom/google/common/collect/cL;->a:I

    .line 1484
    iput-object p4, p0, Lcom/google/common/collect/cL;->b:Lcom/google/common/collect/cs;

    .line 1485
    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/cJ;
    .registers 2

    .prologue
    .line 1554
    iget-object v0, p0, Lcom/google/common/collect/cL;->c:Lcom/google/common/collect/cJ;

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1501
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/common/collect/cJ;)V
    .registers 3
    .parameter

    .prologue
    .line 1559
    iget-object v0, p0, Lcom/google/common/collect/cL;->c:Lcom/google/common/collect/cJ;

    .line 1560
    iput-object p1, p0, Lcom/google/common/collect/cL;->c:Lcom/google/common/collect/cJ;

    .line 1561
    invoke-interface {v0, p1}, Lcom/google/common/collect/cJ;->a(Lcom/google/common/collect/cJ;)V

    .line 1562
    return-void
.end method

.method public a(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1511
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1571
    iget-object v0, p0, Lcom/google/common/collect/cL;->b:Lcom/google/common/collect/cs;

    return-object v0
.end method

.method public b(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1521
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1566
    iget v0, p0, Lcom/google/common/collect/cL;->a:I

    return v0
.end method

.method public c(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1533
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1489
    invoke-virtual {p0}, Lcom/google/common/collect/cL;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1543
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1496
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1506
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1516
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1528
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1538
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
