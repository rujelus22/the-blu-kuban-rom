.class Lcom/google/common/collect/cu;
.super Ljava/lang/ref/SoftReference;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/cs;


# instance fields
.field final a:I

.field final b:Lcom/google/common/collect/cs;

.field volatile c:Lcom/google/common/collect/cJ;


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILcom/google/common/collect/cs;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1225
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1292
    invoke-static {}, Lcom/google/common/collect/bP;->g()Lcom/google/common/collect/cJ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/cu;->c:Lcom/google/common/collect/cJ;

    .line 1226
    iput p3, p0, Lcom/google/common/collect/cu;->a:I

    .line 1227
    iput-object p4, p0, Lcom/google/common/collect/cu;->b:Lcom/google/common/collect/cs;

    .line 1228
    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/cJ;
    .registers 2

    .prologue
    .line 1296
    iget-object v0, p0, Lcom/google/common/collect/cu;->c:Lcom/google/common/collect/cJ;

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/common/collect/cJ;)V
    .registers 3
    .parameter

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/google/common/collect/cu;->c:Lcom/google/common/collect/cJ;

    .line 1302
    iput-object p1, p0, Lcom/google/common/collect/cu;->c:Lcom/google/common/collect/cJ;

    .line 1303
    invoke-interface {v0, p1}, Lcom/google/common/collect/cJ;->a(Lcom/google/common/collect/cJ;)V

    .line 1304
    return-void
.end method

.method public a(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1253
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/google/common/collect/cu;->b:Lcom/google/common/collect/cs;

    return-object v0
.end method

.method public b(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1263
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1308
    iget v0, p0, Lcom/google/common/collect/cu;->a:I

    return v0
.end method

.method public c(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1232
    invoke-virtual {p0}, Lcom/google/common/collect/cu;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/common/collect/cs;)V
    .registers 3
    .parameter

    .prologue
    .line 1285
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()J
    .registers 2

    .prologue
    .line 1238
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1248
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public g()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1258
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public h()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1270
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public i()Lcom/google/common/collect/cs;
    .registers 2

    .prologue
    .line 1280
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
