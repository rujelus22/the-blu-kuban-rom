.class public final Lcom/google/common/collect/Maps;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/common/base/D;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 1982
    sget-object v0, Lcom/google/common/collect/S;->a:Lcom/google/common/base/A;

    const-string v1, "="

    invoke-virtual {v0, v1}, Lcom/google/common/base/A;->b(Ljava/lang/String;)Lcom/google/common/base/D;

    move-result-object v0

    sput-object v0, Lcom/google/common/collect/Maps;->a:Lcom/google/common/base/D;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1991
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v0

    .line 1993
    :goto_4
    return-object v0

    .line 1992
    :catch_5
    move-exception v0

    .line 1993
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a(Ljava/lang/Class;)Ljava/util/EnumMap;
    .registers 3
    .parameter

    .prologue
    .line 245
    new-instance v1, Ljava/util/EnumMap;

    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    invoke-direct {v1, v0}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    return-object v1
.end method

.method public static a()Ljava/util/HashMap;
    .registers 1

    .prologue
    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method public static a(I)Ljava/util/HashMap;
    .registers 3
    .parameter

    .prologue
    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-static {p0}, Lcom/google/common/collect/Maps;->b(I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;)Ljava/util/HashMap;
    .registers 2
    .parameter

    .prologue
    .line 138
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 722
    new-instance v0, Lcom/google/common/collect/av;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/av;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method static a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;
    .registers 2
    .parameter

    .prologue
    .line 749
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 750
    new-instance v0, Lcom/google/common/collect/cR;

    invoke-direct {v0, p0}, Lcom/google/common/collect/cR;-><init>(Ljava/util/Map$Entry;)V

    return-object v0
.end method

.method static a(Ljava/util/Collection;Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2023
    instance-of v0, p1, Ljava/util/Map$Entry;

    if-nez v0, :cond_6

    .line 2024
    const/4 v0, 0x0

    .line 2026
    :goto_5
    return v0

    :cond_6
    check-cast p1, Ljava/util/Map$Entry;

    invoke-static {p1}, Lcom/google/common/collect/Maps;->a(Ljava/util/Map$Entry;)Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method static b(I)I
    .registers 2
    .parameter

    .prologue
    .line 112
    const/4 v0, 0x3

    if-ge p0, v0, :cond_e

    .line 113
    if-ltz p0, :cond_c

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    .line 114
    add-int/lit8 v0, p0, 0x1

    .line 119
    :goto_b
    return v0

    .line 113
    :cond_c
    const/4 v0, 0x0

    goto :goto_6

    .line 116
    :cond_e
    const/high16 v0, 0x4000

    if-ge p0, v0, :cond_16

    .line 117
    div-int/lit8 v0, p0, 0x3

    add-int/2addr v0, p0

    goto :goto_b

    .line 119
    :cond_16
    const v0, 0x7fffffff

    goto :goto_b
.end method

.method static b(Ljava/util/Map;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 2074
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/common/collect/S;->a(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 2076
    sget-object v1, Lcom/google/common/collect/Maps;->a:Lcom/google/common/base/D;

    invoke-virtual {v1, v0, p0}, Lcom/google/common/base/D;->a(Ljava/lang/StringBuilder;Ljava/util/Map;)Ljava/lang/StringBuilder;

    .line 2077
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b()Ljava/util/LinkedHashMap;
    .registers 1

    .prologue
    .line 151
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    return-object v0
.end method

.method static b(Ljava/util/Map;Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2003
    :try_start_0
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_3} :catch_5

    move-result v0

    .line 2005
    :goto_4
    return v0

    .line 2004
    :catch_5
    move-exception v0

    .line 2005
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static c()Ljava/util/concurrent/ConcurrentMap;
    .registers 1

    .prologue
    .line 186
    new-instance v0, Lcom/google/common/collect/bE;

    invoke-direct {v0}, Lcom/google/common/collect/bE;-><init>()V

    invoke-virtual {v0}, Lcom/google/common/collect/bE;->k()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method public static d()Ljava/util/IdentityHashMap;
    .registers 1

    .prologue
    .line 268
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    return-object v0
.end method

.method public static newTreeMap()Ljava/util/TreeMap;
    .registers 1

    .prologue
    .line 199
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    return-object v0
.end method
