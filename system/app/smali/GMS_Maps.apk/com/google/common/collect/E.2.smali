.class Lcom/google/common/collect/E;
.super Lcom/google/common/collect/y;
.source "SourceFile"

# interfaces
.implements Ljava/util/SortedMap;


# instance fields
.field d:Ljava/util/SortedSet;

.field final synthetic e:Lcom/google/common/collect/x;


# direct methods
.method constructor <init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1300
    iput-object p1, p0, Lcom/google/common/collect/E;->e:Lcom/google/common/collect/x;

    .line 1301
    invoke-direct {p0, p1, p2}, Lcom/google/common/collect/y;-><init>(Lcom/google/common/collect/x;Ljava/util/Map;)V

    .line 1302
    return-void
.end method


# virtual methods
.method a()Ljava/util/SortedMap;
    .registers 2

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/google/common/collect/E;->a:Ljava/util/Map;

    check-cast v0, Ljava/util/SortedMap;

    return-object v0
.end method

.method public b()Ljava/util/SortedSet;
    .registers 4

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/google/common/collect/E;->d:Ljava/util/SortedSet;

    .line 1344
    if-nez v0, :cond_11

    new-instance v0, Lcom/google/common/collect/F;

    iget-object v1, p0, Lcom/google/common/collect/E;->e:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/F;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    iput-object v0, p0, Lcom/google/common/collect/E;->d:Ljava/util/SortedSet;

    :cond_11
    return-object v0
.end method

.method public comparator()Ljava/util/Comparator;
    .registers 2

    .prologue
    .line 1310
    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->comparator()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public firstKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1315
    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->firstKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public headMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5
    .parameter

    .prologue
    .line 1325
    new-instance v0, Lcom/google/common/collect/E;

    iget-object v1, p0, Lcom/google/common/collect/E;->e:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->headMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/E;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public synthetic keySet()Ljava/util/Set;
    .registers 2

    .prologue
    .line 1298
    invoke-virtual {p0}, Lcom/google/common/collect/E;->b()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public lastKey()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 1320
    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1330
    new-instance v0, Lcom/google/common/collect/E;

    iget-object v1, p0, Lcom/google/common/collect/E;->e:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Ljava/util/SortedMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/E;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    return-object v0
.end method

.method public tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;
    .registers 5
    .parameter

    .prologue
    .line 1335
    new-instance v0, Lcom/google/common/collect/E;

    iget-object v1, p0, Lcom/google/common/collect/E;->e:Lcom/google/common/collect/x;

    invoke-virtual {p0}, Lcom/google/common/collect/E;->a()Ljava/util/SortedMap;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/SortedMap;->tailMap(Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/common/collect/E;-><init>(Lcom/google/common/collect/x;Ljava/util/SortedMap;)V

    return-object v0
.end method
