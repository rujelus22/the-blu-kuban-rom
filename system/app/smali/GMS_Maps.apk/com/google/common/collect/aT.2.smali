.class public final Lcom/google/common/collect/aT;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/Iterable;)I
    .registers 2
    .parameter

    .prologue
    .line 108
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_b

    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/aZ;->a(Ljava/util/Iterator;)I

    move-result v0

    goto :goto_a
.end method

.method public static a(Ljava/lang/Iterable;Lcom/google/common/base/x;)Ljava/lang/Iterable;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 704
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 706
    new-instance v0, Lcom/google/common/collect/aX;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/aX;-><init>(Ljava/lang/Iterable;Lcom/google/common/base/x;)V

    return-object v0
.end method

.method private static a(Ljava/util/List;Lcom/google/common/base/K;II)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 229
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-le v0, p3, :cond_18

    .line 230
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 231
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 229
    :cond_15
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 235
    :cond_18
    add-int/lit8 v0, p3, -0x1

    :goto_1a
    if-lt v0, p2, :cond_22

    .line 236
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 235
    add-int/lit8 v0, v0, -0x1

    goto :goto_1a

    .line 238
    :cond_22
    return-void
.end method

.method public static a(Ljava/lang/Iterable;Lcom/google/common/base/K;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 183
    instance-of v0, p0, Ljava/util/RandomAccess;

    if-eqz v0, :cond_15

    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_15

    .line 184
    check-cast p0, Ljava/util/List;

    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/base/K;

    invoke-static {p0, v0}, Lcom/google/common/collect/aT;->a(Ljava/util/List;Lcom/google/common/base/K;)Z

    move-result v0

    .line 187
    :goto_14
    return v0

    :cond_15
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/common/collect/aZ;->a(Ljava/util/Iterator;Lcom/google/common/base/K;)Z

    move-result v0

    goto :goto_14
.end method

.method private static a(Ljava/util/List;Lcom/google/common/base/K;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 194
    move v0, v1

    move v2, v1

    .line 197
    :goto_4
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_23

    .line 198
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 199
    invoke-interface {p1, v4}, Lcom/google/common/base/K;->a(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1b

    .line 200
    if-le v2, v0, :cond_19

    .line 202
    :try_start_16
    invoke-interface {p0, v0, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_19
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_16 .. :try_end_19} :catch_1e

    .line 208
    :cond_19
    add-int/lit8 v0, v0, 0x1

    .line 197
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 203
    :catch_1e
    move-exception v1

    .line 204
    invoke-static {p0, p1, v0, v2}, Lcom/google/common/collect/aT;->a(Ljava/util/List;Lcom/google/common/base/K;II)V

    .line 214
    :goto_22
    return v3

    .line 213
    :cond_23
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p0, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 214
    if-eq v2, v0, :cond_31

    move v1, v3

    :cond_31
    move v3, v1

    goto :goto_22
.end method

.method public static b(Ljava/lang/Iterable;Lcom/google/common/base/K;)Ljava/lang/Iterable;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 580
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    invoke-static {p1}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    new-instance v0, Lcom/google/common/collect/aW;

    invoke-direct {v0, p0, p1}, Lcom/google/common/collect/aW;-><init>(Ljava/lang/Iterable;Lcom/google/common/base/K;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Iterable;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 257
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/aZ;->b(Ljava/util/Iterator;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/Iterable;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 268
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/aZ;->c(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static d(Ljava/lang/Iterable;)[Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 306
    invoke-static {p0}, Lcom/google/common/collect/aT;->g(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .registers 2
    .parameter

    .prologue
    .line 485
    invoke-static {p0}, Lcom/google/common/base/J;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    new-instance v0, Lcom/google/common/collect/aU;

    invoke-direct {v0, p0}, Lcom/google/common/collect/aU;-><init>(Ljava/lang/Iterable;)V

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Iterable;)Lcom/google/common/collect/dY;
    .registers 2
    .parameter

    .prologue
    .line 60
    invoke-static {p0}, Lcom/google/common/collect/aT;->h(Ljava/lang/Iterable;)Lcom/google/common/collect/dY;

    move-result-object v0

    return-object v0
.end method

.method private static g(Ljava/lang/Iterable;)Ljava/util/Collection;
    .registers 2
    .parameter

    .prologue
    .line 315
    instance-of v0, p0, Ljava/util/Collection;

    if-eqz v0, :cond_7

    check-cast p0, Ljava/util/Collection;

    :goto_6
    return-object p0

    :cond_7
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/Iterator;)Ljava/util/ArrayList;

    move-result-object p0

    goto :goto_6
.end method

.method private static h(Ljava/lang/Iterable;)Lcom/google/common/collect/dY;
    .registers 3
    .parameter

    .prologue
    .line 499
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 501
    new-instance v1, Lcom/google/common/collect/aV;

    invoke-direct {v1, v0}, Lcom/google/common/collect/aV;-><init>(Ljava/util/Iterator;)V

    return-object v1
.end method
