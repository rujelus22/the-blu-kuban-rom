.class Lcom/google/common/collect/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/ListIterator;


# instance fields
.field a:I

.field b:Lcom/google/common/collect/bt;

.field c:Lcom/google/common/collect/bt;

.field d:Lcom/google/common/collect/bt;

.field final synthetic e:Lcom/google/common/collect/bj;


# direct methods
.method constructor <init>(Lcom/google/common/collect/bj;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/common/collect/bu;->e:Lcom/google/common/collect/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    invoke-virtual {p1}, Lcom/google/common/collect/bj;->c()I

    move-result v1

    .line 290
    invoke-static {p2, v1}, Lcom/google/common/base/J;->b(II)I

    .line 291
    div-int/lit8 v0, v1, 0x2

    if-lt p2, v0, :cond_21

    .line 292
    invoke-static {p1}, Lcom/google/common/collect/bj;->b(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    .line 293
    iput v1, p0, Lcom/google/common/collect/bu;->a:I

    .line 294
    :goto_18
    add-int/lit8 v0, p2, 0x1

    if-ge p2, v1, :cond_30

    .line 295
    invoke-virtual {p0}, Lcom/google/common/collect/bu;->b()Lcom/google/common/collect/bt;

    move p2, v0

    goto :goto_18

    .line 298
    :cond_21
    invoke-static {p1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;)Lcom/google/common/collect/bt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    .line 299
    :goto_27
    add-int/lit8 v0, p2, -0x1

    if-lez p2, :cond_30

    .line 300
    invoke-virtual {p0}, Lcom/google/common/collect/bu;->a()Lcom/google/common/collect/bt;

    move p2, v0

    goto :goto_27

    .line 303
    :cond_30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    .line 304
    return-void
.end method


# virtual methods
.method public a()Lcom/google/common/collect/bt;
    .registers 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    .line 313
    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    .line 314
    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/common/collect/bu;->a:I

    .line 315
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method public a(Lcom/google/common/collect/bt;)V
    .registers 3
    .parameter

    .prologue
    .line 351
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 359
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iput-object p1, v0, Lcom/google/common/collect/bt;->b:Ljava/lang/Object;

    .line 360
    return-void

    .line 358
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public synthetic add(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 279
    check-cast p1, Lcom/google/common/collect/bt;

    invoke-virtual {p0, p1}, Lcom/google/common/collect/bu;->b(Lcom/google/common/collect/bt;)V

    return-void
.end method

.method public b()Lcom/google/common/collect/bt;
    .registers 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    invoke-static {v0}, Lcom/google/common/collect/bj;->e(Ljava/lang/Object;)V

    .line 336
    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    .line 337
    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    .line 338
    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/bu;->a:I

    .line 339
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    return-object v0
.end method

.method public b(Lcom/google/common/collect/bt;)V
    .registers 3
    .parameter

    .prologue
    .line 355
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public hasNext()Z
    .registers 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public hasPrevious()Z
    .registers 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/common/collect/bu;->a()Lcom/google/common/collect/bt;

    move-result-object v0

    return-object v0
.end method

.method public nextIndex()I
    .registers 2

    .prologue
    .line 343
    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    return v0
.end method

.method public synthetic previous()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/common/collect/bu;->b()Lcom/google/common/collect/bt;

    move-result-object v0

    return-object v0
.end method

.method public previousIndex()I
    .registers 2

    .prologue
    .line 347
    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .registers 3

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    if-eqz v0, :cond_25

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lcom/google/common/base/J;->b(Z)V

    .line 320
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iget-object v1, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    if-eq v0, v1, :cond_27

    .line 321
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->d:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->d:Lcom/google/common/collect/bt;

    .line 322
    iget v0, p0, Lcom/google/common/collect/bu;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/common/collect/bu;->a:I

    .line 326
    :goto_1a
    iget-object v0, p0, Lcom/google/common/collect/bu;->e:Lcom/google/common/collect/bj;

    iget-object v1, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    invoke-static {v0, v1}, Lcom/google/common/collect/bj;->a(Lcom/google/common/collect/bj;Lcom/google/common/collect/bt;)V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    .line 328
    return-void

    .line 319
    :cond_25
    const/4 v0, 0x0

    goto :goto_5

    .line 324
    :cond_27
    iget-object v0, p0, Lcom/google/common/collect/bu;->c:Lcom/google/common/collect/bt;

    iget-object v0, v0, Lcom/google/common/collect/bt;->c:Lcom/google/common/collect/bt;

    iput-object v0, p0, Lcom/google/common/collect/bu;->b:Lcom/google/common/collect/bt;

    goto :goto_1a
.end method

.method public synthetic set(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 279
    check-cast p1, Lcom/google/common/collect/bt;

    invoke-virtual {p0, p1}, Lcom/google/common/collect/bu;->a(Lcom/google/common/collect/bt;)V

    return-void
.end method
