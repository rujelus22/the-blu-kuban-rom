.class public Lcom/google/googlenav/android/M;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/s;


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field private static final l:[Ljava/lang/String;

.field private static final m:Lcom/google/common/collect/ImmutableSet;


# instance fields
.field private final i:Lcom/google/googlenav/android/R;

.field private final j:Lcom/google/googlenav/ui/s;

.field private final k:LaN/u;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 256
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/checkin"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->a:Landroid/net/Uri;

    .line 265
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/auto-checkin"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->b:Landroid/net/Uri;

    .line 275
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/location-history"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->c:Landroid/net/Uri;

    .line 285
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/location-history/enable"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->d:Landroid/net/Uri;

    .line 295
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/list"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->e:Landroid/net/Uri;

    .line 305
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "places"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "places"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->f:Landroid/net/Uri;

    .line 313
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/choose-location"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->g:Landroid/net/Uri;

    .line 323
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "latitude"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/manage-places"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->h:Landroid/net/Uri;

    .line 454
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "client"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dc"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "gl"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hl"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "source"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "tab"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "utm_source"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/android/M;->l:[Ljava/lang/String;

    .line 465
    new-instance v0, Lcom/google/common/collect/aD;

    invoke-direct {v0}, Lcom/google/common/collect/aD;-><init>()V

    sget-object v1, Lcom/google/googlenav/android/M;->l:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/common/collect/aD;->b([Ljava/lang/Object;)Lcom/google/common/collect/aD;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/aD;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/M;->m:Lcom/google/common/collect/ImmutableSet;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/R;Lcom/google/googlenav/ui/s;LaN/u;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 675
    iput-object p1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    .line 676
    iput-object p2, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    .line 677
    iput-object p3, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    .line 678
    return-void
.end method

.method private a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;I)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2094
    .line 2096
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2097
    if-eqz v0, :cond_a

    .line 2098
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_9} :catch_b

    move-result p3

    .line 2104
    :cond_a
    :goto_a
    return p3

    .line 2100
    :catch_b
    move-exception v0

    goto :goto_a
.end method

.method static synthetic a(Lcom/google/googlenav/android/M;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 2399
    .line 2400
    const-string v0, "cid:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2401
    const/4 v0, 0x0

    move v1, v0

    .line 2408
    :goto_b
    if-ne v1, v2, :cond_1a

    .line 2422
    :goto_d
    return-object p0

    .line 2403
    :cond_e
    const-string v0, " cid:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2404
    if-eq v0, v2, :cond_55

    .line 2405
    add-int/lit8 v0, v0, 0x1

    move v1, v0

    goto :goto_b

    .line 2413
    :cond_1a
    const-string v0, "cid"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 2416
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_33

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_33

    .line 2417
    add-int/lit8 v0, v0, 0x1

    .line 2419
    :cond_33
    :goto_33
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_46

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_46

    add-int/lit8 v0, v0, 0x1

    goto :goto_33

    .line 2421
    :cond_46
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2422
    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_d

    :cond_55
    move v1, v0

    goto :goto_b
.end method

.method private a(Landroid/net/Uri;)V
    .registers 14
    .parameter

    .prologue
    const/16 v7, 0x2c

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 1365
    :try_start_5
    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_8} :catch_1d

    move-result-object v0

    .line 1371
    const-string v1, "geo"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_26

    .line 1372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geouri is not geo scheme."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1366
    :catch_1d
    move-exception v0

    .line 1368
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geouri not properly formatted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1375
    :cond_26
    if-nez v0, :cond_30

    .line 1376
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geouri link contains no body."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1380
    :cond_30
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1381
    if-ne v2, v4, :cond_5f

    move-object v1, v0

    .line 1383
    :goto_39
    if-ne v2, v4, :cond_64

    move-object v0, v3

    .line 1386
    :goto_3c
    invoke-static {v1}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1387
    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1388
    if-gez v2, :cond_6b

    .line 1389
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid geo uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1381
    :cond_5f
    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_39

    .line 1383
    :cond_64
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3c

    .line 1391
    :cond_6b
    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v1, v7, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    .line 1392
    if-ltz v5, :cond_77

    .line 1394
    invoke-virtual {v1, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1398
    :cond_77
    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1399
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1400
    const v1, 0xf4240

    .line 1402
    :try_start_84
    invoke-static {v5}, LaN/B;->b(Ljava/lang/String;)I

    move-result v7

    .line 1403
    invoke-static {v2}, LaN/B;->b(Ljava/lang/String;)I

    move-result v8

    .line 1406
    const/16 v2, 0x2e

    invoke-virtual {v5, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 1407
    if-lez v2, :cond_a4

    .line 1408
    const-wide/high16 v9, 0x4024

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v2, v1

    add-int/lit8 v1, v1, 0x7

    int-to-double v1, v1

    invoke-static {v9, v10, v1, v2}, Ljava/lang/Math;->pow(DD)D
    :try_end_a2
    .catch Ljava/lang/NumberFormatException; {:try_start_84 .. :try_end_a2} :catch_ec

    move-result-wide v1

    double-to-int v1, v1

    .line 1417
    :cond_a4
    new-instance v9, Lcom/google/googlenav/android/Q;

    invoke-direct {v9}, Lcom/google/googlenav/android/Q;-><init>()V

    .line 1418
    if-eqz v0, :cond_14e

    .line 1419
    invoke-virtual {v9, v0}, Lcom/google/googlenav/android/Q;->parseQuery(Ljava/lang/String;)V

    .line 1420
    const-string v2, "z"

    invoke-virtual {v9, v2}, Lcom/google/googlenav/android/Q;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1421
    if-eqz v2, :cond_14e

    .line 1424
    :try_start_b6
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v5, 0x16

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1425
    invoke-static {}, LaN/Y;->e()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I
    :try_end_c7
    .catch Ljava/lang/NumberFormatException; {:try_start_b6 .. :try_end_c7} :catch_106

    move-result v2

    .line 1436
    :goto_c8
    if-nez v7, :cond_120

    if-nez v8, :cond_120

    .line 1437
    const/4 v5, 0x1

    .line 1448
    :goto_cd
    if-eqz v0, :cond_146

    .line 1449
    const-string v0, "q"

    invoke-virtual {v9, v0}, Lcom/google/googlenav/android/Q;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1450
    if-eqz v1, :cond_13e

    .line 1452
    invoke-static {v1}, Lcom/google/googlenav/android/M;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14c

    .line 1453
    invoke-static {v1}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v5, v6

    :goto_e2
    move-object v0, p0

    move v7, v6

    move v8, v6

    move-object v9, v3

    move v10, v6

    move-object v11, v3

    .line 1459
    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V

    .line 1474
    :goto_eb
    return-void

    .line 1410
    :catch_ec
    move-exception v0

    .line 1411
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid geo uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1426
    :catch_106
    move-exception v0

    .line 1427
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid geo uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1439
    :cond_120
    iget-object v5, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    new-instance v10, LaN/B;

    invoke-direct {v10, v7, v8}, LaN/B;-><init>(II)V

    invoke-virtual {v5, v10}, LaN/u;->c(LaN/B;)V

    .line 1440
    if-lez v2, :cond_137

    .line 1441
    iget-object v1, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-static {v2}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    invoke-virtual {v1, v2}, LaN/u;->a(LaN/Y;)V

    move v5, v6

    goto :goto_cd

    .line 1443
    :cond_137
    iget-object v2, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v2, v1, v1}, LaN/u;->d(II)V

    move v5, v6

    goto :goto_cd

    .line 1463
    :cond_13e
    const-string v0, "cbp"

    invoke-virtual {v9, v0}, Lcom/google/googlenav/android/Q;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1464
    if-eqz v0, :cond_146

    .line 1473
    :cond_146
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    goto :goto_eb

    :cond_14c
    move-object v2, v3

    goto :goto_e2

    :cond_14e
    move v2, v6

    goto/16 :goto_c8
.end method

.method private a(Ljava/lang/String;LaN/M;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2228
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1, v1, v1}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 2232
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    .line 2234
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/googlenav/bg;->a(LaN/M;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 2240
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;II)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1933
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 1934
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v2, 0x4f2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 1941
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2282
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    .line 2284
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    .line 2285
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZLjava/lang/String;ZLaN/B;LaN/H;ZLjava/lang/String;)V
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2168
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    move-object/from16 v0, p10

    invoke-interface {v1, p1, p2, v0}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 2173
    const-string v1, "19"

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 2174
    const/16 v1, 0x57

    const-string v2, "s"

    invoke-static {v1, v2, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2176
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/gk;->a(Ljava/lang/String;)V

    .line 2224
    :goto_29
    return-void

    .line 2182
    :cond_2a
    const-string v1, "20"

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 2183
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    .line 2184
    if-eqz v1, :cond_4c

    invoke-virtual {v1}, Lbf/i;->av()I

    move-result v2

    if-nez v2, :cond_4c

    .line 2185
    check-cast v1, Lbf/bk;

    .line 2186
    invoke-virtual {v1, p1, p3, p4}, Lbf/bk;->a(Ljava/lang/String;LaN/H;I)V

    goto :goto_29

    .line 2194
    :cond_4c
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->au()V

    .line 2196
    if-eqz p5, :cond_6f

    .line 2197
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v1

    new-instance v5, Lcom/google/googlenav/c;

    const/4 v2, 0x2

    invoke-direct {v5, v2}, Lcom/google/googlenav/c;-><init>(I)V

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p13

    move v6, p4

    move v7, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p9

    invoke-virtual/range {v1 .. v10}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/c;IIZZZ)V

    goto :goto_29

    .line 2201
    :cond_6f
    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->e(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/googlenav/bg;->c(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/googlenav/bg;->d(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    if-eqz p8, :cond_d9

    :goto_97
    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p7

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->b(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p12

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->k(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    .line 2215
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_dc

    .line 2216
    const/16 v2, 0x4f2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 2222
    :cond_ce
    :goto_ce
    iget-object v2, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    goto/16 :goto_29

    .line 2201
    :cond_d9
    const-string p8, "100"

    goto :goto_97

    .line 2218
    :cond_dc
    if-eqz p12, :cond_ce

    .line 2219
    const/16 v2, 0x4f1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    goto :goto_ce
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V
    .registers 26
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2139
    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    move/from16 v12, p10

    move-object/from16 v13, p11

    invoke-direct/range {v0 .. v13}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZLjava/lang/String;ZLaN/B;LaN/H;ZLjava/lang/String;)V

    .line 2141
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 844
    invoke-static {p1}, Lcom/google/googlenav/android/M;->d(Ljava/lang/String;)I

    move-result v0

    .line 846
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    .line 849
    invoke-virtual {v1}, Lbb/o;->i()V

    .line 852
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1b

    .line 853
    const-string v2, "s"

    invoke-virtual {v1, p0, v2, p2}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 855
    invoke-static {p0}, Lau/b;->i(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lbb/o;->a(III)V

    .line 858
    :cond_1b
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2294
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 2300
    const-string v0, "saddr"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v0

    .line 2304
    :goto_12
    const-string v2, "daddr"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4a

    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v1

    .line 2310
    :cond_1e
    :goto_1e
    new-instance v2, Lbm/b;

    invoke-direct {v2, p3}, Lbm/b;-><init>(Ljava/lang/String;)V

    .line 2311
    invoke-virtual {v2}, Lbm/b;->a()Z

    move-result v3

    if-eqz v3, :cond_55

    .line 2312
    const/4 v2, 0x2

    .line 2325
    :goto_2a
    new-instance v3, Lax/j;

    invoke-direct {v3, v0, v1}, Lax/j;-><init>(Lax/y;Lax/y;)V

    .line 2326
    packed-switch v2, :pswitch_data_92

    .line 2342
    new-instance v0, Lax/s;

    invoke-direct {v0, v3}, Lax/s;-><init>(Lax/k;)V

    .line 2350
    :goto_37
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lax/k;)V

    .line 2351
    return-void

    .line 2300
    :cond_3d
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_48

    invoke-static {p1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    goto :goto_12

    :cond_48
    move-object v0, v1

    goto :goto_12

    .line 2304
    :cond_4a
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1e

    invoke-static {p2}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v1

    goto :goto_1e

    .line 2313
    :cond_55
    invoke-virtual {v2}, Lbm/b;->d()Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 2314
    const/4 v2, 0x1

    goto :goto_2a

    .line 2315
    :cond_5d
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v3

    if-eqz v3, :cond_6b

    invoke-virtual {v2}, Lbm/b;->c()Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 2316
    const/4 v2, 0x3

    goto :goto_2a

    .line 2317
    :cond_6b
    invoke-virtual {v2}, Lbm/b;->b()Z

    move-result v2

    if-eqz v2, :cond_73

    .line 2318
    const/4 v2, 0x0

    goto :goto_2a

    .line 2321
    :cond_73
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v2

    goto :goto_2a

    .line 2328
    :pswitch_78
    new-instance v0, Lax/x;

    invoke-direct {v0, v3}, Lax/x;-><init>(Lax/k;)V

    goto :goto_37

    .line 2332
    :pswitch_7e
    new-instance v0, Lax/w;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v1

    invoke-direct {v0, v3, v1}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    goto :goto_37

    .line 2337
    :pswitch_8c
    new-instance v0, Lax/i;

    invoke-direct {v0, v3}, Lax/i;-><init>(Lax/k;)V

    goto :goto_37

    .line 2326
    :pswitch_data_92
    .packed-switch 0x1
        :pswitch_7e
        :pswitch_78
        :pswitch_8c
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 2249
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lcom/google/googlenav/android/R;->saveQueryToHistory(Ljava/lang/String;Ljava/lang/String;LaN/B;)V

    .line 2253
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->au()V

    .line 2256
    if-eqz p3, :cond_1e

    .line 2257
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0xf

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 2261
    :cond_1e
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    .line 2268
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_50

    .line 2269
    const/16 v1, 0x4f2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 2272
    :cond_50
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 2273
    return-void
.end method

.method private a(ZLjava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2380
    if-nez p1, :cond_23

    .line 2389
    if-eqz p2, :cond_14

    const-string v0, "19"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    const-string v0, "20"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 2392
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 2396
    :cond_23
    return-void
.end method

.method private a(Landroid/net/UrlQuerySanitizer;)Z
    .registers 5
    .parameter

    .prologue
    .line 2432
    invoke-virtual {p1}, Landroid/net/UrlQuerySanitizer;->getParameterSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2433
    sget-object v2, Lcom/google/googlenav/android/M;->m:Lcom/google/common/collect/ImmutableSet;

    invoke-virtual {v2, v0}, Lcom/google/common/collect/ImmutableSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2434
    const/4 v0, 0x1

    .line 2437
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2020
    :try_start_1
    invoke-virtual {p1, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2021
    if-nez v2, :cond_8

    .line 2036
    :goto_7
    return-object v0

    .line 2024
    :cond_8
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 2025
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 2026
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LaN/B;->b(Ljava/lang/String;)I

    move-result v5

    aput v5, v1, v4

    .line 2027
    const/4 v4, 0x1

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LaN/B;->b(Ljava/lang/String;)I

    move-result v2

    aput v2, v1, v4
    :try_end_2a
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_2a} :catch_2e
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_2a} :catch_2c

    move-object v0, v1

    .line 2028
    goto :goto_7

    .line 2032
    :catch_2c
    move-exception v1

    goto :goto_7

    .line 2029
    :catch_2e
    move-exception v1

    goto :goto_7
.end method

.method private b(Landroid/net/Uri;)I
    .registers 8
    .parameter

    .prologue
    .line 1836
    new-instance v2, Law/r;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, p0}, Law/r;-><init>(Ljava/lang/String;Law/s;)V

    .line 1837
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 1842
    invoke-virtual {v2}, Law/r;->a()V

    .line 1843
    const/4 v0, -0x2

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/android/M;)Lcom/google/googlenav/android/R;
    .registers 2
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    return-object v0
.end method

.method static b(Landroid/content/Intent;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 1311
    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1312
    const-string v0, "source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1314
    :goto_e
    return-object v0

    :cond_f
    const-string v0, "cu"

    goto :goto_e
.end method

.method private b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2115
    invoke-virtual {p1, p2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2117
    if-eqz v0, :cond_1c

    .line 2118
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 2119
    array-length v1, v0

    const/4 v2, 0x3

    if-ne v1, v2, :cond_14

    .line 2120
    const/4 v1, 0x2

    aget-object v0, v0, v1

    .line 2130
    :goto_13
    return-object v0

    .line 2121
    :cond_14
    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1c

    .line 2123
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_13

    .line 2130
    :cond_1c
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2466
    if-nez p0, :cond_4

    .line 2474
    :cond_3
    :goto_3
    return-object v0

    .line 2469
    :cond_4
    const-string v1, ";"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2470
    const/4 v2, -0x1

    if-ne v1, v2, :cond_f

    move-object v0, p0

    .line 2471
    goto :goto_3

    .line 2473
    :cond_f
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2474
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object v0, v1

    goto :goto_3
.end method

.method private b()V
    .registers 5

    .prologue
    .line 1879
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 1880
    monitor-enter v1

    .line 1882
    const/4 v0, 0x0

    .line 1883
    :try_start_7
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/android/P;

    invoke-direct {v3, p0, v1}, Lcom/google/googlenav/android/P;-><init>(Lcom/google/googlenav/android/M;Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_15
    .catchall {:try_start_7 .. :try_end_15} :catchall_2c

    .line 1893
    :goto_15
    :try_start_15
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1894
    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_28
    .catchall {:try_start_15 .. :try_end_28} :catchall_2c
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_28} :catch_29

    goto :goto_15

    .line 1896
    :catch_29
    move-exception v0

    .line 1899
    :cond_2a
    :try_start_2a
    monitor-exit v1

    .line 1900
    return-void

    .line 1899
    :catchall_2c
    move-exception v0

    monitor-exit v1
    :try_end_2e
    .catchall {:try_start_2a .. :try_end_2e} :catchall_2c

    throw v0
.end method

.method public static c(Ljava/lang/String;)LaN/B;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 2485
    if-nez p0, :cond_5

    .line 2501
    :cond_4
    :goto_4
    return-object v0

    .line 2488
    :cond_5
    const-string v1, ";"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2489
    if-eq v1, v3, :cond_4

    .line 2492
    sget-object v0, Lbb/w;->a:Ljava/lang/String;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 2495
    if-ne v0, v3, :cond_27

    .line 2496
    const-string v0, ";"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 2501
    :goto_22
    invoke-static {v0}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v0

    goto :goto_4

    .line 2498
    :cond_27
    const-string v2, ";"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_22
.end method

.method private c()Landroid/net/UrlQuerySanitizer;
    .registers 2

    .prologue
    .line 2004
    new-instance v0, Lcom/google/googlenav/android/U;

    invoke-direct {v0}, Lcom/google/googlenav/android/U;-><init>()V

    return-object v0
.end method

.method private static c(Landroid/content/Intent;)Lcom/google/googlenav/ai;
    .registers 4
    .parameter

    .prologue
    .line 744
    const-string v0, "placemark-proto"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 745
    if-eqz v0, :cond_22

    .line 746
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 748
    :try_start_12
    invoke-static {v1}, Lcom/google/googlenav/ai;->a(Ljava/io/DataInput;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 749
    const-string v1, "insitu"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->e(Z)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_20} :catch_21

    .line 755
    :goto_20
    return-object v0

    .line 751
    :catch_21
    move-exception v0

    .line 755
    :cond_22
    const/4 v0, 0x0

    goto :goto_20
.end method

.method private c(Landroid/net/Uri;)V
    .registers 6
    .parameter

    .prologue
    .line 1906
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 1907
    const-string v1, "output"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1908
    if-eqz v1, :cond_2d

    .line 1909
    const-string v2, "kml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_34

    .line 1910
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load My Maps with output="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1914
    :cond_2d
    const-string v1, "output"

    const-string v2, "kml"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1917
    :cond_34
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1918
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x9

    const/4 v3, 0x3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;II)V

    .line 1921
    return-void
.end method

.method public static d(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 2510
    if-nez p0, :cond_4

    .line 2517
    :cond_3
    :goto_3
    return v0

    .line 2513
    :cond_4
    sget-object v1, Lbb/w;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2514
    if-eq v1, v0, :cond_3

    .line 2517
    new-instance v0, Ljava/lang/Integer;

    sget-object v2, Lbb/w;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3
.end method

.method private static d(Landroid/content/Intent;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 759
    const-string v0, "placemark-url"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/net/Uri;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1962
    .line 1964
    :try_start_3
    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_6} :catch_51

    move-result-object v0

    .line 1971
    if-eqz v0, :cond_5d

    .line 1972
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1973
    array-length v0, v4

    if-lt v0, v9, :cond_5d

    .line 1974
    aget-object v0, v4, v3

    invoke-static {v0}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1975
    aget-object v0, v4, v2

    invoke-static {v0}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1976
    const/4 v1, 0x0

    .line 1978
    const/4 v0, 0x2

    :try_start_20
    aget-object v0, v4, v0

    invoke-static {v0}, LaN/B;->b(Ljava/lang/String;)I

    move-result v7

    .line 1979
    const/4 v0, 0x3

    aget-object v0, v4, v0

    invoke-static {v0}, LaN/B;->b(Ljava/lang/String;)I

    move-result v8

    .line 1980
    new-instance v0, LaN/B;

    invoke-direct {v0, v7, v8}, LaN/B;-><init>(II)V
    :try_end_32
    .catch Ljava/lang/NumberFormatException; {:try_start_20 .. :try_end_32} :catch_5a

    .line 1984
    :goto_32
    const-string v1, ""

    .line 1985
    array-length v7, v4

    const/4 v8, 0x5

    if-lt v7, v8, :cond_3e

    .line 1987
    aget-object v1, v4, v9

    invoke-static {v1}, Lcom/google/googlenav/android/ac;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1989
    :cond_3e
    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5d

    invoke-static {v6}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5d

    if-eqz v0, :cond_5d

    .line 1991
    invoke-direct {p0, v5, v6, v0, v1}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    move v0, v2

    .line 1996
    :goto_50
    return v0

    .line 1965
    :catch_51
    move-exception v0

    .line 1967
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "google.layeritemdetails uri not properly formatted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1981
    :catch_5a
    move-exception v0

    move-object v0, v1

    goto :goto_32

    :cond_5d
    move v0, v3

    .line 1996
    goto :goto_50
.end method

.method private static e(Landroid/content/Intent;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    const/16 v2, 0xa

    .line 763
    const-string v0, "intent-source"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 764
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_15

    .line 767
    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 769
    :cond_15
    return-object v0
.end method

.method private e(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1174
    const/16 v0, 0x35c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1176
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/android/O;

    invoke-direct {v2, p0}, Lcom/google/googlenav/android/O;-><init>(Lcom/google/googlenav/android/M;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1186
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1187
    return-void
.end method

.method private f(Landroid/content/Intent;)I
    .registers 8
    .parameter

    .prologue
    .line 773
    const-string v0, "intent_extra_data_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/M;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 774
    if-nez v2, :cond_e

    .line 776
    const/4 v0, -0x1

    .line 813
    :goto_d
    return v0

    .line 779
    :cond_e
    const/4 v1, 0x0

    .line 780
    const/4 v0, 0x0

    .line 781
    const-string v3, "app_data"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 782
    if-eqz v3, :cond_38

    .line 783
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 784
    const-string v5, "skipSearchWizardOnBack"

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2a

    .line 785
    const-string v0, "skipSearchWizardOnBack"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 788
    :cond_2a
    const-string v5, "searchUiSource"

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_38

    .line 789
    const-string v1, "searchUiSource"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 794
    :cond_38
    const-string v3, "19"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_67

    .line 795
    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/wizard/gk;->b(Ljava/lang/String;)V

    .line 807
    :goto_51
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/android/M;->a(ZLjava/lang/String;)V

    .line 809
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "intent_extra_data_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 813
    const/4 v0, -0x2

    goto :goto_d

    .line 796
    :cond_67
    const-string v3, "20"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_90

    .line 797
    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v3

    invoke-virtual {v3}, Lbf/am;->H()Lbf/i;

    move-result-object v3

    .line 798
    if-eqz v3, :cond_8a

    invoke-virtual {v3}, Lbf/i;->av()I

    move-result v4

    if-nez v4, :cond_8a

    .line 799
    iget-object v4, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v4

    invoke-virtual {v4, v3}, Lbf/am;->h(Lbf/i;)V

    .line 801
    :cond_8a
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v2}, Lcom/google/googlenav/android/R;->showStarDetails(Ljava/lang/String;)V

    goto :goto_51

    .line 803
    :cond_90
    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->au()V

    .line 804
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v2}, Lcom/google/googlenav/android/R;->showStarOnMap(Ljava/lang/String;)V

    goto :goto_51
.end method

.method private static f(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2361
    const-string v1, "cid:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2364
    :cond_9
    :goto_9
    return v0

    :cond_a
    const-string v1, " cid:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_9

    const/4 v0, 0x0

    goto :goto_9
.end method

.method private g(Landroid/content/Intent;)I
    .registers 5
    .parameter

    .prologue
    .line 817
    const-string v0, "intent_extra_data_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/M;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 818
    if-nez v0, :cond_e

    .line 820
    const/4 v0, -0x1

    .line 829
    :goto_d
    return v0

    .line 823
    :cond_e
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1, v0}, Lcom/google/googlenav/android/R;->showBubbleForRecentPlace(Ljava/lang/String;)Lcom/google/googlenav/ag;

    move-result-object v0

    .line 825
    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "intent_extra_data_key"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 829
    const/4 v0, -0x2

    goto :goto_d
.end method

.method private static g(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2372
    const-string v1, "by:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2375
    :cond_9
    :goto_9
    return v0

    :cond_a
    const-string v1, " by:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_9

    const/4 v0, 0x0

    goto :goto_9
.end method

.method private h(Landroid/content/Intent;)I
    .registers 5
    .parameter

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const-string v1, "stars"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Z)V

    .line 835
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "intent_extra_data_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 839
    const/4 v0, -0x2

    return v0
.end method

.method private i(Landroid/content/Intent;)I
    .registers 20
    .parameter

    .prologue
    .line 865
    const-string v1, "google.star"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 866
    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->f(Landroid/content/Intent;)I

    move-result v1

    .line 983
    :goto_10
    return v1

    .line 869
    :cond_11
    const-string v1, "google.recentplace"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 870
    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->g(Landroid/content/Intent;)I

    move-result v1

    goto :goto_10

    .line 873
    :cond_22
    const-string v1, "google.myplaces_panel"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 874
    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->h(Landroid/content/Intent;)I

    move-result v1

    goto :goto_10

    .line 877
    :cond_33
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-nez v1, :cond_3e

    .line 878
    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/android/M;->j(Landroid/content/Intent;)I

    move-result v1

    goto :goto_10

    .line 881
    :cond_3e
    const-string v1, "query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 882
    const-string v2, "intent_extra_data_key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 883
    invoke-static/range {v16 .. v16}, Lcom/google/googlenav/android/M;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 884
    invoke-static/range {v16 .. v16}, Lcom/google/googlenav/android/M;->c(Ljava/lang/String;)LaN/B;

    move-result-object v11

    .line 885
    if-nez v1, :cond_cc

    const-string v2, ""

    .line 892
    :goto_5a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->t()Z

    move-result v8

    .line 897
    const-string v1, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7a

    const-string v1, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_d4

    :cond_7a
    const/4 v1, 0x1

    .line 899
    :goto_7b
    const/4 v4, 0x0

    .line 900
    const/4 v5, 0x0

    .line 901
    const/4 v6, 0x0

    .line 902
    const/4 v10, 0x0

    .line 903
    const/4 v7, 0x0

    .line 905
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/googlenav/K;->an()Z

    move-result v9

    if-eqz v9, :cond_d6

    .line 912
    const/4 v1, 0x0

    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    .line 966
    :goto_90
    const/4 v12, 0x0

    .line 967
    if-eqz v11, :cond_aa

    .line 968
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v5

    invoke-virtual {v5}, LaN/u;->f()LaN/H;

    move-result-object v12

    .line 970
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->t()LaN/u;

    move-result-object v5

    invoke-virtual {v5, v11}, LaN/u;->c(LaN/B;)V

    .line 973
    :cond_aa
    if-nez v1, :cond_1c9

    .line 974
    const/4 v5, -0x1

    const/4 v7, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v14}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZLjava/lang/String;ZLaN/B;LaN/H;ZLjava/lang/String;)V

    .line 980
    :goto_b5
    sget-object v1, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    move-object/from16 v0, v16

    invoke-static {v2, v0, v1}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 981
    move-object/from16 v0, p0

    invoke-direct {v0, v15, v9}, Lcom/google/googlenav/android/M;->a(ZLjava/lang/String;)V

    .line 982
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    .line 983
    const/16 v1, 0x15

    goto/16 :goto_10

    .line 885
    :cond_cc
    const/4 v2, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v1, v2, v4, v5}, Lau/b;->a(Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v2

    goto :goto_5a

    .line 897
    :cond_d4
    const/4 v1, 0x0

    goto :goto_7b

    .line 913
    :cond_d6
    if-nez v1, :cond_1d9

    .line 914
    const-string v9, "app_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    .line 915
    invoke-virtual {v9}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v12

    .line 916
    const-string v13, "centerLatitude"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_110

    const-string v13, "centerLongitude"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_110

    .line 918
    const-string v13, "centerLatitude"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 919
    const-string v14, "centerLongitude"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 920
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    new-instance v17, LaN/B;

    move-object/from16 v0, v17

    invoke-direct {v0, v13, v14}, LaN/B;-><init>(II)V

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, LaN/u;->c(LaN/B;)V

    .line 924
    :cond_110
    const-string v13, "zoomLevel"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1a4

    .line 925
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    const-string v14, "zoomLevel"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-static {v14}, LaN/Y;->b(I)LaN/Y;

    move-result-object v14

    invoke-virtual {v13, v14}, LaN/u;->a(LaN/Y;)V

    .line 934
    :cond_129
    :goto_129
    const-string v13, "routePolyline"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_154

    .line 937
    :try_start_131
    new-instance v13, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v14, Lcom/google/wireless/googlenav/proto/j2me/dp;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 938
    const-string v14, "routePolyline"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v14

    .line 939
    if-eqz v14, :cond_154

    .line 940
    invoke-virtual {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 941
    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_154

    .line 942
    const/4 v14, 0x3

    invoke-virtual {v13, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v13

    invoke-static {v13}, LaN/M;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/M;
    :try_end_153
    .catch Ljava/io/IOException; {:try_start_131 .. :try_end_153} :catch_1d0

    move-result-object v4

    .line 949
    :cond_154
    :goto_154
    const-string v13, "skipSearchWizardOnBack"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_162

    .line 950
    const-string v5, "skipSearchWizardOnBack"

    invoke-virtual {v9, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 953
    :cond_162
    const-string v13, "searchUiSource"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_170

    .line 954
    const-string v6, "searchUiSource"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 956
    :cond_170
    const-string v13, "searchNearBy"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_18f

    .line 957
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v7}, LaN/u;->f()LaN/H;

    move-result-object v7

    const-string v8, "searchNearBy"

    invoke-virtual {v9, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LaN/B;->a(Ljava/lang/String;)LaN/B;

    move-result-object v8

    invoke-virtual {v7, v8}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v7

    .line 959
    const/4 v8, 0x1

    .line 961
    :cond_18f
    const-string v13, "searchIncludeInHistory"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1d2

    .line 962
    const-string v10, "searchIncludeInHistory"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    goto/16 :goto_90

    .line 926
    :cond_1a4
    const-string v13, "latitudeSpan"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_129

    const-string v13, "longitudeSpan"

    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_129

    .line 928
    const-string v13, "latitudeSpan"

    invoke-virtual {v9, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 929
    const-string v14, "longitudeSpan"

    invoke-virtual {v9, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 930
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v15, v13, v14}, LaN/u;->d(II)V

    goto/16 :goto_129

    .line 977
    :cond_1c9
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v1, v10}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;LaN/M;Z)V

    goto/16 :goto_b5

    .line 945
    :catch_1d0
    move-exception v13

    goto :goto_154

    :cond_1d2
    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    goto/16 :goto_90

    :cond_1d9
    move-object v9, v6

    move v15, v5

    move v6, v1

    move-object v1, v4

    move-object v4, v7

    goto/16 :goto_90
.end method

.method private j(Landroid/content/Intent;)I
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 990
    :try_start_1
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbb/o;->c(I)Lbb/w;
    :try_end_9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_9} :catch_4d

    move-result-object v0

    .line 995
    :goto_a
    if-eqz v0, :cond_36

    .line 996
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v1

    .line 997
    invoke-virtual {v0}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v2

    .line 1000
    invoke-virtual {v1}, Lbb/o;->i()V

    .line 1001
    invoke-static {v2}, Lau/b;->i(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3, v4, v4}, Lbb/o;->a(III)V

    .line 1003
    const-string v3, "s"

    sget-object v4, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    invoke-virtual {v1, v2, v3, v4}, Lbb/o;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ag;)V

    .line 1006
    invoke-virtual {v0}, Lbb/w;->e()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_36

    .line 1009
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbf/am;->a(Lbb/w;)V

    .line 1012
    :cond_36
    if-eqz v0, :cond_3f

    invoke-virtual {v0}, Lbb/w;->e()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4a

    .line 1013
    :cond_3f
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x1b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 1016
    :cond_4a
    const/16 v0, 0x15

    return v0

    .line 991
    :catch_4d
    move-exception v0

    .line 992
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private k(Landroid/content/Intent;)I
    .registers 15
    .parameter

    .prologue
    const/4 v12, -0x1

    const/4 v1, -0x2

    .line 1021
    :try_start_2
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->resolveType(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1023
    const-string v2, "vnd.android.cursor.item/postal-address"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    const-string v2, "vnd.android.cursor.item/postal-address_v2"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 1025
    :cond_18
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->getPostalAddress(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1028
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V

    .line 1030
    const/16 v12, 0x12

    .line 1104
    :cond_32
    :goto_32
    return v12

    .line 1031
    :cond_33
    const-string v2, "geo"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_49

    .line 1032
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/Uri;)V

    .line 1033
    const/16 v12, 0x14

    goto :goto_32

    .line 1034
    :cond_49
    const-string v2, "http"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_61

    const-string v2, "https"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_82

    .line 1037
    :cond_61
    const-string v2, "application/vnd.google-earth.kml+xml"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 1038
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1039
    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-direct {p0, v0, v0, v2, v3}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;II)V

    move v0, v1

    .line 1045
    :goto_77
    if-ne v0, v1, :cond_7b

    .line 1046
    const/16 v0, 0x13

    :cond_7b
    move v12, v0

    .line 1048
    goto :goto_32

    .line 1043
    :cond_7d
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->o(Landroid/content/Intent;)I

    move-result v0

    goto :goto_77

    .line 1049
    :cond_82
    const-string v0, "rideabout"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 1050
    const/16 v0, 0x61

    const-string v1, "n"

    const-string v2, "n"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1053
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->m(Landroid/content/Intent;)I

    move-result v12

    goto :goto_32

    .line 1054
    :cond_9c
    const-string v0, "latitude"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 1055
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->n(Landroid/content/Intent;)I

    move-result v12

    goto :goto_32

    .line 1056
    :cond_ad
    const-string v0, "google.layeritemdetails"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ca

    .line 1057
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->d(Landroid/net/Uri;)Z

    move-result v0

    .line 1058
    if-eqz v0, :cond_c8

    const/16 v0, 0x18

    :goto_c5
    move v12, v0

    goto/16 :goto_32

    :cond_c8
    move v0, v12

    goto :goto_c5

    .line 1061
    :cond_ca
    const-string v0, "google.businessdetails"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_103

    .line 1062
    invoke-static {p1}, Lcom/google/googlenav/android/M;->c(Landroid/content/Intent;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1063
    if-eqz v0, :cond_e9

    .line 1064
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1065
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v0}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Lcom/google/googlenav/ai;)V

    move v12, v1

    .line 1066
    goto/16 :goto_32

    .line 1068
    :cond_e9
    invoke-static {p1}, Lcom/google/googlenav/android/M;->d(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1069
    if-eqz v0, :cond_32

    .line 1070
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1071
    const-string v2, "insitu"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 1072
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    move v12, v1

    .line 1073
    goto/16 :goto_32

    .line 1078
    :cond_103
    const-string v0, "google.businessratings"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_140

    .line 1079
    invoke-static {p1}, Lcom/google/googlenav/android/M;->c(Landroid/content/Intent;)Lcom/google/googlenav/ai;

    move-result-object v0

    .line 1080
    invoke-static {p1}, Lcom/google/googlenav/android/M;->e(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 1081
    if-eqz v0, :cond_126

    .line 1082
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1083
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3, v0, v2}, Lcom/google/googlenav/android/R;->startBusinessRatings(Lcom/google/googlenav/ai;Ljava/lang/String;)V

    move v12, v1

    .line 1084
    goto/16 :goto_32

    .line 1086
    :cond_126
    invoke-static {p1}, Lcom/google/googlenav/android/M;->d(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 1087
    if-eqz v0, :cond_32

    .line 1088
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v3}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1089
    const-string v3, "insitu"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 1090
    iget-object v4, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v4, v0, v2, v3}, Lcom/google/googlenav/android/R;->startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V

    move v12, v1

    .line 1091
    goto/16 :goto_32

    .line 1096
    :cond_140
    const-string v0, "google.star"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 1097
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/M;->f(Landroid/content/Intent;)I
    :try_end_14f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_14f} :catch_152

    move-result v12

    goto/16 :goto_32

    .line 1101
    :catch_152
    move-exception v0

    goto/16 :goto_32
.end method

.method private l(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 1190
    const/16 v0, 0x34

    const-string v1, "x"

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1193
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startNextMatchingActivity(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1195
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x6d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 1197
    :cond_1e
    return-void
.end method

.method private m(Landroid/content/Intent;)I
    .registers 3
    .parameter

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitNavigationLayer()V

    .line 1203
    const/16 v0, 0x1b

    return v0
.end method

.method private n(Landroid/content/Intent;)I
    .registers 8
    .parameter

    .prologue
    const-wide/16 v4, -0x1

    .line 1213
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1215
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    .line 1216
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 1217
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1219
    const-string v3, "/list"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    .line 1220
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    .line 1259
    :goto_22
    const-string v0, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_SIGN_IN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_36

    const-string v0, "com.google.googlenav.appwidget.friends.FriendsAppWidgetUpdateService.EXTRA_SIGN_IN"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 1261
    invoke-static {}, Lcom/google/googlenav/friend/ad;->o()V

    .line 1263
    :cond_36
    const/16 v0, 0x1a

    return v0

    .line 1221
    :cond_39
    const-string v3, "/map"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_47

    .line 1222
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsLayer(Lbf/am;)V

    goto :goto_22

    .line 1223
    :cond_47
    const-string v3, "/friends/location"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_59

    .line 1224
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1225
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startFriendsLocation(Lbf/am;Ljava/lang/String;)V

    goto :goto_22

    .line 1226
    :cond_59
    const-string v3, "/friends/profile"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 1227
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 1228
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startFriendsProfile(Lbf/am;Ljava/lang/String;)V

    goto :goto_22

    .line 1229
    :cond_6b
    const-string v0, "/auto-checkin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_98

    .line 1230
    const-string v0, "notification_fired"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1231
    cmp-long v0, v2, v4

    if-eqz v0, :cond_92

    .line 1232
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 1234
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->f(Ljava/lang/String;)V

    .line 1237
    :cond_92
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    goto :goto_22

    .line 1238
    :cond_98
    const-string v0, "/checkin"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 1239
    invoke-virtual {p0, p1}, Lcom/google/googlenav/android/M;->a(Landroid/content/Intent;)V

    goto/16 :goto_22

    .line 1240
    :cond_a5
    const-string v0, "/choose-location"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c0

    .line 1241
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1243
    const-string v2, "start_activity_on_complete"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 1246
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startFriendsLocationChooser(Lbf/am;Ljava/lang/Class;)V

    goto/16 :goto_22

    .line 1247
    :cond_c0
    const-string v0, "/location-history/enable"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cf

    .line 1248
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, p1}, Lcom/google/googlenav/android/R;->startLocationHistoryOptIn(Landroid/content/Intent;)V

    goto/16 :goto_22

    .line 1249
    :cond_cf
    const-string v0, "/location-history"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_de

    .line 1250
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startFriendsLayerHistorySummary()V

    goto/16 :goto_22

    .line 1251
    :cond_de
    const-string v0, "/manage-places"

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 1252
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startManageAutoCheckinPlaces()V

    goto/16 :goto_22

    .line 1254
    :cond_ed
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startFriendsLayer(Lbf/am;)V

    goto/16 :goto_22
.end method

.method private o(Landroid/content/Intent;)I
    .registers 15
    .parameter

    .prologue
    .line 1487
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 1490
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1b

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/u/m/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1491
    invoke-direct {p0, v4}, Lcom/google/googlenav/android/M;->b(Landroid/net/Uri;)I

    move-result v0

    .line 1798
    :goto_1a
    return v0

    .line 1497
    :cond_1b
    const/4 v1, 0x0

    .line 1501
    const/4 v2, 0x0

    .line 1505
    :try_start_1d
    invoke-virtual {v4}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;
    :try_end_20
    .catch Ljava/lang/NullPointerException; {:try_start_1d .. :try_end_20} :catch_a5

    move-result-object v0

    .line 1510
    if-nez v0, :cond_25

    .line 1511
    const-string v0, ""

    .line 1515
    :cond_25
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/latitude"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_ae

    .line 1518
    const/4 v1, 0x1

    .line 1519
    iget-object v3, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    iget-object v5, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/google/googlenav/android/R;->startFriendsListView(Lbf/am;)V

    .line 1537
    :cond_3d
    invoke-direct {p0}, Lcom/google/googlenav/android/M;->c()Landroid/net/UrlQuerySanitizer;

    move-result-object v7

    .line 1542
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->setAllowUnregisteredParamaters(Z)V

    .line 1543
    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    .line 1544
    invoke-direct {p0, v7}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;)Z

    move-result v3

    if-nez v3, :cond_4f

    .line 1545
    const/4 v2, 0x1

    .line 1549
    :cond_4f
    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->setAllowUnregisteredParamaters(Z)V

    .line 1550
    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    .line 1552
    const-string v0, "lci"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    .line 1555
    const-string v0, "cbll"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1556
    if-eqz v0, :cond_ff

    .line 1558
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1559
    const-string v2, "google.streetview:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1560
    const-string v2, "cbll="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1561
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1562
    const-string v0, "cbp"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1563
    if-eqz v0, :cond_85

    .line 1564
    const-string v2, "&cbp="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1565
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1567
    :cond_85
    const-string v0, "panoid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1568
    if-eqz v0, :cond_95

    .line 1569
    const-string v2, "&panoid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1570
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1573
    :cond_95
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->c(Ljava/lang/String;)V

    .line 1574
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1506
    :catch_a5
    move-exception v0

    .line 1508
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Uri not properly formatted."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1520
    :cond_ae
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/my-places"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d7

    .line 1521
    const-string v0, "offline"

    invoke-virtual {v4}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d0

    .line 1524
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const-string v1, "offline"

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startMyPlacesList(Ljava/lang/String;)V

    .line 1528
    :goto_cd
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1526
    :cond_d0
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startMyPlacesList(Ljava/lang/String;)V

    goto :goto_cd

    .line 1529
    :cond_d7
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/offers-list"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_eb

    .line 1530
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startOffersList()V

    .line 1531
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1532
    :cond_eb
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "/transit-entry"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 1533
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->startTransitEntry()V

    .line 1534
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1578
    :cond_ff
    const-string v0, "nav"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1579
    if-eqz v0, :cond_158

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_158

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ak()Z

    move-result v0

    if-eqz v0, :cond_158

    .line 1582
    const-string v0, "daddr"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1586
    const-string v0, "sll"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v0

    .line 1587
    if-eqz v0, :cond_22f

    const-string v0, "sll"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1588
    :goto_12d
    const-string v3, "sspn"

    invoke-direct {p0, v7, v3}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v3

    .line 1589
    if-eqz v3, :cond_232

    const-string v3, "sspn"

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1592
    :goto_13b
    const-string v6, "dirflg"

    invoke-virtual {v7, v6}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1595
    const-string v8, "entry"

    invoke-virtual {v7, v8}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1597
    invoke-static {v5, v0, v3, v6, v8}, Lcom/google/android/maps/driveabout/app/bn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1600
    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;)V

    .line 1605
    :cond_158
    const-string v0, "t"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1606
    const-string v3, "k"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16e

    const-string v3, "h"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_235

    .line 1607
    :cond_16e
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(I)Z

    move-result v1

    .line 1618
    :cond_175
    :goto_175
    const-string v0, "layer"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1619
    if-eqz v0, :cond_18d

    .line 1620
    const/16 v3, 0x74

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_18d

    .line 1621
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    .line 1622
    const/4 v1, 0x1

    .line 1627
    :cond_18d
    const-string v0, "q"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1628
    if-eqz v6, :cond_25b

    .line 1629
    const/4 v3, 0x0

    .line 1630
    const-string v0, "sll"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v8

    .line 1631
    if-eqz v8, :cond_1c7

    .line 1632
    new-instance v0, LaN/H;

    new-instance v1, LaN/B;

    const/4 v2, 0x0

    aget v2, v8, v2

    const/4 v3, 0x1

    aget v3, v8, v3

    invoke-direct {v1, v2, v3}, LaN/B;-><init>(II)V

    const/16 v2, 0xa

    invoke-static {v2}, LaN/Y;->b(I)LaN/Y;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->b()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->c()Z

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->P()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    move-object v3, v0

    .line 1637
    :cond_1c7
    const/4 v4, -0x1

    .line 1638
    const-string v0, "sspn"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v0

    .line 1639
    if-eqz v0, :cond_1d3

    .line 1641
    const/4 v1, 0x0

    aget v4, v0, v1

    .line 1644
    :cond_1d3
    const/4 v0, 0x0

    .line 1648
    invoke-static {v6}, Lcom/google/googlenav/android/M;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_246

    .line 1649
    const-string v1, "cid"

    invoke-direct {p0, v7, v1}, Lcom/google/googlenav/android/M;->b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1650
    if-nez v1, :cond_1e8

    .line 1651
    const-string v1, "latlng"

    invoke-direct {p0, v7, v1}, Lcom/google/googlenav/android/M;->b(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1653
    :cond_1e8
    if-eqz v1, :cond_204

    .line 1655
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " cid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v12, v6

    move-object v6, v0

    move-object v0, v12

    :cond_204
    move-object v2, v0

    move-object v1, v6

    .line 1663
    :goto_206
    const/4 v6, 0x0

    .line 1664
    const-string v0, "iwd"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1665
    const-string v5, "1"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_216

    .line 1666
    const/4 v6, 0x1

    .line 1671
    :cond_216
    invoke-static {v1}, Lcom/google/googlenav/android/M;->g(Ljava/lang/String;)Z

    move-result v10

    .line 1672
    const-string v0, "hnear"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1678
    if-nez v8, :cond_24c

    invoke-static {v1}, Lcom/google/googlenav/android/M;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24c

    .line 1679
    const/4 v0, 0x1

    invoke-direct {p0, v1, v2, v6, v0}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1685
    :goto_22c
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1587
    :cond_22f
    const/4 v0, 0x0

    goto/16 :goto_12d

    .line 1589
    :cond_232
    const/4 v3, 0x0

    goto/16 :goto_13b

    .line 1608
    :cond_235
    const-string v3, "m"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_175

    .line 1609
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(I)Z

    move-result v1

    goto/16 :goto_175

    .line 1658
    :cond_246
    invoke-static {v6}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v6

    goto :goto_206

    .line 1681
    :cond_24c
    if-nez v8, :cond_259

    if-nez v10, :cond_259

    const/4 v5, 0x1

    .line 1682
    :goto_251
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;LaN/H;IZZZZLaN/B;ZLjava/lang/String;)V

    goto :goto_22c

    .line 1681
    :cond_259
    const/4 v5, 0x0

    goto :goto_251

    .line 1689
    :cond_25b
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v3, "/maps/place"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2cb

    .line 1690
    const-string v0, "cid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1691
    const-string v3, "georestrict"

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1692
    invoke-static {v0, v3}, Lcom/google/googlenav/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1695
    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b2

    .line 1696
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1697
    const-string v0, "uc"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1698
    const/4 v0, 0x0

    .line 1699
    const-string v2, "insitu"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_292

    .line 1700
    const/4 v0, 0x1

    .line 1702
    :cond_292
    const-string v1, "openratings"

    const-string v2, "action"

    invoke-virtual {v7, v2}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2ac

    .line 1703
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-static {p1}, Lcom/google/googlenav/android/M;->e(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v2, v0}, Lcom/google/googlenav/android/R;->startBusinessRatings(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1707
    :goto_2a9
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1705
    :cond_2ac
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1, v3, v0}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    goto :goto_2a9

    .line 1709
    :cond_2b2
    const-string v0, "ftid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1710
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2cb

    .line 1711
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1712
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1, v0}, Lcom/google/googlenav/android/R;->startTransitStationPage(Ljava/lang/String;)V

    .line 1713
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1718
    :cond_2cb
    const-string v0, "saddr"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1719
    const-string v3, "daddr"

    invoke-virtual {v7, v3}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1720
    const-string v5, "dirflg"

    invoke-virtual {v7, v5}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1721
    const-string v6, "myl"

    invoke-virtual {v7, v6}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1722
    if-nez v0, :cond_2e7

    if-eqz v3, :cond_2ed

    .line 1724
    :cond_2e7
    invoke-direct {p0, v0, v3, v5, v6}, Lcom/google/googlenav/android/M;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1725
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1729
    :cond_2ed
    const-string v0, "msid"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1730
    if-eqz v0, :cond_2fb

    .line 1731
    invoke-direct {p0, v4}, Lcom/google/googlenav/android/M;->c(Landroid/net/Uri;)V

    .line 1732
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1736
    :cond_2fb
    const-string v0, "ll"

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v0

    .line 1738
    const-string v3, "spn"

    invoke-direct {p0, v7, v3}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;)[I

    move-result-object v3

    .line 1740
    const-string v5, "z"

    const/4 v6, -0x1

    invoke-direct {p0, v7, v5, v6}, Lcom/google/googlenav/android/M;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;I)I

    move-result v5

    .line 1742
    if-eqz v0, :cond_38c

    .line 1744
    new-instance v1, LaN/B;

    const/4 v6, 0x0

    aget v6, v0, v6

    const/4 v8, 0x1

    aget v0, v0, v8

    invoke-direct {v1, v6, v0}, LaN/B;-><init>(II)V

    .line 1745
    iget-object v0, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    .line 1746
    const/4 v0, 0x1

    .line 1748
    :goto_321
    if-eqz v3, :cond_32f

    .line 1749
    iget-object v0, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    const/4 v1, 0x0

    aget v1, v3, v1

    const/4 v6, 0x1

    aget v6, v3, v6

    invoke-virtual {v0, v1, v6}, LaN/u;->d(II)V

    .line 1750
    const/4 v0, 0x1

    .line 1755
    :cond_32f
    if-nez v3, :cond_33d

    if-ltz v5, :cond_33d

    .line 1756
    iget-object v0, p0, Lcom/google/googlenav/android/M;->k:LaN/u;

    invoke-static {v5}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->a(LaN/Y;)V

    .line 1757
    const/4 v0, 0x1

    .line 1771
    :cond_33d
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_378

    .line 1772
    const-string v1, "cid"

    invoke-virtual {v7, v1}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1773
    if-eqz v1, :cond_378

    .line 1774
    invoke-static {v1}, Lcom/google/googlenav/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1775
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_378

    .line 1776
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1777
    const-string v0, "uc"

    invoke-virtual {v7, v0}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1778
    const/4 v0, 0x0

    .line 1779
    const-string v3, "insitu"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_370

    .line 1780
    const/4 v0, 0x1

    .line 1782
    :cond_370
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/android/R;->startBusinessDetailsLayer(Ljava/lang/String;Z)V

    .line 1783
    const/4 v0, -0x2

    goto/16 :goto_1a

    .line 1788
    :cond_378
    if-nez v0, :cond_37c

    if-eqz v2, :cond_389

    .line 1791
    :cond_37c
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->resetForInvocation()V

    .line 1792
    if-eqz v0, :cond_386

    const/4 v0, -0x2

    goto/16 :goto_1a

    :cond_386
    const/4 v0, -0x3

    goto/16 :goto_1a

    .line 1798
    :cond_389
    const/4 v0, -0x1

    goto/16 :goto_1a

    :cond_38c
    move v0, v1

    goto :goto_321
.end method


# virtual methods
.method public a()I
    .registers 7

    .prologue
    const/4 v0, -0x2

    const/4 v1, -0x1

    .line 694
    iget-object v2, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v2}, Lcom/google/googlenav/android/R;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 696
    if-nez v2, :cond_c

    move v0, v1

    .line 739
    :cond_b
    :goto_b
    return v0

    .line 700
    :cond_c
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 701
    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v4

    .line 702
    if-eqz v3, :cond_23

    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_23

    const/high16 v5, 0x10

    and-int/2addr v4, v5

    if-eqz v4, :cond_25

    :cond_23
    move v0, v1

    .line 712
    goto :goto_b

    .line 715
    :cond_25
    const-string v4, "android.intent.action.SEARCH"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 716
    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->i(Landroid/content/Intent;)I

    move-result v0

    goto :goto_b

    .line 717
    :cond_32
    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_40

    .line 718
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/android/M;->a(Landroid/content/Intent;Z)I

    move-result v0

    goto :goto_b

    .line 719
    :cond_40
    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_50

    const-string v4, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 720
    :cond_50
    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->k(Landroid/content/Intent;)I

    move-result v0

    .line 721
    if-ne v0, v1, :cond_5a

    .line 722
    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->l(Landroid/content/Intent;)V

    goto :goto_b

    .line 723
    :cond_5a
    const/4 v1, -0x3

    if-ne v0, v1, :cond_60

    .line 724
    const/16 v0, 0x13

    goto :goto_b

    .line 725
    :cond_60
    const/16 v1, 0x1b

    if-eq v0, v1, :cond_b

    .line 728
    iget-object v1, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    goto :goto_b

    .line 731
    :cond_6a
    const-string v4, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_78

    .line 732
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->startSettingsActivity()V

    goto :goto_b

    .line 734
    :cond_78
    const-string v4, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_86

    .line 735
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->startLatitudeSettingsActivity()V

    goto :goto_b

    .line 738
    :cond_86
    invoke-direct {p0, v2}, Lcom/google/googlenav/android/M;->l(Landroid/content/Intent;)V

    move v0, v1

    .line 739
    goto :goto_b
.end method

.method a(Landroid/content/Intent;Z)I
    .registers 11
    .parameter
    .parameter

    .prologue
    const v7, 0xf4240

    const/4 v6, 0x0

    const/4 v5, -0x2

    .line 1116
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1117
    const/4 v1, 0x0

    .line 1119
    if-nez p2, :cond_32

    .line 1120
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v1}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v0, v1}, Lbm/a;->a(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v1

    .line 1122
    invoke-static {v1}, Lbm/a;->b(Ljava/lang/String;)[F

    move-result-object v0

    .line 1123
    if-nez v0, :cond_38

    .line 1124
    const/16 v0, 0x36f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->e(Ljava/lang/String;)V

    .line 1164
    :goto_31
    return v5

    .line 1130
    :cond_32
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_74

    .line 1133
    :cond_38
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    .line 1134
    const-string v3, "application/vnd.google.panorama360+jpg"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4e

    .line 1135
    const/16 v0, 0x36d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/android/M;->e(Ljava/lang/String;)V

    goto :goto_31

    .line 1140
    :cond_4e
    new-instance v2, LaN/B;

    aget v3, v0, v6

    float-to-int v3, v3

    mul-int/2addr v3, v7

    const/4 v4, 0x1

    aget v0, v0, v4

    float-to-int v0, v0

    mul-int/2addr v0, v7

    invoke-direct {v2, v3, v0}, LaN/B;-><init>(II)V

    .line 1141
    new-instance v0, Lcom/google/googlenav/ai;

    const/16 v3, 0x372

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;B)V

    .line 1143
    const-string v2, "gi"

    .line 1149
    new-instance v3, Lcom/google/googlenav/android/N;

    invoke-direct {v3, p0}, Lcom/google/googlenav/android/N;-><init>(Lcom/google/googlenav/android/M;)V

    .line 1163
    iget-object v4, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v4, v0, v1, v2, v3}, Lcom/google/googlenav/android/R;->startPhotoUploadWizard(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    goto :goto_31

    .line 1130
    :array_74
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method a(Landroid/content/Intent;)V
    .registers 8
    .parameter

    .prologue
    const-wide/16 v4, -0x1

    .line 1273
    const/4 v1, 0x0

    .line 1276
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 1279
    :try_start_b
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 1280
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1281
    new-instance v0, Lcom/google/googlenav/h;

    invoke-direct {v0, v2}, Lcom/google/googlenav/h;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_21} :catch_50

    .line 1290
    :goto_21
    const-string v1, "notification_fired"

    invoke-virtual {p1, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 1291
    cmp-long v3, v1, v4

    if-eqz v3, :cond_40

    .line 1292
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    sub-long v1, v3, v1

    .line 1294
    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/h;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/googlenav/friend/ad;->a(JLjava/lang/String;)V

    .line 1299
    :cond_40
    iget-object v1, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-static {p1}, Lcom/google/googlenav/android/M;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "optout"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/googlenav/android/R;->startCheckinWizardFromIntent(Lcom/google/googlenav/h;Ljava/lang/String;Z)V

    .line 1301
    return-void

    .line 1282
    :catch_50
    move-exception v0

    .line 1285
    const-string v2, "ANDROID_INTENT"

    invoke-static {v2, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_56
    move-object v0, v1

    goto :goto_21
.end method

.method public a(Ljava/lang/String;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1868
    const/16 v0, 0x34

    const-string v1, "f"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1870
    iget-object v0, p0, Lcom/google/googlenav/android/M;->j:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x6d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 1871
    invoke-direct {p0}, Lcom/google/googlenav/android/M;->b()V

    .line 1872
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1853
    invoke-direct {p0}, Lcom/google/googlenav/android/M;->b()V

    .line 1854
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1855
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1857
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0}, Lcom/google/googlenav/android/R;->getActivity()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    const-class v2, Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1858
    iget-object v0, p0, Lcom/google/googlenav/android/M;->i:Lcom/google/googlenav/android/R;

    invoke-interface {v0, v1}, Lcom/google/googlenav/android/R;->startActivity(Landroid/content/Intent;)V

    .line 1859
    return-void
.end method
