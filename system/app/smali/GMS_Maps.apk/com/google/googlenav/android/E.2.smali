.class public Lcom/google/googlenav/android/E;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Z

.field public static final b:Z

.field public static final c:Z

.field public static final d:Z

.field public static e:Z

.field public static final f:Z

.field public static final g:Z

.field public static final h:Z

.field public static final i:Z

.field private static final j:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;

.field private static final l:[Ljava/lang/String;

.field private static final m:[Ljava/lang/String;

.field private static final n:[Ljava/lang/String;

.field private static final o:[Ljava/lang/String;

.field private static final p:[Ljava/lang/String;

.field private static final q:[Ljava/lang/String;

.field private static final r:[Ljava/lang/String;

.field private static final s:[Ljava/lang/String;

.field private static volatile t:Ljava/lang/String;

.field private static u:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 29
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SOJU"

    aput-object v1, v0, v5

    const-string v1, "SOJUA"

    aput-object v1, v0, v4

    const-string v1, "SOJUK"

    aput-object v1, v0, v3

    const-string v1, "SOJU_L10N"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "GT-I9000"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "GT-I9000B"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GT-I9000M"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GT-I9000T"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "SC-02B"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SGH-T959"

    aput-object v2, v0, v1

    const-string v1, "SGH-T959D"

    aput-object v1, v0, v7

    const/16 v1, 0xb

    const-string v2, "SGH-T959V"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "VIBRANT T959"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SHW-M110S"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SCH-I400"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SGH-I897"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SGH-I896"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/android/E;->j:[Ljava/lang/String;

    .line 47
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "RTGB"

    aput-object v1, v0, v5

    const-string v1, "SHADOW_VZW"

    aput-object v1, v0, v4

    const-string v1, "DAYTONA"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/android/E;->k:[Ljava/lang/String;

    .line 60
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "SHADOW_VZW"

    aput-object v1, v0, v5

    const-string v1, "DAYTONA"

    aput-object v1, v0, v4

    const-string v1, "SPYDER_VZW"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/android/E;->l:[Ljava/lang/String;

    .line 78
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "SHADOW"

    aput-object v1, v0, v5

    const-string v1, "DAYTONA"

    aput-object v1, v0, v4

    const-string v1, "SPYDER"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/android/E;->m:[Ljava/lang/String;

    .line 90
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "HTC_VISION"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/googlenav/android/E;->n:[Ljava/lang/String;

    .line 100
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "HTC_MARVEL"

    aput-object v1, v0, v5

    const-string v1, "HTC_MARVELC"

    aput-object v1, v0, v4

    const-string v1, "MARVELC"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/android/E;->o:[Ljava/lang/String;

    .line 113
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "PASSION"

    aput-object v1, v0, v5

    const-string v1, "PASSION_KT"

    aput-object v1, v0, v4

    const-string v1, "PASSION_VF"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/android/E;->p:[Ljava/lang/String;

    .line 123
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "HTC_PYRAMID"

    aput-object v1, v0, v5

    const-string v1, "HTC_VIGOR"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/googlenav/android/E;->q:[Ljava/lang/String;

    .line 132
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "SONY ERICSSON"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/googlenav/android/E;->r:[Ljava/lang/String;

    .line 141
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "TG03"

    aput-object v1, v0, v5

    const-string v1, "F11EIF"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/googlenav/android/E;->s:[Ljava/lang/String;

    .line 219
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    if-nez v0, :cond_176

    const-string v0, ""

    .line 220
    :goto_e2
    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    if-nez v1, :cond_17e

    const-string v1, ""

    .line 221
    :goto_e8
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-nez v2, :cond_186

    const-string v2, ""

    .line 223
    :goto_ee
    sget-object v3, Lcom/google/googlenav/android/E;->j:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18e

    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-nez v3, :cond_18e

    move v3, v4

    :goto_101
    sput-boolean v3, Lcom/google/googlenav/android/E;->a:Z

    .line 225
    sget-object v3, Lcom/google/googlenav/android/E;->n:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    sput-boolean v3, Lcom/google/googlenav/android/E;->b:Z

    .line 227
    sget-object v3, Lcom/google/googlenav/android/E;->k:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    sput-boolean v3, Lcom/google/googlenav/android/E;->c:Z

    .line 229
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v3, v7, :cond_191

    sget-object v3, Lcom/google/googlenav/android/E;->l:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_137

    sget-object v3, Lcom/google/googlenav/android/E;->m:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_191

    :cond_137
    :goto_137
    sput-boolean v4, Lcom/google/googlenav/android/E;->d:Z

    .line 234
    sget-object v1, Lcom/google/googlenav/android/E;->r:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/google/googlenav/android/E;->e:Z

    .line 237
    sget-object v1, Lcom/google/googlenav/android/E;->o:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/google/googlenav/android/E;->f:Z

    .line 239
    sget-object v1, Lcom/google/googlenav/android/E;->p:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/google/googlenav/android/E;->g:Z

    .line 243
    sget-object v1, Lcom/google/googlenav/android/E;->q:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/google/googlenav/android/E;->h:Z

    .line 246
    sget-object v1, Lcom/google/googlenav/android/E;->s:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/google/googlenav/android/E;->i:Z

    .line 249
    return-void

    .line 219
    :cond_176
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_e2

    .line 220
    :cond_17e
    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_e8

    .line 221
    :cond_186
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_ee

    :cond_18e
    move v3, v5

    .line 223
    goto/16 :goto_101

    :cond_191
    move v4, v5

    .line 229
    goto :goto_137
.end method

.method public static a(Ljava/lang/String;)V
    .registers 1
    .parameter

    .prologue
    .line 252
    sput-object p0, Lcom/google/googlenav/android/E;->t:Ljava/lang/String;

    .line 253
    return-void
.end method

.method public static a(Z)V
    .registers 1
    .parameter

    .prologue
    .line 261
    sput-boolean p0, Lcom/google/googlenav/android/E;->u:Z

    .line 262
    return-void
.end method

.method public static a()Z
    .registers 2

    .prologue
    .line 256
    sget-object v0, Lcom/google/googlenav/android/E;->t:Ljava/lang/String;

    .line 257
    if-eqz v0, :cond_12

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "adreno"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public static b()Z
    .registers 1

    .prologue
    .line 265
    sget-boolean v0, Lcom/google/googlenav/android/E;->u:Z

    return v0
.end method
