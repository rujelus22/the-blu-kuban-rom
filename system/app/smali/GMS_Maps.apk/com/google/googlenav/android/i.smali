.class public Lcom/google/googlenav/android/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/clientparam/i;


# static fields
.field private static final s:Ljava/util/List;


# instance fields
.field private final a:Lcom/google/googlenav/android/AndroidGmmApplication;

.field private final b:LaN/p;

.field private final c:LaN/u;

.field private final d:Lcom/google/googlenav/ui/s;

.field private final e:Law/h;

.field private final f:Lcom/google/googlenav/ui/android/L;

.field private final g:Lcom/google/googlenav/android/aa;

.field private final h:Lcom/google/googlenav/layer/r;

.field private final i:Lcom/google/googlenav/android/d;

.field private final j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

.field private final k:Lcom/google/googlenav/prefetch/android/g;

.field private final l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

.field private m:Landroid/view/Menu;

.field private n:Lcom/google/android/maps/MapsActivity;

.field private o:Lcom/google/android/maps/A;

.field private p:Z

.field private final q:Laz/b;

.field private final r:[Laz/b;

.field private t:Ljava/util/Locale;

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 256
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/android/i;->s:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/AndroidGmmApplication;Las/c;Law/h;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->m:Landroid/view/Menu;

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->o:Lcom/google/android/maps/A;

    .line 181
    new-instance v0, Laz/l;

    const v1, 0x13d655

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Laz/l;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->q:Laz/b;

    .line 192
    const/4 v0, 0x4

    new-array v0, v0, [Laz/b;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/android/i;->q:Laz/b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Laz/i;->a:Laz/i;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Laz/g;->a:Laz/g;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Laz/h;->a:Laz/h;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/googlenav/android/i;->r:[Laz/b;

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->u:Z

    .line 305
    const-string v0, "AndroidState.AndroidState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 306
    sget-object v0, Lcom/google/googlenav/android/i;->s:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->s()V

    .line 309
    iput-object p1, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    .line 310
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v8

    .line 312
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    .line 314
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->q()V

    .line 316
    invoke-virtual {v0}, Lcom/google/googlenav/K;->al()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 318
    invoke-static {p1}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    .line 321
    :cond_5f
    invoke-static {p0}, Lcom/google/googlenav/clientparam/f;->a(Lcom/google/googlenav/clientparam/i;)V

    .line 323
    new-instance v0, Lcom/google/googlenav/android/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/android/d;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->i:Lcom/google/googlenav/android/d;

    .line 327
    iput-object p3, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    .line 328
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0}, Law/h;->g()V

    .line 335
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/j;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/googlenav/android/j;-><init>(Lcom/google/googlenav/android/i;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    .line 344
    const/16 v0, 0xb

    invoke-static {v0}, Lbm/m;->a(I)V

    .line 346
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/i;->a(Landroid/content/Context;)V

    .line 348
    new-instance v0, Lcom/google/googlenav/ui/android/L;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/L;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    .line 349
    new-instance v0, Lcom/google/googlenav/android/aa;

    invoke-direct {v0}, Lcom/google/googlenav/android/aa;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    .line 357
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v6

    .line 358
    invoke-virtual {v6}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_169

    .line 359
    new-instance v0, LaO/a;

    invoke-virtual {p1}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10

    const/high16 v3, 0x8

    const/high16 v4, 0x10

    invoke-virtual {v6}, Lcom/google/googlenav/K;->g()LaN/B;

    move-result-object v5

    invoke-virtual {v6}, Lcom/google/googlenav/K;->h()LaN/Y;

    move-result-object v6

    const/16 v7, 0x190

    invoke-direct/range {v0 .. v7}, LaO/a;-><init>(Landroid/content/res/Resources;IIILaN/B;LaN/Y;I)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    .line 377
    :goto_bc
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v0, v1}, LaN/p;->a(Lat/d;)V

    .line 379
    new-instance v0, Lcom/google/googlenav/layer/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/layer/b;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->h:Lcom/google/googlenav/layer/r;

    .line 381
    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-eqz v0, :cond_18b

    .line 384
    invoke-static {}, Lcom/google/googlenav/prefetch/android/h;->d()Lcom/google/googlenav/prefetch/android/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    .line 385
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/n;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/n;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 391
    new-instance v0, Lcom/google/googlenav/prefetch/android/g;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    invoke-direct {v0, v1}, Lcom/google/googlenav/prefetch/android/g;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->k:Lcom/google/googlenav/prefetch/android/g;

    .line 394
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    const-class v1, Lcom/google/googlenav/prefetch/android/PrefetcherService;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    .line 396
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/o;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/o;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 408
    :goto_fc
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->r()V

    .line 410
    iget-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-static {p1, v0}, LaM/a;->a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;

    .line 414
    new-instance v0, LaV/a;

    new-instance v1, Lcom/google/googlenav/android/p;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/p;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-virtual {v8}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LaV/a;-><init>(LaV/e;Lcom/google/googlenav/common/a;)V

    invoke-static {v0}, LaV/h;->a(LaV/h;)V

    .line 439
    new-instance v1, Lcom/google/googlenav/android/w;

    invoke-direct {v1, p0, p2}, Lcom/google/googlenav/android/w;-><init>(Lcom/google/googlenav/android/i;Las/c;)V

    .line 446
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_19e

    .line 447
    new-instance v7, Lbg/i;

    invoke-direct {v7}, Lbg/i;-><init>()V

    .line 448
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    check-cast v0, LaO/a;

    .line 449
    const/4 v4, 0x0

    .line 450
    const/4 v5, 0x0

    .line 451
    new-instance v2, LaO/e;

    invoke-virtual {v0}, LaO/a;->w()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v3

    iget-object v6, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-virtual {v6}, LaN/p;->b()LaN/H;

    move-result-object v6

    iget-object v8, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-direct {v2, v3, v0, v6, v8}, LaO/e;-><init>(Lcom/google/android/maps/driveabout/vector/bk;LaO/a;LaN/H;Lcom/google/googlenav/android/aa;)V

    iput-object v2, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    .line 462
    :goto_142
    new-instance v0, Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    iget-object v3, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    invoke-static {}, LaH/o;->m()LaH/m;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/s;-><init>(Lcom/google/googlenav/ui/aC;LaN/p;LaN/u;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;LaH/m;Lbf/at;)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    .line 466
    iget-object v0, p0, Lcom/google/googlenav/android/i;->r:[Laz/b;

    invoke-static {p3, v0}, Laz/c;->a(Law/h;[Laz/b;)V

    .line 469
    new-instance v0, Lcom/google/googlenav/android/y;

    invoke-direct {v0}, Lcom/google/googlenav/android/y;-><init>()V

    invoke-virtual {v0}, Lcom/google/googlenav/android/y;->a()V

    .line 471
    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/ui/s;)V

    .line 472
    const-string v0, "AndroidState.AndroidState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 473
    return-void

    .line 369
    :cond_169
    const/high16 v1, 0x10

    invoke-static {}, Lcom/google/googlenav/ui/bi;->L()Z

    move-result v0

    if-eqz v0, :cond_189

    const/4 v0, 0x2

    :goto_172
    mul-int/2addr v1, v0

    .line 370
    new-instance v0, LaN/p;

    const/4 v2, -0x1

    const/high16 v3, 0x10

    invoke-virtual {v6}, Lcom/google/googlenav/K;->g()LaN/B;

    move-result-object v4

    invoke-virtual {v6}, Lcom/google/googlenav/K;->h()LaN/Y;

    move-result-object v5

    const/16 v6, 0x190

    invoke-direct/range {v0 .. v6}, LaN/p;-><init>(IIILaN/B;LaN/Y;I)V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    goto/16 :goto_bc

    .line 369
    :cond_189
    const/4 v0, 0x1

    goto :goto_172

    .line 403
    :cond_18b
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/s;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    .line 404
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/r;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/r;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/android/i;->l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    .line 405
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/android/i;->k:Lcom/google/googlenav/prefetch/android/g;

    goto/16 :goto_fc

    .line 454
    :cond_19e
    new-instance v7, Lbf/at;

    invoke-direct {v7}, Lbf/at;-><init>()V

    .line 455
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/aC;->a(LaN/p;)Lcom/google/googlenav/ui/bF;

    move-result-object v4

    .line 456
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/aC;->b(LaN/p;)Lcom/google/googlenav/ui/p;

    move-result-object v5

    .line 457
    const/4 v0, 0x1

    .line 458
    new-instance v2, LaN/h;

    iget-object v3, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    invoke-direct {v2, v3, v4, v5, v0}, LaN/h;-><init>(LaN/p;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;Z)V

    iput-object v2, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    goto :goto_142
.end method

.method private a(Lcom/google/googlenav/ui/android/a;)Landroid/app/ProgressDialog;
    .registers 5
    .parameter

    .prologue
    .line 983
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 984
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 985
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 986
    iget-object v1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d013a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 987
    new-instance v1, Lcom/google/googlenav/android/k;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/android/k;-><init>(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/a;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 997
    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->j:Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 572
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 574
    invoke-static {p1}, Lcom/google/googlenav/android/i;->b(Landroid/content/Context;)Z

    move-result v3

    .line 575
    new-instance v4, LaJ/a;

    if-nez v3, :cond_47

    move v0, v1

    :goto_f
    invoke-direct {v4, v0}, LaJ/a;-><init>(Z)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 576
    if-eqz v3, :cond_52

    .line 577
    new-instance v0, LaH/z;

    invoke-direct {v0, p1}, LaH/z;-><init>(Landroid/content/Context;)V

    .line 578
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->t()Z

    move-result v3

    if-eqz v3, :cond_49

    .line 579
    new-instance v3, LaK/h;

    invoke-direct {v3, p1, v0}, LaK/h;-><init>(Landroid/content/Context;LaH/F;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 597
    :goto_2e
    new-instance v0, LaH/o;

    invoke-direct {v0, v1, v2}, LaH/o;-><init>(ZLjava/util/List;)V

    .line 600
    invoke-static {v0}, LaH/o;->a(LaH/o;)V

    .line 604
    new-instance v0, LaI/a;

    invoke-direct {v0}, LaI/a;-><init>()V

    .line 605
    invoke-static {v0}, LaI/b;->a(LaI/c;)V

    .line 609
    new-instance v0, LaL/a;

    invoke-direct {v0}, LaL/a;-><init>()V

    invoke-static {v0}, LaL/d;->a(LaL/e;)V

    .line 610
    return-void

    .line 575
    :cond_47
    const/4 v0, 0x0

    goto :goto_f

    .line 582
    :cond_49
    new-instance v3, LaH/a;

    invoke-direct {v3, p1, v0}, LaH/a;-><init>(Landroid/content/Context;LaH/F;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 587
    :cond_52
    const-string v0, "nlp_state"

    invoke-virtual {p1, v0}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_2e
.end method

.method public static declared-synchronized a(Lcom/google/googlenav/android/AndroidGmmApplication;)V
    .registers 9
    .parameter

    .prologue
    .line 279
    const-class v6, Lcom/google/googlenav/android/i;

    monitor-enter v6

    :try_start_3
    const-string v0, "AndroidState.setUpSharedState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Lcom/google/googlenav/K;->al()Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 285
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->al()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 286
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/K;->a(Z)V

    .line 290
    :cond_20
    invoke-virtual {p0}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bf;->b:[LA/c;

    const-string v3, "GMM"

    const v4, 0x7f070003

    new-instance v5, Lcom/google/googlenav/android/I;

    invoke-virtual {p0}, Lcom/google/googlenav/android/AndroidGmmApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V

    .line 299
    :cond_3f
    invoke-static {p0}, Lcom/google/googlenav/android/F;->a(Landroid/app/Application;)V

    .line 300
    const-string v0, "AndroidState.setUpSharedState"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_47
    .catchall {:try_start_3 .. :try_end_47} :catchall_49

    .line 301
    monitor-exit v6

    return-void

    .line 279
    :catchall_49
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;ZLaw/q;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 138
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;ZLaw/q;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/google/googlenav/android/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;ZLaw/q;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 879
    const/4 v6, 0x1

    .line 880
    iget-object v7, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    new-instance v0, Lcom/google/googlenav/android/v;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/android/v;-><init>(Lcom/google/googlenav/android/i;Landroid/app/ProgressDialog;ZLcom/google/googlenav/android/x;Law/q;)V

    invoke-virtual {v7, v0, v6}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 913
    return-void
.end method

.method public static a(Lcom/google/googlenav/ui/s;)V
    .registers 1
    .parameter

    .prologue
    .line 1177
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1057
    :goto_8
    return-void

    .line 1009
    :cond_9
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1012
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1}, Law/h;->y()Ljava/lang/Throwable;

    move-result-object v1

    .line 1013
    if-eqz v1, :cond_1d

    .line 1017
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 1020
    :cond_1d
    const v1, 0x7f0d0152

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1021
    invoke-static {}, Lcom/google/googlenav/android/a;->e()Z

    move-result v1

    if-nez v1, :cond_2f

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_93

    .line 1024
    :cond_2f
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    if-nez v1, :cond_74

    .line 1025
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is unavailable. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Check that the server is up and that you have a network connection."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1041
    :goto_59
    const v1, 0x7f0d011a

    new-instance v2, Lcom/google/googlenav/android/l;

    invoke-direct {v2, p0}, Lcom/google/googlenav/android/l;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1048
    new-instance v1, Lcom/google/googlenav/android/m;

    invoke-direct {v1, p0}, Lcom/google/googlenav/android/m;-><init>(Lcom/google/googlenav/android/i;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1056
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_8

    .line 1029
    :cond_74
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wrong remote strings version, or no network connection, or the GMM Server is down.  GMM Server must be hosting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_59

    .line 1039
    :cond_93
    const v1, 0x7f0d0151

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_59
.end method

.method private a(Ljava/lang/String;Lcom/google/googlenav/android/x;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 817
    new-instance v3, Lcom/google/googlenav/ui/android/a;

    invoke-direct {v3, p1}, Lcom/google/googlenav/ui/android/a;-><init>(Ljava/lang/String;)V

    .line 821
    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/a;->a()Z

    move-result v0

    .line 825
    if-eqz v0, :cond_35

    .line 830
    if-eqz p3, :cond_33

    .line 832
    invoke-direct {p0, v3}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/ui/android/a;)Landroid/app/ProgressDialog;

    move-result-object v5

    .line 833
    invoke-static {v5}, Lcom/google/googlenav/ui/android/G;->b(Landroid/app/Dialog;)V

    .line 841
    :goto_14
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0}, Law/h;->f()Z

    move-result v6

    .line 845
    if-eqz v6, :cond_21

    .line 846
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0}, Law/h;->h()V

    .line 852
    :cond_21
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-virtual {v0, v3}, Law/h;->a(Law/q;)V

    .line 855
    new-instance v0, Lcom/google/googlenav/android/u;

    const-string v2, "RemoteStrings"

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/android/u;-><init>(Lcom/google/googlenav/android/i;Ljava/lang/String;Lcom/google/googlenav/ui/android/a;Lcom/google/googlenav/android/x;Landroid/app/ProgressDialog;Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/android/u;->start()V

    .line 872
    :goto_32
    return-void

    .line 835
    :cond_33
    const/4 v5, 0x0

    goto :goto_14

    .line 867
    :cond_35
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/android/a;->a(Z)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/Config;->a([Ljava/lang/String;)V

    .line 869
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->t()V

    .line 870
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->p:Z

    goto :goto_32
.end method

.method static synthetic a(Lcom/google/googlenav/android/i;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/google/googlenav/android/i;->p:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/android/i;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->l:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    return-object v0
.end method

.method private static b(Landroid/content/Context;)Z
    .registers 2
    .parameter

    .prologue
    .line 616
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {p0}, Lcom/google/android/location/clientlib/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {}, LaJ/a;->u()Z

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method static synthetic c(Lcom/google/googlenav/android/i;)Lcom/google/android/maps/MapsActivity;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/AndroidGmmApplication;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/android/i;)Law/h;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/android/i;)V
    .registers 1
    .parameter

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->t()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/d;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->i:Lcom/google/googlenav/android/d;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/ui/android/L;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/android/aa;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/android/i;)Lcom/google/googlenav/layer/r;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->h:Lcom/google/googlenav/layer/r;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/android/i;)LaN/p;
    .registers 2
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    return-object v0
.end method

.method private p()[LaE/e;
    .registers 4

    .prologue
    .line 205
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    .line 206
    const/16 v1, 0xa

    invoke-static {v1}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 209
    sget-object v2, LaE/g;->a:LaE/g;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 213
    sget-object v0, LaE/a;->a:LaE/a;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_1a
    sget-object v0, LaE/b;->a:LaE/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_49

    .line 236
    :goto_25
    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    if-eqz v0, :cond_3c

    .line 237
    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "en"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 238
    if-eqz v0, :cond_3c

    .line 239
    sget-object v0, LaE/d;->a:LaE/d;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_3c
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [LaE/e;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaE/e;

    return-object v0

    .line 222
    :cond_49
    sget-object v0, LaF/b;->a:LaF/b;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v0, LaE/i;->a:LaE/i;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    sget-object v0, LaE/c;->a:LaE/c;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_25
.end method

.method private q()V
    .registers 5

    .prologue
    .line 530
    const/4 v0, 0x0

    .line 533
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->L()Z

    move-result v1

    if-nez v1, :cond_38

    .line 534
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "UserAgentPref"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 538
    :goto_1a
    if-eqz v1, :cond_2a

    .line 539
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->l()Lcom/google/googlenav/common/io/g;

    move-result-object v0

    check-cast v0, Lap/a;

    invoke-virtual {v0, v1}, Lap/a;->a(Ljava/lang/String;)V

    .line 563
    :goto_29
    return-void

    .line 549
    :cond_2a
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/android/r;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/googlenav/android/r;-><init>(Lcom/google/googlenav/android/i;ZZ)V

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;I)V

    goto :goto_29

    :cond_38
    move-object v1, v0

    goto :goto_1a
.end method

.method private r()V
    .registers 3

    .prologue
    .line 566
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->p()Lam/b;

    move-result-object v0

    check-cast v0, Lam/k;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lam/k;->a(Lam/l;)V

    .line 568
    new-instance v0, Lbf/al;

    invoke-direct {v0}, Lbf/al;-><init>()V

    invoke-static {v0}, Lbf/al;->a(Lbf/al;)V

    .line 569
    return-void
.end method

.method private s()V
    .registers 5

    .prologue
    .line 922
    const/4 v0, 0x0

    .line 923
    sget-object v1, Lcom/google/googlenav/android/i;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 924
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_45

    .line 925
    add-int/lit8 v0, v1, 0x1

    :goto_1c
    move v1, v0

    goto :goto_8

    .line 928
    :cond_1e
    const/4 v0, 0x1

    if-le v1, v0, :cond_44

    .line 931
    const-string v0, "AndroidState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "******************** WARNING **** =====>  Memory leak detected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AndroidState instances !!! <==== **** WARNING ********************"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 936
    :cond_44
    return-void

    :cond_45
    move v0, v1

    goto :goto_1c
.end method

.method private t()V
    .registers 2

    .prologue
    .line 971
    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ar()V

    .line 973
    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_12

    .line 976
    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->b()V

    .line 978
    :cond_12
    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    .line 482
    const/4 v0, 0x0

    .line 483
    new-instance v1, Lcom/google/googlenav/android/q;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/googlenav/android/q;-><init>(Lcom/google/googlenav/android/i;Las/c;Lcom/google/googlenav/android/aa;Z)V

    invoke-virtual {v1}, Lcom/google/googlenav/android/q;->g()V

    .line 508
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 947
    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    if-nez v0, :cond_c

    .line 948
    const-string v0, "onConfigurationChanged"

    const-string v1, "currentLocale should not be null"

    invoke-static {v0, v1}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 963
    :cond_b
    :goto_b
    return-void

    .line 952
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 958
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->a([Ljava/lang/String;)V

    .line 961
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->u:Z

    goto :goto_b
.end method

.method public a(Lcom/google/android/maps/A;)V
    .registers 2
    .parameter

    .prologue
    .line 714
    iput-object p1, p0, Lcom/google/googlenav/android/i;->o:Lcom/google/android/maps/A;

    .line 715
    return-void
.end method

.method public a(Lcom/google/android/maps/MapsActivity;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    if-eq v0, p1, :cond_36

    .line 656
    iput-object p1, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    .line 657
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Landroid/content/Context;)V

    .line 659
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    check-cast v0, LaV/a;

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LaV/a;->a(Landroid/content/Context;)V

    .line 663
    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 666
    if-nez p2, :cond_36

    invoke-static {}, Lcom/google/googlenav/ui/view/android/ad;->a()Lcom/google/googlenav/ui/view/android/ad;

    move-result-object v0

    if-eqz v0, :cond_36

    invoke-virtual {p0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    if-eqz v0, :cond_36

    .line 667
    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->initMapUi()V

    .line 670
    :cond_36
    return-void
.end method

.method public a(Ljava/util/Locale;Lcom/google/googlenav/android/x;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 762
    if-nez p1, :cond_7

    .line 763
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    .line 766
    :cond_7
    if-eqz p4, :cond_b

    if-nez p3, :cond_b

    .line 770
    :cond_b
    iput-object p1, p0, Lcom/google/googlenav/android/i;->t:Ljava/util/Locale;

    .line 771
    iput-boolean v1, p0, Lcom/google/googlenav/android/i;->u:Z

    .line 775
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/Config;->a(Ljava/util/Locale;)V

    .line 778
    invoke-virtual {p0}, Lcom/google/googlenav/android/i;->m()Z

    move-result v0

    if-eqz v0, :cond_30

    if-eqz p3, :cond_30

    .line 779
    iput-boolean v1, p0, Lcom/google/googlenav/android/i;->p:Z

    .line 781
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 782
    invoke-direct {p0, v0, p2, p4}, Lcom/google/googlenav/android/i;->a(Ljava/lang/String;Lcom/google/googlenav/android/x;Z)V

    .line 787
    :goto_2f
    return-void

    .line 784
    :cond_30
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->t()V

    .line 785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/android/i;->p:Z

    goto :goto_2f
.end method

.method public b()Lcom/google/googlenav/prefetch/android/g;
    .registers 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/google/googlenav/android/i;->k:Lcom/google/googlenav/prefetch/android/g;

    return-object v0
.end method

.method public c()V
    .registers 6

    .prologue
    .line 625
    invoke-static {}, LaE/f;->a()LaE/f;

    move-result-object v0

    if-nez v0, :cond_29

    .line 627
    invoke-direct {p0}, Lcom/google/googlenav/android/i;->p()[LaE/e;

    move-result-object v2

    .line 628
    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_d
    if-ge v1, v3, :cond_24

    aget-object v0, v2, v1

    .line 629
    instance-of v4, v0, LaF/a;

    if-eqz v4, :cond_20

    .line 630
    check-cast v0, LaF/a;

    iget-object v4, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v4}, Lcom/google/android/maps/MapsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v0, v4}, LaF/a;->a(Landroid/content/Context;)V

    .line 628
    :cond_20
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 634
    :cond_24
    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    invoke-static {v0, v2}, LaE/f;->a(Lcom/google/googlenav/ui/s;[LaE/e;)V

    .line 636
    :cond_29
    return-void
.end method

.method public d()Lcom/google/googlenav/android/AndroidGmmApplication;
    .registers 2

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    return-object v0
.end method

.method public e()Lcom/google/googlenav/android/d;
    .registers 2

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/googlenav/android/i;->i:Lcom/google/googlenav/android/d;

    return-object v0
.end method

.method public f()Lcom/google/android/maps/MapsActivity;
    .registers 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/googlenav/android/i;->n:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method public g()LaN/p;
    .registers 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/googlenav/android/i;->b:LaN/p;

    return-object v0
.end method

.method public h()LaN/u;
    .registers 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/googlenav/android/i;->c:LaN/u;

    return-object v0
.end method

.method public i()Lcom/google/googlenav/ui/s;
    .registers 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/googlenav/android/i;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method public j()Lcom/google/googlenav/ui/android/L;
    .registers 2

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/googlenav/android/i;->f:Lcom/google/googlenav/ui/android/L;

    return-object v0
.end method

.method public k()Lcom/google/googlenav/android/aa;
    .registers 2

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/googlenav/android/i;->g:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method public l()V
    .registers 5

    .prologue
    .line 733
    const/4 v0, 0x0

    .line 734
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 735
    new-instance v2, Lcom/google/googlenav/android/s;

    invoke-direct {v2, p0, v1}, Lcom/google/googlenav/android/s;-><init>(Lcom/google/googlenav/android/i;Landroid/os/Handler;)V

    .line 746
    invoke-static {}, LR/o;->f()V

    .line 747
    iget-object v1, p0, Lcom/google/googlenav/android/i;->a:Lcom/google/googlenav/android/AndroidGmmApplication;

    iget-object v3, p0, Lcom/google/googlenav/android/i;->e:Law/h;

    invoke-static {v1, v3, v2, v0}, LR/o;->a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    .line 749
    return-void
.end method

.method public m()Z
    .registers 2

    .prologue
    .line 795
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->d()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public n()Z
    .registers 2

    .prologue
    .line 805
    iget-boolean v0, p0, Lcom/google/googlenav/android/i;->p:Z

    return v0
.end method

.method public o()Z
    .registers 2

    .prologue
    .line 966
    iget-boolean v0, p0, Lcom/google/googlenav/android/i;->u:Z

    return v0
.end method
