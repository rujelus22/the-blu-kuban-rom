.class public Lcom/google/googlenav/by;
.super Lcom/google/googlenav/ai;
.source "SourceFile"


# instance fields
.field private final c:Z


# direct methods
.method public constructor <init>(LaR/D;)V
    .registers 4
    .parameter

    .prologue
    .line 33
    invoke-virtual {p1}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->d(Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(B)V

    .line 37
    invoke-virtual {p1}, LaR/D;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(Ljava/util/List;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/by;->c:Z

    .line 39
    return-void
.end method

.method public constructor <init>(LaR/D;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p2}, Lcom/google/googlenav/ai;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 46
    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->d(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(B)V

    .line 48
    invoke-virtual {p1}, LaR/D;->d()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 49
    invoke-virtual {p1}, LaR/D;->d()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->c(LaN/g;)V

    .line 51
    :cond_1b
    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->g(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1}, LaR/D;->e()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/by;->a(Ljava/util/List;)V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/by;->c:Z

    .line 54
    return-void
.end method


# virtual methods
.method public f()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/googlenav/by;->c:Z

    return v0
.end method
