.class public final Lcom/google/googlenav/common/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Vector;

.field private static b:[B

.field private static c:J

.field private static d:J


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 33
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    .line 52
    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/google/googlenav/common/k;->c:J

    .line 57
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/googlenav/common/k;->d:J

    .line 60
    invoke-static {}, Lcom/google/googlenav/common/k;->c()V

    .line 61
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 279
    :try_start_0
    const-string v0, "android.os.SystemProperties"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 281
    const-string v1, "get"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 283
    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_29} :catch_2a

    .line 287
    :goto_29
    return-object v0

    .line 284
    :catch_2a
    move-exception v0

    .line 286
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, p1

    .line 287
    goto :goto_29
.end method

.method public static a()V
    .registers 1

    .prologue
    .line 164
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/googlenav/common/k;->a(Z)V

    .line 165
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/h;)V
    .registers 3
    .parameter

    .prologue
    .line 117
    invoke-static {}, Lcom/google/googlenav/common/k;->d()V

    .line 118
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method private static a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 180
    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/common/k;->b:[B

    .line 184
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    if-eqz p0, :cond_30

    const-string v0, "LowOnMemory"

    :goto_9
    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 186
    invoke-static {}, Lcom/google/googlenav/common/k;->d()V

    .line 189
    const/4 v0, 0x0

    move v1, v0

    :goto_11
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_33

    .line 190
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 191
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/h;

    .line 194
    if-eqz v0, :cond_2c

    .line 195
    invoke-interface {v0, p0}, Lcom/google/googlenav/common/h;->a(Z)V

    .line 189
    :cond_2c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_11

    .line 184
    :cond_30
    const-string v0, "OutOfMemory"

    goto :goto_9

    .line 200
    :cond_33
    invoke-static {}, Lcom/google/googlenav/common/k;->c()V

    .line 201
    return-void
.end method

.method public static b()V
    .registers 1

    .prologue
    .line 175
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/googlenav/common/k;->a(Z)V

    .line 176
    return-void
.end method

.method public static b(Lcom/google/googlenav/common/h;)V
    .registers 4
    .parameter

    .prologue
    .line 126
    invoke-static {}, Lcom/google/googlenav/common/k;->d()V

    .line 127
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 128
    return-void
.end method

.method private static c()V
    .registers 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/googlenav/common/k;->b:[B

    if-nez v0, :cond_b

    .line 79
    const v0, 0x8000

    :try_start_7
    new-array v0, v0, [B

    sput-object v0, Lcom/google/googlenav/common/k;->b:[B
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_b} :catch_c

    .line 84
    :cond_b
    :goto_b
    return-void

    .line 80
    :catch_c
    move-exception v0

    goto :goto_b
.end method

.method public static c(Lcom/google/googlenav/common/h;)V
    .registers 3
    .parameter

    .prologue
    .line 137
    invoke-static {}, Lcom/google/googlenav/common/k;->d()V

    .line 139
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_20

    .line 140
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 141
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_21

    .line 142
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    .line 146
    :cond_20
    return-void

    .line 139
    :cond_21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5
.end method

.method private static d()V
    .registers 6

    .prologue
    .line 90
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    .line 91
    sget-wide v0, Lcom/google/googlenav/common/k;->d:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1e

    sget-wide v0, Lcom/google/googlenav/common/k;->d:J

    sub-long v0, v2, v0

    sget-wide v4, Lcom/google/googlenav/common/k;->c:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_42

    .line 94
    :cond_1e
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_27
    if-ltz v1, :cond_40

    .line 95
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 96
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3c

    .line 97
    sget-object v0, Lcom/google/googlenav/common/k;->a:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    .line 94
    :cond_3c
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_27

    .line 100
    :cond_40
    sput-wide v2, Lcom/google/googlenav/common/k;->d:J

    .line 102
    :cond_42
    return-void
.end method
