.class public Lcom/google/googlenav/common/io/protocol/ProtoBuf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final EMPTY_BYTE_ARRAY:[B = null

.field public static final FALSE:Ljava/lang/Boolean; = null

.field private static final MARKER_HOLDER:I = -0x1

.field private static final MSG_EOF:Ljava/lang/String; = "Unexp.EOF"

.field private static final MSG_MISMATCH:Ljava/lang/String; = "Type mismatch"

.field private static final MSG_UNSUPPORTED:Ljava/lang/String; = "Unsupp.Type"

.field private static final NULL_COUNTER:Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter; = null

.field private static final TEXT_FORMAT_TAB_SPACES_PER_LEVEL:I = 0x2

.field public static final TRUE:Ljava/lang/Boolean; = null

.field private static final VARINT_MAX_BYTES:I = 0xa

.field static final WIRETYPE_END_GROUP:I = 0x4

.field static final WIRETYPE_FIXED32:I = 0x5

.field static final WIRETYPE_FIXED64:I = 0x1

.field static final WIRETYPE_LENGTH_DELIMITED:I = 0x2

.field static final WIRETYPE_START_GROUP:I = 0x3

.field static final WIRETYPE_VARINT:I


# instance fields
.field private cachedSize:I

.field private msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

.field private final values:Lcom/google/googlenav/common/util/h;

.field private wireTypes:Lcom/google/googlenav/common/util/h;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 57
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v2}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    .line 58
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    .line 59
    new-array v0, v2, [B

    sput-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->EMPTY_BYTE_ARRAY:[B

    .line 432
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf$1;)V

    sput-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->NULL_COUNTER:Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    .registers 3
    .parameter

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->cachedSize:I

    .line 104
    iput-object p1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 105
    new-instance v0, Lcom/google/googlenav/common/util/h;

    invoke-direct {v0}, Lcom/google/googlenav/common/util/h;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    .line 106
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->cachedSize:I

    .line 113
    iput-object p1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 116
    new-instance v0, Lcom/google/googlenav/common/util/h;

    add-int/lit8 v1, p2, 0x1

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/util/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    .line 117
    return-void
.end method

.method private addObject(ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1543
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;Z)V

    .line 1544
    return-void
.end method

.method private assertTypeMatch(ILjava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1302
    return-void
.end method

.method private static checkTag(I)V
    .registers 1
    .parameter

    .prologue
    .line 1394
    return-void
.end method

.method private convert(Ljava/lang/Object;II)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1551
    packed-switch p2, :pswitch_data_ac

    .line 1622
    :pswitch_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupp.Type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1556
    :pswitch_b
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 1619
    :cond_f
    :goto_f
    :pswitch_f
    return-object p1

    .line 1559
    :cond_10
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    packed-switch v0, :pswitch_data_da

    .line 1565
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Type mismatch"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1561
    :pswitch_22
    sget-object p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_f

    .line 1563
    :pswitch_25
    sget-object p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    goto :goto_f

    .line 1578
    :pswitch_28
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 1579
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3b

    const-wide/16 v0, 0x1

    :goto_36
    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object p1

    goto :goto_f

    :cond_3b
    const-wide/16 v0, 0x0

    goto :goto_36

    .line 1584
    :pswitch_3e
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_49

    .line 1585
    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Lcom/google/googlenav/common/io/o;->a(Ljava/lang/String;)[B

    move-result-object p1

    goto :goto_f

    .line 1586
    :cond_49
    instance-of v0, p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v0, :cond_f

    .line 1587
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1589
    :try_start_52
    check-cast p1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 1590
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_5a
    .catch Ljava/io/IOException; {:try_start_52 .. :try_end_5a} :catch_5c

    move-result-object p1

    goto :goto_f

    .line 1591
    :catch_5c
    move-exception v0

    .line 1592
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1598
    :pswitch_67
    instance-of v0, p1, [B

    if-eqz v0, :cond_f

    .line 1599
    check-cast p1, [B

    check-cast p1, [B

    .line 1600
    const/4 v0, 0x0

    array-length v1, p1

    const/4 v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcom/google/googlenav/common/io/o;->a([BIIZ)Ljava/lang/String;

    move-result-object p1

    goto :goto_f

    .line 1605
    :pswitch_77
    instance-of v0, p1, [B

    if-eqz v0, :cond_f

    .line 1608
    if-lez p3, :cond_99

    :try_start_7d
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-eqz v0, :cond_99

    .line 1609
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    move-object v0, v1

    .line 1614
    :goto_8f
    check-cast p1, [B

    check-cast p1, [B

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object p1

    goto/16 :goto_f

    .line 1611
    :cond_99
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    :try_end_9f
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_9f} :catch_a0

    goto :goto_8f

    .line 1615
    :catch_a0
    move-exception v0

    .line 1616
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1551
    nop

    :pswitch_data_ac
    .packed-switch 0x10
        :pswitch_f
        :pswitch_3
        :pswitch_3
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_b
        :pswitch_3e
        :pswitch_77
        :pswitch_77
        :pswitch_67
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_3e
        :pswitch_67
    .end packed-switch

    .line 1559
    :pswitch_data_da
    .packed-switch 0x0
        :pswitch_22
        :pswitch_25
    .end packed-switch
.end method

.method private fieldToTextFormat(Ljava/io/Writer;IIILjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1011
    invoke-virtual {p1, p5}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1013
    invoke-virtual {p0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 1014
    const/16 v1, 0x1a

    if-ne v0, v1, :cond_2e

    .line 1015
    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 1019
    :goto_18
    packed-switch v0, :pswitch_data_7c

    .line 1061
    const-string v1, "UNSUPPORTED TYPE: "

    invoke-virtual {p1, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1063
    :goto_28
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    .line 1064
    return-void

    .line 1017
    :cond_2e
    const/16 v1, 0x3a

    invoke-virtual {p1, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    goto :goto_18

    .line 1023
    :pswitch_34
    const-string v0, "{\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1024
    invoke-virtual {p0, p3, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toTextFormat(Ljava/io/Writer;I)V

    .line 1025
    invoke-virtual {p1, p5}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(C)Ljava/io/Writer;

    goto :goto_28

    .line 1029
    :pswitch_4c
    invoke-virtual {p0, p3, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(II)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_28

    .line 1033
    :pswitch_58
    invoke-virtual {p0, p3, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDouble(II)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_28

    .line 1051
    :pswitch_64
    invoke-direct {p0, p3, p4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_28

    .line 1057
    :pswitch_70
    invoke-virtual {p0, p3, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->a([B)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    goto :goto_28

    .line 1019
    :pswitch_data_7c
    .packed-switch 0x11
        :pswitch_58
        :pswitch_4c
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_70
        :pswitch_34
        :pswitch_34
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_64
        :pswitch_70
        :pswitch_64
    .end packed-switch
.end method

.method private getCachedDataSize(IIZ)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 718
    shl-int/lit8 v0, p1, 0x3

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v2

    .line 720
    invoke-direct {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getWireType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_5e

    .line 737
    :pswitch_e
    const/16 v0, 0x10

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    .line 741
    instance-of v1, v0, [B

    if-eqz v1, :cond_4a

    .line 742
    check-cast v0, [B

    check-cast v0, [B

    array-length v0, v0

    .line 749
    :goto_1d
    int-to-long v3, v0

    invoke-static {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v1

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :goto_24
    return v0

    .line 722
    :pswitch_25
    add-int/lit8 v0, v2, 0x4

    goto :goto_24

    .line 724
    :pswitch_28
    add-int/lit8 v0, v2, 0x8

    goto :goto_24

    .line 726
    :pswitch_2b
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v0

    .line 727
    invoke-direct {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v3

    if-eqz v3, :cond_39

    .line 728
    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->zigZagEncode(J)J

    move-result-wide v0

    .line 730
    :cond_39
    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_24

    .line 733
    :pswitch_3f
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCachedDataSize(Z)I

    move-result v0

    add-int/2addr v0, v2

    add-int/2addr v0, v2

    goto :goto_24

    .line 743
    :cond_4a
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_57

    .line 744
    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Lcom/google/googlenav/common/io/o;->a(Ljava/lang/String;[BI)I

    move-result v0

    goto :goto_1d

    .line 746
    :cond_57
    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCachedDataSize(Z)I

    move-result v0

    goto :goto_1d

    .line 720
    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_2b
        :pswitch_28
        :pswitch_e
        :pswitch_3f
        :pswitch_e
        :pswitch_25
    .end packed-switch
.end method

.method private getCachedDataSize(Z)I
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 646
    iget v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->cachedSize:I

    const/high16 v2, -0x8000

    if-eq v0, v2, :cond_c

    if-eqz p1, :cond_c

    .line 647
    iget v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->cachedSize:I

    .line 660
    :goto_b
    return v0

    .line 650
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/h;->b()Lcom/google/googlenav/common/util/i;

    move-result-object v4

    move v0, v1

    .line 651
    :cond_13
    invoke-virtual {v4}, Lcom/google/googlenav/common/util/i;->a()Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 652
    invoke-virtual {v4}, Lcom/google/googlenav/common/util/i;->b()I

    move-result v5

    .line 653
    invoke-virtual {p0, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    move v2, v1

    .line 654
    :goto_22
    if-ge v2, v6, :cond_13

    .line 655
    invoke-direct {p0, v5, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCachedDataSize(IIZ)I

    move-result v3

    add-int/2addr v3, v0

    .line 654
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_22

    .line 658
    :cond_2e
    iput v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->cachedSize:I

    .line 660
    iget v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->cachedSize:I

    goto :goto_b
.end method

.method private static getCount(Ljava/lang/Object;)I
    .registers 2
    .parameter

    .prologue
    .line 577
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    instance-of v0, p0, Ljava/util/Vector;

    if-eqz v0, :cond_f

    check-cast p0, Ljava/util/Vector;

    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_3

    :cond_f
    const/4 v0, 0x1

    goto :goto_3
.end method

.method private getDefault(I)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1377
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_14

    .line 1383
    iget-object v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-nez v1, :cond_d

    :goto_c
    :sswitch_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_c

    .line 1377
    :sswitch_data_14
    .sparse-switch
        0x10 -> :sswitch_c
        0x1a -> :sswitch_c
        0x1b -> :sswitch_c
    .end sparse-switch
.end method

.method private static getNumBytesUsed(Ljava/lang/Object;)I
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 680
    if-nez p0, :cond_4

    .line 707
    :cond_3
    :goto_3
    return v0

    .line 683
    :cond_4
    instance-of v1, p0, Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 684
    check-cast p0, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_3

    .line 686
    :cond_f
    instance-of v1, p0, Ljava/util/Vector;

    if-eqz v1, :cond_30

    move v1, v0

    move v2, v0

    :goto_15
    move-object v0, p0

    .line 688
    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2e

    move-object v0, p0

    .line 689
    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 690
    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v2, v0

    .line 688
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    :cond_2e
    move v0, v2

    .line 692
    goto :goto_3

    .line 694
    :cond_30
    instance-of v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_3b

    .line 695
    check-cast p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed()I

    move-result v0

    goto :goto_3

    .line 697
    :cond_3b
    instance-of v1, p0, [B

    if-eqz v1, :cond_45

    .line 698
    check-cast p0, [B

    check-cast p0, [B

    array-length v0, p0

    goto :goto_3

    .line 700
    :cond_45
    instance-of v1, p0, Ljava/lang/Long;

    if-eqz v1, :cond_4c

    .line 701
    const/16 v0, 0x8

    goto :goto_3

    .line 703
    :cond_4c
    instance-of v1, p0, Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 704
    const/4 v0, 0x1

    goto :goto_3
.end method

.method private getObject(II)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1403
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->checkTag(I)V

    .line 1404
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1405
    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(Ljava/lang/Object;)I

    move-result v1

    .line 1407
    if-nez v1, :cond_14

    .line 1408
    invoke-direct {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDefault(I)Ljava/lang/Object;

    move-result-object v0

    .line 1414
    :goto_13
    return-object v0

    .line 1411
    :cond_14
    const/4 v2, 0x1

    if-le v1, v2, :cond_1d

    .line 1412
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1414
    :cond_1d
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObjectWithoutArgChecking(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_13
.end method

.method private getObject(III)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1423
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->checkTag(I)V

    .line 1424
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1425
    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(Ljava/lang/Object;)I

    move-result v1

    .line 1427
    if-lt p2, v1, :cond_15

    .line 1428
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1430
    :cond_15
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObjectWithoutArgChecking(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private getObjectWithoutArgChecking(IIILjava/lang/Object;)Ljava/lang/Object;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1439
    const/4 v0, 0x0

    .line 1440
    instance-of v1, p4, Ljava/util/Vector;

    if-eqz v1, :cond_1d

    .line 1441
    check-cast p4, Ljava/util/Vector;

    .line 1442
    invoke-virtual {p4, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    .line 1445
    :goto_b
    invoke-direct {p0, v0, p3, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->convert(Ljava/lang/Object;II)Ljava/lang/Object;

    move-result-object v1

    .line 1446
    if-eq v1, v0, :cond_18

    if-eqz v0, :cond_18

    .line 1447
    if-nez p4, :cond_19

    .line 1448
    invoke-direct {p0, p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1453
    :cond_18
    :goto_18
    return-object v1

    .line 1450
    :cond_19
    invoke-virtual {p4, v1, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_18

    :cond_1d
    move-object v2, v0

    move-object v0, p4

    move-object p4, v2

    goto :goto_b
.end method

.method private static getVarIntSize(J)I
    .registers 5
    .parameter

    .prologue
    .line 757
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_9

    .line 758
    const/16 v0, 0xa

    .line 765
    :cond_8
    return v0

    .line 760
    :cond_9
    const/4 v0, 0x1

    .line 761
    :goto_a
    const-wide/16 v1, 0x80

    cmp-long v1, p0, v1

    if-ltz v1, :cond_8

    .line 762
    add-int/lit8 v0, v0, 0x1

    .line 763
    const/4 v1, 0x7

    shr-long/2addr p0, v1

    goto :goto_a
.end method

.method private final getWireType(I)I
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x2f

    .line 1463
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 1465
    packed-switch v0, :pswitch_data_3e

    .line 1500
    :pswitch_9
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupp.Type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1482
    :pswitch_34
    const/4 v0, 0x0

    .line 1498
    :goto_35
    :pswitch_35
    return v0

    .line 1488
    :pswitch_36
    const/4 v0, 0x2

    goto :goto_35

    .line 1492
    :pswitch_38
    const/4 v0, 0x1

    goto :goto_35

    .line 1496
    :pswitch_3a
    const/4 v0, 0x5

    goto :goto_35

    .line 1498
    :pswitch_3c
    const/4 v0, 0x3

    goto :goto_35

    .line 1465
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_35
        :pswitch_35
        :pswitch_35
        :pswitch_35
        :pswitch_9
        :pswitch_35
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_35
        :pswitch_38
        :pswitch_3a
        :pswitch_34
        :pswitch_34
        :pswitch_34
        :pswitch_38
        :pswitch_3a
        :pswitch_34
        :pswitch_36
        :pswitch_3c
        :pswitch_36
        :pswitch_36
        :pswitch_34
        :pswitch_34
        :pswitch_3a
        :pswitch_38
        :pswitch_34
        :pswitch_34
        :pswitch_36
        :pswitch_36
    .end packed-switch
.end method

.method private insertObject(IILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1509
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;Z)V

    .line 1510
    return-void
.end method

.method private insertObject(IILjava/lang/Object;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1519
    invoke-static {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->checkTag(I)V

    .line 1520
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 1521
    const/4 v0, 0x0

    .line 1522
    instance-of v2, v1, Ljava/util/Vector;

    if-eqz v2, :cond_11

    move-object v0, v1

    .line 1523
    check-cast v0, Ljava/util/Vector;

    .line 1525
    :cond_11
    if-eqz v1, :cond_1b

    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_1f

    .line 1526
    :cond_1b
    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1540
    :goto_1e
    return-void

    .line 1528
    :cond_1f
    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1529
    if-nez v0, :cond_31

    .line 1530
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 1531
    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1532
    iget-object v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v1, p1, v0}, Lcom/google/googlenav/common/util/h;->a(ILjava/lang/Object;)V

    .line 1534
    :cond_31
    if-eqz p4, :cond_37

    .line 1535
    invoke-virtual {v0, p3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1e

    .line 1537
    :cond_37
    invoke-virtual {v0, p3, p2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_1e
.end method

.method private isZigZagEncodedType(I)Z
    .registers 4
    .parameter

    .prologue
    .line 940
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 941
    const/16 v1, 0x21

    if-eq v0, v1, :cond_c

    const/16 v1, 0x22

    if-ne v0, v1, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private outputField(ILcom/google/googlenav/common/io/MarkedOutputStream;)I
    .registers 18
    .parameter
    .parameter

    .prologue
    .line 854
    invoke-virtual/range {p0 .. p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    .line 855
    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getWireType(I)I

    move-result v9

    .line 856
    shl-int/lit8 v1, p1, 0x3

    or-int v10, v1, v9

    .line 859
    const/4 v2, 0x0

    .line 861
    const/4 v1, 0x0

    move v7, v1

    move v1, v2

    :goto_10
    if-ge v7, v8, :cond_f9

    .line 862
    int-to-long v2, v10

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    move-result v2

    add-int v4, v1, v2

    .line 863
    const/4 v3, 0x0

    .line 864
    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/common/io/MarkedOutputStream;->availableContent()I

    move-result v11

    .line 865
    packed-switch v9, :pswitch_data_fa

    .line 918
    :pswitch_23
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 868
    :pswitch_29
    const/16 v1, 0x13

    move/from16 v0, p1

    invoke-direct {p0, v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 870
    const/4 v1, 0x5

    if-ne v9, v1, :cond_4d

    const/4 v1, 0x4

    .line 871
    :goto_3b
    const/4 v2, 0x0

    :goto_3c
    if-ge v2, v1, :cond_50

    .line 872
    const-wide/16 v12, 0xff

    and-long/2addr v12, v5

    long-to-int v12, v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/google/googlenav/common/io/MarkedOutputStream;->write(I)V

    .line 873
    const/16 v12, 0x8

    shr-long/2addr v5, v12

    .line 871
    add-int/lit8 v2, v2, 0x1

    goto :goto_3c

    .line 870
    :cond_4d
    const/16 v1, 0x8

    goto :goto_3b

    :cond_50
    move v2, v3

    move v1, v4

    .line 920
    :goto_52
    if-nez v2, :cond_5a

    .line 921
    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/common/io/MarkedOutputStream;->availableContent()I

    move-result v2

    sub-int/2addr v2, v11

    add-int/2addr v1, v2

    .line 861
    :cond_5a
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_10

    .line 878
    :pswitch_5e
    const/16 v1, 0x13

    move/from16 v0, p1

    invoke-direct {p0, v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 879
    invoke-direct/range {p0 .. p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v5

    if-eqz v5, :cond_76

    .line 880
    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->zigZagEncode(J)J

    move-result-wide v1

    .line 882
    :cond_76
    move-object/from16 v0, p2

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    move v2, v3

    move v1, v4

    .line 883
    goto :goto_52

    .line 886
    :pswitch_7e
    invoke-virtual/range {p0 .. p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v1

    const/16 v2, 0x1b

    if-ne v1, v2, :cond_a5

    const/16 v1, 0x10

    :goto_88
    move/from16 v0, p1

    invoke-direct {p0, v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v1

    .line 891
    instance-of v2, v1, [B

    if-eqz v2, :cond_a8

    .line 892
    check-cast v1, [B

    check-cast v1, [B

    .line 893
    array-length v2, v1

    int-to-long v5, v2

    move-object/from16 v0, p2

    invoke-static {v0, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    .line 894
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/MarkedOutputStream;->write([B)V

    move v2, v3

    move v1, v4

    .line 895
    goto :goto_52

    .line 886
    :cond_a5
    const/16 v1, 0x19

    goto :goto_88

    .line 896
    :cond_a8
    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/common/io/MarkedOutputStream;->availableContent()I

    move-result v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/MarkedOutputStream;->addMarker(I)V

    .line 898
    invoke-virtual/range {p2 .. p2}, Lcom/google/googlenav/common/io/MarkedOutputStream;->numMarkers()I

    move-result v2

    .line 899
    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/io/MarkedOutputStream;->addMarker(I)V

    .line 900
    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p2

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputToInternal(Lcom/google/googlenav/common/io/MarkedOutputStream;)I

    move-result v1

    .line 902
    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/MarkedOutputStream;->setMarker(II)V

    .line 904
    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v2

    add-int/2addr v1, v2

    add-int v2, v4, v1

    .line 905
    const/4 v1, 0x1

    move v14, v1

    move v1, v2

    move v2, v14

    .line 907
    goto/16 :goto_52

    .line 911
    :pswitch_d6
    const/16 v1, 0x1a

    move/from16 v0, p1

    invoke-direct {p0, v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p2

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputToInternal(Lcom/google/googlenav/common/io/MarkedOutputStream;)I

    move-result v1

    add-int/2addr v1, v4

    .line 913
    shl-int/lit8 v2, p1, 0x3

    or-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    move-result v2

    add-int/2addr v2, v1

    .line 914
    const/4 v1, 0x1

    move v14, v1

    move v1, v2

    move v2, v14

    .line 915
    goto/16 :goto_52

    .line 924
    :cond_f9
    return v1

    .line 865
    :pswitch_data_fa
    .packed-switch 0x0
        :pswitch_5e
        :pswitch_29
        :pswitch_7e
        :pswitch_d6
        :pswitch_23
        :pswitch_29
    .end packed-switch
.end method

.method private outputTo(Ljava/io/OutputStream;Z)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 808
    new-instance v3, Lcom/google/googlenav/common/io/MarkedOutputStream;

    invoke-direct {v3}, Lcom/google/googlenav/common/io/MarkedOutputStream;-><init>()V

    .line 809
    invoke-direct {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputToInternal(Lcom/google/googlenav/common/io/MarkedOutputStream;)I

    move-result v2

    .line 811
    if-eqz p2, :cond_12

    move-object v0, p1

    .line 813
    check-cast v0, Ljava/io/DataOutput;

    invoke-interface {v0, v2}, Ljava/io/DataOutput;->writeInt(I)V

    .line 817
    :cond_12
    invoke-virtual {v3}, Lcom/google/googlenav/common/io/MarkedOutputStream;->numMarkers()I

    move-result v4

    move v0, v1

    :goto_17
    if-ge v0, v4, :cond_30

    .line 818
    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/MarkedOutputStream;->getMarker(I)I

    move-result v2

    .line 819
    sub-int v5, v2, v1

    invoke-virtual {v3, p1, v1, v5}, Lcom/google/googlenav/common/io/MarkedOutputStream;->writeContentsTo(Ljava/io/OutputStream;II)V

    .line 820
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/common/io/MarkedOutputStream;->getMarker(I)I

    move-result v1

    int-to-long v5, v1

    invoke-static {p1, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    .line 817
    add-int/lit8 v0, v0, 0x2

    move v1, v2

    goto :goto_17

    .line 823
    :cond_30
    invoke-virtual {v3}, Lcom/google/googlenav/common/io/MarkedOutputStream;->availableContent()I

    move-result v0

    if-ge v1, v0, :cond_3e

    .line 824
    invoke-virtual {v3}, Lcom/google/googlenav/common/io/MarkedOutputStream;->availableContent()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-virtual {v3, p1, v1, v0}, Lcom/google/googlenav/common/io/MarkedOutputStream;->writeContentsTo(Ljava/io/OutputStream;II)V

    .line 826
    :cond_3e
    return-void
.end method

.method private outputToInternal(Lcom/google/googlenav/common/io/MarkedOutputStream;)I
    .registers 5
    .parameter

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/h;->b()Lcom/google/googlenav/common/util/i;

    move-result-object v1

    .line 836
    const/4 v0, 0x0

    .line 837
    :goto_7
    invoke-virtual {v1}, Lcom/google/googlenav/common/util/i;->a()Z

    move-result v2

    if-eqz v2, :cond_17

    .line 838
    invoke-virtual {v1}, Lcom/google/googlenav/common/util/i;->b()I

    move-result v2

    .line 839
    invoke-direct {p0, v2, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputField(ILcom/google/googlenav/common/io/MarkedOutputStream;)I

    move-result v2

    add-int/2addr v0, v2

    .line 840
    goto :goto_7

    .line 841
    :cond_17
    return v0
.end method

.method private parseInternal(Ljava/io/InputStream;IZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)I
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 466
    if-eqz p3, :cond_5

    .line 467
    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clear()V

    :cond_5
    move v0, p2

    .line 469
    :goto_6
    if-lez v0, :cond_13

    .line 470
    const/4 v1, 0x1

    invoke-static {p1, v1, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v3

    .line 472
    const-wide/16 v1, -0x1

    cmp-long v1, v3, v1

    if-nez v1, :cond_1b

    .line 553
    :cond_13
    :goto_13
    if-gez v0, :cond_e9

    .line 554
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 475
    :cond_1b
    iget v1, p4, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    sub-int v2, v0, v1

    .line 476
    long-to-int v0, v3

    and-int/lit8 v0, v0, 0x7

    .line 477
    const/4 v1, 0x4

    if-ne v0, v1, :cond_27

    move v0, v2

    .line 478
    goto :goto_13

    .line 480
    :cond_27
    const/4 v1, 0x3

    ushr-long/2addr v3, v1

    long-to-int v6, v3

    .line 482
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v1

    .line 483
    const/16 v3, 0x10

    if-ne v1, v3, :cond_46

    .line 484
    iget-object v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    if-nez v1, :cond_3d

    .line 485
    new-instance v1, Lcom/google/googlenav/common/util/h;

    invoke-direct {v1}, Lcom/google/googlenav/common/util/h;-><init>()V

    iput-object v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    .line 487
    :cond_3d
    iget-object v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    invoke-static {v0}, Lcom/google/googlenav/common/util/p;->a(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v6, v3}, Lcom/google/googlenav/common/util/h;->a(ILjava/lang/Object;)V

    .line 493
    :cond_46
    packed-switch v0, :pswitch_data_ea

    .line 547
    :pswitch_49
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown wire type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", reading garbage data?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 495
    :pswitch_68
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v0

    .line 496
    iget v3, p4, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    sub-int/2addr v2, v3

    .line 497
    invoke-direct {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v3

    if-eqz v3, :cond_7a

    .line 498
    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->zigZagDecode(J)J

    move-result-wide v0

    .line 500
    :cond_7a
    invoke-static {v0, v1}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object v0

    move v1, v2

    .line 550
    :cond_7f
    :goto_7f
    invoke-direct {p0, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    move v0, v1

    .line 551
    goto :goto_6

    .line 506
    :pswitch_84
    const-wide/16 v4, 0x0

    .line 507
    const/4 v3, 0x0

    .line 508
    const/4 v1, 0x5

    if-ne v0, v1, :cond_9d

    const/4 v0, 0x4

    .line 509
    :goto_8b
    sub-int v1, v2, v0

    .line 511
    :goto_8d
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_a0

    .line 512
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    int-to-long v7, v0

    .line 513
    shl-long/2addr v7, v3

    or-long/2addr v4, v7

    .line 514
    add-int/lit8 v0, v3, 0x8

    move v3, v0

    move v0, v2

    .line 515
    goto :goto_8d

    .line 508
    :cond_9d
    const/16 v0, 0x8

    goto :goto_8b

    .line 517
    :cond_a0
    invoke-static {v4, v5}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_7f

    .line 521
    :pswitch_a5
    const/4 v0, 0x0

    invoke-static {p1, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v0

    long-to-int v3, v0

    .line 522
    iget v0, p4, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    sub-int v0, v2, v0

    .line 523
    sub-int v1, v0, v3

    .line 525
    if-nez v3, :cond_c8

    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->EMPTY_BYTE_ARRAY:[B

    .line 526
    :goto_b5
    const/4 v2, 0x0

    .line 527
    :goto_b6
    if-ge v2, v3, :cond_7f

    .line 528
    sub-int v4, v3, v2

    invoke-virtual {p1, v0, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 529
    if-gtz v4, :cond_cb

    .line 530
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unexp.EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 525
    :cond_c8
    new-array v0, v3, [B

    goto :goto_b5

    .line 532
    :cond_cb
    add-int/2addr v2, v4

    goto :goto_b6

    .line 538
    :pswitch_cd
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-nez v0, :cond_e0

    const/4 v0, 0x0

    :goto_d4
    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 542
    const/4 v0, 0x0

    invoke-direct {v1, p1, v2, v0, p4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parseInternal(Ljava/io/InputStream;IZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)I

    move-result v0

    move-object v9, v1

    move v1, v0

    move-object v0, v9

    .line 544
    goto :goto_7f

    .line 538
    :cond_e0
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    goto :goto_d4

    .line 557
    :cond_e9
    return v0

    .line 493
    :pswitch_data_ea
    .packed-switch 0x0
        :pswitch_68
        :pswitch_84
        :pswitch_a5
        :pswitch_cd
        :pswitch_49
        :pswitch_84
    .end packed-switch
.end method

.method static readVarInt(Ljava/io/InputStream;Z)J
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1639
    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->NULL_COUNTER:Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;

    invoke-static {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static readVarInt(Ljava/io/InputStream;ZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)J
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1659
    const-wide/16 v1, 0x0

    .line 1662
    iput v0, p2, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    move v3, v0

    move v7, v0

    move-wide v8, v1

    move-wide v0, v8

    move v2, v7

    .line 1666
    :goto_a
    const/16 v4, 0xa

    if-ge v2, v4, :cond_2d

    .line 1667
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v4

    .line 1669
    const/4 v5, -0x1

    if-ne v4, v5, :cond_24

    .line 1670
    if-nez v2, :cond_1c

    if-eqz p1, :cond_1c

    .line 1671
    const-wide/16 v0, -0x1

    .line 1685
    :goto_1b
    return-wide v0

    .line 1673
    :cond_1c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "EOF"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1676
    :cond_24
    and-int/lit8 v5, v4, 0x7f

    int-to-long v5, v5

    shl-long/2addr v5, v3

    or-long/2addr v0, v5

    .line 1678
    and-int/lit16 v4, v4, 0x80

    if-nez v4, :cond_32

    .line 1684
    :cond_2d
    add-int/lit8 v2, v2, 0x1

    iput v2, p2, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    goto :goto_1b

    .line 1682
    :cond_32
    add-int/lit8 v3, v3, 0x7

    .line 1666
    add-int/lit8 v2, v2, 0x1

    goto :goto_a
.end method

.method private setObject(IILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1711
    if-gez p1, :cond_8

    .line 1712
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1714
    :cond_8
    if-eqz p3, :cond_d

    .line 1715
    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1717
    :cond_d
    if-gez p2, :cond_15

    .line 1718
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1721
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1722
    instance-of v1, v0, Ljava/util/Vector;

    if-eqz v1, :cond_2f

    .line 1724
    check-cast v0, Ljava/util/Vector;

    .line 1725
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-ne p2, v1, :cond_2b

    .line 1726
    invoke-virtual {v0, p3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1754
    :goto_2a
    return-object p0

    .line 1728
    :cond_2b
    invoke-virtual {v0, p3, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_2a

    .line 1730
    :cond_2f
    if-nez v0, :cond_3d

    .line 1732
    if-lez p2, :cond_39

    .line 1733
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1735
    :cond_39
    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_2a

    .line 1738
    :cond_3d
    packed-switch p2, :pswitch_data_5c

    .line 1751
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1740
    :pswitch_46
    invoke-direct {p0, p1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_2a

    .line 1745
    :pswitch_4a
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 1746
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1747
    invoke-virtual {v1, p3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1748
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/common/util/h;->a(ILjava/lang/Object;)V

    goto :goto_2a

    .line 1738
    nop

    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_46
        :pswitch_4a
    .end packed-switch
.end method

.method private setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1693
    if-gez p1, :cond_8

    .line 1694
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1696
    :cond_8
    if-eqz p2, :cond_d

    .line 1697
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1699
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/common/util/h;->a(ILjava/lang/Object;)V

    .line 1700
    return-object p0
.end method

.method private spaces(I)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 1002
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1003
    const/4 v0, 0x0

    :goto_6
    if-ge v0, p1, :cond_10

    .line 1004
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1003
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1006
    :cond_10
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private toTextFormat(Ljava/io/Writer;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 990
    mul-int/lit8 v0, p2, 0x2

    invoke-direct {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->spaces(I)Ljava/lang/String;

    move-result-object v5

    move v3, v6

    .line 991
    :goto_8
    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->maxTag()I

    move-result v0

    if-gt v3, v0, :cond_21

    move v4, v6

    .line 992
    :goto_f
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-ge v4, v0, :cond_1e

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    .line 993
    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->fieldToTextFormat(Ljava/io/Writer;IIILjava/lang/String;)V

    .line 992
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 991
    :cond_1e
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 996
    :cond_21
    return-void
.end method

.method static writeVarInt(Ljava/io/OutputStream;J)I
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1775
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0xa

    if-ge v0, v1, :cond_16

    .line 1777
    const-wide/16 v1, 0x7f

    and-long/2addr v1, p1

    long-to-int v1, v1

    .line 1779
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    .line 1781
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_17

    .line 1782
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1783
    add-int/lit8 v0, v0, 0x1

    .line 1788
    :cond_16
    return v0

    .line 1785
    :cond_17
    or-int/lit16 v1, v1, 0x80

    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1775
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static zigZagDecode(J)J
    .registers 6
    .parameter

    .prologue
    .line 957
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long/2addr v0, v2

    .line 958
    return-wide v0
.end method

.method private static zigZagEncode(J)J
    .registers 6
    .parameter

    .prologue
    .line 949
    const/4 v0, 0x1

    shl-long v0, p0, v0

    const/16 v2, 0x3f

    ushr-long v2, p0, v2

    neg-long v2, v2

    xor-long/2addr v0, v2

    .line 950
    return-wide v0
.end method


# virtual methods
.method public addBool(IZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 154
    if-eqz p2, :cond_8

    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    :goto_4
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    .line 155
    return-void

    .line 154
    :cond_8
    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_4
.end method

.method public addBytes(I[B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 161
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    .line 162
    return-void
.end method

.method public addDouble(ID)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 189
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 190
    return-void
.end method

.method public addFloat(IF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 183
    return-void
.end method

.method public addInt(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 168
    int-to-long v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    .line 169
    return-void
.end method

.method public addLong(IJ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-static {p2, p3}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    .line 176
    return-void
.end method

.method public addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 196
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    .line 197
    return-void
.end method

.method public addString(ILjava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 203
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    .line 204
    return-void
.end method

.method public clear()V
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/h;->f()V

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    .line 141
    return-void
.end method

.method public clone()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3

    .prologue
    .line 127
    :try_start_0
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 128
    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_10} :catch_11

    .line 129
    return-object v0

    .line 130
    :catch_11
    move-exception v0

    .line 131
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not serialize and parse ProtoBuf."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->clone()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public createGroup(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 147
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    return-object v1
.end method

.method public getBool(I)Z
    .registers 3
    .parameter

    .prologue
    .line 210
    const/16 v0, 0x18

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getBool(II)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 218
    const/16 v0, 0x18

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getBytes(I)[B
    .registers 3
    .parameter

    .prologue
    .line 226
    const/16 v0, 0x19

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getBytes(II)[B
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 233
    const/16 v0, 0x19

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getCount(I)I
    .registers 3
    .parameter

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getDataSize()I
    .registers 2

    .prologue
    .line 629
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCachedDataSize(Z)I

    move-result v0

    return v0
.end method

.method public getDouble(I)D
    .registers 4
    .parameter

    .prologue
    .line 283
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public getDouble(II)D
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 290
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .registers 3
    .parameter

    .prologue
    .line 269
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public getFloat(II)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 276
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .registers 4
    .parameter

    .prologue
    .line 240
    const/16 v0, 0x15

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getInt(II)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 247
    const/16 v0, 0x15

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getLong(I)J
    .registers 4
    .parameter

    .prologue
    .line 255
    const/16 v0, 0x13

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLong(II)J
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 262
    const/16 v0, 0x13

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getNumBytesUsed()I
    .registers 5

    .prologue
    .line 668
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/h;->g()I

    move-result v0

    .line 669
    iget-object v1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v1}, Lcom/google/googlenav/common/util/h;->b()Lcom/google/googlenav/common/util/i;

    move-result-object v1

    .line 670
    :goto_c
    invoke-virtual {v1}, Lcom/google/googlenav/common/util/i;->a()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 671
    invoke-virtual {v1}, Lcom/google/googlenav/common/util/i;->b()I

    move-result v2

    .line 672
    iget-object v3, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v2

    .line 673
    invoke-static {v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getNumBytesUsed(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 674
    goto :goto_c

    .line 675
    :cond_22
    return v0
.end method

.method public getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3
    .parameter

    .prologue
    .line 297
    const/16 v0, 0x1a

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 305
    const/16 v0, 0x1a

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public getProtoBufExtension(Lcom/google/googlenav/common/io/protocol/Extension;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter

    .prologue
    .line 312
    iget v0, p1, Lcom/google/googlenav/common/io/protocol/Extension;->fieldNumber:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v1

    .line 313
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v2, p1, Lcom/google/googlenav/common/io/protocol/Extension;->messageType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 315
    :try_start_d
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_11

    .line 320
    :goto_10
    return-object v0

    .line 316
    :catch_11
    move-exception v0

    .line 317
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 318
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public getString(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 328
    const/16 v0, 0x1c

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getString(II)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 336
    const/16 v0, 0x1c

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getType(I)I
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x10

    .line 603
    .line 604
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-eqz v0, :cond_3d

    .line 605
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->getType(I)I

    move-result v2

    .line 608
    :goto_d
    if-ne v2, v3, :cond_3b

    .line 609
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 610
    :goto_1b
    if-eqz v0, :cond_3b

    .line 611
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 615
    :goto_21
    if-ne v0, v3, :cond_36

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-lez v2, :cond_36

    .line 616
    invoke-direct {p0, p1, v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    .line 618
    instance-of v2, v0, Ljava/lang/Long;

    if-nez v2, :cond_35

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_39

    :cond_35
    move v0, v1

    .line 622
    :cond_36
    :goto_36
    return v0

    .line 609
    :cond_37
    const/4 v0, 0x0

    goto :goto_1b

    .line 618
    :cond_39
    const/4 v0, 0x2

    goto :goto_36

    :cond_3b
    move v0, v2

    goto :goto_21

    :cond_3d
    move v2, v3

    goto :goto_d
.end method

.method public getType()Lcom/google/googlenav/common/io/protocol/ProtoBufType;
    .registers 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    return-object v0
.end method

.method public has(I)Z
    .registers 3
    .parameter

    .prologue
    .line 389
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->hasFieldSet(I)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getDefault(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public hasFieldSet(I)Z
    .registers 3
    .parameter

    .prologue
    .line 377
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public insertBool(IIZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1245
    if-eqz p3, :cond_8

    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    :goto_4
    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    .line 1246
    return-void

    .line 1245
    :cond_8
    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_4
.end method

.method public insertBytes(II[B)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1252
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    .line 1253
    return-void
.end method

.method public insertDouble(IID)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1280
    invoke-static {p3, p4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertLong(IIJ)V

    .line 1281
    return-void
.end method

.method public insertFloat(IIF)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1273
    invoke-static {p3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertInt(III)V

    .line 1274
    return-void
.end method

.method public insertInt(III)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1259
    int-to-long v0, p3

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertLong(IIJ)V

    .line 1260
    return-void
.end method

.method public insertLong(IIJ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1266
    invoke-static {p3, p4}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    .line 1267
    return-void
.end method

.method public insertProtoBuf(IILcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1287
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    .line 1288
    return-void
.end method

.method public insertString(IILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1294
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;)V

    .line 1295
    return-void
.end method

.method public isValid()Z
    .registers 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->isValidProto(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public maxTag()I
    .registers 2

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/h;->c()I

    move-result v0

    return v0
.end method

.method public outputTo(Ljava/io/OutputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 787
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;Z)V

    .line 788
    return-void
.end method

.method public outputWithSizeTo(Ljava/io/OutputStream;)V
    .registers 3
    .parameter

    .prologue
    .line 777
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;Z)V

    .line 778
    return-void
.end method

.method public parse(Ljava/io/InputStream;I)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 447
    const/4 v0, 0x1

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf$1;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parseInternal(Ljava/io/InputStream;IZLcom/google/googlenav/common/io/protocol/ProtoBuf$SimpleCounter;)I

    move-result v0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3
    .parameter

    .prologue
    .line 417
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 418
    return-object p0
.end method

.method public parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 402
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 403
    return-object p0
.end method

.method public remove(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 564
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    .line 565
    if-lt p2, v0, :cond_c

    .line 566
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 568
    :cond_c
    const/4 v1, 0x1

    if-ne v0, v1, :cond_15

    .line 569
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->b(I)Ljava/lang/Object;

    .line 574
    :goto_14
    return-void

    .line 571
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    .line 572
    invoke-virtual {v0, p2}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_14
.end method

.method public setBool(IIZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1087
    if-eqz p3, :cond_9

    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    :goto_4
    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(IILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0

    :cond_9
    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_4
.end method

.method public setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1077
    if-eqz p2, :cond_9

    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    :goto_4
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0

    :cond_9
    sget-object v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_4
.end method

.method public setBytes(II[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1104
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(IILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1094
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setDouble(ID)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1145
    invoke-static {p2, p3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setDouble(IID)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1155
    invoke-static {p3, p4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IIJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setFloat(IF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1162
    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setFloat(IIF)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1172
    invoke-static {p3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1111
    int-to-long v0, p2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setInt(III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1121
    int-to-long v0, p3

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IIJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setLong(IIJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1138
    invoke-static {p3, p4}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(IILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1128
    invoke-static {p2, p3}, Lcom/google/googlenav/common/util/p;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setProtoBuf(IILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1221
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(IILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1179
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setProtoBufExtension(Lcom/google/googlenav/common/io/protocol/Extension;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1186
    iget v0, p1, Lcom/google/googlenav/common/io/protocol/Extension;->fieldNumber:I

    if-gez v0, :cond_a

    .line 1187
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1190
    :cond_a
    iget-object v0, p2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    iget-object v1, p1, Lcom/google/googlenav/common/io/protocol/Extension;->messageType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-eq v0, v1, :cond_37

    .line 1191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type mismatch type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extension type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/googlenav/common/io/protocol/Extension;->messageType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1195
    :cond_37
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    iget v1, p1, Lcom/google/googlenav/common/io/protocol/Extension;->fieldNumber:I

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/util/h;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 1196
    instance-of v1, v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz v1, :cond_74

    .line 1198
    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 1199
    iget-object v1, p1, Lcom/google/googlenav/common/io/protocol/Extension;->messageType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBufType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9b

    .line 1200
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type mismatch old type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " new type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/google/googlenav/common/io/protocol/Extension;->messageType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1203
    :cond_74
    if-eqz v0, :cond_9b

    .line 1205
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type mismatch old type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " new type:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, Lcom/google/googlenav/common/io/protocol/Extension;->messageType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1209
    :cond_9b
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    iget v1, p1, Lcom/google/googlenav/common/io/protocol/Extension;->fieldNumber:I

    invoke-virtual {p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/util/h;->a(ILjava/lang/Object;)V

    .line 1210
    return-object p0
.end method

.method public setString(IILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1238
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(IILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1228
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method setType(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3
    .parameter

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->values:Lcom/google/googlenav/common/util/h;

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/h;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-eqz v0, :cond_18

    if-eqz p1, :cond_18

    iget-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    if-eq p1, v0, :cond_18

    .line 356
    :cond_12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 358
    :cond_18
    iput-object p1, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->msgType:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    .line 359
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->wireTypes:Lcom/google/googlenav/common/util/h;

    .line 360
    return-object p0
.end method

.method public toByteArray()[B
    .registers 2

    .prologue
    .line 967
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 968
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 969
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 1759
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 1761
    :try_start_5
    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toTextFormat(Ljava/io/Writer;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_8} :catch_d

    .line 1765
    :goto_8
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1762
    :catch_d
    move-exception v1

    .line 1763
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/StringWriter;->write(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public toTextFormat(Ljava/io/Writer;)V
    .registers 3
    .parameter

    .prologue
    .line 986
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toTextFormat(Ljava/io/Writer;I)V

    .line 987
    return-void
.end method
