.class public Lcom/google/googlenav/common/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/common/io/j;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/j;)V
    .registers 2
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/googlenav/common/j;->a:Lcom/google/googlenav/common/io/j;

    .line 33
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-interface {p0, p1}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v1

    .line 47
    if-nez v1, :cond_8

    .line 48
    const/4 v0, 0x0

    .line 51
    :goto_7
    return-object v0

    :cond_8
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_7
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v1

    .line 178
    if-nez v1, :cond_8

    .line 196
    :goto_7
    return-object v0

    .line 183
    :cond_8
    packed-switch p2, :pswitch_data_50

    .line 193
    :try_start_b
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad class: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :catch_2e
    move-exception v1

    goto :goto_7

    .line 185
    :pswitch_30
    invoke-interface {v1}, Ljava/io/DataInput;->readBoolean()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_7

    .line 187
    :pswitch_39
    invoke-interface {v1}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_7

    .line 189
    :pswitch_42
    invoke-interface {v1}, Ljava/io/DataInput;->readLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_7

    .line 191
    :pswitch_4b
    invoke-interface {v1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_4e} :catch_2e

    move-result-object v0

    goto :goto_7

    .line 183
    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_30
        :pswitch_39
        :pswitch_42
        :pswitch_4b
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 94
    if-nez p2, :cond_9

    .line 95
    iget-object v0, p0, Lcom/google/googlenav/common/j;->a:Lcom/google/googlenav/common/io/j;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 120
    :goto_8
    return-void

    .line 99
    :cond_9
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 100
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 104
    :try_start_13
    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_2f

    .line 105
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeBoolean(Z)V

    .line 116
    :goto_20
    iget-object v1, p0, Lcom/google/googlenav/common/j;->a:Lcom/google/googlenav/common/io/j;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, p1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_29} :catch_2a

    goto :goto_8

    .line 117
    :catch_2a
    move-exception v0

    .line 118
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 106
    :cond_2f
    :try_start_2f
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_39

    .line 107
    check-cast p2, Ljava/lang/String;

    invoke-interface {v1, p2}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    goto :goto_20

    .line 108
    :cond_39
    instance-of v2, p2, Ljava/lang/Integer;

    if-eqz v2, :cond_47

    .line 109
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    goto :goto_20

    .line 110
    :cond_47
    instance-of v2, p2, Ljava/lang/Long;

    if-eqz v2, :cond_55

    .line 111
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Ljava/io/DataOutput;->writeLong(J)V

    goto :goto_20

    .line 113
    :cond_55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_7c} :catch_2a
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 127
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 76
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 81
    return-void
.end method

.method public b(Ljava/lang/String;J)J
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 158
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_d

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    :cond_d
    return-wide p2
.end method

.method public b(Ljava/lang/String;)Ljava/io/DataInput;
    .registers 5
    .parameter

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/googlenav/common/j;->a:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0, p1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    .line 213
    if-nez v1, :cond_a

    .line 214
    const/4 v0, 0x0

    .line 217
    :goto_9
    return-object v0

    :cond_a
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_9
.end method

.method public b(Ljava/lang/String;Z)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_d

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_d
    return p2
.end method
