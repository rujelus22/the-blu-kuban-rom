.class public Lcom/google/googlenav/friend/history/c;
.super Lcom/google/googlenav/friend/history/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/friend/history/o;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    .line 29
    return-void
.end method


# virtual methods
.method public a(ZZ)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->m()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 62
    if-eqz p1, :cond_e

    if-eqz p2, :cond_e

    .line 63
    const v0, 0x7f020407

    .line 112
    :goto_d
    return v0

    .line 64
    :cond_e
    if-eqz p1, :cond_14

    .line 65
    const v0, 0x7f020408

    goto :goto_d

    .line 66
    :cond_14
    if-eqz p2, :cond_1a

    .line 67
    const v0, 0x7f020405

    goto :goto_d

    .line 69
    :cond_1a
    const v0, 0x7f020406

    goto :goto_d

    .line 71
    :cond_1e
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->n()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 72
    if-eqz p1, :cond_2c

    if-eqz p2, :cond_2c

    .line 73
    const v0, 0x7f02040b

    goto :goto_d

    .line 74
    :cond_2c
    if-eqz p1, :cond_32

    .line 75
    const v0, 0x7f02040c

    goto :goto_d

    .line 76
    :cond_32
    if-eqz p2, :cond_38

    .line 77
    const v0, 0x7f020409

    goto :goto_d

    .line 79
    :cond_38
    const v0, 0x7f02040a

    goto :goto_d

    .line 81
    :cond_3c
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->o()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 82
    if-eqz p1, :cond_4a

    if-eqz p2, :cond_4a

    .line 83
    const v0, 0x7f02041d

    goto :goto_d

    .line 84
    :cond_4a
    if-eqz p1, :cond_50

    .line 85
    const v0, 0x7f02041e

    goto :goto_d

    .line 86
    :cond_50
    if-eqz p2, :cond_56

    .line 87
    const v0, 0x7f02041b

    goto :goto_d

    .line 89
    :cond_56
    const v0, 0x7f02041c

    goto :goto_d

    .line 91
    :cond_5a
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/c;->p()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 92
    if-eqz p1, :cond_68

    if-eqz p2, :cond_68

    .line 93
    const v0, 0x7f0203ff

    goto :goto_d

    .line 94
    :cond_68
    if-eqz p1, :cond_6e

    .line 95
    const v0, 0x7f020400

    goto :goto_d

    .line 96
    :cond_6e
    if-eqz p2, :cond_74

    .line 97
    const v0, 0x7f0203fd

    goto :goto_d

    .line 99
    :cond_74
    const v0, 0x7f0203fe

    goto :goto_d

    .line 102
    :cond_78
    if-eqz p1, :cond_80

    if-eqz p2, :cond_80

    .line 103
    const v0, 0x7f020419

    goto :goto_d

    .line 104
    :cond_80
    if-eqz p1, :cond_86

    .line 105
    const v0, 0x7f02041a

    goto :goto_d

    .line 106
    :cond_86
    if-eqz p2, :cond_8c

    .line 107
    const v0, 0x7f020417

    goto :goto_d

    .line 109
    :cond_8c
    const v0, 0x7f020418

    goto/16 :goto_d
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/googlenav/friend/history/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_12

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public b()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v3, 0x2

    .line 43
    iget-object v0, p0, Lcom/google/googlenav/friend/history/c;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 44
    const/4 v0, 0x0

    .line 45
    if-eqz v1, :cond_15

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 46
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getFloat(I)F

    move-result v0

    .line 48
    :cond_15
    float-to-int v0, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/l;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
