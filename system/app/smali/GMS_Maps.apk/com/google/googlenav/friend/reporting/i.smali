.class final enum Lcom/google/googlenav/friend/reporting/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/googlenav/friend/reporting/i;

.field public static final enum b:Lcom/google/googlenav/friend/reporting/i;

.field public static final enum c:Lcom/google/googlenav/friend/reporting/i;

.field public static final enum d:Lcom/google/googlenav/friend/reporting/i;

.field private static final synthetic e:[Lcom/google/googlenav/friend/reporting/i;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/google/googlenav/friend/reporting/i;

    const-string v1, "DO_NOT_RECORD"

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/reporting/i;->a:Lcom/google/googlenav/friend/reporting/i;

    .line 35
    new-instance v0, Lcom/google/googlenav/friend/reporting/i;

    const-string v1, "DO_NOT_RECORD_THROTTLED"

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/friend/reporting/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/reporting/i;->b:Lcom/google/googlenav/friend/reporting/i;

    .line 38
    new-instance v0, Lcom/google/googlenav/friend/reporting/i;

    const-string v1, "FIRST_RECORD"

    invoke-direct {v0, v1, v4}, Lcom/google/googlenav/friend/reporting/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/reporting/i;->c:Lcom/google/googlenav/friend/reporting/i;

    .line 41
    new-instance v0, Lcom/google/googlenav/friend/reporting/i;

    const-string v1, "RECORDING_ALL"

    invoke-direct {v0, v1, v5}, Lcom/google/googlenav/friend/reporting/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/reporting/i;->d:Lcom/google/googlenav/friend/reporting/i;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/googlenav/friend/reporting/i;

    sget-object v1, Lcom/google/googlenav/friend/reporting/i;->a:Lcom/google/googlenav/friend/reporting/i;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/googlenav/friend/reporting/i;->b:Lcom/google/googlenav/friend/reporting/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/googlenav/friend/reporting/i;->c:Lcom/google/googlenav/friend/reporting/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/googlenav/friend/reporting/i;->d:Lcom/google/googlenav/friend/reporting/i;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/googlenav/friend/reporting/i;->e:[Lcom/google/googlenav/friend/reporting/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/googlenav/friend/reporting/i;
    .registers 2
    .parameter

    .prologue
    .line 29
    const-class v0, Lcom/google/googlenav/friend/reporting/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/reporting/i;

    return-object v0
.end method

.method public static values()[Lcom/google/googlenav/friend/reporting/i;
    .registers 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/googlenav/friend/reporting/i;->e:[Lcom/google/googlenav/friend/reporting/i;

    invoke-virtual {v0}, [Lcom/google/googlenav/friend/reporting/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/friend/reporting/i;

    return-object v0
.end method
