.class public Lcom/google/googlenav/friend/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:Ljava/lang/Integer;

.field private d:I

.field private e:I

.field private f:I

.field private g:J

.field private h:I

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private r:Lcom/google/googlenav/friend/bf;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    iput v2, p0, Lcom/google/googlenav/friend/bg;->d:I

    .line 455
    iput v2, p0, Lcom/google/googlenav/friend/bg;->e:I

    .line 456
    const v0, 0x1869f

    iput v0, p0, Lcom/google/googlenav/friend/bg;->f:I

    .line 457
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/friend/bg;->g:J

    .line 458
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/googlenav/friend/bg;->h:I

    .line 459
    iput-boolean v2, p0, Lcom/google/googlenav/friend/bg;->i:Z

    .line 465
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/friend/bg;->o:I

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->a:I

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->b:I

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/friend/bg;)Ljava/lang/Integer;
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->d:I

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->e:I

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->f:I

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/friend/bg;)Lcom/google/googlenav/friend/bf;
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->r:Lcom/google/googlenav/friend/bf;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/friend/bg;)J
    .registers 3
    .parameter

    .prologue
    .line 449
    iget-wide v0, p0, Lcom/google/googlenav/friend/bg;->g:J

    return-wide v0
.end method

.method static synthetic i(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->h:I

    return v0
.end method

.method static synthetic j(Lcom/google/googlenav/friend/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/googlenav/friend/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->j:Z

    return v0
.end method

.method static synthetic l(Lcom/google/googlenav/friend/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->k:Z

    return v0
.end method

.method static synthetic m(Lcom/google/googlenav/friend/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->l:Z

    return v0
.end method

.method static synthetic n(Lcom/google/googlenav/friend/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->m:Z

    return v0
.end method

.method static synthetic o(Lcom/google/googlenav/friend/bg;)Z
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/google/googlenav/friend/bg;->n:Z

    return v0
.end method

.method static synthetic p(Lcom/google/googlenav/friend/bg;)I
    .registers 2
    .parameter

    .prologue
    .line 449
    iget v0, p0, Lcom/google/googlenav/friend/bg;->o:I

    return v0
.end method

.method static synthetic q(Lcom/google/googlenav/friend/bg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic r(Lcom/google/googlenav/friend/bg;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/googlenav/friend/bg;->p:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/friend/be;
    .registers 2

    .prologue
    .line 471
    new-instance v0, Lcom/google/googlenav/friend/be;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/be;-><init>(Lcom/google/googlenav/friend/bg;)V

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 478
    iput p1, p0, Lcom/google/googlenav/friend/bg;->a:I

    .line 479
    return-object p0
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 629
    iput-object p1, p0, Lcom/google/googlenav/friend/bg;->q:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 630
    return-object p0
.end method

.method public a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 637
    iput-object p1, p0, Lcom/google/googlenav/friend/bg;->r:Lcom/google/googlenav/friend/bf;

    .line 638
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 620
    iput-object p1, p0, Lcom/google/googlenav/friend/bg;->p:Ljava/lang/String;

    .line 621
    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 557
    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->i:Z

    .line 558
    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 486
    iput p1, p0, Lcom/google/googlenav/friend/bg;->b:I

    .line 487
    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 566
    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->j:Z

    .line 567
    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/friend/bg;
    .registers 3
    .parameter

    .prologue
    .line 494
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/friend/bg;->c:Ljava/lang/Integer;

    .line 495
    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 584
    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->l:Z

    .line 585
    return-object p0
.end method

.method public d(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 503
    iput p1, p0, Lcom/google/googlenav/friend/bg;->d:I

    .line 504
    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 593
    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->m:Z

    .line 594
    return-object p0
.end method

.method public e(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 512
    iput p1, p0, Lcom/google/googlenav/friend/bg;->e:I

    .line 513
    return-object p0
.end method

.method public e(Z)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 602
    iput-boolean p1, p0, Lcom/google/googlenav/friend/bg;->n:Z

    .line 603
    return-object p0
.end method

.method public f(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 522
    iput p1, p0, Lcom/google/googlenav/friend/bg;->f:I

    .line 523
    return-object p0
.end method

.method public g(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 543
    iput p1, p0, Lcom/google/googlenav/friend/bg;->h:I

    .line 544
    return-object p0
.end method

.method public h(I)Lcom/google/googlenav/friend/bg;
    .registers 2
    .parameter

    .prologue
    .line 611
    iput p1, p0, Lcom/google/googlenav/friend/bg;->o:I

    .line 612
    return-object p0
.end method
