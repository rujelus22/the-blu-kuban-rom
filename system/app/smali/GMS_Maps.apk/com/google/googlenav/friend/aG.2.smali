.class public final enum Lcom/google/googlenav/friend/aG;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/googlenav/friend/aG;

.field public static final enum b:Lcom/google/googlenav/friend/aG;

.field public static final enum c:Lcom/google/googlenav/friend/aG;

.field private static final synthetic e:[Lcom/google/googlenav/friend/aG;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 36
    new-instance v0, Lcom/google/googlenav/friend/aG;

    const-string v1, "CIRCLE"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/googlenav/friend/aG;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    .line 39
    new-instance v0, Lcom/google/googlenav/friend/aG;

    const-string v1, "CUSTOM_TARGET"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/googlenav/friend/aG;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    .line 42
    new-instance v0, Lcom/google/googlenav/friend/aG;

    const-string v1, "GAIA_ID"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/googlenav/friend/aG;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    .line 34
    new-array v0, v5, [Lcom/google/googlenav/friend/aG;

    sget-object v1, Lcom/google/googlenav/friend/aG;->a:Lcom/google/googlenav/friend/aG;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/googlenav/friend/aG;->b:Lcom/google/googlenav/friend/aG;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/googlenav/friend/aG;->c:Lcom/google/googlenav/friend/aG;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/googlenav/friend/aG;->e:[Lcom/google/googlenav/friend/aG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lcom/google/googlenav/friend/aG;->d:I

    .line 51
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/googlenav/friend/aG;
    .registers 2
    .parameter

    .prologue
    .line 34
    const-class v0, Lcom/google/googlenav/friend/aG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/aG;

    return-object v0
.end method

.method public static values()[Lcom/google/googlenav/friend/aG;
    .registers 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/googlenav/friend/aG;->e:[Lcom/google/googlenav/friend/aG;

    invoke-virtual {v0}, [Lcom/google/googlenav/friend/aG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/friend/aG;

    return-object v0
.end method
