.class public Lcom/google/googlenav/friend/reporting/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/googlenav/friend/reporting/f;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/friend/reporting/s;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;Lcom/google/googlenav/friend/reporting/f;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    .line 88
    iput-object p2, p0, Lcom/google/googlenav/friend/reporting/s;->c:Lcom/google/googlenav/friend/reporting/f;

    .line 89
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/googlenav/friend/reporting/f;)Lcom/google/googlenav/friend/reporting/s;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 78
    const/4 v0, 0x4

    .line 80
    :cond_8
    new-instance v1, Lcom/google/googlenav/friend/reporting/s;

    const-string v2, "LOCATION_REPORTING"

    invoke-virtual {p0, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Lcom/google/googlenav/friend/reporting/s;-><init>(Landroid/content/SharedPreferences;Lcom/google/googlenav/friend/reporting/f;)V

    return-object v1
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .registers 12

    .prologue
    const-wide v9, 0x416312d000000000L

    const/4 v1, 0x0

    .line 99
    :try_start_6
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v2, "lastPosition"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    if-nez v0, :cond_13

    move-object v0, v1

    .line 124
    :goto_12
    return-object v0

    .line 104
    :cond_13
    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/s;->c:Lcom/google/googlenav/friend/reporting/f;

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/friend/reporting/f;->a([B)Landroid/util/Pair;

    move-result-object v3

    .line 105
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v2, "lastAccuracy"

    const/high16 v4, -0x4080

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v4

    .line 106
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v2, "lastTimestamp"

    const-wide/16 v5, 0x0

    invoke-interface {v0, v2, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 109
    new-instance v2, Landroid/location/Location;

    const-string v0, "LocationReportingPreferences"

    invoke-direct {v2, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 110
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v7, v0

    div-double/2addr v7, v9

    invoke-virtual {v2, v7, v8}, Landroid/location/Location;->setLatitude(D)V

    .line 111
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-double v7, v0

    div-double/2addr v7, v9

    invoke-virtual {v2, v7, v8}, Landroid/location/Location;->setLongitude(D)V

    .line 112
    invoke-virtual {v2, v4}, Landroid/location/Location;->setAccuracy(F)V

    .line 113
    invoke-virtual {v2, v5, v6}, Landroid/location/Location;->setTime(J)V
    :try_end_58
    .catch Ljava/security/GeneralSecurityException; {:try_start_6 .. :try_end_58} :catch_5a
    .catch Lcom/google/googlenav/common/util/d; {:try_start_6 .. :try_end_58} :catch_5d
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_58} :catch_60

    move-object v0, v2

    .line 115
    goto :goto_12

    .line 116
    :catch_5a
    move-exception v0

    move-object v0, v1

    .line 118
    goto :goto_12

    .line 119
    :catch_5d
    move-exception v0

    move-object v0, v1

    .line 121
    goto :goto_12

    .line 122
    :catch_60
    move-exception v0

    move-object v0, v1

    .line 124
    goto :goto_12
.end method

.method public a(J)V
    .registers 5
    .parameter

    .prologue
    .line 152
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 156
    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 157
    const-string v1, "lastIntentProcessedTimestamp"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 159
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 174
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 178
    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 179
    const-string v1, "locationHistoryEnabled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 181
    return-void
.end method

.method public a(Landroid/location/Location;)Z
    .registers 7
    .parameter

    .prologue
    const-wide v3, 0x416312d000000000L

    .line 193
    :try_start_5
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 197
    :cond_b
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    mul-double/2addr v0, v3

    double-to-int v0, v0

    .line 198
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    mul-double/2addr v1, v3

    double-to-int v1, v1

    .line 199
    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/s;->c:Lcom/google/googlenav/friend/reporting/f;

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/friend/reporting/f;->a(II)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/common/util/c;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 202
    const-string v2, "lastPosition"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 203
    const-string v0, "lastAccuracy"

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 204
    const-string v0, "lastTimestamp"

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 206
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_41
    .catch Ljava/security/GeneralSecurityException; {:try_start_5 .. :try_end_41} :catch_43

    move-result v0

    .line 209
    :goto_42
    return v0

    .line 207
    :catch_43
    move-exception v0

    .line 209
    const/4 v0, 0x0

    goto :goto_42
.end method

.method public b()I
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 134
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v1, "deviceTag"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 135
    if-eq v0, v2, :cond_c

    .line 143
    :goto_b
    return v0

    .line 138
    :cond_c
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 139
    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    .line 140
    iget-object v1, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 141
    const-string v2, "deviceTag"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 142
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_b
.end method

.method public b(Z)Ljava/util/List;
    .registers 8
    .parameter

    .prologue
    .line 256
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 260
    :cond_6
    const-string v0, ""

    .line 261
    sget-object v1, Lcom/google/googlenav/friend/reporting/s;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 263
    :try_start_b
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v2, "requestingGaiaIds"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    if-eqz p1, :cond_25

    .line 267
    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 268
    const-string v3, "requestingGaiaIds"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 269
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 271
    :cond_25
    monitor-exit v1
    :try_end_26
    .catchall {:try_start_b .. :try_end_26} :catchall_4a

    .line 273
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 274
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 275
    array-length v3, v1

    const/4 v0, 0x0

    :goto_32
    if-ge v0, v3, :cond_4d

    aget-object v4, v1, v0

    .line 276
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_47

    .line 277
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    :cond_47
    add-int/lit8 v0, v0, 0x1

    goto :goto_32

    .line 271
    :catchall_4a
    move-exception v0

    :try_start_4b
    monitor-exit v1
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_4a

    throw v0

    .line 280
    :cond_4d
    return-object v2
.end method

.method public b(J)V
    .registers 7
    .parameter

    .prologue
    .line 219
    new-instance v0, Lcom/google/googlenav/friend/reporting/t;

    invoke-direct {v0, p0}, Lcom/google/googlenav/friend/reporting/t;-><init>(Lcom/google/googlenav/friend/reporting/s;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/reporting/t;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 228
    return-void
.end method

.method public c()J
    .registers 5

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v1, "lastIntentProcessedTimestamp"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method c(J)V
    .registers 9
    .parameter

    .prologue
    .line 232
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 236
    :cond_6
    sget-object v1, Lcom/google/googlenav/friend/reporting/s;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 237
    :try_start_9
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v2, "requestingGaiaIds"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-object v2, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 240
    const-string v3, "requestingGaiaIds"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 243
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 244
    monitor-exit v1

    .line 245
    return-void

    .line 244
    :catchall_3e
    move-exception v0

    monitor-exit v1
    :try_end_40
    .catchall {:try_start_9 .. :try_end_40} :catchall_3e

    throw v0
.end method

.method public d()Z
    .registers 4

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v1, "locationHistoryEnabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d(J)Z
    .registers 5
    .parameter

    .prologue
    .line 287
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_20

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Lcom/google/common/base/J;->a(Z)V

    .line 289
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 292
    :cond_10
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 293
    const-string v1, "locationReportingIntentTimstamp"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 295
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0

    .line 287
    :cond_20
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public e()J
    .registers 5

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    const-string v1, "locationReportingIntentTimstamp"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/googlenav/friend/reporting/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 311
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 312
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    return v0
.end method
