.class public Lcom/google/googlenav/friend/reporting/h;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    return-void
.end method

.method private a(Landroid/location/Location;)LaN/B;
    .registers 7
    .parameter

    .prologue
    const-wide v3, 0x412e848000000000L

    .line 83
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    mul-double/2addr v0, v3

    double-to-int v0, v0

    .line 84
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v3

    double-to-int v1, v1

    .line 85
    new-instance v2, LaN/B;

    invoke-direct {v2, v1, v0}, LaN/B;-><init>(II)V

    .line 86
    return-object v2
.end method


# virtual methods
.method public a(Landroid/location/Location;Landroid/location/Location;Ljava/util/List;)Lcom/google/googlenav/friend/reporting/j;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 59
    if-nez p2, :cond_b

    .line 62
    new-instance v0, Lcom/google/googlenav/friend/reporting/j;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/googlenav/friend/reporting/i;->c:Lcom/google/googlenav/friend/reporting/i;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/j;-><init>(ZLcom/google/googlenav/friend/reporting/i;)V

    .line 76
    :goto_a
    return-object v0

    .line 66
    :cond_b
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/reporting/h;->a(Landroid/location/Location;)LaN/B;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-long v1, v1

    invoke-direct {p0, p2}, Lcom/google/googlenav/friend/reporting/h;->a(Landroid/location/Location;)LaN/B;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/friend/J;->a(LaN/B;JLaN/B;)Z

    move-result v1

    .line 71
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 72
    const-wide/16 v4, 0x3e8

    cmp-long v0, v2, v4

    if-gez v0, :cond_33

    .line 73
    new-instance v0, Lcom/google/googlenav/friend/reporting/j;

    sget-object v2, Lcom/google/googlenav/friend/reporting/i;->b:Lcom/google/googlenav/friend/reporting/i;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/j;-><init>(ZLcom/google/googlenav/friend/reporting/i;)V

    goto :goto_a

    .line 76
    :cond_33
    new-instance v0, Lcom/google/googlenav/friend/reporting/j;

    sget-object v2, Lcom/google/googlenav/friend/reporting/i;->d:Lcom/google/googlenav/friend/reporting/i;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/reporting/j;-><init>(ZLcom/google/googlenav/friend/reporting/i;)V

    goto :goto_a
.end method
