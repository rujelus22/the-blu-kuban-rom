.class public Lcom/google/googlenav/friend/history/s;
.super Lcom/google/googlenav/friend/history/x;
.source "SourceFile"


# instance fields
.field a:Z

.field private final d:Lcom/google/googlenav/ui/view/dialog/aD;

.field private final e:Lcom/google/googlenav/friend/history/b;

.field private final f:Lcom/google/googlenav/friend/history/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p2, p1}, Lcom/google/googlenav/friend/history/x;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/s;->a:Z

    .line 52
    iput-object p3, p0, Lcom/google/googlenav/friend/history/s;->d:Lcom/google/googlenav/ui/view/dialog/aD;

    .line 53
    iput-object p4, p0, Lcom/google/googlenav/friend/history/s;->e:Lcom/google/googlenav/friend/history/b;

    .line 54
    iput-object p5, p0, Lcom/google/googlenav/friend/history/s;->f:Lcom/google/googlenav/friend/history/b;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/friend/history/s;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/s;->c(Landroid/view/View;)V

    return-void
.end method

.method private c(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 68
    const v0, 0x7f1001ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 69
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 70
    const v0, 0x7f1001ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 71
    const/16 v1, 0x25b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x8

    .line 59
    invoke-virtual {p0, p1}, Lcom/google/googlenav/friend/history/s;->b(Landroid/view/View;)V

    .line 61
    const v0, 0x7f1002ba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 62
    const v0, 0x7f100026

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 64
    invoke-direct {p0, p1}, Lcom/google/googlenav/friend/history/s;->c(Landroid/view/View;)V

    .line 65
    return-void
.end method

.method b(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/googlenav/friend/history/s;->a:Z

    if-nez v0, :cond_17

    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/friend/history/s;->a:Z

    .line 78
    iget-object v0, p0, Lcom/google/googlenav/friend/history/s;->d:Lcom/google/googlenav/ui/view/dialog/aD;

    iget-object v1, p0, Lcom/google/googlenav/friend/history/s;->b:Lcom/google/googlenav/friend/history/b;

    iget-object v2, p0, Lcom/google/googlenav/friend/history/s;->e:Lcom/google/googlenav/friend/history/b;

    iget-object v3, p0, Lcom/google/googlenav/friend/history/s;->f:Lcom/google/googlenav/friend/history/b;

    new-instance v4, Lcom/google/googlenav/friend/history/t;

    invoke-direct {v4, p0, p1}, Lcom/google/googlenav/friend/history/t;-><init>(Lcom/google/googlenav/friend/history/s;Landroid/view/View;)V

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/dialog/aD;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aE;)V

    .line 109
    :cond_17
    return-void
.end method
