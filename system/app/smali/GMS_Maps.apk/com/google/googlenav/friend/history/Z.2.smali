.class public Lcom/google/googlenav/friend/history/Z;
.super Lcom/google/googlenav/friend/history/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/friend/history/o;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 5

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/Z;->g()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/Z;->f()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bd;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public k()Ljava/lang/String;
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x3

    .line 43
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 44
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 45
    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/Z;->f()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 48
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/googlenav/friend/history/Z;->f()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 52
    invoke-static {v7}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v4

    .line 53
    const-wide/32 v5, 0x1ee62800

    cmp-long v2, v2, v5

    if-gtz v2, :cond_51

    .line 54
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "EEEE"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 64
    :goto_2c
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 69
    const-string v2, "%s, %s (%s)"

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    aput-object v1, v3, v8

    const/4 v0, 0x2

    invoke-direct {p0}, Lcom/google/googlenav/friend/history/Z;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 55
    :cond_51
    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v0, v2, :cond_60

    .line 60
    invoke-static {v7}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    goto :goto_2c

    .line 62
    :cond_60
    invoke-static {v7}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    goto :goto_2c
.end method
