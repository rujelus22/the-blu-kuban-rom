.class public final enum Lcom/google/googlenav/friend/history/p;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/googlenav/friend/history/p;

.field public static final enum b:Lcom/google/googlenav/friend/history/p;

.field public static final enum c:Lcom/google/googlenav/friend/history/p;

.field public static final enum d:Lcom/google/googlenav/friend/history/p;

.field private static final synthetic e:[Lcom/google/googlenav/friend/history/p;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lcom/google/googlenav/friend/history/p;

    const-string v1, "PLACE_SEGMENT"

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/friend/history/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/history/p;->a:Lcom/google/googlenav/friend/history/p;

    .line 65
    new-instance v0, Lcom/google/googlenav/friend/history/p;

    const-string v1, "FLIGHT_SEGMENT"

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/friend/history/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/history/p;->b:Lcom/google/googlenav/friend/history/p;

    .line 66
    new-instance v0, Lcom/google/googlenav/friend/history/p;

    const-string v1, "TRAVEL_SEGMENT"

    invoke-direct {v0, v1, v4}, Lcom/google/googlenav/friend/history/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/history/p;->c:Lcom/google/googlenav/friend/history/p;

    .line 67
    new-instance v0, Lcom/google/googlenav/friend/history/p;

    const-string v1, "NO_DATA_SEGMENT"

    invoke-direct {v0, v1, v5}, Lcom/google/googlenav/friend/history/p;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/friend/history/p;->d:Lcom/google/googlenav/friend/history/p;

    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/googlenav/friend/history/p;

    sget-object v1, Lcom/google/googlenav/friend/history/p;->a:Lcom/google/googlenav/friend/history/p;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/googlenav/friend/history/p;->b:Lcom/google/googlenav/friend/history/p;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/googlenav/friend/history/p;->c:Lcom/google/googlenav/friend/history/p;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/googlenav/friend/history/p;->d:Lcom/google/googlenav/friend/history/p;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/googlenav/friend/history/p;->e:[Lcom/google/googlenav/friend/history/p;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/googlenav/friend/history/p;
    .registers 2
    .parameter

    .prologue
    .line 63
    const-class v0, Lcom/google/googlenav/friend/history/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/p;

    return-object v0
.end method

.method public static values()[Lcom/google/googlenav/friend/history/p;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/friend/history/p;->e:[Lcom/google/googlenav/friend/history/p;

    invoke-virtual {v0}, [Lcom/google/googlenav/friend/history/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/friend/history/p;

    return-object v0
.end method
