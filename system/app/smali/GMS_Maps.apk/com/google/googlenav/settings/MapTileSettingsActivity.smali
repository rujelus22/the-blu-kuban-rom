.class public Lcom/google/googlenav/settings/MapTileSettingsActivity;
.super Lcom/google/googlenav/settings/GmmPreferenceActivity;
.source "SourceFile"


# static fields
.field public static final b:Z

.field private static final c:[Ljava/lang/String;


# instance fields
.field private d:Z

.field private final e:Ljava/lang/Object;

.field private f:Landroid/preference/PreferenceScreen;

.field private g:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 85
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->c:[Ljava/lang/String;

    .line 119
    invoke-static {}, Lcom/google/googlenav/capabilities/a;->a()Lcom/google/googlenav/capabilities/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/capabilities/a;->b()Z

    move-result v0

    sput-boolean v0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;-><init>()V

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->d:Z

    .line 128
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->e:Ljava/lang/Object;

    .line 753
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/settings/MapTileSettingsActivity;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Landroid/preference/ListPreference;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 694
    if-nez p1, :cond_20

    .line 695
    const-string v0, "map_tile_settings_prefetching_over_mobile_networks"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 698
    :goto_a
    if-eqz p2, :cond_16

    .line 699
    const/16 v1, 0x5b0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 705
    :goto_15
    return-void

    .line 702
    :cond_16
    const/16 v1, 0x5af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_15

    :cond_20
    move-object v0, p1

    goto :goto_a
.end method

.method static synthetic a(Lcom/google/googlenav/settings/MapTileSettingsActivity;Landroid/preference/ListPreference;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->a(Landroid/preference/ListPreference;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 723
    sget-boolean v1, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b:Z

    if-eqz v1, :cond_6

    .line 726
    :goto_5
    return v0

    :cond_6
    const-string v1, "settings_preference"

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "map_tile_settings_prefetching_over_mobile_networks"

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_5
.end method

.method private b()V
    .registers 7

    .prologue
    .line 708
    const-string v0, "map_tile_settings_clear_cache"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_29

    const/16 v0, 0xcb

    :goto_e
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->g()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {v4}, Lcom/google/googlenav/common/util/k;->c(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 713
    return-void

    .line 708
    :cond_29
    const/16 v0, 0xca

    goto :goto_e
.end method

.method static synthetic b(Lcom/google/googlenav/settings/MapTileSettingsActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/settings/MapTileSettingsActivity;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .registers 2
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->g:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 11

    .prologue
    const/16 v9, 0xa

    .line 790
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 792
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->g:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->c()Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;

    move-result-object v2

    .line 793
    if-eqz v2, :cond_5c

    .line 794
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;->a()J

    move-result-wide v3

    .line 795
    const/16 v0, 0x304

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 796
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    .line 797
    const-wide/16 v7, 0x0

    cmp-long v7, v3, v7

    if-eqz v7, :cond_3b

    cmp-long v7, v3, v5

    if-gez v7, :cond_3b

    .line 798
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->q()Lcom/google/googlenav/common/d;

    move-result-object v0

    invoke-interface {v0, v3, v4, v5, v6}, Lcom/google/googlenav/common/d;->a(JJ)Ljava/lang/String;

    move-result-object v0

    .line 801
    :cond_3b
    const/16 v3, 0x211

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 802
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 805
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/A;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 808
    :cond_5c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 235
    invoke-super {p0, p1}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 237
    invoke-virtual {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 386
    :goto_a
    return-void

    .line 241
    :cond_b
    const v0, 0x7f060008

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->addPreferencesFromResource(I)V

    .line 242
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_10c

    const/16 v0, 0x64

    :goto_19
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 245
    invoke-virtual {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "settings_preference"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 246
    invoke-virtual {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    .line 248
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    const-string v1, "map_tile_settings_prefetching_over_mobile_networks"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 251
    sget-boolean v1, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b:Z

    if-nez v1, :cond_11b

    .line 252
    const/16 v1, 0x5b1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 254
    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 256
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x5ad

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x5ae

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 260
    sget-object v1, Lcom/google/googlenav/settings/MapTileSettingsActivity;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 261
    invoke-static {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->a(Landroid/content/Context;)Z

    move-result v1

    .line 263
    if-eqz v1, :cond_110

    .line 264
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 269
    :goto_74
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->a(Landroid/preference/ListPreference;Z)V

    .line 271
    new-instance v1, Lcom/google/googlenav/settings/Q;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/settings/Q;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;Lcom/google/googlenav/settings/F;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 277
    :goto_80
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    const-string v1, "map_tile_settings_use_cached_tiles_only"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 279
    sget-boolean v1, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b:Z

    if-nez v1, :cond_8e

    .line 292
    :cond_8e
    iget-object v1, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 295
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    const-string v1, "map_tile_settings_clear_cache"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 296
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_122

    const/16 v0, 0xc9

    :goto_a3
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 298
    invoke-direct {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b()V

    .line 300
    iget-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    const-string v1, "map_tile_settings_clear_my_places_cache"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 302
    const/16 v1, 0xd2

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 303
    const/16 v1, 0xd3

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 310
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_100

    .line 311
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 312
    const-string v1, "Debug"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v1, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 315
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 316
    const-string v2, "debug_info_settings_debug_show_prefetch_info"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 317
    const-string v2, "Show prefetch info"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 318
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 379
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->x()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->g:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    .line 383
    :cond_100
    iget-object v1, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 384
    const/4 v0, 0x1

    :try_start_104
    iput-boolean v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->d:Z

    .line 385
    monitor-exit v1

    goto/16 :goto_a

    :catchall_109
    move-exception v0

    monitor-exit v1
    :try_end_10b
    .catchall {:try_start_104 .. :try_end_10b} :catchall_109

    throw v0

    .line 242
    :cond_10c
    const/16 v0, 0x63

    goto/16 :goto_19

    .line 266
    :cond_110
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    goto/16 :goto_74

    .line 274
    :cond_11b
    iget-object v1, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->f:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_80

    .line 296
    :cond_122
    const/16 v0, 0xc8

    goto/16 :goto_a3
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 9
    .parameter

    .prologue
    const/16 v2, 0xcf

    const v6, 0x104000a

    const v5, 0x7f020215

    const/high16 v4, 0x104

    const/4 v1, 0x0

    .line 464
    packed-switch p1, :pswitch_data_10c

    move-object v0, v1

    .line 589
    :goto_f
    return-object v0

    .line 466
    :pswitch_10
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_43

    const/16 v0, 0xce

    :goto_29
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/settings/H;

    invoke-direct {v2, p0}, Lcom/google/googlenav/settings/H;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V

    invoke-virtual {v0, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_f

    :cond_43
    const/16 v0, 0xcc

    goto :goto_29

    .line 481
    :pswitch_46
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/16 v2, 0xcd

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/settings/I;

    invoke-direct {v2, p0}, Lcom/google/googlenav/settings/I;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V

    invoke-virtual {v0, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_f

    .line 494
    :pswitch_73
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_af

    const/16 v0, 0x5aa

    :goto_84
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_b2

    const/16 v0, 0x526

    :goto_94
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/google/googlenav/settings/J;

    invoke-direct {v2, p0}, Lcom/google/googlenav/settings/J;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V

    invoke-virtual {v0, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_f

    :cond_af
    const/16 v0, 0x5a9

    goto :goto_84

    :cond_b2
    const/16 v0, 0x525

    goto :goto_94

    .line 526
    :pswitch_b5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "prefetch Info"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Clear"

    new-instance v2, Lcom/google/googlenav/settings/L;

    invoke-direct {v2, p0}, Lcom/google/googlenav/settings/L;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_f

    .line 543
    :pswitch_e1
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 544
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "Input delay,period (in minutes), e.g. \"5,10\""

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/settings/M;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/settings/M;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;Landroid/widget/EditText;)V

    invoke-virtual {v2, v6, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_f

    .line 464
    :pswitch_data_10c
    .packed-switch 0x0
        :pswitch_73
        :pswitch_10
        :pswitch_b5
        :pswitch_e1
        :pswitch_46
    .end packed-switch
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 400
    iget-object v1, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 401
    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->d:Z

    .line 402
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_b

    .line 403
    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onPause()V

    .line 404
    return-void

    .line 402
    :catchall_b
    move-exception v0

    :try_start_c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_c .. :try_end_d} :catchall_b

    throw v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 408
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 409
    const-string v3, "map_tile_settings_clear_cache"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 410
    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->showDialog(I)V

    .line 459
    :cond_11
    :goto_11
    return v0

    .line 412
    :cond_12
    const-string v3, "map_tile_settings_clear_my_places_cache"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 413
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->showDialog(I)V

    goto :goto_11

    .line 415
    :cond_1f
    const-string v3, "map_tile_settings_prefetching_over_mobile_networks"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 417
    const-string v3, "map_tile_settings_use_cached_tiles_only"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 419
    const-string v3, "debug_info_settings_debug_show_prefetch_info"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_59

    .line 422
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    .line 424
    new-instance v2, Lcom/google/googlenav/prefetch/android/b;

    invoke-static {}, Lcom/google/googlenav/prefetch/android/h;->d()Lcom/google/googlenav/prefetch/android/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/prefetch/android/h;->c()Ljava/util/Vector;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/settings/F;

    invoke-direct {v4, p0, v1}, Lcom/google/googlenav/settings/F;-><init>(Lcom/google/googlenav/settings/MapTileSettingsActivity;Lcom/google/googlenav/ui/s;)V

    invoke-direct {v2, v3, v4}, Lcom/google/googlenav/prefetch/android/b;-><init>(Ljava/util/List;Lcom/google/googlenav/prefetch/android/d;)V

    invoke-virtual {v2}, Lcom/google/googlenav/prefetch/android/b;->a()V

    goto :goto_11

    .line 449
    :cond_59
    const-string v3, "debug_info_settings_debug_force_prefetch"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_70

    .line 450
    iget-object v2, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->g:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->b()V

    .line 451
    const-string v2, "Fire the force prefetch request successfully."

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_11

    .line 455
    :cond_70
    const-string v0, "debug_info_settings_debug_schedule_automatic_prefetch"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7c

    .line 456
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->showDialog(I)V

    :cond_7c
    move v0, v1

    .line 459
    goto :goto_11
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 391
    invoke-super {p0}, Lcom/google/googlenav/settings/GmmPreferenceActivity;->onResume()V

    .line 392
    iget-object v1, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 393
    const/4 v0, 0x1

    :try_start_7
    iput-boolean v0, p0, Lcom/google/googlenav/settings/MapTileSettingsActivity;->d:Z

    .line 394
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_e

    .line 395
    invoke-direct {p0}, Lcom/google/googlenav/settings/MapTileSettingsActivity;->b()V

    .line 396
    return-void

    .line 394
    :catchall_e
    move-exception v0

    :try_start_f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_f .. :try_end_10} :catchall_e

    throw v0
.end method
