.class public Lcom/google/googlenav/aV;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/ab;


# static fields
.field private static a:Lcom/google/googlenav/aV;


# instance fields
.field private b:Ljava/util/concurrent/ConcurrentMap;

.field private c:Ljava/util/concurrent/ExecutorService;

.field private volatile d:Ljava/util/concurrent/Future;

.field private volatile e:Ljava/util/concurrent/Future;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .registers 5
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, LY/e;->a()LY/e;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, LY/e;->a(J)LY/e;

    move-result-object v0

    invoke-virtual {v0}, LY/e;->o()LY/d;

    move-result-object v0

    invoke-interface {v0}, LY/d;->a()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 53
    iput-object p1, p0, Lcom/google/googlenav/aV;->c:Ljava/util/concurrent/ExecutorService;

    .line 54
    invoke-direct {p0}, Lcom/google/googlenav/aV;->e()V

    .line 55
    return-void
.end method

.method public static a()Lcom/google/googlenav/aV;
    .registers 2

    .prologue
    .line 58
    sget-object v0, Lcom/google/googlenav/aV;->a:Lcom/google/googlenav/aV;

    if-nez v0, :cond_f

    .line 59
    new-instance v0, Lcom/google/googlenav/aV;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/aV;-><init>(Ljava/util/concurrent/ExecutorService;)V

    sput-object v0, Lcom/google/googlenav/aV;->a:Lcom/google/googlenav/aV;

    .line 61
    :cond_f
    sget-object v0, Lcom/google/googlenav/aV;->a:Lcom/google/googlenav/aV;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/aV;)V
    .registers 1
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/googlenav/aV;->g()V

    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/googlenav/aV;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/googlenav/aW;

    invoke-direct {v1, p0}, Lcom/google/googlenav/aW;-><init>(Lcom/google/googlenav/aV;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aV;->e:Ljava/util/concurrent/Future;

    .line 86
    return-void
.end method

.method private f()V
    .registers 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/googlenav/aV;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/googlenav/aX;

    invoke-direct {v1, p0}, Lcom/google/googlenav/aX;-><init>(Lcom/google/googlenav/aV;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aV;->d:Ljava/util/concurrent/Future;

    .line 118
    return-void
.end method

.method private declared-synchronized g()V
    .registers 5

    .prologue
    .line 121
    monitor-enter p0

    :try_start_1
    new-instance v1, Laq/b;

    invoke-direct {v1}, Laq/b;-><init>()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_41

    .line 123
    :try_start_6
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Laq/b;->writeInt(I)V

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 125
    invoke-virtual {v1, v0}, Laq/b;->writeUTF(Ljava/lang/String;)V
    :try_end_28
    .catchall {:try_start_6 .. :try_end_28} :catchall_41
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_28} :catch_29

    goto :goto_19

    .line 130
    :catch_29
    move-exception v0

    .line 134
    :goto_2a
    monitor-exit p0

    return-void

    .line 128
    :cond_2c
    :try_start_2c
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v2, "PLACES_REVIEW_PROMPT_BLACKLIST_SETTING"

    invoke-virtual {v1}, Laq/b;->a()[B

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 129
    invoke-virtual {v1}, Laq/b;->close()V
    :try_end_40
    .catchall {:try_start_2c .. :try_end_40} :catchall_41
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_40} :catch_29

    goto :goto_2a

    .line 121
    :catchall_41
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public F_()V
    .registers 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/googlenav/aV;->c()V

    .line 201
    return-void
.end method

.method public a(LaR/P;)V
    .registers 2
    .parameter

    .prologue
    .line 196
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 206
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-direct {p0}, Lcom/google/googlenav/aV;->f()V

    .line 72
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/google/googlenav/aV;->e:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_9

    .line 180
    iget-object v0, p0, Lcom/google/googlenav/aV;->e:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_9} :catch_13

    .line 185
    :cond_9
    :goto_9
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 186
    return-void

    .line 182
    :catch_13
    move-exception v0

    goto :goto_9
.end method

.method public declared-synchronized b()V
    .registers 7

    .prologue
    .line 90
    monitor-enter p0

    :try_start_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PLACES_REVIEW_PROMPT_BLACKLIST_SETTING"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_37

    move-result-object v0

    .line 92
    if-nez v0, :cond_13

    .line 108
    :cond_11
    :goto_11
    monitor-exit p0

    return-void

    .line 96
    :cond_13
    :try_start_13
    new-instance v1, Laq/a;

    invoke-direct {v1, v0}, Laq/a;-><init>([B)V
    :try_end_18
    .catchall {:try_start_13 .. :try_end_18} :catchall_37

    .line 98
    :try_start_18
    invoke-virtual {v1}, Laq/a;->readInt()I

    move-result v2

    .line 99
    const/4 v0, 0x0

    :goto_1d
    if-ge v0, v2, :cond_11

    .line 100
    invoke-virtual {v1}, Laq/a;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 101
    iget-object v4, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2d
    .catchall {:try_start_18 .. :try_end_2d} :catchall_37
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_2d} :catch_30

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 103
    :catch_30
    move-exception v0

    .line 105
    :try_start_31
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V
    :try_end_36
    .catchall {:try_start_31 .. :try_end_36} :catchall_37

    goto :goto_11

    .line 90
    :catchall_37
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/googlenav/aV;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .registers 3

    .prologue
    .line 167
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 168
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->g()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->e()LaR/u;

    move-result-object v1

    invoke-interface {v1}, LaR/u;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 170
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->h()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->e()LaR/u;

    move-result-object v1

    invoke-interface {v1}, LaR/u;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 172
    invoke-virtual {p0, v0}, Lcom/google/googlenav/aV;->a(Ljava/util/Set;)V

    .line 173
    invoke-direct {p0}, Lcom/google/googlenav/aV;->f()V

    .line 174
    return-void
.end method
