.class public Lcom/google/googlenav/aa;
.super Law/a;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/googlenav/Y;

.field private b:Z

.field private c:Z

.field private d:[I

.field private final e:Lcom/google/googlenav/ui/bh;

.field private final f:Lax/y;

.field private final g:I

.field private final h:I

.field private i:Ljava/util/List;

.field private final j:Z

.field private volatile k:Z

.field private l:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lax/y;)V
    .registers 4
    .parameter

    .prologue
    .line 328
    invoke-direct {p0}, Law/a;-><init>()V

    .line 299
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    .line 330
    new-instance v0, Lcom/google/googlenav/Y;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/Y;-><init>(Lax/y;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    .line 331
    iput-object p1, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    .line 332
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/google/googlenav/aa;->g:I

    .line 333
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/aa;->h:I

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    .line 335
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    .line 336
    return-void
.end method

.method public constructor <init>(Lax/y;Ljava/lang/String;Lcom/google/googlenav/ui/bh;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 309
    invoke-direct {p0}, Law/a;-><init>()V

    .line 299
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    .line 310
    new-instance v0, Lcom/google/googlenav/Y;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/Y;-><init>(Lax/y;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    .line 311
    iput-object p1, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    .line 312
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/google/googlenav/aa;->g:I

    .line 313
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/aa;->h:I

    .line 314
    iput-object p3, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    .line 315
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    .line 316
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/Y;Lax/y;IILcom/google/googlenav/ui/bh;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 352
    invoke-direct {p0}, Law/a;-><init>()V

    .line 299
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    .line 353
    iput-object p1, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    .line 354
    iput-object p5, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    .line 355
    iput-object p2, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    .line 356
    iput p3, p0, Lcom/google/googlenav/aa;->g:I

    .line 357
    iput p4, p0, Lcom/google/googlenav/aa;->h:I

    .line 358
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->c:Z

    .line 359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    .line 360
    return-void
.end method

.method private a([ILjava/util/List;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 540
    if-nez p1, :cond_5

    .line 552
    :goto_4
    return v1

    .line 543
    :cond_5
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v3

    .line 544
    array-length v4, p1

    move v0, v2

    :goto_b
    if-ge v0, v4, :cond_19

    aget v5, p1, v0

    .line 545
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 547
    :cond_19
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    .line 548
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_21
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    .line 549
    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 551
    :cond_39
    invoke-interface {v4, v3}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 552
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_45

    move v0, v1

    :goto_43
    move v1, v0

    goto :goto_4

    :cond_45
    move v0, v2

    goto :goto_43
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;
    .registers 10
    .parameter

    .prologue
    const/16 v8, 0x9

    const/16 v7, 0x8

    const/4 v6, 0x6

    .line 557
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 558
    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 559
    const/4 v0, 0x0

    :goto_e
    if-ge v0, v2, :cond_39

    .line 560
    invoke-virtual {p0, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 561
    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 562
    invoke-virtual {v3, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 564
    :cond_25
    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 565
    invoke-virtual {v3, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 559
    :cond_36
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 568
    :cond_39
    return-object v1
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/util/HashMap;
    .registers 6
    .parameter

    .prologue
    .line 532
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 533
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    .line 534
    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 536
    :cond_20
    return-object v1
.end method

.method a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 10
    .parameter

    .prologue
    const/16 v7, 0x8

    .line 516
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 517
    invoke-virtual {p1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 519
    const/4 v0, 0x0

    :goto_b
    if-ge v0, v2, :cond_26

    .line 520
    invoke-virtual {p1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 522
    new-instance v4, Lcom/google/googlenav/ab;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v3, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v3}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    .line 525
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 527
    :cond_26
    return-object v1
.end method

.method public a(Lax/y;)V
    .registers 3
    .parameter

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v0, p1}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Lax/y;)Lax/y;

    .line 370
    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x3e8

    const/4 v5, 0x5

    const/4 v4, 0x3

    .line 384
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 386
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iget-object v2, p0, Lcom/google/googlenav/aa;->f:Lax/y;

    invoke-virtual {v2}, Lax/y;->f()LaN/B;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V

    .line 389
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v0}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;)Lax/y;

    move-result-object v0

    if-eqz v0, :cond_54

    .line 390
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    iget-object v2, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;)Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->f()LaN/B;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaN/B;I)V

    .line 394
    invoke-virtual {v1, v5, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 396
    invoke-virtual {v1, v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 404
    :goto_35
    const/16 v0, 0x9

    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 409
    iget-object v0, p0, Lcom/google/googlenav/aa;->d:[I

    invoke-static {v0}, Lcom/google/googlenav/common/util/a;->a([I)Z

    move-result v0

    if-nez v0, :cond_5f

    .line 410
    iget-object v2, p0, Lcom/google/googlenav/aa;->d:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_48
    if-ge v0, v3, :cond_5f

    aget v4, v2, v0

    .line 411
    const/16 v5, 0x8

    invoke-virtual {v1, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    .line 410
    add-int/lit8 v0, v0, 0x1

    goto :goto_48

    .line 399
    :cond_54
    iget v0, p0, Lcom/google/googlenav/aa;->h:I

    invoke-virtual {v1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 401
    iget v0, p0, Lcom/google/googlenav/aa;->g:I

    invoke-virtual {v1, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_35

    .line 417
    :cond_5f
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    invoke-static {v2}, Lcom/google/googlenav/ui/m;->a(Lcom/google/googlenav/ui/m;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 419
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 420
    return-void
.end method

.method public a([I)V
    .registers 2
    .parameter

    .prologue
    .line 374
    iput-object p1, p0, Lcom/google/googlenav/aa;->d:[I

    .line 375
    return-void
.end method

.method public a(J)Z
    .registers 5
    .parameter

    .prologue
    .line 505
    iget-object v1, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 506
    :try_start_3
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->k:Z

    if-nez v0, :cond_12

    invoke-virtual {p0}, Lcom/google/googlenav/aa;->z_()Z

    move-result v0

    if-nez v0, :cond_12

    .line 509
    iget-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V

    .line 511
    :cond_12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_16

    .line 512
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->k:Z

    return v0

    .line 511
    :catchall_16
    move-exception v0

    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x9

    const/4 v3, 0x3

    const/4 v7, 0x1

    .line 424
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hF;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 427
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v2

    invoke-static {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;Z)Z

    .line 429
    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 430
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/Y;I)I

    .line 434
    :cond_24
    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_a3

    .line 435
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    new-instance v2, Lcom/google/googlenav/layer/m;

    invoke-virtual {v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    .line 442
    :goto_38
    invoke-virtual {p0, v1}, Lcom/google/googlenav/aa;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    .line 443
    iget-object v0, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/aa;->a(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    .line 449
    iget-object v2, p0, Lcom/google/googlenav/aa;->d:[I

    iget-object v3, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/aa;->a([ILjava/util/List;)Z

    move-result v2

    if-nez v2, :cond_51

    .line 450
    invoke-static {}, Lbf/bU;->bM()V

    .line 453
    :cond_51
    iget-object v2, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v2}, Lcom/google/googlenav/Y;->b(Lcom/google/googlenav/Y;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 454
    iget-object v2, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    invoke-static {v2}, Lcom/google/googlenav/Y;->b(Lcom/google/googlenav/Y;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/aa;->i:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 456
    iget-object v2, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    if-eqz v2, :cond_77

    .line 457
    iget-object v2, p0, Lcom/google/googlenav/aa;->e:Lcom/google/googlenav/ui/bh;

    const/4 v3, 0x7

    invoke-virtual {v1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v1}, Lcom/google/googlenav/aa;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Set;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/bh;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/Set;)V

    .line 461
    :cond_77
    invoke-virtual {v1, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 462
    if-eqz v2, :cond_ab

    .line 474
    new-instance v3, Lcom/google/googlenav/Z;

    invoke-direct {v3, v1, v5, v5, v0}, Lcom/google/googlenav/Z;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Lcom/google/googlenav/bZ;[Lcom/google/googlenav/bZ;Ljava/util/Map;)V

    .line 478
    const/4 v0, 0x0

    :goto_83
    if-ge v0, v2, :cond_ab

    .line 479
    invoke-virtual {v1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    .line 481
    new-instance v5, Lcom/google/googlenav/bZ;

    sget-object v6, Lcom/google/wireless/googlenav/proto/j2me/hF;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v5, v4, v6, v3}, Lcom/google/googlenav/bZ;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBufType;Lcom/google/googlenav/cd;)V

    .line 483
    invoke-virtual {v5, v7}, Lcom/google/googlenav/bZ;->a(Z)V

    .line 484
    invoke-virtual {v5, v7}, Lcom/google/googlenav/bZ;->b(Z)V

    .line 485
    iget-object v4, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    new-instance v6, Lcom/google/googlenav/cp;

    invoke-direct {v6, v5}, Lcom/google/googlenav/cp;-><init>(Lcom/google/googlenav/bZ;)V

    invoke-virtual {v4, v6}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/cp;)V

    .line 478
    add-int/lit8 v0, v0, 0x1

    goto :goto_83

    .line 438
    :cond_a3
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    sget-object v2, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    goto :goto_38

    .line 489
    :cond_ab
    iget-object v1, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 490
    const/4 v0, 0x1

    :try_start_af
    iput-boolean v0, p0, Lcom/google/googlenav/aa;->k:Z

    .line 491
    iget-object v0, p0, Lcom/google/googlenav/aa;->l:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 492
    monitor-exit v1

    .line 493
    return v7

    .line 492
    :catchall_b8
    move-exception v0

    monitor-exit v1
    :try_end_ba
    .catchall {:try_start_af .. :try_end_ba} :catchall_b8

    throw v0
.end method

.method public declared-synchronized a_()Z
    .registers 2

    .prologue
    .line 578
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->b:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_a

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :catchall_a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 379
    const/16 v0, 0x7e

    return v0
.end method

.method public declared-synchronized d_()V
    .registers 2

    .prologue
    .line 573
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/googlenav/aa;->b:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 574
    monitor-exit p0

    return-void

    .line 573
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 582
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->c:Z

    return v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 586
    iget-boolean v0, p0, Lcom/google/googlenav/aa;->j:Z

    return v0
.end method

.method public l()Lcom/google/googlenav/Y;
    .registers 2

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/googlenav/aa;->a:Lcom/google/googlenav/Y;

    return-object v0
.end method
