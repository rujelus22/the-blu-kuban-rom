.class public Lcom/google/googlenav/suggest/android/a;
.super Lbb/d;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Lbb/d;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/googlenav/suggest/android/a;->a:Landroid/content/Context;

    .line 38
    return-void
.end method

.method private static a(Landroid/database/Cursor;IIII)Lbb/w;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 98
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x6

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/googlenav/suggest/android/a;->a(II)LaN/B;

    move-result-object v0

    .line 101
    new-instance v1, Lbb/y;

    invoke-direct {v1}, Lbb/y;-><init>()V

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbb/y;->a(Ljava/lang/String;)Lbb/y;

    move-result-object v1

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbb/y;->b(Ljava/lang/String;)Lbb/y;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbb/y;->c(Ljava/lang/String;)Lbb/y;

    move-result-object v1

    invoke-virtual {v1, v3}, Lbb/y;->a(I)Lbb/y;

    move-result-object v1

    invoke-virtual {v1, p2}, Lbb/y;->b(I)Lbb/y;

    move-result-object v1

    invoke-virtual {v1, p3}, Lbb/y;->c(I)Lbb/y;

    move-result-object v1

    const/4 v2, 0x7

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lbb/y;->a(J)Lbb/y;

    move-result-object v1

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lbb/y;->d(Ljava/lang/String;)Lbb/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbb/y;->a(LaN/B;)Lbb/y;

    move-result-object v0

    invoke-virtual {v0, p4}, Lbb/y;->d(I)Lbb/y;

    move-result-object v0

    invoke-virtual {v0}, Lbb/y;->a()Lbb/w;

    move-result-object v0

    return-object v0
.end method

.method private a(Lbb/s;Landroid/database/Cursor;I)Lbb/z;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x3

    .line 79
    new-instance v1, Lbb/z;

    invoke-direct {v1, p1}, Lbb/z;-><init>(Lbb/s;)V

    .line 80
    if-nez p2, :cond_a

    move-object v0, v1

    .line 93
    :goto_9
    return-object v0

    .line 83
    :cond_a
    const/4 v0, 0x0

    :goto_b
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2a

    .line 84
    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 85
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/a;->c()I

    move-result v2

    add-int v3, p3, v0

    invoke-virtual {p0, v5}, Lcom/google/googlenav/suggest/android/a;->a(I)I

    move-result v4

    invoke-static {p2, v5, v2, v3, v4}, Lcom/google/googlenav/suggest/android/a;->a(Landroid/database/Cursor;IIII)Lbb/w;

    move-result-object v2

    .line 88
    if-eqz v2, :cond_27

    .line 89
    invoke-virtual {v1, v2}, Lbb/z;->b(Lbb/w;)V

    .line 83
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 92
    :cond_2a
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-object v0, v1

    .line 93
    goto :goto_9
.end method


# virtual methods
.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 134
    const/16 v0, 0x8

    return v0
.end method

.method protected a(Lbb/s;)Lbb/z;
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 55
    .line 57
    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/provider/LocalSuggestionProvider;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lbb/s;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "History"

    aput-object v7, v4, v5

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1f} :catch_2e

    move-result-object v0

    .line 63
    :goto_20
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/a;->j()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/suggest/android/a;->a(Lbb/s;Landroid/database/Cursor;I)Lbb/z;

    move-result-object v1

    .line 64
    if-eqz v0, :cond_2d

    .line 65
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 67
    :cond_2d
    return-object v1

    .line 59
    :catch_2e
    move-exception v0

    .line 60
    const-string v1, "AndroidLocalHistorySuggestProvider"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v6

    goto :goto_20
.end method

.method protected a(Lbb/z;ZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    if-nez p3, :cond_c

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-nez v0, :cond_26

    :cond_c
    const/4 v0, 0x1

    .line 46
    :goto_d
    if-nez v0, :cond_22

    .line 47
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    .line 48
    const-wide/32 v2, 0x1d4c0

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lbb/z;->a(J)V

    .line 50
    :cond_22
    invoke-super {p0, p1, p2, p3}, Lbb/d;->a(Lbb/z;ZZ)V

    .line 51
    return-void

    .line 44
    :cond_26
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 124
    const-string v0, "h"

    return-object v0
.end method

.method public b(Lbb/s;)Z
    .registers 3
    .parameter

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 129
    const/4 v0, 0x3

    return v0
.end method

.method public d()[I
    .registers 4

    .prologue
    .line 139
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x8

    aput v2, v0, v1

    return-object v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 149
    const/4 v0, 0x1

    return v0
.end method
