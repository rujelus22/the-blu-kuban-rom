.class public Lcom/google/googlenav/suggest/android/k;
.super Lcom/google/googlenav/provider/e;
.source "SourceFile"

# interfaces
.implements Lbb/v;


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private volatile b:Lbb/z;

.field private c:Landroid/os/Bundle;

.field private d:Lbb/z;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 37
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_completion"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/googlenav/suggest/android/k;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/googlenav/provider/e;-><init>()V

    .line 87
    new-instance v0, Lbb/z;

    invoke-direct {v0}, Lbb/z;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/k;->e:Z

    .line 97
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbb/o;->a(Lbb/v;)V

    .line 98
    return-void
.end method

.method private a(Lbb/z;)V
    .registers 2
    .parameter

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    .line 149
    return-void
.end method


# virtual methods
.method public a(I)Lbb/w;
    .registers 3
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v0, p1}, Lbb/z;->a(I)Lbb/w;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbb/s;)V
    .registers 3
    .parameter

    .prologue
    .line 153
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbb/o;->a(Lbb/s;)V

    .line 154
    return-void
.end method

.method public declared-synchronized a(Lbb/z;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 124
    monitor-enter p0

    if-nez p2, :cond_5

    const/4 v0, 0x1

    :cond_5
    :try_start_5
    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/k;->e:Z

    .line 128
    iput-object p1, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/k;->onChange(Z)V
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_f

    .line 130
    monitor-exit p0

    return-void

    .line 124
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .registers 1

    .prologue
    .line 139
    return-void
.end method

.method public getColumnNames()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 163
    sget-object v0, Lcom/google/googlenav/suggest/android/k;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getCount()I
    .registers 2

    .prologue
    .line 158
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    if-nez v0, :cond_8

    const/4 v0, 0x0

    :goto_6
    monitor-exit p0

    return v0

    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v0}, Lbb/z;->d()I
    :try_end_d
    .catchall {:try_start_8 .. :try_end_d} :catchall_f

    move-result v0

    goto :goto_6

    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->c:Landroid/os/Bundle;

    if-nez v0, :cond_b

    .line 227
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/k;->c:Landroid/os/Bundle;

    .line 229
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public getInt(I)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 204
    const/16 v0, 0x8

    if-ne p1, v0, :cond_5

    .line 208
    :cond_5
    return v1
.end method

.method public getLong(I)J
    .registers 4
    .parameter

    .prologue
    .line 213
    if-nez p1, :cond_6

    .line 214
    iget v0, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    int-to-long v0, v0

    return-wide v0

    .line 217
    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized getString(I)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 168
    monitor-enter p0

    :try_start_2
    iget v1, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1a

    iget v1, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    iget-object v2, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v2}, Lbb/z;->d()I

    move-result v2

    if-ge v1, v2, :cond_1a

    .line 169
    iget v1, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    invoke-virtual {p0, v1}, Lcom/google/googlenav/suggest/android/k;->a(I)Lbb/w;
    :try_end_16
    .catchall {:try_start_2 .. :try_end_16} :catchall_46

    move-result-object v1

    .line 170
    packed-switch p1, :pswitch_data_4a

    .line 199
    :cond_1a
    :goto_1a
    :pswitch_1a
    monitor-exit p0

    return-object v0

    .line 172
    :pswitch_1c
    :try_start_1c
    invoke-virtual {v1}, Lbb/w;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 174
    :pswitch_21
    invoke-virtual {v1}, Lbb/w;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 176
    :pswitch_26
    invoke-virtual {v1}, Lbb/w;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 178
    :pswitch_2b
    iget v0, p0, Lcom/google/googlenav/suggest/android/k;->mPos:I

    invoke-virtual {v1, v0}, Lbb/w;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 185
    :pswitch_32
    invoke-virtual {v1}, Lbb/w;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 187
    :pswitch_37
    invoke-virtual {v1}, Lbb/w;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_1a

    .line 189
    :pswitch_3c
    invoke-virtual {v1}, Lbb/w;->e()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1a

    .line 192
    const-string v0, ""
    :try_end_45
    .catchall {:try_start_1c .. :try_end_45} :catchall_46

    goto :goto_1a

    .line 168
    :catchall_46
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_21
        :pswitch_26
        :pswitch_2b
        :pswitch_1a
        :pswitch_32
        :pswitch_37
        :pswitch_1a
        :pswitch_3c
    .end packed-switch
.end method

.method public declared-synchronized requery()Z
    .registers 4

    .prologue
    .line 111
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/k;->b:Lbb/z;

    invoke-virtual {v0, v1}, Lbb/z;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/k;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "in_progress"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/googlenav/suggest/android/k;->e:Z
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_31

    if-ne v0, v1, :cond_1c

    .line 113
    const/4 v0, 0x1

    .line 119
    :goto_1a
    monitor-exit p0

    return v0

    .line 115
    :cond_1c
    :try_start_1c
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/k;->d:Lbb/z;

    invoke-direct {p0, v0}, Lcom/google/googlenav/suggest/android/k;->a(Lbb/z;)V

    .line 118
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/k;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "in_progress"

    iget-boolean v2, p0, Lcom/google/googlenav/suggest/android/k;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 119
    invoke-super {p0}, Lcom/google/googlenav/provider/e;->requery()Z
    :try_end_2f
    .catchall {:try_start_1c .. :try_end_2f} :catchall_31

    move-result v0

    goto :goto_1a

    .line 111
    :catchall_31
    move-exception v0

    monitor-exit p0

    throw v0
.end method
