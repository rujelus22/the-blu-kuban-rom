.class public abstract Lcom/google/googlenav/suggest/android/BaseSuggestView;
.super Landroid/widget/AutoCompleteTextView;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/graphics/drawable/AnimationDrawable;

.field protected b:Z

.field protected final c:Landroid/view/inputmethod/InputMethodManager;

.field protected final d:Ljava/lang/Runnable;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private final g:Ljava/lang/Runnable;

.field private final h:Ljava/lang/Runnable;

.field private i:Lcom/google/googlenav/suggest/android/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b:Z

    .line 67
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    .line 70
    new-instance v0, Lcom/google/googlenav/suggest/android/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/b;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Lcom/google/googlenav/suggest/android/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/c;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->g:Ljava/lang/Runnable;

    .line 84
    new-instance v0, Lcom/google/googlenav/suggest/android/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/d;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->h:Ljava/lang/Runnable;

    .line 111
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a()V

    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Landroid/graphics/drawable/AnimationDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/AnimationDrawable;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b:Z

    .line 67
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    .line 70
    new-instance v0, Lcom/google/googlenav/suggest/android/b;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/b;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Lcom/google/googlenav/suggest/android/c;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/c;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->g:Ljava/lang/Runnable;

    .line 84
    new-instance v0, Lcom/google/googlenav/suggest/android/d;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/d;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->h:Ljava/lang/Runnable;

    .line 116
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a()V

    .line 117
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Lcom/google/googlenav/suggest/android/h;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->i:Lcom/google/googlenav/suggest/android/h;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/BaseSuggestView;Ljava/lang/CharSequence;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->performFiltering(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/suggest/android/BaseSuggestView;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->g:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .registers 11

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 125
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->d()V

    .line 128
    new-instance v0, Lcom/google/googlenav/suggest/android/e;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/e;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 147
    new-instance v0, Lcom/google/googlenav/suggest/android/f;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/f;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 177
    new-instance v0, Lcom/google/googlenav/suggest/android/g;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/g;-><init>(Lcom/google/googlenav/suggest/android/BaseSuggestView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->b()Lam/g;

    move-result-object v5

    .line 191
    invoke-interface {v5}, Lam/g;->a()Ljava/lang/String;

    move-result-object v6

    move v1, v2

    move v3, v2

    move v4, v2

    .line 196
    :goto_28
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_64

    .line 197
    invoke-virtual {v6, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-interface {v5, v0}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 198
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 199
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-le v0, v4, :cond_4f

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 200
    :cond_4f
    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-le v0, v3, :cond_59

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 201
    :cond_59
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    const/16 v8, 0x64

    invoke-virtual {v0, v7, v8}, Landroid/graphics/drawable/AnimationDrawable;->addFrame(Landroid/graphics/drawable/Drawable;I)V

    .line 196
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_28

    .line 203
    :cond_64
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    .line 207
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v2, v2, v4, v3}, Landroid/graphics/drawable/AnimationDrawable;->setBounds(IIII)V

    .line 210
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {p0, v9, v9, v0, v9}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 213
    invoke-virtual {p0, v2}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a(Z)V

    .line 214
    return-void
.end method

.method protected final a(Z)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 233
    if-eqz p1, :cond_1c

    .line 234
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 238
    :goto_8
    iget-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz p1, :cond_22

    const/16 v0, 0xff

    :goto_e
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/AnimationDrawable;->setAlpha(I)V

    .line 239
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/AnimationDrawable;->setVisible(ZZ)Z

    .line 240
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->invalidateSelf()V

    .line 241
    return-void

    .line 236
    :cond_1c
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->a:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_8

    :cond_22
    move v0, v1

    .line 238
    goto :goto_e
.end method

.method protected abstract b()Lam/g;
.end method

.method protected final c()V
    .registers 4

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->c:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 224
    return-void
.end method

.method protected convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 4
    .parameter

    .prologue
    .line 253
    move-object v0, p1

    check-cast v0, Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_c

    .line 258
    :goto_b
    return-object v0

    :cond_c
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_b
.end method

.method protected abstract d()V
.end method

.method public final e()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 291
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 292
    iget-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 304
    :goto_d
    return-object v0

    .line 299
    :cond_e
    iget-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 300
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    goto :goto_d

    .line 302
    :cond_19
    iput-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    .line 303
    iput-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    goto :goto_d
.end method

.method public onFilterComplete(I)V
    .registers 4
    .parameter

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_9

    .line 329
    :cond_8
    :goto_8
    return-void

    .line 322
    :cond_9
    if-lez p1, :cond_21

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->enoughToFilter()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 323
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 324
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->showDropDown()V

    goto :goto_8

    .line 326
    :cond_21
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 327
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->dismissDropDown()V

    goto :goto_8
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 280
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->isPerformingCompletion()Z

    move-result v0

    if-nez v0, :cond_b

    .line 281
    iput-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    .line 282
    iput-object v1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    .line 284
    :cond_b
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AutoCompleteTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 285
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/BaseSuggestView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/i;

    .line 266
    if-eqz v0, :cond_1b

    .line 267
    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/i;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 268
    if-eqz v1, :cond_1b

    .line 269
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->e:Ljava/lang/String;

    .line 270
    invoke-virtual {v0, v1}, Lcom/google/googlenav/suggest/android/i;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->f:Ljava/lang/String;

    .line 273
    :cond_1b
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    .line 274
    return-void
.end method

.method public setOnGetFocusListener(Lcom/google/googlenav/suggest/android/h;)V
    .registers 2
    .parameter

    .prologue
    .line 332
    iput-object p1, p0, Lcom/google/googlenav/suggest/android/BaseSuggestView;->i:Lcom/google/googlenav/suggest/android/h;

    .line 333
    return-void
.end method
