.class public Lcom/google/googlenav/suggest/android/SuggestContentProvider;
.super Landroid/content/ContentProvider;
.source "SourceFile"


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private b:Lcom/google/googlenav/suggest/android/k;

.field private final c:Landroid/content/BroadcastReceiver;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 46
    const-string v0, "content://com.google.android.maps.SuggestionProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 54
    new-instance v0, Lcom/google/googlenav/suggest/android/j;

    invoke-direct {v0, p0}, Lcom/google/googlenav/suggest/android/j;-><init>(Lcom/google/googlenav/suggest/android/SuggestContentProvider;)V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c:Landroid/content/BroadcastReceiver;

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    return-void
.end method

.method private a()V
    .registers 5

    .prologue
    .line 81
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    .line 83
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, LaR/l;->f()LaR/n;

    move-result-object v2

    const/4 v3, 0x4

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    .line 85
    invoke-virtual {v1}, LaR/l;->g()LaR/n;

    move-result-object v2

    const/4 v3, 0x6

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    .line 87
    invoke-virtual {v1}, LaR/l;->h()LaR/n;

    move-result-object v2

    const/16 v3, 0x9

    invoke-direct {p0, v2, v3}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    .line 89
    invoke-virtual {v1}, LaR/l;->i()LaR/n;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {p0, v1, v2}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a(LaR/n;I)V

    .line 96
    new-instance v1, Lbb/j;

    const/4 v2, 0x1

    invoke-static {v2}, Lbb/i;->a(I)Lbb/w;

    move-result-object v2

    invoke-direct {v1, v2}, Lbb/j;-><init>(Lbb/w;)V

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(Lbb/r;I)V

    .line 103
    return-void
.end method

.method private a(LaR/n;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    new-instance v1, Lbb/h;

    invoke-static {p2}, Lbb/i;->a(I)Lbb/w;

    move-result-object v2

    invoke-direct {v1, p1, v2, p2}, Lbb/h;-><init>(LaR/n;Lbb/w;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(Lbb/r;I)V

    .line 70
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 144
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_1a

    .line 148
    :try_start_c
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;

    .line 150
    if-eqz v0, :cond_17

    .line 151
    invoke-direct {v0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b()V
    :try_end_17
    .catchall {:try_start_c .. :try_end_17} :catchall_1b

    .line 154
    :cond_17
    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    .line 157
    :cond_1a
    return-void

    .line 154
    :catchall_1b
    move-exception v0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/suggest/android/SuggestContentProvider;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b()V

    return-void
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 111
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    monitor-enter p0

    .line 117
    :try_start_6
    iget-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    if-eqz v1, :cond_23

    .line 118
    iget-object v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 119
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    .line 123
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_6 .. :try_end_13} :catchall_25

    .line 128
    invoke-static {v0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    .line 129
    invoke-static {v0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    .line 131
    invoke-direct {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c()V

    .line 133
    invoke-static {v2, v2, v2}, Lbb/o;->a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V

    .line 135
    invoke-direct {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->a()V

    .line 136
    :goto_22
    return-void

    .line 121
    :cond_23
    :try_start_23
    monitor-exit p0

    goto :goto_22

    .line 123
    :catchall_25
    move-exception v0

    monitor-exit p0
    :try_end_27
    .catchall {:try_start_23 .. :try_end_27} :catchall_25

    throw v0
.end method

.method private c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 208
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-nez v0, :cond_e

    .line 209
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, LaM/a;->a(Landroid/content/Context;Lcom/google/googlenav/android/aa;)LaM/f;

    .line 211
    :cond_e
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    .line 212
    if-nez v0, :cond_40

    .line 213
    invoke-static {v1}, LaR/l;->a(Lcom/google/googlenav/ui/wizard/jv;)LaR/l;

    move-result-object v0

    .line 216
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->r()Z

    move-result v1

    if-eqz v1, :cond_40

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    if-eqz v1, :cond_40

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    if-eqz v1, :cond_40

    .line 218
    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->C_()V

    .line 219
    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->C_()V

    .line 222
    :cond_40
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 279
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 230
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 268
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 169
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->c:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.googlenav.suggest.android.SuggestContentProvider.INIT_SUGGEST_PROVIDER"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 175
    iput-boolean v5, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->d:Z

    .line 176
    invoke-static {v4, v4, v4}, Lbb/o;->a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V

    .line 177
    new-instance v0, Lcom/google/googlenav/suggest/android/k;

    invoke-direct {v0}, Lcom/google/googlenav/suggest/android/k;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    .line 179
    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    .line 183
    new-instance v1, Lcom/google/googlenav/suggest/android/a;

    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/googlenav/suggest/android/a;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2}, Lbb/o;->a(Lbb/r;I)V

    .line 202
    invoke-virtual {p0}, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.googlenav.suggest.android.SuggestContentProvider.SUGGEST_PROVIDER_CREATED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 203
    return v5
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 249
    new-instance v0, Lbb/u;

    invoke-direct {v0}, Lbb/u;-><init>()V

    .line 251
    :try_start_5
    invoke-virtual {v0, p4}, Lbb/u;->a([Ljava/lang/String;)Lbb/u;
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_8} :catch_31

    .line 257
    invoke-static {}, Lcom/google/googlenav/android/F;->f()Z

    move-result v1

    invoke-virtual {v0, v1}, Lbb/u;->a(Z)Lbb/u;

    .line 259
    iget-object v1, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    invoke-virtual {v0}, Lbb/u;->a()Lbb/s;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/suggest/android/k;->a(Lbb/s;)V

    .line 261
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    invoke-virtual {v0}, Lcom/google/googlenav/suggest/android/k;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "in_progress"

    invoke-static {}, Lbb/o;->a()Lbb/o;

    move-result-object v0

    invoke-virtual {v0}, Lbb/o;->h()Z

    move-result v0

    if-nez v0, :cond_34

    const/4 v0, 0x1

    :goto_2b
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 263
    iget-object v0, p0, Lcom/google/googlenav/suggest/android/SuggestContentProvider;->b:Lcom/google/googlenav/suggest/android/k;

    :goto_30
    return-object v0

    .line 252
    :catch_31
    move-exception v0

    .line 254
    const/4 v0, 0x0

    goto :goto_30

    .line 261
    :cond_34
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 274
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
