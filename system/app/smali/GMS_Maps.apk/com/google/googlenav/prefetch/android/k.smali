.class public Lcom/google/googlenav/prefetch/android/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;


# instance fields
.field private final a:LaN/B;

.field private final b:LaN/B;

.field private final c:Lo/aq;

.field private final d:Lo/aq;

.field private e:I

.field private f:I

.field private g:Lo/aq;

.field private h:Lo/aq;

.field private i:Lo/aq;


# direct methods
.method public constructor <init>(LaN/B;LaN/B;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0xe

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    .line 70
    iput-object p2, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    .line 71
    invoke-virtual {p1}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->a(ILo/T;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    .line 73
    invoke-virtual {p2}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p2}, LaN/B;->e()I

    move-result v1

    invoke-static {v0, v1}, Lo/T;->b(II)Lo/T;

    move-result-object v0

    invoke-static {v2, v0}, Lo/aq;->a(ILo/T;)Lo/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    .line 75
    invoke-direct {p0}, Lcom/google/googlenav/prefetch/android/k;->l()V

    .line 76
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/prefetch/android/k;
    .registers 4
    .parameter

    .prologue
    .line 240
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    .line 242
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, LaN/C;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v1

    .line 244
    new-instance v2, Lcom/google/googlenav/prefetch/android/k;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/prefetch/android/k;-><init>(LaN/B;LaN/B;)V

    return-object v2
.end method

.method private b(I)Landroid/util/Pair;
    .registers 7
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    sub-int v1, p1, v0

    .line 117
    if-lez v1, :cond_49

    .line 118
    new-instance v0, Lo/aq;

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    shl-int/2addr v2, v1

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    shl-int v1, v3, v1

    invoke-direct {v0, p1, v2, v1}, Lo/aq;-><init>(III)V

    .line 126
    :goto_1e
    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    sub-int v2, p1, v1

    .line 128
    if-lez v2, :cond_60

    .line 129
    new-instance v1, Lo/aq;

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->c()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    shl-int/2addr v3, v2

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    shl-int v2, v4, v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v1, p1, v3, v2}, Lo/aq;-><init>(III)V

    .line 139
    :goto_44
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 121
    :cond_49
    new-instance v0, Lo/aq;

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    neg-int v3, v1

    shr-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->c:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    neg-int v1, v1

    shr-int v1, v3, v1

    invoke-direct {v0, p1, v2, v1}, Lo/aq;-><init>(III)V

    goto :goto_1e

    .line 133
    :cond_60
    new-instance v1, Lo/aq;

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->c()I

    move-result v3

    neg-int v4, v2

    shr-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->d:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    neg-int v2, v2

    shr-int v2, v4, v2

    invoke-direct {v1, p1, v3, v2}, Lo/aq;-><init>(III)V

    goto :goto_44
.end method

.method private l()V
    .registers 2

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/googlenav/prefetch/android/k;->m()V

    .line 80
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    .line 81
    return-void
.end method

.method private m()V
    .registers 6

    .prologue
    .line 84
    const/4 v1, 0x0

    .line 87
    const/4 v0, 0x5

    move v2, v1

    move v1, v0

    :goto_4
    const/16 v0, 0xe

    if-gt v1, v0, :cond_34

    .line 88
    invoke-virtual {p0, v1}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    .line 89
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->c()I

    move-result v3

    sub-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    .line 91
    if-gez v0, :cond_1f

    .line 92
    const/4 v3, 0x1

    shl-int/2addr v3, v1

    add-int/2addr v0, v3

    .line 94
    :cond_1f
    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 87
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 97
    :cond_34
    iput v2, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    .line 98
    iput v2, p0, Lcom/google/googlenav/prefetch/android/k;->e:I

    .line 99
    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .registers 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    return-object v0
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/google/googlenav/prefetch/android/k;->b(I)Landroid/util/Pair;

    move-result-object v1

    .line 147
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    .line 148
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    .line 151
    new-instance v0, Lo/aq;

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->b()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v2}, Lo/aq;->c()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v3}, Lo/aq;->d()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lo/aq;-><init>(III)V

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    .line 153
    return-void
.end method

.method public a(Lo/aq;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 211
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/4 v3, 0x5

    if-lt v0, v3, :cond_11

    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    const/16 v3, 0xe

    if-le v0, v3, :cond_13

    :cond_11
    move v2, v1

    .line 224
    :cond_12
    :goto_12
    return v2

    .line 214
    :cond_13
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->b(I)Landroid/util/Pair;

    move-result-object v3

    .line 217
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-le v4, v0, :cond_6c

    .line 218
    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-ge v4, v0, :cond_49

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-gt v4, v0, :cond_6a

    :cond_49
    move v0, v2

    .line 224
    :goto_4a
    if-eqz v0, :cond_68

    invoke-virtual {p1}, Lo/aq;->d()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    if-lt v4, v0, :cond_68

    invoke-virtual {p1}, Lo/aq;->d()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    if-le v4, v0, :cond_12

    :cond_68
    move v2, v1

    goto :goto_12

    :cond_6a
    move v0, v1

    .line 218
    goto :goto_4a

    .line 221
    :cond_6c
    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-lt v4, v0, :cond_8a

    invoke-virtual {p1}, Lo/aq;->c()I

    move-result v4

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    if-gt v4, v0, :cond_8a

    move v0, v2

    goto :goto_4a

    :cond_8a
    move v0, v1

    goto :goto_4a
.end method

.method public b()LaN/B;
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    return-object v0
.end method

.method public c()Lo/aq;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    if-nez v0, :cond_7

    move-object v0, v2

    .line 195
    :goto_6
    return-object v0

    .line 163
    :cond_7
    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    .line 165
    new-instance v3, Lo/aq;

    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->c()I

    move-result v1

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    invoke-direct {v3, v0, v1, v4}, Lo/aq;-><init>(III)V

    .line 169
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->c()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 170
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->d()I

    move-result v0

    .line 172
    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->c()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-ne v1, v4, :cond_44

    .line 174
    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->g:Lo/aq;

    invoke-virtual {v1}, Lo/aq;->c()I

    move-result v1

    .line 175
    add-int/lit8 v0, v0, 0x1

    .line 180
    :cond_44
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v5}, Lo/aq;->b()I

    move-result v5

    shl-int/2addr v4, v5

    if-lt v1, v4, :cond_4f

    .line 181
    const/4 v1, 0x0

    .line 184
    :cond_4f
    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->h:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->d()I

    move-result v4

    if-le v0, v4, :cond_70

    .line 187
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_6d

    .line 188
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v0}, Lo/aq;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    :goto_6b
    move-object v0, v3

    .line 195
    goto :goto_6

    .line 190
    :cond_6d
    iput-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    goto :goto_6b

    .line 193
    :cond_70
    new-instance v2, Lo/aq;

    iget-object v4, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    invoke-virtual {v4}, Lo/aq;->b()I

    move-result v4

    invoke-direct {v2, v4, v1, v0}, Lo/aq;-><init>(III)V

    iput-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->i:Lo/aq;

    goto :goto_6b
.end method

.method public d()V
    .registers 2

    .prologue
    .line 103
    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->e:I

    iput v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    .line 104
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/googlenav/prefetch/android/k;->a(I)V

    .line 105
    return-void
.end method

.method public e()I
    .registers 2

    .prologue
    .line 200
    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->f:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 248
    instance-of v1, p1, Lcom/google/googlenav/prefetch/android/k;

    if-nez v1, :cond_6

    .line 252
    :cond_5
    :goto_5
    return v0

    .line 251
    :cond_6
    check-cast p1, Lcom/google/googlenav/prefetch/android/k;

    .line 252
    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    iget-object v2, p1, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v1, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    iget-object v2, p1, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v1, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public f()I
    .registers 2

    .prologue
    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4

    .prologue
    .line 231
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/r;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 232
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-static {v2}, LaN/C;->c(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 234
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-static {v2}, LaN/C;->c(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 236
    return-object v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 109
    iget v0, p0, Lcom/google/googlenav/prefetch/android/k;->e:I

    return v0
.end method

.method public i()I
    .registers 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public j()I
    .registers 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    sub-int/2addr v0, v1

    .line 270
    if-gez v0, :cond_13

    .line 271
    const v1, 0x15752a00

    add-int/2addr v0, v1

    .line 273
    :cond_13
    return v0
.end method

.method public k()LaN/B;
    .registers 5

    .prologue
    .line 277
    new-instance v0, LaN/B;

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->b:LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/googlenav/prefetch/android/k;->a:LaN/B;

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/googlenav/prefetch/android/k;->j()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method
