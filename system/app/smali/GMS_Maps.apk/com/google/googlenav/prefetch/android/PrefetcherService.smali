.class public Lcom/google/googlenav/prefetch/android/PrefetcherService;
.super Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;
.source "SourceFile"


# static fields
.field private static final d:Lcom/google/googlenav/prefetch/android/a;


# instance fields
.field private e:Lcom/google/googlenav/ui/android/ak;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 32
    new-instance v0, Lcom/google/googlenav/prefetch/android/a;

    const-class v1, Lcom/google/googlenav/prefetch/android/PrefetcherService;

    invoke-direct {v0, v1}, Lcom/google/googlenav/prefetch/android/a;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 95
    sget-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/prefetch/android/a;->a(Landroid/content/Context;)V

    .line 96
    return-void
.end method

.method public static a(Landroid/content/Context;JJ)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 99
    sget-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/prefetch/android/a;->a(Landroid/content/Context;JJ)V

    .line 100
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 103
    sget-object v0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->d:Lcom/google/googlenav/prefetch/android/a;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/prefetch/android/a;->b(Landroid/content/Context;)V

    .line 104
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 1

    .prologue
    .line 86
    invoke-static {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->a(Landroid/content/Context;)V

    .line 87
    return-void
.end method

.method protected b()V
    .registers 1

    .prologue
    .line 91
    invoke-static {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->b(Landroid/content/Context;)V

    .line 92
    return-void
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/googlenav/android/c;->a(Landroid/content/Context;)V

    .line 41
    invoke-static {p0}, Lcom/google/googlenav/android/c;->b(Landroid/content/Context;)Law/h;

    .line 42
    invoke-virtual {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    invoke-static {v0}, Lcom/google/googlenav/android/i;->a(Lcom/google/googlenav/android/AndroidGmmApplication;)V

    .line 45
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-nez v0, :cond_16

    .line 54
    :cond_15
    :goto_15
    return-void

    .line 49
    :cond_16
    invoke-static {}, LaE/f;->a()LaE/f;

    move-result-object v0

    if-nez v0, :cond_15

    .line 50
    sget-object v0, LaF/b;->a:LaF/b;

    invoke-virtual {v0}, LaF/b;->d()I

    move-result v0

    invoke-static {v0}, LaE/f;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Z)V

    goto :goto_15
.end method

.method protected d()V
    .registers 3

    .prologue
    .line 78
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 79
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->e:Lcom/google/googlenav/ui/android/ak;

    invoke-virtual {v0, v1}, LaT/a;->b(LaT/m;)V

    .line 81
    :cond_15
    invoke-static {}, Lcom/google/googlenav/android/c;->f()V

    .line 82
    return-void
.end method

.method protected e()V
    .registers 3

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;->e()V

    .line 59
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 62
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    if-nez v0, :cond_1b

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;

    invoke-virtual {p0}, Lcom/google/googlenav/prefetch/android/PrefetcherService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/y;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/internal/store/prefetch/BasePrefetcherService;)V

    invoke-static {v0}, LaT/a;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V

    .line 66
    :cond_1b
    new-instance v0, Lcom/google/googlenav/ui/android/ak;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/ak;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->e:Lcom/google/googlenav/ui/android/ak;

    .line 70
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/prefetch/android/PrefetcherService;->e:Lcom/google/googlenav/ui/android/ak;

    invoke-virtual {v0, v1}, LaT/a;->a(LaT/m;)V

    .line 72
    :cond_2b
    return-void
.end method
