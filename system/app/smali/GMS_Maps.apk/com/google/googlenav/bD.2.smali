.class Lcom/google/googlenav/bD;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/googlenav/bC;


# direct methods
.method constructor <init>(Lcom/google/googlenav/bC;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 468
    iput-object p1, p0, Lcom/google/googlenav/bD;->b:Lcom/google/googlenav/bC;

    iput p2, p0, Lcom/google/googlenav/bD;->a:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    .line 471
    const-string v0, "Stars$StarsCacheUpdater.runNextTaskInBackground$1.run"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 473
    :try_start_5
    iget-object v0, p0, Lcom/google/googlenav/bD;->b:Lcom/google/googlenav/bC;

    invoke-static {v0}, Lcom/google/googlenav/bC;->c(Lcom/google/googlenav/bC;)LaR/u;

    move-result-object v0

    invoke-interface {v0}, LaR/u;->a()Ljava/util/List;

    move-result-object v0

    .line 474
    new-instance v2, Lcom/google/common/collect/aw;

    invoke-direct {v2}, Lcom/google/common/collect/aw;-><init>()V

    .line 476
    new-instance v3, Lcom/google/common/collect/ay;

    invoke-direct {v3}, Lcom/google/common/collect/ay;-><init>()V

    .line 478
    const/4 v1, 0x0

    .line 479
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_46

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 480
    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v5

    if-eqz v5, :cond_62

    .line 481
    invoke-static {v0}, Lcom/google/googlenav/bz;->a(LaR/D;)Lcom/google/googlenav/by;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    .line 482
    invoke-virtual {v0}, LaR/D;->h()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v0, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v5, v1}, Lcom/google/common/collect/ay;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ay;

    :goto_44
    move v1, v0

    goto :goto_1e

    .line 485
    :cond_46
    iget-object v0, p0, Lcom/google/googlenav/bD;->b:Lcom/google/googlenav/bC;

    iget v1, p0, Lcom/google/googlenav/bD;->a:I

    new-instance v4, Lcom/google/googlenav/bB;

    invoke-virtual {v2}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/common/collect/ay;->a()Lcom/google/common/collect/ax;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Lcom/google/googlenav/bB;-><init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ax;)V

    invoke-static {v0, v1, v4}, Lcom/google/googlenav/bC;->a(Lcom/google/googlenav/bC;ILcom/google/googlenav/bB;)V
    :try_end_5a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5a} :catch_60

    .line 490
    :goto_5a
    const-string v0, "Stars$StarsCacheUpdater.runNextTaskInBackground$1.run"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 491
    return-void

    .line 487
    :catch_60
    move-exception v0

    goto :goto_5a

    :cond_62
    move v0, v1

    goto :goto_44
.end method
