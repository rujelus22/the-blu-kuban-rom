.class public Lcom/google/googlenav/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lam/f;

.field private final b:Lcom/google/googlenav/ui/bs;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;


# direct methods
.method public constructor <init>(Lam/f;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/google/googlenav/ay;->a:Lam/f;

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ay;->b:Lcom/google/googlenav/ui/bs;

    .line 91
    iput-object p2, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object v0, p0, Lcom/google/googlenav/ay;->a:Lam/f;

    .line 101
    iput-object p1, p0, Lcom/google/googlenav/ay;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 102
    iput-object v0, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    .line 103
    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-virtual {p1, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/googlenav/ay;->b:Lcom/google/googlenav/ui/bs;

    .line 105
    iput-boolean v3, p0, Lcom/google/googlenav/ay;->d:Z

    .line 106
    return-void
.end method


# virtual methods
.method public a()Lam/f;
    .registers 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/googlenav/ay;->a:Lam/f;

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 10
    .parameter

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x1

    .line 164
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/iI;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 166
    iget-object v0, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    if-nez v0, :cond_2b

    .line 168
    invoke-virtual {p0}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 169
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 170
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x55

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 171
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 172
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 203
    :goto_2a
    return-object v1

    .line 174
    :cond_2b
    if-ne p1, v7, :cond_63

    .line 176
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 177
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 179
    const/16 v0, 0x1000

    :try_start_40
    new-array v0, v0, [B

    .line 181
    :goto_42
    invoke-virtual {v3, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4

    if-eq v4, v7, :cond_55

    .line 182
    const/4 v5, 0x0

    invoke-virtual {v2, v0, v5, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_4c
    .catchall {:try_start_40 .. :try_end_4c} :catchall_4d

    goto :goto_42

    .line 185
    :catchall_4d
    move-exception v0

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 186
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    throw v0

    .line 185
    :cond_55
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 186
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 188
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 189
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_2a

    .line 195
    :cond_63
    iget-object v0, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    invoke-static {v0}, Lbm/a;->a(Ljava/lang/String;)I

    move-result v0

    .line 196
    iget-object v2, p0, Lcom/google/googlenav/ay;->c:Ljava/lang/String;

    invoke-static {v2, v0, p1}, Lbm/a;->a(Ljava/lang/String;II)[B

    move-result-object v0

    .line 198
    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_2a
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/google/googlenav/ay;->d:Z

    .line 142
    return-void
.end method

.method public b()Lcom/google/googlenav/ui/bs;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/googlenav/ay;->b:Lcom/google/googlenav/ui/bs;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/googlenav/ay;->e:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/googlenav/ay;->d:Z

    return v0
.end method
