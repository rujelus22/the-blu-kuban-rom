.class public Lcom/google/googlenav/ui/wizard/jC;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ui/wizard/eh;

.field private B:Lcom/google/googlenav/ui/wizard/em;

.field private C:Lcom/google/googlenav/ui/wizard/eq;

.field private D:Lcom/google/googlenav/ui/wizard/eO;

.field private E:Lcom/google/googlenav/ui/wizard/eP;

.field private F:Lcom/google/googlenav/ui/wizard/fe;

.field private G:Lcom/google/googlenav/ui/wizard/hh;

.field private H:Lcom/google/googlenav/ui/wizard/hy;

.field private I:Lcom/google/googlenav/ui/wizard/ia;

.field private J:Lcom/google/googlenav/ui/wizard/ib;

.field private K:Lcom/google/googlenav/ui/wizard/iy;

.field private L:Lcom/google/googlenav/ui/wizard/ik;

.field private M:Lcom/google/googlenav/ui/wizard/ie;

.field private N:Lcom/google/googlenav/ui/wizard/fq;

.field private O:Lcom/google/googlenav/ui/wizard/gk;

.field private P:Lcom/google/googlenav/ui/wizard/fY;

.field private Q:Lcom/google/googlenav/ui/wizard/iC;

.field private R:Lcom/google/googlenav/ui/wizard/iH;

.field private S:Lcom/google/googlenav/ui/wizard/iL;

.field private T:Lcom/google/googlenav/ui/wizard/iU;

.field private U:Lcom/google/googlenav/ui/wizard/fl;

.field private V:Lcom/google/googlenav/ui/wizard/ew;

.field private W:Lcom/google/googlenav/ui/wizard/jb;

.field private X:Lcom/google/googlenav/ui/wizard/jk;

.field private Y:Lcom/google/googlenav/ui/wizard/jt;

.field protected final a:Lcom/google/googlenav/ui/wizard/jv;

.field private b:Lcom/google/googlenav/ui/wizard/q;

.field private c:Lcom/google/googlenav/ui/wizard/G;

.field private d:Lcom/google/googlenav/ui/wizard/D;

.field private e:Lcom/google/googlenav/ui/wizard/bk;

.field private f:Lcom/google/googlenav/ui/wizard/aZ;

.field private g:Lcom/google/googlenav/ui/wizard/br;

.field private h:Lcom/google/googlenav/ui/wizard/fk;

.field private i:Lcom/google/googlenav/ui/wizard/bv;

.field private j:Lcom/google/googlenav/ui/wizard/bx;

.field private k:Lcom/google/googlenav/ui/wizard/bI;

.field private l:Lcom/google/googlenav/ui/wizard/bN;

.field private m:Lcom/google/googlenav/ui/wizard/bV;

.field private n:Lcom/google/googlenav/ui/wizard/ca;

.field private o:Lcom/google/googlenav/ui/wizard/cb;

.field private p:Lcom/google/googlenav/ui/wizard/ce;

.field private q:Lcom/google/googlenav/ui/wizard/cf;

.field private r:Lcom/google/googlenav/ui/wizard/cr;

.field private s:Lcom/google/googlenav/ui/wizard/cu;

.field private t:Lcom/google/googlenav/ui/wizard/cx;

.field private u:Lcom/google/googlenav/ui/wizard/M;

.field private v:Lcom/google/googlenav/ui/wizard/gG;

.field private w:Lcom/google/googlenav/ui/wizard/cC;

.field private x:Lcom/google/googlenav/ui/wizard/cH;

.field private y:Lcom/google/googlenav/ui/wizard/cZ;

.field private z:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    .line 96
    return-void
.end method


# virtual methods
.method public A()Lcom/google/googlenav/ui/wizard/fe;
    .registers 3

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->F:Lcom/google/googlenav/ui/wizard/fe;

    if-nez v0, :cond_d

    .line 299
    new-instance v0, Lcom/google/googlenav/ui/wizard/fe;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/fe;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->F:Lcom/google/googlenav/ui/wizard/fe;

    .line 301
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->F:Lcom/google/googlenav/ui/wizard/fe;

    return-object v0
.end method

.method public B()Lcom/google/googlenav/ui/wizard/fY;
    .registers 3

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->P:Lcom/google/googlenav/ui/wizard/fY;

    if-nez v0, :cond_d

    .line 306
    new-instance v0, Lcom/google/googlenav/ui/wizard/fY;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/fY;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->P:Lcom/google/googlenav/ui/wizard/fY;

    .line 308
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->P:Lcom/google/googlenav/ui/wizard/fY;

    return-object v0
.end method

.method public C()Lcom/google/googlenav/ui/wizard/hh;
    .registers 4

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->G:Lcom/google/googlenav/ui/wizard/hh;

    if-nez v0, :cond_13

    .line 313
    new-instance v0, Lcom/google/googlenav/ui/wizard/hh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/hh;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->G:Lcom/google/googlenav/ui/wizard/hh;

    .line 315
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->G:Lcom/google/googlenav/ui/wizard/hh;

    return-object v0
.end method

.method public D()Lcom/google/googlenav/ui/wizard/fk;
    .registers 4

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->h:Lcom/google/googlenav/ui/wizard/fk;

    if-nez v0, :cond_f

    .line 320
    new-instance v0, Lcom/google/googlenav/ui/wizard/fk;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    sget-object v2, Lcom/google/googlenav/offers/j;->b:Lcom/google/googlenav/offers/j;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/fk;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/view/dialog/bl;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->h:Lcom/google/googlenav/ui/wizard/fk;

    .line 322
    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->h:Lcom/google/googlenav/ui/wizard/fk;

    return-object v0
.end method

.method public E()Lcom/google/googlenav/ui/wizard/hy;
    .registers 4

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->H:Lcom/google/googlenav/ui/wizard/hy;

    if-nez v0, :cond_13

    .line 328
    new-instance v0, Lcom/google/googlenav/ui/wizard/hy;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/hy;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->H:Lcom/google/googlenav/ui/wizard/hy;

    .line 330
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->H:Lcom/google/googlenav/ui/wizard/hy;

    return-object v0
.end method

.method public F()Lcom/google/googlenav/ui/wizard/ia;
    .registers 3

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->I:Lcom/google/googlenav/ui/wizard/ia;

    if-nez v0, :cond_d

    .line 335
    new-instance v0, Lcom/google/googlenav/ui/wizard/ia;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/ia;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->I:Lcom/google/googlenav/ui/wizard/ia;

    .line 337
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->I:Lcom/google/googlenav/ui/wizard/ia;

    return-object v0
.end method

.method public G()Lcom/google/googlenav/ui/wizard/fq;
    .registers 3

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->N:Lcom/google/googlenav/ui/wizard/fq;

    if-nez v0, :cond_10

    .line 342
    invoke-static {}, Lcom/google/googlenav/ui/view/android/ad;->a()Lcom/google/googlenav/ui/view/android/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/ad;->a(Lcom/google/googlenav/ui/wizard/jv;)Lcom/google/googlenav/ui/wizard/fq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->N:Lcom/google/googlenav/ui/wizard/fq;

    .line 345
    :cond_10
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->N:Lcom/google/googlenav/ui/wizard/fq;

    return-object v0
.end method

.method public H()Lcom/google/googlenav/ui/wizard/gk;
    .registers 6

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->O:Lcom/google/googlenav/ui/wizard/gk;

    if-nez v0, :cond_24

    .line 350
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->B()Lcom/google/googlenav/ui/ak;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/jv;->D()LaN/u;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;LaH/m;Lcom/google/googlenav/ui/ak;LaN/u;)Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->O:Lcom/google/googlenav/ui/wizard/gk;

    .line 355
    :cond_24
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->O:Lcom/google/googlenav/ui/wizard/gk;

    return-object v0
.end method

.method public I()Lcom/google/googlenav/ui/wizard/br;
    .registers 4

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->g:Lcom/google/googlenav/ui/wizard/br;

    if-nez v0, :cond_20

    .line 360
    new-instance v0, Lcom/google/googlenav/ui/wizard/br;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/br;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaH/m;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->g:Lcom/google/googlenav/ui/wizard/br;

    .line 362
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->g:Lcom/google/googlenav/ui/wizard/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/br;->A()LaM/g;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 365
    :cond_20
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->g:Lcom/google/googlenav/ui/wizard/br;

    return-object v0
.end method

.method public J()Lcom/google/googlenav/ui/wizard/ib;
    .registers 3

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->J:Lcom/google/googlenav/ui/wizard/ib;

    if-nez v0, :cond_d

    .line 370
    new-instance v0, Lcom/google/googlenav/ui/wizard/ib;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/ib;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->J:Lcom/google/googlenav/ui/wizard/ib;

    .line 372
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->J:Lcom/google/googlenav/ui/wizard/ib;

    return-object v0
.end method

.method public K()Lcom/google/googlenav/ui/wizard/iy;
    .registers 3

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->K:Lcom/google/googlenav/ui/wizard/iy;

    if-nez v0, :cond_d

    .line 377
    new-instance v0, Lcom/google/googlenav/ui/wizard/iy;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/iy;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->K:Lcom/google/googlenav/ui/wizard/iy;

    .line 379
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->K:Lcom/google/googlenav/ui/wizard/iy;

    return-object v0
.end method

.method public L()Lcom/google/googlenav/ui/wizard/ik;
    .registers 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->L:Lcom/google/googlenav/ui/wizard/ik;

    if-nez v0, :cond_d

    .line 384
    new-instance v0, Lcom/google/googlenav/ui/wizard/ik;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/ik;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->L:Lcom/google/googlenav/ui/wizard/ik;

    .line 386
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->L:Lcom/google/googlenav/ui/wizard/ik;

    return-object v0
.end method

.method public M()Lcom/google/googlenav/ui/wizard/bV;
    .registers 3

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->m:Lcom/google/googlenav/ui/wizard/bV;

    if-nez v0, :cond_1a

    .line 404
    new-instance v0, Lcom/google/googlenav/ui/wizard/bV;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/bV;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->m:Lcom/google/googlenav/ui/wizard/bV;

    .line 406
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->m:Lcom/google/googlenav/ui/wizard/bV;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/bV;->A()LaM/g;

    move-result-object v1

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 409
    :cond_1a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->m:Lcom/google/googlenav/ui/wizard/bV;

    return-object v0
.end method

.method public N()Lcom/google/googlenav/ui/wizard/iC;
    .registers 4

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Q:Lcom/google/googlenav/ui/wizard/iC;

    if-nez v0, :cond_15

    .line 414
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    .line 415
    new-instance v1, Lcom/google/googlenav/ui/wizard/iC;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/iC;-><init>(Lcom/google/googlenav/ui/wizard/jv;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->Q:Lcom/google/googlenav/ui/wizard/iC;

    .line 418
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Q:Lcom/google/googlenav/ui/wizard/iC;

    return-object v0
.end method

.method public O()Lcom/google/googlenav/ui/wizard/iH;
    .registers 5

    .prologue
    .line 422
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->q()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->R:Lcom/google/googlenav/ui/wizard/iH;

    if-nez v0, :cond_22

    .line 423
    new-instance v0, Lcom/google/googlenav/ui/wizard/iH;

    new-instance v1, Lcom/google/googlenav/bh;

    invoke-direct {v1}, Lcom/google/googlenav/bh;-><init>()V

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/iH;-><init>(Lcom/google/googlenav/bh;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->R:Lcom/google/googlenav/ui/wizard/iH;

    .line 425
    :cond_22
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->R:Lcom/google/googlenav/ui/wizard/iH;

    return-object v0
.end method

.method public P()Lcom/google/googlenav/ui/wizard/iL;
    .registers 6

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->S:Lcom/google/googlenav/ui/wizard/iL;

    if-nez v0, :cond_2c

    .line 430
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    .line 431
    new-instance v1, LaC/a;

    new-instance v2, Landroid/location/Geocoder;

    new-instance v3, Ljava/util/Locale;

    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/googlenav/common/e;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    invoke-direct {v1, v2}, LaC/a;-><init>(Landroid/location/Geocoder;)V

    .line 433
    new-instance v0, Lcom/google/googlenav/ui/wizard/iL;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/ui/wizard/iL;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaC/a;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->S:Lcom/google/googlenav/ui/wizard/iL;

    .line 436
    :cond_2c
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->S:Lcom/google/googlenav/ui/wizard/iL;

    return-object v0
.end method

.method public Q()Lcom/google/googlenav/ui/wizard/iU;
    .registers 4

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->T:Lcom/google/googlenav/ui/wizard/iU;

    if-nez v0, :cond_15

    .line 441
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    .line 442
    new-instance v1, Lcom/google/googlenav/ui/wizard/iU;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/iU;-><init>(Lcom/google/googlenav/ui/wizard/jv;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->T:Lcom/google/googlenav/ui/wizard/iU;

    .line 444
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->T:Lcom/google/googlenav/ui/wizard/iU;

    return-object v0
.end method

.method public R()Lcom/google/googlenav/ui/wizard/fl;
    .registers 3

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->U:Lcom/google/googlenav/ui/wizard/fl;

    if-nez v0, :cond_d

    .line 449
    new-instance v0, Lcom/google/googlenav/ui/wizard/fl;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/fl;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->U:Lcom/google/googlenav/ui/wizard/fl;

    .line 451
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->U:Lcom/google/googlenav/ui/wizard/fl;

    return-object v0
.end method

.method public S()Lcom/google/googlenav/ui/wizard/ew;
    .registers 4

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->V:Lcom/google/googlenav/ui/wizard/ew;

    if-nez v0, :cond_f

    .line 456
    new-instance v0, Lcom/google/googlenav/ui/wizard/ew;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    sget-object v2, Lcom/google/googlenav/offers/j;->a:Lcom/google/googlenav/offers/j;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/ew;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/view/dialog/bl;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->V:Lcom/google/googlenav/ui/wizard/ew;

    .line 459
    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->V:Lcom/google/googlenav/ui/wizard/ew;

    return-object v0
.end method

.method public T()Lcom/google/googlenav/ui/wizard/jb;
    .registers 3

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->W:Lcom/google/googlenav/ui/wizard/jb;

    if-nez v0, :cond_d

    .line 464
    new-instance v0, Lcom/google/googlenav/ui/wizard/jb;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/jb;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->W:Lcom/google/googlenav/ui/wizard/jb;

    .line 466
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->W:Lcom/google/googlenav/ui/wizard/jb;

    return-object v0
.end method

.method public U()Lcom/google/googlenav/ui/wizard/jk;
    .registers 3

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->X:Lcom/google/googlenav/ui/wizard/jk;

    if-nez v0, :cond_d

    .line 471
    new-instance v0, Lcom/google/googlenav/ui/wizard/jk;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/jk;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->X:Lcom/google/googlenav/ui/wizard/jk;

    .line 473
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->X:Lcom/google/googlenav/ui/wizard/jk;

    return-object v0
.end method

.method public V()Lcom/google/googlenav/ui/wizard/eq;
    .registers 4

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->C:Lcom/google/googlenav/ui/wizard/eq;

    if-nez v0, :cond_13

    .line 478
    new-instance v0, Lcom/google/googlenav/ui/wizard/eq;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/eq;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->C:Lcom/google/googlenav/ui/wizard/eq;

    .line 480
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->C:Lcom/google/googlenav/ui/wizard/eq;

    return-object v0
.end method

.method public W()Lcom/google/googlenav/ui/wizard/jt;
    .registers 3

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Y:Lcom/google/googlenav/ui/wizard/jt;

    if-nez v0, :cond_10

    .line 485
    invoke-static {}, Lcom/google/googlenav/ui/view/android/ad;->a()Lcom/google/googlenav/ui/view/android/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/ad;->b(Lcom/google/googlenav/ui/wizard/jv;)Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Y:Lcom/google/googlenav/ui/wizard/jt;

    .line 487
    :cond_10
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Y:Lcom/google/googlenav/ui/wizard/jt;

    return-object v0
.end method

.method public X()Z
    .registers 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->e:Lcom/google/googlenav/ui/wizard/bk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->e:Lcom/google/googlenav/ui/wizard/bk;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bk;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public Y()Z
    .registers 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->i:Lcom/google/googlenav/ui/wizard/bv;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->i:Lcom/google/googlenav/ui/wizard/bv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public Z()Z
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->l:Lcom/google/googlenav/ui/wizard/bN;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->l:Lcom/google/googlenav/ui/wizard/bN;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bN;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a()Lcom/google/googlenav/ui/wizard/q;
    .registers 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->b:Lcom/google/googlenav/ui/wizard/q;

    if-nez v0, :cond_d

    .line 100
    new-instance v0, Lcom/google/googlenav/ui/wizard/q;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/q;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->b:Lcom/google/googlenav/ui/wizard/q;

    .line 102
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->b:Lcom/google/googlenav/ui/wizard/q;

    return-object v0
.end method

.method public aa()Z
    .registers 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->n:Lcom/google/googlenav/ui/wizard/ca;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->n:Lcom/google/googlenav/ui/wizard/ca;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ab()Z
    .registers 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->N:Lcom/google/googlenav/ui/wizard/fq;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->N:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ac()Z
    .registers 2

    .prologue
    .line 583
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->O:Lcom/google/googlenav/ui/wizard/gk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->O:Lcom/google/googlenav/ui/wizard/gk;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ad()Z
    .registers 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->J:Lcom/google/googlenav/ui/wizard/ib;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->J:Lcom/google/googlenav/ui/wizard/ib;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ib;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ae()Z
    .registers 2

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->L:Lcom/google/googlenav/ui/wizard/ik;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->L:Lcom/google/googlenav/ui/wizard/ik;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ik;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public af()Z
    .registers 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->U:Lcom/google/googlenav/ui/wizard/fl;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->U:Lcom/google/googlenav/ui/wizard/fl;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fl;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ag()Z
    .registers 2

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->X:Lcom/google/googlenav/ui/wizard/jk;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->X:Lcom/google/googlenav/ui/wizard/jk;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jk;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ah()Z
    .registers 2

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Y:Lcom/google/googlenav/ui/wizard/jt;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Y:Lcom/google/googlenav/ui/wizard/jt;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->o()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public ai()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 667
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->d:Lcom/google/googlenav/ui/wizard/D;

    .line 668
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->f:Lcom/google/googlenav/ui/wizard/aZ;

    .line 669
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->u:Lcom/google/googlenav/ui/wizard/M;

    .line 670
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->P:Lcom/google/googlenav/ui/wizard/fY;

    .line 671
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->e:Lcom/google/googlenav/ui/wizard/bk;

    .line 672
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->h:Lcom/google/googlenav/ui/wizard/fk;

    .line 673
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->g:Lcom/google/googlenav/ui/wizard/br;

    .line 674
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->f:Lcom/google/googlenav/ui/wizard/aZ;

    .line 675
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->i:Lcom/google/googlenav/ui/wizard/bv;

    .line 676
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->j:Lcom/google/googlenav/ui/wizard/bx;

    .line 677
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->k:Lcom/google/googlenav/ui/wizard/bI;

    .line 678
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->l:Lcom/google/googlenav/ui/wizard/bN;

    .line 679
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->m:Lcom/google/googlenav/ui/wizard/bV;

    .line 680
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->n:Lcom/google/googlenav/ui/wizard/ca;

    .line 681
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->o:Lcom/google/googlenav/ui/wizard/cb;

    .line 682
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->p:Lcom/google/googlenav/ui/wizard/ce;

    .line 683
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->q:Lcom/google/googlenav/ui/wizard/cf;

    .line 684
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->s:Lcom/google/googlenav/ui/wizard/cu;

    .line 685
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->t:Lcom/google/googlenav/ui/wizard/cx;

    .line 686
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->w:Lcom/google/googlenav/ui/wizard/cC;

    .line 687
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->y:Lcom/google/googlenav/ui/wizard/cZ;

    .line 688
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->x:Lcom/google/googlenav/ui/wizard/cH;

    .line 689
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->z:Lcom/google/googlenav/ui/wizard/dg;

    .line 690
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->A:Lcom/google/googlenav/ui/wizard/eh;

    .line 691
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->B:Lcom/google/googlenav/ui/wizard/em;

    .line 692
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->C:Lcom/google/googlenav/ui/wizard/eq;

    .line 693
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->D:Lcom/google/googlenav/ui/wizard/eO;

    .line 694
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->E:Lcom/google/googlenav/ui/wizard/eP;

    .line 695
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->F:Lcom/google/googlenav/ui/wizard/fe;

    .line 696
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->N:Lcom/google/googlenav/ui/wizard/fq;

    .line 697
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->O:Lcom/google/googlenav/ui/wizard/gk;

    .line 698
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->G:Lcom/google/googlenav/ui/wizard/hh;

    .line 699
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->H:Lcom/google/googlenav/ui/wizard/hy;

    .line 700
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->I:Lcom/google/googlenav/ui/wizard/ia;

    .line 701
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->J:Lcom/google/googlenav/ui/wizard/ib;

    .line 702
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->L:Lcom/google/googlenav/ui/wizard/ik;

    .line 703
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->K:Lcom/google/googlenav/ui/wizard/iy;

    .line 704
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->M:Lcom/google/googlenav/ui/wizard/ie;

    .line 705
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->R:Lcom/google/googlenav/ui/wizard/iH;

    .line 706
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->S:Lcom/google/googlenav/ui/wizard/iL;

    .line 707
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->T:Lcom/google/googlenav/ui/wizard/iU;

    .line 708
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->U:Lcom/google/googlenav/ui/wizard/fl;

    .line 709
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->W:Lcom/google/googlenav/ui/wizard/jb;

    .line 710
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->X:Lcom/google/googlenav/ui/wizard/jk;

    .line 711
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->Y:Lcom/google/googlenav/ui/wizard/jt;

    .line 712
    return-void
.end method

.method public b()Lcom/google/googlenav/ui/wizard/D;
    .registers 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->d:Lcom/google/googlenav/ui/wizard/D;

    if-nez v0, :cond_d

    .line 107
    new-instance v0, Lcom/google/googlenav/ui/wizard/D;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/D;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->d:Lcom/google/googlenav/ui/wizard/D;

    .line 109
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->d:Lcom/google/googlenav/ui/wizard/D;

    return-object v0
.end method

.method public c()Lcom/google/googlenav/ui/wizard/G;
    .registers 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->c:Lcom/google/googlenav/ui/wizard/G;

    if-nez v0, :cond_d

    .line 114
    new-instance v0, Lcom/google/googlenav/ui/wizard/G;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/G;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->c:Lcom/google/googlenav/ui/wizard/G;

    .line 116
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->c:Lcom/google/googlenav/ui/wizard/G;

    return-object v0
.end method

.method public d()Lcom/google/googlenav/ui/wizard/bk;
    .registers 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->e:Lcom/google/googlenav/ui/wizard/bk;

    if-nez v0, :cond_d

    .line 121
    new-instance v0, Lcom/google/googlenav/ui/wizard/bk;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/bk;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->e:Lcom/google/googlenav/ui/wizard/bk;

    .line 123
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->e:Lcom/google/googlenav/ui/wizard/bk;

    return-object v0
.end method

.method public e()Lcom/google/googlenav/ui/wizard/bv;
    .registers 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->i:Lcom/google/googlenav/ui/wizard/bv;

    if-nez v0, :cond_13

    .line 128
    new-instance v0, Lcom/google/googlenav/ui/wizard/bv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bv;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->i:Lcom/google/googlenav/ui/wizard/bv;

    .line 130
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->i:Lcom/google/googlenav/ui/wizard/bv;

    return-object v0
.end method

.method public f()Lcom/google/googlenav/ui/wizard/bx;
    .registers 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->j:Lcom/google/googlenav/ui/wizard/bx;

    if-nez v0, :cond_d

    .line 135
    new-instance v0, Lcom/google/googlenav/ui/wizard/bx;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/bx;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->j:Lcom/google/googlenav/ui/wizard/bx;

    .line 137
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->j:Lcom/google/googlenav/ui/wizard/bx;

    return-object v0
.end method

.method public g()Lcom/google/googlenav/ui/wizard/bI;
    .registers 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->k:Lcom/google/googlenav/ui/wizard/bI;

    if-nez v0, :cond_d

    .line 142
    new-instance v0, Lcom/google/googlenav/ui/wizard/bI;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/bI;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->k:Lcom/google/googlenav/ui/wizard/bI;

    .line 144
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->k:Lcom/google/googlenav/ui/wizard/bI;

    return-object v0
.end method

.method public h()Lcom/google/googlenav/ui/wizard/bN;
    .registers 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->l:Lcom/google/googlenav/ui/wizard/bN;

    if-nez v0, :cond_d

    .line 149
    new-instance v0, Lcom/google/googlenav/ui/wizard/bN;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/bN;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->l:Lcom/google/googlenav/ui/wizard/bN;

    .line 151
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->l:Lcom/google/googlenav/ui/wizard/bN;

    return-object v0
.end method

.method public i()Lcom/google/googlenav/ui/wizard/ca;
    .registers 5

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->n:Lcom/google/googlenav/ui/wizard/ca;

    if-nez v0, :cond_19

    .line 156
    new-instance v0, Lcom/google/googlenav/ui/wizard/ca;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/ca;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaH/m;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->n:Lcom/google/googlenav/ui/wizard/ca;

    .line 159
    :cond_19
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->n:Lcom/google/googlenav/ui/wizard/ca;

    return-object v0
.end method

.method public j()Lcom/google/googlenav/ui/wizard/cb;
    .registers 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->o:Lcom/google/googlenav/ui/wizard/cb;

    if-nez v0, :cond_21

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 165
    new-instance v0, Lcom/google/googlenav/ui/wizard/cb;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/cb;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->o:Lcom/google/googlenav/ui/wizard/cb;

    .line 167
    :cond_21
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->o:Lcom/google/googlenav/ui/wizard/cb;

    return-object v0
.end method

.method public k()Lcom/google/googlenav/ui/wizard/ce;
    .registers 4

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->p:Lcom/google/googlenav/ui/wizard/ce;

    if-nez v0, :cond_13

    .line 172
    new-instance v0, Lcom/google/googlenav/ui/wizard/ce;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/ce;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->p:Lcom/google/googlenav/ui/wizard/ce;

    .line 174
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->p:Lcom/google/googlenav/ui/wizard/ce;

    return-object v0
.end method

.method public l()Lcom/google/googlenav/ui/wizard/cf;
    .registers 3

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->q:Lcom/google/googlenav/ui/wizard/cf;

    if-nez v0, :cond_d

    .line 179
    new-instance v0, Lcom/google/googlenav/ui/wizard/cf;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/cf;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->q:Lcom/google/googlenav/ui/wizard/cf;

    .line 181
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->q:Lcom/google/googlenav/ui/wizard/cf;

    return-object v0
.end method

.method public m()Lcom/google/googlenav/ui/wizard/cx;
    .registers 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->t:Lcom/google/googlenav/ui/wizard/cx;

    if-nez v0, :cond_2f

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 187
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->a()LaB/a;

    move-result-object v0

    .line 189
    new-instance v1, Lcom/google/googlenav/ui/wizard/cx;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/cx;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaB/a;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->t:Lcom/google/googlenav/ui/wizard/cx;

    .line 191
    :cond_2f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->t:Lcom/google/googlenav/ui/wizard/cx;

    return-object v0
.end method

.method public n()Lcom/google/googlenav/ui/wizard/M;
    .registers 5

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->u:Lcom/google/googlenav/ui/wizard/M;

    if-nez v0, :cond_14

    .line 196
    new-instance v0, Lcom/google/googlenav/ui/wizard/M;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v2, Lcom/google/googlenav/ui/wizard/aF;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/aF;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/M;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/aF;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->u:Lcom/google/googlenav/ui/wizard/M;

    .line 199
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->u:Lcom/google/googlenav/ui/wizard/M;

    return-object v0
.end method

.method public o()Lcom/google/googlenav/ui/wizard/gG;
    .registers 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->v:Lcom/google/googlenav/ui/wizard/gG;

    if-nez v0, :cond_d

    .line 204
    new-instance v0, Lcom/google/googlenav/ui/wizard/gG;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/gG;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->v:Lcom/google/googlenav/ui/wizard/gG;

    .line 206
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->v:Lcom/google/googlenav/ui/wizard/gG;

    return-object v0
.end method

.method public p()Lcom/google/googlenav/ui/wizard/eh;
    .registers 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->A:Lcom/google/googlenav/ui/wizard/eh;

    if-nez v0, :cond_d

    .line 211
    new-instance v0, Lcom/google/googlenav/ui/wizard/eh;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/eh;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->A:Lcom/google/googlenav/ui/wizard/eh;

    .line 213
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->A:Lcom/google/googlenav/ui/wizard/eh;

    return-object v0
.end method

.method public q()Lcom/google/googlenav/ui/wizard/em;
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->B:Lcom/google/googlenav/ui/wizard/em;

    if-nez v0, :cond_d

    .line 218
    new-instance v0, Lcom/google/googlenav/ui/wizard/em;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/em;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->B:Lcom/google/googlenav/ui/wizard/em;

    .line 220
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->B:Lcom/google/googlenav/ui/wizard/em;

    return-object v0
.end method

.method public r()Lcom/google/googlenav/ui/wizard/cC;
    .registers 4

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->w:Lcom/google/googlenav/ui/wizard/cC;

    if-nez v0, :cond_13

    .line 225
    new-instance v0, Lcom/google/googlenav/ui/wizard/cC;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/cC;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->w:Lcom/google/googlenav/ui/wizard/cC;

    .line 227
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->w:Lcom/google/googlenav/ui/wizard/cC;

    return-object v0
.end method

.method public s()Lcom/google/googlenav/ui/wizard/cr;
    .registers 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->r:Lcom/google/googlenav/ui/wizard/cr;

    if-nez v0, :cond_d

    .line 232
    new-instance v0, Lcom/google/googlenav/ui/wizard/cr;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/cr;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->r:Lcom/google/googlenav/ui/wizard/cr;

    .line 234
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->r:Lcom/google/googlenav/ui/wizard/cr;

    return-object v0
.end method

.method public t()Lcom/google/googlenav/ui/wizard/cu;
    .registers 3

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->s:Lcom/google/googlenav/ui/wizard/cu;

    if-nez v0, :cond_d

    .line 239
    new-instance v0, Lcom/google/googlenav/ui/wizard/cu;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/cu;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->s:Lcom/google/googlenav/ui/wizard/cu;

    .line 241
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->s:Lcom/google/googlenav/ui/wizard/cu;

    return-object v0
.end method

.method public u()Lcom/google/googlenav/ui/wizard/cH;
    .registers 4

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->x:Lcom/google/googlenav/ui/wizard/cH;

    if-nez v0, :cond_15

    .line 246
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    .line 247
    new-instance v1, Lcom/google/googlenav/ui/wizard/cH;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v0}, Lcom/google/googlenav/friend/history/z;->a(Landroid/content/Context;)Lcom/google/googlenav/friend/history/z;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/cH;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/history/z;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->x:Lcom/google/googlenav/ui/wizard/cH;

    .line 250
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->x:Lcom/google/googlenav/ui/wizard/cH;

    return-object v0
.end method

.method public v()Lcom/google/googlenav/ui/wizard/aZ;
    .registers 4

    .prologue
    .line 254
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->f:Lcom/google/googlenav/ui/wizard/aZ;

    if-nez v1, :cond_15

    .line 256
    new-instance v1, Lcom/google/googlenav/ui/wizard/aZ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v0}, Lcom/google/googlenav/friend/history/z;->a(Landroid/content/Context;)Lcom/google/googlenav/friend/history/z;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/aZ;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/history/z;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->f:Lcom/google/googlenav/ui/wizard/aZ;

    .line 259
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->f:Lcom/google/googlenav/ui/wizard/aZ;

    return-object v0
.end method

.method public w()Lcom/google/googlenav/ui/wizard/cZ;
    .registers 4

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->y:Lcom/google/googlenav/ui/wizard/cZ;

    if-nez v0, :cond_13

    .line 264
    new-instance v0, Lcom/google/googlenav/ui/wizard/cZ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/cZ;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->y:Lcom/google/googlenav/ui/wizard/cZ;

    .line 267
    :cond_13
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->y:Lcom/google/googlenav/ui/wizard/cZ;

    return-object v0
.end method

.method public x()Lcom/google/googlenav/ui/wizard/dg;
    .registers 9

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->z:Lcom/google/googlenav/ui/wizard/dg;

    if-nez v0, :cond_31

    .line 272
    new-instance v0, Lcom/google/googlenav/ui/wizard/dg;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->C()LaN/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->D()LaN/u;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/jv;->A()LaH/m;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v6

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v7}, Lcom/google/googlenav/ui/wizard/jv;->B()Lcom/google/googlenav/ui/ak;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/dg;-><init>(Lcom/google/googlenav/ui/wizard/jv;LaN/p;LaN/u;LaH/m;Lcom/google/googlenav/aA;Lcom/google/googlenav/J;Lcom/google/googlenav/ui/ak;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->z:Lcom/google/googlenav/ui/wizard/dg;

    .line 280
    :cond_31
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->z:Lcom/google/googlenav/ui/wizard/dg;

    return-object v0
.end method

.method public y()Lcom/google/googlenav/ui/wizard/eO;
    .registers 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->D:Lcom/google/googlenav/ui/wizard/eO;

    if-nez v0, :cond_d

    .line 285
    new-instance v0, Lcom/google/googlenav/ui/wizard/eO;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/eO;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->D:Lcom/google/googlenav/ui/wizard/eO;

    .line 287
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->D:Lcom/google/googlenav/ui/wizard/eO;

    return-object v0
.end method

.method public z()Lcom/google/googlenav/ui/wizard/eP;
    .registers 3

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->E:Lcom/google/googlenav/ui/wizard/eP;

    if-nez v0, :cond_d

    .line 292
    new-instance v0, Lcom/google/googlenav/ui/wizard/eP;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jC;->a:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/wizard/eP;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->E:Lcom/google/googlenav/ui/wizard/eP;

    .line 294
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jC;->E:Lcom/google/googlenav/ui/wizard/eP;

    return-object v0
.end method
