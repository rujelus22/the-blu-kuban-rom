.class public Lcom/google/googlenav/ui/android/AndroidVectorView;
.super Lcom/google/googlenav/ui/android/BaseAndroidView;
.source "SourceFile"


# instance fields
.field private f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

.field private g:Lcom/google/android/maps/driveabout/vector/j;

.field private h:Lcom/google/android/maps/driveabout/vector/av;

.field private i:Lcom/google/googlenav/ui/android/aa;

.field private j:LaO/e;

.field private final k:Lcom/google/android/maps/driveabout/vector/bz;

.field private final l:Lcom/google/android/maps/driveabout/vector/by;

.field private final m:LaM/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 320
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/BaseAndroidView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 228
    new-instance v0, Lcom/google/googlenav/ui/android/f;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/f;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->k:Lcom/google/android/maps/driveabout/vector/bz;

    .line 249
    new-instance v0, Lcom/google/googlenav/ui/android/g;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/g;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->l:Lcom/google/android/maps/driveabout/vector/by;

    .line 287
    new-instance v0, Lcom/google/googlenav/ui/android/h;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/h;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->m:LaM/h;

    .line 321
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/AndroidVectorView;)LaO/e;
    .registers 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    return-object v0
.end method

.method private a(LA/c;Lr/H;I)Lr/D;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/bf;->a(LA/c;Landroid/content/Context;Landroid/content/res/Resources;)Lr/z;

    move-result-object v0

    check-cast v0, Lr/D;

    .line 633
    invoke-virtual {v0, p2}, Lr/D;->a(Lr/H;)V

    .line 635
    int-to-long v1, p3

    invoke-virtual {v0, v1, v2}, Lr/D;->a(J)V

    .line 637
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LA/c;)V

    .line 638
    return-object v0
.end method

.method private a(LA/c;)V
    .registers 3
    .parameter

    .prologue
    .line 648
    invoke-static {p1}, Lr/C;->b(LA/c;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 650
    invoke-static {p1}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->b()V

    .line 651
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(LA/c;)V

    .line 655
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->f()V

    .line 657
    :cond_1b
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 401
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_78

    sget-object v0, Lcom/google/android/maps/driveabout/vector/m;->b:Lcom/google/android/maps/driveabout/vector/m;

    .line 404
    :goto_d
    new-instance v1, Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/j;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/m;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    .line 407
    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/ButtonContainer;->d()Lcom/google/googlenav/ui/android/ab;

    move-result-object v0

    .line 408
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ab;->a()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    .line 409
    new-instance v1, Lcom/google/googlenav/ui/android/i;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/android/i;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Lcom/google/googlenav/ui/android/ab;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->i:Lcom/google/googlenav/ui/android/aa;

    .line 417
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->i:Lcom/google/googlenav/ui/android/aa;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ab;->a(Lcom/google/googlenav/ui/android/aa;)V

    .line 419
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 420
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 421
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_7b

    .line 422
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    const v2, 0x7f0b004c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b004d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    .line 428
    :cond_60
    :goto_60
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/j;->b(Z)V

    .line 429
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    new-instance v1, Lcom/google/googlenav/ui/android/j;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/j;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/j;->a(Lcom/google/android/maps/driveabout/vector/F;)V

    .line 438
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 439
    return-void

    .line 401
    :cond_78
    sget-object v0, Lcom/google/android/maps/driveabout/vector/m;->a:Lcom/google/android/maps/driveabout/vector/m;

    goto :goto_d

    .line 424
    :cond_7b
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_60

    .line 425
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    const v2, 0x7f0b0053

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v3, v0}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    goto :goto_60
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/AndroidVectorView;)V
    .registers 1
    .parameter

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->q()V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/AndroidVectorView;)Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .registers 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    return-object v0
.end method

.method private n()V
    .registers 2

    .prologue
    .line 445
    invoke-static {}, Lcom/google/googlenav/K;->D()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 446
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->p()V

    .line 450
    :goto_9
    return-void

    .line 448
    :cond_a
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->q()V

    goto :goto_9
.end method

.method private o()V
    .registers 1

    .prologue
    .line 457
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->r()V

    .line 459
    return-void
.end method

.method private p()V
    .registers 5

    .prologue
    .line 540
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->c()I

    move-result v0

    .line 542
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/clientparam/k;->b()Z

    move-result v1

    .line 544
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/clientparam/k;->a()J

    move-result-wide v2

    .line 546
    invoke-static {v0, v1}, Lr/D;->a(IZ)Lr/H;

    move-result-object v0

    .line 548
    sget-object v1, LA/c;->q:LA/c;

    long-to-int v2, v2

    invoke-direct {p0, v1, v0, v2}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;Lr/H;I)Lr/D;

    .line 549
    return-void
.end method

.method private q()V
    .registers 2

    .prologue
    .line 555
    sget-object v0, LA/c;->q:LA/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;)V

    .line 556
    return-void
.end method

.method private r()V
    .registers 2

    .prologue
    .line 607
    sget-object v0, LA/c;->r:LA/c;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;)V

    .line 608
    return-void
.end method

.method private s()V
    .registers 5

    .prologue
    .line 612
    invoke-static {}, Lcom/google/googlenav/clientparam/f;->d()Lcom/google/googlenav/clientparam/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/k;->a()J

    move-result-wide v0

    .line 615
    sget-object v2, LA/c;->s:LA/c;

    invoke-static {}, Lr/D;->n()Lr/H;

    move-result-object v3

    long-to-int v0, v0

    invoke-direct {p0, v2, v3, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(LA/c;Lr/H;I)Lr/D;

    move-result-object v0

    .line 622
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lr/D;->b(Z)V

    .line 623
    return-void
.end method


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 464
    return-void
.end method

.method public a(IFFLcom/google/android/maps/driveabout/vector/aX;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->x()LG/a;

    move-result-object v1

    invoke-static {v1, p1, p2, p3, p4}, LG/a;->a(LG/a;IFFLcom/google/android/maps/driveabout/vector/aX;)LG/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setLabelTheme(LG/a;)V

    .line 711
    return-void
.end method

.method public a(LaN/B;Landroid/view/View;Lcom/google/googlenav/ui/view/d;Lcom/google/googlenav/ui/view/c;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 669
    const/4 v8, 0x0

    .line 670
    new-instance v0, LF/H;

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v3

    invoke-static {v1, v3}, Lo/T;->b(II)Lo/T;

    move-result-object v1

    move-object v3, v2

    move v5, v4

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v8}, LF/H;-><init>(Lo/T;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IILjava/lang/String;Ljava/lang/String;Z)V

    .line 675
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v2, Lcom/google/googlenav/ui/android/k;

    invoke-direct {v2, p0, p4, p3}, Lcom/google/googlenav/ui/android/k;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/d;)V

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    .line 689
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v2, Lcom/google/android/maps/driveabout/vector/f;

    invoke-direct {v2, p2}, Lcom/google/android/maps/driveabout/vector/f;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 690
    return-void
.end method

.method public a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 326
    const-string v0, "AndroidVectorView.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 327
    iput-object p1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    .line 328
    new-instance v0, Lcom/google/googlenav/ui/android/b;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/b;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->c:Lcom/google/googlenav/ui/android/b;

    .line 331
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    invoke-virtual {p1}, Lcom/google/googlenav/android/i;->g()LaN/p;

    move-result-object v4

    check-cast v4, LaO/a;

    .line 334
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 335
    new-instance v0, Lcom/google/googlenav/ui/android/l;

    iget-object v2, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->a:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/l;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Landroid/content/Context;Landroid/content/res/Resources;LaO/a;Lcom/google/googlenav/ui/android/f;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    .line 336
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->k:Lcom/google/android/maps/driveabout/vector/bz;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V

    .line 337
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->l:Lcom/google/android/maps/driveabout/vector/by;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setInterceptingOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/by;)V

    .line 338
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->addView(Landroid/view/View;)V

    .line 340
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v4, v0}, LaO/a;->a(Lcom/google/android/maps/driveabout/vector/bA;)V

    .line 341
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v4, v0}, LaO/a;->a(Lo/C;)V

    .line 342
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->A()Lcom/google/android/maps/driveabout/vector/aq;

    move-result-object v0

    invoke-virtual {v4, v0}, LaO/a;->a(Lcom/google/android/maps/driveabout/vector/aq;)V

    .line 343
    invoke-virtual {p1}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    check-cast v0, LaO/e;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    .line 344
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v1

    invoke-virtual {v0, v1}, LaO/e;->a(F)V

    .line 345
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    invoke-virtual {v4}, LaO/a;->d()LaN/B;

    move-result-object v1

    invoke-virtual {v4}, LaO/a;->c()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaO/e;->e(LaN/B;LaN/Y;)V

    .line 351
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->j:LaO/e;

    const/high16 v1, 0x4248

    invoke-virtual {v0, v1}, LaO/e;->b(F)V

    .line 356
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->n()V

    .line 357
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->o()V

    .line 358
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->s()V

    .line 362
    const/16 v0, 0x16

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->s()F

    move-result v1

    invoke-static {v0, v1}, LaO/e;->a(LaN/Y;F)F

    move-result v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bk;->b(F)V

    .line 365
    invoke-virtual {p1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    .line 366
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    .line 368
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, LS/e;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LS/e;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(LS/a;)V

    .line 371
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, LS/d;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LS/d;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(LS/a;)V

    .line 374
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(Lcom/google/googlenav/ui/android/ButtonContainer;)V

    .line 377
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_da

    .line 383
    :cond_da
    invoke-static {}, Lcom/google/googlenav/K;->C()Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 386
    :cond_e0
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/AndroidVectorView;->setFocusable(Z)V

    .line 387
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/android/AndroidVectorView;->setFocusableInTouchMode(Z)V

    .line 390
    sget-object v0, LaE/d;->a:LaE/d;

    invoke-virtual {v0}, LaE/d;->e()Z

    move-result v0

    if-eqz v0, :cond_fc

    .line 391
    new-instance v0, Lcom/google/android/maps/driveabout/vector/av;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/av;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->h:Lcom/google/android/maps/driveabout/vector/av;

    .line 392
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->h:Lcom/google/android/maps/driveabout/vector/av;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 395
    :cond_fc
    const-string v0, "AndroidVectorView.initialize"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 396
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 470
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->m:LaM/h;

    invoke-virtual {v0, v1}, LaM/f;->b(LaM/h;)V

    .line 471
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b()V

    .line 472
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 479
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_15

    .line 480
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 482
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    new-instance v1, Lcom/google/googlenav/ui/android/m;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/android/m;-><init>(Lcom/google/googlenav/ui/android/AndroidVectorView;Lcom/google/googlenav/ui/android/f;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(Lcom/google/android/maps/driveabout/vector/aG;)V

    .line 483
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->n_()V

    .line 491
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->m:LaM/h;

    invoke-virtual {v0, v1}, LaM/f;->a(LaM/h;)V

    .line 492
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->j()V

    .line 500
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 501
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    .line 502
    invoke-virtual {v0}, Lbg/b;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbg/b;->e(Lbf/i;)V

    .line 504
    :cond_22
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 508
    invoke-super {p0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->e()V

    .line 512
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    .line 513
    invoke-virtual {v0}, Lbg/b;->ab()V

    .line 514
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->o_()V

    .line 519
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    check-cast v0, Lbg/b;

    .line 520
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbg/b;->a(Lcom/google/android/maps/driveabout/vector/VectorMapView;)V

    .line 521
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->i()V

    .line 526
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->n()V

    .line 531
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->s()V

    .line 532
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 698
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    if-eqz v0, :cond_9

    .line 699
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->p()V

    .line 701
    :cond_9
    return-void
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/VectorMapView;
    .registers 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    return-object v0
.end method

.method public k()V
    .registers 2

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->w()V

    .line 715
    return-void
.end method

.method public l()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 789
    .line 791
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    .line 792
    return-void
.end method

.method public setCompassMargin(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 776
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    if-eqz v0, :cond_f

    .line 777
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->g:Lcom/google/android/maps/driveabout/vector/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/j;->a(II)V

    .line 779
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidVectorView;->f:Lcom/google/android/maps/driveabout/vector/VectorMapView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(ZZ)V

    .line 781
    :cond_f
    return-void
.end method
