.class public Lcom/google/googlenav/ui/wizard/dG;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 910
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    .line 911
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/dialog/r;-><init>(Lcom/google/googlenav/ui/e;)V

    .line 912
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 908
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dG;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .registers 14
    .parameter

    .prologue
    const/16 v11, 0x32f

    const/16 v10, 0x328

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 991
    new-instance v4, Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {v4, p1, v8}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    .line 995
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->f(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 996
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->g(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v0

    if-eqz v0, :cond_dd

    :cond_24
    const/16 v0, 0x1cf

    .line 998
    :goto_26
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1000
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dg;->h(Lcom/google/googlenav/ui/wizard/dg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/text/SpannableStringBuilder;)V

    .line 1001
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dJ;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dJ;-><init>(Lcom/google/googlenav/ui/wizard/dG;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 1011
    :cond_4b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_83

    .line 1012
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->e(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-eqz v0, :cond_e1

    const/16 v0, 0x32d

    .line 1013
    :goto_61
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dg;->e(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v1

    if-eqz v1, :cond_e4

    const/16 v1, 0x312

    .line 1015
    :goto_6b
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aw:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    .line 1017
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    new-instance v3, Lcom/google/googlenav/ui/wizard/dK;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/wizard/dK;-><init>(Lcom/google/googlenav/ui/wizard/dG;I)V

    invoke-virtual {v2, v1, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 1027
    :cond_83
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/dg;->i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    move v3, v0

    :goto_8f
    if-ge v3, v5, :cond_104

    .line 1029
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 1030
    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_ff

    .line 1031
    invoke-virtual {v0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-nez v1, :cond_e7

    .line 1033
    invoke-static {}, Lcom/google/googlenav/friend/ad;->v()V

    .line 1034
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1049
    :goto_b2
    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 1050
    if-nez v2, :cond_bd

    .line 1051
    invoke-static {v0, v9}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    .line 1055
    :cond_bd
    new-instance v6, Landroid/text/SpannableStringBuilder;

    sget-object v7, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v7}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-direct {v6, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1059
    invoke-static {v0, v6}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/text/SpannableStringBuilder;)V

    .line 1063
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dL;

    invoke-direct {v2, p0, v1, v3}, Lcom/google/googlenav/ui/wizard/dL;-><init>(Lcom/google/googlenav/ui/wizard/dG;Ljava/lang/Integer;I)V

    invoke-virtual {v0, v6, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 1027
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8f

    .line 996
    :cond_dd
    const/16 v0, 0x3e3

    goto/16 :goto_26

    .line 1012
    :cond_e1
    const/4 v0, -0x1

    goto/16 :goto_61

    .line 1013
    :cond_e4
    const/16 v1, 0x311

    goto :goto_6b

    .line 1035
    :cond_e7
    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-ne v9, v1, :cond_f7

    .line 1038
    invoke-static {}, Lcom/google/googlenav/friend/ad;->u()V

    .line 1039
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_b2

    .line 1041
    :cond_f7
    invoke-static {}, Lcom/google/googlenav/friend/ad;->t()V

    .line 1042
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_b2

    .line 1045
    :cond_ff
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_b2

    .line 1072
    :cond_104
    return-object v4
.end method

.method public c()Landroid/view/View;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 921
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dG;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ea

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 924
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_41

    .line 925
    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 926
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/dg;->d(Lcom/google/googlenav/ui/wizard/dg;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 927
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 928
    const v0, 0x7f100033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 929
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x4

    if-ne v2, v3, :cond_97

    .line 930
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 931
    const v2, 0x7f02022a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 947
    :cond_41
    :goto_41
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 948
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dG;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/dG;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 949
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 950
    new-instance v2, Lcom/google/googlenav/ui/wizard/dH;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dH;-><init>(Lcom/google/googlenav/ui/wizard/dG;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 968
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->e(Lcom/google/googlenav/ui/wizard/dg;)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 969
    const v0, 0x7f10002e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 970
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 971
    const v0, 0x7f100030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 972
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 973
    const/16 v2, 0x4fc

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->o:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 975
    new-instance v2, Lcom/google/googlenav/ui/wizard/dI;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dI;-><init>(Lcom/google/googlenav/ui/wizard/dG;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 983
    :cond_96
    return-object v1

    .line 932
    :cond_97
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x5

    if-ne v2, v3, :cond_a8

    .line 933
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 934
    const v2, 0x7f020228

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_41

    .line 936
    :cond_a8
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_b8

    .line 938
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_41

    .line 940
    :cond_b8
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 941
    const v2, 0x7f020215

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_41
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 916
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dG;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/dg;->d(Lcom/google/googlenav/ui/wizard/dg;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
