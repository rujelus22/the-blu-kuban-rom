.class Lcom/google/googlenav/ui/wizard/U;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/aC;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/S;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/S;)V
    .registers 2
    .parameter

    .prologue
    .line 529
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/U;)V
    .registers 1
    .parameter

    .prologue
    .line 529
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/U;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/U;Lcom/google/googlenav/ui/wizard/bo;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 529
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/U;->a(Lcom/google/googlenav/ui/wizard/bo;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/bo;)V
    .registers 10
    .parameter

    .prologue
    .line 829
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x8e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v7, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 837
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/U;)V
    .registers 1
    .parameter

    .prologue
    .line 529
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/U;->j()V

    return-void
.end method

.method private i()V
    .registers 3

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->f(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/ui/wizard/S;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 815
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->w()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 816
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->b(Z)V

    .line 817
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/U;->e()V

    .line 818
    const/16 v0, 0x8f

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(II)Landroid/widget/Toast;

    .line 822
    :cond_2d
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    .line 841
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    .line 842
    const/16 v1, 0x90

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 844
    new-instance v0, Lcom/google/googlenav/ct;

    invoke-direct {v0}, Lcom/google/googlenav/ct;-><init>()V

    .line 845
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->w()Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 846
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->a:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 850
    :goto_2f
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    .line 851
    return-void

    .line 848
    :cond_3f
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->b:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_2f
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/lang/String;)V

    .line 535
    return-void
.end method

.method public a(Lcom/google/googlenav/h;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 553
    invoke-virtual {p1}, Lcom/google/googlenav/h;->l()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 554
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->e(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/aF;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/V;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/V;-><init>(Lcom/google/googlenav/ui/wizard/U;Lcom/google/googlenav/h;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/wizard/aF;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aK;)V

    .line 575
    :goto_15
    return-void

    .line 571
    :cond_16
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V

    .line 572
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/util/List;)V

    .line 573
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ah;->a()V

    goto :goto_15
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/lang/String;)V

    .line 541
    return-void
.end method

.method public a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->H()Lcom/google/googlenav/friend/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/j;->k()Lcom/google/googlenav/friend/i;

    move-result-object v0

    .line 805
    invoke-virtual {v0, p1}, Lcom/google/googlenav/friend/i;->b(Z)V

    .line 806
    new-instance v0, Lcom/google/googlenav/ct;

    invoke-direct {v0}, Lcom/google/googlenav/ct;-><init>()V

    .line 807
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    .line 808
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->b(Lcom/google/googlenav/ui/wizard/S;)LaG/h;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(LaG/h;)V

    .line 547
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 763
    new-instance v0, Lcom/google/googlenav/ui/wizard/x;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/x;-><init>()V

    .line 765
    if-eqz p1, :cond_38

    :goto_7
    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/x;->a:Ljava/lang/String;

    .line 766
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->l(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/aU;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/x;->b:Lcom/google/googlenav/aU;

    .line 767
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->m(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/ak;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/x;->c:LaN/B;

    .line 770
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/an;->c(Z)V

    .line 771
    new-instance v1, Lcom/google/googlenav/ui/wizard/ab;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/ab;-><init>(Lcom/google/googlenav/ui/wizard/U;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/x;->d:Lcom/google/googlenav/ui/wizard/y;

    .line 796
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/x;)V

    .line 797
    return-void

    .line 765
    :cond_38
    const-string p1, ""

    goto :goto_7
.end method

.method public c()V
    .registers 3

    .prologue
    .line 580
    new-instance v0, Lcom/google/googlenav/ui/wizard/gJ;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/gJ;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gJ;->a(Z)Lcom/google/googlenav/ui/wizard/gJ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gJ;->b(Z)Lcom/google/googlenav/ui/wizard/gJ;

    move-result-object v0

    const v1, 0x7f0200b0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gJ;->a(I)Lcom/google/googlenav/ui/wizard/gJ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->f(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gJ;->a(Ljava/util/List;)Lcom/google/googlenav/ui/wizard/gJ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gJ;->a(LaB/s;)Lcom/google/googlenav/ui/wizard/gJ;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/W;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/W;-><init>(Lcom/google/googlenav/ui/wizard/U;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gJ;->a(Lcom/google/googlenav/ui/wizard/gI;)Lcom/google/googlenav/ui/wizard/gJ;

    move-result-object v0

    .line 598
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/gJ;)V

    .line 599
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 603
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 604
    const-string v0, "Check in"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    .line 607
    :cond_b
    invoke-static {}, Lcom/google/googlenav/friend/W;->j()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 609
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->g(Lcom/google/googlenav/ui/wizard/S;)V

    .line 655
    :goto_16
    return-void

    .line 616
    :cond_17
    new-instance v0, Lcom/google/googlenav/ui/wizard/X;

    invoke-static {}, Lcom/google/googlenav/friend/W;->e()Lcom/google/googlenav/friend/W;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/wizard/X;-><init>(Lcom/google/googlenav/ui/wizard/U;Lcom/google/googlenav/friend/bi;)V

    .line 654
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bd;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    goto :goto_16
.end method

.method public e()V
    .registers 4

    .prologue
    .line 660
    invoke-static {}, Lcom/google/googlenav/friend/as;->j()Z

    move-result v0

    if-nez v0, :cond_2d

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->w()Z

    move-result v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x1

    .line 663
    :goto_13
    new-instance v1, Lcom/google/googlenav/ui/wizard/Y;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/Y;-><init>(Lcom/google/googlenav/ui/wizard/U;)V

    .line 681
    if-eqz v0, :cond_2f

    .line 682
    new-instance v0, Lcom/google/googlenav/ui/wizard/Z;

    invoke-static {}, Lcom/google/googlenav/friend/ac;->e()Lcom/google/googlenav/friend/bi;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1}, Lcom/google/googlenav/ui/wizard/Z;-><init>(Lcom/google/googlenav/ui/wizard/U;Lcom/google/googlenav/friend/bi;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 702
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bd;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 710
    :goto_2c
    return-void

    .line 660
    :cond_2d
    const/4 v0, 0x0

    goto :goto_13

    .line 704
    :cond_2f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->w()Z

    move-result v0

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->f(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/ui/wizard/S;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_4d

    .line 705
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/U;->a(Lcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_2c

    .line 707
    :cond_4d
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/U;->j()V

    goto :goto_2c
.end method

.method public f()V
    .registers 4

    .prologue
    .line 715
    new-instance v0, Lcom/google/googlenav/ct;

    invoke-direct {v0}, Lcom/google/googlenav/ct;-><init>()V

    .line 717
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->x()Z

    move-result v1

    if-eqz v1, :cond_4f

    .line 718
    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v1

    if-nez v1, :cond_42

    .line 719
    invoke-static {}, Lcom/google/googlenav/friend/E;->e()Lcom/google/googlenav/friend/E;

    move-result-object v1

    const/16 v2, 0x842

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(I)V

    .line 721
    new-instance v1, Lcom/google/googlenav/ui/wizard/aa;

    invoke-static {}, Lcom/google/googlenav/friend/E;->e()Lcom/google/googlenav/friend/E;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/google/googlenav/ui/wizard/aa;-><init>(Lcom/google/googlenav/ui/wizard/U;Lcom/google/googlenav/friend/bi;Lcom/google/googlenav/ct;)V

    .line 750
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/bd;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 757
    :goto_32
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/ct;)V

    .line 758
    return-void

    .line 752
    :cond_42
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_32

    .line 755
    :cond_4f
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/S;->k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ct;->c:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    goto :goto_32
.end method

.method public g()V
    .registers 2

    .prologue
    .line 855
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->i(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ac;->e()V

    .line 856
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->h(Lcom/google/googlenav/ui/wizard/S;)V

    .line 867
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/U;->a:Lcom/google/googlenav/ui/wizard/S;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/S;->i(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ac;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ac;->b()V

    .line 868
    return-void
.end method
