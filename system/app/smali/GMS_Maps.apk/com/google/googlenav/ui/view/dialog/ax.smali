.class public Lcom/google/googlenav/ui/view/dialog/ax;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;

.field private b:Lcom/google/googlenav/ui/view/dialog/aD;

.field private c:Lcom/google/googlenav/ui/view/dialog/aB;

.field private d:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/aD;)V
    .registers 5
    .parameter

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    .line 202
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    .line 218
    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    .line 219
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->w_()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f100025

    const v2, 0x7f020218

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/ax;->a(Ljava/lang/CharSequence;II)V

    .line 220
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/ax;)Lcom/google/googlenav/ui/view/dialog/aD;
    .registers 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    return-object v0
.end method

.method private b(Ljava/util/List;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 333
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 334
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 335
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 336
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    .line 337
    instance-of v2, v0, Lcom/google/googlenav/friend/history/x;

    if-eqz v2, :cond_b

    .line 338
    check-cast v0, Lcom/google/googlenav/friend/history/x;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 343
    :cond_21
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 344
    :cond_25
    :goto_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    .line 345
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    .line 349
    instance-of v2, v0, Lcom/google/googlenav/friend/history/aa;

    if-eqz v2, :cond_38

    .line 350
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 354
    :cond_38
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_45

    .line 356
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 361
    :cond_45
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/google/googlenav/friend/history/b;->b(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    const/16 v2, 0xe

    if-le v0, v2, :cond_25

    .line 362
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_25

    .line 368
    :cond_55
    invoke-static {p2, p3}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v0

    .line 370
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v6

    :goto_5e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/b;

    .line 371
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_8c

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/U;->b()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v1

    if-ltz v1, :cond_8c

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/U;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/history/U;->e()Z

    move-result v1

    if-eqz v1, :cond_9b

    .line 374
    :cond_8c
    new-instance v1, Lcom/google/googlenav/friend/history/X;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v0, v4}, Lcom/google/googlenav/friend/history/X;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    :goto_99
    move v2, v0

    .line 378
    goto :goto_5e

    .line 376
    :cond_9b
    new-instance v4, Lcom/google/googlenav/friend/history/j;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/friend/history/U;

    iget-object v8, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-direct {v4, v0, v5, v1, v8}, Lcom/google/googlenav/friend/history/j;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;Lcom/google/googlenav/friend/history/U;Lcom/google/googlenav/ui/view/dialog/aD;)V

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    add-int/lit8 v0, v2, 0x1

    goto :goto_99

    .line 383
    :cond_b2
    invoke-static {v7}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 387
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/friend/history/s;

    if-nez v0, :cond_e0

    .line 388
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    .line 390
    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    const/4 v4, -0x7

    invoke-virtual {v2, v4}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    .line 393
    invoke-interface {v7, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 398
    :cond_e0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/x;

    .line 401
    instance-of v1, v0, Lcom/google/googlenav/friend/history/s;

    if-nez v1, :cond_125

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_125

    .line 403
    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    .line 406
    const/4 v0, 0x7

    invoke-virtual {v2, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    .line 407
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    if-gtz v0, :cond_126

    .line 409
    :goto_116
    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    .line 411
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_125
    return-object v7

    .line 407
    :cond_126
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    goto :goto_116
.end method

.method static synthetic n()Lcom/google/googlenav/friend/history/b;
    .registers 1

    .prologue
    .line 67
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    return-object v0
.end method

.method private static o()Lcom/google/googlenav/friend/history/b;
    .registers 1

    .prologue
    .line 239
    invoke-static {}, Lcom/google/googlenav/friend/history/b;->a()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public I_()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 450
    const v0, 0x7f020218

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ay;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/ay;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    new-array v2, v5, [I

    const/4 v3, 0x0

    const/16 v4, 0xbc3

    aput v4, v2, v3

    invoke-virtual {p0, v5, v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/ax;->a(ZILaA/f;[I)V

    .line 459
    return-void
.end method

.method protected K_()Z
    .registers 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    if-eqz v0, :cond_b

    .line 255
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/aD;->a()V

    .line 256
    const/4 v0, 0x1

    .line 258
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected O_()V
    .registers 2

    .prologue
    .line 602
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_e

    .line 603
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ax;->requestWindowFeature(I)Z

    .line 605
    :cond_e
    return-void
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 3
    .parameter

    .prologue
    .line 249
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 250
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;)V
    .registers 9
    .parameter

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 431
    const/4 v0, -0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    .line 432
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    .line 435
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/friend/history/b;->a(Lcom/google/googlenav/friend/history/b;)I

    move-result v0

    if-gtz v0, :cond_2e

    .line 437
    :goto_19
    iget-object v6, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 440
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->h()V

    .line 441
    return-void

    .line 435
    :cond_2e
    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    goto :goto_19
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 280
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/ax;->b(Ljava/util/List;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)Ljava/util/List;

    move-result-object v1

    .line 288
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1a

    .line 289
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 292
    :cond_1a
    instance-of v2, v0, Lcom/google/googlenav/friend/history/P;

    if-eqz v2, :cond_20

    .line 293
    check-cast v0, Lcom/google/googlenav/friend/history/P;

    .line 296
    :cond_20
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 297
    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 300
    if-eqz v0, :cond_35

    .line 301
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    :cond_31
    :goto_31
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->h()V

    .line 320
    return-void

    .line 306
    :cond_35
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    .line 308
    check-cast v0, Lcom/google/googlenav/friend/history/x;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/x;->c()Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 310
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/friend/history/P;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/friend/history/P;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/aD;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_31
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 617
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-nez v0, :cond_49

    .line 622
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/view/dialog/aA;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/dialog/aA;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/friend/history/b;->g()I

    move-result v3

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/friend/history/b;->f()I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/friend/history/b;->e()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 638
    invoke-static {}, Lcom/google/googlenav/android/a;->b()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 640
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/friend/history/b;->d()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 643
    :cond_41
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 644
    invoke-static {}, Lcom/google/googlenav/friend/history/y;->d()V

    .line 645
    const/4 v0, 0x1

    .line 647
    :goto_48
    return v0

    :cond_49
    const/4 v0, 0x0

    goto :goto_48
.end method

.method public c()Landroid/view/View;
    .registers 8

    .prologue
    const/4 v2, 0x0

    .line 463
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e5

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 465
    const v0, 0x7f1002b6

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    .line 466
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aB;

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/view/dialog/aB;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;Lcom/google/googlenav/ui/view/dialog/ay;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->c:Lcom/google/googlenav/ui/view/dialog/aB;

    .line 468
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/az;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/az;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ai;)V

    .line 485
    new-instance v4, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v4}, Lcom/google/googlenav/friend/history/b;-><init>()V

    .line 486
    const/4 v0, -0x2

    invoke-virtual {v4, v0}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v0

    .line 487
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v2, Lcom/google/googlenav/friend/history/aa;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/friend/history/aa;-><init>(Lcom/google/googlenav/friend/history/b;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 491
    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    const/4 v5, -0x7

    invoke-virtual {v4, v5}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    .line 495
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/friend/history/P;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/friend/history/P;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/dialog/aD;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 503
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->c:Lcom/google/googlenav/ui/view/dialog/aB;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/x;)V

    .line 504
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 505
    return-object v6
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 244
    const/4 v0, 0x1

    return v0
.end method

.method h()V
    .registers 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->c:Lcom/google/googlenav/ui/view/dialog/aB;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aB;->c()V

    .line 446
    return-void
.end method

.method public l()V
    .registers 7

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 655
    new-instance v4, Lcom/google/googlenav/friend/history/b;

    invoke-direct {v4}, Lcom/google/googlenav/friend/history/b;-><init>()V

    .line 656
    new-instance v0, Lcom/google/googlenav/friend/history/s;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    const/4 v5, -0x7

    invoke-virtual {v4, v5}, Lcom/google/googlenav/friend/history/b;->a(I)Lcom/google/googlenav/friend/history/b;

    move-result-object v4

    invoke-static {}, Lcom/google/googlenav/ui/view/dialog/ax;->o()Lcom/google/googlenav/friend/history/b;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/s;-><init>(Landroid/content/Context;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aD;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;)V

    .line 659
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 660
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/ax;->h()V

    .line 661
    return-void
.end method

.method public m()V
    .registers 3

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ax;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/O;->a()V

    .line 668
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    if-eqz v0, :cond_9

    .line 511
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/ax;->b:Lcom/google/googlenav/ui/view/dialog/aD;

    invoke-interface {v0}, Lcom/google/googlenav/ui/view/dialog/aD;->a()V

    .line 513
    :cond_9
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 609
    const/16 v0, 0x286

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v1, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 611
    const v1, 0x7f02023a

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 612
    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 224
    const/16 v0, 0x288

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
