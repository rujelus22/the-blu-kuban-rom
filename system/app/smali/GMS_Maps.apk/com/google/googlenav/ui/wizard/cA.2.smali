.class public Lcom/google/googlenav/ui/wizard/ca;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/dy;


# static fields
.field public static a:[I


# instance fields
.field protected b:I

.field protected c:I

.field protected i:I

.field protected j:Lax/j;

.field protected k:Lcom/google/googlenav/J;

.field private final l:Lax/z;

.field private m:Z

.field private final n:LaH/m;

.field private o:Lcom/google/googlenav/ui/view/p;

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaH/m;Lcom/google/googlenav/J;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 113
    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    .line 116
    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    .line 119
    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    .line 159
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    .line 165
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    .line 170
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    .line 171
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    .line 172
    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->l:Lax/z;

    .line 173
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->i()V

    .line 174
    return-void
.end method

.method public static H()I
    .registers 5

    .prologue
    .line 946
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->Q()I

    move-result v0

    .line 948
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    .line 949
    const-string v2, "DIRECTIONS_MODE"

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v2

    .line 950
    if-eqz v2, :cond_1c

    array-length v3, v2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1c

    .line 951
    const/4 v0, 0x0

    aget-byte v0, v2, v0

    .line 956
    :goto_1b
    return v0

    .line 953
    :cond_1c
    const-string v2, "DIRECTIONS_MODE"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    goto :goto_1b
.end method

.method private I()Lcom/google/googlenav/ui/view/android/j;
    .registers 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/j;

    return-object v0
.end method

.method private J()V
    .registers 5

    .prologue
    .line 616
    new-instance v0, Lcom/google/googlenav/ui/view/p;

    const/16 v1, 0xfb

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget v3, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/p;-><init>(Ljava/lang/String;Lax/j;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    .line 619
    return-void
.end method

.method private K()V
    .registers 3

    .prologue
    .line 625
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(I)V

    .line 626
    return-void
.end method

.method private L()V
    .registers 3

    .prologue
    .line 632
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->a(I)V

    .line 633
    return-void
.end method

.method private M()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 746
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->B()Z

    move-result v1

    if-nez v1, :cond_16

    .line 747
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    .line 748
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    const/16 v2, 0x3c4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 756
    :goto_15
    return v0

    .line 750
    :cond_16
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v1

    if-nez v1, :cond_2b

    .line 751
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    .line 752
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    const/16 v2, 0x3c3

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_15

    .line 755
    :cond_2b
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->g()V

    .line 756
    const/4 v0, 0x1

    goto :goto_15
.end method

.method private static N()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 845
    invoke-static {}, Lcom/google/googlenav/K;->z()Z

    move-result v0

    if-eqz v0, :cond_59

    move v0, v1

    .line 848
    :goto_9
    invoke-static {}, Lcom/google/googlenav/K;->y()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 849
    add-int/lit8 v0, v0, 0x1

    .line 851
    :cond_11
    invoke-static {}, Lcom/google/googlenav/K;->A()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 852
    add-int/lit8 v0, v0, 0x1

    .line 854
    :cond_19
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v3

    if-eqz v3, :cond_21

    .line 855
    add-int/lit8 v0, v0, 0x1

    .line 860
    :cond_21
    new-array v0, v0, [I

    sput-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    .line 861
    invoke-static {}, Lcom/google/googlenav/K;->z()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 862
    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    aput v2, v0, v2

    move v2, v1

    .line 864
    :cond_30
    invoke-static {}, Lcom/google/googlenav/K;->y()Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 865
    sget-object v3, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    add-int/lit8 v0, v2, 0x1

    aput v1, v3, v2

    move v2, v0

    .line 867
    :cond_3d
    invoke-static {}, Lcom/google/googlenav/K;->A()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 868
    sget-object v1, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    add-int/lit8 v0, v2, 0x1

    const/4 v3, 0x2

    aput v3, v1, v2

    move v2, v0

    .line 870
    :cond_4b
    invoke-static {}, Lcom/google/googlenav/K;->B()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 871
    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    add-int/lit8 v1, v2, 0x1

    const/4 v1, 0x3

    aput v1, v0, v2

    .line 873
    :cond_58
    return-void

    :cond_59
    move v0, v2

    goto :goto_9
.end method

.method private O()V
    .registers 3

    .prologue
    .line 896
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v0}, LaH/m;->e()Z

    move-result v0

    if-nez v0, :cond_47

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->B()Z

    move-result v0

    if-nez v0, :cond_47

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->q()Z

    move-result v0

    if-nez v0, :cond_47

    .line 898
    :cond_26
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 899
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->n:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_48

    if-eqz v0, :cond_48

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    invoke-static {v1, v0}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 902
    :goto_42
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, v0}, Lax/j;->a(Lax/y;)V

    .line 904
    :cond_47
    return-void

    .line 899
    :cond_48
    invoke-static {}, Lax/y;->a()Lax/y;

    move-result-object v0

    goto :goto_42
.end method

.method private P()V
    .registers 3

    .prologue
    .line 910
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 919
    :cond_6
    :goto_6
    return-void

    .line 913
    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->h()Lcom/google/googlenav/E;

    move-result-object v0

    .line 914
    if-eqz v0, :cond_6

    .line 915
    invoke-interface {v0}, Lcom/google/googlenav/E;->d()I

    move-result v1

    if-nez v1, :cond_6

    .line 916
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-static {v0}, Lax/y;->a(Lcom/google/googlenav/ai;)Lax/y;

    move-result-object v0

    invoke-virtual {v1, v0}, Lax/j;->b(Lax/y;)V

    goto :goto_6
.end method

.method private static Q()I
    .registers 2

    .prologue
    .line 935
    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    if-nez v0, :cond_7

    .line 936
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->N()V

    .line 938
    :cond_7
    invoke-static {}, Lcom/google/googlenav/common/Config;->w()Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    sget-object v0, Lcom/google/googlenav/ui/wizard/ca;->a:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    goto :goto_e
.end method

.method private R()V
    .registers 2

    .prologue
    .line 960
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/ca;->b(I)V

    .line 961
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/A;)Z
    .registers 4
    .parameter

    .prologue
    .line 426
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_1b

    .line 429
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/ui/view/android/ah;

    if-eqz v0, :cond_1b

    .line 430
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/ah;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/ah;->show()V

    .line 431
    const/4 v0, 0x1

    .line 434
    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public static b(I)V
    .registers 6
    .parameter

    .prologue
    .line 970
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    .line 971
    const-string v1, "DIRECTIONS_MODE"

    const/4 v2, 0x1

    new-array v2, v2, [B

    const/4 v3, 0x0

    int-to-byte v4, p0

    aput-byte v4, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 972
    return-void
.end method

.method private c(I)V
    .registers 4
    .parameter

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_1f

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/j;->o()Lcom/google/googlenav/ui/view/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    if-ne v0, v1, :cond_1f

    .line 363
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/j;->l()V

    .line 374
    :goto_17
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/android/j;->a(I)V

    .line 375
    return-void

    .line 368
    :cond_1f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_28

    .line 369
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 371
    :cond_28
    new-instance v0, Lcom/google/googlenav/ui/view/android/j;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/j;-><init>(Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/view/p;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 372
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    goto :goto_17
.end method


# virtual methods
.method protected A()Ljava/lang/Boolean;
    .registers 2

    .prologue
    .line 810
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->j()Lcom/google/googlenav/ai;

    move-result-object v0

    .line 811
    if-nez v0, :cond_c

    .line 812
    const/4 v0, 0x0

    .line 814
    :goto_b
    return-object v0

    :cond_c
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_b
.end method

.method protected B()Z
    .registers 2

    .prologue
    .line 876
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected C()Lax/y;
    .registers 2

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v0}, Lax/j;->aq()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method protected D()Z
    .registers 2

    .prologue
    .line 884
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected E()Lax/y;
    .registers 2

    .prologue
    .line 888
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v0}, Lax/j;->as()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method protected F()V
    .registers 4

    .prologue
    .line 926
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v0}, Lax/j;->aq()Lax/y;

    move-result-object v0

    .line 927
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v2}, Lax/j;->as()Lax/y;

    move-result-object v2

    invoke-virtual {v1, v2}, Lax/j;->a(Lax/y;)V

    .line 928
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, v0}, Lax/j;->b(Lax/y;)V

    .line 931
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    .line 932
    return-void
.end method

.method public G()V
    .registers 2

    .prologue
    .line 942
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    .line 943
    return-void
.end method

.method public S_()Z
    .registers 2

    .prologue
    .line 1010
    const/4 v0, 0x0

    return v0
.end method

.method public T_()V
    .registers 1

    .prologue
    .line 1022
    return-void
.end method

.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 476
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_13

    .line 477
    const-string v0, "b"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    .line 478
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->h()V

    .line 479
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    .line 481
    :goto_12
    return v0

    :cond_13
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_12
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 486
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    return v0
.end method

.method protected a(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x3

    .line 653
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    .line 654
    iput p1, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    .line 656
    packed-switch p1, :pswitch_data_4c

    .line 688
    :goto_a
    :pswitch_a
    return-void

    .line 658
    :pswitch_b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    goto :goto_a

    .line 661
    :pswitch_14
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    .line 662
    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_a

    .line 665
    :pswitch_1a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    .line 666
    const-string v0, "s"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 667
    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 668
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    .line 669
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_a

    .line 672
    :pswitch_2d
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    .line 673
    const-string v0, "e"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 674
    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 675
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    .line 676
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_a

    .line 679
    :pswitch_40
    const-string v0, "m"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    .line 680
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    .line 681
    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_a

    .line 656
    nop

    :pswitch_data_4c
    .packed-switch -0x1
        :pswitch_b
        :pswitch_14
        :pswitch_1a
        :pswitch_2d
        :pswitch_40
        :pswitch_a
    .end packed-switch
.end method

.method public a(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->b(Lax/b;)I

    move-result v0

    .line 205
    invoke-virtual {p1}, Lax/b;->ap()Lax/j;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/j;I)V

    .line 206
    return-void
.end method

.method public a(Lax/b;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    .line 188
    if-eqz p2, :cond_11

    .line 189
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    .line 190
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->b(I)V

    .line 195
    :goto_10
    return-void

    .line 192
    :cond_11
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    .line 193
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->I()Lcom/google/googlenav/ui/view/android/j;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/j;->b(I)V

    goto :goto_10
.end method

.method public a(Lax/j;)V
    .registers 3
    .parameter

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    .line 244
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    .line 245
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->O()V

    .line 246
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    .line 247
    return-void
.end method

.method public a(Lax/j;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 256
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_8

    .line 257
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    .line 259
    :cond_8
    iput p2, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    .line 260
    invoke-static {p2}, Lcom/google/googlenav/ui/wizard/ca;->b(I)V

    .line 261
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/j;)V

    .line 262
    return-void
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 978
    .line 979
    if-eqz p3, :cond_e

    .line 980
    const/16 v1, 0xb

    invoke-static {p3, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    .line 982
    if-nez v1, :cond_36

    .line 984
    :cond_e
    :goto_e
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/ca;->q:Z

    if-eqz v1, :cond_4a

    .line 985
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, p1}, Lax/j;->a(Lax/y;)V

    .line 986
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "ss"

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    .line 990
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 991
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->J()V

    .line 992
    invoke-direct {p0, v3}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    .line 1005
    :cond_32
    :goto_32
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    .line 1006
    return-void

    .line 982
    :cond_36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    .line 995
    :cond_4a
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-virtual {v1, p1}, Lax/j;->b(Lax/y;)V

    .line 996
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "es"

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 1001
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->J()V

    .line 1002
    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    goto :goto_32
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    .line 299
    new-instance v0, Lax/j;

    invoke-direct {v0}, Lax/j;-><init>()V

    .line 300
    invoke-virtual {v0, p1}, Lax/j;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 302
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->b(Lax/j;)V

    .line 303
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 819
    const/4 v0, 0x4

    const-string v1, "q"

    invoke-static {v0, v1, p1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 821
    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 825
    const/4 v2, 0x4

    const-string v3, "q"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v5, 0x1

    if-nez p2, :cond_1d

    move-object v0, v1

    :goto_e
    aput-object v0, v4, v5

    const/4 v0, 0x2

    if-nez p3, :cond_31

    :goto_13
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 834
    return-void

    .line 825
    :cond_1d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_e

    :cond_31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "is_ad="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_13
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 491
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    iget-object v2, v2, Lcom/google/googlenav/ui/view/p;->a:Lax/j;

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    .line 492
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    iget v2, v2, Lcom/google/googlenav/ui/view/p;->b:I

    iput v2, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    .line 493
    sparse-switch p1, :sswitch_data_66

    move v0, v1

    .line 550
    :goto_12
    return v0

    .line 495
    :sswitch_13
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_12

    .line 498
    :sswitch_17
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_12

    .line 501
    :sswitch_1c
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 502
    const-string v1, "Directions"

    invoke-static {v1}, Laj/a;->b(Ljava/lang/String;)V

    .line 504
    :cond_27
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    .line 508
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    const/16 v2, 0x1d

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(I)V

    .line 510
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 512
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->y()V

    goto :goto_12

    .line 515
    :sswitch_47
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v1

    if-eqz v1, :cond_52

    .line 516
    const-string v1, "Navigation"

    invoke-static {v1}, Laj/a;->b(Ljava/lang/String;)V

    .line 518
    :cond_52
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->z()V

    goto :goto_12

    .line 521
    :sswitch_56
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_12

    .line 524
    :sswitch_5b
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    goto :goto_12

    .line 527
    :sswitch_5f
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->F()V

    goto :goto_12

    :sswitch_63
    move v0, v1

    .line 547
    goto :goto_12

    .line 493
    nop

    :sswitch_data_66
    .sparse-switch
        0xd4 -> :sswitch_13
        0xd5 -> :sswitch_17
        0xd6 -> :sswitch_1c
        0xde -> :sswitch_56
        0xdf -> :sswitch_5b
        0xe4 -> :sswitch_5f
        0xec -> :sswitch_47
        0xb56 -> :sswitch_63
    .end sparse-switch
.end method

.method protected b(Lax/b;)I
    .registers 3
    .parameter

    .prologue
    .line 210
    instance-of v0, p1, Lax/w;

    if-eqz v0, :cond_6

    .line 211
    const/4 v0, 0x1

    .line 219
    :goto_5
    return v0

    .line 212
    :cond_6
    instance-of v0, p1, Lax/x;

    if-eqz v0, :cond_c

    .line 213
    const/4 v0, 0x2

    goto :goto_5

    .line 214
    :cond_c
    instance-of v0, p1, Lax/i;

    if-eqz v0, :cond_12

    .line 215
    const/4 v0, 0x3

    goto :goto_5

    .line 217
    :cond_12
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected b(Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 837
    const/4 v0, 0x4

    const-string v1, "q"

    invoke-static {v0, v1, p1}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .registers 5

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 381
    iput v3, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    .line 382
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->m:Z

    .line 385
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    packed-switch v0, :pswitch_data_3c

    .line 396
    const/4 v0, -0x1

    .line 400
    :goto_10
    iget v2, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-eqz v2, :cond_34

    iget v2, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-eq v2, v3, :cond_34

    .line 401
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    .line 407
    :goto_1b
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-nez v0, :cond_28

    .line 409
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    const-string v1, "route"

    invoke-virtual {v0, v1}, Law/h;->e(Ljava/lang/String;)V

    .line 411
    :cond_28
    const-string v0, "a"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    .line 412
    return-void

    :pswitch_2e
    move v0, v1

    .line 388
    goto :goto_10

    .line 390
    :pswitch_30
    const/4 v0, 0x1

    .line 391
    goto :goto_10

    .line 393
    :pswitch_32
    const/4 v0, 0x2

    .line 394
    goto :goto_10

    .line 403
    :cond_34
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->J()V

    .line 404
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->c(I)V

    goto :goto_1b

    .line 385
    nop

    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_30
        :pswitch_32
    .end packed-switch
.end method

.method public b(Lax/j;)V
    .registers 3
    .parameter

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->i()V

    .line 273
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    .line 274
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->O()V

    .line 279
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->f()Z

    move-result v0

    .line 280
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->P()V

    .line 281
    if-eqz v0, :cond_2e

    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 283
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->H()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    .line 285
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->y()V

    .line 289
    :goto_2d
    return-void

    .line 287
    :cond_2e
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    goto :goto_2d
.end method

.method public b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 11
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 691
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    if-ne v0, v5, :cond_87

    move v4, v5

    .line 696
    :goto_7
    if-eqz v4, :cond_8a

    .line 697
    const/16 v0, 0xc4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 703
    :goto_f
    invoke-static {}, Lcom/google/googlenav/ui/wizard/dg;->e()I

    move-result v1

    .line 704
    if-eqz v4, :cond_25

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_25

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-nez v2, :cond_37

    :cond_25
    if-nez v4, :cond_39

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_39

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-eqz v2, :cond_39

    .line 706
    :cond_37
    xor-int/lit8 v1, v1, 0x4

    .line 712
    :cond_39
    xor-int/lit8 v7, v1, 0x8

    .line 716
    if-eqz v4, :cond_92

    const/16 v1, 0x587

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 720
    :goto_44
    if-eqz v4, :cond_9a

    const/4 v1, 0x4

    move v2, v1

    .line 722
    :goto_48
    if-eqz v4, :cond_9d

    const/16 v1, 0x5e2

    :goto_4c
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 727
    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v8, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v8}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-virtual {v8, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->b(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/ui/wizard/dF;->d(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 739
    return-void

    :cond_87
    move v4, v6

    .line 691
    goto/16 :goto_7

    .line 699
    :cond_8a
    const/16 v0, 0xc2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_f

    .line 716
    :cond_92
    const/16 v1, 0x119

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    goto :goto_44

    .line 720
    :cond_9a
    const/4 v1, 0x5

    move v2, v1

    goto :goto_48

    .line 722
    :cond_9d
    const/16 v1, 0x5e1

    goto :goto_4c
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 416
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 417
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->R()V

    .line 418
    return-void
.end method

.method public c(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 232
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    .line 234
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->y()V

    .line 235
    return-void
.end method

.method public d(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 326
    invoke-virtual {p1}, Lax/b;->y()I

    move-result v0

    packed-switch v0, :pswitch_data_2a

    .line 336
    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v0

    if-nez v0, :cond_25

    .line 337
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    .line 343
    :cond_10
    :goto_10
    return-void

    .line 328
    :pswitch_11
    invoke-virtual {p1}, Lax/b;->aD()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 329
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    goto :goto_10

    .line 330
    :cond_1b
    invoke-virtual {p1}, Lax/b;->aE()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 331
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->L()V

    goto :goto_10

    .line 339
    :cond_25
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->K()V

    goto :goto_10

    .line 326
    nop

    :pswitch_data_2a
    .packed-switch 0x3
        :pswitch_11
    .end packed-switch
.end method

.method public e()Lax/b;
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 565
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->f()Z

    move-result v0

    if-nez v0, :cond_f

    .line 566
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DirectionsWizard.getRequest() called too early!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_f
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    if-ne v0, v1, :cond_30

    .line 570
    new-instance v0, Lax/w;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ca;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lax/w;-><init>(Lax/k;Lcom/google/googlenav/ui/m;)V

    .line 578
    :goto_20
    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/ca;->p:Z

    if-nez v2, :cond_52

    :goto_24
    invoke-virtual {v0, v1}, Lax/b;->a(Z)V

    .line 579
    invoke-virtual {v0}, Lax/b;->aM()V

    .line 580
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/f;->a(Lax/b;Lcom/google/googlenav/ui/view/q;)V

    .line 581
    return-object v0

    .line 571
    :cond_30
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3d

    .line 572
    new-instance v0, Lax/x;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {v0, v2}, Lax/x;-><init>(Lax/k;)V

    goto :goto_20

    .line 573
    :cond_3d
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4a

    .line 574
    new-instance v0, Lax/i;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {v0, v2}, Lax/i;-><init>(Lax/k;)V

    goto :goto_20

    .line 576
    :cond_4a
    new-instance v0, Lax/s;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    invoke-direct {v0, v2}, Lax/s;-><init>(Lax/k;)V

    goto :goto_20

    .line 578
    :cond_52
    const/4 v1, 0x0

    goto :goto_24
.end method

.method public f()Z
    .registers 2

    .prologue
    .line 586
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->B()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->D()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public g()V
    .registers 3

    .prologue
    .line 602
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 603
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->l:Lax/z;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->C()Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/z;->a(Lax/y;)Z

    .line 605
    :cond_19
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_32

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 606
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->l:Lax/z;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/z;->a(Lax/y;)Z

    .line 608
    :cond_32
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, -0x1

    .line 439
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    packed-switch v0, :pswitch_data_5c

    .line 462
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    .line 463
    iput v2, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    .line 469
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget v1, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    .line 472
    :goto_14
    return-void

    .line 441
    :pswitch_15
    const-string v0, "b"

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->g()V

    .line 443
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    .line 444
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 445
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 448
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v1

    invoke-static {v1, v0}, Lbf/O;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/A;)Z

    move-result v1

    if-nez v1, :cond_48

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(Lcom/google/googlenav/ui/wizard/A;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 450
    :cond_48
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 453
    :cond_51
    iput v2, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_14

    .line 458
    :pswitch_54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/ca;->a(I)V

    .line 459
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    goto :goto_14

    .line 439
    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_15
        :pswitch_54
        :pswitch_54
        :pswitch_54
    .end packed-switch
.end method

.method protected i()V
    .registers 3

    .prologue
    const/4 v1, -0x1

    .line 639
    new-instance v0, Lax/j;

    invoke-direct {v0}, Lax/j;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->j:Lax/j;

    .line 640
    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    .line 641
    iput v1, p0, Lcom/google/googlenav/ui/wizard/ca;->i:I

    .line 642
    invoke-static {}, Lcom/google/googlenav/ui/wizard/ca;->N()V

    .line 643
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->G()V

    .line 644
    return-void
.end method

.method public p()Z
    .registers 3

    .prologue
    .line 422
    iget v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_b
    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method protected y()V
    .registers 4

    .prologue
    .line 763
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->M()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 768
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Y()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 769
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    .line 776
    :cond_1f
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->o()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 777
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    .line 779
    :cond_32
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->k()Ljava/lang/String;

    move-result-object v0

    .line 780
    const-string v1, "d"

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->A()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 782
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->I()V

    .line 783
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->c:I

    .line 785
    :cond_4b
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    .line 786
    return-void
.end method

.method protected z()V
    .registers 6

    .prologue
    .line 792
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/ca;->M()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 794
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->k()Ljava/lang/String;

    move-result-object v0

    .line 795
    const-string v1, "n"

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->A()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/wizard/ca;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 797
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ca;->k:Lcom/google/googlenav/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ca;->E()Lax/y;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/wizard/ca;->b:I

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ca;->o:Lcom/google/googlenav/ui/view/p;

    invoke-static {v3}, Lcom/google/googlenav/ui/f;->a(Lcom/google/googlenav/ui/view/q;)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const-string v4, "w"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/googlenav/J;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    .line 801
    :cond_2a
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/ca;->g:I

    .line 802
    return-void
.end method
