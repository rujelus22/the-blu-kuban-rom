.class public Lcom/google/googlenav/ui/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements LaM/h;
.implements Lcom/google/googlenav/J;
.implements Lcom/google/googlenav/common/h;
.implements Lcom/google/googlenav/common/util/n;
.implements Lcom/google/googlenav/ui/android/D;


# static fields
.field private static aE:Lbm/i;

.field private static aF:Lbm/i;

.field private static aj:Ljava/lang/String;


# instance fields
.field private final A:Lcom/google/googlenav/friend/j;

.field private final B:LaN/p;

.field private final C:LaH/m;

.field private final D:LaN/V;

.field private final E:LaN/k;

.field private final F:Lcom/google/googlenav/j;

.field private G:I

.field private H:J

.field private I:I

.field private J:J

.field private K:J

.field private L:J

.field private M:J

.field private N:Z

.field private final O:Lcom/google/googlenav/ui/X;

.field private P:Z

.field private final Q:Lcom/google/googlenav/ui/W;

.field private R:LaN/B;

.field private final S:Lcom/google/googlenav/ui/p;

.field private T:I

.field private U:Z

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:J

.field private Z:J

.field a:Z

.field private aA:Z

.field private aB:LaB/o;

.field private final aC:Lcom/google/googlenav/ui/ac;

.field private final aD:Lcom/google/googlenav/ui/S;

.field private final aG:Lbm/i;

.field private final aH:Lbm/i;

.field private final aI:Lcom/google/googlenav/friend/a;

.field private aJ:Lcom/google/googlenav/ui/android/r;

.field private final aa:Lcom/google/googlenav/A;

.field private final ab:Las/c;

.field private final ac:LaR/b;

.field private final ad:Lcom/google/googlenav/ui/wizard/jv;

.field private final ae:Lbf/am;

.field private final af:Lcom/google/googlenav/ui/wizard/z;

.field private final ag:Lav/a;

.field private final ah:Lcom/google/googlenav/L;

.field private final ai:Lcom/google/googlenav/mylocationnotifier/k;

.field private ak:LaN/B;

.field private final al:Landroid/graphics/Point;

.field private am:Z

.field private an:Lcom/google/googlenav/ui/R;

.field private final ao:Lat/c;

.field private ap:Z

.field private aq:Z

.field private ar:Ljava/lang/String;

.field private as:Z

.field private at:Lcom/google/googlenav/ui/r;

.field private au:Lcom/google/googlenav/ui/b;

.field private final av:LaN/u;

.field private aw:Z

.field private ax:Ljava/lang/Boolean;

.field private final ay:LaN/v;

.field private final az:Lcom/google/googlenav/ui/aC;

.field protected b:Lcom/google/googlenav/ui/ak;

.field volatile c:Z

.field protected final d:Lcom/google/googlenav/offers/a;

.field protected volatile e:Z

.field public final f:Lbm/i;

.field private final g:Lcom/google/googlenav/ui/android/L;

.field private final h:Lcom/google/googlenav/android/aa;

.field private i:Z

.field private j:Z

.field private k:Las/d;

.field private l:I

.field private m:Z

.field private n:I

.field private o:I

.field private p:Lat/a;

.field private q:Z

.field private final r:Lcom/google/googlenav/aA;

.field private final s:Lcom/google/googlenav/common/io/j;

.field private t:Lcom/google/googlenav/ui/as;

.field private final u:Lcom/google/googlenav/ui/bE;

.field private final v:Lcom/google/googlenav/ui/bF;

.field private final w:Lcom/google/googlenav/ui/aD;

.field private final x:Lcom/google/googlenav/friend/J;

.field private final y:Lcom/google/googlenav/friend/p;

.field private final z:Lcom/google/googlenav/friend/ag;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/16 v3, 0x16

    .line 624
    const-string v0, ""

    sput-object v0, Lcom/google/googlenav/ui/s;->aj:Ljava/lang/String;

    .line 765
    new-instance v0, Lbm/i;

    const-string v1, "latitude join"

    const-string v2, "lj"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/s;->aE:Lbm/i;

    .line 770
    new-instance v0, Lbm/i;

    const-string v1, "latitude show"

    const-string v2, "ls"

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/s;->aF:Lbm/i;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/aC;LaN/p;LaN/u;Lcom/google/googlenav/ui/bF;Lcom/google/googlenav/ui/p;LaH/m;Lbf/at;)V
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 815
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 248
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->i:Z

    .line 251
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->j:Z

    .line 352
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    .line 355
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->q:Z

    .line 428
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->I:I

    .line 434
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->K:J

    .line 445
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->L:J

    .line 454
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->M:J

    .line 458
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->N:Z

    .line 494
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->T:I

    .line 530
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->U:Z

    .line 535
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->V:Z

    .line 541
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->W:Z

    .line 544
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->X:Z

    .line 552
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->Y:J

    .line 555
    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/googlenav/ui/s;->Z:J

    .line 591
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->c:Z

    .line 630
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ak:LaN/B;

    .line 637
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->al:Landroid/graphics/Point;

    .line 650
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->am:Z

    .line 666
    new-instance v3, Lat/c;

    invoke-direct {v3}, Lat/c;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    .line 672
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->aq:Z

    .line 683
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->as:Z

    .line 694
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->e:Z

    .line 713
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->aw:Z

    .line 722
    new-instance v3, Lcom/google/googlenav/ui/t;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/t;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ay:LaN/v;

    .line 779
    new-instance v3, Lbm/i;

    const-string v4, "searchprenetworkrequest"

    invoke-direct {v3, v4}, Lbm/i;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    .line 782
    new-instance v3, Lbm/i;

    const-string v4, "searchpostnetworkrequest"

    invoke-direct {v3, v4}, Lbm/i;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    .line 785
    new-instance v3, Lbm/i;

    const-string v4, "search-bubble"

    invoke-direct {v3, v4}, Lbm/i;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->f:Lbm/i;

    .line 816
    const-string v3, "GmmController.GmmController"

    invoke-static {v3}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 817
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    .line 818
    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->a()Lcom/google/googlenav/aA;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    .line 819
    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->e()Lcom/google/googlenav/android/aa;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    .line 820
    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->d()Lcom/google/googlenav/ui/android/L;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    .line 821
    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->b()Lcom/google/googlenav/ui/aD;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    .line 822
    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->i()Las/c;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    .line 828
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->O()V

    .line 830
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->B:LaN/p;

    .line 831
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    .line 832
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    .line 833
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->C:LaH/m;

    .line 834
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    .line 837
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/googlenav/ui/s;->av:LaN/u;

    .line 839
    new-instance v3, Lcom/google/googlenav/ui/bE;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/bE;-><init>(LaN/u;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    .line 840
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ay:LaN/v;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LaN/u;->a(LaN/v;)V

    .line 843
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, LaN/u;->c(II)V

    .line 845
    new-instance v3, Lcom/google/googlenav/ui/wizard/z;

    invoke-direct {v3}, Lcom/google/googlenav/ui/wizard/z;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    .line 850
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\u0001"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-char v4, Lcom/google/googlenav/ui/bi;->z:C

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lam/p;->a(Ljava/lang/String;)V

    .line 853
    new-instance v3, Lcom/google/googlenav/ui/W;

    invoke-direct {v3}, Lcom/google/googlenav/ui/W;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->Q:Lcom/google/googlenav/ui/W;

    .line 855
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/L;->b()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->n:I

    .line 856
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/android/L;->a()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/googlenav/ui/s;->o:I

    .line 857
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v3, v4, v5}, Lcom/google/googlenav/ui/bi;->b(II)V

    .line 858
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/s;->a(II)V

    .line 862
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    invoke-static {v3, v4}, LaN/Y;->b(II)V

    .line 869
    invoke-virtual/range {p2 .. p2}, LaN/p;->c()LaN/Y;

    move-result-object v3

    invoke-static {v3}, LaN/Y;->c(LaN/Y;)LaN/Y;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, LaN/u;->a(LaN/Y;)V

    .line 874
    new-instance v3, Lcom/google/googlenav/ui/ak;

    sget-object v8, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    move-object/from16 v4, p6

    move-object/from16 v5, p0

    move-object/from16 v6, p3

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/google/googlenav/ui/ak;-><init>(LaH/m;Lcom/google/googlenav/ui/s;LaN/u;LaN/p;Lcom/google/googlenav/android/A;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    .line 876
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    .line 878
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, LaN/p;->d(II)V

    .line 879
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/googlenav/ui/s;->n:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/googlenav/ui/s;->o:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, LaN/u;->c(II)V

    .line 880
    invoke-static/range {p0 .. p0}, Lcom/google/googlenav/common/k;->b(Lcom/google/googlenav/common/h;)V

    .line 882
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    .line 886
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v11

    .line 887
    new-instance v3, Lcom/google/googlenav/ui/S;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/ui/S;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aD:Lcom/google/googlenav/ui/S;

    .line 888
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->aD:Lcom/google/googlenav/ui/S;

    invoke-virtual {v11, v3}, Law/h;->a(Law/q;)V

    .line 890
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    .line 892
    new-instance v3, LaN/V;

    const-wide/32 v4, 0x1d4c0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-direct {v3, v4, v5, v6}, LaN/V;-><init>(JLas/c;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    .line 893
    invoke-virtual/range {p1 .. p1}, Lcom/google/googlenav/ui/aC;->c()Lcom/google/googlenav/ui/X;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    .line 894
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-interface {v3, v4}, Lcom/google/googlenav/ui/X;->a(LaN/V;)V

    .line 898
    new-instance v3, LaN/k;

    invoke-direct {v3}, LaN/k;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->E:LaN/k;

    .line 900
    new-instance v3, Lcom/google/googlenav/j;

    invoke-direct {v3}, Lcom/google/googlenav/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->F:Lcom/google/googlenav/j;

    .line 901
    sget-object v3, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v4, Lcom/google/googlenav/ui/F;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/googlenav/ui/F;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v3, v4}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 908
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LaN/p;->a(LaN/Q;)V

    .line 910
    new-instance v3, Lcom/google/googlenav/ui/K;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/K;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LaN/p;->a(LaN/q;)V

    .line 919
    new-instance v3, Lcom/google/googlenav/A;

    move-object/from16 v0, p6

    move-object/from16 v1, p3

    move-object/from16 v2, p0

    invoke-direct {v3, v0, v1, v2}, Lcom/google/googlenav/A;-><init>(LaH/m;LaN/u;Lcom/google/googlenav/J;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    .line 921
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->i()Z

    move-result v3

    if-eqz v3, :cond_480

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->j()Z

    move-result v3

    if-eqz v3, :cond_480

    .line 923
    new-instance v3, Lcom/google/googlenav/friend/j;

    invoke-direct {v3}, Lcom/google/googlenav/friend/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    .line 929
    :goto_2a0
    new-instance v3, LaA/q;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LaA/q;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-static {v3, v4}, LaA/h;->a(LaA/e;Lcom/google/googlenav/android/aa;)V

    .line 931
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p6

    move-object/from16 v9, p0

    invoke-virtual/range {v3 .. v10}, Lcom/google/googlenav/ui/aC;->a(LaN/p;LaN/u;LaH/m;ZLcom/google/googlenav/ui/wizard/z;Lcom/google/googlenav/J;Lcom/google/googlenav/friend/j;)Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    .line 934
    new-instance v3, Lcom/google/googlenav/ui/ac;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/googlenav/ui/ac;-><init>(Lcom/google/googlenav/ui/ak;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/aA;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aC:Lcom/google/googlenav/ui/ac;

    .line 936
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->i()Z

    move-result v3

    if-eqz v3, :cond_487

    .line 937
    new-instance v3, Lcom/google/googlenav/friend/J;

    invoke-direct {v3}, Lcom/google/googlenav/friend/J;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    .line 938
    new-instance v3, Lcom/google/googlenav/friend/p;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    move-object/from16 v0, p0

    invoke-direct {v3, v4, v0}, Lcom/google/googlenav/friend/p;-><init>(Las/c;Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    .line 939
    new-instance v3, Lcom/google/googlenav/friend/ag;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-direct {v3, v4}, Lcom/google/googlenav/friend/ag;-><init>(Lcom/google/googlenav/android/aa;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    .line 946
    :goto_310
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->aA()Z

    move-result v3

    if-eqz v3, :cond_329

    .line 947
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v3

    new-instance v4, Lcom/google/googlenav/ui/V;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/google/googlenav/ui/V;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V

    invoke-virtual {v3, v4}, LaM/f;->a(LaM/h;)V

    .line 950
    :cond_329
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v3}, LaR/l;->a(Lcom/google/googlenav/ui/wizard/jv;)LaR/l;

    .line 952
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v3

    invoke-virtual {v3}, LaR/l;->f()LaR/n;

    move-result-object v3

    .line 954
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v4

    invoke-virtual {v4}, LaR/l;->g()LaR/n;

    move-result-object v4

    .line 956
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v5

    invoke-virtual {v5}, LaR/l;->m()LaR/I;

    move-result-object v5

    .line 957
    new-instance v6, LaR/b;

    invoke-direct {v6, v11, v5}, LaR/b;-><init>(Law/h;LaR/I;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    .line 959
    invoke-interface {v3}, LaR/n;->e()LaR/u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-interface {v3, v5}, LaR/u;->a(LaR/C;)V

    .line 961
    invoke-interface {v4}, LaR/n;->e()LaR/u;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-interface {v3, v4}, LaR/u;->a(LaR/C;)V

    .line 963
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-virtual {v3}, LaR/b;->b()V

    .line 965
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/googlenav/ui/s;->E:LaN/k;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/aC;->f()Lcom/google/googlenav/layer/r;

    move-result-object v15

    sget-object v16, Lcom/google/googlenav/offers/j;->a:Lcom/google/googlenav/offers/j;

    move-object/from16 v3, p7

    move-object/from16 v4, p0

    invoke-virtual/range {v3 .. v16}, Lbf/at;->a(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaH/m;Lcom/google/googlenav/friend/J;Lcom/google/googlenav/friend/p;Lcom/google/googlenav/friend/ag;Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;LaN/k;Lcom/google/googlenav/layer/r;Lcom/google/googlenav/offers/j;)Lbf/am;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    .line 970
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v3}, Lbf/am;->b()V

    .line 973
    new-instance v3, Lcom/google/googlenav/offers/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/google/googlenav/offers/a;-><init>(Lcom/google/googlenav/ui/s;LaN/u;Lcom/google/googlenav/ui/wizard/jv;Lbf/am;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    .line 977
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->aA()Z

    move-result v3

    if-eqz v3, :cond_3e1

    .line 978
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-static {v4}, Lcom/google/googlenav/offers/k;->a(Lcom/google/googlenav/android/aa;)Lcom/google/googlenav/offers/k;

    move-result-object v4

    invoke-virtual {v3, v4}, LaM/f;->a(LaM/h;)V

    .line 982
    :cond_3e1
    new-instance v3, Lav/a;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lav/a;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ag:Lav/a;

    .line 984
    new-instance v3, Lcom/google/googlenav/L;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v3, v4}, Lcom/google/googlenav/L;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ah:Lcom/google/googlenav/L;

    .line 986
    new-instance v3, Lcom/google/googlenav/mylocationnotifier/k;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/googlenav/mylocationnotifier/k;-><init>(Lcom/google/googlenav/ui/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->ai:Lcom/google/googlenav/mylocationnotifier/k;

    .line 991
    new-instance v3, Lcom/google/googlenav/ui/L;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v0, v1}, Lcom/google/googlenav/ui/L;-><init>(Lcom/google/googlenav/ui/s;LaN/u;)V

    new-instance v4, Lcom/google/googlenav/ui/M;

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    invoke-direct {v4, v0, v1}, Lcom/google/googlenav/ui/M;-><init>(Lcom/google/googlenav/ui/s;LaH/m;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-static {v3, v4, v5}, Lbb/o;->a(Lbb/q;Lbb/p;Lcom/google/googlenav/android/aa;)V

    .line 1012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v3}, Lcom/google/googlenav/aA;->d()V

    .line 1014
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->ao()Z

    move-result v3

    if-eqz v3, :cond_43a

    .line 1015
    new-instance v3, Lcom/google/googlenav/ui/ar;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/ar;-><init>(LaN/p;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, LaN/p;->a(LaN/w;)V

    .line 1018
    :cond_43a
    invoke-direct/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->aD()V

    .line 1020
    new-instance v3, Lcom/google/googlenav/friend/a;

    invoke-direct {v3}, Lcom/google/googlenav/friend/a;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    .line 1021
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->aA()Z

    move-result v3

    if-eqz v3, :cond_45b

    .line 1022
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->s()V

    .line 1025
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->z()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/googlenav/ui/s;->c:Z

    .line 1028
    :cond_45b
    invoke-virtual/range {p0 .. p0}, Lcom/google/googlenav/ui/s;->x()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v3

    invoke-static {v3}, LaT/a;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V

    .line 1029
    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v3

    if-nez v3, :cond_46b

    .line 1032
    invoke-static {}, LaT/a;->i()V

    .line 1037
    :cond_46b
    invoke-static {}, Lcom/google/googlenav/aV;->a()Lcom/google/googlenav/aV;

    move-result-object v3

    .line 1038
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v4

    invoke-virtual {v4}, LaR/l;->b()LaR/aa;

    move-result-object v4

    invoke-interface {v4, v3}, LaR/aa;->a(LaR/ab;)V

    .line 1040
    const-string v3, "GmmController.GmmController"

    invoke-static {v3}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 1041
    return-void

    .line 925
    :cond_480
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    goto/16 :goto_2a0

    .line 941
    :cond_487
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    .line 942
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    .line 943
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    goto/16 :goto_310
.end method

.method private a(LaR/H;)Lcom/google/googlenav/ai;
    .registers 4
    .parameter

    .prologue
    .line 3553
    if-eqz p1, :cond_e

    invoke-virtual {p1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, LaR/H;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 3555
    :cond_e
    const/4 v0, 0x0

    .line 3557
    :goto_f
    return-object v0

    :cond_10
    invoke-virtual {p1}, LaR/H;->b()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ai;

    move-result-object v0

    goto :goto_f
.end method

.method static a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1125
    invoke-static {p0}, Lcom/google/googlenav/ui/p;->b(I)I

    move-result v0

    invoke-static {v0}, LaN/p;->a(I)I

    move-result v0

    .line 1127
    invoke-static {p1}, Lcom/google/googlenav/ui/p;->b(I)I

    move-result v1

    invoke-static {v1}, LaN/p;->a(I)I

    move-result v1

    .line 1130
    mul-int/2addr v0, v1

    invoke-static {v0}, LaN/P;->a(I)V

    .line 1131
    return-void
.end method

.method private static a(IILjava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3103
    const/4 v0, 0x4

    const-string v1, "ws"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "m="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3107
    return-void
.end method

.method private a(Lam/e;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 2066
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 2067
    invoke-virtual {v0}, Law/h;->t()I

    move-result v1

    .line 2068
    invoke-virtual {v0}, Law/h;->s()I

    move-result v2

    .line 2069
    invoke-virtual {v0}, Law/h;->r()I

    move-result v0

    .line 2071
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Key: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  Z: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v6}, LaN/u;->d()LaN/Y;

    move-result-object v6

    invoke-virtual {v6}, LaN/Y;->a()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  C: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/googlenav/ui/s;->I:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  S: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p2}, Lcom/google/googlenav/ui/s;->d(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mem: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/util/k;->c()I

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/common/util/k;->a(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/util/k;->b()I

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/common/util/k;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/googlenav/common/util/k;->d()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NW: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    shr-int/lit8 v1, v1, 0xa

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "Kbytes/s"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "  R/S: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v2, v2, 0xa

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "K "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    shr-int/lit8 v0, v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "K"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    .line 2082
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->j()[Ljava/lang/String;

    move-result-object v0

    .line 2083
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v1

    sget-object v2, Lcom/google/googlenav/ui/s;->aj:Ljava/lang/String;

    invoke-static {p1, v1, v3, v0, v2}, Lcom/google/googlenav/ui/aU;->a(Lam/e;I[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 2085
    return-void
.end method

.method static a(Lcom/google/googlenav/aZ;)V
    .registers 8
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1649
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    .line 1650
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v1

    invoke-virtual {v1}, LaN/H;->b()LaN/Y;

    move-result-object v1

    .line 1651
    new-instance v2, LaN/H;

    invoke-direct {v2, v0, v1, v4}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    .line 1652
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-static {v0, v2}, LaN/u;->a(LaN/H;LaN/H;)I

    move-result v0

    .line 1653
    if-gez v0, :cond_1f

    .line 1692
    :cond_1e
    :goto_1e
    return-void

    .line 1657
    :cond_1f
    invoke-static {v0}, LaN/u;->b(I)I

    move-result v0

    .line 1661
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->V()LaN/H;

    move-result-object v1

    if-eqz v1, :cond_73

    .line 1668
    invoke-virtual {p0}, Lcom/google/googlenav/aZ;->V()LaN/H;

    move-result-object v1

    invoke-static {v1, v2}, LaN/u;->a(LaN/H;LaN/H;)I

    move-result v1

    .line 1670
    if-ltz v1, :cond_1e

    .line 1674
    invoke-static {v1}, LaN/u;->b(I)I

    move-result v1

    .line 1675
    sub-int/2addr v1, v0

    .line 1677
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "s=c"

    aput-object v3, v2, v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ts="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1690
    :goto_6d
    const-string v1, "stat"

    invoke-static {v5, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1e

    .line 1684
    :cond_73
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "s=nc"

    aput-object v2, v1, v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v6

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6d
.end method

.method private a(Lcom/google/googlenav/cg;)V
    .registers 5
    .parameter

    .prologue
    .line 3022
    new-instance v0, Lcom/google/googlenav/cp;

    invoke-virtual {p1}, Lcom/google/googlenav/cg;->i()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/cp;-><init>(Lcom/google/googlenav/bZ;)V

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    .line 3024
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/r;)V
    .registers 4
    .parameter

    .prologue
    .line 2011
    if-nez p1, :cond_9

    .line 2017
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbf/am;->b(Lcom/google/googlenav/ui/r;)V

    .line 2057
    :goto_8
    return-void

    .line 2021
    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    if-eqz v0, :cond_12

    .line 2022
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/b;->a(Lcom/google/googlenav/ui/r;)V

    .line 2024
    :cond_12
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/ui/r;)V

    .line 2025
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    if-eqz v0, :cond_1e

    .line 2026
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/b;->b(Lcom/google/googlenav/ui/r;)V

    .line 2028
    :cond_1e
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->g()Z

    move-result v0

    if-nez v0, :cond_27

    .line 2029
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->h()V

    .line 2033
    :cond_27
    iget v0, p0, Lcom/google/googlenav/ui/s;->I:I

    if-nez v0, :cond_2f

    .line 2035
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    goto :goto_8

    .line 2041
    :cond_2f
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    .line 2042
    if-eqz v1, :cond_68

    .line 2043
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_49

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-nez v0, :cond_71

    :cond_49
    const/4 v0, 0x1

    .line 2045
    :goto_4a
    invoke-virtual {v1}, Lbf/i;->ab()Z

    move-result v1

    if-nez v1, :cond_5e

    if-eqz v0, :cond_68

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->aa()Z

    move-result v0

    if-eqz v0, :cond_68

    .line 2047
    :cond_5e
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->F()Z

    .line 2048
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lcom/google/googlenav/ui/r;)V

    .line 2054
    :cond_68
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lcom/google/googlenav/ui/r;)V

    .line 2056
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->b(Lcom/google/googlenav/ui/r;)V

    goto :goto_8

    .line 2043
    :cond_71
    const/4 v0, 0x0

    goto :goto_4a
.end method

.method private a(Lcom/google/googlenav/ui/r;Lam/e;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1942
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-nez v0, :cond_2b

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->G()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1944
    invoke-static {}, Lcom/google/googlenav/ui/aL;->a()Lcom/google/googlenav/ui/aL;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->a()Lam/e;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->o()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/aL;->a(Lam/e;Z)V

    .line 1948
    :cond_2b
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ui/r;)V

    .line 1951
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aD;->a()V

    .line 1954
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->G()I

    move-result v0

    .line 1955
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_42

    .line 1956
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/bE;->a(Lcom/google/googlenav/ui/r;)V

    .line 1958
    :cond_42
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;)V
    .registers 1
    .parameter

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ai;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/ai;)V

    return-void
.end method

.method private a(Lat/a;I)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3961
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v2

    .line 3962
    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 3991
    :cond_c
    :goto_c
    return v0

    .line 3967
    :cond_d
    if-eq v2, v1, :cond_18

    const/4 v3, 0x2

    if-eq v2, v3, :cond_18

    const/4 v3, 0x3

    if-eq v2, v3, :cond_18

    const/4 v3, 0x4

    if-ne v2, v3, :cond_c

    .line 3973
    :cond_18
    packed-switch v2, :pswitch_data_42

    move p2, v0

    .line 3988
    :goto_1c
    :pswitch_1c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->R()V

    .line 3989
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->c()LaN/B;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v4}, LaN/u;->d()LaN/Y;

    move-result-object v4

    invoke-virtual {v3, v0, p2, v4}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {v2, v0}, LaN/u;->c(LaN/B;)V

    move v0, v1

    .line 3991
    goto :goto_c

    .line 3975
    :pswitch_36
    neg-int p2, p2

    .line 3976
    goto :goto_1c

    :pswitch_38
    move v5, v0

    move v0, p2

    move p2, v5

    .line 3982
    goto :goto_1c

    .line 3984
    :pswitch_3c
    neg-int p2, p2

    move v5, v0

    move v0, p2

    move p2, v5

    goto :goto_1c

    .line 3973
    nop

    :pswitch_data_42
    .packed-switch 0x1
        :pswitch_36
        :pswitch_1c
        :pswitch_3c
        :pswitch_38
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/ui/s;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->U:Z

    return p1
.end method

.method private aD()V
    .registers 3

    .prologue
    .line 1439
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0, p0}, LaH/m;->a(LaH/A;)V

    .line 1440
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    if-eqz v0, :cond_10

    .line 1441
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    invoke-interface {v0, v1}, LaH/m;->a(LaH/A;)V

    .line 1445
    :cond_10
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/x;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/x;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1459
    return-void
.end method

.method private aE()V
    .registers 5

    .prologue
    .line 1463
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LastSessionTime"

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->a(Ljava/lang/String;J)V

    .line 1465
    return-void
.end method

.method private aF()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1481
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    const-string v1, "TRAFFIC_ON"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    .line 1482
    if-eqz v0, :cond_16

    array-length v1, v0

    if-ne v1, v2, :cond_16

    .line 1485
    aget-byte v0, v0, v3

    if-ne v0, v2, :cond_16

    .line 1486
    invoke-virtual {p0, v2, v3}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    .line 1489
    :cond_16
    return-void
.end method

.method private aG()Z
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1782
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v2

    .line 1783
    if-eqz v2, :cond_1f

    .line 1784
    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v3

    if-nez v3, :cond_16

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->ag_()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1798
    :cond_16
    :goto_16
    return v0

    .line 1788
    :cond_17
    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/C;->n()Z

    move-result v2

    if-eqz v2, :cond_16

    move v0, v1

    goto :goto_16

    .line 1791
    :cond_1f
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    .line 1792
    if-eqz v2, :cond_16

    .line 1794
    invoke-virtual {v2}, Lbf/i;->aZ()Z

    move-result v2

    if-eqz v2, :cond_16

    move v0, v1

    goto :goto_16
.end method

.method private aH()V
    .registers 3

    .prologue
    .line 1929
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->J:J

    .line 1930
    return-void
.end method

.method private aI()Z
    .registers 2

    .prologue
    .line 2282
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ag()Z

    move-result v0

    return v0
.end method

.method private aJ()V
    .registers 3

    .prologue
    .line 2545
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->M:J

    .line 2546
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->j()V

    .line 2547
    return-void
.end method

.method private aK()V
    .registers 3

    .prologue
    .line 2750
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->p()I

    move-result v0

    sparse-switch v0, :sswitch_data_32

    .line 2769
    :goto_9
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/am;->e(Lbf/i;)V

    .line 2770
    :goto_14
    return-void

    .line 2752
    :sswitch_15
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->r(Z)V

    goto :goto_14

    .line 2756
    :sswitch_1a
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->r(Z)V

    goto :goto_9

    .line 2760
    :sswitch_1f
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    goto :goto_9

    .line 2764
    :sswitch_2d
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_14

    .line 2750
    nop

    :sswitch_data_32
    .sparse-switch
        0x1 -> :sswitch_1a
        0x2 -> :sswitch_15
        0x8 -> :sswitch_1f
        0xa -> :sswitch_2d
    .end sparse-switch
.end method

.method private aL()V
    .registers 2

    .prologue
    .line 4511
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aN()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 4512
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->e()V

    .line 4514
    :cond_d
    return-void
.end method

.method private aM()V
    .registers 2

    .prologue
    .line 4520
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aN()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 4521
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->f()V

    .line 4523
    :cond_d
    return-void
.end method

.method private aN()Z
    .registers 2

    .prologue
    .line 4526
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    .line 4527
    if-eqz v0, :cond_12

    check-cast v0, Lbf/bK;

    invoke-virtual {v0}, Lbf/bK;->c()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private aO()V
    .registers 2

    .prologue
    .line 4683
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    if-eqz v0, :cond_9

    .line 4684
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    invoke-virtual {v0}, Las/d;->c()I

    .line 4686
    :cond_9
    return-void
.end method

.method private aP()V
    .registers 2

    .prologue
    .line 4720
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v0, :cond_9

    .line 4721
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->Q()V

    .line 4723
    :cond_9
    return-void
.end method

.method private aQ()V
    .registers 3

    .prologue
    .line 4982
    invoke-static {}, LaI/b;->c()I

    move-result v0

    .line 4983
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->b(I)V

    .line 4984
    return-void
.end method

.method private aR()V
    .registers 3

    .prologue
    .line 5094
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 5095
    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v1

    if-eqz v1, :cond_1c

    invoke-virtual {v0}, Lbf/i;->as()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 5097
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    .line 5099
    :cond_1c
    return-void
.end method

.method private aS()Z
    .registers 2

    .prologue
    .line 5432
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->as()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 5433
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 5434
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->S()V

    .line 5435
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->au()V

    .line 5436
    const/4 v0, 0x1

    .line 5438
    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private aT()V
    .registers 4

    .prologue
    .line 5590
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/util/l;->a()Ljava/util/List;

    move-result-object v0

    .line 5591
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 5592
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_c

    .line 5594
    :cond_1e
    return-void
.end method

.method private b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3633
    sget-object v0, Lcom/google/googlenav/layer/m;->a:Lcom/google/googlenav/layer/m;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3639
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v1

    .line 3640
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/googlenav/ai;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/googlenav/ai;

    .line 3641
    if-eqz p2, :cond_1c

    iget-object v2, p2, Lcom/google/googlenav/bf;->a:Ljava/lang/String;

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 3642
    :cond_1c
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2, v1}, LaN/u;->a(LaN/H;)I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3, v1}, LaN/u;->b(LaN/H;)I

    move-result v3

    invoke-static {v0, v1, v2, v3, p2}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;IILcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3649
    :goto_2c
    return-object v0

    :cond_2d
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-static {v0, p2, v1, p3}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;Lcom/google/googlenav/bf;LaN/u;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;

    move-result-object v0

    goto :goto_2c
.end method

.method static synthetic b(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/ui/bE;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    return-object v0
.end method

.method private b(Lam/e;)Lcom/google/googlenav/ui/r;
    .registers 9
    .parameter

    .prologue
    .line 1758
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1759
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v1

    .line 1760
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v2

    .line 1762
    if-eqz v0, :cond_3a

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->n()Z

    move-result v3

    if-eqz v3, :cond_3a

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->ag_()Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 1764
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->r()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->s()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->t()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->u()I

    move-result v6

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/google/googlenav/ui/r;->a(Lam/e;IIIIII)Lcom/google/googlenav/ui/r;

    move-result-object v0

    .line 1770
    :goto_39
    return-object v0

    :cond_3a
    invoke-static {p1, v1, v2}, Lcom/google/googlenav/ui/r;->a(Lam/e;II)Lcom/google/googlenav/ui/r;

    move-result-object v0

    goto :goto_39
.end method

.method private b(Lat/b;)V
    .registers 7
    .parameter

    .prologue
    .line 2635
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->G()I

    move-result v0

    .line 2638
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_11

    .line 2639
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/bE;->a(Lat/b;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 2707
    :cond_10
    :goto_10
    return-void

    .line 2644
    :cond_11
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lat/b;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/s;->a(ILat/a;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2647
    :cond_26
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lat/b;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2651
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_d7

    .line 2652
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lat/b;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 2654
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->b(II)LaN/B;

    move-result-object v0

    .line 2656
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(LaN/B;)V

    goto :goto_10

    .line 2663
    :cond_4c
    invoke-virtual {p1}, Lat/b;->d()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 2667
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaN/u;->e(LaN/B;LaN/Y;)V

    .line 2669
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0}, Lat/c;->a()V

    .line 2670
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0, p1}, Lat/c;->a(Lat/b;)V

    .line 2671
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->V:Z

    .line 2674
    :cond_70
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 2675
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/p;->a(Lat/b;)V

    .line 2677
    :cond_7b
    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v0

    if-eqz v0, :cond_b3

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->R:LaN/B;

    if-eqz v0, :cond_b3

    .line 2678
    invoke-virtual {p1}, Lat/b;->m()Landroid/graphics/Point;

    move-result-object v0

    .line 2681
    if-eqz v0, :cond_10

    .line 2682
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v1, p1}, Lat/c;->a(Lat/b;)V

    .line 2685
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v1

    if-nez v1, :cond_10

    .line 2686
    iget v1, v0, Landroid/graphics/Point;->x:I

    .line 2687
    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 2689
    invoke-static {v1, v0}, Lcom/google/googlenav/ui/p;->d(II)Z

    move-result v2

    if-nez v2, :cond_10

    .line 2690
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->R:LaN/B;

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v4}, LaN/u;->d()LaN/Y;

    move-result-object v4

    invoke-virtual {v3, v1, v0, v4}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    invoke-virtual {v2, v0}, LaN/u;->c(LaN/B;)V

    goto/16 :goto_10

    .line 2696
    :cond_b3
    invoke-virtual {p1}, Lat/b;->e()Z

    move-result v0

    if-eqz v0, :cond_d7

    .line 2697
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0, p1}, Lat/c;->a(Lat/b;)V

    .line 2698
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v0}, Lat/c;->d()V

    .line 2699
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->V:Z

    .line 2700
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v1}, Lat/c;->b()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ao:Lat/c;

    invoke-virtual {v2}, Lat/c;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->a(II)V

    .line 2704
    :cond_d7
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v0

    if-nez v0, :cond_10

    .line 2705
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->R:LaN/B;

    goto/16 :goto_10
.end method

.method private b(Lcom/google/googlenav/ai;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3606
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->E()V

    .line 3608
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    .line 3609
    if-eqz v0, :cond_10

    .line 3610
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    .line 3613
    :cond_10
    new-array v0, v3, [Lcom/google/googlenav/ai;

    aput-object p1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3614
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0, v3}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    .line 3615
    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->a(I)V

    .line 3616
    invoke-virtual {v0}, Lbf/bk;->an()Z

    .line 3617
    return-void
.end method

.method private b(Lcom/google/googlenav/ui/r;)V
    .registers 11
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 2143
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    if-nez v0, :cond_7

    .line 2198
    :cond_6
    :goto_6
    return-void

    .line 2155
    :cond_7
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v7

    .line 2158
    invoke-virtual {v7}, Law/h;->g()V

    .line 2168
    :try_start_e
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->e()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->f()I

    move-result v3

    iget v4, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v5, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v3, v4, v5}, LaN/p;->a(IIII)V

    .line 2175
    iget-object v8, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    monitor-enter v8
    :try_end_22
    .catchall {:try_start_e .. :try_end_22} :catchall_6c

    .line 2176
    :try_start_22
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    check-cast v0, LaN/h;

    invoke-virtual {v0}, LaN/h;->g()V

    .line 2180
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v0

    if-nez v0, :cond_67

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->f()Z

    move-result v0

    if-nez v0, :cond_67

    invoke-virtual {v7}, Law/h;->n()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 2183
    :goto_3d
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->g()Z

    move-result v0

    if-eqz v0, :cond_59

    .line 2184
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v3}, LaH/m;->g()Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, LaN/p;->a(Lam/e;ZZZZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->N:Z

    .line 2187
    :cond_59
    monitor-exit v8
    :try_end_5a
    .catchall {:try_start_22 .. :try_end_5a} :catchall_69

    .line 2189
    invoke-virtual {v7}, Law/h;->h()V

    .line 2193
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->am:Z

    if-eqz v0, :cond_6

    .line 2194
    iput-boolean v6, p0, Lcom/google/googlenav/ui/s;->am:Z

    .line 2195
    invoke-virtual {v7}, Law/h;->h()V

    goto :goto_6

    :cond_67
    move v2, v6

    .line 2180
    goto :goto_3d

    .line 2187
    :catchall_69
    move-exception v0

    :try_start_6a
    monitor-exit v8
    :try_end_6b
    .catchall {:try_start_6a .. :try_end_6b} :catchall_69

    :try_start_6b
    throw v0
    :try_end_6c
    .catchall {:try_start_6b .. :try_end_6c} :catchall_6c

    .line 2189
    :catchall_6c
    move-exception v0

    invoke-virtual {v7}, Law/h;->h()V

    .line 2193
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->am:Z

    if-eqz v1, :cond_79

    .line 2194
    iput-boolean v6, p0, Lcom/google/googlenav/ui/s;->am:Z

    .line 2195
    invoke-virtual {v7}, Law/h;->h()V

    :cond_79
    throw v0
.end method

.method private b(Ljava/lang/Object;B)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 4827
    if-eqz p1, :cond_7

    instance-of v0, p1, Lcom/google/googlenav/aZ;

    if-nez v0, :cond_8

    .line 4862
    :cond_7
    :goto_7
    return-void

    .line 4831
    :cond_8
    check-cast p1, Lcom/google/googlenav/aZ;

    .line 4836
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->i()I

    move-result v0

    .line 4840
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_16

    move v0, v1

    .line 4847
    :cond_16
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    invoke-virtual {v2}, Lbf/i;->av()I

    move-result v2

    if-eqz v2, :cond_30

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->H()Lbf/i;

    move-result-object v2

    invoke-virtual {v2}, Lbf/i;->av()I

    move-result v2

    const/16 v3, 0x12

    if-ne v2, v3, :cond_47

    .line 4849
    :cond_30
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/bk;

    .line 4859
    :goto_38
    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_7

    .line 4860
    invoke-virtual {v0, p2}, Lbf/bk;->a(B)V

    goto :goto_7

    .line 4851
    :cond_47
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2, p1, v1}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v1

    .line 4856
    invoke-virtual {v1}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->a(I)V

    move-object v0, v1

    goto :goto_38
.end method

.method private b(Lcom/google/googlenav/aZ;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3268
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aB()Z

    move-result v2

    if-nez v2, :cond_d

    .line 3296
    :cond_c
    :goto_c
    return v0

    .line 3272
    :cond_d
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v2

    .line 3273
    if-eqz v2, :cond_c

    const-string v3, "##exp "

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_c

    .line 3277
    const-string v0, "##exp "

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 3279
    :try_start_25
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 3280
    invoke-static {}, Laz/c;->a()Laz/c;

    move-result-object v3

    .line 3281
    if-eqz v3, :cond_59

    .line 3282
    if-gez v0, :cond_5b

    .line 3283
    neg-int v4, v0

    invoke-virtual {v3, v4}, Laz/c;->b(I)V

    .line 3288
    :goto_35
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Experiment: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-gez v0, :cond_7e

    const-string v0, "disabled"

    :goto_4e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_59
    move v0, v1

    .line 3296
    goto :goto_c

    .line 3285
    :cond_5b
    invoke-virtual {v3, v0}, Laz/c;->a(I)V
    :try_end_5e
    .catch Ljava/lang/NumberFormatException; {:try_start_25 .. :try_end_5e} :catch_5f

    goto :goto_35

    .line 3291
    :catch_5f
    move-exception v0

    .line 3293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad Experiment id: \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    move v0, v1

    .line 3294
    goto :goto_c

    .line 3288
    :cond_7e
    :try_start_7e
    const-string v0, "enabled"
    :try_end_80
    .catch Ljava/lang/NumberFormatException; {:try_start_7e .. :try_end_80} :catch_5f

    goto :goto_4e
.end method

.method static synthetic c(Lcom/google/googlenav/ui/s;)Las/c;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    return-object v0
.end method

.method private c(Law/g;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 3110
    const/16 v0, 0x4ec

    .line 3111
    instance-of v1, p1, Lax/s;

    if-eqz v1, :cond_d

    .line 3112
    const/16 v0, 0x1b6

    .line 3120
    :cond_8
    :goto_8
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3113
    :cond_d
    instance-of v1, p1, Lax/w;

    if-eqz v1, :cond_14

    .line 3114
    const/16 v0, 0x1b7

    goto :goto_8

    .line 3115
    :cond_14
    instance-of v1, p1, Lax/x;

    if-eqz v1, :cond_1b

    .line 3116
    const/16 v0, 0x1b8

    goto :goto_8

    .line 3117
    :cond_1b
    instance-of v1, p1, Lax/i;

    if-eqz v1, :cond_8

    .line 3118
    const/16 v0, 0x1b5

    goto :goto_8
.end method

.method private c(Lam/e;)V
    .registers 12
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1807
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aH()V

    .line 1810
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->N:Z

    .line 1812
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lam/e;)Lcom/google/googlenav/ui/r;

    move-result-object v7

    .line 1813
    iput-object v7, p0, Lcom/google/googlenav/ui/s;->at:Lcom/google/googlenav/ui/r;

    .line 1815
    const/4 v3, -0x1

    .line 1822
    :try_start_10
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->t()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1823
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2, v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/r;)V

    .line 1826
    :cond_1d
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aG()Z

    move-result v2

    if-nez v2, :cond_94

    move v3, v5

    .line 1837
    :goto_24
    if-nez v3, :cond_bc

    move v4, v0

    .line 1841
    :goto_27
    if-ne v3, v0, :cond_bf

    iget v2, p0, Lcom/google/googlenav/ui/s;->T:I

    if-eq v2, v6, :cond_31

    iget v2, p0, Lcom/google/googlenav/ui/s;->T:I

    if-ne v2, v5, :cond_bf

    :cond_31
    move v2, v0

    :goto_32
    or-int/2addr v4, v2

    .line 1846
    if-ne v3, v0, :cond_3d

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/p;->d()Z

    move-result v2

    if-nez v2, :cond_45

    :cond_3d
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/p;->e()Z

    move-result v2

    if-eqz v2, :cond_c2

    :cond_45
    move v2, v0

    :goto_46
    or-int/2addr v2, v4

    .line 1849
    iput v3, p0, Lcom/google/googlenav/ui/s;->T:I

    .line 1852
    if-eqz v2, :cond_58

    .line 1855
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    iget v8, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v9, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v4, v8, v9}, Lcom/google/googlenav/ui/p;->a(II)Lcom/google/googlenav/ui/r;

    move-result-object v4

    .line 1857
    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V

    .line 1861
    :cond_58
    if-ne v3, v6, :cond_c4

    .line 1863
    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V

    .line 1891
    :cond_5d
    :goto_5d
    invoke-direct {p0, v7, p1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;Lam/e;)V
    :try_end_60
    .catchall {:try_start_10 .. :try_end_60} :catchall_e3

    .line 1893
    iget-boolean v2, p0, Lcom/google/googlenav/ui/s;->a:Z

    if-eqz v2, :cond_67

    .line 1894
    invoke-direct {p0, p1, v3}, Lcom/google/googlenav/ui/s;->a(Lam/e;I)V

    .line 1900
    :cond_67
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->a()LaN/D;

    move-result-object v2

    invoke-virtual {v2}, LaN/D;->m()Z

    move-result v2

    if-eqz v2, :cond_118

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->a()LaN/D;

    move-result-object v2

    invoke-virtual {v2}, LaN/D;->n()Z

    move-result v2

    if-nez v2, :cond_118

    .line 1902
    :goto_7f
    invoke-static {}, Lcom/google/googlenav/friend/p;->e()Z

    move-result v1

    .line 1903
    if-nez v0, :cond_87

    if-eqz v1, :cond_8a

    .line 1904
    :cond_87
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lam/e;)V

    .line 1914
    :cond_8a
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v0

    if-eqz v0, :cond_93

    .line 1916
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    .line 1919
    :cond_93
    return-void

    .line 1828
    :cond_94
    :try_start_94
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v2

    if-eqz v2, :cond_a4

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/p;->f()Z

    move-result v2

    if-eqz v2, :cond_a4

    move v3, v0

    .line 1829
    goto :goto_24

    .line 1830
    :cond_a4
    invoke-static {}, Lcom/google/googlenav/ui/p;->c()Z

    move-result v2

    if-eqz v2, :cond_b6

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bF;->d()Z

    move-result v2

    if-nez v2, :cond_b6

    iget v2, p0, Lcom/google/googlenav/ui/s;->I:I

    if-nez v2, :cond_b9

    :cond_b6
    move v3, v6

    .line 1832
    goto/16 :goto_24

    :cond_b9
    move v3, v1

    .line 1834
    goto/16 :goto_24

    :cond_bc
    move v4, v1

    .line 1837
    goto/16 :goto_27

    :cond_bf
    move v2, v1

    .line 1841
    goto/16 :goto_32

    :cond_c2
    move v2, v1

    .line 1846
    goto :goto_46

    .line 1864
    :cond_c4
    if-eq v3, v5, :cond_5d

    .line 1874
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4, v7}, Lcom/google/googlenav/ui/p;->a(Lcom/google/googlenav/ui/r;)V

    .line 1875
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/p;->j()Z

    move-result v4

    if-eqz v4, :cond_d6

    .line 1877
    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V

    .line 1879
    :cond_d6
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4, v7}, Lcom/google/googlenav/ui/p;->b(Lcom/google/googlenav/ui/r;)V

    .line 1880
    if-nez v2, :cond_5d

    .line 1887
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/r;)V
    :try_end_e1
    .catchall {:try_start_94 .. :try_end_e1} :catchall_e3

    goto/16 :goto_5d

    .line 1893
    :catchall_e3
    move-exception v2

    iget-boolean v4, p0, Lcom/google/googlenav/ui/s;->a:Z

    if-eqz v4, :cond_eb

    .line 1894
    invoke-direct {p0, p1, v3}, Lcom/google/googlenav/ui/s;->a(Lam/e;I)V

    .line 1900
    :cond_eb
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v3}, LaN/p;->a()LaN/D;

    move-result-object v3

    invoke-virtual {v3}, LaN/D;->m()Z

    move-result v3

    if-eqz v3, :cond_11b

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v3}, LaN/p;->a()LaN/D;

    move-result-object v3

    invoke-virtual {v3}, LaN/D;->n()Z

    move-result v3

    if-nez v3, :cond_11b

    .line 1902
    :goto_103
    invoke-static {}, Lcom/google/googlenav/friend/p;->e()Z

    move-result v1

    .line 1903
    if-nez v0, :cond_10b

    if-eqz v1, :cond_10e

    .line 1904
    :cond_10b
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lam/e;)V

    .line 1914
    :cond_10e
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v0

    if-eqz v0, :cond_117

    .line 1916
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    .line 1918
    :cond_117
    throw v2

    :cond_118
    move v0, v1

    .line 1900
    goto/16 :goto_7f

    :cond_11b
    move v0, v1

    goto :goto_103
.end method

.method private c(Lax/k;)V
    .registers 4
    .parameter

    .prologue
    .line 3045
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_d

    .line 3070
    :cond_c
    :goto_c
    return-void

    .line 3049
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 3050
    if-eqz v0, :cond_c

    .line 3053
    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v1

    .line 3054
    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    .line 3059
    if-nez v0, :cond_25

    .line 3060
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v0

    .line 3063
    :cond_25
    invoke-static {v1, v0}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 3065
    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->q()Z

    move-result v1

    if-eqz v1, :cond_37

    .line 3066
    invoke-interface {p1, v0}, Lax/k;->a(Lax/y;)V

    goto :goto_c

    .line 3067
    :cond_37
    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->q()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 3068
    invoke-interface {p1, v0}, Lax/k;->b(Lax/y;)V

    goto :goto_c
.end method

.method private c(Lcom/google/googlenav/ui/r;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2212
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 2213
    invoke-virtual {v0}, Law/h;->m()Z

    move-result v0

    if-eqz v0, :cond_65

    .line 2214
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aq:Z

    if-eqz v0, :cond_2d

    .line 2215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x303

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 2217
    iput-boolean v2, p0, Lcom/google/googlenav/ui/s;->aq:Z

    .line 2224
    :cond_2d
    :goto_2d
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v0

    if-eqz v0, :cond_64

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_64

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->ap:Z

    if-eqz v0, :cond_64

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/X;->a(LaN/p;)Z

    move-result v0

    if-eqz v0, :cond_64

    .line 2228
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/X;->b(LaN/p;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 2229
    const/16 v0, 0x316

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 2230
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 2232
    :cond_62
    iput-boolean v2, p0, Lcom/google/googlenav/ui/s;->ap:Z

    .line 2234
    :cond_64
    return-void

    .line 2221
    :cond_65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->aq:Z

    goto :goto_2d
.end method

.method private c(Lat/b;)Z
    .registers 3
    .parameter

    .prologue
    .line 2715
    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v0

    return v0
.end method

.method private c(Lcom/google/googlenav/aZ;)Z
    .registers 4
    .parameter

    .prologue
    .line 3305
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 3306
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    .line 3307
    if-eqz v0, :cond_19

    const-string v1, "##dumpmem"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 3308
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aT()V

    .line 3309
    const/4 v0, 0x1

    .line 3312
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method static synthetic d(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/ui/wizard/jv;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private static d(I)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 2094
    packed-switch p0, :pswitch_data_12

    .line 2099
    const-string v0, "?"

    :goto_5
    return-object v0

    .line 2095
    :pswitch_6
    const-string v0, "Map->Buf->Scr"

    goto :goto_5

    .line 2096
    :pswitch_9
    const-string v0, "Buf->Scr"

    goto :goto_5

    .line 2097
    :pswitch_c
    const-string v0, "Map->Scr"

    goto :goto_5

    .line 2098
    :pswitch_f
    const-string v0, "No map"

    goto :goto_5

    .line 2094
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method private d(Lam/e;)V
    .registers 5
    .parameter

    .prologue
    .line 2291
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->z()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->x:C

    invoke-interface {v0, v1}, Lam/g;->c(C)I

    move-result v0

    .line 2296
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/s;->n:I

    sub-int v0, v2, v0

    add-int/lit8 v0, v0, -0x3

    const/4 v2, 0x6

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/googlenav/ui/bi;->a(Lam/e;II)V

    .line 2297
    return-void
.end method

.method private d(Lax/k;)V
    .registers 5
    .parameter

    .prologue
    .line 3074
    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    .line 3075
    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    .line 3076
    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    .line 3077
    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    .line 3078
    const-string v0, "0"

    .line 3079
    const-string v1, "driving"

    .line 3080
    instance-of v1, p1, Lax/w;

    if-eqz v1, :cond_47

    .line 3081
    const-string v0, "1"

    .line 3082
    const-string v1, "transit"

    .line 3090
    :cond_28
    :goto_28
    const/4 v1, 0x0

    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->d()I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/s;->a(IILjava/lang/String;)V

    .line 3092
    const/4 v1, 0x1

    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->d()I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/s;->a(IILjava/lang/String;)V

    .line 3095
    const/4 v1, 0x4

    const-string v2, "m"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3100
    return-void

    .line 3083
    :cond_47
    instance-of v1, p1, Lax/x;

    if-eqz v1, :cond_50

    .line 3084
    const-string v0, "2"

    .line 3085
    const-string v1, "walking"

    goto :goto_28

    .line 3086
    :cond_50
    instance-of v1, p1, Lax/i;

    if-eqz v1, :cond_28

    .line 3087
    const-string v0, "3"

    .line 3088
    const-string v1, "bicycling"

    goto :goto_28
.end method

.method private d(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2358
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2359
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lat/a;)I

    move-result v1

    .line 2360
    packed-switch v1, :pswitch_data_4a

    .line 2371
    :cond_12
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_42

    .line 2373
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->aC()Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 2374
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/r;->b()V

    .line 2388
    :goto_25
    return v0

    .line 2363
    :pswitch_26
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aK()V

    goto :goto_25

    .line 2378
    :cond_2a
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    .line 2379
    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_42

    .line 2380
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 2381
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    goto :goto_25

    .line 2387
    :cond_42
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->b(Lat/a;)Z

    .line 2388
    const/4 v0, 0x0

    goto :goto_25

    .line 2360
    nop

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_26
    .end packed-switch
.end method

.method static synthetic e(Lcom/google/googlenav/ui/s;)Lbf/am;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    return-object v0
.end method

.method private e(Lat/a;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2441
    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2469
    :cond_7
    :goto_7
    return-void

    .line 2444
    :cond_8
    const-string v0, "8596494"

    iget v2, p0, Lcom/google/googlenav/ui/s;->l:I

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    if-ne v0, v2, :cond_32

    .line 2446
    iget v0, p0, Lcom/google/googlenav/ui/s;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/s;->l:I

    .line 2447
    iget v0, p0, Lcom/google/googlenav/ui/s;->l:I

    const-string v2, "8596494"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_7

    .line 2448
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->a:Z

    if-nez v0, :cond_30

    const/4 v0, 0x1

    :goto_2b
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->a:Z

    .line 2449
    iput v1, p0, Lcom/google/googlenav/ui/s;->l:I

    goto :goto_7

    :cond_30
    move v0, v1

    .line 2448
    goto :goto_2b

    .line 2452
    :cond_32
    iput v1, p0, Lcom/google/googlenav/ui/s;->l:I

    goto :goto_7
.end method

.method static synthetic f(Lcom/google/googlenav/ui/s;)V
    .registers 1
    .parameter

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aF()V

    return-void
.end method

.method private f(Lat/a;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2495
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->q:Z

    .line 2501
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v2

    if-nez v2, :cond_15

    .line 2502
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2, p1}, Lbf/am;->a(Lat/a;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 2537
    :cond_14
    :goto_14
    return v1

    .line 2507
    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v2

    if-eqz v2, :cond_33

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lat/a;)Z

    move-result v2

    if-nez v2, :cond_33

    .line 2509
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lat/a;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/googlenav/ui/s;->q:Z

    .line 2510
    iget-boolean v2, p0, Lcom/google/googlenav/ui/s;->q:Z

    if-eqz v2, :cond_47

    .line 2513
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->a()V

    goto :goto_14

    .line 2516
    :cond_33
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v2

    if-eqz v2, :cond_47

    .line 2517
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Lat/a;)I

    move-result v2

    invoke-virtual {p0, v2, p1}, Lcom/google/googlenav/ui/s;->a(ILat/a;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 2523
    :cond_47
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v2

    if-eqz v2, :cond_77

    .line 2524
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->w()Z

    move-result v2

    if-eqz v2, :cond_63

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x37

    if-ne v2, v3, :cond_63

    .line 2526
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->Q()V

    goto :goto_14

    .line 2530
    :cond_63
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v2

    const/16 v3, 0x35

    if-ne v2, v3, :cond_77

    .line 2531
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    if-eqz v2, :cond_75

    :goto_71
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(I)Z

    goto :goto_14

    :cond_75
    move v0, v1

    goto :goto_71

    :cond_77
    move v1, v0

    .line 2537
    goto :goto_14
.end method

.method static synthetic g(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/android/aa;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method private g(Lat/a;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2805
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->Q:Lcom/google/googlenav/ui/W;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/google/googlenav/ui/W;->a(Lat/a;Z)I

    move-result v2

    .line 2806
    if-eqz v2, :cond_17

    .line 2807
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    if-gez v2, :cond_13

    move v0, v1

    :cond_13
    invoke-virtual {v3, v0}, LaN/u;->a(Z)Z

    .line 2810
    :goto_16
    return v1

    :cond_17
    move v1, v0

    goto :goto_16
.end method

.method static synthetic h(Lcom/google/googlenav/ui/s;)LaH/m;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    return-object v0
.end method

.method private h(Lat/a;)Z
    .registers 7
    .parameter

    .prologue
    .line 2868
    const/16 v0, 0x19

    .line 2869
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    if-nez v1, :cond_1d

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->L:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xfa

    cmp-long v1, v1, v3

    if-gez v1, :cond_1d

    .line 2872
    const/16 v0, 0xc

    .line 2875
    :cond_1d
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/s;->a(Lat/a;I)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 2878
    const/4 v0, 0x1

    .line 2881
    :goto_24
    return v0

    :cond_25
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/ak;->a(Lat/a;)Z

    move-result v0

    goto :goto_24
.end method

.method static synthetic i(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/friend/J;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->x:Lcom/google/googlenav/friend/J;

    return-object v0
.end method

.method private i(Lax/b;)V
    .registers 5
    .parameter

    .prologue
    .line 3033
    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    .line 3036
    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->ar()Lax/y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lax/z;->a(Lax/y;Lax/y;)V

    .line 3037
    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {p1}, Lax/b;->at()Lax/y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lax/z;->a(Lax/y;Lax/y;)V

    .line 3038
    return-void
.end method

.method private j(Lax/b;)V
    .registers 8
    .parameter

    .prologue
    .line 3125
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Law/g;)Ljava/lang/String;

    move-result-object v1

    .line 3130
    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    .line 3131
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 3132
    return-void
.end method

.method static synthetic j(Lcom/google/googlenav/ui/s;)Z
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    return v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/s;)Las/d;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/s;)Lcom/google/googlenav/friend/a;
    .registers 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    return-object v0
.end method

.method private l(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    .line 3533
    new-instance v0, Lcom/google/googlenav/ui/A;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/A;-><init>(Lcom/google/googlenav/ui/s;)V

    .line 3545
    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 3546
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    .line 3548
    const/16 v0, 0x1af

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 3550
    return-void
.end method

.method private m(Ljava/lang/String;)LaR/H;
    .registers 4
    .parameter

    .prologue
    .line 3563
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v0

    .line 3567
    if-eqz v0, :cond_19

    invoke-virtual {v0}, LaR/H;->c()Z

    move-result v1

    if-nez v1, :cond_19

    .line 3579
    :cond_18
    :goto_18
    return-object v0

    .line 3571
    :cond_19
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->g()LaR/n;

    move-result-object v1

    invoke-interface {v1}, LaR/n;->e()LaR/u;

    move-result-object v1

    invoke-interface {v1, p1}, LaR/u;->b(Ljava/lang/String;)LaR/H;

    move-result-object v1

    .line 3574
    if-eqz v1, :cond_18

    move-object v0, v1

    .line 3577
    goto :goto_18
.end method

.method private r(Z)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0xa

    const/4 v1, 0x1

    .line 2776
    if-eqz p1, :cond_22

    .line 2778
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->k()V

    .line 2779
    invoke-static {}, Lcom/google/googlenav/K;->e()V

    .line 2780
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Z)V

    .line 2781
    const-string v0, "a"

    invoke-static {v2, v0}, Lbm/m;->a(ILjava/lang/String;)V

    .line 2786
    sget-object v0, Lcom/google/googlenav/z;->d:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    .line 2788
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->c(Lcom/google/googlenav/ui/s;)V

    .line 2796
    :goto_21
    return-void

    .line 2790
    :cond_22
    const-string v0, "d"

    invoke-static {v2, v0}, Lbm/m;->a(ILjava/lang/String;)V

    .line 2793
    invoke-static {}, Lbm/m;->a()Lbm/m;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbm/m;->a(Z)V

    .line 2794
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_21
.end method


# virtual methods
.method public A()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 1469
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->as:Z

    if-eqz v1, :cond_6

    .line 1476
    :goto_5
    return v0

    .line 1472
    :cond_6
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    const-string v2, "T_AND_C_ACCEPT"

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_13

    .line 1473
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->as:Z

    goto :goto_5

    .line 1476
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public B()Z
    .registers 2

    .prologue
    .line 2136
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public C()V
    .registers 2

    .prologue
    .line 2202
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->am:Z

    if-eqz v0, :cond_e

    .line 2203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->am:Z

    .line 2204
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0}, Law/h;->h()V

    .line 2206
    :cond_e
    return-void
.end method

.method public C_()V
    .registers 4

    .prologue
    .line 5013
    const/4 v0, 0x0

    .line 5014
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/G;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/G;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 5020
    return-void
.end method

.method public D()Z
    .registers 2

    .prologue
    .line 2238
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->R()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->c()Z

    move-result v0

    if-nez v0, :cond_28

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v0

    if-eqz v0, :cond_28

    :cond_26
    const/4 v0, 0x1

    :goto_27
    return v0

    :cond_28
    const/4 v0, 0x0

    goto :goto_27
.end method

.method public D_()V
    .registers 4

    .prologue
    .line 5038
    const/4 v0, 0x0

    .line 5039
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/H;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/H;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 5045
    return-void
.end method

.method public E()V
    .registers 2

    .prologue
    .line 2252
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->R()V

    .line 2255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->e:Z

    .line 2256
    return-void
.end method

.method public E_()V
    .registers 1

    .prologue
    .line 5055
    return-void
.end method

.method public F()Z
    .registers 2

    .prologue
    .line 2263
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->e:Z

    if-eqz v0, :cond_6

    .line 2264
    const/4 v0, 0x0

    .line 2277
    :goto_5
    return v0

    .line 2270
    :cond_6
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-nez v0, :cond_10

    .line 2271
    const/4 v0, 0x1

    goto :goto_5

    .line 2277
    :cond_10
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aI()Z

    move-result v0

    goto :goto_5
.end method

.method public G()I
    .registers 2

    .prologue
    .line 2617
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2618
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->u()I

    move-result v0

    .line 2620
    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->R()Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    goto :goto_e

    :cond_19
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public H()V
    .registers 3

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    .line 3340
    if-eqz v0, :cond_e

    .line 3341
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->e(Lbf/i;)V

    .line 3351
    :goto_d
    return-void

    .line 3344
    :cond_e
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->j()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 3345
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->d()V

    .line 3346
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->f()V

    goto :goto_d

    .line 3348
    :cond_27
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->i()V

    goto :goto_d
.end method

.method public I()V
    .registers 2

    .prologue
    .line 3354
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    .line 3355
    if-eqz v0, :cond_d

    .line 3356
    invoke-virtual {v0}, Lbf/bK;->d()V

    .line 3358
    :cond_d
    return-void
.end method

.method public J()V
    .registers 2

    .prologue
    .line 3361
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->w()Lbf/bK;

    move-result-object v0

    .line 3362
    if-eqz v0, :cond_d

    .line 3363
    invoke-virtual {v0}, Lbf/bK;->f()V

    .line 3365
    :cond_d
    return-void
.end method

.method public K()V
    .registers 2

    .prologue
    .line 3368
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->h()V

    .line 3369
    return-void
.end method

.method L()V
    .registers 6

    .prologue
    .line 3411
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3452
    :goto_a
    return-void

    .line 3416
    :cond_b
    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-nez v0, :cond_19

    .line 3417
    invoke-static {}, Lcom/google/googlenav/friend/ac;->b()Lcom/google/googlenav/friend/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bi;->B_()V

    goto :goto_a

    .line 3422
    :cond_19
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/am;->f(Z)V

    .line 3425
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    .line 3426
    if-eqz v0, :cond_49

    .line 3431
    invoke-virtual {v0}, Lbf/X;->ax()Z

    move-result v1

    if-nez v1, :cond_32

    .line 3432
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->e(Lbf/i;)V

    .line 3434
    :cond_32
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->a(Lbf/X;)V

    .line 3441
    :goto_37
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->aa()Lbf/a;

    move-result-object v1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v4, Lcom/google/googlenav/ui/y;

    invoke-direct {v4, p0, v0}, Lcom/google/googlenav/ui/y;-><init>(Lcom/google/googlenav/ui/s;Lbf/X;)V

    invoke-virtual {v1, v2, v3, v4}, Lbf/a;->a(ILcom/google/googlenav/android/aa;Lbf/f;)V

    goto :goto_a

    .line 3437
    :cond_49
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-direct {v1}, Lcom/google/googlenav/aZ;-><init>()V

    invoke-virtual {v0, v1}, Lbf/am;->e(Lcom/google/googlenav/aZ;)Lbf/X;

    move-result-object v0

    goto :goto_37
.end method

.method public M()J
    .registers 3

    .prologue
    .line 3719
    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    return-wide v0
.end method

.method public M_()V
    .registers 1

    .prologue
    .line 5025
    return-void
.end method

.method public N()J
    .registers 3

    .prologue
    .line 3724
    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Z:J

    return-wide v0
.end method

.method public N_()V
    .registers 1

    .prologue
    .line 5050
    return-void
.end method

.method protected O()V
    .registers 5

    .prologue
    .line 3734
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->Z:J

    .line 3735
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    const-string v1, "LastSessionTime"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    .line 3737
    return-void
.end method

.method public P()Z
    .registers 2

    .prologue
    .line 3791
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->h()Z

    move-result v0

    return v0
.end method

.method public Q()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 3857
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    .line 3858
    return-void

    :cond_c
    move v0, v1

    .line 3857
    goto :goto_8
.end method

.method public R()V
    .registers 2

    .prologue
    .line 3996
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->r()V

    .line 3997
    return-void
.end method

.method public S()V
    .registers 1

    .prologue
    .line 4027
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aO()V

    .line 4033
    return-void
.end method

.method public T()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 4040
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    .line 4041
    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->m:Z

    .line 4056
    invoke-static {}, Lcom/google/googlenav/friend/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 4057
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->k(Z)V

    .line 4059
    :cond_f
    return-void
.end method

.method public U()V
    .registers 12

    .prologue
    const-wide/16 v9, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 4090
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->m:Z

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_d

    .line 4237
    :cond_c
    :goto_c
    return-void

    .line 4094
    :cond_d
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v5

    .line 4097
    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    .line 4101
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->X:Z

    .line 4102
    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->X:Z

    .line 4105
    if-nez v0, :cond_25

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->N:Z

    if-nez v0, :cond_128

    :cond_25
    move v0, v2

    .line 4107
    :goto_26
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    if-eqz v3, :cond_140

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->L:J

    sub-long v3, v5, v3

    const-wide/16 v7, 0x64

    cmp-long v3, v3, v7

    if-lez v3, :cond_140

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v3

    if-eqz v3, :cond_140

    iget-boolean v3, p0, Lcom/google/googlenav/ui/s;->q:Z

    if-eqz v3, :cond_140

    .line 4111
    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->K:J

    cmp-long v3, v3, v9

    if-nez v3, :cond_12b

    .line 4112
    const/4 v3, 0x6

    .line 4113
    iput-wide v5, p0, Lcom/google/googlenav/ui/s;->K:J

    .line 4123
    :goto_47
    const/16 v4, 0xc

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 4124
    iget-boolean v4, p0, Lcom/google/googlenav/ui/s;->U:Z

    iget-object v7, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    invoke-direct {p0, v7, v3}, Lcom/google/googlenav/ui/s;->a(Lat/a;I)Z

    move-result v3

    or-int/2addr v3, v4

    iput-boolean v3, p0, Lcom/google/googlenav/ui/s;->U:Z

    .line 4129
    :goto_58
    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->H:J

    cmp-long v3, v5, v3

    if-lez v3, :cond_164

    .line 4132
    iget-boolean v3, p0, Lcom/google/googlenav/ui/s;->W:Z

    if-nez v3, :cond_6a

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->r()Z

    move-result v3

    if-eqz v3, :cond_144

    :cond_6a
    const-wide/16 v3, 0x5dc

    :goto_6c
    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/googlenav/ui/s;->H:J

    .line 4135
    iget v3, p0, Lcom/google/googlenav/ui/s;->G:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/googlenav/ui/s;->G:I

    .line 4138
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/ak;->h()Z

    move-result v3

    if-eqz v3, :cond_148

    move v0, v2

    move v3, v2

    .line 4149
    :goto_7f
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    if-eqz v4, :cond_8f

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/bF;->d()Z

    move-result v4

    if-eqz v4, :cond_8f

    .line 4151
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    move v0, v2

    .line 4154
    :cond_8f
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    if-eqz v4, :cond_9c

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/p;->i()Z

    move-result v4

    if-eqz v4, :cond_9c

    move v0, v2

    .line 4158
    :cond_9c
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/ak;->a()V

    .line 4160
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v4

    .line 4164
    if-eqz v4, :cond_be

    .line 4167
    iget-boolean v7, p0, Lcom/google/googlenav/ui/s;->U:Z

    invoke-virtual {v4, v3}, Lcom/google/googlenav/ui/wizard/C;->b(Z)Z

    move-result v3

    or-int/2addr v3, v7

    iput-boolean v3, p0, Lcom/google/googlenav/ui/s;->U:Z

    .line 4169
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->m()Z

    move-result v3

    if-eqz v3, :cond_be

    .line 4170
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aK()V

    move v0, v2

    .line 4175
    :cond_be
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3, v5, v6}, Lcom/google/googlenav/ui/ak;->a(J)V

    .line 4177
    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->J:J

    const-wide/16 v7, 0x1388

    add-long/2addr v3, v7

    cmp-long v3, v3, v5

    if-gtz v3, :cond_cd

    move v0, v2

    .line 4182
    :cond_cd
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v3}, Lcom/google/googlenav/A;->b()Z

    move-result v3

    if-nez v3, :cond_d7

    if-eqz v0, :cond_15e

    :cond_d7
    move v0, v2

    .line 4188
    :goto_d8
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v3

    if-eqz v3, :cond_106

    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->M:J

    const-wide/32 v5, 0x2932e0

    add-long/2addr v3, v5

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_106

    .line 4192
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/googlenav/ui/s;->M:J

    .line 4193
    invoke-virtual {p0, v1, v1}, Lcom/google/googlenav/ui/s;->a(ZZ)V

    move v0, v2

    .line 4209
    :cond_106
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    or-int/2addr v0, v1

    .line 4219
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->W()Z

    move-result v2

    or-int/2addr v1, v2

    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->U:Z

    .line 4222
    if-eqz v0, :cond_117

    .line 4223
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    .line 4231
    :cond_117
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->c:Z

    if-nez v0, :cond_121

    .line 4232
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->z()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->c:Z

    .line 4236
    :cond_121
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-virtual {v0}, LaR/b;->a()V

    goto/16 :goto_c

    :cond_128
    move v0, v1

    .line 4105
    goto/16 :goto_26

    .line 4115
    :cond_12b
    iget-wide v3, p0, Lcom/google/googlenav/ui/s;->K:J

    sub-long v3, v5, v3

    long-to-int v3, v3

    int-to-long v3, v3

    .line 4116
    const-wide/16 v7, 0x6

    mul-long/2addr v3, v7

    long-to-int v4, v3

    .line 4117
    div-int/lit8 v3, v4, 0x64

    .line 4120
    rem-int/lit8 v4, v4, 0x64

    int-to-long v7, v4

    sub-long v7, v5, v7

    iput-wide v7, p0, Lcom/google/googlenav/ui/s;->K:J

    goto/16 :goto_47

    .line 4126
    :cond_140
    iput-wide v9, p0, Lcom/google/googlenav/ui/s;->K:J

    goto/16 :goto_58

    .line 4132
    :cond_144
    const-wide/16 v3, 0x1f4

    goto/16 :goto_6c

    .line 4141
    :cond_148
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v3

    invoke-virtual {v3}, Law/h;->l()Z

    move-result v3

    if-nez v3, :cond_15a

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/ak;->d()Z

    move-result v3

    if-eqz v3, :cond_161

    :cond_15a
    move v0, v2

    move v3, v2

    .line 4144
    goto/16 :goto_7f

    :cond_15e
    move v0, v1

    .line 4182
    goto/16 :goto_d8

    :cond_161
    move v3, v2

    goto/16 :goto_7f

    :cond_164
    move v3, v1

    goto/16 :goto_7f
.end method

.method public V()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 4243
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->v()Z

    move-result v1

    if-nez v1, :cond_41

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->G()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 4248
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_38

    .line 4249
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v1, :cond_38

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_38

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    invoke-virtual {v1}, Lbf/i;->L()Z

    move-result v1

    if-eqz v1, :cond_38

    const/4 v0, 0x1

    .line 4253
    :cond_38
    invoke-static {}, Lcom/google/googlenav/ui/aL;->a()Lcom/google/googlenav/ui/aL;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/ui/aL;->a(LaN/p;Z)V

    .line 4255
    :cond_41
    return-void
.end method

.method public W()Z
    .registers 2

    .prologue
    .line 4286
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->l()Z

    move-result v0

    return v0
.end method

.method public X()V
    .registers 2

    .prologue
    .line 4333
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->a()V

    .line 4334
    return-void
.end method

.method public Y()V
    .registers 2

    .prologue
    .line 4340
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->c()V

    .line 4341
    return-void
.end method

.method public declared-synchronized Z()V
    .registers 2

    .prologue
    .line 4504
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->l()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 4505
    monitor-exit p0

    return-void

    .line 4504
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lbf/aJ;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3656
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;Lcom/google/googlenav/layer/m;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3657
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->d(Lcom/google/googlenav/aZ;)Lbf/aJ;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/bN;)Lbf/bH;
    .registers 3
    .parameter

    .prologue
    .line 3828
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lcom/google/googlenav/bN;)Lbf/bH;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/Y;)Lbf/bU;
    .registers 4
    .parameter

    .prologue
    .line 3823
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/am;->a(Lcom/google/googlenav/Y;Z)Lbf/bU;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/googlenav/aZ;Z)Lbf/bk;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3804
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1, p2}, Lbf/am;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/bf;)Lbf/bk;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 3621
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3622
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->c(Lcom/google/googlenav/aZ;)Lbf/bk;

    move-result-object v0

    return-object v0
.end method

.method public a(Lax/y;Lax/y;)Lcom/google/googlenav/aa;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 5599
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 5600
    new-instance v1, Lcom/google/googlenav/aa;

    invoke-static {p1}, Lcom/google/googlenav/ui/bv;->c(Lax/y;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Lcom/google/googlenav/aa;-><init>(Lax/y;Ljava/lang/String;Lcom/google/googlenav/ui/bh;)V

    .line 5603
    if-eqz p2, :cond_1a

    .line 5604
    invoke-virtual {v1, p2}, Lcom/google/googlenav/aa;->a(Lax/y;)V

    .line 5606
    :cond_1a
    invoke-static {}, Lbf/bU;->bL()Lcom/google/googlenav/ab;

    move-result-object v2

    .line 5607
    sget-object v3, Lbf/bU;->C:Lcom/google/googlenav/ab;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_33

    .line 5608
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    aput v2, v3, v4

    invoke-virtual {v1, v3}, Lcom/google/googlenav/aa;->a([I)V

    .line 5610
    :cond_33
    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 5611
    return-object v1
.end method

.method public a(Lbf/bG;Lcom/google/googlenav/bU;Z)Lcom/google/googlenav/bW;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5617
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 5618
    new-instance v1, Lcom/google/googlenav/bW;

    invoke-direct {v1, p1, p3}, Lcom/google/googlenav/bW;-><init>(Lbf/bG;Z)V

    .line 5620
    invoke-virtual {v1, p2}, Lcom/google/googlenav/bW;->a(Lcom/google/googlenav/bU;)V

    .line 5621
    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 5622
    return-object v1
.end method

.method public a()V
    .registers 2

    .prologue
    .line 3146
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    .line 3147
    return-void
.end method

.method public a(ILaH/m;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 4713
    return-void
.end method

.method public a(I[Lba/f;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5235
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(I[Lba/f;)V

    .line 5236
    return-void
.end method

.method public a(LaH/m;LaN/B;ZLjava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-static {p1, p2, p3, p4, v0}, LaH/d;->a(LaH/m;LaN/B;ZLjava/lang/String;Las/c;)V

    .line 1377
    return-void
.end method

.method public a(LaN/B;)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 2800
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LaN/u;->e(LaN/B;LaN/Y;)V

    .line 2801
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1, p1}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v2

    const/4 v4, 0x1

    move-object v1, p1

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(LaN/B;Lo/D;Ljava/lang/String;Z[Ljava/lang/String;)Lbf/C;

    .line 2802
    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4702
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/A;->b()Z

    .line 4703
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Law/h;->a(Z)V

    .line 4708
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aQ()V

    .line 4709
    return-void
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5244
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v5

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v6

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v7

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v9, p5

    invoke-virtual/range {v0 .. v9}, Lcom/google/googlenav/ui/wizard/jv;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;LaN/H;IILcom/google/googlenav/ui/wizard/C;Z)V

    .line 5247
    return-void
.end method

.method public a(Lam/e;)V
    .registers 11
    .parameter

    .prologue
    .line 1705
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_5

    .line 1737
    :goto_4
    return-void

    .line 1709
    :cond_5
    :try_start_5
    iget v0, p0, Lcom/google/googlenav/ui/s;->I:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_26

    .line 1716
    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    const-wide/16 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Landroid/os/Handler;JJLas/c;J)V

    .line 1719
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    const-wide/16 v7, 0x0

    invoke-static/range {v0 .. v8}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Landroid/os/Handler;JJLas/c;J)V

    .line 1732
    :cond_26
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lam/e;)V

    .line 1733
    iget v0, p0, Lcom/google/googlenav/ui/s;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/s;->I:I
    :try_end_2f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_2f} :catch_30

    goto :goto_4

    .line 1734
    :catch_30
    move-exception v0

    .line 1735
    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_4
.end method

.method public a(Lat/b;)V
    .registers 3
    .parameter

    .prologue
    .line 2730
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_5

    .line 2745
    :goto_4
    return-void

    .line 2734
    :cond_5
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aJ()V

    .line 2736
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lat/b;)V

    .line 2738
    invoke-virtual {p1}, Lat/b;->c()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 2740
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->X:Z

    goto :goto_4

    .line 2743
    :cond_15
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    goto :goto_4
.end method

.method public a(Law/g;)V
    .registers 13
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x7

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 1496
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v1, :cond_a

    .line 1642
    :goto_9
    return-void

    .line 1506
    :cond_a
    instance-of v1, p1, Lcom/google/googlenav/aZ;

    if-eqz v1, :cond_17c

    .line 1507
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    .line 1508
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->f:Lbm/i;

    invoke-virtual {v1}, Lbm/i;->a()V

    .line 1509
    check-cast p1, Lcom/google/googlenav/aZ;

    .line 1513
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v1

    if-nez v1, :cond_23

    .line 1514
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    .line 1521
    :cond_23
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->t()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 1522
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->u()I

    move-result v2

    invoke-virtual {v1, v2}, LaM/f;->a(I)V

    .line 1525
    :cond_34
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->x()Z

    move-result v1

    if-eqz v1, :cond_9c

    .line 1528
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->y()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lax/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->b(Lax/b;)V

    .line 1585
    :cond_45
    :goto_45
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ax()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 1586
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aR()V

    .line 1588
    :cond_4e
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 1589
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "s=po"

    aput-object v1, v0, v10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "e="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->p()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aH:Lbm/i;

    invoke-virtual {v2}, Lbm/i;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1595
    const-string v1, "stat"

    invoke-static {v9, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1641
    :cond_97
    :goto_97
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    goto/16 :goto_9

    .line 1529
    :cond_9c
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/googlenav/bf;->G:Z

    if-eqz v1, :cond_f9

    .line 1533
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aD()I

    move-result v1

    .line 1535
    if-lez v1, :cond_b9

    .line 1536
    sget-object v2, LaA/b;->k:LaA/c;

    invoke-virtual {v2, v1}, LaA/c;->a(I)V

    .line 1537
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v2, v1}, Lcom/google/googlenav/offers/a;->a(I)V

    .line 1538
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v2, p1}, Lcom/google/googlenav/offers/a;->a(Lcom/google/googlenav/aZ;)V

    .line 1542
    :cond_b9
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v2

    .line 1543
    const/16 v3, 0x58

    const-string v4, "b"

    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "c="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v10

    if-nez v2, :cond_e5

    :goto_da
    aput-object v0, v5, v8

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_45

    :cond_e5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "u="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_da

    .line 1549
    :cond_f9
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->n()Z

    move-result v1

    if-eqz v1, :cond_12b

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->au()Z

    move-result v1

    if-nez v1, :cond_12b

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v1

    if-ne v1, v4, :cond_111

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->q()Z

    move-result v1

    if-nez v1, :cond_12b

    .line 1551
    :cond_111
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 1555
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 1556
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ai()Z

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V

    goto/16 :goto_45

    .line 1559
    :cond_12b
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aO()Z

    move-result v1

    if-eqz v1, :cond_159

    .line 1560
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ak()I

    move-result v0

    if-ne v0, v8, :cond_152

    .line 1562
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    .line 1563
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->S()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->T()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->d(II)V

    .line 1565
    invoke-virtual {p0, v8}, Lcom/google/googlenav/ui/s;->q(Z)V

    goto/16 :goto_45

    .line 1567
    :cond_152
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1, v10}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    goto/16 :goto_45

    .line 1572
    :cond_159
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ai()Z

    move-result v3

    invoke-virtual {v1, p1, v2, v3}, Lbf/am;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/A;Z)V

    .line 1575
    invoke-static {p1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;)V

    .line 1578
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v1

    if-eq v1, v4, :cond_45

    .line 1580
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v2

    invoke-interface {v1, v2, v9, v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    goto/16 :goto_45

    .line 1597
    :cond_17c
    instance-of v0, p1, Lax/b;

    if-eqz v0, :cond_18a

    .line 1599
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    .line 1600
    check-cast p1, Lax/b;

    .line 1601
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lax/b;)V

    goto/16 :goto_97

    .line 1602
    :cond_18a
    instance-of v0, p1, Lcom/google/googlenav/aa;

    if-eqz v0, :cond_1c0

    .line 1604
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    .line 1606
    check-cast p1, Lcom/google/googlenav/aa;

    .line 1608
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->k()Z

    move-result v0

    if-nez v0, :cond_97

    .line 1609
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->l()Lcom/google/googlenav/Y;

    move-result-object v0

    .line 1610
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->x()Lbf/bU;

    move-result-object v1

    .line 1612
    if-eqz v1, :cond_1b5

    .line 1613
    invoke-virtual {v1}, Lbf/bU;->ax()Z

    move-result v0

    if-nez v0, :cond_1b0

    .line 1614
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, v1}, Lbf/am;->e(Lbf/i;)V

    .line 1616
    :cond_1b0
    invoke-virtual {v1, p1}, Lbf/bU;->a(Lcom/google/googlenav/aa;)V

    goto/16 :goto_97

    .line 1618
    :cond_1b5
    invoke-virtual {p1}, Lcom/google/googlenav/aa;->i()Z

    move-result v1

    if-nez v1, :cond_97

    .line 1619
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/Y;)Lbf/bU;

    goto/16 :goto_97

    .line 1623
    :cond_1c0
    instance-of v0, p1, Lcom/google/googlenav/bW;

    if-eqz v0, :cond_1e1

    .line 1625
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    .line 1626
    check-cast p1, Lcom/google/googlenav/bW;

    .line 1627
    invoke-virtual {p1}, Lcom/google/googlenav/bW;->i()Lcom/google/googlenav/bN;

    move-result-object v0

    .line 1628
    invoke-virtual {v0}, Lcom/google/googlenav/bN;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1dc

    .line 1629
    invoke-virtual {p1}, Lcom/google/googlenav/bW;->k()V

    goto/16 :goto_97

    .line 1631
    :cond_1dc
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bN;)Lbf/bH;

    goto/16 :goto_97

    .line 1633
    :cond_1e1
    instance-of v0, p1, Lcom/google/googlenav/cg;

    if-eqz v0, :cond_97

    .line 1634
    check-cast p1, Lcom/google/googlenav/cg;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/cg;)V

    goto/16 :goto_97
.end method

.method public a(Lax/b;)V
    .registers 5
    .parameter

    .prologue
    .line 2900
    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_40

    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 2901
    invoke-virtual {p1}, Lax/b;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->f()LaN/B;

    move-result-object v0

    .line 2902
    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    .line 2903
    if-eqz v0, :cond_40

    if-eqz v1, :cond_40

    invoke-virtual {v0, v1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 2904
    new-instance v0, LaN/B;

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-direct {v0, v2, v1}, LaN/B;-><init>(II)V

    .line 2905
    invoke-virtual {p1}, Lax/b;->as()Lax/y;

    move-result-object v1

    .line 2906
    invoke-virtual {v1, v0}, Lax/y;->a(LaN/B;)Lax/y;

    move-result-object v0

    invoke-virtual {p1, v0}, Lax/b;->b(Lax/y;)V

    .line 2910
    :cond_40
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lax/k;)V

    .line 2911
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lax/k;)V

    .line 2914
    invoke-virtual {p1}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_61

    const/4 v0, 0x0

    .line 2918
    :goto_4d
    if-eqz v0, :cond_66

    .line 2922
    invoke-virtual {p1}, Lax/b;->aN()I

    move-result v1

    invoke-virtual {v0, v1}, Lax/b;->v(I)V

    .line 2923
    invoke-virtual {p1}, Lax/b;->aP()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->b(Ljava/lang/String;)V

    .line 2924
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->b(Lax/b;)V

    .line 2932
    :cond_60
    :goto_60
    return-void

    .line 2914
    :cond_61
    invoke-static {p1}, Lbf/O;->a(Lax/b;)Lax/b;

    move-result-object v0

    goto :goto_4d

    .line 2927
    :cond_66
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    .line 2929
    invoke-virtual {p1}, Lax/b;->E()Z

    move-result v0

    if-nez v0, :cond_60

    .line 2930
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->j(Lax/b;)V

    goto :goto_60
.end method

.method public a(Lax/b;B)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4896
    invoke-virtual {p1, p2}, Lax/b;->a(B)V

    .line 4897
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lax/b;)Lbf/O;

    .line 4898
    return-void
.end method

.method public a(Lax/b;Ljava/util/List;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 5257
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v2

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v3

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v4

    move-object v1, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/b;LaN/H;IILjava/util/List;)V

    .line 5260
    return-void
.end method

.method public a(Lax/k;)V
    .registers 3
    .parameter

    .prologue
    .line 5336
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->aj()Lcom/google/googlenav/A;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/A;->a(Lax/k;)V

    .line 5337
    return-void
.end method

.method public a(Lax/w;)V
    .registers 3
    .parameter

    .prologue
    .line 5308
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->p(Z)V

    .line 5309
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lax/w;)Lbf/bK;

    .line 5310
    return-void
.end method

.method public a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5298
    invoke-static {p3}, LO/b;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/b;

    move-result-object v0

    invoke-static {p1, p2, v0, p4}, Lcom/google/android/maps/driveabout/app/bn;->a(Lax/y;I[LO/b;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5300
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;)V

    .line 5301
    return-void
.end method

.method public a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5190
    new-instance v0, Lax/j;

    invoke-direct {v0, p1, p2, p3}, Lax/j;-><init>(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 5192
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/j;)V

    .line 5193
    return-void
.end method

.method public a(Lbf/aU;)V
    .registers 3
    .parameter

    .prologue
    .line 4955
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p0, p1}, Lbf/am;->b(Lcom/google/googlenav/J;Lbf/aU;)V

    .line 4956
    return-void
.end method

.method public a(Lbf/i;)V
    .registers 3
    .parameter

    .prologue
    .line 4987
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->h(Lbf/i;)V

    .line 4988
    return-void
.end method

.method public a(Lcom/google/android/maps/A;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1159
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aA:Z

    if-eqz v0, :cond_44

    .line 1161
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aS()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1296
    :cond_b
    :goto_b
    return-void

    .line 1172
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 1173
    if-eqz v0, :cond_32

    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    if-eqz v1, :cond_32

    .line 1174
    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    .line 1178
    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v2

    .line 1179
    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v3

    .line 1180
    invoke-virtual {v0}, Lbf/i;->aX()V

    .line 1183
    invoke-virtual {v0, v2}, Lbf/i;->b(B)V

    .line 1184
    invoke-interface {v1, v3}, Lcom/google/googlenav/F;->a(I)V

    .line 1185
    invoke-virtual {v0}, Lbf/i;->aW()V

    .line 1190
    :cond_32
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1191
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v1

    if-nez v1, :cond_b

    .line 1192
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->d()V

    goto :goto_b

    .line 1203
    :cond_44
    iput v1, p0, Lcom/google/googlenav/ui/s;->I:I

    .line 1204
    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    .line 1205
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    .line 1209
    const/4 v0, 0x1

    .line 1210
    const/4 v1, 0x0

    .line 1211
    const/4 v2, 0x1

    .line 1212
    :try_start_53
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v3, v0, v1, v2}, Lbf/am;->a(ZZZ)V

    .line 1217
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/N;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/N;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1228
    sget-object v0, Lcom/google/android/maps/A;->b:Lcom/google/android/maps/A;

    if-eq p1, v0, :cond_7a

    .line 1233
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/O;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/O;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1240
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/P;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/P;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1251
    :cond_7a
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/Q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/Q;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1258
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/u;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 1281
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aS()Z

    .line 1282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->aA:Z
    :try_end_94
    .catch Ljava/lang/RuntimeException; {:try_start_53 .. :try_end_94} :catch_96

    goto/16 :goto_b

    .line 1283
    :catch_96
    move-exception v0

    .line 1289
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    const-string v2, ""

    invoke-interface {v1, v2}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    .line 1291
    const-string v1, "GmmController-loadSavedState"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1294
    throw v0
.end method

.method public a(Lcom/google/googlenav/aZ;IZ)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 3174
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-nez v0, :cond_11

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 3248
    :cond_11
    :goto_11
    return-void

    .line 3178
    :cond_12
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 3179
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-static {v0, v1, v2, v3, v4}, LaH/d;->a(Ljava/lang/String;ILaN/B;ILas/c;)V

    .line 3182
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    .line 3183
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 3186
    if-eqz p3, :cond_145

    const-string v0, "a"

    .line 3188
    :goto_44
    new-array v1, v10, [Ljava/lang/String;

    const-string v2, "s=pr"

    aput-object v2, v1, v8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aG:Lbm/i;

    invoke-virtual {v2}, Lbm/i;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v7

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3194
    const-string v1, "stat"

    invoke-static {v7, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3199
    if-eqz p3, :cond_13c

    .line 3200
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, p2, v6}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 3204
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3206
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v0

    .line 3207
    const-string v2, "19"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_bd

    .line 3213
    const-string v1, ""

    .line 3218
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gk;->B()I

    move-result v2

    if-ne v2, v7, :cond_bd

    .line 3220
    const-string v0, "26"

    .line 3224
    :cond_bd
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_149

    const-string v2, "rf"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v2

    .line 3227
    :goto_d1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v3

    if-eqz v3, :cond_e1

    const-string v3, "ro"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/googlenav/aZ;->b(Ljava/lang/String;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v6

    .line 3231
    :cond_e1
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "q="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->z()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    const-string v0, "c="

    aput-object v0, v3, v10

    const/4 v0, 0x4

    aput-object v2, v3, v0

    const/4 v0, 0x5

    aput-object v6, v3, v0

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3237
    invoke-static {v7, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 3241
    :cond_13c
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v0}, Lcom/google/googlenav/A;->c()V

    .line 3247
    iput-boolean v8, p0, Lcom/google/googlenav/ui/s;->ap:Z

    goto/16 :goto_11

    .line 3186
    :cond_145
    const-string v0, "p"

    goto/16 :goto_44

    :cond_149
    move-object v2, v6

    .line 3224
    goto :goto_d1
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 3135
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;)V

    .line 3136
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;BZ)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3482
    const/4 v2, 0x1

    .line 3483
    const/4 v4, 0x0

    .line 3484
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    move-object v1, p1

    move v3, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;

    .line 3486
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 3519
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    .line 3520
    if-eqz v0, :cond_b

    .line 3521
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    .line 3523
    :cond_b
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/s;->b(Ljava/util/List;Lcom/google/googlenav/bf;)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 3524
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v3}, Lbf/am;->H()Lbf/i;

    move-result-object v3

    invoke-direct {v2, p2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 3526
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v0}, Lbf/am;->b(Lcom/google/googlenav/aZ;)Lbf/x;

    .line 3527
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    .line 3529
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v0}, Lcom/google/googlenav/A;->d()V

    .line 3530
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;ILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3672
    invoke-virtual {p0, p1, p2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;I)V

    .line 3675
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    invoke-virtual {v0}, Lcom/google/googlenav/A;->d()V

    .line 3676
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2, p3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    .line 3677
    return-void
.end method

.method public a(Lcom/google/googlenav/bf;)V
    .registers 5
    .parameter

    .prologue
    .line 3151
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0, p1}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    .line 3152
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v1

    .line 3153
    if-eqz v1, :cond_10

    .line 3154
    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(LaN/B;)Lcom/google/googlenav/bg;

    .line 3157
    :cond_10
    new-instance v1, Lcom/google/googlenav/aZ;

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    .line 3158
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->c(Z)V

    .line 3159
    iget v0, p1, Lcom/google/googlenav/bf;->j:I

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    .line 3160
    return-void
.end method

.method public a(Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/R;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 1383
    const/4 v3, 0x0

    .line 1384
    const/4 v4, 0x0

    .line 1387
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    move-object v1, p0

    move-object v2, p1

    move v6, v5

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V

    .line 1389
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/as;)V
    .registers 2
    .parameter

    .prologue
    .line 1049
    iput-object p1, p0, Lcom/google/googlenav/ui/s;->t:Lcom/google/googlenav/ui/as;

    .line 1050
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/b;)V
    .registers 2
    .parameter

    .prologue
    .line 5511
    iput-object p1, p0, Lcom/google/googlenav/ui/s;->au:Lcom/google/googlenav/ui/b;

    .line 5512
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 4806
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/Object;B)V

    .line 4807
    return-void
.end method

.method public a(Ljava/lang/Object;B)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4933
    check-cast p1, Lcom/google/googlenav/Y;

    .line 4934
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbf/am;->a(Lcom/google/googlenav/Y;Z)Lbf/bU;

    move-result-object v0

    .line 4935
    invoke-virtual {p1}, Lcom/google/googlenav/Y;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lbf/bU;->b(I)V

    .line 4936
    invoke-virtual {v0, p2}, Lbf/bU;->a(B)V

    .line 4937
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5074
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aw:Z

    if-eqz v0, :cond_9

    .line 5075
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/aD;->b(Ljava/lang/String;)V

    .line 5077
    :cond_9
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5228
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;I)V

    .line 5229
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3694
    new-instance v0, Lcom/google/googlenav/ui/B;

    invoke-direct {v0, p0, p4, p2, p3}, Lcom/google/googlenav/ui/B;-><init>(Lcom/google/googlenav/ui/s;ZILjava/lang/String;)V

    .line 3708
    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 3709
    const/16 v0, 0x1af

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 3711
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    .line 3712
    return-void
.end method

.method public a(Ljava/lang/String;LaN/B;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 5220
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;LaN/B;)V

    .line 5221
    return-void
.end method

.method public a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3141
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 3142
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/ai;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const-wide/16 v2, -0x1

    .line 2822
    :try_start_2
    new-instance v0, Lcom/google/googlenav/H;

    invoke-direct {v0, p0}, Lcom/google/googlenav/H;-><init>(Lcom/google/googlenav/ui/s;)V

    .line 2823
    invoke-static {p1}, Lcom/google/googlenav/H;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2825
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/H;->a(Ljava/lang/String;Z)V

    .line 2857
    :goto_11
    return-void

    .line 2828
    :cond_12
    if-eqz p2, :cond_91

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    .line 2829
    :goto_18
    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v4

    .line 2830
    const-string v5, "http:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2c

    const-string v5, "https:"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_99

    .line 2832
    :cond_2c
    const/16 v5, 0x37

    const-string v6, "u"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "url="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_93

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_61
    aput-object v0, v7, v8

    const/4 v1, 0x2

    if-eqz v4, :cond_96

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_79
    aput-object v0, v7, v1

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2851
    :goto_82
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0, p1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V
    :try_end_87
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_87} :catch_88

    goto :goto_11

    .line 2853
    :catch_88
    move-exception v0

    .line 2855
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_11

    :cond_91
    move-wide v0, v2

    .line 2828
    goto :goto_18

    .line 2832
    :cond_93
    :try_start_93
    const-string v0, ""

    goto :goto_61

    :cond_96
    const-string v0, ""

    goto :goto_79

    .line 2842
    :cond_99
    const/16 v5, 0x37

    const-string v6, "x"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "url="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_f0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_ce
    aput-object v0, v7, v8

    const/4 v1, 0x2

    if-eqz v4, :cond_f3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_e6
    aput-object v0, v7, v1

    invoke-static {v7}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v6, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_82

    :cond_f0
    const-string v0, ""

    goto :goto_ce

    :cond_f3
    const-string v0, ""
    :try_end_f5
    .catch Ljava/lang/Throwable; {:try_start_93 .. :try_end_f5} :catch_88

    goto :goto_e6
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 3252
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v1, 0x259

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    .line 3260
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3464
    new-instance v1, Lcom/google/googlenav/W;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/googlenav/W;-><init>(Ljava/lang/String;Ljava/lang/String;LaN/B;Ljava/lang/String;)V

    .line 3467
    const/4 v4, 0x1

    .line 3468
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v3, 0x2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lbf/am;->a(Lcom/google/googlenav/ai;ZBZZ)Lbf/ak;

    .line 3470
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2860
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2861
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5163
    if-eqz p4, :cond_9

    .line 5164
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 5166
    :cond_9
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 5170
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->e()Z

    .line 5174
    :goto_1a
    return-void

    .line 5172
    :cond_1b
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1a
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1358
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    .line 1359
    return-void
.end method

.method public a(Ljava/lang/String;ZI)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3495
    new-instance v0, Lcom/google/googlenav/ui/z;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/googlenav/ui/z;-><init>(Lcom/google/googlenav/ui/s;ZI)V

    .line 3511
    new-instance v2, Lcom/google/googlenav/f;

    invoke-direct {v2, v0, p1}, Lcom/google/googlenav/f;-><init>(Lcom/google/googlenav/g;Ljava/lang/String;)V

    .line 3512
    const/16 v0, 0x1af

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 3514
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Law/h;->c(Law/g;)V

    .line 3515
    return-void
.end method

.method public a(Lo/S;)V
    .registers 3
    .parameter

    .prologue
    .line 4716
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lo/S;)V

    .line 4717
    return-void
.end method

.method public a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 4295
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    .line 4298
    if-eqz v1, :cond_27

    .line 4299
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GmmController.handleOutOfMemory("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 4300
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aT()V

    .line 4306
    :cond_27
    if-eqz p1, :cond_2a

    .line 4327
    :goto_29
    return-void

    .line 4310
    :cond_2a
    const/16 v0, 0x36a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 4312
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v2

    if-eqz v2, :cond_5a

    .line 4313
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->Q()V

    .line 4314
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5c0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 4326
    :cond_56
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    goto :goto_29

    .line 4316
    :cond_5a
    if-nez v1, :cond_56

    goto :goto_29
.end method

.method public a(ZI)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3373
    if-eqz p1, :cond_e

    sget-object v0, Lcom/google/googlenav/ui/s;->aE:Lbm/i;

    .line 3375
    :goto_4
    invoke-virtual {v0}, Lbm/i;->a()V

    .line 3376
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/s;->c(I)V

    .line 3377
    invoke-virtual {v0}, Lbm/i;->b()V

    .line 3378
    return-void

    .line 3373
    :cond_e
    sget-object v0, Lcom/google/googlenav/ui/s;->aF:Lbm/i;

    goto :goto_4
.end method

.method public a(ZZ)V
    .registers 13
    .parameter
    .parameter

    .prologue
    const/16 v9, 0xd

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 3870
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->w()Z

    move-result v1

    if-nez v1, :cond_f

    .line 3927
    :goto_e
    return-void

    .line 3876
    :cond_f
    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->P:Z

    .line 3878
    if-eqz p1, :cond_a9

    .line 3880
    if-eqz p2, :cond_89

    .line 3889
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_b0

    .line 3890
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0, v8}, Lbf/i;->c(Z)I

    move-result v1

    .line 3891
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->q()I

    move-result v0

    .line 3893
    :goto_31
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->c()LaN/B;

    move-result-object v2

    .line 3894
    iget-object v3, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/ak;->j()LaN/B;

    move-result-object v3

    .line 3895
    if-eqz v3, :cond_89

    .line 3898
    iget-object v4, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/B;->c()I

    move-result v5

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v6

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-virtual {v4, v5, v6, v1, v0}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v0

    .line 3902
    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v1

    if-le v1, v9, :cond_69

    .line 3903
    invoke-static {v9}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    .line 3907
    :cond_69
    new-instance v1, LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v4

    invoke-virtual {v3}, LaN/B;->c()I

    move-result v5

    add-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v1, v4, v2}, LaN/B;-><init>(II)V

    .line 3911
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2, v1, v0}, LaN/u;->e(LaN/B;LaN/Y;)V

    .line 3914
    :cond_89
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->q()Lbf/bE;

    .line 3919
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_a5

    .line 3921
    const/16 v0, 0x2c1

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 3923
    :cond_a5
    iput-boolean v8, p0, Lcom/google/googlenav/ui/s;->ap:Z

    goto/16 :goto_e

    .line 3925
    :cond_a9
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->r()V

    goto/16 :goto_e

    :cond_b0
    move v1, v0

    goto :goto_31
.end method

.method public a(I)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3746
    packed-switch p1, :pswitch_data_28

    .line 3759
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    if-nez v2, :cond_12

    .line 3772
    :cond_b
    :goto_b
    return v0

    .line 3748
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    if-eq v2, v1, :cond_b

    .line 3765
    :cond_12
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0, p1}, LaN/u;->a(I)V

    .line 3769
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->V()V

    .line 3770
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    move v0, v1

    .line 3772
    goto :goto_b

    .line 3753
    :pswitch_1f
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->b()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_12

    goto :goto_b

    .line 3746
    nop

    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_c
        :pswitch_1f
    .end packed-switch
.end method

.method public a(ILat/a;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2570
    packed-switch p1, :pswitch_data_4c

    .line 2601
    :pswitch_5
    if-eqz p2, :cond_a

    .line 2603
    packed-switch p1, :pswitch_data_5e

    .line 2609
    :cond_a
    :goto_a
    :pswitch_a
    return v0

    :pswitch_b
    move v0, v1

    .line 2576
    goto :goto_a

    .line 2578
    :pswitch_d
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aK()V

    move v0, v1

    .line 2579
    goto :goto_a

    .line 2581
    :pswitch_12
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->o()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 2582
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    .line 2586
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_2c

    .line 2587
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lbf/i;->a(B)V

    :cond_2c
    move v0, v1

    .line 2589
    goto :goto_a

    .line 2591
    :pswitch_2e
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->o()Lcom/google/googlenav/ui/wizard/A;

    .line 2592
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;Z)V

    move v0, v1

    .line 2593
    goto :goto_a

    .line 2596
    :pswitch_3c
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->o()Lcom/google/googlenav/ui/wizard/A;

    .line 2597
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->Y()V

    move v0, v1

    .line 2598
    goto :goto_a

    .line 2605
    :pswitch_46
    invoke-virtual {p0, p2}, Lcom/google/googlenav/ui/s;->c(Lat/a;)Z

    move v0, v1

    .line 2606
    goto :goto_a

    .line 2570
    nop

    :pswitch_data_4c
    .packed-switch 0x1
        :pswitch_d
        :pswitch_12
        :pswitch_b
        :pswitch_a
        :pswitch_5
        :pswitch_2e
        :pswitch_3c
    .end packed-switch

    .line 2603
    :pswitch_data_5e
    .packed-switch 0x5
        :pswitch_46
    .end packed-switch
.end method

.method public a(LaD/q;)Z
    .registers 3
    .parameter

    .prologue
    .line 2719
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/p;->a(LaD/q;)Z

    move-result v0

    return v0
.end method

.method public a(Lat/a;)Z
    .registers 4
    .parameter

    .prologue
    .line 2337
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->d(Lat/a;)Z

    move-result v0

    .line 2338
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    .line 2341
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V

    .line 2342
    return v0
.end method

.method public aA()Lcom/google/googlenav/offers/a;
    .registers 2

    .prologue
    .line 5868
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    return-object v0
.end method

.method public aB()Lav/a;
    .registers 2

    .prologue
    .line 5872
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ag:Lav/a;

    return-object v0
.end method

.method public aC()Z
    .registers 2

    .prologue
    .line 5922
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/r;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public aa()Z
    .registers 2

    .prologue
    .line 4536
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    return v0
.end method

.method public ab()Lcom/google/googlenav/android/aa;
    .registers 2

    .prologue
    .line 4754
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method public ac()Lcom/google/googlenav/mylocationnotifier/k;
    .registers 2

    .prologue
    .line 4763
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ai:Lcom/google/googlenav/mylocationnotifier/k;

    return-object v0
.end method

.method public ad()Lcom/google/googlenav/aA;
    .registers 2

    .prologue
    .line 4767
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    return-object v0
.end method

.method public ae()Lcom/google/googlenav/j;
    .registers 2

    .prologue
    .line 4771
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->F:Lcom/google/googlenav/j;

    return-object v0
.end method

.method public af()Lcom/google/googlenav/ui/p;
    .registers 2

    .prologue
    .line 4791
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    return-object v0
.end method

.method public ag()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    .line 4901
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v3

    .line 4902
    const/4 v2, 0x0

    .line 4903
    const/4 v1, 0x0

    .line 4905
    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v3

    sparse-switch v3, :sswitch_data_5a

    move v0, v1

    move-object v1, v2

    .line 4927
    :cond_12
    :goto_12
    if-eqz v1, :cond_17

    .line 4928
    invoke-virtual {v1, v0}, Lbf/i;->a(B)V

    .line 4930
    :cond_17
    return-void

    .line 4907
    :sswitch_18
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 4908
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->v()Lbf/O;

    move-result-object v1

    .line 4910
    if-eqz v1, :cond_12

    .line 4912
    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v2

    invoke-virtual {v1}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Lcom/google/googlenav/F;->a(I)V

    goto :goto_12

    .line 4916
    :sswitch_37
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    .line 4917
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->w()Lbf/bK;

    move-result-object v2

    .line 4919
    if-eqz v2, :cond_57

    .line 4922
    invoke-virtual {v2}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-virtual {v2}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Lcom/google/googlenav/F;->a(I)V

    move-object v1, v2

    goto :goto_12

    :cond_57
    move v0, v1

    move-object v1, v2

    goto :goto_12

    .line 4905
    :sswitch_data_5a
    .sparse-switch
        0x4 -> :sswitch_18
        0x1b -> :sswitch_37
    .end sparse-switch
.end method

.method public ah()Lcom/google/googlenav/ui/wizard/C;
    .registers 2

    .prologue
    .line 5059
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    return-object v0
.end method

.method public ai()Z
    .registers 2

    .prologue
    .line 5066
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->s()Z

    move-result v0

    return v0
.end method

.method public aj()Lcom/google/googlenav/A;
    .registers 2

    .prologue
    .line 5331
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aa:Lcom/google/googlenav/A;

    return-object v0
.end method

.method public ak()Las/c;
    .registers 2

    .prologue
    .line 5340
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    return-object v0
.end method

.method public al()Lbf/am;
    .registers 2

    .prologue
    .line 5344
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    return-object v0
.end method

.method public am()LaR/n;
    .registers 2

    .prologue
    .line 5357
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->f()LaR/n;

    move-result-object v0

    return-object v0
.end method

.method public an()LaR/n;
    .registers 2

    .prologue
    .line 5362
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->g()LaR/n;

    move-result-object v0

    return-object v0
.end method

.method public ao()Lcom/google/googlenav/ui/R;
    .registers 2

    .prologue
    .line 5366
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->an:Lcom/google/googlenav/ui/R;

    return-object v0
.end method

.method public ap()Lcom/google/googlenav/ui/wizard/z;
    .registers 2

    .prologue
    .line 5370
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->af:Lcom/google/googlenav/ui/wizard/z;

    return-object v0
.end method

.method public aq()Lcom/google/googlenav/ui/bE;
    .registers 2

    .prologue
    .line 5378
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->u:Lcom/google/googlenav/ui/bE;

    return-object v0
.end method

.method public ar()V
    .registers 3

    .prologue
    .line 5391
    invoke-static {}, Lcom/google/googlenav/common/Config;->e()Ljava/lang/String;

    move-result-object v0

    .line 5393
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ar:Ljava/lang/String;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ar:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    .line 5394
    :cond_10
    invoke-static {}, Lcom/google/googlenav/ui/l;->a()V

    .line 5395
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->d()V

    .line 5396
    invoke-static {}, Lax/o;->b()V

    .line 5397
    invoke-static {}, Lcom/google/googlenav/ui/wizard/gk;->C()V

    .line 5398
    iput-object v0, p0, Lcom/google/googlenav/ui/s;->ar:Ljava/lang/String;

    .line 5400
    :cond_22
    return-void
.end method

.method public as()Z
    .registers 5

    .prologue
    .line 5421
    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_16

    iget-wide v0, p0, Lcom/google/googlenav/ui/s;->Y:J

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/googlenav/ui/s;->Z:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public at()V
    .registers 4

    .prologue
    .line 5447
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->au()V

    .line 5451
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2a

    .line 5453
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 5456
    :cond_2a
    return-void
.end method

.method public au()V
    .registers 2

    .prologue
    .line 5459
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->p(Z)V

    .line 5460
    return-void
.end method

.method public av()Lcom/google/googlenav/ui/wizard/jv;
    .registers 2

    .prologue
    .line 5500
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method public aw()Lcom/google/googlenav/friend/j;
    .registers 2

    .prologue
    .line 5823
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method public ax()Z
    .registers 2

    .prologue
    .line 5842
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    .line 5843
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->g()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    .line 5845
    :cond_12
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public ay()V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 5849
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-static {v0, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 5850
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1}, Lbf/am;->x()Lbf/bU;

    move-result-object v1

    .line 5851
    if-eqz v1, :cond_22

    .line 5852
    invoke-virtual {v1}, Lbf/bU;->ax()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 5853
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, v1}, Lbf/am;->e(Lbf/i;)V

    .line 5855
    :cond_1e
    invoke-virtual {v1}, Lbf/bU;->d()V

    .line 5861
    :goto_21
    return-void

    .line 5857
    :cond_22
    invoke-virtual {p0, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;)Lcom/google/googlenav/aa;

    move-result-object v2

    .line 5858
    const/16 v0, 0x4c7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    goto :goto_21
.end method

.method public az()Z
    .registers 2

    .prologue
    .line 5864
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->O()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->N()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public b()I
    .registers 2

    .prologue
    .line 3741
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->c()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .registers 4
    .parameter

    .prologue
    .line 1369
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lbf/am;I)V

    .line 1370
    return-void
.end method

.method public b(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 4262
    :try_start_0
    iput p1, p0, Lcom/google/googlenav/ui/s;->n:I

    .line 4263
    iput p2, p0, Lcom/google/googlenav/ui/s;->o:I

    .line 4264
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget v1, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v2}, LaN/p;->d(II)V

    .line 4265
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    iget v1, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v2}, LaN/u;->c(II)V

    .line 4266
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/s;->n:I

    iget v2, p0, Lcom/google/googlenav/ui/s;->o:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->b(II)V

    .line 4267
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->V()V

    .line 4268
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aP()V

    .line 4277
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    if-eqz v0, :cond_30

    .line 4278
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/p;->c(II)V
    :try_end_30
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_30} :catch_31

    .line 4283
    :cond_30
    :goto_30
    return-void

    .line 4280
    :catch_31
    move-exception v0

    .line 4281
    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_30
.end method

.method public b(LaN/B;)V
    .registers 3
    .parameter

    .prologue
    .line 5212
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(LaN/B;)V

    .line 5213
    return-void
.end method

.method public b(Law/g;)V
    .registers 2
    .parameter

    .prologue
    .line 1698
    invoke-interface {p1}, Law/g;->u_()V

    .line 1699
    return-void
.end method

.method public b(Lax/b;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 2938
    const/4 v0, 0x4

    const-string v1, "rs"

    invoke-virtual {p1}, Lax/b;->y()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2944
    invoke-static {}, Lax/o;->a()Lax/o;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aF()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/o;->a([I)V

    .line 2948
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->i(Lax/b;)V

    .line 2950
    invoke-virtual {p1}, Lax/b;->y()I

    move-result v0

    if-ne v0, v3, :cond_44

    .line 2951
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 2952
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    .line 2954
    :cond_3e
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->c(Lax/b;)V

    .line 3013
    :goto_43
    return-void

    .line 2955
    :cond_44
    invoke-virtual {p1}, Lax/b;->z()Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 2956
    invoke-virtual {p1}, Lax/b;->k()I

    move-result v0

    invoke-virtual {p1, v0}, Lax/b;->u(I)V

    .line 2960
    invoke-virtual {p1}, Lax/b;->A()Z

    move-result v0

    if-eqz v0, :cond_9b

    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 2961
    invoke-virtual {p1}, Lax/b;->aO()Lax/b;

    move-result-object v0

    .line 2965
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->Z()Z

    move-result v1

    if-eqz v1, :cond_7a

    .line 2966
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/bN;->a()V

    .line 2971
    :cond_7a
    const/16 v1, 0x5bf

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lax/b;->aQ()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lax/b;->aQ()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lax/b;->b(Ljava/lang/String;)V

    .line 2974
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lax/b;)V

    goto :goto_43

    .line 2976
    :cond_9b
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->c(Lax/b;)V

    goto :goto_43

    .line 2980
    :cond_9f
    invoke-virtual {p1}, Lax/b;->m()Z

    move-result v0

    if-eqz v0, :cond_cb

    .line 2985
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->i()V

    .line 2986
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/b;)V

    .line 3008
    :cond_af
    :goto_af
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aw()LaN/B;

    move-result-object v1

    invoke-interface {v0, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    .line 3010
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v0

    invoke-virtual {p1}, Lax/b;->aC()LaN/B;

    move-result-object v1

    invoke-interface {v0, v1, v4, v5}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->a(LaN/B;ILjava/lang/String;)V

    goto/16 :goto_43

    .line 2990
    :cond_cb
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_e4

    .line 2991
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    .line 2993
    :cond_e4
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Y()Z

    move-result v0

    if-eqz v0, :cond_fd

    .line 2994
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->a()V

    .line 2997
    :cond_fd
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->e(Lax/b;)Lbf/O;

    move-result-object v0

    .line 3001
    if-eqz v0, :cond_af

    .line 3002
    invoke-virtual {v0, p1}, Lbf/O;->c(Lcom/google/googlenav/F;)V

    goto :goto_af
.end method

.method public b(Lax/k;)V
    .registers 3
    .parameter

    .prologue
    .line 5126
    instance-of v0, p1, Lax/b;

    if-eqz v0, :cond_c

    .line 5127
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    check-cast p1, Lax/b;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    .line 5132
    :goto_b
    return-void

    .line 5129
    :cond_c
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    check-cast p1, Lax/j;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lax/j;)V

    goto :goto_b
.end method

.method public b(Lbf/aU;)V
    .registers 3
    .parameter

    .prologue
    .line 4963
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lbf/aU;)V

    .line 4964
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 4815
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/s;->b(Ljava/lang/Object;B)V

    .line 4816
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5084
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aw:Z

    if-eqz v0, :cond_9

    .line 5085
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->w:Lcom/google/googlenav/ui/aD;

    invoke-interface {v0, p1}, Lcom/google/googlenav/ui/aD;->a(Ljava/lang/String;)V

    .line 5087
    :cond_9
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 3938
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-nez v0, :cond_b

    .line 3947
    :goto_a
    return-void

    .line 3942
    :cond_b
    if-eqz p1, :cond_11

    .line 3943
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->L()V

    goto :goto_a

    .line 3945
    :cond_11
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_a
.end method

.method public b(Lat/a;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 2396
    iget-boolean v1, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v1, :cond_6

    .line 2434
    :cond_5
    :goto_5
    return v0

    .line 2400
    :cond_6
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aJ()V

    .line 2402
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2408
    :cond_15
    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v0

    if-nez v0, :cond_20

    .line 2411
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->Q:Lcom/google/googlenav/ui/W;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/W;->a(Lat/a;)V

    .line 2414
    :cond_20
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->e(Lat/a;)V

    .line 2416
    const/4 v0, 0x0

    .line 2419
    :try_start_24
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->f(Lat/a;)Z

    move-result v0

    .line 2421
    invoke-virtual {p1}, Lat/a;->d()Z

    move-result v1

    if-nez v1, :cond_3e

    .line 2424
    iput-object p1, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    .line 2425
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/googlenav/ui/s;->L:J

    .line 2429
    :cond_3e
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->q()V
    :try_end_41
    .catch Ljava/lang/OutOfMemoryError; {:try_start_24 .. :try_end_41} :catch_42

    goto :goto_5

    .line 2430
    :catch_42
    move-exception v1

    .line 2431
    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_5
.end method

.method public c(I)V
    .registers 3
    .parameter

    .prologue
    .line 3381
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->d(I)V

    .line 3382
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->L()V

    .line 3383
    return-void
.end method

.method public c(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 5495
    invoke-virtual {p1}, Lax/b;->t()V

    .line 5496
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->e(Lax/b;)V

    .line 5497
    return-void
.end method

.method public c(Lbf/aU;)V
    .registers 4
    .parameter

    .prologue
    .line 4971
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, v1, p1}, Lbf/am;->a(Lcom/google/googlenav/ui/wizard/jv;Lbf/aU;)V

    .line 4973
    return-void
.end method

.method public c(Ljava/lang/Object;)V
    .registers 6
    .parameter

    .prologue
    .line 4871
    check-cast p1, Lcom/google/googlenav/bl;

    .line 4872
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Lbf/am;->a(Lcom/google/googlenav/bl;BZ[Ljava/lang/String;)Lbf/C;

    .line 4873
    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 5837
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ac:LaR/b;

    invoke-virtual {v0, p1}, LaR/b;->a(Z)V

    .line 5838
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 3777
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    return v0
.end method

.method c(Lat/a;)Z
    .registers 3
    .parameter

    .prologue
    .line 2551
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->g(Lat/a;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2552
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->h(Lat/a;)Z

    move-result v0

    .line 2554
    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x1

    goto :goto_a
.end method

.method public c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 3952
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c_(Z)V
    .registers 4
    .parameter

    .prologue
    .line 3782
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v0}, LaN/u;->f()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->g()Z

    move-result v0

    if-ne p1, v0, :cond_d

    .line 3787
    :goto_c
    return-void

    .line 3786
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1}, LaN/p;->b()LaN/H;

    move-result-object v1

    invoke-virtual {v1, p1}, LaN/H;->a(Z)LaN/H;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/H;)V

    goto :goto_c
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/ag;
    .registers 4
    .parameter

    .prologue
    .line 3589
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->m(Ljava/lang/String;)LaR/H;

    move-result-object v0

    .line 3590
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->a(LaR/H;)Lcom/google/googlenav/ai;

    move-result-object v1

    .line 3594
    if-nez v1, :cond_15

    .line 3595
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->l(Ljava/lang/String;)V

    .line 3597
    if-nez v0, :cond_12

    sget-object v0, Lcom/google/googlenav/ag;->a:Lcom/google/googlenav/ag;

    .line 3601
    :goto_11
    return-object v0

    .line 3597
    :cond_12
    sget-object v0, Lcom/google/googlenav/ag;->b:Lcom/google/googlenav/ag;

    goto :goto_11

    .line 3599
    :cond_15
    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/s;->b(Lcom/google/googlenav/ai;)V

    .line 3601
    sget-object v0, Lcom/google/googlenav/ag;->c:Lcom/google/googlenav/ag;

    goto :goto_11
.end method

.method public d(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 5139
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->c(Lax/b;)V

    .line 5140
    return-void
.end method

.method public d(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 4881
    check-cast p1, Ljava/lang/String;

    .line 4882
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/16 v1, 0x14a

    invoke-virtual {v0, v1, p1}, Lbf/am;->a(ILjava/lang/Object;)V

    .line 4883
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->L()V

    .line 4884
    return-void
.end method

.method public d(Z)V
    .registers 2
    .parameter

    .prologue
    .line 1147
    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->aA:Z

    .line 1148
    return-void
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 3850
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->P:Z

    return v0
.end method

.method public e(Lax/b;)Lbf/O;
    .registers 3
    .parameter

    .prologue
    .line 3813
    invoke-static {}, Lbf/O;->bq()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 3814
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->b(Lax/b;)Lbf/O;

    move-result-object v0

    .line 3816
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0, p1}, Lbf/am;->a(Lax/b;)Lbf/O;

    move-result-object v0

    goto :goto_c
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/ai;
    .registers 3
    .parameter

    .prologue
    .line 3584
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/s;->m(Ljava/lang/String;)LaR/H;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/s;->a(LaR/H;)Lcom/google/googlenav/ai;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/Object;)V
    .registers 4
    .parameter

    .prologue
    .line 4887
    check-cast p1, Ljava/lang/String;

    .line 4888
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Ljava/lang/String;Z)V

    .line 4889
    return-void
.end method

.method public e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 3326
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v0

    invoke-virtual {v0}, LaS/a;->j()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 3330
    :goto_a
    return-void

    .line 3329
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aC:Lcom/google/googlenav/ui/ac;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/ac;->a(Z)V

    goto :goto_a
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 3932
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->B()Lbf/X;

    move-result-object v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public f()V
    .registers 2

    .prologue
    .line 3840
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->U()V

    .line 3841
    return-void
.end method

.method public f(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 4892
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->d(Lax/b;)V

    .line 4893
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3016
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    .line 3017
    new-instance v1, Lcom/google/googlenav/cg;

    invoke-direct {v1, p1}, Lcom/google/googlenav/cg;-><init>(Ljava/lang/String;)V

    .line 3018
    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 3019
    return-void
.end method

.method public f(Z)V
    .registers 5
    .parameter

    .prologue
    .line 3392
    if-eqz p1, :cond_15

    .line 3393
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->c(Ljava/lang/String;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    .line 3394
    if-eqz v0, :cond_14

    .line 3395
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lbf/am;->b(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    .line 3400
    :cond_14
    :goto_14
    return-void

    .line 3398
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    const-string v2, "LayerTransit"

    invoke-virtual {v1, v2}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/am;->h(Lbf/i;)V

    goto :goto_14
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 5570
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 5573
    const/4 v0, 0x6

    new-array v3, v0, [Lcom/google/googlenav/common/util/n;

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->S:Lcom/google/googlenav/ui/p;

    aput-object v0, v3, v1

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    if-eqz v0, :cond_43

    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->a()LaN/D;

    move-result-object v0

    :goto_17
    aput-object v0, v3, v4

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x4

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x5

    iget-object v4, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    aput-object v4, v3, v0

    move v0, v1

    .line 5575
    :goto_32
    array-length v4, v3

    if-ge v0, v4, :cond_45

    .line 5576
    aget-object v4, v3, v0

    .line 5579
    if-eqz v4, :cond_40

    .line 5580
    invoke-interface {v4}, Lcom/google/googlenav/common/util/n;->g()Lcom/google/googlenav/common/util/l;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5575
    :cond_40
    add-int/lit8 v0, v0, 0x1

    goto :goto_32

    .line 5573
    :cond_43
    const/4 v0, 0x0

    goto :goto_17

    .line 5585
    :cond_45
    new-instance v0, Lcom/google/googlenav/common/util/l;

    const-string v3, "GMM memory usage"

    invoke-direct {v0, v3, v1, v2}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;ILjava/util/List;)V

    return-object v0
.end method

.method public g(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 4941
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Lax/b;)V

    .line 4942
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 3387
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    .line 3388
    return-void
.end method

.method public g(Z)V
    .registers 4
    .parameter

    .prologue
    .line 3796
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->P()Z

    move-result v0

    if-ne p1, v0, :cond_7

    .line 3800
    :goto_6
    return-void

    .line 3799
    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v1}, LaN/p;->b()LaN/H;

    move-result-object v1

    invoke-virtual {v1, p1}, LaN/H;->b(Z)LaN/H;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/H;)V

    goto :goto_6
.end method

.method public h()Lcom/google/googlenav/E;
    .registers 2

    .prologue
    .line 3845
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->V()Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method

.method public h(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 4946
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->g()Lcom/google/googlenav/ui/wizard/bI;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bI;->a(Lax/b;)V

    .line 4947
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 4693
    new-instance v0, Lcom/google/googlenav/aZ;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/aZ;-><init>(Lcom/google/googlenav/bf;LaN/u;)V

    .line 4697
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 4698
    return-void
.end method

.method public h(Z)V
    .registers 3
    .parameter

    .prologue
    .line 4005
    const-string v0, "GmmController.showNotify"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 4006
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->p:Lat/a;

    .line 4009
    const/4 v0, 0x1

    :try_start_9
    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->m:Z

    .line 4010
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/s;->l(Z)V

    .line 4014
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->U()V
    :try_end_11
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_11} :catch_17

    .line 4018
    :goto_11
    const-string v0, "GmmController.showNotify"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 4019
    return-void

    .line 4015
    :catch_17
    move-exception v0

    .line 4016
    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    goto :goto_11
.end method

.method public i()V
    .registers 6

    .prologue
    .line 1315
    new-instance v0, Lcom/google/googlenav/layer/k;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v2}, LaN/u;->a()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->b()I

    move-result v3

    const-string v4, "__LAYERS"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/layer/k;-><init>(LaN/H;IILjava/lang/String;)V

    .line 1318
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 1321
    new-instance v1, Lcom/google/googlenav/ui/w;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/w;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/k;->a(Lcom/google/googlenav/layer/l;)V

    .line 1328
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5147
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 5148
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;)V

    .line 5149
    return-void
.end method

.method public i(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 4348
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-eqz v0, :cond_6

    .line 4403
    :goto_5
    return-void

    .line 4352
    :cond_6
    iput-boolean v1, p0, Lcom/google/googlenav/ui/s;->j:Z

    .line 4354
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    invoke-virtual {v0}, Las/c;->e()V

    .line 4355
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    if-eqz v0, :cond_18

    .line 4356
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->v:Lcom/google/googlenav/ui/bF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bF;->e()V

    .line 4358
    :cond_18
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->S()V

    .line 4359
    if-eqz p1, :cond_20

    .line 4360
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/s;->j(Z)V

    .line 4381
    :cond_20
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->O:Lcom/google/googlenav/ui/X;

    invoke-interface {v0}, Lcom/google/googlenav/ui/X;->c()V

    .line 4382
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0, p1}, LaN/p;->a(Z)V

    .line 4383
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->c()V

    .line 4384
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual {v0}, LaN/V;->a()V

    .line 4385
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->E:LaN/k;

    invoke-virtual {v0}, LaN/k;->d()V

    .line 4389
    invoke-static {}, Lcom/google/googlenav/friend/E;->i()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 4390
    invoke-static {}, Lcom/google/googlenav/friend/E;->e()Lcom/google/googlenav/friend/E;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/E;->b(LaM/h;)V

    .line 4392
    :cond_46
    invoke-static {}, Lcom/google/googlenav/friend/as;->i()Z

    move-result v0

    if-eqz v0, :cond_53

    .line 4393
    invoke-static {}, Lcom/google/googlenav/friend/as;->e()Lcom/google/googlenav/friend/as;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/as;->b(LaM/h;)V

    .line 4395
    :cond_53
    invoke-static {}, Lcom/google/googlenav/friend/W;->i()Z

    move-result v0

    if-eqz v0, :cond_60

    .line 4396
    invoke-static {}, Lcom/google/googlenav/friend/W;->e()Lcom/google/googlenav/friend/W;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/W;->b(LaM/h;)V

    .line 4398
    :cond_60
    invoke-static {}, Lcom/google/googlenav/bm;->b()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 4399
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    .line 4402
    :cond_6d
    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    goto :goto_5
.end method

.method public j()Lbf/by;
    .registers 2

    .prologue
    .line 1343
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->y()Lbf/by;

    move-result-object v0

    return-object v0
.end method

.method public j(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5806
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->j()Lbf/by;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbf/by;->d(Ljava/lang/String;)V

    .line 5807
    return-void
.end method

.method public j(Z)V
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4413
    if-eqz p1, :cond_1a

    .line 4417
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/m;->h()V

    .line 4418
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bh;->h()V

    .line 4421
    :cond_1a
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v2}, Lbf/am;->L()V

    .line 4426
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    invoke-virtual {v3}, LaN/u;->f()LaN/H;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/H;)V

    .line 4427
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v2}, LaN/p;->f()V

    .line 4429
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v2

    if-eqz v2, :cond_3c

    .line 4430
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v2

    invoke-virtual {v2}, LaR/l;->n()V

    .line 4433
    :cond_3c
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v2

    if-eqz v2, :cond_4d

    .line 4434
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/aC;->g()Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/C;->b()V

    .line 4438
    :cond_4d
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    const-string v3, "TRAFFIC_ON"

    new-array v4, v0, [B

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->d()Z

    move-result v5

    if-eqz v5, :cond_6d

    :goto_59
    int-to-byte v0, v0

    aput-byte v0, v4, v1

    invoke-interface {v2, v3, v4}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    .line 4441
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    invoke-static {v0}, Lbf/bU;->a(Lcom/google/googlenav/common/io/j;)V

    .line 4442
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->s:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    .line 4444
    invoke-static {}, Lbm/m;->d()V

    .line 4445
    return-void

    :cond_6d
    move v0, v1

    .line 4438
    goto :goto_59
.end method

.method public k()LaB/o;
    .registers 4

    .prologue
    .line 5349
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    if-nez v0, :cond_f

    .line 5350
    new-instance v0, LaB/o;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-direct {v0, v1, v2}, LaB/o;-><init>(Lcom/google/googlenav/android/aa;Las/c;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    .line 5352
    :cond_f
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aB:LaB/o;

    return-object v0
.end method

.method public k(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 5815
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->j()Lbf/by;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbf/by;->a(Ljava/lang/String;Z)V

    .line 5816
    return-void
.end method

.method public declared-synchronized k(Z)V
    .registers 3
    .parameter

    .prologue
    .line 4460
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->j:Z

    if-nez v0, :cond_43

    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    if-nez v0, :cond_43

    .line 4461
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aE()V

    .line 4463
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    .line 4464
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    if-eqz v0, :cond_18

    .line 4465
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual {v0}, LaN/V;->d()V

    .line 4467
    :cond_18
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->w()V

    .line 4470
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 4471
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->q()V

    .line 4473
    :cond_2a
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->h()V

    .line 4475
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aO()V

    .line 4486
    if-eqz p1, :cond_37

    .line 4487
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->au()V

    .line 4490
    :cond_37
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aM()V

    .line 4492
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    if-eqz v0, :cond_43

    .line 4493
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->b()V
    :try_end_43
    .catchall {:try_start_1 .. :try_end_43} :catchall_45

    .line 4496
    :cond_43
    monitor-exit p0

    return-void

    .line 4460
    :catchall_45
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()Lcom/google/googlenav/L;
    .registers 2

    .prologue
    .line 4759
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ah:Lcom/google/googlenav/L;

    return-object v0
.end method

.method public declared-synchronized l(Z)V
    .registers 6
    .parameter

    .prologue
    .line 4596
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/googlenav/ui/s;->ax:Ljava/lang/Boolean;

    .line 4597
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    if-eqz v0, :cond_96

    .line 4598
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->O()V

    .line 4606
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/s;->I:I

    .line 4607
    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    .line 4608
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;)V

    .line 4610
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->y()V

    .line 4612
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aJ()V

    .line 4613
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->m:Z

    .line 4614
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/s;->i:Z

    .line 4615
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    if-eqz v0, :cond_2f

    .line 4616
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->D:LaN/V;

    invoke-virtual {v0}, LaN/V;->c()V

    .line 4620
    :cond_2f
    if-nez p1, :cond_3c

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 4621
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->C:LaH/m;

    invoke-interface {v0}, LaH/m;->k()V

    .line 4625
    :cond_3c
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 4626
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->r()V

    .line 4629
    :cond_49
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->B:LaN/p;

    invoke-virtual {v0}, LaN/p;->i()V

    .line 4632
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aO()V

    .line 4633
    const/4 v0, 0x0

    .line 4634
    new-instance v1, Lcom/google/googlenav/ui/C;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/googlenav/ui/C;-><init>(Lcom/google/googlenav/ui/s;Las/c;Lcom/google/googlenav/android/aa;Z)V

    iput-object v1, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    .line 4641
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Las/d;->c(J)V

    .line 4642
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->k:Las/d;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Las/d;->a(J)V

    .line 4643
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    new-instance v1, Lcom/google/googlenav/ui/D;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/D;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Lcom/google/googlenav/x;)V

    .line 4658
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->x()V

    .line 4660
    const/4 v0, 0x0

    .line 4661
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/E;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/E;-><init>(Lcom/google/googlenav/ui/s;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 4669
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aS()Z

    .line 4671
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aL()V

    .line 4673
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    if-eqz v0, :cond_96

    .line 4674
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->d:Lcom/google/googlenav/offers/a;

    invoke-virtual {v0}, Lcom/google/googlenav/offers/a;->a()V

    .line 4679
    :cond_96
    sget-object v0, Lcom/google/googlenav/z;->e:Lcom/google/googlenav/z;

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ab:Las/c;

    invoke-static {v0, v1}, Lcom/google/googlenav/u;->a(Lcom/google/googlenav/z;Las/c;)V
    :try_end_9d
    .catchall {:try_start_2 .. :try_end_9d} :catchall_9f

    .line 4680
    monitor-exit p0

    return-void

    .line 4596
    :catchall_9f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public m()Lbf/a;
    .registers 2

    .prologue
    .line 5832
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->aa()Lbf/a;

    move-result-object v0

    return-object v0
.end method

.method public m(Z)V
    .registers 3
    .parameter

    .prologue
    .line 4997
    if-nez p1, :cond_b

    .line 4998
    const/16 v0, 0x2a4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 5000
    :cond_b
    return-void
.end method

.method public n()Lcom/google/googlenav/ui/as;
    .registers 2

    .prologue
    .line 3318
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->t:Lcom/google/googlenav/ui/as;

    return-object v0
.end method

.method public n(Z)V
    .registers 4
    .parameter

    .prologue
    .line 5320
    if-eqz p1, :cond_7

    .line 5325
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->L()V

    .line 5327
    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-static {p1}, Lcom/google/android/maps/driveabout/app/bn;->a(Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/aA;->b(Ljava/lang/String;)V

    .line 5328
    return-void
.end method

.method public o()Lbf/i;
    .registers 2

    .prologue
    .line 5927
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    return-object v0
.end method

.method public o(Z)V
    .registers 10
    .parameter

    .prologue
    const-wide/16 v6, 0x1f4

    .line 5409
    iput-boolean p1, p0, Lcom/google/googlenav/ui/s;->W:Z

    .line 5410
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    .line 5411
    if-eqz p1, :cond_18

    .line 5412
    const-wide/16 v2, 0x5dc

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->H:J

    .line 5418
    :cond_17
    :goto_17
    return-void

    .line 5414
    :cond_18
    iget-wide v2, p0, Lcom/google/googlenav/ui/s;->H:J

    add-long v4, v0, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_17

    .line 5415
    add-long/2addr v0, v6

    iput-wide v0, p0, Lcom/google/googlenav/ui/s;->H:J

    goto :goto_17
.end method

.method public p()Lcom/google/googlenav/ui/ak;
    .registers 2

    .prologue
    .line 5374
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->b:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method public p(Z)V
    .registers 3
    .parameter

    .prologue
    .line 5471
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->a()V

    .line 5474
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 5475
    if-eqz v0, :cond_e

    .line 5476
    invoke-virtual {v0}, Lbf/i;->Z()V

    .line 5480
    :cond_e
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 5481
    if-eqz v0, :cond_19

    .line 5482
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    .line 5486
    :cond_19
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->r:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->f()V

    .line 5488
    if-eqz p1, :cond_27

    .line 5489
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    .line 5491
    :cond_27
    return-void
.end method

.method public q()V
    .registers 3

    .prologue
    .line 4727
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 4733
    sget-object v0, Lcom/google/googlenav/z;->a:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->c(Lcom/google/googlenav/z;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 4751
    :goto_12
    return-void

    .line 4739
    :cond_13
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aH()V

    .line 4742
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 4743
    invoke-direct {p0}, Lcom/google/googlenav/ui/s;->aG()Z

    move-result v1

    if-eqz v1, :cond_3e

    iget-object v1, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    if-nez v1, :cond_3e

    if-eqz v0, :cond_3e

    invoke-virtual {v0}, Lbf/i;->aa()Z

    move-result v1

    if-nez v1, :cond_39

    invoke-virtual {v0}, Lbf/i;->av()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3e

    .line 4745
    :cond_39
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->F()Z

    .line 4747
    :cond_3e
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->G()V

    goto :goto_12

    .line 4749
    :cond_44
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->g:Lcom/google/googlenav/ui/android/L;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/L;->q()V

    goto :goto_12
.end method

.method public q(Z)V
    .registers 6
    .parameter

    .prologue
    .line 5881
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 5882
    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    .line 5884
    :cond_d
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    .line 5886
    new-instance v1, Lcom/google/googlenav/ui/I;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/I;-><init>(Lcom/google/googlenav/ui/s;Z)V

    new-instance v2, Lcom/google/googlenav/ui/J;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/J;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/android/maps/MapsActivity;)V

    new-instance v3, LaT/h;

    invoke-direct {v3}, LaT/h;-><init>()V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/MapsActivity;->activateAreaSelector(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)Lcom/google/googlenav/ui/android/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/s;->aJ:Lcom/google/googlenav/ui/android/r;

    .line 5919
    return-void
.end method

.method public r()Lcom/google/googlenav/friend/ag;
    .registers 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->z:Lcom/google/googlenav/friend/ag;

    return-object v0
.end method

.method public s()V
    .registers 5

    .prologue
    .line 1064
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1114
    :goto_a
    return-void

    .line 1067
    :cond_b
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    .line 1069
    new-instance v1, Lcom/google/googlenav/ui/R;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/R;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/t;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/s;->an:Lcom/google/googlenav/ui/R;

    .line 1070
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->an:Lcom/google/googlenav/ui/R;

    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 1072
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->i()Z

    move-result v1

    if-eqz v1, :cond_69

    .line 1073
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/friend/E;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/E;

    move-result-object v1

    .line 1078
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ae:Lbf/am;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(Lbf/am;)V

    .line 1080
    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 1081
    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/E;->a(LaM/h;)V

    .line 1082
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v3, Lcom/google/googlenav/friend/s;

    invoke-direct {v3, v1}, Lcom/google/googlenav/friend/s;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    .line 1084
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->y:Lcom/google/googlenav/friend/p;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(LaM/h;)V

    .line 1087
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    if-eqz v2, :cond_51

    .line 1088
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->A:Lcom/google/googlenav/friend/j;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/friend/E;->a(LaM/h;)V

    .line 1091
    :cond_51
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/friend/as;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/as;

    move-result-object v1

    .line 1093
    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 1094
    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/as;->a(LaM/h;)V

    .line 1095
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v3, Lcom/google/googlenav/friend/ar;

    invoke-direct {v3, v1}, Lcom/google/googlenav/friend/ar;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    .line 1100
    :cond_69
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->j()Z

    move-result v1

    if-eqz v1, :cond_8b

    .line 1101
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/friend/W;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/friend/W;

    move-result-object v1

    .line 1103
    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 1104
    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/W;->a(LaM/h;)V

    .line 1105
    iget-object v2, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v3, Lcom/google/googlenav/friend/V;

    invoke-direct {v3, v1}, Lcom/google/googlenav/friend/V;-><init>(Lcom/google/googlenav/friend/bi;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    .line 1109
    :cond_8b
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->h:Lcom/google/googlenav/android/aa;

    iget-object v2, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-static {v1, v2, p0}, Lcom/google/googlenav/bm;->a(Lcom/google/googlenav/android/aa;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)Lcom/google/googlenav/bm;

    move-result-object v1

    .line 1111
    invoke-virtual {v0, v1}, LaM/f;->e(LaM/g;)V

    .line 1112
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->aI:Lcom/google/googlenav/friend/a;

    new-instance v2, Lcom/google/googlenav/friend/bu;

    invoke-direct {v2, v1}, Lcom/google/googlenav/friend/bu;-><init>(Lcom/google/googlenav/bm;)V

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/a;->a(Lcom/google/googlenav/friend/b;)V

    goto/16 :goto_a
.end method

.method public t()LaN/u;
    .registers 2

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->av:LaN/u;

    return-object v0
.end method

.method public u()V
    .registers 2

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;)V

    .line 1140
    return-void
.end method

.method public v()Z
    .registers 2

    .prologue
    .line 1151
    iget-boolean v0, p0, Lcom/google/googlenav/ui/s;->aA:Z

    return v0
.end method

.method public w()V
    .registers 3

    .prologue
    .line 1304
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    .line 1309
    iget-object v1, p0, Lcom/google/googlenav/ui/s;->F:Lcom/google/googlenav/j;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/j;->a([I)V

    .line 1310
    return-void

    .line 1304
    :array_c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public x()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .registers 2

    .prologue
    .line 1348
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->az:Lcom/google/googlenav/ui/aC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/aC;->h()Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    return-object v0
.end method

.method public y()V
    .registers 2

    .prologue
    .line 1363
    iget-object v0, p0, Lcom/google/googlenav/ui/s;->ad:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ui/s;)V

    .line 1364
    return-void
.end method

.method public z()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 1405
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aA()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1412
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    .line 1417
    if-nez v1, :cond_1d

    invoke-virtual {p0}, Lcom/google/googlenav/ui/s;->A()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 1418
    const/4 v0, 0x0

    .line 1426
    :cond_1c
    :goto_1c
    return v0

    .line 1421
    :cond_1d
    if-eqz v1, :cond_27

    .line 1422
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->s()V

    goto :goto_1c

    .line 1424
    :cond_27
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->g()V

    goto :goto_1c
.end method
