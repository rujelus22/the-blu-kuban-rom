.class public Lcom/google/googlenav/ui/view/android/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field final A:Z

.field final B:Lcom/google/googlenav/ai;

.field C:Z

.field final D:Z

.field final E:Lcom/google/googlenav/ui/e;

.field final F:I

.field final G:Z

.field final H:I

.field final I:Z

.field J:Z

.field final K:Ljava/lang/CharSequence;

.field final L:Ljava/lang/CharSequence;

.field final M:I

.field final N:Z

.field final O:Ljava/lang/CharSequence;

.field final P:Ljava/lang/String;

.field final Q:I

.field public final R:I

.field private final S:Lcom/google/googlenav/ui/br;

.field final b:Ljava/lang/CharSequence;

.field final c:[Ljava/lang/CharSequence;

.field final d:[Ljava/lang/CharSequence;

.field final e:[Ljava/lang/CharSequence;

.field final f:Ljava/lang/CharSequence;

.field final g:Z

.field final h:Ljava/lang/CharSequence;

.field final i:Ljava/lang/String;

.field final j:Ljava/lang/CharSequence;

.field final k:Ljava/lang/CharSequence;

.field final l:Ljava/lang/CharSequence;

.field final m:Ljava/lang/CharSequence;

.field final n:Ljava/lang/CharSequence;

.field final o:Ljava/lang/CharSequence;

.field final p:Ljava/lang/CharSequence;

.field final q:Ljava/lang/String;

.field final r:Ljava/lang/CharSequence;

.field final s:Ljava/lang/CharSequence;

.field final t:Ljava/lang/CharSequence;

.field final u:Lcom/google/googlenav/ui/bs;

.field final v:Lcom/google/googlenav/ui/bs;

.field final w:Lam/f;

.field final x:Lcom/google/googlenav/ui/view/a;

.field final y:Lan/f;

.field final z:LaN/B;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/android/bs;->a:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/view/android/bt;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->E:Lcom/google/googlenav/ui/e;

    .line 154
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->b(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/br;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    .line 156
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    .line 157
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->d(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    .line 158
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->e(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->d:[Ljava/lang/CharSequence;

    .line 159
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->f(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->e:[Ljava/lang/CharSequence;

    .line 160
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->g(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    .line 161
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->h(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->g:Z

    .line 162
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->i(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->h:Ljava/lang/CharSequence;

    .line 163
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->j(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->i:Ljava/lang/String;

    .line 164
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->k(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->j:Ljava/lang/CharSequence;

    .line 165
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->l(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->k:Ljava/lang/CharSequence;

    .line 166
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->m(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    .line 167
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->b(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    .line 168
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->n(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    .line 169
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->o(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    .line 170
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->p(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ac;

    move-result-object v0

    invoke-static {v0}, Lbf/aS;->a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    .line 171
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->q(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->q:Ljava/lang/String;

    .line 172
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->q(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->aW:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lbf/aS;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->r:Ljava/lang/CharSequence;

    .line 174
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->a(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    .line 175
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    .line 176
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/bs;->d(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->u:Lcom/google/googlenav/ui/bs;

    .line 177
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->r(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ap;

    move-result-object v0

    invoke-static {v0}, Lbf/m;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    .line 178
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->s(Lcom/google/googlenav/ui/view/android/bt;)Lam/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->w:Lam/f;

    .line 179
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->t(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->x:Lcom/google/googlenav/ui/view/a;

    .line 180
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->u(Lcom/google/googlenav/ui/view/android/bt;)Lan/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->y:Lan/f;

    .line 181
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->v(Lcom/google/googlenav/ui/view/android/bt;)LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->z:LaN/B;

    .line 182
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->w(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->A:Z

    .line 183
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->x(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->B:Lcom/google/googlenav/ai;

    .line 184
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->y(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    .line 185
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->z(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->D:Z

    .line 186
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->A(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    .line 187
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->B(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->R:I

    .line 188
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->C(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->F:I

    .line 189
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->D(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->G:Z

    .line 190
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->E(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->H:I

    .line 191
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->F(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->I:Z

    .line 192
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->G(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->J:Z

    .line 194
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_144

    const/4 v0, 0x1

    :goto_fe
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/bs;->a(Lcom/google/googlenav/ui/view/android/bt;Z)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->K:Ljava/lang/CharSequence;

    .line 195
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->J:Z

    if-eqz v0, :cond_146

    move-object v0, v2

    :goto_109
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->L:Ljava/lang/CharSequence;

    .line 197
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bs;->M:I

    .line 198
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->I(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->N:Z

    .line 200
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;

    move-result-object v0

    if-eqz v0, :cond_14f

    .line 201
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 202
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ar;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->aV:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    .line 205
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    .line 206
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ar;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    .line 211
    :goto_143
    return-void

    :cond_144
    move v0, v1

    .line 194
    goto :goto_fe

    .line 195
    :cond_146
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bC;->a(Ljava/lang/String;Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_109

    .line 208
    :cond_14f
    iput-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    .line 209
    iput-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    goto :goto_143
.end method

.method private a(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 4
    .parameter

    .prologue
    .line 214
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->z(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 215
    const/16 v0, 0x3bf

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_19

    .line 219
    :goto_18
    return-object v0

    .line 217
    :cond_19
    const/4 v0, 0x0

    goto :goto_18

    .line 219
    :cond_1b
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->K(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_18
.end method

.method private a(Lcom/google/googlenav/ui/view/android/bt;Z)Ljava/lang/CharSequence;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 224
    if-nez p2, :cond_f

    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->J:Z

    if-nez v0, :cond_f

    .line 225
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->L(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ui/bC;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 232
    :goto_e
    return-object v0

    .line 228
    :cond_f
    const/4 v0, 0x0

    .line 229
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1e

    .line 230
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    :cond_1e
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->F(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v1

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->L(Lcom/google/googlenav/ui/view/android/bt;)I

    move-result v2

    invoke-static {v1, v2, v0}, Lcom/google/googlenav/ui/bC;->a(ZILjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_e
.end method

.method private a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 642
    .line 643
    if-eqz p1, :cond_2f

    .line 644
    array-length v4, p1

    move v3, v2

    move v0, v2

    :goto_6
    if-ge v3, v4, :cond_1e

    aget-object v5, p1, v3

    .line 645
    if-nez v5, :cond_10

    .line 644
    :goto_c
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_6

    .line 648
    :cond_10
    add-int/lit8 v1, v0, 0x1

    aget-object v0, p2, v0

    .line 649
    if-eqz v0, :cond_1c

    .line 650
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 651
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1c
    move v0, v1

    goto :goto_c

    .line 655
    :cond_1e
    :goto_1e
    array-length v1, p2

    if-ge v0, v1, :cond_2e

    .line 656
    add-int/lit8 v1, v0, 0x1

    aget-object v0, p2, v0

    .line 657
    if-eqz v0, :cond_2c

    .line 658
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2c
    move v0, v1

    .line 660
    goto :goto_1e

    .line 661
    :cond_2e
    return-void

    :cond_2f
    move v0, v2

    goto :goto_1e
.end method

.method private b(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 237
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->z(Lcom/google/googlenav/ui/view/android/bt;)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 238
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 239
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->N(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_19

    .line 240
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->N(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 242
    :cond_19
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_31

    .line 243
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_2a

    .line 244
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 246
    :cond_2a
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 248
    :cond_31
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_38

    .line 252
    :goto_37
    return-object v0

    :cond_38
    move-object v0, v1

    .line 248
    goto :goto_37

    .line 250
    :cond_3a
    const/16 v0, 0x3bf

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 252
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4d

    move-object v1, v0

    :cond_4d
    move-object v0, v1

    goto :goto_37
.end method

.method private c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 257
    sget-object v1, Lcom/google/googlenav/ui/aV;->bw:Lcom/google/googlenav/ui/aV;

    .line 258
    sget-object v3, Lcom/google/googlenav/ui/aV;->bv:Lcom/google/googlenav/ui/aV;

    .line 260
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    if-eqz v0, :cond_66

    .line 261
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->a()[Lcom/google/googlenav/an;

    move-result-object v4

    .line 262
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 263
    const/4 v0, 0x0

    :goto_19
    array-length v6, v4

    if-ge v0, v6, :cond_42

    .line 264
    aget-object v6, v4, v0

    iget-object v6, v6, Lcom/google/googlenav/an;->b:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    .line 266
    aget-object v6, v4, v0

    iget-object v6, v6, Lcom/google/googlenav/an;->a:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    .line 268
    aget-object v6, v4, v0

    iget-object v6, v6, Lcom/google/googlenav/an;->c:Ljava/lang/String;

    invoke-static {v5, v6, v3, v1}, Lcom/google/googlenav/ui/aX;->a(Ljava/util/Vector;Ljava/lang/String;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    .line 270
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_3f

    .line 271
    const-string v6, " "

    invoke-static {v6, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 263
    :cond_3f
    add-int/lit8 v0, v0, 0x1

    goto :goto_19

    .line 275
    :cond_42
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 276
    invoke-virtual {v5}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    .line 277
    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_4b

    .line 279
    :cond_5b
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_64

    move-object v0, v1

    :goto_62
    move-object v2, v0

    .line 296
    :cond_63
    :goto_63
    return-object v2

    :cond_64
    move-object v0, v2

    .line 279
    goto :goto_62

    .line 280
    :cond_66
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->P(Lcom/google/googlenav/ui/view/android/bt;)Ljava/util/Vector;

    move-result-object v0

    if-eqz v0, :cond_63

    .line 285
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->P(Lcom/google/googlenav/ui/view/android/bt;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_74
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/al;

    .line 286
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 287
    invoke-static {v0, v4, v3, v3}, Lbf/aS;->a(Lcom/google/googlenav/al;Ljava/util/Vector;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)Z

    move-result v0

    if-eqz v0, :cond_74

    .line 288
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 289
    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_94
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    .line 290
    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    goto :goto_94

    .line 292
    :cond_a4
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_63

    move-object v2, v1

    goto :goto_63
.end method

.method private d(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/bs;
    .registers 5
    .parameter

    .prologue
    .line 300
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    if-eqz v0, :cond_20

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aq;->c()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 301
    new-instance v0, Lcom/google/googlenav/ui/bs;

    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/bt;->O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aq;->b()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    .line 304
    :goto_1f
    return-object v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 552
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bs;->H:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 3
    .parameter

    .prologue
    .line 557
    new-instance v0, Lcom/google/googlenav/ui/view/android/bv;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/bv;-><init>()V

    .line 559
    invoke-virtual {p0, v0, p1}, Lcom/google/googlenav/ui/view/android/bs;->a(Lcom/google/googlenav/ui/view/android/bv;Landroid/view/View;)V

    .line 561
    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 665
    check-cast p2, Lcom/google/googlenav/ui/view/android/bv;

    .line 666
    invoke-virtual {p2}, Lcom/google/googlenav/ui/view/android/bv;->a()V

    .line 669
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    iget v3, v3, Lcom/google/googlenav/ui/view/android/bw;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1d

    .line 670
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    iget v4, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    iput v4, v3, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 671
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->a:Landroid/view/View;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 681
    :cond_1d
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->b:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 687
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    if-eqz v3, :cond_4f

    .line 688
    iget-boolean v3, p0, Lcom/google/googlenav/ui/view/android/bs;->A:Z

    if-eqz v3, :cond_193

    .line 689
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 690
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    iget-boolean v4, p0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 693
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iget v4, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    iput v4, v3, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 694
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iput-object p0, v3, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    .line 695
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->B:Lcom/google/googlenav/ai;

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bw;->c:Lcom/google/googlenav/ai;

    .line 696
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 703
    :cond_4f
    :goto_4f
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/view/android/bs;->a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V

    .line 704
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->d:[Ljava/lang/CharSequence;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/view/android/bs;->a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V

    .line 705
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->e:[Ljava/lang/CharSequence;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    invoke-direct {p0, v3, v4}, Lcom/google/googlenav/ui/view/android/bs;->a([Ljava/lang/CharSequence;[Landroid/widget/TextView;)V

    .line 708
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 709
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->O:Ljava/lang/CharSequence;

    if-eqz v3, :cond_80

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    if-eqz v3, :cond_80

    .line 710
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bu;->a:Ljava/lang/String;

    .line 711
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->g:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 716
    :cond_80
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->j:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 717
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    if-eqz v3, :cond_a0

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    if-eqz v3, :cond_a0

    .line 718
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bx;->a:Ljava/lang/String;

    .line 719
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->j:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 724
    :cond_a0
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->k:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->h:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 725
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->h:Ljava/lang/CharSequence;

    if-eqz v3, :cond_bc

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v3, :cond_bc

    .line 726
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->i:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    .line 727
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->k:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    invoke-static {v3, v4}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 732
    :cond_bc
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->l:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->j:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 733
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->m:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->k:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 736
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->n:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 739
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->o:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 743
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->p:Landroid/view/View;

    if-eqz v3, :cond_f3

    .line 744
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->p:Landroid/view/View;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    if-nez v4, :cond_19a

    :goto_e2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 745
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->q:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 746
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 749
    :cond_f3
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 752
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->t:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->r:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 753
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->r:Ljava/lang/CharSequence;

    if-eqz v0, :cond_116

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    if-eqz v0, :cond_116

    .line 754
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->q:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/googlenav/ui/view/android/bz;->a:Ljava/lang/String;

    .line 755
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->t:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 760
    :cond_116
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->u:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    invoke-static {v0, v3}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 765
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->w:Lam/f;

    if-eqz v0, :cond_19d

    .line 766
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->w:Lam/f;

    check-cast v0, Lan/f;

    .line 780
    :cond_125
    :goto_125
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    invoke-static {v3, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    .line 781
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->A:Landroid/widget/ImageView;

    invoke-static {v3, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    .line 783
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    if-eqz v0, :cond_144

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    if-eqz v0, :cond_144

    .line 784
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->x:Lcom/google/googlenav/ui/view/a;

    iput-object v3, v0, Lcom/google/googlenav/ui/view/android/by;->a:Lcom/google/googlenav/ui/view/a;

    .line 785
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 789
    :cond_144
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->h:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->i:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->z:LaN/B;

    invoke-static {v0, v3, v4}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    .line 792
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->u:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_1db

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->u:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 794
    :goto_15b
    iget-object v2, p2, Lcom/google/googlenav/ui/view/android/bv;->w:Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    .line 797
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->v:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 800
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_172

    .line 801
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->x:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->y:Lan/f;

    invoke-static {v0, v2}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    .line 804
    :cond_172
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_187

    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    if-eqz v0, :cond_187

    .line 807
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    iget v2, p0, Lcom/google/googlenav/ui/view/android/bs;->Q:I

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 808
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    iget-object v2, p2, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 812
    :cond_187
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->B:Lcom/google/googlenav/ui/bD;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bs;->L:Ljava/lang/CharSequence;

    iget v3, p0, Lcom/google/googlenav/ui/view/android/bs;->M:I

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bs;->K:Ljava/lang/CharSequence;

    invoke-static {v0, v2, v3, v4, v1}, Lcom/google/googlenav/ui/bC;->a(Lcom/google/googlenav/ui/bD;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Z)V

    .line 814
    return-void

    .line 698
    :cond_193
    iget-object v3, p2, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_4f

    :cond_19a
    move v0, v1

    .line 744
    goto/16 :goto_e2

    .line 767
    :cond_19d
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/bv;->A:Landroid/widget/ImageView;

    if-eqz v0, :cond_1cb

    .line 770
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_1e1

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v3}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v0

    if-eqz v0, :cond_1e1

    .line 771
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    .line 773
    :goto_1bd
    if-nez v0, :cond_125

    .line 774
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->r()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    goto/16 :goto_125

    .line 776
    :cond_1cb
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    if-eqz v0, :cond_1de

    .line 777
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->S:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->v:Lcom/google/googlenav/ui/bs;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/br;->a(Lcom/google/googlenav/ui/ab;)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    goto/16 :goto_125

    :cond_1db
    move-object v0, v2

    .line 792
    goto/16 :goto_15b

    :cond_1de
    move-object v0, v2

    goto/16 :goto_125

    :cond_1e1
    move-object v0, v2

    goto :goto_1bd
.end method

.method protected a(Lcom/google/googlenav/ui/view/android/bv;Landroid/view/View;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 565
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->N:Z

    if-eqz v0, :cond_b

    .line 566
    invoke-static {p2}, Lbf/aS;->a(Landroid/view/View;)V

    .line 568
    :cond_b
    iput-object p2, p1, Lcom/google/googlenav/ui/view/android/bv;->a:Landroid/view/View;

    .line 570
    const v0, 0x7f10001e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->b:Landroid/widget/TextView;

    .line 571
    const v0, 0x7f100305

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->c:Landroid/widget/CheckBox;

    .line 573
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    const v0, 0x7f10019f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 574
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    const v0, 0x7f1001a0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    .line 575
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->d:[Landroid/widget/TextView;

    const v0, 0x7f100308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v4

    .line 577
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    const v0, 0x7f100306

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 578
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->e:[Landroid/widget/TextView;

    const v0, 0x7f100307

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    .line 580
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    const v0, 0x7f10030a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 582
    iget-object v1, p1, Lcom/google/googlenav/ui/view/android/bv;->f:[Landroid/widget/TextView;

    const v0, 0x7f10030b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v3

    .line 585
    const v0, 0x7f1002b3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->g:Landroid/widget/TextView;

    .line 587
    new-instance v0, Lcom/google/googlenav/ui/view/android/bu;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->P:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bu;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->F:Lcom/google/googlenav/ui/view/android/bu;

    .line 590
    const v0, 0x7f10019b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->h:Lcom/google/googlenav/ui/view/android/DistanceView;

    .line 591
    const v0, 0x7f10019c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->i:Lcom/google/googlenav/ui/view/android/HeadingView;

    .line 593
    const v0, 0x7f10030c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->j:Landroid/widget/TextView;

    .line 594
    const v0, 0x7f10030d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->k:Landroid/widget/TextView;

    .line 595
    const v0, 0x7f10030e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->l:Landroid/widget/TextView;

    .line 596
    const v0, 0x7f10030f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->m:Landroid/widget/TextView;

    .line 598
    const v0, 0x7f100262

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->n:Landroid/widget/TextView;

    .line 599
    const v0, 0x7f100310

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->o:Landroid/widget/TextView;

    .line 601
    const v0, 0x7f10007a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->p:Landroid/view/View;

    .line 602
    const v0, 0x7f100313

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->q:Landroid/widget/TextView;

    .line 603
    const v0, 0x7f100314

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->r:Landroid/widget/TextView;

    .line 604
    const v0, 0x7f10007d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->s:Landroid/widget/TextView;

    .line 605
    const v0, 0x7f100311

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->t:Landroid/widget/TextView;

    .line 606
    const v0, 0x7f10007e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->u:Landroid/widget/TextView;

    .line 608
    const v0, 0x7f100316

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->v:Landroid/widget/TextView;

    .line 610
    const v0, 0x7f100315

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->w:Landroid/widget/ImageView;

    .line 612
    const v0, 0x7f10001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->x:Landroid/widget/ImageView;

    .line 613
    const v0, 0x7f100270

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    .line 615
    const v0, 0x7f1002a8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->A:Landroid/widget/ImageView;

    .line 616
    const v0, 0x7f10004e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->z:Landroid/widget/ImageView;

    .line 618
    new-instance v0, Lcom/google/googlenav/ui/view/android/bw;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/bs;->R:I

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bw;-><init>(Lcom/google/googlenav/ui/view/android/bs;I)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->C:Lcom/google/googlenav/ui/view/android/bw;

    .line 619
    new-instance v0, Lcom/google/googlenav/ui/view/android/bw;

    const/16 v1, 0x578

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bw;-><init>(Lcom/google/googlenav/ui/view/android/bs;I)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->D:Lcom/google/googlenav/ui/view/android/bw;

    .line 621
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1c6

    iget-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->y:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1c6

    .line 622
    new-instance v0, Lcom/google/googlenav/ui/view/android/bw;

    invoke-direct {v0, p0, v4}, Lcom/google/googlenav/ui/view/android/bw;-><init>(Lcom/google/googlenav/ui/view/android/bs;I)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    .line 627
    :goto_193
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->g:Z

    if-eqz v0, :cond_1c9

    .line 628
    new-instance v0, Lcom/google/googlenav/ui/view/android/bx;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->f:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bx;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    .line 633
    :goto_1a4
    new-instance v0, Lcom/google/googlenav/ui/view/android/by;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->x:Lcom/google/googlenav/ui/view/a;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/by;-><init>(Lcom/google/googlenav/ui/view/android/bs;Lcom/google/googlenav/ui/view/a;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->H:Lcom/google/googlenav/ui/view/android/by;

    .line 634
    new-instance v0, Lcom/google/googlenav/ui/view/android/bz;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->q:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bz;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->I:Lcom/google/googlenav/ui/view/android/bz;

    .line 635
    new-instance v0, Lcom/google/googlenav/ui/view/android/bz;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->i:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/view/android/bz;-><init>(Lcom/google/googlenav/ui/view/android/bs;Ljava/lang/String;)V

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->J:Lcom/google/googlenav/ui/view/android/bz;

    .line 637
    invoke-static {p2}, Lcom/google/googlenav/ui/bC;->a(Landroid/view/View;)Lcom/google/googlenav/ui/bD;

    move-result-object v0

    iput-object v0, p1, Lcom/google/googlenav/ui/view/android/bv;->B:Lcom/google/googlenav/ui/bD;

    .line 638
    return-void

    .line 624
    :cond_1c6
    iput-object v5, p1, Lcom/google/googlenav/ui/view/android/bv;->E:Lcom/google/googlenav/ui/view/android/bw;

    goto :goto_193

    .line 630
    :cond_1c9
    iput-object v5, p1, Lcom/google/googlenav/ui/view/android/bv;->G:Lcom/google/googlenav/ui/view/android/bx;

    goto :goto_1a4
.end method

.method public b()I
    .registers 2

    .prologue
    .line 542
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bs;->F:I

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 547
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bs;->G:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/16 v7, 0xa

    .line 1173
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1175
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_11

    .line 1176
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1179
    :cond_11
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_31

    .line 1180
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1182
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bs;->c:[Ljava/lang/CharSequence;

    array-length v4, v3

    move v1, v0

    :goto_1c
    if-ge v1, v4, :cond_31

    aget-object v5, v3, v1

    .line 1183
    if-eqz v5, :cond_2e

    .line 1184
    if-lez v0, :cond_29

    .line 1185
    const-string v6, ", "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1187
    :cond_29
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1188
    add-int/lit8 v0, v0, 0x1

    .line 1182
    :cond_2e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c

    .line 1193
    :cond_31
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3d

    .line 1194
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1195
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->l:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1198
    :cond_3d
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    if-eqz v0, :cond_49

    .line 1199
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1200
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->m:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1203
    :cond_49
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    if-eqz v0, :cond_55

    .line 1204
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1205
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->n:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1208
    :cond_55
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    if-eqz v0, :cond_61

    .line 1209
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1210
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->o:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1213
    :cond_61
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6d

    .line 1214
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1215
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->p:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1218
    :cond_6d
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    if-eqz v0, :cond_79

    .line 1219
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1220
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->s:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1223
    :cond_79
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    if-eqz v0, :cond_85

    .line 1224
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1225
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bs;->t:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1228
    :cond_85
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
