.class public final Lcom/google/googlenav/ui/android/N;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/v;
.implements Lbf/au;


# instance fields
.field private final a:Lcom/google/googlenav/ui/android/FloorPickerView;

.field private final b:Lcom/google/googlenav/ui/s;

.field private final c:Lcom/google/googlenav/ui/android/AndroidVectorView;

.field private d:Lo/z;

.field private volatile e:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    .line 85
    iput-object p2, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    .line 86
    iput-object p3, p0, Lcom/google/googlenav/ui/android/N;->c:Lcom/google/googlenav/ui/android/AndroidVectorView;

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/android/FloorPickerView;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)Lcom/google/googlenav/ui/android/N;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 94
    new-instance v0, Lcom/google/googlenav/ui/android/N;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/ui/android/N;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/android/AndroidVectorView;)V

    .line 95
    invoke-direct {v0}, Lcom/google/googlenav/ui/android/N;->a()V

    .line 96
    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/N;Lo/z;)Lo/z;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/googlenav/ui/android/N;->d:Lo/z;

    return-object p1
.end method

.method private a()V
    .registers 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_1e

    invoke-static {v1}, LR/a;->a([I)LR/a;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lbf/am;->a(Lbf/au;LR/a;)V

    .line 103
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    new-instance v1, Lcom/google/googlenav/ui/android/O;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/O;-><init>(Lcom/google/googlenav/ui/android/N;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    return-void

    .line 100
    :array_1e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x15t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/N;)Lcom/google/googlenav/ui/s;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method private b()V
    .registers 6

    .prologue
    .line 137
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 138
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->I()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/i;

    .line 139
    instance-of v3, v0, Lbf/bk;

    if-eqz v3, :cond_12

    .line 140
    check-cast v0, Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v3

    .line 141
    const/4 v0, 0x0

    :goto_29
    invoke-interface {v3}, Lcom/google/googlenav/F;->f()I

    move-result v4

    if-ge v0, v4, :cond_12

    .line 142
    invoke-interface {v3, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    .line 143
    invoke-interface {v4}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v4

    .line 144
    if-eqz v4, :cond_3c

    .line 145
    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 141
    :cond_3c
    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    .line 150
    :cond_3f
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Ljava/util/Collection;)V

    .line 151
    return-void
.end method


# virtual methods
.method public a(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/N;->b()V

    .line 124
    return-void
.end method

.method public a(ZZZII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/N;->e:Z

    .line 259
    return-void
.end method

.method public b(Lbf/i;)V
    .registers 4
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/googlenav/ui/android/N;->a:Lcom/google/googlenav/ui/android/FloorPickerView;

    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Ljava/util/Collection;)V

    .line 129
    return-void
.end method

.method public c(Lbf/i;)V
    .registers 2
    .parameter

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/N;->b()V

    .line 134
    return-void
.end method

.method public i()V
    .registers 1

    .prologue
    .line 263
    return-void
.end method
