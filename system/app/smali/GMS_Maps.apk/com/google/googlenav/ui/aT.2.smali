.class public Lcom/google/googlenav/ui/aT;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 98
    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_1a

    .line 100
    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    .line 101
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    .line 102
    new-instance v0, LaN/B;

    div-int/lit8 v1, v1, 0xa

    div-int/lit8 v2, v2, 0xa

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    .line 104
    :goto_19
    return-object v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;B)Lcom/google/googlenav/ai;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x4

    .line 73
    new-instance v0, Lcom/google/googlenav/ai;

    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {p0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, v5, v3}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {p0, v5, v4}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    .line 80
    const/16 v1, 0xb

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(Ljava/lang/String;)V

    .line 83
    const/16 v1, 0xa

    invoke-static {p0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->d(Ljava/lang/String;)V

    .line 86
    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;

    move-result-object v1

    .line 87
    if-eqz v1, :cond_36

    .line 88
    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(Lo/D;)V

    .line 91
    :cond_36
    return-object v0
.end method

.method public static a(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 4
    .parameter

    .prologue
    .line 153
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 154
    const/4 v1, 0x1

    invoke-virtual {p0}, LaN/B;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 155
    const/4 v1, 0x2

    invoke-virtual {p0}, LaN/B;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 156
    return-object v0
.end method

.method public static a(LaN/B;[Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 137
    if-nez p0, :cond_4

    .line 138
    const/4 v0, 0x0

    .line 146
    :goto_3
    return-object v0

    .line 140
    :cond_4
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 141
    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->a(LaN/B;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 142
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 144
    invoke-static {v0, p1}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 9
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 246
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    .line 247
    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 250
    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aT;->a(LaN/B;[Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    .line 252
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2a

    .line 253
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 255
    :cond_2a
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3c

    .line 256
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 258
    :cond_3c
    invoke-static {p0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_47

    .line 260
    const/16 v2, 0xb

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 262
    :cond_47
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v0

    .line 263
    if-eqz v0, :cond_77

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_77

    .line 265
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    .line 267
    const/16 v2, 0x16

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v3}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 268
    invoke-virtual {v0}, Lo/D;->b()I

    move-result v2

    const/high16 v3, -0x8000

    if-eq v2, v3, :cond_77

    .line 269
    const/16 v2, 0x17

    invoke-virtual {v0}, Lo/D;->b()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 273
    :cond_77
    return-object v1
.end method

.method public static a(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 223
    if-nez p1, :cond_9

    .line 224
    const/16 v0, 0x291

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 233
    :goto_8
    return-object v0

    .line 227
    :cond_9
    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 228
    const/16 v0, 0x3a4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object p0

    .line 233
    :cond_15
    const/16 v0, 0x3a5

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Landroid/text/SpannableStringBuilder;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 53
    invoke-static {p0, v2, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    .line 54
    if-nez v0, :cond_16

    .line 55
    const/16 v0, 0x10

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 56
    const/16 v0, 0x7c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    :cond_16
    :goto_16
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2a

    .line 63
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 64
    sget-object v1, Lcom/google/googlenav/ui/aV;->aw:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 66
    :cond_2a
    return-void

    .line 58
    :cond_2b
    invoke-static {v2, v1, p0}, Lcom/google/googlenav/d;->a(IILcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    goto :goto_16
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 166
    if-nez p1, :cond_6

    .line 176
    :cond_5
    :goto_5
    return-void

    .line 170
    :cond_6
    array-length v0, p1

    if-lez v0, :cond_16

    aget-object v0, p1, v2

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 171
    aget-object v0, p1, v2

    invoke-virtual {p0, v3, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertString(IILjava/lang/String;)V

    .line 173
    :cond_16
    array-length v0, p1

    if-le v0, v1, :cond_5

    aget-object v0, p1, v1

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 174
    aget-object v0, p1, v1

    invoke-virtual {p0, v3, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->insertString(IILjava/lang/String;)V

    goto :goto_5
.end method

.method public static a(LaN/B;LaN/B;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 210
    if-eqz p0, :cond_10

    if-eqz p1, :cond_10

    invoke-virtual {p0, p1}, LaN/B;->a(LaN/B;)J

    move-result-wide v0

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-gez v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;
    .registers 5
    .parameter

    .prologue
    const/16 v3, 0x16

    .line 112
    const/4 v0, 0x0

    .line 114
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 115
    const/16 v1, 0x17

    const/high16 v2, -0x8000

    invoke-static {p0, v1, v2}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    .line 118
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lo/r;->b(Ljava/lang/String;)Lo/r;

    move-result-object v2

    .line 119
    if-eqz v2, :cond_20

    .line 120
    new-instance v0, Lo/D;

    invoke-direct {v0, v2, v1}, Lo/D;-><init>(Lo/r;I)V

    .line 124
    :cond_20
    return-object v0
.end method
