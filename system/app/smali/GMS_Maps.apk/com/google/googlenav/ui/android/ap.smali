.class public Lcom/google/googlenav/ui/android/ap;
.super Lcom/google/googlenav/ui/view/d;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ai;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 54
    const v0, 0x7f10005e

    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1, v2}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    .line 55
    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/android/ap;->a(Lcom/google/googlenav/ai;)V

    .line 56
    invoke-virtual {p2}, Lcom/google/googlenav/ai;->aA()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ap;->c:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ap;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 31
    invoke-static {p0}, Lcom/google/googlenav/ui/android/ap;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/ap;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ap;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ai;)V
    .registers 7
    .parameter

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aA()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 98
    invoke-static {v0}, Lcom/google/googlenav/ui/android/ap;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    .line 99
    invoke-static {v0}, Lcom/google/googlenav/ui/android/ap;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v1

    .line 100
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/google/googlenav/ui/android/ap;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/google/googlenav/ui/android/ap;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ap;->d()Landroid/view/View;

    move-result-object v3

    .line 103
    const v4, 0x7f10038e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 104
    new-instance v4, Lcom/google/googlenav/ui/android/aq;

    invoke-direct {v4, p0, v0}, Lcom/google/googlenav/ui/android/aq;-><init>(Lcom/google/googlenav/ui/android/ap;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    const v0, 0x7f10038f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ap;->a:Landroid/widget/TextView;

    .line 112
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ap;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    const v0, 0x7f100390

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/ap;->b:Landroid/widget/TextView;

    .line 115
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ap;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/ap;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 126
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/ap;->d()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 128
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 130
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 136
    const/4 v2, 0x0

    const/16 v3, 0x602

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 137
    const/4 v2, 0x1

    const/16 v3, 0x1d9

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 139
    new-instance v2, Lcom/google/googlenav/ui/android/ar;

    invoke-direct {v2, p0, p2}, Lcom/google/googlenav/ui/android/ar;-><init>(Lcom/google/googlenav/ui/android/ap;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 161
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 162
    return-void
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .registers 6
    .parameter

    .prologue
    const/4 v4, 0x6

    .line 66
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 67
    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    .line 68
    if-lez v2, :cond_18

    .line 69
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v2, :cond_18

    .line 70
    invoke-virtual {p0, v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 73
    :cond_18
    return-object v1
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 77
    const/4 v0, 0x7

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 81
    const/4 v0, 0x5

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 85
    const/4 v0, 0x4

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 89
    const/16 v0, 0x8

    invoke-static {p0, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/googlenav/ui/android/ap;->c:Ljava/lang/String;

    return-object v0
.end method
