.class public Lcom/google/googlenav/ui/wizard/bx;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field protected a:Lax/b;

.field protected b:Lcom/google/googlenav/bZ;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 44
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    .line 45
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Lax/b;)V
    .registers 2
    .parameter

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/bx;->b(Lax/b;)V

    .line 58
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    .line 59
    return-void
.end method

.method public a(Lcom/google/googlenav/bZ;)V
    .registers 2
    .parameter

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/bx;->b(Lcom/google/googlenav/bZ;)V

    .line 63
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    .line 64
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    const-string v0, "al"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 99
    new-instance v0, Lcom/google/googlenav/ui/wizard/bA;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bA;-><init>(Lcom/google/googlenav/ui/wizard/bx;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 100
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 101
    return-void
.end method

.method public b(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    .line 69
    return-void
.end method

.method public b(Lcom/google/googlenav/bZ;)V
    .registers 3
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    .line 74
    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 88
    const-string v0, "ac"

    invoke-static {v0}, Lbf/O;->b(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-static {p1}, Lau/b;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Lcom/google/googlenav/aB;Z)V

    .line 95
    return-void
.end method

.method protected c()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 345
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 346
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    .line 347
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    .line 348
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    .line 353
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    .line 354
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bx;->a()V

    .line 355
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bx;->a:Lax/b;

    .line 356
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/bx;->b:Lcom/google/googlenav/bZ;

    .line 357
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bx;->j()V

    .line 358
    return-void
.end method
