.class Lcom/google/googlenav/ui/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:C

.field final b:J


# direct methods
.method constructor <init>(CJ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 3801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3802
    iput-char p1, p0, Lcom/google/googlenav/ui/bp;->a:C

    .line 3803
    iput-wide p2, p0, Lcom/google/googlenav/ui/bp;->b:J

    .line 3804
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 3808
    instance-of v1, p1, Lcom/google/googlenav/ui/bp;

    if-nez v1, :cond_6

    .line 3812
    :cond_5
    :goto_5
    return v0

    .line 3811
    :cond_6
    check-cast p1, Lcom/google/googlenav/ui/bp;

    .line 3812
    iget-wide v1, p0, Lcom/google/googlenav/ui/bp;->b:J

    iget-wide v3, p1, Lcom/google/googlenav/ui/bp;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    iget-char v1, p0, Lcom/google/googlenav/ui/bp;->a:C

    iget-char v2, p1, Lcom/google/googlenav/ui/bp;->a:C

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 5

    .prologue
    .line 3817
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-char v2, p0, Lcom/google/googlenav/ui/bp;->a:C

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/googlenav/ui/bp;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
