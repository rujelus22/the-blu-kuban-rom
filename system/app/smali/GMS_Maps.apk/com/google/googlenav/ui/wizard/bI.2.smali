.class public Lcom/google/googlenav/ui/wizard/bI;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lax/b;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 47
    return-void
.end method

.method private static a(Ljava/lang/String;)[Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 50
    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 51
    const/4 v0, 0x0

    .line 58
    :goto_7
    return-object v0

    .line 54
    :cond_8
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 58
    :cond_27
    invoke-static {p0}, Lau/b;->h(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 63
    invoke-static {p0}, Lcom/google/googlenav/ui/wizard/bI;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_a

    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_c

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_b
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 69
    invoke-static {p0}, Lcom/google/googlenav/ui/wizard/bI;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_a

    array-length v1, v0

    const/4 v2, 0x2

    if-ge v1, v2, :cond_c

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return-object v0

    :cond_c
    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_b
.end method


# virtual methods
.method public a(Lax/b;)V
    .registers 2
    .parameter

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    .line 191
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    .line 192
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    packed-switch p1, :pswitch_data_26

    .line 152
    :cond_3
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 144
    :pswitch_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {v0, p2}, Lax/b;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 145
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/bI;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 147
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bI;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x1

    goto :goto_4

    .line 142
    nop

    :pswitch_data_26
    .packed-switch 0x5
        :pswitch_5
    .end packed-switch
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bI;->b:Z

    .line 159
    new-instance v0, Lcom/google/googlenav/ui/wizard/bK;

    invoke-direct {v0, p0, p0}, Lcom/google/googlenav/ui/wizard/bK;-><init>(Lcom/google/googlenav/ui/wizard/bI;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 169
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 170
    return-void
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    .line 175
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 176
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    .line 181
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bI;->a()V

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bI;->a(Lax/b;)V

    .line 183
    return-void
.end method

.method protected e()Ljava/util/Vector;
    .registers 16

    .prologue
    const/4 v14, 0x3

    const/16 v13, 0x11

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 78
    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    .line 80
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {v0}, Lax/b;->ab()Lax/h;

    move-result-object v4

    move v0, v1

    .line 81
    :goto_12
    invoke-virtual {v4}, Lax/h;->d()I

    move-result v2

    if-ge v0, v2, :cond_11d

    .line 82
    invoke-virtual {v4, v0}, Lax/h;->a(I)Lax/m;

    move-result-object v2

    .line 83
    invoke-virtual {v2}, Lax/m;->q()Z

    move-result v5

    if-nez v5, :cond_2b

    invoke-virtual {v2}, Lax/m;->o()Z

    move-result v5

    if-nez v5, :cond_2b

    .line 81
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 87
    :cond_2b
    invoke-virtual {v2}, Lax/m;->m()Lax/t;

    move-result-object v5

    invoke-virtual {v5}, Lax/t;->f()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    .line 88
    invoke-virtual {v5, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v6

    if-eqz v6, :cond_28

    .line 92
    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/bI;->e:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v6

    invoke-virtual {v2}, Lax/m;->m()Lax/t;

    move-result-object v7

    invoke-virtual {v7}, Lax/t;->y()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lcom/google/googlenav/ui/m;->a(J)C

    move-result v6

    invoke-static {v6}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v6

    .line 95
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lax/m;->m()Lax/t;

    move-result-object v2

    invoke-virtual {v2}, Lax/t;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 96
    new-instance v6, Lbj/m;

    sget-object v7, Lcom/google/googlenav/ui/aV;->P:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v7}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v6, v2, v1}, Lbj/m;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual {v3, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 99
    :goto_81
    invoke-virtual {v5, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    if-ge v2, v6, :cond_28

    .line 100
    invoke-virtual {v5, v13, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v6

    .line 101
    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/bI;->a:Lax/b;

    invoke-virtual {v7, v6}, Lax/b;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 103
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-nez v8, :cond_9d

    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_d3

    .line 105
    :cond_9d
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 106
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_b4

    .line 107
    invoke-virtual {v7, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/googlenav/ui/aV;->C:Lcom/google/googlenav/ui/aV;

    invoke-static {v9, v10}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_b4
    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_c7

    .line 111
    invoke-virtual {v7, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v9, v10}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    :cond_c7
    new-instance v9, Lcom/google/googlenav/ui/wizard/bL;

    invoke-static {v8}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-direct {v9, v8}, Lcom/google/googlenav/ui/wizard/bL;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 118
    :cond_d3
    invoke-virtual {v7, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v8

    if-eqz v8, :cond_119

    .line 119
    invoke-virtual {v7, v14}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/googlenav/ui/wizard/bI;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 121
    invoke-static {v7}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_119

    .line 122
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v9, Lcom/google/googlenav/ui/bi;->R:C

    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v7, v8}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v7

    .line 126
    new-instance v8, Lcom/google/googlenav/ui/wizard/bL;

    invoke-static {v7}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v7

    new-instance v9, Lcom/google/googlenav/ui/wizard/bJ;

    invoke-direct {v9, p0, v6}, Lcom/google/googlenav/ui/wizard/bJ;-><init>(Lcom/google/googlenav/ui/wizard/bI;I)V

    invoke-direct {v8, v7, v9}, Lcom/google/googlenav/ui/wizard/bL;-><init>(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_119
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_81

    .line 137
    :cond_11d
    return-object v3
.end method

.method public p()Z
    .registers 2

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bI;->b:Z

    return v0
.end method
