.class Lcom/google/googlenav/ui/wizard/ht;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/bG;


# instance fields
.field private final a:Lcom/google/googlenav/ai;

.field private final b:Lcom/google/googlenav/J;

.field private final c:Lbf/am;

.field private final d:Lcom/google/googlenav/ui/wizard/hq;

.field private final e:Lcom/google/googlenav/ui/wizard/hx;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    .line 109
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    .line 110
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/ht;->c:Lbf/am;

    .line 111
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/ht;->d:Lcom/google/googlenav/ui/wizard/hq;

    .line 112
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    .line 113
    return-void
.end method


# virtual methods
.method public a(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 117
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->d:Lcom/google/googlenav/ui/wizard/hq;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->d:Lcom/google/googlenav/ui/wizard/hq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hq;->a(Lcom/google/googlenav/ui/wizard/hq;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 118
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    .line 159
    :goto_12
    return-void

    .line 123
    :cond_13
    if-nez p1, :cond_ad

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-nez v0, :cond_a6

    move v1, v2

    .line 125
    :goto_22
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bF()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ai;->n(I)V

    .line 126
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v3}, Lcom/google/googlenav/aC;->a(Lcom/google/googlenav/ai;)V

    .line 127
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bk()V

    .line 131
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->j()Lbf/by;

    move-result-object v0

    invoke-virtual {v0}, Lbf/by;->bJ()LaR/n;

    move-result-object v3

    .line 133
    invoke-interface {v3}, LaR/n;->e()LaR/u;

    move-result-object v0

    .line 134
    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 135
    if-eqz v0, :cond_6c

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 136
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->C()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-interface {v3, v0, v4}, LaR/n;->b(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    .line 141
    :cond_6c
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->c:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbf/i;->f(Z)V

    .line 142
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->b()V

    .line 143
    if-eqz v1, :cond_aa

    const/16 v0, 0x40f

    .line 145
    :goto_7e
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ax;->b()Z

    move-result v1

    if-eqz v1, :cond_91

    .line 147
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/ht;->a:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v3, v2}, Lcom/google/googlenav/ui/wizard/hx;->a(Lcom/google/googlenav/ai;Z)V

    .line 156
    :cond_91
    :goto_91
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->a()V

    .line 157
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/ht;->b:Lcom/google/googlenav/J;

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ht;->e:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->a()V

    goto/16 :goto_12

    .line 124
    :cond_a6
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_22

    .line 143
    :cond_aa
    const/16 v0, 0x41c

    goto :goto_7e

    .line 150
    :cond_ad
    const/4 v0, 0x5

    if-ne p1, v0, :cond_b3

    .line 151
    const/16 v0, 0x40e

    goto :goto_91

    .line 153
    :cond_b3
    const/16 v0, 0x40d

    goto :goto_91
.end method
