.class public Lcom/google/googlenav/ui/view/dialog/ai;
.super Lcom/google/googlenav/ui/view/android/aL;
.source "SourceFile"


# static fields
.field private static final g:Ljava/util/List;


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:[Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    .line 38
    const v0, 0x7f1001f2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f1001f3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f1001f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f1001f5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const v4, 0x7f1001f6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    const v0, 0x7f0f001d

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/aL;-><init>(Landroid/content/Context;I)V

    .line 47
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->setCancelable(Z)V

    .line 48
    return-void
.end method

.method public static a()Lcom/google/googlenav/ui/view/dialog/ai;
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 98
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/MapsActivity;->getMapsActivity(Landroid/content/Context;)Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/ai;

    invoke-direct {v1, v0}, Lcom/google/googlenav/ui/view/dialog/ai;-><init>(Landroid/content/Context;)V

    .line 100
    const/16 v0, 0x42a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->a:Ljava/lang/CharSequence;

    .line 101
    const/16 v0, 0x429

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->b:Ljava/lang/CharSequence;

    .line 102
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    .line 103
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    .line 104
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "26-30"

    aput-object v2, v0, v3

    .line 105
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x428

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 106
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "21-25"

    aput-object v2, v0, v4

    .line 107
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x427

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    .line 108
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "16-20"

    aput-object v2, v0, v5

    .line 109
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x426

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    .line 110
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "11-15"

    aput-object v2, v0, v6

    .line 111
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x425

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v6

    .line 112
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    const-string v2, "0-10"

    aput-object v2, v0, v7

    .line 113
    iget-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    const/16 v2, 0x424

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v7

    .line 114
    const/16 v0, 0x423

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/googlenav/ui/view/dialog/ai;->f:Ljava/lang/CharSequence;

    .line 115
    return-object v1
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 51
    if-eqz p2, :cond_6

    .line 52
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    :goto_5
    return-void

    .line 54
    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 60
    invoke-virtual {p0, v5}, Lcom/google/googlenav/ui/view/dialog/ai;->requestWindowFeature(I)Z

    .line 62
    const v0, 0x7f040089

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->setContentView(I)V

    .line 63
    const v0, 0x7f1001f0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-nez v1, :cond_1e

    .line 65
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 67
    :cond_1e
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->a:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/ai;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 68
    const v0, 0x7f1001f1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->b:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/ai;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    move v2, v3

    .line 70
    :goto_32
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7c

    .line 71
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/ai;->g:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 72
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    if-eqz v1, :cond_58

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    if-nez v1, :cond_61

    .line 73
    :cond_58
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 70
    :goto_5d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_32

    .line 75
    :cond_61
    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 76
    iget-object v4, p0, Lcom/google/googlenav/ui/view/dialog/ai;->c:[Ljava/lang/CharSequence;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {v0, v5}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->d:[Ljava/lang/CharSequence;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5d

    .line 82
    :cond_7c
    const v0, 0x7f1001f7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->e:Ljava/lang/CharSequence;

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/ai;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 84
    const v0, 0x7f1001f8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/ai;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 85
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/ai;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 86
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aj;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/aj;-><init>(Lcom/google/googlenav/ui/view/dialog/ai;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-void
.end method
