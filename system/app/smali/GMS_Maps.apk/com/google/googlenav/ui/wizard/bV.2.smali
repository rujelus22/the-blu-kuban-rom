.class public Lcom/google/googlenav/ui/wizard/bV;
.super Lcom/google/googlenav/ui/wizard/hH;
.source "SourceFile"


# instance fields
.field private k:Lax/b;

.field private l:Ljava/util/List;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/hH;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 80
    return-void
.end method

.method private B()V
    .registers 5

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    .line 138
    iget v1, p0, Lcom/google/googlenav/ui/wizard/bV;->c:I

    .line 139
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    .line 140
    iget-boolean v3, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    .line 141
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bV;->a()V

    .line 142
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    .line 143
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    .line 144
    iput v1, p0, Lcom/google/googlenav/ui/wizard/bV;->c:I

    .line 145
    return-void
.end method

.method private static a(Ljava/util/List;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 389
    new-instance v0, Lbj/bt;

    invoke-static {p1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-direct {v0, v1, v2, p2}, Lbj/bt;-><init>(Ljava/lang/String;Lcom/google/googlenav/ui/aV;I)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    return-void
.end method


# virtual methods
.method protected R_()S
    .registers 2

    .prologue
    .line 149
    const/16 v0, 0x56

    return v0
.end method

.method protected a(LaZ/b;)LaZ/a;
    .registers 19
    .parameter

    .prologue
    .line 430
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    if-eqz v1, :cond_12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_17

    .line 435
    :cond_12
    invoke-super/range {p0 .. p1}, Lcom/google/googlenav/ui/wizard/hH;->a(LaZ/b;)LaZ/a;

    move-result-object v1

    .line 438
    :goto_16
    return-object v1

    :cond_17
    new-instance v1, LaZ/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v5, v5, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v6, v6, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v7, v7, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v7}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v9, v9, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v10, v10, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v11, v11, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v12, v12, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    invoke-virtual {v13}, Lax/b;->az()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v14, v14, Lcom/google/googlenav/ui/wizard/hQ;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v15, v15, Lcom/google/googlenav/ui/wizard/hQ;->c:Ljava/lang/String;

    move-object/from16 v16, p1

    invoke-direct/range {v1 .. v16}, LaZ/a;-><init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IIIILjava/lang/String;Ljava/lang/String;LaZ/b;)V

    goto :goto_16
.end method

.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .registers 6
    .parameter

    .prologue
    .line 397
    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bV;->i()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    return-object v0
.end method

.method public a(Lax/b;LaN/H;IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    .line 90
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/bV;->l:Ljava/util/List;

    .line 91
    const/16 v0, 0x444

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->a:Ljava/lang/String;

    .line 92
    const/4 v0, 0x0

    invoke-super {p0, p2, p3, p4, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(LaN/H;IILcom/google/googlenav/ui/wizard/C;)V

    .line 93
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x5

    const/4 v9, 0x0

    const/4 v1, 0x3

    const/4 v10, 0x1

    .line 154
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    if-eqz v0, :cond_11

    move v0, v1

    .line 157
    :goto_9
    packed-switch p1, :pswitch_data_132

    .line 227
    :pswitch_c
    invoke-super {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/hH;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_10
    return v0

    :cond_11
    move v0, v2

    .line 154
    goto :goto_9

    .line 160
    :pswitch_13
    iput-boolean v9, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    .line 161
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(I)V

    move v0, v10

    .line 162
    goto :goto_10

    .line 165
    :pswitch_1a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p2, v0, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    .line 167
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v0, v3}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v3, 0x4

    if-ne v0, v3, :cond_62

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v0, v3}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    :goto_3f
    iput-object v0, v2, Lcom/google/googlenav/ui/wizard/hQ;->o:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v2, v3}, Lax/b;->h(I)LaN/B;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    .line 172
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v2, v3}, Lax/b;->i(I)Lo/D;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    .line 173
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/bV;->a(I)V

    move v0, v10

    .line 174
    goto :goto_10

    .line 167
    :cond_62
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v0, v3}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    goto :goto_3f

    .line 177
    :pswitch_71
    const/16 v1, 0x460

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/lang/String;II)V

    move v0, v10

    .line 179
    goto :goto_10

    .line 182
    :pswitch_7c
    const/16 v1, 0x461

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/lang/String;II)V

    move v0, v10

    .line 185
    goto :goto_10

    .line 188
    :pswitch_88
    const/16 v1, 0x462

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/lang/String;II)V

    move v0, v10

    .line 190
    goto/16 :goto_10

    .line 196
    :pswitch_95
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    .line 197
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->C()LaN/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    .line 198
    const/16 v0, 0x464

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {p0, v0, v2, v1}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/lang/String;II)V

    move v0, v10

    .line 201
    goto/16 :goto_10

    .line 206
    :pswitch_c1
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    if-nez v0, :cond_e3

    .line 207
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    invoke-virtual {v1}, LaN/H;->a()LaN/B;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    .line 208
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->C()LaN/p;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    invoke-virtual {v1, v2}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    .line 210
    :cond_e3
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->d:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v4, v4, Lcom/google/googlenav/ui/wizard/hQ;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v5, v5, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v6, v6, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v7, v7, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    move-object v8, p0

    invoke-virtual/range {v0 .. v9}, Lcom/google/googlenav/ui/wizard/jv;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;LaN/H;IILcom/google/googlenav/ui/wizard/C;Z)V

    .line 213
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bV;->B()V

    move v0, v10

    .line 214
    goto/16 :goto_10

    .line 219
    :pswitch_108
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    .line 220
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jv;->C()LaN/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v2

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    .line 221
    const/16 v0, 0x46c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v9, v1}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/lang/String;II)V

    move v0, v10

    .line 224
    goto/16 :goto_10

    .line 157
    :pswitch_data_132
    .packed-switch 0x5e1
        :pswitch_108
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_71
        :pswitch_7c
        :pswitch_88
        :pswitch_95
        :pswitch_c1
        :pswitch_1a
        :pswitch_13
    .end packed-switch
.end method

.method protected b()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 100
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    invoke-virtual {v1}, Lax/b;->c()I

    move-result v1

    iput v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    .line 102
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_71

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    .line 104
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v0, v2}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_62

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v0, v2}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    :goto_36
    iput-object v0, v1, Lcom/google/googlenav/ui/wizard/hQ;->o:Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v1, v2}, Lax/b;->h(I)LaN/B;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    .line 109
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v1, v2}, Lax/b;->i(I)Lo/D;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    .line 116
    :goto_54
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    invoke-virtual {v1}, Lax/b;->al()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->c:Ljava/lang/String;

    .line 118
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/hH;->b()V

    .line 119
    return-void

    .line 104
    :cond_62
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    invoke-virtual {v0, v2}, Lax/b;->n(I)Lax/t;

    move-result-object v0

    invoke-virtual {v0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    goto :goto_36

    .line 112
    :cond_71
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    .line 113
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    goto :goto_54
.end method

.method protected c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 123
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/bV;->k:Lax/b;

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    const/4 v1, -0x1

    iput v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->n:I

    .line 125
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object v2, v0, Lcom/google/googlenav/ui/wizard/hQ;->o:Ljava/lang/String;

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    .line 127
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/hH;->c()V

    .line 128
    return-void
.end method

.method public d()V
    .registers 1

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/bV;->B()V

    .line 133
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/bV;->j()V

    .line 134
    return-void
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method protected g()Lcom/google/googlenav/ui/view/android/aL;
    .registers 2

    .prologue
    .line 420
    new-instance v0, Lcom/google/googlenav/ui/wizard/bY;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bY;-><init>(Lcom/google/googlenav/ui/wizard/bV;)V

    return-object v0
.end method

.method public h()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 233
    iget v0, p0, Lcom/google/googlenav/ui/wizard/bV;->c:I

    packed-switch v0, :pswitch_data_2a

    .line 247
    :pswitch_6
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/hH;->h()V

    .line 248
    :goto_9
    return-void

    .line 235
    :pswitch_a
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(I)V

    goto :goto_9

    .line 238
    :pswitch_e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_20

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/bV;->m:Z

    if-eqz v0, :cond_24

    .line 241
    :cond_20
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(I)V

    goto :goto_9

    .line 243
    :cond_24
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/bV;->a(I)V

    goto :goto_9

    .line 233
    nop

    :pswitch_data_2a
    .packed-switch 0x3
        :pswitch_e
        :pswitch_6
        :pswitch_a
    .end packed-switch
.end method

.method public i()Ljava/util/List;
    .registers 4

    .prologue
    .line 356
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 357
    iget v1, p0, Lcom/google/googlenav/ui/wizard/bV;->c:I

    sparse-switch v1, :sswitch_data_38

    .line 378
    :goto_9
    return-object v0

    .line 359
    :sswitch_a
    const/16 v1, 0x460

    const/16 v2, 0x5e7

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/util/List;II)V

    .line 361
    const/16 v1, 0x461

    const/16 v2, 0x5e8

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/util/List;II)V

    .line 363
    const/16 v1, 0x462

    const/16 v2, 0x5e9

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/util/List;II)V

    .line 365
    const/16 v1, 0x464

    const/16 v2, 0x5ea

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/util/List;II)V

    .line 367
    const/16 v1, 0x463

    const/16 v2, 0x5eb

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/util/List;II)V

    .line 369
    const/16 v1, 0x46c

    const/16 v2, 0x5e1

    invoke-static {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/bV;->a(Ljava/util/List;II)V

    goto :goto_9

    .line 373
    :sswitch_35
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/bV;->l:Ljava/util/List;

    goto :goto_9

    .line 357
    :sswitch_data_38
    .sparse-switch
        0x1 -> :sswitch_a
        0x5 -> :sswitch_35
    .end sparse-switch
.end method

.method protected y()Lcom/google/googlenav/ui/view/android/aL;
    .registers 2

    .prologue
    .line 425
    new-instance v0, Lcom/google/googlenav/ui/wizard/bX;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/bX;-><init>(Lcom/google/googlenav/ui/wizard/bV;)V

    return-object v0
.end method
