.class public Lcom/google/googlenav/ui/wizard/S;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Lcom/google/googlenav/ui/wizard/ac;

.field private final c:Lcom/google/googlenav/ui/wizard/aC;

.field private final d:Lcom/google/googlenav/ui/wizard/ad;

.field private e:Lcom/google/googlenav/ui/wizard/ah;

.field private final f:Lcom/google/googlenav/ui/wizard/aF;

.field private g:Z

.field private h:Lcom/google/googlenav/ui/wizard/an;

.field private i:Lcom/google/googlenav/h;

.field private j:Ljava/util/List;

.field private k:Z

.field private final l:Lcom/google/googlenav/ui/wizard/jv;

.field private m:Lcom/google/googlenav/aU;

.field private final n:Lcom/google/googlenav/ui/ak;

.field private final o:LaG/h;

.field private p:Lcom/google/googlenav/ui/wizard/aL;

.field private q:LaL/g;

.field private r:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 65
    const-class v0, Lcom/google/googlenav/ui/wizard/S;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/googlenav/ui/wizard/S;->a:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/ak;Lbf/a;ZLcom/google/googlenav/ui/wizard/aF;Lcom/google/googlenav/ui/wizard/ac;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    .line 153
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    .line 158
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/S;->k:Z

    .line 213
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    .line 214
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/S;->n:Lcom/google/googlenav/ui/ak;

    .line 215
    iput-boolean p4, p0, Lcom/google/googlenav/ui/wizard/S;->g:Z

    .line 216
    iput-object p6, p0, Lcom/google/googlenav/ui/wizard/S;->b:Lcom/google/googlenav/ui/wizard/ac;

    .line 217
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/S;->f:Lcom/google/googlenav/ui/wizard/aF;

    .line 219
    new-instance v0, LaG/h;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v1

    invoke-direct {v0, v1}, LaG/h;-><init>(LaB/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->o:LaG/h;

    .line 223
    new-instance v0, Lcom/google/googlenav/ui/wizard/U;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/U;-><init>(Lcom/google/googlenav/ui/wizard/S;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->c:Lcom/google/googlenav/ui/wizard/aC;

    .line 227
    new-instance v0, Lcom/google/googlenav/ui/wizard/ad;

    invoke-direct {v0, p0, v2}, Lcom/google/googlenav/ui/wizard/ad;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->d:Lcom/google/googlenav/ui/wizard/ad;

    .line 231
    new-instance v0, Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->H()Lcom/google/googlenav/friend/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v2

    invoke-direct {v0, v1, p3, v2}, Lcom/google/googlenav/ui/wizard/ah;-><init>(Lcom/google/googlenav/friend/j;Lbf/a;Lcom/google/googlenav/android/aa;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    .line 233
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->d:Lcom/google/googlenav/ui/wizard/ad;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->d:Lcom/google/googlenav/ui/wizard/ad;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/ui/wizard/al;Lcom/google/googlenav/ui/wizard/L;)V

    .line 235
    invoke-virtual {p2}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    .line 236
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    .line 237
    new-instance v2, Lcom/google/googlenav/ui/wizard/aL;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/wizard/aL;-><init>(LaH/m;Lcom/google/googlenav/android/aa;)V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->p:Lcom/google/googlenav/ui/wizard/aL;

    .line 239
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/aU;)Lcom/google/googlenav/aU;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->m:Lcom/google/googlenav/aU;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 466
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ac()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 467
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 470
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->k()I

    .line 473
    :cond_22
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v1, p2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->h(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/T;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/T;-><init>(Lcom/google/googlenav/ui/wizard/S;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 515
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/S;Ljava/util/List;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/S;->b(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/S;)LaG/h;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->o:LaG/h;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/h;)Z
    .registers 3
    .parameter

    .prologue
    .line 433
    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/google/googlenav/h;->f()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/google/googlenav/h;->k()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private b(Ljava/util/List;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 518
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_9

    .line 521
    :goto_8
    return v0

    :cond_9
    invoke-static {}, Lcom/google/googlenav/friend/aD;->j()Lcom/google/googlenav/friend/aD;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/aD;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_8
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/jv;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->l:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method private c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/an;->b(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->p:Lcom/google/googlenav/ui/wizard/aL;

    new-instance v1, Lcom/google/googlenav/ui/wizard/af;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/af;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/aL;->a(LaH/A;)V

    .line 306
    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ah;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    return-object v0
.end method

.method private d()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 327
    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v1

    .line 328
    const-string v0, "wifi"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 329
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/ArrayList;

    .line 330
    new-instance v2, LaL/g;

    new-instance v3, Lcom/google/googlenav/ui/wizard/ag;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/googlenav/ui/wizard/ag;-><init>(Lcom/google/googlenav/ui/wizard/S;Lcom/google/googlenav/ui/wizard/T;)V

    invoke-direct {v2, v1, v0, v3, v5}, LaL/g;-><init>(Landroid/content/Context;Landroid/net/wifi/WifiManager;LaL/i;I)V

    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    .line 331
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    invoke-virtual {v0}, LaL/g;->a()V

    .line 332
    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/aF;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->f:Lcom/google/googlenav/ui/wizard/aF;

    return-object v0
.end method

.method private e()V
    .registers 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    if-eqz v0, :cond_9

    .line 339
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->q:LaL/g;

    invoke-virtual {v0}, LaL/g;->b()V

    .line 343
    :cond_9
    return-void
.end method

.method private f()LaH/h;
    .registers 6

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->n:Lcom/google/googlenav/ui/ak;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->b()LaH/h;

    move-result-object v0

    .line 369
    if-eqz v0, :cond_2f

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_2f

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0}, LaH/h;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long v1, v1, v3

    if-gez v1, :cond_2f

    .line 374
    :goto_2e
    return-object v0

    :cond_2f
    const/4 v0, 0x0

    goto :goto_2e
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/S;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    return-object v0
.end method

.method private g()V
    .registers 6

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    .line 423
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->r:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/an;->y()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/ah;->a(Lcom/google/googlenav/h;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)V

    .line 425
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    const/16 v1, 0xb4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(I)V

    .line 426
    return-void
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/S;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->g()V

    return-void
.end method

.method private h()V
    .registers 6

    .prologue
    const/16 v1, 0xae

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 440
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/S;->g:Z

    if-eqz v0, :cond_38

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v0}, Lcom/google/googlenav/h;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_38

    .line 441
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v0}, Lcom/google/googlenav/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v3}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    :goto_2f
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    .line 457
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->b:Lcom/google/googlenav/ui/wizard/ac;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/ac;->a()V

    .line 458
    return-void

    .line 449
    :cond_38
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    invoke-virtual {v2}, Lcom/google/googlenav/h;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lcom/google/googlenav/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_2f
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/S;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    return-void
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/ac;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->b:Lcom/google/googlenav/ui/wizard/ac;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/wizard/an;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/h;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/aU;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->m:Lcom/google/googlenav/aU;

    return-object v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/wizard/S;)Lcom/google/googlenav/ui/ak;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->n:Lcom/google/googlenav/ui/ak;

    return-object v0
.end method

.method static synthetic n(Lcom/google/googlenav/ui/wizard/S;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->h()V

    return-void
.end method

.method static synthetic o(Lcom/google/googlenav/ui/wizard/S;)LaH/h;
    .registers 2
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->f()LaH/h;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/wizard/aC;
    .registers 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->c:Lcom/google/googlenav/ui/wizard/aC;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/h;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 272
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->d()V

    .line 273
    if-nez p1, :cond_12

    .line 274
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ah;->a()V

    .line 275
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/S;->k:Z

    .line 276
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->c()V

    .line 282
    :goto_11
    return-void

    .line 278
    :cond_12
    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V

    .line 279
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/S;->a(Ljava/util/List;)V

    .line 280
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ah;->a()V

    goto :goto_11
.end method

.method a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 390
    sget-boolean v0, Lcom/google/googlenav/ui/wizard/S;->a:Z

    if-nez v0, :cond_10

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/S;->b(Lcom/google/googlenav/h;)Z

    move-result v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 391
    :cond_10
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->i:Lcom/google/googlenav/h;

    .line 392
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/an;->a(Lcom/google/googlenav/h;Lcom/google/googlenav/friend/i;)V

    .line 393
    if-eqz p2, :cond_1f

    .line 394
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->n()V

    .line 398
    :goto_1e
    return-void

    .line 396
    :cond_1f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    const/16 v1, 0x604

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->a(I)V

    goto :goto_1e
.end method

.method public a(Lcom/google/googlenav/ui/wizard/an;)V
    .registers 2
    .parameter

    .prologue
    .line 247
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    .line 250
    if-nez p1, :cond_7

    .line 251
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->e()V

    .line 253
    :cond_7
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 351
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/S;->f()LaH/h;

    move-result-object v0

    .line 352
    if-nez v0, :cond_e

    .line 353
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    const/16 v1, 0xbc

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/an;->b(I)V

    .line 360
    :goto_d
    return-void

    .line 355
    :cond_e
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/ui/wizard/an;->b(Ljava/lang/String;)V

    .line 356
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v3

    invoke-virtual {v3}, LaN/B;->e()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/ah;->a(II)V

    .line 358
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->e:Lcom/google/googlenav/ui/wizard/ah;

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/ui/wizard/ah;->a(LaH/h;Ljava/lang/String;)V

    goto :goto_d
.end method

.method a(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 407
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/S;->j:Ljava/util/List;

    .line 410
    if-eqz p1, :cond_9

    .line 411
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/an;->b(Ljava/util/List;)V

    .line 413
    :cond_9
    return-void
.end method

.method public b()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 290
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->m()Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->o()Z

    move-result v1

    if-nez v1, :cond_12

    .line 299
    :cond_11
    :goto_11
    return v0

    .line 294
    :cond_12
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/S;->k:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/an;->o()Z

    move-result v1

    if-nez v1, :cond_11

    .line 295
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->h()V

    .line 296
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/S;->h:Lcom/google/googlenav/ui/wizard/an;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/an;->n()V

    .line 297
    const/4 v0, 0x1

    goto :goto_11
.end method
