.class public Lcom/google/googlenav/ui/wizard/jv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Lbm/i;

.field private static l:Z


# instance fields
.field protected final a:Lcom/google/googlenav/J;

.field protected final b:LaN/p;

.field protected final c:LaN/u;

.field public final d:Lcom/google/googlenav/ui/wizard/jC;

.field private final e:Lcom/google/googlenav/ui/wizard/z;

.field private final g:LaH/m;

.field private final h:Lcom/google/googlenav/android/aa;

.field private i:Z

.field private final j:Lcom/google/googlenav/aA;

.field private final k:Lcom/google/googlenav/friend/j;

.field private final m:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 115
    new-instance v0, Lbm/i;

    const-string v1, "layers open"

    const-string v2, "lo"

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lbm/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/googlenav/ui/wizard/jv;->f:Lbm/i;

    .line 138
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/googlenav/ui/wizard/jv;->l:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/aa;LaN/p;LaN/u;LaH/m;Lcom/google/googlenav/aA;ZLcom/google/googlenav/ui/wizard/z;Lcom/google/googlenav/J;Lcom/google/googlenav/friend/j;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    .line 151
    new-instance v0, Lcom/google/googlenav/ui/wizard/jC;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/jC;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    .line 152
    iput-object p8, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    .line 153
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/jv;->g:LaH/m;

    .line 154
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    .line 155
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/jv;->b:LaN/p;

    .line 156
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    .line 157
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    .line 158
    iput-boolean p6, p0, Lcom/google/googlenav/ui/wizard/jv;->i:Z

    .line 159
    iput-object p7, p0, Lcom/google/googlenav/ui/wizard/jv;->e:Lcom/google/googlenav/ui/wizard/z;

    .line 160
    iput-object p9, p0, Lcom/google/googlenav/ui/wizard/jv;->k:Lcom/google/googlenav/friend/j;

    .line 164
    return-void
.end method

.method private a(Ljava/lang/String;LaM/g;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 821
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->b(Ljava/lang/String;)V

    .line 825
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0, p2}, LaM/f;->a(LaM/g;)V

    .line 826
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    invoke-interface {v0, p1}, Lcom/google/googlenav/aA;->c(Ljava/lang/String;)V

    .line 502
    return-void
.end method


# virtual methods
.method public A()LaH/m;
    .registers 2

    .prologue
    .line 1522
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->g:LaH/m;

    return-object v0
.end method

.method public B()Lcom/google/googlenav/ui/ak;
    .registers 2

    .prologue
    .line 1526
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    return-object v0
.end method

.method public C()LaN/p;
    .registers 2

    .prologue
    .line 1530
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->b:LaN/p;

    return-object v0
.end method

.method public D()LaN/u;
    .registers 2

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    return-object v0
.end method

.method public E()Lcom/google/googlenav/aA;
    .registers 2

    .prologue
    .line 1538
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    return-object v0
.end method

.method public F()Lcom/google/googlenav/J;
    .registers 2

    .prologue
    .line 1542
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    return-object v0
.end method

.method public G()LaB/s;
    .registers 2

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    return-object v0
.end method

.method public H()Lcom/google/googlenav/friend/j;
    .registers 2

    .prologue
    .line 1554
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->k:Lcom/google/googlenav/friend/j;

    return-object v0
.end method

.method public I()V
    .registers 3

    .prologue
    .line 1569
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->e()Lax/b;

    move-result-object v0

    .line 1570
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lax/k;)V

    .line 1571
    return-void
.end method

.method public J()V
    .registers 3

    .prologue
    .line 1574
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->e()Lax/b;

    move-result-object v0

    .line 1575
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lax/k;)V

    .line 1576
    return-void
.end method

.method public K()V
    .registers 3

    .prologue
    .line 1579
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->h()Lcom/google/googlenav/ui/wizard/bN;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/bN;->e()Lax/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/l;)V

    .line 1581
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->J()V

    .line 1582
    return-void
.end method

.method public L()Lcom/google/googlenav/ui/wizard/jC;
    .registers 2

    .prologue
    .line 1606
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    return-object v0
.end method

.method public M()V
    .registers 2

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ai()V

    .line 1619
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1620
    return-void
.end method

.method public N()Z
    .registers 2

    .prologue
    .line 1683
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 1243
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(Lat/b;)I

    move-result v0

    return v0
.end method

.method a()V
    .registers 3

    .prologue
    .line 219
    const/16 v0, 0xa

    const-string v1, "i"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->c()V

    .line 222
    return-void
.end method

.method public a(ILat/a;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1558
    const/4 v0, 0x0

    .line 1559
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/jA;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/googlenav/ui/wizard/jA;-><init>(Lcom/google/googlenav/ui/wizard/jv;ILat/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 1565
    return-void
.end method

.method public a(ILcom/google/googlenav/ui/wizard/cc;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 552
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 554
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->j()Lcom/google/googlenav/ui/wizard/cb;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/cb;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    .line 556
    :cond_1d
    return-void
.end method

.method public a(I[Lba/f;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lba/b;->a(I[Lba/f;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    .line 495
    return-void
.end method

.method public a(LaG/h;)V
    .registers 3
    .parameter

    .prologue
    .line 769
    if-nez p1, :cond_b

    .line 770
    new-instance p1, LaG/h;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v0

    invoke-direct {p1, v0}, LaG/h;-><init>(LaB/s;)V

    .line 772
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->c()Lcom/google/googlenav/ui/wizard/G;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/G;->a(LaG/h;)V

    .line 773
    return-void
.end method

.method public a(LaN/B;)V
    .registers 3
    .parameter

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, v0}, Lba/b;->a(LaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    .line 447
    return-void
.end method

.method public a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;LaN/H;IILcom/google/googlenav/ui/wizard/C;Z)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->I()Lcom/google/googlenav/ui/wizard/br;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move-object v7, p4

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/google/googlenav/ui/wizard/br;->a(LaN/B;Lo/D;Ljava/lang/String;LaN/H;IILjava/lang/String;Lcom/google/googlenav/ui/wizard/C;Z)V

    .line 1046
    return-void
.end method

.method public a(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/b;)V

    .line 273
    invoke-virtual {p1}, Lax/b;->E()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lax/b;->F()Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-virtual {p1}, Lax/b;->d()B

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_36

    .line 276
    :cond_1c
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 277
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->a()V

    .line 279
    :cond_2d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    .line 281
    :cond_36
    return-void
.end method

.method public a(Lax/b;LaN/H;IILjava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->M()Lcom/google/googlenav/ui/wizard/bV;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/bV;->a(Lax/b;LaN/H;IILjava/util/List;)V

    .line 1062
    return-void
.end method

.method public a(Lax/j;)V
    .registers 3
    .parameter

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ca;->b(Lax/j;)V

    .line 333
    return-void
.end method

.method public a(Lbf/am;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/gk;->a(Lbf/am;I)V

    .line 963
    return-void
.end method

.method public a(Lbf/am;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 836
    sget-object v0, Lcom/google/googlenav/ui/wizard/jv;->f:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->a()V

    .line 837
    const/16 v0, 0x43

    const-string v1, "d"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 840
    if-eqz p2, :cond_13

    .line 841
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->i()V

    .line 848
    :cond_13
    invoke-static {}, Lcom/google/googlenav/layer/f;->a()Lcom/google/googlenav/layer/f;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/layer/f;->a(Z)V

    .line 850
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->r()Lcom/google/googlenav/ui/wizard/cC;

    move-result-object v0

    .line 853
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cC;->b(Lbf/am;)Lcom/google/googlenav/ui/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/av;->a()LaM/g;

    move-result-object v2

    invoke-virtual {v1, v2}, LaM/f;->e(LaM/g;)V

    .line 856
    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cC;->a(Lbf/am;)V

    .line 857
    sget-object v0, Lcom/google/googlenav/ui/wizard/jv;->f:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    .line 858
    return-void
.end method

.method public a(Lcom/google/googlenav/L;)V
    .registers 3
    .parameter

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->t()Lcom/google/googlenav/ui/wizard/cu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cu;->a(Lcom/google/googlenav/L;)V

    .line 658
    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1085
    invoke-virtual {p1, p3}, Lcom/google/googlenav/aZ;->g(I)Lcom/google/googlenav/bc;

    move-result-object v1

    .line 1086
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/google/googlenav/bc;->d()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1097
    :cond_c
    :goto_c
    return-void

    .line 1090
    :cond_d
    new-instance v0, Lcom/google/googlenav/bc;

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v0, p3, v2, v3}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    .line 1091
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v2

    if-eqz v2, :cond_37

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_37

    .line 1093
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bc;

    .line 1095
    :cond_37
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->k()Lcom/google/googlenav/ui/wizard/ce;

    move-result-object v2

    invoke-virtual {v2, p1, p2, v1, v0}, Lcom/google/googlenav/ui/wizard/ce;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Lcom/google/googlenav/bc;Lcom/google/googlenav/bc;)V

    goto :goto_c
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1070
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->E()Lcom/google/googlenav/ui/wizard/hy;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    .line 1072
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 646
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->q()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 647
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->O()Lcom/google/googlenav/ui/wizard/iH;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iH;->a(Lcom/google/googlenav/ai;)V

    .line 648
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->O()Lcom/google/googlenav/ui/wizard/iH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iH;->e()V

    .line 649
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->O()Lcom/google/googlenav/ui/wizard/iH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/iH;->j()V

    .line 651
    :cond_25
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v7, 0x55

    .line 938
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    .line 941
    const-string v0, "ns"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_62

    if-nez p4, :cond_13

    if-eqz v2, :cond_62

    .line 943
    :cond_13
    const-string v3, "e"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    if-nez p4, :cond_3a

    move-object v0, v1

    :goto_1c
    aput-object v0, v4, v5

    const/4 v0, 0x1

    if-eqz v2, :cond_27

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4e

    :cond_27
    :goto_27
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 955
    :goto_30
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->C()Lcom/google/googlenav/ui/wizard/hh;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    .line 956
    return-void

    .line 943
    :cond_3a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "s="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1c

    :cond_4e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_27

    .line 951
    :cond_62
    const-string v0, "e"

    invoke-static {v7, v0}, Lbm/m;->a(ILjava/lang/String;)V

    goto :goto_30
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 864
    invoke-virtual {p0, p1, v0, p2, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V

    .line 865
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/fA;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v7, 0x65

    const/4 v6, 0x0

    .line 873
    invoke-static {p1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    .line 876
    const-string v0, "ns"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_62

    if-nez p3, :cond_14

    if-eqz v2, :cond_62

    .line 878
    :cond_14
    const-string v3, "e"

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    if-nez p3, :cond_3a

    move-object v0, v1

    :goto_1c
    aput-object v0, v4, v6

    const/4 v0, 0x1

    if-eqz v2, :cond_27

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4e

    :cond_27
    :goto_27
    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 889
    :goto_30
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->G()Lcom/google/googlenav/ui/wizard/fq;

    move-result-object v0

    invoke-virtual {v0, p1, p2, v6, p4}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/fA;)V

    .line 890
    return-void

    .line 878
    :cond_3a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1c

    :cond_4e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "u="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_27

    .line 886
    :cond_62
    const-string v0, "e"

    invoke-static {v7, v0}, Lbm/m;->a(ILjava/lang/String;)V

    goto :goto_30
.end method

.method public a(Lcom/google/googlenav/bZ;)V
    .registers 5
    .parameter

    .prologue
    .line 288
    const/16 v0, 0x54

    const-string v1, "ts"

    const-string v2, "c"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->f()Lcom/google/googlenav/ui/wizard/bx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bx;->a(Lcom/google/googlenav/bZ;)V

    .line 292
    return-void
.end method

.method public a(Lcom/google/googlenav/bu;Lcom/google/googlenav/br;Lcom/google/googlenav/ui/wizard/jj;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->T()Lcom/google/googlenav/ui/wizard/jb;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/jb;->a(Lcom/google/googlenav/bu;Lcom/google/googlenav/br;Lcom/google/googlenav/ui/wizard/jj;)V

    .line 566
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->P()Lcom/google/googlenav/ui/wizard/iL;

    move-result-object v0

    invoke-virtual {v0, p4, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/iL;->a(Lcom/google/googlenav/ui/wizard/iS;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;Z)V

    .line 419
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/wizard/ba;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->v()Lcom/google/googlenav/ui/wizard/aZ;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/aZ;->a(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/wizard/ba;)V

    .line 628
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1381
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1385
    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(Lcom/google/googlenav/ui/r;)V

    .line 1386
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;)V
    .registers 3
    .parameter

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 215
    :goto_6
    return-void

    .line 205
    :cond_7
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/jv;->i:Z

    if-nez v0, :cond_1d

    .line 206
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aj()Z

    move-result v0

    if-nez v0, :cond_19

    .line 207
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lcom/google/googlenav/ui/s;)V

    goto :goto_6

    .line 209
    :cond_19
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->a()V

    goto :goto_6

    .line 213
    :cond_1d
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lcom/google/googlenav/ui/s;)V

    goto :goto_6
.end method

.method public a(Lcom/google/googlenav/ui/s;LaH/h;Lcom/google/googlenav/ui/view/android/aB;ZLcom/google/googlenav/ui/wizard/gb;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 663
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->B()Lcom/google/googlenav/ui/wizard/fY;

    move-result-object v6

    new-instance v0, Lcom/google/googlenav/ui/wizard/ga;

    move-object v1, p1

    move-object v2, p5

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ga;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/wizard/gb;LaH/h;Lcom/google/googlenav/ui/view/android/aB;Z)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/wizard/fY;->a(Lcom/google/googlenav/ui/wizard/ga;)V

    .line 666
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Ljava/lang/String;ZZZLcom/google/googlenav/ui/wizard/R;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 683
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->j()Z

    move-result v0

    if-nez v0, :cond_b

    .line 716
    :goto_a
    return-void

    .line 687
    :cond_b
    new-instance v0, Lcom/google/googlenav/ui/wizard/Q;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p7

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/Q;-><init>(Lcom/google/googlenav/ui/s;Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/R;ZZZ)V

    .line 691
    if-eqz p3, :cond_32

    .line 695
    invoke-static {p3}, Lcom/google/googlenav/friend/ad;->d(Ljava/lang/String;)V

    .line 696
    invoke-static {p2}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/h;)Ljava/lang/String;

    move-result-object v1

    .line 697
    if-eqz v1, :cond_32

    .line 698
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 701
    const-string v3, "u"

    invoke-static {v3, v1, v2}, Lbm/m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 703
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/friend/ad;->d(Ljava/lang/String;)V

    .line 707
    :cond_32
    new-instance v1, Lcom/google/googlenav/ui/wizard/jB;

    invoke-static {}, Lcom/google/googlenav/friend/W;->e()Lcom/google/googlenav/friend/W;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/google/googlenav/ui/wizard/jB;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/friend/bi;Lcom/google/googlenav/ui/wizard/Q;Lcom/google/googlenav/ui/wizard/jw;)V

    .line 712
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->n()Lcom/google/googlenav/ui/wizard/M;

    move-result-object v2

    invoke-virtual {p0, v2, v1, p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/M;Lcom/google/googlenav/friend/bd;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/wizard/Q;)V

    goto :goto_a
.end method

.method public a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->z()Lcom/google/googlenav/ui/wizard/eP;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V

    .line 431
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/android/aL;)V
    .registers 3
    .parameter

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->S()Lcom/google/googlenav/ui/wizard/ew;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ew;->a(Lcom/google/googlenav/ui/view/android/aL;)V

    .line 1105
    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/C;)V
    .registers 3
    .parameter

    .prologue
    .line 171
    instance-of v0, p1, Lcom/google/googlenav/ui/wizard/jt;

    if-nez v0, :cond_7

    .line 172
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    .line 177
    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 178
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 180
    :cond_14
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/E;)V
    .registers 3
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/D;->a(Lcom/google/googlenav/ui/wizard/E;)V

    .line 247
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->j()V

    .line 248
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/D;->a(Lcom/google/googlenav/ui/wizard/F;Lcom/google/googlenav/ui/wizard/E;)V

    .line 253
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->j()V

    .line 254
    return-void
.end method

.method a(Lcom/google/googlenav/ui/wizard/M;Lcom/google/googlenav/friend/bd;Lcom/google/googlenav/ui/s;Lcom/google/googlenav/ui/wizard/Q;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 726
    invoke-static {}, Lcom/google/googlenav/friend/W;->j()Z

    move-result v0

    if-nez v0, :cond_18

    .line 731
    invoke-virtual {p3}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 732
    if-eqz v0, :cond_14

    .line 733
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    .line 735
    :cond_14
    invoke-virtual {p2, p0}, Lcom/google/googlenav/friend/bd;->a(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 739
    :goto_17
    return-void

    .line 737
    :cond_18
    invoke-virtual {p1, p4}, Lcom/google/googlenav/ui/wizard/M;->a(Lcom/google/googlenav/ui/wizard/Q;)V

    goto :goto_17
.end method

.method public a(Lcom/google/googlenav/ui/wizard/bo;)V
    .registers 3
    .parameter

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->K()Lcom/google/googlenav/ui/wizard/iy;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iy;->a(Lcom/google/googlenav/ui/wizard/bo;)V

    .line 1117
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/ct;)V
    .registers 3
    .parameter

    .prologue
    .line 746
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->s()Lcom/google/googlenav/ui/wizard/cr;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/cr;->a(Lcom/google/googlenav/ui/wizard/ct;)V

    .line 747
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/dF;)V
    .registers 3
    .parameter

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v0

    .line 637
    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 638
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/el;)V
    .registers 3
    .parameter

    .prologue
    .line 765
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->p()Lcom/google/googlenav/ui/wizard/eh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/eh;->a(Lcom/google/googlenav/ui/wizard/el;)V

    .line 766
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/ep;)V
    .registers 3
    .parameter

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->q()Lcom/google/googlenav/ui/wizard/em;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/em;->a(Lcom/google/googlenav/ui/wizard/ep;)V

    .line 751
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/gJ;)V
    .registers 3
    .parameter

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->o()Lcom/google/googlenav/ui/wizard/gG;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/gG;->a(Lcom/google/googlenav/ui/wizard/gJ;)V

    .line 758
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/iG;)V
    .registers 3
    .parameter

    .prologue
    .line 530
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 532
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->N()Lcom/google/googlenav/ui/wizard/iC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iC;->a(Lcom/google/googlenav/ui/wizard/iG;)V

    .line 534
    :cond_1d
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/x;)V
    .registers 3
    .parameter

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->a()Lcom/google/googlenav/ui/wizard/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/q;->a(Lcom/google/googlenav/ui/wizard/x;)V

    .line 402
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 380
    invoke-virtual {p0, v0, p1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lba/b;->a(Ljava/lang/String;ILaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    .line 476
    return-void
.end method

.method public a(Ljava/lang/String;LaN/B;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->c:LaN/u;

    invoke-virtual {v0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lba/b;->a(Ljava/lang/String;LaN/B;LaN/Y;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/jv;->d(Ljava/lang/String;)V

    .line 465
    return-void
.end method

.method public a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1017
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    .line 1018
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->W()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jt;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 1020
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/ui/s;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1151
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->F()Lcom/google/googlenav/ui/wizard/ia;

    move-result-object v0

    invoke-virtual {p4}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ia;->a(Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lbf/m;Lcom/google/googlenav/J;Lbf/am;)V

    .line 1153
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/google/googlenav/ui/wizard/bk;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    .line 586
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/googlenav/ui/wizard/bk;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    .line 598
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;LaM/g;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 789
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 790
    invoke-direct {p0, p2, p3}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;LaM/g;)V

    .line 792
    :cond_d
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    invoke-interface {v0, p1, p3, p2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->J()Lcom/google/googlenav/ui/wizard/ib;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/ib;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/googlenav/ui/wizard/bo;)V

    .line 1112
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->d()Lcom/google/googlenav/ui/wizard/bk;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/bk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 607
    return-void
.end method

.method public a(Ljava/util/List;Lcom/google/googlenav/ui/wizard/cA;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 571
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->i()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 573
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->m()Lcom/google/googlenav/ui/wizard/cx;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/cx;->a(Ljava/util/List;Lcom/google/googlenav/ui/wizard/cA;)V

    .line 575
    :cond_1d
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 225
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/jv;->i:Z

    .line 226
    return-void
.end method

.method public a(ZLcom/google/googlenav/ui/wizard/db;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 632
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->w()Lcom/google/googlenav/ui/wizard/cZ;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/cZ;->a(ZLcom/google/googlenav/ui/wizard/db;)V

    .line 633
    return-void
.end method

.method public a([Lcom/google/googlenav/aw;IZ)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 922
    const/16 v0, 0x65

    const-string v1, "vg"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "st="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "gt=t"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 929
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->l()Lcom/google/googlenav/ui/wizard/cf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/cf;->a(Ljava/lang/String;[Lcom/google/googlenav/aw;IZ)V

    .line 930
    return-void
.end method

.method public a(Lat/a;)Z
    .registers 4
    .parameter

    .prologue
    .line 1215
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1216
    if-eqz v0, :cond_25

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-virtual {p1}, Lat/a;->b()I

    move-result v0

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_23

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_23

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_25

    :cond_23
    const/4 v0, 0x1

    :goto_24
    return v0

    :cond_25
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public b(LaN/B;)I
    .registers 3
    .parameter

    .prologue
    .line 1253
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(LaN/B;)I

    move-result v0

    return v0
.end method

.method public b(Lat/a;)I
    .registers 3
    .parameter

    .prologue
    .line 1227
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->a(Lat/a;)I

    move-result v0

    return v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->b()Lcom/google/googlenav/ui/wizard/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/D;->a()V

    .line 258
    return-void
.end method

.method public b(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->f()Lcom/google/googlenav/ui/wizard/bx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bx;->a(Lax/b;)V

    .line 285
    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)V
    .registers 3
    .parameter

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Q()Lcom/google/googlenav/ui/wizard/iU;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/iU;->a(Lcom/google/googlenav/ai;)V

    .line 1066
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1394
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1395
    if-eqz v0, :cond_9

    .line 1396
    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Lcom/google/googlenav/ui/r;)V

    .line 1398
    :cond_9
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/s;)V
    .registers 3
    .parameter

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->z()Lcom/google/googlenav/ui/wizard/eP;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/J;)V

    .line 439
    return-void
.end method

.method b(Lcom/google/googlenav/ui/wizard/C;)V
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 188
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 9
    .parameter

    .prologue
    .line 984
    new-instance v2, Lcom/google/googlenav/ui/wizard/jx;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/jx;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 1001
    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 901
    const/16 v0, 0x65

    const-string v1, "vg"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "st="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "gt=m"

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 908
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->l()Lcom/google/googlenav/ui/wizard/cf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, v5}, Lcom/google/googlenav/ui/wizard/cf;->a(Ljava/lang/String;[Lcom/google/googlenav/aw;IZ)V

    .line 909
    return-void
.end method

.method public c(Lat/a;)I
    .registers 3
    .parameter

    .prologue
    .line 1235
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Lat/a;)I

    move-result v0

    return v0
.end method

.method c()V
    .registers 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->U()Lcom/google/googlenav/ui/wizard/jk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jk;->j()V

    .line 510
    return-void
.end method

.method public c(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ca;->a(Lax/b;)V

    .line 353
    return-void
.end method

.method public c(Lcom/google/googlenav/ui/s;)V
    .registers 3
    .parameter

    .prologue
    .line 517
    sget-boolean v0, Lcom/google/googlenav/ui/wizard/jv;->l:Z

    if-nez v0, :cond_5

    .line 526
    :cond_4
    :goto_4
    return-void

    .line 522
    :cond_5
    if-eqz p1, :cond_4

    .line 523
    const/4 v0, 0x0

    .line 524
    invoke-virtual {p1, v0}, Lcom/google/googlenav/ui/s;->e(Z)V

    goto :goto_4
.end method

.method public c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->R()Lcom/google/googlenav/ui/wizard/fl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/fl;->a(Ljava/lang/String;)V

    .line 1101
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 613
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aA()Z

    move-result v0

    if-nez v0, :cond_b

    .line 618
    :goto_a
    return-void

    .line 617
    :cond_b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->u()Lcom/google/googlenav/ui/wizard/cH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cH;->j()V

    goto :goto_a
.end method

.method public d(Lax/b;)V
    .registers 3
    .parameter

    .prologue
    .line 1355
    instance-of v0, p1, Lax/w;

    if-eqz v0, :cond_10

    .line 1356
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    .line 1359
    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/bv;->a(Lax/b;)V

    .line 1360
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    .line 1362
    :cond_10
    return-void
.end method

.method public e()V
    .registers 4

    .prologue
    .line 805
    const/4 v0, 0x0

    .line 806
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/jw;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/jw;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 812
    return-void
.end method

.method public e(Lax/b;)V
    .registers 4
    .parameter

    .prologue
    .line 1591
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Y()Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->Z()Z

    move-result v0

    if-eqz v0, :cond_2d

    :cond_10
    instance-of v0, p1, Lax/w;

    if-eqz v0, :cond_2d

    .line 1593
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->f()V

    .line 1600
    :goto_1d
    invoke-virtual {p1}, Lax/b;->v()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1601
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, Lax/b;->B()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 1603
    :cond_2c
    return-void

    .line 1594
    :cond_2d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->aa()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 1595
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/ca;->d(Lax/b;)V

    goto :goto_1d

    .line 1597
    :cond_3f
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/wizard/jv;->c(Lax/b;)V

    goto :goto_1d
.end method

.method public f()V
    .registers 2

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->A()Lcom/google/googlenav/ui/wizard/fe;

    move-result-object v0

    .line 967
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fe;->j()V

    .line 968
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 974
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->V()Lcom/google/googlenav/ui/wizard/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eq;->j()V

    .line 975
    return-void
.end method

.method public h()V
    .registers 5

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ae()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1142
    :goto_8
    return-void

    .line 1125
    :cond_9
    new-instance v0, Lcom/google/googlenav/ui/wizard/jy;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/jy;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 1136
    invoke-static {}, LaS/a;->z()LaS/a;

    move-result-object v1

    .line 1137
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/jC;->L()Lcom/google/googlenav/ui/wizard/ik;

    move-result-object v2

    invoke-virtual {v1}, LaS/a;->u()Ljava/util/EnumSet;

    move-result-object v3

    invoke-virtual {v1}, LaS/a;->t()Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/googlenav/ui/wizard/ik;->a(Lcom/google/googlenav/ui/wizard/im;Ljava/util/EnumSet;Ljava/util/EnumSet;)V

    .line 1139
    const/16 v0, 0x61

    const-string v1, "f"

    const-string v2, "o"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public i()V
    .registers 2

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->ah()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1164
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->W()Lcom/google/googlenav/ui/wizard/jt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jt;->e()V

    .line 1167
    :cond_11
    return-void
.end method

.method public j()V
    .registers 2

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gk;->j()V

    .line 1171
    return-void
.end method

.method public k()I
    .registers 3

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    .line 1175
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    .line 1176
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->c()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 1177
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/16 v1, 0x12

    if-ne v0, v1, :cond_21

    .line 1182
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->j:Lcom/google/googlenav/aA;

    invoke-interface {v0}, Lcom/google/googlenav/aA;->j()V

    .line 1183
    const/16 v0, 0xa

    .line 1185
    :goto_20
    return v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public l()Lcom/google/googlenav/ui/wizard/C;
    .registers 2

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1191
    const/4 v0, 0x0

    .line 1193
    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    goto :goto_9
.end method

.method public m()Z
    .registers 3

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    .line 1199
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1200
    const/4 v0, 0x1

    .line 1203
    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method protected n()V
    .registers 3

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->a:Lcom/google/googlenav/J;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    .line 1208
    return-void
.end method

.method public o()Lcom/google/googlenav/ui/wizard/A;
    .registers 4

    .prologue
    .line 1262
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1263
    if-eqz v0, :cond_34

    .line 1264
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    .line 1267
    instance-of v1, v0, Lcom/google/googlenav/ui/wizard/dg;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/dg;->f()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_26

    .line 1270
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->i()Lcom/google/googlenav/ui/wizard/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/ca;->j()V

    .line 1281
    :cond_23
    :goto_23
    sget-object v0, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    :goto_25
    return-object v0

    .line 1271
    :cond_26
    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/bN;

    if-eqz v0, :cond_23

    .line 1272
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->d:Lcom/google/googlenav/ui/wizard/jC;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->e()Lcom/google/googlenav/ui/wizard/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/bv;->j()V

    goto :goto_23

    .line 1274
    :cond_34
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->v()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1275
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    .line 1276
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_23

    goto :goto_25
.end method

.method public p()I
    .registers 5

    .prologue
    .line 1294
    const/4 v0, 0x0

    .line 1295
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_33

    .line 1296
    const/4 v1, 0x0

    .line 1302
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    .line 1303
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->p()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1310
    :goto_22
    if-nez v0, :cond_2c

    .line 1311
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/C;

    .line 1315
    :cond_2c
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->a()V

    .line 1316
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->k()I

    move-result v0

    .line 1318
    :cond_33
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    .line 1319
    return v0

    :cond_37
    move-object v0, v1

    goto :goto_22
.end method

.method public q()V
    .registers 4

    .prologue
    .line 1335
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->P()Lcom/google/googlenav/ui/m;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/bv;->a(Lcom/google/googlenav/ui/m;)Lax/b;

    move-result-object v0

    .line 1337
    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Lax/b;->m()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 1339
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    new-instance v2, Lcom/google/googlenav/ui/wizard/jz;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/wizard/jz;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lax/b;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 1346
    :cond_1f
    return-void
.end method

.method public r()Z
    .registers 2

    .prologue
    .line 1370
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public s()Z
    .registers 2

    .prologue
    .line 1404
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1405
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public t()Z
    .registers 2

    .prologue
    .line 1412
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1413
    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->n()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public u()I
    .registers 3

    .prologue
    .line 1473
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1474
    if-nez v0, :cond_8

    .line 1475
    const/4 v0, 0x0

    .line 1480
    :goto_7
    return v0

    .line 1477
    :cond_8
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->v()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1478
    const/4 v0, 0x1

    goto :goto_7

    .line 1480
    :cond_10
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->ah_()I

    move-result v0

    goto :goto_7
.end method

.method public v()Z
    .registers 2

    .prologue
    .line 1484
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    instance-of v0, v0, Lcom/google/googlenav/ui/wizard/ca;

    return v0
.end method

.method public w()V
    .registers 2

    .prologue
    .line 1497
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1498
    if-eqz v0, :cond_9

    .line 1499
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->af_()V

    .line 1501
    :cond_9
    return-void
.end method

.method public x()V
    .registers 2

    .prologue
    .line 1507
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/jv;->l()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v0

    .line 1508
    if-eqz v0, :cond_9

    .line 1509
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->m()V

    .line 1511
    :cond_9
    return-void
.end method

.method public y()Lcom/google/googlenav/ui/wizard/z;
    .registers 2

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->e:Lcom/google/googlenav/ui/wizard/z;

    return-object v0
.end method

.method public z()Lcom/google/googlenav/android/aa;
    .registers 2

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/jv;->h:Lcom/google/googlenav/android/aa;

    return-object v0
.end method
