.class Lcom/google/googlenav/ui/wizard/eE;
.super Lcom/google/googlenav/ui/wizard/eI;
.source "SourceFile"


# instance fields
.field private final a:LaR/u;

.field private final b:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(LaR/u;)V
    .registers 3
    .parameter

    .prologue
    .line 29
    const/16 v0, 0x2ec

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eI;-><init>(Ljava/lang/String;)V

    .line 26
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eE;->b:Ljava/text/DateFormat;

    .line 30
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eE;->a:LaR/u;

    .line 31
    return-void
.end method

.method private a(LaR/L;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eE;->b:Ljava/text/DateFormat;

    invoke-virtual {p1}, LaR/L;->i()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 36
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x4f4

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .registers 14

    .prologue
    const/4 v8, 0x0

    .line 41
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eE;->a:LaR/u;

    invoke-interface {v0}, LaR/u;->b()Ljava/util/List;

    move-result-object v9

    .line 42
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    .line 43
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    move v7, v8

    .line 44
    :goto_11
    if-ge v7, v10, :cond_4d

    .line 45
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eE;->a:LaR/u;

    invoke-interface {v9, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LaR/L;

    .line 46
    if-eqz v6, :cond_2e

    invoke-virtual {v6}, LaR/L;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 44
    :cond_2e
    :goto_2e
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_11

    .line 50
    :cond_32
    new-instance v0, Lcom/google/googlenav/ui/view/android/be;

    invoke-virtual {v6}, LaR/L;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/wizard/eE;->a(LaR/L;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, LaR/L;->i()J

    move-result-wide v3

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/4 v12, 0x4

    invoke-direct {v5, v12, v8, v6}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/be;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;JLcom/google/googlenav/ui/view/a;)V

    .line 58
    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 60
    :cond_4d
    return-object v11
.end method
