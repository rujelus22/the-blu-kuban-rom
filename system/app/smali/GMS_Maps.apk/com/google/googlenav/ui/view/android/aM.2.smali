.class public Lcom/google/googlenav/ui/view/android/aM;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Lbf/bk;

.field private final b:Ljava/lang/CharSequence;

.field private final c:Ljava/lang/CharSequence;

.field private final d:Ljava/lang/CharSequence;

.field private final e:Lan/f;

.field private final f:LaN/B;

.field private final g:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lan/f;LaN/B;ILbf/bk;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    sget-object v0, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aM;->b:Ljava/lang/CharSequence;

    .line 72
    if-nez p2, :cond_1e

    move-object v0, v1

    :goto_f
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aM;->c:Ljava/lang/CharSequence;

    .line 74
    if-nez p3, :cond_25

    :goto_13
    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->d:Ljava/lang/CharSequence;

    .line 76
    iput-object p4, p0, Lcom/google/googlenav/ui/view/android/aM;->e:Lan/f;

    .line 77
    iput-object p5, p0, Lcom/google/googlenav/ui/view/android/aM;->f:LaN/B;

    .line 78
    iput p6, p0, Lcom/google/googlenav/ui/view/android/aM;->g:I

    .line 79
    iput-object p7, p0, Lcom/google/googlenav/ui/view/android/aM;->a:Lbf/bk;

    .line 80
    return-void

    .line 72
    :cond_1e
    sget-object v0, Lcom/google/googlenav/ui/aV;->D:Lcom/google/googlenav/ui/aV;

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_f

    .line 74
    :cond_25
    sget-object v0, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    invoke-static {p3, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto :goto_13
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 169
    const/4 v0, 0x2

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 4
    .parameter

    .prologue
    .line 116
    new-instance v1, Lcom/google/googlenav/ui/view/android/aN;

    invoke-direct {v1}, Lcom/google/googlenav/ui/view/android/aN;-><init>()V

    .line 118
    iput-object p1, v1, Lcom/google/googlenav/ui/view/android/aN;->a:Landroid/view/View;

    .line 119
    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->b:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f100198

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->c:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f100262

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->d:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f100033

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->e:Landroid/widget/ImageView;

    .line 123
    const v0, 0x7f10019b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->f:Lcom/google/googlenav/ui/view/android/DistanceView;

    .line 124
    const v0, 0x7f10019c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/HeadingView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->g:Lcom/google/googlenav/ui/view/android/HeadingView;

    .line 126
    new-instance v0, Lcom/google/googlenav/ui/view/android/aO;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/aO;-><init>(Lcom/google/googlenav/ui/view/android/aM;)V

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/aN;->h:Lcom/google/googlenav/ui/view/android/aO;

    .line 128
    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 133
    check-cast p2, Lcom/google/googlenav/ui/view/android/aN;

    .line 136
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->e:Lan/f;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/ImageView;Lan/f;)V

    .line 148
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aM;->f:LaN/B;

    if-eqz v0, :cond_2b

    .line 149
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->f:Lcom/google/googlenav/ui/view/android/DistanceView;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/android/aN;->g:Lcom/google/googlenav/ui/view/android/HeadingView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aM;->f:LaN/B;

    invoke-static {v0, v1, v2}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    .line 153
    :cond_2b
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->h:Lcom/google/googlenav/ui/view/android/aO;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/aM;->g:I

    iput v1, v0, Lcom/google/googlenav/ui/view/android/aO;->a:I

    .line 154
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/aN;->a:Landroid/view/View;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/android/aN;->h:Lcom/google/googlenav/ui/view/android/aO;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 155
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 159
    const v0, 0x7f0400bb

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    const/16 v2, 0xa

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_10

    .line 183
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 186
    :cond_10
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->c:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1c

    .line 187
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 188
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 191
    :cond_1c
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_28

    .line 192
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 193
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aM;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 196
    :cond_28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
