.class public Lcom/google/googlenav/ui/view/dialog/bo;
.super Lcom/google/googlenav/ui/view/dialog/bT;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lbf/aJ;)V
    .registers 2
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/bT;-><init>(Lbf/bk;)V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/bo;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 104
    const v0, 0x7f100200

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 106
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->ar()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 107
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bo;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/dialog/bo;->setTitle(Ljava/lang/CharSequence;)V

    .line 108
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 135
    :goto_1f
    return-void

    .line 110
    :cond_20
    const v1, 0x7f1001af

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 111
    const v1, 0x7f1001b0

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v3, 0x7f020351

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 114
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v1

    new-instance v3, Lcom/google/googlenav/ui/view/dialog/bp;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/view/dialog/bp;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-virtual {v1, v2, v3}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    .line 123
    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 124
    const/16 v2, 0x31c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 126
    const v1, 0x7f100209

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 127
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bq;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bq;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    goto :goto_1f
.end method

.method private d(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 211
    const v0, 0x7f100251

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 213
    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 214
    const/16 v2, 0x323

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aR:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    const v1, 0x7f100023

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 218
    const/16 v2, 0x328

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/bs;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/bs;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 227
    return-void
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;IZ)Lbj/H;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 232
    const/16 v0, 0x2bc

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/googlenav/ui/view/dialog/bo;->b(Lcom/google/googlenav/ai;IZI)Lbj/H;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 239
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-gtz v0, :cond_9

    .line 240
    const/4 v0, 0x0

    .line 258
    :cond_8
    :goto_8
    return-object v0

    .line 243
    :cond_9
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/ai;IZI)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    .line 245
    const v1, 0x7f0400c5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    .line 248
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->bJ:Lcom/google/googlenav/ui/aV;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    .line 251
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    .line 254
    invoke-virtual {p1, v3}, Lcom/google/googlenav/ai;->h(I)Lcom/google/googlenav/ap;

    move-result-object v1

    .line 255
    if-eqz v1, :cond_8

    .line 256
    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/view/android/bt;

    goto :goto_8
.end method

.method protected a(Lcom/google/googlenav/F;)Ljava/util/List;
    .registers 10
    .parameter

    .prologue
    const v7, 0x7f0400ca

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 185
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aD()I

    move-result v2

    .line 186
    if-lez v2, :cond_2d

    .line 187
    if-ne v2, v6, :cond_5e

    const/16 v0, 0x31d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 192
    :goto_1e
    new-instance v3, Lbj/bv;

    invoke-direct {v3, v6, v7, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {p0, p1, v5, v2, v5}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 196
    :cond_2d
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-le v0, v2, :cond_4e

    .line 197
    if-lez v2, :cond_71

    const/16 v0, 0x31e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 200
    :goto_3b
    new-instance v3, Lbj/bv;

    invoke-direct {v3, v6, v7, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    invoke-virtual {p0, p1, v2, v0, v5}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Lcom/google/googlenav/F;III)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 205
    :cond_4e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/common/offerslib/y;->a:Lcom/google/android/apps/common/offerslib/y;

    invoke-virtual {v2}, Lcom/google/android/apps/common/offerslib/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/offers/i;->a(Lcom/google/googlenav/aZ;Ljava/lang/String;)V

    .line 207
    return-object v1

    .line 187
    :cond_5e
    const/16 v0, 0x31f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1e

    .line 197
    :cond_71
    const/16 v0, 0x320

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3b
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 4
    .parameter

    .prologue
    .line 139
    const v0, 0x7f020351

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setIcon(I)V

    .line 140
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/view/dialog/br;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/br;-><init>(Lcom/google/googlenav/ui/view/dialog/bo;)V

    invoke-virtual {v0, p1, v1}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;)V

    .line 148
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f1004cb

    if-ne v0, v1, :cond_14

    .line 175
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->b:Lbf/m;

    const/16 v1, 0x76f

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    .line 177
    const/4 v0, 0x1

    .line 179
    :goto_13
    return v0

    :cond_14
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/dialog/bT;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_13
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 3
    .parameter

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method protected c()Landroid/view/View;
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 71
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bo;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 72
    const v0, 0x7f04010b

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 74
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    .line 75
    const v0, 0x7f100026

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    .line 76
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/dialog/bo;->c(Landroid/view/View;)V

    .line 78
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/view/dialog/bo;->d(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-virtual {p0, v1, v0}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Landroid/view/LayoutInflater;Landroid/widget/ListView;)V

    .line 85
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/bo;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/dialog/bo;->a(Landroid/content/Context;I)V

    .line 86
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bo;->c:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/bo;->d:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->F()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 91
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 95
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/bo;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 97
    return-object v2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 152
    sget-object v0, Lcom/google/googlenav/ui/view/dialog/bo;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 153
    const v1, 0x7f11001a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 154
    const v0, 0x7f1004b3

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_1e

    .line 157
    const/16 v1, 0x2ae

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 159
    :cond_1e
    const v0, 0x7f1004cb

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 160
    const/16 v1, 0x2da

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 162
    const/4 v0, 0x1

    return v0
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    const/16 v0, 0x31c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
