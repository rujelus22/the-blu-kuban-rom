.class Lcom/google/googlenav/ui/wizard/hr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaY/b;


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private final b:Lbf/am;

.field private final c:Lcom/google/googlenav/ai;

.field private final d:Lcom/google/googlenav/ui/wizard/hx;

.field private final e:Lcom/google/googlenav/ui/wizard/hq;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    .line 176
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hr;->b:Lbf/am;

    .line 177
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/hr;->c:Lcom/google/googlenav/ai;

    .line 178
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/hr;->d:Lcom/google/googlenav/ui/wizard/hx;

    .line 179
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hr;->e:Lcom/google/googlenav/ui/wizard/hq;

    .line 180
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    .line 211
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x3f4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 212
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    .line 186
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->e:Lcom/google/googlenav/ui/wizard/hq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/hq;->a(Lcom/google/googlenav/ui/wizard/hq;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 206
    :goto_d
    return-void

    .line 191
    :cond_e
    if-eqz p1, :cond_3e

    .line 195
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hr;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aC;->b(Lcom/google/googlenav/ai;)V

    .line 196
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bk()V

    .line 199
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->b:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->f(Z)V

    .line 200
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->d:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->b()V

    .line 201
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x3f5

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->d:Lcom/google/googlenav/ui/wizard/hx;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hx;->a()V

    goto :goto_d

    .line 204
    :cond_3e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hr;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x3f4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    goto :goto_d
.end method
