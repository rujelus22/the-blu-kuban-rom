.class public Lcom/google/googlenav/ui/wizard/fl;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field private b:Lcom/google/googlenav/ui/view/dialog/bk;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    .line 43
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fl;->j()V

    .line 44
    return-void
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/googlenav/ui/wizard/fm;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/fm;-><init>(Lcom/google/googlenav/ui/wizard/fl;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 49
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    .line 50
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_a

    .line 70
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fl;->b()V

    .line 75
    :goto_9
    return-void

    .line 72
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->h:Lcom/google/googlenav/ui/view/android/aL;

    const v1, 0x7f100034

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 73
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->b:Lcom/google/googlenav/ui/view/dialog/bk;

    if-eqz v0, :cond_f

    .line 56
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->b:Lcom/google/googlenav/ui/view/dialog/bk;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->dismiss()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->b:Lcom/google/googlenav/ui/view/dialog/bk;

    .line 59
    :cond_f
    const-string v0, ""

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fl;->a:Ljava/lang/String;

    .line 60
    return-void
.end method
