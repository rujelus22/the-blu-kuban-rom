.class public abstract Lcom/google/googlenav/ui/wizard/B;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field protected a:Lcom/google/googlenav/J;

.field protected b:LaR/R;

.field c:Ljava/lang/String;

.field private i:Lcom/google/googlenav/ui/view/dialog/bD;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 91
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->c()LaR/R;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->b:LaR/R;

    .line 92
    return-void
.end method


# virtual methods
.method protected a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 256
    return-void
.end method

.method public a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/B;->a:Lcom/google/googlenav/J;

    .line 111
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->o()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 122
    :goto_8
    return-void

    .line 115
    :cond_9
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/B;->c:Ljava/lang/String;

    .line 116
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->j()V

    .line 118
    if-eqz p3, :cond_18

    .line 119
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->b:LaR/R;

    const/4 v1, 0x0

    sget-object v2, LaR/O;->b:LaR/O;

    invoke-interface {v0, v1, v2}, LaR/R;->a(ZLaR/O;)V

    .line 121
    :cond_18
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/googlenav/J;->c(Z)V

    goto :goto_8
.end method

.method protected a(Z)V
    .registers 16
    .parameter

    .prologue
    const/4 v13, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 131
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v7

    .line 133
    new-instance v8, Lcom/google/googlenav/ui/wizard/eB;

    invoke-virtual {v7}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/google/googlenav/ui/wizard/eB;-><init>(LaR/n;)V

    .line 135
    new-instance v9, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->f()LaR/n;

    move-result-object v0

    const/16 v2, 0x2ed

    invoke-direct {v9, v0, v2}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;I)V

    .line 138
    new-instance v10, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->g()LaR/n;

    move-result-object v0

    invoke-direct {v10, v0}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;)V

    .line 140
    if-nez p1, :cond_131

    move v0, v4

    .line 141
    :goto_28
    if-eqz v0, :cond_134

    new-instance v0, Lcom/google/googlenav/ui/wizard/eE;

    invoke-virtual {v7}, LaR/l;->l()LaR/n;

    move-result-object v2

    invoke-interface {v2}, LaR/n;->e()LaR/u;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/googlenav/ui/wizard/eE;-><init>(LaR/u;)V

    move-object v6, v0

    .line 144
    :goto_38
    if-nez p1, :cond_137

    move v0, v4

    .line 145
    :goto_3b
    if-eqz v0, :cond_13a

    new-instance v0, Lcom/google/googlenav/ui/wizard/ex;

    invoke-virtual {v7}, LaR/l;->k()LaR/n;

    move-result-object v2

    const/16 v3, 0x2e8

    invoke-direct {v0, v2, v3}, Lcom/google/googlenav/ui/wizard/ex;-><init>(LaR/n;I)V

    .line 148
    :goto_48
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->j()Z

    move-result v2

    .line 149
    if-eqz v2, :cond_13d

    new-instance v2, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->h()LaR/n;

    move-result-object v3

    const/16 v11, 0x2e7

    invoke-direct {v2, v3, v11}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;I)V

    .line 152
    :goto_5d
    new-instance v11, Lcom/google/googlenav/ui/wizard/eD;

    invoke-virtual {v7}, LaR/l;->i()LaR/n;

    move-result-object v3

    const/16 v12, 0x2ea

    invoke-direct {v11, v3, v12}, Lcom/google/googlenav/ui/wizard/eD;-><init>(LaR/n;I)V

    .line 154
    if-eqz p1, :cond_140

    move-object v3, v1

    .line 159
    :goto_6b
    const/4 v7, 0x6

    new-array v7, v7, [Lcom/google/googlenav/ui/wizard/eI;

    aput-object v9, v7, v5

    aput-object v10, v7, v4

    aput-object v0, v7, v13

    const/4 v10, 0x3

    aput-object v2, v7, v10

    const/4 v10, 0x4

    aput-object v3, v7, v10

    const/4 v10, 0x5

    aput-object v11, v7, v10

    invoke-static {v7}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    .line 162
    :cond_81
    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_81

    .line 165
    new-instance v1, Lcom/google/googlenav/ui/wizard/aX;

    const/16 v10, 0x2eb

    invoke-direct {v1, v7, v10}, Lcom/google/googlenav/ui/wizard/aX;-><init>(Ljava/util/List;I)V

    .line 171
    new-array v7, v13, [Lcom/google/googlenav/ui/wizard/eI;

    aput-object v8, v7, v5

    aput-object v9, v7, v4

    invoke-static {v7}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 172
    new-instance v5, Lcom/google/googlenav/ui/wizard/aX;

    invoke-direct {v5, v4}, Lcom/google/googlenav/ui/wizard/aX;-><init>(Ljava/util/List;)V

    .line 175
    new-instance v4, Lcom/google/googlenav/ui/wizard/eF;

    iget-object v7, p0, Lcom/google/googlenav/ui/wizard/B;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v4, p0, v7}, Lcom/google/googlenav/ui/wizard/eF;-><init>(Lcom/google/googlenav/ui/wizard/B;Lcom/google/googlenav/ui/wizard/jv;)V

    .line 176
    if-nez p1, :cond_c2

    invoke-static {}, Lcom/google/googlenav/K;->K()Z

    move-result v7

    if-eqz v7, :cond_c2

    .line 177
    new-instance v7, Lcom/google/googlenav/ui/view/dialog/bD;

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/B;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v7, v8}, Lcom/google/googlenav/ui/view/dialog/bD;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v7, p0, Lcom/google/googlenav/ui/wizard/B;->i:Lcom/google/googlenav/ui/view/dialog/bD;

    .line 178
    const/16 v7, 0x35a

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "offline"

    iget-object v9, p0, Lcom/google/googlenav/ui/wizard/B;->i:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-virtual {v4, v7, v8, v9}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/bD;)V

    .line 181
    :cond_c2
    const/16 v7, 0x579

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "stars"

    invoke-virtual {v4, v7, v8, v5}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 183
    const/16 v5, 0x430

    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v5

    const-string v7, "recent"

    invoke-virtual {v4, v5, v7, v1}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 185
    if-eqz v0, :cond_e5

    .line 186
    const/16 v1, 0xfb

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v5, "directions"

    invoke-virtual {v4, v1, v5, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 189
    :cond_e5
    if-eqz v3, :cond_f2

    .line 190
    const/16 v0, 0x2d6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mymaps"

    invoke-virtual {v4, v0, v1, v3}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 192
    :cond_f2
    if-eqz v2, :cond_ff

    .line 193
    const/16 v0, 0x6f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "checked-in"

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 196
    :cond_ff
    const/16 v0, 0x3ef

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "rated"

    invoke-virtual {v4, v0, v1, v11}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 197
    if-eqz v6, :cond_117

    .line 199
    const/16 v0, 0x4f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "queries"

    invoke-virtual {v4, v0, v1, v6}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    .line 203
    :cond_117
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->c:Ljava/lang/String;

    if-eqz v0, :cond_14d

    .line 204
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;)V

    .line 210
    :goto_120
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_129

    .line 211
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    .line 214
    :cond_129
    iput-object v4, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 215
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/B;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 216
    return-void

    :cond_131
    move v0, v5

    .line 140
    goto/16 :goto_28

    :cond_134
    move-object v6, v1

    .line 141
    goto/16 :goto_38

    :cond_137
    move v0, v5

    .line 144
    goto/16 :goto_3b

    :cond_13a
    move-object v0, v1

    .line 145
    goto/16 :goto_48

    :cond_13d
    move-object v2, v1

    .line 149
    goto/16 :goto_5d

    .line 154
    :cond_140
    new-instance v3, Lcom/google/googlenav/ui/wizard/eA;

    invoke-virtual {v7}, LaR/l;->j()LaR/n;

    move-result-object v7

    const/16 v12, 0x2e9

    invoke-direct {v3, v7, v12}, Lcom/google/googlenav/ui/wizard/eA;-><init>(LaR/n;I)V

    goto/16 :goto_6b

    .line 207
    :cond_14d
    const-string v0, "stars"

    invoke-virtual {v4, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/String;)V

    goto :goto_120
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 220
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/B;->a(Z)V

    .line 221
    return-void
.end method

.method protected b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 259
    const/4 v0, 0x0

    return v0
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 225
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 226
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->a()V

    .line 231
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->o()Z

    move-result v0

    if-nez v0, :cond_7

    .line 242
    :goto_6
    return-void

    .line 241
    :cond_7
    invoke-virtual/range {p0 .. p5}, Lcom/google/googlenav/ui/wizard/B;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_6
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/B;->o()Z

    move-result v0

    if-nez v0, :cond_8

    .line 250
    const/4 v0, 0x0

    .line 252
    :goto_7
    return v0

    :cond_8
    invoke-virtual/range {p0 .. p5}, Lcom/google/googlenav/ui/wizard/B;->b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_7
.end method
