.class public Lcom/google/googlenav/ui/android/AndroidView;
.super Lcom/google/googlenav/ui/android/BaseAndroidView;
.source "SourceFile"

# interfaces
.implements LaD/m;
.implements Lcom/google/googlenav/ui/b;


# instance fields
.field private final f:Lan/e;

.field private g:LaV/h;

.field private h:Lcom/google/googlenav/ui/android/F;

.field private i:Lcom/google/googlenav/ui/android/o;

.field private j:Lcom/google/googlenav/ui/android/o;

.field private final k:Landroid/graphics/Point;

.field private l:Z

.field private m:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/BaseAndroidView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-instance v0, Lan/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lan/e;-><init>(Landroid/graphics/Canvas;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->f:Lan/e;

    .line 54
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    .line 71
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/android/AndroidView;->setFocusable(Z)V

    .line 72
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/android/AndroidView;->setFocusableInTouchMode(Z)V

    .line 73
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/AndroidView;->setWillNotDraw(Z)V

    .line 75
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->e:LaD/g;

    invoke-virtual {v0, p1, p0}, LaD/g;->a(Landroid/content/Context;LaD/m;)V

    .line 77
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 78
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->i()V

    .line 80
    :cond_2d
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/AndroidView;)F
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->k()F

    move-result v0

    return v0
.end method

.method private a(Landroid/view/MotionEvent;IIJ)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 165
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->j()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 166
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/android/o;->a(Landroid/view/MotionEvent;)V

    .line 167
    new-instance v0, Lat/b;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v3, v1, Lcom/google/googlenav/ui/android/o;->a:I

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v4, v1, Lcom/google/googlenav/ui/android/o;->b:I

    move v1, p2

    move v2, p3

    move-wide v5, p4

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    .line 175
    :goto_1c
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lat/b;)V

    .line 176
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->invalidate()V

    .line 177
    return-void

    .line 172
    :cond_29
    new-instance v0, Lat/b;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    move v1, p2

    move v2, p3

    move-wide v5, p4

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    goto :goto_1c
.end method

.method private i()V
    .registers 2

    .prologue
    .line 114
    invoke-static {}, LaV/h;->j()LaV/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    .line 115
    new-instance v0, Lcom/google/googlenav/ui/android/F;

    invoke-direct {v0}, Lcom/google/googlenav/ui/android/F;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->h:Lcom/google/googlenav/ui/android/F;

    .line 116
    new-instance v0, Lcom/google/googlenav/ui/android/o;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/o;-><init>(Lcom/google/googlenav/ui/android/AndroidView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    .line 117
    new-instance v0, Lcom/google/googlenav/ui/android/o;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/o;-><init>(Lcom/google/googlenav/ui/android/AndroidView;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    .line 118
    return-void
.end method

.method private j()Z
    .registers 2

    .prologue
    .line 144
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->c()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->l()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private k()F
    .registers 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    invoke-virtual {v0}, LaV/h;->c()Z

    move-result v0

    if-nez v0, :cond_a

    .line 350
    const/4 v0, 0x0

    .line 352
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    invoke-virtual {v0}, LaV/h;->d()F

    move-result v0

    goto :goto_9
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->e:LaD/g;

    invoke-virtual {v0}, LaD/g;->a()V

    .line 91
    return-void
.end method

.method public a(Lcom/google/googlenav/android/i;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    .line 85
    new-instance v0, Lcom/google/googlenav/ui/android/b;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/android/b;-><init>(Lcom/google/googlenav/android/i;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->c:Lcom/google/googlenav/ui/android/b;

    .line 86
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f00

    const/4 v2, 0x0

    .line 324
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    if-eqz v0, :cond_47

    .line 325
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->g:LaV/h;

    if-nez v0, :cond_e

    .line 328
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->i()V

    .line 331
    :cond_e
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    invoke-virtual {v0}, Lan/e;->f()Landroid/graphics/Canvas;

    move-result-object v0

    .line 332
    iput-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->m:Landroid/graphics/Canvas;

    .line 336
    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 337
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->save(I)I

    .line 338
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->k()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 339
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->h:Lcom/google/googlenav/ui/android/F;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/android/F;->a(Landroid/graphics/Canvas;)V

    .line 340
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->h:Lcom/google/googlenav/ui/android/F;

    invoke-virtual {v0, v1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    .line 342
    :cond_47
    return-void
.end method

.method public a(LaD/o;)Z
    .registers 3
    .parameter

    .prologue
    .line 277
    const/4 v0, 0x0

    return v0
.end method

.method public a(LaD/q;)Z
    .registers 3
    .parameter

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/s;->a(LaD/q;)Z

    move-result v0

    .line 271
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->invalidate()V

    .line 272
    return v0
.end method

.method public a(LaD/u;)Z
    .registers 3
    .parameter

    .prologue
    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .registers 1

    .prologue
    .line 101
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .registers 4
    .parameter

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    if-eqz v0, :cond_1c

    .line 358
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    invoke-virtual {v0}, Lan/e;->f()Landroid/graphics/Canvas;

    move-result-object v0

    .line 359
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 360
    invoke-virtual {p1}, Lcom/google/googlenav/ui/r;->b()Lam/e;

    move-result-object v0

    check-cast v0, Lan/e;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->m:Landroid/graphics/Canvas;

    invoke-virtual {v0, v1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    .line 362
    :cond_1c
    return-void
.end method

.method public c()V
    .registers 1

    .prologue
    .line 106
    return-void
.end method

.method public d()V
    .registers 1

    .prologue
    .line 96
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 111
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    .line 188
    const/4 v2, -0x1

    const/4 v3, 0x2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    .line 190
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 199
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 204
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    .line 123
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->l:Z

    if-eqz v0, :cond_13

    .line 124
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ui/b;)V

    .line 126
    :cond_13
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->d:Z

    .line 127
    iget-boolean v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->d:Z

    if-eqz v1, :cond_1f

    .line 128
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->d:Z

    .line 129
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->m()V

    .line 132
    :cond_1f
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->f:Lan/e;

    invoke-virtual {v1, p1}, Lan/e;->a(Landroid/graphics/Canvas;)V

    .line 133
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/AndroidView;->f:Lan/e;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/s;->a(Lam/e;)V

    .line 134
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/android/BaseAndroidView;->onDraw(Landroid/graphics/Canvas;)V

    .line 136
    if-nez v0, :cond_3d

    .line 137
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->screenDrawn()V

    .line 139
    :cond_3d
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 259
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->a()V

    .line 260
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 8
    .parameter

    .prologue
    .line 253
    const/4 v2, -0x1

    const/4 v3, 0x7

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    .line 255
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 223
    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v0}, Lcom/google/googlenav/android/A;->a()V

    .line 225
    if-eqz p1, :cond_b

    if-nez p2, :cond_c

    .line 248
    :cond_b
    :goto_b
    return v2

    .line 233
    :cond_c
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/AndroidView;->j()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 234
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/android/o;->a(Landroid/view/MotionEvent;)V

    .line 235
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ui/android/o;->a(Landroid/view/MotionEvent;)V

    .line 236
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    iget-object v3, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v3, v3, Lcom/google/googlenav/ui/android/o;->a:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v4, v4, Lcom/google/googlenav/ui/android/o;->a:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/googlenav/ui/android/AndroidView;->i:Lcom/google/googlenav/ui/android/o;

    iget v4, v4, Lcom/google/googlenav/ui/android/o;->b:I

    iget-object v5, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v5, v5, Lcom/google/googlenav/ui/android/o;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 238
    new-instance v0, Lat/b;

    iget-object v3, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v3, v3, Lcom/google/googlenav/ui/android/o;->a:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/AndroidView;->j:Lcom/google/googlenav/ui/android/o;

    iget v4, v4, Lcom/google/googlenav/ui/android/o;->b:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-object v7, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    .line 246
    :goto_46
    iget-object v1, p0, Lcom/google/googlenav/ui/android/AndroidView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lat/b;)V

    .line 247
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/AndroidView;->invalidate()V

    .line 248
    const/4 v2, 0x1

    goto :goto_b

    .line 241
    :cond_54
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 243
    new-instance v0, Lat/b;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-object v7, p0, Lcom/google/googlenav/ui/android/AndroidView;->k:Landroid/graphics/Point;

    invoke-direct/range {v0 .. v7}, Lat/b;-><init>(IIIIJLandroid/graphics/Point;)V

    goto :goto_46
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter

    .prologue
    .line 211
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 181
    const/4 v2, -0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    .line 183
    return v3
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 215
    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    .line 217
    return v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 150
    iget-object v0, p0, Lcom/google/googlenav/ui/android/AndroidView;->e:LaD/g;

    invoke-virtual {v0, p1}, LaD/g;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 160
    :goto_a
    return v2

    .line 154
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1b

    .line 155
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/AndroidView;->a(Landroid/view/MotionEvent;IIJ)V

    goto :goto_a

    :cond_1b
    move v2, v3

    .line 160
    goto :goto_a
.end method
