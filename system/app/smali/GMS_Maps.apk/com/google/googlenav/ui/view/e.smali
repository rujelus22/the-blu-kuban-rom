.class public Lcom/google/googlenav/ui/view/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/Object;

.field private static g:Z

.field private static h:Lcom/google/googlenav/ui/view/e;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/google/googlenav/ui/view/d;

.field private final c:Lcom/google/googlenav/ui/android/BaseAndroidView;

.field private final d:Lcom/google/googlenav/ui/android/ButtonContainer;

.field private e:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/googlenav/ui/view/e;->f:Ljava/lang/Object;

    .line 138
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/googlenav/ui/view/e;->g:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/android/BaseAndroidView;Lcom/google/googlenav/ui/android/ButtonContainer;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p2, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    .line 154
    iput-object p1, p0, Lcom/google/googlenav/ui/view/e;->a:Landroid/view/ViewGroup;

    .line 156
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 159
    new-instance v0, Lcom/google/googlenav/ui/view/d;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/google/googlenav/ui/view/d;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Z)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/e;->b:Lcom/google/googlenav/ui/view/d;

    .line 164
    :goto_19
    iput-object p3, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    .line 165
    return-void

    .line 161
    :cond_1c
    new-instance v1, Lcom/google/googlenav/ui/view/android/K;

    invoke-virtual {p2}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v2, 0x7f10004f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidBubbleView;

    invoke-direct {v1, p1, v0}, Lcom/google/googlenav/ui/view/android/K;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/android/AndroidBubbleView;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/view/e;->b:Lcom/google/googlenav/ui/view/d;

    goto :goto_19
.end method

.method public static a(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;
    .registers 1
    .parameter

    .prologue
    .line 823
    return-object p0
.end method

.method public static a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/c;
    .registers 1
    .parameter

    .prologue
    .line 784
    return-object p0
.end method

.method public static a()Lcom/google/googlenav/ui/view/e;
    .registers 1

    .prologue
    .line 147
    sget-object v0, Lcom/google/googlenav/ui/view/e;->h:Lcom/google/googlenav/ui/view/e;

    return-object v0
.end method

.method public static a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 849
    invoke-static {p1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View$OnClickListener;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 850
    return-void
.end method

.method public static a(Lcom/google/googlenav/ui/view/e;)V
    .registers 1
    .parameter

    .prologue
    .line 143
    sput-object p0, Lcom/google/googlenav/ui/view/e;->h:Lcom/google/googlenav/ui/view/e;

    .line 144
    return-void
.end method

.method private b(I)I
    .registers 3
    .parameter

    .prologue
    .line 725
    packed-switch p1, :pswitch_data_1a

    .line 737
    const/4 v0, -0x1

    :goto_4
    return v0

    .line 727
    :pswitch_5
    const v0, 0x7f020084

    goto :goto_4

    .line 729
    :pswitch_9
    const v0, 0x7f020088

    goto :goto_4

    .line 731
    :pswitch_d
    const v0, 0x7f020086

    goto :goto_4

    .line 733
    :pswitch_11
    const v0, 0x7f02003f

    goto :goto_4

    .line 735
    :pswitch_15
    const v0, 0x7f020041

    goto :goto_4

    .line 725
    nop

    :pswitch_data_1a
    .packed-switch 0x1
        :pswitch_5
        :pswitch_9
        :pswitch_d
        :pswitch_11
        :pswitch_15
    .end packed-switch
.end method

.method private b(Lcom/google/googlenav/ui/s;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 291
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 294
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v3, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/R;

    .line 297
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/R;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/e;->e:Landroid/view/View;

    .line 298
    invoke-static {}, LaA/h;->a()LaA/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/e;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    .line 301
    :cond_22
    const/16 v0, 0xb

    new-instance v1, Lcom/google/googlenav/ui/view/g;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/g;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    .line 310
    const/16 v0, 0x4f6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bF:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    .line 312
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aW;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/e;->a(Ljava/util/List;)V

    .line 315
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-nez v0, :cond_59

    .line 316
    const/16 v0, 0xc

    new-instance v1, Lcom/google/googlenav/ui/view/h;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/h;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    .line 334
    :cond_59
    const/16 v0, 0xd

    new-instance v1, Lcom/google/googlenav/ui/view/i;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/i;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    .line 353
    const/16 v0, 0xe

    new-instance v1, Lcom/google/googlenav/ui/view/j;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/j;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    .line 370
    return-void
.end method


# virtual methods
.method public a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v1, 0x1a9

    const/16 v0, 0xf6

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 428
    .line 433
    packed-switch p1, :pswitch_data_1dc

    .line 560
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown touchscreen button type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 435
    :pswitch_24
    const v5, 0x7f10006c

    .line 436
    const v1, 0x7f02008f

    .line 437
    const/16 v0, 0x621

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 564
    :goto_31
    packed-switch p1, :pswitch_data_20a

    .line 586
    :pswitch_34
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    move-object v1, v0

    .line 590
    :goto_37
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 591
    if-nez v9, :cond_184

    .line 595
    const/4 v0, 0x0

    .line 634
    :goto_3e
    return-object v0

    .line 440
    :pswitch_3f
    const v5, 0x7f10006b

    .line 441
    const v1, 0x7f02008b

    .line 442
    const/16 v0, 0x622

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 443
    goto :goto_31

    .line 445
    :pswitch_4d
    const v5, 0x7f100067

    .line 446
    const v1, 0x7f02007b

    .line 447
    const/16 v0, 0x3e6

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 448
    goto :goto_31

    .line 450
    :pswitch_5b
    const v5, 0x7f100069

    .line 451
    const v1, 0x7f020071

    .line 452
    const/16 v0, 0x306

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 453
    goto :goto_31

    .line 455
    :pswitch_69
    const v5, 0x7f100070

    .line 456
    const v1, 0x7f02002d

    .line 457
    const/16 v0, 0x58

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 458
    goto :goto_31

    .line 460
    :pswitch_77
    const v5, 0x7f10006e

    .line 461
    const v1, 0x7f020060

    .line 462
    const/16 v0, 0x2b1

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 463
    goto :goto_31

    .line 465
    :pswitch_85
    const v5, 0x7f10006f

    .line 466
    const v1, 0x7f020061

    .line 467
    const/16 v0, 0x2b2

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 468
    goto :goto_31

    .line 470
    :pswitch_93
    const v1, 0x7f100203

    .line 471
    const/16 v0, 0x4f4

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    .line 472
    goto :goto_31

    .line 474
    :pswitch_9e
    const v1, 0x7f1001af

    .line 475
    const/16 v0, 0x1df

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    .line 476
    goto :goto_31

    .line 478
    :pswitch_a9
    const v1, 0x7f100205

    .line 479
    const/16 v0, 0x38b

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    .line 480
    goto/16 :goto_31

    .line 482
    :pswitch_b5
    const v1, 0x7f100207

    .line 483
    const/16 v0, 0x230

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    .line 484
    goto/16 :goto_31

    .line 486
    :pswitch_c1
    const v1, 0x7f100208

    .line 487
    const/16 v0, 0x2c3

    move v5, v0

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v1

    .line 488
    goto/16 :goto_31

    .line 494
    :pswitch_cd
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/K;->an()Z

    move-result v5

    if-eqz v5, :cond_106

    .line 495
    const v7, 0x7f1002d4

    .line 497
    packed-switch p1, :pswitch_data_22a

    .line 514
    const v0, 0x7f02004d

    move v1, v0

    move v0, v2

    .line 518
    :goto_e2
    const v6, 0x7f1002d5

    .line 519
    const v5, 0x7f1002d3

    move v8, v1

    move v9, v7

    move v7, v6

    move v6, v5

    move v5, v0

    goto/16 :goto_31

    .line 499
    :pswitch_ef
    const v1, 0x7f02005c

    .line 500
    const/16 v0, 0x4f8

    .line 501
    goto :goto_e2

    .line 503
    :pswitch_f5
    const v1, 0x7f020050

    .line 505
    goto :goto_e2

    .line 507
    :pswitch_f9
    const v0, 0x7f020055

    move v10, v1

    move v1, v0

    move v0, v10

    .line 509
    goto :goto_e2

    .line 511
    :pswitch_100
    const v0, 0x7f02005e

    move v1, v0

    move v0, v2

    .line 512
    goto :goto_e2

    .line 521
    :cond_106
    const v5, 0x7f100066

    .line 523
    packed-switch p1, :pswitch_data_236

    .line 537
    const v1, 0x7f02004d

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v2

    .line 541
    goto/16 :goto_31

    .line 525
    :pswitch_116
    const v1, 0x7f02005a

    .line 526
    const/16 v0, 0x4f8

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 527
    goto/16 :goto_31

    .line 529
    :pswitch_122
    const v1, 0x7f02004e

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 531
    goto/16 :goto_31

    .line 533
    :pswitch_12c
    const v0, 0x7f020053

    move v6, v2

    move v7, v2

    move v8, v0

    move v9, v5

    move v5, v1

    .line 535
    goto/16 :goto_31

    .line 545
    :pswitch_136
    const v5, 0x7f100071

    .line 546
    const v1, 0x7f02006e

    .line 547
    const/16 v0, 0x2c3

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 548
    goto/16 :goto_31

    .line 550
    :pswitch_145
    const v5, 0x7f100068

    .line 551
    const v1, 0x7f020078

    .line 552
    const/16 v0, 0x30b

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 553
    goto/16 :goto_31

    .line 555
    :pswitch_154
    const v5, 0x7f100072

    .line 556
    const v1, 0x7f02003d

    .line 557
    const/16 v0, 0xda

    move v6, v2

    move v7, v2

    move v8, v1

    move v9, v5

    move v5, v0

    .line 558
    goto/16 :goto_31

    .line 570
    :pswitch_163
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 571
    goto/16 :goto_37

    .line 579
    :pswitch_16c
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_17f

    .line 580
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_37

    .line 582
    :cond_17f
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    move-object v1, v0

    .line 584
    goto/16 :goto_37

    .line 597
    :cond_184
    if-eq v8, v2, :cond_189

    .line 598
    invoke-virtual {v9, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 601
    :cond_189
    if-eq v7, v2, :cond_194

    .line 602
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 603
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    :cond_194
    if-eq v5, v2, :cond_19d

    .line 608
    invoke-static {v5}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 611
    :cond_19d
    invoke-static {p2}, Lcom/google/googlenav/ui/view/e;->a(Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/c;

    move-result-object v0

    .line 613
    if-ne v6, v2, :cond_1bb

    .line 614
    new-instance v2, Lcom/google/googlenav/ui/view/android/R;

    invoke-direct {v2, v9, v0}, Lcom/google/googlenav/ui/view/android/R;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/view/c;)V

    .line 623
    :goto_1a8
    if-ne p1, v4, :cond_1ca

    .line 624
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->n()Z

    move-result v0

    if-eqz v0, :cond_1c8

    move v0, v3

    :goto_1b5
    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1b8
    :goto_1b8
    move-object v0, v2

    .line 634
    goto/16 :goto_3e

    .line 618
    :cond_1bb
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 619
    new-instance v2, Lcom/google/googlenav/ui/view/android/R;

    invoke-direct {v2, v5, v0}, Lcom/google/googlenav/ui/view/android/R;-><init>(Landroid/view/View;Lcom/google/googlenav/ui/view/c;)V

    .line 620
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1a8

    :cond_1c8
    move v0, v4

    .line 624
    goto :goto_1b5

    .line 627
    :cond_1ca
    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    .line 628
    const/16 v0, 0xc

    if-ne p1, v0, :cond_1b8

    .line 629
    const v0, 0x7f100206

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1b8

    .line 433
    :pswitch_data_1dc
    .packed-switch 0x1
        :pswitch_24
        :pswitch_3f
        :pswitch_4d
        :pswitch_5b
        :pswitch_cd
        :pswitch_136
        :pswitch_69
        :pswitch_9e
        :pswitch_77
        :pswitch_85
        :pswitch_93
        :pswitch_a9
        :pswitch_b5
        :pswitch_c1
        :pswitch_cd
        :pswitch_cd
        :pswitch_cd
        :pswitch_cd
        :pswitch_136
        :pswitch_145
        :pswitch_154
    .end packed-switch

    .line 564
    :pswitch_data_20a
    .packed-switch 0x5
        :pswitch_16c
        :pswitch_34
        :pswitch_34
        :pswitch_163
        :pswitch_34
        :pswitch_34
        :pswitch_163
        :pswitch_163
        :pswitch_163
        :pswitch_163
        :pswitch_16c
        :pswitch_16c
        :pswitch_16c
        :pswitch_16c
    .end packed-switch

    .line 497
    :pswitch_data_22a
    .packed-switch 0xf
        :pswitch_ef
        :pswitch_f5
        :pswitch_f9
        :pswitch_100
    .end packed-switch

    .line 523
    :pswitch_data_236
    .packed-switch 0xf
        :pswitch_116
        :pswitch_122
        :pswitch_12c
    .end packed-switch
.end method

.method public a(Lax/b;Lcom/google/googlenav/ui/e;Z)Lcom/google/googlenav/ui/view/d;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 670
    new-instance v0, Lcom/google/googlenav/ui/view/android/x;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/googlenav/ui/view/android/x;-><init>(Landroid/view/ViewGroup;Lax/b;Lcom/google/googlenav/ui/e;Z)V

    return-object v0
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 718
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100208

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 719
    if-eqz v0, :cond_18

    .line 720
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/e;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 722
    :cond_18
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 978
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    instance-of v0, v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    if-eqz v0, :cond_d

    .line 979
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0, p1, p2}, Lcom/google/googlenav/ui/android/AndroidVectorView;->setCompassMargin(II)V

    .line 981
    :cond_d
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/aW;)V
    .registers 7
    .parameter

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f10006a

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 649
    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 652
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->G()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 656
    const/16 v1, 0x64

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 659
    :cond_31
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/s;)V
    .registers 5
    .parameter

    .prologue
    .line 185
    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->aq()Lcom/google/googlenav/ui/bE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bE;->b()V

    .line 188
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 189
    const/4 v0, 0x6

    new-instance v1, Lcom/google/googlenav/ui/view/f;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/view/f;-><init>(Lcom/google/googlenav/ui/view/e;Lcom/google/googlenav/ui/s;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    .line 281
    :cond_17
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 282
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/e;->b(Lcom/google/googlenav/ui/s;)V

    .line 284
    :cond_24
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 682
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 686
    sget-object v1, Lcom/google/googlenav/ui/aV;->bF:Lcom/google/googlenav/ui/aV;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/aW;

    iget-object v0, v0, Lcom/google/googlenav/ui/aW;->c:Lcom/google/googlenav/ui/aV;

    if-ne v1, v0, :cond_21

    .line 687
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    .line 699
    :cond_20
    :goto_20
    return-void

    .line 690
    :cond_21
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/actionbar/a;->a(Ljava/lang/String;)V

    goto :goto_20

    .line 694
    :cond_31
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 695
    if-eqz v0, :cond_20

    .line 696
    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_20
.end method

.method public b()Landroid/view/View;
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->e:Landroid/view/View;

    return-object v0
.end method

.method public c()V
    .registers 2

    .prologue
    .line 395
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->v()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 396
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    .line 397
    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->i()V

    .line 399
    :cond_11
    return-void
.end method

.method public d()Lcom/google/googlenav/ui/android/ButtonContainer;
    .registers 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    return-object v0
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 707
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public f()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 853
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public g()Landroid/view/View;
    .registers 3

    .prologue
    .line 860
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100200

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public h()V
    .registers 4

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100200

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 869
    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1b

    .line 870
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 872
    :cond_1b
    return-void
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 880
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->c:Lcom/google/googlenav/ui/android/BaseAndroidView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/BaseAndroidView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100200

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 883
    if-eqz v0, :cond_1c

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1c

    .line 885
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 886
    const/4 v0, 0x1

    .line 888
    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public j()V
    .registers 2

    .prologue
    .line 896
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->b()V

    .line 897
    return-void
.end method

.method public k()V
    .registers 2

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/ButtonContainer;->a()V

    .line 904
    return-void
.end method

.method public l()Z
    .registers 4

    .prologue
    const v2, 0x7f020066

    .line 914
    iget-object v0, p0, Lcom/google/googlenav/ui/view/e;->d:Lcom/google/googlenav/ui/android/ButtonContainer;

    const v1, 0x7f100071

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 915
    if-eqz v0, :cond_14

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_16

    .line 916
    :cond_14
    const/4 v0, 0x0

    .line 932
    :goto_15
    return v0

    .line 918
    :cond_16
    sget-object v1, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {v1}, Lcom/google/googlenav/android/A;->k()I

    move-result v1

    .line 919
    packed-switch v1, :pswitch_data_36

    .line 930
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 932
    :goto_22
    const/4 v0, 0x1

    goto :goto_15

    .line 921
    :pswitch_24
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_22

    .line 924
    :pswitch_28
    const v1, 0x7f020067

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_22

    .line 927
    :pswitch_2f
    const v1, 0x7f02006a

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_22

    .line 919
    :pswitch_data_36
    .packed-switch 0x3
        :pswitch_24
        :pswitch_28
        :pswitch_2f
    .end packed-switch
.end method
