.class public Lcom/google/googlenav/ui/wizard/hh;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field protected a:I

.field private b:Lcom/google/googlenav/ai;

.field private c:Lcom/google/googlenav/J;

.field private i:Lbf/am;

.field private j:Z

.field private k:Lcom/google/googlenav/ui/br;

.field private l:LaB/s;

.field private m:Ljava/lang/String;

.field private n:Lbf/bk;

.field private o:Lcom/google/googlenav/ui/wizard/hb;

.field private p:Lcom/google/googlenav/ui/wizard/hg;

.field private q:LaM/h;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/J;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 98
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    .line 103
    new-instance v0, Lcom/google/googlenav/ui/wizard/hi;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hi;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->p:Lcom/google/googlenav/ui/wizard/hg;

    .line 555
    new-instance v0, Lcom/google/googlenav/ui/wizard/ho;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/ho;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    .line 120
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    .line 121
    invoke-interface {p2}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->l:LaB/s;

    .line 122
    new-instance v0, Lcom/google/googlenav/ui/br;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->l:LaB/s;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->k:Lcom/google/googlenav/ui/br;

    .line 124
    return-void
.end method

.method private A()V
    .registers 4

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 373
    if-eqz v0, :cond_21

    .line 374
    iget v1, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1a

    .line 375
    invoke-virtual {v0}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    iget v2, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    invoke-interface {v1, v2}, Lcom/google/googlenav/F;->a(I)V

    .line 376
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lbf/i;->a(B)V

    .line 378
    :cond_1a
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbf/i;->f(Z)V

    .line 379
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->B()V

    .line 381
    :cond_21
    return-void
.end method

.method private B()V
    .registers 3

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    .line 429
    instance-of v1, v0, Lbf/m;

    if-eqz v1, :cond_10

    .line 430
    check-cast v0, Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bp()V

    .line 434
    :goto_f
    return-void

    .line 432
    :cond_10
    invoke-virtual {v0}, Lbf/i;->J()V

    goto :goto_f
.end method

.method private C()V
    .registers 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 495
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bK()[I

    move-result-object v0

    .line 496
    aget v1, v0, v8

    aget v2, v0, v9

    add-int/2addr v1, v2

    .line 497
    const/16 v2, 0x55

    const-string v3, "d"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "s="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->bF()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "a="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->bJ()[Lcom/google/googlenav/ak;

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "f="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "l="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v6, v0, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const/4 v1, 0x4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "d="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget v0, v0, v9

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "t="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v5}, Lcom/google/googlenav/ai;->bG()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 506
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hh;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/hh;)LaY/c;
    .registers 2
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->z()LaY/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/hh;)Lcom/google/googlenav/J;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/hh;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->A()V

    return-void
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/hh;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->B()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/hh;)LaM/h;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/hh;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->y()V

    return-void
.end method

.method private y()V
    .registers 10

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    if-eqz v0, :cond_9

    .line 208
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hb;->a()V

    .line 211
    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_2f

    .line 212
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v0

    .line 213
    new-instance v1, LaY/c;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    new-instance v3, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-direct {v1, p0, v2, v3, v0}, LaY/c;-><init>(Lcom/google/googlenav/ui/wizard/hh;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;Lcom/google/googlenav/ui/wizard/hb;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 217
    :cond_2f
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->k()LaB/o;

    move-result-object v0

    invoke-virtual {v0}, LaB/o;->b()LaB/s;

    move-result-object v1

    .line 218
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaY/c;

    new-instance v2, Lcom/google/googlenav/ui/br;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/bi;->q()Lam/f;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/googlenav/ui/br;-><init>(LaB/s;Lam/f;)V

    invoke-virtual {v0, v2}, LaY/c;->a(Lcom/google/googlenav/ui/br;)V

    .line 222
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 226
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->E()Z

    move-result v0

    if-eqz v0, :cond_a4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-eqz v0, :cond_a4

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->d()Z

    move-result v0

    if-nez v0, :cond_a4

    .line 229
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 230
    const/4 v1, 0x7

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->d(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)[Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 233
    array-length v0, v0

    if-lez v0, :cond_a4

    .line 236
    new-instance v0, Lcom/google/googlenav/cv;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v2

    new-instance v4, Lcom/google/googlenav/ui/wizard/hw;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->k:Lcom/google/googlenav/ui/br;

    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->z()LaY/c;

    move-result-object v7

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/googlenav/ui/wizard/hw;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/br;LaY/c;Lcom/google/googlenav/J;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/cv;-><init>(IJLcom/google/googlenav/cw;)V

    .line 240
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 245
    :cond_a4
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ax;->a(Z)V

    .line 246
    return-void
.end method

.method private z()LaY/c;
    .registers 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaY/c;

    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 530
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 531
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->h()V

    .line 532
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    .line 538
    :goto_d
    return v0

    .line 534
    :cond_e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_14

    .line 536
    const/4 v0, 0x4

    goto :goto_d

    .line 538
    :cond_14
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    goto :goto_d
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-nez v0, :cond_6

    .line 545
    const/4 v0, 0x4

    .line 547
    :goto_5
    return v0

    :cond_6
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    goto :goto_5
.end method

.method public a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    .line 139
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    .line 140
    iput-boolean p3, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    .line 141
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hh;->m:Ljava/lang/String;

    .line 143
    new-instance v0, Lcom/google/googlenav/ui/wizard/hb;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->p:Lcom/google/googlenav/ui/wizard/hg;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/googlenav/ui/wizard/hb;-><init>(Lcom/google/googlenav/ui/wizard/hg;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/jv;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    .line 145
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->j()V

    .line 146
    return-void
.end method

.method protected a(Lcom/google/googlenav/ai;Z)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 438
    if-nez p1, :cond_5

    .line 467
    :cond_4
    :goto_4
    return-void

    .line 444
    :cond_5
    if-eqz p2, :cond_31

    new-instance v0, Lcom/google/googlenav/ui/wizard/hu;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/ui/wizard/hu;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;)V

    .line 450
    :goto_e
    new-instance v1, Lcom/google/googlenav/bF;

    const-string v2, "lo-gmm"

    invoke-direct {v1, p1, v2, p2, v0}, Lcom/google/googlenav/bF;-><init>(Lcom/google/googlenav/ai;Ljava/lang/String;ZLcom/google/googlenav/bG;)V

    .line 456
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    .line 459
    if-nez p2, :cond_4

    .line 460
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v0, 0x42b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v5, v8}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v8}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    goto :goto_4

    .line 444
    :cond_31
    new-instance v0, Lcom/google/googlenav/ui/wizard/ht;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->i()Lcom/google/googlenav/ui/wizard/hx;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/ht;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V

    goto :goto_e
.end method

.method public a(Lcom/google/googlenav/ay;)V
    .registers 10
    .parameter

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x404

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x403

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x61a

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x314

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/ui/wizard/hk;

    invoke-direct {v7, p0, p1}, Lcom/google/googlenav/ui/wizard/hk;-><init>(Lcom/google/googlenav/ui/wizard/hh;Lcom/google/googlenav/ay;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 309
    return-void
.end method

.method public a(Lcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 390
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_19

    .line 391
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ai;->a(Lcom/google/googlenav/cx;)V

    .line 392
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/ai;->n(Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p3}, Lcom/google/googlenav/ai;->m(Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, p4}, Lcom/google/googlenav/ai;->a(Ljava/lang/Integer;)V

    .line 398
    :cond_19
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->B()V

    .line 399
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_25

    .line 400
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 403
    :cond_25
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    if-nez v0, :cond_33

    .line 406
    const/16 v0, 0x40d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 416
    :cond_32
    :goto_32
    return-void

    .line 411
    :cond_33
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/ai;Z)V

    .line 413
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->m:Ljava/lang/String;

    const-string v1, "plrp"

    if-ne v0, v1, :cond_32

    .line 414
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->H()Lcom/google/googlenav/ui/wizard/gk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gk;->b(Lcom/google/googlenav/ai;)V

    goto :goto_32
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 348
    sparse-switch p1, :sswitch_data_1a

    .line 359
    const/4 v0, 0x0

    :goto_5
    return v0

    .line 352
    :sswitch_6
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    goto :goto_5

    .line 355
    :sswitch_a
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/googlenav/F;->a(I)V

    .line 356
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    invoke-virtual {v1, p1, p2, p3}, Lbf/bk;->a(IILjava/lang/Object;)Z

    goto :goto_5

    .line 348
    nop

    :sswitch_data_1a
    .sparse-switch
        0xe -> :sswitch_6
        0x6a4 -> :sswitch_a
    .end sparse-switch
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->b()V

    .line 154
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bm;->v()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 158
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(LaM/h;)V

    .line 160
    sget-object v1, Lcom/google/googlenav/br;->a:Lcom/google/googlenav/br;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->a(Lcom/google/googlenav/br;)V

    .line 165
    :goto_1b
    return-void

    .line 163
    :cond_1c
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->y()V

    goto :goto_1b
.end method

.method protected c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 169
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 171
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    .line 172
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hh;->a:I

    .line 173
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    if-eqz v0, :cond_14

    .line 174
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    invoke-virtual {v0, v1}, Lbf/am;->h(Lbf/i;)V

    .line 176
    :cond_14
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->n:Lbf/bk;

    .line 177
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    if-eqz v0, :cond_21

    .line 178
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/hb;->c()V

    .line 179
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/hh;->o:Lcom/google/googlenav/ui/wizard/hb;

    .line 181
    :cond_21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    .line 184
    invoke-static {}, Lcom/google/googlenav/bm;->a()Lcom/google/googlenav/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->q:LaM/h;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bm;->b(LaM/h;)V

    .line 185
    return-void
.end method

.method public d()V
    .registers 5

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    .line 191
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    .line 192
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    .line 193
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iget-boolean v2, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hh;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/hh;->a(Lcom/google/googlenav/ai;Lbf/am;ZLjava/lang/String;)V

    .line 194
    return-void
.end method

.method public e()V
    .registers 4

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/googlenav/ui/wizard/hj;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hj;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILcom/google/googlenav/ui/wizard/cc;)V

    .line 274
    return-void
.end method

.method public f()V
    .registers 10

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->C()V

    .line 314
    new-instance v2, Lcom/google/googlenav/ui/wizard/hq;

    new-instance v0, Lcom/google/googlenav/ui/wizard/hl;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hl;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    invoke-direct {v2, v0}, Lcom/google/googlenav/ui/wizard/hq;-><init>(Ljava/lang/Runnable;)V

    .line 320
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    const/16 v1, 0x1b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    .line 323
    new-instance v3, Lcom/google/googlenav/ui/wizard/hr;

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hh;->i:Lbf/am;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    new-instance v8, Lcom/google/googlenav/ui/wizard/hm;

    invoke-direct {v8, p0}, Lcom/google/googlenav/ui/wizard/hm;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    move-object v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/googlenav/ui/wizard/hr;-><init>(Lcom/google/googlenav/J;Lbf/am;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/hq;Lcom/google/googlenav/ui/wizard/hx;)V

    .line 337
    new-instance v0, LaY/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v1

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bI()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v0, v1, v2, v4, v3}, LaY/a;-><init>(JLcom/google/googlenav/common/io/protocol/ProtoBuf;LaY/b;)V

    .line 340
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 343
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aq()Lcom/google/googlenav/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->a()V

    .line 344
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->c:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    const/16 v2, 0x2a

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/ai;I)V

    .line 421
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    .line 422
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_f

    .line 513
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hh;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->bL()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->m(I)V

    .line 516
    :cond_f
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hh;->j:Z

    if-eqz v0, :cond_1e

    .line 517
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hh;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ILat/a;)V

    .line 518
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hh;->g:I

    .line 526
    :goto_1d
    return-void

    .line 524
    :cond_1e
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hh;->A()V

    .line 525
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hh;->a()V

    goto :goto_1d
.end method

.method i()Lcom/google/googlenav/ui/wizard/hx;
    .registers 2

    .prologue
    .line 472
    new-instance v0, Lcom/google/googlenav/ui/wizard/hn;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hn;-><init>(Lcom/google/googlenav/ui/wizard/hh;)V

    .line 489
    return-object v0
.end method
