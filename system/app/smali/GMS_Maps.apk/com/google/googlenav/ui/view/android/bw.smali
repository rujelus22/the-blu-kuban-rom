.class Lcom/google/googlenav/ui/view/android/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field static final synthetic e:Z


# instance fields
.field a:I

.field b:I

.field c:Lcom/google/googlenav/ai;

.field d:Lcom/google/googlenav/ui/view/android/bs;

.field final synthetic f:Lcom/google/googlenav/ui/view/android/bs;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 314
    const-class v0, Lcom/google/googlenav/ui/view/android/bs;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/googlenav/ui/view/android/bw;->e:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/view/android/bs;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bw;->f:Lcom/google/googlenav/ui/view/android/bs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 320
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    .line 335
    iput p2, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    .line 336
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0x578

    .line 342
    sget-boolean v0, Lcom/google/googlenav/ui/view/android/bw;->e:Z

    if-nez v0, :cond_11

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 346
    :cond_11
    sget-boolean v0, Lcom/google/googlenav/ui/view/android/bw;->e:Z

    if-nez v0, :cond_23

    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    if-ne v0, v4, :cond_23

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    if-nez v0, :cond_23

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 348
    :cond_23
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    if-ne v0, v4, :cond_4d

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->c:Lcom/google/googlenav/ai;

    .line 349
    :goto_29
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bw;->f:Lcom/google/googlenav/ui/view/android/bs;

    iget-object v1, v1, Lcom/google/googlenav/ui/view/android/bs;->E:Lcom/google/googlenav/ui/e;

    iget v2, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    iget v3, p0, Lcom/google/googlenav/ui/view/android/bw;->b:I

    invoke-interface {v1, v2, v3, v0}, Lcom/google/googlenav/ui/e;->a(IILjava/lang/Object;)Z

    .line 353
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bw;->a:I

    if-ne v0, v4, :cond_4c

    .line 354
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    if-nez v0, :cond_4f

    const/4 v0, 0x1

    :goto_41
    iput-boolean v0, v1, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    .line 355
    check-cast p1, Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bw;->d:Lcom/google/googlenav/ui/view/android/bs;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/view/android/bs;->C:Z

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 357
    :cond_4c
    return-void

    .line 348
    :cond_4d
    const/4 v0, 0x0

    goto :goto_29

    .line 354
    :cond_4f
    const/4 v0, 0x0

    goto :goto_41
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 3
    .parameter

    .prologue
    .line 367
    const/4 v0, 0x0

    return v0
.end method
