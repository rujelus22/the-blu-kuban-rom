.class public Lcom/google/googlenav/ui/android/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/android/D;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final s:LE/o;

.field private static final t:LE/d;

.field private static final u:LE/d;

.field private static final v:LE/d;

.field private static final w:LE/o;

.field private static final x:LE/d;


# instance fields
.field private final A:LE/i;

.field private final B:Lcom/google/android/maps/driveabout/vector/aV;

.field private final C:Lcom/google/googlenav/ui/android/w;

.field private D:LC/a;

.field private E:Lbf/ah;

.field private volatile F:Z

.field private G:Z

.field private volatile e:Landroid/graphics/Point;

.field private volatile f:Landroid/graphics/Point;

.field private g:Lcom/google/googlenav/ui/android/y;

.field private h:Lcom/google/googlenav/ui/android/B;

.field private i:Lcom/google/googlenav/ui/android/A;

.field private j:Lcom/google/googlenav/ui/android/x;

.field private k:Lcom/google/googlenav/ui/au;

.field private l:Lcom/google/googlenav/ui/android/AndroidVectorView;

.field private m:Lcom/google/android/maps/MapsActivity;

.field private n:LaN/u;

.field private o:Z

.field private p:I

.field private volatile q:I

.field private volatile r:I

.field private y:[F

.field private z:LD/b;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x6

    const/4 v3, 0x2

    .line 68
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->a:I

    .line 73
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->b:I

    .line 78
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->c:I

    .line 93
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, Lcom/google/googlenav/ui/android/r;->d:I

    .line 217
    new-instance v0, LE/d;

    invoke-direct {v0, v4}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    .line 218
    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    invoke-virtual {v0, v5, v6}, LE/d;->a(SS)V

    .line 219
    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    const/4 v1, 0x7

    invoke-virtual {v0, v6, v1}, LE/d;->a(SS)V

    .line 220
    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, LE/d;->a(SS)V

    .line 222
    new-instance v0, LE/d;

    invoke-direct {v0, v5}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    .line 223
    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    invoke-virtual {v0, v5}, LE/d;->a(S)V

    .line 224
    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    invoke-virtual {v0, v6}, LE/d;->a(S)V

    .line 225
    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    invoke-virtual {v0, v4}, LE/d;->a(S)V

    .line 226
    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, LE/d;->a(S)V

    .line 228
    new-instance v0, LE/d;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    .line 229
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0x8

    const/16 v2, 0x9

    invoke-virtual {v0, v7, v1, v2}, LE/d;->a(SSS)V

    .line 230
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0x9

    const/4 v2, 0x1

    invoke-virtual {v0, v7, v1, v2}, LE/d;->a(SSS)V

    .line 231
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v5, v6}, LE/d;->a(SSS)V

    .line 232
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6, v3}, LE/d;->a(SSS)V

    .line 233
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0x9

    const/16 v2, 0xa

    invoke-virtual {v0, v4, v1, v2}, LE/d;->a(SSS)V

    .line 234
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0xa

    const/4 v2, 0x7

    invoke-virtual {v0, v4, v1, v2}, LE/d;->a(SSS)V

    .line 235
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0xa

    const/16 v2, 0xb

    invoke-virtual {v0, v3, v1, v2}, LE/d;->a(SSS)V

    .line 236
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    const/16 v1, 0xb

    const/4 v2, 0x3

    invoke-virtual {v0, v3, v1, v2}, LE/d;->a(SSS)V

    .line 238
    new-instance v0, LE/d;

    invoke-direct {v0, v4}, LE/d;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    .line 239
    sget-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    const/4 v1, 0x1

    invoke-virtual {v0, v7, v3, v1}, LE/d;->a(SSS)V

    .line 240
    sget-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    const/4 v1, 0x3

    invoke-virtual {v0, v7, v1, v3}, LE/d;->a(SSS)V

    .line 242
    new-instance v0, LE/o;

    invoke-direct {v0, v5}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->w:LE/o;

    .line 244
    new-instance v0, LE/o;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, LE/o;-><init>(I)V

    sput-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    .line 245
    return-void
.end method

.method public constructor <init>(Lcom/google/android/maps/MapsActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    new-instance v0, LE/i;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, LE/i;-><init>(I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->A:LE/i;

    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    .line 302
    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    .line 303
    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    .line 304
    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/AndroidVectorView;

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    .line 305
    new-instance v0, Lcom/google/googlenav/ui/android/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/android/w;-><init>(Lcom/google/googlenav/ui/android/r;Lcom/google/googlenav/ui/android/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    .line 306
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aV;

    invoke-virtual {p1}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;-><init>(F)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->B:Lcom/google/android/maps/driveabout/vector/aV;

    .line 307
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput p1, p0, Lcom/google/googlenav/ui/android/r;->p:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;LC/a;)LC/a;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;LD/b;)LD/b;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->z:LD/b;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;)Lbf/ah;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    if-eqz v0, :cond_e

    .line 390
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 392
    :cond_e
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/googlenav/ui/android/r;->o:Z

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/r;[F)[F
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    return-object p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/r;)V
    .registers 1
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->t()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/r;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    return p1
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/r;)V
    .registers 1
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->s()V

    return-void
.end method

.method static synthetic d()LE/o;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/w;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    return-object v0
.end method

.method static synthetic e()LE/d;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/ui/android/r;->v:LE/d;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/MapsActivity;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    return-object v0
.end method

.method static synthetic f()I
    .registers 1

    .prologue
    .line 63
    sget v0, Lcom/google/googlenav/ui/android/r;->a:I

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/android/r;)LC/a;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    return-object v0
.end method

.method static synthetic g()LE/d;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/ui/android/r;->t:LE/d;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/android/r;)V
    .registers 1
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->u()V

    return-void
.end method

.method static synthetic h(Lcom/google/googlenav/ui/android/r;)LD/b;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->z:LD/b;

    return-object v0
.end method

.method static synthetic h()LE/d;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/ui/android/r;->u:LE/d;

    return-object v0
.end method

.method static synthetic i()I
    .registers 1

    .prologue
    .line 63
    sget v0, Lcom/google/googlenav/ui/android/r;->d:I

    return v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/android/r;)Lcom/google/android/maps/driveabout/vector/aV;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->B:Lcom/google/android/maps/driveabout/vector/aV;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/android/r;)LE/i;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->A:LE/i;

    return-object v0
.end method

.method static synthetic j()LE/o;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/ui/android/r;->w:LE/o;

    return-object v0
.end method

.method static synthetic k()LE/d;
    .registers 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/googlenav/ui/android/r;->x:LE/d;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/android/r;)[F
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    return-object v0
.end method

.method static synthetic l()I
    .registers 1

    .prologue
    .line 63
    sget v0, Lcom/google/googlenav/ui/android/r;->c:I

    return v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/android/r;)Z
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->o:Z

    return v0
.end method

.method static synthetic m()I
    .registers 1

    .prologue
    .line 63
    sget v0, Lcom/google/googlenav/ui/android/r;->b:I

    return v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/android/r;)I
    .registers 2
    .parameter

    .prologue
    .line 63
    iget v0, p0, Lcom/google/googlenav/ui/android/r;->p:I

    return v0
.end method

.method static synthetic n(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    return-object v0
.end method

.method private n()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 313
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->t()V

    .line 314
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowRotateGesture(Z)V

    .line 315
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowTiltGesture(Z)V

    .line 318
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowLongPressGesture(Z)V

    .line 321
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->setAllowSingleTapGesture(Z)V

    .line 322
    return-void
.end method

.method static synthetic o(Lcom/google/googlenav/ui/android/r;)Landroid/graphics/Point;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    return-object v0
.end method

.method private o()V
    .registers 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->u()V

    .line 326
    return-void
.end method

.method static synthetic p(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/x;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    return-object v0
.end method

.method private p()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 395
    iput-boolean v2, p0, Lcom/google/googlenav/ui/android/r;->G:Z

    .line 398
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getButtonContainer()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/android/ButtonContainer;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->p()V

    .line 400
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->g:Lcom/google/googlenav/ui/android/y;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->b(LS/a;)V

    .line 401
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->b(Lcom/google/googlenav/ui/android/D;)V

    .line 404
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_73

    .line 405
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/view/android/bC;->a(Lcom/google/googlenav/ui/au;)V

    .line 411
    :goto_43
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 412
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->d()Z

    .line 427
    :cond_54
    :goto_54
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->o()V

    .line 428
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    if-eqz v0, :cond_62

    .line 429
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/x;->c()V

    .line 430
    iput-object v4, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    .line 432
    :cond_62
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    if-eqz v0, :cond_72

    .line 436
    new-instance v0, Lcom/google/googlenav/ui/android/s;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/googlenav/ui/android/s;-><init>(Lcom/google/googlenav/ui/android/r;Las/c;)V

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/s;->g()V

    .line 442
    :cond_72
    return-void

    .line 407
    :cond_73
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->k:Lcom/google/googlenav/ui/au;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/as;->a(Lcom/google/googlenav/ui/au;)V

    goto :goto_43

    .line 413
    :cond_7f
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_91

    .line 414
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->j()V

    goto :goto_54

    .line 416
    :cond_91
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBottomBar()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 417
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    const v1, 0x7f100200

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_54

    .line 422
    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 423
    const v1, 0x7f10003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_54
.end method

.method private q()V
    .registers 9

    .prologue
    const v7, 0x7f020222

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/16 v3, 0x348

    .line 452
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 455
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v7, v1, v2, v6}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 480
    :cond_1f
    :goto_1f
    return-void

    .line 457
    :cond_20
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 460
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04000f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 461
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 462
    const v0, 0x7f10001e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 464
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v0, v7, v2, v3, v6}, Lcom/google/googlenav/actionbar/a;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    .line 466
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x13

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_1f

    .line 470
    :cond_67
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    const v1, 0x7f100200

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 473
    if-eqz v0, :cond_1f

    .line 474
    const v1, 0x7f100201

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 475
    const v1, 0x7f10003c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 476
    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1f
.end method

.method static synthetic q(Lcom/google/googlenav/ui/android/r;)V
    .registers 1
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->v()V

    return-void
.end method

.method static synthetic r(Lcom/google/googlenav/ui/android/r;)Lcom/google/googlenav/ui/android/AndroidVectorView;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    return-object v0
.end method

.method private r()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 485
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_57

    .line 486
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getBottomBar()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 487
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 488
    const v1, 0x7f100030

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 489
    const/16 v2, 0x347

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 490
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 491
    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    iget-boolean v2, v2, Lcom/google/googlenav/ui/android/w;->i:Z

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 492
    new-instance v2, Lcom/google/googlenav/ui/android/t;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/android/t;-><init>(Lcom/google/googlenav/ui/android/r;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 498
    const v1, 0x7f100031

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 499
    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 500
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 501
    new-instance v1, Lcom/google/googlenav/ui/android/u;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/u;-><init>(Lcom/google/googlenav/ui/android/r;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 508
    :cond_57
    return-void
.end method

.method private s()V
    .registers 2

    .prologue
    .line 511
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->p()V

    .line 512
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->h:Lcom/google/googlenav/ui/android/B;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/B;->a()V

    .line 513
    return-void
.end method

.method private t()V
    .registers 7

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->p()V

    .line 517
    const-string v0, "s"

    const-string v1, ""

    invoke-static {v0, v1}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 521
    invoke-static {v0}, LJ/a;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 524
    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v5

    invoke-static {v0, v1, v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 527
    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->i:Lcom/google/googlenav/ui/android/A;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->w()LaN/B;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->x()LaN/B;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/google/googlenav/ui/android/A;->a(LaN/B;LaN/B;Landroid/graphics/Bitmap;)V

    .line 528
    return-void
.end method

.method private u()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 636
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    if-nez v0, :cond_6

    .line 659
    :goto_5
    return-void

    .line 639
    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    .line 640
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    .line 641
    iget v1, p0, Lcom/google/googlenav/ui/android/r;->r:I

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    .line 642
    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    .line 643
    iget v3, p0, Lcom/google/googlenav/ui/android/r;->r:I

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    .line 646
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LE/o;->a(LD/a;)V

    .line 647
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5, v7}, LE/o;->a(FFF)V

    .line 648
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v0

    iget v6, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    .line 649
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v2

    iget v6, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    .line 650
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->q:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/googlenav/ui/android/r;->r:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    .line 651
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v0

    int-to-float v6, v1

    invoke-virtual {v4, v5, v6, v7}, LE/o;->a(FFF)V

    .line 652
    sget-object v4, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v5, v2

    int-to-float v1, v1

    invoke-virtual {v4, v5, v1, v7}, LE/o;->a(FFF)V

    .line 653
    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v4, v0

    int-to-float v5, v3

    invoke-virtual {v1, v4, v5, v7}, LE/o;->a(FFF)V

    .line 654
    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v4, v2

    int-to-float v3, v3

    invoke-virtual {v1, v4, v3, v7}, LE/o;->a(FFF)V

    .line 655
    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    invoke-virtual {v1, v7, v7, v7}, LE/o;->a(FFF)V

    .line 656
    sget-object v1, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v0, v0

    invoke-virtual {v1, v0, v7, v7}, LE/o;->a(FFF)V

    .line 657
    sget-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    int-to-float v1, v2

    invoke-virtual {v0, v1, v7, v7}, LE/o;->a(FFF)V

    .line 658
    sget-object v0, Lcom/google/googlenav/ui/android/r;->s:LE/o;

    iget v1, p0, Lcom/google/googlenav/ui/android/r;->q:I

    int-to-float v1, v1

    invoke-virtual {v0, v1, v7, v7}, LE/o;->a(FFF)V

    goto :goto_5
.end method

.method private v()V
    .registers 4

    .prologue
    .line 663
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getUiThreadHandler()Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/android/v;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/v;-><init>(Lcom/google/googlenav/ui/android/r;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 675
    return-void
.end method

.method private w()LaN/B;
    .registers 4

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method private x()LaN/B;
    .registers 4

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/android/B;Lcom/google/googlenav/ui/android/A;Lcom/google/googlenav/ui/android/x;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 337
    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/r;->G:Z

    .line 338
    iput-object p1, p0, Lcom/google/googlenav/ui/android/r;->h:Lcom/google/googlenav/ui/android/B;

    .line 339
    iput-object p2, p0, Lcom/google/googlenav/ui/android/r;->i:Lcom/google/googlenav/ui/android/A;

    .line 340
    iput-object p3, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    .line 341
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->u()Lbf/bk;

    move-result-object v0

    .line 343
    if-eqz v0, :cond_e0

    invoke-virtual {v0}, Lbf/bk;->bI()Lbf/ah;

    move-result-object v0

    :goto_23
    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    .line 344
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    if-eqz v0, :cond_2e

    .line 345
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->E:Lbf/ah;

    invoke-virtual {v0, v1}, Lbf/ah;->a(Z)V

    .line 349
    :cond_2e
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->E()V

    .line 350
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, v5}, Lbf/am;->b(Z)V

    .line 351
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, v6}, Lbf/am;->a(Ljava/lang/String;)Lbf/aM;

    .line 355
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->a()Lcom/google/googlenav/ui/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->k:Lcom/google/googlenav/ui/au;

    .line 356
    new-instance v0, Lcom/google/googlenav/ui/android/z;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/z;-><init>(Lcom/google/googlenav/ui/android/r;)V

    .line 357
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_e3

    .line 358
    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getTabletDialog()Lcom/google/googlenav/ui/view/android/bC;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bC;->a(Lcom/google/googlenav/ui/au;)V

    .line 363
    :goto_81
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->q()V

    .line 364
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->r()V

    .line 366
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->a(Lcom/google/googlenav/ui/android/D;)V

    .line 367
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {v0}, LaN/u;->i()V

    .line 368
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->n()V

    .line 371
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->B:Lcom/google/android/maps/driveabout/vector/aV;

    const/16 v1, 0x35b

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aV;->b:Lcom/google/android/maps/driveabout/vector/aX;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/aV;->a:Lo/ao;

    sget v4, Lcom/google/googlenav/ui/android/r;->d:I

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aV;->a(Ljava/lang/String;Lcom/google/android/maps/driveabout/vector/aX;Lo/ao;FZ)[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    .line 375
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {v0}, LaN/u;->n()I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->n:LaN/u;

    invoke-virtual {v1}, LaN/u;->o()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/android/r;->b(II)V

    .line 377
    iput-boolean v5, p0, Lcom/google/googlenav/ui/android/r;->o:Z

    .line 380
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getButtonContainer()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/android/ButtonContainer;->setVisibility(I)V

    .line 383
    new-instance v0, Lcom/google/googlenav/ui/android/y;

    invoke-direct {v0, p0, v6}, Lcom/google/googlenav/ui/android/y;-><init>(Lcom/google/googlenav/ui/android/r;Lcom/google/googlenav/ui/android/s;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/r;->g:Lcom/google/googlenav/ui/android/y;

    .line 384
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->l:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/AndroidVectorView;->j()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->g:Lcom/google/googlenav/ui/android/y;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->a(LS/a;)V

    .line 385
    const/16 v0, 0x34c

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/android/r;->a(Ljava/lang/String;)V

    .line 386
    return-void

    :cond_e0
    move-object v0, v6

    .line 343
    goto/16 :goto_23

    .line 360
    :cond_e3
    iget-object v1, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/MapsActivity;->getMapViewMenuController()Lcom/google/googlenav/ui/as;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/as;->a(Lcom/google/googlenav/ui/au;)V

    goto :goto_81
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 448
    iget-boolean v0, p0, Lcom/google/googlenav/ui/android/r;->G:Z

    return v0
.end method

.method public b()V
    .registers 1

    .prologue
    .line 534
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->s()V

    .line 535
    return-void
.end method

.method public b(II)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 591
    iput p2, p0, Lcom/google/googlenav/ui/android/r;->r:I

    .line 592
    iput p1, p0, Lcom/google/googlenav/ui/android/r;->q:I

    .line 595
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_7f

    move v0, v1

    .line 602
    :goto_15
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->ar()Z

    move-result v3

    if-eqz v3, :cond_a0

    .line 603
    if-eqz v0, :cond_81

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_81

    .line 606
    int-to-float v0, v2

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v2, v2, v1

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 607
    int-to-float v2, p2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v4, v4, v1

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 621
    :goto_45
    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_af

    const/16 v2, 0x46

    :goto_55
    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    .line 624
    new-instance v3, Landroid/graphics/Point;

    iget v4, p0, Lcom/google/googlenav/ui/android/r;->q:I

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->r:I

    sub-int/2addr v5, v2

    add-int/2addr v5, v0

    div-int/lit8 v5, v5, 0x2

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    iput-object v3, p0, Lcom/google/googlenav/ui/android/r;->e:Landroid/graphics/Point;

    .line 626
    new-instance v3, Landroid/graphics/Point;

    iget v4, p0, Lcom/google/googlenav/ui/android/r;->q:I

    add-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    iget v5, p0, Lcom/google/googlenav/ui/android/r;->r:I

    add-int/2addr v2, v5

    add-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    invoke-direct {v3, v4, v0}, Landroid/graphics/Point;-><init>(II)V

    iput-object v3, p0, Lcom/google/googlenav/ui/android/r;->f:Landroid/graphics/Point;

    .line 628
    iput-boolean v1, p0, Lcom/google/googlenav/ui/android/r;->F:Z

    .line 629
    return-void

    :cond_7f
    move v0, v2

    .line 595
    goto :goto_15

    .line 611
    :cond_81
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v0

    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v2, v2, v1

    float-to-int v2, v2

    sub-int/2addr v0, v2

    .line 612
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v3, v3, v1

    float-to-int v3, v3

    add-int/2addr v2, v3

    sub-int v2, p2, v2

    goto :goto_45

    .line 618
    :cond_a0
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v0, v0, v1

    float-to-int v0, v0

    sub-int v0, v2, v0

    .line 619
    iget-object v2, p0, Lcom/google/googlenav/ui/android/r;->y:[F

    aget v2, v2, v1

    float-to-int v2, v2

    sub-int v2, p2, v2

    goto :goto_45

    .line 621
    :cond_af
    const/16 v2, 0x5a

    goto :goto_55
.end method

.method public c()V
    .registers 8

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    iget-boolean v6, v0, Lcom/google/googlenav/ui/android/w;->i:Z

    .line 679
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->j:Lcom/google/googlenav/ui/android/x;

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->w()LaN/B;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->x()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/android/r;->m:Lcom/google/android/maps/MapsActivity;

    invoke-virtual {v3}, Lcom/google/android/maps/MapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/android/i;->h()LaN/u;

    move-result-object v3

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/android/r;->D:LC/a;

    invoke-virtual {v4}, LC/a;->r()F

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/ui/android/x;->a(LaN/B;LaN/B;IFLcom/google/googlenav/ui/android/w;)V

    .line 683
    iget-object v0, p0, Lcom/google/googlenav/ui/android/r;->C:Lcom/google/googlenav/ui/android/w;

    iget-boolean v0, v0, Lcom/google/googlenav/ui/android/w;->i:Z

    if-eq v6, v0, :cond_34

    .line 684
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/r;->v()V

    .line 686
    :cond_34
    return-void
.end method
