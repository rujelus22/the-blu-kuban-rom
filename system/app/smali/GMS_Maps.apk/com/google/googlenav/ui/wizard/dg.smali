.class public Lcom/google/googlenav/ui/wizard/dg;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/c;


# instance fields
.field private A:Lcom/google/googlenav/ui/view/d;

.field private B:LaN/B;

.field private C:LaN/B;

.field private D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private E:Ljava/util/List;

.field private F:Lcom/google/googlenav/ui/wizard/dy;

.field private G:Z

.field private H:Ljava/lang/String;

.field private I:Z

.field private J:Z

.field private K:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private L:Z

.field private M:Z

.field private final N:Lcom/google/googlenav/aA;

.field private final O:Landroid/content/DialogInterface$OnCancelListener;

.field private volatile P:LaH/A;

.field private final Q:Lbf/az;

.field protected a:I

.field protected b:Ljava/lang/String;

.field protected c:B

.field protected final i:LaH/m;

.field protected j:Z

.field protected final k:Landroid/graphics/Point;

.field private l:I

.field private m:Z

.field private n:I

.field private o:I

.field private p:I

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Z

.field private final v:LaN/p;

.field private final w:LaN/u;

.field private final x:Lcom/google/googlenav/J;

.field private final y:Lcom/google/googlenav/ui/ak;

.field private z:J


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/jv;LaN/p;LaN/u;LaH/m;Lcom/google/googlenav/aA;Lcom/google/googlenav/J;Lcom/google/googlenav/ui/ak;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 355
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 114
    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    .line 117
    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    .line 143
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->o:I

    .line 157
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    .line 268
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    .line 333
    new-instance v0, Lcom/google/googlenav/ui/wizard/dh;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dh;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    .line 356
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    .line 357
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/dg;->w:LaN/u;

    .line 358
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    .line 359
    iput-object p6, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    .line 360
    iput-object p7, p0, Lcom/google/googlenav/ui/wizard/dg;->y:Lcom/google/googlenav/ui/ak;

    .line 361
    iput-object p5, p0, Lcom/google/googlenav/ui/wizard/dg;->N:Lcom/google/googlenav/aA;

    .line 362
    new-instance v0, Lbf/az;

    invoke-direct {v0, p0}, Lbf/az;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->Q:Lbf/az;

    .line 363
    return-void
.end method

.method private declared-synchronized B()V
    .registers 3

    .prologue
    .line 450
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    if-eqz v0, :cond_c

    .line 451
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    invoke-interface {v0, v1}, LaH/m;->b(LaH/A;)V

    .line 453
    :cond_c
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_11

    .line 454
    monitor-exit p0

    return-void

    .line 450
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private C()V
    .registers 3

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_9

    .line 600
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 603
    :cond_9
    new-instance v0, Lcom/google/googlenav/ui/wizard/dC;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/googlenav/ui/wizard/dC;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 604
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 605
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 606
    return-void
.end method

.method private D()V
    .registers 3

    .prologue
    .line 609
    new-instance v0, Lcom/google/googlenav/ui/wizard/dG;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/googlenav/ui/wizard/dG;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 610
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 611
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 612
    return-void
.end method

.method private E()V
    .registers 3

    .prologue
    .line 615
    new-instance v0, Lcom/google/googlenav/ui/wizard/dM;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/googlenav/ui/wizard/dM;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 616
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->O:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/aL;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 617
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 618
    return-void
.end method

.method private F()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 681
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    .line 682
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_e

    .line 683
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 684
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    .line 687
    :cond_e
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->c()V

    .line 688
    return-void
.end method

.method private G()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x3

    .line 1414
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_40

    .line 1448
    :goto_7
    :pswitch_7
    return-void

    .line 1416
    :pswitch_8
    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1417
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->H()V

    goto :goto_7

    .line 1421
    :pswitch_f
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->F()V

    .line 1422
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1b

    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->L:Z

    if-eqz v0, :cond_22

    .line 1423
    :cond_1b
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->H()V

    .line 1424
    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_7

    .line 1426
    :cond_22
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1427
    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_7

    .line 1434
    :pswitch_29
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    .line 1435
    iput v3, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    .line 1436
    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_7

    .line 1439
    :pswitch_32
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1440
    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_7

    .line 1444
    :pswitch_38
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1445
    iput v2, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_7

    .line 1414
    nop

    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_8
        :pswitch_f
        :pswitch_7
        :pswitch_29
        :pswitch_7
        :pswitch_f
        :pswitch_38
        :pswitch_7
        :pswitch_32
        :pswitch_38
    .end packed-switch
.end method

.method private H()V
    .registers 2

    .prologue
    .line 1454
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    .line 1455
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    if-eqz v0, :cond_c

    .line 1456
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/dy;->T_()V

    .line 1458
    :cond_c
    return-void
.end method

.method private I()V
    .registers 3

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->f()V

    .line 1600
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1601
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    .line 1603
    :cond_14
    return-void
.end method

.method private a(Landroid/os/Handler;)Las/b;
    .registers 5
    .parameter

    .prologue
    .line 501
    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dp;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/dp;-><init>(Lcom/google/googlenav/ui/wizard/dg;Landroid/os/Handler;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    .line 523
    return-object v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;
    .registers 4
    .parameter

    .prologue
    .line 1656
    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/googlenav/ui/aT;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/D;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lax/y;->a(LaN/B;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    return-object p1
.end method

.method private a(Lax/y;)V
    .registers 5
    .parameter

    .prologue
    .line 1613
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/dy;->S_()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-virtual {p1}, Lax/y;->o()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 1614
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1615
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {p1}, Lax/y;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dk;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dk;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->i(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 1634
    :goto_37
    return-void

    .line 1632
    :cond_38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;Lcom/google/googlenav/aZ;)V

    goto :goto_37
.end method

.method private a(Lax/y;Lcom/google/googlenav/aZ;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 1638
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    invoke-virtual {v0}, LaN/p;->e()I

    move-result v0

    .line 1639
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    int-to-long v2, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2, p2}, Lcom/google/googlenav/ui/wizard/dy;->a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V

    .line 1640
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1641
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/r;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1687
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 1688
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->e:Lcom/google/googlenav/ui/bi;

    iget-byte v1, p0, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/bi;->a(BC)Lcom/google/googlenav/e;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->k:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v0, p1, v1, v2}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/r;II)V

    .line 1690
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->t:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_34

    if-eqz p2, :cond_34

    .line 1691
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_34

    .line 1692
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    .line 1693
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    .line 1696
    :cond_34
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/dQ;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 391
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v0}, LaH/m;->k()V

    .line 392
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 393
    invoke-interface {p1, v8}, Lcom/google/googlenav/ui/wizard/dQ;->a(Z)V

    .line 444
    :goto_12
    return-void

    .line 407
    :cond_13
    new-instance v7, Landroid/os/Handler;

    invoke-direct {v7}, Landroid/os/Handler;-><init>()V

    .line 409
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x291

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dl;

    invoke-direct {v2, p0, p1}, Lcom/google/googlenav/ui/wizard/dl;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dQ;)V

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v6}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 435
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    .line 436
    if-eqz v0, :cond_37

    .line 437
    const/4 v1, 0x0

    invoke-interface {v0, v8, v1}, LaH/A;->a(ILaH/m;)V

    .line 439
    :cond_37
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->b(Lcom/google/googlenav/ui/wizard/dQ;)LaH/A;

    move-result-object v0

    .line 440
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    .line 441
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1, v0}, LaH/m;->a(LaH/A;)V

    .line 442
    invoke-direct {p0, v7}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/os/Handler;)Las/b;

    move-result-object v0

    .line 443
    invoke-virtual {v0}, Las/b;->g()V

    goto :goto_12
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;)V
    .registers 1
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->G()V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Lax/y;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Lax/y;Lcom/google/googlenav/aZ;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;Lcom/google/googlenav/aZ;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dg;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    return p1
.end method

.method private b(Lcom/google/googlenav/ui/wizard/dQ;)LaH/A;
    .registers 3
    .parameter

    .prologue
    .line 463
    new-instance v0, Lcom/google/googlenav/ui/wizard/dm;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/dm;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dQ;)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/dg;)LaH/A;
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->P:LaH/A;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object p1
.end method

.method private b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 621
    const/4 v0, 0x1

    .line 622
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    sget-object v2, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    new-instance v3, Lcom/google/googlenav/ui/wizard/dr;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/dr;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {v1, p1, v2, v0, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V

    .line 635
    return-void
.end method

.method private b(Lcom/google/googlenav/ui/wizard/dF;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 538
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->b:Ljava/lang/String;

    .line 539
    iget v0, p1, Lcom/google/googlenav/ui/wizard/dF;->b:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    .line 541
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->j:Z

    .line 542
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/dF;->a()V

    .line 544
    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->c:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    .line 545
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->q:Ljava/lang/String;

    .line 547
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->h:Lcom/google/googlenav/ui/wizard/dy;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    .line 548
    iget v0, p1, Lcom/google/googlenav/ui/wizard/dF;->i:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->o:I

    .line 550
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    .line 552
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    .line 553
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->t:Ljava/lang/String;

    .line 554
    iget-byte v0, p1, Lcom/google/googlenav/ui/wizard/dF;->l:B

    iput-byte v0, p0, Lcom/google/googlenav/ui/wizard/dg;->c:B

    .line 555
    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->m:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->u:Z

    .line 556
    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->n:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->J:Z

    .line 557
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->o:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->K:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 559
    iget-boolean v0, p1, Lcom/google/googlenav/ui/wizard/dF;->r:Z

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->M:Z

    .line 561
    invoke-virtual {p1}, Lcom/google/googlenav/ui/wizard/dF;->b()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 562
    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->L:Z

    .line 563
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->p:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 564
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->q:Ljava/util/List;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    .line 565
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    .line 566
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 582
    :cond_59
    :goto_59
    return-void

    .line 567
    :cond_5a
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->e:LaN/B;

    if-eqz v0, :cond_73

    .line 568
    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->L:Z

    .line 569
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->e:LaN/B;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->C:LaN/B;

    .line 570
    iget-object v0, p1, Lcom/google/googlenav/ui/wizard/dF;->e:LaN/B;

    iget v1, p1, Lcom/google/googlenav/ui/wizard/dF;->f:I

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    .line 571
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    .line 572
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_59

    .line 575
    :cond_73
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    if-ne v0, v1, :cond_7e

    .line 576
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    .line 577
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_59

    .line 578
    :cond_7e
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    if-eqz v0, :cond_59

    .line 579
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_59
.end method

.method private c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 6
    .parameter

    .prologue
    .line 1647
    invoke-static {p1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lax/y;

    move-result-object v0

    .line 1649
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, p1, v3}, Lcom/google/googlenav/ui/wizard/dy;->a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V

    .line 1650
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1651
    return-void
.end method

.method private c(Lcom/google/googlenav/ui/r;)V
    .registers 5
    .parameter

    .prologue
    .line 1668
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    if-eqz v0, :cond_21

    .line 1669
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1670
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->e()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 1671
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v1, v2, v0}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    .line 1675
    :goto_1c
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1, v0, p1}, Lcom/google/googlenav/ui/view/d;->a(Landroid/graphics/Point;Lcom/google/googlenav/ui/r;)V

    .line 1677
    :cond_21
    return-void

    .line 1673
    :cond_22
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v1, v2, v0}, LaN/p;->b(LaN/B;Landroid/graphics/Point;)V

    goto :goto_1c
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/dg;)V
    .registers 1
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->B()V

    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/dg;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->q:Ljava/lang/String;

    return-object v0
.end method

.method public static e()I
    .registers 1

    .prologue
    .line 1153
    const/16 v0, 0x1f

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/dg;)Z
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->M:Z

    return v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/dg;)Z
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->u:Z

    return v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/wizard/dg;)Z
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    return v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/wizard/dg;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/wizard/dg;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/wizard/dg;)Z
    .registers 2
    .parameter

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    return v0
.end method


# virtual methods
.method protected A()V
    .registers 4

    .prologue
    .line 1526
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1527
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->a()V

    .line 1528
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->y()Lcom/google/googlenav/ui/wizard/eO;

    move-result-object v0

    .line 1530
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->F:Lcom/google/googlenav/ui/wizard/dy;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/eO;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/ui/wizard/dy;)V

    .line 1531
    return-void
.end method

.method public a(LaN/B;)I
    .registers 4
    .parameter

    .prologue
    .line 1312
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_11

    .line 1313
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    .line 1314
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->Q:Lbf/az;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v0, v1}, Lbf/az;->a(LaN/B;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    .line 1316
    :cond_11
    const/4 v0, 0x3

    return v0
.end method

.method public a(Lat/a;)I
    .registers 5
    .parameter

    .prologue
    .line 1276
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 1277
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->h()V

    .line 1278
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    .line 1307
    :goto_d
    return v0

    .line 1281
    :cond_e
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_54

    .line 1307
    :cond_13
    :goto_13
    :pswitch_13
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_d

    .line 1285
    :pswitch_16
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v0

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_13

    .line 1286
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->j:Z

    if-eqz v0, :cond_13

    .line 1287
    const/16 v0, 0x32b

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(IILjava/lang/Object;)Z

    goto :goto_13

    .line 1293
    :pswitch_2a
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_35

    .line 1294
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_13

    .line 1296
    :cond_35
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->w:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    .line 1297
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    if-eqz v0, :cond_4e

    .line 1298
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    const v1, 0x1869f

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    .line 1299
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_13

    .line 1301
    :cond_4e
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_13

    .line 1281
    nop

    :pswitch_data_54
    .packed-switch 0x0
        :pswitch_16
        :pswitch_2a
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_16
    .end packed-switch
.end method

.method public a(Lat/b;)I
    .registers 5
    .parameter

    .prologue
    .line 1321
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_34

    .line 1340
    :goto_5
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    return v0

    .line 1327
    :pswitch_8
    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v0

    if-nez v0, :cond_14

    invoke-virtual {p1}, Lat/b;->j()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1328
    :cond_14
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->Q:Lbf/az;

    invoke-virtual {v0}, Lbf/az;->b()Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->A:Lcom/google/googlenav/ui/view/d;

    .line 1329
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->w:LaN/u;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/u;->b(II)LaN/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    .line 1334
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_5

    .line 1336
    :cond_30
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_5

    .line 1321
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method protected a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 902
    new-instance v0, Lcom/google/googlenav/ui/wizard/dz;

    invoke-direct {v0, p1, p2}, Lcom/google/googlenav/ui/wizard/dz;-><init>(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 780
    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {v0, p1, v3}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    .line 783
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 784
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1}, LaH/m;->h()Z

    move-result v1

    if-eqz v1, :cond_35

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v1}, LaH/m;->e()Z

    move-result v1

    if-nez v1, :cond_35

    .line 785
    const/16 v1, 0x53d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/ds;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/ds;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 797
    :cond_35
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_55

    .line 798
    const/16 v1, 0x11a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dt;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dt;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 809
    :cond_55
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_75

    .line 810
    const/16 v1, 0x53c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/du;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/du;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 821
    :cond_75
    invoke-virtual {p0, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(I)Z

    move-result v1

    if-eqz v1, :cond_93

    .line 822
    const/16 v1, 0x53e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dv;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dv;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 833
    :cond_93
    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->j:Z

    if-eqz v1, :cond_af

    .line 834
    const/16 v1, 0x2db

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->au:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/di;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/di;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/text/SpannableStringBuilder;Landroid/view/View$OnClickListener;)Lbj/H;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 845
    :cond_af
    return-object v0
.end method

.method protected a(II)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x3

    .line 1196
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    .line 1197
    iput p1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    .line 1203
    if-eq p1, v7, :cond_15

    .line 1204
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_12

    .line 1205
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 1207
    :cond_12
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 1211
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_1e

    .line 1212
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->i()V

    .line 1215
    :cond_1e
    packed-switch p1, :pswitch_data_90

    .line 1263
    :goto_21
    :pswitch_21
    return-void

    .line 1217
    :pswitch_22
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1220
    :pswitch_26
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    .line 1221
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1224
    :pswitch_2c
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->I()V

    .line 1225
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1228
    :pswitch_32
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/ui/wizard/dg;->z:J

    .line 1229
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->t:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 1230
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1233
    :pswitch_4d
    const-string v0, ""

    .line 1234
    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_57

    .line 1236
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->H:Ljava/lang/String;

    .line 1238
    :cond_57
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Ljava/lang/String;)V

    .line 1239
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1243
    :pswitch_5d
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    if-eqz v0, :cond_72

    .line 1244
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x4f1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 1247
    :cond_72
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1251
    :pswitch_75
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->D()V

    .line 1252
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1255
    :pswitch_7b
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->E()V

    .line 1256
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1259
    :pswitch_81
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 1260
    iput v7, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    goto :goto_21

    .line 1215
    nop

    :pswitch_data_90
    .packed-switch -0x1
        :pswitch_22
        :pswitch_26
        :pswitch_2c
        :pswitch_32
        :pswitch_4d
        :pswitch_5d
        :pswitch_75
        :pswitch_7b
        :pswitch_21
        :pswitch_21
        :pswitch_81
    .end packed-switch
.end method

.method protected a(LaN/B;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1723
    new-instance v0, Lcom/google/googlenav/friend/bg;

    invoke-direct {v0}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/friend/bg;->f(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/dP;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/dP;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->u:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->J:Z

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->b(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->K:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v0

    .line 1734
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 1735
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/dF;)V
    .registers 2
    .parameter

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->b(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 535
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 1268
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1269
    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v1

    const-class v2, Lcom/google/googlenav/ui/wizard/EnterAddressActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1271
    invoke-static {}, Lcom/google/googlenav/android/S;->a()Lcom/google/googlenav/android/S;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/dB;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/googlenav/ui/wizard/dB;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dh;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/android/S;->a(Landroid/content/Intent;Lcom/google/googlenav/android/T;)V

    .line 1272
    return-void
.end method

.method protected a(I)Z
    .registers 3
    .parameter

    .prologue
    .line 1164
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 1535
    sparse-switch p1, :sswitch_data_8c

    .line 1591
    const/4 v0, 0x0

    :goto_6
    return v0

    .line 1537
    :sswitch_7
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->A()V

    move v0, v1

    .line 1538
    goto :goto_6

    .line 1540
    :sswitch_c
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->n:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    .line 1541
    goto :goto_6

    .line 1543
    :sswitch_13
    check-cast p3, Ljava/lang/String;

    .line 1544
    invoke-static {p3}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    .line 1545
    invoke-virtual {p0, v2}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    .line 1546
    goto :goto_6

    .line 1548
    :sswitch_21
    if-ne p2, v2, :cond_2a

    .line 1549
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_28
    :goto_28
    move v0, v1

    .line 1555
    goto :goto_6

    .line 1551
    :cond_2a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    if-eqz v0, :cond_28

    .line 1552
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_28

    .line 1557
    :sswitch_3a
    check-cast p3, Ljava/lang/String;

    .line 1558
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1559
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4b

    .line 1560
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(Ljava/lang/String;)V

    :goto_49
    move v0, v1

    .line 1564
    goto :goto_6

    .line 1562
    :cond_4b
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_49

    .line 1566
    :sswitch_50
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    .line 1567
    goto :goto_6

    .line 1569
    :sswitch_56
    const/16 v0, 0x9

    invoke-virtual {p0, v0, p2}, Lcom/google/googlenav/ui/wizard/dg;->a(II)V

    move v0, v1

    .line 1570
    goto :goto_6

    .line 1576
    :sswitch_5d
    check-cast p3, Ljava/lang/String;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    move v0, v1

    .line 1577
    goto :goto_6

    .line 1580
    :sswitch_63
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->i:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 1581
    if-eqz v0, :cond_82

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    if-eqz v2, :cond_82

    .line 1582
    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v0}, LaH/h;->a(Landroid/location/Location;)I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    .line 1583
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1584
    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    :cond_82
    move v0, v1

    .line 1586
    goto :goto_6

    .line 1588
    :sswitch_84
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    move v0, v1

    .line 1589
    goto/16 :goto_6

    .line 1535
    nop

    :sswitch_data_8c
    .sparse-switch
        0x1f5 -> :sswitch_63
        0x1f6 -> :sswitch_84
        0x327 -> :sswitch_13
        0x328 -> :sswitch_21
        0x329 -> :sswitch_3a
        0x32b -> :sswitch_7
        0x32c -> :sswitch_c
        0x32d -> :sswitch_50
        0x32e -> :sswitch_5d
        0x32f -> :sswitch_56
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .registers 4
    .parameter

    .prologue
    .line 1712
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->m:Z

    if-eqz v0, :cond_12

    .line 1713
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    const v1, 0x1869f

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(LaN/B;I)V

    .line 1714
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1718
    :goto_10
    const/4 v0, 0x1

    return v0

    .line 1716
    :cond_12
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_10
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 588
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_9

    .line 589
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->D()V

    .line 596
    :cond_8
    :goto_8
    return-void

    .line 590
    :cond_9
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->l:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->C:LaN/B;

    if-nez v0, :cond_8

    .line 594
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->C()V

    goto :goto_8
.end method

.method protected b(I)V
    .registers 3
    .parameter

    .prologue
    .line 1184
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(II)V

    .line 1185
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/r;)V
    .registers 3
    .parameter

    .prologue
    .line 1345
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v0, :pswitch_data_14

    .line 1357
    :goto_5
    :pswitch_5
    return-void

    .line 1348
    :pswitch_6
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Lcom/google/googlenav/ui/r;)V

    goto :goto_5

    .line 1351
    :pswitch_a
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/dg;->c(Lcom/google/googlenav/ui/r;)V

    goto :goto_5

    .line 1354
    :pswitch_e
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/r;Z)V

    goto :goto_5

    .line 1345
    nop

    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_6
        :pswitch_a
        :pswitch_e
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1701
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_9

    .line 1702
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->dismiss()V

    .line 1704
    :cond_9
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 1706
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dg;->H:Ljava/lang/String;

    .line 1707
    invoke-static {p1}, Lax/y;->a(Ljava/lang/String;)Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    .line 1708
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/t;)Z
    .registers 3
    .parameter

    .prologue
    .line 1739
    const/4 v0, 0x0

    return v0
.end method

.method public b(Z)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1361
    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    packed-switch v1, :pswitch_data_36

    .line 1376
    :pswitch_6
    const/4 v0, 0x0

    :cond_7
    :goto_7
    :pswitch_7
    return v0

    .line 1364
    :pswitch_8
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/wizard/C;->b(Z)Z

    move-result v0

    goto :goto_7

    .line 1368
    :pswitch_d
    iget-wide v1, p0, Lcom/google/googlenav/ui/wizard/dg;->z:J

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_7

    .line 1370
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->v:LaN/p;

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/dg;->B:LaN/B;

    invoke-virtual {v2, v3}, LaN/p;->a(LaN/B;)Lo/D;

    move-result-object v2

    invoke-static {v1, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->a(Lax/y;)V

    .line 1372
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    goto :goto_7

    .line 1361
    nop

    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_d
        :pswitch_6
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method

.method protected c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 639
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 642
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    if-nez v0, :cond_11

    .line 643
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->F()V

    .line 646
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->C:LaN/B;

    .line 647
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->D:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 648
    iput-object v2, p0, Lcom/google/googlenav/ui/wizard/dg;->E:Ljava/util/List;

    .line 651
    :cond_11
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->f()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_22

    .line 652
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->j()Lbf/by;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lbf/by;->e(ILjava/lang/Object;)V

    .line 654
    :cond_22
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 668
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    .line 669
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    .line 670
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    .line 671
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->a()V

    .line 672
    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->I:Z

    .line 673
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->j()V

    .line 674
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->r:Ljava/lang/String;

    .line 675
    return-void
.end method

.method protected f()I
    .registers 2

    .prologue
    .line 1174
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->o:I

    return v0
.end method

.method protected g()V
    .registers 2

    .prologue
    .line 1462
    new-instance v0, Lcom/google/googlenav/ui/wizard/dj;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dj;-><init>(Lcom/google/googlenav/ui/wizard/dg;)V

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->a(Lcom/google/googlenav/ui/wizard/dQ;)V

    .line 1504
    return-void
.end method

.method public h()V
    .registers 1

    .prologue
    .line 1410
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/dg;->G()V

    .line 1411
    return-void
.end method

.method protected i()V
    .registers 2

    .prologue
    .line 1508
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1509
    return-void
.end method

.method public k()I
    .registers 3

    .prologue
    .line 658
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 659
    const/16 v0, 0x8

    .line 661
    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public m()V
    .registers 3

    .prologue
    .line 1787
    iget v0, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_19

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1791
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->n()V

    .line 1792
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->x:Lcom/google/googlenav/J;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dg;->s:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->b(Ljava/lang/String;)V

    .line 1794
    :cond_19
    return-void
.end method

.method public p()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 1393
    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_15

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_15

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    if-eq v1, v0, :cond_15

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->p:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_16

    :cond_15
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public v()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1405
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dg;->o()Z

    move-result v1

    if-eqz v1, :cond_c

    iget v1, p0, Lcom/google/googlenav/ui/wizard/dg;->a:I

    if-ne v1, v0, :cond_c

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected y()V
    .registers 4

    .prologue
    .line 1513
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dg;->N:Lcom/google/googlenav/aA;

    check-cast v0, Lcom/google/googlenav/android/d;

    new-instance v1, Lcom/google/googlenav/ui/wizard/dx;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/dx;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/wizard/dh;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/d;->a(Lcom/google/googlenav/android/T;)V

    .line 1515
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1516
    return-void
.end method

.method public z()V
    .registers 2

    .prologue
    .line 1520
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dg;->b(I)V

    .line 1521
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/dg;->G:Z

    .line 1522
    return-void
.end method
