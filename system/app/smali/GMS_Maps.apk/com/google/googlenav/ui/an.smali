.class Lcom/google/googlenav/ui/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaH/m;

.field final synthetic b:I

.field final synthetic c:Lcom/google/googlenav/ui/ak;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/ak;LaH/m;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    iput-object p2, p0, Lcom/google/googlenav/ui/an;->a:LaH/m;

    iput p3, p0, Lcom/google/googlenav/ui/an;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 372
    iget-object v1, p0, Lcom/google/googlenav/ui/an;->a:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 420
    :goto_a
    return-void

    .line 378
    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 381
    iget v1, p0, Lcom/google/googlenav/ui/an;->b:I

    packed-switch v1, :pswitch_data_7e

    goto :goto_a

    .line 405
    :pswitch_16
    const-string v0, "u"

    .line 406
    iget-object v1, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->h(Lcom/google/googlenav/ui/ak;)Ljava/lang/String;

    move-result-object v1

    .line 410
    :goto_1e
    if-eqz v0, :cond_2c

    .line 411
    const-string v3, "t"

    invoke-static {v3, v0, v2}, Lbm/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 413
    iget-object v0, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    const-string v3, "e"

    invoke-virtual {v0, v3, v2}, Lcom/google/googlenav/ui/ak;->a(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 416
    :cond_2c
    if-eqz v1, :cond_37

    .line 417
    iget-object v0, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->i(Lcom/google/googlenav/ui/ak;)Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    .line 419
    :cond_37
    iget-object v0, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v0}, Lcom/google/googlenav/ui/ak;->e(Lcom/google/googlenav/ui/ak;)V

    goto :goto_a

    .line 383
    :pswitch_3d
    iget-object v1, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->f(Lcom/google/googlenav/ui/ak;)Z

    move-result v1

    if-eqz v1, :cond_7b

    .line 384
    iget-object v0, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/ak;->b(Lcom/google/googlenav/ui/ak;Z)Z

    .line 385
    const/16 v0, 0x3e7

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 386
    const-string v0, "p"

    goto :goto_1e

    .line 390
    :pswitch_53
    iget-object v1, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->g(Lcom/google/googlenav/ui/ak;)Z

    move-result v1

    if-eqz v1, :cond_7b

    .line 391
    iget-object v0, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/ak;->c(Lcom/google/googlenav/ui/ak;Z)Z

    .line 392
    const/16 v0, 0x500

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 393
    const-string v0, "s"

    goto :goto_1e

    .line 397
    :pswitch_69
    const-string v0, "nr"

    .line 398
    iget-object v1, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->h(Lcom/google/googlenav/ui/ak;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1e

    .line 401
    :pswitch_72
    const-string v0, "ns"

    .line 402
    iget-object v1, p0, Lcom/google/googlenav/ui/an;->c:Lcom/google/googlenav/ui/ak;

    invoke-static {v1}, Lcom/google/googlenav/ui/ak;->h(Lcom/google/googlenav/ui/ak;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1e

    :cond_7b
    move-object v1, v0

    goto :goto_1e

    .line 381
    nop

    :pswitch_data_7e
    .packed-switch 0x1
        :pswitch_16
        :pswitch_3d
        :pswitch_53
        :pswitch_69
        :pswitch_72
    .end packed-switch
.end method
