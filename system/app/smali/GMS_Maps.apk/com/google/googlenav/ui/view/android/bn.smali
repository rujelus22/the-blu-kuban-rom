.class public Lcom/google/googlenav/ui/view/android/bn;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaB/p;


# static fields
.field private static final a:Ljava/lang/CharSequence;


# instance fields
.field private b:Landroid/widget/ListView;

.field c:Lcom/google/googlenav/ai;

.field protected final d:Lbf/m;

.field protected final l:Lcom/google/googlenav/ui/br;

.field protected m:Lcom/google/googlenav/ui/view/android/J;

.field protected n:Landroid/view/View;

.field protected o:Z

.field p:Landroid/view/View;

.field private q:Lcom/google/googlenav/ui/view/dialog/ba;

.field private r:Lcom/google/googlenav/common/a;

.field private s:J

.field private final t:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 129
    const/16 v0, 0x603

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/view/android/bn;->a:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 181
    const v0, 0x7f0f0018

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    .line 161
    new-instance v0, Lcom/google/googlenav/ui/view/android/bo;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/bo;-><init>(Lcom/google/googlenav/ui/view/android/bn;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    .line 182
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    .line 183
    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    .line 184
    invoke-virtual {p2}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    .line 185
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    .line 186
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    const-wide/high16 v0, -0x8000

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    .line 189
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbl/o;->b(Ljava/lang/Runnable;)V

    .line 190
    return-void
.end method

.method private J()V
    .registers 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-eqz v0, :cond_c

    .line 473
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ba;->b()V

    .line 474
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    .line 476
    :cond_c
    return-void
.end method

.method private K()Lbj/H;
    .registers 14

    .prologue
    .line 688
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    .line 689
    invoke-direct {p0, v4}, Lcom/google/googlenav/ui/view/android/bn;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 691
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v6

    .line 692
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/aA;->e()Z

    move-result v7

    .line 695
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v8

    .line 696
    if-eqz v8, :cond_2a

    .line 697
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 701
    :cond_2a
    const/4 v0, 0x0

    .line 702
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->by()Z

    move-result v1

    if-eqz v1, :cond_14a

    .line 703
    const/16 v0, 0x5f2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 707
    :goto_3a
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bz()Z

    move-result v9

    .line 711
    if-nez v9, :cond_5b

    .line 712
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->d(Z)Ljava/lang/String;

    .line 713
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v0

    .line 714
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5b

    .line 715
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x1f

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    .line 721
    :cond_5b
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v10

    .line 723
    const/4 v0, 0x0

    .line 724
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v2

    .line 725
    if-eqz v2, :cond_87

    .line 726
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    .line 728
    invoke-interface {v0, v2}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    .line 729
    if-eqz v0, :cond_142

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_142

    const/4 v0, 0x1

    .line 733
    :cond_87
    :goto_87
    const/4 v3, 0x0

    .line 734
    const/4 v2, 0x0

    .line 735
    iget-object v11, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v11}, Lcom/google/googlenav/ai;->aj()Z

    move-result v11

    if-eqz v11, :cond_9d

    .line 736
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->aP()Ljava/lang/String;

    move-result-object v3

    .line 737
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->aQ()Ljava/lang/String;

    move-result-object v2

    .line 740
    :cond_9d
    new-instance v11, Lcom/google/googlenav/ui/view/android/bt;

    invoke-direct {v11}, Lcom/google/googlenav/ui/view/android/bt;-><init>()V

    sget-object v12, Lcom/google/googlenav/ui/aV;->aP:Lcom/google/googlenav/ui/aV;

    invoke-virtual {v11, v5, v12}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/googlenav/ui/view/android/bt;->a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/String;Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v5

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aK()Z

    move-result v4

    if-eqz v4, :cond_145

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->aL()Lai/d;

    move-result-object v4

    :goto_be
    invoke-virtual {v5, v4}, Lcom/google/googlenav/ui/view/android/bt;->a(Lai/d;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/googlenav/ui/view/android/bt;->b(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/ui/view/android/bt;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/ui/view/android/bt;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/google/googlenav/ui/view/android/bt;->a(LaN/B;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/googlenav/ui/view/android/bt;->a(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bt;->b(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/google/googlenav/ui/view/android/bt;->c(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->be()Lcom/google/googlenav/ui/br;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/br;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const v1, 0x7f0400c2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->c(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->d(Z)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/bt;->d(I)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v1

    .line 759
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ci()Lcom/google/googlenav/ar;

    move-result-object v0

    .line 760
    if-eqz v0, :cond_110

    .line 761
    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ar;)Lcom/google/googlenav/ui/view/android/bt;

    .line 765
    :cond_110
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ay()Lcom/google/googlenav/ap;

    move-result-object v2

    .line 766
    if-eqz v2, :cond_13d

    .line 767
    invoke-virtual {v2}, Lcom/google/googlenav/ap;->b()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_148

    const/4 v0, 0x1

    .line 769
    :goto_120
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v4, 0x1

    invoke-static {v3, v0, v4}, Lbf/aS;->b(Lcom/google/googlenav/ai;ZZ)Lcom/google/googlenav/ui/view/a;

    move-result-object v3

    .line 771
    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v4, v0}, Lbf/aS;->b(Lcom/google/googlenav/ai;Z)V

    .line 772
    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->L()Z

    move-result v4

    if-nez v4, :cond_136

    if-nez v0, :cond_13d

    .line 773
    :cond_136
    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/bt;->a(Lcom/google/googlenav/ui/view/a;)Lcom/google/googlenav/ui/view/android/bt;

    .line 778
    :cond_13d
    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/bt;->a()Lcom/google/googlenav/ui/view/android/bs;

    move-result-object v0

    return-object v0

    .line 729
    :cond_142
    const/4 v0, 0x0

    goto/16 :goto_87

    .line 740
    :cond_145
    const/4 v4, 0x0

    goto/16 :goto_be

    .line 767
    :cond_148
    const/4 v0, 0x0

    goto :goto_120

    :cond_14a
    move-object v1, v0

    goto/16 :goto_3a
.end method

.method private a([Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    .line 648
    new-instance v2, Ljava/util/Vector;

    const/4 v0, 0x3

    invoke-direct {v2, v0}, Ljava/util/Vector;-><init>(I)V

    .line 650
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->bl()Z

    move-result v0

    if-nez v0, :cond_87

    .line 651
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    .line 657
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->an()Z

    move-result v3

    if-eqz v3, :cond_52

    .line 658
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 659
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    .line 660
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 681
    :goto_39
    if-eqz p1, :cond_3e

    .line 682
    invoke-virtual {v2, p1}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 684
    :cond_3e
    return-object v0

    .line 663
    :cond_3f
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 664
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_39

    .line 670
    :cond_52
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 671
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 672
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_39

    .line 674
    :cond_6c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 675
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 676
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->H()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_87
    move-object v0, v1

    goto :goto_39
.end method

.method private a(Landroid/view/ViewGroup;)V
    .registers 5
    .parameter

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-nez v0, :cond_13

    .line 460
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/ba;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-direct {v0, v1, v2, p0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;-><init>(Lcom/google/googlenav/bZ;Lbf/i;Lcom/google/googlenav/ui/view/android/bn;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    .line 463
    :cond_13
    return-void
.end method

.method private e(Ljava/util/List;)V
    .registers 12
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v1, 0x1

    .line 829
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aB()Ljava/lang/String;

    move-result-object v5

    .line 830
    invoke-static {v5}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3d

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 831
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v5, v0}, Lbf/aS;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 833
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    sget-object v2, Lcom/google/googlenav/ui/view/android/bn;->a:Ljava/lang/CharSequence;

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/16 v7, 0x19

    const/4 v8, -0x1

    invoke-direct {v6, v7, v8, v5}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    move-object v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 836
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x1c

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    .line 841
    :cond_3d
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bz()Z

    move-result v0

    if-nez v0, :cond_5a

    .line 842
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bc()Lcom/google/googlenav/ac;

    move-result-object v0

    .line 843
    if-eqz v0, :cond_5a

    .line 844
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v2, v1, v9}, Lbf/aS;->a(Lcom/google/googlenav/ai;ZZ)Ljava/util/Vector;

    move-result-object v2

    .line 846
    invoke-static {v0}, Lbf/aS;->a(Lcom/google/googlenav/ac;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 847
    invoke-virtual {p0, p1, v0, v2}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Ljava/lang/CharSequence;Ljava/util/Vector;)V

    .line 852
    :cond_5a
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 853
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v2, v0, v9}, Lbf/aS;->a(Lcom/google/googlenav/ai;Ljava/util/Vector;Z)V

    .line 854
    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_68
    :goto_68
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbf/aT;

    .line 855
    invoke-static {v0, v1}, Lbf/aS;->a(Lbf/aT;I)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    .line 857
    if-eqz v0, :cond_68

    .line 858
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_68

    .line 861
    :cond_7e
    return-void
.end method

.method private f(Ljava/util/List;)Z
    .registers 3
    .parameter

    .prologue
    .line 888
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-nez v0, :cond_d

    .line 889
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->J()V

    .line 890
    const/4 v0, 0x0

    .line 895
    :goto_c
    return v0

    .line 892
    :cond_d
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->h:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Landroid/view/ViewGroup;)V

    .line 893
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->v()V

    .line 894
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/view/dialog/ba;->a(Ljava/util/List;)Ljava/util/List;

    .line 895
    const/4 v0, 0x1

    goto :goto_c
.end method

.method private n()Z
    .registers 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private v()V
    .registers 2

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-eqz v0, :cond_9

    .line 467
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ba;->c()V

    .line 469
    :cond_9
    return-void
.end method


# virtual methods
.method protected A()Lbj/H;
    .registers 5

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aJ()Z

    move-result v0

    if-eqz v0, :cond_19

    const/16 v0, 0x577

    .line 611
    :goto_a
    new-instance v1, Lbj/bv;

    const/16 v2, 0x1b

    const v3, 0x7f040119

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    return-object v1

    .line 610
    :cond_19
    const/16 v0, 0x575

    goto :goto_a
.end method

.method protected B()Ljava/util/List;
    .registers 3

    .prologue
    .line 629
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 630
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->K()Lbj/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 631
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Z)V

    .line 633
    return-object v0
.end method

.method C()Z
    .registers 2

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->K()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method protected D()I
    .registers 2

    .prologue
    .line 810
    const v0, 0x7f040118

    return v0
.end method

.method protected E()I
    .registers 2

    .prologue
    .line 925
    const v0, 0x7f040116

    return v0
.end method

.method protected F()Z
    .registers 2

    .prologue
    .line 991
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bX()Z

    move-result v0

    return v0
.end method

.method protected G()Z
    .registers 3

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aR;->a(Lcom/google/googlenav/ai;)I

    move-result v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/aR;->b(Lcom/google/googlenav/ai;)I

    move-result v1

    add-int/2addr v0, v1

    if-lez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public H()V
    .registers 3

    .prologue
    .line 1124
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1125
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    .line 1127
    :cond_10
    return-void
.end method

.method public I()V
    .registers 9

    .prologue
    const-wide/high16 v6, -0x8000

    .line 1135
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-wide v0, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2a

    .line 1138
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Lai/a;->a(Ljava/lang/String;IJ)Z

    .line 1140
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->r:Lcom/google/googlenav/common/a;

    iput-wide v6, p0, Lcom/google/googlenav/ui/view/android/bn;->s:J

    .line 1142
    :cond_2a
    return-void
.end method

.method public I_()V
    .registers 3

    .prologue
    .line 213
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 214
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    .line 216
    :cond_17
    return-void
.end method

.method protected O_()V
    .registers 2

    .prologue
    .line 277
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_e

    .line 278
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->requestWindowFeature(I)Z

    .line 280
    :cond_e
    return-void
.end method

.method public Q_()V
    .registers 3

    .prologue
    .line 1057
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    if-ne v0, v1, :cond_d

    .line 1058
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    .line 1060
    :cond_d
    return-void
.end method

.method protected a(Lbf/aQ;)Lbj/H;
    .registers 9
    .parameter

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aR;->c(Lcom/google/googlenav/ai;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 1007
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aR;->d(Lcom/google/googlenav/ai;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    .line 1008
    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v6

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1015
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1016
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1017
    invoke-static {p2, v4, v3}, Lbf/aR;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;)V

    .line 1020
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 1023
    new-instance v0, Lbj/aE;

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    move-object v1, p2

    move-object v2, p3

    move-object v6, p1

    move v7, p4

    move v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lbj/aE;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Ljava/util/List;Lcom/google/googlenav/ui/aa;Lbf/aQ;IZZ)V

    .line 1029
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_38

    .line 1030
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/br;->b()LaB/s;

    move-result-object v1

    invoke-static {v3}, Lcom/google/common/collect/aT;->e(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/view/android/bq;

    invoke-direct {v3, p0, v0}, Lcom/google/googlenav/ui/view/android/bq;-><init>(Lcom/google/googlenav/ui/view/android/bn;Lbj/aE;)V

    invoke-virtual {v1, v2, v3}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    .line 1038
    :cond_38
    return-object v0
.end method

.method protected a(Landroid/app/ActionBar;)V
    .registers 3
    .parameter

    .prologue
    .line 268
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 269
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 270
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    const v2, 0x7f100216

    .line 492
    const v0, 0x7f1001ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    .line 493
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    const v1, 0x7f1001ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x1af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_3c

    .line 496
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    :goto_3b
    return-void

    .line 499
    :cond_3c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3b
.end method

.method protected a(Lbf/aQ;Ljava/util/List;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    invoke-virtual {v0}, Lbl/o;->j()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1048
    new-instance v0, Lbj/ao;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v4, 0xe

    new-instance v5, Lcom/google/googlenav/bh;

    invoke-direct {v5}, Lcom/google/googlenav/bh;-><init>()V

    move-object v2, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lbj/ao;-><init>(Lcom/google/googlenav/ai;Lbf/aQ;Lbf/m;ILcom/google/googlenav/bh;Z)V

    .line 1051
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1053
    :cond_21
    return-void
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 4
    .parameter

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-eq p1, v0, :cond_18

    .line 195
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbl/o;->c(Ljava/lang/Runnable;)V

    .line 196
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ce()Lbl/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->t:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbl/o;->b(Ljava/lang/Runnable;)V

    .line 199
    :cond_18
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    .line 200
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 7
    .parameter

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cj()Lcom/google/googlenav/ao;

    move-result-object v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cj()Lcom/google/googlenav/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ao;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2e

    .line 592
    new-instance v0, Lbj/B;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v3}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbj/B;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/J;Z)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 595
    :cond_2e
    return-void
.end method

.method protected a(Ljava/util/List;Lbf/aQ;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->c(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 902
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 905
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aZ()I

    move-result v1

    .line 906
    invoke-static {v1}, Lcom/google/common/collect/bx;->c(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 907
    const/4 v0, 0x0

    :goto_1a
    if-ge v0, v1, :cond_28

    .line 908
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ai;->k(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 907
    add-int/lit8 v0, v0, 0x1

    goto :goto_1a

    .line 911
    :cond_28
    invoke-virtual {p2}, Lbf/aQ;->a()Z

    move-result v5

    .line 912
    new-instance v0, Lbj/bc;

    const/16 v1, 0xf

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v2

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    invoke-direct/range {v0 .. v5}, Lbj/bc;-><init>(IZLjava/util/List;Lcom/google/googlenav/ui/br;Z)V

    .line 915
    invoke-virtual {v0}, Lbj/bc;->d()Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 916
    new-instance v1, Lbj/bv;

    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->E()I

    move-result v3

    const/16 v4, 0x31c

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 919
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 922
    :cond_5a
    return-void
.end method

.method protected a(Ljava/util/List;Lbf/aQ;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 932
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->b()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 933
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 934
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bj()Lcom/google/googlenav/cu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/cu;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 935
    invoke-virtual {v2, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 937
    const/4 v0, 0x3

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 939
    const/16 v1, 0x61c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 944
    const/4 v3, 0x0

    const/16 v4, 0x14

    move-object v0, p0

    move-object v1, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    .line 946
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 948
    :cond_3c
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/CharSequence;Ljava/util/Vector;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 872
    const/4 v0, 0x0

    .line 873
    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_11

    .line 875
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    move v0, v1

    .line 878
    :cond_11
    if-nez p2, :cond_33

    const/16 v2, 0x5ff

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 880
    :goto_19
    if-eqz v0, :cond_32

    .line 881
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    new-instance v6, Lcom/google/googlenav/ui/view/a;

    const/16 v3, 0xf

    const/16 v5, 0x14

    invoke-direct {v6, v3, v5, v4}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    sget-object v7, Lcom/google/googlenav/ui/aV;->M:Lcom/google/googlenav/ui/aV;

    sget-object v8, Lcom/google/googlenav/ui/aV;->aY:Lcom/google/googlenav/ui/aV;

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/a;-><init>(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;Lcom/google/googlenav/ui/aV;Lcom/google/googlenav/ui/aV;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 885
    :cond_32
    return-void

    .line 878
    :cond_33
    const/16 v2, 0x1e1

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_19
.end method

.method protected a(Ljava/util/List;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    .line 786
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->n()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 787
    new-instance v0, Lbj/bx;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-direct {v0, v1, v2, v3, p2}, Lbj/bx;-><init>(Lcom/google/googlenav/ai;Lbf/i;IZ)V

    .line 794
    :goto_10
    invoke-virtual {v0}, Lbj/aR;->d()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 795
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 797
    :cond_19
    return-void

    .line 790
    :cond_1a
    new-instance v0, Lbj/aR;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-direct {v0, v1, v2, v3, p2}, Lbj/aR;-><init>(Lcom/google/googlenav/ai;Lbf/i;IZ)V

    goto :goto_10
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 417
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 418
    sparse-switch v0, :sswitch_data_76

    .line 450
    const/4 v0, 0x0

    :goto_b
    return v0

    .line 420
    :sswitch_c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    goto :goto_b

    .line 423
    :sswitch_15
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x258

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 424
    goto :goto_b

    .line 426
    :sswitch_1e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 427
    goto :goto_b

    .line 429
    :sswitch_26
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->z()I

    move-result v0

    if-ne v0, v1, :cond_37

    const/16 v0, 0x4b1

    .line 430
    :goto_30
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v2, v0, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 431
    goto :goto_b

    .line 429
    :cond_37
    const/16 v0, 0x4b0

    goto :goto_30

    .line 433
    :sswitch_3a
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x909

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 434
    goto :goto_b

    .line 436
    :sswitch_43
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x2bf

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 437
    goto :goto_b

    .line 439
    :sswitch_4c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x14

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 440
    goto :goto_b

    .line 442
    :sswitch_55
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->Z()Z

    move-result v0

    if-eqz v0, :cond_66

    .line 443
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x2c0

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    :cond_64
    :goto_64
    move v0, v1

    .line 448
    goto :goto_b

    .line 444
    :cond_66
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->Y()Z

    move-result v0

    if-eqz v0, :cond_64

    .line 445
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v2, 0x5dc

    invoke-virtual {v0, v2, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    goto :goto_64

    .line 418
    :sswitch_data_76
    .sparse-switch
        0x7f1002a7 -> :sswitch_3a
        0x7f1004b1 -> :sswitch_c
        0x7f1004b7 -> :sswitch_15
        0x7f1004b8 -> :sswitch_1e
        0x7f1004b9 -> :sswitch_43
        0x7f1004ba -> :sswitch_4c
        0x7f1004bb -> :sswitch_55
        0x7f1004ce -> :sswitch_26
    .end sparse-switch
.end method

.method protected b(Landroid/view/Menu;)V
    .registers 5
    .parameter

    .prologue
    .line 312
    const v0, 0x7f1004b7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 313
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->n()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 315
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 325
    :cond_11
    :goto_11
    return-void

    .line 318
    :cond_12
    const/16 v1, 0x598

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 319
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v1

    .line 320
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 321
    if-eqz v1, :cond_11

    .line 322
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_11
.end method

.method protected b(Ljava/util/List;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 616
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 617
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aS;->g(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    .line 618
    new-instance v1, Lbf/aT;

    const/4 v2, 0x0

    const/16 v3, 0x2c4

    invoke-direct {v1, v0, v2, v3, v4}, Lbf/aT;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 620
    invoke-static {v1, v4}, Lbf/aS;->a(Lbf/aT;I)Lcom/google/googlenav/ui/view/android/a;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_24

    .line 623
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 626
    :cond_24
    return-void
.end method

.method protected b(Ljava/util/List;Lbf/aQ;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 952
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bh()Ljava/util/List;

    move-result-object v0

    .line 953
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_f

    .line 967
    :goto_e
    return-void

    .line 956
    :cond_f
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 957
    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 959
    const/4 v0, 0x3

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 961
    const/16 v1, 0x126

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 964
    const/4 v3, 0x0

    const/16 v4, 0x16

    move-object v0, p0

    move-object v1, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    .line 966
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 203
    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    .line 204
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->l()V

    .line 205
    return-void
.end method

.method public c()Landroid/view/View;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-nez v0, :cond_6

    .line 262
    :goto_5
    return-object v1

    .line 228
    :cond_6
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040114

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 229
    const v0, 0x7f100352

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->p:Landroid/view/View;

    .line 232
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Landroid/view/View;)V

    .line 235
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/aC;->c(Lcom/google/googlenav/ai;)Z

    .line 236
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aS;->e(Lcom/google/googlenav/ai;)V

    .line 237
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/aS;->f(Lcom/google/googlenav/ai;)V

    .line 239
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->n()Z

    move-result v0

    if-eqz v0, :cond_92

    move-object v0, v1

    .line 241
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Landroid/view/ViewGroup;)V

    .line 247
    :goto_3c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->m()Ljava/util/List;

    move-result-object v2

    .line 250
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    .line 251
    new-instance v0, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    const/16 v5, 0x24

    invoke-direct {v0, v3, v4, v2, v5}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    .line 252
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 253
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    new-instance v2, Lcom/google/googlenav/ui/view/android/br;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/br;-><init>(Lcom/google/googlenav/ui/view/android/bn;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 254
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 255
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 256
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->C()Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 257
    new-instance v0, Lbj/aV;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v2}, Lbj/aV;-><init>(Lcom/google/googlenav/ai;)V

    .line 258
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->p:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbj/aV;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 261
    :cond_8d
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->l()V

    goto/16 :goto_5

    .line 243
    :cond_92
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->J()V

    goto :goto_3c
.end method

.method protected c(Landroid/view/Menu;)V
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 328
    const v0, 0x7f1004b8

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 329
    const/16 v0, 0x52e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 330
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_20

    const/4 v0, 0x1

    .line 331
    :goto_1a
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 332
    if-nez v0, :cond_22

    .line 345
    :goto_1f
    return-void

    :cond_20
    move v0, v1

    .line 330
    goto :goto_1a

    .line 337
    :cond_22
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-nez v0, :cond_46

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_46

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_46

    .line 340
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1f

    .line 343
    :cond_46
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    goto :goto_1f
.end method

.method protected c(Ljava/util/List;)V
    .registers 6
    .parameter

    .prologue
    .line 800
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbf/aS;->a(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    .line 801
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    .line 802
    new-instance v1, Lbj/bv;

    const/16 v2, 0x1d

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->D()I

    move-result v3

    invoke-direct {v1, v2, v3, v0}, Lbj/bv;-><init>(IILjava/lang/String;)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 804
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 807
    :cond_22
    return-void
.end method

.method protected c(Ljava/util/List;Lbf/aQ;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 971
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LbK/O;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 972
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bi()Ljava/util/List;

    move-result-object v0

    .line 973
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_15

    .line 988
    :goto_14
    return-void

    .line 976
    :cond_15
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 977
    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_19

    .line 980
    :cond_29
    const/4 v0, 0x3

    invoke-static {v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->j(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 982
    const/16 v1, 0x1e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 985
    const/4 v3, 0x0

    const/16 v4, 0x15

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/common/io/protocol/ProtoBuf;IZZ)Lbj/H;

    move-result-object v0

    .line 987
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_14
.end method

.method public d()V
    .registers 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    if-eqz v0, :cond_9

    .line 290
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->q:Lcom/google/googlenav/ui/view/dialog/ba;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ba;->a()V

    .line 292
    :cond_9
    return-void
.end method

.method protected d(Landroid/view/Menu;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 348
    .line 349
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-nez v0, :cond_5c

    :cond_12
    move v0, v2

    .line 355
    :goto_13
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->z()I

    move-result v3

    if-ne v3, v1, :cond_30

    .line 356
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->A()[Lcom/google/googlenav/ai;

    move-result-object v1

    aget-object v1, v1, v2

    .line 357
    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_30

    :cond_2f
    move v0, v2

    .line 362
    :cond_30
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v1}, Lbf/m;->bm()Z

    move-result v1

    if-eqz v1, :cond_55

    const/16 v1, 0x254

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 366
    :goto_3e
    const v2, 0x7f1004ce

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 367
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 368
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 369
    if-eqz v0, :cond_54

    .line 370
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 373
    :cond_54
    return-void

    .line 362
    :cond_55
    const/16 v1, 0x256

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3e

    :cond_5c
    move v0, v1

    goto :goto_13
.end method

.method protected d(Ljava/util/List;)V
    .registers 9
    .parameter

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bf()Lcom/google/googlenav/cx;

    move-result-object v2

    .line 815
    if-eqz v2, :cond_2a

    .line 816
    new-instance v0, Lbj/bq;

    const/16 v1, 0x1a

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->be()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->bd()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v5}, Lcom/google/googlenav/ai;->T()I

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v6}, Lcom/google/googlenav/ai;->bg()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lbj/bq;-><init>(ILcom/google/googlenav/cx;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 824
    :cond_2a
    return-void
.end method

.method protected e(Landroid/view/Menu;)V
    .registers 4
    .parameter

    .prologue
    .line 376
    const v0, 0x7f1004b9

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 377
    const/16 v1, 0x23

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 378
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/aS;->b(Lcom/google/googlenav/ai;)Z

    move-result v1

    .line 379
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 381
    if-eqz v1, :cond_22

    .line 382
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 385
    :cond_22
    return-void
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 284
    const/4 v0, 0x1

    return v0
.end method

.method protected f(Landroid/view/Menu;)V
    .registers 4
    .parameter

    .prologue
    .line 388
    const v0, 0x7f1004ba

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 389
    const/16 v1, 0x349

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 393
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 394
    return-void
.end method

.method protected g(Landroid/view/Menu;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 397
    const v1, 0x7f1004bb

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 398
    const/16 v1, 0x439

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 400
    const/4 v1, 0x0

    .line 401
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->bA()Z

    move-result v3

    if-eqz v3, :cond_4f

    .line 402
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->Z()Z

    move-result v3

    if-eqz v3, :cond_39

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/K;->q()Z

    move-result v3

    if-eqz v3, :cond_39

    .line 408
    :cond_2c
    :goto_2c
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 409
    if-eqz v0, :cond_38

    .line 410
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 413
    :cond_38
    return-void

    .line 404
    :cond_39
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->Y()Z

    move-result v3

    if-eqz v3, :cond_4f

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v4

    invoke-virtual {v3, v4}, Lbf/m;->c(LaN/B;)Z

    move-result v3

    if-nez v3, :cond_2c

    :cond_4f
    move v0, v1

    goto :goto_2c
.end method

.method public h()V
    .registers 4

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->af()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1070
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    .line 1071
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->m()Ljava/util/List;

    move-result-object v0

    .line 1072
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/view/android/J;->a(Ljava/util/List;)V

    .line 1077
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->C()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1078
    new-instance v0, Lbj/aV;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1}, Lbj/aV;-><init>(Lcom/google/googlenav/ai;)V

    .line 1079
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->p:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbj/aV;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 1082
    :cond_30
    return-void
.end method

.method protected l()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 507
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    if-eqz v0, :cond_15

    .line 508
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    if-eqz v0, :cond_16

    .line 509
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 510
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 516
    :cond_15
    :goto_15
    return-void

    .line 512
    :cond_16
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_15
.end method

.method protected m()Ljava/util/List;
    .registers 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 520
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->z()Ljava/util/List;

    move-result-object v0

    .line 524
    :goto_c
    return-object v0

    .line 521
    :cond_d
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->C()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 522
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->B()Ljava/util/List;

    move-result-object v0

    goto :goto_c

    .line 524
    :cond_18
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->y()Ljava/util/List;

    move-result-object v0

    goto :goto_c
.end method

.method protected o()Z
    .registers 2

    .prologue
    .line 1094
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 296
    sget-object v0, Lcom/google/googlenav/ui/view/android/bn;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f11001d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 298
    const v0, 0x7f1004b1

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 299
    const/16 v1, 0x37

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 300
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->x()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 302
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->b(Landroid/view/Menu;)V

    .line 303
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->c(Landroid/view/Menu;)V

    .line 304
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->d(Landroid/view/Menu;)V

    .line 305
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->e(Landroid/view/Menu;)V

    .line 306
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->f(Landroid/view/Menu;)V

    .line 307
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/bn;->g(Landroid/view/Menu;)V

    .line 308
    const/4 v0, 0x1

    return v0
.end method

.method public w()Z
    .registers 2

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bn;->o:Z

    return v0
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected x()Z
    .registers 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aw()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->m()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method protected y()Ljava/util/List;
    .registers 9

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v0

    .line 530
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-virtual {v2}, Lbf/m;->bs()Ljava/util/Hashtable;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Ljava/util/Hashtable;Ljava/lang/Object;)Lbf/aQ;

    move-result-object v0

    .line 534
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 535
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->K()Lbj/H;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v5

    .line 538
    invoke-virtual {p0, v6, v5}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Z)V

    .line 539
    invoke-virtual {p0, v0, v6, v5}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;Ljava/util/List;Z)V

    .line 542
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->f(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_2f

    move-object v0, v6

    .line 585
    :goto_2e
    return-object v0

    .line 546
    :cond_2f
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->c(Ljava/util/List;)V

    .line 547
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->d(Ljava/util/List;)V

    .line 548
    invoke-direct {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->e(Ljava/util/List;)V

    .line 549
    invoke-virtual {p0, v6, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Lbf/aQ;)V

    .line 550
    invoke-virtual {p0, v6, v0, v5}, Lcom/google/googlenav/ui/view/android/bn;->b(Ljava/util/List;Lbf/aQ;Z)V

    .line 551
    invoke-virtual {p0, v6, v0, v5}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Lbf/aQ;Z)V

    .line 552
    invoke-virtual {p0, v6, v0, v5}, Lcom/google/googlenav/ui/view/android/bn;->c(Ljava/util/List;Lbf/aQ;Z)V

    .line 554
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->F()Z

    move-result v1

    if-eqz v1, :cond_56

    .line 555
    new-instance v1, Lbj/aX;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3, v5}, Lbj/aX;-><init>(Lcom/google/googlenav/ai;IZ)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 559
    :cond_56
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->G()Z

    move-result v1

    if-eqz v1, :cond_63

    .line 560
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->a(Lbf/aQ;)Lbj/H;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 563
    :cond_63
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;)V

    .line 565
    new-instance v0, Lbj/bm;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->l:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v7, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4, v7}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-direct/range {v0 .. v5}, Lbj/bm;-><init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;ZZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 567
    new-instance v0, Lbj/bi;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lbj/bi;-><init>(Lcom/google/googlenav/ai;I)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 569
    new-instance v0, Lbj/al;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2, v5}, Lbj/al;-><init>(ILcom/google/googlenav/ai;Z)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 573
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lbf/m;->b(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 574
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    check-cast v3, Lbf/bk;

    .line 575
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3, v0}, Lbf/bk;->l(Lcom/google/googlenav/ai;)I

    move-result v2

    .line 576
    invoke-virtual {v3}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    const/16 v1, 0x575

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x13

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/g;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;

    move-result-object v0

    .line 580
    if-eqz v0, :cond_c2

    .line 581
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_c2
    move-object v0, v6

    .line 585
    goto/16 :goto_2e
.end method

.method protected z()Ljava/util/List;
    .registers 4

    .prologue
    .line 599
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 600
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->A()Lbj/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 601
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/bn;->K()Lbj/H;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 602
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/bn;->o()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/bn;->a(Ljava/util/List;Z)V

    .line 603
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/bn;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/bn;->d:Lbf/m;

    invoke-static {v1, v2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/e;Ljava/util/List;)V

    .line 604
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/bn;->b(Ljava/util/List;)V

    .line 605
    return-object v0
.end method
