.class Lcom/google/googlenav/ui/wizard/aQ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/f;


# instance fields
.field final synthetic a:Lcom/google/googlenav/h;

.field final synthetic b:Lcom/google/googlenav/ui/wizard/aW;

.field final synthetic c:Lcom/google/googlenav/ui/wizard/aP;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/h;Lcom/google/googlenav/ui/wizard/aW;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/aQ;->c:Lcom/google/googlenav/ui/wizard/aP;

    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/aQ;->a:Lcom/google/googlenav/h;

    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/aQ;->b:Lcom/google/googlenav/ui/wizard/aW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/F;)V
    .registers 12
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 83
    if-eqz p1, :cond_9

    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-gtz v0, :cond_25

    .line 86
    :cond_9
    invoke-static {}, Lcom/google/googlenav/aM;->a()Lcom/google/googlenav/aM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/aQ;->a:Lcom/google/googlenav/h;

    invoke-virtual {v1}, Lcom/google/googlenav/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aM;->a(Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aQ;->c:Lcom/google/googlenav/ui/wizard/aP;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/aP;->a(Lcom/google/googlenav/ui/wizard/aP;)Lcom/google/googlenav/android/aa;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/aR;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/aR;-><init>(Lcom/google/googlenav/ui/wizard/aQ;)V

    invoke-virtual {v0, v1, v3}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 145
    :goto_24
    return-void

    .line 100
    :cond_25
    new-instance v0, Lcom/google/googlenav/friend/aK;

    invoke-direct {v0, p1}, Lcom/google/googlenav/friend/aK;-><init>(Lcom/google/googlenav/F;)V

    invoke-virtual {v0}, Lcom/google/googlenav/friend/aK;->d()Lcom/google/googlenav/friend/aI;

    move-result-object v8

    .line 101
    invoke-virtual {v8}, Lcom/google/googlenav/friend/aI;->B()[Lcom/google/googlenav/a;

    move-result-object v0

    .line 102
    array-length v1, v0

    const/4 v2, 0x1

    if-ge v1, v2, :cond_3e

    .line 103
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/aQ;->c:Lcom/google/googlenav/ui/wizard/aP;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/aQ;->b:Lcom/google/googlenav/ui/wizard/aW;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/aP;->a(Lcom/google/googlenav/ui/wizard/aP;Lcom/google/googlenav/ui/wizard/aW;)V

    goto :goto_24

    .line 106
    :cond_3e
    aget-object v9, v0, v3

    .line 107
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v5

    .line 108
    new-instance v0, Lcom/google/googlenav/cq;

    invoke-virtual {v9}, Lcom/google/googlenav/a;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v9}, Lcom/google/googlenav/a;->b()J

    move-result-wide v3

    new-instance v7, Lcom/google/googlenav/ui/wizard/aS;

    invoke-direct {v7, p0, v8, p1, v9}, Lcom/google/googlenav/ui/wizard/aS;-><init>(Lcom/google/googlenav/ui/wizard/aQ;Lcom/google/googlenav/friend/aI;Lcom/google/googlenav/F;Lcom/google/googlenav/a;)V

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/cq;-><init>(Ljava/lang/String;IJJLcom/google/googlenav/cr;)V

    .line 144
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_24
.end method
