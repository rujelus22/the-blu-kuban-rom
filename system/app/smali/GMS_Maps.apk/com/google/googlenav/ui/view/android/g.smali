.class public Lcom/google/googlenav/ui/view/android/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field final a:Lbf/bk;

.field final b:I

.field c:Ljava/lang/String;

.field d:Lcom/google/googlenav/ui/view/android/h;

.field e:Lcom/google/googlenav/ui/view/android/c;

.field private final f:Ljava/lang/CharSequence;

.field private final g:Ljava/lang/CharSequence;

.field private final h:Ljava/lang/CharSequence;

.field private final i:Ljava/lang/CharSequence;

.field private final j:Ljava/lang/CharSequence;

.field private final k:Ljava/lang/CharSequence;

.field private final l:Z

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private final p:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbf/bk;IZ)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput v3, p0, Lcom/google/googlenav/ui/view/android/g;->m:I

    .line 77
    if-nez p1, :cond_2c

    move-object v0, v1

    :goto_a
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->f:Ljava/lang/CharSequence;

    .line 79
    if-nez p2, :cond_33

    move-object v0, v1

    :goto_f
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    .line 81
    if-nez p3, :cond_3a

    move-object v0, v1

    :goto_14
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    .line 84
    if-nez p4, :cond_45

    move-object v0, v1

    :goto_19
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    .line 86
    if-nez p5, :cond_4c

    :goto_1d
    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    .line 88
    iput-object p6, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    .line 89
    iput p7, p0, Lcom/google/googlenav/ui/view/android/g;->p:I

    .line 90
    iput-object p8, p0, Lcom/google/googlenav/ui/view/android/g;->a:Lbf/bk;

    .line 91
    iput p9, p0, Lcom/google/googlenav/ui/view/android/g;->b:I

    .line 92
    iput-boolean p10, p0, Lcom/google/googlenav/ui/view/android/g;->l:Z

    .line 93
    iput v3, p0, Lcom/google/googlenav/ui/view/android/g;->m:I

    .line 94
    return-void

    .line 77
    :cond_2c
    sget-object v0, Lcom/google/googlenav/ui/aV;->ac:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_a

    .line 79
    :cond_33
    sget-object v0, Lcom/google/googlenav/ui/aV;->z:Lcom/google/googlenav/ui/aV;

    invoke-static {p2, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_f

    .line 81
    :cond_3a
    invoke-static {p3}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->bP:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_14

    .line 84
    :cond_45
    sget-object v0, Lcom/google/googlenav/ui/aV;->A:Lcom/google/googlenav/ui/aV;

    invoke-static {p4, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_19

    .line 86
    :cond_4c
    sget-object v0, Lcom/google/googlenav/ui/aV;->A:Lcom/google/googlenav/ui/aV;

    invoke-static {p5, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    goto :goto_1d
.end method

.method public static a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-nez v0, :cond_8

    .line 336
    const/4 v0, 0x0

    .line 377
    :cond_7
    :goto_7
    return-object v0

    .line 341
    :cond_8
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_a8

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a8

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v2

    .line 344
    :goto_18
    const/4 v3, 0x0

    .line 345
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aK()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 346
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aL()Lai/d;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Lai/d;->d()Ljava/lang/String;

    move-result-object v1

    .line 348
    invoke-virtual {v0}, Lai/d;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v0

    .line 351
    const/16 v3, 0x3ee

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 355
    :cond_42
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v4

    .line 356
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v5

    .line 357
    const/4 v6, 0x0

    .line 358
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_59

    .line 359
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v6

    .line 362
    :cond_59
    new-instance v0, Lcom/google/googlenav/ui/view/android/g;

    move-object v1, p1

    move v7, p2

    move-object v8, p3

    move v9, p4

    move/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/googlenav/ui/view/android/g;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILbf/bk;IZ)V

    .line 366
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/ui/view/android/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/android/g;->a(Ljava/lang/String;)V

    .line 370
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/g;->e:Lcom/google/googlenav/ui/view/android/c;

    .line 371
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->d()Z

    move-result v1

    if-nez v1, :cond_8f

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->e()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 373
    :cond_8f
    new-instance v1, Lcom/google/googlenav/ui/view/android/c;

    iget v3, v0, Lcom/google/googlenav/ui/view/android/g;->p:I

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->d()Z

    move-result v5

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/g;->e()Z

    move-result v6

    move v2, v3

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/c;-><init>(ILbf/bk;Ljava/lang/String;ZZ)V

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/g;->e:Lcom/google/googlenav/ui/view/android/c;

    goto/16 :goto_7

    .line 341
    :cond_a8
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_18
.end method

.method private a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/g;->c:Ljava/lang/String;

    .line 128
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->n:Ljava/lang/String;

    .line 109
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 110
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/g;->n:Ljava/lang/String;

    .line 112
    :cond_d
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 113
    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/g;->o:Ljava/lang/String;

    .line 115
    :cond_15
    return-void
.end method

.method private d()Z
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->n:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->o:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->c:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 202
    iget v0, p0, Lcom/google/googlenav/ui/view/android/g;->b:I

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .registers 8
    .parameter

    .prologue
    const v5, 0x7f100413

    const v4, 0x7f100022

    .line 207
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/g;->l:Z

    if-eqz v0, :cond_23

    .line 208
    invoke-static {p1}, Lbf/aS;->a(Landroid/view/View;)V

    .line 209
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget v3, p0, Lcom/google/googlenav/ui/view/android/g;->m:I

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 212
    :cond_23
    new-instance v0, Lcom/google/googlenav/ui/view/android/h;

    invoke-direct {v0}, Lcom/google/googlenav/ui/view/android/h;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    .line 213
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v1, 0x7f1003e2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->a:Landroid/view/View;

    .line 214
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->b:Landroid/widget/TextView;

    .line 215
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f10001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->c:Landroid/widget/TextView;

    .line 216
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->e:Landroid/widget/TextView;

    .line 217
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->f:Landroid/widget/TextView;

    .line 218
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f1003e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->g:Landroid/widget/TextView;

    .line 219
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_e6

    .line 220
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 221
    const v0, 0x7f090095

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 223
    :cond_8c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v1, 0x7f1001a3

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->j:Landroid/view/View;

    .line 224
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->j:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->l:Landroid/widget/TextView;

    .line 226
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->n:Landroid/widget/ImageView;

    .line 228
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v1, 0x7f1001a5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->i:Landroid/view/View;

    .line 229
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->i:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->k:Landroid/widget/TextView;

    .line 230
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/android/h;->i:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->m:Landroid/widget/ImageView;

    .line 234
    :goto_da
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    new-instance v1, Lcom/google/googlenav/ui/view/android/i;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/i;-><init>(Lcom/google/googlenav/ui/view/android/g;)V

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    .line 236
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    return-object v0

    .line 232
    :cond_e6
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->d:Lcom/google/googlenav/ui/view/android/h;

    const v0, 0x7f10030d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/googlenav/ui/view/android/h;->d:Landroid/widget/TextView;

    goto :goto_da
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 268
    check-cast p2, Lcom/google/googlenav/ui/view/android/h;

    .line 270
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->f:Ljava/lang/CharSequence;

    invoke-static {v1}, Lau/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    invoke-static {v1}, Lau/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 275
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    invoke-static {v1}, Lau/b;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 276
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 278
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-nez v0, :cond_46

    .line 280
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lbj/G;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 283
    :cond_46
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    iget v1, p0, Lcom/google/googlenav/ui/view/android/g;->p:I

    iput v1, v0, Lcom/google/googlenav/ui/view/android/i;->a:I

    .line 284
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->e:Lcom/google/googlenav/ui/view/android/c;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/i;->b:Lcom/google/googlenav/ui/view/android/c;

    .line 285
    iget-object v0, p2, Lcom/google/googlenav/ui/view/android/h;->a:Landroid/view/View;

    iget-object v1, p2, Lcom/google/googlenav/ui/view/android/h;->h:Lcom/google/googlenav/ui/view/android/i;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 286
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 182
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 183
    const v0, 0x7f04016c

    .line 185
    :goto_d
    return v0

    :cond_e
    const v0, 0x7f04016d

    goto :goto_d
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    const/16 v2, 0xa

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    if-eqz v1, :cond_10

    .line 299
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 302
    :cond_10
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1c

    .line 303
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 304
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->h:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 307
    :cond_1c
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    if-eqz v1, :cond_28

    .line 308
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 309
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->i:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 312
    :cond_28
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    if-eqz v1, :cond_34

    .line 313
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 314
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 317
    :cond_34
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    if-eqz v1, :cond_40

    .line 318
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 319
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/g;->k:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 322
    :cond_40
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
