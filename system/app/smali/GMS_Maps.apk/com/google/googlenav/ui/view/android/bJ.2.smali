.class Lcom/google/googlenav/ui/view/android/bJ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaR/bE;


# instance fields
.field A:Landroid/widget/ImageView;

.field B:Lcom/google/googlenav/ui/bL;

.field C:Lcom/google/googlenav/ui/view/android/bK;

.field D:Lcom/google/googlenav/ui/view/android/bK;

.field E:Lcom/google/googlenav/ui/view/android/bK;

.field F:Lcom/google/googlenav/ui/view/android/bI;

.field G:Lcom/google/googlenav/ui/view/android/bL;

.field H:Lcom/google/googlenav/ui/view/android/bM;

.field I:Lcom/google/googlenav/ui/view/android/bN;

.field J:Lcom/google/googlenav/ui/view/android/bN;

.field a:Landroid/view/View;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/CheckBox;

.field d:[Landroid/widget/TextView;

.field e:[Landroid/widget/TextView;

.field f:[Landroid/widget/TextView;

.field g:Landroid/widget/TextView;

.field h:Lcom/google/googlenav/ui/view/android/DistanceView;

.field i:Lcom/google/googlenav/ui/view/android/HeadingView;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field n:Landroid/widget/TextView;

.field o:Landroid/widget/TextView;

.field p:Landroid/view/View;

.field q:Landroid/widget/TextView;

.field r:Landroid/widget/TextView;

.field s:Landroid/widget/TextView;

.field t:Landroid/widget/TextView;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/widget/ImageView;

.field x:Landroid/widget/ImageView;

.field y:Landroid/widget/LinearLayout;

.field z:Landroid/widget/ImageView;


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x2

    .line 453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->d:[Landroid/widget/TextView;

    .line 460
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->e:[Landroid/widget/TextView;

    .line 461
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->f:[Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 511
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->C:Lcom/google/googlenav/ui/view/android/bK;

    if-eqz v0, :cond_a

    .line 512
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->C:Lcom/google/googlenav/ui/view/android/bK;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bK;->b:I

    .line 514
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->D:Lcom/google/googlenav/ui/view/android/bK;

    if-eqz v0, :cond_16

    .line 515
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->D:Lcom/google/googlenav/ui/view/android/bK;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bK;->b:I

    .line 516
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->D:Lcom/google/googlenav/ui/view/android/bK;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bK;->d:Lcom/google/googlenav/ui/view/android/bG;

    .line 518
    :cond_16
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->E:Lcom/google/googlenav/ui/view/android/bK;

    if-eqz v0, :cond_1e

    .line 519
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->E:Lcom/google/googlenav/ui/view/android/bK;

    iput v2, v0, Lcom/google/googlenav/ui/view/android/bK;->b:I

    .line 521
    :cond_1e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->G:Lcom/google/googlenav/ui/view/android/bL;

    if-eqz v0, :cond_26

    .line 522
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->G:Lcom/google/googlenav/ui/view/android/bL;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bL;->a:Ljava/lang/String;

    .line 524
    :cond_26
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->H:Lcom/google/googlenav/ui/view/android/bM;

    if-eqz v0, :cond_2e

    .line 525
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->H:Lcom/google/googlenav/ui/view/android/bM;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bM;->a:Lcom/google/googlenav/ui/view/a;

    .line 527
    :cond_2e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->I:Lcom/google/googlenav/ui/view/android/bN;

    if-eqz v0, :cond_36

    .line 528
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->I:Lcom/google/googlenav/ui/view/android/bN;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bN;->a:Ljava/lang/String;

    .line 530
    :cond_36
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->J:Lcom/google/googlenav/ui/view/android/bN;

    if-eqz v0, :cond_3e

    .line 531
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->J:Lcom/google/googlenav/ui/view/android/bN;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bN;->a:Ljava/lang/String;

    .line 533
    :cond_3e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->F:Lcom/google/googlenav/ui/view/android/bI;

    if-eqz v0, :cond_46

    .line 534
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bJ;->F:Lcom/google/googlenav/ui/view/android/bI;

    iput-object v1, v0, Lcom/google/googlenav/ui/view/android/bI;->a:Ljava/lang/String;

    .line 536
    :cond_46
    return-void
.end method
