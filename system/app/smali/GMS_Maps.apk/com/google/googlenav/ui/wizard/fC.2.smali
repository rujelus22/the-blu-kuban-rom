.class public Lcom/google/googlenav/ui/wizard/fC;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/fq;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/fq;)V
    .registers 3
    .parameter

    .prologue
    .line 475
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    .line 476
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_13

    const v0, 0x7f0f001b

    :goto_f
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 479
    return-void

    .line 476
    :cond_13
    const v0, 0x7f0f0018

    goto :goto_f
.end method

.method private a(Landroid/view/View;)V
    .registers 9
    .parameter

    .prologue
    const v6, 0x7f10003e

    const v5, 0x7f10002e

    const/4 v2, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 510
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ar()Lcom/google/googlenav/ax;

    move-result-object v0

    .line 511
    invoke-virtual {v0}, Lcom/google/googlenav/ax;->b()Z

    move-result v1

    if-eqz v1, :cond_110

    .line 512
    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ax;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ay;

    .line 513
    const v1, 0x7f100210

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 514
    invoke-virtual {v0}, Lcom/google/googlenav/ay;->a()Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 515
    const v0, 0x7f100211

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 516
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->c(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    const v0, 0x7f100212

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 519
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-eqz v1, :cond_111

    .line 520
    const/16 v1, 0x370

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    :goto_74
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/fC;->b(Landroid/view/View;)V

    .line 528
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_122

    .line 529
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 530
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->y()Z

    move-result v5

    .line 535
    const v0, 0x7f10003f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 536
    const/16 v1, 0x385

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 537
    new-instance v1, Lcom/google/googlenav/ui/wizard/fD;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fD;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 543
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 544
    if-eqz v5, :cond_11c

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->k(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-nez v1, :cond_11c

    move v1, v2

    :goto_bd
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 546
    const v0, 0x7f100031

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 547
    const/16 v1, 0x69

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 548
    new-instance v1, Lcom/google/googlenav/ui/wizard/fE;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fE;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->k(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-nez v1, :cond_11e

    :goto_e2
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 558
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 560
    const v0, 0x7f100030

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 561
    const/16 v1, 0x42d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 562
    new-instance v1, Lcom/google/googlenav/ui/wizard/fF;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fF;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 568
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 569
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v1

    if-eqz v1, :cond_120

    :goto_10d
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 577
    :cond_110
    :goto_110
    return-void

    .line 522
    :cond_111
    const/16 v1, 0x380

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_74

    :cond_11c
    move v1, v3

    .line 544
    goto :goto_bd

    :cond_11e
    move v2, v3

    .line 557
    goto :goto_e2

    :cond_120
    move v3, v4

    .line 569
    goto :goto_10d

    .line 573
    :cond_122
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 574
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_110
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/fC;)V
    .registers 1
    .parameter

    .prologue
    .line 473
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fC;->m()V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 637
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    if-eqz v0, :cond_6e

    const/16 v0, 0x40a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/fq;->a:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 641
    :goto_1b
    const v0, 0x7f1003aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 642
    const v1, 0x7f100489

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 644
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 645
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 646
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 647
    const/16 v0, 0x36b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 648
    invoke-static {}, Lcom/google/googlenav/K;->S()Z

    move-result v0

    if-nez v0, :cond_50

    invoke-static {}, Lcom/google/googlenav/K;->T()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 649
    :cond_50
    const v0, 0x7f10048a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 650
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 651
    const/16 v1, 0x373

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 652
    new-instance v1, Lcom/google/googlenav/ui/wizard/fG;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/fG;-><init>(Lcom/google/googlenav/ui/wizard/fC;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 663
    :cond_6d
    :goto_6d
    return-void

    .line 637
    :cond_6e
    const/16 v0, 0x40b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1b

    .line 661
    :cond_76
    const/16 v0, 0x384

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6d
.end method

.method static synthetic l()Lcom/google/googlenav/android/BaseMapsActivity;
    .registers 1

    .prologue
    .line 473
    sget-object v0, Lcom/google/googlenav/ui/wizard/fC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    return-object v0
.end method

.method private m()V
    .registers 3

    .prologue
    .line 674
    sget-object v0, Lcom/google/googlenav/ui/wizard/fC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getState()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->e()Lcom/google/googlenav/android/d;

    move-result-object v0

    const-string v1, "http://maps.google.com/help/maps/streetview/contribute/policies.html"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/d;->a(Ljava/lang/String;)V

    .line 676
    return-void
.end method


# virtual methods
.method public I_()V
    .registers 3

    .prologue
    .line 667
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 668
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fC;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x385

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    .line 670
    :cond_17
    return-void
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 581
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 582
    sparse-switch v1, :sswitch_data_34

    .line 602
    const/4 v0, 0x0

    :cond_9
    :goto_9
    return v0

    .line 585
    :sswitch_a
    const/16 v1, 0x65

    const-string v2, "w"

    const-string v3, "cp"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 589
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    .line 591
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->h(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 592
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->h(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/ui/wizard/fA;->b()V

    goto :goto_9

    .line 596
    :sswitch_2a
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/fq;->j(Lcom/google/googlenav/ui/wizard/fq;)V

    goto :goto_9

    .line 599
    :sswitch_30
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/fC;->m()V

    goto :goto_9

    .line 582
    :sswitch_data_34
    .sparse-switch
        0x7f100171 -> :sswitch_a
        0x7f1004cc -> :sswitch_30
        0x7f1004cd -> :sswitch_2a
    .end sparse-switch
.end method

.method protected c()Landroid/view/View;
    .registers 4

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fC;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/wizard/fq;->a(Lcom/google/googlenav/ui/wizard/fq;Landroid/view/LayoutInflater;)Landroid/view/LayoutInflater;

    .line 484
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->g(Lcom/google/googlenav/ui/wizard/fq;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040092

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 486
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fC;->a(Landroid/view/View;)V

    .line 488
    return-object v0
.end method

.method public h()V
    .registers 2

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fC;->q()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/fC;->a(Landroid/view/View;)V

    .line 507
    return-void
.end method

.method public onBackPressed()V
    .registers 4

    .prologue
    .line 494
    const/16 v0, 0x65

    const-string v1, "w"

    const-string v2, "cd"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->a()V

    .line 500
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->h(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 501
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->h(Lcom/google/googlenav/ui/wizard/fq;)Lcom/google/googlenav/ui/wizard/fA;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/fA;->b()V

    .line 503
    :cond_1f
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 607
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_d

    .line 632
    :goto_c
    return v2

    .line 612
    :cond_d
    sget-object v0, Lcom/google/googlenav/ui/wizard/fC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v3, 0x7f11001c

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 613
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->y()Z

    move-result v3

    .line 615
    const v0, 0x7f1004cc

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 616
    const v4, 0x7f100171

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 617
    const v5, 0x7f1004cd

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 619
    const/16 v6, 0x42d

    invoke-static {v6}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 620
    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v6}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v6

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 621
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 623
    const/16 v0, 0x69

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 624
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->k(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-nez v0, :cond_8f

    move v0, v1

    :goto_5b
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 626
    const/16 v0, 0x386

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 627
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->k(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-nez v0, :cond_72

    if-eqz v3, :cond_72

    move v2, v1

    :cond_72
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 629
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/fq;->i(Lcom/google/googlenav/ui/wizard/fq;)Z

    move-result v0

    if-eqz v0, :cond_8c

    invoke-interface {v5}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 630
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fC;->a:Lcom/google/googlenav/ui/wizard/fq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/fq;->f()Z

    move-result v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_8c
    move v2, v1

    .line 632
    goto/16 :goto_c

    :cond_8f
    move v0, v2

    .line 624
    goto :goto_5b
.end method
