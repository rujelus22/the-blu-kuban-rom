.class public Lcom/google/googlenav/ui/android/FloorPickerView;
.super Landroid/widget/ListView;
.source "SourceFile"

# interfaces
.implements LaH/A;
.implements Ln/s;


# instance fields
.field private a:I

.field private b:Lo/y;

.field private c:Ln/q;

.field private d:I

.field private volatile e:Lo/D;

.field private f:Lcom/google/googlenav/ui/android/V;

.field private final g:Ljava/util/Set;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 285
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    .line 286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/googlenav/ui/android/FloorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V

    .line 301
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 292
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V

    .line 293
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/content/res/Resources;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 304
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    iput v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    .line 85
    iput v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    .line 99
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    .line 305
    iput-object p3, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->h:Landroid/content/res/Resources;

    .line 306
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/FloorPickerView;)Landroid/content/res/Resources;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->h:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/android/FloorPickerView;Lo/y;Lo/D;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;Lo/D;)V

    return-void
.end method

.method private a(Lo/y;Lo/D;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 504
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;)V

    .line 507
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    if-nez v0, :cond_8

    .line 513
    :goto_7
    return-void

    .line 510
    :cond_8
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-static {v0, p2}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/D;)I

    move-result v0

    .line 511
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setSelectedPosition(I)V

    .line 512
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->b()V

    goto :goto_7
.end method

.method private static a(Lo/z;Lo/y;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 673
    if-nez p0, :cond_63

    const-string v0, "none"

    move-object v1, v0

    .line 674
    :goto_5
    if-nez p1, :cond_6d

    const-string v0, "none"

    .line 676
    :goto_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?sa=T&oi=m_map:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lbm/l;->c:Lbm/l;

    invoke-virtual {v3}, Lbm/l;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 678
    const/16 v3, 0x68

    const-string v4, "s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "l="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "b="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v1, 0x2

    if-nez v2, :cond_76

    const/4 v0, 0x0

    :goto_59
    aput-object v0, v5, v1

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 686
    return-void

    .line 673
    :cond_63
    invoke-virtual {p0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_5

    .line 674
    :cond_6d
    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v0}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 678
    :cond_76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_59
.end method

.method static synthetic a(Lo/y;Lo/y;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-static {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/y;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/android/FloorPickerView;)I
    .registers 2
    .parameter

    .prologue
    .line 55
    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    return v0
.end method

.method private static b(Lo/y;Lo/D;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 613
    if-nez p0, :cond_4

    .line 628
    :cond_3
    :goto_3
    return v0

    .line 617
    :cond_4
    if-nez p1, :cond_11

    .line 618
    invoke-virtual {p0}, Lo/y;->d()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 619
    const/4 v1, 0x0

    .line 628
    :cond_d
    :goto_d
    if-ltz v1, :cond_3

    move v0, v1

    goto :goto_3

    .line 622
    :cond_11
    invoke-virtual {p0, p1}, Lo/y;->b(Lo/D;)I

    move-result v1

    .line 623
    if-ltz v1, :cond_d

    invoke-virtual {p0}, Lo/y;->d()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 625
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_20
    move v1, v0

    goto :goto_d
.end method

.method private b()V
    .registers 3

    .prologue
    .line 519
    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_a

    .line 520
    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->smoothScrollToPosition(I)V

    .line 522
    :cond_a
    return-void
.end method

.method private static b(Lo/y;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 693
    if-nez p0, :cond_5

    .line 697
    :goto_4
    return v2

    .line 696
    :cond_5
    invoke-virtual {p0}, Lo/y;->d()Z

    move-result v0

    if-eqz v0, :cond_18

    move v0, v1

    .line 697
    :goto_c
    invoke-virtual {p0}, Lo/y;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v3, v0, :cond_1a

    :goto_16
    move v2, v1

    goto :goto_4

    .line 696
    :cond_18
    const/4 v0, 0x2

    goto :goto_c

    :cond_1a
    move v1, v2

    .line 697
    goto :goto_16
.end method

.method private static b(Lo/y;Lo/y;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 408
    if-ne p0, p1, :cond_4

    .line 409
    const/4 v0, 0x1

    .line 414
    :goto_3
    return v0

    .line 411
    :cond_4
    if-eqz p0, :cond_8

    if-nez p1, :cond_a

    .line 412
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 414
    :cond_a
    invoke-virtual {p0}, Lo/y;->a()Lo/r;

    move-result-object v0

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo/r;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method static synthetic c(Lcom/google/googlenav/ui/android/FloorPickerView;)I
    .registers 2
    .parameter

    .prologue
    .line 55
    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    return v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    if-nez v0, :cond_5

    .line 589
    :cond_4
    :goto_4
    return-void

    .line 577
    :cond_5
    const/4 v0, -0x1

    .line 580
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    if-eqz v1, :cond_12

    .line 581
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/D;)I

    move-result v0

    .line 584
    :cond_12
    iget v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    if-eq v0, v1, :cond_4

    .line 587
    iput v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    .line 588
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/V;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/V;->notifyDataSetChanged()V

    goto :goto_4
.end method

.method private c(Lo/y;Lo/D;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 653
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?sa=T&oi=m_map:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbm/l;->c:Lbm/l;

    invoke-virtual {v1}, Lbm/l;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 655
    if-nez p2, :cond_84

    const-string v0, "0"

    .line 656
    :goto_1d
    invoke-static {p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;)Z

    move-result v1

    if-eqz v1, :cond_87

    const-string v1, "1"

    .line 657
    :goto_25
    const/16 v3, 0x68

    const-string v4, "f"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "b="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v8

    invoke-virtual {v8}, Lo/r;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "p="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "v="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v1, 0x3

    if-nez v2, :cond_8a

    const/4 v0, 0x0

    :goto_7a
    aput-object v0, v5, v1

    invoke-static {v5}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 666
    return-void

    .line 655
    :cond_84
    const-string v0, "1"

    goto :goto_1d

    .line 656
    :cond_87
    const-string v1, "0"

    goto :goto_25

    .line 657
    :cond_8a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "u="

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7a
.end method

.method static synthetic d(Lcom/google/googlenav/ui/android/FloorPickerView;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/android/FloorPickerView;)V
    .registers 1
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->b()V

    return-void
.end method

.method static synthetic f(Lcom/google/googlenav/ui/android/FloorPickerView;)Lo/y;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 525
    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    return v0
.end method

.method public a(I)Lo/z;
    .registers 3
    .parameter

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/W;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/W;->a()Lo/z;

    move-result-object v0

    return-object v0
.end method

.method public a(ILaH/m;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 467
    return-void
.end method

.method public a(LaN/B;LaH/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 452
    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_1f

    invoke-interface {p2}, LaH/m;->s()LaH/h;

    move-result-object v0

    invoke-virtual {v0}, LaH/h;->b()Lo/D;

    move-result-object v0

    .line 455
    :goto_e
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    .line 456
    invoke-static {v1, v0}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 457
    new-instance v1, Lcom/google/googlenav/ui/android/T;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/android/T;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Lo/D;)V

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    .line 464
    :cond_1e
    return-void

    .line 453
    :cond_1f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method a(Ljava/util/Collection;)V
    .registers 3
    .parameter

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 602
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 603
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/V;

    if-eqz v0, :cond_13

    .line 604
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/V;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/V;->notifyDataSetChanged()V

    .line 606
    :cond_13
    return-void
.end method

.method public a(Ln/q;)V
    .registers 3
    .parameter

    .prologue
    .line 420
    new-instance v0, Lcom/google/googlenav/ui/android/R;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/android/R;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Ln/q;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    .line 426
    return-void
.end method

.method public a(Ln/q;Lo/y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 431
    new-instance v0, Lcom/google/googlenav/ui/android/S;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/googlenav/ui/android/S;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Ln/q;Lo/y;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    .line 443
    return-void
.end method

.method a(Lo/D;)V
    .registers 2
    .parameter

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->e:Lo/D;

    .line 564
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->c()V

    .line 565
    return-void
.end method

.method a(Lo/y;)V
    .registers 9
    .parameter

    .prologue
    const-wide/16 v5, 0x1f4

    const/4 v1, -0x1

    const/high16 v4, 0x3f80

    const/4 v3, 0x0

    .line 345
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;Lo/y;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 403
    :cond_e
    :goto_e
    return-void

    .line 351
    :cond_f
    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->clearAnimation()V

    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    .line 353
    iput v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->d:I

    .line 354
    iput v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    .line 356
    if-eqz p1, :cond_53

    .line 357
    invoke-static {p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->b(Lo/y;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 358
    iput-object p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    .line 359
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setVisibility(I)V

    .line 360
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 361
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 362
    invoke-virtual {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 363
    new-instance v1, Lcom/google/googlenav/ui/android/P;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/P;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 373
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 374
    new-instance v0, Lcom/google/googlenav/ui/android/V;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/android/V;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;Landroid/content/Context;Lo/y;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/V;

    .line 375
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/V;

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 376
    invoke-direct {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->c()V

    .line 380
    :cond_53
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/google/googlenav/ui/android/FloorPickerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    .line 381
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->setVisibility(I)V

    .line 382
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 383
    invoke-virtual {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 389
    new-instance v1, Lcom/google/googlenav/ui/android/Q;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/android/Q;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 401
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_e
.end method

.method public b(Ln/q;)V
    .registers 2
    .parameter

    .prologue
    .line 446
    return-void
.end method

.method c(Ln/q;)V
    .registers 5
    .parameter

    .prologue
    .line 487
    invoke-virtual {p1}, Ln/q;->c()Lo/y;

    move-result-object v1

    .line 488
    const/4 v0, 0x0

    .line 489
    if-eqz v1, :cond_18

    .line 490
    invoke-virtual {v1}, Lo/y;->a()Lo/r;

    move-result-object v2

    invoke-virtual {p1, v2}, Ln/q;->b(Lo/r;)Lo/E;

    move-result-object v2

    .line 491
    if-eqz v2, :cond_15

    .line 492
    invoke-virtual {v2}, Lo/E;->c()Lo/D;

    move-result-object v0

    .line 494
    :cond_15
    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->c(Lo/y;Lo/D;)V

    .line 497
    :cond_18
    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;Lo/D;)V

    .line 498
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 633
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onSizeChanged(IIII)V

    .line 637
    new-instance v0, Lcom/google/googlenav/ui/android/U;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/U;-><init>(Lcom/google/googlenav/ui/android/FloorPickerView;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->post(Ljava/lang/Runnable;)Z

    .line 643
    return-void
.end method

.method public setIndoorState(Ln/q;)V
    .registers 3
    .parameter

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/q;

    if-eqz v0, :cond_9

    .line 327
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/q;

    invoke-virtual {v0, p0}, Ln/q;->b(Ln/s;)V

    .line 330
    :cond_9
    if-eqz p1, :cond_11

    .line 333
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Ln/q;)V

    .line 334
    invoke-virtual {p1, p0}, Ln/q;->a(Ln/s;)V

    .line 336
    :cond_11
    iput-object p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/q;

    .line 337
    return-void
.end method

.method public setSelectedLevel(Lo/y;Lo/z;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 478
    if-eqz p2, :cond_a

    invoke-virtual {p2}, Lo/z;->a()Lo/D;

    move-result-object v0

    .line 479
    :goto_6
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/y;Lo/D;)V

    .line 480
    return-void

    .line 478
    :cond_a
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public setSelectedPosition(I)V
    .registers 4
    .parameter

    .prologue
    .line 529
    iget v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    if-ne p1, v0, :cond_5

    .line 553
    :cond_4
    :goto_4
    return-void

    .line 532
    :cond_5
    iput p1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->a:I

    .line 533
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->f:Lcom/google/googlenav/ui/android/V;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/V;->notifyDataSetChanged()V

    .line 535
    const/4 v0, -0x1

    if-eq p1, v0, :cond_4

    .line 539
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/android/FloorPickerView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/W;

    .line 540
    if-eqz v0, :cond_4

    .line 544
    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/W;->a()Lo/z;

    move-result-object v0

    .line 545
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/android/FloorPickerView;->a(Lo/z;Lo/y;)V

    .line 546
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/q;

    if-eqz v1, :cond_4

    .line 547
    if-nez v0, :cond_2e

    .line 548
    iget-object v0, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/q;

    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->b:Lo/y;

    invoke-virtual {v0, v1}, Ln/q;->a(Lo/y;)V

    goto :goto_4

    .line 550
    :cond_2e
    iget-object v1, p0, Lcom/google/googlenav/ui/android/FloorPickerView;->c:Ln/q;

    invoke-virtual {v0}, Lo/z;->a()Lo/D;

    move-result-object v0

    invoke-virtual {v1, v0}, Ln/q;->a(Lo/D;)V

    goto :goto_4
.end method
