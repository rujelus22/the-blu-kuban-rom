.class public Lcom/google/googlenav/ui/wizard/fk;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private a:Lbf/m;

.field private b:Lcom/google/googlenav/ui/view/dialog/bl;

.field private c:Lcom/google/googlenav/ai;

.field private i:I


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    .line 35
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/fk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ai;ILbf/m;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aZ()I

    move-result v0

    if-lt p2, v0, :cond_7

    .line 49
    :goto_6
    return-void

    .line 45
    :cond_7
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    .line 46
    iput p2, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    .line 47
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/fk;->a:Lbf/m;

    .line 48
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->j()V

    goto :goto_6
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .registers 2
    .parameter

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->a()V

    .line 86
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    packed-switch p1, :pswitch_data_a

    .line 72
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 69
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->h()V

    .line 70
    const/4 v0, 0x1

    goto :goto_4

    .line 67
    :pswitch_data_a
    .packed-switch 0x3f9
        :pswitch_5
    .end packed-switch
.end method

.method protected b()V
    .registers 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->b:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    .line 54
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 55
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    iget v2, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/dialog/bk;->a(Lcom/google/googlenav/ai;I)V

    .line 56
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/fk;->a()V

    .line 91
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->a:Lbf/m;

    if-eqz v0, :cond_c

    .line 92
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->a:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->h()V

    .line 94
    :cond_c
    const/4 v0, 0x1

    return v0
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 60
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/fk;->i:I

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fk;->c:Lcom/google/googlenav/ai;

    .line 62
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 63
    return-void
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method
