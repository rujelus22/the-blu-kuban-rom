.class public Lcom/google/googlenav/ui/view/android/bt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ai;

.field private B:Z

.field private C:Z

.field private D:I

.field private E:Lcom/google/googlenav/ui/e;

.field private F:Lcom/google/googlenav/ui/br;

.field private G:I

.field private H:Z

.field private I:Z

.field private J:Ljava/lang/String;

.field private K:I

.field private L:Z

.field private M:Lcom/google/googlenav/ar;

.field private N:I

.field private O:Z

.field private P:I

.field private a:Ljava/lang/CharSequence;

.field private b:[Ljava/lang/CharSequence;

.field private c:[Ljava/lang/CharSequence;

.field private d:[Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Z

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Ljava/lang/CharSequence;

.field private o:Ljava/lang/CharSequence;

.field private p:Lcom/google/googlenav/ac;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/CharSequence;

.field private s:Lcom/google/googlenav/aq;

.field private t:Ljava/util/Vector;

.field private u:Lcom/google/googlenav/ap;

.field private v:Lam/f;

.field private w:Lcom/google/googlenav/ui/view/a;

.field private x:Lan/f;

.field private y:LaN/B;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 863
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bt;->G:I

    .line 869
    iput-boolean v1, p0, Lcom/google/googlenav/ui/view/android/bt;->L:Z

    .line 875
    const v0, 0x7f0400c4

    iput v0, p0, Lcom/google/googlenav/ui/view/android/bt;->N:I

    .line 878
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->O:Z

    .line 887
    iput v1, p0, Lcom/google/googlenav/ui/view/android/bt;->P:I

    return-void
.end method

.method static synthetic A(Lcom/google/googlenav/ui/view/android/bt;)I
    .registers 2
    .parameter

    .prologue
    .line 817
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->D:I

    return v0
.end method

.method static synthetic B(Lcom/google/googlenav/ui/view/android/bt;)I
    .registers 2
    .parameter

    .prologue
    .line 817
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->G:I

    return v0
.end method

.method static synthetic C(Lcom/google/googlenav/ui/view/android/bt;)I
    .registers 2
    .parameter

    .prologue
    .line 817
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->N:I

    return v0
.end method

.method static synthetic D(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->O:Z

    return v0
.end method

.method static synthetic E(Lcom/google/googlenav/ui/view/android/bt;)I
    .registers 2
    .parameter

    .prologue
    .line 817
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->P:I

    return v0
.end method

.method static synthetic F(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->I:Z

    return v0
.end method

.method static synthetic G(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->H:Z

    return v0
.end method

.method static synthetic H(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->J:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic I(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->L:Z

    return v0
.end method

.method static synthetic J(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ar;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->M:Lcom/google/googlenav/ar;

    return-object v0
.end method

.method static synthetic K(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->r:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic L(Lcom/google/googlenav/ui/view/android/bt;)I
    .registers 2
    .parameter

    .prologue
    .line 817
    iget v0, p0, Lcom/google/googlenav/ui/view/android/bt;->K:I

    return v0
.end method

.method static synthetic M(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->m:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic N(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->l:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic O(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/aq;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->s:Lcom/google/googlenav/aq;

    return-object v0
.end method

.method static synthetic P(Lcom/google/googlenav/ui/view/android/bt;)Ljava/util/Vector;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->t:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->E:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/br;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->F:Lcom/google/googlenav/ui/br;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic d(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->b:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->c:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/view/android/bt;)[Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->d:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic g(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic h(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->f:Z

    return v0
.end method

.method static synthetic i(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic j(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic l(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic m(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic n(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic o(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/CharSequence;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->o:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic p(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ac;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->p:Lcom/google/googlenav/ac;

    return-object v0
.end method

.method static synthetic q(Lcom/google/googlenav/ui/view/android/bt;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ap;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->u:Lcom/google/googlenav/ap;

    return-object v0
.end method

.method static synthetic s(Lcom/google/googlenav/ui/view/android/bt;)Lam/f;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->v:Lam/f;

    return-object v0
.end method

.method static synthetic t(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ui/view/a;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->w:Lcom/google/googlenav/ui/view/a;

    return-object v0
.end method

.method static synthetic u(Lcom/google/googlenav/ui/view/android/bt;)Lan/f;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->x:Lan/f;

    return-object v0
.end method

.method static synthetic v(Lcom/google/googlenav/ui/view/android/bt;)LaN/B;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->y:LaN/B;

    return-object v0
.end method

.method static synthetic w(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->z:Z

    return v0
.end method

.method static synthetic x(Lcom/google/googlenav/ui/view/android/bt;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->A:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic y(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->B:Z

    return v0
.end method

.method static synthetic z(Lcom/google/googlenav/ui/view/android/bt;)Z
    .registers 2
    .parameter

    .prologue
    .line 817
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->C:Z

    return v0
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/view/android/bs;
    .registers 2

    .prologue
    .line 1159
    new-instance v0, Lcom/google/googlenav/ui/view/android/bs;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/android/bs;-><init>(Lcom/google/googlenav/ui/view/android/bt;)V

    return-object v0
.end method

.method public a(I)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1096
    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->D:I

    .line 1097
    return-object p0
.end method

.method public a(LaN/B;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1070
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->y:LaN/B;

    .line 1071
    return-object p0
.end method

.method public a(Lai/d;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 4
    .parameter

    .prologue
    .line 945
    if-eqz p1, :cond_14

    .line 946
    invoke-static {p1}, Lbf/aS;->a(Lai/d;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->g:Ljava/lang/CharSequence;

    .line 947
    invoke-virtual {p1}, Lai/d;->c()Ljava/lang/String;

    move-result-object v0

    .line 948
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 949
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->h:Ljava/lang/String;

    .line 952
    :cond_14
    return-object p0
.end method

.method public a(Lam/f;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->v:Lam/f;

    .line 1056
    return-object p0
.end method

.method public a(Lan/f;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1065
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->x:Lan/f;

    .line 1066
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ac;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1017
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->p:Lcom/google/googlenav/ac;

    .line 1018
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ai;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1081
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->A:Lcom/google/googlenav/ai;

    .line 1082
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ap;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->u:Lcom/google/googlenav/ap;

    .line 1048
    return-object p0
.end method

.method public a(Lcom/google/googlenav/aq;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1037
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->s:Lcom/google/googlenav/aq;

    .line 1038
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ar;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1149
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->M:Lcom/google/googlenav/ar;

    .line 1150
    return-object p0
.end method

.method public a(Lcom/google/googlenav/cx;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 1136
    if-eqz p1, :cond_10

    .line 1137
    invoke-virtual {p1}, Lcom/google/googlenav/cx;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/bt;->I:Z

    .line 1138
    invoke-virtual {p1}, Lcom/google/googlenav/cx;->a()Lcom/google/googlenav/cy;

    move-result-object v0

    iget-object v0, v0, Lcom/google/googlenav/cy;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->J:Ljava/lang/String;

    .line 1140
    :cond_10
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/br;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1106
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->F:Lcom/google/googlenav/ui/br;

    .line 1107
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/e;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1101
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->E:Lcom/google/googlenav/ui/e;

    .line 1102
    return-object p0
.end method

.method public a(Lcom/google/googlenav/ui/view/a;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1060
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->w:Lcom/google/googlenav/ui/view/a;

    .line 1061
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 895
    if-eqz p1, :cond_8

    .line 896
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->a:Ljava/lang/CharSequence;

    .line 898
    :cond_8
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 890
    sget-object v0, Lcom/google/googlenav/ui/aV;->bs:Lcom/google/googlenav/ui/aV;

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    .line 891
    return-object p0
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 982
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 983
    invoke-static {p1}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->l:Ljava/lang/CharSequence;

    .line 986
    :cond_10
    return-object p0
.end method

.method public a(Ljava/lang/String;Z)Lcom/google/googlenav/ui/view/android/bt;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 933
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 934
    invoke-static {p1}, Lbf/aS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 935
    if-eqz p2, :cond_1f

    sget-object v0, Lcom/google/googlenav/ui/aV;->aV:Lcom/google/googlenav/ui/aV;

    .line 936
    :goto_e
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 937
    invoke-static {v1, v0}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    .line 938
    iput-object v2, p0, Lcom/google/googlenav/ui/view/android/bt;->e:Ljava/lang/CharSequence;

    .line 939
    iput-boolean p2, p0, Lcom/google/googlenav/ui/view/android/bt;->f:Z

    .line 941
    :cond_1e
    return-object p0

    .line 935
    :cond_1f
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    goto :goto_e
.end method

.method public a(Ljava/util/Vector;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1042
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->t:Ljava/util/Vector;

    .line 1043
    return-object p0
.end method

.method public a(Z)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1075
    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->z:Z

    .line 1076
    return-object p0
.end method

.method public a([Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 902
    if-eqz p1, :cond_30

    array-length v1, p1

    if-lez v1, :cond_30

    .line 903
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/googlenav/ui/view/android/bt;->b:[Ljava/lang/CharSequence;

    .line 905
    array-length v3, p1

    move v1, v0

    move v2, v0

    :goto_e
    if-ge v1, v3, :cond_30

    aget-object v4, p1, v1

    .line 906
    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_26

    .line 907
    const/4 v0, 0x2

    if-ne v2, v0, :cond_2c

    const v0, 0x7f0f00a5

    .line 908
    :goto_1e
    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/bt;->b:[Ljava/lang/CharSequence;

    invoke-static {v4, v0}, Lcom/google/googlenav/ui/bi;->a(Ljava/lang/CharSequence;I)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    aput-object v0, v5, v2

    .line 910
    :cond_26
    add-int/lit8 v2, v2, 0x1

    .line 905
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    .line 907
    :cond_2c
    const v0, 0x7f0f0106

    goto :goto_1e

    .line 913
    :cond_30
    return-object p0
.end method

.method public b(I)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1111
    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->G:I

    .line 1112
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 956
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 957
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->i:Ljava/lang/CharSequence;

    .line 959
    :cond_e
    return-object p0
.end method

.method public b(Z)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1086
    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->B:Z

    .line 1087
    return-object p0
.end method

.method public c(I)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1116
    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->N:I

    .line 1117
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 963
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 964
    sget-object v0, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->j:Ljava/lang/CharSequence;

    .line 966
    :cond_e
    return-object p0
.end method

.method public c(Z)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1091
    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->C:Z

    .line 1092
    return-object p0
.end method

.method public d(I)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1131
    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->P:I

    .line 1132
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 970
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 971
    sget-object v0, Lcom/google/googlenav/ui/aV;->ba:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->k:Ljava/lang/CharSequence;

    .line 974
    :cond_e
    return-object p0
.end method

.method public d(Z)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1121
    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->O:Z

    .line 1122
    return-object p0
.end method

.method public e(I)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1144
    iput p1, p0, Lcom/google/googlenav/ui/view/android/bt;->K:I

    .line 1145
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 978
    sget-object v0, Lcom/google/googlenav/ui/aV;->bv:Lcom/google/googlenav/ui/aV;

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/bt;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/view/android/bt;

    move-result-object v0

    return-object v0
.end method

.method public e(Z)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1154
    iput-boolean p1, p0, Lcom/google/googlenav/ui/view/android/bt;->H:Z

    .line 1155
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 4
    .parameter

    .prologue
    .line 990
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 991
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 992
    sget-object v1, Lcom/google/googlenav/ui/aV;->bl:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/googlenav/ui/bi;->a(Lcom/google/googlenav/ui/aW;Landroid/text/SpannableStringBuilder;)V

    .line 993
    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->m:Ljava/lang/CharSequence;

    .line 995
    :cond_16
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 1005
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1006
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->n:Ljava/lang/CharSequence;

    .line 1008
    :cond_8
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 2
    .parameter

    .prologue
    .line 1012
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/bt;->o:Ljava/lang/CharSequence;

    .line 1013
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/bt;
    .registers 3
    .parameter

    .prologue
    .line 1029
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1030
    sget-object v0, Lcom/google/googlenav/ui/aV;->bv:Lcom/google/googlenav/ui/aV;

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/bt;->r:Ljava/lang/CharSequence;

    .line 1033
    :cond_e
    return-object p0
.end method
