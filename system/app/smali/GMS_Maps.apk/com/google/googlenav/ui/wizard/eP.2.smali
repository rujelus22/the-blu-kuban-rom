.class public Lcom/google/googlenav/ui/wizard/eP;
.super Lcom/google/googlenav/ui/wizard/B;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/googlenav/ui/wizard/eJ;


# instance fields
.field private j:Lcom/google/googlenav/ui/wizard/eX;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 68
    const/4 v0, 0x0

    sput-object v0, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 2
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/B;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 80
    return-void
.end method

.method private a(LaR/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 489
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbO/G;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 491
    if-nez p1, :cond_b

    .line 504
    :goto_a
    return-object v0

    .line 497
    :cond_b
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, LbO/G;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 498
    invoke-virtual {p1}, LaR/a;->f()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 499
    const/4 v2, 0x2

    invoke-virtual {p1}, LaR/a;->g()I

    move-result v3

    mul-int/lit8 v3, v3, 0xa

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 500
    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 502
    const/4 v1, 0x4

    invoke-virtual {p1}, LaR/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addString(ILjava/lang/String;)V

    goto :goto_a
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;)Lcom/google/googlenav/ui/wizard/eX;
    .registers 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;Lcom/google/googlenav/ui/wizard/eX;)Lcom/google/googlenav/ui/wizard/eX;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    return-object p1
.end method

.method private a(I)V
    .registers 10
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 335
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    .line 336
    if-nez v0, :cond_8

    .line 360
    :goto_7
    return-void

    .line 339
    :cond_8
    const/16 v1, 0x2dc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, LaR/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 341
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x2dd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    const/16 v3, 0x35b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/ui/wizard/eU;

    invoke-direct {v7, p0, p1}, Lcom/google/googlenav/ui/wizard/eU;-><init>(Lcom/google/googlenav/ui/wizard/eP;I)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    goto :goto_7
.end method

.method private a(LaR/D;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x1c

    .line 110
    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v0

    .line 111
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->e(Ljava/lang/String;)Lcom/google/googlenav/ai;

    move-result-object v1

    .line 112
    if-eqz v1, :cond_17

    .line 113
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-interface {v0, v1, v3}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/ai;I)V

    .line 119
    :goto_13
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    .line 120
    return-void

    .line 116
    :cond_17
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/google/googlenav/J;->a(Ljava/lang/String;ZI)V

    goto :goto_13
.end method

.method private a(LaR/D;Ljava/util/List;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 256
    invoke-virtual {p1}, LaR/D;->n()Z

    move-result v0

    if-eqz v0, :cond_22

    const/16 v0, 0x2df

    .line 258
    :goto_8
    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(Ljava/lang/String;LaR/t;Ljava/util/List;)V

    .line 261
    return-void

    .line 256
    :cond_22
    const/16 v0, 0x2dc

    goto :goto_8
.end method

.method private a(LaR/L;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x1c

    .line 131
    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {p1}, LaR/L;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "30"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    .line 140
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 141
    return-void
.end method

.method private a(LaR/h;)V
    .registers 5
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    .line 127
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, LaR/h;->e()Lax/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lax/b;)V

    .line 128
    return-void
.end method

.method private a(LaR/t;)V
    .registers 5
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 105
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, LaR/t;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->d(Ljava/lang/String;)Lcom/google/googlenav/ag;

    .line 106
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    .line 107
    return-void
.end method

.method private a(LaR/t;Ljava/util/List;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 300
    new-instance v0, LaX/a;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eT;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/eT;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V

    invoke-direct {v0, p2, v1}, LaX/a;-><init>(Ljava/util/List;LaX/b;)V

    .line 313
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 314
    return-void
.end method

.method private a(LaR/t;Ljava/util/List;Ljava/util/List;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 229
    invoke-virtual {p1}, LaR/t;->b()Ljava/lang/String;

    move-result-object v0

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 231
    const/16 v2, 0x2e0

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    const-string v0, "<br/>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_29
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    const-string v3, "<br/>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_29

    .line 237
    :cond_43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(Ljava/lang/String;LaR/t;Ljava/util/List;)V

    .line 238
    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 508
    if-nez p2, :cond_10

    sget-object v0, Lcom/google/googlenav/ui/wizard/iT;->b:Lcom/google/googlenav/ui/wizard/iT;

    .line 510
    :goto_4
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v2, 0x0

    new-instance v3, Lcom/google/googlenav/ui/wizard/eW;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/eW;-><init>(Lcom/google/googlenav/ui/wizard/eP;)V

    invoke-virtual {v1, p1, v0, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/ui/wizard/iT;ZLcom/google/googlenav/ui/wizard/iS;)V

    .line 527
    return-void

    .line 508
    :cond_10
    sget-object v0, Lcom/google/googlenav/ui/wizard/iT;->a:Lcom/google/googlenav/ui/wizard/iT;

    goto :goto_4
.end method

.method private a(Lcom/google/googlenav/ui/view/a;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x2

    .line 405
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->m()Lcom/google/googlenav/ui/wizard/eJ;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    .line 407
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v1

    .line 408
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v2

    .line 409
    if-ne v1, v7, :cond_17

    if-eqz v2, :cond_6e

    .line 412
    :cond_17
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->l()I

    move-result v0

    .line 413
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "at="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ai="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ti="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 418
    const/16 v2, 0x7b

    const-string v3, "a"

    invoke-static {v2, v3, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_6e
    packed-switch v1, :pswitch_data_86

    .line 439
    :goto_71
    return-void

    .line 424
    :pswitch_72
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->f(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_71

    .line 427
    :pswitch_76
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->b(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_71

    .line 430
    :pswitch_7a
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->c(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_71

    .line 433
    :pswitch_7e
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_71

    .line 436
    :pswitch_82
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->e(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_71

    .line 422
    :pswitch_data_86
    .packed-switch 0x0
        :pswitch_76
        :pswitch_7a
        :pswitch_72
        :pswitch_7e
        :pswitch_82
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/D;Ljava/util/List;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/D;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->c(LaR/t;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/t;Ljava/util/List;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;LaR/t;Ljava/util/List;Ljava/util/List;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eP;Lcom/google/googlenav/ui/view/a;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/ui/view/a;)V

    return-void
.end method

.method private a(Ljava/lang/String;LaR/t;Ljava/util/List;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x2dd

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    const/16 v3, 0x35b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x69

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v7, Lcom/google/googlenav/ui/wizard/eS;

    invoke-direct {v7, p0, p3, p2}, Lcom/google/googlenav/ui/wizard/eS;-><init>(Lcom/google/googlenav/ui/wizard/eP;Ljava/util/List;LaR/t;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    .line 290
    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eP;I)LaR/a;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .registers 10
    .parameter

    .prologue
    .line 363
    invoke-static {p1}, LaR/a;->a(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    .line 365
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 367
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    .line 369
    new-instance v1, Lcom/google/googlenav/ui/wizard/eV;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/eV;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/n;)V

    sget-object v2, LaR/O;->d:LaR/O;

    invoke-interface {v0, v7, v1, v2}, LaR/n;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LaR/p;LaR/O;)V

    .line 388
    return-void
.end method

.method private b(LaR/D;)V
    .registers 9
    .parameter

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 199
    new-instance v0, Lcom/google/googlenav/ui/wizard/eR;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/eR;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/D;)V

    invoke-static {p1, v0}, LaX/c;->a(LaR/t;LaX/d;)LaX/c;

    move-result-object v0

    .line 220
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    .line 221
    return-void
.end method

.method private b(LaR/t;)V
    .registers 9
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    .line 151
    invoke-virtual {p1}, LaR/t;->j()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_e

    .line 153
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->c(LaR/t;)V

    .line 187
    :goto_d
    return-void

    .line 156
    :cond_e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1b4

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 160
    new-instance v0, Lcom/google/googlenav/ui/wizard/eQ;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/eQ;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V

    invoke-static {p1, v0}, LaX/c;->a(LaR/t;LaX/d;)LaX/c;

    move-result-object v0

    .line 186
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_d
.end method

.method private b(Lcom/google/googlenav/ui/view/a;)V
    .registers 3
    .parameter

    .prologue
    .line 442
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_26

    .line 453
    :goto_7
    return-void

    .line 444
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;)V

    goto :goto_7

    .line 447
    :pswitch_12
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/D;)V

    goto :goto_7

    .line 450
    :pswitch_1c
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/t;)V

    goto :goto_7

    .line 442
    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_8
        :pswitch_12
        :pswitch_1c
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eP;LaR/t;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(LaR/t;)V

    return-void
.end method

.method private c(I)V
    .registers 3
    .parameter

    .prologue
    .line 530
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->k()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 531
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/a;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 532
    invoke-direct {p0, v0, p1}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V

    .line 536
    :goto_15
    return-void

    .line 534
    :cond_16
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->B_()V

    goto :goto_15
.end method

.method private c(LaR/D;)V
    .registers 5
    .parameter

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    .line 399
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    .line 400
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->a:Lcom/google/googlenav/J;

    invoke-virtual {p1}, LaR/D;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LaR/D;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    return-void
.end method

.method private c(LaR/t;)V
    .registers 6
    .parameter

    .prologue
    .line 246
    invoke-virtual {p1}, LaR/t;->b()Ljava/lang/String;

    move-result-object v0

    .line 247
    const/16 v1, 0x2dc

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0}, Lau/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/googlenav/ui/wizard/eP;->a(Ljava/lang/String;LaR/t;Ljava/util/List;)V

    .line 250
    return-void
.end method

.method private c(Lcom/google/googlenav/ui/view/a;)V
    .registers 3
    .parameter

    .prologue
    .line 456
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_1c

    .line 464
    :goto_7
    return-void

    .line 458
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->c(LaR/D;)V

    goto :goto_7

    .line 461
    :pswitch_12
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/D;)V

    goto :goto_7

    .line 456
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_12
    .end packed-switch
.end method

.method private d(I)LaR/a;
    .registers 3
    .parameter

    .prologue
    .line 539
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->e()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    check-cast v0, LaR/E;

    .line 542
    invoke-virtual {v0, p1}, LaR/E;->a(I)LaR/a;

    move-result-object v0

    return-object v0
.end method

.method private d(LaR/t;)V
    .registers 7
    .parameter

    .prologue
    .line 320
    invoke-virtual {p1}, LaR/t;->h()Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    .line 323
    invoke-virtual {v0}, LaR/l;->d()[LaR/n;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_e
    if-ge v0, v3, :cond_1e

    aget-object v4, v2, v0

    .line 324
    if-eqz v4, :cond_1b

    .line 325
    invoke-interface {v4}, LaR/n;->e()LaR/u;

    move-result-object v4

    invoke-interface {v4, v1}, LaR/u;->e(Ljava/lang/String;)V

    .line 323
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 329
    :cond_1e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_29

    .line 330
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eF;->h()V

    .line 332
    :cond_29
    return-void
.end method

.method private d(Lcom/google/googlenav/ui/view/a;)V
    .registers 3
    .parameter

    .prologue
    .line 467
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_1c

    .line 475
    :goto_7
    return-void

    .line 469
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/h;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/h;)V

    goto :goto_7

    .line 472
    :pswitch_12
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/h;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/t;)V

    goto :goto_7

    .line 467
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_12
    .end packed-switch
.end method

.method private e(Lcom/google/googlenav/ui/view/a;)V
    .registers 3
    .parameter

    .prologue
    .line 478
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_1c

    .line 486
    :goto_7
    return-void

    .line 480
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/L;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/L;)V

    goto :goto_7

    .line 483
    :pswitch_12
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/L;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->b(LaR/t;)V

    goto :goto_7

    .line 478
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_8
        :pswitch_12
    .end packed-switch
.end method

.method private f(Lcom/google/googlenav/ui/view/a;)V
    .registers 3
    .parameter

    .prologue
    .line 546
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_36

    .line 557
    :goto_7
    return-void

    .line 548
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->d(I)LaR/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(LaR/t;)V

    goto :goto_7

    .line 551
    :pswitch_1a
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->c(I)V

    goto :goto_7

    .line 554
    :pswitch_28
    invoke-virtual {p1}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(I)V

    goto :goto_7

    .line 546
    :pswitch_data_36
    .packed-switch 0x0
        :pswitch_8
        :pswitch_1a
        :pswitch_28
    .end packed-switch
.end method


# virtual methods
.method public a(LaN/B;)I
    .registers 3
    .parameter

    .prologue
    .line 563
    const/4 v0, 0x4

    return v0
.end method

.method protected a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 581
    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    .line 583
    if-nez v0, :cond_f

    .line 589
    :goto_e
    return-void

    .line 587
    :cond_f
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/U;->f()Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    .line 588
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eP;->a(Lcom/google/googlenav/ui/view/a;)V

    goto :goto_e
.end method

.method public a(Lcom/google/googlenav/J;)V
    .registers 4
    .parameter

    .prologue
    .line 90
    const-string v0, "stars"

    const/4 v1, 0x0

    invoke-super {p0, p1, v0, v1}, Lcom/google/googlenav/ui/wizard/B;->a(Lcom/google/googlenav/J;Ljava/lang/String;Z)V

    .line 91
    sget-object v0, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    if-eqz v0, :cond_13

    .line 92
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/eF;

    sget-object v1, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/eF;->a(Lcom/google/googlenav/ui/wizard/eJ;)V

    .line 94
    :cond_13
    return-void
.end method

.method protected b(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 593
    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/U;

    .line 595
    if-nez v0, :cond_11

    move v0, v1

    .line 621
    :goto_10
    return v0

    .line 600
    :cond_11
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/U;->f()Lcom/google/googlenav/ui/view/a;

    move-result-object v0

    .line 601
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_6e

    move v0, v1

    .line 618
    goto :goto_10

    .line 603
    :pswitch_1e
    new-instance v1, Lcom/google/googlenav/ui/wizard/fc;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fc;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/D;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    .line 620
    :goto_2b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eX;->show()V

    .line 621
    const/4 v0, 0x1

    goto :goto_10

    .line 606
    :pswitch_32
    new-instance v1, Lcom/google/googlenav/ui/wizard/fa;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/D;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fa;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/D;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_2b

    .line 609
    :pswitch_40
    new-instance v1, Lcom/google/googlenav/ui/wizard/fb;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fb;-><init>(Lcom/google/googlenav/ui/wizard/eP;I)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_2b

    .line 612
    :pswitch_52
    new-instance v1, Lcom/google/googlenav/ui/wizard/eZ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/h;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/eZ;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/h;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_2b

    .line 615
    :pswitch_60
    new-instance v1, Lcom/google/googlenav/ui/wizard/fd;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaR/L;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/fd;-><init>(Lcom/google/googlenav/ui/wizard/eP;LaR/L;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    goto :goto_2b

    .line 601
    :pswitch_data_6e
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_32
        :pswitch_40
        :pswitch_52
        :pswitch_60
    .end packed-switch
.end method

.method protected c()V
    .registers 1

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/B;->c()V

    .line 99
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 568
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eX;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 571
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eX;->dismiss()V

    .line 572
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eP;->j:Lcom/google/googlenav/ui/wizard/eX;

    .line 577
    :goto_14
    return-void

    .line 574
    :cond_15
    sput-object v1, Lcom/google/googlenav/ui/wizard/eP;->i:Lcom/google/googlenav/ui/wizard/eJ;

    .line 575
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eP;->a()V

    goto :goto_14
.end method
