.class public Lcom/google/googlenav/ui/wizard/eF;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"

# interfaces
.implements LaR/ab;
.implements Landroid/support/v4/view/ai;


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/B;

.field private b:LaR/R;

.field private c:Lcom/google/googlenav/ui/wizard/jv;

.field private d:Landroid/support/v4/view/ViewPager;

.field private l:Lcom/google/googlenav/ui/wizard/eL;

.field private m:I

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Ljava/util/List;

.field private q:Ljava/util/Map;

.field private r:Ljava/lang/String;

.field private s:Lcom/google/googlenav/ui/view/dialog/bD;

.field private t:Ljava/lang/String;

.field private u:I


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/B;Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 191
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_37

    const v0, 0x7f0f001b

    :goto_e
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    .line 170
    iput v1, p0, Lcom/google/googlenav/ui/wizard/eF;->m:I

    .line 177
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    .line 178
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->q:Ljava/util/Map;

    .line 188
    iput v1, p0, Lcom/google/googlenav/ui/wizard/eF;->u:I

    .line 193
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    .line 194
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    .line 196
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, LaR/l;->c()LaR/R;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->b:LaR/R;

    .line 198
    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->a(LaR/ab;)V

    .line 199
    return-void

    .line 191
    :cond_37
    const v0, 0x7f0f0018

    goto :goto_e
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/eF;)Landroid/support/v4/view/ViewPager;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(Landroid/support/v4/view/PagerTitleStrip;)V
    .registers 6
    .parameter

    .prologue
    .line 209
    invoke-virtual {p1}, Landroid/support/v4/view/PagerTitleStrip;->getChildCount()I

    move-result v2

    .line 212
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-ge v1, v2, :cond_2c

    .line 213
    invoke-virtual {p1, v1}, Landroid/support/v4/view/PagerTitleStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 214
    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 215
    const/4 v3, 0x1

    if-ne v1, v3, :cond_25

    .line 217
    const v3, 0x7f0202ce

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 218
    const/16 v3, 0xc8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMinimumWidth(I)V

    .line 212
    :goto_21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 220
    :cond_25
    const v3, 0x7f0202cd

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_21

    .line 223
    :cond_2c
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 226
    const v0, 0x7f100086

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->n:Landroid/view/View;

    .line 227
    const v0, 0x7f100087

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->o:Landroid/view/View;

    .line 228
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->n:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eN;

    const/4 v2, -0x1

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/eN;-><init>(Lcom/google/googlenav/ui/wizard/eF;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->o:Landroid/view/View;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eN;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/eN;-><init>(Lcom/google/googlenav/ui/wizard/eF;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 230
    return-void
.end method

.method private b(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 471
    if-nez p1, :cond_5

    move v0, v1

    .line 478
    :goto_4
    return v0

    .line 474
    :cond_5
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->q:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 475
    if-nez v0, :cond_11

    move v0, v1

    .line 476
    goto :goto_4

    .line 478
    :cond_11
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_4
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/eF;)Lcom/google/googlenav/ui/wizard/eL;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    const v5, 0x7f02021c

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 292
    const/16 v0, 0x2db

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f10001e

    invoke-virtual {p0, v0, v1, v5}, Lcom/google/googlenav/ui/wizard/eF;->a(Ljava/lang/CharSequence;II)V

    .line 295
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 297
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 300
    :cond_1e
    new-instance v0, Lcom/google/googlenav/ui/wizard/eG;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/eG;-><init>(Lcom/google/googlenav/ui/wizard/eF;)V

    new-array v1, v4, [I

    const/16 v2, 0xbc0

    aput v2, v1, v3

    invoke-virtual {p0, v4, v5, v0, v1}, Lcom/google/googlenav/ui/wizard/eF;->a(ZILaA/f;[I)V

    .line 308
    return-void
.end method

.method private c(I)Landroid/widget/ListView;
    .registers 4
    .parameter

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()Landroid/support/v4/view/x;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eL;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/eL;->c(I)Landroid/view/View;

    move-result-object v0

    .line 502
    const v1, 0x7f100026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/eF;)Lcom/google/googlenav/ui/wizard/B;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eF;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 483
    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/eF;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/eF;)I
    .registers 2
    .parameter

    .prologue
    .line 57
    iget v0, p0, Lcom/google/googlenav/ui/wizard/eF;->m:I

    return v0
.end method

.method private n()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 391
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->b:LaR/R;

    sget-object v1, LaR/O;->c:LaR/O;

    invoke-interface {v0, v2, v1}, LaR/R;->a(ZLaR/O;)V

    .line 396
    const/16 v0, 0x2f2

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 397
    return-void
.end method


# virtual methods
.method public F_()V
    .registers 1

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eF;->h()V

    .line 427
    return-void
.end method

.method protected K_()Z
    .registers 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/B;->h()V

    .line 387
    const/4 v0, 0x1

    return v0
.end method

.method protected O_()V
    .registers 2

    .prologue
    .line 315
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-nez v0, :cond_e

    .line 316
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->requestWindowFeature(I)Z

    .line 318
    :cond_e
    return-void
.end method

.method public a(IFI)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 439
    return-void
.end method

.method public a(LaR/P;)V
    .registers 2
    .parameter

    .prologue
    .line 422
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 431
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/wizard/eJ;)V
    .registers 4
    .parameter

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    iget v1, p1, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 514
    iget v0, p1, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->c(I)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p1, Lcom/google/googlenav/ui/wizard/eJ;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 515
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 497
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/eF;->r:Ljava/lang/String;

    .line 498
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/bD;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/ui/wizard/eF;->u:I

    .line 280
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eM;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eM;-><init>(Lcom/google/googlenav/ui/wizard/eF;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/bD;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/eF;->s:Lcom/google/googlenav/ui/view/dialog/bD;

    .line 282
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/eF;->t:Ljava/lang/String;

    .line 283
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-virtual {v0, p3}, LaT/a;->a(LaT/m;)V

    .line 284
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    new-instance v1, Lcom/google/googlenav/ui/wizard/eK;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/eK;-><init>(Lcom/google/googlenav/ui/wizard/eF;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/ui/wizard/eI;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    return-void
.end method

.method public a(ILandroid/view/MenuItem;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 358
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 359
    sparse-switch v2, :sswitch_data_4e

    move v0, v1

    .line 381
    :goto_a
    return v0

    .line 361
    :sswitch_b
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->as()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 362
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/B;->h()V

    goto :goto_a

    :cond_1b
    move v0, v1

    .line 365
    goto :goto_a

    .line 367
    :sswitch_1d
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/eF;->n()V

    goto :goto_a

    .line 370
    :sswitch_21
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/aA;->h()V

    .line 373
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/B;->a()V

    .line 374
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->y()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/A;

    const/16 v3, 0x1c

    invoke-direct {v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_a

    .line 378
    :sswitch_40
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->E()Lcom/google/googlenav/aA;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/aA;->a(Ljava/lang/String;)V

    goto :goto_a

    .line 359
    :sswitch_data_4e
    .sparse-switch
        0x102002c -> :sswitch_b
        0x7f1001e4 -> :sswitch_1d
        0x7f1004b6 -> :sswitch_40
        0x7f1004c0 -> :sswitch_21
    .end sparse-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 7
    .parameter

    .prologue
    const v4, 0x7f1001e4

    const/4 v3, 0x1

    .line 344
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()Landroid/support/v4/view/x;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eL;

    .line 347
    const/16 v1, 0x35a

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 348
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/eL;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 349
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 353
    :goto_2e
    return v3

    .line 351
    :cond_2f
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2e
.end method

.method public b_(I)V
    .registers 6
    .parameter

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eF;->invalidateOptionsMenu()V

    .line 445
    iput p1, p0, Lcom/google/googlenav/ui/wizard/eF;->m:I

    .line 446
    iget v0, p0, Lcom/google/googlenav/ui/wizard/eF;->u:I

    if-ne p1, v0, :cond_2b

    .line 447
    const-string v0, "v"

    const-string v1, ""

    invoke-static {v0, v1}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :goto_10
    const/16 v0, 0x7b

    const-string v1, "t"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ti="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 457
    return-void

    .line 450
    :cond_2b
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eK;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eK;->b()V

    goto :goto_10
.end method

.method protected c()Landroid/view/View;
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/eF;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0400f6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 243
    const v0, 0x7f100200

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 244
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->b(Landroid/view/View;)V

    .line 246
    const v0, 0x7f10029c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    .line 247
    new-instance v0, Lcom/google/googlenav/ui/wizard/eL;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/eL;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    .line 249
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    const v3, 0x7f1000df

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTitleStrip;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->a(Landroid/support/v4/view/PagerTitleStrip;)V

    .line 250
    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/wizard/eF;->a(Landroid/view/View;)V

    .line 252
    :goto_3a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6a

    .line 253
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/wizard/eK;

    .line 254
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/eF;->q:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/googlenav/ui/wizard/eK;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/wizard/eL;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    iget-object v4, v0, Lcom/google/googlenav/ui/wizard/eK;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/eK;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/googlenav/ui/wizard/eL;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    .line 252
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3a

    .line 258
    :cond_6a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->l:Lcom/google/googlenav/ui/wizard/eL;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/x;)V

    .line 259
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ai;)V

    .line 261
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->r:Ljava/lang/String;

    if-eqz v0, :cond_7f

    .line 262
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->r:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/wizard/eF;->c(Ljava/lang/String;)V

    .line 265
    :cond_7f
    return-object v2
.end method

.method public c_(I)V
    .registers 2
    .parameter

    .prologue
    .line 435
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 461
    const/4 v0, 0x1

    return v0
.end method

.method protected f()V
    .registers 3

    .prologue
    .line 234
    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v0

    invoke-virtual {v0}, LaR/l;->b()LaR/aa;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/aa;->b(LaR/ab;)V

    .line 235
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->s:Lcom/google/googlenav/ui/view/dialog/bD;

    if-eqz v0, :cond_18

    .line 236
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->s:Lcom/google/googlenav/ui/view/dialog/bD;

    invoke-virtual {v0, v1}, LaT/a;->b(LaT/m;)V

    .line 238
    :cond_18
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    .line 406
    const/4 v0, 0x0

    .line 407
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->c:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/jv;->z()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/eH;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/eH;-><init>(Lcom/google/googlenav/ui/wizard/eF;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    .line 418
    return-void
.end method

.method public l()I
    .registers 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v0

    return v0
.end method

.method public m()Lcom/google/googlenav/ui/wizard/eJ;
    .registers 3

    .prologue
    .line 506
    new-instance v0, Lcom/google/googlenav/ui/wizard/eJ;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/eJ;-><init>()V

    .line 507
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/eF;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->c()I

    move-result v1

    iput v1, v0, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    .line 508
    iget v1, v0, Lcom/google/googlenav/ui/wizard/eJ;->a:I

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/wizard/eF;->c(I)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, v0, Lcom/google/googlenav/ui/wizard/eJ;->b:I

    .line 509
    return-object v0
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/eF;->a:Lcom/google/googlenav/ui/wizard/B;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/B;->h()V

    .line 289
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 322
    sget-object v0, Lcom/google/googlenav/ui/wizard/eF;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110016

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 324
    const v0, 0x7f1001e4

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 325
    const/16 v1, 0x57d

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 327
    const v0, 0x7f1004c0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 328
    if-eqz v0, :cond_2e

    .line 330
    const/16 v1, 0x524

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 333
    :cond_2e
    const v0, 0x7f1004b6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_40

    .line 336
    const/16 v1, 0x1d8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 339
    :cond_40
    const/4 v0, 0x1

    return v0
.end method
