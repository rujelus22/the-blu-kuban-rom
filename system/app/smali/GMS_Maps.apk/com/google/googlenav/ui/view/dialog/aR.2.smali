.class public Lcom/google/googlenav/ui/view/dialog/aR;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/view/dialog/aW;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/googlenav/ui/view/dialog/aV;

.field private final l:Lcom/google/googlenav/friend/af;

.field private final m:LaB/s;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/view/dialog/aW;ILjava/lang/String;Lcom/google/googlenav/ui/view/dialog/aV;Lcom/google/googlenav/friend/af;LaB/s;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 86
    const v0, 0x7f0f0018

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(I)V

    .line 87
    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    .line 88
    iput p2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->b:I

    .line 89
    iput-object p3, p0, Lcom/google/googlenav/ui/view/dialog/aR;->c:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lcom/google/googlenav/ui/view/dialog/aR;->d:Lcom/google/googlenav/ui/view/dialog/aV;

    .line 91
    iput-object p5, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    .line 92
    iput-object p6, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/dialog/aR;)LaB/s;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/dialog/aR;)Lcom/google/googlenav/ui/view/dialog/aV;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->d:Lcom/google/googlenav/ui/view/dialog/aV;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/app/ActionBar;)V
    .registers 4
    .parameter

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->b:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setIcon(I)V

    .line 181
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 182
    return-void
.end method

.method protected c()Landroid/view/View;
    .registers 8

    .prologue
    const/4 v5, 0x0

    .line 102
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e9

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 104
    const v0, 0x7f100039

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 105
    if-eqz v0, :cond_f7

    .line 106
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->w_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :goto_1e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->a()Z

    move-result v0

    if-eqz v0, :cond_107

    .line 112
    const v0, 0x7f100092

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    invoke-virtual {v1}, Lcom/google/googlenav/friend/af;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->l:Lcom/google/googlenav/friend/af;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/af;->f()Ljava/lang/String;

    move-result-object v1

    .line 116
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    if-eqz v0, :cond_6b

    if-eqz v1, :cond_6b

    .line 117
    const v0, 0x7f1001bc

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 118
    new-instance v2, Lcom/google/googlenav/ui/bs;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/ui/bi;->e(I)I

    move-result v3

    invoke-direct {v2, v1, v3}, Lcom/google/googlenav/ui/bs;-><init>(Ljava/lang/String;I)V

    .line 120
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aS;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/googlenav/ui/view/dialog/aS;-><init>(Lcom/google/googlenav/ui/view/dialog/aR;Landroid/widget/ImageView;Lcom/google/googlenav/ui/bs;)V

    .line 128
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    invoke-virtual {v0, v2}, LaB/s;->c(Lcom/google/googlenav/ui/bs;)Z

    move-result v0

    if-eqz v0, :cond_100

    .line 130
    invoke-interface {v1}, LaB/p;->Q_()V

    .line 140
    :cond_6b
    :goto_6b
    const v0, 0x7f1001fd

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/aW;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const v0, 0x7f100093

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 143
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/aW;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    const v0, 0x7f100230

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 147
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->c:Ljava/lang/String;

    if-eqz v1, :cond_115

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<head><meta name=\"viewport\" content=\"target-densitydpi=medium-dpi, user-scalable=no, initial-scale=1.0\" /></head><body style=\"padding: 0px; margin:0px;\"><img src=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" style=\"float:right;padding-left: 1em;padding-bottom: 1em;vertical-align:text-top\" />"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/dialog/aW;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</body>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 153
    :goto_c7
    const-string v1, "file:///android_res/drawable/"

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const v0, 0x7f1002bd

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 158
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 159
    iget-object v1, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/dialog/aW;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 160
    new-instance v1, Lcom/google/googlenav/ui/view/dialog/aT;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/dialog/aT;-><init>(Lcom/google/googlenav/ui/view/dialog/aR;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aU;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/aU;-><init>(Lcom/google/googlenav/ui/view/dialog/aR;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aR;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 175
    return-object v6

    .line 108
    :cond_f7
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aR;->w_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/dialog/aR;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1e

    .line 133
    :cond_100
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->m:LaB/s;

    invoke-virtual {v0, v2, v1}, LaB/s;->a(Lcom/google/googlenav/ui/bs;LaB/p;)V

    goto/16 :goto_6b

    .line 137
    :cond_107
    const v0, 0x7f1002be

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6b

    .line 151
    :cond_115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<head><meta name=\"viewport\" content=\"target-densitydpi=medium-dpi, user-scalable=no, initial-scale=1.0\" /></head><body style=\"padding: 0px; margin:0px;\">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/dialog/aW;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</body>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_c7
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aR;->a:Lcom/google/googlenav/ui/view/dialog/aW;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aW;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
