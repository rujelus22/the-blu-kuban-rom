.class public Lcom/google/googlenav/ui/view/android/aV;
.super Lcom/google/googlenav/ui/view/android/bn;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/googlenav/L;

.field protected final b:Lcom/google/googlenav/ui/wizard/jv;

.field private q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

.field private r:Landroid/support/v4/view/ViewPager;

.field private s:Lcom/google/googlenav/ui/ba;

.field private t:Lbk/a;

.field private u:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ai;Lbf/m;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-virtual {p2}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->l()Lcom/google/googlenav/L;

    move-result-object v0

    invoke-virtual {p2}, Lbf/m;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/googlenav/ui/view/android/aV;-><init>(Lcom/google/googlenav/ai;Lbf/m;Lcom/google/googlenav/L;Lcom/google/googlenav/ui/wizard/jv;)V

    .line 86
    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ai;Lbf/m;Lcom/google/googlenav/L;Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/bn;-><init>(Lcom/google/googlenav/ai;Lbf/m;)V

    .line 92
    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/aV;->a:Lcom/google/googlenav/L;

    .line 93
    iput-object p4, p0, Lcom/google/googlenav/ui/view/android/aV;->b:Lcom/google/googlenav/ui/wizard/jv;

    .line 94
    return-void
.end method

.method private J()Lcom/google/googlenav/ui/ba;
    .registers 8

    .prologue
    .line 211
    new-instance v0, Lcom/google/googlenav/ui/view/android/aY;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/aY;-><init>(Lcom/google/googlenav/ui/view/android/aV;Landroid/content/Context;Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;Landroid/support/v4/view/ViewPager;Landroid/view/LayoutInflater;Lcom/google/googlenav/ui/bc;)V

    return-object v0
.end method

.method private K()Landroid/view/View;
    .registers 8

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 237
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->m()Ljava/util/List;

    move-result-object v2

    .line 240
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 241
    new-instance v3, Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    const/16 v6, 0x24

    invoke-direct {v3, v4, v5, v2, v6}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/e;Ljava/util/List;I)V

    iput-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->m:Lcom/google/googlenav/ui/view/android/J;

    .line 242
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->m:Lcom/google/googlenav/ui/view/android/J;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 243
    new-instance v2, Lcom/google/googlenav/ui/view/android/br;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/br;-><init>(Lcom/google/googlenav/ui/view/android/bn;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 244
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 245
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 247
    return-object v1
.end method

.method private L()Landroid/view/View;
    .registers 7

    .prologue
    .line 251
    new-instance v5, Lbk/e;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->a:Lcom/google/googlenav/L;

    invoke-direct {v5, v0, v1, v2}, Lbk/e;-><init>(Landroid/widget/TabHost;ILcom/google/googlenav/L;)V

    .line 253
    new-instance v0, Lbk/a;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->b:Lcom/google/googlenav/ui/wizard/jv;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->l:Lcom/google/googlenav/ui/br;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lbk/a;-><init>(Landroid/view/LayoutInflater;Lcom/google/googlenav/ui/view/android/aV;Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/br;Lbk/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    .line 255
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    invoke-virtual {v0}, Lbk/a;->a()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private M()V
    .registers 5

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    const/16 v1, 0x578

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1, v2, v3}, Lbf/m;->a(IILjava/lang/Object;)Z

    .line 441
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    .line 442
    return-void

    .line 441
    :cond_12
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private N()V
    .registers 3

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lbf/m;->g(Lcom/google/googlenav/ai;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    .line 458
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_3b

    .line 459
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    if-eqz v0, :cond_3b

    .line 460
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v1, 0x7f10029a

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 461
    if-eqz v0, :cond_3b

    .line 462
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v1

    if-eqz v1, :cond_40

    .line 463
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464
    iget-boolean v1, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-eqz v1, :cond_3c

    const v1, 0x7f020269

    :goto_38
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 472
    :cond_3b
    :goto_3b
    return-void

    .line 464
    :cond_3c
    const v1, 0x7f020268

    goto :goto_38

    .line 467
    :cond_40
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3b
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/aV;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->M()V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/aV;)Z
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    return v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/aV;)Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    return-object v0
.end method


# virtual methods
.method protected K_()Z
    .registers 2

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-virtual {v0}, Lbf/m;->h()V

    .line 415
    const/4 v0, 0x1

    return v0
.end method

.method protected a(ILandroid/view/MenuItem;)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 394
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 395
    sparse-switch v0, :sswitch_data_3c

    .line 408
    invoke-super {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/bn;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    :goto_c
    return v0

    .line 397
    :sswitch_d
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->M()V

    .line 398
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-eqz v0, :cond_1c

    const v0, 0x7f020269

    :goto_17
    invoke-interface {p2, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move v0, v1

    .line 399
    goto :goto_c

    .line 398
    :cond_1c
    const v0, 0x7f020268

    goto :goto_17

    .line 401
    :sswitch_20
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->M()V

    move v0, v1

    .line 402
    goto :goto_c

    .line 404
    :sswitch_25
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-eqz v0, :cond_38

    const/16 v0, 0x843

    :goto_31
    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lbf/m;->a(IILjava/lang/Object;)Z

    move v0, v1

    .line 406
    goto :goto_c

    .line 404
    :cond_38
    const/16 v0, 0x834

    goto :goto_31

    .line 395
    nop

    :sswitch_data_3c
    .sparse-switch
        0x7f1001e2 -> :sswitch_25
        0x7f100305 -> :sswitch_d
        0x7f1004bc -> :sswitch_20
    .end sparse-switch
.end method

.method public a(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    .line 380
    const v0, 0x7f1004bc

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 382
    if-eqz v1, :cond_1c

    .line 383
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v2}, Lbf/m;->g(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_36

    const/16 v0, 0x438

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_19
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 386
    :cond_1c
    const v0, 0x7f1001e2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 387
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bU()Z

    move-result v0

    if-eqz v0, :cond_3d

    const/16 v0, 0x6e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_31
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 389
    const/4 v0, 0x1

    return v0

    .line 383
    :cond_36
    const/16 v0, 0x57e

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_19

    .line 387
    :cond_3d
    const/16 v0, 0x99

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_31
.end method

.method public c()Landroid/view/View;
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/16 v3, 0x8

    .line 98
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    if-nez v1, :cond_8

    .line 169
    :goto_7
    return-object v0

    .line 102
    :cond_8
    invoke-static {}, Lcom/google/googlenav/aC;->a()Lcom/google/googlenav/aC;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/aC;->c(Lcom/google/googlenav/ai;)Z

    .line 103
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/aS;->f(Lcom/google/googlenav/ai;)V

    .line 105
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0400db

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 106
    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/aV;->a(Landroid/view/View;)V

    .line 107
    const v0, 0x1020012

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    .line 109
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v2}, Lbf/m;->g(Lcom/google/googlenav/ai;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    .line 112
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-nez v0, :cond_de

    .line 113
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10001e

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029b

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 118
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->I()Z

    move-result v2

    if-eqz v2, :cond_da

    .line 119
    new-instance v2, Lcom/google/googlenav/ui/view/android/aW;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/aW;-><init>(Lcom/google/googlenav/ui/view/android/aV;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ai;->o(I)V

    .line 133
    :goto_79
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029a

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 134
    new-instance v2, Lcom/google/googlenav/ui/view/android/aX;

    invoke-direct {v2, p0, v0}, Lcom/google/googlenav/ui/view/android/aX;-><init>(Lcom/google/googlenav/ui/view/android/aV;Landroid/widget/ImageView;)V

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/e;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    .line 143
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->N()V

    .line 152
    :cond_8f
    :goto_8f
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setup()V

    .line 154
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f10029c

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->r:Landroid/support/v4/view/ViewPager;

    .line 158
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->J()Lcom/google/googlenav/ui/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    .line 159
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    const/16 v2, 0x3e0

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->K()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/ba;->a(ILandroid/view/View;)V

    .line 160
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    const/16 v2, 0x3e1

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->L()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/ba;->a(ILandroid/view/View;)V

    .line 164
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->cg()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d4

    .line 165
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->s:Lcom/google/googlenav/ui/ba;

    const/16 v2, 0x3e2

    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->n()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/ui/ba;->a(ILandroid/view/View;)V

    .line 168
    :cond_d4
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->l()V

    move-object v0, v1

    .line 169
    goto/16 :goto_7

    .line 130
    :cond_da
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_79

    .line 144
    :cond_de
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 146
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    const v2, 0x7f100299

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 147
    if-eqz v0, :cond_8f

    .line 148
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_8f
.end method

.method public h()V
    .registers 2

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/bn;->h()V

    .line 175
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    if-eqz v0, :cond_c

    .line 176
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->t:Lbk/a;

    invoke-virtual {v0}, Lbk/a;->b()V

    .line 181
    :cond_c
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/aV;->N()V

    .line 183
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 185
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->invalidateOptionsMenu()V

    .line 187
    :cond_1c
    return-void
.end method

.method protected l()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->n:Landroid/view/View;

    if-eqz v0, :cond_15

    .line 195
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->o:Z

    if-eqz v0, :cond_16

    .line 196
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setVisibility(I)V

    .line 203
    :cond_15
    :goto_15
    return-void

    .line 199
    :cond_16
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->q:Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/InstrumentableTabHost;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->n:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_15
.end method

.method protected m()Ljava/util/List;
    .registers 10

    .prologue
    .line 260
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 261
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->o:Z

    if-eqz v0, :cond_a

    move-object v0, v6

    .line 319
    :goto_9
    return-object v0

    .line 267
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-virtual {v2}, Lbf/m;->bs()Ljava/util/Hashtable;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lbf/aS;->a(Lcom/google/googlenav/ai;Ljava/util/Hashtable;Ljava/lang/Object;)Lbf/aQ;

    move-result-object v7

    .line 272
    new-instance v0, Lbj/N;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->l:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-virtual {v4}, Lbf/m;->bf()Lcom/google/googlenav/ui/a;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lbj/N;-><init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/view/c;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->o()Z

    move-result v5

    .line 277
    invoke-virtual {p0, v6, v5}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;Z)V

    .line 278
    invoke-virtual {p0, v7, v6, v5}, Lcom/google/googlenav/ui/view/android/aV;->a(Lbf/aQ;Ljava/util/List;Z)V

    .line 279
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/aV;->d(Ljava/util/List;)V

    .line 280
    invoke-virtual {p0, v6, v7}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;Lbf/aQ;)V

    .line 281
    new-instance v0, Lbj/P;

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    invoke-direct {v0, v1, v2, v7, v3}, Lbj/P;-><init>(ILcom/google/googlenav/ai;Lbf/aQ;Lbf/i;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    new-instance v0, Lbj/bm;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->l:Lcom/google/googlenav/ui/br;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v8, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v4, v8}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-direct/range {v0 .. v5}, Lbj/bm;-><init>(ILcom/google/googlenav/ui/br;Lcom/google/googlenav/ai;ZZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    new-instance v0, Lbj/I;

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2}, Lbj/I;-><init>(ILcom/google/googlenav/ai;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    invoke-virtual {p0, v6, v7, v5}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;Lbf/aQ;Z)V

    .line 289
    invoke-virtual {p0, v6, v7, v5}, Lcom/google/googlenav/ui/view/android/aV;->b(Ljava/util/List;Lbf/aQ;Z)V

    .line 290
    invoke-virtual {p0, v6, v7, v5}, Lcom/google/googlenav/ui/view/android/aV;->c(Ljava/util/List;Lbf/aQ;Z)V

    .line 292
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->G()Z

    move-result v0

    if-eqz v0, :cond_84

    .line 293
    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/view/android/aV;->a(Lbf/aQ;)Lbj/H;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_84
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->F()Z

    move-result v0

    if-eqz v0, :cond_96

    .line 297
    new-instance v0, Lbj/aX;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v5}, Lbj/aX;-><init>(Lcom/google/googlenav/ai;IZ)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    :cond_96
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/view/android/aV;->a(Ljava/util/List;)V

    .line 303
    new-instance v0, Lbj/al;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-direct {v0, v1, v2, v5}, Lbj/al;-><init>(ILcom/google/googlenav/ai;Z)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v0, v1}, Lbf/m;->b(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_d4

    .line 308
    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/aV;->d:Lbf/m;

    check-cast v3, Lbf/bk;

    .line 309
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-virtual {v3, v0}, Lbf/bk;->l(Lcom/google/googlenav/ai;)I

    move-result v2

    .line 310
    invoke-virtual {v3}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    const/16 v1, 0x575

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0x13

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/g;->a(Lcom/google/googlenav/ai;Ljava/lang/String;ILbf/bk;IZ)Lcom/google/googlenav/ui/view/android/g;

    move-result-object v0

    .line 314
    if-eqz v0, :cond_d4

    .line 315
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_d4
    move-object v0, v6

    .line 319
    goto/16 :goto_9
.end method

.method n()Landroid/view/View;
    .registers 4

    .prologue
    .line 323
    new-instance v0, Lbk/h;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/aV;->a:Lcom/google/googlenav/L;

    invoke-direct {v0, v1, v2}, Lbk/h;-><init>(Lcom/google/googlenav/ai;Lcom/google/googlenav/L;)V

    .line 324
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/aV;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbk/h;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected o()Z
    .registers 2

    .prologue
    .line 329
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 334
    sget-object v0, Lcom/google/googlenav/ui/view/android/aV;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 335
    const v1, 0x7f110009

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 337
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 339
    const v0, 0x7f1002a7

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 340
    const/16 v1, 0x3c7

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 341
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->I()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 342
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 343
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->o(I)V

    .line 346
    :cond_3f
    const v0, 0x7f100305

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 347
    if-eqz v1, :cond_5d

    .line 348
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v0}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v0

    if-eqz v0, :cond_93

    .line 349
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/aV;->u:Z

    if-eqz v0, :cond_8f

    const v0, 0x7f020269

    :goto_57
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 350
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 357
    :cond_5d
    :goto_5d
    const v0, 0x7f1004bc

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_6f

    .line 360
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/m;->f(Lcom/google/googlenav/ai;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 363
    :cond_6f
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->b(Landroid/view/Menu;)V

    .line 364
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->c(Landroid/view/Menu;)V

    .line 366
    const v0, 0x7f1001e2

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 367
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    invoke-static {v1}, Lbf/m;->e(Lcom/google/googlenav/ai;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 369
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->e(Landroid/view/Menu;)V

    .line 370
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->f(Landroid/view/Menu;)V

    .line 371
    invoke-virtual {p0, p1}, Lcom/google/googlenav/ui/view/android/aV;->g(Landroid/view/Menu;)V

    .line 373
    return v2

    .line 349
    :cond_8f
    const v0, 0x7f020268

    goto :goto_57

    .line 352
    :cond_93
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_5d
.end method

.method public v()Lcom/google/googlenav/ai;
    .registers 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/aV;->c:Lcom/google/googlenav/ai;

    return-object v0
.end method
