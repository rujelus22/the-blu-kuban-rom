.class public Lcom/google/googlenav/ui/wizard/hb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/ui/wizard/hg;

.field private final b:Lcom/google/googlenav/ai;

.field private final c:Lcom/google/googlenav/ui/wizard/jv;

.field private d:Lcom/google/googlenav/f;

.field private e:Ljava/util/List;

.field private f:Z

.field private g:Lcom/google/googlenav/ui/wizard/jq;

.field private h:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hg;Lcom/google/googlenav/ai;Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/google/googlenav/ui/wizard/hc;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hc;-><init>(Lcom/google/googlenav/ui/wizard/hb;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->h:Ljava/util/Comparator;

    .line 51
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->a:Lcom/google/googlenav/ui/wizard/hg;

    .line 52
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    .line 53
    iput-object p3, p0, Lcom/google/googlenav/ui/wizard/hb;->c:Lcom/google/googlenav/ui/wizard/jv;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ai;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Lcom/google/googlenav/f;)Lcom/google/googlenav/f;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Lcom/google/googlenav/ui/wizard/jq;)Lcom/google/googlenav/ui/wizard/jq;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hb;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/ui/wizard/hb;)Ljava/util/Comparator;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->h:Ljava/util/Comparator;

    return-object v0
.end method

.method private d()V
    .registers 8

    .prologue
    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    .line 120
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->c:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x1af

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/ui/wizard/he;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/he;-><init>(Lcom/google/googlenav/ui/wizard/hb;)V

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;JZ)V

    .line 137
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    if-nez v0, :cond_1e

    .line 138
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hb;->a()V

    .line 140
    :cond_1e
    return-void
.end method

.method static synthetic d(Lcom/google/googlenav/ui/wizard/hb;)Z
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ui/wizard/jv;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->c:Lcom/google/googlenav/ui/wizard/jv;

    return-object v0
.end method

.method static synthetic f(Lcom/google/googlenav/ui/wizard/hb;)Lcom/google/googlenav/ui/wizard/hg;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->a:Lcom/google/googlenav/ui/wizard/hg;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    if-eqz v0, :cond_9

    .line 116
    :cond_8
    :goto_8
    return-void

    .line 76
    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->W()Ljava/lang/String;

    move-result-object v0

    .line 77
    new-instance v1, Lcom/google/googlenav/ui/wizard/hd;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/ui/wizard/hd;-><init>(Lcom/google/googlenav/ui/wizard/hb;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->b:Lcom/google/googlenav/ai;

    invoke-static {v1, v0}, Lcom/google/googlenav/f;->a(Lcom/google/googlenav/g;Lcom/google/googlenav/ai;)Lcom/google/googlenav/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    .line 115
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hb;->d:Lcom/google/googlenav/f;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    goto :goto_8
.end method

.method public b()V
    .registers 5

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    if-nez v0, :cond_8

    .line 144
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hb;->d()V

    .line 171
    :goto_7
    return-void

    .line 146
    :cond_8
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 147
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/cx;

    .line 148
    iget-object v0, v0, Lcom/google/googlenav/cx;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 151
    :cond_2b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_49

    .line 152
    new-instance v0, Lcom/google/googlenav/ui/wizard/jq;

    new-instance v2, Lcom/google/googlenav/ui/wizard/hf;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hf;-><init>(Lcom/google/googlenav/ui/wizard/hb;)V

    const/16 v3, 0x3f0

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/googlenav/ui/wizard/jq;-><init>(Lcom/google/googlenav/ui/wizard/js;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    .line 165
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jq;->show()V

    goto :goto_7

    .line 167
    :cond_49
    const/16 v0, 0x3f8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    goto :goto_7
.end method

.method public c()V
    .registers 2

    .prologue
    .line 177
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hb;->f:Z

    .line 178
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    if-eqz v0, :cond_f

    .line 179
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jq;->dismiss()V

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hb;->g:Lcom/google/googlenav/ui/wizard/jq;

    .line 182
    :cond_f
    return-void
.end method
