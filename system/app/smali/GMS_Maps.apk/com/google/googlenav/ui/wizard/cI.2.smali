.class public Lcom/google/googlenav/ui/wizard/cI;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/aD;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/cH;

.field private b:Lcom/google/googlenav/ui/wizard/cH;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/cH;Lcom/google/googlenav/ui/wizard/cH;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/cI;->b:Lcom/google/googlenav/ui/wizard/cH;

    .line 60
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/cI;)V
    .registers 1
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/cI;->b()V

    return-void
.end method

.method private b()V
    .registers 4

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cH;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/4 v1, 0x1

    new-instance v2, Lcom/google/googlenav/ui/wizard/cL;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/cL;-><init>(Lcom/google/googlenav/ui/wizard/cI;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(ZLcom/google/googlenav/ui/wizard/db;)V

    .line 132
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->b:Lcom/google/googlenav/ui/wizard/cH;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/cH;->a()V

    .line 65
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aE;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v6, v0, Lcom/google/googlenav/ui/wizard/cH;->a:Lcom/google/googlenav/friend/history/z;

    new-instance v0, Lcom/google/googlenav/ui/wizard/cJ;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/wizard/cJ;-><init>(Lcom/google/googlenav/ui/wizard/cI;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/ui/view/dialog/aE;)V

    invoke-virtual {v6, p2, p3, v0}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/b;Lcom/google/googlenav/friend/history/L;)V

    .line 91
    return-void
.end method

.method public a(Lcom/google/googlenav/friend/history/o;)V
    .registers 4
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->b:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cH;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cM;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/cM;-><init>(Lcom/google/googlenav/ui/wizard/cI;Lcom/google/googlenav/friend/history/o;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/friend/history/o;Lcom/google/googlenav/ui/wizard/ba;)V

    .line 162
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/aG;)V
    .registers 4
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cH;->a:Lcom/google/googlenav/friend/history/z;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cK;

    invoke-direct {v1, p0, p1}, Lcom/google/googlenav/ui/wizard/cK;-><init>(Lcom/google/googlenav/ui/wizard/cI;Lcom/google/googlenav/ui/view/dialog/aG;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/N;)V

    .line 113
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/googlenav/ui/view/dialog/aF;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cH;->a:Lcom/google/googlenav/friend/history/z;

    new-instance v1, Lcom/google/googlenav/ui/wizard/cN;

    invoke-direct {v1, p0, p2}, Lcom/google/googlenav/ui/wizard/cN;-><init>(Lcom/google/googlenav/ui/wizard/cI;Lcom/google/googlenav/ui/view/dialog/aF;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/friend/history/z;->a(Ljava/lang/String;Lcom/google/googlenav/friend/history/M;)V

    .line 180
    return-void
.end method

.method public b(Lcom/google/googlenav/friend/history/o;)V
    .registers 9
    .parameter

    .prologue
    .line 136
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v6

    new-instance v0, Lcom/google/googlenav/friend/history/a;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/o;->f()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/o;->g()J

    move-result-wide v3

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/friend/history/a;-><init>(JJLcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v6, v0}, Law/h;->c(Law/g;)V

    .line 138
    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/o;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/friend/history/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 139
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cH;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ax;->m()V

    .line 141
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/cI;->a:Lcom/google/googlenav/ui/wizard/cH;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/cH;->a:Lcom/google/googlenav/friend/history/z;

    invoke-virtual {p1}, Lcom/google/googlenav/friend/history/o;->t()Lcom/google/googlenav/friend/history/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/z;->a(Lcom/google/googlenav/friend/history/b;)V

    .line 142
    return-void
.end method
