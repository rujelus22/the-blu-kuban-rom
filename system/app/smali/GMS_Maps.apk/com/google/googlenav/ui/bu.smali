.class public Lcom/google/googlenav/ui/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private final c:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/bu;->a:Ljava/util/List;

    .line 39
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/bu;->b:Ljava/util/List;

    .line 43
    iput p1, p0, Lcom/google/googlenav/ui/bu;->c:I

    .line 44
    return-void
.end method


# virtual methods
.method public a()Lcom/google/googlenav/ui/bt;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 59
    new-instance v2, Lcom/google/googlenav/ui/bt;

    iget-object v0, p0, Lcom/google/googlenav/ui/bu;->a:Ljava/util/List;

    new-array v1, v3, [Lam/g;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lam/g;

    iget-object v1, p0, Lcom/google/googlenav/ui/bu;->b:Ljava/util/List;

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/bt;-><init>([Lam/g;[Ljava/lang/String;)V

    return-object v2
.end method

.method public a(I)Lcom/google/googlenav/ui/bu;
    .registers 3
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lcom/google/googlenav/ui/bu;->c:I

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bi;->c(II)Lam/g;

    move-result-object v0

    .line 48
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/bu;->a(Lam/g;)Lcom/google/googlenav/ui/bu;

    .line 49
    return-object p0
.end method

.method public a(Lam/g;)Lcom/google/googlenav/ui/bu;
    .registers 4
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/googlenav/ui/bu;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/google/googlenav/ui/bu;->b:Ljava/util/List;

    invoke-interface {p1}, Lam/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    return-object p0
.end method
