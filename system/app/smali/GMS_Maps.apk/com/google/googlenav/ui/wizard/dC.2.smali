.class public Lcom/google/googlenav/ui/wizard/dC;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/dg;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 693
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    .line 694
    const v0, 0x7f0f001b

    invoke-direct {p0, p2, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 695
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/wizard/dh;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 691
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/wizard/dC;-><init>(Lcom/google/googlenav/ui/wizard/dg;Lcom/google/googlenav/ui/e;)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/dC;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/dC;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method


# virtual methods
.method protected I_()V
    .registers 3

    .prologue
    .line 708
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dC;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 709
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-nez v1, :cond_19

    .line 710
    const v1, 0x7f0201a7

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 714
    :goto_10
    new-instance v0, Lcom/google/googlenav/ui/wizard/dD;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/dD;-><init>(Lcom/google/googlenav/ui/wizard/dC;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dC;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 720
    return-void

    .line 712
    :cond_19
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/dg;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_10
.end method

.method protected O_()V
    .registers 2

    .prologue
    .line 699
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_14

    .line 700
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_14

    .line 701
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/dC;->requestWindowFeature(I)Z

    .line 704
    :cond_14
    return-void
.end method

.method public c()Landroid/view/View;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 724
    sget-object v0, Lcom/google/googlenav/ui/wizard/dC;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    invoke-virtual {v0}, Lcom/google/googlenav/android/BaseMapsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400ea

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 727
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-nez v0, :cond_41

    .line 728
    const v0, 0x7f100025

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 729
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/dg;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 730
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 731
    const v0, 0x7f100033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 732
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x4

    if-ne v2, v3, :cond_64

    .line 733
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 734
    const v2, 0x7f02022a

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 755
    :cond_41
    :goto_41
    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 756
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/dC;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 757
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 758
    new-instance v2, Lcom/google/googlenav/ui/wizard/dE;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/dE;-><init>(Lcom/google/googlenav/ui/wizard/dC;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 774
    return-object v1

    .line 735
    :cond_64
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/dC;->a:Lcom/google/googlenav/ui/wizard/dg;

    iget-byte v2, v2, Lcom/google/googlenav/ui/wizard/dg;->c:B

    const/4 v3, 0x5

    if-ne v2, v3, :cond_75

    .line 736
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 737
    const v2, 0x7f020228

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_41

    .line 739
    :cond_75
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_85

    .line 742
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_41

    .line 744
    :cond_85
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 745
    const v2, 0x7f020215

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_41
.end method
