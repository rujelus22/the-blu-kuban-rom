.class public Lcom/google/googlenav/ui/view/android/au;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field private a:LaB/s;

.field private b:Landroid/widget/ListView;

.field private c:Lcom/google/googlenav/as;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/S;-><init>()V

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/e;)V
    .registers 3
    .parameter

    .prologue
    .line 62
    const v0, 0x7f0f0018

    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/au;)Landroid/widget/ListView;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method private a(Lcom/google/googlenav/at;)Lcom/google/googlenav/ui/bs;
    .registers 4
    .parameter

    .prologue
    .line 306
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/google/googlenav/at;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 307
    :cond_c
    invoke-static {}, Lcom/google/googlenav/ui/view/android/aA;->c()Lcom/google/googlenav/ui/view/android/aA;

    move-result-object v0

    .line 309
    :goto_10
    return-object v0

    :cond_11
    new-instance v0, Lcom/google/googlenav/ui/view/android/aA;

    invoke-virtual {p1}, Lcom/google/googlenav/at;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/googlenav/ui/view/android/aA;-><init>(Ljava/lang/String;)V

    goto :goto_10
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/au;Lcom/google/googlenav/at;)Lcom/google/googlenav/ui/bs;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/at;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;C)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 181
    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->n()Lam/g;

    move-result-object v0

    .line 182
    invoke-interface {v0, p2}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 183
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 184
    return-void
.end method

.method private a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-virtual {p2}, Lcom/google/googlenav/ui/aW;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 82
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    :goto_f
    return-void

    .line 84
    :cond_10
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/bi;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 85
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_f
.end method

.method private a(Lcom/google/googlenav/as;)V
    .registers 6
    .parameter

    .prologue
    .line 220
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 221
    invoke-virtual {p1}, Lcom/google/googlenav/as;->g()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_c

    .line 238
    :goto_b
    return-void

    .line 225
    :cond_c
    invoke-virtual {p1}, Lcom/google/googlenav/as;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_14
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/at;

    .line 226
    invoke-virtual {v0}, Lcom/google/googlenav/at;->a()Lcom/google/googlenav/ar;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 228
    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/at;)Lcom/google/googlenav/ui/bs;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_14

    .line 232
    :cond_2e
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->a:LaB/s;

    new-instance v2, Lcom/google/googlenav/ui/view/android/ay;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/view/android/ay;-><init>(Lcom/google/googlenav/ui/view/android/au;)V

    invoke-virtual {v0, v1, v2}, LaB/s;->a(Ljava/lang/Iterable;LaB/p;)V

    goto :goto_b
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/au;Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    return-void
.end method

.method static synthetic b(Lcom/google/googlenav/ui/view/android/au;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/au;->m()V

    return-void
.end method

.method static synthetic c(Lcom/google/googlenav/ui/view/android/au;)LaB/s;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->a:LaB/s;

    return-object v0
.end method

.method private l()V
    .registers 6

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-virtual {v0}, Lcom/google/googlenav/as;->f()Lcom/google/googlenav/ai;

    move-result-object v2

    .line 96
    const v0, 0x7f10019a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 97
    invoke-virtual {v2}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 98
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 100
    const v0, 0x7f10001e

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 101
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-virtual {v1}, Lcom/google/googlenav/as;->d()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->aP:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 102
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 104
    const v0, 0x7f10019d

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 105
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-virtual {v1}, Lcom/google/googlenav/as;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 106
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 108
    const v0, 0x7f10019e

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 109
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-virtual {v1}, Lcom/google/googlenav/as;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->bI:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 110
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 112
    const v0, 0x7f10019f

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    invoke-virtual {v2}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 114
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 116
    const v0, 0x7f1001a0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 117
    invoke-virtual {v2}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->aT:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    .line 118
    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/TextView;Lcom/google/googlenav/ui/aW;)V

    .line 120
    const v0, 0x7f1001a3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 121
    sget-char v1, Lcom/google/googlenav/ui/bi;->aX:C

    const/16 v3, 0x509

    const/4 v4, 0x1

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/view/View;CII)V

    .line 124
    const v0, 0x7f1001a5

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 125
    sget-char v1, Lcom/google/googlenav/ui/bi;->aW:C

    const/16 v3, 0xf1

    const/16 v4, 0x25b

    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/view/View;CII)V

    .line 128
    const v0, 0x7f1001a7

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 129
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    const v0, 0x7f100024

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    const v0, 0x7f10019b

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/view/android/DistanceView;

    .line 134
    const v1, 0x7f10019c

    invoke-virtual {p0, v1}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/googlenav/ui/view/android/HeadingView;

    .line 135
    invoke-virtual {v2}, Lcom/google/googlenav/ai;->D()LaN/g;

    move-result-object v2

    invoke-interface {v2}, LaN/g;->b()LaN/B;

    move-result-object v2

    invoke-static {v0, v1, v2}, LaV/g;->a(Lcom/google/googlenav/ui/view/android/DistanceView;Lcom/google/googlenav/ui/view/android/HeadingView;LaN/B;)V

    .line 138
    const v0, 0x7f1001a1

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->b:Landroid/widget/ListView;

    .line 139
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-virtual {v0}, Lcom/google/googlenav/as;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_10c

    .line 140
    new-instance v0, Lcom/google/googlenav/ui/view/android/az;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/au;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-virtual {v2}, Lcom/google/googlenav/as;->g()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/googlenav/ui/view/android/az;-><init>(Lcom/google/googlenav/ui/view/android/au;Landroid/content/Context;Ljava/util/List;)V

    .line 142
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/au;->b:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 144
    :cond_10c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/google/googlenav/ui/view/android/av;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/av;-><init>(Lcom/google/googlenav/ui/view/android/au;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/au;->a(Lcom/google/googlenav/as;)V

    .line 160
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 206
    sget-object v0, Lcom/google/googlenav/ui/view/android/au;->e:Lcom/google/googlenav/android/BaseMapsActivity;

    new-instance v1, Lcom/google/googlenav/ui/view/android/ax;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/view/android/ax;-><init>(Lcom/google/googlenav/ui/view/android/au;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/BaseMapsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 213
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;CII)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 163
    const v0, 0x7f100022

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 164
    invoke-direct {p0, v0, p2}, Lcom/google/googlenav/ui/view/android/au;->a(Landroid/widget/ImageView;C)V

    .line 165
    const v0, 0x7f100413

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    invoke-static {p3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    new-instance v1, Lcom/google/googlenav/ui/view/android/aw;

    invoke-direct {v1, p0, p4}, Lcom/google/googlenav/ui/view/android/aw;-><init>(Lcom/google/googlenav/ui/view/android/au;I)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-nez v1, :cond_31

    .line 174
    const v1, 0x7f0201f8

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 176
    :cond_31
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 178
    return-void
.end method

.method public a(Lcom/google/googlenav/as;LaB/s;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 192
    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/au;->a:LaB/s;

    .line 193
    iput-object p1, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    .line 196
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/au;->show()V

    .line 197
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/au;->l()V

    .line 198
    return-void
.end method

.method protected c()Landroid/view/View;
    .registers 4

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/au;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040071

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public h()Lcom/google/googlenav/as;
    .registers 2

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/au;->c:Lcom/google/googlenav/as;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/googlenav/ui/view/android/S;->onCreate(Landroid/os/Bundle;)V

    .line 73
    return-void
.end method
