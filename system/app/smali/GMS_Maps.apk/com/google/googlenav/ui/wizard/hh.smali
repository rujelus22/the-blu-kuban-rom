.class public Lcom/google/googlenav/ui/wizard/hH;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/dy;


# static fields
.field private static final k:Ljava/util/Set;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Lcom/google/googlenav/ui/wizard/hQ;

.field protected c:I

.field protected i:I

.field protected j:Lcom/google/googlenav/ui/wizard/C;

.field private l:Z

.field private m:Lcom/google/googlenav/ui/wizard/hZ;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    sput-object v0, Lcom/google/googlenav/ui/wizard/hH;->k:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    .line 216
    new-instance v0, Lcom/google/googlenav/ui/wizard/hZ;

    invoke-direct {v0, p1, p0}, Lcom/google/googlenav/ui/wizard/hZ;-><init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/wizard/hH;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->m:Lcom/google/googlenav/ui/wizard/hZ;

    .line 217
    return-void
.end method

.method private B()V
    .registers 4

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 466
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    .line 473
    :cond_10
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->e()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 474
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[v"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->O()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    .line 478
    :cond_4d
    new-instance v0, Lcom/google/googlenav/ui/wizard/hI;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/wizard/hI;-><init>(Lcom/google/googlenav/ui/wizard/hH;)V

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(LaZ/b;)LaZ/a;

    move-result-object v0

    .line 490
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 491
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->m:Lcom/google/googlenav/ui/wizard/hZ;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/hZ;->a(LaZ/a;)V

    .line 495
    :goto_65
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    .line 496
    return-void

    .line 493
    :cond_6a
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(Law/g;)V

    goto :goto_65
.end method

.method private C()V
    .registers 3

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 559
    iget v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    packed-switch v1, :pswitch_data_38

    .line 574
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 578
    :goto_a
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v1, :cond_13

    .line 579
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 582
    :cond_13
    if-eqz v0, :cond_18

    .line 583
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->hide()V

    .line 585
    :cond_18
    return-void

    .line 562
    :pswitch_19
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->g()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_a

    .line 565
    :pswitch_20
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->y()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v1

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_a

    .line 568
    :pswitch_27
    new-instance v1, Lcom/google/googlenav/ui/wizard/hJ;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hJ;-><init>(Lcom/google/googlenav/ui/wizard/hH;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_a

    .line 571
    :pswitch_2f
    new-instance v1, Lcom/google/googlenav/ui/wizard/hR;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hR;-><init>(Lcom/google/googlenav/ui/wizard/hH;)V

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    goto :goto_a

    .line 559
    nop

    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_19
        :pswitch_19
        :pswitch_27
        :pswitch_2f
        :pswitch_20
    .end packed-switch
.end method

.method private D()Z
    .registers 3

    .prologue
    .line 784
    sget-object v0, Lcom/google/googlenav/ui/wizard/hH;->k:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/wizard/hH;)Z
    .registers 2
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->D()Z

    move-result v0

    return v0
.end method

.method private i()V
    .registers 3

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    .line 355
    iget v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    .line 356
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->a()V

    .line 357
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    .line 358
    iput v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    .line 359
    return-void
.end method

.method public static z()Ljava/lang/String;
    .registers 4

    .prologue
    .line 548
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v0

    invoke-virtual {v0}, LaM/f;->v()Ljava/lang/String;

    move-result-object v0

    .line 549
    if-nez v0, :cond_11

    .line 550
    const/16 v0, 0x455

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 552
    :goto_10
    return-object v0

    :cond_11
    const/16 v1, 0x456

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_10
.end method


# virtual methods
.method public A()LaM/g;
    .registers 2

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->m:Lcom/google/googlenav/ui/wizard/hZ;

    return-object v0
.end method

.method protected R_()S
    .registers 2

    .prologue
    .line 282
    const/16 v0, 0x4a

    return v0
.end method

.method public S_()Z
    .registers 2

    .prologue
    .line 613
    const/4 v0, 0x0

    return v0
.end method

.method public T_()V
    .registers 1

    .prologue
    .line 617
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    .line 618
    return-void
.end method

.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 287
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hH;->l:Z

    if-eqz v0, :cond_7

    .line 288
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    .line 295
    :goto_6
    return v0

    .line 291
    :cond_7
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_15

    .line 292
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->h()V

    .line 293
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    goto :goto_6

    .line 295
    :cond_15
    const/4 v0, 0x3

    goto :goto_6
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hH;->l:Z

    if-eqz v0, :cond_7

    .line 301
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    .line 303
    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x3

    goto :goto_6
.end method

.method protected a(LaZ/b;)LaZ/a;
    .registers 15
    .parameter

    .prologue
    .line 511
    new-instance v0, LaZ/a;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v4, v4, Lcom/google/googlenav/ui/wizard/hQ;->a:LaN/B;

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v5, v5, Lcom/google/googlenav/ui/wizard/hQ;->b:Lo/D;

    iget-object v6, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v6, v6, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v6}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v6

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v8, v8, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    iget-object v9, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v9, v9, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    iget-object v10, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v10, v10, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    iget-object v11, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v11, v11, Lcom/google/googlenav/ui/wizard/hQ;->c:Ljava/lang/String;

    move-object v12, p1

    invoke-direct/range {v0 .. v12}, LaZ/a;-><init>(ILjava/lang/String;[BLaN/B;Lo/D;ZILaN/H;IILjava/lang/String;LaZ/b;)V

    return-object v0
.end method

.method protected a(I)V
    .registers 6
    .parameter

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    .line 270
    const-string v1, "m"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 274
    iput p1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    .line 275
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->C()V

    .line 276
    return-void
.end method

.method public a(LaN/H;IILcom/google/googlenav/ui/wizard/C;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 316
    new-instance v0, Lcom/google/googlenav/ui/wizard/hQ;

    invoke-direct {v0}, Lcom/google/googlenav/ui/wizard/hQ;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    .line 317
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->d:LaN/H;

    .line 318
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p2, v0, Lcom/google/googlenav/ui/wizard/hQ;->e:I

    .line 319
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p3, v0, Lcom/google/googlenav/ui/wizard/hQ;->f:I

    .line 320
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    .line 321
    iput-object p4, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    .line 322
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    .line 323
    return-void
.end method

.method protected a(Law/g;)V
    .registers 4
    .parameter

    .prologue
    .line 499
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    .line 503
    invoke-static {}, Lcom/google/googlenav/android/V;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 508
    :goto_9
    return-void

    .line 506
    :cond_a
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->F()Lcom/google/googlenav/J;

    move-result-object v0

    const/16 v1, 0x50f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 507
    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    goto :goto_9
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    invoke-virtual {p1}, Lax/y;->f()LaN/B;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    .line 609
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    .line 610
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 442
    const/16 v0, 0x45a

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 445
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->d:Lcom/google/googlenav/ui/wizard/jv;

    new-instance v2, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v2}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    const/16 v3, 0x43c

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/googlenav/ui/wizard/dF;->b(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    const/16 v2, 0x450

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dF;->d(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    .line 457
    return-void
.end method

.method protected a(Ljava/lang/String;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput p2, v0, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    .line 521
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->h:Ljava/lang/String;

    .line 522
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    .line 523
    const-string v1, "s"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0, p3}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    .line 527
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 369
    sparse-switch p1, :sswitch_data_d0

    .line 431
    invoke-super {p0, p1, p2, p3}, Lcom/google/googlenav/ui/wizard/C;->a(IILjava/lang/Object;)Z

    move-result v7

    :goto_b
    return v7

    .line 371
    :sswitch_c
    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_b

    .line 374
    :sswitch_10
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_b

    .line 377
    :sswitch_15
    invoke-virtual {p0, v6}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_b

    .line 380
    :sswitch_19
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "a"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 382
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    if-ne v0, v6, :cond_4a

    .line 387
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->D()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 388
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->B()V

    goto :goto_b

    .line 390
    :cond_45
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_b

    .line 393
    :cond_4a
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->B()V

    goto :goto_b

    .line 397
    :sswitch_4e
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->a()V

    goto :goto_b

    .line 402
    :sswitch_70
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    check-cast p3, Ljava/lang/String;

    iput-object p3, v0, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    goto :goto_b

    .line 405
    :sswitch_77
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->i()V

    .line 406
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "p"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 408
    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 409
    invoke-static {v0}, Lbm/r;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto/16 :goto_b

    .line 412
    :sswitch_8e
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    if-nez v1, :cond_9d

    move v4, v7

    :cond_9d
    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/d;->a(Z)V

    goto/16 :goto_b

    .line 419
    :sswitch_a2
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->d:Lcom/google/googlenav/ui/wizard/jv;

    const/16 v1, 0x44c

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x44b

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35b

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    goto/16 :goto_b

    .line 428
    :sswitch_bc
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v0, v0, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    if-nez v1, :cond_cb

    move v4, v7

    :cond_cb
    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/d;->a(Z)V

    goto/16 :goto_b

    .line 369
    :sswitch_data_d0
    .sparse-switch
        0x1f5 -> :sswitch_19
        0x1f6 -> :sswitch_4e
        0x5dd -> :sswitch_c
        0x5e3 -> :sswitch_70
        0x5e4 -> :sswitch_bc
        0x5e5 -> :sswitch_15
        0x5ee -> :sswitch_77
        0x5ef -> :sswitch_8e
        0x5f0 -> :sswitch_a2
        0x5f1 -> :sswitch_10
    .end sparse-switch
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 327
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    .line 328
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->i:I

    .line 329
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/ui/wizard/hH;->l:Z

    .line 330
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->C()V

    .line 331
    return-void
.end method

.method protected b(Ljava/lang/String;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iput-object p1, v0, Lcom/google/googlenav/ui/wizard/hQ;->i:Ljava/lang/String;

    .line 538
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    .line 539
    const-string v1, "ss"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "si"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-virtual {p0, p3}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    .line 544
    return-void
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 343
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    .line 344
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    .line 345
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 346
    return-void
.end method

.method public d()V
    .registers 1

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/google/googlenav/ui/wizard/hH;->i()V

    .line 364
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->j()V

    .line 365
    return-void
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method protected g()Lcom/google/googlenav/ui/view/android/aL;
    .registers 2

    .prologue
    .line 592
    const/4 v0, 0x0

    return-object v0
.end method

.method public h()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 229
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "b"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 231
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->i:I

    iget v1, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    if-ne v0, v1, :cond_52

    .line 232
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->R_()S

    move-result v0

    const-string v1, "c"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "o="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 234
    iput v4, p0, Lcom/google/googlenav/ui/wizard/hH;->g:I

    .line 255
    :goto_45
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    if-eqz v0, :cond_4e

    .line 256
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/hH;->j:Lcom/google/googlenav/ui/wizard/C;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    .line 258
    :cond_4e
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->a()V

    .line 261
    :goto_51
    return-void

    .line 236
    :cond_52
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    packed-switch v0, :pswitch_data_70

    goto :goto_45

    .line 238
    :pswitch_58
    invoke-virtual {p0, v4}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_51

    .line 242
    :pswitch_5c
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hH;->e()Z

    move-result v0

    if-eqz v0, :cond_67

    .line 243
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_51

    .line 245
    :cond_67
    invoke-virtual {p0, v4}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_51

    .line 250
    :pswitch_6b
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/wizard/hH;->a(I)V

    goto :goto_51

    .line 236
    :pswitch_data_70
    .packed-switch 0x2
        :pswitch_58
        :pswitch_5c
        :pswitch_6b
    .end packed-switch
.end method

.method public p()Z
    .registers 2

    .prologue
    .line 350
    iget v0, p0, Lcom/google/googlenav/ui/wizard/hH;->c:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected y()Lcom/google/googlenav/ui/view/android/aL;
    .registers 2

    .prologue
    .line 600
    const/4 v0, 0x0

    return-object v0
.end method
