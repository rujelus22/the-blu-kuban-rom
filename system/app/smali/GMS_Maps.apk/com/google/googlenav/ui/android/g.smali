.class Lcom/google/googlenav/ui/android/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/by;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/android/AndroidVectorView;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/android/AndroidVectorView;)V
    .registers 2
    .parameter

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/googlenav/ui/android/g;->a:Lcom/google/googlenav/ui/android/AndroidVectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/googlenav/ui/android/g;->a:Lcom/google/googlenav/ui/android/AndroidVectorView;

    iget-object v0, v0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    if-eqz v1, :cond_20

    .line 258
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-static {p2}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->b(LaN/B;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(ILat/a;)Z

    move-result v0

    .line 271
    :goto_1f
    return v0

    .line 264
    :cond_20
    sget-object v1, LaE/g;->a:LaE/g;

    invoke-virtual {v1}, LaE/g;->e()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 265
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->n()Lbf/aA;

    move-result-object v0

    .line 266
    if-eqz v0, :cond_41

    invoke-virtual {v0}, Lbf/aA;->a()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 267
    invoke-static {p2}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbf/aA;->d(LaN/B;)Z

    move-result v0

    goto :goto_1f

    .line 271
    :cond_41
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lo/T;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/googlenav/ui/android/g;->a:Lcom/google/googlenav/ui/android/AndroidVectorView;

    iget-object v0, v0, Lcom/google/googlenav/ui/android/AndroidVectorView;->b:Lcom/google/googlenav/android/i;

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->i()Lcom/google/googlenav/ui/s;

    move-result-object v0

    .line 278
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ah()Lcom/google/googlenav/ui/wizard/C;

    move-result-object v1

    if-eqz v1, :cond_20

    .line 279
    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v1

    invoke-static {p2}, LR/e;->b(Lo/T;)LaN/B;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/jv;->b(LaN/B;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(ILat/a;)Z

    move-result v0

    .line 283
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method
