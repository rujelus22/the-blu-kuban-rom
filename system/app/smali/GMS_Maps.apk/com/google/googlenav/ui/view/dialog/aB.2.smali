.class Lcom/google/googlenav/ui/view/dialog/aB;
.super Landroid/support/v4/view/x;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/view/dialog/ax;


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ui/view/dialog/ax;)V
    .registers 2
    .parameter

    .prologue
    .line 539
    iput-object p1, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-direct {p0}, Landroid/support/v4/view/x;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/googlenav/ui/view/dialog/ax;Lcom/google/googlenav/ui/view/dialog/ay;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 539
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/dialog/aB;-><init>(Lcom/google/googlenav/ui/view/dialog/ax;)V

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/Object;)I
    .registers 5
    .parameter

    .prologue
    .line 558
    check-cast p1, Lcom/google/googlenav/ui/view/dialog/aC;

    .line 561
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2e

    .line 562
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    .line 563
    invoke-static {p1}, Lcom/google/googlenav/ui/view/dialog/aC;->a(Lcom/google/googlenav/ui/view/dialog/aC;)Lcom/google/googlenav/friend/history/O;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/history/O;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 568
    invoke-static {p1}, Lcom/google/googlenav/ui/view/dialog/aC;->b(Lcom/google/googlenav/ui/view/dialog/aC;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/history/O;->a(Landroid/view/View;)V

    .line 572
    :goto_29
    return v1

    .line 561
    :cond_2a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 572
    :cond_2e
    const/4 v1, -0x2

    goto :goto_29
.end method

.method public a(I)Ljava/lang/CharSequence;
    .registers 3
    .parameter

    .prologue
    .line 588
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/dialog/aB;->a()I

    move-result v0

    if-lt p1, v0, :cond_9

    .line 591
    const-string v0, ""

    .line 593
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    invoke-virtual {v0}, Lcom/google/googlenav/friend/history/O;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_8
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/ax;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 550
    iget-object v0, p0, Lcom/google/googlenav/ui/view/dialog/aB;->a:Lcom/google/googlenav/ui/view/dialog/ax;

    iget-object v0, v0, Lcom/google/googlenav/ui/view/dialog/ax;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/friend/history/O;

    .line 551
    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/history/O;->a(Landroid/view/View;)V

    .line 552
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 553
    new-instance v2, Lcom/google/googlenav/ui/view/dialog/aC;

    invoke-direct {v2, v0, v1}, Lcom/google/googlenav/ui/view/dialog/aC;-><init>(Lcom/google/googlenav/friend/history/O;Landroid/view/View;)V

    return-object v2
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 578
    check-cast p1, Landroid/support/v4/view/ViewPager;

    check-cast p3, Lcom/google/googlenav/ui/view/dialog/aC;

    invoke-static {p3}, Lcom/google/googlenav/ui/view/dialog/aC;->b(Lcom/google/googlenav/ui/view/dialog/aC;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 579
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 583
    check-cast p2, Lcom/google/googlenav/ui/view/dialog/aC;

    invoke-static {p2}, Lcom/google/googlenav/ui/view/dialog/aC;->b(Lcom/google/googlenav/ui/view/dialog/aC;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method
