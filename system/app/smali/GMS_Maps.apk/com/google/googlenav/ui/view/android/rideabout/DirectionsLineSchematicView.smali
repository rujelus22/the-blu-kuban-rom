.class public Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;
.super Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;
.source "SourceFile"


# instance fields
.field private final b:Lax/w;

.field private final c:Lbi/d;

.field private final d:Lcom/google/googlenav/ui/view/android/rideabout/r;

.field private final e:Lcom/google/googlenav/ui/view/android/rideabout/q;

.field private final f:Lcom/google/googlenav/ui/e;

.field private final g:Lcom/google/googlenav/ui/m;

.field private final h:Lbi/v;

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lax/w;Lbi/d;Lcom/google/googlenav/ui/e;Lcom/google/googlenav/ui/m;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;-><init>(Landroid/content/Context;)V

    .line 68
    iput-object p2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    .line 69
    iput-object p3, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    .line 70
    iput-object p4, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->f:Lcom/google/googlenav/ui/e;

    .line 71
    iput-object p5, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    .line 72
    iput-boolean p6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->i:Z

    .line 73
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-direct {v0, p1}, Lcom/google/googlenav/ui/view/android/rideabout/r;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    .line 74
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/q;

    const v1, 0x7f0f00de

    const v2, 0x7f0f00dc

    invoke-direct {v0, p1, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/q;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    .line 76
    new-instance v0, Lbi/v;

    invoke-direct {v0, p3}, Lbi/v;-><init>(Lbi/d;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->h:Lbi/v;

    .line 77
    invoke-direct {p0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d()V

    .line 78
    return-void
.end method

.method private a(Landroid/view/ViewGroup;I)Landroid/widget/LinearLayout;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 360
    invoke-static {p1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/ViewGroup;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04015b

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 361
    const v1, 0x7f1003c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 362
    invoke-direct {p0, v0, p2}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/View;I)V

    .line 363
    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/widget/LinearLayout;
    .registers 4
    .parameter

    .prologue
    .line 342
    const-string v0, ""

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/widget/LinearLayout;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 346
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/widget/LinearLayout;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/widget/LinearLayout;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 351
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 352
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 353
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {v1, p1, p2}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 354
    invoke-virtual {p0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v3, v3, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 356
    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;)Lcom/google/googlenav/ui/e;
    .registers 2
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->f:Lcom/google/googlenav/ui/e;

    return-object v0
.end method

.method private a(ILjava/util/TreeMap;)Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {v0, p1}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->m()Lax/t;

    move-result-object v1

    .line 298
    if-eqz p2, :cond_1e

    .line 299
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p2}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v2, v3, v1}, Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;-><init>(Landroid/content/Context;Ljava/util/List;Lax/t;)V

    .line 302
    :goto_1d
    return-object v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private a(Lbi/t;Lbi/r;)Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 210
    new-instance v7, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p2}, Lbi/r;->d()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {v7, v0, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;-><init>(Landroid/content/Context;IZ)V

    .line 211
    invoke-virtual {p0, v7}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/widget/TableRow;)V

    .line 212
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/r;->I()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v5

    .line 213
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/r;->J()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v4

    .line 214
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentStation;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    sget-object v3, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/RouteSegmentStation;-><init>(Landroid/content/Context;Lcom/google/googlenav/ui/view/android/rideabout/m;Lcom/google/googlenav/ui/view/android/rideabout/m;II)V

    .line 216
    invoke-virtual {p2}, Lbi/r;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v8

    .line 217
    invoke-virtual {p2}, Lbi/r;->E()Z

    move-result v1

    if-eqz v1, :cond_97

    .line 220
    invoke-virtual {p2}, Lbi/r;->d()I

    move-result v1

    invoke-direct {p0, v8, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/ViewGroup;I)Landroid/widget/LinearLayout;

    move-result-object v9

    .line 221
    invoke-virtual {p2}, Lbi/r;->f()Ljava/lang/String;

    move-result-object v2

    .line 222
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 223
    invoke-virtual {p2}, Lbi/r;->g()Ljava/lang/String;

    move-result-object v2

    .line 226
    :cond_52
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p2}, Lbi/r;->L()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v10

    .line 227
    invoke-virtual {p2}, Lbi/r;->K()Ljava/lang/String;

    move-result-object v11

    .line 228
    invoke-static {v11}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9b

    .line 229
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p2}, Lbi/r;->h()J

    move-result-wide v3

    invoke-virtual {p2}, Lbi/r;->F()Z

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    invoke-static/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/google/googlenav/ui/m;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v1

    .line 231
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v3, 0x494

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v10, v4, v1

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-static {v5, v11}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 242
    :goto_94
    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 244
    :cond_97
    invoke-virtual {v7, v0, v8}, Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;->setContent(Lcom/google/googlenav/ui/view/android/rideabout/RouteSegment;Landroid/widget/LinearLayout;)V

    .line 245
    return-object v7

    .line 236
    :cond_9b
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p2}, Lbi/r;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lbi/r;->h()J

    move-result-wide v3

    invoke-virtual {p2}, Lbi/r;->F()Z

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    invoke-static/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/google/googlenav/ui/m;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v1

    .line 239
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v3, 0x493

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v10, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v1

    goto :goto_94
.end method

.method private a(Lbi/v;)Ljava/util/TreeMap;
    .registers 7
    .parameter

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    invoke-virtual {p1}, Lbi/v;->d()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 138
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    .line 140
    :cond_c
    invoke-virtual {p1}, Lbi/v;->a()Lbi/t;

    move-result-object v2

    .line 141
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    invoke-virtual {v0, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lbi/h;->b()Lbi/q;

    move-result-object v3

    sget-object v4, Lbi/q;->c:Lbi/q;

    if-ne v3, v4, :cond_27

    .line 143
    check-cast v0, Lbi/r;

    invoke-direct {p0, v2, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/t;Lbi/r;)Lcom/google/googlenav/ui/view/android/rideabout/RouteTableRow;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    :cond_27
    invoke-virtual {p1}, Lbi/v;->d()Z

    move-result v0

    if-nez v0, :cond_c

    move-object v0, v1

    .line 148
    :cond_2e
    return-object v0
.end method

.method private a(Landroid/view/View;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 367
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/b;

    invoke-direct {v0, p0, p2}, Lcom/google/googlenav/ui/view/android/rideabout/b;-><init>(Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iget-boolean v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->i:Z

    if-nez v0, :cond_14

    .line 375
    new-instance v0, Lcom/google/googlenav/ui/view/android/rideabout/c;

    invoke-direct {v0, p0, p2}, Lcom/google/googlenav/ui/view/android/rideabout/c;-><init>(Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 382
    :cond_14
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 383
    return-void
.end method

.method private a(Lbi/j;Landroid/widget/LinearLayout;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 186
    invoke-virtual {p1}, Lbi/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-virtual {p1}, Lbi/j;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 188
    invoke-virtual {p1}, Lbi/j;->g()Ljava/lang/String;

    move-result-object v1

    .line 192
    :cond_15
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lbi/j;->L()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v6

    .line 193
    invoke-virtual {p1}, Lbi/j;->K()Ljava/lang/String;

    move-result-object v7

    .line 194
    invoke-static {v7}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_58

    .line 195
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lbi/j;->h()J

    move-result-wide v2

    invoke-virtual {p1}, Lbi/j;->F()Z

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/google/googlenav/ui/m;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v2, 0x48d

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    aput-object v0, v3, v8

    aput-object v6, v3, v9

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-static {v0, v7}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v0

    aput-object v0, v3, v10

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 207
    :goto_57
    return-void

    .line 201
    :cond_58
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lbi/j;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbi/j;->h()J

    move-result-wide v2

    invoke-virtual {p1}, Lbi/j;->F()Z

    move-result v4

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    invoke-static/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/google/googlenav/ui/m;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v2, 0x48c

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v10, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    aput-object v0, v3, v8

    aput-object v6, v3, v9

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_57
.end method

.method private a(Lbi/k;Lbi/t;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 336
    invoke-virtual {p1}, Lbi/k;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 337
    invoke-virtual {p1}, Lbi/k;->d()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/View;I)V

    .line 338
    invoke-virtual {p1}, Lbi/k;->d()I

    move-result v1

    invoke-super {p0, v1, v0, p2}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->b(ILandroid/widget/LinearLayout;Lbi/t;)V

    .line 339
    return-void
.end method

.method private a(Lbi/l;Lbi/t;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 152
    invoke-virtual {p1}, Lbi/l;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 153
    invoke-virtual {p1}, Lbi/l;->d()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/ViewGroup;I)Landroid/widget/LinearLayout;

    move-result-object v1

    .line 154
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v3, 0x4e9

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lbi/l;->w()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 156
    new-instance v1, Lcom/google/googlenav/ui/view/android/rideabout/WalkingDirectionsView;

    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {p1}, Lbi/l;->d()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/googlenav/ui/view/android/rideabout/WalkingDirectionsView;-><init>(Landroid/content/Context;Lax/w;I)V

    .line 158
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 159
    invoke-virtual {p1}, Lbi/l;->d()I

    move-result v1

    invoke-super {p0, v1, v0, p2}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(ILandroid/widget/LinearLayout;Lbi/t;)V

    .line 160
    return-void
.end method

.method private a(Lbi/n;ZLbi/t;)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p1}, Lbi/n;->I()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v9

    .line 307
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p1}, Lbi/n;->J()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v10

    .line 308
    invoke-virtual {p1}, Lbi/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbi/n;->j()Ljava/lang/String;

    move-result-object v2

    if-nez p2, :cond_94

    const/4 v0, 0x1

    :goto_1f
    invoke-direct {p0, v1, v2, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/widget/LinearLayout;

    move-result-object v7

    .line 310
    invoke-virtual {p1}, Lbi/n;->d()I

    move-result v0

    invoke-direct {p0, v7, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/ViewGroup;I)Landroid/widget/LinearLayout;

    move-result-object v11

    .line 312
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {v0}, Lax/w;->aW()Z

    move-result v0

    if-eqz v0, :cond_96

    invoke-virtual {p1}, Lbi/n;->P()Z

    move-result v0

    if-eqz v0, :cond_96

    const/4 v5, 0x1

    .line 313
    :goto_3a
    const/4 v6, 0x0

    .line 314
    if-eqz p2, :cond_98

    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->a:Lcom/google/googlenav/ui/view/android/rideabout/m;

    move-object v8, v0

    .line 315
    :goto_40
    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    if-ne v8, v0, :cond_9c

    .line 316
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v1, 0x4a1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lbi/n;->w()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 319
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p1}, Lbi/n;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbi/n;->l()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 322
    new-instance v6, Lcom/google/googlenav/ui/view/android/rideabout/WalkingDirectionsView;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {p1}, Lbi/n;->d()I

    move-result v2

    invoke-direct {v6, v0, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/WalkingDirectionsView;-><init>(Landroid/content/Context;Lax/w;I)V

    .line 324
    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 331
    :goto_86
    invoke-virtual {p1}, Lbi/n;->d()I

    move-result v2

    move-object v0, p0

    move-object v1, v8

    move v3, p2

    move v4, v10

    move v5, v9

    move-object v8, p3

    invoke-super/range {v0 .. v8}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/m;IZIILcom/google/googlenav/ui/view/android/rideabout/WalkingDirectionsView;Landroid/widget/LinearLayout;Lbi/t;)V

    .line 333
    return-void

    .line 308
    :cond_94
    const/4 v0, 0x0

    goto :goto_1f

    .line 312
    :cond_96
    const/4 v5, 0x0

    goto :goto_3a

    .line 314
    :cond_98
    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    move-object v8, v0

    goto :goto_40

    .line 327
    :cond_9c
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v1, 0x4a0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 328
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p1}, Lbi/n;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbi/n;->l()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_86
.end method

.method private a(Lcom/google/googlenav/ui/view/android/rideabout/m;Lbi/j;Ljava/util/TreeMap;Lbi/t;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 164
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/j;->I()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v7

    .line 165
    invoke-virtual {p2}, Lbi/j;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lbi/j;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v6

    .line 166
    invoke-virtual {p2}, Lbi/j;->d()I

    move-result v0

    invoke-direct {p0, v6, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/ViewGroup;I)Landroid/widget/LinearLayout;

    move-result-object v8

    .line 167
    invoke-virtual {p2}, Lbi/j;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_38

    .line 168
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/j;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 170
    :cond_38
    invoke-direct {p0, p2, v8}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/j;Landroid/widget/LinearLayout;)V

    .line 171
    invoke-virtual {p2}, Lbi/j;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_60

    .line 172
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    const/16 v3, 0x4a7

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/String;

    invoke-virtual {p2}, Lbi/j;->H()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v2

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/view/android/rideabout/r;->b(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 176
    :cond_60
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/j;->D()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lbi/j;->k()Ljava/lang/String;

    move-result-object v4

    iget-object v9, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {v9}, Lax/w;->aW()Z

    move-result v9

    if-eqz v9, :cond_95

    invoke-virtual {p2}, Lbi/j;->O()Z

    move-result v9

    if-eqz v9, :cond_95

    :goto_78
    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 179
    invoke-virtual {p2}, Lbi/j;->d()I

    move-result v2

    invoke-virtual {p2}, Lbi/j;->d()I

    move-result v0

    invoke-direct {p0, v0, p3}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(ILjava/util/TreeMap;)Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move v5, v7

    move-object v7, p4

    invoke-super/range {v0 .. v7}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/m;ILcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;ILandroid/widget/LinearLayout;Lbi/t;)V

    .line 182
    return-void

    :cond_95
    move v5, v2

    .line 176
    goto :goto_78
.end method

.method private a(Lcom/google/googlenav/ui/view/android/rideabout/m;Lbi/n;Lbi/j;Ljava/util/TreeMap;Lbi/t;Lbi/t;)V
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/n;->I()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v9

    .line 253
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual/range {p3 .. p3}, Lbi/j;->I()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(I)I

    move-result v7

    .line 255
    invoke-virtual {p2}, Lbi/n;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Ljava/lang/String;)Landroid/widget/LinearLayout;

    move-result-object v8

    .line 256
    invoke-virtual/range {p3 .. p3}, Lbi/j;->d()I

    move-result v1

    invoke-direct {p0, v8, v1}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Landroid/view/ViewGroup;I)Landroid/widget/LinearLayout;

    move-result-object v10

    .line 257
    invoke-virtual/range {p3 .. p3}, Lbi/j;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3b

    .line 258
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual/range {p3 .. p3}, Lbi/j;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 260
    :cond_3b
    invoke-virtual/range {p3 .. p3}, Lbi/j;->f()Ljava/lang/String;

    move-result-object v2

    .line 261
    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_49

    .line 262
    invoke-virtual/range {p3 .. p3}, Lbi/j;->g()Ljava/lang/String;

    move-result-object v2

    .line 264
    :cond_49
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual/range {p3 .. p3}, Lbi/j;->L()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v11

    .line 266
    invoke-virtual/range {p3 .. p3}, Lbi/j;->K()Ljava/lang/String;

    move-result-object v12

    .line 267
    invoke-static {v12}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10b

    .line 268
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual/range {p3 .. p3}, Lbi/j;->h()J

    move-result-wide v3

    invoke-virtual/range {p3 .. p3}, Lbi/j;->F()Z

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    invoke-static/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/google/googlenav/ui/m;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v1

    .line 270
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v3, 0x4d8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    new-array v4, v4, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v11, v4, v1

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-static {v5, v12}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 279
    :goto_8e
    invoke-virtual/range {p3 .. p3}, Lbi/j;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c2

    .line 280
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x4a7

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lbi/j;->H()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/view/android/rideabout/r;->b(Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 285
    :cond_c2
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->d:Lcom/google/googlenav/ui/view/android/rideabout/r;

    invoke-virtual {p2}, Lbi/n;->E()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lbi/n;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lbi/j;->D()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p3 .. p3}, Lbi/j;->k()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {v6}, Lax/w;->aW()Z

    move-result v6

    if-eqz v6, :cond_139

    invoke-virtual {p2}, Lbi/n;->P()Z

    move-result v6

    if-nez v6, :cond_e8

    invoke-virtual/range {p3 .. p3}, Lbi/j;->O()Z

    move-result v6

    if-eqz v6, :cond_139

    :cond_e8
    const/4 v6, 0x1

    :goto_e9
    invoke-virtual/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 290
    invoke-virtual/range {p3 .. p3}, Lbi/j;->d()I

    move-result v3

    invoke-virtual/range {p3 .. p3}, Lbi/j;->d()I

    move-result v1

    move-object/from16 v0, p4

    invoke-direct {p0, v1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(ILjava/util/TreeMap;)Lcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v5, p4

    move v6, v9

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    invoke-super/range {v1 .. v10}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/m;ILcom/google/googlenav/ui/view/android/rideabout/ExpandIntermediateStopCommandView;Ljava/util/TreeMap;IILandroid/widget/LinearLayout;Lbi/t;Lbi/t;)V

    .line 293
    return-void

    .line 274
    :cond_10b
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a:Landroid/content/Context;

    invoke-virtual/range {p3 .. p3}, Lbi/j;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lbi/j;->h()J

    move-result-wide v3

    invoke-virtual/range {p3 .. p3}, Lbi/j;->F()Z

    move-result v5

    iget-object v6, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->g:Lcom/google/googlenav/ui/m;

    invoke-static/range {v1 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/p;->a(Landroid/content/Context;Ljava/lang/String;JZLcom/google/googlenav/ui/m;)Lcom/google/googlenav/ui/view/android/rideabout/n;

    move-result-object v1

    .line 276
    iget-object v2, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->e:Lcom/google/googlenav/ui/view/android/rideabout/q;

    const/16 v3, 0x4d7

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/googlenav/ui/view/android/rideabout/n;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v11, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/googlenav/ui/view/android/rideabout/q;->a(Ljava/lang/String;[Lcom/google/googlenav/ui/view/android/rideabout/n;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_8e

    .line 285
    :cond_139
    const/4 v6, 0x0

    goto :goto_e9
.end method

.method private d()V
    .registers 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 81
    new-instance v10, Lbi/v;

    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    invoke-direct {v10, v0}, Lbi/v;-><init>(Lbi/d;)V

    .line 82
    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->a:Lcom/google/googlenav/ui/view/android/rideabout/m;

    .line 83
    :cond_b
    :goto_b
    invoke-virtual {v10}, Lbi/v;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_83

    .line 84
    invoke-virtual {v10}, Lbi/v;->a()Lbi/t;

    move-result-object v5

    .line 85
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    invoke-virtual {v0, v5}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v7

    .line 87
    sget-object v0, Lcom/google/googlenav/ui/view/android/rideabout/d;->a:[I

    invoke-virtual {v7}, Lbi/h;->b()Lbi/q;

    move-result-object v2

    invoke-virtual {v2}, Lbi/q;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_86

    goto :goto_b

    :pswitch_2b
    move-object v0, v7

    .line 89
    check-cast v0, Lbi/l;

    invoke-direct {p0, v0, v5}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/l;Lbi/t;)V

    .line 90
    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_b

    .line 93
    :pswitch_34
    check-cast v7, Lbi/j;

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/v;)Ljava/util/TreeMap;

    move-result-object v0

    invoke-direct {p0, v1, v7, v0, v5}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/m;Lbi/j;Ljava/util/TreeMap;Lbi/t;)V

    .line 95
    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_b

    .line 101
    :pswitch_40
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    invoke-virtual {v0, v5}, Lbi/d;->f(Lbi/t;)Z

    move-result v0

    if-nez v0, :cond_84

    .line 102
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    invoke-virtual {v0, v5}, Lbi/d;->c(Lbi/t;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 105
    invoke-virtual {v10}, Lbi/v;->a()Lbi/t;

    move-result-object v6

    .line 107
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->c:Lbi/d;

    invoke-virtual {v0, v6}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v3

    move-object v2, v7

    .line 108
    check-cast v2, Lbi/n;

    check-cast v3, Lbi/j;

    invoke-direct {p0, v10}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/v;)Ljava/util/TreeMap;

    move-result-object v4

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lcom/google/googlenav/ui/view/android/rideabout/m;Lbi/n;Lbi/j;Ljava/util/TreeMap;Lbi/t;Lbi/t;)V

    .line 111
    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->b:Lcom/google/googlenav/ui/view/android/rideabout/m;

    move v0, v8

    .line 114
    :goto_6a
    if-nez v0, :cond_b

    .line 115
    check-cast v7, Lbi/n;

    invoke-virtual {v10}, Lbi/v;->hasNext()Z

    move-result v0

    if-nez v0, :cond_7b

    move v0, v8

    :goto_75
    invoke-direct {p0, v7, v0, v5}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/n;ZLbi/t;)V

    .line 116
    sget-object v1, Lcom/google/googlenav/ui/view/android/rideabout/m;->c:Lcom/google/googlenav/ui/view/android/rideabout/m;

    goto :goto_b

    :cond_7b
    move v0, v9

    .line 115
    goto :goto_75

    .line 120
    :pswitch_7d
    check-cast v7, Lbi/k;

    invoke-direct {p0, v7, v5}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(Lbi/k;Lbi/t;)V

    goto :goto_b

    .line 124
    :cond_83
    return-void

    :cond_84
    move v0, v9

    goto :goto_6a

    .line 87
    :pswitch_data_86
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_34
        :pswitch_40
        :pswitch_7d
    .end packed-switch
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->b:Lax/w;

    invoke-virtual {v0}, Lax/w;->aU()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->a(I)I

    move-result v0

    return v0
.end method

.method public a(Lbi/a;)I
    .registers 5
    .parameter

    .prologue
    .line 393
    const/4 v0, 0x0

    .line 394
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->h:Lbi/v;

    iget-object v2, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {v1, v2}, Lbi/v;->a(Lbi/t;)Lbi/v;

    .line 395
    iget-object v1, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->h:Lbi/v;

    invoke-virtual {v1}, Lbi/v;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 396
    iget-object v0, p0, Lcom/google/googlenav/ui/view/android/rideabout/DirectionsLineSchematicView;->h:Lbi/v;

    invoke-virtual {v0}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    .line 398
    :cond_16
    invoke-super {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/rideabout/LineSchematicView;->a(Lbi/a;Lbi/t;)I

    move-result v0

    return v0
.end method
