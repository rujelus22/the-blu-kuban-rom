.class public Lcom/google/googlenav/ui/wizard/ew;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/view/dialog/bn;


# instance fields
.field private a:Lcom/google/googlenav/ui/view/dialog/bl;

.field private b:Lcom/google/googlenav/ui/view/android/aL;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;Lcom/google/googlenav/ui/view/dialog/bl;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 27
    iput-object p2, p0, Lcom/google/googlenav/ui/wizard/ew;->a:Lcom/google/googlenav/ui/view/dialog/bl;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/android/aL;)V
    .registers 2
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    .line 32
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->j()V

    .line 33
    return-void
.end method

.method public a(Lcom/google/googlenav/ui/view/dialog/bk;)V
    .registers 3
    .parameter

    .prologue
    .line 61
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    .line 62
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    .line 63
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->a()V

    .line 64
    if-eqz v0, :cond_11

    .line 65
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 67
    :cond_11
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    packed-switch p1, :pswitch_data_a

    .line 55
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 52
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->h()V

    .line 53
    const/4 v0, 0x1

    goto :goto_4

    .line 50
    :pswitch_data_a
    .packed-switch 0x3f9
        :pswitch_5
    .end packed-switch
.end method

.method protected b()V
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->a:Lcom/google/googlenav/ui/view/dialog/bl;

    invoke-interface {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bl;->a(Lcom/google/googlenav/ui/view/dialog/bn;)Lcom/google/googlenav/ui/view/dialog/bk;

    move-result-object v0

    .line 38
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 39
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bk;->n()V

    .line 40
    return-void
.end method

.method public b(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 72
    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->e()V

    .line 73
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    .line 74
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/ew;->a()V

    .line 75
    if-eqz v0, :cond_11

    .line 76
    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 78
    :cond_11
    const/4 v0, 0x1

    return v0
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/ew;->b:Lcom/google/googlenav/ui/view/android/aL;

    .line 45
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 46
    return-void
.end method

.method public c(Lcom/google/googlenav/ui/view/dialog/bk;)Z
    .registers 3
    .parameter

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method
