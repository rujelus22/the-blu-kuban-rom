.class public Lcom/google/googlenav/ui/wizard/hJ;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/hH;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hH;)V
    .registers 3
    .parameter

    .prologue
    .line 793
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    .line 794
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_13

    const v0, 0x7f0f001b

    :goto_f
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 796
    return-void

    .line 794
    :cond_13
    invoke-static {}, Lcom/google/googlenav/ui/wizard/hJ;->p()I

    move-result v0

    goto :goto_f
.end method


# virtual methods
.method public P_()Z
    .registers 2

    .prologue
    .line 806
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected c()Landroid/view/View;
    .registers 15

    .prologue
    .line 819
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hJ;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_242

    const v0, 0x7f04015d

    :goto_11
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 823
    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 824
    const v2, 0x7f100198

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 825
    const v3, 0x7f100233

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 826
    const v3, 0x7f1003ca

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 827
    const v4, 0x7f100234

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 828
    const v4, 0x7f1003cb

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 830
    const v5, 0x7f1003cc

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 831
    const v6, 0x7f1003cd

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 832
    const v7, 0x7f100235

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 833
    const v7, 0x7f1003ce

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 834
    const v8, 0x7f1003cf

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 836
    iget-object v8, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v8}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v8

    if-eqz v8, :cond_247

    const/16 v8, 0x443

    .line 839
    :goto_7d
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v13

    if-eqz v13, :cond_8d

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/googlenav/K;->an()Z

    move-result v13

    if-eqz v13, :cond_24b

    .line 840
    :cond_8d
    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v8

    sget-object v13, Lcom/google/googlenav/ui/aV;->aN:Lcom/google/googlenav/ui/aV;

    invoke-static {v8, v13}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 847
    :goto_9a
    const/4 v1, 0x3

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setLines(I)V

    .line 848
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 849
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v1

    if-eqz v1, :cond_25d

    const/16 v1, 0x43e

    .line 852
    :goto_b1
    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 854
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/4 v8, 0x4

    if-ne v1, v8, :cond_269

    .line 855
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 856
    const/16 v1, 0x44f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v9, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v9}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 859
    const/16 v1, 0xa

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 860
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    if-eqz v1, :cond_261

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    invoke-virtual {v1}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    .line 863
    :goto_ec
    sget-object v9, Lcom/google/googlenav/ui/aV;->bt:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v9}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 865
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 867
    new-instance v1, Lcom/google/googlenav/ui/wizard/hK;

    invoke-direct {v1, p0, v2}, Lcom/google/googlenav/ui/wizard/hK;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/EditText;)V

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 876
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    if-nez v1, :cond_114

    .line 877
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v3, Lcom/google/googlenav/ui/d;

    const/4 v9, 0x0

    invoke-direct {v3, v9}, Lcom/google/googlenav/ui/d;-><init>(Z)V

    iput-object v3, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    .line 879
    :cond_114
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->m:Lcom/google/googlenav/ui/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v1

    invoke-virtual {v5, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 880
    new-instance v1, Lcom/google/googlenav/ui/wizard/hL;

    invoke-direct {v1, p0, v5}, Lcom/google/googlenav/ui/wizard/hL;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 888
    const/16 v1, 0x448

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 891
    invoke-virtual {v8}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 892
    const/16 v1, 0x451

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->Y:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 895
    const/16 v1, 0x20

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 896
    const/16 v1, 0x452

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->Z:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 899
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 900
    new-instance v1, Lcom/google/googlenav/ui/wizard/hM;

    invoke-direct {v1, p0}, Lcom/google/googlenav/ui/wizard/hM;-><init>(Lcom/google/googlenav/ui/wizard/hJ;)V

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 916
    :goto_169
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v1}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v1

    if-eqz v1, :cond_295

    .line 918
    const v1, 0x7f1003d1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 919
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    if-nez v3, :cond_18e

    .line 920
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v4, Lcom/google/googlenav/ui/d;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/google/googlenav/ui/d;-><init>(Z)V

    iput-object v4, v3, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    .line 922
    :cond_18e
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 925
    const v3, 0x7f1003d2

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 926
    invoke-static {}, Lcom/google/googlenav/ui/wizard/hH;->z()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 929
    const v3, 0x7f1003d4

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 931
    const/16 v4, 0x44a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/googlenav/ui/aV;->Y:Lcom/google/googlenav/ui/aV;

    invoke-static {v4, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 934
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_28e

    .line 935
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 941
    :goto_1d3
    const v4, 0x7f1003d0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 942
    new-instance v5, Lcom/google/googlenav/ui/wizard/hN;

    invoke-direct {v5, p0, v1, v3}, Lcom/google/googlenav/ui/wizard/hN;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/CheckBox;Landroid/widget/TextView;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 962
    :goto_1e4
    const v1, 0x7f10002e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 963
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 966
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_2bb

    const v1, 0x7f100031

    .line 967
    :goto_1fa
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v3

    if-eqz v3, :cond_2c0

    const v3, 0x7f100030

    .line 969
    :goto_203
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 970
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 971
    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-static {v4}, Lcom/google/googlenav/ui/wizard/hH;->a(Lcom/google/googlenav/ui/wizard/hH;)Z

    move-result v4

    if-eqz v4, :cond_2c5

    .line 972
    const/16 v4, 0x454

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 976
    :goto_21e
    new-instance v4, Lcom/google/googlenav/ui/wizard/hO;

    invoke-direct {v4, p0, v2}, Lcom/google/googlenav/ui/wizard/hO;-><init>(Lcom/google/googlenav/ui/wizard/hJ;Landroid/widget/EditText;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 985
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 986
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 987
    const/16 v2, 0x6a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 988
    new-instance v2, Lcom/google/googlenav/ui/wizard/hP;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hP;-><init>(Lcom/google/googlenav/ui/wizard/hJ;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 995
    return-object v0

    .line 819
    :cond_242
    const v0, 0x7f04015c

    goto/16 :goto_11

    .line 836
    :cond_247
    const/16 v8, 0x442

    goto/16 :goto_7d

    .line 843
    :cond_24b
    iget-object v13, p0, Lcom/google/googlenav/ui/wizard/hJ;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v13, v13, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-static {v8}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v13, v8}, Lcom/google/googlenav/ui/view/android/aL;->setTitle(Ljava/lang/CharSequence;)V

    .line 844
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_9a

    .line 849
    :cond_25d
    const/16 v1, 0x43d

    goto/16 :goto_b1

    .line 860
    :cond_261
    const/16 v1, 0x44e

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_ec

    .line 907
    :cond_269
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/view/View;->setVisibility(I)V

    .line 908
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 909
    const/16 v1, 0x8

    invoke-virtual {v10, v1}, Landroid/view/View;->setVisibility(I)V

    .line 910
    const/16 v1, 0x8

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 911
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/view/View;->setVisibility(I)V

    .line 912
    const/16 v1, 0x8

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 913
    const/16 v1, 0x8

    invoke-virtual {v12, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_169

    .line 937
    :cond_28e
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1d3

    .line 957
    :cond_295
    const v1, 0x7f1003d0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 958
    const v1, 0x7f1003d3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 959
    const v1, 0x7f1003d4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1e4

    .line 966
    :cond_2bb
    const v1, 0x7f100030

    goto/16 :goto_1fa

    .line 967
    :cond_2c0
    const v3, 0x7f100031

    goto/16 :goto_203

    .line 974
    :cond_2c5
    const/16 v4, 0xdd

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_21e
.end method

.method protected d()V
    .registers 3

    .prologue
    .line 811
    invoke-super {p0}, Lcom/google/googlenav/ui/view/android/S;->d()V

    .line 814
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hJ;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 815
    return-void
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 800
    const/4 v0, 0x0

    return v0
.end method
