.class public Lcom/google/googlenav/ui/wizard/gG;
.super Lcom/google/googlenav/ui/wizard/C;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/googlenav/ui/wizard/gJ;

.field private b:Lcom/google/googlenav/ui/wizard/gK;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/wizard/jv;)V
    .registers 3
    .parameter

    .prologue
    .line 239
    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/C;-><init>(Lcom/google/googlenav/ui/wizard/jv;)V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    .line 240
    return-void
.end method

.method private a(Lcom/google/googlenav/ui/wizard/gI;)Lcom/google/googlenav/ui/wizard/gO;
    .registers 3
    .parameter

    .prologue
    .line 282
    new-instance v0, Lcom/google/googlenav/ui/wizard/gH;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/ui/wizard/gH;-><init>(Lcom/google/googlenav/ui/wizard/gG;Lcom/google/googlenav/ui/wizard/gI;)V

    .line 300
    return-object v0
.end method


# virtual methods
.method public a(Lat/a;)I
    .registers 4
    .parameter

    .prologue
    .line 336
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_e

    .line 337
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->h()V

    .line 338
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    .line 340
    :goto_d
    return v0

    :cond_e
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    goto :goto_d
.end method

.method public a(Lat/b;)I
    .registers 3
    .parameter

    .prologue
    .line 345
    iget v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    return v0
.end method

.method protected a(Lcom/google/googlenav/ui/wizard/gJ;)V
    .registers 3
    .parameter

    .prologue
    .line 248
    const-string v0, "o"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->g(Ljava/lang/String;)V

    .line 249
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    .line 250
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->j()V

    .line 251
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    .line 256
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/googlenav/ui/wizard/gG;->g:I

    .line 259
    new-instance v0, Lcom/google/googlenav/ui/wizard/gK;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gJ;->b()LaB/s;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gJ;->e()Lcom/google/googlenav/ui/wizard/gI;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/googlenav/ui/wizard/gG;->a(Lcom/google/googlenav/ui/wizard/gI;)Lcom/google/googlenav/ui/wizard/gO;

    move-result-object v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/gJ;->c()Z

    move-result v3

    iget-object v4, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v4}, Lcom/google/googlenav/ui/wizard/gJ;->d()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/googlenav/ui/wizard/gK;-><init>(LaB/s;Lcom/google/googlenav/ui/wizard/gO;ZZ)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    .line 264
    new-instance v0, Lcom/google/googlenav/ui/wizard/gU;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gJ;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gJ;->h()I

    move-result v2

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/gG;->d:Lcom/google/googlenav/ui/wizard/jv;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/jv;->G()LaB/s;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/googlenav/ui/wizard/gU;-><init>(Ljava/lang/String;ILaB/s;Lcom/google/googlenav/ui/e;)V

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    .line 268
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/gU;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gK;->a()Lcom/google/googlenav/ui/wizard/gM;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gU;->a(Lcom/google/googlenav/ui/wizard/gM;)V

    .line 271
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/wizard/gU;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gU;)V

    .line 274
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/aL;->show()V

    .line 277
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/gJ;->f()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/wizard/gJ;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/wizard/gK;->a(Ljava/util/List;Ljava/util/List;)V

    .line 278
    return-void
.end method

.method protected c()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/gK;->a(Lcom/google/googlenav/ui/wizard/gU;)V

    .line 306
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->b:Lcom/google/googlenav/ui/wizard/gK;

    .line 309
    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    .line 312
    invoke-super {p0}, Lcom/google/googlenav/ui/wizard/C;->c()V

    .line 313
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    .line 318
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    .line 319
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->a()V

    .line 321
    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    .line 322
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->j()V

    .line 323
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gJ;->e()Lcom/google/googlenav/ui/wizard/gI;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 328
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gG;->a:Lcom/google/googlenav/ui/wizard/gJ;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/gJ;->e()Lcom/google/googlenav/ui/wizard/gI;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/ui/wizard/gI;->a()V

    .line 330
    :cond_11
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/gG;->a()V

    .line 331
    const-string v0, "d"

    invoke-static {v0}, Lcom/google/googlenav/friend/ad;->g(Ljava/lang/String;)V

    .line 332
    return-void
.end method
