.class Lcom/google/googlenav/ui/wizard/gw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/ui/wizard/dy;


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/gk;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/gk;)V
    .registers 2
    .parameter

    .prologue
    .line 919
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public S_()Z
    .registers 2

    .prologue
    .line 942
    const/4 v0, 0x1

    return v0
.end method

.method public T_()V
    .registers 1

    .prologue
    .line 948
    return-void
.end method

.method public a(Lax/y;Ljava/lang/Long;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lcom/google/googlenav/aZ;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 923
    if-eqz p1, :cond_30

    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->d(Lcom/google/googlenav/ui/wizard/gk;)LaW/g;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 924
    invoke-virtual {p1}, Lax/y;->q()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 925
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->e(Lcom/google/googlenav/ui/wizard/gk;)V

    .line 926
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->d(Lcom/google/googlenav/ui/wizard/gk;)LaW/g;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v2}, Lcom/google/googlenav/ui/wizard/gk;->f(Lcom/google/googlenav/ui/wizard/gk;)LaH/m;

    move-result-object v2

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    invoke-virtual {v0, v1, v3, v2}, LaW/g;->a(Ljava/lang/String;ZZ)V

    .line 927
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->g(Lcom/google/googlenav/ui/wizard/gk;)V

    .line 937
    :cond_30
    :goto_30
    return-void

    .line 928
    :cond_31
    invoke-virtual {p1}, Lax/y;->f()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_30

    .line 929
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->e(Lcom/google/googlenav/ui/wizard/gk;)V

    .line 930
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->d(Lcom/google/googlenav/ui/wizard/gk;)LaW/g;

    move-result-object v0

    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v3}, LaW/g;->a(Ljava/lang/String;ZZ)V

    .line 931
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v0}, Lcom/google/googlenav/ui/wizard/gk;->h(Lcom/google/googlenav/ui/wizard/gk;)LaN/u;

    move-result-object v0

    invoke-virtual {p1}, Lax/y;->m()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Lax/y;->n()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v3

    invoke-virtual {v0, v1, v2, v4, v3}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v0

    .line 933
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-virtual {p1}, Lax/y;->h()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LaN/H;

    invoke-virtual {p1}, Lax/y;->f()LaN/B;

    move-result-object v4

    iget-object v5, p0, Lcom/google/googlenav/ui/wizard/gw;->a:Lcom/google/googlenav/ui/wizard/gk;

    invoke-static {v5}, Lcom/google/googlenav/ui/wizard/gk;->h(Lcom/google/googlenav/ui/wizard/gk;)LaN/u;

    move-result-object v5

    invoke-virtual {v5}, LaN/u;->f()LaN/H;

    move-result-object v5

    invoke-virtual {v5}, LaN/H;->c()I

    move-result v5

    invoke-direct {v3, v4, v0, v5}, LaN/H;-><init>(LaN/B;LaN/Y;I)V

    invoke-static {v1, v2, v3, p4}, Lcom/google/googlenav/ui/wizard/gk;->a(Lcom/google/googlenav/ui/wizard/gk;Ljava/lang/String;LaN/H;Lcom/google/googlenav/aZ;)V

    goto :goto_30
.end method
