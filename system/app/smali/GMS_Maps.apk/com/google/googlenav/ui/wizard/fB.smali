.class Lcom/google/googlenav/ui/wizard/fb;
.super Lcom/google/googlenav/ui/wizard/eX;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/googlenav/ui/wizard/eP;

.field private final c:I

.field private final d:LaR/a;


# direct methods
.method constructor <init>(Lcom/google/googlenav/ui/wizard/eP;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 815
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/fb;->b:Lcom/google/googlenav/ui/wizard/eP;

    invoke-direct {p0, p1}, Lcom/google/googlenav/ui/wizard/eX;-><init>(Lcom/google/googlenav/ui/wizard/eP;)V

    .line 816
    iput p2, p0, Lcom/google/googlenav/ui/wizard/fb;->c:I

    .line 817
    invoke-static {p1, p2}, Lcom/google/googlenav/ui/wizard/eP;->b(Lcom/google/googlenav/ui/wizard/eP;I)LaR/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/ui/wizard/fb;->d:LaR/a;

    .line 818
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 822
    new-instance v6, Lcom/google/googlenav/ui/view/android/J;

    invoke-direct {v6, p1, v8}, Lcom/google/googlenav/ui/view/android/J;-><init>(Landroid/content/Context;I)V

    .line 824
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fb;->d:LaR/a;

    if-eqz v0, :cond_28

    .line 825
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x2ef

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/googlenav/ui/wizard/fb;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v5, v7, v3, v4}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 831
    :cond_28
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x113

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    iget v3, p0, Lcom/google/googlenav/ui/wizard/fb;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v5, v7, v8, v3}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 836
    iget-object v0, p0, Lcom/google/googlenav/ui/wizard/fb;->d:LaR/a;

    if-eqz v0, :cond_62

    .line 837
    new-instance v0, Lcom/google/googlenav/ui/view/android/a;

    const/16 v1, 0x2de

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/google/googlenav/ui/view/a;

    iget v3, p0, Lcom/google/googlenav/ui/wizard/fb;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v5, v7, v7, v3}, Lcom/google/googlenav/ui/view/a;-><init>(IILjava/lang/Object;)V

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/view/android/a;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lan/f;Lan/f;Lcom/google/googlenav/ui/view/a;)V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/view/android/J;->add(Ljava/lang/Object;)V

    .line 845
    :cond_62
    return-object v6
.end method

.method public w_()Ljava/lang/String;
    .registers 2

    .prologue
    .line 850
    iget v0, p0, Lcom/google/googlenav/ui/wizard/fb;->c:I

    if-nez v0, :cond_b

    .line 851
    const/16 v0, 0x1e0

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 853
    :goto_a
    return-object v0

    :cond_b
    const/16 v0, 0x619

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method
