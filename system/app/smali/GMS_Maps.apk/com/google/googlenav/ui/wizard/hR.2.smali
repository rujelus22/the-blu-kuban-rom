.class public Lcom/google/googlenav/ui/wizard/hR;
.super Lcom/google/googlenav/ui/view/android/S;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/googlenav/ui/wizard/hH;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/wizard/hH;)V
    .registers 3
    .parameter

    .prologue
    .line 1003
    iput-object p1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    .line 1004
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_13

    const v0, 0x7f0f001b

    :goto_f
    invoke-direct {p0, p1, v0}, Lcom/google/googlenav/ui/view/android/S;-><init>(Lcom/google/googlenav/ui/e;I)V

    .line 1006
    return-void

    .line 1004
    :cond_13
    invoke-static {}, Lcom/google/googlenav/ui/wizard/hR;->p()I

    move-result v0

    goto :goto_f
.end method


# virtual methods
.method public P_()Z
    .registers 2

    .prologue
    .line 1016
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected c()Landroid/view/View;
    .registers 10

    .prologue
    const v4, 0x7f100030

    const/16 v3, 0x453

    const/16 v8, 0x8

    const/4 v7, 0x0

    const/16 v6, 0xa

    .line 1021
    invoke-virtual {p0}, Lcom/google/googlenav/ui/wizard/hR;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04015f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1024
    const v1, 0x7f10001e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1025
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->an()Z

    move-result v2

    if-eqz v2, :cond_219

    .line 1026
    :cond_31
    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->aN:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1033
    :goto_3e
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1036
    const/16 v1, 0x478

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1038
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->h:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_70

    .line 1039
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1040
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->h:Ljava/lang/String;

    sget-object v3, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1042
    :cond_70
    const v1, 0x7f1003d5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1043
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1044
    new-instance v3, Lcom/google/googlenav/ui/wizard/hS;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/hS;-><init>(Lcom/google/googlenav/ui/wizard/hR;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1052
    const v1, 0x7f1003d6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1053
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 1054
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/wizard/hH;->e()Z

    move-result v3

    if-eqz v3, :cond_229

    .line 1056
    const/16 v3, 0x459

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1058
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->i:Ljava/lang/String;

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c5

    .line 1059
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1060
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->i:Ljava/lang/String;

    sget-object v5, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1062
    :cond_c5
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1063
    new-instance v3, Lcom/google/googlenav/ui/wizard/hT;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/hT;-><init>(Lcom/google/googlenav/ui/wizard/hR;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1095
    :goto_d0
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 1096
    const/16 v1, 0x43f

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1098
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_108

    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    if-nez v1, :cond_108

    .line 1099
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1100
    const/16 v1, 0x441

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1103
    :cond_108
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_126

    .line 1104
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1105
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->j:Ljava/lang/String;

    sget-object v3, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1107
    :cond_126
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    if-eqz v1, :cond_144

    .line 1108
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1109
    iget-object v1, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v1, v1, Lcom/google/googlenav/ui/wizard/hQ;->l:LaN/B;

    invoke-virtual {v1}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1112
    :cond_144
    const v1, 0x7f100198

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1113
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1114
    new-instance v2, Lcom/google/googlenav/ui/wizard/hV;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hV;-><init>(Lcom/google/googlenav/ui/wizard/hR;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1122
    const v1, 0x7f1003d1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1123
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    if-nez v2, :cond_174

    .line 1124
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    new-instance v3, Lcom/google/googlenav/ui/d;

    invoke-direct {v3, v7}, Lcom/google/googlenav/ui/d;-><init>(Z)V

    iput-object v3, v2, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    .line 1126
    :cond_174
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hQ;->k:Lcom/google/googlenav/ui/d;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/d;->a()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1129
    const v2, 0x7f1003d2

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1130
    invoke-static {}, Lcom/google/googlenav/ui/wizard/hH;->z()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1133
    const v2, 0x7f1003d4

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1134
    const/16 v3, 0x44a

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/googlenav/ui/aV;->Y:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1137
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_283

    .line 1138
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1144
    :goto_1b8
    const v3, 0x7f1003d0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1145
    new-instance v5, Lcom/google/googlenav/ui/wizard/hW;

    invoke-direct {v5, p0, v1, v2}, Lcom/google/googlenav/ui/wizard/hW;-><init>(Lcom/google/googlenav/ui/wizard/hR;Landroid/widget/CheckBox;Landroid/widget/TextView;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1161
    const v1, 0x7f10002e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1162
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1165
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v1

    if-eqz v1, :cond_288

    const v1, 0x7f100031

    .line 1166
    :goto_1de
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v2

    if-eqz v2, :cond_28b

    .line 1168
    :goto_1e4
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1169
    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1170
    const/16 v2, 0x454

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1171
    new-instance v2, Lcom/google/googlenav/ui/wizard/hX;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hX;-><init>(Lcom/google/googlenav/ui/wizard/hR;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1178
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1179
    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1180
    const/16 v2, 0x6a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1181
    new-instance v2, Lcom/google/googlenav/ui/wizard/hY;

    invoke-direct {v2, p0}, Lcom/google/googlenav/ui/wizard/hY;-><init>(Lcom/google/googlenav/ui/wizard/hR;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1188
    return-object v0

    .line 1029
    :cond_219
    iget-object v2, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v2, v2, Lcom/google/googlenav/ui/wizard/hH;->h:Lcom/google/googlenav/ui/view/android/aL;

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/view/android/aL;->setTitle(Ljava/lang/CharSequence;)V

    .line 1030
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3e

    .line 1071
    :cond_229
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/4 v5, 0x5

    if-eq v3, v5, :cond_244

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/4 v5, 0x6

    if-eq v3, v5, :cond_244

    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->g:I

    const/4 v5, 0x7

    if-ne v3, v5, :cond_27e

    .line 1075
    :cond_244
    const/16 v3, 0x446

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/googlenav/ui/aV;->t:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1077
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->o:Ljava/lang/String;

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_271

    .line 1078
    invoke-virtual {v2, v6}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 1079
    iget-object v3, p0, Lcom/google/googlenav/ui/wizard/hR;->a:Lcom/google/googlenav/ui/wizard/hH;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hH;->b:Lcom/google/googlenav/ui/wizard/hQ;

    iget-object v3, v3, Lcom/google/googlenav/ui/wizard/hQ;->o:Ljava/lang/String;

    sget-object v5, Lcom/google/googlenav/ui/aV;->J:Lcom/google/googlenav/ui/aV;

    invoke-static {v3, v5}, Lcom/google/googlenav/ui/bi;->b(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1081
    :cond_271
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1082
    new-instance v3, Lcom/google/googlenav/ui/wizard/hU;

    invoke-direct {v3, p0}, Lcom/google/googlenav/ui/wizard/hU;-><init>(Lcom/google/googlenav/ui/wizard/hR;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_d0

    .line 1090
    :cond_27e
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_d0

    .line 1140
    :cond_283
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1b8

    :cond_288
    move v1, v4

    .line 1165
    goto/16 :goto_1de

    .line 1166
    :cond_28b
    const v4, 0x7f100031

    goto/16 :goto_1e4
.end method

.method protected e()Z
    .registers 2

    .prologue
    .line 1010
    const/4 v0, 0x0

    return v0
.end method
