.class Lcom/google/googlenav/bj;
.super Law/a;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Lcom/google/googlenav/bi;

.field private i:Z


# direct methods
.method private constructor <init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 237
    invoke-direct {p0}, Law/a;-><init>()V

    .line 159
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/googlenav/bj;->b:J

    .line 161
    iput v2, p0, Lcom/google/googlenav/bj;->d:I

    .line 169
    iput v2, p0, Lcom/google/googlenav/bj;->g:I

    .line 238
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bB()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/googlenav/bj;->c:Ljava/lang/String;

    .line 239
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->V()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/googlenav/bj;->b:J

    .line 240
    iput p2, p0, Lcom/google/googlenav/bj;->a:I

    .line 241
    iput-object p3, p0, Lcom/google/googlenav/bj;->h:Lcom/google/googlenav/bi;

    .line 242
    return-void
.end method

.method public static a(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 191
    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1, p2}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 193
    iput p1, v0, Lcom/google/googlenav/bj;->g:I

    .line 194
    return-object v0
.end method

.method public static a(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 176
    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 178
    return-object v0
.end method

.method public static b(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 207
    new-instance v0, Lcom/google/googlenav/bj;

    invoke-direct {v0, p0, v1, p2}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 209
    iput p1, v0, Lcom/google/googlenav/bj;->d:I

    .line 210
    if-ne p1, v1, :cond_12

    if-eqz p0, :cond_12

    .line 211
    invoke-virtual {p0}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/googlenav/bj;->f:Ljava/lang/String;

    .line 213
    :cond_12
    return-object v0
.end method

.method public static b(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 183
    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x4

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 185
    return-object v0
.end method

.method public static c(Lcom/google/googlenav/ai;Lcom/google/googlenav/bi;)Lcom/google/googlenav/bj;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 199
    new-instance v0, Lcom/google/googlenav/bj;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1, p1}, Lcom/google/googlenav/bj;-><init>(Lcom/google/googlenav/ai;ILcom/google/googlenav/bi;)V

    .line 201
    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, -0x1

    .line 256
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/gf;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 259
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/googlenav/bj;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 261
    iget-wide v1, p0, Lcom/google/googlenav/bj;->b:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1c

    .line 262
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/googlenav/bj;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 265
    :cond_1c
    iget-object v1, p0, Lcom/google/googlenav/bj;->c:Ljava/lang/String;

    if-eqz v1, :cond_26

    .line 266
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/googlenav/bj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 269
    :cond_26
    iget v1, p0, Lcom/google/googlenav/bj;->d:I

    if-eq v1, v5, :cond_30

    .line 270
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/googlenav/bj;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 273
    :cond_30
    iget-object v1, p0, Lcom/google/googlenav/bj;->e:Ljava/lang/String;

    if-eqz v1, :cond_3a

    .line 274
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/googlenav/bj;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 277
    :cond_3a
    iget-object v1, p0, Lcom/google/googlenav/bj;->f:Ljava/lang/String;

    if-eqz v1, :cond_45

    .line 278
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/googlenav/bj;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 281
    :cond_45
    iget v1, p0, Lcom/google/googlenav/bj;->g:I

    if-eq v1, v5, :cond_4f

    .line 282
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/googlenav/bj;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 285
    :cond_4f
    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    .line 286
    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 291
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/gf;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 293
    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v1

    .line 296
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    .line 298
    invoke-static {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v0

    .line 301
    packed-switch v1, :pswitch_data_24

    .line 308
    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1, v0}, LaM/f;->a(I)V

    .line 311
    :goto_1f
    return v3

    .line 303
    :pswitch_20
    iput-boolean v3, p0, Lcom/google/googlenav/bj;->i:Z

    goto :goto_1f

    .line 301
    nop

    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_20
    .end packed-switch
.end method

.method public b()I
    .registers 2

    .prologue
    .line 246
    const/16 v0, 0x4e

    return v0
.end method

.method public d_()V
    .registers 2

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/google/googlenav/bj;->i:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/googlenav/bj;->h:Lcom/google/googlenav/bi;

    if-eqz v0, :cond_d

    .line 317
    iget-object v0, p0, Lcom/google/googlenav/bj;->h:Lcom/google/googlenav/bi;

    invoke-interface {v0}, Lcom/google/googlenav/bi;->a()V

    .line 319
    :cond_d
    return-void
.end method

.method public s()Z
    .registers 2

    .prologue
    .line 251
    const/4 v0, 0x1

    return v0
.end method
