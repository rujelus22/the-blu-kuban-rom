.class Lcom/google/googlenav/bC;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/googlenav/bB;

.field private b:Z

.field private c:Ljava/lang/Thread;

.field private d:I

.field private e:I

.field private final f:LaR/u;


# direct methods
.method constructor <init>(LaR/u;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    iput-object p1, p0, Lcom/google/googlenav/bC;->f:LaR/u;

    .line 416
    iput-boolean v1, p0, Lcom/google/googlenav/bC;->b:Z

    .line 417
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    .line 418
    iput v1, p0, Lcom/google/googlenav/bC;->d:I

    .line 419
    iput v1, p0, Lcom/google/googlenav/bC;->e:I

    .line 420
    new-instance v0, Lcom/google/googlenav/bB;

    invoke-direct {v0}, Lcom/google/googlenav/bB;-><init>()V

    iput-object v0, p0, Lcom/google/googlenav/bC;->a:Lcom/google/googlenav/bB;

    .line 421
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bC;)I
    .registers 2
    .parameter

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/google/googlenav/bC;->b()I

    move-result v0

    return v0
.end method

.method private declared-synchronized a()Lcom/google/googlenav/bB;
    .registers 2

    .prologue
    .line 424
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/googlenav/bC;->a:Lcom/google/googlenav/bB;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 505
    :goto_0
    :try_start_0
    monitor-enter p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_1} :catch_10

    .line 506
    :try_start_1
    iget v0, p0, Lcom/google/googlenav/bC;->e:I

    if-lt v0, p1, :cond_7

    .line 507
    monitor-exit p0

    .line 519
    :cond_6
    :goto_6
    return-void

    .line 509
    :cond_7
    iget-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    .line 510
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_12

    .line 511
    if-eqz v0, :cond_6

    .line 514
    :try_start_c
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_f} :catch_10

    goto :goto_0

    .line 516
    :catch_10
    move-exception v0

    goto :goto_6

    .line 510
    :catchall_12
    move-exception v0

    :try_start_13
    monitor-exit p0
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    :try_start_14
    throw v0
    :try_end_15
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_15} :catch_10
.end method

.method private declared-synchronized a(ILcom/google/googlenav/bB;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 451
    monitor-enter p0

    :try_start_1
    iput-object p2, p0, Lcom/google/googlenav/bC;->a:Lcom/google/googlenav/bB;

    .line 452
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    .line 453
    iget v0, p0, Lcom/google/googlenav/bC;->e:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/googlenav/bC;->e:I

    .line 454
    iget-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    if-eqz v0, :cond_15

    .line 455
    invoke-direct {p0}, Lcom/google/googlenav/bC;->c()V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 457
    :cond_15
    monitor-exit p0

    return-void

    .line 451
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/googlenav/bC;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/google/googlenav/bC;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/bC;ILcom/google/googlenav/bB;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 378
    invoke-direct {p0, p1, p2}, Lcom/google/googlenav/bC;->a(ILcom/google/googlenav/bB;)V

    return-void
.end method

.method private declared-synchronized b()I
    .registers 2

    .prologue
    .line 432
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    if-nez v0, :cond_e

    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    .line 434
    iget v0, p0, Lcom/google/googlenav/bC;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/googlenav/bC;->d:I

    .line 436
    :cond_e
    iget-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    if-nez v0, :cond_15

    .line 437
    invoke-direct {p0}, Lcom/google/googlenav/bC;->c()V

    .line 439
    :cond_15
    iget v0, p0, Lcom/google/googlenav/bC;->d:I
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    monitor-exit p0

    return v0

    .line 432
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/googlenav/bC;)Lcom/google/googlenav/bB;
    .registers 2
    .parameter

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/google/googlenav/bC;->a()Lcom/google/googlenav/bB;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/googlenav/bC;)LaR/u;
    .registers 2
    .parameter

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/googlenav/bC;->f:LaR/u;

    return-object v0
.end method

.method private declared-synchronized c()V
    .registers 3

    .prologue
    .line 463
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/googlenav/bC;->b:Z

    .line 465
    iget v0, p0, Lcom/google/googlenav/bC;->d:I

    .line 468
    new-instance v1, Lcom/google/googlenav/bD;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/bD;-><init>(Lcom/google/googlenav/bC;I)V

    iput-object v1, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    .line 494
    iget-object v0, p0, Lcom/google/googlenav/bC;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_12
    .catchall {:try_start_2 .. :try_end_12} :catchall_14

    .line 495
    monitor-exit p0

    return-void

    .line 463
    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method
