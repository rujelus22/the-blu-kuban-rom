.class public final Lcom/google/googlenav/clientparam/EnableFeatureParameters;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile isActivePlaceSuggestionsEnabled:Z

.field private volatile isAndroidNativeNetworkLocationProviderEnabled:Z

.field private volatile isBicyclingEnabled:Z

.field private volatile isBuzzEnabled:Z

.field private volatile isCheckinAddPlaceEnabled:Z

.field private volatile isDownloadMapAreaEnabled:Z

.field private volatile isGaiaLoginEnabled:Z

.field private volatile isGooglePlusEnabled:Z

.field private volatile isKmlSearchEnabled:Z

.field private volatile isLatitudeEnabled:Z

.field private volatile isMyMapsEnabled:Z

.field private volatile isOffersFeatureSwitcherEnabled:Z

.field private volatile isOfflineMapsEnabled:Z

.field private volatile isPanoramaPhotoUploadEnabled:Z

.field private volatile isPersonalizedSmartMapsDisabled:Z

.field private volatile isPertileEnabled:Z

.field private volatile isPhotoUploadEnabled:Z

.field private volatile isRasterMapsEnabled:Z

.field private volatile isRatingsReviewTextEnabled:Z

.field private volatile isRoadLayersEnabled:Z

.field private volatile isSesameMapDataEditsEnabled:Z

.field private volatile isStarringSyncEnabled:Z

.field private volatile isTilePrefetchingEnabled:Z

.field private volatile isUserGeneratedContentEnabled:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    .line 47
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    .line 48
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    .line 49
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    .line 50
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    .line 51
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    .line 53
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    .line 54
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    .line 55
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    .line 56
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    .line 57
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    .line 58
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    .line 59
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    .line 60
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    .line 61
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    .line 62
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    .line 63
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    .line 69
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPersonalizedSmartMapsDisabled:Z

    .line 71
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    .line 73
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRoadLayersEnabled:Z

    .line 75
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    .line 77
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled:Z

    .line 79
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    .line 81
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    .line 90
    invoke-direct {p0, p1}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->mergeFlags(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 91
    return-void
.end method

.method public static getDefaultProto()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 2

    .prologue
    .line 289
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->e:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    return-object v0
.end method

.method private mergeFlags(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    if-eqz v0, :cond_145

    invoke-virtual {p1, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_145

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    .line 115
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    if-eqz v0, :cond_148

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_148

    move v0, v1

    :goto_1b
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    .line 117
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    if-eqz v0, :cond_14b

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_14b

    move v0, v1

    :goto_29
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    .line 119
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    if-eqz v0, :cond_14e

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_14e

    move v0, v1

    :goto_37
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    .line 122
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    if-eqz v0, :cond_151

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_151

    move v0, v1

    :goto_45
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    .line 125
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    if-eqz v0, :cond_154

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_154

    move v0, v1

    :goto_53
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    .line 127
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    if-eqz v0, :cond_157

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_157

    move v0, v1

    :goto_62
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    .line 130
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    if-eqz v0, :cond_15a

    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_15a

    move v0, v1

    :goto_70
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    .line 133
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    if-eqz v0, :cond_15d

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_15d

    move v0, v1

    :goto_7f
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    .line 136
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    if-eqz v0, :cond_160

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_160

    move v0, v1

    :goto_8e
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    .line 139
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    if-eqz v0, :cond_163

    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_163

    move v0, v1

    :goto_9d
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    .line 143
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    if-eqz v0, :cond_166

    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_166

    move v0, v1

    :goto_ac
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    .line 146
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    if-eqz v0, :cond_169

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_169

    move v0, v1

    :goto_bb
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    .line 150
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    if-eqz v0, :cond_16c

    const/16 v0, 0xe

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_16c

    move v0, v1

    :goto_ca
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    .line 153
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    if-eqz v0, :cond_16f

    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_16f

    move v0, v1

    :goto_d9
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    .line 156
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    if-eqz v0, :cond_172

    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_172

    move v0, v1

    :goto_e8
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    .line 161
    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPersonalizedSmartMapsDisabled:Z

    .line 164
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    if-eqz v0, :cond_175

    const/16 v0, 0x12

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_175

    move v0, v1

    :goto_ff
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    .line 167
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    if-eqz v0, :cond_177

    const/16 v0, 0x13

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_177

    move v0, v1

    :goto_10e
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    .line 169
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    if-eqz v0, :cond_179

    const/16 v0, 0x14

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_179

    move v0, v1

    :goto_11d
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    .line 173
    const/16 v0, 0x15

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled:Z

    .line 176
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    if-eqz v0, :cond_17b

    const/16 v0, 0x16

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_17b

    move v0, v1

    :goto_134
    iput-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    .line 179
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    if-eqz v0, :cond_17d

    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBool(I)Z

    move-result v0

    if-eqz v0, :cond_17d

    :goto_142
    iput-boolean v1, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    .line 182
    return-void

    :cond_145
    move v0, v2

    .line 112
    goto/16 :goto_d

    :cond_148
    move v0, v2

    .line 115
    goto/16 :goto_1b

    :cond_14b
    move v0, v2

    .line 117
    goto/16 :goto_29

    :cond_14e
    move v0, v2

    .line 119
    goto/16 :goto_37

    :cond_151
    move v0, v2

    .line 122
    goto/16 :goto_45

    :cond_154
    move v0, v2

    .line 125
    goto/16 :goto_53

    :cond_157
    move v0, v2

    .line 127
    goto/16 :goto_62

    :cond_15a
    move v0, v2

    .line 130
    goto/16 :goto_70

    :cond_15d
    move v0, v2

    .line 133
    goto/16 :goto_7f

    :cond_160
    move v0, v2

    .line 136
    goto/16 :goto_8e

    :cond_163
    move v0, v2

    .line 139
    goto/16 :goto_9d

    :cond_166
    move v0, v2

    .line 143
    goto/16 :goto_ac

    :cond_169
    move v0, v2

    .line 146
    goto/16 :goto_bb

    :cond_16c
    move v0, v2

    .line 150
    goto/16 :goto_ca

    :cond_16f
    move v0, v2

    .line 153
    goto/16 :goto_d9

    :cond_172
    move v0, v2

    .line 156
    goto/16 :goto_e8

    :cond_175
    move v0, v2

    .line 164
    goto :goto_ff

    :cond_177
    move v0, v2

    .line 167
    goto :goto_10e

    :cond_179
    move v0, v2

    .line 169
    goto :goto_11d

    :cond_17b
    move v0, v2

    .line 176
    goto :goto_134

    :cond_17d
    move v1, v2

    .line 179
    goto :goto_142
.end method


# virtual methods
.method public isActivePlaceSuggestionsEnabled()Z
    .registers 2

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isActivePlaceSuggestionsEnabled:Z

    return v0
.end method

.method public isAndroidNativeNetworkLocationProviderEnabled()Z
    .registers 2

    .prologue
    .line 225
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isAndroidNativeNetworkLocationProviderEnabled:Z

    return v0
.end method

.method public isBicyclingEnabled()Z
    .registers 2

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBicyclingEnabled:Z

    return v0
.end method

.method public isBuzzEnabled()Z
    .registers 2

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isBuzzEnabled:Z

    return v0
.end method

.method public isCheckinAddPlaceEnabled()Z
    .registers 2

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isCheckinAddPlaceEnabled:Z

    return v0
.end method

.method public isDownloadMapAreaEnabled()Z
    .registers 2

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isDownloadMapAreaEnabled:Z

    return v0
.end method

.method public isGaiaLoginEnabled()Z
    .registers 2

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGaiaLoginEnabled:Z

    return v0
.end method

.method public isGooglePlusEnabled()Z
    .registers 2

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isGooglePlusEnabled:Z

    return v0
.end method

.method public isKmlSearchEnabled()Z
    .registers 2

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isKmlSearchEnabled:Z

    return v0
.end method

.method public isLatitudeEnabled()Z
    .registers 2

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isLatitudeEnabled:Z

    return v0
.end method

.method public isMyMapsEnabled()Z
    .registers 2

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isMyMapsEnabled:Z

    return v0
.end method

.method public isOffersFeatureSwitcherEnabled()Z
    .registers 2

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOffersFeatureSwitcherEnabled:Z

    return v0
.end method

.method public isOfflineMapsEnabled()Z
    .registers 2

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled:Z

    return v0
.end method

.method public isPanoramaPhotoUploadEnabled()Z
    .registers 2

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPanoramaPhotoUploadEnabled:Z

    return v0
.end method

.method public isPersonalizedSmartMapsDisabled()Z
    .registers 2

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPersonalizedSmartMapsDisabled:Z

    return v0
.end method

.method public isPertileEnabled()Z
    .registers 2

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPertileEnabled:Z

    return v0
.end method

.method public isPhotoUploadEnabled()Z
    .registers 2

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isPhotoUploadEnabled:Z

    return v0
.end method

.method public isRasterMapsEnabled()Z
    .registers 2

    .prologue
    .line 281
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRasterMapsEnabled:Z

    return v0
.end method

.method public isRatingsReviewTextEnabled()Z
    .registers 2

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRatingsReviewTextEnabled:Z

    return v0
.end method

.method public isRoadLayersEnabled()Z
    .registers 2

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isRoadLayersEnabled:Z

    return v0
.end method

.method public isSesameMapDataEditsEnabled()Z
    .registers 2

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isSesameMapDataEditsEnabled:Z

    return v0
.end method

.method public isStarringSyncEnabled()Z
    .registers 2

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isStarringSyncEnabled:Z

    return v0
.end method

.method public isTilePrefetchingEnabled()Z
    .registers 2

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isTilePrefetchingEnabled:Z

    return v0
.end method

.method public isUserGeneratedContentEnabled()Z
    .registers 2

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isUserGeneratedContentEnabled:Z

    return v0
.end method

.method public mergeEnabledFeatures(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 2
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->mergeFlags(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 104
    return-void
.end method
