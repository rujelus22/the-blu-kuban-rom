.class public Lcom/google/googlenav/A;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static j:J


# instance fields
.field private final a:Lcom/google/googlenav/J;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lax/k;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:J

.field private final k:LaH/m;

.field private final l:LaN/u;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 142
    const-wide/16 v0, 0x2710

    sput-wide v0, Lcom/google/googlenav/A;->j:J

    return-void
.end method

.method public constructor <init>(LaH/m;LaN/u;Lcom/google/googlenav/J;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/googlenav/A;->m:Z

    .line 156
    iput-object p1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    .line 157
    iput-object p2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    .line 158
    iput-object p3, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    .line 159
    return-void
.end method

.method static synthetic a(Lcom/google/googlenav/A;)Lcom/google/googlenav/J;
    .registers 2
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    return-object v0
.end method

.method static synthetic a(Lcom/google/googlenav/A;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lax/y;)V
    .registers 3
    .parameter

    .prologue
    .line 359
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lax/y;->c()Z

    move-result v0

    if-nez v0, :cond_9

    .line 366
    :cond_8
    :goto_8
    return-void

    .line 363
    :cond_9
    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    .line 365
    invoke-virtual {v0, p1}, Lax/z;->a(Lax/y;)Z

    goto :goto_8
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;II)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 605
    new-instance v0, Lcom/google/googlenav/ai;

    const/16 v6, 0xa

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    .line 607
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ai;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v2, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    const/4 v2, -0x1

    if-ne p6, v2, :cond_29

    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v2}, LaN/u;->a()I

    move-result p6

    :cond_29
    const/4 v2, -0x1

    if-ne p7, v2, :cond_32

    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v2}, LaN/u;->b()I

    move-result p7

    :cond_32
    invoke-static {v1, v0, p6, p7}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    .line 611
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/aZ;Z)Lbf/bk;

    move-result-object v0

    .line 612
    invoke-virtual {v0}, Lbf/bk;->bN()V

    .line 613
    return-void
.end method

.method private static b(Lax/k;)Z
    .registers 2
    .parameter

    .prologue
    .line 400
    invoke-interface {p0}, Lax/k;->aq()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-interface {p0}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->q()Z

    move-result v0

    if-nez v0, :cond_20

    :cond_10
    invoke-interface {p0}, Lax/k;->as()Lax/y;

    move-result-object v0

    if-eqz v0, :cond_22

    invoke-interface {p0}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-virtual {v0}, Lax/y;->q()Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_20
    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method private e()LaN/H;
    .registers 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_2b

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    .line 231
    :goto_c
    iget-object v1, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v1}, LaN/u;->f()LaN/H;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    .line 232
    iget-boolean v1, p0, Lcom/google/googlenav/A;->m:Z

    if-eqz v1, :cond_2a

    .line 236
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-interface {v1}, Lcom/google/googlenav/J;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v1

    .line 237
    invoke-virtual {v1}, Lcom/google/googlenav/ui/ak;->m()LaN/Y;

    move-result-object v1

    .line 238
    if-eqz v1, :cond_2d

    .line 239
    invoke-virtual {v0, v1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v0

    .line 247
    :cond_2a
    :goto_2a
    return-object v0

    .line 230
    :cond_2b
    const/4 v0, 0x0

    goto :goto_c

    .line 244
    :cond_2d
    const/16 v1, 0x16

    invoke-static {v1}, LaN/Y;->b(I)LaN/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v0

    goto :goto_2a
.end method

.method private f()V
    .registers 6

    .prologue
    const/16 v4, 0x5f1

    .line 409
    iget-object v1, p0, Lcom/google/googlenav/A;->e:Lax/k;

    .line 411
    invoke-virtual {p0}, Lcom/google/googlenav/A;->c()V

    .line 412
    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-interface {v0}, Lcom/google/googlenav/J;->a()V

    .line 414
    const/4 v0, 0x0

    .line 415
    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->h()Z

    move-result v2

    if-eqz v2, :cond_37

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_37

    .line 416
    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->s()LaH/h;

    move-result-object v2

    .line 417
    if-eqz v2, :cond_37

    invoke-virtual {v2}, LaH/h;->a()LaN/B;

    move-result-object v3

    if-eqz v3, :cond_37

    .line 418
    invoke-virtual {v2}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v2}, LaH/h;->b()Lo/D;

    move-result-object v2

    invoke-static {v0, v2}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    .line 423
    :cond_37
    invoke-interface {v1}, Lax/k;->aq()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_72

    invoke-interface {v1}, Lax/k;->aq()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-eqz v2, :cond_72

    .line 424
    if-nez v0, :cond_52

    .line 426
    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 429
    :cond_52
    invoke-interface {v1, v0}, Lax/k;->a(Lax/y;)V

    .line 442
    :cond_55
    :goto_55
    instance-of v0, v1, Lax/b;

    if-nez v0, :cond_99

    .line 443
    new-instance v0, Lax/s;

    invoke-direct {v0, v1}, Lax/s;-><init>(Lax/k;)V

    .line 447
    :goto_5e
    invoke-interface {v0}, Lax/k;->aq()Lax/y;

    move-result-object v1

    if-eqz v1, :cond_6a

    invoke-interface {v0}, Lax/k;->as()Lax/y;

    move-result-object v1

    if-nez v1, :cond_91

    .line 448
    :cond_6a
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    check-cast v0, Lax/b;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->c(Lax/b;)V

    .line 452
    :goto_71
    return-void

    .line 430
    :cond_72
    invoke-interface {v1}, Lax/k;->as()Lax/y;

    move-result-object v2

    if-eqz v2, :cond_55

    invoke-interface {v1}, Lax/k;->as()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->q()Z

    move-result v2

    if-eqz v2, :cond_55

    .line 431
    if-nez v0, :cond_8d

    .line 433
    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/J;->a(Ljava/lang/String;)V

    .line 436
    :cond_8d
    invoke-interface {v1, v0}, Lax/k;->b(Lax/y;)V

    goto :goto_55

    .line 450
    :cond_91
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    check-cast v0, Lax/b;

    invoke-interface {v1, v0}, Lcom/google/googlenav/J;->a(Lax/b;)V

    goto :goto_71

    :cond_99
    move-object v0, v1

    goto :goto_5e
.end method


# virtual methods
.method public a(Lax/k;)V
    .registers 8
    .parameter

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/google/googlenav/A;->c()V

    .line 377
    iput-object p1, p0, Lcom/google/googlenav/A;->e:Lax/k;

    .line 380
    invoke-interface {p1}, Lax/k;->aq()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/A;->a(Lax/y;)V

    .line 381
    invoke-interface {p1}, Lax/k;->as()Lax/y;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/googlenav/A;->a(Lax/y;)V

    .line 385
    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->h()Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->g()Z

    move-result v0

    if-nez v0, :cond_29

    invoke-static {p1}, Lcom/google/googlenav/A;->b(Lax/k;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 387
    :cond_29
    invoke-direct {p0}, Lcom/google/googlenav/A;->f()V

    .line 396
    :goto_2c
    return-void

    .line 392
    :cond_2d
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    sget-wide v2, Lcom/google/googlenav/A;->j:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/A;->i:J

    .line 394
    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    const/16 v1, 0x291

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    goto :goto_2c
.end method

.method public a(Lcom/google/googlenav/ai;)V
    .registers 10
    .parameter

    .prologue
    const/4 v6, -0x1

    .line 511
    iget-object v0, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    .line 513
    if-eqz v0, :cond_21

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v2

    .line 515
    :goto_d
    if-eqz v0, :cond_24

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v3

    .line 516
    :goto_13
    iget-object v1, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v5

    move-object v0, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;II)V

    .line 518
    return-void

    .line 513
    :cond_21
    iget-object v2, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    goto :goto_d

    .line 515
    :cond_24
    const/4 v3, 0x0

    goto :goto_13
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/c;IIZZZ)V
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 272
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    .line 273
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    .line 274
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    .line 275
    const/4 v1, -0x1

    if-ne p5, v1, :cond_f

    const/4 v1, -0x1

    if-eq p6, v1, :cond_67

    .line 276
    :cond_f
    iget-object v1, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v1, p5, p6}, LaN/u;->d(II)V

    .line 277
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/googlenav/A;->m:Z

    .line 282
    :goto_17
    const/4 v1, 0x0

    .line 283
    invoke-virtual {p4}, Lcom/google/googlenav/c;->c()I

    move-result v2

    packed-switch v2, :pswitch_data_f8

    .line 340
    :cond_1f
    :goto_1f
    iget-object v2, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    if-nez v2, :cond_66

    .line 341
    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v2, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/googlenav/bg;->e(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    move/from16 v0, p9

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    .line 349
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5d

    .line 350
    const/16 v2, 0x4f2

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 353
    :cond_5d
    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 355
    :cond_66
    return-void

    .line 279
    :cond_67
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/googlenav/A;->m:Z

    goto :goto_17

    .line 286
    :pswitch_6b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " loc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Lcom/google/googlenav/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1f

    .line 290
    :pswitch_87
    iget-object v2, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {p4}, Lcom/google/googlenav/c;->b()LaN/B;

    move-result-object v3

    invoke-virtual {v2, v3}, LaN/u;->c(LaN/B;)V

    goto :goto_1f

    .line 294
    :pswitch_91
    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->h()Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->e()Z

    move-result v2

    if-nez v2, :cond_1f

    .line 296
    iget-object v2, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v2}, LaH/m;->g()Z

    move-result v2

    if-eqz v2, :cond_f6

    .line 297
    invoke-direct {p0}, Lcom/google/googlenav/A;->e()LaN/H;

    move-result-object v7

    .line 302
    :goto_ad
    if-eqz v7, :cond_b5

    invoke-virtual {v7}, LaN/H;->a()LaN/B;

    move-result-object v1

    if-nez v1, :cond_f3

    .line 304
    :cond_b5
    iput-object p1, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    .line 305
    iput-object p2, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    .line 306
    iput-object p3, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    .line 307
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    sget-wide v3, Lcom/google/googlenav/A;->j:J

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/googlenav/A;->i:J

    .line 312
    new-instance v3, Lcom/google/googlenav/B;

    invoke-direct {v3, p0}, Lcom/google/googlenav/B;-><init>(Lcom/google/googlenav/A;)V

    .line 320
    const/16 v1, 0x4fa

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 323
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    new-instance v4, Lcom/google/googlenav/ui/wizard/A;

    const/16 v5, 0x12

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    const-wide/16 v5, 0x0

    invoke-interface/range {v1 .. v6}, Lcom/google/googlenav/J;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    move-object v1, v7

    .line 326
    goto/16 :goto_1f

    :cond_f3
    move-object v1, v7

    goto/16 :goto_1f

    :cond_f6
    move-object v7, v1

    goto :goto_ad

    .line 283
    :pswitch_data_f8
    .packed-switch 0x0
        :pswitch_6b
        :pswitch_87
        :pswitch_91
    .end packed-switch
.end method

.method public a()Z
    .registers 5

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-wide v0, p0, Lcom/google/googlenav/A;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a(Lcom/google/googlenav/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 537
    if-ne p5, v1, :cond_7

    if-eq p6, v1, :cond_c

    .line 539
    :cond_7
    iget-object v1, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v1, p5, p6}, LaN/u;->d(II)V

    .line 542
    :cond_c
    invoke-virtual {p1}, Lcom/google/googlenav/c;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_86

    .line 595
    :cond_13
    :goto_13
    :pswitch_13
    return v0

    .line 545
    :pswitch_14
    iput-object p2, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    .line 546
    iput-object p3, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    .line 547
    iput-object p4, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    .line 548
    iget-object v0, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-direct {v1}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/google/googlenav/bg;->a(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    move v0, v8

    .line 552
    goto :goto_13

    .line 555
    :pswitch_36
    invoke-virtual {p1}, Lcom/google/googlenav/c;->b()LaN/B;

    move-result-object v5

    .line 593
    :goto_3a
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/googlenav/A;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LaN/B;II)V

    .line 594
    invoke-virtual {p0}, Lcom/google/googlenav/A;->d()V

    move v0, v8

    .line 595
    goto :goto_13

    .line 559
    :pswitch_49
    iget-object v0, p0, Lcom/google/googlenav/A;->l:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v5

    goto :goto_3a

    .line 563
    :pswitch_50
    iget-object v1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v1}, LaH/m;->h()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 564
    iget-object v0, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    .line 565
    iget-object v1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_6d

    if-eqz v0, :cond_6d

    .line 566
    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v5

    goto :goto_3a

    .line 568
    :cond_6d
    iput-object p2, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    .line 569
    iput-object p3, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    .line 570
    iput-object p4, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    .line 571
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    sget-wide v2, Lcom/google/googlenav/A;->j:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/googlenav/A;->i:J

    move v0, v8

    .line 574
    goto :goto_13

    .line 542
    :pswitch_data_86
    .packed-switch 0x0
        :pswitch_14
        :pswitch_36
        :pswitch_50
        :pswitch_49
        :pswitch_13
    .end packed-switch
.end method

.method public b()Z
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 196
    iget-wide v2, p0, Lcom/google/googlenav/A;->i:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_78

    .line 197
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v2

    .line 198
    iget-object v4, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v4}, LaH/m;->g()Z

    move-result v4

    if-nez v4, :cond_24

    iget-wide v4, p0, Lcom/google/googlenav/A;->i:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_78

    .line 199
    :cond_24
    iget-object v2, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    if-eqz v2, :cond_70

    .line 201
    iget-object v2, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-interface {v2}, Lcom/google/googlenav/J;->a()V

    .line 203
    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    .line 204
    iget-object v3, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 205
    iget-object v3, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->e(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 206
    iget-object v3, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_57

    .line 207
    const/16 v3, 0x4f2

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    .line 210
    :cond_57
    iget-object v1, p0, Lcom/google/googlenav/A;->k:LaH/m;

    invoke-interface {v1}, LaH/m;->g()Z

    move-result v1

    if-eqz v1, :cond_66

    .line 211
    invoke-direct {p0}, Lcom/google/googlenav/A;->e()LaN/H;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    .line 213
    :cond_66
    iget-object v1, p0, Lcom/google/googlenav/A;->a:Lcom/google/googlenav/J;

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/googlenav/J;->a(Lcom/google/googlenav/bf;)V

    .line 220
    :cond_6f
    :goto_6f
    return v0

    .line 214
    :cond_70
    iget-object v1, p0, Lcom/google/googlenav/A;->e:Lax/k;

    if-eqz v1, :cond_6f

    .line 215
    invoke-direct {p0}, Lcom/google/googlenav/A;->f()V

    goto :goto_6f

    :cond_78
    move v0, v1

    .line 220
    goto :goto_6f
.end method

.method public c()V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 620
    iput-object v0, p0, Lcom/google/googlenav/A;->b:Ljava/lang/String;

    .line 621
    iput-object v0, p0, Lcom/google/googlenav/A;->c:Ljava/lang/String;

    .line 622
    iput-object v0, p0, Lcom/google/googlenav/A;->d:Ljava/lang/String;

    .line 623
    iput-object v0, p0, Lcom/google/googlenav/A;->e:Lax/k;

    .line 624
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/googlenav/A;->i:J

    .line 625
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 633
    iput-object v0, p0, Lcom/google/googlenav/A;->g:Ljava/lang/String;

    .line 634
    iput-object v0, p0, Lcom/google/googlenav/A;->f:Ljava/lang/String;

    .line 635
    iput-object v0, p0, Lcom/google/googlenav/A;->h:Ljava/lang/String;

    .line 636
    return-void
.end method
