.class public final enum Lcom/google/commerce/wireless/topiary/X;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/commerce/wireless/topiary/X;

.field public static final enum b:Lcom/google/commerce/wireless/topiary/X;

.field public static final enum c:Lcom/google/commerce/wireless/topiary/X;

.field private static final synthetic d:[Lcom/google/commerce/wireless/topiary/X;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lcom/google/commerce/wireless/topiary/X;

    const-string v1, "NotAuthenticated"

    invoke-direct {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    .line 24
    new-instance v0, Lcom/google/commerce/wireless/topiary/X;

    const-string v1, "InProgress"

    invoke-direct {v0, v1, v3}, Lcom/google/commerce/wireless/topiary/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    .line 25
    new-instance v0, Lcom/google/commerce/wireless/topiary/X;

    const-string v1, "Authenticated"

    invoke-direct {v0, v1, v4}, Lcom/google/commerce/wireless/topiary/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/commerce/wireless/topiary/X;

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/commerce/wireless/topiary/X;->d:[Lcom/google/commerce/wireless/topiary/X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/X;
    .registers 2
    .parameter

    .prologue
    .line 22
    const-class v0, Lcom/google/commerce/wireless/topiary/X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/X;

    return-object v0
.end method

.method public static values()[Lcom/google/commerce/wireless/topiary/X;
    .registers 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/commerce/wireless/topiary/X;->d:[Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0}, [Lcom/google/commerce/wireless/topiary/X;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/commerce/wireless/topiary/X;

    return-object v0
.end method
