.class public Lcom/google/commerce/wireless/topiary/HybridWebView;
.super Landroid/webkit/WebView;
.source "SourceFile"

# interfaces
.implements Lcom/google/commerce/wireless/topiary/g;


# static fields
.field private static o:Ljava/lang/reflect/Method;


# instance fields
.field a:Lcom/google/commerce/wireless/topiary/v;

.field private b:Ljava/lang/String;

.field private final c:[I

.field private d:I

.field private e:Lcom/google/commerce/wireless/topiary/D;

.field private f:Lcom/google/commerce/wireless/topiary/f;

.field private g:Lcom/google/commerce/wireless/topiary/T;

.field private h:Ljava/lang/String;

.field private i:Lcom/google/commerce/wireless/topiary/T;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Landroid/os/Handler;

.field private m:Ljava/util/Hashtable;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    .line 827
    const/4 v0, 0x0

    sput-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    .line 832
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1b

    .line 834
    :try_start_9
    const-class v0, Landroid/webkit/WebView;

    .line 835
    const-string v1, "removeJavascriptInterface"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;
    :try_end_1b
    .catch Ljava/lang/NoSuchMethodException; {:try_start_9 .. :try_end_1b} :catch_1c

    .line 840
    :cond_1b
    return-void

    .line 836
    :catch_1c
    move-exception v0

    .line 837
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 61
    const-string v0, "HybridWebView"

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    .line 66
    const/16 v0, 0x14

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    .line 68
    iput v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    .line 79
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    .line 146
    invoke-direct {p0, p1, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Landroid/content/Context;Z)V

    .line 147
    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(ILjava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    if-eqz v0, :cond_5

    .line 347
    :cond_4
    :goto_4
    return-void

    .line 344
    :cond_5
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_4

    .line 345
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;ILjava/lang/String;I)V

    goto :goto_4
.end method

.method private a(Landroid/content/Context;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 160
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne v0, p1, :cond_20

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-preloading "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    .line 163
    :cond_20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    .line 164
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    aput v3, v0, v3

    .line 165
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    .line 166
    invoke-virtual {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setPreloading(Z)V

    .line 167
    invoke-virtual {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setScrollBarStyle(I)V

    .line 168
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 169
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 170
    sget-object v1, Landroid/webkit/WebSettings$RenderPriority;->HIGH:Landroid/webkit/WebSettings$RenderPriority;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setRenderPriority(Landroid/webkit/WebSettings$RenderPriority;)V

    .line 171
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 172
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 173
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 176
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 177
    const-wide/32 v1, 0x800000

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    .line 178
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "appcache"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 182
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 184
    if-nez p2, :cond_a1

    .line 185
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    :cond_a1
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->e()Landroid/webkit/WebViewClient;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->f()Landroid/webkit/WebChromeClient;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 191
    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/T;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Lcom/google/commerce/wireless/topiary/T;)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/commerce/wireless/topiary/T;)V
    .registers 8
    .parameter

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting authentication for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 429
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    .line 430
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    .line 431
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v1, p1, v0}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/T;)V

    .line 433
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->e:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/D;->d()Lcom/google/commerce/wireless/topiary/a;

    move-result-object v2

    .line 434
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/f;->a()Landroid/accounts/Account;

    move-result-object v3

    .line 435
    iget-object v5, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    .line 437
    new-instance v0, Lcom/google/commerce/wireless/topiary/p;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/commerce/wireless/topiary/p;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/a;Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/T;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/p;->b()V

    .line 462
    return-void
.end method

.method private a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 472
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v0, :cond_c

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 490
    :cond_c
    :goto_c
    return-void

    .line 475
    :cond_d
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Processing token for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    if-nez p2, :cond_46

    .line 477
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 478
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p1, v3}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Z)V

    .line 479
    const/4 v0, 0x2

    const-string v1, ""

    invoke-direct {p0, v0, v1, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ILjava/lang/String;I)V

    goto :goto_c

    .line 480
    :cond_46
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-ne v0, p1, :cond_5e

    .line 481
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p1}, Lcom/google/commerce/wireless/topiary/f;->b(Lcom/google/commerce/wireless/topiary/T;)V

    .line 482
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Loading token into web view"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 484
    invoke-virtual {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_c

    .line 486
    :cond_5e
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not loading token since pending service is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-eqz v0, :cond_81

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    :goto_75
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_c

    :cond_81
    const-string v0, "null"

    goto :goto_75
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 591
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_e
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-eqz v0, :cond_1c

    .line 593
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    .line 594
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    .line 595
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v1, v0, p1}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Z)V

    .line 597
    :cond_1c
    return-void
.end method

.method private a(ZILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    if-nez v0, :cond_b

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 336
    :cond_b
    :goto_b
    return-void

    .line 333
    :cond_c
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_b

    .line 334
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;ZILjava/lang/String;)V

    goto :goto_b
.end method

.method private a(Ljava/lang/String;Z)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 562
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_7c

    .line 563
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v2, p1}, Lcom/google/commerce/wireless/topiary/T;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_78

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    iget-boolean v2, v2, Lcom/google/commerce/wireless/topiary/T;->c:Z

    if-eqz v2, :cond_19

    if-eqz p2, :cond_78

    :cond_19
    move v3, v0

    .line 566
    :goto_1a
    if-eqz p2, :cond_7a

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/commerce/wireless/topiary/h;->c(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_7a

    move v2, v0

    .line 568
    :goto_27
    if-eqz v2, :cond_41

    .line 569
    iget-object v4, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Auth speeedbump detected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/commerce/wireless/topiary/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_41
    if-nez v3, :cond_45

    if-eqz v2, :cond_7c

    .line 572
    :cond_45
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Detected auth completion for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/commerce/wireless/topiary/r;

    invoke-direct {v2, p0}, Lcom/google/commerce/wireless/topiary/r;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 583
    :goto_77
    return v0

    :cond_78
    move v3, v1

    .line 563
    goto :goto_1a

    :cond_7a
    move v2, v1

    .line 566
    goto :goto_27

    .line 582
    :cond_7c
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not an auth completion: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 583
    goto :goto_77
.end method

.method private b(Lcom/google/commerce/wireless/topiary/T;)V
    .registers 6
    .parameter

    .prologue
    .line 644
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    .line 645
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-eqz v1, :cond_36

    .line 646
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "auth ok for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Now loading "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 648
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    .line 654
    :goto_35
    return-void

    .line 650
    :cond_36
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "auth ok for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". No url to load, so done."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    .line 652
    const/4 v0, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    goto :goto_35
.end method

.method private d(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 815
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Cleanup pending state"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 816
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Z)V

    .line 817
    invoke-virtual {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 818
    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    .line 819
    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    .line 820
    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    .line 821
    return-void
.end method

.method private d(Ljava/lang/String;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 666
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-nez v2, :cond_1f

    .line 667
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No account so can\'t do auth redirects "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :goto_1e
    return v0

    .line 670
    :cond_1f
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 671
    invoke-static {v3}, Lcom/google/commerce/wireless/topiary/h;->b(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_42

    .line 672
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not an auth redirect: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1e

    .line 675
    :cond_42
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Potential auth redirect: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_69

    .line 677
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "We are in the middle of auth - let redirects happen"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1e

    .line 683
    :cond_69
    const-string v2, "passive"

    invoke-virtual {v3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 684
    if-eqz v2, :cond_7f

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7f

    const-string v4, "false"

    invoke-virtual {v2, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_12d

    :cond_7f
    move v2, v0

    .line 687
    :goto_80
    if-eqz v2, :cond_96

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v2, :cond_96

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/f;->c()Z

    move-result v2

    if-eqz v2, :cond_96

    .line 688
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "Passive ServiceLogin likely to succeed - letting it proceed"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1e

    .line 692
    :cond_96
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying to process auth redirect: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v2, "service"

    invoke-virtual {v3, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 694
    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    if-nez v3, :cond_d8

    .line 695
    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No service specified - picking one based on param "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v3, v2}, Lcom/google/commerce/wireless/topiary/f;->a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v2

    iput-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    .line 698
    :cond_d8
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    if-nez v2, :cond_e5

    .line 699
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "No service found for auth - letting redirect happen..."

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1e

    .line 702
    :cond_e5
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Will do auth for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-nez v0, :cond_10b

    .line 704
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    .line 706
    :cond_10b
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "After auth will load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/T;)V

    move v0, v1

    .line 708
    goto/16 :goto_1e

    :cond_12d
    move v2, v1

    goto/16 :goto_80
.end method

.method private e(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 855
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_f

    .line 857
    :try_start_4
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_f} :catch_14
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_f} :catch_12
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_f} :catch_10

    .line 869
    :cond_f
    :goto_f
    return-void

    .line 865
    :catch_10
    move-exception v0

    goto :goto_f

    .line 863
    :catch_12
    move-exception v0

    goto :goto_f

    .line 858
    :catch_14
    move-exception v0

    goto :goto_f
.end method

.method private i()Z
    .registers 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v0, v0, v1

    .line 318
    if-eqz v0, :cond_c

    const/16 v1, 0x64

    if-lt v0, v1, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method a(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 257
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v0, v0, v1

    if-ne v0, p1, :cond_a

    .line 278
    :goto_9
    return-void

    .line 261
    :cond_a
    invoke-direct {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->i()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 262
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v1, v1, v2

    aput v1, v0, v3

    .line 263
    iput v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    .line 268
    :cond_1c
    iget v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2f

    .line 269
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "State history too long"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    .line 274
    :cond_2f
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aput p1, v0, v1

    .line 277
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "State transition "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9
.end method

.method a(ILjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x3

    .line 778
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceivedError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    packed-switch p1, :pswitch_data_34

    .line 805
    :goto_26
    :pswitch_26
    const/16 v1, 0x65

    invoke-direct {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    .line 807
    invoke-direct {p0, v0, p2, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ILjava/lang/String;I)V

    .line 808
    return-void

    .line 791
    :pswitch_2f
    const/4 v0, 0x1

    .line 792
    goto :goto_26

    .line 795
    :pswitch_31
    const/4 v0, 0x2

    .line 796
    goto :goto_26

    .line 781
    nop

    :pswitch_data_34
    .packed-switch -0xe
        :pswitch_2f
        :pswitch_26
        :pswitch_26
        :pswitch_2f
        :pswitch_26
        :pswitch_2f
        :pswitch_2f
        :pswitch_2f
        :pswitch_2f
        :pswitch_2f
        :pswitch_31
        :pswitch_26
        :pswitch_2f
        :pswitch_2f
    .end packed-switch
.end method

.method a(Lcom/google/commerce/wireless/topiary/D;Landroid/accounts/Account;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->e:Lcom/google/commerce/wireless/topiary/D;

    .line 225
    invoke-virtual {p1, p2}, Lcom/google/commerce/wireless/topiary/D;->a(Landroid/accounts/Account;)Lcom/google/commerce/wireless/topiary/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    .line 226
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p0}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/g;)V

    .line 227
    return-void
.end method

.method a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 497
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PageStarted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageStarted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-virtual {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 500
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->j:Ljava/lang/String;

    .line 501
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 503
    :cond_3b
    iput-boolean v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    .line 504
    const/4 v0, -0x1

    invoke-direct {p0, v3, v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    .line 505
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_4a

    .line 506
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1}, Lcom/google/commerce/wireless/topiary/v;->b(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    .line 508
    :cond_4a
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 384
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v0, :cond_7

    if-nez p2, :cond_b

    .line 385
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->loadUrl(Ljava/lang/String;)V

    .line 418
    :goto_a
    return-void

    .line 388
    :cond_b
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUrlAuthenticated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadUrlAuthenticated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    .line 391
    invoke-direct {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    .line 392
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    .line 393
    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    .line 395
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p2}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    .line 396
    sget-object v1, Lcom/google/commerce/wireless/topiary/u;->a:[I

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/X;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_114

    goto :goto_a

    .line 398
    :pswitch_73
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p2, p1}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 399
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "will do auth for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Lcom/google/commerce/wireless/topiary/T;)V

    goto/16 :goto_a

    .line 402
    :cond_9c
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decided not to do auth for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", policy was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/commerce/wireless/topiary/T;->e:Lcom/google/commerce/wireless/topiary/V;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/V;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Lcom/google/commerce/wireless/topiary/T;)V

    goto/16 :goto_a

    .line 408
    :pswitch_cd
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(I)V

    .line 409
    const/4 v0, -0x1

    invoke-direct {p0, v3, v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(ZILjava/lang/String;)V

    .line 411
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "auth in progress for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 414
    :pswitch_f3
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "already authenticated for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    invoke-direct {p0, p2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(Lcom/google/commerce/wireless/topiary/T;)V

    goto/16 :goto_a

    .line 396
    :pswitch_data_114
    .packed-switch 0x1
        :pswitch_73
        :pswitch_cd
        :pswitch_f3
    .end packed-switch
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    return v0
.end method

.method public a(Lcom/google/commerce/wireless/topiary/T;ZZ)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 610
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-nez v2, :cond_10

    invoke-virtual {p0, v4}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_32

    :cond_10
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->i:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 612
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a()Z

    move-result v2

    if-eqz v2, :cond_33

    if-eqz p3, :cond_33

    .line 613
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "Preloading view is abandoning load"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    .line 637
    :cond_32
    :goto_32
    return v0

    .line 617
    :cond_33
    if-eqz p2, :cond_5b

    .line 618
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got auth, will now load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/commerce/wireless/topiary/s;

    invoke-direct {v2, p0, p1}, Lcom/google/commerce/wireless/topiary/s;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;Lcom/google/commerce/wireless/topiary/T;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 625
    goto :goto_32

    .line 627
    :cond_5b
    invoke-virtual {p0, v4}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_82

    .line 628
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Auth failed or cancelled, will re-load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    move v0, v1

    .line 633
    goto :goto_32

    .line 635
    :cond_82
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v2, "Auth failed or cancelled in this web view - exiting"

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_32
.end method

.method public addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 844
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-virtual {v0, p2, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 845
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 846
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x1e

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 283
    const/4 v0, 0x0

    :goto_8
    iget v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    if-gt v0, v2, :cond_1d

    .line 284
    if-lez v0, :cond_13

    .line 285
    const-string v2, "->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    :cond_13
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 283
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 289
    :cond_1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Z
    .registers 3
    .parameter

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c()I

    move-result v0

    if-ne v0, p1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method b(Ljava/lang/String;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 520
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ShouldOverride?: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v2, p0, p1}, Lcom/google/commerce/wireless/topiary/v;->a(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 542
    :goto_26
    return v0

    .line 524
    :cond_27
    invoke-direct {p0, p1, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_33

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 525
    :cond_33
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Overriding loading of: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_26

    .line 528
    :cond_4c
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v2

    if-eqz v2, :cond_89

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v2, :cond_89

    .line 529
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->e:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v2, p1}, Lcom/google/commerce/wireless/topiary/D;->a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;

    move-result-object v2

    .line 530
    if-eqz v2, :cond_89

    .line 531
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Overrideing load of : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " will do auth load for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-virtual {p0, p1, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Lcom/google/commerce/wireless/topiary/T;)V

    goto :goto_26

    .line 537
    :cond_89
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 538
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Detected non auth redirect, setting desiredUrl to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    .line 541
    :cond_aa
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ProceedingToLoad: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 542
    goto/16 :goto_26
.end method

.method public c()I
    .registers 3

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    iget v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    aget v0, v0, v1

    return v0
.end method

.method c(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 718
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageFinished: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PageFinished: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/commerce/wireless/topiary/m;->a(ZLjava/lang/String;)V

    .line 720
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 721
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageFinished - detected auth completion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_4e
    :goto_4e
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    if-eqz v0, :cond_57

    .line 738
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    invoke-interface {v0, p0, p1}, Lcom/google/commerce/wireless/topiary/v;->c(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    .line 740
    :cond_57
    return-void

    .line 723
    :cond_58
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_9b

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-eqz v0, :cond_9b

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/commerce/wireless/topiary/T;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 725
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Clearing pending load for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    .line 730
    :goto_88
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    if-nez v0, :cond_4e

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-nez v0, :cond_4e

    .line 731
    invoke-direct {p0, v3}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    .line 732
    iget-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    if-eqz v0, :cond_4e

    .line 733
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->g()V

    goto :goto_4e

    .line 728
    :cond_9b
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NOT clearing pending load for : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", desired = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_88
.end method

.method public c(I)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 307
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->d:I

    if-gt v0, v2, :cond_d

    .line 308
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->c:[I

    aget v2, v2, v0

    if-ne v2, p1, :cond_e

    .line 309
    const/4 v1, 0x1

    .line 312
    :cond_d
    return v1

    .line 307
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public d()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 323
    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v1

    if-nez v1, :cond_1c

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v1

    if-nez v1, :cond_1c

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1d

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c(I)Z

    move-result v1

    if-nez v1, :cond_1d

    :cond_1c
    const/4 v0, 0x1

    :cond_1d
    return v0
.end method

.method public destroy()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x6

    .line 201
    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 217
    :goto_8
    return-void

    .line 204
    :cond_9
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Destroying..."

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->stopLoading()V

    .line 206
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->h()V

    .line 207
    invoke-direct {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->d(I)V

    .line 208
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->freeMemory()V

    .line 209
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->destroyDrawingCache()V

    .line 210
    iput-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    .line 211
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    if-eqz v0, :cond_2a

    .line 212
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->f:Lcom/google/commerce/wireless/topiary/f;

    invoke-virtual {v0, p0}, Lcom/google/commerce/wireless/topiary/f;->b(Lcom/google/commerce/wireless/topiary/g;)V

    .line 214
    :cond_2a
    iput-object v3, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    .line 215
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    .line 216
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Destroyed"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method protected e()Landroid/webkit/WebViewClient;
    .registers 2

    .prologue
    .line 356
    new-instance v0, Lcom/google/commerce/wireless/topiary/w;

    invoke-direct {v0, p0}, Lcom/google/commerce/wireless/topiary/w;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-object v0
.end method

.method protected f()Landroid/webkit/WebChromeClient;
    .registers 2

    .prologue
    .line 366
    new-instance v0, Lcom/google/commerce/wireless/topiary/o;

    invoke-direct {v0, p0}, Lcom/google/commerce/wireless/topiary/o;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;)V

    return-object v0
.end method

.method g()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 748
    invoke-virtual {p0, v2}, Lcom/google/commerce/wireless/topiary/HybridWebView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 749
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    const-string v1, "Load finished - showing WebView"

    invoke-static {v0, v1}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    iput-boolean v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    .line 753
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 754
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->clearHistory()V

    .line 757
    :cond_1a
    invoke-virtual {p0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 758
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/commerce/wireless/topiary/t;

    invoke-direct {v2, p0, v0}, Lcom/google/commerce/wireless/topiary/t;-><init>(Lcom/google/commerce/wireless/topiary/HybridWebView;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 770
    :goto_28
    return-void

    .line 765
    :cond_29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->n:Z

    .line 766
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring progress done "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->g:Lcom/google/commerce/wireless/topiary/T;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    :goto_4f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_28

    :cond_5b
    const-string v0, " no service"

    goto :goto_4f
.end method

.method h()V
    .registers 3

    .prologue
    .line 873
    sget-object v0, Lcom/google/commerce/wireless/topiary/HybridWebView;->o:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1e

    .line 874
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 875
    invoke-direct {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->e(Ljava/lang/String;)V

    goto :goto_e

    .line 878
    :cond_1e
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 879
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->m:Ljava/util/Hashtable;

    .line 880
    return-void
.end method

.method public setHybridWebViewUiClient(Lcom/google/commerce/wireless/topiary/v;)V
    .registers 2
    .parameter

    .prologue
    .line 231
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->a:Lcom/google/commerce/wireless/topiary/v;

    .line 232
    return-void
.end method

.method public setPreloading(Z)V
    .registers 3
    .parameter

    .prologue
    .line 246
    iput-boolean p1, p0, Lcom/google/commerce/wireless/topiary/HybridWebView;->k:Z

    .line 247
    if-eqz p1, :cond_9

    const/4 v0, 0x4

    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/commerce/wireless/topiary/HybridWebView;->setVisibility(I)V

    .line 248
    return-void

    .line 247
    :cond_9
    const/4 v0, 0x0

    goto :goto_5
.end method
