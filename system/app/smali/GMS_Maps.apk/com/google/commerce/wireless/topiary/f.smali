.class public Lcom/google/commerce/wireless/topiary/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/accounts/Account;

.field private final b:Lcom/google/commerce/wireless/topiary/D;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Lcom/google/commerce/wireless/topiary/D;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    .line 57
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    .line 60
    iput-object p1, p0, Lcom/google/commerce/wireless/topiary/f;->a:Landroid/accounts/Account;

    .line 61
    iput-object p2, p0, Lcom/google/commerce/wireless/topiary/f;->b:Lcom/google/commerce/wireless/topiary/D;

    .line 62
    return-void
.end method

.method private b(Lcom/google/commerce/wireless/topiary/T;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 82
    const/4 v0, 0x0

    .line 83
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/g;

    .line 84
    invoke-interface {v0}, Lcom/google/commerce/wireless/topiary/g;->a()Z

    move-result v3

    if-nez v3, :cond_3e

    .line 85
    invoke-interface {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/g;->a(Lcom/google/commerce/wireless/topiary/T;ZZ)Z

    move-result v0

    or-int/2addr v0, v1

    :goto_1f
    move v1, v0

    goto :goto_8

    .line 88
    :cond_21
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_27
    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/g;

    .line 89
    invoke-interface {v0}, Lcom/google/commerce/wireless/topiary/g;->a()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 90
    invoke-interface {v0, p1, p2, v1}, Lcom/google/commerce/wireless/topiary/g;->a(Lcom/google/commerce/wireless/topiary/T;ZZ)Z

    goto :goto_27

    .line 93
    :cond_3d
    return-void

    :cond_3e
    move v0, v1

    goto :goto_1f
.end method

.method private c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;
    .registers 3
    .parameter

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->d(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    return-object v0
.end method

.method private d(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;
    .registers 6
    .parameter

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v1

    .line 145
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    .line 146
    if-nez v0, :cond_30

    .line 147
    const-string v0, "AuthState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new service: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v0, Lcom/google/commerce/wireless/topiary/W;

    invoke-direct {v0, p1}, Lcom/google/commerce/wireless/topiary/W;-><init>(Lcom/google/commerce/wireless/topiary/T;)V

    .line 149
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_30
    return-object v0
.end method

.method private e(Lcom/google/commerce/wireless/topiary/T;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 215
    iget-object v2, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 224
    :cond_a
    :goto_a
    return v0

    .line 218
    :cond_b
    iget-object v2, p0, Lcom/google/commerce/wireless/topiary/f;->b:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v2}, Lcom/google/commerce/wireless/topiary/D;->e()Lcom/google/commerce/wireless/topiary/B;

    move-result-object v2

    iget-object v3, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/google/commerce/wireless/topiary/B;->a(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_1b

    move v0, v1

    .line 219
    goto :goto_a

    .line 221
    :cond_1b
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->f(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    .line 222
    goto :goto_a
.end method

.method private f(Lcom/google/commerce/wireless/topiary/T;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 234
    iget-object v0, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/U;

    .line 235
    iget v3, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    if-lez v3, :cond_7

    .line 236
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v3

    .line 237
    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v4

    sget-object v5, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-eq v4, v5, :cond_51

    .line 238
    const-string v2, "AuthState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cookie "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/U;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " requires service "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/google/commerce/wireless/topiary/T;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to be authenticated in this session"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 253
    :goto_50
    return v0

    .line 243
    :cond_51
    invoke-virtual {v3}, Lcom/google/commerce/wireless/topiary/W;->b()J

    move-result-wide v3

    .line 244
    iget v5, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_7

    .line 245
    const-string v2, "AuthState"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cookie "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/commerce/wireless/topiary/U;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " allows max age of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sec. but it has been "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Lcom/google/commerce/wireless/topiary/U;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " secs."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 249
    goto :goto_50

    .line 253
    :cond_9e
    const/4 v0, 0x0

    goto :goto_50
.end method


# virtual methods
.method public a()Landroid/accounts/Account;
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/commerce/wireless/topiary/T;
    .registers 3
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    .line 157
    if-nez v0, :cond_c

    .line 158
    const/4 v0, 0x0

    .line 160
    :goto_b
    return-object v0

    :cond_c
    iget-object v0, v0, Lcom/google/commerce/wireless/topiary/W;->a:Lcom/google/commerce/wireless/topiary/T;

    goto :goto_b
.end method

.method public a(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;
    .registers 5
    .parameter

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v1

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    if-ne v1, v2, :cond_1c

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/W;->a(J)Z

    move-result v1

    if-nez v1, :cond_1c

    .line 129
    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    .line 135
    :cond_1b
    :goto_1b
    return-object v0

    .line 130
    :cond_1c
    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v1

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-ne v1, v2, :cond_1b

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->e(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 132
    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    goto :goto_1b
.end method

.method a(Lcom/google/commerce/wireless/topiary/T;Lcom/google/commerce/wireless/topiary/T;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->c(Lcom/google/commerce/wireless/topiary/T;)Lcom/google/commerce/wireless/topiary/W;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    .line 173
    if-eqz p2, :cond_25

    .line 174
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    .line 176
    if-eqz v0, :cond_25

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->b:Lcom/google/commerce/wireless/topiary/X;

    if-ne v0, v1, :cond_25

    .line 178
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/commerce/wireless/topiary/f;->a(Lcom/google/commerce/wireless/topiary/T;Z)V

    .line 181
    :cond_25
    return-void
.end method

.method a(Lcom/google/commerce/wireless/topiary/T;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 188
    invoke-virtual {p1}, Lcom/google/commerce/wireless/topiary/T;->b()Ljava/lang/String;

    move-result-object v0

    .line 189
    const-string v1, "AuthState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Processing auth completion for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/commerce/wireless/topiary/G;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v1, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    .line 191
    if-eqz p2, :cond_2f

    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    :goto_28
    invoke-virtual {v0, v1}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    .line 193
    invoke-direct {p0, p1, p2}, Lcom/google/commerce/wireless/topiary/f;->b(Lcom/google/commerce/wireless/topiary/T;Z)V

    .line 194
    return-void

    .line 191
    :cond_2f
    sget-object v1, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    goto :goto_28
.end method

.method a(Lcom/google/commerce/wireless/topiary/g;)V
    .registers 3
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public a(Lcom/google/commerce/wireless/topiary/T;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 205
    iget-object v0, p1, Lcom/google/commerce/wireless/topiary/T;->e:Lcom/google/commerce/wireless/topiary/V;

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->c:Lcom/google/commerce/wireless/topiary/V;

    if-eq v0, v1, :cond_12

    iget-object v0, p1, Lcom/google/commerce/wireless/topiary/T;->e:Lcom/google/commerce/wireless/topiary/V;

    sget-object v1, Lcom/google/commerce/wireless/topiary/V;->b:Lcom/google/commerce/wireless/topiary/V;

    if-ne v0, v1, :cond_14

    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->e(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public b()V
    .registers 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    .line 104
    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->a:Lcom/google/commerce/wireless/topiary/X;

    invoke-virtual {v0, v2}, Lcom/google/commerce/wireless/topiary/W;->a(Lcom/google/commerce/wireless/topiary/X;)V

    goto :goto_a

    .line 106
    :cond_1c
    return-void
.end method

.method public b(Lcom/google/commerce/wireless/topiary/T;)V
    .registers 5
    .parameter

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lcom/google/commerce/wireless/topiary/f;->f(Lcom/google/commerce/wireless/topiary/T;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 263
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->b:Lcom/google/commerce/wireless/topiary/D;

    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/D;->e()Lcom/google/commerce/wireless/topiary/B;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/commerce/wireless/topiary/T;->d:Z

    iget-object v2, p1, Lcom/google/commerce/wireless/topiary/T;->f:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/commerce/wireless/topiary/B;->a(ZLjava/util/List;)V

    .line 266
    :cond_13
    return-void
.end method

.method b(Lcom/google/commerce/wireless/topiary/g;)V
    .registers 3
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 71
    return-void
.end method

.method public c()Z
    .registers 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/commerce/wireless/topiary/f;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/commerce/wireless/topiary/W;

    .line 114
    invoke-virtual {v0}, Lcom/google/commerce/wireless/topiary/W;->a()Lcom/google/commerce/wireless/topiary/X;

    move-result-object v0

    sget-object v2, Lcom/google/commerce/wireless/topiary/X;->c:Lcom/google/commerce/wireless/topiary/X;

    if-ne v0, v2, :cond_a

    .line 115
    const/4 v0, 0x1

    .line 118
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method
