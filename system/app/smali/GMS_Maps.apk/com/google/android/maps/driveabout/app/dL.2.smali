.class public Lcom/google/android/maps/driveabout/app/dL;
.super Lcom/google/android/maps/driveabout/app/aP;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/Path;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Lo/T;

.field private g:Lo/X;

.field private h:LD/b;

.field private i:F

.field private j:[I

.field private k:I

.field private l:Landroid/graphics/Bitmap;

.field private final m:Lo/T;

.field private final n:Lo/T;


# direct methods
.method public constructor <init>(LO/z;LO/N;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/aP;-><init>()V

    .line 84
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->m:Lo/T;

    .line 85
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->n:Lo/T;

    .line 91
    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dL;->a(LO/z;LO/N;)V

    .line 92
    return-void
.end method

.method static a(LC/a;Lo/X;Lo/T;I)Lo/X;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 329
    new-instance v0, Lo/T;

    invoke-direct {v0, p3, p3}, Lo/T;-><init>(II)V

    .line 330
    new-instance v1, Lo/ad;

    invoke-virtual {p2, v0}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {p2, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    .line 332
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 333
    new-instance v0, Lo/h;

    invoke-direct {v0, v1}, Lo/h;-><init>(Lo/ae;)V

    .line 334
    invoke-virtual {v0, p1, v6}, Lo/h;->a(Lo/X;Ljava/util/List;)V

    .line 337
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_28

    .line 359
    :cond_27
    :goto_27
    return-object v4

    .line 339
    :cond_28
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_40

    .line 340
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 356
    :goto_35
    if-eqz v0, :cond_27

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_27

    move-object v4, v0

    .line 359
    goto :goto_27

    :cond_40
    move v1, v2

    move-object v3, v4

    .line 343
    :goto_42
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6b

    .line 344
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    move v5, v2

    .line 345
    :goto_4f
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v7

    if-ge v5, v7, :cond_60

    .line 346
    invoke-virtual {v0, v5}, Lo/X;->a(I)Lo/T;

    move-result-object v7

    invoke-virtual {v7, p2}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_64

    move-object v3, v0

    .line 351
    :cond_60
    if-eqz v3, :cond_67

    move-object v0, v3

    .line 352
    goto :goto_35

    .line 345
    :cond_64
    add-int/lit8 v5, v5, 0x1

    goto :goto_4f

    .line 343
    :cond_67
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_42

    :cond_6b
    move-object v0, v3

    goto :goto_35
.end method

.method static a(LO/z;I)Lo/X;
    .registers 10
    .parameter
    .parameter

    .prologue
    const-wide v6, 0x409f400000000000L

    .line 126
    invoke-virtual {p0, p1}, LO/z;->b(I)D

    move-result-wide v0

    .line 127
    const-wide/16 v2, 0x0

    sub-double v4, v0, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, LO/z;->a(D)I

    move-result v2

    .line 129
    invoke-virtual {p0}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v3

    add-double/2addr v0, v6

    const-wide/high16 v4, 0x3ff0

    sub-double/2addr v0, v4

    invoke-virtual {p0, v0, v1}, LO/z;->a(D)I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 132
    new-instance v1, Lo/am;

    invoke-virtual {p0}, LO/z;->n()Lo/X;

    move-result-object v3

    invoke-direct {v1, v3, v2, v0}, Lo/am;-><init>(Lo/X;II)V

    invoke-virtual {v1}, Lo/am;->e()Lo/X;

    move-result-object v0

    return-object v0
.end method

.method private a(F)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, -0x1

    const v9, -0xbbbbbc

    const/4 v8, 0x1

    const/high16 v7, 0x3f80

    .line 171
    const/high16 v0, 0x4080

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 172
    const/high16 v1, 0x4198

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 173
    const/high16 v2, 0x41c0

    mul-float/2addr v2, p1

    const/high16 v3, 0x3f00

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 174
    const/high16 v3, 0x4000

    mul-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 177
    div-int/lit8 v4, v0, 0x2

    .line 178
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    iput-object v5, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    .line 179
    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    int-to-float v6, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 180
    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    int-to-float v6, v2

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    const/4 v6, 0x0

    neg-int v1, v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    invoke-virtual {v5, v6, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 183
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    neg-int v2, v4

    int-to-float v2, v2

    invoke-virtual {v1, v2, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    .line 187
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 188
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 189
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 190
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 192
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    int-to-float v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 194
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->c:Landroid/graphics/Paint;

    .line 195
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 196
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 197
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 199
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    .line 200
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 201
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 202
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 203
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 204
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 205
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    mul-int/lit8 v2, v3, 0x2

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 207
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    .line 208
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 209
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 210
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 212
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 213
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 216
    const/high16 v0, 0x432a

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 218
    const/16 v1, 0x40

    invoke-static {v0, v1}, LD/b;->c(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dL;->k:I

    .line 219
    return-void
.end method

.method private a(LD/a;LC/a;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 265
    invoke-virtual {p2}, LC/a;->m()F

    move-result v0

    .line 266
    invoke-virtual {p2}, LC/a;->y()F

    move-result v1

    mul-float/2addr v1, v0

    .line 267
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    if-nez v2, :cond_10

    .line 268
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dL;->a(F)V

    .line 271
    :cond_10
    const/4 v0, 0x0

    .line 273
    const/high16 v2, 0x44fa

    const/high16 v3, 0x4140

    mul-float/2addr v3, v1

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2c

    .line 274
    const/16 v0, 0x7d0

    const/high16 v2, 0x4200

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 276
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->g:Lo/X;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    invoke-static {p2, v1, v2, v0}, Lcom/google/android/maps/driveabout/app/dL;->a(LC/a;Lo/X;Lo/T;I)Lo/X;

    move-result-object v0

    .line 279
    :cond_2c
    if-eqz v0, :cond_7b

    .line 280
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dL;->k:I

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dL;->b(LC/a;Lo/X;Lo/T;I)[I

    move-result-object v1

    .line 285
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->j:[I

    if-eqz v2, :cond_42

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->j:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_7a

    .line 287
    :cond_42
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->j:[I

    .line 288
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->m:Lo/T;

    .line 289
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dL;->n:Lo/T;

    .line 290
    invoke-virtual {v0}, Lo/X;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v4, v2}, Lo/X;->a(ILo/T;)V

    .line 291
    invoke-virtual {v0, v3}, Lo/X;->a(Lo/T;)V

    .line 292
    invoke-static {v2, v3}, Lo/T;->a(Lo/T;Lo/T;)D

    move-result-wide v2

    double-to-float v0, v2

    .line 294
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dL;->i()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 295
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/maps/driveabout/app/dL;->a(Landroid/graphics/Canvas;[IF)V

    .line 297
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dL;->e()V

    .line 298
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    .line 299
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    .line 300
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    invoke-virtual {v0, v2}, LD/b;->b(Landroid/graphics/Bitmap;)V

    .line 305
    :cond_7a
    :goto_7a
    return-void

    .line 303
    :cond_7b
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dL;->e()V

    goto :goto_7a
.end method

.method private a(Landroid/graphics/Canvas;[IF)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 396
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 397
    const/4 v0, 0x0

    aget v0, p2, v0

    int-to-float v0, v0

    const/4 v2, 0x1

    aget v2, p2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 398
    const/4 v0, 0x2

    :goto_11
    array-length v2, p2

    if-ge v0, v2, :cond_22

    .line 399
    aget v2, p2, v0

    int-to-float v2, v2

    add-int/lit8 v3, v0, 0x1

    aget v3, p2, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 398
    add-int/lit8 v0, v0, 0x2

    goto :goto_11

    .line 401
    :cond_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 405
    array-length v0, p2

    add-int/lit8 v0, v0, -0x2

    aget v0, p2, v0

    int-to-float v0, v0

    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    aget v1, p2, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 406
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 407
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 408
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 409
    return-void
.end method

.method static b(LC/a;Lo/X;Lo/T;I)[I
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f00

    .line 368
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    .line 369
    invoke-virtual {p0}, LC/a;->y()F

    move-result v2

    .line 370
    new-instance v3, Lo/T;

    int-to-float v4, p3

    mul-float/2addr v4, v2

    mul-float/2addr v4, v6

    float-to-int v4, v4

    int-to-float v5, p3

    mul-float/2addr v5, v2

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v3, v4, v5}, Lo/T;-><init>(II)V

    invoke-virtual {p2, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    .line 374
    const/high16 v4, 0x3f80

    div-float v2, v4, v2

    .line 375
    invoke-virtual {p1, v7}, Lo/X;->a(I)Lo/T;

    move-result-object v4

    .line 376
    invoke-static {v4, v3, v4}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 377
    invoke-virtual {v4}, Lo/T;->f()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    aput v5, v1, v7

    .line 378
    invoke-virtual {v4}, Lo/T;->g()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    sub-int v5, p3, v5

    aput v5, v1, v0

    .line 379
    :goto_46
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v5

    if-ge v0, v5, :cond_75

    .line 380
    invoke-virtual {p1, v0, v4}, Lo/X;->a(ILo/T;)V

    .line 381
    invoke-static {v4, v3, v4}, Lo/T;->b(Lo/T;Lo/T;Lo/T;)V

    .line 382
    mul-int/lit8 v5, v0, 0x2

    invoke-virtual {v4}, Lo/T;->f()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    aput v6, v1, v5

    .line 383
    mul-int/lit8 v5, v0, 0x2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4}, Lo/T;->g()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    sub-int v6, p3, v6

    aput v6, v1, v5

    .line 379
    add-int/lit8 v0, v0, 0x1

    goto :goto_46

    .line 386
    :cond_75
    return-object v1
.end method

.method private e()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    if-eqz v0, :cond_e

    .line 150
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    invoke-virtual {v0}, LD/b;->g()V

    .line 151
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    .line 152
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->j:[I

    .line 154
    :cond_e
    return-void
.end method

.method private h()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 157
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->a:Landroid/graphics/Path;

    .line 158
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->b:Landroid/graphics/Paint;

    .line 159
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->c:Landroid/graphics/Paint;

    .line 160
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->d:Landroid/graphics/Paint;

    .line 161
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->e:Landroid/graphics/Paint;

    .line 162
    return-void
.end method

.method private i()Landroid/graphics/Bitmap;
    .registers 4

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->l:Landroid/graphics/Bitmap;

    if-nez v0, :cond_10

    .line 313
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dL;->k:I

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dL;->k:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->l:Landroid/graphics/Bitmap;

    .line 316
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->l:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->l:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x3dcccccd

    .line 223
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_15

    invoke-virtual {p2}, LC/a;->B()Lo/aQ;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    invoke-virtual {v0, v1}, Lo/aQ;->a(Lo/T;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 258
    :cond_15
    :goto_15
    return-void

    .line 227
    :cond_16
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 228
    invoke-virtual {p2}, LC/a;->r()F

    move-result v1

    .line 230
    monitor-enter p0

    .line 231
    :try_start_1f
    iget v2, p0, Lcom/google/android/maps/driveabout/app/dL;->i:F

    add-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_2d

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dL;->i:F

    sub-float/2addr v2, v3

    cmpg-float v2, v1, v2

    if-gez v2, :cond_32

    .line 233
    :cond_2d
    iput v1, p0, Lcom/google/android/maps/driveabout/app/dL;->i:F

    .line 234
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dL;->a(LD/a;LC/a;)V

    .line 236
    :cond_32
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    .line 237
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_1f .. :try_end_35} :catchall_76

    .line 238
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    if-eqz v2, :cond_15

    .line 243
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 244
    iget v2, p0, Lcom/google/android/maps/driveabout/app/dL;->k:I

    int-to-float v2, v2

    const/high16 v3, 0x3f00

    mul-float/2addr v2, v3

    invoke-virtual {p2}, LC/a;->y()F

    move-result v3

    mul-float/2addr v2, v3

    .line 245
    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 248
    invoke-virtual {p1}, LD/a;->p()V

    .line 249
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 250
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x1e01

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 253
    iget-object v1, p1, LD/a;->h:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 254
    iget-object v1, p1, LD/a;->d:LE/i;

    invoke-virtual {v1, p1}, LE/i;->d(LD/a;)V

    .line 255
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->h:LD/b;

    invoke-virtual {v1, v0}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 256
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 257
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_15

    .line 237
    :catchall_76
    move-exception v0

    :try_start_77
    monitor-exit p0
    :try_end_78
    .catchall {:try_start_77 .. :try_end_78} :catchall_76

    throw v0
.end method

.method public declared-synchronized a(LO/z;LO/N;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 95
    monitor-enter p0

    :try_start_1
    invoke-virtual {p2}, LO/N;->z()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/dL;->a(LO/z;I)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->g:Lo/X;

    .line 96
    invoke-virtual {p2}, LO/N;->a()Lo/T;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    .line 97
    const/high16 v0, -0x4080

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dL;->i:F
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 98
    monitor-exit p0

    return-void

    .line 95
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dL;->e()V

    .line 144
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dL;->h()V

    .line 145
    const/high16 v0, -0x4080

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dL;->i:F

    .line 146
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 102
    if-ne p0, p1, :cond_5

    .line 110
    :cond_4
    :goto_4
    return v0

    .line 105
    :cond_5
    instance-of v2, p1, Lcom/google/android/maps/driveabout/app/dL;

    if-eqz v2, :cond_21

    .line 106
    check-cast p1, Lcom/google/android/maps/driveabout/app/dL;

    .line 107
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    invoke-virtual {v2, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dL;->g:Lo/X;

    iget-object v3, p1, Lcom/google/android/maps/driveabout/app/dL;->g:Lo/X;

    invoke-virtual {v2, v3}, Lo/X;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1f
    move v0, v1

    goto :goto_4

    :cond_21
    move v0, v1

    .line 110
    goto :goto_4
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 115
    .line 116
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dL;->g:Lo/X;

    invoke-virtual {v0}, Lo/X;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dL;->f:Lo/T;

    invoke-virtual {v1}, Lo/T;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x25

    add-int/2addr v0, v1

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 138
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->o:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
