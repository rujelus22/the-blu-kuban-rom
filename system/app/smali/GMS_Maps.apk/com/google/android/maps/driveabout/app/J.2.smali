.class Lcom/google/android/maps/driveabout/app/J;
.super Lcom/google/android/maps/driveabout/app/Q;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/DestinationActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 381
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/Q;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 381
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/J;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 389
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 390
    new-instance v0, Lcom/google/android/maps/driveabout/app/cl;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/cl;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v3}, LaA/h;->b(LaA/e;Lcom/google/googlenav/android/aa;)V

    .line 393
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_50

    const/4 v0, 0x1

    .line 394
    :goto_22
    invoke-static {}, LaA/h;->b()LaA/h;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    if-eqz v0, :cond_52

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    :goto_38
    invoke-virtual {v1, v2, v3, v0}, LaA/h;->a(Landroid/app/ActionBar;LaA/g;Landroid/content/Context;)V

    .line 398
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/J;->c()Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/T;)Lcom/google/android/maps/driveabout/app/T;

    .line 401
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/bR;)Lcom/google/android/maps/driveabout/app/bR;

    .line 402
    return-void

    .line 393
    :cond_50
    const/4 v0, 0x0

    goto :goto_22

    .line 394
    :cond_52
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    goto :goto_38
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Z)Z

    .line 385
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/J;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->invalidateOptionsMenu()V

    .line 407
    return-void
.end method
