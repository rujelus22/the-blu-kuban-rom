.class public Lcom/google/android/maps/driveabout/app/NavigationActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/Y;


# static fields
.field private static a:J


# instance fields
.field private b:Z

.field private c:Z

.field private d:Landroid/view/ViewGroup;

.field private e:Landroid/view/ViewGroup;

.field private f:Lcom/google/android/maps/driveabout/app/NavigationView;

.field private g:Lcom/google/android/maps/driveabout/app/LoadingView;

.field private h:Lcom/google/android/maps/driveabout/app/an;

.field private i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

.field private j:Lcom/google/android/maps/driveabout/app/NavigationService;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Landroid/os/Handler;

.field private final p:Landroid/content/ServiceConnection;

.field private final q:Lcom/google/android/maps/driveabout/app/l;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 140
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 122
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m:Z

    .line 199
    new-instance v0, Lcom/google/android/maps/driveabout/app/bU;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/bU;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    .line 238
    new-instance v0, Lcom/google/android/maps/driveabout/app/l;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    return-void
.end method

.method static synthetic a(J)J
    .registers 4
    .parameter

    .prologue
    .line 122
    sget-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    add-long/2addr v0, p0

    sput-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/LoadingView;)Lcom/google/android/maps/driveabout/app/LoadingView;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/app/NavigationService;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-object p1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 988
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->k()LO/U;

    move-result-object v1

    .line 989
    :goto_b
    if-nez v1, :cond_10

    .line 992
    :goto_d
    return-object v0

    :cond_e
    move-object v1, v0

    .line 988
    goto :goto_b

    .line 992
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->l()I

    move-result v0

    invoke-static {v1, v0, p1}, Lcom/google/android/maps/driveabout/app/bn;->a(LO/U;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method

.method private a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 397
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 398
    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 399
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 400
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 122
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 946
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 948
    return-void
.end method

.method private a(I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 701
    .line 703
    sparse-switch p1, :sswitch_data_108

    .line 795
    :cond_5
    :goto_5
    :sswitch_5
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_16

    if-eqz v0, :cond_16

    .line 796
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->v()V

    .line 799
    :cond_16
    return v1

    .line 706
    :sswitch_17
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->onSearchRequested()Z

    goto :goto_5

    .line 709
    :sswitch_1b
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dM;->e()V

    goto :goto_5

    .line 712
    :sswitch_25
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dM;->j()V

    goto :goto_5

    .line 716
    :sswitch_2f
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dM;->h()V

    goto :goto_5

    .line 719
    :sswitch_39
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->a()Lo/aR;

    move-result-object v2

    .line 721
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/app/dM;->a(Lo/aR;)V

    goto :goto_5

    .line 724
    :sswitch_51
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 725
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dM;->d()V

    goto :goto_5

    .line 729
    :sswitch_61
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dM;->a(Z)V

    goto :goto_5

    .line 732
    :sswitch_6b
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Z)V

    goto :goto_5

    .line 746
    :sswitch_75
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dM;->i()V

    goto :goto_5

    .line 749
    :sswitch_7f
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {}, Lcom/google/googlenav/K;->ae()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_5

    .line 754
    :sswitch_93
    const-string v2, "A"

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 755
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/maps/driveabout/app/SettingsActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_5

    .line 758
    :sswitch_a4
    const-string v2, "I"

    invoke-static {v2, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 759
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_bb

    .line 760
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v3, Lcom/google/android/maps/driveabout/app/bY;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/bY;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/an;->b(Landroid/content/DialogInterface$OnClickListener;)V

    goto/16 :goto_5

    .line 769
    :cond_bb
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/dM;->r()Z

    goto/16 :goto_5

    .line 773
    :sswitch_c6
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    goto/16 :goto_5

    .line 776
    :sswitch_cb
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 778
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->w()V

    move v0, v1

    goto/16 :goto_5

    .line 782
    :sswitch_dd
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/maps/driveabout/app/dM;->g(Z)V

    goto/16 :goto_5

    .line 785
    :sswitch_e8
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dM;->g(Z)V

    goto/16 :goto_5

    .line 788
    :sswitch_f3
    invoke-static {}, Ll/f;->e()Ll/f;

    move-result-object v2

    invoke-virtual {v2}, Ll/f;->c()V

    goto/16 :goto_5

    .line 791
    :sswitch_fc
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->r()V

    goto/16 :goto_5

    .line 703
    nop

    :sswitch_data_108
    .sparse-switch
        0x7f1000f1 -> :sswitch_75
        0x7f1000f2 -> :sswitch_17
        0x7f1000f3 -> :sswitch_39
        0x7f1000f4 -> :sswitch_c6
        0x7f1000f5 -> :sswitch_93
        0x7f1000f6 -> :sswitch_7f
        0x7f100117 -> :sswitch_25
        0x7f100118 -> :sswitch_1b
        0x7f100119 -> :sswitch_51
        0x7f10011a -> :sswitch_cb
        0x7f10011c -> :sswitch_5
        0x7f10011d -> :sswitch_75
        0x7f10011f -> :sswitch_a4
        0x7f1004a1 -> :sswitch_2f
        0x7f1004a2 -> :sswitch_2f
        0x7f1004a3 -> :sswitch_17
        0x7f1004a4 -> :sswitch_93
        0x7f1004a5 -> :sswitch_61
        0x7f1004a6 -> :sswitch_6b
        0x7f1004a7 -> :sswitch_dd
        0x7f1004a8 -> :sswitch_e8
        0x7f1004a9 -> :sswitch_f3
        0x7f1004aa -> :sswitch_fc
    .end sparse-switch
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    return p1
.end method

.method public static b()J
    .registers 2

    .prologue
    .line 426
    sget-wide v0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationActivity;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/NavigationView;
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/NavigationMapView;
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Landroid/view/ViewGroup;
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private f()V
    .registers 3

    .prologue
    .line 328
    new-instance v0, Lcom/google/android/maps/driveabout/app/LoadingView;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/LoadingView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    .line 329
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cd;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/cd;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/LoadingView;->setOnFirstDrawListener(Lcom/google/android/maps/driveabout/app/bQ;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setContentView(Landroid/view/View;)V

    .line 346
    return-void
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/NavigationService;
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-object v0
.end method

.method private g()V
    .registers 5

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    .line 357
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    .line 363
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/driveabout/app/ce;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/ce;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LR/o;->a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    .line 370
    return-void
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Lcom/google/android/maps/driveabout/app/an;
    .registers 2
    .parameter

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    return-object v0
.end method

.method private h()V
    .registers 3

    .prologue
    .line 433
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    .line 434
    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    .line 437
    const/4 v0, 0x0

    .line 447
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v2, Lcom/google/android/maps/driveabout/app/cf;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/cf;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    new-instance v3, Lcom/google/android/maps/driveabout/app/cg;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/cg;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnClickListener;)V

    .line 460
    return-void
.end method

.method static synthetic i(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f()V

    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ch;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ch;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/bV;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/bV;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 476
    return-void
.end method

.method static synthetic j(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g()V

    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 482
    new-instance v0, Lcom/google/android/maps/driveabout/app/bW;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/bW;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 499
    return-void
.end method

.method static synthetic k(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l()V

    return-void
.end method

.method private l()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 519
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g:Lcom/google/android/maps/driveabout/app/LoadingView;

    if-eqz v0, :cond_a

    .line 567
    :cond_9
    :goto_9
    return-void

    .line 525
    :cond_a
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 526
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j()V

    goto :goto_9

    .line 531
    :cond_14
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c:Z

    if-nez v0, :cond_22

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 532
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i()V

    goto :goto_9

    .line 537
    :cond_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-nez v0, :cond_35

    .line 538
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 539
    new-instance v1, Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    .line 558
    :cond_35
    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    .line 559
    sget-object v0, Lcom/google/googlenav/z;->b:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    .line 562
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 563
    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    .line 564
    const-string v0, ")"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n_()V

    .line 566
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    goto :goto_9
.end method

.method static synthetic l(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h()V

    return-void
.end method

.method private m()Z
    .registers 3

    .prologue
    .line 649
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_8

    const/4 v0, 0x0

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x1

    goto :goto_7
.end method

.method private n()V
    .registers 4

    .prologue
    .line 959
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/NavigationView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    .line 961
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setDialogManager(Lcom/google/android/maps/driveabout/app/an;)V

    .line 962
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMapView(Lcom/google/android/maps/driveabout/app/NavigationMapView;Landroid/view/ViewGroup;)V

    .line 963
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setVisibility(I)V

    .line 964
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->m_()V

    .line 965
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 969
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_4a

    .line 970
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    const v2, 0x7f10010f

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Landroid/view/ViewGroup;)V

    .line 973
    :cond_4a
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/app/NavigationView;
    .registers 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-object v0
.end method

.method public a(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 984
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->o:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 985
    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 980
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->o:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 981
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 854
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 855
    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 857
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->stopService(Landroid/content/Intent;)Z

    .line 864
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    if-eqz v0, :cond_19

    .line 865
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 866
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    .line 868
    :cond_19
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->finish()V

    .line 869
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 951
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.maps.driveabout.app.STARTING_NAVIGATION_INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 952
    return-void
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 409
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 410
    invoke-static {}, Ll/f;->e()Ll/f;

    move-result-object v0

    .line 413
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_16

    if-eqz v0, :cond_16

    .line 414
    invoke-virtual {v0}, Ll/f;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 416
    :cond_16
    return-void
.end method

.method public e()Lcom/google/android/maps/driveabout/app/an;
    .registers 2

    .prologue
    .line 976
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    return-object v0
.end method

.method public getNfcUrl()Ljava/lang/String;
    .registers 3

    .prologue
    .line 999
    const-string v0, "n"

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1000
    if-nez v0, :cond_a

    .line 1001
    const/4 v0, 0x0

    .line 1007
    :goto_9
    return-object v0

    .line 1004
    :cond_a
    const-string v1, "N"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    goto :goto_9
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 925
    const/4 v0, 0x1

    if-ne p1, v0, :cond_d

    .line 926
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ca;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ca;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-static {p0, p2, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/eP;->a(Landroid/content/Context;ILandroid/content/Intent;Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/eS;)V

    .line 942
    :cond_d
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter

    .prologue
    .line 612
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 613
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_8

    .line 642
    :cond_7
    :goto_7
    return-void

    .line 616
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->j()V

    .line 627
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 630
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n()V

    .line 632
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_2b

    .line 635
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dM;->a(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    .line 636
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->a()V

    .line 639
    :cond_2b
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 640
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->v()V

    goto :goto_7
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/l;->a(Landroid/app/Activity;)V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 247
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 248
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v3, 0x48

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x80

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    .line 262
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setDefaultKeyMode(I)V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 270
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 271
    if-eqz v0, :cond_82

    const-string v3, "com.google.android.maps.driveabout.REPLAY_LOG"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_82

    move v0, v1

    .line 272
    :goto_50
    const-string v3, "Show Disclaimer"

    invoke-static {p0, v3, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_84

    if-nez v0, :cond_84

    :goto_5a
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    .line 273
    new-instance v0, Lcom/google/android/maps/driveabout/app/an;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    .line 275
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-eqz v0, :cond_86

    .line 277
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 278
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    new-instance v3, Lcom/google/android/maps/driveabout/app/cb;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/maps/driveabout/app/cb;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;J)V

    new-instance v0, Lcom/google/android/maps/driveabout/app/cc;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cc;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    invoke-virtual {v2, v3, v0}, Lcom/google/android/maps/driveabout/app/an;->b(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 321
    :goto_7a
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->o:Landroid/os/Handler;

    .line 322
    :goto_81
    return-void

    :cond_82
    move v0, v2

    .line 271
    goto :goto_50

    :cond_84
    move v1, v2

    .line 272
    goto :goto_5a

    .line 301
    :cond_86
    if-eqz p1, :cond_94

    const-string v0, "IsActivityRestart"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 306
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->g()V

    goto :goto_7a

    .line 307
    :cond_94
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x10

    and-int/2addr v0, v1

    if-eqz v0, :cond_ba

    invoke-static {p0}, Lcom/google/googlenav/android/W;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_ba

    .line 311
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 312
    const-class v1, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 313
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->startActivity(Landroid/content/Intent;)V

    .line 314
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->finish()V

    goto :goto_81

    .line 318
    :cond_ba
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f()V

    goto :goto_7a
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/dM;->b(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 677
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 679
    const/4 v0, 0x1

    .line 681
    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 883
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 884
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->a()V

    .line 885
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_11

    .line 886
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o_()V

    .line 888
    :cond_11
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_1a

    .line 889
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->f()V

    .line 891
    :cond_1a
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    .line 892
    if-eqz v0, :cond_24

    .line 893
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V

    .line 895
    :cond_24
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_27

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/RmiPreference;->d(Landroid/content/Context;)I

    move-result v0

    if-ne p1, v0, :cond_27

    .line 818
    const-string v0, "I"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;Z)V

    .line 819
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->r()Z

    move-result v0

    .line 821
    :goto_26
    return v0

    :cond_27
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_26
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 826
    const/4 v1, 0x4

    if-ne p1, v1, :cond_15

    .line 827
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v1, :cond_12

    .line 828
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/dM;->s()V

    .line 846
    :cond_11
    :goto_11
    return v0

    .line 834
    :cond_12
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k:Z

    goto :goto_11

    .line 836
    :cond_15
    const/16 v1, 0x52

    if-ne p1, v1, :cond_2d

    .line 837
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 839
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v1, :cond_11

    .line 840
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/dM;->t()V

    goto :goto_11

    .line 846
    :cond_2d
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_11
.end method

.method public onLowMemory()V
    .registers 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_9

    .line 805
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->i()V

    .line 807
    :cond_9
    return-void
.end method

.method public onMenuButtonClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 657
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 658
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_13

    .line 659
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dM;->t()V

    .line 664
    :cond_13
    :goto_13
    return-void

    .line 662
    :cond_14
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->openOptionsMenu()V

    goto :goto_13
.end method

.method public onMenuItemClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 670
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(I)Z

    .line 671
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 382
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 388
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 389
    const-string v1, "UserRequestedReroute"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setIntent(Landroid/content/Intent;)V

    .line 392
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(Landroid/content/Intent;)V

    .line 393
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 696
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 697
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a(I)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 571
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 572
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->c()V

    .line 574
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_4d

    .line 575
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->c()V

    .line 583
    :cond_12
    :goto_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_1b

    .line 584
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->i:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b()V

    .line 586
    :cond_1b
    const-string v0, "("

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 587
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    if-eqz v0, :cond_2c

    .line 588
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->p:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 589
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->n:Z

    .line 593
    :cond_2c
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 594
    invoke-static {p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;)V

    .line 596
    :cond_35
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l:Z

    .line 598
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_40

    .line 599
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->e()V

    .line 604
    :cond_40
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-eqz v0, :cond_4c

    .line 605
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->h:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->a()V

    .line 606
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    .line 608
    :cond_4c
    return-void

    .line 576
    :cond_4d
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->b:Z

    if-nez v0, :cond_12

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->c()Z

    move-result v0

    if-nez v0, :cond_57

    :cond_57
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_61

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c:Z

    if-eqz v0, :cond_12

    :cond_61
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 581
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->k:Z

    goto :goto_12
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_14

    .line 687
    new-instance v0, Lcom/google/android/maps/driveabout/app/bS;

    invoke-direct {v0, p1}, Lcom/google/android/maps/driveabout/app/bS;-><init>(Landroid/view/Menu;)V

    .line 688
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Lcom/google/android/maps/driveabout/app/bS;)V

    .line 689
    const/4 v0, 0x1

    .line 691
    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 503
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 504
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->q:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->b()V

    .line 505
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->l()V

    .line 508
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 509
    invoke-static {p0, p0}, Lcom/google/googlenav/android/X;->a(Landroid/app/Activity;Lcom/google/googlenav/android/Y;)V

    .line 511
    :cond_14
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 873
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 874
    const-string v0, "IsActivityRestart"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 875
    return-void
.end method

.method public startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 903
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->b()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 921
    :cond_10
    :goto_10
    return-void

    .line 906
    :cond_11
    if-nez p3, :cond_18

    .line 907
    new-instance p3, Landroid/os/Bundle;

    invoke-direct {p3}, Landroid/os/Bundle;-><init>()V

    .line 909
    :cond_18
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->f:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->c()Lo/ae;

    move-result-object v0

    .line 911
    new-instance v1, Lcom/google/android/maps/driveabout/app/bZ;

    invoke-direct {v1, p0, p1, p2, p4}, Lcom/google/android/maps/driveabout/app/bZ;-><init>(Lcom/google/android/maps/driveabout/app/NavigationActivity;Ljava/lang/String;ZZ)V

    .line 920
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationActivity;->j:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v2

    invoke-virtual {v2, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/dM;->a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eC;)V

    goto :goto_10
.end method
