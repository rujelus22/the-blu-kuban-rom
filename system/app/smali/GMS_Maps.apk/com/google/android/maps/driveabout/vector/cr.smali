.class public Lcom/google/android/maps/driveabout/vector/cr;
.super Lcom/google/android/maps/driveabout/vector/db;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/maps/driveabout/vector/da;

.field private b:Lcom/google/android/maps/driveabout/vector/cL;

.field private c:Lcom/google/android/maps/driveabout/vector/di;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/db;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/di;ZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/cL;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cr;->b:Lcom/google/android/maps/driveabout/vector/cL;

    if-nez v0, :cond_b

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/db;->a(Lcom/google/android/maps/driveabout/vector/di;ZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/cL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cr;->b:Lcom/google/android/maps/driveabout/vector/cL;

    .line 49
    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cr;->b:Lcom/google/android/maps/driveabout/vector/cL;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    monitor-exit p0

    return-object v0

    .line 44
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/di;IZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/da;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cr;->a:Lcom/google/android/maps/driveabout/vector/da;

    if-nez v0, :cond_11

    .line 25
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/db;->a(Lcom/google/android/maps/driveabout/vector/di;IZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/da;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cr;->a:Lcom/google/android/maps/driveabout/vector/da;

    .line 27
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cr;->c:Lcom/google/android/maps/driveabout/vector/di;

    .line 28
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/cr;->d:I

    .line 29
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/vector/cr;->e:Z

    .line 38
    :cond_11
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cr;->a:Lcom/google/android/maps/driveabout/vector/da;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_15

    monitor-exit p0

    return-object v0

    .line 24
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method
