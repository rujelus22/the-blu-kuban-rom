.class public Lcom/google/android/maps/driveabout/vector/cp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 993
    const v0, 0x73217bce

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->h:I

    .line 994
    const v0, 0x338cc6ef

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->i:I

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/vector/cp;
    .registers 2

    .prologue
    .line 1040
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->g:Z

    .line 1041
    return-object p0
.end method

.method public a(I)Lcom/google/android/maps/driveabout/vector/cp;
    .registers 3
    .parameter

    .prologue
    .line 1015
    invoke-virtual {p0, p1, p1}, Lcom/google/android/maps/driveabout/vector/cp;->a(II)Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lcom/google/android/maps/driveabout/vector/cp;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1023
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->d:Ljava/lang/Integer;

    .line 1024
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->e:Ljava/lang/Integer;

    .line 1025
    return-object p0
.end method

.method public a(Z)Lcom/google/android/maps/driveabout/vector/cp;
    .registers 3
    .parameter

    .prologue
    .line 997
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->a:Ljava/lang/Boolean;

    .line 998
    return-object p0
.end method

.method public b()Lcom/google/android/maps/driveabout/vector/cp;
    .registers 2

    .prologue
    .line 1045
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->g:Z

    .line 1046
    return-object p0
.end method

.method public b(II)Lcom/google/android/maps/driveabout/vector/cp;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1034
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/cp;->h:I

    .line 1035
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/cp;->i:I

    .line 1036
    return-object p0
.end method

.method public b(Z)Lcom/google/android/maps/driveabout/vector/cp;
    .registers 3
    .parameter

    .prologue
    .line 1002
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->b:Ljava/lang/Boolean;

    .line 1003
    return-object p0
.end method

.method public c()Lcom/google/android/maps/driveabout/vector/co;
    .registers 11

    .prologue
    const/4 v7, 0x0

    .line 1050
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2d

    const/4 v0, 0x1

    :goto_6
    const-string v1, "Texture ID must be specified."

    invoke-static {v0, v1}, Lcom/google/common/base/P;->b(ZLjava/lang/Object;)V

    .line 1051
    new-instance v0, Lcom/google/android/maps/driveabout/vector/co;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cp;->a:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cp;->b:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cp;->c:Ljava/lang/Boolean;

    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/cp;->g:Z

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/cp;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/cp;->e:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/cp;->f:Ljava/lang/Integer;

    if-nez v8, :cond_2f

    :goto_25
    iget v8, p0, Lcom/google/android/maps/driveabout/vector/cp;->h:I

    iget v9, p0, Lcom/google/android/maps/driveabout/vector/cp;->i:I

    invoke-direct/range {v0 .. v9}, Lcom/google/android/maps/driveabout/vector/co;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIII)V

    return-object v0

    :cond_2d
    move v0, v7

    .line 1050
    goto :goto_6

    .line 1051
    :cond_2f
    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/cp;->f:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_25
.end method

.method public c(Z)Lcom/google/android/maps/driveabout/vector/cp;
    .registers 3
    .parameter

    .prologue
    .line 1007
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cp;->c:Ljava/lang/Boolean;

    .line 1008
    return-object p0
.end method
