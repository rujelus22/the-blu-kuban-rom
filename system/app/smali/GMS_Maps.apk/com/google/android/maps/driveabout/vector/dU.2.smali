.class final enum Lcom/google/android/maps/driveabout/vector/dU;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/maps/driveabout/vector/dU;

.field public static final enum b:Lcom/google/android/maps/driveabout/vector/dU;

.field public static final enum c:Lcom/google/android/maps/driveabout/vector/dU;

.field public static final enum d:Lcom/google/android/maps/driveabout/vector/dU;

.field private static final synthetic e:[Lcom/google/android/maps/driveabout/vector/dU;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dU;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dU;->a:Lcom/google/android/maps/driveabout/vector/dU;

    .line 179
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dU;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/dU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dU;->b:Lcom/google/android/maps/driveabout/vector/dU;

    .line 180
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dU;

    const-string v1, "ZOOM"

    invoke-direct {v0, v1, v4}, Lcom/google/android/maps/driveabout/vector/dU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dU;->c:Lcom/google/android/maps/driveabout/vector/dU;

    .line 181
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dU;

    const-string v1, "ROTATE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/maps/driveabout/vector/dU;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dU;->d:Lcom/google/android/maps/driveabout/vector/dU;

    .line 177
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/dU;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/dU;->a:Lcom/google/android/maps/driveabout/vector/dU;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/maps/driveabout/vector/dU;->b:Lcom/google/android/maps/driveabout/vector/dU;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/dU;->c:Lcom/google/android/maps/driveabout/vector/dU;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/dU;->d:Lcom/google/android/maps/driveabout/vector/dU;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dU;->e:[Lcom/google/android/maps/driveabout/vector/dU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 177
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dU;
    .registers 2
    .parameter

    .prologue
    .line 177
    const-class v0, Lcom/google/android/maps/driveabout/vector/dU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dU;

    return-object v0
.end method

.method public static values()[Lcom/google/android/maps/driveabout/vector/dU;
    .registers 1

    .prologue
    .line 177
    sget-object v0, Lcom/google/android/maps/driveabout/vector/dU;->e:[Lcom/google/android/maps/driveabout/vector/dU;

    invoke-virtual {v0}, [Lcom/google/android/maps/driveabout/vector/dU;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/dU;

    return-object v0
.end method
