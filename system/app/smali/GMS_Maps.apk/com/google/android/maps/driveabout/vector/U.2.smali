.class public Lcom/google/android/maps/driveabout/vector/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/aU;


# static fields
.field static final a:I


# instance fields
.field final b:Lcom/google/android/maps/driveabout/vector/w;

.field protected c:Lcom/google/android/maps/driveabout/vector/v;

.field private d:I

.field private e:J

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 45
    invoke-static {}, Lcom/google/googlenav/android/a;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0x10

    :goto_8
    sput v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    return-void

    :cond_b
    const/16 v0, 0x14

    goto :goto_8
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/w;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z

    .line 120
    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(I)V

    .line 121
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/u;->b:Lcom/google/android/maps/driveabout/vector/w;

    .line 122
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 242
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 243
    const v1, 0x3e6b8520

    cmpg-float v1, v0, v1

    if-gez v1, :cond_24

    .line 244
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    int-to-float v0, v0

    const v1, 0x3f8ccccd

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(I)V

    .line 248
    :cond_1d
    :goto_1d
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/u;->l:I

    .line 249
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    .line 250
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    .line 251
    return-void

    .line 245
    :cond_24
    const v1, 0x3ebd70a4

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1d

    .line 246
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    int-to-float v0, v0

    const v1, 0x3f666666

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(I)V

    goto :goto_1d
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 152
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    .line 153
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/u;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/u;->e:J

    .line 154
    return-void
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 364
    monitor-enter p0

    .line 365
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_a

    .line 366
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/v;->a(J)V

    .line 368
    :cond_a
    monitor-exit p0

    .line 369
    return-void

    .line 368
    :catchall_c
    move-exception v0

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_c

    throw v0
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 214
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    .line 215
    return-void
.end method

.method public a(ZZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 286
    monitor-enter p0

    .line 287
    if-eqz p1, :cond_7

    .line 288
    const/4 v0, 0x1

    :try_start_4
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->c(Z)V

    .line 290
    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_17

    .line 291
    if-eqz p2, :cond_12

    .line 292
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->d()V

    .line 294
    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->e()V

    .line 296
    :cond_17
    monitor-exit p0

    .line 297
    return-void

    .line 296
    :catchall_19
    move-exception v0

    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_4 .. :try_end_1b} :catchall_19

    throw v0
.end method

.method public a(I)Z
    .registers 4
    .parameter

    .prologue
    .line 134
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    add-int/2addr v0, p1

    .line 138
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    if-eqz v1, :cond_c

    const v1, 0x88b8

    if-gt v0, v1, :cond_10

    .line 139
    :cond_c
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    .line 140
    const/4 v0, 0x1

    .line 142
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public b()V
    .registers 7

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/u;->d()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/u;->e:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit8 v1, v0, 0x5

    .line 162
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    if-eqz v0, :cond_5d

    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    .line 163
    :goto_10
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->h:Z

    if-eqz v2, :cond_16

    .line 164
    add-int/lit16 v0, v0, 0x1f4

    .line 169
    :cond_16
    const/16 v2, 0xf

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 171
    monitor-enter p0

    .line 172
    :try_start_1d
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v2, :cond_3c

    .line 173
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->c()I

    .line 174
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->i:Z

    if-nez v2, :cond_7c

    .line 175
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    if-eqz v2, :cond_60

    .line 176
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->e()V

    .line 180
    :cond_33
    :goto_33
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/u;->d(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/v;->a(I)V

    .line 186
    :cond_3c
    :goto_3c
    monitor-exit p0
    :try_end_3d
    .catchall {:try_start_1d .. :try_end_3d} :catchall_85

    .line 197
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->h:Z

    if-nez v2, :cond_5c

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->g:Z

    if-nez v2, :cond_5c

    .line 199
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/u;->j:I

    .line 200
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->k:I

    .line 201
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->l:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_5c

    .line 202
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/u;->j()V

    .line 205
    :cond_5c
    return-void

    .line 162
    :cond_5d
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    goto :goto_10

    .line 177
    :cond_60
    :try_start_60
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->f()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v2, v2, v4

    if-eqz v2, :cond_33

    .line 178
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->f()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    goto :goto_33

    .line 182
    :cond_7c
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/v;->d()V

    .line 183
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/u;->i:Z

    goto :goto_3c

    .line 186
    :catchall_85
    move-exception v0

    monitor-exit p0
    :try_end_87
    .catchall {:try_start_60 .. :try_end_87} :catchall_85

    throw v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 147
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->d:I

    .line 148
    return-void
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 222
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/u;->h:Z

    .line 223
    return-void
.end method

.method public declared-synchronized c()V
    .registers 2

    .prologue
    .line 230
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->i:Z

    .line 231
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_d

    .line 232
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->d()V
    :try_end_d
    .catchall {:try_start_2 .. :try_end_d} :catchall_f

    .line 234
    :cond_d
    monitor-exit p0

    return-void

    .line 230
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c(I)V
    .registers 3
    .parameter

    .prologue
    .line 255
    sget v0, Lcom/google/android/maps/driveabout/vector/u;->a:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/u;->f:I

    .line 256
    return-void
.end method

.method public declared-synchronized c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 336
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 337
    monitor-exit p0

    return-void

    .line 336
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d(I)I
    .registers 2
    .parameter

    .prologue
    .line 279
    return p1
.end method

.method d()J
    .registers 3

    .prologue
    .line 264
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()V
    .registers 2

    .prologue
    .line 305
    monitor-enter p0

    .line 306
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_c

    .line 307
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->a()V

    .line 311
    :goto_a
    monitor-exit p0

    .line 312
    return-void

    .line 309
    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->b:Lcom/google/android/maps/driveabout/vector/w;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/w;->q_()V

    goto :goto_a

    .line 311
    :catchall_12
    move-exception v0

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_12

    throw v0
.end method

.method public f()V
    .registers 2

    .prologue
    .line 315
    monitor-enter p0

    .line 316
    :try_start_1
    new-instance v0, Lcom/google/android/maps/driveabout/vector/v;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/v;-><init>(Lcom/google/android/maps/driveabout/vector/u;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    .line 317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->start()V

    .line 318
    monitor-exit p0

    .line 319
    return-void

    .line 318
    :catchall_f
    move-exception v0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public g()V
    .registers 2

    .prologue
    .line 322
    monitor-enter p0

    .line 323
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    if-eqz v0, :cond_d

    .line 324
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/v;->b()V

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    .line 327
    :cond_d
    monitor-exit p0

    .line 328
    return-void

    .line 327
    :catchall_f
    move-exception v0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public declared-synchronized h()Z
    .registers 3

    .prologue
    .line 344
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z

    .line 345
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/u;->m:Z
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 346
    monitor-exit p0

    return v0

    .line 344
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 376
    monitor-enter p0

    .line 377
    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/u;->c:Lcom/google/android/maps/driveabout/vector/v;

    .line 378
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_e

    .line 379
    if-eqz v0, :cond_c

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/v;->a(Lcom/google/android/maps/driveabout/vector/v;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    .line 378
    :catchall_e
    move-exception v0

    :try_start_f
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_f .. :try_end_10} :catchall_e

    throw v0

    .line 379
    :cond_11
    const/4 v0, 0x0

    goto :goto_d
.end method
