.class public Lcom/google/android/maps/driveabout/app/NavigationView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/app/cQ;


# instance fields
.field private A:Landroid/widget/LinearLayout;

.field private B:Landroid/view/View;

.field private C:Landroid/widget/LinearLayout;

.field private D:Landroid/view/animation/Animation;

.field private E:Landroid/view/animation/Animation;

.field private F:Landroid/widget/TextView;

.field private G:Lcom/google/android/maps/driveabout/app/cO;

.field private H:Landroid/widget/TextView;

.field private I:Lcom/google/android/maps/driveabout/app/cO;

.field private J:Landroid/widget/RelativeLayout;

.field private K:Landroid/view/View;

.field private L:Landroid/view/View;

.field private M:Landroid/view/View;

.field private N:Landroid/view/View;

.field private O:Landroid/view/View;

.field private P:Landroid/view/View;

.field private Q:Landroid/widget/ImageView;

.field private R:Landroid/view/View;

.field private S:Landroid/view/View;

.field private T:Landroid/view/View;

.field private U:Landroid/widget/Button;

.field private V:Landroid/widget/Button;

.field private W:Ljava/lang/Runnable;

.field private Z:J

.field private a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

.field private aa:Landroid/location/Location;

.field private ab:Z

.field private ac:I

.field private final ad:Lcom/google/android/maps/driveabout/app/dx;

.field private ae:Landroid/view/View$OnClickListener;

.field private af:Landroid/view/View$OnClickListener;

.field private final ag:I

.field private ah:Landroid/content/DialogInterface$OnClickListener;

.field private ai:Landroid/view/View$OnTouchListener;

.field private final b:Lcom/google/android/maps/driveabout/app/aI;

.field private c:Landroid/widget/ListView;

.field private d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

.field private e:Lcom/google/android/maps/driveabout/app/an;

.field private f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

.field private g:Lcom/google/android/maps/driveabout/app/cO;

.field private h:Lcom/google/android/maps/driveabout/app/TopBarView;

.field private i:Landroid/view/ViewGroup;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View$OnClickListener;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/view/View;

.field private r:Landroid/view/ViewGroup;

.field private s:Landroid/view/ViewGroup;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/ImageView;

.field private z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ab:Z

    .line 215
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    .line 216
    invoke-static {}, Lcom/google/android/maps/driveabout/app/dx;->a()Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dx;

    .line 217
    new-instance v0, Lcom/google/android/maps/driveabout/app/aI;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/aI;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    .line 218
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    .line 219
    const-string v0, "/"

    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;I)V

    .line 220
    return-void
.end method

.method private K()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 376
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 377
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setDrawingCacheEnabled(Z)V

    .line 378
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setDrawingCacheEnabled(Z)V

    .line 379
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 380
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 381
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 382
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 383
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 384
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 385
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 386
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setDrawingCacheEnabled(Z)V

    .line 387
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 388
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 389
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 390
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 391
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDrawingCacheEnabled(Z)V

    .line 392
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 393
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setDrawingCacheEnabled(Z)V

    .line 394
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setDrawingCacheEnabled(Z)V

    .line 395
    return-void
.end method

.method private L()Z
    .registers 4

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->g()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    move-result v0

    .line 920
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->n()LC/a;

    move-result-object v2

    invoke-virtual {v2}, LC/a;->h()Lo/T;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lo/T;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_28

    const/4 v0, 0x1

    :goto_27
    return v0

    :cond_28
    const/4 v0, 0x0

    goto :goto_27
.end method

.method private M()Z
    .registers 3

    .prologue
    .line 925
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->g()F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    .line 927
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->e()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1e

    const/4 v0, 0x1

    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method private N()V
    .registers 4

    .prologue
    .line 932
    new-instance v0, Lcom/google/android/maps/driveabout/app/cK;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cK;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    .line 945
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    const-wide/16 v1, 0x1388

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/NavigationView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 946
    return-void
.end method

.method private O()V
    .registers 6

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1044
    const v0, 0x7f100102

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1045
    const v0, 0x7f1000eb

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    .line 1046
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v1, 0x7f1000ed

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1048
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v2, 0x7f1000ee

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    .line 1050
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v2, 0x7f1000ef

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    .line 1051
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const v2, 0x7f1000ec

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    .line 1053
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6c

    .line 1054
    if-eqz v0, :cond_51

    .line 1055
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1057
    :cond_51
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1066
    :goto_56
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1067
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1068
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1069
    return-void

    .line 1059
    :cond_6c
    if-eqz v0, :cond_71

    .line 1060
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1062
    :cond_71
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_56
.end method

.method private P()V
    .registers 5

    .prologue
    .line 1110
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1111
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->i()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 1112
    const v1, 0x7f0b0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 1114
    const v2, 0x7f0b0014

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1126
    :goto_1a
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    if-eqz v2, :cond_37

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_37

    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_37

    .line 1128
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 1130
    add-int/2addr v0, v2

    .line 1132
    :cond_37
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v2, :cond_40

    .line 1133
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCompassMargins(II)V

    .line 1135
    :cond_40
    return-void

    .line 1117
    :cond_41
    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 1119
    const v2, 0x7f0b0016

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_1a
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ae:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 648
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_7
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_7} :catch_8

    .line 652
    :goto_7
    return-void

    .line 649
    :catch_8
    move-exception v0

    .line 650
    const-string v0, "Navigation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to start activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->a(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->af:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private b(I)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1078
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_14

    .line 1079
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1081
    :cond_14
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_26

    .line 1082
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/TopBarView;->setVisibility(I)V

    .line 1083
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1085
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->c(Landroid/view/View;I)V

    .line 1087
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->s:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eD;->c(Landroid/view/View;I)V

    .line 1088
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    .line 1089
    return-void
.end method

.method private c(I)Landroid/view/View;
    .registers 3
    .parameter

    .prologue
    .line 1138
    const/4 v0, 0x0

    .line 1139
    packed-switch p1, :pswitch_data_e

    .line 1152
    :goto_4
    return-object v0

    .line 1143
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    goto :goto_4

    .line 1146
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    goto :goto_4

    .line 1149
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    goto :goto_4

    .line 1139
    :pswitch_data_e
    .packed-switch 0x1
        :pswitch_5
        :pswitch_8
        :pswitch_5
        :pswitch_5
        :pswitch_b
    .end packed-switch
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/NavigationView;)J
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Z:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/NavigationView;)Landroid/location/Location;
    .registers 2
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->aa:Landroid/location/Location;

    return-object v0
.end method


# virtual methods
.method public A()V
    .registers 3

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 831
    return-void
.end method

.method public B()V
    .registers 3

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 836
    return-void
.end method

.method public C()V
    .registers 3

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 841
    return-void
.end method

.method public D()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 845
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 846
    return-void
.end method

.method public E()V
    .registers 4

    .prologue
    .line 850
    const/4 v0, 0x4

    .line 851
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 852
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 854
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCopyrightPadding(II)V

    .line 855
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 858
    :cond_23
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    const v2, 0x7f050008

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 859
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->J:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->d(Landroid/view/View;I)V

    .line 860
    return-void
.end method

.method public F()V
    .registers 4

    .prologue
    .line 864
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 866
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 867
    add-int/lit8 v0, v0, 0x4

    .line 868
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 870
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCopyrightPadding(II)V

    .line 871
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 874
    :cond_2f
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    const v2, 0x7f05000d

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 875
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->J:Landroid/widget/RelativeLayout;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->d(Landroid/view/View;I)V

    .line 876
    return-void
.end method

.method public G()V
    .registers 5

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 1018
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1a

    .line 1019
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setVisibility(I)V

    .line 1020
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1021
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->a()V

    .line 1023
    :cond_1a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    if-nez v0, :cond_21

    .line 1024
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->O()V

    .line 1026
    :cond_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2e

    .line 1027
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1030
    :cond_2e
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1032
    iget v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_4c

    .line 1033
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->c(Landroid/view/View;I)V

    .line 1038
    :goto_43
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->s:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->c(Landroid/view/View;I)V

    .line 1039
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    .line 1040
    return-void

    .line 1035
    :cond_4c
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    invoke-static {v1, v3}, Lcom/google/android/maps/driveabout/app/eD;->c(Landroid/view/View;I)V

    goto :goto_43
.end method

.method public H()V
    .registers 3

    .prologue
    .line 1322
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(Z)V

    .line 1323
    return-void
.end method

.method public I()V
    .registers 2

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->p()V

    .line 1333
    return-void
.end method

.method public J()V
    .registers 2

    .prologue
    .line 1347
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->h()V

    .line 1348
    return-void
.end method

.method public a()Lcom/google/android/maps/driveabout/app/NavigationMapView;
    .registers 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    return-object v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/an;->a(I)V

    .line 563
    return-void
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 599
    const v0, 0x7f0d0048

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->a(IIILandroid/content/Intent;)V

    .line 600
    return-void
.end method

.method public a(IIILandroid/content/Intent;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/maps/driveabout/app/cG;

    invoke-direct {v5, p0, p4}, Lcom/google/android/maps/driveabout/app/cG;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V

    new-instance v6, Lcom/google/android/maps/driveabout/app/cH;

    invoke-direct {v6, p0, p4}, Lcom/google/android/maps/driveabout/app/cH;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V

    move v1, p1

    move v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/an;->a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 628
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 632
    new-instance v0, Lcom/google/android/maps/driveabout/app/cI;

    invoke-direct {v0, p0, p3}, Lcom/google/android/maps/driveabout/app/cI;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/content/Intent;)V

    .line 643
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/maps/driveabout/app/an;->a(IILandroid/content/DialogInterface$OnClickListener;)V

    .line 644
    return-void
.end method

.method public a(IIZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1463
    const/4 v0, 0x2

    if-ne p2, v0, :cond_c

    .line 1464
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02013e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1488
    :goto_b
    return-void

    .line 1465
    :cond_c
    const/4 v0, 0x3

    if-ne p2, v0, :cond_18

    .line 1466
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f0202b9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1467
    :cond_18
    if-nez p3, :cond_23

    .line 1468
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f020103

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1470
    :cond_23
    packed-switch p1, :pswitch_data_54

    .line 1485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1472
    :pswitch_2f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1475
    :pswitch_38
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1478
    :pswitch_41
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1482
    :pswitch_4a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    const v1, 0x7f02042d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_b

    .line 1470
    nop

    :pswitch_data_54
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_4a
        :pswitch_41
        :pswitch_38
        :pswitch_2f
    .end packed-switch
.end method

.method public a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/an;->a(ILandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 527
    return-void
.end method

.method public a(LO/U;)V
    .registers 3
    .parameter

    .prologue
    .line 712
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p0}, Lcom/google/android/maps/driveabout/app/an;->a(LO/U;Lcom/google/android/maps/driveabout/app/cQ;)V

    .line 713
    return-void
.end method

.method public a(LO/z;[LO/z;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 969
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(LO/z;[LO/z;)V

    .line 970
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 971
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    .line 972
    return-void
.end method

.method public a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 702
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/an;->c(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 703
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 4
    .parameter

    .prologue
    .line 961
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Lcom/google/android/maps/driveabout/app/aQ;)V

    .line 962
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 963
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    .line 964
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 980
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cO;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 981
    const/4 p4, 0x0

    .line 983
    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Lcom/google/android/maps/driveabout/app/aQ;LO/N;ZZ)V

    .line 985
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 986
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    .line 987
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    .line 988
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/maps/driveabout/app/an;->a(Lcom/google/android/maps/driveabout/app/bI;Lcom/google/android/maps/driveabout/app/bI;IZZZLcom/google/android/maps/driveabout/app/by;)V

    .line 692
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bb;)V
    .registers 3
    .parameter

    .prologue
    .line 1715
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 1716
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;)V
    .registers 3
    .parameter

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Lcom/google/android/maps/driveabout/vector/c;)V

    .line 1328
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    .line 717
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->F:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 718
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->G:Lcom/google/android/maps/driveabout/app/cO;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(I)V

    .line 719
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1372
    .line 1374
    new-instance v0, Lcom/google/android/maps/driveabout/app/cN;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/maps/driveabout/app/cN;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->post(Ljava/lang/Runnable;)Z

    .line 1380
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 992
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;Z)V

    .line 993
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 994
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    .line 995
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    .line 996
    return-void
.end method

.method public a(Lo/o;)V
    .registers 3
    .parameter

    .prologue
    .line 707
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/google/android/maps/driveabout/app/bp;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cQ;Lo/o;)V

    .line 708
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 532
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/an;->a(Z)V

    .line 533
    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 1002
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1003
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    .line 1004
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    .line 1005
    return-void
.end method

.method public a(ZZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const v3, 0x7f020117

    const v2, 0x7f020113

    .line 1448
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const v1, 0x7f100119

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;

    .line 1450
    if-eqz p1, :cond_2a

    .line 1451
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1452
    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    .line 1453
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0076

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    .line 1459
    :goto_29
    return-void

    .line 1455
    :cond_2a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1456
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setIcon(I)V

    .line 1457
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0077

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/NavigationMenuItem;->setText(Ljava/lang/String;)V

    goto :goto_29
.end method

.method public a(ZZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 893
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/maps/driveabout/app/TopBarView;->setButtonVisibility(ZZZ)V

    .line 895
    if-eqz p3, :cond_2a

    .line 896
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->L()Z

    move-result v0

    if-nez v0, :cond_38

    move v0, v1

    :goto_12
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 897
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->M()Z

    move-result v3

    if-nez v3, :cond_3a

    :goto_1d
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 898
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 899
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 901
    :cond_2a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Z:J

    .line 902
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    if-nez v0, :cond_37

    .line 903
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->N()V

    .line 905
    :cond_37
    return-void

    :cond_38
    move v0, v2

    .line 896
    goto :goto_12

    :cond_3a
    move v1, v2

    .line 897
    goto :goto_1d
.end method

.method public a([LO/U;ZZLcom/google/android/maps/driveabout/app/cR;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 574
    .line 575
    if-eqz p3, :cond_8

    .line 577
    new-instance v0, Lcom/google/android/maps/driveabout/app/cF;

    invoke-direct {v0, p0, p4}, Lcom/google/android/maps/driveabout/app/cF;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Lcom/google/android/maps/driveabout/app/cR;)V

    move-object p4, v0

    .line 588
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p4}, Lcom/google/android/maps/driveabout/app/an;->a([LO/U;ILcom/google/android/maps/driveabout/app/cR;)V

    .line 590
    return-void
.end method

.method public a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/an;->a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V

    .line 546
    return-void
.end method

.method public a(FF)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 489
    .line 496
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_37

    .line 497
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    .line 506
    :goto_c
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 507
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 508
    aget v4, v3, v2

    .line 509
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v4

    .line 510
    aget v3, v3, v1

    .line 511
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v3

    .line 513
    int-to-float v4, v4

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_4d

    int-to-float v4, v5

    cmpg-float v4, p1, v4

    if-gtz v4, :cond_4d

    int-to-float v3, v3

    cmpl-float v3, p2, v3

    if-ltz v3, :cond_4d

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_4d

    move v0, v1

    :goto_35
    move v2, v0

    .line 518
    :cond_36
    return v2

    .line 498
    :cond_37
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_42

    .line 499
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    goto :goto_c

    .line 500
    :cond_42
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_36

    .line 501
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    goto :goto_c

    :cond_4d
    move v0, v2

    goto :goto_35
.end method

.method public a(LO/z;)Z
    .registers 3
    .parameter

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/aI;->a(LO/z;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/io/OutputStream;)Z
    .registers 13
    .parameter

    .prologue
    .line 1399
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getWidth()I

    move-result v0

    .line 1400
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getHeight()I

    move-result v1

    .line 1401
    if-eqz v0, :cond_c

    if-nez v1, :cond_e

    .line 1402
    :cond_c
    const/4 v0, 0x0

    .line 1443
    :goto_d
    return v0

    .line 1406
    :cond_e
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1407
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1408
    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/app/NavigationView;->draw(Landroid/graphics/Canvas;)V

    .line 1412
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->l()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1413
    if-eqz v8, :cond_a7

    .line 1416
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1417
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v2, v1

    .line 1418
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v4, v1

    .line 1419
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->getHeight()I

    move-result v1

    int-to-float v5, v1

    .line 1420
    const/16 v1, 0x8

    new-array v1, v1, [F

    const/4 v3, 0x0

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x1

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x2

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x3

    aput v4, v1, v3

    const/4 v3, 0x4

    aput v2, v1, v3

    const/4 v3, 0x5

    const/4 v9, 0x0

    aput v9, v1, v3

    const/4 v3, 0x6

    aput v2, v1, v3

    const/4 v3, 0x7

    aput v4, v1, v3

    .line 1421
    const/16 v3, 0x8

    new-array v3, v3, [F

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v3, v9

    const/4 v9, 0x1

    add-float v10, v4, v5

    aput v10, v3, v9

    const/4 v9, 0x2

    const/4 v10, 0x0

    aput v10, v3, v9

    const/4 v9, 0x3

    aput v5, v3, v9

    const/4 v9, 0x4

    aput v2, v3, v9

    const/4 v9, 0x5

    add-float/2addr v4, v5

    aput v4, v3, v9

    const/4 v4, 0x6

    aput v2, v3, v4

    const/4 v2, 0x7

    aput v5, v3, v2

    .line 1422
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->setPolyToPoly([FI[FII)Z

    .line 1425
    new-instance v1, Landroid/graphics/ColorMatrix;

    const/16 v2, 0x14

    new-array v2, v2, [F

    fill-array-data v2, :array_b2

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 1434
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 1435
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 1436
    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1439
    invoke-virtual {v7, v8, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 1442
    :cond_a7
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {v6, v0, v1, p1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1443
    const/4 v0, 0x1

    goto/16 :goto_d

    .line 1425
    nop

    :array_b2
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public b(LO/z;)V
    .registers 3
    .parameter

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(LO/z;)V

    .line 1343
    return-void
.end method

.method public b(LO/z;[LO/z;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(LO/z;[LO/z;)V

    .line 1302
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/app/aQ;)V
    .registers 4
    .parameter

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->setRoute(LO/z;)V

    .line 1317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(I)V

    .line 1318
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/bb;)V
    .registers 3
    .parameter

    .prologue
    .line 1720
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 1721
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->a(Ljava/lang/String;)V

    .line 1010
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1012
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b(I)V

    .line 1013
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->P()V

    .line 1014
    return-void
.end method

.method public b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 729
    if-eqz p1, :cond_12

    .line 730
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    const v1, 0x7f0d0078

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 734
    :goto_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->I:Lcom/google/android/maps/driveabout/app/cO;

    const/16 v1, 0x9c4

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(I)V

    .line 735
    return-void

    .line 732
    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_a
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1a

    :cond_18
    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method public c()V
    .registers 3

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_17

    .line 437
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 440
    :cond_17
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1367
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->a(Ljava/lang/String;I)V

    .line 1368
    return-void
.end method

.method public c(Z)V
    .registers 4
    .parameter

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 755
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 756
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_16

    .line 445
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 446
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 448
    :cond_16
    return-void
.end method

.method public d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    new-instance v1, Lcom/google/android/maps/driveabout/app/cJ;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/cJ;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Z)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 774
    return-void
.end method

.method public e(Z)V
    .registers 4
    .parameter

    .prologue
    .line 778
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 779
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 780
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public f()V
    .registers 3

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_17

    .line 458
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 461
    :cond_17
    return-void
.end method

.method public f(Z)V
    .registers 6
    .parameter

    .prologue
    const v1, 0x7f050011

    const/4 v2, 0x0

    .line 950
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, v2, v2, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setButtonVisibility(ZZZ)V

    .line 951
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    if-eqz p1, :cond_25

    move v0, v1

    :goto_e
    invoke-static {v3, v0}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 952
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    if-eqz p1, :cond_27

    :goto_15
    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 953
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    if-eqz v0, :cond_24

    .line 954
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 955
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->W:Ljava/lang/Runnable;

    .line 957
    :cond_24
    return-void

    :cond_25
    move v0, v2

    .line 951
    goto :goto_e

    :cond_27
    move v1, v2

    .line 952
    goto :goto_15
.end method

.method public g()V
    .registers 3

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_16

    .line 466
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 467
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 469
    :cond_16
    return-void
.end method

.method public g(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->a(Z)V

    .line 1307
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_17

    .line 474
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 477
    :cond_17
    return-void
.end method

.method public h(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->b(Z)V

    .line 1385
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_16

    .line 482
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 483
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 485
    :cond_16
    return-void
.end method

.method public j()V
    .registers 2

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->b()V

    .line 568
    return-void
.end method

.method public k()V
    .registers 2

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->d()V

    .line 595
    return-void
.end method

.method public l()V
    .registers 3

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ah:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;)V

    .line 657
    return-void
.end method

.method public m()V
    .registers 2

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->r()V

    .line 697
    return-void
.end method

.method public n()V
    .registers 3

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->G:Lcom/google/android/maps/driveabout/app/cO;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(Z)V

    .line 725
    return-void
.end method

.method public o()V
    .registers 3

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->I:Lcom/google/android/maps/driveabout/app/cO;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/cO;->a(Z)V

    .line 740
    return-void
.end method

.method public onFinishInflate()V
    .registers 7

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 228
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 229
    const v0, 0x7f100103

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->s:Landroid/view/ViewGroup;

    .line 230
    const v0, 0x7f100106

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    .line 231
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 232
    const v0, 0x7f10010e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/TopBarView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    .line 234
    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    const v0, 0x7f100108

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f100109

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v4, v0, v1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setConnectedViews(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 237
    const v0, 0x7f100104

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->t:Landroid/view/View;

    .line 238
    const v0, 0x7f100107

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    .line 245
    const v0, 0x7f10010b

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->q:Landroid/view/View;

    .line 247
    const v0, 0x7f1000ff

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 248
    if-eqz v0, :cond_6e

    .line 249
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 251
    :cond_6e
    const v0, 0x7f100145

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    .line 252
    const v0, 0x7f10010a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    .line 254
    new-instance v0, Lcom/google/android/maps/driveabout/app/cO;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    const/4 v4, 0x1

    invoke-direct {v0, p0, v1, v4}, Lcom/google/android/maps/driveabout/app/cO;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->g:Lcom/google/android/maps/driveabout/app/cO;

    .line 255
    const v0, 0x7f100146

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    .line 256
    const v0, 0x7f100147

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->y:Landroid/widget/ImageView;

    .line 257
    const v0, 0x7f100148

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    .line 258
    const v0, 0x7f100149

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    .line 260
    const v0, 0x7f10014a

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    .line 261
    const v0, 0x7f10010c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->F:Landroid/widget/TextView;

    .line 263
    new-instance v0, Lcom/google/android/maps/driveabout/app/cO;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->F:Landroid/widget/TextView;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/maps/driveabout/app/cO;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->G:Lcom/google/android/maps/driveabout/app/cO;

    .line 264
    const v0, 0x7f10010d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    .line 266
    new-instance v0, Lcom/google/android/maps/driveabout/app/cO;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->H:Landroid/widget/TextView;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/maps/driveabout/app/cO;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->I:Lcom/google/android/maps/driveabout/app/cO;

    .line 268
    const v0, 0x7f100105

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/NavigationImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    .line 271
    const v0, 0x7f1000cd

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->J:Landroid/widget/RelativeLayout;

    .line 272
    const v0, 0x7f1000ce

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    .line 273
    const v0, 0x7f1000d2

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    .line 274
    const v0, 0x7f1000d3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    .line 275
    const v0, 0x7f1000d4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    .line 276
    const v0, 0x7f1000d5

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    .line 277
    const v0, 0x7f1000d6

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    .line 278
    const v0, 0x7f1000d1

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    .line 279
    const v0, 0x7f1000d0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    .line 280
    const v0, 0x7f1000cf

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    .line 281
    const v0, 0x7f10012c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->T:Landroid/view/View;

    .line 282
    const v0, 0x7f10012e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    .line 284
    const v0, 0x7f10012d

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    .line 290
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 295
    const v0, 0x7f10010f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->A:Landroid/widget/LinearLayout;

    .line 296
    const v0, 0x7f100110

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->B:Landroid/view/View;

    .line 297
    const v0, 0x7f10011e

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 298
    const v0, 0x7f10011f

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 299
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20b

    move v0, v2

    .line 301
    :goto_1c8
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 302
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 304
    const v0, 0x7f10011c

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 305
    const v1, 0x7f10011b

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 307
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 308
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 310
    const v0, 0x7f100111

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->C:Landroid/widget/LinearLayout;

    .line 311
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f05000d

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->D:Landroid/view/animation/Animation;

    .line 313
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f050008

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->E:Landroid/view/animation/Animation;

    .line 316
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->K()V

    .line 317
    return-void

    :cond_20b
    move v0, v3

    .line 299
    goto :goto_1c8
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 1730
    const-string v0, "User interaction"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->d(Ljava/lang/String;)V

    .line 1732
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ai:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_10

    .line 1733
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ai:Landroid/view/View$OnTouchListener;

    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1737
    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public onLayout(ZIIII)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 321
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 322
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ab:Z

    if-eqz v0, :cond_33

    .line 323
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_31

    .line 325
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b002f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 328
    const v0, 0x7f100100

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 329
    if-nez v0, :cond_34

    move v0, v1

    .line 330
    :goto_22
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v2, v4, v2

    sub-int v0, v2, v0

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/app/SqueezedLabelView;->setMaxWidth(I)V

    .line 332
    :cond_31
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ab:Z

    .line 334
    :cond_33
    return-void

    .line 329
    :cond_34
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_22
.end method

.method public p()V
    .registers 3

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 745
    return-void
.end method

.method public q()V
    .registers 3

    .prologue
    .line 749
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 750
    return-void
.end method

.method public r()V
    .registers 3

    .prologue
    .line 760
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 761
    return-void
.end method

.method public s()V
    .registers 3

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 785
    return-void
.end method

.method public setAlternateRouteButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1556
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1557
    return-void
.end method

.method public setAlternateRouteButtonTouchListener(Landroid/view/View$OnTouchListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1561
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1562
    return-void
.end method

.method public setBackToMyLocationButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1532
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->K:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1533
    return-void
.end method

.method public setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setBaseMapOverlays(Lcom/google/android/maps/driveabout/vector/q;Z)V

    .line 1358
    return-void
.end method

.method public setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V
    .registers 3
    .parameter

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    .line 1524
    return-void
.end method

.method public setCloseActivityListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 1607
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->af:Landroid/view/View$OnClickListener;

    .line 1608
    return-void
.end method

.method public setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V
    .registers 3
    .parameter

    .prologue
    .line 1510
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V

    .line 1511
    return-void
.end method

.method public setCurrentDirectionsListStep(LO/N;)V
    .registers 3
    .parameter

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->b:Lcom/google/android/maps/driveabout/app/aI;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/aI;->a(LO/N;)V

    .line 420
    return-void
.end method

.method public setCurrentRoadName(Landroid/location/Location;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 1248
    if-eqz p1, :cond_73

    .line 1249
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->aa:Landroid/location/Location;

    move-object v0, p1

    .line 1250
    check-cast v0, LaH/h;

    .line 1251
    invoke-virtual {v0}, LaH/h;->l()Lo/af;

    move-result-object v1

    .line 1252
    invoke-virtual {v0}, LaH/h;->i()Z

    move-result v0

    if-eqz v0, :cond_73

    if-eqz v1, :cond_73

    invoke-virtual {v1}, Lo/af;->c()I

    move-result v0

    if-lez v0, :cond_73

    .line 1255
    const/4 v0, 0x0

    .line 1256
    invoke-virtual {v1, v5}, Lo/af;->d(I)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 1258
    invoke-static {}, Lv/d;->c()Lv/d;

    move-result-object v2

    invoke-virtual {v1, v5}, Lo/af;->b(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/maps/driveabout/app/cL;

    invoke-direct {v4, p0, p1}, Lcom/google/android/maps/driveabout/app/cL;-><init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/location/Location;)V

    invoke-virtual {v2, v3, v4}, Lv/d;->a(Ljava/lang/String;Lv/c;)Lv/a;

    move-result-object v2

    .line 1276
    invoke-virtual {v2}, Lv/a;->c()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3d

    .line 1277
    invoke-virtual {v2}, Lv/a;->f()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1280
    :cond_3d
    if-eqz v0, :cond_63

    .line 1282
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v5}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1283
    new-instance v1, Lcom/google/android/maps/driveabout/app/dj;

    const/high16 v3, 0x3f80

    invoke-direct {v1, v0, v3}, Lcom/google/android/maps/driveabout/app/dj;-><init>(Landroid/graphics/drawable/Drawable;F)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const/16 v3, 0x21

    invoke-virtual {v2, v1, v5, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1287
    const-string v0, "\n"

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1288
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    .line 1297
    :goto_62
    return-void

    .line 1290
    :cond_63
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dx;

    invoke-virtual {v1, v5}, Lo/af;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/app/dx;->a(Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_62

    .line 1296
    :cond_73
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->z:Lcom/google/android/maps/driveabout/app/SqueezedLabelView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_62
.end method

.method public setDestinationButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1611
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->o:Landroid/view/View$OnClickListener;

    .line 1612
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 1613
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->l:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1615
    :cond_b
    return-void
.end method

.method public setDestinationInfoButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->g()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1599
    return-void
.end method

.method public setDialogManager(Lcom/google/android/maps/driveabout/app/an;)V
    .registers 2
    .parameter

    .prologue
    .line 1636
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    .line 1637
    return-void
.end method

.method public setDoubleTapZoomsAboutCenter(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDoubleTapZoomsAboutCenter(Z)V

    .line 1363
    return-void
.end method

.method public setExitButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1602
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->h()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1603
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ae:Landroid/view/View$OnClickListener;

    .line 1604
    return-void
.end method

.method public setFoundDestinationButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1594
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->f()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1595
    return-void
.end method

.method public setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .registers 3
    .parameter

    .prologue
    .line 1518
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 1519
    return-void
.end method

.method public setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V
    .registers 3
    .parameter

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V

    .line 349
    return-void
.end method

.method public setLayersButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1625
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->n:Landroid/view/View$OnClickListener;

    .line 1626
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 1627
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1629
    :cond_b
    return-void
.end method

.method public setListItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1507
    return-void
.end method

.method public setListViewButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1580
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1581
    return-void
.end method

.method public setLoadingDialogListeners(Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1492
    return-void
.end method

.method public setMapController(Lcom/google/android/maps/driveabout/vector/bk;)V
    .registers 3
    .parameter

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setController(Lcom/google/android/maps/driveabout/vector/bk;)V

    .line 345
    return-void
.end method

.method public setMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V
    .registers 3
    .parameter

    .prologue
    .line 1496
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setOnMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V

    .line 1497
    return-void
.end method

.method public setMapView(Lcom/google/android/maps/driveabout/app/NavigationMapView;Landroid/view/ViewGroup;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 356
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    .line 357
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->r:Landroid/view/ViewGroup;

    .line 358
    return-void
.end method

.method public setMapViewKeyListener(Landroid/view/View$OnKeyListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1527
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1528
    return-void
.end method

.method public setMapViewVisibility(I)V
    .registers 3
    .parameter

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setVisibility(I)V

    .line 1353
    return-void
.end method

.method public setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V
    .registers 3
    .parameter

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 1515
    return-void
.end method

.method public setMuteButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1544
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1545
    return-void
.end method

.method public setMyLocation(Landroid/location/Location;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    check-cast p1, LaH/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setMyLocation(LaH/h;Z)V

    .line 1390
    return-void
.end method

.method public setNavigationImageStep(LO/N;)V
    .registers 3
    .parameter

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationImageView;->setStep(LO/N;)V

    .line 1338
    return-void
.end method

.method public setNavigationImageViewClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1501
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->d:Lcom/google/android/maps/driveabout/app/NavigationImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1502
    return-void
.end method

.method public setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V
    .registers 2
    .parameter

    .prologue
    .line 1725
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ai:Landroid/view/View$OnTouchListener;

    .line 1726
    return-void
.end method

.method public setRawLocation(Landroid/location/Location;)V
    .registers 3
    .parameter

    .prologue
    .line 1394
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setRawLocation(Landroid/location/Location;)V

    .line 1395
    return-void
.end method

.method public setRecordingSample(IF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->e:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/app/an;->a(IF)V

    .line 539
    return-void
.end method

.method public setRerouteToDestinationButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->e()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1590
    return-void
.end method

.method public setRouteAroundTrafficCancelButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1648
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->V:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1649
    return-void
.end method

.method public setRouteAroundTrafficConfirmButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1644
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->U:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1645
    return-void
.end method

.method public setRouteOptionsButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1566
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->P:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1567
    return-void
.end method

.method public setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->f:Lcom/google/android/maps/driveabout/app/RouteSummaryView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/RouteSummaryView;->setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1641
    return-void
.end method

.method public setSpeechInstallButtonListener(Landroid/content/DialogInterface$OnClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 1632
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ah:Landroid/content/DialogInterface$OnClickListener;

    .line 1633
    return-void
.end method

.method public setStatusBarContent(Ljava/lang/CharSequence;)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 880
    const-string v0, "__route_status"

    if-ne p1, v0, :cond_12

    .line 881
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 882
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 888
    :goto_11
    return-void

    .line 884
    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 885
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 886
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->w:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_11
.end method

.method public setStreetViewButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1536
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1537
    return-void
.end method

.method public setStreetViewLaunchButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1540
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->M:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1541
    return-void
.end method

.method public setTimeRemaining(IZIZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1211
    if-ltz p1, :cond_30

    .line 1212
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1213
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1214
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1216
    if-eqz p4, :cond_29

    .line 1217
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dx;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    mul-int/lit16 v3, p1, 0x3e8

    int-to-long v3, v3

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1222
    :goto_23
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    .line 1244
    :goto_28
    return-void

    .line 1220
    :cond_29
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ad:Lcom/google/android/maps/driveabout/app/dx;

    invoke-virtual {v0, p1, v4}, Lcom/google/android/maps/driveabout/app/dx;->a(IZ)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_23

    .line 1223
    :cond_30
    if-nez p2, :cond_7b

    .line 1224
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1225
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1226
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1228
    if-nez p3, :cond_6a

    .line 1229
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d003f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1233
    :goto_53
    new-instance v1, Landroid/text/style/RelativeSizeSpan;

    const v2, 0x3f19999a

    invoke-direct {v1, v2}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1235
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_28

    .line 1231
    :cond_6a
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0040

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_53

    .line 1237
    :cond_7b
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8d

    .line 1238
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->v:Landroid/widget/TextView;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    .line 1239
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    goto :goto_28

    .line 1241
    :cond_8d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_28
.end method

.method public setTopBarClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/maps/driveabout/app/dg;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/TopBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1574
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1575
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1576
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->h:Lcom/google/android/maps/driveabout/app/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/TopBarView;->d()Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->setListener(Lcom/google/android/maps/driveabout/app/dg;)V

    .line 1577
    return-void
.end method

.method public setTopOverlayText(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    .line 1098
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_10

    .line 1099
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1104
    :goto_f
    return-void

    .line 1101
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    .line 1102
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->p:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_f
.end method

.method public setTrafficButtonClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1585
    return-void
.end method

.method public setTrafficButtonState(Z)V
    .registers 4
    .parameter

    .prologue
    .line 401
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->x:Landroid/view/View;

    if-eqz p1, :cond_b

    const v0, 0x7f020155

    :goto_7
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 404
    return-void

    .line 401
    :cond_b
    const v0, 0x7f020153

    goto :goto_7
.end method

.method public setTrafficMode(I)V
    .registers 3
    .parameter

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setTrafficMode(I)V

    .line 1312
    return-void
.end method

.method public setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setTurnArrowOverlay(LO/z;LO/N;Lcom/google/android/maps/driveabout/vector/F;)V

    .line 415
    return-void
.end method

.method public setViewMode(I)V
    .registers 9
    .parameter

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1157
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    if-ne p1, v0, :cond_a

    .line 1206
    :goto_9
    return-void

    .line 1160
    :cond_a
    iget v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1161
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->c(I)Landroid/view/View;

    move-result-object v1

    .line 1162
    if-eq v1, v0, :cond_28

    .line 1163
    if-ne p1, v4, :cond_38

    .line 1165
    if-eqz v0, :cond_20

    .line 1166
    const v2, 0x7f050008

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 1168
    :cond_20
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    const v2, 0x7f050007

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 1200
    :cond_28
    :goto_28
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-ne v1, v0, :cond_35

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eqz v0, :cond_35

    .line 1203
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setViewMode(I)V

    .line 1205
    :cond_35
    iput p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    goto :goto_9

    .line 1169
    :cond_38
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    if-ne v2, v4, :cond_4d

    .line 1171
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->c:Landroid/widget/ListView;

    const v2, 0x7f05000e

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 1172
    if-eqz v1, :cond_28

    .line 1173
    const v0, 0x7f05000d

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    goto :goto_28

    .line 1175
    :cond_4d
    if-ne p1, v5, :cond_5f

    .line 1179
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->a:Lcom/google/android/maps/driveabout/app/NavigationMapView;

    if-eq v0, v2, :cond_58

    if-eqz v0, :cond_58

    .line 1180
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1182
    :cond_58
    const v0, 0x7f050010

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    goto :goto_28

    .line 1183
    :cond_5f
    iget v2, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->ac:I

    if-ne v2, v5, :cond_6f

    .line 1185
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1186
    if-eqz v0, :cond_28

    .line 1187
    const v2, 0x7f050011

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    goto :goto_28

    .line 1192
    :cond_6f
    if-eqz v1, :cond_74

    .line 1193
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1195
    :cond_74
    if-eqz v0, :cond_28

    .line 1196
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_28
.end method

.method public setVoiceSearchButtonListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 1618
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->m:Landroid/view/View$OnClickListener;

    .line 1619
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 1620
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1622
    :cond_b
    return-void
.end method

.method public setZoomButtonClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->R:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1551
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->S:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1552
    return-void
.end method

.method public t()V
    .registers 3

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->T:Landroid/view/View;

    const v1, 0x7f05000d

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 790
    return-void
.end method

.method public u()V
    .registers 3

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->T:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 795
    return-void
.end method

.method public v()V
    .registers 3

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 800
    return-void
.end method

.method public w()V
    .registers 3

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->Q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 805
    return-void
.end method

.method public x()V
    .registers 4

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 813
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/NavigationView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->e(Landroid/view/View;I)V

    .line 815
    return-void
.end method

.method public y()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 819
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->N:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 820
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->O:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->e(Landroid/view/View;I)V

    .line 821
    return-void
.end method

.method public z()V
    .registers 3

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/NavigationView;->L:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->b(Landroid/view/View;I)V

    .line 826
    return-void
.end method
