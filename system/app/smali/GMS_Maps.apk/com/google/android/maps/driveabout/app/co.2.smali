.class Lcom/google/android/maps/driveabout/app/cO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/NavigationView;

.field private final b:Landroid/view/View;

.field private final c:Z

.field private d:Ljava/lang/Runnable;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/NavigationView;Landroid/view/View;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1665
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cO;->a:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1666
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/cO;->b:Landroid/view/View;

    .line 1667
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/app/cO;->c:Z

    .line 1668
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    .line 1669
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/cO;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1654
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/cO;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1707
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    .line 1708
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cO;->e:Z

    .line 1709
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cO;->b:Landroid/view/View;

    if-eqz p1, :cond_e

    :goto_a
    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;I)V

    .line 1710
    return-void

    .line 1709
    :cond_e
    const v0, 0x7f050011

    goto :goto_a
.end method


# virtual methods
.method public a(I)V
    .registers 6
    .parameter

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_24

    .line 1682
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->a:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1691
    :goto_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1694
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cO;->e:Z

    .line 1695
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->b:Landroid/view/View;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/app/cO;->c:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/view/View;IZ)V

    .line 1696
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->a:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/NavigationView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1697
    return-void

    .line 1684
    :cond_24
    new-instance v0, Lcom/google/android/maps/driveabout/app/cP;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cP;-><init>(Lcom/google/android/maps/driveabout/app/cO;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    goto :goto_b
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_b

    .line 1701
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cO;->a:Lcom/google/android/maps/driveabout/app/NavigationView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cO;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1703
    :cond_b
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/cO;->b(Z)V

    .line 1704
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 1672
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cO;->e:Z

    return v0
.end method
