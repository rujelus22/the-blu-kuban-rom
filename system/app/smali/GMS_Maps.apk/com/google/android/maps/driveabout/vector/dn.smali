.class Lcom/google/android/maps/driveabout/vector/dn;
.super Lcom/google/android/maps/driveabout/vector/di;
.source "SourceFile"


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/do;)V
    .registers 3
    .parameter

    .prologue
    .line 823
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/di;-><init>(Lcom/google/android/maps/driveabout/vector/dm;Lcom/google/android/maps/driveabout/vector/dj;)V

    .line 824
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/do;Lcom/google/android/maps/driveabout/vector/dj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 760
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dn;-><init>(Lcom/google/android/maps/driveabout/vector/do;)V

    return-void
.end method


# virtual methods
.method a()I
    .registers 2

    .prologue
    .line 819
    const/16 v0, 0x800

    return v0
.end method

.method public a(Lo/aa;)Lo/Q;
    .registers 3
    .parameter

    .prologue
    .line 803
    invoke-virtual {p1}, Lo/aa;->d()Lo/Q;

    move-result-object v0

    return-object v0
.end method

.method public a(Lad/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Ls/aH;
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 770
    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/di;->b(I)I

    move-result v3

    .line 772
    sget-object v0, Lcom/google/android/maps/driveabout/vector/di;->e:Lcom/google/android/maps/driveabout/vector/di;

    if-ne p0, v0, :cond_2b

    .line 773
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/dd;->b(Landroid/content/res/Resources;I)I

    move-result v4

    .line 779
    :goto_14
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/di;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    .line 780
    :goto_20
    new-instance v0, Ls/I;

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p0

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Ls/I;-><init>(Lad/p;Lcom/google/android/maps/driveabout/vector/di;IIFLjava/util/Locale;Ljava/io/File;Ls/t;)V

    return-object v0

    .line 776
    :cond_2b
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/dd;->a(Landroid/content/res/Resources;I)I

    move-result v4

    goto :goto_14

    .line 779
    :cond_32
    const/high16 v5, 0x3f80

    goto :goto_20
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 798
    const/high16 v0, 0x3f80

    const v1, 0x3e99999a

    invoke-interface {p1, v2, v2, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 799
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 760
    check-cast p1, Lcom/google/android/maps/driveabout/vector/di;

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/di;->a(Lcom/google/android/maps/driveabout/vector/di;)I

    move-result v0

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 792
    const/4 v0, 0x1

    return v0
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/aJ;
    .registers 4

    .prologue
    .line 808
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aJ;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->h:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aj;->b:Lcom/google/android/maps/driveabout/vector/aj;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aJ;-><init>(Lcom/google/android/maps/driveabout/vector/aI;Lcom/google/android/maps/driveabout/vector/aj;)V

    return-object v0
.end method

.method public k()Lo/ag;
    .registers 2

    .prologue
    .line 814
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->o()Lo/ag;

    move-result-object v0

    return-object v0
.end method

.method public l()Ls/aB;
    .registers 2

    .prologue
    .line 787
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dp;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/dp;-><init>(Lcom/google/android/maps/driveabout/vector/di;)V

    return-object v0
.end method
