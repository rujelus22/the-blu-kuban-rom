.class public Lcom/google/android/maps/driveabout/vector/aq;
.super Lcom/google/android/maps/driveabout/vector/aZ;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;
.implements Ln/f;
.implements Ln/p;
.implements Ln/s;


# instance fields
.field private volatile d:Z

.field private volatile e:Z

.field private final f:Lr/n;

.field private final g:Ln/q;

.field private final h:Ln/n;

.field private i:Lk/a;

.field private final j:Lcom/google/android/maps/driveabout/vector/as;

.field private final k:Ljava/util/Set;

.field private volatile l:Ljava/util/Set;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Ljava/util/List;

.field private final p:Lcom/google/android/maps/driveabout/vector/aJ;


# direct methods
.method protected constructor <init>(Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;ILandroid/content/Context;Ln/q;)V
    .registers 30
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 334
    sget-object v5, LA/c;->n:LA/c;

    new-instance v7, Lcom/google/android/maps/driveabout/vector/au;

    move-object/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, p8

    move-object/from16 v3, p9

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/au;-><init>(Lg/c;ILandroid/content/Context;Ln/q;)V

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x1

    const/16 v19, 0x0

    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p7

    invoke-direct/range {v4 .. v19}, Lcom/google/android/maps/driveabout/vector/aZ;-><init>(LA/c;Lu/d;Lg/c;IIILcom/google/android/maps/driveabout/vector/E;IIZZZZZZ)V

    .line 86
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    .line 88
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    .line 213
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    .line 219
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    .line 225
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    .line 231
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    .line 236
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->o:Ljava/util/List;

    .line 303
    new-instance v4, Lcom/google/android/maps/driveabout/vector/ar;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/maps/driveabout/vector/ar;-><init>(Lcom/google/android/maps/driveabout/vector/aq;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->p:Lcom/google/android/maps/driveabout/vector/aJ;

    .line 343
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->f:Lr/n;

    .line 344
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    .line 345
    new-instance v4, Ln/n;

    invoke-direct {v4}, Ln/n;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    .line 346
    new-instance v4, Lcom/google/android/maps/driveabout/vector/as;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/E;->d:Lcom/google/android/maps/driveabout/vector/E;

    const/16 v6, 0x12c

    invoke-direct {v4, v5, v6}, Lcom/google/android/maps/driveabout/vector/as;-><init>(Lcom/google/android/maps/driveabout/vector/E;I)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    .line 348
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    const v5, -0x7fafafb0

    invoke-virtual {v4, v5}, Lcom/google/android/maps/driveabout/vector/as;->b(I)V

    .line 349
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aq;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    return-object v0
.end method

.method private a(Lo/r;)Ly/b;
    .registers 3
    .parameter

    .prologue
    .line 777
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->c:Ly/b;

    invoke-static {v0, p1}, Ly/a;->a(Ly/b;Ljava/lang/Object;)Ly/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Lo/y;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 813
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_6

    .line 877
    :cond_5
    :goto_5
    return-void

    .line 818
    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln/q;->a(Lo/r;)Lo/D;

    move-result-object v0

    .line 819
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Ln/q;->c(Lo/r;)Lo/D;

    move-result-object v1

    .line 821
    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    .line 827
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v2, v1, v0}, Ln/q;->b(Lo/D;Lo/D;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 832
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v2}, Lk/a;->c()V

    .line 836
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v2, v1, v0}, Ln/q;->a(Lo/D;Lo/D;)V

    .line 843
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    invoke-virtual {v2, v0, v5, v5, v5}, Ln/q;->a(Lo/r;ZZZ)Ln/k;

    move-result-object v2

    .line 846
    const/4 v0, 0x0

    .line 847
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v1}, Lo/D;->a()Lo/r;

    move-result-object v4

    invoke-virtual {v3, v4, v5, v5, v0}, Ln/q;->a(Lo/r;ZZZ)Ln/k;

    move-result-object v3

    .line 849
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    .line 853
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    monitor-enter v4

    .line 855
    :try_start_4c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_52
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    .line 856
    invoke-virtual {v0}, Ln/k;->e()V

    goto :goto_52

    .line 874
    :catchall_62
    move-exception v0

    monitor-exit v4
    :try_end_64
    .catchall {:try_start_4c .. :try_end_64} :catchall_62

    throw v0

    .line 863
    :cond_65
    :try_start_65
    invoke-virtual {v2}, Ln/k;->b()F

    move-result v0

    invoke-virtual {v3}, Ln/k;->b()F

    move-result v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_95

    .line 864
    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Ln/k;->a(I)V

    .line 865
    const/16 v0, 0x18

    invoke-virtual {v3, v0}, Ln/k;->a(I)V

    .line 871
    :goto_7a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 872
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 873
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 874
    monitor-exit v4
    :try_end_8a
    .catchall {:try_start_65 .. :try_end_8a} :catchall_62

    .line 876
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {p1}, Lo/y;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lk/a;->a(Lo/r;Lo/D;)V

    goto/16 :goto_5

    .line 867
    :cond_95
    const/16 v0, 0x14

    :try_start_97
    invoke-virtual {v2, v0}, Ln/k;->a(I)V

    .line 868
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ln/k;->a(I)V
    :try_end_9f
    .catchall {:try_start_97 .. :try_end_9f} :catchall_62

    goto :goto_7a
.end method

.method private c(LC/a;)V
    .registers 8
    .parameter

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 432
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 438
    invoke-virtual {p1}, LC/a;->s()F

    move-result v0

    const/high16 v1, 0x4188

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7a

    .line 441
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1c
    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 442
    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v1

    sget-object v3, Lo/av;->c:Lo/av;

    invoke-virtual {v1, v3}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v1

    check-cast v1, Lo/E;

    .line 443
    invoke-virtual {v1}, Lo/E;->b()Lo/r;

    move-result-object v3

    .line 444
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly/b;

    .line 445
    if-nez v1, :cond_4b

    .line 446
    invoke-direct {p0, v3}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/r;)Ly/b;

    move-result-object v1

    .line 447
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aq;->n:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    :cond_4b
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v0

    invoke-virtual {v3, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    move-result-object v3

    .line 450
    if-eqz v3, :cond_1c

    sget-object v0, Ln/n;->a:Ln/l;

    if-eq v3, v0, :cond_1c

    .line 451
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/k;

    .line 452
    if-nez v0, :cond_76

    .line 453
    new-instance v0, Lo/k;

    const/4 v4, 0x1

    new-array v4, v4, [Lo/j;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-direct {v0, v4}, Lo/k;-><init>([Lo/j;)V

    .line 454
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1c

    .line 456
    :cond_76
    invoke-virtual {v0, v3}, Lo/k;->a(Lo/j;)V

    goto :goto_1c

    .line 461
    :cond_7a
    return-void
.end method

.method private d(LC/a;)V
    .registers 5
    .parameter

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_5

    .line 479
    :cond_4
    :goto_4
    return-void

    .line 467
    :cond_5
    invoke-virtual {p1}, LC/a;->s()F

    move-result v0

    const v1, 0x416e6666

    cmpl-float v0, v0, v1

    if-lez v0, :cond_37

    .line 468
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->c(LC/a;)Ljava/util/Set;

    move-result-object v0

    .line 469
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v1, v0}, Ln/q;->a(Ljava/util/Set;)V

    .line 472
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->a(LC/a;)Ljava/util/List;

    move-result-object v0

    .line 473
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 474
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-virtual {v2, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    goto :goto_25

    .line 477
    :cond_37
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ln/q;->a(Ljava/util/Set;)V

    goto :goto_4
.end method

.method private e(LC/a;)V
    .registers 5
    .parameter

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_5

    .line 511
    :goto_4
    return-void

    .line 488
    :cond_5
    const/4 v0, 0x0

    .line 489
    invoke-virtual {p1}, LC/a;->s()F

    move-result v1

    const/high16 v2, 0x4188

    cmpl-float v1, v1, v2

    if-lez v1, :cond_16

    .line 490
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->e(LC/a;)Lo/r;

    move-result-object v0

    .line 492
    :cond_16
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v1, v0}, Ln/q;->d(Lo/r;)V

    goto :goto_4
.end method

.method private s()V
    .registers 3

    .prologue
    .line 632
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0}, Ln/q;->c()Lo/y;

    move-result-object v0

    .line 633
    if-nez v0, :cond_18

    const/4 v0, 0x0

    .line 635
    :goto_9
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    if-eqz v0, :cond_1f

    invoke-virtual {v0}, Lo/z;->f()I

    move-result v0

    if-gez v0, :cond_1f

    const/4 v0, 0x1

    :goto_14
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/as;->b(Z)V

    .line 637
    return-void

    .line 633
    :cond_18
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v1, v0}, Ln/q;->b(Lo/y;)Lo/z;

    move-result-object v0

    goto :goto_9

    .line 635
    :cond_1f
    const/4 v0, 0x0

    goto :goto_14
.end method


# virtual methods
.method public a(Lo/aQ;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;)I
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0}, Ln/q;->g()Ljava/util/Set;

    move-result-object v2

    .line 741
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_e
    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 742
    if-eqz v0, :cond_e

    .line 746
    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v1

    sget-object v4, Lo/av;->c:Lo/av;

    invoke-virtual {v1, v4}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v1

    check-cast v1, Lo/E;

    .line 747
    invoke-virtual {v1}, Lo/E;->b()Lo/r;

    move-result-object v1

    .line 748
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 752
    if-eqz p1, :cond_42

    invoke-interface {v0}, LF/T;->b()Lo/aq;

    move-result-object v4

    invoke-virtual {v4}, Lo/aq;->i()Lo/ad;

    move-result-object v4

    invoke-virtual {p1, v4}, Lo/aQ;->b(Lo/ae;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 754
    :cond_42
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/r;)Ly/b;

    move-result-object v1

    invoke-interface {v0, v1}, LF/T;->a(Ly/b;)V

    .line 755
    invoke-interface {v0, p2}, LF/T;->a(Lcom/google/android/maps/driveabout/vector/aF;)Z

    goto :goto_e

    .line 760
    :cond_4d
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lo/T;)Lo/D;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 790
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_7

    move-object v0, v1

    .line 809
    :goto_6
    return-object v0

    .line 793
    :cond_7
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0}, Lk/a;->d()Ljava/util/List;

    move-result-object v0

    .line 794
    if-nez v0, :cond_11

    move-object v0, v1

    .line 795
    goto :goto_6

    .line 798
    :cond_11
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    .line 799
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-virtual {v3, v0}, Ln/n;->a(Lo/aq;)Ln/l;

    move-result-object v3

    .line 800
    if-eqz v3, :cond_15

    .line 801
    invoke-virtual {v3, p1}, Ln/l;->a(Lo/T;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 802
    sget-object v1, Lo/av;->c:Lo/av;

    invoke-virtual {v0, v1}, Lo/aq;->a(Lo/av;)Lo/at;

    move-result-object v0

    check-cast v0, Lo/E;

    .line 803
    invoke-virtual {v0}, Lo/E;->c()Lo/D;

    move-result-object v0

    goto :goto_6

    :cond_3c
    move-object v0, v1

    .line 809
    goto :goto_6
.end method

.method public a()V
    .registers 1

    .prologue
    .line 932
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    .line 933
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 588
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->d:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_1a

    .line 589
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/as;->e()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 590
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/as;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 605
    :cond_19
    :goto_19
    return-void

    .line 593
    :cond_1a
    invoke-virtual {p2}, LC/a;->s()F

    move-result v0

    const v1, 0x416e6666

    cmpl-float v0, v0, v1

    if-lez v0, :cond_29

    .line 594
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    goto :goto_19

    .line 597
    :cond_29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->b:Z

    goto :goto_19
.end method

.method protected a(Lg/a;)V
    .registers 3
    .parameter

    .prologue
    .line 679
    move-object v0, p1

    check-cast v0, Lk/a;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    .line 680
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lg/a;)V

    .line 681
    return-void
.end method

.method public a(Ljava/util/Set;Ljava/util/Map;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0}, Ln/q;->e()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/z;

    .line 768
    invoke-virtual {v0}, Lo/z;->b()Lo/r;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/r;)Ly/b;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 773
    :cond_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 774
    return-void
.end method

.method public a(Ln/q;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 642
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    .line 643
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    .line 644
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aq;->s()V

    .line 646
    invoke-virtual {p1}, Ln/q;->c()Lo/y;

    move-result-object v0

    .line 649
    if-eqz v0, :cond_22

    .line 650
    invoke-virtual {v0}, Lo/y;->b()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lo/A;->a:Lcom/google/common/base/x;

    invoke-static {v0, v1}, Lcom/google/common/collect/aT;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    .line 657
    :goto_1e
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    .line 658
    return-void

    .line 654
    :cond_22
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->l:Ljava/util/Set;

    goto :goto_1e
.end method

.method public a(Ln/q;Lo/y;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 662
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    .line 663
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    .line 664
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aq;->s()V

    .line 665
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lo/y;)V

    .line 666
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    .line 667
    return-void
.end method

.method public a(Lo/aq;Ln/l;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 925
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    .line 926
    return-void
.end method

.method a(Ljava/util/List;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 520
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    if-eqz v0, :cond_f7

    .line 521
    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    .line 522
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 524
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/as;->e()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 525
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->d:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 529
    :cond_1d
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v4

    .line 530
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v5

    .line 533
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v0

    .line 534
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v1}, Ln/q;->i()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 535
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    monitor-enter v1

    .line 536
    :try_start_35
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 537
    monitor-exit v1
    :try_end_3b
    .catchall {:try_start_35 .. :try_end_3b} :catchall_74

    .line 538
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3f
    :goto_3f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    .line 542
    invoke-virtual {v0}, Ln/k;->d()Z

    move-result v1

    if-eqz v1, :cond_77

    .line 543
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->h:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v7, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    .line 554
    :goto_5b
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555
    invoke-virtual {v0}, Ln/k;->b()F

    move-result v1

    cmpl-float v1, v1, v9

    if-lez v1, :cond_a1

    invoke-virtual {v0}, Ln/k;->d()Z

    move-result v1

    if-nez v1, :cond_a1

    .line 556
    invoke-virtual {v0}, Ln/k;->g()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_3f

    .line 537
    :catchall_74
    move-exception v0

    :try_start_75
    monitor-exit v1
    :try_end_76
    .catchall {:try_start_75 .. :try_end_76} :catchall_74

    throw v0

    .line 545
    :cond_77
    invoke-virtual {v0}, Ln/k;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_90

    .line 546
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->f:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aq;->p:Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-static {v8}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    goto :goto_5b

    .line 550
    :cond_90
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->c:Lcom/google/android/maps/driveabout/vector/aI;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aq;->p:Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-static {v8}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v8

    invoke-virtual {p0, v1, v7, v8}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    goto :goto_5b

    .line 557
    :cond_a1
    invoke-virtual {v0}, Ln/k;->b()F

    move-result v1

    cmpg-float v1, v1, v9

    if-gez v1, :cond_3f

    invoke-virtual {v0}, Ln/k;->d()Z

    move-result v1

    if-nez v1, :cond_3f

    .line 558
    invoke-virtual {v0}, Ln/k;->g()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 559
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->g:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v7, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    aput-object v0, v7, v3

    invoke-virtual {p0, v1, v7}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    .line 560
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3f

    .line 565
    :cond_c5
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_dd

    .line 566
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->b:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v1, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    new-instance v6, Lcom/google/android/maps/driveabout/vector/at;

    invoke-direct {v6, v4}, Lcom/google/android/maps/driveabout/vector/at;-><init>(Ljava/util/Set;)V

    aput-object v6, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    .line 568
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 570
    :cond_dd
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f5

    .line 571
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v1, v2, [Lcom/google/android/maps/driveabout/vector/aJ;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/at;

    invoke-direct {v4, v5}, Lcom/google/android/maps/driveabout/vector/at;-><init>(Ljava/util/Set;)V

    aput-object v4, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    .line 573
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f5
    move v0, v2

    .line 577
    :goto_f6
    return v0

    :cond_f7
    move v0, v3

    goto :goto_f6
.end method

.method public a_(LC/a;)I
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 882
    const/4 v0, 0x0

    .line 883
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->j:Lcom/google/android/maps/driveabout/vector/as;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/as;->h()Z

    move-result v2

    if-eqz v2, :cond_b

    move v0, v1

    .line 886
    :cond_b
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    monitor-enter v2

    .line 887
    :try_start_e
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 888
    monitor-exit v2

    .line 911
    :goto_17
    return v0

    .line 890
    :cond_18
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_38

    .line 891
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/k;

    .line 892
    invoke-virtual {v0}, Ln/k;->f()Z

    move-result v0

    if-nez v0, :cond_36

    .line 893
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 894
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    :cond_36
    move v0, v1

    .line 897
    goto :goto_1e

    .line 898
    :cond_38
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->k:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 901
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v1}, Lk/a;->c()V

    .line 904
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v1}, Ln/q;->b()V

    .line 908
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aq;->h()V

    .line 910
    :cond_4d
    monitor-exit v2

    goto :goto_17

    :catchall_4f
    move-exception v0

    monitor-exit v2
    :try_end_51
    .catchall {:try_start_e .. :try_end_51} :catchall_4f

    throw v0
.end method

.method protected b(LC/a;)Ljava/util/Set;
    .registers 3
    .parameter

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_9

    .line 421
    invoke-static {}, Lcom/google/common/collect/ImmutableSet;->f()Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    .line 423
    :goto_8
    return-object v0

    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0, p1}, Lk/a;->b(LC/a;)Ljava/util/Set;

    move-result-object v0

    goto :goto_8
.end method

.method public b(Ln/q;)V
    .registers 3
    .parameter

    .prologue
    .line 671
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->e:Z

    .line 672
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 404
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->e(LC/a;)V

    .line 405
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->d(LC/a;)V

    .line 406
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aq;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 407
    invoke-virtual {p1}, LC/a;->s()F

    move-result v1

    const v2, 0x416e6666

    cmpl-float v1, v1, v2

    if-lez v1, :cond_22

    .line 408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    .line 409
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/aZ;->b(LC/a;LD/a;)Z

    move-result v0

    .line 410
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->c(LC/a;)V

    .line 414
    :goto_21
    return v0

    .line 413
    :cond_22
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->d:Z

    goto :goto_21
.end method

.method public b(Ljava/util/List;)Z
    .registers 3
    .parameter

    .prologue
    .line 515
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/aq;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public c()LC/b;
    .registers 2

    .prologue
    .line 916
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(LD/a;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 609
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->c(LD/a;)V

    .line 610
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0, v1}, Ln/q;->d(Lo/r;)V

    .line 611
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0, v1}, Ln/q;->a(Ljava/util/Set;)V

    .line 612
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0, p0}, Ln/q;->a(Ln/s;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->h:Ln/n;

    invoke-virtual {v0, p0}, Ln/n;->a(Ln/p;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->g:Ln/q;

    invoke-virtual {v0}, Ln/q;->j()Ln/e;

    move-result-object v0

    invoke-interface {v0, p0}, Ln/e;->a(Ln/f;)V

    .line 355
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    .line 616
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    if-nez v0, :cond_5

    .line 629
    :cond_4
    :goto_4
    return-void

    .line 622
    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->i:Lk/a;

    invoke-virtual {v0}, Lk/a;->b()V

    .line 623
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aq;->a:Lcom/google/android/maps/driveabout/vector/aU;

    .line 624
    if-eqz v0, :cond_4

    .line 625
    const/4 v1, 0x1

    .line 626
    const/4 v2, 0x0

    .line 627
    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aU;->a(ZZ)V

    goto :goto_4
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .registers 1

    .prologue
    .line 583
    return-object p0
.end method
