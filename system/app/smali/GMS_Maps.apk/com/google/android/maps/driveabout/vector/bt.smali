.class public Lcom/google/android/maps/driveabout/vector/bt;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SourceFile"

# interfaces
.implements LaD/m;


# static fields
.field private static final c:F

.field private static final d:F

.field private static final e:D


# instance fields
.field private final a:F

.field private final b:F

.field private final f:Lcom/google/android/maps/driveabout/vector/bv;

.field private g:Lcom/google/android/maps/driveabout/vector/bw;

.field private h:Lcom/google/android/maps/driveabout/vector/bw;

.field private i:Landroid/view/MotionEvent;

.field private j:F

.field private k:F

.field private l:Lcom/google/android/maps/driveabout/vector/bu;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 144
    sget-boolean v0, Lcom/google/googlenav/android/E;->g:Z

    if-eqz v0, :cond_19

    const v0, 0x3f7f3b64

    :goto_7
    sput v0, Lcom/google/android/maps/driveabout/vector/bt;->c:F

    .line 150
    const/high16 v0, 0x3f80

    sget v1, Lcom/google/android/maps/driveabout/vector/bt;->c:F

    div-float/2addr v0, v1

    sput v0, Lcom/google/android/maps/driveabout/vector/bt;->d:F

    .line 152
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/vector/bt;->e:D

    return-void

    .line 144
    :cond_19
    const v0, 0x3f7fbe77

    goto :goto_7
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/bv;)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x14

    .line 187
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 185
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    .line 188
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    .line 189
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bw;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/bw;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    .line 190
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->b:F

    .line 191
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->a:F

    .line 193
    return-void
.end method

.method private static b(LaD/q;)Z
    .registers 3
    .parameter

    .prologue
    .line 395
    invoke-virtual {p0}, LaD/q;->e()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, LaD/q;->c()F

    move-result v0

    sget v1, Lcom/google/android/maps/driveabout/vector/bt;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1c

    invoke-virtual {p0}, LaD/q;->c()F

    move-result v0

    sget v1, Lcom/google/android/maps/driveabout/vector/bt;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 446
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bw;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bw;-><init>(Lcom/google/android/maps/driveabout/vector/bw;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->h:Lcom/google/android/maps/driveabout/vector/bw;

    .line 447
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 414
    const/16 v0, 0x63

    invoke-static {v0, p1}, Lbm/m;->a(ILjava/lang/String;)V

    .line 415
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->a:Z

    .line 419
    return-void
.end method

.method public a(LaD/o;)Z
    .registers 6
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->e:Z

    if-eqz v0, :cond_32

    .line 403
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, LaD/o;->a(FF)V

    .line 404
    invoke-virtual {p1}, LaD/o;->c()F

    move-result v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    .line 405
    invoke-virtual {p1}, LaD/o;->a()F

    move-result v1

    .line 406
    invoke-virtual {p1}, LaD/o;->b()F

    move-result v2

    .line 407
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/bk;->b(FFF)F

    .line 408
    const/4 v0, 0x1

    .line 410
    :goto_31
    return v0

    :cond_32
    const/4 v0, 0x0

    goto :goto_31
.end method

.method public a(LaD/q;)Z
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x4000

    .line 373
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->b:Z

    if-eqz v0, :cond_31

    .line 374
    invoke-virtual {p1}, LaD/q;->g()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 375
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    const/high16 v1, -0x4080

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(FI)F

    move-result v0

    .line 377
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    .line 391
    :cond_31
    :goto_31
    const/4 v0, 0x1

    return v0

    .line 380
    :cond_33
    invoke-virtual {p1}, LaD/q;->c()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/google/android/maps/driveabout/vector/bt;->e:D

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 382
    invoke-virtual {p1}, LaD/q;->a()F

    move-result v1

    .line 383
    invoke-virtual {p1}, LaD/q;->b()F

    move-result v2

    .line 384
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/bt;->b(LaD/q;)Z

    move-result v3

    if-eqz v3, :cond_4f

    .line 385
    const/4 v0, 0x0

    .line 387
    :cond_4f
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(FFF)F

    move-result v0

    .line 388
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    goto :goto_31
.end method

.method public a(LaD/u;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 364
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v1, v1, Lcom/google/android/maps/driveabout/vector/bw;->d:Z

    if-eqz v1, :cond_15

    .line 365
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v1

    invoke-virtual {p1}, LaD/u;->a()F

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/bk;->b(FI)V

    .line 366
    const/4 v0, 0x1

    .line 368
    :cond_15
    return v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->h:Lcom/google/android/maps/driveabout/vector/bw;

    if-eqz v0, :cond_8

    .line 451
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->h:Lcom/google/android/maps/driveabout/vector/bw;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    .line 453
    :cond_8
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->b:Z

    .line 423
    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->c:Z

    .line 427
    return-void
.end method

.method public d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->d:Z

    .line 431
    return-void
.end method

.method public e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->e:Z

    .line 435
    return-void
.end method

.method public f(Z)V
    .registers 3
    .parameter

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->f:Z

    .line 439
    return-void
.end method

.method public g(Z)V
    .registers 3
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/bw;->g:Z

    .line 443
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 252
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bu;->b:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    .line 253
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    .line 254
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->d(FF)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 255
    const/4 v0, 0x1

    .line 260
    :goto_1a
    return v0

    .line 257
    :cond_1b
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    .line 258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    .line 259
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    .line 260
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter

    .prologue
    const/16 v5, 0x14a

    const/4 v0, 0x0

    const/high16 v4, 0x4000

    const/high16 v6, 0x3f00

    const/4 v3, 0x1

    .line 270
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    if-eqz v1, :cond_70

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_70

    .line 271
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v1, v1, Lcom/google/android/maps/driveabout/vector/bw;->b:Z

    if-eqz v1, :cond_69

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bu;->b:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v1, v2, :cond_69

    .line 273
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->c:Z

    if-eqz v0, :cond_55

    .line 274
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v1, v0, v4

    .line 275
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 280
    :goto_35
    const/high16 v2, 0x3f80

    .line 289
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v4, v4, Lcom/google/android/maps/driveabout/vector/bw;->c:Z

    if-eqz v4, :cond_5e

    .line 294
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v4

    invoke-virtual {v4, v2, v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(FI)F

    move-result v2

    .line 299
    :goto_47
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v4, v2, v1, v0}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    .line 301
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    move v0, v3

    .line 351
    :cond_54
    :goto_54
    return v0

    .line 277
    :cond_55
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 278
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_35

    .line 296
    :cond_5e
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v4

    invoke-virtual {v4, v2, v1, v0, v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(FFFI)F

    move-result v2

    goto :goto_47

    .line 304
    :cond_69
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    .line 305
    sget-object v1, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    .line 309
    :cond_70
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    if-eqz v1, :cond_54

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_54

    .line 310
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    sub-float/2addr v1, v2

    .line 311
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    sub-float/2addr v2, v4

    .line 313
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/bu;->b:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v4, v5, :cond_d8

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/bt;->b:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_d8

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->b:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_d8

    .line 315
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    .line 316
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 317
    int-to-float v2, v2

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->a:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_114

    .line 318
    sget-object v2, Lcom/google/android/maps/driveabout/vector/bu;->c:Lcom/google/android/maps/driveabout/vector/bu;

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    .line 319
    const-string v2, "d"

    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/vector/bt;->a(Ljava/lang/String;)V

    .line 332
    :cond_d8
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/bu;->c:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v2, v4, :cond_117

    .line 333
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x40c0

    mul-float/2addr v1, v2

    .line 334
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(FI)F

    move-result v0

    .line 335
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-interface {v1, v0, v2, v4}, Lcom/google/android/maps/driveabout/vector/bv;->a(FFF)V

    .line 347
    :cond_105
    :goto_105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    .line 348
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    move v0, v3

    .line 349
    goto/16 :goto_54

    :cond_114
    move v0, v3

    .line 329
    goto/16 :goto_54

    .line 336
    :cond_117
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bu;->d:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v0, v1, :cond_105

    .line 337
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    .line 338
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/bv;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    .line 339
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bt;->j:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bt;->k:F

    invoke-static {v0, v1, v2, v4}, LaD/j;->a(FFFF)F

    move-result v2

    .line 341
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v0, v1, v4, v5}, LaD/j;->a(FFFF)F

    move-result v4

    .line 344
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v5}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v5

    sub-float v2, v4, v2

    const/high16 v4, 0x4334

    mul-float/2addr v2, v4

    float-to-double v6, v2

    const-wide v8, 0x400921fb54442d18L

    div-double/2addr v6, v8

    double-to-float v2, v6

    invoke-virtual {v5, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->b(FFF)F

    goto :goto_105
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->l:Lcom/google/android/maps/driveabout/vector/bu;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bu;->a:Lcom/google/android/maps/driveabout/vector/bu;

    if-ne v0, v1, :cond_13

    .line 200
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->a(FF)V

    .line 202
    :cond_13
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->a:Z

    if-eqz v0, :cond_1a

    .line 233
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/vector/bt;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 234
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/bk;->b(FF)V

    .line 235
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    .line 238
    :cond_1a
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->i:Landroid/view/MotionEvent;

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->f:Z

    if-eqz v0, :cond_17

    .line 358
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->e(FF)V

    .line 360
    :cond_17
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 213
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/bv;->a(Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 222
    :cond_9
    :goto_9
    return v1

    .line 215
    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->a:Z

    if-eqz v0, :cond_9

    .line 217
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->p_()Lcom/google/android/maps/driveabout/vector/bk;

    move-result-object v0

    .line 218
    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/bk;->a(FF)V

    .line 219
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/bv;->f(FF)V

    .line 220
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    goto :goto_9
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->g:Lcom/google/android/maps/driveabout/vector/bw;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/bw;->g:Z

    if-eqz v0, :cond_18

    .line 244
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bv;->d()V

    .line 245
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->c(FF)V

    .line 247
    :cond_18
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bt;->f:Lcom/google/android/maps/driveabout/vector/bv;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bv;->b(FF)Z

    move-result v0

    return v0
.end method
