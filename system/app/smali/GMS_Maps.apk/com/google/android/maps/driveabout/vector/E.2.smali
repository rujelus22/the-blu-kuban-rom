.class public final enum Lcom/google/android/maps/driveabout/vector/E;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum A:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum B:Lcom/google/android/maps/driveabout/vector/E;

.field private static final synthetic C:[Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum a:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum b:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum c:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum d:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum e:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum f:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum g:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum h:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum i:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum j:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum k:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum l:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum m:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum n:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum o:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum p:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum q:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum r:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum s:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum t:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum u:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum v:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum w:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum x:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum y:Lcom/google/android/maps/driveabout/vector/E;

.field public static final enum z:Lcom/google/android/maps/driveabout/vector/E;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "UNUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    .line 26
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "BASE_IMAGERY"

    invoke-direct {v0, v1, v4}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->b:Lcom/google/android/maps/driveabout/vector/E;

    .line 29
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "VECTORS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->c:Lcom/google/android/maps/driveabout/vector/E;

    .line 32
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "NIGHT_DIMMER"

    invoke-direct {v0, v1, v6}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->d:Lcom/google/android/maps/driveabout/vector/E;

    .line 34
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "DESATURATE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->e:Lcom/google/android/maps/driveabout/vector/E;

    .line 37
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "TRAFFIC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->f:Lcom/google/android/maps/driveabout/vector/E;

    .line 40
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "INDOOR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->g:Lcom/google/android/maps/driveabout/vector/E;

    .line 43
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "LAYER_SHAPES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->h:Lcom/google/android/maps/driveabout/vector/E;

    .line 46
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "TRANSIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->i:Lcom/google/android/maps/driveabout/vector/E;

    .line 49
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "BUILDINGS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->j:Lcom/google/android/maps/driveabout/vector/E;

    .line 52
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "POLYLINE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->k:Lcom/google/android/maps/driveabout/vector/E;

    .line 55
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "LABELS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->l:Lcom/google/android/maps/driveabout/vector/E;

    .line 58
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "FADE_OUT_OVERLAY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->m:Lcom/google/android/maps/driveabout/vector/E;

    .line 61
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "ROUTE_OVERVIEW_POLYLINE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    .line 64
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "TURN_ARROW_OVERLAY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->o:Lcom/google/android/maps/driveabout/vector/E;

    .line 67
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "IMPORTANT_LABELS"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->p:Lcom/google/android/maps/driveabout/vector/E;

    .line 70
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "DEBUG_LABELS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->q:Lcom/google/android/maps/driveabout/vector/E;

    .line 73
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "INTERSECTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->r:Lcom/google/android/maps/driveabout/vector/E;

    .line 76
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "SKY"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->s:Lcom/google/android/maps/driveabout/vector/E;

    .line 79
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "MY_LOCATION_OVERLAY_DA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->t:Lcom/google/android/maps/driveabout/vector/E;

    .line 82
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "LAYERS_RAW_GPS"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->u:Lcom/google/android/maps/driveabout/vector/E;

    .line 85
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "LAYER_MARKERS_SHADOW"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->v:Lcom/google/android/maps/driveabout/vector/E;

    .line 88
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "LAYER_MARKERS"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    .line 91
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "MY_LOCATION_OVERLAY_VECTORMAPS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->x:Lcom/google/android/maps/driveabout/vector/E;

    .line 94
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "COMPASS_OVERLAY"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->y:Lcom/google/android/maps/driveabout/vector/E;

    .line 97
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "LOADING_SPINNY"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->z:Lcom/google/android/maps/driveabout/vector/E;

    .line 100
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "BUBBLE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->A:Lcom/google/android/maps/driveabout/vector/E;

    .line 103
    new-instance v0, Lcom/google/android/maps/driveabout/vector/E;

    const-string v1, "HEADS_UP_DISPLAY"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/E;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->B:Lcom/google/android/maps/driveabout/vector/E;

    .line 22
    const/16 v0, 0x1c

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/E;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->b:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->c:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->d:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->e:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->f:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->g:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->h:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->i:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->j:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->k:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->l:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->m:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->n:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->o:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->p:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->q:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->r:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->s:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->t:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->u:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->v:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->w:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->x:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->y:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->z:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->A:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/maps/driveabout/vector/E;->B:Lcom/google/android/maps/driveabout/vector/E;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/vector/E;->C:[Lcom/google/android/maps/driveabout/vector/E;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/E;
    .registers 2
    .parameter

    .prologue
    .line 22
    const-class v0, Lcom/google/android/maps/driveabout/vector/E;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method

.method public static values()[Lcom/google/android/maps/driveabout/vector/E;
    .registers 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->C:[Lcom/google/android/maps/driveabout/vector/E;

    invoke-virtual {v0}, [Lcom/google/android/maps/driveabout/vector/E;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/E;->ordinal()I

    move-result v0

    return v0
.end method
