.class Lcom/google/android/maps/driveabout/app/L;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/DestinationActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1100
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/L;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1100
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/L;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .registers 5
    .parameter

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/L;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i(Lcom/google/android/maps/driveabout/app/DestinationActivity;)LaH/h;

    move-result-object v0

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    .line 1104
    :goto_9
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/L;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    new-instance v2, LaH/j;

    invoke-direct {v2}, LaH/j;-><init>()V

    invoke-virtual {v2, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v2

    invoke-virtual {v2}, LaH/j;->d()LaH/h;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;LaH/h;)LaH/h;

    .line 1105
    if-nez v0, :cond_22

    .line 1106
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/L;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    .line 1108
    :cond_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/L;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/bR;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/L;->a:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i(Lcom/google/android/maps/driveabout/app/DestinationActivity;)LaH/h;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/bR;->a(LaH/h;)V

    .line 1109
    return-void

    .line 1103
    :cond_32
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1111
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 1113
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1115
    return-void
.end method
