.class public Lcom/google/android/maps/driveabout/app/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/maps/driveabout/app/NavigationService;

.field private final c:Lcom/google/android/maps/driveabout/app/j;

.field private d:LK/ac;

.field private e:LK/T;

.field private f:LK/T;

.field private final g:LK/T;

.field private final h:LK/c;

.field private final i:LK/c;

.field private j:Lcom/google/android/maps/driveabout/app/g;

.field private k:Lcom/google/android/maps/driveabout/app/g;

.field private l:Lcom/google/android/maps/driveabout/app/g;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private final q:Lcom/google/android/maps/driveabout/app/aK;

.field private final r:Ljava/util/HashMap;

.field private s:I

.field private t:LK/V;

.field private u:J

.field private v:Landroid/os/Handler;

.field private w:Z

.field private final x:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/app/a;->o:Z

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    .line 115
    iput v1, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    .line 127
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    .line 380
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    .line 381
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/a;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    .line 382
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->v:Landroid/os/Handler;

    .line 384
    new-instance v0, LK/ac;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    .line 385
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    new-instance v1, Lcom/google/android/maps/driveabout/app/e;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/app/e;-><init>(Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/b;)V

    invoke-virtual {v0, v1}, LK/ac;->a(LK/ae;)V

    .line 387
    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {v0, v1}, LK/F;->a(Law/p;Landroid/content/Context;)LK/F;

    move-result-object v0

    .line 390
    new-instance v1, LK/u;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, LK/u;-><init>(Landroid/content/Context;LK/C;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->g:LK/T;

    .line 392
    new-instance v0, LK/J;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/J;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->h:LK/c;

    .line 393
    new-instance v0, LK/ah;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LK/ah;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->i:LK/c;

    .line 395
    new-instance v0, Lcom/google/android/maps/driveabout/app/j;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    .line 396
    new-instance v0, Lcom/google/android/maps/driveabout/app/aK;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/app/aK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    .line 397
    iget v0, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->c(I)V

    .line 398
    return-void
.end method

.method private a(LO/j;Z)LK/V;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 868
    const/4 v0, 0x0

    .line 869
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->b()I

    move-result v1

    const/16 v2, 0x10

    if-ne v1, v2, :cond_55

    .line 872
    if-eqz p2, :cond_3a

    .line 873
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_6e

    .line 883
    const v1, 0x7f0d000f

    .line 884
    const/4 v0, 0x6

    .line 903
    :goto_1e
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    invoke-direct {v2, v0}, LK/s;-><init>(I)V

    invoke-static {p1, v1, v2}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 913
    :cond_2d
    :goto_2d
    return-object v0

    .line 875
    :pswitch_2e
    const v1, 0x7f0d000d

    .line 876
    const/16 v0, 0x9

    .line 877
    goto :goto_1e

    .line 879
    :pswitch_34
    const v1, 0x7f0d000e

    .line 880
    const/16 v0, 0xa

    .line 881
    goto :goto_1e

    .line 888
    :cond_3a
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_76

    .line 898
    const v1, 0x7f0d0012

    .line 899
    const/4 v0, 0x5

    goto :goto_1e

    .line 890
    :pswitch_4a
    const v1, 0x7f0d0010

    .line 891
    const/4 v0, 0x7

    .line 892
    goto :goto_1e

    .line 894
    :pswitch_4f
    const v1, 0x7f0d0011

    .line 895
    const/16 v0, 0x8

    .line 896
    goto :goto_1e

    .line 906
    :cond_55
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v1

    invoke-virtual {v1}, LO/N;->o()Landroid/text/Spanned;

    move-result-object v1

    .line 907
    invoke-virtual {p1}, LO/j;->j()LK/m;

    move-result-object v2

    .line 908
    if-eqz v1, :cond_2d

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_2d

    .line 909
    invoke-static {p1, v1, v2}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    goto :goto_2d

    .line 873
    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_2e
        :pswitch_34
    .end packed-switch

    .line 888
    :pswitch_data_76
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_4f
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/a;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->v:Landroid/os/Handler;

    return-object v0
.end method

.method private a(LK/V;)V
    .registers 4
    .parameter

    .prologue
    .line 1052
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/a;->t:LK/V;

    .line 1053
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/app/a;->u:J

    .line 1054
    return-void
.end method

.method private declared-synchronized a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 796
    monitor-enter p0

    if-nez p1, :cond_a

    .line 797
    if-eqz p2, :cond_8

    .line 798
    :try_start_5
    invoke-interface {p2}, Lcom/google/android/maps/driveabout/app/d;->a()V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_21

    .line 825
    :cond_8
    :goto_8
    monitor-exit p0

    return-void

    .line 802
    :cond_a
    :try_start_a
    new-instance v1, Lcom/google/android/maps/driveabout/app/g;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/maps/driveabout/app/g;-><init>(Lcom/google/android/maps/driveabout/app/a;LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    .line 803
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    .line 804
    :goto_15
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/g;->a(Lcom/google/android/maps/driveabout/app/g;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 807
    if-eqz p2, :cond_8

    .line 808
    invoke-interface {p2}, Lcom/google/android/maps/driveabout/app/d;->a()V
    :try_end_20
    .catchall {:try_start_a .. :try_end_20} :catchall_21

    goto :goto_8

    .line 796
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0

    .line 803
    :cond_24
    :try_start_24
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->j:Lcom/google/android/maps/driveabout/app/g;

    goto :goto_15

    .line 812
    :cond_27
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_46

    .line 817
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_40

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/g;->e(Lcom/google/android/maps/driveabout/app/g;)Lcom/google/android/maps/driveabout/app/d;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 818
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/g;->e(Lcom/google/android/maps/driveabout/app/g;)Lcom/google/android/maps/driveabout/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/app/d;->a()V

    .line 820
    :cond_40
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    .line 824
    :goto_42
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;)V

    goto :goto_8

    .line 822
    :cond_46
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/g;)V
    :try_end_49
    .catchall {:try_start_24 .. :try_end_49} :catchall_21

    goto :goto_42
.end method

.method private a(LO/j;)V
    .registers 2
    .parameter

    .prologue
    .line 638
    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/app/g;)V
    .registers 2
    .parameter

    .prologue
    .line 831
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    .line 833
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/g;->a()V

    .line 834
    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 588
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_17

    .line 589
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 590
    if-nez v0, :cond_12

    const/4 v0, 0x0

    .line 592
    :goto_11
    return v0

    .line 590
    :cond_12
    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    goto :goto_11

    .line 592
    :cond_17
    const/4 v0, 0x1

    goto :goto_11
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/a;)Lcom/google/android/maps/driveabout/app/j;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    return-object v0
.end method

.method private b(LO/j;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 708
    new-instance v1, Lcom/google/android/maps/driveabout/app/c;

    invoke-direct {v1, p1}, Lcom/google/android/maps/driveabout/app/c;-><init>(LO/j;)V

    .line 709
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/c;->a()LO/j;

    .line 710
    const/4 v0, 0x0

    :goto_9
    const/16 v2, 0xa

    if-ge v0, v2, :cond_25

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/c;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_25

    .line 711
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/c;->a()LO/j;

    move-result-object v2

    .line 712
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    .line 713
    invoke-direct {p0, v2, p2}, Lcom/google/android/maps/driveabout/app/a;->c(LO/j;I)V

    .line 710
    :cond_22
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 716
    :cond_25
    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/a;)LK/c;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->i:LK/c;

    return-object v0
.end method

.method private c(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1005
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {p1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v3

    .line 1006
    const-string v0, "VoiceGuidanceEnabled"

    invoke-virtual {v3, v0, v1}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_45

    move v0, v1

    :goto_15
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    .line 1011
    const-string v0, "AlertMode"

    invoke-virtual {v3, v0, v1}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    .line 1012
    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_23

    .line 1013
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    .line 1018
    :cond_23
    const-string v0, "VolumeMode"

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    .line 1019
    if-eqz v0, :cond_2e

    if-ne v0, v1, :cond_30

    .line 1020
    :cond_2e
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    .line 1025
    :cond_30
    const-string v0, "AlertsDisabled"

    invoke-virtual {v3, v0, v2}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    .line 1026
    if-ne v0, v1, :cond_3a

    .line 1027
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    .line 1032
    :cond_3a
    const-string v0, "AlertsMuted"

    invoke-virtual {v3, v0, v2}, LR/u;->a(Ljava/lang/String;I)I

    move-result v0

    .line 1033
    if-ne v0, v1, :cond_44

    .line 1034
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    .line 1036
    :cond_44
    return-void

    :cond_45
    move v0, v2

    .line 1006
    goto :goto_15
.end method

.method private c(LO/j;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 719
    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v0

    .line 720
    if-nez v0, :cond_8

    .line 736
    :cond_7
    :goto_7
    return-void

    .line 723
    :cond_8
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v1, v0, v6}, LK/T;->a(LK/V;LK/U;)V

    .line 724
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 726
    invoke-virtual {p1}, LO/j;->a()I

    move-result v0

    if-nez v0, :cond_7

    .line 727
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v0

    .line 728
    invoke-virtual {v0}, LO/N;->e()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, LO/N;->f()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 730
    invoke-virtual {p1}, LO/j;->d()I

    move-result v2

    int-to-double v2, v2

    invoke-virtual {v0}, LO/N;->a()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->e()D

    move-result-wide v4

    div-double/2addr v2, v4

    invoke-virtual {p1}, LO/j;->c()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 733
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/maps/driveabout/app/aK;->a(II)LK/V;

    move-result-object v0

    .line 734
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v1, v0, v6}, LK/T;->a(LK/V;LK/U;)V

    goto :goto_7
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/a;)LK/T;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    return-object v0
.end method

.method private d(I)V
    .registers 5
    .parameter

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {p1}, LO/T;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;)LR/u;

    move-result-object v1

    .line 1041
    const-string v2, "VoiceGuidanceEnabled"

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    :goto_11
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LR/u;->a(Ljava/lang/String;Ljava/lang/Object;)LR/u;

    .line 1044
    const-string v0, "Volume"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    .line 1045
    const-string v0, "AlertMode"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    .line 1046
    const-string v0, "VolumeMode"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    .line 1047
    const-string v0, "AlertsDisabled"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    .line 1048
    const-string v0, "AlertsMuted"

    invoke-virtual {v1, v0}, LR/u;->a(Ljava/lang/String;)LR/u;

    .line 1049
    return-void

    .line 1041
    :cond_32
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/a;)Z
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/a;)LK/T;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->f:LK/T;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/a;)LK/T;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->g:LK/T;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/app/a;)Z
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->o:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/maps/driveabout/app/a;)LK/c;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->h:LK/c;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/maps/driveabout/app/a;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/a;->o()V

    return-void
.end method

.method private declared-synchronized o()V
    .registers 3

    .prologue
    .line 839
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->j:Lcom/google/android/maps/driveabout/app/g;

    .line 842
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_17

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 843
    :cond_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->c:Lcom/google/android/maps/driveabout/app/j;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/j;->b()V

    .line 845
    :cond_1c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_2a

    .line 848
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    .line 849
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->l:Lcom/google/android/maps/driveabout/app/g;

    .line 850
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->a(Lcom/google/android/maps/driveabout/app/g;)V
    :try_end_28
    .catchall {:try_start_1 .. :try_end_28} :catchall_2e

    .line 854
    :goto_28
    monitor-exit p0

    return-void

    .line 852
    :cond_2a
    const/4 v0, 0x0

    :try_start_2b
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;
    :try_end_2d
    .catchall {:try_start_2b .. :try_end_2d} :catchall_2e

    goto :goto_28

    .line 839
    :catchall_2e
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method a(LO/j;I)LK/V;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 922
    const/4 v0, 0x0

    .line 923
    invoke-virtual {p1}, LO/j;->i()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 924
    invoke-virtual {p1}, LO/j;->h()Ljava/lang/CharSequence;

    move-result-object v1

    .line 925
    invoke-virtual {p1}, LO/j;->j()LK/m;

    move-result-object v2

    .line 926
    if-eqz v1, :cond_1b

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1b

    .line 927
    invoke-static {p1, v1, v2}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 942
    :cond_1b
    :goto_1b
    return-object v0

    .line 929
    :cond_1c
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_38

    .line 932
    invoke-virtual {p1}, LO/j;->e()LO/N;

    move-result-object v1

    .line 933
    invoke-virtual {v1}, LO/N;->j()LO/N;

    move-result-object v1

    .line 934
    if-eqz v1, :cond_1b

    .line 935
    invoke-virtual {v1}, LO/N;->e()I

    move-result v0

    .line 936
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/aK;->a(LO/j;II)LK/V;

    move-result-object v0

    goto :goto_1b

    .line 940
    :cond_38
    invoke-virtual {p1}, LO/j;->a()I

    move-result v0

    if-nez v0, :cond_44

    const/4 v0, 0x1

    :goto_3f
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;Z)LK/V;

    move-result-object v0

    goto :goto_1b

    :cond_44
    const/4 v0, 0x0

    goto :goto_3f
.end method

.method public a(I)V
    .registers 6
    .parameter

    .prologue
    .line 401
    if-nez p1, :cond_40

    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    invoke-virtual {v0}, LK/ac;->a()LK/ab;

    move-result-object v0

    .line 403
    new-instance v1, LK/d;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->v:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, v0}, LK/d;-><init>(Landroid/content/Context;Landroid/os/Handler;LK/ab;)V

    .line 405
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    .line 407
    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2, v0}, LK/E;->a(Law/p;Landroid/content/Context;LK/ab;)LK/E;

    move-result-object v1

    .line 410
    new-instance v2, LK/u;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, LK/u;-><init>(Landroid/content/Context;LK/C;)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/a;->f:LK/T;

    .line 413
    const-string v1, "com.google.android.apps.networktts"

    invoke-interface {v0}, LK/ab;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3c

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3f

    .line 415
    :cond_3c
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->n()V

    .line 427
    :cond_3f
    :goto_3f
    return-void

    .line 417
    :cond_40
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3f

    .line 419
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->b:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->h()Lcom/google/android/maps/driveabout/app/dM;

    move-result-object v0

    .line 420
    new-instance v1, Lcom/google/android/maps/driveabout/app/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/maps/driveabout/app/b;-><init>(Lcom/google/android/maps/driveabout/app/a;Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dM;->a(Ljava/lang/Runnable;)V

    goto :goto_3f
.end method

.method public a(LO/j;II)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 610
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;)V

    .line 611
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-nez v0, :cond_45

    .line 617
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 618
    if-eqz v0, :cond_3a

    .line 619
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e4ccccd

    mul-float/2addr v1, v2

    .line 620
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3ecccccd

    mul-float/2addr v2, v3

    .line 621
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int v3, p2, v3

    .line 622
    int-to-float v4, v3

    neg-float v1, v1

    cmpl-float v1, v4, v1

    if-ltz v1, :cond_3a

    int-to-float v1, v3

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_3a

    .line 623
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 626
    :cond_3a
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/a;->c(LO/j;II)LK/V;

    move-result-object v0

    .line 627
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    .line 628
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;)V

    .line 630
    :cond_45
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/d;)V
    .registers 6
    .parameter

    .prologue
    .line 993
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 994
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/app/d;->a()V

    .line 1002
    :goto_9
    return-void

    .line 997
    :cond_a
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v2, 0x7f0d0016

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, LK/s;-><init>(I)V

    invoke-static {v0, v1, v2}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 1001
    invoke-direct {p0, v0, p1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_9
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 436
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    .line 447
    :goto_9
    return-void

    .line 439
    :cond_a
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 442
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->p:Z

    goto :goto_9
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 493
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/a;->o:Z

    .line 494
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 512
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->m:Z

    return v0
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 482
    iput p1, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    .line 483
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/a;->c(I)V

    .line 484
    return-void
.end method

.method public b(LO/j;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    if-eqz v0, :cond_26

    .line 655
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/a;->c(LO/j;II)LK/V;

    move-result-object v0

    .line 657
    if-eqz v0, :cond_1f

    .line 659
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 660
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LK/T;->a(LK/V;LK/U;)V

    .line 663
    :cond_1f
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->w:Z

    if-eqz v0, :cond_26

    .line 664
    invoke-direct {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->b(LO/j;I)V

    .line 667
    :cond_26
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 984
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 990
    :goto_7
    return-void

    .line 987
    :cond_8
    const/4 v0, 0x3

    invoke-static {v0, p1, v1}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 989
    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_7
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 503
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/a;->m:Z

    .line 504
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    return v0
.end method

.method public c(LO/j;II)LK/V;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 752
    const/4 v0, 0x0

    .line 753
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    if-ne v1, v6, :cond_61

    .line 754
    invoke-virtual {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v1

    .line 755
    if-eqz v1, :cond_8a

    invoke-virtual {p1}, LO/j;->f()Z

    move-result v0

    if-eqz v0, :cond_8a

    invoke-virtual {p1}, LO/j;->g()LO/j;

    move-result-object v0

    if-eqz v0, :cond_8a

    .line 758
    invoke-virtual {p1}, LO/j;->g()LO/j;

    move-result-object v0

    invoke-direct {p0, v0, v6}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;Z)LK/V;

    move-result-object v2

    .line 759
    if-eqz v2, :cond_88

    .line 760
    invoke-virtual {v1}, LK/V;->a()Ljava/lang/String;

    move-result-object v0

    .line 761
    invoke-virtual {p1}, LO/j;->i()Z

    move-result v3

    if-nez v3, :cond_43

    .line 762
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v4, 0x7f0d00a3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-virtual {v2}, LK/V;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 768
    :cond_43
    invoke-virtual {v1}, LK/V;->b()LK/m;

    move-result-object v1

    .line 769
    new-instance v3, LK/s;

    invoke-direct {v3, v7}, LK/s;-><init>(I)V

    invoke-virtual {v2}, LK/V;->b()LK/m;

    move-result-object v2

    invoke-static {v3, v2}, LK/m;->a(LK/m;LK/m;)LK/m;

    move-result-object v2

    .line 772
    invoke-static {v2}, LK/m;->a(LK/m;)LK/m;

    move-result-object v2

    invoke-static {v1, v2}, LK/m;->a(LK/m;LK/m;)LK/m;

    move-result-object v1

    .line 774
    invoke-static {p1, v0, v1}, LK/V;->a(LO/j;Ljava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 788
    :cond_60
    :goto_60
    return-object v0

    .line 777
    :cond_61
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    if-nez v1, :cond_7d

    if-ltz p2, :cond_7d

    .line 779
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->q:Lcom/google/android/maps/driveabout/app/aK;

    invoke-virtual {v1, p2, p3}, Lcom/google/android/maps/driveabout/app/aK;->a(II)LK/V;

    move-result-object v1

    .line 781
    invoke-virtual {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v2

    .line 782
    if-eqz v2, :cond_60

    .line 783
    new-instance v0, LK/W;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-direct {v0, v3, v1, v2}, LK/W;-><init>(Landroid/content/Context;LK/V;LK/V;)V

    goto :goto_60

    .line 785
    :cond_7d
    invoke-virtual {p1}, LO/j;->a()I

    move-result v1

    if-ne v1, v5, :cond_60

    .line 786
    invoke-virtual {p0, p1, p3}, Lcom/google/android/maps/driveabout/app/a;->a(LO/j;I)LK/V;

    move-result-object v0

    goto :goto_60

    :cond_88
    move-object v0, v1

    goto :goto_60

    :cond_8a
    move-object v0, v1

    goto :goto_60
.end method

.method public c(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x3

    .line 526
    if-eqz p1, :cond_29

    .line 527
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 528
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-nez v1, :cond_18

    .line 529
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 531
    :cond_18
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_21

    .line 532
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/g;->c()V

    .line 539
    :cond_21
    :goto_21
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/a;->n:Z

    .line 540
    iget v0, p0, Lcom/google/android/maps/driveabout/app/a;->s:I

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/a;->d(I)V

    .line 541
    return-void

    .line 535
    :cond_29
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    if-eqz v0, :cond_21

    .line 536
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->k:Lcom/google/android/maps/driveabout/app/g;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/g;->b()V

    goto :goto_21
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 557
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->d()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public d()Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 567
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 569
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getVibrateSetting(I)I

    move-result v0

    .line 572
    if-eq v0, v2, :cond_1b

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2b

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->e()Z

    move-result v0

    if-nez v0, :cond_2b

    :cond_1b
    move v0, v2

    .line 575
    :goto_1c
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/app/a;->m:Z

    if-eqz v3, :cond_2d

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2d

    :goto_2a
    return v2

    :cond_2b
    move v0, v1

    .line 572
    goto :goto_1c

    :cond_2d
    move v2, v1

    .line 575
    goto :goto_2a
.end method

.method public e()Z
    .registers 3

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 602
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-lez v0, :cond_19

    const/4 v0, 0x1

    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public f()V
    .registers 2

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 743
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    if-eqz v0, :cond_e

    .line 744
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v0}, LK/T;->a()V

    .line 746
    :cond_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->x:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 747
    return-void
.end method

.method public g()V
    .registers 5

    .prologue
    .line 946
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 954
    :goto_6
    return-void

    .line 949
    :cond_7
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v2, 0x7f0d0013

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, LK/s;-><init>(I)V

    invoke-static {v0, v1, v2}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 953
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_6
.end method

.method public h()V
    .registers 5

    .prologue
    .line 957
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 965
    :goto_6
    return-void

    .line 960
    :cond_7
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v2, 0x7f0d0014

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LK/s;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, LK/s;-><init>(I)V

    invoke-static {v0, v1, v2}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 964
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_6
.end method

.method public i()V
    .registers 4

    .prologue
    const/4 v2, 0x3

    .line 968
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/a;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 976
    :goto_7
    return-void

    .line 971
    :cond_8
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->a:Landroid/content/Context;

    const v1, 0x7f0d0015

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LK/s;

    invoke-direct {v1, v2}, LK/s;-><init>(I)V

    invoke-static {v2, v0, v1}, LK/V;->a(ILjava/lang/CharSequence;LK/m;)LK/V;

    move-result-object v0

    .line 975
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/a;->a(LK/V;Lcom/google/android/maps/driveabout/app/d;)V

    goto :goto_7
.end method

.method public j()LK/V;
    .registers 2

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->t:LK/V;

    return-object v0
.end method

.method public k()J
    .registers 3

    .prologue
    .line 1066
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/app/a;->u:J

    return-wide v0
.end method

.method public l()V
    .registers 2

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    if-eqz v0, :cond_9

    .line 1074
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-interface {v0}, LK/T;->b()V

    .line 1076
    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->h:LK/c;

    invoke-interface {v0}, LK/c;->b()V

    .line 1077
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->i:LK/c;

    invoke-interface {v0}, LK/c;->b()V

    .line 1078
    return-void
.end method

.method public m()Ljava/lang/String;
    .registers 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->d:LK/ac;

    invoke-virtual {v0}, LK/ac;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method n()V
    .registers 3

    .prologue
    .line 1107
    new-instance v0, LK/N;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    invoke-direct {v0, v1}, LK/N;-><init>(LK/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/a;->e:LK/T;

    .line 1108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/a;->w:Z

    .line 1109
    return-void
.end method
