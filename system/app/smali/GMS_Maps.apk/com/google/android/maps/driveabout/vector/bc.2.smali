.class public Lcom/google/android/maps/driveabout/vector/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lo/aq;Lo/aq;)I
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 308
    invoke-virtual {p1}, Lo/aq;->b()I

    move-result v0

    .line 309
    invoke-virtual {p2}, Lo/aq;->b()I

    move-result v1

    .line 310
    if-eq v0, v1, :cond_d

    .line 311
    sub-int v0, v1, v0

    .line 322
    :goto_c
    return v0

    .line 313
    :cond_d
    const/high16 v1, 0x2000

    shr-int v0, v1, v0

    .line 314
    invoke-virtual {p1}, Lo/aq;->e()I

    move-result v1

    add-int/2addr v1, v0

    .line 315
    invoke-virtual {p1}, Lo/aq;->f()I

    move-result v2

    add-int/2addr v2, v0

    .line 316
    invoke-virtual {p2}, Lo/aq;->e()I

    move-result v3

    add-int/2addr v3, v0

    .line 317
    invoke-virtual {p2}, Lo/aq;->f()I

    move-result v4

    add-int/2addr v0, v4

    .line 318
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bc;->a:I

    sub-int/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bc;->b:I

    sub-int/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 320
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bc;->a:I

    sub-int v2, v3, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bc;->b:I

    sub-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    add-int/2addr v0, v2

    .line 322
    sub-int v0, v1, v0

    goto :goto_c
.end method

.method public a(Lo/T;)V
    .registers 3
    .parameter

    .prologue
    .line 302
    invoke-virtual {p1}, Lo/T;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bc;->a:I

    .line 303
    invoke-virtual {p1}, Lo/T;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bc;->b:I

    .line 304
    return-void
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 296
    check-cast p1, Lo/aq;

    check-cast p2, Lo/aq;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/bc;->a(Lo/aq;Lo/aq;)I

    move-result v0

    return v0
.end method
