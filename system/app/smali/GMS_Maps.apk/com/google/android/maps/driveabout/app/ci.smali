.class public Lcom/google/android/maps/driveabout/app/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/android/b;


# static fields
.field private static a:Z

.field private static b:Lcom/google/android/maps/driveabout/app/ci;

.field private static final c:Lcom/google/googlenav/j;

.field private static d:Lcom/google/googlenav/ui/android/L;

.field private static volatile e:Z


# instance fields
.field private final f:Landroid/app/Application;

.field private g:Lcom/google/android/maps/driveabout/app/NavigationActivity;

.field private h:Lcom/google/android/maps/driveabout/app/NavigationService;

.field private i:LaH/h;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/googlenav/j;

    invoke-direct {v0}, Lcom/google/googlenav/j;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    return-void
.end method

.method private constructor <init>(Landroid/app/Application;)V
    .registers 2
    .parameter

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->f:Landroid/app/Application;

    .line 139
    return-void
.end method

.method public static a()Lcom/google/android/maps/driveabout/app/ci;
    .registers 1

    .prologue
    .line 121
    sget-object v0, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    return-object v0
.end method

.method public static a(Landroid/app/Application;)V
    .registers 3
    .parameter

    .prologue
    .line 85
    sget-boolean v0, Lcom/google/android/maps/driveabout/app/ci;->a:Z

    if-nez v0, :cond_7

    .line 86
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->e(Landroid/content/Context;)V

    :cond_7
    move-object v0, p0

    .line 88
    check-cast v0, Lcom/google/googlenav/android/AndroidGmmApplication;

    .line 89
    invoke-virtual {v0}, Lcom/google/googlenav/android/AndroidGmmApplication;->b()Lcom/google/googlenav/android/b;

    move-result-object v1

    if-nez v1, :cond_20

    .line 90
    sget-object v1, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    if-nez v1, :cond_1b

    .line 91
    new-instance v1, Lcom/google/android/maps/driveabout/app/ci;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ci;-><init>(Landroid/app/Application;)V

    sput-object v1, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    .line 93
    :cond_1b
    sget-object v1, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/android/AndroidGmmApplication;->b(Lcom/google/googlenav/android/b;)V

    .line 95
    :cond_20
    return-void
.end method

.method private static a(Landroid/content/Context;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x138e

    const/16 v3, 0x1068

    const/16 v0, 0xfa0

    .line 407
    if-ge p1, v0, :cond_1c

    if-lt p2, v0, :cond_1c

    .line 408
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "google_maps_navigation"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, p0}, LJ/a;->a(Ljava/io/File;Landroid/content/Context;)Z

    .line 413
    :cond_1c
    if-ge p1, v3, :cond_42

    if-lt p2, v3, :cond_42

    .line 415
    const/4 v0, 0x6

    :goto_21
    if-ltz v0, :cond_42

    .line 416
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "._speech_nav_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".wav"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 417
    invoke-virtual {p0, v1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_64

    .line 422
    :cond_42
    if-ge p1, v4, :cond_63

    if-lt p2, v4, :cond_63

    .line 423
    invoke-static {p0}, LJ/a;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 425
    const-string v1, "cache_ImageTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 427
    const-string v1, "cache_LayerTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 429
    const-string v1, "cache_RoadGraphTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 431
    const-string v1, "cache_VectorTileStore"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 433
    const-string v1, "cache_Resource"

    invoke-static {v0, v1}, LJ/a;->a(Ljava/io/File;Ljava/lang/String;)V

    .line 435
    :cond_63
    return-void

    .line 415
    :cond_64
    add-int/lit8 v0, v0, -0x1

    goto :goto_21
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 383
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 384
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 385
    const/high16 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_17

    const/4 v0, 0x1

    :goto_16
    return v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method private static a(Ljava/lang/String;I)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 342
    if-gtz p1, :cond_5

    .line 352
    :cond_4
    :goto_4
    return v0

    .line 345
    :cond_5
    const/16 v2, 0x64

    if-lt p1, v2, :cond_b

    move v0, v1

    .line 346
    goto :goto_4

    .line 348
    :cond_b
    if-eqz p0, :cond_4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    rem-int/lit8 v2, v2, 0x64

    if-ge v2, p1, :cond_4

    move v0, v1

    .line 350
    goto :goto_4
.end method

.method public static b()Z
    .registers 1

    .prologue
    .line 126
    sget-object v0, Lcom/google/android/maps/driveabout/app/ci;->b:Lcom/google/android/maps/driveabout/app/ci;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public static b(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 362
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_providers_allowed"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 364
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .registers 1

    .prologue
    .line 201
    sget-boolean v0, Lcom/google/android/maps/driveabout/app/ci;->e:Z

    return v0
.end method

.method public static c(Landroid/content/Context;)Z
    .registers 2
    .parameter

    .prologue
    .line 373
    invoke-static {p0}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public static d(Landroid/content/Context;)Z
    .registers 2
    .parameter

    .prologue
    .line 378
    const-string v0, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static e(Landroid/content/Context;)V
    .registers 7
    .parameter

    .prologue
    .line 145
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    new-instance v1, LR/d;

    invoke-direct {v1}, LR/d;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 147
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->f(Landroid/content/Context;)V

    .line 148
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    const-string v3, "DriveAbout"

    const v4, 0x7f070003

    new-instance v5, Lcom/google/googlenav/android/I;

    invoke-direct {v5, p0}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V

    .line 154
    invoke-static {}, Law/h;->b()Law/p;

    move-result-object v0

    .line 159
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->j()V

    .line 160
    invoke-static {p0, v0}, LO/c;->a(Landroid/content/Context;Law/p;)V

    .line 161
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/dx;->a(Landroid/content/Context;)V

    .line 162
    new-instance v0, Lcom/google/googlenav/ui/android/L;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/android/L;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/maps/driveabout/app/ci;->d:Lcom/google/googlenav/ui/android/L;

    .line 169
    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v0

    invoke-virtual {v0}, Las/c;->d()V

    .line 171
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/maps/driveabout/app/ci;->a:Z

    .line 172
    return-void
.end method

.method private static f(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 394
    const-string v0, "LastRunVersion"

    const/16 v1, 0xce4

    invoke-static {p0, v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 395
    const-string v1, "6140011"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 396
    if-eq v0, v1, :cond_18

    .line 399
    invoke-static {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;II)V

    .line 400
    const-string v0, "LastRunVersion"

    invoke-static {p0, v0, v1}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 402
    :cond_18
    return-void
.end method

.method private static j()V
    .registers 2

    .prologue
    .line 175
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    .line 180
    sget-object v1, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/j;->a([I)V

    .line 181
    return-void

    .line 175
    :array_c
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method


# virtual methods
.method public a(LaH/h;)V
    .registers 2
    .parameter

    .prologue
    .line 450
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->i:LaH/h;

    .line 451
    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 219
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    .line 220
    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/Config;->a(Landroid/content/res/Configuration;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ci;->f:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LO/c;->a(Landroid/content/Context;)V

    .line 226
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bf;->a(Ljava/util/Locale;)V

    .line 227
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->g:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    .line 245
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .registers 2
    .parameter

    .prologue
    .line 261
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/ci;->h:Lcom/google/android/maps/driveabout/app/NavigationService;

    .line 262
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 280
    const-string v1, "DriveAbout"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 282
    const-string v2, "RmiOverride"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 283
    const-string v2, "RmiOverride"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 308
    :cond_15
    :goto_15
    return v0

    .line 293
    :cond_16
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ci;->i:LaH/h;

    if-eqz v1, :cond_15

    .line 296
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/ci;->i:LaH/h;

    invoke-virtual {v1}, LaH/h;->r()LaN/B;

    move-result-object v1

    .line 297
    sget-object v2, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v1, v0}, Lcom/google/googlenav/j;->a(ILaN/B;Z)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 304
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v1

    .line 305
    if-eqz v1, :cond_15

    .line 308
    invoke-static {p1}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, LR/m;->A()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_15
.end method

.method public a(Landroid/content/Context;LR/m;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 320
    const-string v1, "DriveAbout"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 322
    const-string v2, "OfflineRoutingOverride"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 323
    const-string v2, "OfflineRoutingOverride"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 337
    :cond_15
    :goto_15
    return v0

    .line 328
    :cond_16
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 329
    const/4 v0, 0x1

    goto :goto_15

    .line 334
    :cond_1e
    if-eqz p2, :cond_15

    .line 337
    invoke-static {p1}, LJ/a;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, LR/m;->B()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Ljava/lang/String;I)Z

    move-result v0

    goto :goto_15
.end method

.method public d()V
    .registers 1

    .prologue
    .line 210
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->c()V

    .line 211
    return-void
.end method

.method public e()V
    .registers 1

    .prologue
    .line 236
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->d()V

    .line 237
    return-void
.end method

.method public f()Lcom/google/android/maps/driveabout/app/NavigationActivity;
    .registers 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ci;->g:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    return-object v0
.end method

.method public g()Lcom/google/android/maps/driveabout/app/NavigationService;
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/ci;->h:Lcom/google/android/maps/driveabout/app/NavigationService;

    return-object v0
.end method

.method public h()Lcom/google/googlenav/j;
    .registers 2

    .prologue
    .line 357
    sget-object v0, Lcom/google/android/maps/driveabout/app/ci;->c:Lcom/google/googlenav/j;

    return-object v0
.end method

.method public synthetic i()Landroid/app/Activity;
    .registers 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/ci;->f()Lcom/google/android/maps/driveabout/app/NavigationActivity;

    move-result-object v0

    return-object v0
.end method
