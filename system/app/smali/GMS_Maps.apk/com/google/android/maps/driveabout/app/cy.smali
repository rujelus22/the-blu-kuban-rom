.class Lcom/google/android/maps/driveabout/app/cy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/NavigationService;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/app/NavigationService;)V
    .registers 2
    .parameter

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    check-cast p2, Lcom/google/android/maps/driveabout/publisher/d;

    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/publisher/d;->a()Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    .line 194
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->d(Lcom/google/android/maps/driveabout/app/NavigationService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a()LO/q;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;LO/q;)LO/q;

    .line 195
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->f(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/t;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->e(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/q;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/t;->a(LO/q;)V

    .line 196
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 200
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->f(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/t;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/NavigationService;->e(Lcom/google/android/maps/driveabout/app/NavigationService;)LO/q;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/t;->b(LO/q;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    .line 202
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cy;->a:Lcom/google/android/maps/driveabout/app/NavigationService;

    invoke-static {v0, v2}, Lcom/google/android/maps/driveabout/app/NavigationService;->a(Lcom/google/android/maps/driveabout/app/NavigationService;LO/q;)LO/q;

    .line 203
    return-void
.end method
