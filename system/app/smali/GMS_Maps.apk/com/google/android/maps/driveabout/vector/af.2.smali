.class public Lcom/google/android/maps/driveabout/vector/aF;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private a:Ljava/util/ArrayList;

.field private b:[Ly/c;

.field private c:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->c:I

    .line 33
    return-void
.end method

.method private c()V
    .registers 5

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ly/c;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    .line 118
    const/4 v0, 0x0

    move v2, v0

    :goto_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_36

    .line 122
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 123
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly/c;

    aput-object v1, v3, v2

    .line 124
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_32

    .line 125
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 118
    :cond_32
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_c

    .line 128
    :cond_36
    return-void
.end method


# virtual methods
.method public a()Ly/c;
    .registers 8

    .prologue
    const/4 v5, 0x0

    const/4 v1, -0x1

    .line 56
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    if-nez v0, :cond_9

    .line 57
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aF;->c()V

    .line 65
    :cond_9
    const/4 v0, 0x0

    move v2, v1

    :goto_b
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    array-length v3, v3

    if-ge v0, v3, :cond_30

    .line 66
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v3, v3, v0

    if-eqz v3, :cond_5f

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ly/c;->b()I

    move-result v3

    if-le v3, v1, :cond_5f

    .line 68
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ly/c;->b()I

    move-result v1

    move v2, v1

    move v1, v0

    .line 65
    :goto_2a
    add-int/lit8 v0, v0, 0x1

    move v6, v1

    move v1, v2

    move v2, v6

    goto :goto_b

    .line 71
    :cond_30
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v3, v0, v2

    .line 74
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 75
    if-eqz v0, :cond_54

    .line 76
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly/c;

    aput-object v1, v4, v2

    .line 77
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_53

    .line 78
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v5}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 84
    :cond_53
    :goto_53
    return-object v3

    .line 81
    :cond_54
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aput-object v5, v0, v2

    .line 82
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->c:I

    goto :goto_53

    :cond_5f
    move v6, v2

    move v2, v1

    move v1, v6

    goto :goto_2a
.end method

.method public a(Ljava/util/Iterator;)V
    .registers 4
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    if-eqz v0, :cond_c

    .line 41
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call addIterator after next has been called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_c
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 45
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_17
    return-void
.end method

.method public b()Ly/c;
    .registers 5

    .prologue
    const/4 v1, -0x1

    .line 89
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    if-nez v0, :cond_8

    .line 90
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aF;->c()V

    .line 98
    :cond_8
    const/4 v0, 0x0

    move v2, v1

    :goto_a
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    array-length v3, v3

    if-ge v0, v3, :cond_2b

    .line 99
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v3, v3, v0

    if-eqz v3, :cond_28

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ly/c;->b()I

    move-result v3

    if-le v3, v2, :cond_28

    .line 101
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Ly/c;->b()I

    move-result v2

    move v1, v0

    .line 98
    :cond_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 104
    :cond_2b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->b:[Ly/c;

    aget-object v0, v0, v1

    .line 105
    return-object v0
.end method

.method public hasNext()Z
    .registers 3

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aF;->c:I

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aF;->a()Ly/c;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "[RankMergingFeatureIterator"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 133
    const/4 v0, 0x0

    :goto_8
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_22

    .line 134
    const/16 v2, 0x7c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aF;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 136
    :cond_22
    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
