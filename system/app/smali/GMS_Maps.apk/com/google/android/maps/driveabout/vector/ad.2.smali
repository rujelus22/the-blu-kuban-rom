.class public Lcom/google/android/maps/driveabout/vector/aD;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Z

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 1022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1031
    const v0, 0x73217bce

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->i:I

    .line 1032
    const v0, 0x338cc6ef

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->j:I

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/vector/aD;
    .registers 2

    .prologue
    .line 1083
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->h:Z

    .line 1084
    return-object p0
.end method

.method public a(I)Lcom/google/android/maps/driveabout/vector/aD;
    .registers 3
    .parameter

    .prologue
    .line 1053
    invoke-virtual {p0, p1, p1}, Lcom/google/android/maps/driveabout/vector/aD;->a(II)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lcom/google/android/maps/driveabout/vector/aD;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1061
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->d:Ljava/lang/Integer;

    .line 1062
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->e:Ljava/lang/Integer;

    .line 1063
    return-object p0
.end method

.method public a(Z)Lcom/google/android/maps/driveabout/vector/aD;
    .registers 3
    .parameter

    .prologue
    .line 1035
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->a:Ljava/lang/Boolean;

    .line 1036
    return-object p0
.end method

.method public b()Lcom/google/android/maps/driveabout/vector/aD;
    .registers 2

    .prologue
    .line 1088
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->h:Z

    .line 1089
    return-object p0
.end method

.method public b(II)Lcom/google/android/maps/driveabout/vector/aD;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1077
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aD;->i:I

    .line 1078
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/aD;->j:I

    .line 1079
    return-object p0
.end method

.method public b(Z)Lcom/google/android/maps/driveabout/vector/aD;
    .registers 3
    .parameter

    .prologue
    .line 1040
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->b:Ljava/lang/Boolean;

    .line 1041
    return-object p0
.end method

.method public c()Lcom/google/android/maps/driveabout/vector/aC;
    .registers 12

    .prologue
    const/4 v8, 0x0

    .line 1093
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    :goto_6
    const-string v1, "Texture ID must be specified."

    invoke-static {v0, v1}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    .line 1094
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aC;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aD;->a:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aD;->b:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aD;->c:Ljava/lang/Boolean;

    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aD;->h:Z

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aD;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aD;->e:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/aD;->f:Ljava/lang/Integer;

    if-nez v7, :cond_34

    move v7, v8

    :goto_26
    iget-object v9, p0, Lcom/google/android/maps/driveabout/vector/aD;->g:Ljava/lang/Integer;

    if-nez v9, :cond_3b

    :goto_2a
    iget v9, p0, Lcom/google/android/maps/driveabout/vector/aD;->i:I

    iget v10, p0, Lcom/google/android/maps/driveabout/vector/aD;->j:I

    invoke-direct/range {v0 .. v10}, Lcom/google/android/maps/driveabout/vector/aC;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIIII)V

    return-object v0

    :cond_32
    move v0, v8

    .line 1093
    goto :goto_6

    .line 1094
    :cond_34
    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/aD;->f:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    goto :goto_26

    :cond_3b
    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aD;->g:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_2a
.end method

.method public c(Z)Lcom/google/android/maps/driveabout/vector/aD;
    .registers 3
    .parameter

    .prologue
    .line 1045
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aD;->c:Ljava/lang/Boolean;

    .line 1046
    return-object p0
.end method
