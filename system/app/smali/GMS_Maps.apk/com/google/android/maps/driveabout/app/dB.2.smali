.class public Lcom/google/android/maps/driveabout/app/dB;
.super LR/c;
.source "SourceFile"

# interfaces
.implements LO/q;
.implements Lcom/google/android/maps/driveabout/vector/ay;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/dE;

.field private b:Landroid/os/Handler;

.field private c:Landroid/os/Looper;

.field private final d:Lcom/google/android/maps/driveabout/app/dz;

.field private e:Z

.field private f:LO/z;

.field private g:LO/N;

.field private h:I

.field private i:Z

.field private final j:Lr/t;

.field private k:Z

.field private l:I

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Lr/t;Lcom/google/android/maps/driveabout/app/dz;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 117
    const-string v0, "TilePrefetcher"

    invoke-direct {p0, v0}, LR/c;-><init>(Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/google/android/maps/driveabout/app/dE;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/dE;-><init>(Lcom/google/android/maps/driveabout/app/dB;Lcom/google/android/maps/driveabout/app/dC;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->a:Lcom/google/android/maps/driveabout/app/dE;

    .line 93
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    .line 118
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lr/t;

    .line 119
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    .line 120
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dB;->start()V

    .line 123
    :try_start_17
    monitor-enter p0
    :try_end_18
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_18} :catch_23

    .line 124
    :goto_18
    :try_start_18
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->b:Landroid/os/Handler;

    if-nez v0, :cond_2f

    .line 125
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    goto :goto_18

    .line 127
    :catchall_20
    move-exception v0

    monitor-exit p0
    :try_end_22
    .catchall {:try_start_18 .. :try_end_22} :catchall_20

    :try_start_22
    throw v0
    :try_end_23
    .catch Ljava/lang/InterruptedException; {:try_start_22 .. :try_end_23} :catch_23

    .line 128
    :catch_23
    move-exception v0

    .line 133
    :goto_24
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lr/t;

    new-instance v1, Lcom/google/android/maps/driveabout/app/dC;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dC;-><init>(Lcom/google/android/maps/driveabout/app/dB;)V

    invoke-interface {v0, v1}, Lr/t;->a(Lr/A;)V

    .line 144
    return-void

    .line 127
    :cond_2f
    :try_start_2f
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_20

    goto :goto_24
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 152
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(IILjava/lang/Object;)V

    .line 153
    return-void
.end method

.method private a(IILjava/lang/Object;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1, p3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 175
    return-void
.end method

.method private a(ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/maps/driveabout/app/dB;->a(IILjava/lang/Object;)V

    .line 163
    return-void
.end method

.method private a(J)V
    .registers 5
    .parameter

    .prologue
    .line 447
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:Z

    if-nez v0, :cond_5

    .line 462
    :cond_4
    :goto_4
    return-void

    .line 449
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    if-nez v0, :cond_4

    .line 451
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->k:Z

    if-nez v0, :cond_4

    .line 453
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    if-gtz v0, :cond_4

    .line 455
    const-wide/16 v0, 0x4

    cmp-long v0, p1, v0

    if-gtz v0, :cond_1b

    .line 456
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->j()V

    goto :goto_4

    .line 458
    :cond_1b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->b:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 459
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->k:Z

    goto :goto_4
.end method

.method private a(LO/N;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 393
    if-eqz p1, :cond_20

    if-ltz p2, :cond_20

    .line 394
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    .line 395
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dB;->g:LO/N;

    .line 396
    iput p2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    .line 397
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->g:LO/N;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/N;I)V

    .line 398
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->w()I

    move-result v0

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(J)V

    .line 401
    :cond_20
    return-void
.end method

.method private a(LO/z;)V
    .registers 4
    .parameter

    .prologue
    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    .line 386
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dB;->f:LO/z;

    .line 387
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->f:LO/z;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/z;)V

    .line 388
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->f()V

    .line 389
    const-wide/16 v0, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(J)V

    .line 390
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dB;)V
    .registers 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dB;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dB;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dB;IILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/dB;->a(IILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dB;LO/N;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/dB;->a(LO/N;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dB;LO/z;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dB;->a(LO/z;)V

    return-void
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 414
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    .line 415
    if-nez p1, :cond_e

    .line 416
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->n:I

    .line 424
    :cond_e
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    .line 425
    iget v1, p0, Lcom/google/android/maps/driveabout/app/dB;->n:I

    if-nez v1, :cond_1f

    invoke-virtual {v0}, LR/m;->v()I

    move-result v0

    int-to-long v0, v0

    :goto_1b
    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(J)V

    .line 428
    return-void

    .line 425
    :cond_1f
    invoke-virtual {v0}, LR/m;->w()I

    move-result v0

    int-to-long v0, v0

    goto :goto_1b
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/dB;)V
    .registers 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->e()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/dB;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/dB;->b(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/dB;)V
    .registers 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->g()V

    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:Z

    .line 353
    const-wide/16 v0, 0x4e20

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(J)V

    .line 354
    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/dB;)V
    .registers 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->h()V

    return-void
.end method

.method private e()V
    .registers 2

    .prologue
    .line 357
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:Z

    .line 358
    return-void
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/dB;)V
    .registers 1
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->i()V

    return-void
.end method

.method private f()V
    .registers 6

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dz;->c()Lo/aq;

    move-result-object v0

    .line 362
    if-eqz v0, :cond_18

    .line 366
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lr/t;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dB;->a:Lcom/google/android/maps/driveabout/app/dE;

    sget-object v3, Lr/c;->d:Lr/c;

    const/4 v4, 0x0

    invoke-interface {v1, v0, v2, v3, v4}, Lr/t;->a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;

    .line 368
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    .line 370
    :cond_18
    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->f:LO/z;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->g:LO/N;

    if-eqz v0, :cond_2c

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    if-ltz v0, :cond_2c

    .line 374
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    .line 375
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dz;->a()V

    .line 376
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->f:LO/z;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/z;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dB;->g:LO/N;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dB;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(LO/N;I)V

    .line 378
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->f()V

    .line 380
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(J)V

    .line 382
    :cond_2c
    return-void
.end method

.method private h()V
    .registers 2

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->k:Z

    .line 405
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->j()V

    .line 406
    return-void
.end method

.method private i()V
    .registers 2

    .prologue
    .line 436
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    .line 437
    return-void
.end method

.method private j()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x190

    .line 468
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->e:Z

    if-eqz v0, :cond_1b

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    if-nez v0, :cond_1b

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    if-nez v0, :cond_1b

    .line 470
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lr/t;

    invoke-interface {v0}, Lr/t;->j()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1c

    .line 471
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dB;->k()V

    .line 480
    :cond_1b
    :goto_1b
    return-void

    .line 477
    :cond_1c
    invoke-direct {p0, v2, v3}, Lcom/google/android/maps/driveabout/app/dB;->a(J)V

    goto :goto_1b
.end method

.method private k()V
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 484
    iput v1, p0, Lcom/google/android/maps/driveabout/app/dB;->n:I

    .line 485
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->u()I

    move-result v2

    move v0, v1

    .line 487
    :goto_c
    if-ge v0, v2, :cond_19

    .line 488
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dB;->d:Lcom/google/android/maps/driveabout/app/dz;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/app/dz;->c()Lo/aq;

    move-result-object v3

    .line 489
    if-nez v3, :cond_1a

    .line 490
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dB;->i:Z

    .line 497
    :cond_19
    return-void

    .line 493
    :cond_1a
    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/dB;->j:Lr/t;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dB;->a:Lcom/google/android/maps/driveabout/app/dE;

    sget-object v6, Lr/c;->d:Lr/c;

    invoke-interface {v4, v3, v5, v6, v1}, Lr/t;->a(Lo/aq;Ls/e;Lr/c;Z)Lr/v;

    .line 495
    iget v3, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/maps/driveabout/app/dB;->m:I

    .line 487
    add-int/lit8 v0, v0, 0x1

    goto :goto_c
.end method


# virtual methods
.method public a(ILO/g;LO/s;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 224
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(I)V

    .line 225
    return-void
.end method

.method public a(LO/j;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 230
    return-void
.end method

.method public a(LO/s;)V
    .registers 3
    .parameter

    .prologue
    .line 211
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(I)V

    .line 212
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 188
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(I)V

    .line 189
    return-void
.end method

.method public b(LO/j;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 235
    return-void
.end method

.method public b(LO/s;)V
    .registers 5
    .parameter

    .prologue
    .line 240
    invoke-virtual {p1}, LO/s;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->l:I

    .line 241
    const/4 v0, 0x5

    invoke-virtual {p1}, LO/s;->e()I

    move-result v1

    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dB;->a(IILjava/lang/Object;)V

    .line 243
    return-void
.end method

.method public c(LO/s;)V
    .registers 5
    .parameter

    .prologue
    .line 248
    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v0

    if-nez v0, :cond_7

    .line 258
    :cond_6
    :goto_6
    return-void

    .line 251
    :cond_7
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dB;->l:I

    invoke-virtual {p1}, LO/s;->b()I

    move-result v1

    sub-int/2addr v0, v1

    .line 253
    const/16 v1, 0x7530

    if-lt v0, v1, :cond_6

    .line 254
    invoke-virtual {p1}, LO/s;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dB;->l:I

    .line 255
    const/4 v0, 0x5

    invoke-virtual {p1}, LO/s;->e()I

    move-result v1

    invoke-virtual {p1}, LO/s;->h()LO/N;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/maps/driveabout/app/dB;->a(IILjava/lang/Object;)V

    goto :goto_6
.end method

.method public d(LO/s;)V
    .registers 4
    .parameter

    .prologue
    .line 217
    const/4 v0, 0x4

    invoke-virtual {p1}, LO/s;->g()LO/z;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dB;->a(ILjava/lang/Object;)V

    .line 218
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/dB;->b(LO/s;)V

    .line 219
    return-void
.end method

.method public e(LO/s;)V
    .registers 3
    .parameter

    .prologue
    .line 263
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(I)V

    .line 264
    return-void
.end method

.method public f(LO/s;)V
    .registers 2
    .parameter

    .prologue
    .line 269
    return-void
.end method

.method public g(LO/s;)V
    .registers 2
    .parameter

    .prologue
    .line 274
    return-void
.end method

.method public l()V
    .registers 5

    .prologue
    .line 306
    const/16 v0, 0xa

    :try_start_2
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_5
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_5} :catch_1e

    .line 312
    :goto_5
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 313
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->c:Landroid/os/Looper;

    .line 314
    new-instance v0, Lcom/google/android/maps/driveabout/app/dD;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dD;-><init>(Lcom/google/android/maps/driveabout/app/dB;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->b:Landroid/os/Handler;

    .line 345
    monitor-enter p0

    .line 346
    :try_start_16
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 347
    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_38

    .line 348
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 349
    return-void

    .line 307
    :catch_1e
    move-exception v0

    .line 308
    const-string v1, "TilePrefetcherThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 347
    :catchall_38
    move-exception v0

    :try_start_39
    monitor-exit p0
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_38

    throw v0
.end method

.method public n_()V
    .registers 2

    .prologue
    .line 182
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dB;->a(I)V

    .line 183
    return-void
.end method

.method public o_()V
    .registers 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->c:Landroid/os/Looper;

    if-eqz v0, :cond_9

    .line 195
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dB;->c:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 197
    :cond_9
    return-void
.end method
