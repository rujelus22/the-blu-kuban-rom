.class public Lcom/google/android/maps/driveabout/vector/G;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# static fields
.field private static final a:Lo/T;

.field private static final b:Lo/T;


# instance fields
.field private final c:Ljava/util/List;

.field private final d:Lo/ad;

.field private final e:Ljava/util/List;

.field private final f:Ljava/lang/Object;

.field private g:Lm/v;

.field private h:Lo/ad;

.field private i:F

.field private j:F

.field private k:B

.field private l:Z

.field private m:Z

.field private n:LE/o;

.field private o:LE/d;

.field private p:Lo/ad;

.field private q:I

.field private final r:Ljava/lang/Object;

.field private s:I

.field private t:I

.field private final u:Ljava/util/List;

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 102
    new-instance v0, Lo/T;

    const/high16 v1, -0x4000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/G;->a:Lo/T;

    .line 103
    new-instance v0, Lo/T;

    const/high16 v1, 0x4000

    invoke-direct {v0, v1, v2}, Lo/T;-><init>(II)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/G;->b:Lo/T;

    return-void
.end method

.method public constructor <init>(Lo/X;Ljava/util/List;III)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 264
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    .line 135
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    .line 141
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    .line 147
    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    .line 203
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    .line 245
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/G;->v:Z

    .line 265
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    .line 266
    invoke-virtual {p1}, Lo/X;->f()I

    move-result v2

    .line 267
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-virtual {p1, v2}, Lo/X;->c(I)Lo/X;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_37
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 269
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-virtual {v0, v2}, Lo/X;->c(I)Lo/X;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_37

    .line 271
    :cond_4d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    invoke-virtual {v0}, Lo/X;->a()Lo/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    .line 273
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/G;->t:I

    .line 274
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/G;->s:I

    .line 275
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    .line 276
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_88

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    .line 277
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    new-instance v4, Lcom/google/android/maps/driveabout/vector/z;

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/G;->t:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/G;->s:I

    const/4 v7, 0x0

    invoke-direct {v4, v0, v5, v6, v7}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6b

    .line 280
    :cond_88
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/G;->q:I

    .line 282
    invoke-static {}, Lm/v;->a()Lm/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p5, :cond_ae

    move v0, v1

    :goto_9e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 287
    const/16 v1, 0x78

    const-string v2, "t"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 290
    return-void

    .line 284
    :cond_ae
    const/4 v0, 0x1

    goto :goto_9e
.end method

.method private static a(Lo/ad;Lo/ad;)B
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 512
    const/4 v0, 0x0

    .line 513
    invoke-virtual {p1, p0}, Lo/ad;->a(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 514
    const/4 v0, 0x1

    int-to-byte v0, v0

    .line 517
    :cond_9
    sget-object v1, Lcom/google/android/maps/driveabout/vector/G;->b:Lo/T;

    .line 518
    new-instance v2, Lo/ad;

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3, v1}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v4

    invoke-virtual {v4, v1}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    .line 519
    invoke-virtual {v2, p0}, Lo/ad;->a(Lo/ae;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 520
    or-int/lit8 v0, v0, 0x4

    int-to-byte v0, v0

    .line 523
    :cond_29
    new-instance v2, Lo/ad;

    invoke-virtual {p1}, Lo/ad;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v3

    invoke-virtual {p1}, Lo/ad;->e()Lo/T;

    move-result-object v4

    invoke-virtual {v4, v1}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    .line 525
    invoke-virtual {v2, p0}, Lo/ad;->a(Lo/ae;)Z

    move-result v1

    if-eqz v1, :cond_47

    .line 526
    or-int/lit8 v0, v0, 0x2

    int-to-byte v0, v0

    .line 529
    :cond_47
    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/G;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/G;Lm/v;)Lm/v;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    return-object p1
.end method

.method private a(LC/a;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 323
    if-nez p2, :cond_e

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->a(LC/a;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->b(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_e
    const/4 v0, 0x1

    .line 325
    :goto_f
    if-eqz v0, :cond_14

    .line 326
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->f(LC/a;)V

    .line 328
    :cond_14
    if-nez v0, :cond_1c

    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->c(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 329
    :cond_1c
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->g(LC/a;)V

    .line 330
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/G;->h()V

    .line 332
    :cond_22
    return-void

    .line 323
    :cond_23
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private a(LD/a;LC/a;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 426
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 429
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->p:Lo/ad;

    invoke-virtual {v1}, Lo/ad;->d()Lo/T;

    move-result-object v1

    .line 430
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->p:Lo/ad;

    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v2

    int-to-float v2, v2

    .line 431
    invoke-static {p1, p2, v1, v2}, Lcom/google/android/maps/driveabout/vector/be;->b(LD/a;LC/a;Lo/T;F)V

    .line 434
    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 440
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    invoke-virtual {v1, p1}, LE/o;->d(LD/a;)V

    .line 441
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 442
    :try_start_22
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/G;->q:I

    invoke-static {v0, v2}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 443
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_22 .. :try_end_28} :catchall_2f

    .line 444
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->o:LE/d;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LE/d;->a(LD/a;I)V

    .line 445
    return-void

    .line 443
    :catchall_2f
    move-exception v0

    :try_start_30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    throw v0
.end method

.method private a(LC/a;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 452
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    invoke-static {v0, v3}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v0

    if-nez v0, :cond_22

    move v0, v1

    :goto_11
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v3

    if-nez v3, :cond_24

    move v3, v1

    :goto_1e
    if-eq v0, v3, :cond_21

    :cond_20
    move v2, v1

    :cond_21
    return v2

    :cond_22
    move v0, v2

    goto :goto_11

    :cond_24
    move v3, v2

    goto :goto_1e
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/G;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/G;->m:Z

    return p1
.end method

.method private static b(I)I
    .registers 3
    .parameter

    .prologue
    .line 661
    const/4 v0, 0x2

    rsub-int/lit8 v1, p0, 0x1e

    shl-int/2addr v0, v1

    div-int/lit16 v0, v0, 0x100

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    return-object v0
.end method

.method private b(LD/a;LC/a;)V
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 605
    invoke-static {p2}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v4

    .line 607
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 608
    :try_start_7
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/G;->g:Lm/v;

    .line 609
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_93

    .line 617
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {v0, v4}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v6

    .line 618
    and-int/lit8 v0, v6, 0x1

    if-eqz v0, :cond_96

    const/4 v0, 0x1

    move v3, v0

    .line 619
    :goto_16
    if-eqz v3, :cond_9a

    const/4 v0, 0x1

    .line 621
    :goto_19
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_9d

    const/4 v1, 0x1

    move v2, v1

    .line 622
    :goto_1f
    if-eqz v2, :cond_23

    add-int/lit8 v0, v0, 0x1

    .line 624
    :cond_23
    and-int/lit8 v1, v6, 0x4

    if-eqz v1, :cond_a0

    const/4 v1, 0x1

    .line 625
    :goto_28
    if-eqz v1, :cond_2c

    add-int/lit8 v0, v0, 0x1

    .line 627
    :cond_2c
    invoke-virtual {v5}, Lm/v;->e()I

    move-result v7

    .line 628
    new-instance v8, LE/o;

    mul-int v9, v7, v0

    invoke-direct {v8, v9}, LE/o;-><init>(I)V

    .line 629
    new-instance v9, LE/d;

    invoke-virtual {v5}, Lm/v;->d()I

    move-result v10

    mul-int/lit8 v10, v10, 0x3

    mul-int/2addr v0, v10

    invoke-direct {v9, v0}, LE/d;-><init>(I)V

    .line 631
    const/4 v0, 0x0

    .line 632
    invoke-virtual {v4}, Lo/ad;->d()Lo/T;

    move-result-object v10

    .line 633
    invoke-virtual {v4}, Lo/ad;->g()I

    move-result v11

    .line 634
    if-eqz v3, :cond_56

    .line 635
    const/4 v0, 0x0

    invoke-static {v5, v9, v0}, Lm/o;->a(Lm/v;LE/e;I)V

    .line 636
    invoke-static {v5, v8, v10, v11}, Lm/o;->a(Lm/v;LE/q;Lo/T;I)V

    .line 637
    const/4 v0, 0x1

    .line 639
    :cond_56
    if-eqz v2, :cond_68

    .line 640
    mul-int v2, v7, v0

    invoke-static {v5, v9, v2}, Lm/o;->a(Lm/v;LE/e;I)V

    .line 641
    sget-object v2, Lcom/google/android/maps/driveabout/vector/G;->a:Lo/T;

    invoke-virtual {v10, v2}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v2

    invoke-static {v5, v8, v2, v11}, Lm/o;->a(Lm/v;LE/q;Lo/T;I)V

    .line 643
    add-int/lit8 v0, v0, 0x1

    .line 645
    :cond_68
    if-eqz v1, :cond_77

    .line 646
    mul-int/2addr v0, v7

    invoke-static {v5, v9, v0}, Lm/o;->a(Lm/v;LE/e;I)V

    .line 647
    sget-object v0, Lcom/google/android/maps/driveabout/vector/G;->b:Lo/T;

    invoke-virtual {v10, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    invoke-static {v5, v8, v0, v11}, Lm/o;->a(Lm/v;LE/q;Lo/T;I)V

    .line 651
    :cond_77
    iput-object v8, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    .line 652
    iput-object v9, p0, Lcom/google/android/maps/driveabout/vector/G;->o:LE/d;

    .line 654
    new-instance v0, Lo/ad;

    invoke-virtual {v4}, Lo/ad;->d()Lo/T;

    move-result-object v1

    invoke-virtual {v4}, Lo/ad;->e()Lo/T;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->p:Lo/ad;

    .line 655
    invoke-virtual {p2}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/G;->j:F

    .line 656
    iput-byte v6, p0, Lcom/google/android/maps/driveabout/vector/G;->k:B

    .line 657
    return-void

    .line 609
    :catchall_93
    move-exception v0

    :try_start_94
    monitor-exit v1
    :try_end_95
    .catchall {:try_start_94 .. :try_end_95} :catchall_93

    throw v0

    .line 618
    :cond_96
    const/4 v0, 0x0

    move v3, v0

    goto/16 :goto_16

    .line 619
    :cond_9a
    const/4 v0, 0x0

    goto/16 :goto_19

    .line 621
    :cond_9d
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1f

    .line 624
    :cond_a0
    const/4 v1, 0x0

    goto :goto_28
.end method

.method private b(LC/a;)Z
    .registers 5
    .parameter

    .prologue
    .line 464
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    const/high16 v2, 0x4000

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/G;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    return-object v0
.end method

.method private c(LC/a;)Z
    .registers 5
    .parameter

    .prologue
    const/high16 v2, 0x4000

    .line 469
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    .line 471
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    mul-float/2addr v1, v2

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_14

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    .line 474
    :goto_15
    return v0

    .line 471
    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private d(LC/a;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v6, 0x3fa0

    .line 481
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 482
    :try_start_7
    iget-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/G;->m:Z

    if-eqz v3, :cond_10

    .line 483
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/G;->m:Z

    .line 484
    monitor-exit v2

    .line 489
    :goto_f
    return v1

    .line 486
    :cond_10
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_34

    .line 488
    invoke-virtual {p1}, LC/a;->o()F

    move-result v2

    .line 489
    iget-byte v3, p0, Lcom/google/android/maps/driveabout/vector/G;->k:B

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v4

    if-ne v3, v4, :cond_31

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/G;->j:F

    mul-float/2addr v3, v6

    cmpl-float v3, v2, v3

    if-gtz v3, :cond_31

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/G;->j:F

    div-float/2addr v3, v6

    cmpg-float v2, v2, v3

    if-gez v2, :cond_32

    :cond_31
    move v0, v1

    :cond_32
    move v1, v0

    goto :goto_f

    .line 486
    :catchall_34
    move-exception v0

    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    throw v0
.end method

.method private static e(LC/a;)Lo/ad;
    .registers 6
    .parameter

    .prologue
    const/high16 v4, 0x2000

    .line 533
    invoke-virtual {p0}, LC/a;->B()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->b()Lo/ad;

    move-result-object v2

    .line 534
    invoke-virtual {v2}, Lo/ad;->g()I

    move-result v0

    .line 535
    invoke-virtual {v2}, Lo/ad;->h()I

    move-result v1

    .line 536
    const v3, 0x71c71c7

    .line 539
    if-gt v0, v3, :cond_19

    if-le v1, v3, :cond_42

    .line 542
    :cond_19
    new-instance v1, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->f()I

    move-result v0

    sub-int/2addr v0, v4

    const/high16 v3, -0x2000

    invoke-direct {v1, v0, v3}, Lo/T;-><init>(II)V

    .line 543
    new-instance v0, Lo/T;

    invoke-virtual {v2}, Lo/ad;->f()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v2

    add-int/2addr v2, v4

    add-int/lit8 v2, v2, -0x1

    const v3, 0x1fffffff

    invoke-direct {v0, v2, v3}, Lo/T;-><init>(II)V

    .line 550
    :goto_3c
    new-instance v2, Lo/ad;

    invoke-direct {v2, v1, v0}, Lo/ad;-><init>(Lo/T;Lo/T;)V

    return-object v2

    .line 546
    :cond_42
    new-instance v3, Lo/T;

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v1, v1, 0x4

    invoke-direct {v3, v0, v1}, Lo/T;-><init>(II)V

    .line 547
    invoke-virtual {v2}, Lo/ad;->d()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->f(Lo/T;)Lo/T;

    move-result-object v1

    .line 548
    invoke-virtual {v2}, Lo/ad;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0, v3}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    goto :goto_3c
.end method

.method private e()Z
    .registers 3

    .prologue
    .line 500
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 501
    :try_start_3
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/G;->q:I

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_8
    monitor-exit v1

    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_8

    .line 502
    :catchall_c
    move-exception v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_c

    throw v0
.end method

.method private f(LC/a;)V
    .registers 6
    .parameter

    .prologue
    .line 560
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/G;->e(LC/a;)Lo/ad;

    move-result-object v0

    .line 562
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 563
    :try_start_7
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 564
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->d:Lo/ad;

    invoke-static {v2, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(Lo/ad;Lo/ad;)B

    move-result v2

    if-eqz v2, :cond_1b

    .line 565
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/G;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 567
    :cond_1b
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    .line 568
    monitor-exit v1

    .line 569
    return-void

    .line 568
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_7 .. :try_end_21} :catchall_1f

    throw v0
.end method

.method private g(LC/a;)V
    .registers 8
    .parameter

    .prologue
    .line 574
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 575
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 576
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_2f

    .line 578
    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/G;->b(I)I

    move-result v3

    .line 579
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 580
    const/4 v0, 0x0

    move v1, v0

    :goto_1b
    if-ge v1, v4, :cond_32

    .line 581
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/X;

    int-to-float v5, v3

    invoke-virtual {v0, v5}, Lo/X;->b(F)Lo/X;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 580
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1b

    .line 576
    :catchall_2f
    move-exception v0

    :try_start_30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_30 .. :try_end_31} :catchall_2f

    throw v0

    .line 583
    :cond_32
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 584
    :try_start_35
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 585
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 586
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_35 .. :try_end_40} :catchall_47

    .line 587
    invoke-virtual {p1}, LC/a;->o()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/G;->i:F

    .line 588
    return-void

    .line 586
    :catchall_47
    move-exception v0

    :try_start_48
    monitor-exit v1
    :try_end_49
    .catchall {:try_start_48 .. :try_end_49} :catchall_47

    throw v0
.end method

.method private h()V
    .registers 4

    .prologue
    .line 592
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/maps/driveabout/vector/I;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/maps/driveabout/vector/I;-><init>(Lcom/google/android/maps/driveabout/vector/G;Lcom/google/android/maps/driveabout/vector/H;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 593
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 594
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/G;->v:Z

    if-eqz v1, :cond_15

    .line 596
    :try_start_12
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_15
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_15} :catch_16

    .line 601
    :cond_15
    :goto_15
    return-void

    .line 597
    :catch_16
    move-exception v0

    goto :goto_15
.end method


# virtual methods
.method public a(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    if-eqz v0, :cond_e

    .line 372
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    invoke-virtual {v0, p1}, LE/o;->c(LD/a;)V

    .line 373
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->o:LE/d;

    invoke-virtual {v0, p1}, LE/d;->c(LD/a;)V

    .line 376
    :cond_e
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 384
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-nez v0, :cond_6a

    .line 386
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/G;->e()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 389
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 390
    :try_start_f
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    .line 391
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    .line 392
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_f .. :try_end_15} :catchall_63

    .line 393
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->h:Lo/ad;

    if-nez v1, :cond_1c

    .line 394
    invoke-direct {p0, p2, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(LC/a;Z)V

    .line 397
    :cond_1c
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/G;->d(LC/a;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 398
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/G;->b(LD/a;LC/a;)V

    .line 400
    :cond_25
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 401
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 402
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    if-eqz v1, :cond_3b

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->n:LE/o;

    invoke-virtual {v1}, LE/o;->a()I

    move-result v1

    if-lez v1, :cond_3b

    .line 403
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/G;->a(LD/a;LC/a;)V

    .line 405
    :cond_3b
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 413
    :cond_3e
    invoke-virtual {p1}, LD/a;->B()V

    .line 414
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    monitor-enter v1

    .line 415
    :try_start_44
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_66

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/z;

    .line 416
    invoke-virtual {p1}, LD/a;->A()V

    .line 417
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/z;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 418
    invoke-virtual {p1}, LD/a;->B()V

    goto :goto_4a

    .line 420
    :catchall_60
    move-exception v0

    monitor-exit v1
    :try_end_62
    .catchall {:try_start_44 .. :try_end_62} :catchall_60

    throw v0

    .line 392
    :catchall_63
    move-exception v0

    :try_start_64
    monitor-exit v1
    :try_end_65
    .catchall {:try_start_64 .. :try_end_65} :catchall_63

    throw v0

    .line 420
    :cond_66
    :try_start_66
    monitor-exit v1
    :try_end_67
    .catchall {:try_start_66 .. :try_end_67} :catchall_60

    .line 421
    invoke-virtual {p1}, LD/a;->A()V

    .line 423
    :cond_6a
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/G;->e()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 302
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->r:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_9
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    .line 304
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/G;->l:Z

    .line 305
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_9 .. :try_end_f} :catchall_2e

    .line 306
    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/G;->a(LC/a;Z)V

    .line 311
    :cond_12
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    monitor-enter v1

    .line 312
    :try_start_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/G;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/z;

    .line 313
    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/z;->b(LC/a;LD/a;)Z

    goto :goto_1b

    .line 315
    :catchall_2b
    move-exception v0

    monitor-exit v1
    :try_end_2d
    .catchall {:try_start_15 .. :try_end_2d} :catchall_2b

    throw v0

    .line 305
    :catchall_2e
    move-exception v0

    :try_start_2f
    monitor-exit v1
    :try_end_30
    .catchall {:try_start_2f .. :try_end_30} :catchall_2e

    throw v0

    .line 315
    :cond_31
    :try_start_31
    monitor-exit v1
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_2b

    .line 316
    const/4 v0, 0x1

    return v0
.end method

.method public c(LD/a;)V
    .registers 2
    .parameter

    .prologue
    .line 356
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/G;->a(LD/a;)V

    .line 357
    return-void
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 295
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->a:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
