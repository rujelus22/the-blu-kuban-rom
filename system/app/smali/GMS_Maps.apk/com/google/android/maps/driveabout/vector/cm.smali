.class public Lcom/google/android/maps/driveabout/vector/cm;
.super Lcom/google/android/maps/driveabout/vector/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;
.implements Lcom/google/android/maps/driveabout/vector/d;


# static fields
.field private static F:Lcom/google/android/maps/driveabout/vector/p;

.field private static final e:F


# instance fields
.field private A:I

.field private final B:I

.field private C:F

.field private D:Z

.field private E:Lcom/google/android/maps/driveabout/vector/cn;

.field public d:Ljava/util/List;

.field private f:F

.field private g:F

.field private h:F

.field private final i:Landroid/content/res/Resources;

.field private final j:Z

.field private k:I

.field private final l:Ljava/util/Map;

.field private m:Lv/l;

.field private n:Lcom/google/android/maps/driveabout/vector/a;

.field private final o:Lo/P;

.field private final p:Lo/P;

.field private final q:Lo/P;

.field private r:Z

.field private s:F

.field private t:F

.field private u:I

.field private v:Z

.field private volatile w:Lo/B;

.field private volatile x:Z

.field private y:F

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x4

    .line 65
    const/high16 v0, 0x4000

    sput v0, Lcom/google/android/maps/driveabout/vector/cm;->e:F

    .line 204
    new-instance v0, Lcom/google/android/maps/driveabout/vector/p;

    invoke-direct {v0, v2}, Lcom/google/android/maps/driveabout/vector/p;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cm;->F:Lcom/google/android/maps/driveabout/vector/p;

    .line 207
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cm;->F:Lcom/google/android/maps/driveabout/vector/p;

    const v1, 0x73217bce

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/p;->b(II)V

    .line 208
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/S;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x3f00

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 212
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/e;-><init>(Lcom/google/android/maps/driveabout/vector/S;)V

    .line 105
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->l:Ljava/util/Map;

    .line 140
    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->r:Z

    .line 151
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/cm;->x:Z

    .line 213
    new-instance v0, Lo/P;

    invoke-direct {v0}, Lo/P;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    .line 214
    new-instance v0, Lo/P;

    invoke-direct {v0}, Lo/P;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    .line 215
    new-instance v0, Lo/P;

    invoke-direct {v0}, Lo/P;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    .line 216
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cm;->i:Landroid/content/res/Resources;

    .line 217
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/vector/cm;->j:Z

    .line 221
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/co;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/co;->a()Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/cp;->a(Z)Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/cp;->a()Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    const v2, 0x7f02016c

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/cp;->a(I)Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/cp;->c()Lcom/google/android/maps/driveabout/vector/co;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/co;->a()Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/maps/driveabout/vector/cp;->a(Z)Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/cp;->b()Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    const v2, 0x7f02016a

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/cp;->a(I)Lcom/google/android/maps/driveabout/vector/cp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/cp;->c()Lcom/google/android/maps/driveabout/vector/co;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cm;->a([Lcom/google/android/maps/driveabout/vector/co;)V

    .line 235
    const/high16 v0, 0x4280

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    .line 236
    iput v5, p0, Lcom/google/android/maps/driveabout/vector/cm;->y:F

    .line 237
    iput v5, p0, Lcom/google/android/maps/driveabout/vector/cm;->z:F

    .line 238
    const/high16 v0, 0x3f80

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cm;->a(F)V

    .line 239
    const/16 v0, 0x4000

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cm;->d(I)V

    .line 242
    const v0, 0x64190

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->A:I

    .line 245
    const/high16 v0, 0x4180

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->f:F

    .line 246
    const/high16 v0, 0x4140

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->g:F

    .line 247
    const/high16 v0, 0x3f40

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->h:F

    .line 249
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->i:Landroid/content/res/Resources;

    const v1, 0x7f0b004c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->B:I

    .line 253
    new-instance v0, Lv/m;

    invoke-direct {v0}, Lv/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    .line 255
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/maps/driveabout/vector/aV;I)Lcom/google/android/maps/driveabout/vector/cX;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 834
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->l:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cX;

    .line 835
    if-nez v0, :cond_26

    .line 836
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cX;

    invoke-direct {v0, p1}, Lcom/google/android/maps/driveabout/vector/cX;-><init>(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 837
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cX;->c(Z)V

    .line 838
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->i:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/maps/driveabout/vector/cX;->a(Landroid/content/res/Resources;I)V

    .line 839
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->l:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_28

    .line 841
    :cond_26
    monitor-exit p0

    return-object v0

    .line 834
    :catchall_28
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ZZZ)Lcom/google/android/maps/driveabout/vector/co;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 884
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/co;

    .line 885
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/co;->a(ZZZ)Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_1d

    move-result v2

    if-eqz v2, :cond_7

    .line 889
    :goto_19
    monitor-exit p0

    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_19

    .line 884
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/high16 v10, 0x3f80

    const/4 v9, 0x0

    .line 727
    monitor-enter p0

    :try_start_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v0}, Lo/P;->c()I

    move-result v0

    if-lez v0, :cond_51

    .line 728
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    if-nez v0, :cond_27

    .line 729
    new-instance v0, Lcom/google/android/maps/driveabout/vector/a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "MyLocation"

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/a;-><init>(Lo/Q;IIILo/p;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    .line 730
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    sget v1, Lcom/google/android/maps/driveabout/vector/cm;->e:F

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(F)V

    .line 732
    :cond_27
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v1}, Lo/P;->d()Lo/Q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v2}, Lo/P;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/Q;I)V

    .line 734
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->w()Lcom/google/android/maps/driveabout/vector/co;

    move-result-object v0

    .line 735
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    iget v2, v0, Lcom/google/android/maps/driveabout/vector/co;->e:I

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->a_(I)V

    .line 736
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    iget v0, v0, Lcom/google/android/maps/driveabout/vector/co;->f:I

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/a;->b(I)V

    .line 737
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->n:Lcom/google/android/maps/driveabout/vector/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/a;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;)V

    .line 744
    :cond_51
    const/4 v0, 0x1

    .line 745
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v1}, Lo/P;->a()Lo/Q;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, Lcom/google/android/maps/driveabout/vector/k;->a(Lo/Q;Z)F

    move-result v1

    .line 748
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->u()Z

    move-result v0

    if-eqz v0, :cond_169

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->s:F

    .line 749
    :goto_64
    invoke-virtual {p2, v0, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->C:F

    mul-float/2addr v0, v1

    .line 751
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->x()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 754
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v2}, Lo/P;->a()Lo/Q;

    move-result-object v2

    invoke-static {p1, p2, v2, v0}, Lcom/google/android/maps/driveabout/vector/dB;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lo/Q;F)V

    .line 757
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->o()V

    .line 758
    iget-object v0, p1, Lcom/google/android/maps/driveabout/vector/aV;->h:Lcom/google/android/maps/driveabout/vector/eb;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/eb;->d(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 759
    iget-object v0, p1, Lcom/google/android/maps/driveabout/vector/aV;->d:Lcom/google/android/maps/driveabout/vector/cO;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/cO;->d(Lcom/google/android/maps/driveabout/vector/aV;)V

    .line 761
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->w()Lcom/google/android/maps/driveabout/vector/co;

    move-result-object v2

    .line 763
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->D:Z

    if-nez v0, :cond_16d

    iget v0, v2, Lcom/google/android/maps/driveabout/vector/co;->c:I

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/co;->b:I

    if-eq v0, v3, :cond_16d

    move v0, v7

    .line 766
    :goto_94
    iget-boolean v3, v2, Lcom/google/android/maps/driveabout/vector/co;->a:Z

    if-nez v3, :cond_a1

    .line 767
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->u:I

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/cm;->u:I

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/cm;->u:I

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    .line 770
    :cond_a1
    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 773
    iget v3, v2, Lcom/google/android/maps/driveabout/vector/co;->d:I

    if-eqz v3, :cond_10e

    .line 774
    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 775
    iget v3, v2, Lcom/google/android/maps/driveabout/vector/co;->d:I

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/driveabout/vector/cm;->a(Lcom/google/android/maps/driveabout/vector/aV;I)Lcom/google/android/maps/driveabout/vector/cX;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/maps/driveabout/vector/cX;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 776
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v3}, Lo/P;->h()F

    move-result v3

    const/high16 v4, 0x4120

    mul-float/2addr v3, v4

    add-float/2addr v3, v10

    .line 777
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v4}, Lo/P;->h()F

    move-result v4

    const/high16 v5, 0x4040

    mul-float/2addr v4, v5

    sub-float v4, v10, v4

    .line 778
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v5}, Lo/P;->h()F

    move-result v5

    const/high16 v6, 0x4080

    mul-float/2addr v5, v6

    .line 779
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v6}, Lo/P;->h()F

    move-result v6

    const/high16 v7, -0x3f80

    mul-float/2addr v6, v7

    .line 780
    invoke-interface {v1, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 781
    const/4 v4, 0x0

    invoke-interface {v1, v5, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 782
    invoke-interface {v1, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 783
    const/4 v4, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-interface {v1, v4, v7, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 784
    div-float v4, v10, v3

    div-float v7, v10, v3

    div-float v3, v10, v3

    invoke-interface {v1, v4, v7, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 785
    neg-float v3, v5

    neg-float v4, v6

    const/4 v5, 0x0

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 786
    const/high16 v3, 0x1

    const/high16 v4, 0x1

    const/high16 v5, 0x1

    const/high16 v6, 0x1

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 790
    :cond_10e
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v3}, Lo/P;->h()F

    move-result v3

    cmpl-float v3, v3, v9

    if-eqz v3, :cond_12f

    .line 791
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v3}, Lo/P;->h()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v4}, Lo/P;->a()Lo/Q;

    move-result-object v4

    invoke-virtual {v4}, Lo/Q;->e()D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    .line 793
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 796
    :cond_12f
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->u()Z

    move-result v3

    if-eqz v3, :cond_170

    .line 797
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v3}, Lo/P;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 803
    :goto_143
    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 804
    if-eqz v0, :cond_18b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v0}, Lo/P;->j()F

    move-result v0

    cmpl-float v0, v0, v10

    if-nez v0, :cond_18b

    .line 805
    iget v0, v2, Lcom/google/android/maps/driveabout/vector/co;->c:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/cm;->a(Lcom/google/android/maps/driveabout/vector/aV;I)Lcom/google/android/maps/driveabout/vector/cX;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cX;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 810
    :goto_161
    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V
    :try_end_167
    .catchall {:try_start_6 .. :try_end_167} :catchall_188

    .line 811
    monitor-exit p0

    return-void

    .line 748
    :cond_169
    :try_start_169
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    goto/16 :goto_64

    :cond_16d
    move v0, v8

    .line 763
    goto/16 :goto_94

    .line 799
    :cond_170
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/k;->j()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 800
    invoke-virtual {p2}, Lcom/google/android/maps/driveabout/vector/k;->k()F

    move-result v3

    const/high16 v4, 0x3f80

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V
    :try_end_187
    .catchall {:try_start_169 .. :try_end_187} :catchall_188

    goto :goto_143

    .line 727
    :catchall_188
    move-exception v0

    monitor-exit p0

    throw v0

    .line 807
    :cond_18b
    :try_start_18b
    iget v0, v2, Lcom/google/android/maps/driveabout/vector/co;->b:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/cm;->a(Lcom/google/android/maps/driveabout/vector/aV;I)Lcom/google/android/maps/driveabout/vector/cX;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cX;->a(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_194
    .catchall {:try_start_18b .. :try_end_194} :catchall_188

    goto :goto_161
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/k;)F
    .registers 6
    .parameter

    .prologue
    const/high16 v0, 0x3f80

    .line 711
    .line 712
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->l()F

    move-result v1

    .line 713
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->f:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_14

    .line 714
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->g:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_15

    .line 715
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->h:F

    .line 722
    :cond_14
    :goto_14
    return v0

    .line 717
    :cond_15
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->h:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->f:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->g:F

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    .line 719
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->h:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->g:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_14
.end method

.method private q()V
    .registers 1

    .prologue
    .line 313
    return-void
.end method

.method private declared-synchronized r()V
    .registers 3

    .prologue
    .line 349
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cX;

    .line 350
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/cX;->g()V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1b

    goto :goto_b

    .line 349
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 352
    :cond_1e
    :try_start_1e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_1b

    .line 353
    monitor-exit p0

    return-void
.end method

.method private s()V
    .registers 6

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->E:Lcom/google/android/maps/driveabout/vector/cn;

    if-eqz v0, :cond_13

    .line 403
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->E:Lcom/google/android/maps/driveabout/vector/cn;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->s:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->u:I

    int-to-float v3, v3

    const/high16 v4, 0x4780

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/cn;->a(FFF)V

    .line 406
    :cond_13
    return-void
.end method

.method private t()F
    .registers 3

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->u()Z

    move-result v0

    if-eqz v0, :cond_21

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->s:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    div-float/2addr v0, v1

    .line 471
    :goto_b
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->C:F

    mul-float/2addr v1, v0

    .line 475
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->u()Z

    move-result v0

    if-eqz v0, :cond_24

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->s:F

    .line 476
    :goto_16
    mul-float/2addr v1, v0

    .line 477
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->u()Z

    move-result v0

    if-eqz v0, :cond_27

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->z:F

    .line 479
    :goto_1f
    mul-float/2addr v0, v1

    return v0

    .line 470
    :cond_21
    const/high16 v0, 0x3e80

    goto :goto_b

    .line 475
    :cond_24
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    goto :goto_16

    .line 477
    :cond_27
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->y:F

    goto :goto_1f
.end method

.method private u()Z
    .registers 2

    .prologue
    .line 568
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->w()Lcom/google/android/maps/driveabout/vector/co;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/co;->a:Z

    return v0
.end method

.method private declared-synchronized v()Lo/P;
    .registers 3

    .prologue
    .line 849
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->r:Z

    if-eqz v0, :cond_1f

    .line 851
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0, v1}, Lo/P;->a(Lo/P;)V

    .line 853
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-interface {v0, v1}, Lv/l;->a(Lo/P;)Z

    .line 854
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 855
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_22

    .line 858
    :goto_1d
    monitor-exit p0

    return-object v0

    :cond_1f
    :try_start_1f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;
    :try_end_21
    .catchall {:try_start_1f .. :try_end_21} :catchall_22

    goto :goto_1d

    .line 849
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private w()Lcom/google/android/maps/driveabout/vector/co;
    .registers 4

    .prologue
    .line 868
    monitor-enter p0

    .line 869
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->v()Lo/P;

    move-result-object v0

    .line 870
    invoke-virtual {v0}, Lo/P;->e()Z

    move-result v1

    .line 871
    invoke-virtual {v0}, Lo/P;->g()Z

    move-result v2

    .line 872
    invoke-virtual {v0}, Lo/P;->i()Z

    move-result v0

    .line 873
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_17

    .line 874
    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/cm;->a(ZZZ)Lcom/google/android/maps/driveabout/vector/co;

    move-result-object v0

    return-object v0

    .line 873
    :catchall_17
    move-exception v0

    :try_start_18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    throw v0
.end method


# virtual methods
.method public a(FFLo/Q;Lcom/google/android/maps/driveabout/vector/k;)I
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 542
    monitor-enter p0

    .line 543
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-nez v0, :cond_10

    .line 544
    const v0, 0x7fffffff

    monitor-exit p0

    .line 549
    :goto_f
    return v0

    .line 547
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->a()Lo/Q;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/google/android/maps/driveabout/vector/k;->b(Lo/Q;)[I

    move-result-object v0

    .line 548
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_34

    .line 549
    aget v1, v0, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    mul-float/2addr v1, v2

    aget v2, v0, v3

    int-to-float v2, v2

    sub-float v2, p2, v2

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_f

    .line 548
    :catchall_34
    move-exception v0

    :try_start_35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/k;)I
    .registers 3
    .parameter

    .prologue
    .line 820
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->k:I

    return v0
.end method

.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 375
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->s:F

    .line 376
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->s()V

    .line 377
    return-void
.end method

.method public a(FFF)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 343
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/cm;->f:F

    .line 344
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/cm;->g:F

    .line 345
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/cm;->h:F

    .line 346
    return-void
.end method

.method public a(FII)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x42c8

    .line 366
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->s:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    div-float/2addr v0, v1

    .line 367
    const/high16 v1, 0x3f00

    mul-float/2addr v1, p1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->t:F

    .line 368
    int-to-float v1, p2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->y:F

    .line 369
    int-to-float v1, p3

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->z:F

    .line 370
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cm;->a(F)V

    .line 371
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->s()V

    .line 372
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 662
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/E;->b()I

    move-result v0

    if-gtz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-nez v0, :cond_f

    .line 707
    :cond_e
    :goto_e
    return-void

    .line 668
    :cond_f
    monitor-enter p0

    .line 669
    :try_start_10
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->r:Z

    if-eqz v0, :cond_54

    .line 670
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->d()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lv/l;->a(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->k:I

    .line 674
    :goto_20
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->k:I

    if-eqz v0, :cond_58

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-interface {v0, v1}, Lv/l;->a(Lo/P;)Z

    move-result v0

    if-eqz v0, :cond_58

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 677
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0, v1}, Lo/P;->a(Lo/P;)V

    .line 678
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->d()J

    move-result-wide v0

    const-wide/16 v2, 0xc8

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/aV;->a(J)V

    .line 683
    :goto_47
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-nez v0, :cond_60

    .line 684
    monitor-exit p0

    goto :goto_e

    .line 686
    :catchall_51
    move-exception v0

    monitor-exit p0
    :try_end_53
    .catchall {:try_start_10 .. :try_end_53} :catchall_51

    throw v0

    .line 672
    :cond_54
    const/4 v0, 0x0

    :try_start_55
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->k:I

    goto :goto_20

    .line 680
    :cond_58
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0, v1}, Lo/P;->a(Lo/P;)V

    goto :goto_47

    .line 686
    :cond_60
    monitor-exit p0
    :try_end_61
    .catchall {:try_start_55 .. :try_end_61} :catchall_51

    .line 688
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aV;->x()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 690
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 692
    const/4 v0, 0x0

    .line 693
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->w:Lo/B;

    if-eqz v2, :cond_86

    .line 694
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/E;->c()Lcom/google/android/maps/driveabout/vector/cw;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->w:Lo/B;

    invoke-virtual {v2}, Lo/B;->a()Lo/p;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/cw;->a(Lo/m;)Lcom/google/android/maps/driveabout/vector/cy;

    move-result-object v0

    .line 695
    if-eqz v0, :cond_86

    .line 696
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->q:Lo/P;

    invoke-virtual {v2}, Lo/P;->a()Lo/Q;

    move-result-object v2

    invoke-interface {v0, p1, p2, p3, v2}, Lcom/google/android/maps/driveabout/vector/cy;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;Lo/Q;)V

    .line 700
    :cond_86
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/cm;->b(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/E;)V

    .line 702
    if-eqz v0, :cond_8e

    .line 703
    invoke-interface {v0, p1, p3}, Lcom/google/android/maps/driveabout/vector/cy;->a(Lcom/google/android/maps/driveabout/vector/aV;Lcom/google/android/maps/driveabout/vector/E;)V

    .line 706
    :cond_8e
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_e
.end method

.method public a(Ljava/util/List;FFLo/Q;Lcom/google/android/maps/driveabout/vector/k;I)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/cm;->k()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 556
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/cm;->a(FFLo/Q;Lcom/google/android/maps/driveabout/vector/k;)I

    move-result v0

    .line 557
    if-ge v0, p6, :cond_14

    .line 558
    new-instance v1, Lcom/google/android/maps/driveabout/vector/H;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/maps/driveabout/vector/H;-><init>(Lcom/google/android/maps/driveabout/vector/d;Lcom/google/android/maps/driveabout/vector/e;I)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    :cond_14
    return-void
.end method

.method public declared-synchronized a(Lo/P;)V
    .registers 4
    .parameter

    .prologue
    .line 283
    monitor-enter p0

    if-eqz p1, :cond_2b

    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    invoke-virtual {p1}, Lo/P;->l()Z

    move-result v1

    if-ne v0, v1, :cond_2b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->f()Lo/B;

    move-result-object v0

    invoke-virtual {p1}, Lo/P;->f()Lo/B;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/K;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->g()Z

    move-result v0

    invoke-virtual {p1}, Lo/P;->g()Z

    move-result v1

    if-eq v0, v1, :cond_2e

    .line 288
    :cond_2b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->x:Z

    .line 290
    :cond_2e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0, p1}, Lo/P;->a(Lo/P;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-eqz v0, :cond_47

    .line 292
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-interface {v0, v1}, Lv/l;->b(Lo/P;)V

    .line 300
    :goto_42
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->q()V
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_4f

    .line 301
    monitor-exit p0

    return-void

    .line 297
    :cond_47
    :try_start_47
    new-instance v0, Lv/m;

    invoke-direct {v0}, Lv/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;
    :try_end_4e
    .catchall {:try_start_47 .. :try_end_4e} :catchall_4f

    goto :goto_42

    .line 283
    :catchall_4f
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public varargs declared-synchronized a([Lcom/google/android/maps/driveabout/vector/co;)V
    .registers 3
    .parameter

    .prologue
    .line 332
    monitor-enter p0

    :try_start_1
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->a([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->d:Ljava/util/List;

    .line 333
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->r()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 339
    monitor-exit p0

    return-void

    .line 332
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/aV;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/cm;->c(Lcom/google/android/maps/driveabout/vector/k;)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->C:F

    .line 265
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    invoke-interface {v0, p1}, Lv/l;->a(Lcom/google/android/maps/driveabout/vector/k;)V

    .line 271
    const/4 v0, 0x1

    return v0
.end method

.method public a_(Lcom/google/android/maps/driveabout/vector/aV;)V
    .registers 3
    .parameter

    .prologue
    .line 578
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->r()V

    .line 579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->x:Z

    .line 586
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 275
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/cm;->A:I

    .line 276
    return-void
.end method

.method public declared-synchronized b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 573
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/cm;->r:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 574
    monitor-exit p0

    return-void

    .line 573
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/k;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 444
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/cm;->e()Lo/Q;

    move-result-object v2

    .line 445
    if-nez v2, :cond_9

    .line 458
    :goto_8
    return v1

    .line 450
    :cond_9
    invoke-virtual {p1, v2}, Lcom/google/android/maps/driveabout/vector/k;->b(Lo/Q;)[I

    move-result-object v2

    .line 451
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->t()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    .line 452
    aget v4, v2, v1

    sub-int/2addr v4, v3

    .line 453
    aget v5, v2, v1

    add-int/2addr v5, v3

    .line 454
    aget v6, v2, v0

    sub-int/2addr v6, v3

    .line 455
    aget v2, v2, v0

    add-int/2addr v2, v3

    .line 458
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->e()I

    move-result v3

    if-ge v4, v3, :cond_35

    if-ltz v5, :cond_35

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->f()I

    move-result v3

    if-ge v6, v3, :cond_35

    if-ltz v2, :cond_35

    :goto_33
    move v1, v0

    goto :goto_8

    :cond_35
    move v0, v1

    goto :goto_33
.end method

.method public b(Ljava/util/List;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 638
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->x:Z

    if-eqz v0, :cond_3e

    .line 639
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cm;->x:Z

    .line 641
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/cm;->f()Lo/B;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->w:Lo/B;

    .line 642
    const/4 v0, 0x0

    .line 643
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->w:Lo/B;

    if-eqz v3, :cond_21

    .line 644
    invoke-static {}, Lm/q;->a()Lm/q;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->w:Lo/B;

    invoke-virtual {v3}, Lo/B;->a()Lo/p;

    move-result-object v3

    invoke-virtual {v0, v3}, Lm/q;->e(Lo/p;)Lm/k;

    move-result-object v0

    .line 648
    :cond_21
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 649
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/cm;->w:Lo/B;

    if-eqz v3, :cond_2a

    if-nez v0, :cond_2f

    .line 650
    :cond_2a
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/e;->b(Ljava/util/List;)Z

    move-result v0

    .line 656
    :goto_2e
    return v0

    .line 652
    :cond_2f
    sget-object v3, Lcom/google/android/maps/driveabout/vector/cx;->i:Lcom/google/android/maps/driveabout/vector/cx;

    new-array v4, v1, [Lcom/google/android/maps/driveabout/vector/cy;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/google/android/maps/driveabout/vector/cm;->a(Lcom/google/android/maps/driveabout/vector/cx;[Lcom/google/android/maps/driveabout/vector/cy;)Lcom/google/android/maps/driveabout/vector/cw;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 653
    goto :goto_2e

    :cond_3e
    move v0, v2

    .line 656
    goto :goto_2e
.end method

.method public c()Lcom/google/android/maps/driveabout/vector/l;
    .registers 2

    .prologue
    .line 827
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)V
    .registers 3
    .parameter

    .prologue
    const v0, 0x3f2b851f

    .line 382
    packed-switch p1, :pswitch_data_c

    .line 390
    const/high16 v0, 0x3f80

    .line 392
    :pswitch_8
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/cm;->a(F)V

    .line 393
    return-void

    .line 382
    :pswitch_data_c
    .packed-switch 0x2
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public d()V
    .registers 2

    .prologue
    .line 417
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->v:Z

    .line 418
    return-void
.end method

.method public d(I)V
    .registers 2
    .parameter

    .prologue
    .line 396
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/cm;->u:I

    .line 397
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->s()V

    .line 398
    return-void
.end method

.method public declared-synchronized e()Lo/Q;
    .registers 3

    .prologue
    .line 427
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->r:Z

    if-eqz v0, :cond_1c

    .line 428
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-interface {v0, v1}, Lv/l;->a(Lo/P;)Z

    .line 429
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 430
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0}, Lo/P;->a()Lo/Q;
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_23

    move-result-object v0

    .line 433
    :goto_1a
    monitor-exit p0

    return-object v0

    :cond_1c
    :try_start_1c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->a()Lo/Q;
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_23

    move-result-object v0

    goto :goto_1a

    .line 427
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Lo/B;
    .registers 2

    .prologue
    .line 438
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->v()Lo/P;

    move-result-object v0

    invoke-virtual {v0}, Lo/P;->f()Lo/B;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_b

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()I
    .registers 2

    .prologue
    .line 494
    const/4 v0, 0x0

    return v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 504
    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 509
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 514
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->j:Z

    return v0
.end method

.method public declared-synchronized o()F
    .registers 3

    .prologue
    .line 316
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->r:Z

    if-eqz v0, :cond_1c

    .line 317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->m:Lv/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-interface {v0, v1}, Lv/l;->a(Lo/P;)Z

    .line 318
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0}, Lo/P;->l()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 319
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->p:Lo/P;

    invoke-virtual {v0}, Lo/P;->b()F
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_23

    move-result v0

    .line 322
    :goto_1a
    monitor-exit p0

    return v0

    :cond_1c
    :try_start_1c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->o:Lo/P;

    invoke-virtual {v0}, Lo/P;->b()F
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_23

    move-result v0

    goto :goto_1a

    .line 316
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized p()Lo/P;
    .registers 3

    .prologue
    .line 845
    monitor-enter p0

    :try_start_1
    new-instance v0, Lo/P;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/cm;->v()Lo/P;

    move-result-object v1

    invoke-direct {v0, v1}, Lo/P;-><init>(Lo/P;)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    monitor-exit p0

    return-object v0

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public r_()V
    .registers 2

    .prologue
    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->v:Z

    .line 413
    return-void
.end method

.method public s_()I
    .registers 3

    .prologue
    .line 489
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->B:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/cm;->C:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public u_()I
    .registers 2

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cm;->A:I

    return v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .registers 1

    .prologue
    .line 815
    return-object p0
.end method
