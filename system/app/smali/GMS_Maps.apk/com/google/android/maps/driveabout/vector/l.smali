.class Lcom/google/android/maps/driveabout/vector/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/b;


# instance fields
.field private final a:Lz/K;

.field private final b:Lz/i;

.field private final c:Lz/h;

.field private d:Z

.field private e:Z

.field private final f:I

.field private g:Lz/c;

.field private final h:Lh/h;

.field private i:F

.field private volatile j:Z


# virtual methods
.method public a()V
    .registers 1

    .prologue
    .line 312
    return-void
.end method

.method public a(Lz/c;)V
    .registers 3
    .parameter

    .prologue
    .line 303
    monitor-enter p0

    .line 304
    :try_start_1
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/l;->g:Lz/c;

    .line 305
    sget-object v0, Lz/k;->a:Lz/P;

    invoke-interface {p1, p0, v0}, Lz/c;->a(Lz/b;Lz/O;)V

    .line 306
    monitor-exit p0

    .line 307
    return-void

    .line 306
    :catchall_a
    move-exception v0

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_a

    throw v0
.end method

.method public declared-synchronized b(Lz/c;)V
    .registers 7
    .parameter

    .prologue
    .line 316
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->d:Z

    if-eqz v0, :cond_f

    .line 317
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->b:Lz/i;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/l;->a:Lz/K;

    invoke-virtual {v0, v1}, Lz/i;->a(Lz/K;)V

    .line 318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->d:Z

    .line 321
    :cond_f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    if-eqz v0, :cond_2b

    .line 322
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    invoke-virtual {v0}, Lh/h;->hasEnded()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->j:Z

    .line 323
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->j:Z

    if-nez v0, :cond_2b

    .line 324
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lh/h;->a(J)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    .line 328
    :cond_2b
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/l;->e:Z

    if-eqz v0, :cond_47

    .line 329
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->c:Lz/h;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/l;->f:I

    invoke-interface {v0, v1}, Lz/h;->a(I)V

    .line 334
    :goto_36
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->h:Lh/h;

    invoke-virtual {v0}, Lh/h;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_45

    .line 335
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->g:Lz/c;

    sget-object v1, Lz/k;->a:Lz/P;

    invoke-interface {v0, p0, v1}, Lz/c;->a(Lz/b;Lz/O;)V
    :try_end_45
    .catchall {:try_start_1 .. :try_end_45} :catchall_55

    .line 337
    :cond_45
    monitor-exit p0

    return-void

    .line 331
    :cond_47
    :try_start_47
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/l;->c:Lz/h;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/l;->i:F

    invoke-interface {v0, v1, v2, v3, v4}, Lz/h;->a(FFFF)V
    :try_end_54
    .catchall {:try_start_47 .. :try_end_54} :catchall_55

    goto :goto_36

    .line 316
    :catchall_55
    move-exception v0

    monitor-exit p0

    throw v0
.end method
