.class public Lcom/google/android/maps/driveabout/app/bl;
.super Lcom/google/android/maps/driveabout/app/cU;
.source "SourceFile"


# instance fields
.field protected d:F


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct/range {p0 .. p5}, Lcom/google/android/maps/driveabout/app/cU;-><init>(Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lcom/google/android/maps/driveabout/app/cV;Lq/e;)V

    .line 21
    const/high16 v0, 0x4168

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    .line 26
    return-void
.end method


# virtual methods
.method protected a()F
    .registers 2

    .prologue
    .line 30
    const/high16 v0, 0x4170

    return v0
.end method

.method protected a(LaH/h;)F
    .registers 5
    .parameter

    .prologue
    const/high16 v0, 0x4138

    .line 39
    invoke-virtual {p1}, LaH/h;->f()Z

    move-result v1

    if-nez v1, :cond_b

    .line 40
    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    .line 58
    :goto_a
    return v0

    .line 42
    :cond_b
    invoke-virtual {p1}, LaH/h;->l()Lo/af;

    move-result-object v1

    .line 43
    invoke-virtual {p1}, LaH/h;->i()Z

    move-result v2

    if-eqz v2, :cond_17

    if-nez v1, :cond_1a

    .line 44
    :cond_17
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_a

    .line 46
    :cond_1a
    invoke-virtual {v1}, Lo/af;->j()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 50
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_a

    .line 52
    :cond_23
    invoke-virtual {v1}, Lo/af;->f()I

    move-result v1

    .line 53
    const/16 v2, 0x80

    if-lt v1, v2, :cond_2e

    .line 54
    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_a

    .line 55
    :cond_2e
    const/16 v0, 0x50

    if-lt v1, v0, :cond_37

    .line 56
    const/high16 v0, 0x4148

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_a

    .line 58
    :cond_37
    const/high16 v0, 0x4168

    iput v0, p0, Lcom/google/android/maps/driveabout/app/bl;->d:F

    goto :goto_a
.end method
