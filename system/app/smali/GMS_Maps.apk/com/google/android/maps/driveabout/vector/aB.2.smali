.class Lcom/google/android/maps/driveabout/vector/aB;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lz/b;


# static fields
.field static final a:Lz/L;


# instance fields
.field final b:Lz/r;

.field final c:Ljava/util/List;

.field final d:Ljava/lang/ref/WeakReference;

.field final e:Landroid/content/res/Resources;

.field f:Lz/c;

.field g:LC/a;

.field final h:Lh/l;

.field i:Z

.field final j:Lo/S;

.field k:Lcom/google/android/maps/driveabout/vector/aC;

.field l:I

.field final m:Ljava/util/Map;

.field n:F

.field o:F

.field p:F

.field q:F

.field r:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 1111
    new-instance v0, Lz/L;

    const/high16 v1, 0x3f80

    invoke-direct {v0, v2, v2, v1}, Lz/L;-><init>(FFF)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aB;->a:Lz/L;

    return-void
.end method


# virtual methods
.method declared-synchronized a(LD/a;I)LD/b;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1200
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 1201
    if-nez v0, :cond_27

    .line 1202
    const/4 v1, 0x1

    .line 1203
    new-instance v0, LD/b;

    invoke-direct {v0, p1, v1}, LD/b;-><init>(LD/a;Z)V

    .line 1204
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    .line 1205
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->e:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, LD/b;->a(Landroid/content/res/Resources;I)V

    .line 1206
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    .line 1208
    :cond_27
    monitor-exit p0

    return-object v0

    .line 1200
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 1249
    return-void
.end method

.method declared-synchronized a(FFF)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1321
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aB;->n:F

    .line 1322
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/aB;->o:F

    .line 1323
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/aB;->p:F
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 1324
    monitor-exit p0

    return-void

    .line 1321
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lz/c;)V
    .registers 3
    .parameter

    .prologue
    .line 1242
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aB;->f:Lz/c;

    .line 1243
    sget-object v0, Lz/c;->a:Lz/P;

    invoke-interface {p1, p0, v0}, Lz/c;->a(Lz/b;Lz/O;)V

    .line 1244
    return-void
.end method

.method public declared-synchronized b(Lz/c;)V
    .registers 10
    .parameter

    .prologue
    const/4 v3, 0x0

    const/high16 v1, 0x3f80

    const/4 v2, 0x1

    .line 1254
    monitor-enter p0

    :try_start_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/a;
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_92

    .line 1255
    if-nez v0, :cond_11

    .line 1317
    :cond_f
    :goto_f
    monitor-exit p0

    return-void

    .line 1260
    :cond_11
    :try_start_11
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->h:Lh/l;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lh/l;->a(J)I

    move-result v4

    .line 1261
    if-eqz v4, :cond_20

    .line 1265
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    .line 1269
    :cond_20
    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->r:Z

    if-nez v4, :cond_95

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v4, v4, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v5, v5, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    if-eq v4, v5, :cond_95

    .line 1271
    :goto_2e
    if-eqz v2, :cond_97

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v2}, Lo/S;->j()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_97

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v2, v2, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    .line 1274
    :goto_3e
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aB;->l:I

    if-eq v3, v2, :cond_47

    .line 1275
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->l:I

    .line 1276
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    .line 1280
    :cond_47
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    if-eqz v2, :cond_f

    .line 1281
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->i:Z

    .line 1283
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->b:Lz/r;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aB;->l:I

    invoke-virtual {p0, v0, v3}, Lcom/google/android/maps/driveabout/vector/aB;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lz/r;->a(Lz/o;)V

    .line 1285
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->h:Lh/l;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-interface {v0, v2}, Lh/l;->a(Lo/S;)Z

    .line 1286
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;

    move-result-object v3

    .line 1289
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->g:LC/a;

    if-eqz v0, :cond_f

    if-eqz v3, :cond_f

    .line 1290
    const/4 v0, 0x4

    new-array v0, v0, [F

    .line 1291
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v0}, Lo/S;->c()I

    move-result v0

    int-to-double v4, v0

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v2, v4

    .line 1293
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_82
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lz/r;

    .line 1294
    invoke-virtual {v0, v3, v2}, Lz/r;->b(Lo/T;F)V
    :try_end_91
    .catchall {:try_start_11 .. :try_end_91} :catchall_92

    goto :goto_82

    .line 1254
    :catchall_92
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_95
    move v2, v3

    .line 1269
    goto :goto_2e

    .line 1271
    :cond_97
    :try_start_97
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget v2, v2, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    goto :goto_3e

    .line 1300
    :cond_9c
    const/4 v0, 0x1

    .line 1301
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aB;->g:LC/a;

    invoke-virtual {v2, v3, v0}, LC/a;->a(Lo/T;Z)F

    .line 1304
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-eqz v0, :cond_da

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->o:F

    move v2, v0

    .line 1305
    :goto_ab
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/aB;->q:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-eqz v0, :cond_de

    move v0, v1

    :goto_b4
    mul-float/2addr v0, v4

    mul-float/2addr v0, v2

    .line 1306
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->b:Lz/r;

    invoke-virtual {v1, v3, v0}, Lz/r;->a(Lo/T;F)V

    .line 1307
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->k:Lcom/google/android/maps/driveabout/vector/aC;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-eqz v0, :cond_cd

    .line 1308
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->b:Lz/r;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aB;->j:Lo/S;

    invoke-virtual {v1}, Lo/S;->b()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, Lz/r;->a(F)V

    .line 1312
    :cond_cd
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->f:Lz/c;

    if-eqz v0, :cond_f

    .line 1313
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->f:Lz/c;

    sget-object v1, Lz/k;->a:Lz/P;

    invoke-interface {v0, p0, v1}, Lz/c;->a(Lz/b;Lz/O;)V

    goto/16 :goto_f

    .line 1304
    :cond_da
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->n:F

    move v2, v0

    goto :goto_ab

    .line 1305
    :cond_de
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aB;->p:F
    :try_end_e0
    .catchall {:try_start_97 .. :try_end_e0} :catchall_92

    goto :goto_b4
.end method
