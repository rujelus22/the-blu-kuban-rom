.class Lcom/google/android/maps/driveabout/vector/bo;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:I

.field private final f:J

.field private final g:Lw/b;

.field private h:Lw/d;


# direct methods
.method protected constructor <init>(LC/b;Lw/b;FFFI)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1148
    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    .line 1149
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bo;->g:Lw/b;

    .line 1150
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/bo;->b:F

    .line 1151
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/bo;->c:F

    .line 1152
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/bo;->d:F

    .line 1153
    iput p6, p0, Lcom/google/android/maps/driveabout/vector/bo;->e:I

    .line 1154
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->f:J

    .line 1155
    return-void
.end method


# virtual methods
.method public a(LC/a;)LC/c;
    .registers 9
    .parameter

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->a:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bo;->g:Lw/b;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bo;->b:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bo;->c:F

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/bo;->d:F

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;Lw/b;FFF)LC/b;

    move-result-object v2

    .line 1164
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/google/android/maps/driveabout/vector/bo;->f:J

    sub-long/2addr v0, v3

    long-to-int v3, v0

    .line 1168
    const/4 v4, 0x1

    .line 1169
    const/4 v5, 0x0

    .line 1170
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bl;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bo;->a:LC/b;

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/bo;->e:I

    sub-int v3, v6, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bl;-><init>(LC/b;LC/c;IZF)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    .line 1173
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    invoke-interface {v0, p1}, Lw/d;->a(LC/a;)LC/c;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    if-nez v0, :cond_6

    .line 1183
    const/4 v0, 0x0

    .line 1185
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bo;->h:Lw/d;

    invoke-interface {v0}, Lw/d;->c()I

    move-result v0

    goto :goto_5
.end method
