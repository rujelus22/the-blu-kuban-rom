.class public Lcom/google/android/maps/driveabout/app/LaneIconView;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field public static final a:[I


# instance fields
.field b:[Landroid/graphics/Bitmap;

.field c:[F

.field d:F

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Ljava/util/List;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 61
    const/16 v0, 0x8

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/maps/driveabout/app/LaneIconView;->a:[I

    .line 64
    sget-object v0, Lcom/google/android/maps/driveabout/app/LaneIconView;->a:[I

    const/4 v1, 0x0

    const v2, 0x7f020167

    aput v2, v0, v1

    .line 65
    sget-object v0, Lcom/google/android/maps/driveabout/app/LaneIconView;->a:[I

    const/4 v1, 0x1

    const v2, 0x7f020152

    aput v2, v0, v1

    .line 66
    sget-object v0, Lcom/google/android/maps/driveabout/app/LaneIconView;->a:[I

    const/4 v1, 0x2

    const v2, 0x7f02015c

    aput v2, v0, v1

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 126
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 118
    const/16 v0, 0x7f

    iput v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->i:I

    .line 127
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->a(Landroid/content/res/Resources;)V

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 118
    const/16 v0, 0x7f

    iput v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->i:I

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->a(Landroid/content/res/Resources;)V

    .line 133
    return-void
.end method

.method private a(Landroid/content/res/Resources;)V
    .registers 5
    .parameter

    .prologue
    .line 144
    const v0, 0x7f09008b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const v1, 0x7f09008c

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/LaneIconView;->a(II)V

    .line 146
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->g:Landroid/graphics/Paint;

    .line 147
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->g:Landroid/graphics/Paint;

    const/high16 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 148
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->g:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->XOR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 149
    return-void
.end method

.method private static b(II)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 311
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 312
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 313
    if-nez v1, :cond_d

    .line 314
    const v0, 0xffffff

    .line 316
    :cond_d
    if-le v0, p1, :cond_14

    const/high16 v2, 0x4000

    if-eq v1, v2, :cond_14

    .line 321
    :goto_13
    return p1

    .line 318
    :cond_14
    if-ge v0, p1, :cond_1b

    .line 319
    const/high16 v1, 0x100

    or-int p1, v0, v1

    goto :goto_13

    :cond_1b
    move p1, v0

    goto :goto_13
.end method


# virtual methods
.method a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 137
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->e:Landroid/graphics/Paint;

    .line 138
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->f:Landroid/graphics/Paint;

    .line 140
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 141
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 15
    .parameter

    .prologue
    const/high16 v12, 0x3f80

    const/4 v11, 0x0

    .line 385
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->b:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->h:Ljava/util/List;

    if-nez v0, :cond_c

    .line 422
    :cond_b
    :goto_b
    return-void

    .line 395
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 396
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 399
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v5, v0, Landroid/util/DisplayMetrics;->density:F

    .line 401
    iget v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->d:F

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 402
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bs;

    .line 403
    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    .line 404
    iget v1, v0, Lcom/google/android/maps/driveabout/app/bs;->b:F

    mul-float/2addr v1, v5

    invoke-virtual {v4, v1, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 405
    iget-object v1, v0, Lcom/google/android/maps/driveabout/app/bs;->a:[Lcom/google/android/maps/driveabout/app/br;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_58
    if-ltz v2, :cond_9d

    .line 406
    invoke-virtual {v4}, Landroid/graphics/Canvas;->save()I

    .line 407
    iget-object v1, v0, Lcom/google/android/maps/driveabout/app/bs;->a:[Lcom/google/android/maps/driveabout/app/br;

    aget-object v1, v1, v2

    .line 408
    iget v7, v1, Lcom/google/android/maps/driveabout/app/br;->a:I

    .line 409
    iget-boolean v8, v1, Lcom/google/android/maps/driveabout/app/br;->b:Z

    if-eqz v8, :cond_6c

    .line 410
    const/high16 v8, -0x4080

    invoke-virtual {v4, v8, v12}, Landroid/graphics/Canvas;->scale(FF)V

    .line 412
    :cond_6c
    iget-object v8, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->c:[F

    aget v8, v8, v7

    neg-float v8, v8

    iget v9, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->i:I

    int-to-float v9, v9

    const/high16 v10, 0x42fe

    div-float/2addr v9, v10

    sub-float/2addr v9, v12

    add-float/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->b:[Landroid/graphics/Bitmap;

    aget-object v9, v9, v7

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 414
    iget-object v8, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->b:[Landroid/graphics/Bitmap;

    aget-object v7, v8, v7

    iget-boolean v1, v1, Lcom/google/android/maps/driveabout/app/br;->c:Z

    if-eqz v1, :cond_9a

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->e:Landroid/graphics/Paint;

    :goto_90
    invoke-virtual {v4, v7, v11, v11, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 416
    invoke-virtual {v4}, Landroid/graphics/Canvas;->restore()V

    .line 405
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_58

    .line 414
    :cond_9a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->f:Landroid/graphics/Paint;

    goto :goto_90

    .line 418
    :cond_9d
    invoke-virtual {v4}, Landroid/graphics/Canvas;->restore()V

    goto :goto_3d

    .line 421
    :cond_a1
    const/4 v0, 0x0

    invoke-virtual {p1, v3, v11, v11, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_b
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->b:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->h:Ljava/util/List;

    if-nez v0, :cond_16

    .line 332
    :cond_8
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->b(II)I

    move-result v0

    .line 333
    const/4 v1, 0x0

    invoke-static {p2, v1}, Lcom/google/android/maps/driveabout/app/LaneIconView;->b(II)I

    move-result v1

    .line 334
    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/LaneIconView;->setMeasuredDimension(II)V

    .line 381
    :goto_15
    return-void

    .line 339
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v6, v0, Landroid/util/DisplayMetrics;->density:F

    .line 341
    const v2, 0x7f7fffff

    .line 342
    const v1, -0x800001

    .line 343
    const/4 v0, 0x0

    .line 348
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_34
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/app/bs;

    .line 349
    iget v4, v0, Lcom/google/android/maps/driveabout/app/bs;->b:F

    mul-float v8, v4, v6

    .line 350
    iget-object v9, v0, Lcom/google/android/maps/driveabout/app/bs;->a:[Lcom/google/android/maps/driveabout/app/br;

    array-length v10, v9

    const/4 v0, 0x0

    move v5, v0

    move v0, v1

    move v1, v2

    move v2, v3

    :goto_4c
    if-ge v5, v10, :cond_8c

    aget-object v3, v9, v5

    .line 351
    iget v4, v3, Lcom/google/android/maps/driveabout/app/br;->a:I

    .line 352
    iget-object v11, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->b:[Landroid/graphics/Bitmap;

    aget-object v11, v11, v4

    .line 353
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    if-le v12, v0, :cond_60

    .line 354
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 356
    :cond_60
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    int-to-float v11, v11

    iget-object v12, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->c:[F

    aget v12, v12, v4

    sub-float/2addr v11, v12

    .line 358
    iget-boolean v3, v3, Lcom/google/android/maps/driveabout/app/br;->b:Z

    if-eqz v3, :cond_83

    .line 359
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->c:[F

    aget v3, v3, v4

    add-float/2addr v3, v8

    .line 360
    sub-float v4, v8, v11

    .line 365
    :goto_75
    cmpg-float v11, v4, v2

    if-gez v11, :cond_7a

    move v2, v4

    .line 368
    :cond_7a
    cmpl-float v4, v3, v1

    if-lez v4, :cond_7f

    move v1, v3

    .line 350
    :cond_7f
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_4c

    .line 362
    :cond_83
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->c:[F

    aget v3, v3, v4

    sub-float v4, v8, v3

    .line 363
    add-float v3, v8, v11

    goto :goto_75

    :cond_8c
    move v3, v2

    move v2, v1

    move v1, v0

    .line 372
    goto :goto_34

    .line 373
    :cond_90
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v3, v0

    .line 374
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->getPaddingRight()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 377
    neg-float v3, v0

    iput v3, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->d:F

    .line 378
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    sub-int v0, v2, v0

    invoke-static {p1, v0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->b(II)I

    move-result v0

    .line 379
    invoke-static {p2, v1}, Lcom/google/android/maps/driveabout/app/LaneIconView;->b(II)I

    move-result v1

    .line 380
    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/LaneIconView;->setMeasuredDimension(II)V

    goto/16 :goto_15
.end method

.method public setLaneIcons(Ljava/util/List;)V
    .registers 2
    .parameter

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/LaneIconView;->h:Ljava/util/List;

    .line 306
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->invalidate()V

    .line 307
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/LaneIconView;->requestLayout()V

    .line 308
    return-void
.end method
