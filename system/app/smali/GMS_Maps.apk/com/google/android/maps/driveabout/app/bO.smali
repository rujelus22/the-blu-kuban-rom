.class public Lcom/google/android/maps/driveabout/app/bo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:[LO/U;

.field private b:LO/U;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:[LO/b;

.field private f:Landroid/content/Intent;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LO/U;Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 574
    invoke-virtual {p1}, LO/U;->c()Lo/u;

    move-result-object v0

    .line 575
    if-eqz v0, :cond_d

    .line 576
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/bn;->a(Lo/u;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 578
    :cond_d
    invoke-virtual {p1}, LO/U;->d()LO/V;

    move-result-object v0

    .line 579
    if-eqz v0, :cond_1a

    .line 580
    invoke-virtual {v0}, LO/V;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 582
    :cond_1a
    invoke-virtual {p1}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    .line 583
    if-eqz v0, :cond_23

    .line 584
    invoke-virtual {p2, p5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 586
    :cond_23
    invoke-virtual {p1}, LO/U;->f()Ljava/lang/String;

    move-result-object v0

    .line 587
    if-eqz v0, :cond_2c

    .line 588
    invoke-virtual {p2, p6, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 591
    :cond_2c
    return-void
.end method


# virtual methods
.method a()Landroid/net/Uri;
    .registers 8

    .prologue
    .line 519
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "google.navigation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 526
    const/4 v0, 0x0

    :goto_1e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3b

    .line 527
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    aget-object v1, v1, v0

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    .line 528
    if-eqz v1, :cond_38

    .line 529
    const-string v3, "altvia"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(Lo/u;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 526
    :cond_38
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 535
    :cond_3b
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    array-length v1, v1

    if-ge v0, v1, :cond_50

    .line 536
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    aget-object v1, v1, v0

    const-string v3, "q"

    const-string v4, "ll"

    const-string v5, "title"

    const-string v6, "token"

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    :cond_50
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->b:LO/U;

    if-eqz v0, :cond_62

    .line 541
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->b:LO/U;

    const-string v3, "s"

    const-string v4, "sll"

    const-string v5, "stitle"

    const-string v6, "stoken"

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_62
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bo;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b0

    .line 546
    const-string v0, "mode"

    const-string v1, "w"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 551
    :cond_6e
    :goto_6e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    if-eqz v0, :cond_81

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_81

    .line 552
    const-string v0, "entry"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 555
    :cond_81
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->e:[LO/b;

    if-eqz v0, :cond_90

    .line 556
    const-string v0, "opt"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->e:[LO/b;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a([LO/b;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 560
    :cond_90
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->f:Landroid/content/Intent;

    if-eqz v0, :cond_a0

    .line 561
    const-string v0, "r"

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/bo;->f:Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 565
    :cond_a0
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/bo;->g:Z

    if-eqz v0, :cond_ab

    .line 566
    const-string v0, "goff"

    const-string v1, "true"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 569
    :cond_ab
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 547
    :cond_b0
    iget v0, p0, Lcom/google/android/maps/driveabout/app/bo;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6e

    .line 548
    const-string v0, "mode"

    const-string v1, "b"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_6e
.end method

.method a(I)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 494
    iput p1, p0, Lcom/google/android/maps/driveabout/app/bo;->c:I

    .line 495
    return-object p0
.end method

.method a(LO/U;)Lcom/google/android/maps/driveabout/app/bo;
    .registers 4
    .parameter

    .prologue
    .line 479
    const/4 v0, 0x1

    new-array v0, v0, [LO/U;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    .line 480
    return-object p0
.end method

.method a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 509
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->f:Landroid/content/Intent;

    .line 510
    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 499
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->d:Ljava/lang/String;

    .line 500
    return-object p0
.end method

.method a(Z)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 514
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/bo;->g:Z

    .line 515
    return-object p0
.end method

.method a([LO/U;)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 484
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->a:[LO/U;

    .line 485
    return-object p0
.end method

.method a([LO/b;)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 504
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->e:[LO/b;

    .line 505
    return-object p0
.end method

.method b(LO/U;)Lcom/google/android/maps/driveabout/app/bo;
    .registers 2
    .parameter

    .prologue
    .line 489
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/bo;->b:LO/U;

    .line 490
    return-object p0
.end method
