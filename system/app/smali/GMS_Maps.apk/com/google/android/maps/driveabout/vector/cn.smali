.class Lcom/google/android/maps/driveabout/vector/cn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LA/b;


# static fields
.field static final a:LA/D;


# instance fields
.field final b:LA/p;

.field final c:Ljava/util/List;

.field final d:Ljava/lang/ref/WeakReference;

.field final e:Landroid/content/res/Resources;

.field f:LA/c;

.field g:Lcom/google/android/maps/driveabout/vector/k;

.field final h:Lv/l;

.field i:Z

.field final j:Lo/P;

.field k:Lcom/google/android/maps/driveabout/vector/co;

.field l:I

.field final m:Ljava/util/Map;

.field n:F

.field o:F

.field p:F

.field q:F

.field r:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 1067
    new-instance v0, LA/D;

    const/high16 v1, 0x3f80

    invoke-direct {v0, v2, v2, v1}, LA/D;-><init>(FFF)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cn;->a:LA/D;

    return-void
.end method


# virtual methods
.method declared-synchronized a(Lcom/google/android/maps/driveabout/vector/aV;I)Lcom/google/android/maps/driveabout/vector/cX;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1156
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cX;

    .line 1157
    if-nez v0, :cond_27

    .line 1158
    const/4 v1, 0x1

    .line 1159
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cX;

    invoke-direct {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/cX;-><init>(Lcom/google/android/maps/driveabout/vector/aV;Z)V

    .line 1160
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/cX;->c(Z)V

    .line 1161
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cn;->e:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/maps/driveabout/vector/cX;->a(Landroid/content/res/Resources;I)V

    .line 1162
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cn;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    .line 1164
    :cond_27
    monitor-exit p0

    return-object v0

    .line 1156
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 1205
    return-void
.end method

.method declared-synchronized a(FFF)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1277
    monitor-enter p0

    :try_start_1
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/cn;->n:F

    .line 1278
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/cn;->o:F

    .line 1279
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/cn;->p:F
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 1280
    monitor-exit p0

    return-void

    .line 1277
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LA/c;)V
    .registers 3
    .parameter

    .prologue
    .line 1198
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/cn;->f:LA/c;

    .line 1199
    sget-object v0, LA/c;->a:LA/H;

    invoke-interface {p1, p0, v0}, LA/c;->a(LA/b;LA/G;)V

    .line 1200
    return-void
.end method

.method public declared-synchronized b(LA/c;)V
    .registers 10
    .parameter

    .prologue
    const/4 v3, 0x0

    const/high16 v1, 0x3f80

    const/4 v2, 0x1

    .line 1210
    monitor-enter p0

    :try_start_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aV;
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_92

    .line 1211
    if-nez v0, :cond_11

    .line 1273
    :cond_f
    :goto_f
    monitor-exit p0

    return-void

    .line 1216
    :cond_11
    :try_start_11
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cn;->h:Lv/l;

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v5

    invoke-interface {v4, v5, v6}, Lv/l;->a(J)I

    move-result v4

    .line 1217
    if-eqz v4, :cond_20

    .line 1221
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/cn;->i:Z

    .line 1225
    :cond_20
    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/cn;->r:Z

    if-nez v4, :cond_95

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget v4, v4, Lcom/google/android/maps/driveabout/vector/co;->c:I

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget v5, v5, Lcom/google/android/maps/driveabout/vector/co;->b:I

    if-eq v4, v5, :cond_95

    .line 1227
    :goto_2e
    if-eqz v2, :cond_97

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->j:Lo/P;

    invoke-virtual {v2}, Lo/P;->j()F

    move-result v2

    cmpl-float v2, v2, v1

    if-nez v2, :cond_97

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget v2, v2, Lcom/google/android/maps/driveabout/vector/co;->c:I

    .line 1230
    :goto_3e
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/cn;->l:I

    if-eq v3, v2, :cond_47

    .line 1231
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->l:I

    .line 1232
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->i:Z

    .line 1236
    :cond_47
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->i:Z

    if-eqz v2, :cond_f

    .line 1237
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->i:Z

    .line 1239
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->b:LA/p;

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/cn;->l:I

    invoke-virtual {p0, v0, v3}, Lcom/google/android/maps/driveabout/vector/cn;->a(Lcom/google/android/maps/driveabout/vector/aV;I)Lcom/google/android/maps/driveabout/vector/cX;

    move-result-object v0

    invoke-virtual {v2, v0}, LA/p;->a(LA/m;)V

    .line 1241
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->h:Lv/l;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->j:Lo/P;

    invoke-interface {v0, v2}, Lv/l;->a(Lo/P;)Z

    .line 1242
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->j:Lo/P;

    invoke-virtual {v0}, Lo/P;->a()Lo/Q;

    move-result-object v3

    .line 1245
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->g:Lcom/google/android/maps/driveabout/vector/k;

    if-eqz v0, :cond_f

    if-eqz v3, :cond_f

    .line 1246
    const/4 v0, 0x4

    new-array v0, v0, [F

    .line 1247
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->j:Lo/P;

    invoke-virtual {v0}, Lo/P;->c()I

    move-result v0

    int-to-double v4, v0

    invoke-virtual {v3}, Lo/Q;->e()D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v2, v4

    .line 1249
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_82
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LA/p;

    .line 1250
    invoke-virtual {v0, v3, v2}, LA/p;->a(Lo/Q;F)V
    :try_end_91
    .catchall {:try_start_11 .. :try_end_91} :catchall_92

    goto :goto_82

    .line 1210
    :catchall_92
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_95
    move v2, v3

    .line 1225
    goto :goto_2e

    .line 1227
    :cond_97
    :try_start_97
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget v2, v2, Lcom/google/android/maps/driveabout/vector/co;->b:I

    goto :goto_3e

    .line 1256
    :cond_9c
    const/4 v0, 0x1

    .line 1257
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/cn;->g:Lcom/google/android/maps/driveabout/vector/k;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/maps/driveabout/vector/k;->a(Lo/Q;Z)F

    .line 1260
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/co;->a:Z

    if-eqz v0, :cond_da

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->o:F

    move v2, v0

    .line 1261
    :goto_ab
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/cn;->q:F

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/co;->a:Z

    if-eqz v0, :cond_de

    move v0, v1

    :goto_b4
    mul-float/2addr v0, v4

    mul-float/2addr v0, v2

    .line 1262
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cn;->b:LA/p;

    invoke-virtual {v1, v3, v0}, LA/p;->a(Lo/Q;F)V

    .line 1263
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->k:Lcom/google/android/maps/driveabout/vector/co;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/co;->a:Z

    if-eqz v0, :cond_cd

    .line 1264
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->b:LA/p;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/cn;->j:Lo/P;

    invoke-virtual {v1}, Lo/P;->b()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {v0, v1}, LA/p;->a(F)V

    .line 1268
    :cond_cd
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->f:LA/c;

    if-eqz v0, :cond_f

    .line 1269
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->f:LA/c;

    sget-object v1, LA/j;->a:LA/H;

    invoke-interface {v0, p0, v1}, LA/c;->a(LA/b;LA/G;)V

    goto/16 :goto_f

    .line 1260
    :cond_da
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->n:F

    move v2, v0

    goto :goto_ab

    .line 1261
    :cond_de
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/cn;->p:F
    :try_end_e0
    .catchall {:try_start_97 .. :try_end_e0} :catchall_92

    goto :goto_b4
.end method
