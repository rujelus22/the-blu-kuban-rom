.class public Lcom/google/android/maps/driveabout/vector/bl;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private final b:LC/b;

.field private c:LC/c;

.field private final d:I

.field private final e:J

.field private f:I

.field private final g:Lcom/google/googlenav/common/a;

.field private h:F


# direct methods
.method protected constructor <init>(LC/b;LC/c;IZF)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 988
    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    .line 976
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/bl;->f:I

    .line 977
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->g:Lcom/google/googlenav/common/a;

    .line 989
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bl;->b:LC/b;

    .line 990
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    .line 991
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->g:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->e:J

    .line 993
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->b:LC/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 995
    iput v2, p0, Lcom/google/android/maps/driveabout/vector/bl;->d:I

    .line 1002
    :goto_2c
    if-eqz p4, :cond_39

    .line 1003
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->h:F

    .line 1007
    :goto_31
    return-void

    .line 999
    :cond_32
    invoke-static {v2, p3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->d:I

    goto :goto_2c

    .line 1005
    :cond_39
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/bl;->h:F

    goto :goto_31
.end method

.method private a(F)F
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 1047
    float-to-double v0, p1

    const-wide v2, 0x400921fb54442d18L

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    add-float/2addr v0, v4

    const/high16 v1, 0x3f00

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    return v0
.end method


# virtual methods
.method public a(LC/a;)LC/c;
    .registers 8
    .parameter

    .prologue
    const/high16 v2, 0x3f80

    .line 1011
    const/4 v3, 0x0

    .line 1012
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->g:Lcom/google/googlenav/common/a;

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    .line 1013
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bl;->d:I

    if-nez v4, :cond_32

    move v1, v2

    .line 1015
    :goto_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    instance-of v0, v0, Lw/d;

    if-eqz v0, :cond_5b

    .line 1016
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    check-cast v0, Lw/d;

    .line 1018
    cmpl-float v3, v1, v2

    if-ltz v3, :cond_1f

    .line 1020
    invoke-interface {v0}, Lw/d;->a()V

    .line 1022
    :cond_1f
    invoke-interface {v0, p1}, Lw/d;->a(LC/a;)LC/c;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    .line 1023
    invoke-interface {v0}, Lw/d;->c()I

    move-result v0

    .line 1026
    :goto_29
    cmpl-float v2, v1, v2

    if-ltz v2, :cond_3c

    .line 1027
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->f:I

    .line 1028
    iget-object p0, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    .line 1035
    :goto_31
    return-object p0

    .line 1013
    :cond_32
    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/bl;->e:J

    sub-long/2addr v0, v4

    long-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bl;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_e

    .line 1030
    :cond_3c
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/bl;->a(F)F

    move-result v1

    .line 1031
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bl;->b:LC/b;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bl;->c:LC/c;

    invoke-interface {v3}, LC/c;->b()LC/b;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bl;->b:LC/b;

    invoke-virtual {v3, v4}, LC/b;->a(LC/b;)LC/b;

    move-result-object v3

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bl;->h:F

    invoke-static {v2, v3, v1, v4}, LC/b;->a(LC/b;LC/b;FF)LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bl;->a:LC/b;

    .line 1034
    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->f:I

    goto :goto_31

    :cond_5b
    move v0, v3

    goto :goto_29
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1040
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bl;->f:I

    return v0
.end method
