.class Lcom/google/android/maps/driveabout/vector/bR;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/dc;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/google/android/maps/driveabout/vector/dc;

.field private c:I

.field private d:Ls/Q;

.field private e:Lm/e;

.field private f:Lm/q;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/dc;ILandroid/content/Context;Lm/q;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/bR;->a:Landroid/content/Context;

    .line 678
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bR;->b:Lcom/google/android/maps/driveabout/vector/dc;

    .line 679
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/bR;->c:I

    .line 680
    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/bR;->f:Lm/q;

    .line 681
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/di;ZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/cL;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 705
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cL;

    invoke-direct {v0, p1, p3}, Lcom/google/android/maps/driveabout/vector/cL;-><init>(Lcom/google/android/maps/driveabout/vector/di;Lcom/google/android/maps/driveabout/vector/dh;)V

    return-object v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/di;IZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/da;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 686
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bR;->b:Lcom/google/android/maps/driveabout/vector/dc;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/dc;->a(Lcom/google/android/maps/driveabout/vector/di;IZLcom/google/android/maps/driveabout/vector/dh;)Lcom/google/android/maps/driveabout/vector/da;

    move-result-object v1

    .line 689
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bR;->d:Ls/Q;

    if-nez v0, :cond_10

    .line 690
    invoke-static {}, Ls/Q;->b()Ls/Q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bR;->d:Ls/Q;

    .line 692
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bR;->e:Lm/e;

    if-nez v0, :cond_1c

    .line 693
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bR;->f:Lm/q;

    invoke-virtual {v0}, Lm/q;->j()Lm/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bR;->e:Lm/e;

    .line 695
    :cond_1c
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bM;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bR;->e:Lm/e;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bR;->d:Ls/Q;

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bR;->c:I

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bR;->f:Lm/q;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bM;-><init>(Lcom/google/android/maps/driveabout/vector/da;Lm/e;Ls/Q;ILm/q;)V

    return-object v0
.end method
