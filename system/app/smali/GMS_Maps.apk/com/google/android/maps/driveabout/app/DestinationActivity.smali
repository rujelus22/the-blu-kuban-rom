.class public Lcom/google/android/maps/driveabout/app/DestinationActivity;
.super Landroid/app/ListActivity;
.source "SourceFile"


# static fields
.field private static final a:[Lcom/google/android/maps/driveabout/app/P;


# instance fields
.field private b:[Lcom/google/android/maps/driveabout/app/P;

.field private c:Lcom/google/android/maps/driveabout/app/P;

.field private d:[LO/b;

.field private e:Landroid/os/Handler;

.field private f:Lcom/google/android/maps/driveabout/app/af;

.field private g:Landroid/location/LocationManager;

.field private h:Landroid/location/LocationListener;

.field private i:LaH/h;

.field private j:Lcom/google/android/maps/driveabout/app/an;

.field private k:Lcom/google/android/maps/driveabout/app/T;

.field private l:Lcom/google/android/maps/driveabout/app/bR;

.field private final m:Lcom/google/android/maps/driveabout/app/l;

.field private final n:Ljava/lang/Runnable;

.field private final o:LR/b;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

.field private u:Z

.field private v:LO/f;

.field private w:Lcom/google/android/maps/driveabout/app/Q;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 121
    new-array v0, v5, [Lcom/google/android/maps/driveabout/app/P;

    new-instance v1, Lcom/google/android/maps/driveabout/app/P;

    const v2, 0x7f020139

    const v3, 0x7f0d00da

    invoke-direct {v1, v2, v3, v6, v4}, Lcom/google/android/maps/driveabout/app/P;-><init>(IIII)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/google/android/maps/driveabout/app/P;

    const v2, 0x7f02013c

    const v3, 0x7f0d00db

    invoke-direct {v1, v2, v3, v7, v5}, Lcom/google/android/maps/driveabout/app/P;-><init>(IIII)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/google/android/maps/driveabout/app/P;

    const v2, 0x7f020136

    const v3, 0x7f0d00dc

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/google/android/maps/driveabout/app/P;-><init>(IIII)V

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 134
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/P;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    .line 150
    new-instance v0, Lcom/google/android/maps/driveabout/app/l;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    .line 152
    new-instance v0, Lcom/google/android/maps/driveabout/app/r;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/r;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n:Ljava/lang/Runnable;

    .line 159
    new-instance v0, Lcom/google/android/maps/driveabout/app/A;

    const-string v1, "DestinationActivityIdleHandler"

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/A;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    .line 180
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->u:Z

    .line 1328
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/Y;)I
    .registers 2
    .parameter

    .prologue
    .line 69
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/android/maps/driveabout/app/Y;)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/location/Location;)J
    .registers 5
    .parameter

    .prologue
    .line 1092
    if-nez p0, :cond_8

    .line 1093
    const-wide v0, 0x7fffffffffffffffL

    .line 1095
    :goto_7
    return-wide v0

    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_7
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;LaH/h;)LaH/h;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    return-object p1
.end method

.method private a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/P;
    .registers 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 495
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_25

    const-string v1, "TravelMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 497
    const-string v1, "TravelMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 502
    :goto_15
    sget-object v4, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v5, v4

    move v2, v3

    :goto_19
    if-ge v2, v5, :cond_30

    aget-object v1, v4, v2

    .line 503
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v6

    if-ne v6, v0, :cond_2c

    move-object v0, v1

    .line 507
    :goto_24
    return-object v0

    .line 499
    :cond_25
    const-string v0, "PickerTravelMode"

    invoke-static {p0, v0, v3}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    goto :goto_15

    .line 502
    :cond_2c
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_19

    .line 507
    :cond_30
    sget-object v0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v0, v0, v3

    goto :goto_24
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/P;)Lcom/google/android/maps/driveabout/app/P;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/T;)Lcom/google/android/maps/driveabout/app/T;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/bR;)Lcom/google/android/maps/driveabout/app/bR;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l:Lcom/google/android/maps/driveabout/app/bR;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    return-object p1
.end method

.method private a(I)V
    .registers 2
    .parameter

    .prologue
    .line 979
    packed-switch p1, :pswitch_data_14

    .line 993
    :goto_3
    return-void

    .line 981
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i()V

    goto :goto_3

    .line 984
    :pswitch_8
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->onSearchRequested()Z

    goto :goto_3

    .line 987
    :pswitch_c
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k()V

    goto :goto_3

    .line 990
    :pswitch_10
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j()V

    goto :goto_3

    .line 979
    :pswitch_data_14
    .packed-switch 0x7f0d00c9
        :pswitch_8
        :pswitch_4
        :pswitch_10
        :pswitch_c
    .end packed-switch
.end method

.method private a(LO/U;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 869
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-nez v0, :cond_7

    .line 926
    :cond_6
    :goto_6
    return-void

    .line 875
    :cond_7
    const/4 v0, 0x0

    .line 876
    const/4 v3, 0x6

    if-ne p2, v3, :cond_2e

    .line 878
    invoke-static {p0}, LO/M;->a(Landroid/content/Context;)LO/M;

    move-result-object v0

    .line 883
    invoke-virtual {v0}, LO/M;->b()LO/a;

    move-result-object v3

    .line 884
    invoke-virtual {v0}, LO/M;->d()V

    .line 885
    invoke-virtual {v3}, LO/a;->a()LO/g;

    move-result-object v0

    .line 886
    if-eqz v0, :cond_6

    .line 890
    invoke-virtual {v0}, LO/g;->f()[LO/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    .line 892
    invoke-virtual {v3}, LO/a;->d()[LO/U;

    move-result-object v0

    .line 893
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object p1, v0, v3

    .line 894
    aget-object v0, v0, v1

    move v1, v2

    .line 896
    :cond_2e
    const-string v3, "PickerTravelMode"

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v4

    invoke-static {p0, v3, v4}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 899
    const-string v3, "D"

    invoke-static {v3, p2}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;I)V

    .line 901
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 902
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 903
    const-string v4, "TravelMode"

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 904
    const-string v4, "ForceNewDestination"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 905
    const/4 v2, 0x2

    if-ne p2, v2, :cond_ae

    .line 906
    const-string v2, "Target"

    const-string v4, "Contact"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 910
    :cond_68
    :goto_68
    const/high16 v2, 0x1002

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 913
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 914
    new-instance v4, Lcom/google/android/maps/driveabout/app/bo;

    invoke-direct {v4}, Lcom/google/android/maps/driveabout/app/bo;-><init>()V

    invoke-virtual {v4, p1}, Lcom/google/android/maps/driveabout/app/bo;->a(LO/U;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/maps/driveabout/app/bo;->b(LO/U;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/bo;->a(I)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/app/bo;->a([LO/b;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bo;->a(Z)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/bo;->a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/bo;->a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/app/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bo;->a()Landroid/net/Uri;

    move-result-object v0

    .line 923
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 924
    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    .line 925
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    goto/16 :goto_6

    .line 907
    :cond_ae
    const/4 v2, 0x3

    if-ne p2, v2, :cond_68

    .line 908
    const-string v2, "Target"

    const-string v4, "StarredItem"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_68
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;LO/U;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(LO/U;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;[LO/b;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a([LO/b;)V

    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/app/P;)V
    .registers 3
    .parameter

    .prologue
    .line 1292
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    .line 1293
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    if-eqz v0, :cond_b

    .line 1294
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;)V

    .line 1297
    :cond_b
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/android/maps/driveabout/app/P;)V

    .line 1298
    return-void
.end method

.method private a(Lcom/google/googlenav/j;)V
    .registers 7
    .parameter

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    invoke-virtual {v0}, LaH/h;->r()LaN/B;

    move-result-object v1

    .line 1166
    new-instance v2, Ljava/util/ArrayList;

    sget-object v0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1168
    const/4 v0, 0x0

    :goto_f
    sget-object v3, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v3, v3

    if-ge v0, v3, :cond_34

    .line 1169
    sget-object v3, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v3, v3, v0

    invoke-virtual {v3, p1, v1}, Lcom/google/android/maps/driveabout/app/P;->a(Lcom/google/googlenav/j;LaN/B;)Z

    move-result v3

    if-eqz v3, :cond_28

    .line 1170
    sget-object v3, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1168
    :cond_25
    :goto_25
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 1171
    :cond_28
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    sget-object v4, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v4, v4, v0

    if-ne v3, v4, :cond_25

    .line 1172
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    goto :goto_25

    .line 1175
    :cond_34
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/app/P;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    .line 1176
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    if-eqz v0, :cond_4d

    .line 1177
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setVisibleTravelModes([Lcom/google/android/maps/driveabout/app/P;)V

    .line 1179
    :cond_4d
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 579
    const-string v0, "Contact"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 580
    const v0, 0x7f0d00cb

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    .line 593
    :cond_e
    :goto_e
    return-void

    .line 581
    :cond_f
    const-string v0, "StarredItem"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 582
    const v0, 0x7f0d00cc

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    goto :goto_e

    .line 583
    :cond_1e
    const-string v0, "Speak"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 585
    const v0, 0x7f0d00ca

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    goto :goto_e

    .line 588
    :cond_33
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 589
    const-string v0, "Search"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 590
    const v0, 0x7f0d00c9

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    goto :goto_e
.end method

.method private a(Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 864
    new-instance v0, LO/U;

    invoke-direct {v0, p1, v1, v1, v1}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(LO/U;I)V

    .line 865
    return-void
.end method

.method private a([LO/b;)V
    .registers 2
    .parameter

    .prologue
    .line 1324
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    .line 1325
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->u:Z

    return p1
.end method

.method private static b(Lcom/google/android/maps/driveabout/app/Y;)I
    .registers 2
    .parameter

    .prologue
    .line 847
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/Y;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_12

    .line 858
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 849
    :pswitch_9
    const/4 v0, 0x1

    goto :goto_8

    .line 851
    :pswitch_b
    const/4 v0, 0x2

    goto :goto_8

    .line 853
    :pswitch_d
    const/4 v0, 0x3

    goto :goto_8

    .line 856
    :pswitch_f
    const/4 v0, 0x6

    goto :goto_8

    .line 847
    nop

    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_9
        :pswitch_b
        :pswitch_d
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method private b(Landroid/content/Intent;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 569
    const/4 v0, 0x0

    .line 570
    if-eqz p1, :cond_23

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_23

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_23

    const-string v1, "Target"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 572
    const-string v0, "Target"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 574
    :cond_23
    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 475
    .line 476
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    if-eqz v1, :cond_14

    .line 477
    invoke-static {}, LO/c;->a()LO/c;

    move-result-object v1

    .line 478
    if-eqz v1, :cond_14

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    invoke-virtual {v1, v2}, LO/c;->b([LO/b;)I

    move-result v1

    if-lez v1, :cond_14

    const/4 v0, 0x1

    .line 483
    :cond_14
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->x:Z

    if-eq v1, v0, :cond_1f

    .line 484
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->x:Z

    .line 485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    .line 487
    :cond_1f
    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/P;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/android/maps/driveabout/app/P;)V

    return-void
.end method

.method private b(Lcom/google/android/maps/driveabout/app/P;)V
    .registers 4
    .parameter

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v0

    invoke-static {v0}, LO/c;->b(I)[LO/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a([LO/b;)V

    .line 1304
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b()V

    .line 1308
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l:Lcom/google/android/maps/driveabout/app/bR;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/bR;->a_(I)V

    .line 1309
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1236
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    .line 1237
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/t;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/t;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/u;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/u;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->b(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1254
    return-void
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/DestinationActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    return p1
.end method

.method private b(Lcom/google/googlenav/j;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1186
    move v0, v1

    :goto_2
    sget-object v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    array-length v2, v2

    if-ge v0, v2, :cond_19

    .line 1187
    sget-object v2, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v2, v2, v0

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/P;->a(Lcom/google/android/maps/driveabout/app/P;)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/googlenav/j;->a(I)Z

    move-result v2

    if-nez v2, :cond_16

    .line 1191
    :goto_15
    return v1

    .line 1186
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1191
    :cond_19
    const/4 v1, 0x1

    goto :goto_15
.end method

.method private c()V
    .registers 3

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-eqz v0, :cond_5

    .line 562
    :goto_4
    return-void

    .line 518
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/app/Application;)V

    .line 522
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 523
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r()V

    goto :goto_4

    .line 528
    :cond_16
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 529
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q()V

    goto :goto_4

    .line 534
    :cond_20
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d006a

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(I)V

    .line 540
    new-instance v0, Lcom/google/android/maps/driveabout/app/L;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/L;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    .line 541
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    .line 542
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l()LaH/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    .line 543
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    if-eqz v0, :cond_47

    .line 544
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m()V

    .line 548
    :cond_47
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e()V

    .line 552
    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ad;->a(Landroid/content/Context;)V

    .line 558
    sget-object v0, Lcom/google/googlenav/z;->c:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    .line 559
    sget-object v0, Lcom/google/googlenav/z;->b:Lcom/google/googlenav/z;

    invoke-static {v0}, Lcom/google/googlenav/u;->b(Lcom/google/googlenav/z;)V

    .line 561
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    goto :goto_4
.end method

.method private c(Landroid/content/Intent;)V
    .registers 5
    .parameter

    .prologue
    .line 971
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_3} :catch_4

    .line 975
    :goto_3
    return-void

    .line 972
    :catch_4
    move-exception v0

    .line 973
    const-string v0, "DestinationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to start activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    return-object v0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 674
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-nez v0, :cond_11

    .line 675
    const-string v0, "Show Disclaimer"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 676
    const/4 v0, 0x0

    .line 677
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Ljava/lang/String;)V

    .line 682
    :cond_11
    :goto_11
    return-void

    .line 679
    :cond_12
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    goto :goto_11
.end method

.method private e()V
    .registers 8

    .prologue
    const-wide/16 v2, 0x2710

    const/4 v4, 0x0

    .line 816
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v6

    .line 818
    if-eqz v6, :cond_2d

    .line 819
    const-string v0, "gps"

    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 820
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 823
    :cond_1c
    const-string v0, "network"

    invoke-interface {v6, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 824
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 828
    :cond_2d
    return-void
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/T;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    return-object v0
.end method

.method private f()V
    .registers 4

    .prologue
    .line 930
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 931
    const-string v1, "http://maps.google.com/?myl=saddr&dirflg=d&daddr="

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 932
    const-string v1, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 934
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    .line 935
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    .line 936
    return-void
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/af;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    return-object v0
.end method

.method private g()V
    .registers 4

    .prologue
    .line 943
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-nez v0, :cond_5

    .line 959
    :goto_4
    return-void

    .line 949
    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 950
    invoke-static {p0}, Lcom/google/googlenav/android/W;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_23

    .line 951
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(Z)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 957
    :goto_1a
    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 958
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    goto :goto_4

    .line 953
    :cond_23
    const-string v1, "PickerTravelMode"

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v2

    invoke-static {p0, v1, v2}, LR/s;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 955
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/P;->a()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/bn;->a(I)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_1a
.end method

.method private h()V
    .registers 3

    .prologue
    .line 963
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 964
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 965
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c(Landroid/content/Intent;)V

    .line 966
    return-void
.end method

.method static synthetic h(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/maps/driveabout/app/DestinationActivity;)LaH/h;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 996
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p:Z

    .line 997
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v2, v0, v1}, Lcom/google/android/maps/driveabout/app/eP;->a(Landroid/app/Activity;ILjava/lang/String;Z)V

    .line 998
    return-void
.end method

.method private j()V
    .registers 2

    .prologue
    .line 1001
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;Z)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 1002
    return-void
.end method

.method static synthetic j(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/bR;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->l:Lcom/google/android/maps/driveabout/app/bR;

    return-object v0
.end method

.method private k()V
    .registers 3

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i:LaH/h;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;LaH/h;Z)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 1006
    return-void
.end method

.method private l()LaH/h;
    .registers 8

    .prologue
    const/4 v1, 0x0

    .line 1061
    .line 1064
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v2

    .line 1066
    if-eqz v2, :cond_6f

    .line 1067
    const-string v0, "gps"

    invoke-interface {v2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 1068
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 1070
    :goto_19
    const-string v3, "network"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6b

    .line 1071
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    const-string v3, "network"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    .line 1075
    :goto_29
    if-eqz v0, :cond_44

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v3

    const-wide/32 v5, 0x927c0

    cmp-long v3, v3, v5

    if-gez v3, :cond_44

    .line 1076
    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    invoke-virtual {v1, v0}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v1

    .line 1087
    :cond_43
    :goto_43
    return-object v1

    .line 1079
    :cond_44
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v3

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_69

    .line 1084
    :goto_50
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/location/Location;)J

    move-result-wide v2

    const-wide/32 v4, 0xdbba00

    cmp-long v2, v2, v4

    if-gez v2, :cond_43

    .line 1085
    new-instance v1, LaH/j;

    invoke-direct {v1}, LaH/j;-><init>()V

    invoke-virtual {v1, v0}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v1

    goto :goto_43

    :cond_69
    move-object v0, v2

    .line 1082
    goto :goto_50

    :cond_6b
    move-object v2, v1

    goto :goto_29

    :cond_6d
    move-object v0, v1

    goto :goto_19

    :cond_6f
    move-object v2, v1

    move-object v0, v1

    goto :goto_29
.end method

.method static synthetic l(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/an;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    return-object v0
.end method

.method private m()V
    .registers 1

    .prologue
    .line 1120
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n()V

    .line 1121
    return-void
.end method

.method private n()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1128
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/ci;->h()Lcom/google/googlenav/j;

    move-result-object v2

    .line 1130
    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/googlenav/j;)V

    .line 1131
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    array-length v3, v3

    if-eqz v3, :cond_36

    .line 1136
    const-string v2, "e"

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 1137
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/an;->b()V

    .line 1138
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->s:Z

    .line 1140
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-nez v2, :cond_29

    .line 1141
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/P;)V

    .line 1144
    :cond_29
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    array-length v2, v2

    if-eq v2, v0, :cond_34

    .line 1145
    :goto_2e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/Q;->a(Z)V

    .line 1162
    :goto_33
    return-void

    :cond_34
    move v0, v1

    .line 1144
    goto :goto_2e

    .line 1146
    :cond_36
    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Lcom/google/googlenav/j;)Z

    move-result v0

    if-nez v0, :cond_58

    .line 1147
    invoke-virtual {v2}, Lcom/google/googlenav/j;->b()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 1151
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o()V

    goto :goto_33

    .line 1156
    :cond_46
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d006b

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(I)V

    .line 1157
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_33

    .line 1160
    :cond_58
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p()V

    goto :goto_33
.end method

.method private o()V
    .registers 8

    .prologue
    .line 1199
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d0003

    const v2, 0x7f0d0004

    const v3, 0x7f0d0048

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/maps/driveabout/app/H;

    invoke-direct {v5, p0}, Lcom/google/android/maps/driveabout/app/H;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/an;->a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1211
    return-void
.end method

.method private p()V
    .registers 8

    .prologue
    .line 1218
    new-instance v5, Lcom/google/android/maps/driveabout/app/I;

    invoke-direct {v5, p0}, Lcom/google/android/maps/driveabout/app/I;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    .line 1224
    new-instance v6, Lcom/google/android/maps/driveabout/app/s;

    invoke-direct {v6, p0}, Lcom/google/android/maps/driveabout/app/s;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    .line 1230
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const v1, 0x7f0d005c

    const v2, 0x7f0d00c4

    const v3, 0x7f0d0082

    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/app/an;->a(IIIZLandroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1233
    return-void
.end method

.method private q()V
    .registers 5

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/v;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/v;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/w;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/w;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1272
    return-void
.end method

.method private r()V
    .registers 4

    .prologue
    .line 1275
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/x;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/x;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    new-instance v2, Lcom/google/android/maps/driveabout/app/y;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/y;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1289
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 4

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d:[LO/b;

    new-instance v2, Lcom/google/android/maps/driveabout/app/z;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/z;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a([LO/b;Lcom/google/android/maps/driveabout/app/aH;)V

    .line 1320
    return-void
.end method

.method public handleContactsClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 1019
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j()V

    .line 1020
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    .line 1021
    return-void
.end method

.method public handleDriveHomeClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 1029
    new-instance v0, Lcom/google/android/maps/driveabout/app/G;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/G;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    .line 1041
    const-string v1, "HomeAddress"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LR/s;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1042
    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1043
    :cond_14
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/an;->a(Lcom/google/android/maps/driveabout/app/aG;)V

    .line 1047
    :goto_19
    return-void

    .line 1045
    :cond_1a
    const/4 v0, 0x7

    invoke-direct {p0, v1, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;I)V

    goto :goto_19
.end method

.method public handleFreeDriveClick(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 1050
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g()V

    .line 1051
    return-void
.end method

.method public handleSpeakDestinationClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 1009
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->i()V

    .line 1010
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    .line 1011
    return-void
.end method

.method public handleStarredItemsClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 1024
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k()V

    .line 1025
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    .line 1026
    return-void
.end method

.method public handleTypeDestinationClick(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 1014
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->onSearchRequested()Z

    .line 1015
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->b()V

    .line 1016
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 686
    const/4 v0, 0x1

    if-ne p1, v0, :cond_10

    .line 687
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p:Z

    .line 688
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/E;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/E;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-static {p0, p2, p3, v0, v1}, Lcom/google/android/maps/driveabout/app/eP;->a(Landroid/content/Context;ILandroid/content/Intent;Lcom/google/android/maps/driveabout/app/an;Lcom/google/android/maps/driveabout/app/eS;)V

    .line 706
    :cond_10
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 459
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 461
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    if-eqz v0, :cond_c

    .line 462
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a()V

    .line 464
    :cond_c
    invoke-static {}, LaA/h;->b()LaA/h;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 465
    invoke-static {}, LaA/h;->b()LaA/h;

    move-result-object v0

    invoke-virtual {v0}, LaA/h;->e()V

    .line 467
    :cond_19
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    const v3, 0x7f04002c

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 189
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0, p0}, Lcom/google/android/maps/driveabout/app/l;->a(Landroid/app/Activity;)V

    .line 191
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setDefaultKeyMode(I)V

    .line 192
    invoke-static {v6}, Lcom/google/android/maps/driveabout/app/dp;->a(Z)V

    .line 194
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_7f

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_7f

    .line 196
    const v0, 0x7f040031

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setContentView(I)V

    .line 197
    new-instance v0, Lcom/google/android/maps/driveabout/app/O;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/O;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    .line 214
    :goto_31
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bf;->a:[LA/c;

    const-string v3, "DriveAbout"

    const v4, 0x7f070003

    new-instance v5, Lcom/google/googlenav/android/I;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/googlenav/android/I;-><init>(Landroid/content/Context;)V

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bf;->a(Landroid/content/Context;Landroid/content/res/Resources;[LA/c;Ljava/lang/String;ILbm/q;)V

    .line 221
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e:Landroid/os/Handler;

    .line 222
    new-instance v0, Lcom/google/android/maps/driveabout/app/af;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    .line 223
    new-instance v0, Lcom/google/android/maps/driveabout/app/an;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    .line 227
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ForceNewDestination"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_be

    invoke-static {p0}, Lcom/google/googlenav/android/W;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_be

    .line 229
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    .line 277
    :goto_7e
    return-void

    .line 198
    :cond_7f
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_a2

    .line 199
    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setContentView(I)V

    .line 200
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 201
    new-instance v0, Lcom/google/android/maps/driveabout/app/K;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/K;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    goto :goto_31

    .line 203
    :cond_9a
    new-instance v0, Lcom/google/android/maps/driveabout/app/J;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/J;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    goto :goto_31

    .line 206
    :cond_a2
    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setContentView(I)V

    .line 207
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_b8

    new-instance v0, Lcom/google/android/maps/driveabout/app/N;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/N;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    :goto_b4
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    goto/16 :goto_31

    :cond_b8
    new-instance v0, Lcom/google/android/maps/driveabout/app/Q;

    invoke-direct {v0, p0, v2}, Lcom/google/android/maps/driveabout/app/Q;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    goto :goto_b4

    .line 236
    :cond_be
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    new-instance v1, Lcom/google/android/maps/driveabout/app/B;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/B;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/an;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/Q;->a()V

    .line 245
    new-instance v0, Lcom/google/android/maps/driveabout/app/C;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/C;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->v:LO/f;

    .line 256
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->v:LO/f;

    invoke-static {v0}, LO/c;->a(LO/f;)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/M;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/M;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 261
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/P;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/P;)V

    .line 262
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 263
    const-string v1, "Speak"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_113

    invoke-static {p0}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_113

    .line 269
    const v0, 0x7f0d00ca

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_7e

    .line 271
    :cond_113
    const-string v1, "Show Disclaimer"

    invoke-static {p0, v1, v6}, LR/s;->b(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_120

    .line 272
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_7e

    .line 274
    :cond_120
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v1, v2}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 275
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;)V

    goto/16 :goto_7e
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 724
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 725
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 656
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    if-eqz v0, :cond_c

    .line 657
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/af;->a()V

    .line 658
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f:Lcom/google/android/maps/driveabout/app/af;

    .line 660
    :cond_c
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 661
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->v:LO/f;

    invoke-static {v0}, LO/c;->b(LO/f;)V

    .line 663
    :cond_1b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->a()V

    .line 664
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->a()V

    .line 666
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 667
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 710
    const/4 v0, 0x4

    if-ne p1, v0, :cond_15

    .line 712
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    if-eq v0, v1, :cond_12

    .line 713
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 714
    const/4 v0, 0x1

    .line 718
    :goto_11
    return v0

    .line 716
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    .line 718
    :cond_15
    invoke-super {p0, p1, p2}, Landroid/app/ListActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_11
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 597
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 599
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->s:Z

    if-nez v0, :cond_8

    .line 616
    :cond_7
    :goto_7
    return-void

    .line 604
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 605
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 606
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 607
    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;I)V

    goto :goto_7

    .line 610
    :cond_2d
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Landroid/content/Intent;)Lcom/google/android/maps/driveabout/app/P;

    move-result-object v0

    .line 611
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    if-eq v0, v1, :cond_38

    .line 612
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/P;)V

    .line 614
    :cond_38
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Ljava/lang/String;)V

    goto :goto_7
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 771
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 772
    sparse-switch v1, :sswitch_data_40

    .line 794
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 774
    :sswitch_a
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h()V

    goto :goto_9

    .line 777
    :sswitch_e
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g()V

    goto :goto_9

    .line 780
    :sswitch_12
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a()V

    goto :goto_9

    .line 783
    :sswitch_16
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Lcom/google/android/maps/driveabout/app/aG;)V

    goto :goto_9

    .line 786
    :sswitch_1d
    const-string v1, "A"

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/dp;->a(Ljava/lang/String;)V

    .line 787
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/maps/driveabout/app/SettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_9

    .line 790
    :sswitch_2d
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {}, Lcom/google/googlenav/K;->ae()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_9

    .line 772
    :sswitch_data_40
    .sparse-switch
        0x7f1000f5 -> :sswitch_1d
        0x7f1000f6 -> :sswitch_2d
        0x7f10049c -> :sswitch_a
        0x7f10049d -> :sswitch_12
        0x7f10049e -> :sswitch_16
        0x7f1004a0 -> :sswitch_e
    .end sparse-switch
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 637
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 638
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->c()V

    .line 640
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-nez v0, :cond_15

    .line 641
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->removeIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 643
    :cond_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    if-eqz v0, :cond_20

    .line 644
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->h:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 648
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_3d

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->p:Z

    if-nez v0, :cond_3d

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->s:Z

    if-nez v0, :cond_3d

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    if-nez v0, :cond_3d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->j:Lcom/google/android/maps/driveabout/app/an;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/an;->m()Z

    move-result v0

    if-nez v0, :cond_3d

    .line 650
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->finish()V

    .line 652
    :cond_3d
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    const v1, 0x7f10049c

    const/4 v2, 0x1

    .line 730
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 733
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->w:Lcom/google/android/maps/driveabout/app/Q;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/Q;->a(Landroid/view/Menu;)V

    .line 735
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/maps/driveabout/app/W;

    if-eqz v0, :cond_62

    .line 736
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 741
    :goto_1b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_53

    .line 742
    const v0, 0x7f10049f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 743
    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_53

    .line 744
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->u:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 746
    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    .line 747
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    new-instance v1, Lcom/google/android/maps/driveabout/app/F;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/F;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setTravelModeChangedListener(Lcom/google/android/maps/driveabout/widgets/f;)V

    .line 755
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->c:Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->a(Lcom/google/android/maps/driveabout/app/P;)V

    .line 756
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->t:Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->b:[Lcom/google/android/maps/driveabout/app/P;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setVisibleTravelModes([Lcom/google/android/maps/driveabout/app/P;)V

    .line 760
    :cond_53
    const v0, 0x7f10049d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 761
    if-eqz v0, :cond_61

    .line 762
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->x:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 765
    :cond_61
    return v2

    .line 738
    :cond_62
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1b
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 620
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 621
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->m:Lcom/google/android/maps/driveabout/app/l;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/l;->b()V

    .line 625
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    if-eqz v0, :cond_18

    .line 626
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->r:Z

    .line 627
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->o:LR/b;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 630
    :cond_18
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->q:Z

    if-eqz v0, :cond_1f

    .line 631
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->e()V

    .line 633
    :cond_1f
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 803
    invoke-super {p0, p1}, Landroid/app/ListActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 804
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/DestinationActivity;->k:Lcom/google/android/maps/driveabout/app/T;

    if-ne p1, v0, :cond_1f

    .line 805
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 806
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 811
    :goto_17
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 812
    return-void

    .line 808
    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 809
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    goto :goto_17
.end method
