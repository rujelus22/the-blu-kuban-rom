.class public Lcom/google/android/maps/driveabout/vector/aK;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/ac;
.implements Lw/c;


# static fields
.field public static volatile a:Z

.field public static final b:Ljava/lang/ThreadLocal;

.field private static final f:[I

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final l:Ljava/util/Comparator;


# instance fields
.field private final A:Lcom/google/android/maps/driveabout/vector/ax;

.field private final B:Lcom/google/android/maps/driveabout/vector/n;

.field private final C:Lcom/google/android/maps/driveabout/vector/x;

.field private final D:LS/b;

.field private E:LS/c;

.field private final F:Ljava/util/HashSet;

.field private final G:Ljava/util/HashSet;

.field private final H:[I

.field private final I:Ljava/util/List;

.field private J:J

.field private K:Z

.field private final L:Lcom/google/android/maps/driveabout/vector/bb;

.field private M:Ljava/util/List;

.field private N:Z

.field private O:Landroid/graphics/Bitmap;

.field private P:Z

.field private Q:F

.field private R:J

.field private volatile S:Lcom/google/android/maps/driveabout/vector/q;

.field private final T:Ljava/util/List;

.field private U:Ljava/util/List;

.field private V:Lcom/google/android/maps/driveabout/vector/aG;

.field private volatile W:Lcom/google/android/maps/driveabout/vector/aG;

.field private volatile X:Lcom/google/android/maps/driveabout/vector/aG;

.field private volatile Y:Z

.field private Z:J

.field private aa:I

.field private volatile ab:Lcom/google/android/maps/driveabout/vector/aZ;

.field private ac:Z

.field private volatile ad:F

.field private volatile ae:Z

.field private af:Z

.field private ag:Ljava/lang/Object;

.field private ah:Z

.field private volatile ai:I

.field private aj:Z

.field private ak:I

.field private al:J

.field private am:Z

.field private an:Lz/t;

.field private final ao:Lz/k;

.field private ap:LZ/a;

.field private volatile aq:J

.field private ar:Ljava/lang/Object;

.field protected c:Ljava/util/Map;

.field protected d:Ljava/util/List;

.field protected e:Z

.field private volatile j:LG/a;

.field private volatile k:LC/b;

.field private m:LD/a;

.field private volatile n:I

.field private volatile o:I

.field private final p:Ljava/util/LinkedList;

.field private final q:Ljava/util/ArrayList;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private final t:LC/a;

.field private final u:Lcom/google/android/maps/driveabout/vector/u;

.field private final v:Lx/k;

.field private final w:Landroid/content/res/Resources;

.field private final x:F

.field private y:Ly/d;

.field private final z:Lcom/google/android/maps/driveabout/vector/ax;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x4

    .line 95
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    .line 101
    new-array v0, v1, [I

    fill-array-data v0, :array_30

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    .line 106
    new-array v0, v1, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->g:[I

    .line 111
    new-array v0, v1, [I

    fill-array-data v0, :array_48

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->h:[I

    .line 116
    new-array v0, v1, [I

    fill-array-data v0, :array_54

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->i:[I

    .line 138
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aL;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aL;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->b:Ljava/lang/ThreadLocal;

    .line 154
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aM;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aM;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aK;->l:Ljava/util/Comparator;

    return-void

    .line 101
    nop

    :array_30
    .array-data 0x4
        0x0t 0xedt 0x0t 0x0t
        0x0t 0xeat 0x0t 0x0t
        0x0t 0xe2t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 106
    :array_3c
    .array-data 0x4
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 111
    :array_48
    .array-data 0x4
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x80t 0x0t 0x0t
        0x0t 0x0t 0x1t 0x0t
    .end array-data

    .line 116
    :array_54
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/u;Landroid/content/res/Resources;LC/a;Lcom/google/android/maps/driveabout/vector/aZ;Lz/k;Lz/t;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->o:I

    .line 265
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    .line 271
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    .line 277
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    .line 282
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    .line 286
    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->J:J

    .line 294
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/aN;-><init>(Lcom/google/android/maps/driveabout/vector/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->L:Lcom/google/android/maps/driveabout/vector/bb;

    .line 350
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    .line 356
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->U:Ljava/util/List;

    .line 419
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    .line 424
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ah:Z

    .line 437
    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    .line 445
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    .line 450
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    .line 455
    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->al:J

    .line 499
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    .line 505
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    .line 507
    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    .line 782
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    .line 787
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    .line 555
    sget-object v0, LG/a;->s:LG/a;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    .line 556
    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->a:Lcom/google/android/maps/driveabout/vector/q;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    .line 557
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    .line 558
    new-instance v0, Lx/k;

    invoke-direct {v0, p0}, Lx/k;-><init>(Lcom/google/android/maps/driveabout/vector/aK;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->v:Lx/k;

    .line 559
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    .line 560
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    .line 561
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-static {v0}, LF/G;->a(F)V

    .line 562
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-static {v0}, LF/w;->a(F)V

    .line 564
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    .line 565
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    .line 566
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    .line 568
    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    .line 569
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    .line 570
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    .line 577
    new-instance v0, Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, p2}, Lcom/google/android/maps/driveabout/vector/x;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    .line 578
    new-instance v0, Lcom/google/android/maps/driveabout/vector/ax;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/ax;-><init>(ILcom/google/android/maps/driveabout/vector/x;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    .line 579
    new-instance v0, Lcom/google/android/maps/driveabout/vector/ax;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, v3, v1}, Lcom/google/android/maps/driveabout/vector/ax;-><init>(ILcom/google/android/maps/driveabout/vector/x;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    .line 580
    new-instance v0, Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->B:Lcom/google/android/maps/driveabout/vector/n;

    .line 581
    new-instance v0, LS/b;

    invoke-direct {v0}, LS/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    .line 583
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 584
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 585
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 586
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 587
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->B:Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 589
    new-instance v0, Lcom/google/android/maps/driveabout/vector/M;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/E;->d:Lcom/google/android/maps/driveabout/vector/E;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/M;-><init>(Lcom/google/android/maps/driveabout/vector/E;)V

    .line 591
    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->c:Lcom/google/android/maps/driveabout/vector/q;

    const/high16 v2, -0x8000

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/M;->a(Lcom/google/android/maps/driveabout/vector/q;I)V

    .line 592
    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 593
    new-instance v0, Lcom/google/android/maps/driveabout/vector/L;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/L;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 595
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bf;->a()Z

    move-result v0

    if-eqz v0, :cond_137

    .line 596
    new-instance v0, LS/c;

    invoke-direct {v0, p2}, LS/c;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    .line 597
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    invoke-virtual {v0, v1}, LS/b;->a(LS/a;)V

    .line 609
    :goto_120
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->L:Lcom/google/android/maps/driveabout/vector/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 612
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    .line 617
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->R:J

    .line 619
    iput-object p5, p0, Lcom/google/android/maps/driveabout/vector/aK;->ao:Lz/k;

    .line 620
    iput-object p6, p0, Lcom/google/android/maps/driveabout/vector/aK;->an:Lz/t;

    .line 630
    return-void

    .line 599
    :cond_137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    goto :goto_120
.end method

.method static a(IIF)F
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 958
    add-int v0, p0, p1

    .line 959
    int-to-float v0, v0

    const/high16 v1, 0x4380

    mul-float/2addr v1, p2

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    .line 963
    const/high16 v1, 0x3f80

    add-float/2addr v0, v1

    .line 968
    const/high16 v1, 0x4000

    invoke-static {v0}, LC/a;->a(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aK;)LS/c;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    return-object v0
.end method

.method private a(LC/a;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1889
    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    .line 1890
    invoke-virtual {p1}, LC/a;->l()I

    move-result v1

    .line 1892
    if-lez v0, :cond_2d

    if-lez v1, :cond_2d

    .line 1898
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v2

    .line 1899
    const/16 v3, 0x1701

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 1900
    invoke-interface {v2}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 1901
    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glViewport(IIII)V

    .line 1903
    invoke-virtual {p1}, LC/a;->A()[F

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 1907
    const/16 v3, 0xc11

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 1908
    invoke-interface {v2, v4, v4, v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glScissor(IIII)V

    .line 1911
    :cond_2d
    return-void
.end method

.method private a(LC/a;IZZ)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1418
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->u()Lcom/google/android/maps/driveabout/vector/aP;

    move-result-object v8

    .line 1420
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1422
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_10
    if-ge v0, v2, :cond_1a

    aget-object v3, v1, v0

    .line 1423
    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aZ;->i()V

    .line 1422
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 1428
    :cond_1a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->C()V

    .line 1430
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    .line 1431
    invoke-virtual {p1}, LC/a;->n()F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->Q:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_34

    .line 1432
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;)V

    .line 1433
    invoke-virtual {p1}, LC/a;->n()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Q:F

    .line 1435
    :cond_34
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aK;->b(LC/a;)V

    .line 1439
    const/4 v0, 0x0

    :goto_38
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_4b

    .line 1440
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1, p2}, Lcom/google/android/maps/driveabout/vector/D;->a(I)V

    .line 1439
    add-int/lit8 v0, v0, 0x1

    goto :goto_38

    .line 1447
    :cond_4b
    if-eqz p3, :cond_b3

    .line 1448
    const/4 v0, 0x0

    :goto_4e
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_63

    .line 1449
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v1, p1, v3}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    .line 1448
    add-int/lit8 v0, v0, 0x1

    goto :goto_4e

    .line 1451
    :cond_63
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1452
    sget-boolean v0, Lcom/google/android/maps/driveabout/vector/aK;->a:Z

    if-eqz v0, :cond_ac

    .line 1453
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_73
    if-ge v3, v5, :cond_ac

    aget-object v0, v4, v3

    .line 1454
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7f
    :goto_7f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/T;

    .line 1455
    invoke-interface {v0}, LF/T;->e()LF/m;

    move-result-object v1

    .line 1456
    if-nez v1, :cond_9a

    .line 1457
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v1, v0, p1}, Ly/d;->a(LF/T;LC/a;)LF/m;

    move-result-object v1

    .line 1458
    invoke-interface {v0, v1}, LF/T;->a(LF/m;)V

    :cond_9a
    move-object v0, v1

    .line 1460
    if-eqz v0, :cond_7f

    .line 1461
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0, p1, v1}, LF/m;->b(LC/a;LD/a;)Z

    .line 1462
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7f

    .line 1453
    :cond_a8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_73

    .line 1467
    :cond_ac
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->B:Lcom/google/android/maps/driveabout/vector/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->I:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/n;->a(Ljava/util/List;)V

    .line 1478
    :cond_b3
    if-eqz p4, :cond_c4

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v2, v0, :cond_c4

    sget-object v0, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v2, v0, :cond_c4

    .line 1479
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;IZ[Lcom/google/android/maps/driveabout/vector/aZ;)V

    .line 1485
    :cond_c4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    if-eqz v0, :cond_11a

    .line 1487
    monitor-enter p0

    .line 1488
    :try_start_c9
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    .line 1489
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    .line 1490
    monitor-exit p0
    :try_end_cf
    .catchall {:try_start_c9 .. :try_end_cf} :catchall_10c

    .line 1491
    if-eqz v0, :cond_11a

    .line 1492
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1493
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1494
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    const/4 v3, -0x1

    aput v3, v0, v1

    .line 1495
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    aget v6, v0, v1

    .line 1496
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->b()[Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v9

    array-length v10, v9

    const/4 v0, 0x0

    move v7, v0

    :goto_ed
    if-ge v7, v10, :cond_10f

    aget-object v0, v9, v7

    .line 1497
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/a;Lcom/google/android/maps/driveabout/vector/q;Ljava/util/HashSet;Ljava/util/HashSet;[I)V

    .line 1499
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-le v0, v6, :cond_23b

    .line 1500
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->H:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1496
    :goto_107
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v0

    goto :goto_ed

    .line 1490
    :catchall_10c
    move-exception v0

    :try_start_10d
    monitor-exit p0
    :try_end_10e
    .catchall {:try_start_10d .. :try_end_10e} :catchall_10c

    throw v0

    .line 1503
    :cond_10f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->F:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->G:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    invoke-virtual {v0, v1, v3, v6, v4}, LS/c;->a(Ljava/util/HashSet;Ljava/util/HashSet;ILcom/google/android/maps/driveabout/vector/q;)V

    .line 1508
    :cond_11a
    if-nez p3, :cond_11e

    if-eqz p2, :cond_197

    :cond_11e
    const/4 v0, 0x1

    :goto_11f
    invoke-virtual {p0, v8, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aP;Z)V

    .line 1510
    new-instance v3, Lcom/google/android/maps/driveabout/vector/aT;

    const/4 v0, 0x0

    invoke-direct {v3, v2, v0}, Lcom/google/android/maps/driveabout/vector/aT;-><init>(Lcom/google/android/maps/driveabout/vector/q;I)V

    .line 1512
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    .line 1528
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->m()V

    .line 1529
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->r()V

    .line 1533
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13b
    :goto_13b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aH;

    .line 1534
    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/aT;->a(Lcom/google/android/maps/driveabout/vector/aH;)V

    .line 1535
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    .line 1537
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v4, :cond_157

    .line 1538
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v4, v1}, Lcom/google/android/maps/driveabout/vector/aG;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1541
    :cond_157
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    sget-object v4, Lcom/google/android/maps/driveabout/vector/aI;->a:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v4, :cond_16a

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v0

    sget-object v4, LA/c;->a:LA/c;

    if-ne v0, v4, :cond_16a

    .line 1546
    :cond_16a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->A()V

    .line 1547
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v1, v0, p1, v3}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 1548
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->B()V

    .line 1553
    instance-of v0, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_18d

    .line 1554
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    if-eqz v0, :cond_199

    move-object v0, v1

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->o()Z

    move-result v0

    if-eqz v0, :cond_199

    const/4 v0, 0x1

    :goto_18b
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    .line 1558
    :cond_18d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v0, :cond_13b

    .line 1559
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aG;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_13b

    .line 1508
    :cond_197
    const/4 v0, 0x0

    goto :goto_11f

    .line 1554
    :cond_199
    const/4 v0, 0x0

    goto :goto_18b

    .line 1564
    :cond_19b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->C()V

    .line 1567
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetError()I

    move-result v0

    .line 1568
    if-eqz v0, :cond_21f

    .line 1569
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1570
    const/16 v2, 0x505

    if-ne v0, v2, :cond_1d9

    .line 1571
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->Z:J

    sub-long/2addr v2, v4

    .line 1572
    const-string v4, "\nTime in current GL context: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1573
    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v2

    invoke-virtual {v2}, LB/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1574
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    .line 1576
    :cond_1d9
    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GL Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/opengl/GLU;->gluErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LJ/a;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    const-string v2, "Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawFrameInternal GL ERROR: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    :cond_21f
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    if-eqz v1, :cond_223

    .line 1586
    :cond_223
    const/16 v1, 0x505

    if-ne v0, v1, :cond_237

    .line 1588
    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_22d
    if-ge v0, v2, :cond_237

    aget-object v3, v1, v0

    .line 1589
    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/D;->i_()V

    .line 1588
    add-int/lit8 v0, v0, 0x1

    goto :goto_22d

    .line 1592
    :cond_237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    .line 1593
    return-void

    :cond_23b
    move v0, v6

    goto/16 :goto_107
.end method

.method private a(LC/a;IZ[Lcom/google/android/maps/driveabout/vector/aZ;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1801
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v0, :cond_e

    .line 1802
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-nez p3, :cond_32

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/aG;->a(Ly/d;Z)V

    .line 1805
    :cond_e
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_34

    const/4 v0, 0x1

    .line 1807
    :goto_13
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v1, p2}, Ly/d;->a(I)V

    .line 1808
    if-eqz v0, :cond_36

    .line 1811
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {p1}, LC/a;->C()Lo/aQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ly/d;->a(Lo/aS;)V

    .line 1812
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    .line 1860
    :goto_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v0, :cond_31

    .line 1861
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aG;->a(Ly/d;)V

    .line 1863
    :cond_31
    return-void

    .line 1802
    :cond_32
    const/4 v0, 0x0

    goto :goto_b

    .line 1805
    :cond_34
    const/4 v0, 0x0

    goto :goto_13

    .line 1814
    :cond_36
    if-nez p3, :cond_44

    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    if-nez v0, :cond_44

    .line 1818
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Ly/d;->b(I)V

    goto :goto_26

    .line 1822
    :cond_44
    new-instance v5, Lcom/google/android/maps/driveabout/vector/aF;

    invoke-direct {v5}, Lcom/google/android/maps/driveabout/vector/aF;-><init>()V

    .line 1823
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1824
    invoke-virtual {p1}, LC/a;->C()Lo/aQ;

    move-result-object v2

    .line 1825
    invoke-virtual {p1}, LC/a;->q()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_80

    invoke-virtual {p1}, LC/a;->p()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_80

    const/4 v0, 0x1

    .line 1828
    :goto_65
    if-eqz v0, :cond_82

    const/4 v0, 0x0

    .line 1829
    :goto_68
    const/4 v3, 0x0

    .line 1830
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v7

    .line 1831
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v8

    .line 1833
    array-length v4, p4

    const/4 v1, 0x0

    :goto_73
    if-ge v1, v4, :cond_90

    aget-object v9, p4, v1

    .line 1834
    invoke-virtual {v9}, Lcom/google/android/maps/driveabout/vector/aZ;->j()Z

    move-result v10

    if-nez v10, :cond_84

    .line 1833
    :goto_7d
    add-int/lit8 v1, v1, 0x1

    goto :goto_73

    .line 1825
    :cond_80
    const/4 v0, 0x0

    goto :goto_65

    :cond_82
    move-object v0, v2

    .line 1828
    goto :goto_68

    .line 1837
    :cond_84
    invoke-virtual {v9, v0, v5, v6}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lo/aQ;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;)I

    move-result v10

    .line 1839
    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 1840
    invoke-virtual {v9, v7, v8}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Ljava/util/Set;Ljava/util/Map;)V

    goto :goto_7d

    .line 1843
    :cond_90
    monitor-enter p0

    .line 1844
    :try_start_91
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->M:Ljava/util/List;

    if-eqz v0, :cond_af

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1846
    :goto_9b
    monitor-exit p0
    :try_end_9c
    .catchall {:try_start_91 .. :try_end_9c} :catchall_b1

    .line 1847
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    const/16 v9, 0x14

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v10

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, Ly/d;->a(LC/a;Lo/aS;ILjava/util/Iterator;Lcom/google/android/maps/driveabout/vector/aF;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;ILA/c;)V

    .line 1857
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    goto/16 :goto_26

    .line 1844
    :cond_af
    const/4 v4, 0x0

    goto :goto_9b

    .line 1846
    :catchall_b1
    move-exception v0

    :try_start_b2
    monitor-exit p0
    :try_end_b3
    .catchall {:try_start_b2 .. :try_end_b3} :catchall_b1

    throw v0
.end method

.method private a(LD/a;)V
    .registers 5
    .parameter

    .prologue
    .line 666
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 667
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 668
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    goto :goto_9

    .line 671
    :catchall_19
    move-exception v0

    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    throw v0

    .line 670
    :cond_1c
    const/4 v0, 0x0

    :try_start_1d
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    .line 671
    monitor-exit v1
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_19

    .line 672
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/aK;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    return p1
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/q;)[I
    .registers 3
    .parameter

    .prologue
    .line 531
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aO;->a:[I

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1e

    .line 539
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->i:[I

    :goto_d
    return-object v0

    .line 532
    :pswitch_e
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    goto :goto_d

    .line 533
    :pswitch_11
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    goto :goto_d

    .line 536
    :pswitch_14
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->f:[I

    goto :goto_d

    .line 537
    :pswitch_17
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->g:[I

    goto :goto_d

    .line 538
    :pswitch_1a
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aK;->h:[I

    goto :goto_d

    .line 531
    nop

    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
        :pswitch_14
        :pswitch_17
        :pswitch_1a
    .end packed-switch
.end method

.method private b(LC/a;)V
    .registers 5
    .parameter

    .prologue
    .line 1920
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 1921
    const/16 v1, 0x1700

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 1922
    invoke-interface {v0}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 1923
    invoke-virtual {p1}, LC/a;->z()[F

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 1924
    return-void
.end method

.method private c(I)V
    .registers 10
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1322
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->c()Z

    move-result v0

    if-eqz v0, :cond_d5

    :cond_e
    move v0, v2

    .line 1325
    :goto_f
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v3}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/maps/driveabout/vector/u;->a(Z)V

    .line 1327
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->h()Z

    move-result v0

    .line 1328
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v3}, LC/a;->g()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/maps/driveabout/vector/aK;->J:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_d8

    move v3, v2

    .line 1329
    :goto_2b
    if-eqz v3, :cond_36

    .line 1330
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v0}, LC/a;->g()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->J:J

    move v0, v2

    .line 1334
    :cond_36
    invoke-direct {p0, v3}, Lcom/google/android/maps/driveabout/vector/aK;->d(Z)V

    .line 1336
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v3}, LD/a;->e()V

    .line 1339
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->v()V

    .line 1342
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->W:Lcom/google/android/maps/driveabout/vector/aG;

    iput-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    .line 1343
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v3, :cond_53

    .line 1344
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3, p0}, Lcom/google/android/maps/driveabout/vector/aG;->a(Lcom/google/android/maps/driveabout/vector/aK;)V

    .line 1345
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aG;->a()V

    .line 1348
    :cond_53
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->X:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v3, :cond_5c

    .line 1349
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->X:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/aG;->a()V

    .line 1355
    :cond_5c
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v3}, LC/a;->o()F

    move-result v3

    const/high16 v4, 0x3f80

    cmpl-float v3, v3, v4

    if-lez v3, :cond_72

    .line 1357
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-direct {p0, v3, p1, v0, v2}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;IZZ)V

    .line 1358
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v3}, LD/a;->f()V

    .line 1361
    :cond_72
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v3}, Lcom/google/android/maps/driveabout/vector/u;->i()Z

    move-result v3

    if-eqz v3, :cond_db

    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_db

    move v3, v2

    .line 1363
    :goto_7f
    iget-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->am:Z

    if-eqz v4, :cond_dd

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v4}, Ly/d;->c()Z

    move-result v4

    if-nez v4, :cond_dd

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v4}, LD/a;->c()Z

    move-result v4

    if-nez v4, :cond_dd

    if-nez v3, :cond_dd

    .line 1368
    :goto_95
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    if-eqz v3, :cond_ab

    .line 1369
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v3, v2}, Lcom/google/android/maps/driveabout/vector/aG;->b(Z)V

    .line 1370
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->V:Lcom/google/android/maps/driveabout/vector/aG;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aG;->b()Z

    move-result v2

    if-eqz v2, :cond_ab

    .line 1371
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v2, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1376
    :cond_ab
    monitor-enter p0

    .line 1377
    :try_start_ac
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->N:Z

    .line 1378
    monitor-exit p0
    :try_end_af
    .catchall {:try_start_ac .. :try_end_af} :catchall_df

    .line 1380
    if-eqz v2, :cond_bf

    .line 1381
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->t()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1382
    monitor-enter p0

    .line 1383
    :try_start_b6
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;

    .line 1384
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->N:Z

    .line 1385
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1386
    monitor-exit p0
    :try_end_bf
    .catchall {:try_start_b6 .. :try_end_bf} :catchall_e2

    .line 1389
    :cond_bf
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v2}, Ly/d;->c()Z

    move-result v2

    if-nez v2, :cond_cf

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->c()Z

    move-result v2

    if-eqz v2, :cond_e5

    .line 1391
    :cond_cf
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1409
    :cond_d4
    :goto_d4
    return-void

    :cond_d5
    move v0, v1

    .line 1322
    goto/16 :goto_f

    :cond_d8
    move v3, v1

    .line 1328
    goto/16 :goto_2b

    :cond_db
    move v3, v1

    .line 1361
    goto :goto_7f

    :cond_dd
    move v2, v1

    .line 1363
    goto :goto_95

    .line 1378
    :catchall_df
    move-exception v0

    :try_start_e0
    monitor-exit p0
    :try_end_e1
    .catchall {:try_start_e0 .. :try_end_e1} :catchall_df

    throw v0

    .line 1386
    :catchall_e2
    move-exception v0

    :try_start_e3
    monitor-exit p0
    :try_end_e4
    .catchall {:try_start_e3 .. :try_end_e4} :catchall_e2

    throw v0

    .line 1395
    :cond_e5
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x9

    if-ge v2, v3, :cond_102

    if-nez v0, :cond_102

    if-nez p1, :cond_102

    .line 1400
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 1401
    iget-wide v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->R:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x2710

    cmp-long v0, v4, v6

    if-lez v0, :cond_102

    .line 1402
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 1403
    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->R:J

    .line 1406
    :cond_102
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_d4

    .line 1407
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    goto :goto_d4
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 7
    .parameter

    .prologue
    .line 1654
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 1655
    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v0, :cond_5a

    move-object v0, p1

    .line 1656
    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1657
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/driveabout/vector/D;

    .line 1658
    instance-of v3, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v3, :cond_13

    .line 1659
    check-cast v1, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1660
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->r()LB/e;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->r()LB/e;

    move-result-object v4

    if-ne v3, v4, :cond_13

    .line 1662
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The added tile Overlay "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " shares the same GLTileCache with "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->m()LA/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1674
    :cond_5a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    .line 1675
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1676
    return-void
.end method

.method private c(Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 1867
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 1868
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/q;)[I

    move-result-object v0

    .line 1869
    aget v2, v0, v6

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x2

    aget v4, v0, v4

    const/4 v5, 0x3

    aget v0, v0, v5

    invoke-interface {v1, v2, v3, v4, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClearColorx(IIII)V

    .line 1870
    const/16 v0, 0x4000

    .line 1872
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->g()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 1873
    const/16 v0, 0x4100

    .line 1874
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->h()V

    .line 1877
    :cond_2a
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->i()Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 1878
    invoke-interface {v1, v6}, Ljavax/microedition/khronos/opengles/GL10;->glClearStencil(I)V

    .line 1879
    or-int/lit16 v0, v0, 0x400

    .line 1880
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2}, LD/a;->j()V

    .line 1883
    :cond_3c
    invoke-interface {v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glClear(I)V

    .line 1886
    return-void
.end method

.method private declared-synchronized d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 1603
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aQ;

    .line 1604
    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/vector/aQ;->a(Z)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_17

    goto :goto_7

    .line 1603
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1606
    :cond_1a
    monitor-exit p0

    return-void
.end method

.method private r()V
    .registers 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_9

    .line 676
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0}, Ly/d;->a()V

    .line 678
    :cond_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-eqz v0, :cond_25

    .line 679
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->G()Lx/a;

    move-result-object v0

    invoke-virtual {v0}, Lx/a;->a()V

    .line 680
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-static {v0}, Lx/o;->a(LD/a;)V

    .line 681
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->C()V

    .line 682
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->D()V

    .line 684
    :cond_25
    return-void
.end method

.method private s()V
    .registers 5

    .prologue
    .line 1007
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->n:I

    if-gez v0, :cond_5

    .line 1017
    :goto_4
    return-void

    .line 1010
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ac:Z

    if-eqz v0, :cond_2b

    const/16 v0, 0xa

    .line 1012
    :goto_b
    :try_start_b
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->n:I

    invoke-static {v1, v0}, Landroid/os/Process;->setThreadPriority(II)V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_10} :catch_11

    goto :goto_4

    .line 1014
    :catch_11
    move-exception v0

    .line 1015
    const-string v1, "Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1010
    :cond_2b
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->o:I

    goto :goto_b
.end method

.method private t()Landroid/graphics/Bitmap;
    .registers 15

    .prologue
    const/4 v1, 0x0

    .line 1634
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 1635
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v2}, LC/a;->k()I

    move-result v3

    .line 1636
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v2}, LC/a;->l()I

    move-result v4

    .line 1637
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->v:Lx/k;

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3, v4, v5}, Lx/k;->a(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 1639
    invoke-static {v13}, Landroid/opengl/GLUtils;->getInternalFormat(Landroid/graphics/Bitmap;)I

    move-result v5

    .line 1640
    invoke-static {v13}, Landroid/opengl/GLUtils;->getType(Landroid/graphics/Bitmap;)I

    move-result v6

    .line 1641
    mul-int v2, v3, v4

    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v7

    move v2, v1

    .line 1642
    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 1643
    invoke-virtual {v7}, Ljava/nio/IntBuffer;->array()[I

    move-result-object v6

    move-object v5, v13

    move v7, v1

    move v8, v3

    move v9, v1

    move v10, v1

    move v11, v3

    move v12, v4

    invoke-virtual/range {v5 .. v12}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 1644
    return-object v13
.end method

.method private u()Lcom/google/android/maps/driveabout/vector/aP;
    .registers 10

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 1696
    iget-object v7, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v7

    .line 1697
    :try_start_6
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    if-nez v0, :cond_2a

    .line 1700
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 1701
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v4, v6}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V

    goto :goto_10

    .line 1792
    :catchall_24
    move-exception v0

    monitor-exit v7
    :try_end_26
    .catchall {:try_start_6 .. :try_end_26} :catchall_24

    throw v0

    .line 1703
    :cond_27
    const/4 v0, 0x1

    :try_start_28
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aj:Z

    :cond_2a
    move v6, v1

    move v2, v1

    move v4, v1

    .line 1709
    :goto_2d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_110

    .line 1710
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    .line 1711
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aR;

    .line 1712
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aO;->b:[I

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->b:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-virtual {v8}, Lcom/google/android/maps/driveabout/vector/aS;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_178

    :cond_4d
    :goto_4d
    move v0, v2

    move v1, v4

    .line 1709
    :goto_4f
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v4, v1

    move v2, v0

    goto :goto_2d

    .line 1714
    :pswitch_55
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4d

    .line 1717
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    .line 1718
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v1, :cond_175

    .line 1719
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    check-cast v1, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1720
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->L:Lcom/google/android/maps/driveabout/vector/bb;

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aZ;->a(Lcom/google/android/maps/driveabout/vector/bb;)V

    .line 1721
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1722
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v4

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    if-ge v4, v8, :cond_a2

    move v4, v3

    .line 1731
    :goto_86
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aZ;->k()Z

    move-result v2

    if-eqz v2, :cond_a8

    .line 1732
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_92
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LA/c;

    .line 1733
    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;)V

    goto :goto_92

    .line 1727
    :cond_a2
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    move v4, v2

    goto :goto_86

    :cond_a8
    move v1, v4

    .line 1737
    :goto_a9
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-direct {p0, v2}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1738
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/D;->z()Lcom/google/android/maps/driveabout/vector/b;

    move-result-object v2

    .line 1739
    if-eqz v2, :cond_b9

    .line 1740
    invoke-virtual {p0, v2}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/b;)V

    .line 1742
    :cond_b9
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v2, v4}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V

    move v0, v1

    move v1, v3

    .line 1744
    goto :goto_4f

    .line 1747
    :pswitch_c5
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v1, v8}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 1748
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/aZ;

    if-eqz v1, :cond_172

    .line 1749
    iget-object v1, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/D;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v1

    iget v8, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    if-ne v1, v8, :cond_16f

    move v1, v3

    .line 1754
    :goto_e4
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1756
    :goto_eb
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    iget-object v8, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-interface {v2, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1757
    iget-object v2, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    iget-object v8, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v2, v8}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    .line 1758
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/D;->z()Lcom/google/android/maps/driveabout/vector/b;

    move-result-object v0

    .line 1759
    if-eqz v0, :cond_104

    .line 1760
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/b;)V

    :cond_104
    move v0, v1

    move v1, v4

    .line 1762
    goto/16 :goto_4f

    .line 1765
    :pswitch_108
    iget-object v0, v0, Lcom/google/android/maps/driveabout/vector/aR;->a:Lcom/google/android/maps/driveabout/vector/D;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    goto/16 :goto_4d

    .line 1769
    :cond_110
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1771
    if-eqz v2, :cond_151

    .line 1774
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    .line 1776
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v5

    :goto_123
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1777
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    .line 1778
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v5

    iget v6, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    if-ge v5, v6, :cond_16d

    .line 1779
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->p()Lcom/google/android/maps/driveabout/vector/E;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/E;->a()I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ak:I

    :goto_149
    move-object v1, v0

    .line 1780
    goto :goto_123

    .line 1783
    :cond_14b
    if-eqz v1, :cond_151

    .line 1784
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Z)V

    .line 1788
    :cond_151
    if-eqz v4, :cond_162

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v3, :cond_162

    .line 1789
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aK;->l:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1791
    :cond_162
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aP;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aP;-><init>(Ljava/util/List;Ljava/util/List;)V

    monitor-exit v7
    :try_end_16c
    .catchall {:try_start_28 .. :try_end_16c} :catchall_24

    return-object v0

    :cond_16d
    move-object v0, v1

    goto :goto_149

    :cond_16f
    move v1, v2

    goto/16 :goto_e4

    :cond_172
    move v1, v2

    goto/16 :goto_eb

    :cond_175
    move v1, v2

    goto/16 :goto_a9

    .line 1712
    :pswitch_data_178
    .packed-switch 0x1
        :pswitch_55
        :pswitch_c5
        :pswitch_108
    .end packed-switch
.end method

.method private v()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 1990
    monitor-enter p0

    .line 1991
    :try_start_2
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->aa:I

    .line 1992
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aa:I

    .line 1993
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_2 .. :try_end_8} :catchall_30

    .line 1995
    if-eqz v1, :cond_36

    .line 1996
    const/4 v2, 0x2

    if-ne v1, v2, :cond_33

    const/4 v0, 0x1

    move v1, v0

    .line 2000
    :goto_f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Ly/d;->a(Z)V

    .line 2003
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v2

    .line 2004
    :try_start_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/D;

    .line 2005
    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/D;->a(Z)V

    goto :goto_1d

    .line 2007
    :catchall_2d
    move-exception v0

    monitor-exit v2
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_2d

    throw v0

    .line 1993
    :catchall_30
    move-exception v0

    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_30

    throw v0

    :cond_33
    move v1, v0

    .line 1996
    goto :goto_f

    .line 2007
    :cond_35
    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_2d

    .line 2009
    :cond_36
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 1213
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ad:F

    return v0
.end method

.method public a(Lo/T;)F
    .registers 6
    .parameter

    .prologue
    .line 1199
    const/high16 v0, 0x41a8

    .line 1200
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v2

    .line 1201
    :try_start_5
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1202
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->b(Lo/T;)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v1, v0

    goto :goto_c

    .line 1204
    :cond_22
    monitor-exit v2

    .line 1205
    return v1

    .line 1204
    :catchall_24
    move-exception v0

    monitor-exit v2
    :try_end_26
    .catchall {:try_start_5 .. :try_end_26} :catchall_24

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)Lcom/google/android/maps/driveabout/vector/A;
    .registers 4
    .parameter

    .prologue
    .line 1060
    new-instance v0, Lcom/google/android/maps/driveabout/vector/A;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, p1, v1}, Lcom/google/android/maps/driveabout/vector/A;-><init>(Lcom/google/android/maps/driveabout/vector/E;Lcom/google/android/maps/driveabout/vector/x;)V

    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 973
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->o:I

    .line 974
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->s()V

    .line 975
    return-void
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1965
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    if-eqz v0, :cond_9

    .line 1966
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->E:LS/c;

    invoke-virtual {v0, p1, p2}, LS/c;->a(II)V

    .line 1968
    :cond_9
    return-void
.end method

.method public a(LA/c;)V
    .registers 6
    .parameter

    .prologue
    .line 1088
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1090
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1091
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->k()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1092
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LA/c;)V

    goto :goto_9

    .line 1098
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    throw v0

    .line 1097
    :cond_22
    :try_start_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1098
    monitor-exit v1
    :try_end_28
    .catchall {:try_start_22 .. :try_end_28} :catchall_1f

    .line 1099
    return-void
.end method

.method public a(LG/a;)V
    .registers 5
    .parameter

    .prologue
    .line 1951
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    if-eq p1, v0, :cond_16

    .line 1952
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    .line 1953
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_16

    .line 1954
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, p1}, Ly/d;->a(LG/a;)V

    .line 1955
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1958
    :cond_16
    return-void
.end method

.method public a(LZ/a;)V
    .registers 3
    .parameter

    .prologue
    .line 2104
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ap:LZ/a;

    .line 2105
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_b

    .line 2106
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, p1}, Ly/d;->a(LZ/a;)V

    .line 2108
    :cond_b
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 5
    .parameter

    .prologue
    .line 1125
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1127
    :try_start_3
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->a:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1128
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1129
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_18

    .line 1130
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1131
    return-void

    .line 1129
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method a(Lcom/google/android/maps/driveabout/vector/aG;)V
    .registers 2
    .parameter

    .prologue
    .line 1043
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->W:Lcom/google/android/maps/driveabout/vector/aG;

    .line 1044
    return-void
.end method

.method a(Lcom/google/android/maps/driveabout/vector/aP;Z)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1266
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v5

    .line 1267
    :try_start_5
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    if-nez v0, :cond_d

    if-nez p2, :cond_d

    .line 1268
    monitor-exit v5

    .line 1316
    :goto_c
    return-void

    .line 1270
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    .line 1271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->e:Z

    .line 1272
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v6

    array-length v7, v6

    move v4, v1

    :goto_18
    if-ge v4, v7, :cond_39

    aget-object v8, v6, v4

    .line 1273
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1274
    if-nez v0, :cond_30

    .line 1275
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1276
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-interface {v2, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    .line 1279
    :cond_30
    invoke-virtual {v8, v0}, Lcom/google/android/maps/driveabout/vector/D;->b(Ljava/util/List;)Z

    move-result v0

    or-int/2addr v2, v0

    .line 1272
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_18

    .line 1281
    :cond_39
    if-eqz v2, :cond_c9

    .line 1282
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1286
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/aP;->a()[Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v2

    array-length v3, v2

    :goto_45
    if-ge v1, v3, :cond_5c

    aget-object v0, v2, v1

    .line 1287
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->c:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1288
    if-eqz v0, :cond_58

    .line 1289
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1286
    :cond_58
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_45

    .line 1292
    :cond_5c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1296
    invoke-static {}, Lcom/google/common/collect/dA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 1297
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aH;

    .line 1298
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_90

    .line 1299
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->a(Z)V

    .line 1300
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_6b

    .line 1315
    :catchall_8d
    move-exception v0

    monitor-exit v5
    :try_end_8f
    .catchall {:try_start_5 .. :try_end_8f} :catchall_8d

    throw v0

    .line 1302
    :cond_90
    const/4 v3, 0x0

    :try_start_91
    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->a(Z)V

    goto :goto_6b

    .line 1305
    :cond_95
    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 1306
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->d:Ljava/util/List;

    invoke-static {v0}, Lcom/google/common/collect/bx;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aH;

    .line 1307
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c4

    .line 1308
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->b(Z)V

    .line 1309
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->a()Lcom/google/android/maps/driveabout/vector/D;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_a2

    .line 1311
    :cond_c4
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/aH;->b(Z)V

    goto :goto_a2

    .line 1315
    :cond_c9
    monitor-exit v5
    :try_end_ca
    .catchall {:try_start_91 .. :try_end_ca} :catchall_8d

    goto/16 :goto_c
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aZ;)V
    .registers 6
    .parameter

    .prologue
    .line 2179
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 2181
    :try_start_3
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->a:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    .line 2182
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2184
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->c:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    .line 2185
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2187
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->b:Lcom/google/android/maps/driveabout/vector/aS;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-direct {v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    .line 2188
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2189
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_3b

    .line 2190
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_33

    .line 2191
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0}, Ly/d;->b()V

    .line 2193
    :cond_33
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 2194
    return-void

    .line 2189
    :catchall_3b
    move-exception v0

    :try_start_3c
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_3c .. :try_end_3d} :catchall_3b

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/b;)V
    .registers 3
    .parameter

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1188
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2016
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/x;->a(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/f;)V

    .line 2017
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/e;)V
    .registers 3
    .parameter

    .prologue
    .line 2097
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/ax;->a(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 2098
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/y;)V
    .registers 3
    .parameter

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/x;->a(Lcom/google/android/maps/driveabout/vector/y;)V

    .line 1080
    return-void
.end method

.method public a(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 1168
    if-eqz p1, :cond_1c

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1170
    :goto_7
    monitor-enter p0

    .line 1171
    :try_start_8
    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->M:Ljava/util/List;

    .line 1172
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_1e

    .line 1173
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_21

    .line 1174
    :cond_13
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->b(Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1178
    :goto_18
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aK;->f()V

    .line 1179
    return-void

    .line 1168
    :cond_1c
    const/4 v0, 0x0

    goto :goto_7

    .line 1172
    :catchall_1e
    move-exception v0

    :try_start_1f
    monitor-exit p0
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1e

    throw v0

    .line 1176
    :cond_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/D;)V

    goto :goto_18
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 9
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 828
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_27

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lx/e;

    if-nez v0, :cond_27

    .line 829
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 831
    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 832
    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 837
    :cond_27
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->J()Z

    move-result v1

    invoke-virtual {v0, v1}, LD/a;->a(Z)V

    .line 840
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(Lcom/google/android/maps/driveabout/vector/q;)V

    .line 844
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    if-lez v0, :cond_49

    .line 845
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    .line 851
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 917
    :goto_48
    return-void

    .line 859
    :cond_49
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ae:Z

    if-eqz v0, :cond_65

    .line 860
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    monitor-enter v1

    .line 861
    const/4 v0, 0x0

    :try_start_51
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ae:Z

    .line 862
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 863
    :goto_58
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->af:Z
    :try_end_5a
    .catchall {:try_start_51 .. :try_end_5a} :catchall_a4

    if-nez v0, :cond_64

    .line 866
    :try_start_5c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_61
    .catchall {:try_start_5c .. :try_end_61} :catchall_a4
    .catch Ljava/lang/InterruptedException; {:try_start_5c .. :try_end_61} :catch_62

    goto :goto_58

    .line 867
    :catch_62
    move-exception v0

    goto :goto_58

    .line 871
    :cond_64
    :try_start_64
    monitor-exit v1
    :try_end_65
    .catchall {:try_start_64 .. :try_end_65} :catchall_a4

    .line 876
    :cond_65
    iget-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_82

    .line 877
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    monitor-enter v1

    .line 878
    :try_start_6e
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_7c

    .line 879
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    .line 881
    :cond_7c
    monitor-exit v1
    :try_end_7d
    .catchall {:try_start_6e .. :try_end_7d} :catchall_a7

    .line 882
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->e()V

    .line 892
    :cond_82
    invoke-static {}, LR/o;->e()Z

    move-result v0

    if-nez v0, :cond_aa

    .line 895
    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 896
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 897
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->al:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-lez v2, :cond_9e

    .line 900
    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->al:J

    .line 903
    :cond_9e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    goto :goto_48

    .line 871
    :catchall_a4
    move-exception v0

    :try_start_a5
    monitor-exit v1
    :try_end_a6
    .catchall {:try_start_a5 .. :try_end_a6} :catchall_a4

    throw v0

    .line 881
    :catchall_a7
    move-exception v0

    :try_start_a8
    monitor-exit v1
    :try_end_a9
    .catchall {:try_start_a8 .. :try_end_a9} :catchall_a7

    throw v0

    .line 909
    :cond_aa
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aK;->h()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->c(I)V

    goto :goto_48
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 752
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_23

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    instance-of v0, v0, Lx/e;

    if-nez v0, :cond_23

    .line 753
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GL object has changed since onSurfaceCreated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 755
    const-string v1, "DA:Renderer"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 756
    const-string v1, "OpenGL error during initialization."

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 758
    :cond_23
    if-lez p2, :cond_27

    if-gtz p3, :cond_28

    .line 774
    :cond_27
    :goto_27
    return-void

    .line 762
    :cond_28
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-virtual {v0, p2, p3, v1}, LC/a;->a(IIF)V

    .line 767
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(LC/a;)V

    .line 768
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-virtual {v0}, LC/a;->n()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Q:F

    .line 770
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->x:F

    invoke-static {p2, p3, v0}, Lcom/google/android/maps/driveabout/vector/aK;->a(IIF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ad:F

    .line 773
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    goto :goto_27
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 688
    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    .line 689
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->n:I

    .line 690
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->s()V

    .line 692
    const/16 v0, 0x1f01

    invoke-interface {p1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/android/E;->a(Ljava/lang/String;)V

    .line 694
    invoke-direct {p0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(LD/a;)V

    .line 704
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    if-eq v0, p1, :cond_2d

    .line 705
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->r()V

    .line 706
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    .line 709
    :cond_2d
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-nez v0, :cond_85

    .line 710
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Z:J

    .line 711
    new-instance v0, LD/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->v:Lx/k;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->ao:Lz/k;

    invoke-direct {v0, p1, v1, v2, v3}, LD/a;-><init>(Ljavax/microedition/khronos/opengles/GL10;Lx/k;Lcom/google/android/maps/driveabout/vector/u;Lz/k;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    .line 713
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ac:Z

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/u;->b(Z)V

    .line 717
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-static {v0, v1}, Lx/o;->a(Landroid/content/res/Resources;LD/a;)V

    .line 719
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->F()I

    move-result v0

    invoke-static {v0}, LF/G;->a(I)V

    .line 721
    new-instance v0, Ly/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->j:LG/a;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, Ly/d;-><init>(LG/a;LD/a;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    .line 722
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ap:LZ/a;

    if-eqz v0, :cond_77

    .line 723
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ap:LZ/a;

    invoke-virtual {v0, v1}, Ly/d;->a(LZ/a;)V

    .line 725
    :cond_77
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/ax;->a(Ly/d;)V

    .line 726
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->A:Lcom/google/android/maps/driveabout/vector/ax;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/ax;->a(Ly/d;)V

    .line 735
    :cond_85
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/u;->c(Z)V

    .line 737
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->P:Z

    .line 741
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ah:Z

    if-eqz v0, :cond_93

    .line 742
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ai:I

    .line 744
    :cond_93
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aK;->ah:Z

    .line 747
    const-string v0, "Renderer.onSurfaceCreated"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V

    .line 748
    return-void
.end method

.method declared-synchronized a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 995
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ac:Z

    .line 996
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    if-eqz v0, :cond_1b

    .line 997
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/u;->b(Z)V

    .line 999
    if-nez p1, :cond_1b

    .line 1000
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    invoke-virtual {v0}, LD/a;->z()Lcom/google/android/maps/driveabout/vector/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->c()V

    .line 1003
    :cond_1b
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aK;->s()V
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_20

    .line 1004
    monitor-exit p0

    return-void

    .line 995
    :catchall_20
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)Lcom/google/android/maps/driveabout/vector/aA;
    .registers 5
    .parameter

    .prologue
    .line 1073
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aA;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->w:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/maps/driveabout/vector/aA;-><init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/x;Z)V

    return-object v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 655
    invoke-static {}, LB/a;->a()LB/a;

    move-result-object v0

    .line 656
    if-eqz v0, :cond_6

    .line 663
    :cond_6
    return-void
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 2115
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->z:Lcom/google/android/maps/driveabout/vector/ax;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/ax;->b(I)V

    .line 2116
    return-void
.end method

.method public b(LA/c;)V
    .registers 6
    .parameter

    .prologue
    .line 1107
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1109
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aZ;

    .line 1110
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->k()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1111
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aZ;->l()Lu/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lu/d;->b(LA/c;)V

    goto :goto_9

    .line 1116
    :catchall_23
    move-exception v0

    monitor-exit v1
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_23

    throw v0

    .line 1115
    :cond_26
    :try_start_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1116
    monitor-exit v1
    :try_end_2c
    .catchall {:try_start_26 .. :try_end_2c} :catchall_23

    .line 1117
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 5
    .parameter

    .prologue
    .line 1139
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1141
    :try_start_3
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aR;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aS;->b:Lcom/google/android/maps/driveabout/vector/aS;

    invoke-direct {v0, v2, p1}, Lcom/google/android/maps/driveabout/vector/aR;-><init>(Lcom/google/android/maps/driveabout/vector/aS;Lcom/google/android/maps/driveabout/vector/D;)V

    .line 1142
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1143
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_18

    .line 1144
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1145
    return-void

    .line 1143
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/b;)V
    .registers 3
    .parameter

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1195
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/vector/q;)V
    .registers 3
    .parameter

    .prologue
    .line 1931
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    if-eq p1, v0, :cond_e

    .line 1932
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    .line 1933
    monitor-enter p0

    .line 1934
    const/4 v0, 0x1

    :try_start_8
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->K:Z

    .line 1935
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_f

    .line 1936
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aK;->f()V

    .line 1938
    :cond_e
    return-void

    .line 1935
    :catchall_f
    move-exception v0

    :try_start_10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_10 .. :try_end_11} :catchall_f

    throw v0
.end method

.method public c()V
    .registers 7

    .prologue
    .line 796
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    monitor-enter v1

    .line 797
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x4e20

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    .line 799
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_13

    .line 801
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/u;->e()V

    .line 802
    return-void

    .line 799
    :catchall_13
    move-exception v0

    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    throw v0
.end method

.method public c(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1978
    monitor-enter p0

    .line 1979
    if-eqz p1, :cond_e

    const/4 v0, 0x2

    :goto_5
    :try_start_5
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->aa:I

    .line 1981
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_10

    .line 1985
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1986
    return-void

    .line 1979
    :cond_e
    const/4 v0, 0x1

    goto :goto_5

    .line 1981
    :catchall_10
    move-exception v0

    :try_start_11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_11 .. :try_end_12} :catchall_10

    throw v0
.end method

.method public d()V
    .registers 7

    .prologue
    .line 812
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->ar:Ljava/lang/Object;

    monitor-enter v1

    .line 813
    :try_start_3
    iget-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_14

    .line 814
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x7d0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->aq:J

    .line 818
    :cond_14
    monitor-exit v1

    .line 819
    return-void

    .line 818
    :catchall_16
    move-exception v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_16

    throw v0
.end method

.method public declared-synchronized e()V
    .registers 3

    .prologue
    .line 1024
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/u;->c(Z)V
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 1025
    monitor-exit p0

    return-void

    .line 1024
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .registers 4

    .prologue
    .line 1031
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    if-eqz v0, :cond_10

    .line 1032
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->y:Ly/d;

    invoke-virtual {v0}, Ly/d;->b()V

    .line 1033
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1035
    :cond_10
    return-void
.end method

.method public g()Ljava/util/ArrayList;
    .registers 4

    .prologue
    .line 1153
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 1155
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    monitor-enter v1

    .line 1156
    :try_start_e
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->p:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1157
    monitor-exit v1

    .line 1158
    return-object v0

    .line 1157
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_15

    throw v0
.end method

.method public h()I
    .registers 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1227
    .line 1229
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->T:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v2

    move v3, v4

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/b;

    .line 1230
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    invoke-interface {v0, v6}, Lcom/google/android/maps/driveabout/vector/b;->a_(LC/a;)I

    move-result v6

    .line 1231
    if-eqz v6, :cond_56

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/b;->c()LC/b;

    move-result-object v7

    if-eqz v7, :cond_56

    .line 1232
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/b;->c()LC/b;

    move-result-object v0

    .line 1235
    :goto_28
    or-int v1, v3, v6

    move v3, v1

    move-object v1, v0

    .line 1236
    goto :goto_a

    .line 1238
    :cond_2d
    if-eqz v3, :cond_4e

    .line 1240
    if-eqz v1, :cond_40

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->k:LC/b;

    invoke-virtual {v1, v0}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_40

    .line 1242
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/b;)V

    .line 1243
    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->k:LC/b;

    .line 1247
    :cond_40
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->u:Lcom/google/android/maps/driveabout/vector/u;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/maps/driveabout/vector/u;->a(ZZ)V

    .line 1255
    :goto_45
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->t:LC/a;

    if-eqz v3, :cond_4a

    const/4 v4, 0x1

    :cond_4a
    invoke-virtual {v0, v4}, LC/a;->a(Z)V

    .line 1256
    return v3

    .line 1249
    :cond_4e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/aZ;->a(LC/b;)V

    .line 1250
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/aK;->k:LC/b;

    goto :goto_45

    :cond_56
    move-object v0, v1

    goto :goto_28
.end method

.method public declared-synchronized i()Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 1616
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->N:Z

    .line 1617
    :goto_4
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;
    :try_end_6
    .catchall {:try_start_2 .. :try_end_6} :catchall_15

    if-nez v0, :cond_e

    .line 1619
    :try_start_8
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_15
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_b} :catch_c

    goto :goto_4

    .line 1620
    :catch_c
    move-exception v0

    goto :goto_4

    .line 1624
    :cond_e
    :try_start_e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;

    .line 1625
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/aK;->O:Landroid/graphics/Bitmap;
    :try_end_13
    .catchall {:try_start_e .. :try_end_13} :catchall_15

    .line 1626
    monitor-exit p0

    return-object v0

    .line 1616
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/q;
    .registers 2

    .prologue
    .line 1944
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->S:Lcom/google/android/maps/driveabout/vector/q;

    return-object v0
.end method

.method public k()V
    .registers 2

    .prologue
    .line 2024
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->f()V

    .line 2025
    return-void
.end method

.method public l()LF/H;
    .registers 3

    .prologue
    .line 2032
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/x;->e()Lcom/google/android/maps/driveabout/vector/c;

    move-result-object v0

    .line 2033
    instance-of v1, v0, LF/H;

    if-eqz v1, :cond_d

    .line 2034
    check-cast v0, LF/H;

    .line 2036
    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public m()Lcom/google/android/maps/driveabout/vector/x;
    .registers 2

    .prologue
    .line 2041
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->C:Lcom/google/android/maps/driveabout/vector/x;

    return-object v0
.end method

.method public n()LS/b;
    .registers 2

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->D:LS/b;

    return-object v0
.end method

.method public o()Lcom/google/android/maps/driveabout/vector/aZ;
    .registers 2

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->ab:Lcom/google/android/maps/driveabout/vector/aZ;

    return-object v0
.end method

.method public p()LD/a;
    .registers 2

    .prologue
    .line 2064
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->m:LD/a;

    return-object v0
.end method

.method public q()V
    .registers 2

    .prologue
    .line 2132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aK;->Y:Z

    .line 2133
    return-void
.end method
