.class public abstract Lcom/google/android/maps/driveabout/vector/aG;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/android/maps/driveabout/vector/aK;

.field private c:Z


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method


# virtual methods
.method protected a()V
    .registers 1

    .prologue
    .line 64
    return-void
.end method

.method protected a(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 2
    .parameter

    .prologue
    .line 101
    return-void
.end method

.method final a(Lcom/google/android/maps/driveabout/vector/aK;)V
    .registers 2
    .parameter

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aG;->b:Lcom/google/android/maps/driveabout/vector/aK;

    .line 128
    return-void
.end method

.method protected a(Ly/d;)V
    .registers 2
    .parameter

    .prologue
    .line 95
    return-void
.end method

.method protected a(Ly/d;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 89
    return-void
.end method

.method protected a(Z)Z
    .registers 3
    .parameter

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method protected b(Lcom/google/android/maps/driveabout/vector/D;)V
    .registers 2
    .parameter

    .prologue
    .line 107
    return-void
.end method

.method final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/aG;->a(Z)Z

    move-result v0

    .line 114
    if-eqz v0, :cond_14

    .line 115
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aG;->b:Lcom/google/android/maps/driveabout/vector/aK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/aK;->a(Lcom/google/android/maps/driveabout/vector/aG;)V

    .line 116
    monitor-enter p0

    .line 117
    const/4 v0, 0x1

    :try_start_e
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aG;->a:Z

    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 119
    monitor-exit p0

    .line 121
    :cond_14
    return-void

    .line 119
    :catchall_15
    move-exception v0

    monitor-exit p0
    :try_end_17
    .catchall {:try_start_e .. :try_end_17} :catchall_15

    throw v0
.end method

.method final b()Z
    .registers 3

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aG;->c:Z

    .line 144
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/aG;->c:Z

    .line 145
    return v0
.end method
