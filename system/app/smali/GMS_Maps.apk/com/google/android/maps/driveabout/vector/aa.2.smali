.class public Lcom/google/android/maps/driveabout/vector/aA;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;
.implements Lcom/google/android/maps/driveabout/vector/c;


# static fields
.field private static G:LE/a;

.field private static final f:F


# instance fields
.field private A:F

.field private B:Lcom/google/android/maps/driveabout/vector/E;

.field private final C:I

.field private D:F

.field private E:Z

.field private F:Lcom/google/android/maps/driveabout/vector/aB;

.field protected volatile d:Lcom/google/android/maps/driveabout/vector/aU;

.field public e:Ljava/util/List;

.field private g:F

.field private h:F

.field private i:F

.field private final j:Landroid/content/res/Resources;

.field private final k:Z

.field private l:I

.field private final m:Ljava/util/Map;

.field private n:Lh/l;

.field private o:Lcom/google/android/maps/driveabout/vector/a;

.field private final p:Lo/S;

.field private final q:Lo/S;

.field private final r:Lo/S;

.field private s:Z

.field private t:F

.field private u:F

.field private v:I

.field private w:Z

.field private volatile x:Lo/D;

.field private volatile y:Z

.field private z:F


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x4

    .line 72
    const/high16 v0, 0x4000

    sput v0, Lcom/google/android/maps/driveabout/vector/aA;->f:F

    .line 217
    new-instance v0, LE/a;

    invoke-direct {v0, v2}, LE/a;-><init>(I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/aA;->G:LE/a;

    .line 220
    sget-object v0, Lcom/google/android/maps/driveabout/vector/aA;->G:LE/a;

    const v1, 0x73217bce

    invoke-virtual {v0, v1, v2}, LE/a;->b(II)V

    .line 221
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/maps/driveabout/vector/x;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v5, 0x3f00

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 225
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    .line 118
    invoke-static {}, Lcom/google/common/collect/Maps;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    .line 153
    iput-boolean v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    .line 164
    iput-boolean v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    .line 226
    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    .line 227
    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    .line 228
    new-instance v0, Lo/S;

    invoke-direct {v0}, Lo/S;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    .line 229
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->j:Landroid/content/res/Resources;

    .line 230
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/vector/aA;->k:Z

    .line 234
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/aC;

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    const v2, 0x7f02016e

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/aC;->a()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/maps/driveabout/vector/aD;->a(Z)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->b()Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    const v2, 0x7f02016b

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/aD;->a(I)Lcom/google/android/maps/driveabout/vector/aD;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aD;->c()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a([Lcom/google/android/maps/driveabout/vector/aC;)V

    .line 248
    const/high16 v0, 0x4280

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    .line 249
    iput v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->z:F

    .line 250
    iput v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->A:F

    .line 251
    const/high16 v0, 0x3f80

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(F)V

    .line 252
    const/16 v0, 0x4000

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->d(I)V

    .line 255
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->t:Lcom/google/android/maps/driveabout/vector/E;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->B:Lcom/google/android/maps/driveabout/vector/E;

    .line 258
    const/high16 v0, 0x4180

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    .line 259
    const/high16 v0, 0x4140

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    .line 260
    const/high16 v0, 0x3f40

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    .line 262
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->j:Landroid/content/res/Resources;

    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->C:I

    .line 266
    new-instance v0, Lh/m;

    invoke-direct {v0}, Lh/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    .line 268
    return-void
.end method

.method private declared-synchronized a(LD/a;I)LD/b;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 863
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 864
    if-nez v0, :cond_26

    .line 865
    new-instance v0, LD/b;

    invoke-direct {v0, p1}, LD/b;-><init>(LD/a;)V

    .line 866
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LD/b;->c(Z)V

    .line 867
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->j:Landroid/content/res/Resources;

    invoke-virtual {v0, v1, p2}, LD/b;->a(Landroid/content/res/Resources;I)V

    .line 868
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_28

    .line 870
    :cond_26
    monitor-exit p0

    return-object v0

    .line 863
    :catchall_28
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ZZZ)Lcom/google/android/maps/driveabout/vector/aC;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 919
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/aC;

    .line 920
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aC;->a(ZZZ)Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_1d

    move-result v2

    if-eqz v2, :cond_7

    .line 924
    :goto_19
    monitor-exit p0

    return-object v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_19

    .line 919
    :catchall_1d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(LC/a;)F
    .registers 6
    .parameter

    .prologue
    const/high16 v0, 0x3f80

    .line 729
    .line 730
    invoke-virtual {p1}, LC/a;->r()F

    move-result v1

    .line 731
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_14

    .line 732
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_15

    .line 733
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    .line 740
    :cond_14
    :goto_14
    return v0

    .line 735
    :cond_15
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    .line 737
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    goto :goto_14
.end method

.method private declared-synchronized b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/high16 v10, 0x3f80

    const/4 v9, 0x0

    .line 745
    monitor-enter p0

    :try_start_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v0}, Lo/S;->c()I

    move-result v0

    if-lez v0, :cond_50

    .line 746
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    if-nez v0, :cond_26

    .line 747
    new-instance v0, Lcom/google/android/maps/driveabout/vector/a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "MyLocation"

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/a;-><init>(Lo/T;IIILo/r;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    .line 748
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    sget v1, Lcom/google/android/maps/driveabout/vector/aA;->f:F

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(F)V

    .line 750
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v1}, Lo/S;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v2}, Lo/S;->c()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/T;I)V

    .line 752
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->w()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v0

    .line 753
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget v2, v0, Lcom/google/android/maps/driveabout/vector/aC;->f:I

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->b(I)V

    .line 754
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    iget v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->g:I

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/vector/a;->c(I)V

    .line 755
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->o:Lcom/google/android/maps/driveabout/vector/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/a;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 762
    :cond_50
    const/4 v0, 0x1

    .line 763
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v1}, Lo/S;->a()Lo/T;

    move-result-object v1

    invoke-virtual {p2, v1, v0}, LC/a;->a(Lo/T;Z)F

    move-result v1

    .line 766
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_1c4

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    .line 767
    :goto_63
    invoke-virtual {p2, v0, v1}, LC/a;->a(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    mul-float/2addr v0, v1

    .line 769
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 772
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v2}, Lo/S;->a()Lo/T;

    move-result-object v2

    invoke-static {p1, p2, v2, v0}, Lcom/google/android/maps/driveabout/vector/be;->a(LD/a;LC/a;Lo/T;F)V

    .line 775
    invoke-virtual {p1}, LD/a;->p()V

    .line 776
    iget-object v0, p1, LD/a;->h:LE/o;

    invoke-virtual {v0, p1}, LE/o;->d(LD/a;)V

    .line 777
    iget-object v0, p1, LD/a;->d:LE/i;

    invoke-virtual {v0, p1}, LE/i;->d(LD/a;)V

    .line 779
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->w()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v2

    .line 781
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->E:Z

    if-nez v0, :cond_1c8

    iget v0, v2, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    if-eq v0, v3, :cond_1c8

    const/4 v0, 0x1

    .line 784
    :goto_93
    iget-boolean v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    if-nez v3, :cond_a0

    .line 785
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalex(III)V

    .line 788
    :cond_a0
    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-interface {v1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 789
    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 790
    const/high16 v3, 0x1

    const/high16 v4, 0x1

    const/high16 v5, 0x1

    const/high16 v6, 0x1

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 793
    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->d:I

    if-eqz v3, :cond_12a

    .line 794
    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x2100

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 795
    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->d:I

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v3

    invoke-virtual {v3, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 796
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->h()F

    move-result v3

    const/high16 v4, 0x4120

    mul-float/2addr v3, v4

    add-float/2addr v3, v10

    .line 797
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v4}, Lo/S;->h()F

    move-result v4

    const/high16 v5, 0x4040

    mul-float/2addr v4, v5

    sub-float v4, v10, v4

    .line 798
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v5}, Lo/S;->h()F

    move-result v5

    const/high16 v6, 0x4080

    mul-float/2addr v5, v6

    .line 799
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v6}, Lo/S;->h()F

    move-result v6

    const/high16 v7, -0x3f80

    mul-float/2addr v6, v7

    .line 800
    invoke-interface {v1, v4, v4, v4, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 801
    const/4 v4, 0x0

    invoke-interface {v1, v5, v6, v4}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 802
    invoke-interface {v1, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 803
    const/4 v4, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-interface {v1, v4, v7, v8}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 804
    div-float v4, v10, v3

    div-float v7, v10, v3

    div-float v3, v10, v3

    invoke-interface {v1, v4, v7, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 805
    neg-float v3, v5

    neg-float v4, v6

    const/4 v5, 0x0

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 806
    const/high16 v3, 0x1

    const/high16 v4, 0x1

    const/high16 v5, 0x1

    const/high16 v6, 0x1

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 808
    const/16 v3, 0x2300

    const/16 v4, 0x2200

    const/16 v5, 0x1e01

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 811
    :cond_12a
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->h()F

    move-result v3

    cmpl-float v3, v3, v9

    if-eqz v3, :cond_14b

    .line 812
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->h()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v4}, Lo/S;->a()Lo/T;

    move-result-object v4

    invoke-virtual {v4}, Lo/T;->e()D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    .line 814
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 817
    :cond_14b
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->e()Z

    move-result v3

    if-eqz v3, :cond_193

    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->e:I

    if-eqz v3, :cond_193

    .line 818
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 819
    const/high16 v3, 0x4000

    const/high16 v4, 0x4000

    const/high16 v5, 0x4000

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 820
    iget v3, v2, Lcom/google/android/maps/driveabout/vector/aC;->e:I

    invoke-direct {p0, p1, v3}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v3

    invoke-virtual {v3, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 821
    const/4 v3, 0x5

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V

    .line 822
    const/high16 v3, 0x3f00

    const/high16 v4, 0x3f00

    const/high16 v5, 0x3f00

    invoke-interface {v1, v3, v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 823
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->b()F

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 826
    :cond_193
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v3

    if-eqz v3, :cond_1cb

    .line 827
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v3}, Lo/S;->b()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 833
    :goto_1a7
    if-eqz v0, :cond_1e6

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v0}, Lo/S;->j()F

    move-result v0

    cmpl-float v0, v0, v10

    if-nez v0, :cond_1e6

    .line 834
    iget v0, v2, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 839
    :goto_1bc
    const/4 v0, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x4

    invoke-interface {v1, v0, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glDrawArrays(III)V
    :try_end_1c2
    .catchall {:try_start_5 .. :try_end_1c2} :catchall_1e3

    .line 840
    monitor-exit p0

    return-void

    .line 766
    :cond_1c4
    :try_start_1c4
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    goto/16 :goto_63

    :cond_1c8
    move v0, v7

    .line 781
    goto/16 :goto_93

    .line 829
    :cond_1cb
    invoke-virtual {p2}, LC/a;->p()F

    move-result v3

    neg-float v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/high16 v6, 0x3f80

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 830
    invoke-virtual {p2}, LC/a;->q()F

    move-result v3

    const/high16 v4, 0x3f80

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v1, v3, v4, v5, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V
    :try_end_1e2
    .catchall {:try_start_1c4 .. :try_end_1e2} :catchall_1e3

    goto :goto_1a7

    .line 745
    :catchall_1e3
    move-exception v0

    monitor-exit p0

    throw v0

    .line 836
    :cond_1e6
    :try_start_1e6
    iget v0, v2, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(LD/a;I)LD/b;

    move-result-object v0

    invoke-virtual {v0, v1}, LD/b;->a(Ljavax/microedition/khronos/opengles/GL10;)V
    :try_end_1ef
    .catchall {:try_start_1e6 .. :try_end_1ef} :catchall_1e3

    goto :goto_1bc
.end method

.method private o()V
    .registers 1

    .prologue
    .line 331
    return-void
.end method

.method private declared-synchronized q()V
    .registers 3

    .prologue
    .line 367
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LD/b;

    .line 368
    invoke-virtual {v0}, LD/b;->g()V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1b

    goto :goto_b

    .line 367
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 370
    :cond_1e
    :try_start_1e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_23
    .catchall {:try_start_1e .. :try_end_23} :catchall_1b

    .line 371
    monitor-exit p0

    return-void
.end method

.method private r()V
    .registers 6

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->F:Lcom/google/android/maps/driveabout/vector/aB;

    if-eqz v0, :cond_13

    .line 421
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->F:Lcom/google/android/maps/driveabout/vector/aB;

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    int-to-float v3, v3

    const/high16 v4, 0x4780

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/aB;->a(FFF)V

    .line 424
    :cond_13
    return-void
.end method

.method private s()F
    .registers 3

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_21

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    div-float/2addr v0, v1

    .line 489
    :goto_b
    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    mul-float/2addr v1, v0

    .line 493
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_24

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    .line 494
    :goto_16
    mul-float/2addr v1, v0

    .line 495
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->t()Z

    move-result v0

    if-eqz v0, :cond_27

    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->A:F

    .line 497
    :goto_1f
    mul-float/2addr v0, v1

    return v0

    .line 488
    :cond_21
    const/high16 v0, 0x3e80

    goto :goto_b

    .line 493
    :cond_24
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    goto :goto_16

    .line 495
    :cond_27
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->z:F

    goto :goto_1f
.end method

.method private t()Z
    .registers 2

    .prologue
    .line 586
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->w()Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    return v0
.end method

.method private declared-synchronized u()Lo/S;
    .registers 3

    .prologue
    .line 878
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    if-eqz v0, :cond_1f

    .line 880
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/S;)V

    .line 882
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->a(Lo/S;)Z

    .line 883
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 884
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_22

    .line 887
    :goto_1d
    monitor-exit p0

    return-object v0

    :cond_1f
    :try_start_1f
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;
    :try_end_21
    .catchall {:try_start_1f .. :try_end_21} :catchall_22

    goto :goto_1d

    .line 878
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private v()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 891
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->d:Lcom/google/android/maps/driveabout/vector/aU;

    if-eqz v0, :cond_a

    .line 892
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->d:Lcom/google/android/maps/driveabout/vector/aU;

    invoke-interface {v0, v1, v1}, Lcom/google/android/maps/driveabout/vector/aU;->a(ZZ)V

    .line 894
    :cond_a
    return-void
.end method

.method private w()Lcom/google/android/maps/driveabout/vector/aC;
    .registers 4

    .prologue
    .line 903
    monitor-enter p0

    .line 904
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->u()Lo/S;

    move-result-object v0

    .line 905
    invoke-virtual {v0}, Lo/S;->e()Z

    move-result v1

    .line 906
    invoke-virtual {v0}, Lo/S;->g()Z

    move-result v2

    .line 907
    invoke-virtual {v0}, Lo/S;->i()Z

    move-result v0

    .line 908
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_17

    .line 909
    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/aA;->a(ZZZ)Lcom/google/android/maps/driveabout/vector/aC;

    move-result-object v0

    return-object v0

    .line 908
    :catchall_17
    move-exception v0

    :try_start_18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_18 .. :try_end_19} :catchall_17

    throw v0
.end method


# virtual methods
.method public a(FFLo/T;LC/a;)I
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 560
    monitor-enter p0

    .line 561
    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-nez v0, :cond_10

    .line 562
    const v0, 0x7fffffff

    monitor-exit p0

    .line 567
    :goto_f
    return v0

    .line 565
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;

    move-result-object v0

    invoke-virtual {p4, v0}, LC/a;->b(Lo/T;)[I

    move-result-object v0

    .line 566
    monitor-exit p0
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_34

    .line 567
    aget v1, v0, v2

    int-to-float v1, v1

    sub-float v1, p1, v1

    aget v2, v0, v2

    int-to-float v2, v2

    sub-float v2, p1, v2

    mul-float/2addr v1, v2

    aget v2, v0, v3

    int-to-float v2, v2

    sub-float v2, p2, v2

    aget v0, v0, v3

    int-to-float v0, v0

    sub-float v0, p2, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_f

    .line 566
    :catchall_34
    move-exception v0

    :try_start_35
    monitor-exit p0
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_34

    throw v0
.end method

.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 393
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    .line 394
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->r()V

    .line 395
    return-void
.end method

.method public a(FFF)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 361
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->g:F

    .line 362
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/aA;->h:F

    .line 363
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/aA;->i:F

    .line 364
    return-void
.end method

.method public a(FII)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x42c8

    .line 384
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->t:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    div-float/2addr v0, v1

    .line 385
    const/high16 v1, 0x3f00

    mul-float/2addr v1, p1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->u:F

    .line 386
    int-to-float v1, p2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->z:F

    .line 387
    int-to-float v1, p3

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->A:F

    .line 388
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(F)V

    .line 389
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->r()V

    .line 390
    return-void
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 680
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-nez v0, :cond_f

    .line 725
    :cond_e
    :goto_e
    return-void

    .line 686
    :cond_f
    monitor-enter p0

    .line 687
    :try_start_10
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    if-eqz v0, :cond_54

    .line 688
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    invoke-virtual {p1}, LD/a;->d()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lh/l;->a(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    .line 692
    :goto_20
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    if-eqz v0, :cond_58

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->a(Lo/S;)Z

    move-result v0

    if-eqz v0, :cond_58

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 695
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/S;)V

    .line 696
    invoke-virtual {p1}, LD/a;->d()J

    move-result-wide v0

    const-wide/16 v2, 0xc8

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, LD/a;->a(J)V

    .line 701
    :goto_47
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-nez v0, :cond_60

    .line 702
    monitor-exit p0

    goto :goto_e

    .line 704
    :catchall_51
    move-exception v0

    monitor-exit p0
    :try_end_53
    .catchall {:try_start_10 .. :try_end_53} :catchall_51

    throw v0

    .line 690
    :cond_54
    const/4 v0, 0x0

    :try_start_55
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    goto :goto_20

    .line 698
    :cond_58
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0, v1}, Lo/S;->a(Lo/S;)V

    goto :goto_47

    .line 704
    :cond_60
    monitor-exit p0
    :try_end_61
    .catchall {:try_start_55 .. :try_end_61} :catchall_51

    .line 706
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v1

    .line 708
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 710
    const/4 v0, 0x0

    .line 711
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    if-eqz v2, :cond_86

    .line 712
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    invoke-virtual {v2}, Lo/D;->a()Lo/r;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/vector/aH;->a(Lo/o;)Lcom/google/android/maps/driveabout/vector/aJ;

    move-result-object v0

    .line 713
    if-eqz v0, :cond_86

    .line 714
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->r:Lo/S;

    invoke-virtual {v2}, Lo/S;->a()Lo/T;

    move-result-object v2

    invoke-interface {v0, p1, p2, p3, v2}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V

    .line 718
    :cond_86
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/aA;->b(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 720
    if-eqz v0, :cond_8e

    .line 721
    invoke-interface {v0, p1, p3}, Lcom/google/android/maps/driveabout/vector/aJ;->a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 724
    :cond_8e
    invoke-interface {v1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto/16 :goto_e
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 272
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aA;->d:Lcom/google/android/maps/driveabout/vector/aU;

    .line 273
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/E;)V
    .registers 2
    .parameter

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->B:Lcom/google/android/maps/driveabout/vector/E;

    .line 294
    return-void
.end method

.method public a(Ljava/util/List;FFLo/T;LC/a;I)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aA;->k()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 574
    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/aA;->a(FFLo/T;LC/a;)I

    move-result v0

    .line 575
    if-ge v0, p6, :cond_14

    .line 576
    new-instance v1, Lcom/google/android/maps/driveabout/vector/t;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/maps/driveabout/vector/t;-><init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 579
    :cond_14
    return-void
.end method

.method public declared-synchronized a(Lo/S;)V
    .registers 4
    .parameter

    .prologue
    .line 300
    monitor-enter p0

    if-eqz p1, :cond_2b

    :try_start_3
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    invoke-virtual {p1}, Lo/S;->l()Z

    move-result v1

    if-ne v0, v1, :cond_2b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->f()Lo/D;

    move-result-object v0

    invoke-virtual {p1}, Lo/S;->f()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->g()Z

    move-result v0

    invoke-virtual {p1}, Lo/S;->g()Z

    move-result v1

    if-eq v0, v1, :cond_2e

    .line 305
    :cond_2b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    .line 307
    :cond_2e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0, p1}, Lo/S;->a(Lo/S;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 309
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->b(Lo/S;)V

    .line 317
    :goto_42
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->o()V

    .line 318
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->v()V
    :try_end_48
    .catchall {:try_start_3 .. :try_end_48} :catchall_52

    .line 319
    monitor-exit p0

    return-void

    .line 314
    :cond_4a
    :try_start_4a
    new-instance v0, Lh/m;

    invoke-direct {v0}, Lh/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;
    :try_end_51
    .catchall {:try_start_4a .. :try_end_51} :catchall_52

    goto :goto_42

    .line 300
    :catchall_52
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public varargs declared-synchronized a([Lcom/google/android/maps/driveabout/vector/aC;)V
    .registers 3
    .parameter

    .prologue
    .line 350
    monitor-enter p0

    :try_start_1
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->a([Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->e:Ljava/util/List;

    .line 351
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->q()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    .line 357
    monitor-exit p0

    return-void

    .line 350
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LC/a;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 462
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aA;->e()Lo/T;

    move-result-object v2

    .line 463
    if-nez v2, :cond_9

    .line 476
    :goto_8
    return v1

    .line 468
    :cond_9
    invoke-virtual {p1, v2}, LC/a;->b(Lo/T;)[I

    move-result-object v2

    .line 469
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->s()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    .line 470
    aget v4, v2, v1

    sub-int/2addr v4, v3

    .line 471
    aget v5, v2, v1

    add-int/2addr v5, v3

    .line 472
    aget v6, v2, v0

    sub-int/2addr v6, v3

    .line 473
    aget v2, v2, v0

    add-int/2addr v2, v3

    .line 476
    invoke-virtual {p1}, LC/a;->k()I

    move-result v3

    if-ge v4, v3, :cond_35

    if-ltz v5, :cond_35

    invoke-virtual {p1}, LC/a;->l()I

    move-result v3

    if-ge v6, v3, :cond_35

    if-ltz v2, :cond_35

    :goto_33
    move v1, v0

    goto :goto_8

    :cond_35
    move v0, v1

    goto :goto_33
.end method

.method public a_(LC/a;)I
    .registers 3
    .parameter

    .prologue
    .line 849
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->l:I

    return v0
.end method

.method public declared-synchronized b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 591
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 592
    monitor-exit p0

    return-void

    .line 591
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 282
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/aA;->b(LC/a;)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    .line 283
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    invoke-interface {v0, p1}, Lh/l;->a(LC/a;)V

    .line 289
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/util/List;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 656
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    if-eqz v0, :cond_3e

    .line 657
    iput-boolean v2, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    .line 659
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/aA;->f()Lo/D;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    .line 660
    const/4 v0, 0x0

    .line 661
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    if-eqz v3, :cond_21

    .line 662
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    invoke-virtual {v3}, Lo/D;->a()Lo/r;

    move-result-object v3

    invoke-virtual {v0, v3}, Ln/q;->e(Lo/r;)Ln/k;

    move-result-object v0

    .line 666
    :cond_21
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 667
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/aA;->x:Lo/D;

    if-eqz v3, :cond_2a

    if-nez v0, :cond_2f

    .line 668
    :cond_2a
    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/d;->b(Ljava/util/List;)Z

    move-result v0

    .line 674
    :goto_2e
    return v0

    .line 670
    :cond_2f
    sget-object v3, Lcom/google/android/maps/driveabout/vector/aI;->i:Lcom/google/android/maps/driveabout/vector/aI;

    new-array v4, v1, [Lcom/google/android/maps/driveabout/vector/aJ;

    aput-object v0, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/google/android/maps/driveabout/vector/aA;->a(Lcom/google/android/maps/driveabout/vector/aI;[Lcom/google/android/maps/driveabout/vector/aJ;)Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 671
    goto :goto_2e

    :cond_3e
    move v0, v2

    .line 674
    goto :goto_2e
.end method

.method public c()LC/b;
    .registers 2

    .prologue
    .line 856
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)V
    .registers 3
    .parameter

    .prologue
    const v0, 0x3f2b851f

    .line 400
    packed-switch p1, :pswitch_data_c

    .line 408
    const/high16 v0, 0x3f80

    .line 410
    :pswitch_8
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/vector/aA;->a(F)V

    .line 411
    return-void

    .line 400
    :pswitch_data_c
    .packed-switch 0x2
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public c(LD/a;)V
    .registers 3
    .parameter

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->q()V

    .line 597
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->y:Z

    .line 604
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 435
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->w:Z

    .line 436
    return-void
.end method

.method public d(I)V
    .registers 2
    .parameter

    .prologue
    .line 414
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/aA;->v:I

    .line 415
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->r()V

    .line 416
    return-void
.end method

.method public declared-synchronized e()Lo/T;
    .registers 3

    .prologue
    .line 445
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->s:Z

    if-eqz v0, :cond_1c

    .line 446
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->n:Lh/l;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-interface {v0, v1}, Lh/l;->a(Lo/S;)Z

    .line 447
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->l()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 448
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->q:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_23

    move-result-object v0

    .line 451
    :goto_1a
    monitor-exit p0

    return-object v0

    :cond_1c
    :try_start_1c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->p:Lo/S;

    invoke-virtual {v0}, Lo/S;->a()Lo/T;
    :try_end_21
    .catchall {:try_start_1c .. :try_end_21} :catchall_23

    move-result-object v0

    goto :goto_1a

    .line 445
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Lo/D;
    .registers 2

    .prologue
    .line 456
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->u()Lo/S;

    move-result-object v0

    invoke-virtual {v0}, Lo/S;->f()Lo/D;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_b

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g_()V
    .registers 2

    .prologue
    .line 430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->w:Z

    .line 431
    return-void
.end method

.method public h()I
    .registers 2

    .prologue
    .line 512
    const/4 v0, 0x0

    return v0
.end method

.method public h_()I
    .registers 3

    .prologue
    .line 507
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->C:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/aA;->D:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .registers 2

    .prologue
    .line 522
    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .registers 2

    .prologue
    .line 527
    const/4 v0, 0x0

    return-object v0
.end method

.method public k()Z
    .registers 2

    .prologue
    .line 532
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->k:Z

    return v0
.end method

.method public declared-synchronized n()Lo/S;
    .registers 3

    .prologue
    .line 874
    monitor-enter p0

    :try_start_1
    new-instance v0, Lo/S;

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/aA;->u()Lo/S;

    move-result-object v1

    invoke-direct {v0, v1}, Lo/S;-><init>(Lo/S;)V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    monitor-exit p0

    return-object v0

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/aA;->B:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method

.method public z()Lcom/google/android/maps/driveabout/vector/b;
    .registers 1

    .prologue
    .line 844
    return-object p0
.end method
