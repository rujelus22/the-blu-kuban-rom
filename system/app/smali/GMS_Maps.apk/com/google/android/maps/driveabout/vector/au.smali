.class Lcom/google/android/maps/driveabout/vector/au;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lg/c;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Lg/c;

.field private final c:I

.field private d:Lr/n;

.field private e:Ln/e;

.field private final f:Ln/q;


# direct methods
.method public constructor <init>(Lg/c;ILandroid/content/Context;Ln/q;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 705
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/au;->a:Landroid/content/Context;

    .line 706
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/au;->b:Lg/c;

    .line 707
    iput p2, p0, Lcom/google/android/maps/driveabout/vector/au;->c:I

    .line 708
    iput-object p4, p0, Lcom/google/android/maps/driveabout/vector/au;->f:Ln/q;

    .line 709
    return-void
.end method


# virtual methods
.method public a(LA/c;IZLA/b;)Lg/a;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 714
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->b:Lg/c;

    invoke-interface {v0, p1, p2, p3, p4}, Lg/c;->a(LA/c;IZLA/b;)Lg/a;

    move-result-object v1

    .line 717
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->d:Lr/n;

    if-nez v0, :cond_10

    .line 718
    invoke-static {}, Lr/n;->a()Lr/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->d:Lr/n;

    .line 720
    :cond_10
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->e:Ln/e;

    if-nez v0, :cond_1c

    .line 721
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->f:Ln/q;

    invoke-virtual {v0}, Ln/q;->j()Ln/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/au;->e:Ln/e;

    .line 723
    :cond_1c
    new-instance v0, Lk/a;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/au;->e:Ln/e;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/au;->d:Lr/n;

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/au;->c:I

    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/au;->f:Ln/q;

    invoke-direct/range {v0 .. v5}, Lk/a;-><init>(Lg/a;Ln/e;Lr/n;ILn/q;)V

    return-object v0
.end method

.method public a(LA/c;ZLA/b;)Lk/f;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 733
    new-instance v0, Lk/f;

    invoke-direct {v0, p1, p3}, Lk/f;-><init>(LA/c;LA/b;)V

    return-object v0
.end method
