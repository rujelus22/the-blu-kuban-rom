.class Lcom/google/android/maps/driveabout/app/Q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic d:Lcom/google/android/maps/driveabout/app/DestinationActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 283
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/Q;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 304
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    const v1, 0x7f1000d8

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 305
    new-instance v1, Lcom/google/android/maps/driveabout/app/cl;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/app/cl;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v3}, LaA/h;->b(LaA/e;Lcom/google/googlenav/android/aa;)V

    .line 308
    invoke-static {}, LaA/h;->b()LaA/h;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, LaA/h;->a(Landroid/view/View;LaA/g;)V

    .line 311
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    const v1, 0x7f1000db

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 312
    new-instance v1, Lcom/google/android/maps/driveabout/app/R;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/R;-><init>(Lcom/google/android/maps/driveabout/app/Q;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/Q;->c()Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/T;)Lcom/google/android/maps/driveabout/app/T;

    .line 323
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->f(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/bR;)Lcom/google/android/maps/driveabout/app/bR;

    .line 326
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    const v2, 0x7f10015b

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    .line 327
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/S;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/S;-><init>(Lcom/google/android/maps/driveabout/app/Q;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setTravelModeChangedListener(Lcom/google/android/maps/driveabout/widgets/f;)V

    .line 336
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .registers 4
    .parameter

    .prologue
    .line 356
    const v0, 0x7f10049e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 357
    return-void
.end method

.method public a(Z)V
    .registers 6
    .parameter

    .prologue
    const v3, 0x7f1000da

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 291
    if-nez p1, :cond_1b

    .line 292
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setVisibility(I)V

    .line 298
    :goto_1a
    return-void

    .line 295
    :cond_1b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 296
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->d(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/widgets/TravelModeSelector;->setVisibility(I)V

    goto :goto_1a
.end method

.method public b()V
    .registers 1

    .prologue
    .line 365
    return-void
.end method

.method protected c()Lcom/google/android/maps/driveabout/app/T;
    .registers 5

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/af;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/Q;->d:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v2}, Lcom/google/android/maps/driveabout/app/ci;->d(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/T;->a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;ZZ)Lcom/google/android/maps/driveabout/app/T;

    move-result-object v0

    return-object v0
.end method
