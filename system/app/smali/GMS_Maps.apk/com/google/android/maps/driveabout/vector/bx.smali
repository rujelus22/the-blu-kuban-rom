.class public Lcom/google/android/maps/driveabout/vector/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:LC/b;


# direct methods
.method public constructor <init>(LC/b;)V
    .registers 2
    .parameter

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bx;->a:LC/b;

    .line 20
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/maps/driveabout/vector/bx;
    .registers 3
    .parameter

    .prologue
    .line 23
    if-eqz p0, :cond_b

    const-string v0, "vector.version"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_14

    .line 24
    :cond_b
    const-string v0, "VectorMapSavedState"

    const-string v1, "Couldn\'t load from bundle"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x0

    .line 27
    :goto_13
    return-object v0

    :cond_14
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bx;

    invoke-static {p0}, LC/b;->b(Landroid/os/Bundle;)LC/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bx;-><init>(LC/b;)V

    goto :goto_13
.end method


# virtual methods
.method public b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 33
    const-string v0, "vector.version"

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 34
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bx;->a:LC/b;

    invoke-virtual {v0, p1}, LC/b;->a(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method
