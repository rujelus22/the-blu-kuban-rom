.class public Lcom/google/android/maps/driveabout/vector/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;


# static fields
.field private static final b:F

.field private static final c:LC/b;

.field private static d:F


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private e:Lcom/google/android/maps/driveabout/vector/bm;

.field private final f:Z

.field private final g:Lw/b;

.field private volatile h:LC/b;

.field private volatile i:LC/b;

.field private volatile j:F

.field private volatile k:LC/c;

.field private volatile l:Z

.field private m:Z

.field private n:Lg/a;

.field private o:Lcom/google/android/maps/driveabout/vector/aU;

.field private p:Lcom/google/android/maps/driveabout/vector/bB;

.field private q:Lcom/google/android/maps/driveabout/vector/bq;

.field private r:Lcom/google/android/maps/driveabout/vector/aE;

.field private s:Z

.field private t:I

.field private u:F

.field private final v:Ln/q;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 79
    const-wide/high16 v0, 0x3ff0

    const-wide/high16 v2, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/vector/bk;->b:F

    .line 126
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    .line 129
    const/high16 v0, 0x41a8

    sput v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    .line 227
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->u:F

    .line 258
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->a:Landroid/content/res/Resources;

    .line 259
    sget-object v0, LC/a;->d:LC/b;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 260
    sget-object v0, LC/a;->d:LC/b;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    .line 262
    if-eqz p1, :cond_3e

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 263
    :goto_1d
    new-instance v2, Lw/b;

    int-to-float v0, v0

    invoke-direct {v2, v0}, Lw/b;-><init>(F)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    .line 266
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_40

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_40

    :goto_35
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->f:Z

    .line 269
    invoke-static {}, Ln/q;->a()Ln/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->v:Ln/q;

    .line 270
    return-void

    :cond_3e
    move v0, v1

    .line 262
    goto :goto_1d

    .line 266
    :cond_40
    const/4 v1, 0x0

    goto :goto_35
.end method

.method private a(F)F
    .registers 10
    .parameter

    .prologue
    .line 622
    const-wide/high16 v0, 0x4000

    const-wide v2, 0x40031eb851eb851fL

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget v6, Lcom/google/android/maps/driveabout/vector/bk;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404d5ae147ae147bL

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(LC/a;Lw/b;Lo/T;F)LC/b;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1536
    invoke-virtual {p0}, LC/a;->h()Lo/T;

    move-result-object v0

    .line 1537
    invoke-virtual {v0}, Lo/T;->f()I

    move-result v1

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1538
    invoke-virtual {v0}, Lo/T;->g()I

    move-result v0

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1539
    float-to-double v2, p3

    const-wide v4, 0x400921fb54442d18L

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L

    div-double/2addr v2, v4

    double-to-float v2, v2

    .line 1540
    neg-float v3, v2

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    .line 1541
    neg-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    .line 1542
    int-to-float v4, v1

    mul-float/2addr v4, v2

    int-to-float v5, v0

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    .line 1543
    int-to-float v1, v1

    mul-float/2addr v1, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 1544
    new-instance v1, Lo/T;

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Lo/T;-><init>(II)V

    .line 1548
    invoke-virtual {p0}, LC/a;->p()F

    move-result v0

    add-float/2addr v0, p3

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f(F)F

    move-result v4

    .line 1549
    new-instance v0, LC/b;

    invoke-virtual {p0}, LC/a;->r()F

    move-result v2

    invoke-virtual {p0}, LC/a;->q()F

    move-result v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {p1, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(LC/b;LC/a;FF)LC/b;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1504
    invoke-virtual {p1}, LC/a;->y()F

    move-result v0

    mul-float/2addr v0, p2

    .line 1505
    neg-float v1, p3

    invoke-virtual {p1}, LC/a;->y()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1}, LC/a;->q()F

    move-result v2

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    div-float/2addr v1, v2

    .line 1508
    invoke-virtual {p1}, LC/a;->u()Lo/T;

    move-result-object v2

    .line 1509
    invoke-virtual {p1}, LC/a;->v()Lo/T;

    move-result-object v3

    .line 1510
    new-instance v4, Lo/T;

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v5

    invoke-virtual {v2}, Lo/T;->g()I

    move-result v2

    invoke-direct {v4, v5, v2}, Lo/T;-><init>(II)V

    .line 1511
    new-instance v5, Lo/T;

    invoke-virtual {v3}, Lo/T;->f()I

    move-result v2

    invoke-virtual {v3}, Lo/T;->g()I

    move-result v3

    invoke-direct {v5, v2, v3}, Lo/T;-><init>(II)V

    .line 1512
    invoke-static {v4, v0, v4}, Lo/T;->b(Lo/T;FLo/T;)V

    .line 1513
    invoke-static {v5, v1, v5}, Lo/T;->b(Lo/T;FLo/T;)V

    .line 1515
    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v0

    .line 1516
    invoke-virtual {p0}, LC/b;->a()F

    move-result v2

    .line 1517
    invoke-virtual {v0}, Lo/T;->h()I

    move-result v3

    .line 1518
    invoke-virtual {v0, v4}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v1

    .line 1519
    invoke-static {v1, v5, v1}, Lo/T;->a(Lo/T;Lo/T;Lo/T;)V

    .line 1520
    invoke-virtual {v1, v3}, Lo/T;->b(I)V

    .line 1521
    new-instance v0, LC/b;

    invoke-virtual {p0}, LC/b;->d()F

    move-result v3

    invoke-virtual {p0}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method public static a(LC/b;LC/a;Lw/b;FFF)LC/b;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x4000

    .line 1562
    invoke-virtual {p1}, LC/a;->k()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v6, p4, v0

    .line 1563
    invoke-virtual {p1}, LC/a;->l()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v7, p5, v0

    .line 1572
    invoke-static {p0, p1, v6, v7}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v0

    invoke-virtual {p2, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v4

    .line 1574
    new-instance v0, LC/b;

    invoke-virtual {v4}, LC/b;->c()Lo/T;

    move-result-object v1

    sget v2, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    invoke-virtual {v4}, LC/b;->a()F

    move-result v3

    add-float/2addr v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 1577
    invoke-virtual {p1, v0}, LC/a;->a(LC/b;)V

    .line 1578
    neg-float v1, v6

    neg-float v2, v7

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v0

    return-object v0
.end method

.method private a(LC/c;)V
    .registers 3
    .parameter

    .prologue
    .line 607
    sget-object v0, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    .line 611
    instance-of v0, p1, Lw/d;

    if-eqz v0, :cond_e

    .line 612
    check-cast p1, Lw/d;

    invoke-interface {p1}, Lw/d;->a()V

    .line 614
    :cond_e
    return-void
.end method

.method private a(LC/c;LC/b;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 473
    monitor-enter p0

    .line 474
    :try_start_2
    instance-of v0, p1, Lw/d;

    if-eqz v0, :cond_9

    .line 475
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->l:Z

    .line 477
    :cond_9
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    .line 478
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->m:Z

    .line 479
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 481
    if-eqz p2, :cond_24

    .line 482
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0, p2}, Lw/b;->a(LC/b;)LC/b;

    move-result-object p2

    .line 485
    :cond_24
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bk;->i:LC/b;

    .line 486
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 487
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_2 .. :try_end_2a} :catchall_35

    .line 488
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->o:Lcom/google/android/maps/driveabout/vector/aU;

    if-eqz v0, :cond_34

    .line 489
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->o:Lcom/google/android/maps/driveabout/vector/aU;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aU;->a(ZZ)V

    .line 491
    :cond_34
    return-void

    .line 487
    :catchall_35
    move-exception v0

    :try_start_36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    throw v0
.end method

.method public static b(F)V
    .registers 1
    .parameter

    .prologue
    .line 307
    sput p0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    .line 308
    return-void
.end method

.method public static d()F
    .registers 1

    .prologue
    .line 299
    sget v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return v0
.end method

.method static synthetic e(F)F
    .registers 2
    .parameter

    .prologue
    .line 45
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/bk;->f(F)F

    move-result v0

    return v0
.end method

.method private static f(F)F
    .registers 8
    .parameter

    .prologue
    const-wide v5, 0x4076800000000000L

    .line 1582
    move v0, p0

    :goto_6
    float-to-double v1, v0

    cmpl-double v1, v1, v5

    if-ltz v1, :cond_f

    .line 1583
    float-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_6

    .line 1585
    :cond_f
    :goto_f
    float-to-double v1, v0

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1a

    .line 1586
    float-to-double v0, v0

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_f

    .line 1588
    :cond_1a
    return v0
.end method

.method static synthetic j()F
    .registers 1

    .prologue
    .line 45
    sget v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    return v0
.end method


# virtual methods
.method public declared-synchronized a(FFF)F
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 791
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bp;

    if-eqz v0, :cond_1c

    .line 792
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bp;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 800
    :goto_18
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_3d

    .line 801
    monitor-exit p0

    return v0

    .line 794
    :cond_1c
    :try_start_1c
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bp;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bp;-><init>(LC/b;Lw/b;)V

    .line 796
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 797
    sget-object v2, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V
    :try_end_3b
    .catchall {:try_start_1c .. :try_end_3b} :catchall_3d

    move v0, v1

    goto :goto_18

    .line 791
    :catchall_3d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(FFFI)F
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 852
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 853
    invoke-virtual {v1}, LC/b;->a()F

    move-result v0

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F

    .line 854
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bo;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bo;-><init>(LC/b;Lw/b;FFFI)V

    sget-object v2, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    .line 857
    invoke-virtual {v1}, LC/b;->a()F

    move-result v0

    add-float/2addr v0, p1

    sget v1, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public a(FI)F
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 831
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 832
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v5}, LC/b;->a()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v5}, LC/b;->d()F

    move-result v3

    invoke-virtual {v5}, LC/b;->e()F

    move-result v4

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {v6, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    .line 836
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    .line 837
    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    return v0
.end method

.method public a(Lo/T;)F
    .registers 3
    .parameter

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0}, Lw/b;->a()Lw/c;

    move-result-object v0

    .line 366
    if-nez v0, :cond_b

    sget v0, Lcom/google/android/maps/driveabout/vector/bk;->d:F

    :goto_a
    return v0

    :cond_b
    invoke-interface {v0, p1}, Lw/c;->a(Lo/T;)F

    move-result v0

    goto :goto_a
.end method

.method public declared-synchronized a(FF)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 730
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bp;

    if-eqz v0, :cond_16

    .line 731
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bp;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_32

    .line 738
    :goto_14
    monitor-exit p0

    return-void

    .line 733
    :cond_16
    :try_start_16
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bp;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bp;-><init>(LC/b;Lw/b;)V

    .line 735
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    .line 736
    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V
    :try_end_31
    .catchall {:try_start_16 .. :try_end_31} :catchall_32

    goto :goto_14

    .line 730
    :catchall_32
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LC/c;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 496
    .line 497
    if-nez p2, :cond_7

    const/4 v0, 0x0

    .line 498
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V

    .line 499
    return-void

    .line 497
    :cond_7
    const/4 v0, -0x1

    goto :goto_3
.end method

.method public a(LC/c;II)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x442f

    const/high16 v8, 0x4100

    const/4 v0, 0x0

    .line 517
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 603
    :goto_12
    return-void

    .line 520
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    if-eqz v1, :cond_19

    move p3, v0

    move p2, v0

    .line 525
    :cond_19
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    .line 527
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 528
    invoke-interface {p1}, LC/c;->b()LC/b;

    move-result-object v2

    invoke-virtual {v2, v1}, LC/b;->a(LC/b;)LC/b;

    move-result-object v6

    .line 530
    invoke-virtual {v6}, LC/b;->a()F

    move-result v2

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F

    .line 532
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    .line 533
    invoke-virtual {v6}, LC/b;->a()F

    move-result v3

    invoke-virtual {v1}, LC/b;->a()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 534
    invoke-virtual {v2}, LR/m;->y()Z

    move-result v4

    if-eqz v4, :cond_4b

    invoke-virtual {v2}, LR/m;->z()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_4f

    .line 536
    :cond_4b
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;)V

    goto :goto_12

    .line 541
    :cond_4f
    invoke-virtual {v6}, LC/b;->a()F

    move-result v2

    invoke-virtual {v1}, LC/b;->a()F

    move-result v4

    add-float/2addr v2, v4

    const/high16 v4, 0x3f00

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 542
    const/16 v4, 0x1e

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 543
    const/high16 v4, 0x4000

    shr-int v2, v4, v2

    .line 544
    invoke-virtual {v6}, LC/b;->c()Lo/T;

    move-result-object v4

    invoke-virtual {v1}, LC/b;->c()Lo/T;

    move-result-object v5

    invoke-virtual {v4, v5}, Lo/T;->c(Lo/T;)F

    move-result v5

    .line 545
    int-to-float v4, v2

    div-float v7, v5, v4

    .line 546
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    int-to-float v4, v4

    cmpg-float v4, v7, v4

    if-gtz v4, :cond_88

    const/4 v4, 0x1

    .line 548
    :goto_80
    if-eqz v4, :cond_a7

    .line 549
    if-nez p2, :cond_8a

    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;)V

    goto :goto_12

    :cond_88
    move v4, v0

    .line 546
    goto :goto_80

    .line 553
    :cond_8a
    if-ne p2, v10, :cond_e5

    .line 555
    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 556
    div-float/2addr v0, v8

    .line 557
    const/high16 v2, 0x3f40

    mul-float/2addr v0, v2

    const/high16 v2, 0x3fc0

    add-float/2addr v0, v2

    .line 560
    mul-float/2addr v0, v9

    float-to-int v3, v0

    .line 563
    :goto_99
    const/4 v5, 0x0

    .line 564
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bl;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bl;-><init>(LC/b;LC/c;IZF)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    goto/16 :goto_12

    .line 569
    :cond_a7
    if-nez p3, :cond_ae

    .line 570
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;)V

    goto/16 :goto_12

    .line 573
    :cond_ae
    if-ne p3, v10, :cond_e3

    .line 579
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    int-to-float v0, v0

    sub-float v0, v7, v0

    const/high16 v3, 0x4e80

    int-to-float v2, v2

    div-float v2, v3, v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bk;->t:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    .line 581
    const v2, 0x40833333

    mul-float/2addr v0, v2

    const v2, 0x3fb33333

    add-float/2addr v0, v2

    .line 583
    mul-float/2addr v0, v9

    float-to-int v0, v0

    const/16 v2, 0x9c4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 588
    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->u:F

    mul-float/2addr v0, v2

    float-to-int v3, v0

    .line 591
    :goto_d4
    invoke-direct {p0, v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(F)F

    move-result v5

    .line 597
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bl;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bl;-><init>(LC/b;LC/c;IZF)V

    invoke-direct {p0, v0, v6}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    goto/16 :goto_12

    :cond_e3
    move v3, p3

    goto :goto_d4

    :cond_e5
    move v3, p2

    goto :goto_99
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/aE;)V
    .registers 3
    .parameter

    .prologue
    .line 337
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->r:Lcom/google/android/maps/driveabout/vector/aE;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 338
    monitor-exit p0

    return-void

    .line 337
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/aU;)V
    .registers 3
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->o:Lcom/google/android/maps/driveabout/vector/aU;

    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->s:Z

    .line 313
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bB;)V
    .registers 2
    .parameter

    .prologue
    .line 341
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->p:Lcom/google/android/maps/driveabout/vector/bB;

    .line 342
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bm;)V
    .registers 2
    .parameter

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->e:Lcom/google/android/maps/driveabout/vector/bm;

    .line 274
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bq;)V
    .registers 2
    .parameter

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->q:Lcom/google/android/maps/driveabout/vector/bq;

    .line 346
    return-void
.end method

.method public a(Lo/D;)V
    .registers 3
    .parameter

    .prologue
    .line 1601
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->v:Ln/q;

    invoke-virtual {v0, p1}, Ln/q;->a(Lo/D;)V

    .line 1602
    return-void
.end method

.method public a(Lo/T;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 411
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 412
    new-instance v0, LC/b;

    invoke-virtual {v1}, LC/b;->a()F

    move-result v2

    invoke-virtual {v1}, LC/b;->d()F

    move-result v3

    invoke-virtual {v1}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 414
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    .line 415
    return-void
.end method

.method public a(Lo/ad;IIFI)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4380

    const/4 v3, 0x0

    .line 447
    if-eqz p2, :cond_7

    if-nez p3, :cond_8

    .line 460
    :cond_7
    :goto_7
    return-void

    .line 450
    :cond_8
    invoke-virtual {p1}, Lo/ad;->f()Lo/T;

    move-result-object v1

    .line 451
    invoke-virtual {p1}, Lo/ad;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    mul-float/2addr v0, p4

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 453
    invoke-virtual {p1}, Lo/ad;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    mul-float/2addr v2, p4

    int-to-float v4, p3

    div-float/2addr v2, v4

    .line 455
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 456
    const/high16 v2, 0x41f0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v0, v4

    sget v4, Lcom/google/android/maps/driveabout/vector/bk;->b:F

    mul-float/2addr v0, v4

    sub-float/2addr v2, v0

    .line 458
    new-instance v0, LC/b;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 459
    invoke-virtual {p0, v0, p5}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    goto :goto_7
.end method

.method public a(Lw/c;)V
    .registers 3
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0, p1}, Lw/b;->a(Lw/c;)V

    .line 317
    return-void
.end method

.method protected a(ZZZII)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->e:Lcom/google/android/maps/driveabout/vector/bm;

    if-eqz v0, :cond_e

    .line 283
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->e:Lcom/google/android/maps/driveabout/vector/bm;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bm;->a(ZZZII)V

    .line 286
    :cond_e
    return-void
.end method

.method public a_(LC/a;)I
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 632
    .line 633
    monitor-enter p0

    .line 634
    :try_start_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v1, v1, Lw/d;

    if-eqz v1, :cond_6a

    .line 635
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lw/d;

    .line 637
    invoke-interface {v0}, Lw/d;->c()I

    move-result v1

    .line 638
    invoke-interface {v0, p1}, Lw/d;->a(LC/a;)LC/c;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    .line 639
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v3}, LC/c;->b()LC/b;

    move-result-object v3

    invoke-virtual {v2, v3}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 641
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 647
    invoke-interface {v0}, Lw/d;->c()I

    move-result v0

    or-int/2addr v0, v1

    .line 662
    :goto_2c
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-virtual {p1, v1}, LC/a;->a(LC/b;)V

    .line 664
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->m:Z

    if-eqz v1, :cond_5b

    if-nez v0, :cond_5b

    .line 665
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->r:Lcom/google/android/maps/driveabout/vector/aE;

    if-eqz v1, :cond_58

    .line 666
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->n:Lg/a;

    if-nez v1, :cond_4b

    .line 670
    new-instance v1, Lcom/google/android/maps/driveabout/vector/bs;

    new-instance v2, LA/a;

    invoke-direct {v2}, LA/a;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/vector/bs;-><init>(LA/b;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->n:Lg/a;

    .line 675
    :cond_4b
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->n:Lg/a;

    invoke-interface {v1, p1}, Lg/a;->a(LC/a;)Ljava/util/List;

    move-result-object v1

    .line 677
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->r:Lcom/google/android/maps/driveabout/vector/aE;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    invoke-interface {v2, v3, v1}, Lcom/google/android/maps/driveabout/vector/aE;->a(LC/b;Ljava/util/List;)V

    .line 680
    :cond_58
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->m:Z

    .line 682
    :cond_5b
    monitor-exit p0
    :try_end_5c
    .catchall {:try_start_2 .. :try_end_5c} :catchall_87

    .line 684
    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_69

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->p:Lcom/google/android/maps/driveabout/vector/bB;

    if-eqz v1, :cond_69

    .line 686
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->p:Lcom/google/android/maps/driveabout/vector/bB;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/bB;->a(LC/a;)V

    .line 689
    :cond_69
    return v0

    .line 656
    :cond_6a
    :try_start_6a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v2}, LC/c;->b()LC/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    .line 658
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 659
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->l:Z

    .line 660
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_2c

    .line 682
    :catchall_87
    move-exception v0

    monitor-exit p0
    :try_end_89
    .catchall {:try_start_6a .. :try_end_89} :catchall_87

    throw v0
.end method

.method public declared-synchronized b(FFF)F
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 902
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bp;

    if-eqz v0, :cond_1a

    .line 903
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bp;

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_36

    .line 908
    :goto_18
    monitor-exit p0

    return v0

    .line 905
    :cond_1a
    :try_start_1a
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bp;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bp;-><init>(LC/b;Lw/b;)V

    .line 907
    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    .line 908
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/bp;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_35
    .catchall {:try_start_1a .. :try_end_35} :catchall_36

    goto :goto_18

    .line 902
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FF)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 747
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/bn;

    if-nez v0, :cond_17

    .line 748
    new-instance v0, Lcom/google/android/maps/driveabout/vector/bn;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    invoke-interface {v1}, LC/c;->b()LC/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/bn;-><init>(LC/b;)V

    .line 749
    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    .line 751
    :cond_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/bn;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/bn;->a(FF)V

    .line 755
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    float-to-int v4, p1

    float-to-int v5, p2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(ZZZII)V
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    .line 756
    monitor-exit p0

    return-void

    .line 747
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FI)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 938
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->f:Z

    if-eqz v0, :cond_29

    .line 939
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/br;

    if-eqz v0, :cond_14

    .line 940
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->k:LC/c;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/br;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/br;->a(F)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_26

    .line 955
    :goto_12
    monitor-exit p0

    return-void

    .line 942
    :cond_14
    :try_start_14
    new-instance v0, Lcom/google/android/maps/driveabout/vector/br;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/br;-><init>(LC/b;Lw/b;)V

    .line 944
    sget-object v1, Lcom/google/android/maps/driveabout/vector/bk;->c:LC/b;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;LC/b;)V

    .line 945
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/br;->a(F)V
    :try_end_25
    .catchall {:try_start_14 .. :try_end_25} :catchall_26

    goto :goto_12

    .line 938
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0

    .line 948
    :cond_29
    :try_start_29
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    .line 949
    invoke-virtual {v5}, LC/b;->d()F

    move-result v0

    add-float v3, v0, p1

    .line 950
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    new-instance v0, LC/b;

    invoke-virtual {v5}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v5}, LC/b;->a()F

    move-result v2

    invoke-virtual {v5}, LC/b;->e()F

    move-result v4

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-virtual {v6, v0}, Lw/b;->a(LC/b;)LC/b;

    move-result-object v0

    .line 953
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V
    :try_end_4f
    .catchall {:try_start_29 .. :try_end_4f} :catchall_26

    goto :goto_12
.end method

.method public c()LC/b;
    .registers 2

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->i:LC/b;

    return-object v0
.end method

.method public c(F)V
    .registers 3
    .parameter

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0, p1}, Lw/b;->a(F)V

    .line 378
    return-void
.end method

.method public d(F)V
    .registers 2
    .parameter

    .prologue
    .line 403
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/bk;->u:F

    .line 404
    return-void
.end method

.method public e()F
    .registers 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->g:Lw/b;

    invoke-virtual {v0}, Lw/b;->a()Lw/c;

    move-result-object v0

    .line 372
    if-nez v0, :cond_b

    const/high16 v0, 0x4000

    :goto_a
    return v0

    :cond_b
    invoke-interface {v0}, Lw/c;->a()F

    move-result v0

    goto :goto_a
.end method

.method public f()LC/b;
    .registers 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->h:LC/b;

    return-object v0
.end method

.method public g()F
    .registers 2

    .prologue
    .line 393
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->j:F

    return v0
.end method

.method public declared-synchronized h()V
    .registers 7

    .prologue
    .line 772
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->q:Lcom/google/android/maps/driveabout/vector/bq;

    if-eqz v0, :cond_a

    .line 773
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->q:Lcom/google/android/maps/driveabout/vector/bq;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bq;->a()V

    .line 775
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v2

    .line 776
    new-instance v0, LC/b;

    invoke-virtual {v2}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {v2}, LC/b;->a()F

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    .line 778
    const/16 v1, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_25

    .line 779
    monitor-exit p0

    return-void

    .line 772
    :catchall_25
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 959
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bk;->l:Z

    return v0
.end method
