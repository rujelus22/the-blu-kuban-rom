.class public Lcom/google/android/maps/driveabout/app/cT;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/app/Service;

.field protected b:Landroid/app/Notification;

.field private final c:Landroid/app/NotificationManager;

.field private final d:Landroid/content/Intent;

.field private final e:Landroid/app/PendingIntent;

.field private f:Z

.field private g:Landroid/app/PendingIntent;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private j:[LO/U;

.field private k:I

.field private l:[LO/b;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    .line 65
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->c:Landroid/app/NotificationManager;

    .line 66
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    .line 67
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    .line 68
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->e:Landroid/app/PendingIntent;

    .line 69
    return-void
.end method

.method private constructor <init>(Landroid/app/Service;)V
    .registers 7
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    .line 45
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->c:Landroid/app/NotificationManager;

    .line 48
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    const-class v2, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-static {}, Lcom/google/android/maps/driveabout/app/bn;->l()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 51
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    const/4 v2, 0x0

    const/high16 v3, 0x800

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->e:Landroid/app/PendingIntent;

    .line 54
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f0202d0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    .line 55
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 57
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    const-class v2, Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    .line 58
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 60
    return-void
.end method

.method private static a(Z)I
    .registers 2
    .parameter

    .prologue
    .line 140
    if-eqz p0, :cond_6

    .line 141
    const v0, 0x7f0202d0

    .line 143
    :goto_5
    return v0

    :cond_6
    const v0, 0x7f0202d1

    goto :goto_5
.end method

.method public static a(Landroid/app/Service;)Lcom/google/android/maps/driveabout/app/cT;
    .registers 2
    .parameter

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/maps/driveabout/app/cT;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/cT;-><init>(Landroid/app/Service;)V

    return-object v0
.end method

.method private a(ILjava/lang/CharSequence;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 73
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4e

    .line 74
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->h:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->g:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 80
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_47

    .line 81
    const v1, 0x1080038

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cT;->e:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 85
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    .line 87
    :cond_47
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    .line 92
    :goto_4d
    return-void

    .line 89
    :cond_4e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 90
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->h:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/cT;->g:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, p2, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_4d
.end method

.method private a([LO/U;I[LO/b;)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 204
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->j:[LO/U;

    if-ne p1, v1, :cond_e

    iget v1, p0, Lcom/google/android/maps/driveabout/app/cT;->k:I

    if-ne p2, v1, :cond_e

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->l:[LO/b;

    if-ne p3, v1, :cond_e

    .line 219
    :goto_d
    return v0

    .line 208
    :cond_e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    invoke-static {p1, p2, p3}, Lcom/google/android/maps/driveabout/app/bn;->a([LO/U;I[LO/b;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 210
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    const/high16 v3, 0x800

    invoke-static {v1, v0, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->g:Landroid/app/PendingIntent;

    .line 213
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    .line 214
    invoke-static {}, Lcom/google/android/maps/driveabout/app/dx;->a()Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v1

    .line 215
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dx;->a(LO/U;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->h:Ljava/lang/CharSequence;

    .line 216
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/cT;->j:[LO/U;

    .line 217
    iput p2, p0, Lcom/google/android/maps/driveabout/app/cT;->k:I

    .line 218
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/cT;->l:[LO/b;

    .line 219
    const/4 v0, 0x1

    goto :goto_d
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cT;->d()V

    .line 152
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->c:Landroid/app/NotificationManager;

    const/16 v1, 0x174f

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 153
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 154
    return-void
.end method

.method public a([LO/U;I[LO/b;Ljava/lang/CharSequence;ZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-static {p5}, Lcom/google/android/maps/driveabout/app/cT;->a(Z)I

    move-result v0

    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/cT;->a([LO/U;I[LO/b;)Z

    move-result v1

    .line 115
    if-nez v1, :cond_1c

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    iget v1, v1, Landroid/app/Notification;->icon:I

    if-ne v0, v1, :cond_1c

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/cT;->i:Ljava/lang/CharSequence;

    invoke-static {p4, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/app/cT;->f:Z

    if-eq p6, v1, :cond_2f

    .line 118
    :cond_1c
    invoke-direct {p0, v0, p4}, Lcom/google/android/maps/driveabout/app/cT;->a(ILjava/lang/CharSequence;)V

    .line 119
    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/cT;->i:Ljava/lang/CharSequence;

    .line 122
    if-eqz p6, :cond_30

    .line 123
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cT;->c()V

    .line 129
    :goto_26
    :try_start_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->c:Landroid/app/NotificationManager;

    const/16 v1, 0x174f

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_2f
    .catch Ljava/lang/RuntimeException; {:try_start_26 .. :try_end_2f} :catch_34

    .line 136
    :cond_2f
    :goto_2f
    return-void

    .line 125
    :cond_30
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/cT;->d()V

    goto :goto_26

    .line 130
    :catch_34
    move-exception v0

    goto :goto_2f
.end method

.method public b()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->d:Landroid/content/Intent;

    return-object v0
.end method

.method public c()V
    .registers 4

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cT;->f:Z

    if-eqz v0, :cond_5

    .line 174
    :goto_4
    return-void

    .line 172
    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    const/16 v1, 0x174f

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/cT;->b:Landroid/app/Notification;

    invoke-virtual {v0, v1, v2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    .line 173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cT;->f:Z

    goto :goto_4
.end method

.method public d()V
    .registers 3

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cT;->f:Z

    if-nez v0, :cond_5

    .line 189
    :goto_4
    return-void

    .line 187
    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/cT;->a:Landroid/app/Service;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopForeground(Z)V

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/cT;->f:Z

    goto :goto_4
.end method
