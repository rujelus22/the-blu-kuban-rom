.class Lcom/google/android/maps/driveabout/publisher/f;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;


# direct methods
.method constructor <init>(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)V
    .registers 2
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/maps/driveabout/publisher/f;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter

    .prologue
    .line 107
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_38

    .line 129
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 131
    :goto_8
    return-void

    .line 109
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/f;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->b(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 112
    :pswitch_15
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/f;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->b(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_8

    .line 116
    :pswitch_21
    :try_start_21
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/f;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;Landroid/os/Messenger;I)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_29} :catch_2a

    goto :goto_8

    .line 117
    :catch_2a
    move-exception v0

    goto :goto_8

    .line 123
    :pswitch_2c
    :try_start_2c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/publisher/f;->a:Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;

    iget-object v1, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;->a(Lcom/google/android/maps/driveabout/publisher/NavigationEventPublisherService;Landroid/os/Messenger;I)V
    :try_end_34
    .catch Landroid/os/RemoteException; {:try_start_2c .. :try_end_34} :catch_35

    goto :goto_8

    .line 124
    :catch_35
    move-exception v0

    goto :goto_8

    .line 107
    nop

    :pswitch_data_38
    .packed-switch 0x2
        :pswitch_9
        :pswitch_15
        :pswitch_21
        :pswitch_2c
    .end packed-switch
.end method
