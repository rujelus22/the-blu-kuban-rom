.class Lcom/google/android/maps/driveabout/vector/bp;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private volatile h:Z

.field private final i:Lw/b;

.field private final j:[F


# direct methods
.method public constructor <init>(LC/b;Lw/b;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1256
    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    .line 1253
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F

    .line 1257
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/bp;->i:Lw/b;

    .line 1258
    return-void
.end method


# virtual methods
.method public a(LC/a;)LC/c;
    .registers 13
    .parameter

    .prologue
    .line 1296
    monitor-enter p0

    .line 1297
    :try_start_1
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/bp;->f:F

    .line 1298
    iget v5, p0, Lcom/google/android/maps/driveabout/vector/bp;->g:F

    .line 1299
    iget v7, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    .line 1300
    iget v8, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    .line 1302
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_76

    .line 1303
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c8

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 1315
    :goto_21
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_88

    .line 1316
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    mul-float/2addr v1, v2

    const v2, -0x42333333

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v6, v0

    .line 1324
    :goto_38
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    sub-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    .line 1325
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    sub-float/2addr v0, v6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    .line 1326
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    .line 1327
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    .line 1330
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_99

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v9, 0x3f50624dd2f1a9fcL

    cmpg-double v0, v0, v9

    if-gez v0, :cond_99

    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_99

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-nez v0, :cond_99

    .line 1332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->h:Z

    .line 1333
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    monitor-exit p0

    move-object p0, v0

    .line 1370
    :cond_75
    :goto_75
    return-object p0

    .line 1306
    :cond_76
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x42c8

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_21

    .line 1319
    :cond_88
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    mul-float/2addr v1, v2

    const v2, 0x3dcccccd

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move v6, v0

    goto :goto_38

    .line 1335
    :cond_99
    monitor-exit p0
    :try_end_9a
    .catchall {:try_start_1 .. :try_end_9a} :catchall_e9

    .line 1338
    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_a4

    const/4 v0, 0x0

    cmpl-float v0, v8, v0

    if-eqz v0, :cond_ec

    :cond_a4
    const/4 v0, 0x1

    move v2, v0

    .line 1339
    :goto_a6
    const/4 v0, 0x0

    cmpl-float v0, v6, v0

    if-eqz v0, :cond_ef

    const/4 v0, 0x1

    move v1, v0

    .line 1340
    :goto_ad
    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_f2

    const/4 v0, 0x1

    .line 1343
    :goto_b3
    if-eqz v2, :cond_c6

    .line 1344
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-static {v2, p1, v7, v8}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;FF)LC/b;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    .line 1347
    if-nez v1, :cond_c1

    if-eqz v0, :cond_c6

    .line 1348
    :cond_c1
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {p1, v2}, LC/a;->a(LC/b;)V

    .line 1353
    :cond_c6
    if-eqz v1, :cond_db

    .line 1355
    invoke-virtual {p1, v4, v5}, LC/a;->d(FF)Lo/T;

    move-result-object v1

    .line 1358
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->i:Lw/b;

    invoke-static {p1, v2, v1, v6}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/a;Lw/b;Lo/T;F)LC/b;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    .line 1361
    if-eqz v0, :cond_db

    .line 1362
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {p1, v1}, LC/a;->a(LC/b;)V

    .line 1367
    :cond_db
    if-eqz v0, :cond_75

    .line 1368
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->i:Lw/b;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/b;LC/a;Lw/b;FFF)LC/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    goto :goto_75

    .line 1335
    :catchall_e9
    move-exception v0

    :try_start_ea
    monitor-exit p0
    :try_end_eb
    .catchall {:try_start_ea .. :try_end_eb} :catchall_e9

    throw v0

    .line 1338
    :cond_ec
    const/4 v0, 0x0

    move v2, v0

    goto :goto_a6

    .line 1339
    :cond_ef
    const/4 v0, 0x0

    move v1, v0

    goto :goto_ad

    .line 1340
    :cond_f2
    const/4 v0, 0x0

    goto :goto_b3
.end method

.method declared-synchronized a(FFFFFF)[F
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1268
    monitor-enter p0

    :try_start_2
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    .line 1269
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    .line 1270
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    add-float/2addr v0, p5

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->d:F

    .line 1271
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    add-float/2addr v0, p6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->e:F

    .line 1278
    cmpl-float v0, p1, v1

    if-nez v0, :cond_1e

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_22

    .line 1279
    :cond_1e
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/bp;->f:F

    .line 1280
    iput p4, p0, Lcom/google/android/maps/driveabout/vector/bp;->g:F

    .line 1283
    :cond_22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->h:Z

    .line 1286
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {v2}, LC/b;->a()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bp;->b:F

    add-float/2addr v2, v3

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bk;->j()F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    const/high16 v3, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aput v2, v0, v1

    .line 1288
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bp;->a:LC/b;

    invoke-virtual {v2}, LC/b;->e()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/bp;->c:F

    add-float/2addr v2, v3

    invoke-static {v2}, Lcom/google/android/maps/driveabout/vector/bk;->e(F)F

    move-result v2

    aput v2, v0, v1

    .line 1289
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->j:[F
    :try_end_55
    .catchall {:try_start_2 .. :try_end_55} :catchall_57

    monitor-exit p0

    return-object v0

    .line 1268
    :catchall_57
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 1375
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/bp;->h:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method
