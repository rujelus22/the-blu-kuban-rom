.class abstract Lcom/google/android/maps/driveabout/vector/dm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z


# direct methods
.method private constructor <init>(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 276
    iput v1, p0, Lcom/google/android/maps/driveabout/vector/dm;->b:I

    .line 277
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->c:Ljava/lang/String;

    .line 278
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dm;->d:Z

    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->e:Z

    .line 280
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dm;->f:Z

    .line 281
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dm;->g:Z

    .line 284
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->a:I

    .line 285
    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/maps/driveabout/vector/dj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dm;-><init>(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/dm;)I
    .registers 2
    .parameter

    .prologue
    .line 274
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->a:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/vector/dm;)I
    .registers 2
    .parameter

    .prologue
    .line 274
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->b:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/vector/dm;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/vector/dm;)Z
    .registers 2
    .parameter

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->d:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/vector/dm;)Z
    .registers 2
    .parameter

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->e:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/maps/driveabout/vector/dm;)Z
    .registers 2
    .parameter

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/maps/driveabout/vector/dm;)Z
    .registers 2
    .parameter

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dm;->g:Z

    return v0
.end method


# virtual methods
.method abstract a()Lcom/google/android/maps/driveabout/vector/di;
.end method

.method a(I)Lcom/google/android/maps/driveabout/vector/dm;
    .registers 2
    .parameter

    .prologue
    .line 288
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->b:I

    .line 289
    return-object p0
.end method

.method a(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/dm;
    .registers 2
    .parameter

    .prologue
    .line 293
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->c:Ljava/lang/String;

    .line 294
    return-object p0
.end method

.method b(Z)Lcom/google/android/maps/driveabout/vector/dm;
    .registers 2
    .parameter

    .prologue
    .line 298
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->d:Z

    .line 299
    return-object p0
.end method

.method c(Z)Lcom/google/android/maps/driveabout/vector/dm;
    .registers 2
    .parameter

    .prologue
    .line 303
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->e:Z

    .line 304
    return-object p0
.end method

.method d(Z)Lcom/google/android/maps/driveabout/vector/dm;
    .registers 2
    .parameter

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->f:Z

    .line 309
    return-object p0
.end method

.method e(Z)Lcom/google/android/maps/driveabout/vector/dm;
    .registers 2
    .parameter

    .prologue
    .line 313
    iput-boolean p1, p0, Lcom/google/android/maps/driveabout/vector/dm;->g:Z

    .line 314
    return-object p0
.end method
