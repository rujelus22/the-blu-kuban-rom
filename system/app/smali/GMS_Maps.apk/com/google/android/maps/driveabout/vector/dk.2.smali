.class Lcom/google/android/maps/driveabout/vector/dk;
.super Lcom/google/android/maps/driveabout/vector/dx;
.source "SourceFile"


# instance fields
.field private final A:Z


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/dl;)V
    .registers 3
    .parameter

    .prologue
    .line 625
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/dx;-><init>(Lcom/google/android/maps/driveabout/vector/dy;Lcom/google/android/maps/driveabout/vector/dj;)V

    .line 627
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dl;->a(Lcom/google/android/maps/driveabout/vector/dl;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dk;->A:Z

    .line 628
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/dl;Lcom/google/android/maps/driveabout/vector/dj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dk;-><init>(Lcom/google/android/maps/driveabout/vector/dl;)V

    return-void
.end method


# virtual methods
.method public a(ILcom/google/android/maps/driveabout/vector/D;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 640
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dk;->A:Z

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->b:Lcom/google/android/maps/driveabout/vector/D;

    if-ne p2, v0, :cond_11

    .line 641
    :cond_8
    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->e:Lcom/google/android/maps/driveabout/vector/D;

    if-eq p2, v0, :cond_10

    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->d:Lcom/google/android/maps/driveabout/vector/D;

    if-ne p2, v0, :cond_12

    .line 642
    :cond_10
    const/4 p1, 0x0

    .line 651
    :cond_11
    :goto_11
    return p1

    .line 643
    :cond_12
    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->a:Lcom/google/android/maps/driveabout/vector/D;

    if-eq p2, v0, :cond_11

    sget-object v0, Lcom/google/android/maps/driveabout/vector/D;->c:Lcom/google/android/maps/driveabout/vector/D;

    if-eq p2, v0, :cond_11

    .line 644
    and-int/lit16 p1, p1, -0x1a07

    goto :goto_11
.end method

.method public a(Lo/aa;)Lo/Q;
    .registers 3
    .parameter

    .prologue
    .line 667
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lo/aa;->a(I)Lo/Q;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljavax/microedition/khronos/opengles/GL10;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 662
    const v0, 0x3e99999a

    invoke-interface {p1, v1, v1, v1, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4f(FFFF)V

    .line 663
    return-void
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 632
    const/4 v0, 0x1

    return v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 656
    const/4 v0, 0x1

    return v0
.end method

.method public j()Lcom/google/android/maps/driveabout/vector/aJ;
    .registers 4

    .prologue
    .line 672
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aJ;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->f:Lcom/google/android/maps/driveabout/vector/aI;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/aj;->b:Lcom/google/android/maps/driveabout/vector/aj;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/aJ;-><init>(Lcom/google/android/maps/driveabout/vector/aI;Lcom/google/android/maps/driveabout/vector/aj;)V

    return-object v0
.end method

.method public k()Lo/ag;
    .registers 2

    .prologue
    .line 678
    invoke-static {}, Lcom/google/android/maps/driveabout/vector/di;->m()Lo/ag;

    move-result-object v0

    return-object v0
.end method
