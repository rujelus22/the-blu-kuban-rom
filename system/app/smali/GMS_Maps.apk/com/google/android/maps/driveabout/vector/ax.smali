.class public Lcom/google/android/maps/driveabout/vector/ax;
.super Lcom/google/android/maps/driveabout/vector/d;
.source "SourceFile"


# instance fields
.field private final d:Ljava/util/ArrayList;

.field private e:Ly/d;

.field private final f:I


# direct methods
.method public constructor <init>(ILcom/google/android/maps/driveabout/vector/x;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p2}, Lcom/google/android/maps/driveabout/vector/d;-><init>(Lcom/google/android/maps/driveabout/vector/x;)V

    .line 44
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    .line 46
    return-void
.end method

.method private e()V
    .registers 5

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 102
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->e:Ly/d;

    invoke-virtual {v0}, Ly/d;->d()Ly/g;

    move-result-object v0

    .line 103
    :cond_b
    :goto_b
    invoke-virtual {v0}, Ly/g;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 104
    invoke-virtual {v0}, Ly/g;->a()LF/m;

    move-result-object v1

    .line 105
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_20

    invoke-virtual {v1}, LF/m;->w()Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_20
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2b

    invoke-virtual {v1}, LF/m;->w()Z

    move-result v2

    if-nez v2, :cond_b

    .line 109
    :cond_2b
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 111
    :cond_31
    return-void
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->e:Ly/d;

    if-eqz v0, :cond_1a

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->b()I

    move-result v0

    if-gtz v0, :cond_1a

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->f:Lcom/google/android/maps/driveabout/vector/q;

    if-eq v0, v1, :cond_1a

    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->a()Lcom/google/android/maps/driveabout/vector/q;

    move-result-object v0

    sget-object v1, Lcom/google/android/maps/driveabout/vector/q;->e:Lcom/google/android/maps/driveabout/vector/q;

    if-ne v0, v1, :cond_1b

    .line 96
    :cond_1a
    :goto_1a
    return-void

    .line 75
    :cond_1b
    invoke-virtual {p1}, LD/a;->p()V

    .line 76
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 77
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/16 v3, 0x2100

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 83
    monitor-enter p0

    .line 84
    :try_start_36
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/ax;->e()V

    .line 85
    const/4 v0, 0x0

    move v1, v0

    :goto_3b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_58

    .line 86
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 87
    invoke-virtual {p1}, LD/a;->A()V

    .line 88
    invoke-virtual {v0, p1, p2, p3}, LF/m;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    .line 89
    invoke-virtual {p1}, LD/a;->B()V

    .line 85
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3b

    .line 91
    :cond_58
    monitor-exit p0

    goto :goto_1a

    :catchall_5a
    move-exception v0

    monitor-exit p0
    :try_end_5c
    .catchall {:try_start_36 .. :try_end_5c} :catchall_5a

    throw v0
.end method

.method public declared-synchronized a(Ljava/util/List;FFLo/T;LC/a;I)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 116
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LF/m;

    .line 117
    instance-of v2, v0, Lcom/google/android/maps/driveabout/vector/c;

    if-eqz v2, :cond_7

    .line 120
    check-cast v0, Lcom/google/android/maps/driveabout/vector/c;

    .line 121
    invoke-interface {v0, p2, p3, p4, p5}, Lcom/google/android/maps/driveabout/vector/c;->a(FFLo/T;LC/a;)I

    move-result v2

    .line 122
    if-ge v2, p6, :cond_7

    .line 123
    new-instance v3, Lcom/google/android/maps/driveabout/vector/t;

    invoke-direct {v3, v0, p0, v2}, Lcom/google/android/maps/driveabout/vector/t;-><init>(Lcom/google/android/maps/driveabout/vector/c;Lcom/google/android/maps/driveabout/vector/d;I)V

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_28

    goto :goto_7

    .line 116
    :catchall_28
    move-exception v0

    monitor-exit p0

    throw v0

    .line 126
    :cond_2b
    monitor-exit p0

    return-void
.end method

.method public a(Ly/d;)V
    .registers 2
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/ax;->e:Ly/d;

    .line 53
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .registers 3

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/ax;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_8

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->p:Lcom/google/android/maps/driveabout/vector/E;

    :goto_7
    return-object v0

    :cond_8
    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->l:Lcom/google/android/maps/driveabout/vector/E;

    goto :goto_7
.end method
