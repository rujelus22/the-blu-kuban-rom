.class public final enum Lcom/google/android/maps/driveabout/vector/cx;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum b:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum c:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum d:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum e:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum f:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum g:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum h:Lcom/google/android/maps/driveabout/vector/cx;

.field public static final enum i:Lcom/google/android/maps/driveabout/vector/cx;

.field private static final synthetic j:[Lcom/google/android/maps/driveabout/vector/cx;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "BASE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->a:Lcom/google/android/maps/driveabout/vector/cx;

    .line 32
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "DROP_SHADOWS_OUTER"

    invoke-direct {v0, v1, v4}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->b:Lcom/google/android/maps/driveabout/vector/cx;

    .line 38
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "ELEVATED_COLOR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->c:Lcom/google/android/maps/driveabout/vector/cx;

    .line 44
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "UNDERGROUND_MODE_MASK"

    invoke-direct {v0, v1, v6}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->d:Lcom/google/android/maps/driveabout/vector/cx;

    .line 50
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "UNDERGROUND_STENCIL"

    invoke-direct {v0, v1, v7}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->e:Lcom/google/android/maps/driveabout/vector/cx;

    .line 56
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "UNDERGROUND_COLOR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->f:Lcom/google/android/maps/driveabout/vector/cx;

    .line 58
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "DROP_SHADOWS_INNER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->g:Lcom/google/android/maps/driveabout/vector/cx;

    .line 63
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "ANIMATED_ELEVATED_COLOR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->h:Lcom/google/android/maps/driveabout/vector/cx;

    .line 69
    new-instance v0, Lcom/google/android/maps/driveabout/vector/cx;

    const-string v1, "DEFAULT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->i:Lcom/google/android/maps/driveabout/vector/cx;

    .line 26
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/maps/driveabout/vector/cx;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cx;->a:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cx;->b:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cx;->c:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cx;->d:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/maps/driveabout/vector/cx;->e:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cx;->f:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cx;->g:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cx;->h:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/maps/driveabout/vector/cx;->i:Lcom/google/android/maps/driveabout/vector/cx;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/vector/cx;->j:[Lcom/google/android/maps/driveabout/vector/cx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/maps/driveabout/vector/cx;
    .registers 2
    .parameter

    .prologue
    .line 26
    const-class v0, Lcom/google/android/maps/driveabout/vector/cx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/driveabout/vector/cx;

    return-object v0
.end method

.method public static values()[Lcom/google/android/maps/driveabout/vector/cx;
    .registers 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/maps/driveabout/vector/cx;->j:[Lcom/google/android/maps/driveabout/vector/cx;

    invoke-virtual {v0}, [Lcom/google/android/maps/driveabout/vector/cx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/maps/driveabout/vector/cx;

    return-object v0
.end method
