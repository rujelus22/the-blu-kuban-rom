.class public Lcom/google/android/maps/driveabout/vector/dK;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/b;


# static fields
.field private static final b:F

.field private static final c:Lcom/google/android/maps/driveabout/vector/l;

.field private static d:F


# instance fields
.field protected final a:Landroid/content/res/Resources;

.field private e:Lcom/google/android/maps/driveabout/vector/dM;

.field private final f:Z

.field private final g:Lcom/google/android/maps/driveabout/vector/n;

.field private volatile h:Lcom/google/android/maps/driveabout/vector/l;

.field private volatile i:Lcom/google/android/maps/driveabout/vector/l;

.field private volatile j:F

.field private volatile k:Lcom/google/android/maps/driveabout/vector/m;

.field private volatile l:Z

.field private m:Z

.field private n:Lcom/google/android/maps/driveabout/vector/da;

.field private o:Lcom/google/android/maps/driveabout/vector/cJ;

.field private p:Lcom/google/android/maps/driveabout/vector/eh;

.field private q:Lcom/google/android/maps/driveabout/vector/dQ;

.field private r:Lcom/google/android/maps/driveabout/vector/ct;

.field private s:Z

.field private t:I

.field private u:F

.field private final v:Lm/q;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 69
    const-wide/high16 v0, 0x3ff0

    const-wide/high16 v2, 0x4000

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/maps/driveabout/vector/dK;->b:F

    .line 116
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    .line 119
    const/high16 v0, 0x41a8

    sput v0, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->t:I

    .line 216
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->u:F

    .line 247
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->a:Landroid/content/res/Resources;

    .line 248
    sget-object v0, Lcom/google/android/maps/driveabout/vector/k;->b:Lcom/google/android/maps/driveabout/vector/l;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 249
    sget-object v0, Lcom/google/android/maps/driveabout/vector/k;->b:Lcom/google/android/maps/driveabout/vector/l;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    .line 251
    if-eqz p1, :cond_3e

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 252
    :goto_1d
    new-instance v2, Lcom/google/android/maps/driveabout/vector/n;

    int-to-float v0, v0

    invoke-direct {v2, v0}, Lcom/google/android/maps/driveabout/vector/n;-><init>(F)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    .line 255
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    if-eqz v0, :cond_40

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->am()Z

    move-result v0

    if-eqz v0, :cond_40

    :goto_35
    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->f:Z

    .line 258
    invoke-static {}, Lm/q;->a()Lm/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->v:Lm/q;

    .line 259
    return-void

    :cond_3e
    move v0, v1

    .line 251
    goto :goto_1d

    .line 255
    :cond_40
    const/4 v1, 0x0

    goto :goto_35
.end method

.method private a(F)F
    .registers 10
    .parameter

    .prologue
    .line 611
    const-wide/high16 v0, 0x4000

    const-wide v2, 0x40031eb851eb851fL

    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    sget v6, Lcom/google/android/maps/driveabout/vector/dK;->b:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide v4, 0x404d5ae147ae147bL

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/n;Lo/Q;F)Lcom/google/android/maps/driveabout/vector/l;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1525
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/k;->b()Lo/Q;

    move-result-object v0

    .line 1526
    invoke-virtual {v0}, Lo/Q;->f()I

    move-result v1

    invoke-virtual {p2}, Lo/Q;->f()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1527
    invoke-virtual {v0}, Lo/Q;->g()I

    move-result v0

    invoke-virtual {p2}, Lo/Q;->g()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1528
    float-to-double v2, p3

    const-wide v4, 0x400921fb54442d18L

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L

    div-double/2addr v2, v4

    double-to-float v2, v2

    .line 1529
    neg-float v3, v2

    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v3

    .line 1530
    neg-float v2, v2

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    .line 1531
    int-to-float v4, v1

    mul-float/2addr v4, v2

    int-to-float v5, v0

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    .line 1532
    int-to-float v1, v1

    mul-float/2addr v1, v3

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 1533
    new-instance v1, Lo/Q;

    invoke-virtual {p2}, Lo/Q;->f()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {p2}, Lo/Q;->g()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Lo/Q;-><init>(II)V

    .line 1537
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/k;->j()F

    move-result v0

    add-float/2addr v0, p3

    invoke-static {v0}, Lcom/google/android/maps/driveabout/vector/dK;->f(F)F

    move-result v4

    .line 1538
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/k;->l()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/k;->k()F

    move-result v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/k;FF)Lcom/google/android/maps/driveabout/vector/l;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1493
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->s()F

    move-result v0

    mul-float/2addr v0, p2

    .line 1494
    neg-float v1, p3

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->s()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->k()F

    move-result v2

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v2

    div-float/2addr v1, v2

    .line 1497
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->o()Lo/Q;

    move-result-object v2

    .line 1498
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->p()Lo/Q;

    move-result-object v3

    .line 1499
    new-instance v4, Lo/Q;

    invoke-virtual {v2}, Lo/Q;->f()I

    move-result v5

    invoke-virtual {v2}, Lo/Q;->g()I

    move-result v2

    invoke-direct {v4, v5, v2}, Lo/Q;-><init>(II)V

    .line 1500
    new-instance v5, Lo/Q;

    invoke-virtual {v3}, Lo/Q;->f()I

    move-result v2

    invoke-virtual {v3}, Lo/Q;->g()I

    move-result v3

    invoke-direct {v5, v2, v3}, Lo/Q;-><init>(II)V

    .line 1501
    invoke-static {v4, v0, v4}, Lo/Q;->b(Lo/Q;FLo/Q;)V

    .line 1502
    invoke-static {v5, v1, v5}, Lo/Q;->b(Lo/Q;FLo/Q;)V

    .line 1504
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->b()Lo/Q;

    move-result-object v0

    .line 1505
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    .line 1506
    invoke-virtual {v0}, Lo/Q;->h()I

    move-result v3

    .line 1507
    invoke-virtual {v0, v4}, Lo/Q;->e(Lo/Q;)Lo/Q;

    move-result-object v1

    .line 1508
    invoke-static {v1, v5, v1}, Lo/Q;->a(Lo/Q;Lo/Q;Lo/Q;)V

    .line 1509
    invoke-virtual {v1, v3}, Lo/Q;->b(I)V

    .line 1510
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    return-object v0
.end method

.method public static a(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/k;Lcom/google/android/maps/driveabout/vector/n;FFF)Lcom/google/android/maps/driveabout/vector/l;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x4000

    .line 1551
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->e()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v6, p4, v0

    .line 1552
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/vector/k;->f()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    sub-float v7, p5, v0

    .line 1561
    invoke-static {p0, p1, v6, v7}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/k;FF)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v4

    .line 1563
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v1

    sget v2, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v3

    add-float/2addr v3, p3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 1566
    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 1567
    neg-float v1, v6

    neg-float v2, v7

    invoke-static {v0, p1, v1, v2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/k;FF)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/m;)V
    .registers 3
    .parameter

    .prologue
    .line 596
    sget-object v0, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    .line 600
    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/F;

    if-eqz v0, :cond_e

    .line 601
    check-cast p1, Lcom/google/android/maps/driveabout/vector/F;

    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/F;->b()V

    .line 603
    :cond_e
    return-void
.end method

.method private a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 462
    monitor-enter p0

    .line 463
    :try_start_2
    instance-of v0, p1, Lcom/google/android/maps/driveabout/vector/F;

    if-eqz v0, :cond_9

    .line 464
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->l:Z

    .line 466
    :cond_9
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    .line 467
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->m:Z

    .line 468
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 470
    if-eqz p2, :cond_24

    .line 471
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-virtual {v0, p2}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object p2

    .line 474
    :cond_24
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/dK;->i:Lcom/google/android/maps/driveabout/vector/l;

    .line 475
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 476
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_2 .. :try_end_2a} :catchall_35

    .line 477
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->o:Lcom/google/android/maps/driveabout/vector/cJ;

    if-eqz v0, :cond_34

    .line 478
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->o:Lcom/google/android/maps/driveabout/vector/cJ;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/cJ;->a(ZZ)V

    .line 480
    :cond_34
    return-void

    .line 476
    :catchall_35
    move-exception v0

    :try_start_36
    monitor-exit p0
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    throw v0
.end method

.method public static b(F)V
    .registers 1
    .parameter

    .prologue
    .line 296
    sput p0, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    .line 297
    return-void
.end method

.method public static d()F
    .registers 1

    .prologue
    .line 288
    sget v0, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    return v0
.end method

.method static synthetic e(F)F
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/dK;->f(F)F

    move-result v0

    return v0
.end method

.method private static f(F)F
    .registers 8
    .parameter

    .prologue
    const-wide v5, 0x4076800000000000L

    .line 1571
    move v0, p0

    :goto_6
    float-to-double v1, v0

    cmpl-double v1, v1, v5

    if-ltz v1, :cond_f

    .line 1572
    float-to-double v0, v0

    sub-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_6

    .line 1574
    :cond_f
    :goto_f
    float-to-double v1, v0

    const-wide/16 v3, 0x0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1a

    .line 1575
    float-to-double v0, v0

    add-double/2addr v0, v5

    double-to-float v0, v0

    goto :goto_f

    .line 1577
    :cond_1a
    return v0
.end method

.method static synthetic j()F
    .registers 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    return v0
.end method


# virtual methods
.method public declared-synchronized a(FFF)F
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 780
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/dP;

    if-eqz v0, :cond_1c

    .line 781
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dP;

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dP;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 789
    :goto_18
    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->j:F
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_3d

    .line 790
    monitor-exit p0

    return v0

    .line 783
    :cond_1c
    :try_start_1c
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dP;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dP;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/n;)V

    .line 785
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dP;->a(FFFFFF)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 786
    sget-object v2, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V
    :try_end_3b
    .catchall {:try_start_1c .. :try_end_3b} :catchall_3d

    move v0, v1

    goto :goto_18

    .line 780
    :catchall_3d
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(FFFI)F
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 841
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 842
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->j:F

    .line 843
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dO;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dO;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/n;FFFI)V

    sget-object v2, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    .line 846
    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    add-float/2addr v0, p1

    sget v1, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public a(FI)F
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 820
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 821
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    add-float/2addr v2, p1

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    invoke-virtual {v6, v0}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    .line 825
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V

    .line 826
    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v0

    return v0
.end method

.method public a(Lo/Q;)F
    .registers 3
    .parameter

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/n;->a()Lcom/google/android/maps/driveabout/vector/o;

    move-result-object v0

    .line 355
    if-nez v0, :cond_b

    sget v0, Lcom/google/android/maps/driveabout/vector/dK;->d:F

    :goto_a
    return v0

    :cond_b
    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/vector/o;->a(Lo/Q;)F

    move-result v0

    goto :goto_a
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/k;)I
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 621
    .line 622
    monitor-enter p0

    .line 623
    :try_start_2
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    instance-of v1, v1, Lcom/google/android/maps/driveabout/vector/F;

    if-eqz v1, :cond_6a

    .line 624
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/F;

    .line 626
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/F;->a()I

    move-result v1

    .line 627
    invoke-interface {v0, p1}, Lcom/google/android/maps/driveabout/vector/F;->a(Lcom/google/android/maps/driveabout/vector/k;)Lcom/google/android/maps/driveabout/vector/m;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    .line 628
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 630
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 636
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/F;->a()I

    move-result v0

    or-int/2addr v0, v1

    .line 651
    :goto_2c
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/vector/k;->a(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 653
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->m:Z

    if-eqz v1, :cond_5b

    if-nez v0, :cond_5b

    .line 654
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->r:Lcom/google/android/maps/driveabout/vector/ct;

    if-eqz v1, :cond_58

    .line 655
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->n:Lcom/google/android/maps/driveabout/vector/da;

    if-nez v1, :cond_4b

    .line 659
    new-instance v1, Lcom/google/android/maps/driveabout/vector/dS;

    new-instance v2, Lcom/google/android/maps/driveabout/vector/cl;

    invoke-direct {v2}, Lcom/google/android/maps/driveabout/vector/cl;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/maps/driveabout/vector/dS;-><init>(Lcom/google/android/maps/driveabout/vector/dh;)V

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->n:Lcom/google/android/maps/driveabout/vector/da;

    .line 664
    :cond_4b
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->n:Lcom/google/android/maps/driveabout/vector/da;

    invoke-interface {v1, p1}, Lcom/google/android/maps/driveabout/vector/da;->a(Lcom/google/android/maps/driveabout/vector/k;)Ljava/util/List;

    move-result-object v1

    .line 666
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->r:Lcom/google/android/maps/driveabout/vector/ct;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    invoke-interface {v2, v3, v1}, Lcom/google/android/maps/driveabout/vector/ct;->a(Lcom/google/android/maps/driveabout/vector/l;Ljava/util/List;)V

    .line 669
    :cond_58
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->m:Z

    .line 671
    :cond_5b
    monitor-exit p0
    :try_end_5c
    .catchall {:try_start_2 .. :try_end_5c} :catchall_87

    .line 673
    and-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_69

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->p:Lcom/google/android/maps/driveabout/vector/eh;

    if-eqz v1, :cond_69

    .line 675
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->p:Lcom/google/android/maps/driveabout/vector/eh;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/driveabout/vector/eh;->a(Lcom/google/android/maps/driveabout/vector/k;)V

    .line 678
    :cond_69
    return v0

    .line 645
    :cond_6a
    :try_start_6a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    .line 647
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 648
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->l:Z

    .line 649
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_2c

    .line 671
    :catchall_87
    move-exception v0

    monitor-exit p0
    :try_end_89
    .catchall {:try_start_6a .. :try_end_89} :catchall_87

    throw v0
.end method

.method public declared-synchronized a(FF)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 719
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/dP;

    if-eqz v0, :cond_16

    .line 720
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dP;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dP;->a(FFFFFF)[F
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_32

    .line 727
    :goto_14
    monitor-exit p0

    return-void

    .line 722
    :cond_16
    :try_start_16
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dP;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dP;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/n;)V

    .line 724
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dP;->a(FFFFFF)[F

    .line 725
    sget-object v1, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V
    :try_end_31
    .catchall {:try_start_16 .. :try_end_31} :catchall_32

    goto :goto_14

    .line 719
    :catchall_32
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/cJ;)V
    .registers 3
    .parameter

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->o:Lcom/google/android/maps/driveabout/vector/cJ;

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->s:Z

    .line 302
    return-void
.end method

.method public declared-synchronized a(Lcom/google/android/maps/driveabout/vector/ct;)V
    .registers 3
    .parameter

    .prologue
    .line 326
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->r:Lcom/google/android/maps/driveabout/vector/ct;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 327
    monitor-exit p0

    return-void

    .line 326
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/dM;)V
    .registers 2
    .parameter

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->e:Lcom/google/android/maps/driveabout/vector/dM;

    .line 263
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/dQ;)V
    .registers 2
    .parameter

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->q:Lcom/google/android/maps/driveabout/vector/dQ;

    .line 335
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/eh;)V
    .registers 2
    .parameter

    .prologue
    .line 330
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->p:Lcom/google/android/maps/driveabout/vector/eh;

    .line 331
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/m;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 485
    .line 486
    if-nez p2, :cond_7

    const/4 v0, 0x0

    .line 487
    :goto_3
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;II)V

    .line 488
    return-void

    .line 486
    :cond_7
    const/4 v0, -0x1

    goto :goto_3
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/m;II)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, -0x1

    const/high16 v9, 0x442f

    const/high16 v8, 0x4100

    const/4 v0, 0x0

    .line 506
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->s:Z

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 592
    :goto_12
    return-void

    .line 509
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->s:Z

    if-eqz v1, :cond_19

    move p3, v0

    move p2, v0

    .line 514
    :cond_19
    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->s:Z

    .line 516
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 517
    invoke-interface {p1}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/maps/driveabout/vector/l;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v6

    .line 519
    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    iput v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->j:F

    .line 521
    invoke-static {}, Lu/m;->a()Lu/k;

    move-result-object v2

    .line 522
    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 523
    invoke-virtual {v2}, Lu/k;->y()Z

    move-result v4

    if-eqz v4, :cond_4b

    invoke-virtual {v2}, Lu/k;->z()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v3, v2

    if-lez v2, :cond_4f

    .line 525
    :cond_4b
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;)V

    goto :goto_12

    .line 530
    :cond_4f
    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v4

    add-float/2addr v2, v4

    const/high16 v4, 0x3f00

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 531
    const/16 v4, 0x1e

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 532
    const/high16 v4, 0x4000

    shr-int v2, v4, v2

    .line 533
    invoke-virtual {v6}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v5

    invoke-virtual {v4, v5}, Lo/Q;->c(Lo/Q;)F

    move-result v5

    .line 534
    int-to-float v4, v2

    div-float v7, v5, v4

    .line 535
    iget v4, p0, Lcom/google/android/maps/driveabout/vector/dK;->t:I

    int-to-float v4, v4

    cmpg-float v4, v7, v4

    if-gtz v4, :cond_88

    const/4 v4, 0x1

    .line 537
    :goto_80
    if-eqz v4, :cond_a7

    .line 538
    if-nez p2, :cond_8a

    .line 539
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;)V

    goto :goto_12

    :cond_88
    move v4, v0

    .line 535
    goto :goto_80

    .line 542
    :cond_8a
    if-ne p2, v10, :cond_e5

    .line 544
    invoke-static {v3, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 545
    div-float/2addr v0, v8

    .line 546
    const/high16 v2, 0x3f40

    mul-float/2addr v0, v2

    const/high16 v2, 0x3fc0

    add-float/2addr v0, v2

    .line 549
    mul-float/2addr v0, v9

    float-to-int v3, v0

    .line 552
    :goto_99
    const/4 v5, 0x0

    .line 553
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dL;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dL;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/m;IZF)V

    sget-object v1, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    goto/16 :goto_12

    .line 558
    :cond_a7
    if-nez p3, :cond_ae

    .line 559
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;)V

    goto/16 :goto_12

    .line 562
    :cond_ae
    if-ne p3, v10, :cond_e3

    .line 568
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->t:I

    int-to-float v0, v0

    sub-float v0, v7, v0

    const/high16 v3, 0x4e80

    int-to-float v2, v2

    div-float v2, v3, v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/dK;->t:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    div-float/2addr v0, v2

    .line 570
    const v2, 0x40833333

    mul-float/2addr v0, v2

    const v2, 0x3fb33333

    add-float/2addr v0, v2

    .line 572
    mul-float/2addr v0, v9

    float-to-int v0, v0

    const/16 v2, 0x9c4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 577
    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->u:F

    mul-float/2addr v0, v2

    float-to-int v3, v0

    .line 580
    :goto_d4
    invoke-direct {p0, v5}, Lcom/google/android/maps/driveabout/vector/dK;->a(F)F

    move-result v5

    .line 586
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dL;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dL;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/m;IZF)V

    invoke-direct {p0, v0, v6}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    goto/16 :goto_12

    :cond_e3
    move v3, p3

    goto :goto_d4

    :cond_e5
    move v3, p2

    goto :goto_99
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/o;)V
    .registers 3
    .parameter

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/o;)V

    .line 306
    return-void
.end method

.method public a(Lo/B;)V
    .registers 3
    .parameter

    .prologue
    .line 1590
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->v:Lm/q;

    invoke-virtual {v0, p1}, Lm/q;->a(Lo/B;)V

    .line 1591
    return-void
.end method

.method public a(Lo/Q;I)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 400
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 401
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    const/4 v5, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 403
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V

    .line 404
    return-void
.end method

.method public a(Lo/aa;IIFI)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x4380

    const/4 v3, 0x0

    .line 436
    if-eqz p2, :cond_7

    if-nez p3, :cond_8

    .line 449
    :cond_7
    :goto_7
    return-void

    .line 439
    :cond_8
    invoke-virtual {p1}, Lo/aa;->f()Lo/Q;

    move-result-object v1

    .line 440
    invoke-virtual {p1}, Lo/aa;->g()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    mul-float/2addr v0, p4

    int-to-float v2, p2

    div-float/2addr v0, v2

    .line 442
    invoke-virtual {p1}, Lo/aa;->h()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v4

    mul-float/2addr v2, p4

    int-to-float v4, p3

    div-float/2addr v2, v4

    .line 444
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 445
    const/high16 v2, 0x41f0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    double-to-float v0, v4

    sget v4, Lcom/google/android/maps/driveabout/vector/dK;->b:F

    mul-float/2addr v0, v4

    sub-float/2addr v2, v0

    .line 447
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 448
    invoke-virtual {p0, v0, p5}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V

    goto :goto_7
.end method

.method protected a(ZZZII)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->e:Lcom/google/android/maps/driveabout/vector/dM;

    if-eqz v0, :cond_e

    .line 272
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->e:Lcom/google/android/maps/driveabout/vector/dM;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dM;->a(ZZZII)V

    .line 275
    :cond_e
    return-void
.end method

.method public declared-synchronized b(FFF)F
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 891
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/dP;

    if-eqz v0, :cond_1a

    .line 892
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dP;

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dP;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_36

    .line 897
    :goto_18
    monitor-exit p0

    return v0

    .line 894
    :cond_1a
    :try_start_1a
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dP;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dP;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/n;)V

    .line 896
    sget-object v1, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    .line 897
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v2, p3

    move v3, p1

    move v4, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/dP;->a(FFFFFF)[F

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1
    :try_end_35
    .catchall {:try_start_1a .. :try_end_35} :catchall_36

    goto :goto_18

    .line 891
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FF)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 736
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/dN;

    if-nez v0, :cond_17

    .line 737
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dN;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/m;->c()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/dN;-><init>(Lcom/google/android/maps/driveabout/vector/l;)V

    .line 738
    sget-object v1, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    .line 740
    :cond_17
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dN;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/dN;->a(FF)V

    .line 744
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    float-to-int v4, p1

    float-to-int v5, p2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/dK;->a(ZZZII)V
    :try_end_27
    .catchall {:try_start_1 .. :try_end_27} :catchall_29

    .line 745
    monitor-exit p0

    return-void

    .line 736
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(FI)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 927
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->f:Z

    if-eqz v0, :cond_29

    .line 928
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/dR;

    if-eqz v0, :cond_14

    .line 929
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->k:Lcom/google/android/maps/driveabout/vector/m;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/dR;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/dR;->a(F)V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_26

    .line 944
    :goto_12
    monitor-exit p0

    return-void

    .line 931
    :cond_14
    :try_start_14
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dR;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dR;-><init>(Lcom/google/android/maps/driveabout/vector/l;Lcom/google/android/maps/driveabout/vector/n;)V

    .line 933
    sget-object v1, Lcom/google/android/maps/driveabout/vector/dK;->c:Lcom/google/android/maps/driveabout/vector/l;

    invoke-direct {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;Lcom/google/android/maps/driveabout/vector/l;)V

    .line 934
    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/dR;->a(F)V
    :try_end_25
    .catchall {:try_start_14 .. :try_end_25} :catchall_26

    goto :goto_12

    .line 927
    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0

    .line 937
    :cond_29
    :try_start_29
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    .line 938
    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->d()F

    move-result v0

    add-float v3, v0, p1

    .line 939
    iget-object v6, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->e()F

    move-result v4

    invoke-virtual {v5}, Lcom/google/android/maps/driveabout/vector/l;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    invoke-virtual {v6, v0}, Lcom/google/android/maps/driveabout/vector/n;->a(Lcom/google/android/maps/driveabout/vector/l;)Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v0

    .line 942
    invoke-virtual {p0, v0, p2}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V
    :try_end_4f
    .catchall {:try_start_29 .. :try_end_4f} :catchall_26

    goto :goto_12
.end method

.method public c()Lcom/google/android/maps/driveabout/vector/l;
    .registers 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->i:Lcom/google/android/maps/driveabout/vector/l;

    return-object v0
.end method

.method public c(F)V
    .registers 3
    .parameter

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/n;->a(F)V

    .line 367
    return-void
.end method

.method public d(F)V
    .registers 2
    .parameter

    .prologue
    .line 392
    iput p1, p0, Lcom/google/android/maps/driveabout/vector/dK;->u:F

    .line 393
    return-void
.end method

.method public e()F
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->g:Lcom/google/android/maps/driveabout/vector/n;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/n;->a()Lcom/google/android/maps/driveabout/vector/o;

    move-result-object v0

    .line 361
    if-nez v0, :cond_b

    const/high16 v0, 0x4000

    :goto_a
    return v0

    :cond_b
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/o;->a()F

    move-result v0

    goto :goto_a
.end method

.method public f()Lcom/google/android/maps/driveabout/vector/l;
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->h:Lcom/google/android/maps/driveabout/vector/l;

    return-object v0
.end method

.method public g()F
    .registers 2

    .prologue
    .line 382
    iget v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->j:F

    return v0
.end method

.method public declared-synchronized h()V
    .registers 7

    .prologue
    .line 761
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->q:Lcom/google/android/maps/driveabout/vector/dQ;

    if-eqz v0, :cond_a

    .line 762
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->q:Lcom/google/android/maps/driveabout/vector/dQ;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dQ;->a()V

    .line 764
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/vector/dK;->f()Lcom/google/android/maps/driveabout/vector/l;

    move-result-object v2

    .line 765
    new-instance v0, Lcom/google/android/maps/driveabout/vector/l;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/l;->b()Lo/Q;

    move-result-object v1

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/l;->a()F

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/l;-><init>(Lo/Q;FFFF)V

    .line 767
    const/16 v1, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(Lcom/google/android/maps/driveabout/vector/m;I)V
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_25

    .line 768
    monitor-exit p0

    return-void

    .line 761
    :catchall_25
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public i()Z
    .registers 2

    .prologue
    .line 948
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dK;->l:Z

    return v0
.end method
