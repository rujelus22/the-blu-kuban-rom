.class Lcom/google/android/maps/driveabout/app/dh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private g:LO/z;

.field private final h:Z


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dh;->a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    .line 42
    const v0, 0x7f100138

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->c:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f100124

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->d:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f100139

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->e:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f10013a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->f:Landroid/view/View;

    .line 48
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    new-instance v1, Lcom/google/android/maps/driveabout/app/di;

    invoke-direct {v1, p0, p1}, Lcom/google/android/maps/driveabout/app/di;-><init>(Lcom/google/android/maps/driveabout/app/dh;Lcom/google/android/maps/driveabout/app/RouteSelectorView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iput-boolean p3, p0, Lcom/google/android/maps/driveabout/app/dh;->h:Z

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;ZLcom/google/android/maps/driveabout/app/df;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/maps/driveabout/app/dh;-><init>(Lcom/google/android/maps/driveabout/app/RouteSelectorView;Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dh;)LO/z;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->g:LO/z;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 107
    return-void
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 75
    if-gez p1, :cond_b

    .line 76
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    :goto_a
    return-void

    .line 78
    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dh;->a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b(Lcom/google/android/maps/driveabout/app/RouteSelectorView;)Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v1

    invoke-virtual {v1, p1, p2, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(IIZ)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_a
.end method

.method public a(IZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 86
    if-eqz p2, :cond_14

    .line 87
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->f:Landroid/view/View;

    if-eqz v0, :cond_13

    .line 89
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    :cond_13
    :goto_13
    return-void

    .line 92
    :cond_14
    if-gez p1, :cond_25

    .line 93
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    :goto_1b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->f:Landroid/view/View;

    if-eqz v0, :cond_13

    .line 100
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_13

    .line 95
    :cond_25
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dh;->a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b(Lcom/google/android/maps/driveabout/app/RouteSelectorView;)Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v1

    invoke-virtual {v1, p1, v2}, Lcom/google/android/maps/driveabout/app/dx;->a(IZ)Landroid/text/Spannable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_1b
.end method

.method public a(LO/z;)V
    .registers 6
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dh;->g:LO/z;

    .line 129
    invoke-virtual {p1}, LO/z;->p()I

    move-result v0

    invoke-virtual {p1}, LO/z;->q()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dh;->a(II)V

    .line 131
    invoke-virtual {p1}, LO/z;->o()I

    move-result v0

    invoke-virtual {p1}, LO/z;->y()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/driveabout/app/dh;->a(IZ)V

    .line 132
    invoke-virtual {p1}, LO/z;->w()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_37

    .line 133
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->g:LO/z;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dh;->a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1}, LO/z;->p()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LO/z;->a(Landroid/content/Context;II)LO/E;

    move-result-object v0

    invoke-virtual {v0}, LO/E;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dh;->a(Ljava/lang/String;)V

    .line 138
    :goto_36
    return-void

    .line 136
    :cond_37
    invoke-virtual {p1}, LO/z;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dh;->a(Ljava/lang/String;)V

    goto :goto_36
.end method

.method public a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 59
    if-nez p1, :cond_b

    .line 60
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 72
    :goto_a
    return-void

    .line 62
    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dh;->h:Z

    if-eqz v0, :cond_26

    .line 65
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0092

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 67
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->g:LO/z;

    invoke-virtual {v0}, LO/z;->E()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 68
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->a:Lcom/google/android/maps/driveabout/app/RouteSelectorView;

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/RouteSelectorView;->b(Lcom/google/android/maps/driveabout/app/RouteSelectorView;)Lcom/google/android/maps/driveabout/app/dx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/dx;->a(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object p1

    .line 70
    :cond_38
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/eD;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)Z

    goto :goto_a
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p1, :cond_24

    const v0, 0x7f090079

    :goto_b
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 117
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dh;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 118
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dh;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 119
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dh;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    .line 121
    return-void

    .line 114
    :cond_24
    const v0, 0x7f09007a

    goto :goto_b
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dh;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 125
    return-void
.end method
