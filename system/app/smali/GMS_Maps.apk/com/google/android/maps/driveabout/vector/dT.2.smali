.class public Lcom/google/android/maps/driveabout/vector/dT;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "SourceFile"

# interfaces
.implements Lak/m;


# static fields
.field private static final c:F

.field private static final d:F

.field private static final e:D


# instance fields
.field private final a:F

.field private final b:F

.field private final f:Lcom/google/android/maps/driveabout/vector/dV;

.field private g:Lcom/google/android/maps/driveabout/vector/dW;

.field private h:Lcom/google/android/maps/driveabout/vector/dW;

.field private i:Landroid/view/MotionEvent;

.field private j:F

.field private k:F

.field private l:Lcom/google/android/maps/driveabout/vector/dU;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 144
    sget-boolean v0, Lcom/google/googlenav/android/E;->g:Z

    if-eqz v0, :cond_19

    const v0, 0x3f7f3b64

    :goto_7
    sput v0, Lcom/google/android/maps/driveabout/vector/dT;->c:F

    .line 150
    const/high16 v0, 0x3f80

    sget v1, Lcom/google/android/maps/driveabout/vector/dT;->c:F

    div-float/2addr v0, v1

    sput v0, Lcom/google/android/maps/driveabout/vector/dT;->d:F

    .line 152
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/driveabout/vector/dT;->e:D

    return-void

    .line 144
    :cond_19
    const v0, 0x3f7fbe77

    goto :goto_7
.end method

.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/dV;)V
    .registers 4
    .parameter

    .prologue
    const/16 v1, 0x14

    .line 187
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    .line 185
    sget-object v0, Lcom/google/android/maps/driveabout/vector/dU;->a:Lcom/google/android/maps/driveabout/vector/dU;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    .line 188
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    .line 189
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dW;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/dW;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    .line 190
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->b:F

    .line 191
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->a:F

    .line 193
    return-void
.end method

.method private static b(Lak/q;)Z
    .registers 3
    .parameter

    .prologue
    .line 381
    invoke-virtual {p0}, Lak/q;->e()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lak/q;->c()F

    move-result v0

    sget v1, Lcom/google/android/maps/driveabout/vector/dT;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1c

    invoke-virtual {p0}, Lak/q;->c()F

    move-result v0

    sget v1, Lcom/google/android/maps/driveabout/vector/dT;->d:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 428
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dW;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    invoke-direct {v0, v1}, Lcom/google/android/maps/driveabout/vector/dW;-><init>(Lcom/google/android/maps/driveabout/vector/dW;)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->h:Lcom/google/android/maps/driveabout/vector/dW;

    .line 429
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 400
    const/16 v0, 0x63

    invoke-static {v0, p1}, LaU/m;->a(ILjava/lang/String;)V

    .line 401
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/dW;->a:Z

    .line 405
    return-void
.end method

.method public a(Lak/o;)Z
    .registers 6
    .parameter

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/dW;->d:Z

    if-eqz v0, :cond_32

    .line 389
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/dV;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Lak/o;->a(FF)V

    .line 390
    invoke-virtual {p1}, Lak/o;->c()F

    move-result v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    .line 391
    invoke-virtual {p1}, Lak/o;->a()F

    move-result v1

    .line 392
    invoke-virtual {p1}, Lak/o;->b()F

    move-result v2

    .line 393
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v3

    invoke-virtual {v3, v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/dK;->b(FFF)F

    .line 394
    const/4 v0, 0x1

    .line 396
    :goto_31
    return v0

    :cond_32
    const/4 v0, 0x0

    goto :goto_31
.end method

.method public a(Lak/q;)Z
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x4000

    .line 359
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/dW;->b:Z

    if-eqz v0, :cond_31

    .line 360
    invoke-virtual {p1}, Lak/q;->g()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 361
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v0

    const/high16 v1, -0x4080

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dK;->a(FI)F

    move-result v0

    .line 363
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/dV;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/dV;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/maps/driveabout/vector/dV;->a(FFF)V

    .line 377
    :cond_31
    :goto_31
    const/4 v0, 0x1

    return v0

    .line 366
    :cond_33
    invoke-virtual {p1}, Lak/q;->c()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/google/android/maps/driveabout/vector/dT;->e:D

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 368
    invoke-virtual {p1}, Lak/q;->a()F

    move-result v1

    .line 369
    invoke-virtual {p1}, Lak/q;->b()F

    move-result v2

    .line 370
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dT;->b(Lak/q;)Z

    move-result v3

    if-eqz v3, :cond_4f

    .line 371
    const/4 v0, 0x0

    .line 373
    :cond_4f
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dK;->a(FFF)F

    move-result v0

    .line 374
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v3, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->a(FFF)V

    goto :goto_31
.end method

.method public a(Lak/u;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 350
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v1, v1, Lcom/google/android/maps/driveabout/vector/dW;->c:Z

    if-eqz v1, :cond_15

    .line 351
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v1

    invoke-virtual {p1}, Lak/u;->a()F

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/maps/driveabout/vector/dK;->b(FI)V

    .line 352
    const/4 v0, 0x1

    .line 354
    :cond_15
    return v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->h:Lcom/google/android/maps/driveabout/vector/dW;

    if-eqz v0, :cond_8

    .line 433
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->h:Lcom/google/android/maps/driveabout/vector/dW;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    .line 435
    :cond_8
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 408
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/dW;->b:Z

    .line 409
    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/dW;->c:Z

    .line 413
    return-void
.end method

.method public d(Z)V
    .registers 3
    .parameter

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/dW;->d:Z

    .line 417
    return-void
.end method

.method public e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/dW;->e:Z

    .line 421
    return-void
.end method

.method public f(Z)V
    .registers 3
    .parameter

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iput-boolean p1, v0, Lcom/google/android/maps/driveabout/vector/dW;->f:Z

    .line 425
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 252
    sget-object v0, Lcom/google/android/maps/driveabout/vector/dU;->b:Lcom/google/android/maps/driveabout/vector/dU;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    .line 253
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->e()V

    .line 254
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->d(FF)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 255
    const/4 v0, 0x1

    .line 260
    :goto_1a
    return v0

    .line 257
    :cond_1b
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    .line 258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->j:F

    .line 259
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->k:F

    .line 260
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    const/high16 v6, 0x3f00

    const/4 v0, 0x1

    .line 270
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    if-eqz v2, :cond_43

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_43

    .line 271
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v2, v2, Lcom/google/android/maps/driveabout/vector/dW;->b:Z

    if-eqz v2, :cond_3d

    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    sget-object v3, Lcom/google/android/maps/driveabout/vector/dU;->b:Lcom/google/android/maps/driveabout/vector/dU;

    if-ne v2, v3, :cond_3d

    .line 273
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 274
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 275
    const/high16 v3, 0x3f80

    .line 283
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v4}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v4

    const/16 v5, 0x14a

    invoke-virtual {v4, v3, v1, v2, v5}, Lcom/google/android/maps/driveabout/vector/dK;->a(FFFI)F

    move-result v3

    .line 285
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v4, v3, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->a(FFF)V

    .line 286
    iput-object v7, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    .line 287
    sget-object v1, Lcom/google/android/maps/driveabout/vector/dU;->a:Lcom/google/android/maps/driveabout/vector/dU;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    .line 337
    :cond_3c
    :goto_3c
    return v0

    .line 290
    :cond_3d
    iput-object v7, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    .line 291
    sget-object v2, Lcom/google/android/maps/driveabout/vector/dU;->a:Lcom/google/android/maps/driveabout/vector/dU;

    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    .line 295
    :cond_43
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    if-eqz v2, :cond_127

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_127

    .line 296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->k:F

    sub-float/2addr v2, v3

    .line 297
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->j:F

    sub-float/2addr v3, v4

    .line 299
    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    sget-object v5, Lcom/google/android/maps/driveabout/vector/dU;->b:Lcom/google/android/maps/driveabout/vector/dU;

    if-ne v4, v5, :cond_ab

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/google/android/maps/driveabout/vector/dT;->b:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_ab

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->b:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_ab

    .line 301
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    .line 302
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 303
    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->a:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3c

    .line 304
    sget-object v3, Lcom/google/android/maps/driveabout/vector/dU;->c:Lcom/google/android/maps/driveabout/vector/dU;

    iput-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    .line 305
    const-string v3, "d"

    invoke-virtual {p0, v3}, Lcom/google/android/maps/driveabout/vector/dT;->a(Ljava/lang/String;)V

    .line 318
    :cond_ab
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    sget-object v4, Lcom/google/android/maps/driveabout/vector/dU;->c:Lcom/google/android/maps/driveabout/vector/dU;

    if-ne v3, v4, :cond_e6

    .line 319
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/dV;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x40c0

    mul-float/2addr v2, v3

    .line 320
    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v3}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/google/android/maps/driveabout/vector/dK;->a(FI)F

    move-result v1

    .line 321
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-interface {v2, v1, v3, v4}, Lcom/google/android/maps/driveabout/vector/dV;->a(FFF)V

    .line 333
    :cond_d8
    :goto_d8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->j:F

    .line 334
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->k:F

    goto/16 :goto_3c

    .line 322
    :cond_e6
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    sget-object v2, Lcom/google/android/maps/driveabout/vector/dU;->d:Lcom/google/android/maps/driveabout/vector/dU;

    if-ne v1, v2, :cond_d8

    .line 323
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v1}, Lcom/google/android/maps/driveabout/vector/dV;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    .line 324
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v2}, Lcom/google/android/maps/driveabout/vector/dV;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v6

    .line 325
    iget v3, p0, Lcom/google/android/maps/driveabout/vector/dT;->j:F

    iget v4, p0, Lcom/google/android/maps/driveabout/vector/dT;->k:F

    invoke-static {v1, v2, v3, v4}, Lak/j;->a(FFFF)F

    move-result v3

    .line 327
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v1, v2, v4, v5}, Lak/j;->a(FFFF)F

    move-result v4

    .line 330
    iget-object v5, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v5}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v5

    sub-float v3, v4, v3

    const/high16 v4, 0x4334

    mul-float/2addr v3, v4

    float-to-double v3, v3

    const-wide v6, 0x400921fb54442d18L

    div-double/2addr v3, v6

    double-to-float v3, v3

    invoke-virtual {v5, v1, v2, v3}, Lcom/google/android/maps/driveabout/vector/dK;->b(FFF)F

    goto :goto_d8

    :cond_127
    move v0, v1

    .line 337
    goto/16 :goto_3c
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->l:Lcom/google/android/maps/driveabout/vector/dU;

    sget-object v1, Lcom/google/android/maps/driveabout/vector/dU;->a:Lcom/google/android/maps/driveabout/vector/dU;

    if-ne v0, v1, :cond_13

    .line 200
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->a(FF)V

    .line 202
    :cond_13
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/dW;->a:Z

    if-eqz v0, :cond_1a

    .line 233
    invoke-virtual {p0, p2}, Lcom/google/android/maps/driveabout/vector/dT;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 234
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/dK;->b(FF)V

    .line 235
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->e()V

    .line 238
    :cond_1a
    const/4 v0, 0x1

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->i:Landroid/view/MotionEvent;

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/dW;->e:Z

    if-eqz v0, :cond_17

    .line 344
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->e(FF)V

    .line 346
    :cond_17
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 213
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/maps/driveabout/vector/dV;->a(Landroid/view/MotionEvent;FF)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 222
    :cond_9
    :goto_9
    return v1

    .line 215
    :cond_a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/dW;->a:Z

    if-eqz v0, :cond_9

    .line 217
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->c_()Lcom/google/android/maps/driveabout/vector/dK;

    move-result-object v0

    .line 218
    invoke-virtual {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/dK;->a(FF)V

    .line 219
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0, p3, p4}, Lcom/google/android/maps/driveabout/vector/dV;->f(FF)V

    .line 220
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->e()V

    goto :goto_9
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->g:Lcom/google/android/maps/driveabout/vector/dW;

    iget-boolean v0, v0, Lcom/google/android/maps/driveabout/vector/dW;->f:Z

    if-eqz v0, :cond_18

    .line 244
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/dV;->e()V

    .line 245
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->c(FF)V

    .line 247
    :cond_18
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 5
    .parameter

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/dT;->f:Lcom/google/android/maps/driveabout/vector/dV;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/dV;->b(FF)Z

    move-result v0

    return v0
.end method
