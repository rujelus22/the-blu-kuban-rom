.class Lcom/google/android/maps/driveabout/vector/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/aJ;


# instance fields
.field private final a:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .registers 3
    .parameter

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    invoke-static {p1}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/at;->a:Ljava/util/Set;

    .line 263
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/maps/driveabout/vector/aJ;)I
    .registers 3
    .parameter

    .prologue
    .line 294
    const/4 v0, 0x0

    return v0
.end method

.method public a()Ljava/util/Set;
    .registers 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/at;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;Lo/T;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x80

    const/16 v3, 0x1e01

    .line 272
    invoke-virtual {p1}, LD/a;->y()Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v0

    .line 273
    invoke-interface {p3}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v1

    .line 274
    sget-object v2, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v1, v2, :cond_31

    .line 275
    invoke-virtual {p1}, LD/a;->w()V

    .line 276
    invoke-interface {v0, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glStencilOp(III)V

    .line 277
    const/16 v1, 0x207

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilFunc(III)V

    .line 278
    invoke-interface {v0, v4}, Ljavax/microedition/khronos/opengles/GL10;->glStencilMask(I)V

    .line 279
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvx(III)V

    .line 280
    const v1, -0x9f9fa0

    invoke-static {v0, v1}, Lx/d;->a(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 282
    :cond_31
    return-void
.end method

.method public a(LD/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 286
    invoke-interface {p2}, Lcom/google/android/maps/driveabout/vector/r;->c()Lcom/google/android/maps/driveabout/vector/aH;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/aH;->b()Lcom/google/android/maps/driveabout/vector/aI;

    move-result-object v0

    .line 287
    sget-object v1, Lcom/google/android/maps/driveabout/vector/aI;->e:Lcom/google/android/maps/driveabout/vector/aI;

    if-ne v0, v1, :cond_f

    .line 288
    invoke-virtual {p1}, LD/a;->x()V

    .line 290
    :cond_f
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 257
    check-cast p1, Lcom/google/android/maps/driveabout/vector/aJ;

    invoke-virtual {p0, p1}, Lcom/google/android/maps/driveabout/vector/at;->a(Lcom/google/android/maps/driveabout/vector/aJ;)I

    move-result v0

    return v0
.end method
