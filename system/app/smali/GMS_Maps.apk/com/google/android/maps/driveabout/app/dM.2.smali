.class public Lcom/google/android/maps/driveabout/app/dM;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

.field private c:Lcom/google/android/maps/driveabout/app/cS;

.field private d:Lcom/google/android/maps/driveabout/app/NavigationView;

.field private e:Lcom/google/android/maps/driveabout/app/cQ;

.field private f:Lcom/google/android/maps/driveabout/app/cQ;

.field private g:Lcom/google/android/maps/driveabout/app/aQ;

.field private h:LQ/p;

.field private i:Ll/f;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Lcom/google/android/maps/driveabout/app/eB;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/eB;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->a:Landroid/os/Handler;

    .line 99
    return-void
.end method

.method private B()V
    .registers 4

    .prologue
    .line 520
    new-instance v0, Lcom/google/android/maps/driveabout/app/dN;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dN;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    .line 526
    new-instance v1, Lcom/google/android/maps/driveabout/app/dY;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dY;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    .line 534
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-interface {v2, v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->a(Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 535
    return-void
.end method

.method private final C()V
    .registers 2

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->j()V

    .line 1108
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/dM;)LQ/p;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/maps/driveabout/app/dM;)Lcom/google/android/maps/driveabout/app/NavigationActivity;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    return-object v0
.end method

.method private b(Lcom/google/android/maps/driveabout/app/NavigationView;)V
    .registers 6
    .parameter

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLayerManager(Lcom/google/android/maps/driveabout/app/bC;)V

    .line 732
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/et;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/et;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setOnSizeChangedListener(Lcom/google/android/maps/driveabout/app/cv;)V

    .line 745
    new-instance v0, Lcom/google/android/maps/driveabout/app/eu;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eu;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMapGestureListener(Lcom/google/android/maps/driveabout/vector/bz;)V

    .line 769
    new-instance v0, Lcom/google/android/maps/driveabout/app/ev;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ev;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setNavigationImageViewClickListener(Landroid/view/View$OnClickListener;)V

    .line 776
    new-instance v0, Lcom/google/android/maps/driveabout/app/ew;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ew;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLoadingDialogListeners(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 786
    new-instance v0, Lcom/google/android/maps/driveabout/app/ex;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ex;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setListItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 797
    new-instance v0, Lcom/google/android/maps/driveabout/app/ey;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ey;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    .line 804
    new-instance v1, Lcom/google/android/maps/driveabout/app/dO;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dO;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    .line 814
    new-instance v2, Lcom/google/android/maps/driveabout/app/dP;

    invoke-direct {v2, p0}, Lcom/google/android/maps/driveabout/app/dP;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    .line 824
    new-instance v3, Lcom/google/android/maps/driveabout/app/dQ;

    invoke-direct {v3, p0}, Lcom/google/android/maps/driveabout/app/dQ;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    .line 832
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/maps/driveabout/app/NavigationView;->setTopBarClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/maps/driveabout/app/dg;)V

    .line 835
    new-instance v0, Lcom/google/android/maps/driveabout/app/dR;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dR;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMuteButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 843
    new-instance v0, Lcom/google/android/maps/driveabout/app/dS;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dS;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    new-instance v1, Lcom/google/android/maps/driveabout/app/dT;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/dT;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setZoomButtonClickListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 859
    new-instance v0, Lcom/google/android/maps/driveabout/app/dU;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dU;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setAlternateRouteButtonTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 909
    new-instance v0, Lcom/google/android/maps/driveabout/app/dV;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dV;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteOptionsButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 916
    new-instance v0, Lcom/google/android/maps/driveabout/app/dW;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dW;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setStreetViewButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 923
    new-instance v0, Lcom/google/android/maps/driveabout/app/dX;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dX;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setStreetViewLaunchButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 932
    new-instance v0, Lcom/google/android/maps/driveabout/app/dZ;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/dZ;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setCompassTapListener(Lcom/google/android/maps/driveabout/vector/F;)V

    .line 939
    new-instance v0, Lcom/google/android/maps/driveabout/app/ea;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ea;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMarkerTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 946
    new-instance v0, Lcom/google/android/maps/driveabout/app/eb;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eb;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLabelTapListener(Lcom/google/android/maps/driveabout/vector/e;)V

    .line 953
    new-instance v0, Lcom/google/android/maps/driveabout/app/ec;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ec;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setBubbleTapListener(Lcom/google/android/maps/driveabout/vector/y;)V

    .line 966
    new-instance v0, Lcom/google/android/maps/driveabout/app/ed;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ed;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setBackToMyLocationButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 974
    new-instance v0, Lcom/google/android/maps/driveabout/app/ee;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ee;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setListViewButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 981
    new-instance v0, Lcom/google/android/maps/driveabout/app/eA;

    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/driveabout/app/eA;-><init>(Lcom/google/android/maps/driveabout/app/dM;Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setMapViewKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 983
    new-instance v0, Lcom/google/android/maps/driveabout/app/ef;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ef;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setTrafficButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 990
    new-instance v0, Lcom/google/android/maps/driveabout/app/eg;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eg;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRerouteToDestinationButtonListener(Landroid/view/View$OnClickListener;)V

    .line 999
    new-instance v0, Lcom/google/android/maps/driveabout/app/eh;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eh;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setFoundDestinationButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1007
    new-instance v0, Lcom/google/android/maps/driveabout/app/ei;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ei;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setDestinationInfoButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1015
    new-instance v0, Lcom/google/android/maps/driveabout/app/ek;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ek;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteOverviewButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1022
    new-instance v0, Lcom/google/android/maps/driveabout/app/el;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/el;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteAroundTrafficConfirmButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1029
    new-instance v0, Lcom/google/android/maps/driveabout/app/em;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/em;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setRouteAroundTrafficCancelButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1036
    new-instance v0, Lcom/google/android/maps/driveabout/app/en;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/en;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setExitButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1043
    new-instance v0, Lcom/google/android/maps/driveabout/app/eo;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eo;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setCloseActivityListener(Landroid/view/View$OnClickListener;)V

    .line 1050
    new-instance v0, Lcom/google/android/maps/driveabout/app/ep;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/ep;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setDestinationButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1057
    new-instance v0, Lcom/google/android/maps/driveabout/app/eq;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/eq;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setLayersButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1064
    new-instance v0, Lcom/google/android/maps/driveabout/app/er;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/er;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setVoiceSearchButtonListener(Landroid/view/View$OnClickListener;)V

    .line 1073
    new-instance v0, Lcom/google/android/maps/driveabout/app/es;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/app/es;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setSpeechInstallButtonListener(Landroid/content/DialogInterface$OnClickListener;)V

    .line 1080
    return-void
.end method

.method static synthetic c(Lcom/google/android/maps/driveabout/app/dM;)Lcom/google/android/maps/driveabout/app/NavigationView;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/maps/driveabout/app/dM;)Lcom/google/android/maps/driveabout/app/cS;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/maps/driveabout/app/dM;)Lcom/google/android/maps/driveabout/app/aQ;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .registers 2

    .prologue
    .line 694
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->al()V

    .line 695
    return-void
.end method

.method public a()LQ/p;
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    return-object v0
.end method

.method public a(LO/N;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 496
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 497
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, LQ/s;->b(LO/N;Z)V

    .line 498
    return-void
.end method

.method public a(LO/U;)V
    .registers 4
    .parameter

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LQ/s;->a(LO/U;Z)V

    .line 690
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/cS;Lcom/google/android/maps/driveabout/app/aQ;Ll/f;LQ/p;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 110
    iput-object p2, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    .line 111
    iput-object p3, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    .line 112
    iput-object p4, p0, Lcom/google/android/maps/driveabout/app/dM;->i:Ll/f;

    .line 113
    iput-object p5, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    .line 114
    new-instance v0, Lcom/google/android/maps/driveabout/app/dv;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dv;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->f:Lcom/google/android/maps/driveabout/app/cQ;

    .line 115
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->f:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    .line 117
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    sget-object v1, LQ/x;->b:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 118
    return-void
.end method

.method public a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eC;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 682
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 683
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LQ/s;->a(Landroid/os/Bundle;Lo/ae;Lcom/google/android/maps/driveabout/app/eC;)V

    .line 685
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .registers 3
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->a(Landroid/view/ViewGroup;)V

    .line 258
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .registers 5
    .parameter

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    .line 144
    if-eqz p1, :cond_18

    .line 145
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->a()Lcom/google/android/maps/driveabout/app/NavigationView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->F()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_30

    .line 148
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setRequestedOrientation(I)V

    .line 154
    :cond_18
    :goto_18
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    sget-object v1, LQ/x;->e:LQ/x;

    invoke-virtual {v0, v1}, LQ/p;->b(LQ/x;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 155
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    sget-object v1, LQ/x;->i:LQ/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LQ/p;->a(LQ/x;Z)Z

    .line 157
    :cond_2a
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->P()V

    .line 158
    return-void

    .line 150
    :cond_30
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->setRequestedOrientation(I)V

    goto :goto_18
.end method

.method public a(Lcom/google/android/maps/driveabout/app/NavigationView;)V
    .registers 4
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eq p1, v0, :cond_b

    .line 188
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    .line 189
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-direct {p0, v0}, Lcom/google/android/maps/driveabout/app/dM;->b(Lcom/google/android/maps/driveabout/app/NavigationView;)V

    .line 191
    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->p()V

    .line 192
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    .line 193
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setMapController(Lcom/google/android/maps/driveabout/vector/bk;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-virtual {v0, v1}, LQ/p;->a(Lcom/google/android/maps/driveabout/app/cQ;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->b()V

    .line 196
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->q()V

    .line 197
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->J()V

    .line 198
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->I()V

    .line 199
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bE;)V
    .registers 3
    .parameter

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 406
    invoke-virtual {p1}, Lcom/google/android/maps/driveabout/app/bE;->h()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 407
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bC;->b(Lcom/google/android/maps/driveabout/app/bE;)V

    .line 411
    :goto_12
    return-void

    .line 409
    :cond_13
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/app/bC;->a(Lcom/google/android/maps/driveabout/app/bE;)V

    goto :goto_12
.end method

.method public a(Lcom/google/android/maps/driveabout/app/bS;)V
    .registers 3
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->b(Lcom/google/android/maps/driveabout/app/bS;)V

    .line 267
    return-void
.end method

.method public a(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1087
    return-void
.end method

.method public a(Ljava/lang/Runnable;J)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1094
    return-void
.end method

.method public a(Lo/aR;)V
    .registers 4
    .parameter

    .prologue
    .line 382
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 383
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    new-instance v1, Lcom/google/android/maps/driveabout/app/ez;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ez;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {v0, p1, v1}, LQ/s;->a(Lo/aR;Lcom/google/android/maps/driveabout/app/by;)V

    .line 385
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 284
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->c(Z)V

    .line 285
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->F()Z

    move-result v0

    if-nez v0, :cond_12

    .line 168
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    .line 176
    :goto_11
    return-void

    .line 170
    :cond_12
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->O()V

    .line 171
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->e()V

    .line 172
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->f:Lcom/google/android/maps/driveabout/app/cQ;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    .line 173
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/cS;->a()Lcom/google/android/maps/driveabout/app/cq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/maps/driveabout/app/cQ;->setMapController(Lcom/google/android/maps/driveabout/vector/bk;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->e:Lcom/google/android/maps/driveabout/app/cQ;

    invoke-virtual {v0, v1}, LQ/p;->a(Lcom/google/android/maps/driveabout/app/cQ;)V

    goto :goto_11
.end method

.method public b(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->a:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1101
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 396
    if-eqz p1, :cond_c

    .line 397
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dM;->c(Z)V

    .line 398
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dM;->d(Z)V

    .line 399
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->p()V

    .line 401
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->o()V

    .line 402
    return-void
.end method

.method public b(Lcom/google/android/maps/driveabout/app/NavigationActivity;)Z
    .registers 3
    .parameter

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 206
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    .line 207
    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    .line 208
    return-void
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 425
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 426
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->e(Z)V

    .line 427
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->j()Lcom/google/android/maps/driveabout/app/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/a;->b()Z

    move-result v0

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Z)V

    .line 275
    return-void

    .line 274
    :cond_11
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 437
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_12

    .line 438
    if-eqz p1, :cond_13

    const/4 v0, 0x1

    .line 440
    :goto_a
    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->setDefaultTrafficMode(I)V

    .line 441
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->setTrafficMode(I)V

    .line 443
    :cond_12
    return-void

    .line 438
    :cond_13
    const/4 v0, 0x2

    goto :goto_a
.end method

.method public e()V
    .registers 2

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 293
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->V()V

    .line 294
    return-void
.end method

.method public e(Z)V
    .registers 3
    .parameter

    .prologue
    .line 452
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 453
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->d(Z)V

    .line 454
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 301
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->aj()V

    .line 302
    return-void
.end method

.method public f(Z)V
    .registers 3
    .parameter

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 463
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0, p1}, LQ/s;->f(Z)V

    .line 464
    return-void
.end method

.method public g()V
    .registers 5

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->l()LO/z;

    move-result-object v1

    .line 309
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->b()LO/N;

    move-result-object v0

    .line 310
    if-nez v0, :cond_20

    .line 311
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->q()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 313
    invoke-virtual {v1}, LO/z;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LO/z;->a(I)LO/N;

    move-result-object v0

    .line 319
    :cond_20
    invoke-virtual {v0}, LO/N;->b()I

    move-result v2

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3e

    .line 320
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v1}, LO/z;->m()LO/U;

    move-result-object v1

    invoke-virtual {v1}, LO/U;->c()Lo/u;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/maps/driveabout/app/du;->a(Landroid/content/Context;LO/N;Lo/u;)V

    .line 324
    :goto_35
    return-void

    .line 315
    :cond_36
    const-string v0, "UIEventHandler"

    const-string v1, "Unable to show street view: no step"

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_35

    .line 322
    :cond_3e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-static {v1, v0}, Lcom/google/android/maps/driveabout/app/du;->a(Landroid/content/Context;LO/N;)V

    goto :goto_35
.end method

.method public g(Z)V
    .registers 3
    .parameter

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 472
    if-eqz p1, :cond_b

    .line 473
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->M()V

    .line 477
    :goto_a
    return-void

    .line 475
    :cond_b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->N()V

    goto :goto_a
.end method

.method public h()V
    .registers 2

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 332
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->W()V

    .line 333
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->X()V

    .line 351
    return-void
.end method

.method public j()V
    .registers 2

    .prologue
    .line 357
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 358
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->Q()V

    .line 359
    return-void
.end method

.method public k()V
    .registers 2

    .prologue
    .line 362
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 363
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->R()V

    .line 364
    return-void
.end method

.method public l()V
    .registers 2

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 368
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->S()V

    .line 369
    return-void
.end method

.method public m()V
    .registers 2

    .prologue
    .line 372
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 373
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->T()V

    .line 374
    return-void
.end method

.method public n()V
    .registers 2

    .prologue
    .line 377
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 378
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->U()V

    .line 379
    return-void
.end method

.method public o()V
    .registers 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_15

    .line 389
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->a()Lcom/google/android/maps/driveabout/app/NavigationMapView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationMapView;->o()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    .line 391
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dM;->a(Lo/aR;)V

    .line 393
    :cond_15
    return-void
.end method

.method public p()V
    .registers 2

    .prologue
    .line 414
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 415
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->c:Lcom/google/android/maps/driveabout/app/cS;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/cS;->k()Lcom/google/android/maps/driveabout/app/bC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/bC;->f()V

    .line 416
    return-void
.end method

.method public q()V
    .registers 2

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 485
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->M()V

    .line 486
    return-void
.end method

.method public r()Z
    .registers 3

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 506
    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/ci;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 508
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dM;->i:Ll/f;

    invoke-virtual {v0, v1}, LQ/s;->a(Ll/f;)V

    .line 509
    const/4 v0, 0x1

    .line 511
    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public s()V
    .registers 2

    .prologue
    .line 541
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 543
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->e()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 544
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->v()V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->u()V

    .line 559
    :cond_15
    :goto_15
    return-void

    .line 546
    :cond_16
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 547
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->v()V

    goto :goto_15

    .line 548
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->P()Z

    move-result v0

    if-nez v0, :cond_15

    .line 549
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 550
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->B()V

    goto :goto_15

    .line 551
    :cond_3e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    if-eqz v0, :cond_15

    .line 556
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->c()V

    goto :goto_15
.end method

.method public t()V
    .registers 2

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 566
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-eqz v0, :cond_12

    .line 567
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->b()Z

    move-result v0

    if-nez v0, :cond_13

    .line 568
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->u()V

    .line 573
    :cond_12
    :goto_12
    return-void

    .line 570
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/maps/driveabout/app/dM;->v()V

    goto :goto_12
.end method

.method public u()V
    .registers 3

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_5

    .line 603
    :goto_4
    return-void

    .line 586
    :cond_5
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->g:Lcom/google/android/maps/driveabout/app/aQ;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/aQ;->D()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 587
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->c()V

    .line 592
    :goto_12
    const-string v0, "UIEventHandler"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->a(Ljava/lang/String;)V

    .line 593
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    new-instance v1, Lcom/google/android/maps/driveabout/app/ej;

    invoke-direct {v1, p0}, Lcom/google/android/maps/driveabout/app/ej;-><init>(Lcom/google/android/maps/driveabout/app/dM;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_4

    .line 589
    :cond_22
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->h()V

    goto :goto_12
.end method

.method public v()V
    .registers 3

    .prologue
    .line 612
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_5

    .line 621
    :goto_4
    return-void

    .line 616
    :cond_5
    const-string v0, "UIEventHandler"

    invoke-static {v0}, Lcom/google/android/maps/driveabout/power/a;->b(Ljava/lang/String;)V

    .line 617
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/NavigationView;->setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 618
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->d()V

    .line 619
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->g()V

    .line 620
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->i()V

    goto :goto_4
.end method

.method public w()V
    .registers 2

    .prologue
    .line 630
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    if-nez v0, :cond_5

    .line 638
    :goto_4
    return-void

    .line 634
    :cond_5
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 635
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->d()V

    .line 636
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->i()V

    .line 637
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->d:Lcom/google/android/maps/driveabout/app/NavigationView;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationView;->f()V

    goto :goto_4
.end method

.method public x()V
    .registers 2

    .prologue
    .line 646
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 647
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->E()Z

    move-result v0

    if-nez v0, :cond_12

    .line 652
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->B()V

    .line 654
    :cond_12
    return-void
.end method

.method public y()V
    .registers 2

    .prologue
    .line 658
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 659
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->O()V

    .line 660
    return-void
.end method

.method public z()V
    .registers 2

    .prologue
    .line 664
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dM;->C()V

    .line 665
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dM;->h:LQ/p;

    invoke-virtual {v0}, LQ/p;->g()LQ/s;

    move-result-object v0

    invoke-virtual {v0}, LQ/s;->N()V

    .line 666
    return-void
.end method
