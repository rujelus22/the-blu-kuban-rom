.class public Lcom/google/android/maps/driveabout/app/dz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lo/ar;


# static fields
.field private static final a:Lo/ar;

.field private static final b:Lo/ar;


# instance fields
.field private volatile c:I

.field private volatile d:Z

.field private e:LO/z;

.field private f:Lo/X;

.field private g:I

.field private h:I

.field private i:I

.field private j:Lo/ar;

.field private final k:LR/h;

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/maps/driveabout/app/dz;->a:Lo/ar;

    .line 37
    new-instance v0, Lcom/google/android/maps/driveabout/app/dA;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/app/dA;-><init>()V

    sput-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lo/ar;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/16 v0, 0xc8

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:I

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dz;->d:Z

    .line 55
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->a:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 66
    new-instance v0, LR/h;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, LR/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->k:LR/h;

    .line 74
    return-void
.end method

.method static a(ILo/T;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 237
    invoke-virtual {p1}, Lo/T;->e()D

    move-result-wide v0

    .line 238
    int-to-double v2, p0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method private b()V
    .registers 6

    .prologue
    const/16 v4, 0xe

    .line 138
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    if-nez v0, :cond_b

    .line 139
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->a:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 159
    :goto_a
    return-void

    .line 140
    :cond_b
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    invoke-virtual {v1}, LO/z;->k()I

    move-result v1

    if-ge v0, v1, :cond_36

    .line 142
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    invoke-virtual {v0, v1}, LO/z;->a(I)LO/N;

    move-result-object v0

    invoke-virtual {v0}, LO/N;->a()Lo/T;

    move-result-object v0

    .line 144
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Lo/T;)I

    move-result v1

    invoke-static {v0, v4, v1}, Lcom/google/android/maps/driveabout/app/bP;->a(Lo/T;II)Lcom/google/android/maps/driveabout/app/bP;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 146
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->l:I

    goto :goto_a

    .line 147
    :cond_36
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/app/dz;->d:Z

    if-eqz v0, :cond_68

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(I)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 150
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->f:Lo/X;

    iget v1, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    invoke-virtual {v0, v1}, Lo/X;->a(I)Lo/T;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->f:Lo/X;

    iget v2, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    invoke-virtual {v1, v2}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    .line 152
    new-instance v2, Lcom/google/android/maps/driveabout/app/bP;

    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/dz;->a(Lo/T;)I

    move-result v3

    invoke-direct {v2, v0, v1, v4, v3}, Lcom/google/android/maps/driveabout/app/bP;-><init>(Lo/T;Lo/T;II)V

    iput-object v2, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 154
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->m:I

    goto :goto_a

    .line 157
    :cond_68
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->a:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    goto :goto_a
.end method


# virtual methods
.method a(Lo/T;)I
    .registers 3
    .parameter

    .prologue
    .line 218
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:I

    invoke-static {v0, p1}, Lcom/google/android/maps/driveabout/app/dz;->a(ILo/T;)I

    move-result v0

    return v0
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 81
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->f:Lo/X;

    .line 82
    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    .line 83
    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    .line 84
    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    .line 85
    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->i:I

    .line 86
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 88
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->k:LR/h;

    invoke-virtual {v0}, LR/h;->e()V

    .line 89
    return-void
.end method

.method public a(LO/N;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 116
    invoke-virtual {p1}, LO/N;->i()I

    move-result v0

    .line 117
    iget v1, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    if-gt v1, v0, :cond_13

    .line 119
    sget-object v1, Lcom/google/android/maps/driveabout/app/dz;->b:Lo/ar;

    iput-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 120
    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    .line 121
    iput p2, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    .line 132
    :cond_10
    :goto_10
    iput p2, p0, Lcom/google/android/maps/driveabout/app/dz;->i:I

    .line 133
    return-void

    .line 122
    :cond_13
    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    if-gt v0, p2, :cond_1e

    .line 124
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 125
    iput p2, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    goto :goto_10

    .line 126
    :cond_1e
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    sget-object v1, Lcom/google/android/maps/driveabout/app/dz;->a:Lo/ar;

    if-ne v0, v1, :cond_10

    iget v0, p0, Lcom/google/android/maps/driveabout/app/dz;->i:I

    if-ge v0, p2, :cond_10

    .line 130
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    goto :goto_10
.end method

.method public a(LO/z;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 94
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    .line 95
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    invoke-virtual {v0}, LO/z;->n()Lo/X;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->f:Lo/X;

    .line 96
    iput v1, p0, Lcom/google/android/maps/driveabout/app/dz;->g:I

    .line 97
    iput v1, p0, Lcom/google/android/maps/driveabout/app/dz;->h:I

    .line 98
    iput v1, p0, Lcom/google/android/maps/driveabout/app/dz;->i:I

    .line 99
    sget-object v0, Lcom/google/android/maps/driveabout/app/dz;->b:Lo/ar;

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    .line 100
    return-void
.end method

.method a(I)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 183
    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dz;->f:Lo/X;

    invoke-virtual {v3}, Lo/X;->b()I

    move-result v3

    if-lt v2, v3, :cond_e

    move v0, v1

    .line 194
    :cond_d
    :goto_d
    return v0

    .line 185
    :cond_e
    iget v2, p0, Lcom/google/android/maps/driveabout/app/dz;->i:I

    if-eq p1, v2, :cond_d

    .line 189
    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/dz;->f:Lo/X;

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    .line 190
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v3

    invoke-virtual {v3}, LR/m;->t()I

    move-result v3

    .line 191
    mul-int/lit16 v3, v3, 0x3e8

    invoke-static {v3, v2}, Lcom/google/android/maps/driveabout/app/dz;->a(ILo/T;)I

    move-result v2

    .line 192
    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v3, v4}, LO/z;->b(I)D

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/dz;->e:LO/z;

    iget v6, p0, Lcom/google/android/maps/driveabout/app/dz;->i:I

    invoke-virtual {v5, v6}, LO/z;->b(I)D

    move-result-wide v5

    sub-double/2addr v3, v5

    .line 194
    int-to-double v5, v2

    cmpg-double v2, v3, v5

    if-lez v2, :cond_d

    move v0, v1

    goto :goto_d
.end method

.method b(I)V
    .registers 3
    .parameter

    .prologue
    .line 211
    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/maps/driveabout/app/dz;->c:I

    .line 212
    return-void
.end method

.method public c()Lo/aq;
    .registers 4

    .prologue
    .line 164
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    sget-object v1, Lcom/google/android/maps/driveabout/app/dz;->a:Lo/ar;

    if-eq v0, v1, :cond_22

    .line 165
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/dz;->j:Lo/ar;

    invoke-interface {v0}, Lo/ar;->c()Lo/aq;

    move-result-object v0

    .line 166
    if-nez v0, :cond_12

    .line 167
    invoke-direct {p0}, Lcom/google/android/maps/driveabout/app/dz;->b()V

    goto :goto_0

    .line 168
    :cond_12
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->k:LR/h;

    invoke-virtual {v1, v0}, LR/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/dz;->k:LR/h;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v2}, LR/h;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 173
    :goto_21
    return-object v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method
