.class public Lcom/google/android/maps/driveabout/vector/aC;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field private final h:Ljava/lang/Boolean;

.field private final i:Ljava/lang/Boolean;

.field private final j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ZIIIIII)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 986
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/aC;->h:Ljava/lang/Boolean;

    .line 987
    iput-object p2, p0, Lcom/google/android/maps/driveabout/vector/aC;->i:Ljava/lang/Boolean;

    .line 988
    iput-object p3, p0, Lcom/google/android/maps/driveabout/vector/aC;->j:Ljava/lang/Boolean;

    .line 989
    iput-boolean p4, p0, Lcom/google/android/maps/driveabout/vector/aC;->a:Z

    .line 990
    iput p5, p0, Lcom/google/android/maps/driveabout/vector/aC;->b:I

    .line 991
    iput p6, p0, Lcom/google/android/maps/driveabout/vector/aC;->c:I

    .line 992
    iput p7, p0, Lcom/google/android/maps/driveabout/vector/aC;->d:I

    .line 993
    iput p8, p0, Lcom/google/android/maps/driveabout/vector/aC;->e:I

    .line 994
    iput p9, p0, Lcom/google/android/maps/driveabout/vector/aC;->f:I

    .line 995
    iput p10, p0, Lcom/google/android/maps/driveabout/vector/aC;->g:I

    .line 996
    return-void
.end method

.method public static a()Lcom/google/android/maps/driveabout/vector/aD;
    .registers 1

    .prologue
    .line 1012
    new-instance v0, Lcom/google/android/maps/driveabout/vector/aD;

    invoke-direct {v0}, Lcom/google/android/maps/driveabout/vector/aD;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(ZZZ)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 999
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v1, p1, :cond_e

    .line 1008
    :cond_d
    :goto_d
    return v0

    .line 1002
    :cond_e
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p2, :cond_d

    .line 1005
    :cond_1a
    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/google/android/maps/driveabout/vector/aC;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-ne v1, p3, :cond_d

    .line 1008
    :cond_26
    const/4 v0, 0x1

    goto :goto_d
.end method
