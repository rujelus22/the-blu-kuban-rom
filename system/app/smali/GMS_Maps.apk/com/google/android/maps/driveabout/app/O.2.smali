.class Lcom/google/android/maps/driveabout/app/O;
.super Lcom/google/android/maps/driveabout/app/K;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/maps/driveabout/app/DestinationActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/android/maps/driveabout/app/O;->c:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/app/K;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/r;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/O;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 7

    .prologue
    .line 428
    invoke-super {p0}, Lcom/google/android/maps/driveabout/app/K;->a()V

    .line 431
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/O;->c:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    const v1, 0x7f1000de

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 432
    if-nez v0, :cond_11

    .line 444
    :goto_10
    return-void

    .line 435
    :cond_11
    new-instance v1, Lcom/google/android/maps/driveabout/widgets/a;

    iget-object v2, p0, Lcom/google/android/maps/driveabout/app/O;->c:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    iget-object v3, p0, Lcom/google/android/maps/driveabout/app/O;->c:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v3}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->g(Lcom/google/android/maps/driveabout/app/DestinationActivity;)Lcom/google/android/maps/driveabout/app/af;

    move-result-object v3

    new-instance v4, Lcom/google/android/maps/driveabout/app/M;

    iget-object v5, p0, Lcom/google/android/maps/driveabout/app/O;->c:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-direct {v4, v5}, Lcom/google/android/maps/driveabout/app/M;-><init>(Lcom/google/android/maps/driveabout/app/DestinationActivity;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/maps/driveabout/widgets/a;-><init>(Landroid/content/Context;Lcom/google/android/maps/driveabout/app/af;Lcom/google/android/maps/driveabout/app/M;)V

    .line 437
    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/x;)V

    .line 438
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 439
    const/16 v2, 0x10

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 443
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/O;->c:Lcom/google/android/maps/driveabout/app/DestinationActivity;

    invoke-static {v0, v1}, Lcom/google/android/maps/driveabout/app/DestinationActivity;->a(Lcom/google/android/maps/driveabout/app/DestinationActivity;Lcom/google/android/maps/driveabout/app/bR;)Lcom/google/android/maps/driveabout/app/bR;

    goto :goto_10
.end method

.method public a(Landroid/view/Menu;)V
    .registers 4
    .parameter

    .prologue
    .line 449
    const v0, 0x7f1004a0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 453
    const v0, 0x7f10049e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 454
    return-void
.end method
