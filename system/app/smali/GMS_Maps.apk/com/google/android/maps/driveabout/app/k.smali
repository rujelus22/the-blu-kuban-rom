.class public abstract Lcom/google/android/maps/driveabout/app/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:LC/a;

.field protected b:I

.field protected c:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(LC/b;F)LC/b;
.end method

.method public a(LC/b;FIIF)LC/b;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 114
    float-to-double v0, p2

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_8

    .line 125
    :cond_7
    :goto_7
    return-object p1

    .line 117
    :cond_8
    invoke-virtual {p0, p1, p3, p4, p5}, Lcom/google/android/maps/driveabout/app/k;->a(LC/b;IIF)V

    .line 118
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 119
    iget-object v1, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    const/high16 v2, 0x4000

    mul-float/2addr v2, p2

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, LC/a;->c(FF)F

    move-result v2

    .line 120
    invoke-virtual {p1}, LC/b;->a()F

    move-result v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    .line 121
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v1

    invoke-virtual {p1}, LC/b;->d()F

    move-result v3

    invoke-virtual {p1}, LC/b;->e()F

    move-result v4

    invoke-virtual {p1}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    move-object p1, v0

    goto :goto_7
.end method

.method public abstract a(LC/b;LO/N;Z)LC/b;
.end method

.method public abstract a(LC/b;LaH/h;IIF)LC/b;
.end method

.method public abstract a(LC/b;LaH/h;Lo/S;LO/N;FIIF)LC/b;
.end method

.method public abstract a(LC/b;LaH/h;Lo/am;IIF)LC/b;
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 151
    iput p1, p0, Lcom/google/android/maps/driveabout/app/k;->b:I

    .line 152
    return-void
.end method

.method protected a(LC/b;IIF)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    if-nez v0, :cond_c

    .line 134
    new-instance v0, LC/a;

    invoke-direct {v0, p1, p2, p3, p4}, LC/a;-><init>(LC/b;IIF)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    .line 143
    :goto_b
    return-void

    .line 136
    :cond_c
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    invoke-virtual {v0}, LC/a;->k()I

    move-result v0

    if-ne p2, v0, :cond_26

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    invoke-virtual {v0}, LC/a;->l()I

    move-result v0

    if-ne p3, v0, :cond_26

    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    invoke-virtual {v0}, LC/a;->m()F

    move-result v0

    cmpl-float v0, p4, v0

    if-eqz v0, :cond_2b

    .line 139
    :cond_26
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    invoke-virtual {v0, p2, p3, p4}, LC/a;->a(IIF)V

    .line 141
    :cond_2b
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/k;->a:LC/a;

    invoke-virtual {v0, p1}, LC/a;->a(LC/b;)V

    goto :goto_b
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 160
    iput p1, p0, Lcom/google/android/maps/driveabout/app/k;->c:I

    .line 161
    return-void
.end method
