.class Lcom/google/android/maps/driveabout/vector/dx;
.super Lcom/google/android/maps/driveabout/vector/di;
.source "SourceFile"


# instance fields
.field private final A:Z


# direct methods
.method private constructor <init>(Lcom/google/android/maps/driveabout/vector/dy;)V
    .registers 3
    .parameter

    .prologue
    .line 587
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/maps/driveabout/vector/di;-><init>(Lcom/google/android/maps/driveabout/vector/dm;Lcom/google/android/maps/driveabout/vector/dj;)V

    .line 589
    invoke-static {p1}, Lcom/google/android/maps/driveabout/vector/dy;->a(Lcom/google/android/maps/driveabout/vector/dy;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/dx;->A:Z

    .line 590
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/maps/driveabout/vector/dy;Lcom/google/android/maps/driveabout/vector/dj;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 531
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/vector/dx;-><init>(Lcom/google/android/maps/driveabout/vector/dy;)V

    return-void
.end method


# virtual methods
.method a()I
    .registers 2

    .prologue
    .line 583
    const/16 v0, 0x1000

    return v0
.end method

.method public a(Lad/p;Landroid/content/Context;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZZ)Ls/aH;
    .registers 18
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 547
    sget-object v0, Lcom/google/android/maps/driveabout/vector/dx;->o:Lcom/google/android/maps/driveabout/vector/di;

    if-ne p0, v0, :cond_10

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->t()Z

    move-result v0

    if-nez v0, :cond_10

    .line 548
    const/4 v0, 0x0

    .line 573
    :cond_f
    :goto_f
    return-object v0

    .line 550
    :cond_10
    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/di;->b(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-virtual {p3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 551
    :goto_1c
    const/16 v0, 0x100

    invoke-static {p3, v0}, Lcom/google/android/maps/driveabout/vector/dd;->a(Landroid/content/res/Resources;I)I

    move-result v3

    .line 553
    if-eqz p6, :cond_41

    invoke-static {p0}, Lcom/google/android/maps/driveabout/vector/di;->c(Lcom/google/android/maps/driveabout/vector/di;)Z

    move-result v0

    if-eqz v0, :cond_41

    const/4 v6, 0x1

    .line 555
    :goto_2b
    const/4 v9, 0x0

    check-cast v9, Ls/t;

    .line 557
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/di;->x:Z

    if-eqz v0, :cond_43

    .line 558
    new-instance v0, Ls/aK;

    sget-object v8, Ls/aK;->h:Ls/aO;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v9}, Ls/aK;-><init>(Lad/p;Lcom/google/android/maps/driveabout/vector/di;IFLjava/util/Locale;ZLjava/io/File;Ls/aO;Ls/t;)V

    goto :goto_f

    .line 550
    :cond_3e
    const/high16 v4, 0x3f80

    goto :goto_1c

    .line 553
    :cond_41
    const/4 v6, 0x0

    goto :goto_2b

    .line 562
    :cond_43
    new-instance v0, Ls/aP;

    move-object v1, p1

    move-object v2, p0

    move-object v5, p4

    move-object v7, p5

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Ls/aP;-><init>(Lad/p;Lcom/google/android/maps/driveabout/vector/di;IFLjava/util/Locale;ZLjava/io/File;Ls/t;)V

    .line 565
    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->g:Lcom/google/android/maps/driveabout/vector/di;

    if-eq p0, v1, :cond_55

    sget-object v1, Lcom/google/android/maps/driveabout/vector/di;->h:Lcom/google/android/maps/driveabout/vector/di;

    if-ne p0, v1, :cond_5b

    .line 566
    :cond_55
    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v1, v2}, Ls/aP;->a(J)V

    .line 569
    :cond_5b
    iget-boolean v1, p0, Lcom/google/android/maps/driveabout/vector/dx;->A:Z

    if-eqz v1, :cond_f

    if-eqz p7, :cond_f

    .line 570
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/googlenav/bE;->a(B)Lcom/google/googlenav/bE;

    move-result-object v1

    invoke-virtual {v0, v1}, Ls/aP;->a(Lcom/google/googlenav/bE;)V

    goto :goto_f
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 531
    check-cast p1, Lcom/google/android/maps/driveabout/vector/di;

    invoke-super {p0, p1}, Lcom/google/android/maps/driveabout/vector/di;->a(Lcom/google/android/maps/driveabout/vector/di;)I

    move-result v0

    return v0
.end method

.method public l()Ls/aB;
    .registers 2

    .prologue
    .line 578
    new-instance v0, Lcom/google/android/maps/driveabout/vector/dz;

    invoke-direct {v0, p0}, Lcom/google/android/maps/driveabout/vector/dz;-><init>(Lcom/google/android/maps/driveabout/vector/di;)V

    return-object v0
.end method
