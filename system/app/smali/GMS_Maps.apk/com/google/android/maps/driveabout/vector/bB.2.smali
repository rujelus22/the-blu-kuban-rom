.class public Lcom/google/android/maps/driveabout/vector/bB;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Law/p;

.field private b:Lo/T;

.field private c:I

.field private final d:Lo/T;

.field private e:Lcom/google/android/maps/driveabout/vector/bC;

.field private f:Lcom/google/android/maps/driveabout/vector/bD;


# direct methods
.method public constructor <init>(Law/p;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lo/T;

    invoke-direct {v0, v1, v1}, Lo/T;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->b:Lo/T;

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->c:I

    .line 50
    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->d:Lo/T;

    .line 53
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bB;->e:Lcom/google/android/maps/driveabout/vector/bC;

    .line 57
    iput-object v2, p0, Lcom/google/android/maps/driveabout/vector/bB;->f:Lcom/google/android/maps/driveabout/vector/bD;

    .line 60
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bB;->a:Law/p;

    .line 61
    return-void
.end method


# virtual methods
.method public a(LC/a;Lo/T;)Lo/T;
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 160
    const/4 v0, 0x0

    .line 162
    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v1

    .line 164
    invoke-virtual {v1}, Lo/T;->f()I

    move-result v2

    invoke-virtual {p2}, Lo/T;->f()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1}, LC/a;->y()F

    move-result v4

    div-float/2addr v2, v4

    float-to-int v5, v2

    .line 166
    invoke-virtual {v1}, Lo/T;->g()I

    move-result v2

    invoke-virtual {p2}, Lo/T;->g()I

    move-result v4

    sub-int/2addr v2, v4

    int-to-float v2, v2

    invoke-virtual {p1}, LC/a;->y()F

    move-result v4

    div-float/2addr v2, v4

    float-to-int v6, v2

    .line 170
    invoke-virtual {p1}, LC/a;->k()I

    move-result v4

    .line 171
    invoke-virtual {p1}, LC/a;->l()I

    move-result v2

    .line 172
    div-int/lit8 v7, v4, 0x2

    .line 173
    div-int/lit8 v8, v2, 0x2

    .line 175
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-gt v9, v4, :cond_3e

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v9

    if-le v9, v2, :cond_40

    :cond_3e
    move-object v0, v1

    .line 210
    :cond_3f
    :goto_3f
    return-object v0

    .line 186
    :cond_40
    neg-int v1, v7

    if-ge v5, v1, :cond_67

    .line 187
    neg-int v1, v4

    move v4, v1

    .line 193
    :cond_45
    :goto_45
    neg-int v1, v8

    if-ge v6, v1, :cond_6b

    .line 194
    neg-int v1, v2

    .line 199
    :goto_49
    if-nez v1, :cond_4d

    if-eqz v4, :cond_3f

    .line 203
    :cond_4d
    int-to-float v0, v4

    invoke-virtual {p1}, LC/a;->y()F

    move-result v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 204
    int-to-float v1, v1

    invoke-virtual {p1}, LC/a;->y()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 205
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bB;->d:Lo/T;

    invoke-virtual {v2, v0, v1}, Lo/T;->d(II)V

    .line 206
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->d:Lo/T;

    invoke-virtual {p2, v0}, Lo/T;->e(Lo/T;)Lo/T;

    move-result-object v0

    goto :goto_3f

    .line 188
    :cond_67
    if-gt v5, v7, :cond_45

    move v4, v3

    goto :goto_45

    .line 195
    :cond_6b
    if-le v6, v8, :cond_6f

    move v1, v2

    .line 196
    goto :goto_49

    :cond_6f
    move v1, v3

    goto :goto_49
.end method

.method public a(LC/a;)V
    .registers 7
    .parameter

    .prologue
    .line 131
    invoke-virtual {p1}, LC/a;->h()Lo/T;

    move-result-object v0

    .line 132
    const/4 v1, 0x0

    const/16 v2, 0x1e

    invoke-virtual {p1}, LC/a;->r()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 133
    iget v2, p0, Lcom/google/android/maps/driveabout/vector/bB;->c:I

    if-eq v1, v2, :cond_27

    .line 136
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v2

    invoke-virtual {v2}, Lo/aQ;->a()Lo/aR;

    move-result-object v2

    .line 137
    invoke-virtual {p0, v0, v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bB;->a(Lo/T;Lo/T;ILo/aR;)V

    .line 151
    :cond_26
    :goto_26
    return-void

    .line 138
    :cond_27
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bB;->b:Lo/T;

    invoke-virtual {v2, v0}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 139
    iget-object v2, p0, Lcom/google/android/maps/driveabout/vector/bB;->b:Lo/T;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/maps/driveabout/vector/bB;->a(LC/a;Lo/T;)Lo/T;

    move-result-object v2

    .line 143
    if-eqz v2, :cond_26

    iget-object v3, p0, Lcom/google/android/maps/driveabout/vector/bB;->b:Lo/T;

    if-eqz v3, :cond_49

    invoke-virtual {v0, v2}, Lo/T;->d(Lo/T;)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/maps/driveabout/vector/bB;->b:Lo/T;

    invoke-virtual {v0, v4}, Lo/T;->d(Lo/T;)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_26

    .line 147
    :cond_49
    invoke-virtual {p1}, LC/a;->B()Lo/aQ;

    move-result-object v3

    invoke-virtual {v3}, Lo/aQ;->a()Lo/aR;

    move-result-object v3

    .line 148
    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/maps/driveabout/vector/bB;->a(Lo/T;Lo/T;ILo/aR;)V

    goto :goto_26
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bC;)V
    .registers 2
    .parameter

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bB;->e:Lcom/google/android/maps/driveabout/vector/bC;

    .line 220
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/bD;)V
    .registers 2
    .parameter

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bB;->f:Lcom/google/android/maps/driveabout/vector/bD;

    .line 229
    return-void
.end method

.method a(Lo/T;Lo/T;ILo/aR;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/bB;->b:Lo/T;

    .line 74
    iput p3, p0, Lcom/google/android/maps/driveabout/vector/bB;->c:I

    .line 76
    invoke-virtual {p1}, Lo/T;->a()I

    move-result v1

    .line 77
    invoke-virtual {p1}, Lo/T;->c()I

    move-result v2

    .line 80
    invoke-virtual {p4}, Lo/aR;->f()I

    move-result v0

    int-to-double v3, v0

    int-to-double v5, v1

    const-wide v7, 0x3eb0c6f7a0b5ed8dL

    mul-double/2addr v5, v7

    const-wide v7, 0x3f91df46a2529d39L

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    const-wide v5, 0x3fd5752a00000000L

    mul-double/2addr v3, v5

    double-to-int v3, v3

    .line 82
    invoke-virtual {p4}, Lo/aR;->e()I

    move-result v0

    int-to-double v4, v0

    const-wide v6, 0x3fd5752a00000000L

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 87
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->f:Lcom/google/android/maps/driveabout/vector/bD;

    if-eqz v0, :cond_42

    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->f:Lcom/google/android/maps/driveabout/vector/bD;

    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/bD;->a()Z

    move-result v0

    if-eqz v0, :cond_88

    :cond_42
    const/4 v0, 0x1

    .line 92
    :goto_43
    const/4 v5, 0x1

    .line 94
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 95
    new-instance v7, Ljava/io/DataOutputStream;

    invoke-direct {v7, v6}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 97
    :try_start_4e
    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 98
    invoke-virtual {v7, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 99
    invoke-virtual {p2}, Lo/T;->a()I

    move-result v1

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 100
    invoke-virtual {p2}, Lo/T;->c()I

    move-result v1

    invoke-virtual {v7, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 101
    invoke-virtual {v7, p3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 102
    invoke-virtual {v7, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 103
    invoke-virtual {v7, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 104
    invoke-virtual {v7, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 105
    invoke-virtual {v7, v5}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 108
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->a:Law/p;

    const/4 v1, 0x7

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-interface/range {v0 .. v5}, Law/p;->a(I[BZZZ)V

    .line 116
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->e:Lcom/google/android/maps/driveabout/vector/bC;

    if-eqz v0, :cond_87

    .line 117
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/bB;->e:Lcom/google/android/maps/driveabout/vector/bC;

    invoke-interface {v0, p1, p3}, Lcom/google/android/maps/driveabout/vector/bC;->a(Lo/T;I)V
    :try_end_87
    .catch Ljava/io/IOException; {:try_start_4e .. :try_end_87} :catch_8a

    .line 123
    :cond_87
    :goto_87
    return-void

    .line 87
    :cond_88
    const/4 v0, 0x0

    goto :goto_43

    .line 119
    :catch_8a
    move-exception v0

    .line 121
    const-string v1, "view point"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_87
.end method
