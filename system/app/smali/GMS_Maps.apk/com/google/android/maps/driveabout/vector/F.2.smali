.class public Lcom/google/android/maps/driveabout/vector/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private volatile b:Z

.field private volatile c:Lcom/google/android/maps/driveabout/vector/h;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/f;->a:Landroid/view/View;

    .line 47
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/maps/driveabout/vector/g;
    .registers 2

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 61
    return-void
.end method

.method public a(Lcom/google/android/maps/driveabout/vector/h;)V
    .registers 2
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/maps/driveabout/vector/f;->c:Lcom/google/android/maps/driveabout/vector/h;

    .line 110
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/f;->b:Z

    .line 83
    return-void
.end method

.method public b(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 68
    return-void
.end method

.method public c(FF)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/f;->b:Z

    .line 76
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/maps/driveabout/vector/f;->b:Z

    return v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/f;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 94
    return-void
.end method

.method public d(FF)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 102
    return-void
.end method

.method public e()Landroid/view/View;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/f;->a:Landroid/view/View;

    return-object v0
.end method

.method public f()V
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/maps/driveabout/vector/f;->c:Lcom/google/android/maps/driveabout/vector/h;

    .line 114
    if-eqz v0, :cond_7

    .line 115
    invoke-interface {v0}, Lcom/google/android/maps/driveabout/vector/h;->a()V

    .line 117
    :cond_7
    return-void
.end method
