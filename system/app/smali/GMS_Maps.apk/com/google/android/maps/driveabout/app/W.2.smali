.class Lcom/google/android/maps/driveabout/app/W;
.super Lcom/google/android/maps/driveabout/app/T;
.source "SourceFile"


# static fields
.field static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 949
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "data1"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/maps/driveabout/app/W;->e:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 956
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/T;-><init>(Landroid/content/Context;)V

    .line 957
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 958
    if-eqz p2, :cond_14

    .line 959
    const v1, 0x7f0d00cb

    invoke-static {v1}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 961
    :cond_14
    invoke-static {}, Lcom/google/android/maps/driveabout/app/Y;->a()Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 962
    invoke-virtual {p0, v0}, Lcom/google/android/maps/driveabout/app/W;->a(Ljava/util/ArrayList;)V

    .line 964
    new-instance v0, Lcom/google/android/maps/driveabout/app/X;

    invoke-direct {v0, p0, p2}, Lcom/google/android/maps/driveabout/app/X;-><init>(Lcom/google/android/maps/driveabout/app/W;Z)V

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/X;->start()V

    .line 970
    return-void
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/app/W;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 928
    invoke-direct {p0, p1}, Lcom/google/android/maps/driveabout/app/W;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 973
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 974
    if-eqz p1, :cond_14

    .line 975
    const v0, 0x7f0d00cb

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/Y;->a(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 979
    :cond_14
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 982
    iget-object v0, p0, Lcom/google/android/maps/driveabout/app/W;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 983
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/maps/driveabout/app/W;->e:[Ljava/lang/String;

    const-string v3, "mimetype=\'vnd.android.cursor.item/postal-address_v2\' AND in_visible_group=1"

    const-string v5, "sort_key, is_super_primary desc, is_primary desc, data1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 989
    if-eqz v0, :cond_55

    .line 990
    :cond_2d
    :goto_2d
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_52

    .line 991
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 992
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 993
    if-eqz v1, :cond_2d

    if-eqz v2, :cond_2d

    .line 994
    new-instance v3, LO/U;

    invoke-direct {v3, v2, v4, v1, v4}, LO/U;-><init>(Ljava/lang/String;Lo/u;Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    invoke-virtual {v8, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 996
    invoke-static {v3, v9}, Lcom/google/android/maps/driveabout/app/Y;->a(LO/U;I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2d

    .line 1000
    :cond_52
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1005
    :cond_55
    if-eqz p1, :cond_6c

    move v0, v6

    .line 1006
    :goto_58
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_68

    .line 1007
    const v0, 0x7f0d00d9

    invoke-static {v0}, Lcom/google/android/maps/driveabout/app/Y;->c(I)Lcom/google/android/maps/driveabout/app/Y;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1009
    :cond_68
    invoke-virtual {p0, v7}, Lcom/google/android/maps/driveabout/app/W;->b(Ljava/util/ArrayList;)V

    .line 1010
    return-void

    .line 1005
    :cond_6c
    const/4 v0, 0x0

    goto :goto_58
.end method
