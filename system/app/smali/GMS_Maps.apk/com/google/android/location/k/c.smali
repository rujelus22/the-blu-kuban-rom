.class public final Lcom/google/android/location/k/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(II)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 76
    if-lez p0, :cond_6

    .line 77
    int-to-float v0, p1

    int-to-float v1, p0

    div-float/2addr v0, v1

    .line 79
    :goto_5
    return v0

    :cond_6
    const/high16 v0, -0x4080

    goto :goto_5
.end method

.method public static a(IIII)J
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 135
    int-to-long v0, p0

    const-wide/32 v2, 0x36ee80

    mul-long/2addr v0, v2

    int-to-long v2, p1

    const-wide/32 v4, 0xea60

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    int-to-long v2, p2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    int-to-long v2, p3

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(J)J
    .registers 4
    .parameter

    .prologue
    .line 151
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 152
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 153
    invoke-static {v0}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Ljava/util/Calendar;)J
    .registers 5
    .parameter

    .prologue
    .line 143
    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/16 v2, 0xd

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/16 v3, 0xe

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/location/k/c;->a(IIII)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 229
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    .line 230
    invoke-static {p0, v0}, Lcom/google/android/location/k/c;->a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    .line 231
    return-object v0
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 63
    :goto_0
    if-eqz p0, :cond_d

    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-lez v0, :cond_d

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->remove(II)V

    goto :goto_0

    .line 66
    :cond_d
    return-void
.end method

.method public static a(Ljava/io/Closeable;)V
    .registers 2
    .parameter

    .prologue
    .line 186
    if-eqz p0, :cond_5

    .line 188
    :try_start_2
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    .line 193
    :cond_5
    :goto_5
    return-void

    .line 189
    :catch_6
    move-exception v0

    goto :goto_5
.end method

.method public static a(Ljava/io/InputStream;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 241
    :try_start_0
    invoke-virtual {p1, p0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    .line 245
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->toByteArray()[B

    .line 248
    invoke-virtual {p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->isValid()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 249
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Missing required field or has more than one value for no repeated field."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_14} :catch_14
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_14} :catch_16

    .line 252
    :catch_14
    move-exception v0

    .line 253
    throw v0

    .line 254
    :catch_16
    move-exception v0

    .line 257
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Runtime exception while parsing."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_1f
    return-void
.end method

.method public static a(Ljava/util/Calendar;J)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const-wide/32 v3, 0x36ee80

    const-wide/32 v7, 0xea60

    const-wide/16 v5, 0x3e8

    .line 160
    const/16 v0, 0xb

    div-long v1, p1, v3

    long-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 161
    rem-long v0, p1, v3

    .line 162
    const/16 v2, 0xc

    div-long v3, v0, v7

    long-to-int v3, v3

    invoke-virtual {p0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 163
    rem-long/2addr v0, v7

    .line 164
    const/16 v2, 0xd

    div-long v3, v0, v5

    long-to-int v3, v3

    invoke-virtual {p0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 165
    rem-long/2addr v0, v5

    .line 166
    const/16 v2, 0xe

    long-to-int v0, v0

    invoke-virtual {p0, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 167
    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 37
    if-eqz p0, :cond_a

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-nez v1, :cond_a

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public static a(Ljava/io/File;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_d

    .line 90
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 101
    :cond_c
    :goto_c
    return v1

    .line 95
    :cond_d
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_14
    if-ge v3, v5, :cond_26

    aget-object v6, v4, v3

    .line 96
    if-eqz v0, :cond_24

    invoke-static {v6}, Lcom/google/android/location/k/c;->a(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_24

    move v0, v1

    .line 95
    :goto_21
    add-int/lit8 v3, v3, 0x1

    goto :goto_14

    :cond_24
    move v0, v2

    .line 96
    goto :goto_21

    .line 100
    :cond_26
    if-eqz v0, :cond_2e

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_c

    :cond_2e
    move v1, v2

    goto :goto_c
.end method

.method public static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 126
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_22

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_22

    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_22

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method public static a([BI)[B
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 214
    if-gez p1, :cond_8

    .line 215
    new-instance v0, Ljava/lang/NegativeArraySizeException;

    invoke-direct {v0}, Ljava/lang/NegativeArraySizeException;-><init>()V

    throw v0

    .line 217
    :cond_8
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/location/k/c;->a([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static a([BII)[B
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 199
    if-le p1, p2, :cond_8

    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 202
    :cond_8
    array-length v0, p0

    .line 203
    if-ltz p1, :cond_d

    if-le p1, v0, :cond_13

    .line 204
    :cond_d
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 206
    :cond_13
    sub-int v1, p2, p1

    .line 207
    sub-int/2addr v0, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 208
    new-array v1, v1, [B

    .line 209
    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    return-object v1
.end method

.method public static b(J)Ljava/lang/String;
    .registers 13
    .parameter

    .prologue
    const-wide/32 v2, 0x36ee80

    const-wide/32 v6, 0xea60

    const-wide/16 v8, 0x3e8

    .line 173
    div-long v0, p0, v2

    .line 174
    rem-long v2, p0, v2

    .line 175
    div-long v4, v2, v6

    .line 176
    rem-long/2addr v2, v6

    .line 177
    div-long v6, v2, v8

    .line 178
    rem-long/2addr v2, v8

    .line 179
    const-string v8, "%02d:%02d:%02d.%03d"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v9, v10

    const/4 v0, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v9, v0

    const/4 v0, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v9, v0

    const/4 v0, 0x3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v9, v0

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/util/List;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/googlenav/common/io/protocol/ProtoBuf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    if-nez p0, :cond_4

    .line 113
    const/4 v0, 0x0

    .line 119
    :goto_3
    return-object v0

    .line 115
    :cond_4
    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 116
    const/4 v0, 0x0

    :goto_9
    invoke-virtual {p0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    if-ge v0, v2, :cond_19

    .line 117
    invoke-virtual {p0, p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_19
    move-object v0, v1

    .line 119
    goto :goto_3
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46
    invoke-static {p0}, Lcom/google/android/location/k/c;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 56
    :cond_9
    :goto_9
    return v0

    .line 49
    :cond_a
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 52
    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    .line 53
    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 56
    goto :goto_9
.end method
