.class public Lcom/google/android/location/u;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/location/u$a;
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/location/k/b;

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/location/u$a;

.field private final d:J


# direct methods
.method public constructor <init>(Lcom/google/android/location/k/b;Ljava/util/List;Lcom/google/android/location/u$a;ZLjava/util/Calendar;Ljava/util/Calendar;J)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/location/k/b;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;",
            "Lcom/google/android/location/u$a;",
            "Z",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k/b;

    .line 86
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/location/u;->d:J

    .line 87
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/aw;

    move-result-object v6

    .line 88
    const/4 v4, 0x0

    .line 89
    if-eqz p2, :cond_51

    .line 90
    sget-object v2, Lcom/google/android/location/k/b;->c:Ljava/util/Comparator;

    invoke-static {p2, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 91
    const/4 v3, 0x0

    .line 92
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_51

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/k/b;

    .line 93
    invoke-virtual {v2, p1}, Lcom/google/android/location/k/b;->a(Lcom/google/android/location/k/b;)Lcom/google/android/location/k/b;

    move-result-object v5

    .line 94
    if-eqz v5, :cond_85

    .line 96
    if-eqz v3, :cond_85

    iget-wide v8, v5, Lcom/google/android/location/k/b;->a:J

    iget-wide v10, v3, Lcom/google/android/location/k/b;->b:J

    cmp-long v2, v8, v10

    if-gez v2, :cond_85

    .line 97
    iget-wide v8, v3, Lcom/google/android/location/k/b;->b:J

    .line 98
    iget-wide v10, v5, Lcom/google/android/location/k/b;->b:J

    cmp-long v2, v8, v10

    if-gez v2, :cond_4f

    .line 99
    new-instance v2, Lcom/google/android/location/k/b;

    iget-wide v10, v5, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v2, v8, v9, v10, v11}, Lcom/google/android/location/k/b;-><init>(JJ)V

    .line 105
    :goto_45
    if-eqz v2, :cond_82

    .line 106
    invoke-virtual {v6, v2}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    .line 107
    add-int/lit8 v3, v4, 0x1

    :goto_4c
    move v4, v3

    move-object v3, v2

    .line 110
    goto :goto_1a

    .line 101
    :cond_4f
    const/4 v2, 0x0

    goto :goto_45

    .line 112
    :cond_51
    if-nez v4, :cond_56

    .line 113
    invoke-virtual {v6, p1}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    .line 116
    :cond_56
    invoke-virtual {v6}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    .line 117
    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_70

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/location/k/b;

    invoke-virtual {v2, p1}, Lcom/google/android/location/k/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_70

    sget-object p3, Lcom/google/android/location/u$a;->a:Lcom/google/android/location/u$a;

    :cond_70
    iput-object p3, p0, Lcom/google/android/location/u;->c:Lcom/google/android/location/u$a;

    .line 122
    if-eqz p4, :cond_7f

    .line 123
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/location/u;->a(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    .line 128
    :goto_7e
    return-void

    .line 126
    :cond_7f
    iput-object v3, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    goto :goto_7e

    :cond_82
    move-object v2, v3

    move v3, v4

    goto :goto_4c

    :cond_85
    move-object v2, v5

    goto :goto_45
.end method

.method private a(JJLjava/util/List;)J
    .registers 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 226
    .line 227
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 228
    invoke-virtual {v0, p1, p2}, Lcom/google/android/location/k/b;->d(J)J

    move-result-wide v2

    .line 229
    cmp-long v4, v2, p3

    if-gtz v4, :cond_1a

    .line 230
    sub-long/2addr p3, v2

    goto :goto_4

    .line 232
    :cond_1a
    iget-wide v0, v0, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    add-long/2addr v0, p3

    .line 235
    :goto_21
    return-wide v0

    :cond_22
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_21
.end method

.method private a(Ljava/util/Calendar;JLjava/util/List;)J
    .registers 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 210
    iget-object v1, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k/b;

    iget-wide v1, v1, Lcom/google/android/location/k/b;->a:J

    invoke-static {v0, v1, v2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;J)V

    .line 211
    new-instance v1, Ljava/util/Random;

    iget-wide v2, p0, Lcom/google/android/location/u;->d:J

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    xor-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 212
    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    long-to-double v2, p2

    mul-double/2addr v0, v2

    double-to-long v3, v0

    .line 216
    iget-object v0, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k/b;

    iget-wide v1, v0, Lcom/google/android/location/k/b;->a:J

    move-object v0, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/location/u;->a(JJLjava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/util/List;)J
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 239
    const-wide/16 v0, 0x0

    .line 240
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 241
    invoke-virtual {v0}, Lcom/google/android/location/k/b;->a()J

    move-result-wide v4

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_7

    .line 243
    :cond_1b
    return-wide v1
.end method

.method private a(Ljava/util/Calendar;Ljava/util/Calendar;Ljava/util/List;)Ljava/util/List;
    .registers 12
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/location/k/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p3}, Lcom/google/android/location/u;->a(Ljava/util/List;)J

    move-result-wide v0

    .line 133
    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    .line 134
    invoke-direct {p0, p1, v0, v1, p3}, Lcom/google/android/location/u;->a(Ljava/util/Calendar;JLjava/util/List;)J

    move-result-wide v2

    .line 138
    if-eqz p2, :cond_58

    iget-object v0, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k/b;

    invoke-virtual {v0, p2}, Lcom/google/android/location/k/b;->c(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-static {p1, p2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 140
    invoke-static {p2}, Lcom/google/android/location/k/c;->a(Ljava/util/Calendar;)J

    move-result-wide v0

    .line 141
    cmp-long v4, v0, v2

    if-gez v4, :cond_58

    move-wide v1, v0

    .line 149
    :goto_24
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->builder()Lcom/google/common/collect/aw;

    move-result-object v3

    .line 150
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2c
    :goto_2c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 151
    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/k/b;->b(J)Z

    move-result v5

    if-nez v5, :cond_2c

    .line 152
    invoke-virtual {v0, v1, v2}, Lcom/google/android/location/k/b;->c(J)Z

    move-result v5

    if-eqz v5, :cond_4f

    .line 153
    new-instance v5, Lcom/google/android/location/k/b;

    iget-wide v6, v0, Lcom/google/android/location/k/b;->b:J

    invoke-direct {v5, v1, v2, v6, v7}, Lcom/google/android/location/k/b;-><init>(JJ)V

    invoke-virtual {v3, v5}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    goto :goto_2c

    .line 155
    :cond_4f
    invoke-virtual {v3, v0}, Lcom/google/common/collect/aw;->b(Ljava/lang/Object;)Lcom/google/common/collect/aw;

    goto :goto_2c

    .line 159
    :cond_53
    invoke-virtual {v3}, Lcom/google/common/collect/aw;->a()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0

    :cond_58
    move-wide v1, v2

    goto :goto_24
.end method


# virtual methods
.method public a(Ljava/util/Calendar;)Lcom/google/android/location/k/b;
    .registers 5
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/k/b;

    .line 180
    invoke-virtual {v0, p1}, Lcom/google/android/location/k/b;->b(Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 184
    :goto_18
    return-object v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method a()Lcom/google/android/location/u$a;
    .registers 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/location/u;->c:Lcom/google/android/location/u$a;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SensorCollectionTimeSpan [targetTimeSpan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/u;->a:Lcom/google/android/location/k/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTimeSpans="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/u;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subTimeSpanType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/location/u;->c:Lcom/google/android/location/u$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
